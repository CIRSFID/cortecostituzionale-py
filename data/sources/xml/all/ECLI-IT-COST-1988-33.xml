<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>33</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:33</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/01/1988</data_decisione>
    <data_deposito>19/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 42, settimo    &#13;
 comma, del  d.l.  13  maggio  1976,  n.  227,  ("Provvidenze  per  le    &#13;
 popolazioni  dei  comuni  della regione Friuli-Venezia Giulia colpiti    &#13;
 dal terremoto del maggio 1976")  convertito  nella  legge  29  maggio    &#13;
 1976,   n.   336  ("Conversione  in  legge,  con  modificazioni,  del    &#13;
 decreto-legge 13 maggio 1976, n. 227, concernente provvidenze per  le    &#13;
 popolazioni  dei  comuni  della Regione Friuli-Venezia Giulia colpiti    &#13;
 dal terremoto del maggio 1976"), promosso con ordinanza emessa il  24    &#13;
 novembre  1983  dal  T.A.R. del Friuli-Venezia Giulia, iscritta al n.    &#13;
 581 del registro ordinanze 1984 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 252 dell'anno 1984;                                  &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto   che   il   Tribunale   amministrativo   regionale   del    &#13;
 Friuli-Venezia Giulia, con ordinanza in data  24  novembre  1983,  ha    &#13;
 sollevato  questione  di  legittimità costituzionale, in riferimento    &#13;
 agli artt, 3, 51 e  120  Cost.,  dell'art.  42,  settimo  comma,  del    &#13;
 decreto-legge 13 maggio 1976, n. 227 ("Provvidenze per le popolazioni    &#13;
 dei comuni della regione Friuli-Venezia Giulia colpiti dal  terremoto    &#13;
 del  maggio  1976"),  convertito  nella legge 29 maggio 1976, n. 336,    &#13;
 nella parte in cui, per  l'assunzione  di  personale  per  i  servizi    &#13;
 A.N.A.S.,  accorda la precedenza a coloro che risiedono nella regione    &#13;
 Friuli-Venezia Giulia;                                                   &#13;
      che  è  intervenuta  la  Presidenza  del Consiglio dei Ministri    &#13;
 chiedendo che la questione sia dichiarata infondata;                     &#13;
    Considerato  che  la Corte ha affermato il principio che l'accesso    &#13;
 in condizioni di parità ai pubblici uffici può subire deroghe,  con    &#13;
 specifico  riferimento  al luogo di residenza dei concorrenti, quando    &#13;
 il requisito medesimo sia ricollegabile, come  mezzo  al  fine,  allo    &#13;
 assolvimento  di  servizi  altrimenti  non  attuabili  o  almeno  non    &#13;
 attuabili con identico risultato (sent. 158 del 1969, 86 del 1963, 13    &#13;
 del 1961, 15 del 1960);                                                  &#13;
      che  la  norma  di cui all'art. 42, settimo comma, citato appare    &#13;
 ragionevole in considerazione  della  urgenza  degli  interventi,  in    &#13;
 connessione con la immediata immissibilità in servizio del personale    &#13;
 assunto  e  della  maggiore  adeguatezza  delle  prestazioni   svolte    &#13;
 nell'ambito locale di appartenenza;                                      &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 42,  settimo  comma,  del  decreto-legge  13    &#13;
 maggio  1976, n. 227 (Provvidenze per le popolazioni dei comuni della    &#13;
 regione Friuli-Venezia Giulia colpiti dal terremoto del maggio 1976),    &#13;
 convertito  nella  legge  29  maggio  1976,  n. 336, ("Conversione in    &#13;
 legge, con modificazioni, del decreto-legge 13 maggio 1976,  n.  227,    &#13;
 concernente  provvidenze  per le popolazioni dei comuni della Regione    &#13;
 Friuli-Venezia  Giulia  colpiti  dal  terremoto  del  maggio  1976"),    &#13;
 sollevata  in  riferimento agli artt. 3, 51 e 120 Cost. dal Tribunale    &#13;
 amministrativo regionale del Friuli-Venezia  Giulia  con  l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta il 13 gennaio 1988.                               &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
