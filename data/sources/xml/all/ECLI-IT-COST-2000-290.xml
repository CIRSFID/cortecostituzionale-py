<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>290</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:290</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/07/2000</data_decisione>
    <data_deposito>14/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 334 del d.P.R.    &#13;
 23 gennaio   1973,   n. 43   (Approvazione   del  testo  unico  delle    &#13;
 disposizioni   legislative   in   materia   doganale),  in  relazione    &#13;
 all'art. 282  del  medesimo  d.P.R., promosso con ordinanza emessa il    &#13;
 17 febbraio  1999 dalla Corte di cassazione nel procedimento penale a    &#13;
 carico di Roberto Nassetti, iscritta al n. 301 del registro ordinanze    &#13;
 1999  e  pubblicata  nella Gazzetta Ufficiale della Repubblica n. 22,    &#13;
 prima serie specie, dell'anno 1999.                                      &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 22 marzo 2000 il giudice    &#13;
 relatore Cesare Mirabelli.                                               &#13;
     Ritenuto  che  la  Corte  di  cassazione, con ordinanza emessa il    &#13;
 17 febbraio  1999  in  un  procedimento nel quale il ricorrente aveva    &#13;
 impugnato  la  sentenza che lo aveva condannato alla pena della multa    &#13;
 di  L.  2.556.000  per  il  reato di contrabbando nel movimento delle    &#13;
 merci  attraverso  i  confini di terra e gli spazi doganali (art. 282    &#13;
 del  decreto del Presidente della Repubblica 23 gennaio 1973, n. 43),    &#13;
 ha  sollevato  questione di legittimità costituzionale dell'art. 334    &#13;
 del d.P.R. 23 gennaio 1973, n. 43 (Approvazione del testo unico delle    &#13;
 disposizioni   legislative   in   materia   doganale),  in  relazione    &#13;
 all'art. 282  del medesimo d.P.R., per contrasto con gli artt. 3, 25,    &#13;
 101, 111 e 113 della Costituzione;                                       &#13;
         che  l'art. 334  del d.P.R. n. 43 del 1973 prevede che, per i    &#13;
 delitti  di  contrabbando  punibili  con  la  sola  pena della multa,    &#13;
 l'amministrazione  doganale può consentire all'interessato di pagare    &#13;
 una somma non inferiore al doppio del tributo evaso, oltre al tributo    &#13;
 stesso, con estinzione del reato;                                        &#13;
         che  il  giudice  rimettente  ritiene che la norma denunciata    &#13;
 configuri   un   meccanismo   di  conciliazione  affidato  alla  mera    &#13;
 discrezionalità  dell'amministrazione; ne deriverebbe una violazione    &#13;
 sia  del  principio  di  eguaglianza  (art. 3 della Costituzione) tra    &#13;
 soggetti  tutti  potenzialmente  interessati  alla definizione in via    &#13;
 amministrativa,  sia, in contrasto con il principio di legalità e di    &#13;
 indefettibilità  della  tutela  giurisdizionale  nel controllo degli    &#13;
 atti  della  pubblica amministrazione, degli artt. 25, secondo comma,    &#13;
 101, 111 e 113 della Costituzione;                                       &#13;
         che   nel  giudizio  dinanzi  alla  Corte  costituzionale  è    &#13;
 intervenuto il Presidente del Consiglio dei Ministri, rappresentato e    &#13;
 difeso   dall'Avvocatura   generale   dello  Stato,  concludendo  per    &#13;
 l'inammissibilità  e,  in  subordine,  per  la  non fondatezza della    &#13;
 questione.                                                               &#13;
                                                                          &#13;
     Considerato  che, successivamente all'ordinanza di rimessione, il    &#13;
 reato  di  contrabbando doganale di cui all'art. 282 del d.P.R. n. 43    &#13;
 del  1973,  oggetto  del  giudizio  dinanzi alla Corte rimettente, è    &#13;
 stato  depenalizzato dall'art. 25 del decreto legislativo 30 dicembre    &#13;
 1999, n. 507 (Depenalizzazione dei reati minori e riforma del sistema    &#13;
 sanzionatorio,  ai  sensi  dell'art. 1  della  legge  25 giugno 1999,    &#13;
 n. 205)  allorché  l'ammontare  dei  diritti  di  confine dovuti non    &#13;
 superi   lire  sette  milioni  e  non  ricorrano  talune  circostanze    &#13;
 aggravanti  (indicate  dall'art. 295,  secondo  comma,  dello  stesso    &#13;
 d.P.R.);                                                                 &#13;
         che,  pertanto,  gli atti devono essere restituiti al giudice    &#13;
 rimettente  affinché  verifichi se la questione di costituzionalità    &#13;
 sia tuttora rilevante (cfr. ordinanza n. 124 del 2000).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Ordina la restituzione degli atti alla Corte di cassazione.          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 6 luglio 2000.                                &#13;
                 Il presidente e redattore: Mirabelli                     &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 14 luglio 2000.                         &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
