<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>336</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:336</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1994</data_decisione>
    <data_deposito>22/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di  legittimità  costituzionale  dell'art.  60,  secondo    &#13;
 comma,  della  legge  24  novembre 1981, n. 689 (Modifiche al sistema    &#13;
 penale), promossi con ordinanze emesse il 22 novembre 1993  dal  Pretore  di  Asti, il 16 marzo 1994 dalla Corte di appello di Torino, il    &#13;
 15 febbraio 1994 dal Pretore di Saluzzo (n. 2 ordinanze), il 9 e il 4    &#13;
 febbraio, il 9 marzo ed il 14 gennaio 1994 dal  Pretore  di  Bologna,    &#13;
 rispettivamente  iscritte  ai nn. 182, 275, 293, 294, 298, 299, 300 e    &#13;
 301 del registro ordinanze 1994 e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica nn. 15, 21, 22 e 23, prima serie speciale, dell'anno    &#13;
 1994;                                                                    &#13;
    Udito nella camera di consiglio del 6 luglio il  Giudice  relatore    &#13;
 Giuliano Vassalli.                                                       &#13;
    Ritenuto  che il Pretore di Asti ha, con ordinanza del 22 novembre    &#13;
 1993,  sollevato,  in  riferimento  all'art.  3  della  Costituzione,    &#13;
 questione  di legittimità dell'art. 60 della legge 24 novembre 1981,    &#13;
 n. 689, nella parte in cui esclude  l'applicabilità  delle  sanzioni    &#13;
 sostitutive  ai reati previsti dall'art. 21, terzo comma, della legge    &#13;
 10 maggio 1976, n. 319;                                                  &#13;
      che un'identica questione hanno sollevato anche  il  Pretore  di    &#13;
 Bologna,  con quattro ordinanze del 14 gennaio 1994, 4 febbraio 1994,    &#13;
 9 febbraio 1994 e 9 marzo  1994,  il  Pretore  di  Saluzzo,  con  due    &#13;
 ordinanze,  entrambe  del  15  febbraio 1994 e la Corte di appello di    &#13;
 Torino, con ordinanza del 16 marzo 1994;                                 &#13;
      che in nessuno dei giudizi si è costituita la parte privata né    &#13;
 ha spiegato intervento il Presidente del Consiglio dei ministri;         &#13;
    Considerato  che,  attesa  l'identità delle questioni, i relativi    &#13;
 giudizi vanno riuniti;                                                   &#13;
      che questa  Corte,  con  sentenza  n.  254  del  1994,  ha  già    &#13;
 dichiarato  l'illegittimità  costituzionale  dell'art.  60,  secondo    &#13;
 comma, della legge 24 novembre 1981, n. 689, proprio "nella parte  in    &#13;
 cui  esclude  che le pene sostitutive si applichino ai reati previsti    &#13;
 dagli artt. 21 e 22 della legge 10 maggio 1976, n. 319 (norme per  la    &#13;
 tutela delle acque dall'inquinamento)";                                  &#13;
      e  che, dunque, la questione ora proposta deve essere dichiarata    &#13;
 manifestamente inammissibile.                                            &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 60, secondo comma, della  legge    &#13;
 24   novembre  1981,  n.  689  (Modifiche  al  sistema  penale)  già    &#13;
 dichiarato costituzionalmente illegittimo con  sentenza  n.  254  del    &#13;
 1994,  "nella  parte  in  cui  esclude  che  le  pene  sostitutive si    &#13;
 applichino ai reati previsti dagli artt.  21  e  22  della  legge  10    &#13;
 maggio   1976,   n.   319   (norme   per   la   tutela   delle  acque    &#13;
 dall'inquinamento)", questione sollevata dal  Pretore  di  Asti,  dal    &#13;
 Pretore  di  Bologna, dal Pretore di Saluzzo e dalla Corte di appello    &#13;
 di Torino con le ordinanze in epigrafe.                                  &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, 7 luglio 1994.                                   &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 22 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
