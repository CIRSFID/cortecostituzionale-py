<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>270</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:270</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1999</data_decisione>
    <data_deposito>30/06/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art. 4, primo comma,    &#13;
 lettera  c)  della  legge  30  dicembre  1971,  n. 1204 (Tutela delle    &#13;
 lavoratrici madri), promosso con ordinanza emessa il 15  giugno  1998    &#13;
 dal  pretore  di Bergamo nel procedimento civile vertente tra Crosera    &#13;
 Laura e l'Istituto Scolastico Suore Sacramentine ed altro iscritta al    &#13;
 n. 827 del  registro  ordinanze  1998  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  45, prima serie speciale, dell'anno    &#13;
 1998.                                                                    &#13;
   Visto l'atto di costituzione dell'INPS;                                &#13;
   Udito nella Camera di  consiglio  del  24  marzo  1999  il  giudice    &#13;
 relatore Fernando Santosuosso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  corso  di  un  giudizio promosso per l'accertamento del    &#13;
 diritto di avvalersi  dell'astensione  obbligatoria  dal  lavoro,  il    &#13;
 pretore  di Bergamo, in funzione di giudice del lavoro, con ordinanza    &#13;
 del 15 giugno 1998 (r.o. n. 827 del 1998), ha sollevato questione  di    &#13;
 legittimità  costituzionale,  in riferimento agli artt. 3, 29, primo    &#13;
 comma, 30, primo comma, 31 e  37  della  Costituzione,  dell'art.  4,    &#13;
 primo comma, lettera c) della legge 30 dicembre 1971, n. 1204 (Tutela    &#13;
 delle lavoratrici madri).                                                &#13;
   Il   rimettente  dubita  della  legittimità  costituzionale  della    &#13;
 disposizione citata, atteso che vi sarebbe violazione  del  principio    &#13;
 della  parità di trattamento tra le fattispecie di parto a termine e    &#13;
 parto prematuro poiché sarebbe adeguatamente tutelato solo il  primo    &#13;
 e  non  anche  il  secondo;  sarebbe  altresì pregiudicato il valore    &#13;
 costituzionale della protezione della famiglia e quello della  tutela    &#13;
 del  minore,  in quanto la disposizione denunciata non consentirebbe,    &#13;
 nel caso  di  parto  prematuro,  la  frazionabilità  dell'astensione    &#13;
 obbligatoria  e  la  decorrenza  di  parte della stessa dalla data di    &#13;
 ingresso del bambino nella famiglia o quanto meno dalla data prevista    &#13;
 del parto, anziché da quella reale, così da consentire  un'adeguata    &#13;
 tutela   della   puerpera,   costretta   invece   a   beneficiare  di    &#13;
 un'aspettativa eccessiva con  sacrificio  degli  altri  interessi  di    &#13;
 rilevanza costituzionale sopra illustrati.                               &#13;
   Osserva  il  rimettente che l'istituto dall'astensione obbligatoria    &#13;
 dal lavoro di cui alla legge citata ha subito nel tempo un'evoluzione    &#13;
 legislativa e giurisprudenziale che ne ha esteso  l'originaria  ratio    &#13;
 di tutela a favore della puerpera, anche al minore e più in generale    &#13;
 alla famiglia nel delicato momento dell'ingresso in essa del neonato.    &#13;
   Da  ciò  conseguirebbe,  che  la  disposizione censurata, a tenore    &#13;
 della quale: "È vietato adibire al lavoro le donne: ... c) durante i    &#13;
 tre mesi dopo il parto" non consentirebbe di assicurare efficacemente    &#13;
 la tutela predetta del minore e della  famiglia  nel  caso  di  parti    &#13;
 prematuri  in  cui, grazie all'attuale sviluppo della scienza medica,    &#13;
 è possibile la sopravvivenza di feti nati prematuramente e assistiti    &#13;
 da una lunga permanenza in incubatrice.                                  &#13;
   Nel caso di  specie,  quindi,  l'obbligatorietà  della  decorrenza    &#13;
 dell'astensione  dal  lavoro dalla data del parto, avrebbe comportato    &#13;
 che la stessa si sarebbe esaurita prima dell'ingresso del bambino  in    &#13;
 famiglia,  sicché la madre non sarebbe stata in grado di beneficiare    &#13;
 in modo effettivo della tutela legale.                                   &#13;
   Osserva,  da  ultimo, il rimettente che gli interessi della madre e    &#13;
 del   minore   non   possono   dirsi   efficacemente    salvaguardati    &#13;
 dall'esistenza  di  altri  istituti,  come  l'astensione  facoltativa    &#13;
 prevista dalla stessa  legge,  atteso  che  l'esaurimento  anticipato    &#13;
 dell'astensione  obbligatoria  riduce  la  durata  complessiva  della    &#13;
 tutela, proprio in un caso meritevole, caratterizzato da un lato  dai    &#13;
 rischi  che  presenta  il  bambino nato prematuro in relazione al suo    &#13;
 sviluppo neuropsichico e affettivo e,  dall'altro,  dalla  situazione    &#13;
 della   madre   dopo  l'esperienza  traumatizzante  dell'interruzione    &#13;
 prematura della gravidanza e il distacco dal bambino nel  periodo  di    &#13;
 ricovero di questi in ospedale.                                          &#13;
   2.  - Si è costituito, nel presente giudizio, l'Istituto Nazionale    &#13;
 della Previdenza Sociale (I.N.P.S.) con atto di intervento depositato    &#13;
 fuori  termine,  chiedendo  che   la   questione   venga   dichiarata    &#13;
 inammissibile o infondata.<diritto>Considerato in diritto</diritto>1. - La questione sottoposta dal pretore di Bergamo, in funzione di    &#13;
 giudice   del  lavoro,  a  questa  Corte,  concerne  la  legittimità    &#13;
 costituzionale con riferimento agli artt. 3,  29,  primo  comma,  30,    &#13;
 primo  comma,  31  e  37 della Costituzione dell'art. 4, primo comma,    &#13;
 lett. c)  della  legge  30  dicembre  1971,  n.  1204  (Tutela  delle    &#13;
 lavoratrici  madri)  in  quanto  la  disposizione censurata, vietando    &#13;
 espressamente di adibire al lavoro le donne durante i tre  mesi  dopo    &#13;
 il parto, violerebbe il principio della parità di trattamento tra le    &#13;
 fattispecie  di  parto  a  termine  e  di quello prematuro, in quanto    &#13;
 sarebbe adeguatamente tutelato solo il primo e non anche il  secondo.    &#13;
 Sarebbe   altresì   pregiudicato   il  valore  costituzionale  della    &#13;
 protezione della famiglia e quello della tutela  del  minore,  atteso    &#13;
 che  la  disposizione denunciata non consentirebbe, nel caso di parto    &#13;
 pretermine,  la   "frazionabilità"   del   periodo   di   astensione    &#13;
 obbligatoria  e  la  decorrenza  di  parte della stessa dalla data di    &#13;
 ingresso del bambino nella famiglia o quanto meno dalla data prevista    &#13;
 del parto, anziché da quella reale, così da consentire  un'adeguata    &#13;
 tutela della puerpera.                                                   &#13;
   2. - La questione è fondata.                                          &#13;
   3.  -  Giova premettere che la lavoratrice madre è destinataria di    &#13;
 una   specifica   legislazione   protettiva,    che    trova    ampia    &#13;
 giustificazione  nelle  norme  costituzionali  di  cui  agli artt. 3,    &#13;
 secondo  comma,  4,  31,  32  e  37  della  Costituzione.  Il  nostro    &#13;
 ordinamento giuridico risulta inoltre integrato dalle fonti normative    &#13;
 comunitarie  e  internazionali  dirette  ad una incisiva tutela degli    &#13;
 interessi sia delle lavoratrici gestanti, puerpere o  in  periodo  di    &#13;
 allattamento  (Direttiva  del Consiglio, 19 ottobre 1992, n. 92/1985,    &#13;
 recepita con il decreto legislativo 25 novembre 1996,  n.  645),  sia    &#13;
 del  figlio  (Convenzione  di  New  York  del  1989  sui  diritti del    &#13;
 fanciullo resa esecutiva con la legge 27 maggio 1991, n. 176).           &#13;
   In  questo  quadro,  l'istituto  dell'astensione  obbligatoria  dal    &#13;
 lavoro  post  partum  previsto dalla norma impugnata - come osservato    &#13;
 dalla giurisprudenza di questa Corte (sentenze n. 332 del 1988 e n. 1    &#13;
 del 1987) - oltre ad essere volto a tutelare la salute  della  donna,    &#13;
 considera   e   protegge   il   rapporto   che,   in   tale  periodo,    &#13;
 necessariamente si instaura tra madre e figlio, anche in  riferimento    &#13;
 alle esigenze di carattere relazionale ed affettivo che sono decisive    &#13;
 sia  per un corretto sviluppo del bambino, sia per lo svolgimento del    &#13;
 ruolo della madre.                                                       &#13;
   4. - L'art. 4, primo comma, della  legge  n.  1204  del  1971,  nel    &#13;
 prevedere due periodi di astensione obbligatoria (uno anteriore e uno    &#13;
 posteriore  al  parto) contiene una formulazione letterale che appare    &#13;
 rigidamente  determinata  sia  in  ordine  alla  durata,   che   alla    &#13;
 decorrenza.  Ciò  è  confermato  dall'art. 6 del d.P.R. 25 novembre    &#13;
 1976, n. 1026 (Regolamento di  esecuzione  della  legge  30  dicembre    &#13;
 1971,  n.  1204,  sulla tutela delle lavoratrici madri) che individua    &#13;
 nel giorno successivo al parto il dies a quo del secondo  periodo  di    &#13;
 astensione dal lavoro; ma tale rigidità rivela aspetti irragionevoli    &#13;
 in relazione a casi di parto prematuro.                                  &#13;
   In  questa  ipotesi è notoriamente indispensabile che il bambino -    &#13;
 per un periodo talvolta lungo - sia affidato alle cure di specialisti    &#13;
 ed all'apparato sanitario, mentre la madre, una volta dimessa  e  pur    &#13;
 in  astensione  obbligatoria  dal  lavoro,  non  può svolgere alcuna    &#13;
 attività  per  assistere  il  figlio  ricoverato   nelle   strutture    &#13;
 ospedaliere;   ed   è  invece  obbligata  a  riprendere  l'attività    &#13;
 lavorativa quando il figlio deve essere assistito a casa. È pertanto    &#13;
 innegabile che detta situazione contrasti  sia  col  principio  della    &#13;
 parità  di  trattamento,  sia  col  valore  della  protezione  della    &#13;
 famiglia e con quello della tutela del  minore,  con  violazione  dei    &#13;
 parametri   costituzionali   invocati.      Va   pertanto  dichiarata    &#13;
 l'incostituzionalità della norma censurata.                             &#13;
   5. - È appena il caso di accennare che da tempo è stata  rilevata    &#13;
 l'incongruenza  della  disposizione  in  parola nell'ipotesi di parto    &#13;
 prematuro, e si propongono diverse soluzioni con  specifico  riguardo    &#13;
 alla  decorrenza del periodo di astensione, spostandone l'inizio o al    &#13;
 momento dell'ingresso del neonato nella casa familiare, o  alla  data    &#13;
 presunta  del termine fisiologico di una gravidanza normale; la prima    &#13;
 soluzione è analoga a quella  relativa  all'ipotesi  di  affidamento    &#13;
 preadottivo  del  neonato  (sentenza  n. 332 del 1998). La seconda è    &#13;
 parsa meritevole di essere seguita dal disegno di legge n.  4624  che    &#13;
 detta "Disposizioni per sostenere la maternità e la paternità e per    &#13;
 armonizzare  i  tempi di lavoro, di cura e della famiglia" presentato    &#13;
 dal Governo alla Camera dei Deputati in data 3 marzo 1998.               &#13;
   La scelta fra le diverse possibili soluzioni spetta al legislatore.    &#13;
   Peraltro, accertata l'illegittimità costituzionale della norma, in    &#13;
 assenza di intervento legislativo sarà il giudice a individuare  nel    &#13;
 complessivo  sistema  normativo  la  regola  idonea a disciplinare la    &#13;
 fattispecie in conformità dei principi indicati (sentenze  n.    347    &#13;
 del 1998 e n. 295 del 1991).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la illegittimità costituzionale dell'art. 4, primo comma,    &#13;
 lettera  c)  della  legge  30  dicembre  1971,  n. 1204 (Tutela delle    &#13;
 lavoratrici madri) nella parte in cui non prevede  per  l'ipotesi  di    &#13;
 parto    prematuro   una   decorrenza   dei   termini   del   periodo    &#13;
 dell'astensione obbligatoria idonea ad assicurare una adeguata tutela    &#13;
 della madre e del bambino.                                               &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 24 giugno 1999.                               &#13;
                        Il Presidente: Vassalli                           &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 30 giugno 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
