<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>8</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:8</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/1992</data_decisione>
    <data_deposito>22/01/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 419, primo    &#13;
 comma, del codice di procedura penale, promosso con ordinanza  emessa    &#13;
 l'8  maggio  1991  dal  Giudice per le indagini preliminari presso il    &#13;
 Tribunale di Reggio  Emilia  nel  procedimento  penale  a  carico  di    &#13;
 Zambelli  Andrea,  iscritta  al  n. 476 del registro ordinanze 1991 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  28,  prima    &#13;
 serie speciale, dell'anno 1991;                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 4  dicembre  1991  il  Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto  che, con ordinanza dell'8 maggio 1991, il Giudice per le    &#13;
 indagini  preliminari  presso  il  Tribunale  di  Reggio  Emilia   ha    &#13;
 sollevato,  in  riferimento all'art. 24 della Costituzione, questione    &#13;
 di legittimità dell'art. 419, primo comma, del codice  di  procedura    &#13;
 penale,  nella  parte in cui, ai fini della notificazione dell'avviso    &#13;
 di fissazione della udienza preliminare, non consente al  giudice  di    &#13;
 procedere  alla  identificazione della persona offesa della quale non    &#13;
 risulti agli atti l'identità e il domicilio ma che  sia  sicuramente    &#13;
 identificabile,  ovvero  di richiederne l'identificazione al pubblico    &#13;
 ministero;                                                               &#13;
    Considerato che l'art. 419 del codice di procedura  penale,  nello    &#13;
 stabilire  che  il  giudice  dispone  la notificazione dell'avviso di    &#13;
 fissazione della udienza preliminare alla persona offesa "della quale    &#13;
 risulti agli atti l'identità e il domicilio", fissa una  regola  che    &#13;
 deve  intendersi come intimamente correlata alle prescrizioni enunciate dall'art. 417, primo comma, lettera a) dallo stesso  codice,  ove    &#13;
 è  stabilito che fra i requisiti formali della richiesta di rinvio a    &#13;
 giudizio formulata dal pubblico ministero devono essere, fra l'altro,    &#13;
 enunciate "le generalità della persona offesa dal reato  qualora  ne    &#13;
 sia   possibile   l'identificazione",   sicché  spetta  al  pubblico    &#13;
 ministero compiere tutti gli  accertamenti  necessari  a  rendere  in    &#13;
 concreto  "possibile"  l'identificazione della persona offesa o, come    &#13;
 nel caso di specie, dei  prossimi  congiunti  ai  quali  spettano  le    &#13;
 medesime  facoltà  e  diritti  a  norma  dell'art.  90, terzo comma,    &#13;
 c.p.p.;                                                                  &#13;
      che, pertanto, ove il pubblico  ministero  non  abbia  espletato    &#13;
 simili  accertamenti, venendo meno ad uno specifico obbligo di legge,    &#13;
 non potranno dirsi soddisfatti i requisiti per  ritenere  ritualmente    &#13;
 formulata,  a  norma  dell'art.  417 c.p.p., la richiesta di rinvio a    &#13;
 giudizio, con l'ovvia conseguenza  di  impedire  al  giudice  l'avvio    &#13;
 della  fase degli atti introduttivi, disciplinata dalla norma oggetto    &#13;
 di impugnativa;                                                          &#13;
      che, per l'effetto, in presenza di una  richiesta  irritualmente    &#13;
 formulata,   è   certamente   consentito   al  giudice  disporre  la    &#13;
 restituzione degli atti al pubblico ministero perché questi provveda    &#13;
 a  svolgere  l'attività  necessaria  al  fine  di   pervenire   alla    &#13;
 identificazione   della  persona  offesa  o  accertarne  la  relativa    &#13;
 impossibilità,  salvo  che  il  giudice  stesso   non   ritenga   di    &#13;
 provvedervi  direttamente,  così  da surrogare l'inerzia della parte    &#13;
 pubblica in ossequio  al  principio  della  "massima  semplificazione    &#13;
 nello   svolgimento   del   processo"   programmaticamente  enunciato    &#13;
 dall'art. 2, n. 2, della  legge  16  febbraio  1987,  n.  81  (Delega    &#13;
 legislativa  al  Governo  della Repubblica per l'emanazione del nuovo    &#13;
 codice di procedura penale);                                             &#13;
      che, alla  luce  delle  esposte  considerazioni,  deve  pertanto    &#13;
 escludersi  che  la  norma  denunciata  determini, anche nell'ipotesi    &#13;
 dedotta con l'ordinanza remissiva, una lesione dei  diritti  e  delle    &#13;
 facoltà che la legge riconosce alla persona offesa dal reato;           &#13;
      e  che,  di  conseguenza,  la questione qui proposta deve essere    &#13;
 dichiarata manifestamente infondata.                                     &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 419, primo comma, del  codice  di  procedura    &#13;
 penale, sollevata, in riferimento all'art. 24 della Costituzione, dal    &#13;
 Giudice  per  le  indagini  preliminari presso il Tribunale di Reggio    &#13;
 Emilia con l'ordinanza in epigrafe.                                      &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 gennaio 1992.                              &#13;
                       Il presidente: CORASANITI                          &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 22 gennaio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
