<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>324</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:324</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2001</data_decisione>
    <data_deposito>27/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY,Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, &#13;
Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 7 del decreto &#13;
legislativo  31 dicembre  1992,  n. 546  (Disposizioni  sul  processo &#13;
tributario   in   attuazione   della   delega  al  Governo  contenuta &#13;
nell'art. 30  della  legge  30 dicembre  1991, n. 413), promosso, con &#13;
ordinanza  emessa  il  13 dicembre 2000, dalla commissione tributaria &#13;
provinciale  di  Verbania,  sui ricorsi riuniti proposti da Piazzolla &#13;
Donato  e  C.  s.n.c.  ed  altri  contro  l'ufficio  delle entrate di &#13;
Verbania,  iscritta al n. 66 del registro ordinanze 2001 e pubblicata &#13;
nella  Gazzetta  ufficiale  della Repubblica n. 6, 1ª serie speciale, &#13;
dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 20 giugno 2001 il giudice &#13;
relatore Massimo Vari. &#13;
    Ritenuto che la commissione tributaria provinciale di Verbania ha &#13;
sollevato,   con   ordinanza   del  13 dicembre  2000,  questione  di &#13;
legittimità  costituzionale  "delle  norme  che regolano il processo &#13;
tributario e quindi del decreto legislativo 31 dicembre 1992, n. 546" &#13;
(Disposizioni  sul  processo tributario in attuazione della delega al &#13;
Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413), &#13;
"in  particolare  l'art. 7,  nella  parte in cui non prevedono che il &#13;
giudice  tributario possa e debba disporre l'accompagnamento coattivo &#13;
del  terzo  inadempiente ad un ordine del giudice al fine di ottenere &#13;
informazioni   e   chiarimenti  relativi  ad  operazioni  fiscalmente &#13;
rilevanti,  in relazione al principio di ragionevolezza (art. 3 della &#13;
Costituzione)"; &#13;
        che  il  rimettente  premette che gli avvisi di accertamento, &#13;
oggetto  dei giudizi riuniti innanzi al medesimo pendenti, si fondano &#13;
esclusivamente     sulle    dichiarazioni    di    terzi,    raccolte &#13;
dall'amministrazione  finanziaria nella fase amministrativa, relative &#13;
a  fatture  passive emesse in relazione ad operazioni inesistenti, la &#13;
cui  veridicità  è contestata in giudizio, sicché il Collegio, "al &#13;
fine di rinnovare e, eventualmente, integrare l'attività istruttoria &#13;
svolta  dall'ufficio",  ha  ordinato  la  comparizione  di coloro che &#13;
avevano    rilasciato    le   suddette   dichiarazioni   nella   fase &#13;
procedimentale; &#13;
        che il giudice a quo nel dare atto della mancata comparizione &#13;
di  questi ultimi, benché "tempestivamente e ritualmente convocati", &#13;
ritiene  che  la  questione  sia  rilevante  ai fini della decisione, &#13;
perché  senza  la  conferma  o  la ritrattazione delle dichiarazioni &#13;
rilasciate dai terzi, la causa non può essere decisa; &#13;
        che,  quanto  alla  non manifesta infondatezza, richiamata la &#13;
sentenza  della  Corte  costituzionale  n. 18 del 2000, il rimettente &#13;
osserva  che  la  mancata  attribuzione,  al  giudice tributario, del &#13;
potere  di  disporre l'accompagnamento coattivo dei "terzi che, senza &#13;
alcuna giustificazione, non hanno osservato un ordine di un'autorità &#13;
giurisdizionale",  viola  il  principio  di ragionevolezza, in quanto &#13;
vanifica,  "con  grave  pregiudizio  anche  per la credibilità della &#13;
giustizia",   il   potere   del   giudice  tributario  di  rinnovare, &#13;
eventualmente     integrando,    l'attività    istruttoria    svolta &#13;
dall'ufficio,   nell'ipotesi  in  cui  il  contribuente  contesti  la &#13;
veridicità  delle  dichiarazioni raccolte dall'amministrazione nella &#13;
fase procedimentale; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,   concludendo  per  l'inammissibilità  o,  comunque,  per  la &#13;
manifesta infondatezza dellaquestione. &#13;
    Considerato  che  la  questione investe segnatamente l'art. 7 del &#13;
decreto legislativo 31 dicembre 1992, n. 546; &#13;
        che  l'esclusione  del  potere  di disporre l'accompagnamento &#13;
coattivo    dei    terzi   che   abbiano   rilasciato   dichiarazioni &#13;
all'amministrazione   finanziaria   è   giustificata   dalle  stesse &#13;
caratteristiche del processo tributario, peculiarmente conformato dal &#13;
legislatore  sotto l'aspetto probatorio (vedi sentenze n. 18 del 2000 &#13;
e n. 141 del 1998); &#13;
        che  il  rimettente,  richiedendo  la  estensione al processo &#13;
tributario  di  una  disciplina,  quale  quella  dell'accompagnamento &#13;
coattivo,  che  nel  processo  civile  è  essenzialmente preordinata &#13;
all'assunzione  della  prova  testimoniale,  viene,  in definitiva, a &#13;
sollecitare,   sia  pure  indirettamente,  la  introduzione  in  quel &#13;
processo  di  un  mezzo  di prova non previsto dal legislatore, senza &#13;
prospettare  convincenti ragioni di ordine costituzionale che possono &#13;
a ciò indurre; &#13;
        che,  pertanto,  la  questione  va  dichiarata manifestamente &#13;
inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità   costituzionale  dell'art. 7  del  decreto  legislativo &#13;
31 dicembre  1992,  n. 546  (Disposizioni  sul processo tributario in &#13;
attuazione della delega al Governo contenuta nell'art. 30 della legge &#13;
30 dicembre 1991, n. 413), sollevata, in riferimento all'art. 3 della &#13;
Costituzione,  dalla  Commissione  tributaria provinciale di Verbania &#13;
con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 12 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Vari &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 27 luglio 2001. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
