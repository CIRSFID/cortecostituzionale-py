<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>93</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:93</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1988</data_decisione>
    <data_deposito>26/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  28, terzo    &#13;
 comma, della legge 20 maggio 1970, n. 300 (Norme sulla  tutela  della    &#13;
 libertà  e  dignità  dei  lavoratori,  della  libertà  sindacale e    &#13;
 dell'attività  sindacale  nei  luoghi  di   lavoro   e   norme   sul    &#13;
 collocamento),  come  modificato  dalla legge 8 novembre 1977, n. 847    &#13;
 (Norme di coordinamento tra la legge 11 agosto 1973,  n.  533,  e  la    &#13;
 procedura  di cui all'articolo 28 della legge 20 maggio 1970, n. 300)    &#13;
 e  dell'art.  34  del  r.d.  30  gennaio  1941,  n.  12  (Ordinamento    &#13;
 giudiziario),  promosso  con  ordinanza  emessa  il 26 marzo 1980 dal    &#13;
 Pretore di Firenze, iscritta al n. 591 del registro ordinanze 1980  e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 298 dell'anno    &#13;
 1980;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 14 ottobre 1987 il giudice    &#13;
 relatore Francesco Greco;                                                &#13;
   Ritenuto  che,  con  ordinanza  in data 26 marzo 1980 il Pretore di    &#13;
 Firenze, designato per la trattazione del giudizio di opposizione  al    &#13;
 decreto  ex  art.  28  legge  20  maggio 1970 n. 300, col quale altro    &#13;
 magistato dello stesso ufficio aveva respinto un ricorso proposto  ai    &#13;
 sensi  di  una  tale norma, ha sollevato la questione di legittimità    &#13;
 costituzionale dell'art. 34 del r.d. 30 gennaio 1941 n.  12  e  dello    &#13;
 stesso  art.  28 della legge n. 300/70, in riferimento agli artt. 25,    &#13;
 primo comma e 101, secondo comma Cost.;                                  &#13;
      che, ad avviso del giudice a quo la prima delle norme censurate,    &#13;
 disponendo che, nelle preture, i magistrati in sottordine  coadiuvano    &#13;
 il  titolare  nell'adempimento delle sue funzioni e giustificando, di    &#13;
 conseguenza, il potere discrezionale di questo nell'assegnazione  dei    &#13;
 processi,  vanifica  la  garanzia di indipendenza posta dall'art. 101    &#13;
 Cost. anche a tutela del  singolo  giudice,  persona  fisica  poiché    &#13;
 l'opinione  da  lui espressa in sede scientifica o in altra sede può    &#13;
 precludergli l'assegnazione di un determinato processo;                  &#13;
      che,  in  tale  ordine  di  idee,  allo  stesso  giudice  appare    &#13;
 censurabile anche l'art.  28  della  legge  n.  300  del  1970  (come    &#13;
 modificato  dalla  legge  8  novembre  1977  n.  847),  in quanto, in    &#13;
 combinato disposto con la precedente norma, consente,  in  violazione    &#13;
 dell'art.  25, primo comma Cost., al Pretore titolare di assegnare il    &#13;
 giudizio di opposizione ad un giudice dell'ufficio  che  sia  persona    &#13;
 diversa  dal  giudice che ha pronunziato il decreto opposto, laddove,    &#13;
 secondo il  menzionato  precetto  costituzionale,  "giudice  naturale    &#13;
 precostituito"  è  non  solo  l'ufficio, ma anche la persona del suo    &#13;
 titolare e, secondo le norme che, come  nella  specie,  prevedono  il    &#13;
 rimedio  dell'opposizione avverso un provvedimento, il rimedio stesso    &#13;
 è individuato come un mezzo di riesame da parte dello stesso giudice    &#13;
 che tale provvedimento ha pronunziato;                                   &#13;
      che,  quanto  al  primo  punto, questa Corte, con la sentenza n.    &#13;
 143/73  ha  già  affermato  la  legittimità  costituzionale   delle    &#13;
 disposizioni  dell'Ordinamento  Giudiziario,  ivi compreso l'art. 34,    &#13;
 che fondano il potere discrezionale del Pretore titolare  in  materia    &#13;
 di distribuzione del lavoro fra i vari magistrati dell'ufficio;          &#13;
      che,   peraltro,  con  la  medesima  sentenza,  è  stato  anche    &#13;
 precisato che qualsiasi applicazione della norma  censurata,  diretta    &#13;
 al   raggiungimento   di   scopi  diversi  da  quello,  correttamente    &#13;
 individuato  dal  legislatore,  del   buon   andamento   dell'ufficio    &#13;
 "costituisce  attività  viziata per sviamento del fine o per eccesso    &#13;
 di potere o  per  esercizio  arbitrario  e  illegittimo  di  funzioni    &#13;
 pubbliche, con le conseguenze giuridiche inerenti";                      &#13;
      che  nessuna  prospettazione  nuova  emerge  nell'ordinanza  del    &#13;
 Pretore di Firenze i  cui  rilievi  si  sostanziano  nella  paventata    &#13;
 possibilità  che  la  norma  consenta  comportamenti non ortodossi e    &#13;
 perciò in argomenti che nella  suddetta  sentenza  trovano  puntuale    &#13;
 confutazione;                                                            &#13;
      che  la  ritenuta  legittimità  dell'art. 34 del r.d. n. 12 del    &#13;
 1941 toglie fondamento anche alla censura relativa all'art. 28  della    &#13;
 legge  n.  300 del 1970, letto, nella prospettazione della questione,    &#13;
 in  combinato  disposto  con  l'altra  norma,   poiché   la   stessa    &#13;
 assegnazione  del  giudizio  di  opposizione ad un giudice diverso da    &#13;
 quello designato per  la  trattazione  in  via  urgente  del  ricorso    &#13;
 costituisce  un  esercizio  del  potere  discrezionale  del  titolare    &#13;
 dell'ufficio, da esercitarsi ai sensi  e  secondo  le  finalità  del    &#13;
 citato  art.  34 dell'Ordinamento Giudiziario; e poiché la reciproca    &#13;
 autonomia della fase urgente e di quella a cognizione ordinaria dello    &#13;
 speciale  procedimento  ex  art.  28 - testimoniata, a tacer d'altro,    &#13;
 dalla idoneità del decreto non opposto a conseguire l'efficacia  del    &#13;
 giudicato  formale  -  appare  di per sé coerente con lo svolgimento    &#13;
 rispettivo di dette fasi  davanti  a  giudici  diversi  del  medesimo    &#13;
 ufficio;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale degli artt. 34 del r.d. 30 gennaio 1941  n.  12  e  28    &#13;
 della  legge  20  maggio  1970 n. 300, sollevata, in riferimento agli    &#13;
 artt. 25, primo comma, e 101, secondo comma, Cost.,  dal  Pretore  di    &#13;
 Firenze con l'ordinanza in epigrafe.                                     &#13;
    Così  deciso  in  Roma,  in Camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
