<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>246</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:246</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giulio Gionfrida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/07/1974</data_decisione>
    <data_deposito>23/07/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA- Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  88  del  &#13;
 contratto collettivo nazionale  di  lavoro  28  febbraio  1941  per  il  &#13;
 personale  dipendente da Casse di risparmio, Enti equiparati e Monti di  &#13;
 credito su pegno di prima categoria, promosso con ordinanza  emessa  il  &#13;
 17  aprile 1972 dalla Corte suprema di cassazione  -  sezione II civile  &#13;
 -  nel procedimento civile vertente tra la Cassa di risparmio di  Lucca  &#13;
 e  Sebastiani  Pier  Giorgio  ed altri, iscritta al n. 299 del registro  &#13;
 ordinanze 1972 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 247 del 20 settembre 1972.                                            &#13;
     Udito  nella  camera  di  consiglio  del  12 giugno 1974 il Giudice  &#13;
 relatore Giulio Gionfrida.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     In un giudizio civile promosso da Pier Giorgio Sebastiani contro la  &#13;
 Cassa di risparmio di Lucca per ottenere il trattamento di  quiescenza,  &#13;
 la  convenuta,  rilevato  che  la cessazione del rapporto di lavoro era  &#13;
 avvenuta per destituzione, resistette alla domanda invocando l'art.  88  &#13;
 del  proprio  regolamento  organico  del 7 giugno 1938, secondo cui "la  &#13;
 destituzione importa perdita del diritto a pensione".                    &#13;
     Il tribunale di Lucca e poi la Corte d'appello di Firenze accolsero  &#13;
 la domanda, in base alla duplice considerazione:  a) che l'art. 88  del  &#13;
 predetto  regolamento  organico doveva ritenersi abrogato dal combinato  &#13;
 disposto degli artt. 88 e 82  del  c.c.n.1.  28  febbraio  1941  per  i  &#13;
 dipendenti  delle  Casse  di  risparmio (secondo cui "al lavoratore che  &#13;
 cessa dal servizio... spetta il  trattamento  di  quiescenza  stabilito  &#13;
 dalle  norme  in  atto  presso ciascun istituto alla data di entrata in  &#13;
 vigore del presente contratto"); b)  perché  comunque  l'art.  88  del  &#13;
 Regolamento  organico,  se  non  si  ritenesse  già  abrogato, sarebbe  &#13;
 divenuto inapplicabile per effetto della sentenza n. 75 del 1968  della  &#13;
 Corte costituzionale, dichiarativa di parziale illegittimità dell'art.  &#13;
 2120 del codice civile.                                                  &#13;
     Su  ricorso  della  Cassa  di  risparmio  di  Lucca,  la  Corte  di  &#13;
 cassazione, con ordinanza del 17 aprile 1972 - rilevato che  l'art.  88  &#13;
 del   Regolamento  organico,  lungi  dall'essere  stato  abrogato,  era  &#13;
 recettiziamente richiamato dall'art. 88 del  contratto  collettivo  del  &#13;
 1941,  la cui efficacia non poteva dirsi venuta meno, per questa parte,  &#13;
 in dipendenza della su citata sentenza della Corte costituzionale -  ha  &#13;
 sollevato, con riferimento all'art. 36 della Costituzione, questione di  &#13;
 legittimità costituzionale della predetta disposizione del c.c.n.1. 28  &#13;
 febbraio  1941,  per  la  parte,  appunto,  in  cui  contiene un rinvio  &#13;
 recettizio al primo comma dell'art. 88 del Regolamento  organico  della  &#13;
 Cassa di risparmio di Lucca.                                             &#13;
     Ritualmente   notificata,   comunicata   e  pubblicata  l'ordinanza  &#13;
 indicata, nel giudizio innanzi questa Corte non si è costituita alcune  &#13;
 delle parti né vi è stato intervento del Presidente del Consiglio dei  &#13;
 ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'ordinanza de qua denuncia, per  contrasto  con  l'art.  36  della  &#13;
 Costituzione, l'art. 88 del contratto collettivo nazionale di lavoro 28  &#13;
 febbraio  1941  per i dipendenti delle Casse di risparmio, per la parte  &#13;
 in cui contiene un rinvio recettizio al primo comma  dell'art.  88  del  &#13;
 Regolamento organico della Cassa di risparmio di Lucca.                  &#13;
     La questione è inammissibile.                                       &#13;
     Questa  Corte, con le sentenze n. 1 del 1963 e n. 76 del 1969, dopo  &#13;
 aver  precisato  che  i  contratti  collettivi,  come  gli  altri  atti  &#13;
 normativi  previsti  nell'art.  5  delle  disposizioni  sulla  legge in  &#13;
 generale, non avevano forza di legge nel sistema in cui sorsero,  tanto  &#13;
 che  non  potevano  derogare  neanche  alle disposizioni imperative dei  &#13;
 regolamenti (art. 7 delle disposizioni sulla  legge  in  generale),  ha  &#13;
 ritenuto  che  -  caduto  il sistema - l'art. 43 del d.l.1. 23 novembre  &#13;
 1944, n.  369, non dette alle norme predette  forza  di  legge,  ma  si  &#13;
 limitò  a  mantenere  inalterata  per  l'avvenire  la  loro originaria  &#13;
 efficacia. Dal che ha dedotto che  rispetto  ad  esse  non  si  possono  &#13;
 sollevare   questioni  di  legittimità  costituzionale  e  che  spetta  &#13;
 soltanto al giudice competente per le singole controversie  l'esame  se  &#13;
 le  disposizioni  di  cui  si è detto contrastino con norme imperative  &#13;
 appartenenti ad un livello superiore nella gerarchia delle fonti ed  in  &#13;
 primo luogo se contrastino con norme costituzionali.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 88 del contratto collettivo nazionale di lavoro  28  febbraio  &#13;
 1941 per i dipendenti delle Casse di risparmio, in riferimento all'art.  &#13;
 36 della Costituzione, sollevata con l'ordinanza indicata in epigrafe.   &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 10 luglio 1974.         &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIOVANNI  &#13;
                                   BATTISTA  BENEDETTI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE ROCCHETTI  -  &#13;
                                   ENZO  CAPALOZZA  -  VIN CENZO MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI.                        &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
