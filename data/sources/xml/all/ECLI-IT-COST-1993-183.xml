<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>183</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:183</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/04/1993</data_decisione>
    <data_deposito>21/04/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio sull'ammissibilità del conflitto  di  attribuzione  fra    &#13;
 poteri dello Stato sollevato dal Magistrato di sorveglianza di Ancona    &#13;
 nei  confronti  del  Ministro  di  grazia  e  giustizia,  con ricorso    &#13;
 depositato in cancelleria il 5 febbraio 1993 ed iscritto al n. 43 del    &#13;
 registro ammissibilità conflitti;                                       &#13;
    Udito nella camera di consiglio  del  24  marzo  1993  il  Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto  che il Magistrato di sorveglianza di Ancona ha sollevato    &#13;
 conflitto di attribuzione nei confronti  del  Ministro  di  grazia  e    &#13;
 giustizia,  in riferimento al decreto 25 novembre 1992 concernente la    &#13;
 sottoposizione di taluni detenuti al regime detentivo di cui all'art.    &#13;
 41-bis, secondo comma, della legge  26  luglio  1975  n.  354  (Norme    &#13;
 sull'ordinamento   penitenziario   e   sull'esecuzione  delle  misure    &#13;
 privative e limitative della libertà), "per la dichiarazione che non    &#13;
 spetta   al   Ministro   di   grazia   e   giustizia,    Dipartimento    &#13;
 dell'amministrazione   penitenziaria,   il   potere  di  disporre  la    &#13;
 sottoposizione  della  corrispondenza  dei  detenuti  al   visto   di    &#13;
 controllo";                                                              &#13;
      che  il Magistrato ricorrente, dopo aver premesso che il decreto    &#13;
 in esame prevede, tra le altre disposizioni, al punto n. 4  dell'art.    &#13;
 1 la sottoposizione della corrispondenza epistolare e telegrafica dei    &#13;
 detenuti individuati nel decreto stesso a visto di controllo da parte    &#13;
 del  direttore  dell'istituto  penitenziario, ha lamentato la lesione    &#13;
 delle attribuzioni direttamente garantite  all'Autorità  giudiziaria    &#13;
 dall'art. 15 della Costituzione in tema di limitazione della libertà    &#13;
 e  della  segretezza  della  corrispondenza  e di ogni altra forma di    &#13;
 comunicazione.                                                           &#13;
    Considerato che ricorrono i requisiti di  cui  all'art.  37  della    &#13;
 legge  11  marzo  1953,  n.  87, ai fini della configurabilità di un    &#13;
 conflitto di attribuzione tra poteri dello Stato la  cui  risoluzione    &#13;
 spetti a questa Corte;                                                   &#13;
      che, infatti, ciascuno degli organi fra i quali si assume essere    &#13;
 insorto  il  conflitto  è  abilitato  ad  esercitare, nella materia,    &#13;
 attribuzioni proprie ad esso conferite dalla Costituzione (artt. 15 e    &#13;
 110 della Costituzione);                                                 &#13;
      che  inoltre  è   lamentata   in   concreto   la   lesione   di    &#13;
 un'attribuzione    costituzionalmente   garantita,   qual'è   quella    &#13;
 conferita   dall'art.   15,   secondo   comma,   della   Costituzione    &#13;
 all'Autorità  giudiziaria  in  tema  di limitazioni alla libertà ed    &#13;
 alla segretezza  della  corrispondenza  e  di  ogni  altra  forma  di    &#13;
 comunicazione;                                                           &#13;
      che,  pertanto,  il  ricorso deve essere dichiarato ammissibile,    &#13;
 mentre,   atteso   il   carattere   di   mera   delibazione,    senza    &#13;
 contraddittorio,  della  presente  pronuncia,  resta  impregiudicata,    &#13;
 secondo la costante giurisprudenza di questa  Corte,  ogni  decisione    &#13;
 anche in punto di ammissibilità.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  ammissibile,  ai  sensi dell'art. 37 della legge 11 marzo    &#13;
 1953, n. 87, il ricorso per  conflitto  di  attribuzione  tra  poteri    &#13;
 dello  Stato  proposto  dal  Magistrato di sorveglianza di Ancona nei    &#13;
 confronti del Ministro di grazia e giustizia;                            &#13;
    Dispone:                                                              &#13;
       a) che la cancelleria della Corte dia  immediata  comunicazione    &#13;
 al ricorrente della presente ordinanza;                                  &#13;
       b) che a cura del ricorrente il ricorso e la presente ordinanza    &#13;
 siano  notificati  al Ministro di grazia e giustizia entro il termine    &#13;
 di gg. 30 dalla comunicazione.                                           &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 2 aprile 1993.                                &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 21 aprile 1993.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
