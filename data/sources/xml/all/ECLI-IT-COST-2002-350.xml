<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>350</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:350</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia>Fernanda Contri</redattore_pronuncia>
    <data_decisione>08/07/2002</data_decisione>
    <data_deposito>16/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale del combinato disposto &#13;
degli  artt. 35,  27  e 28 della legge 4 maggio 1983, n. 184 (Diritto &#13;
del  minore  ad  una  famiglia),  nel  testo  modificato  dalla legge &#13;
28 marzo  2001,  n. 149  (Modifiche alla legge 4 maggio 1983, n. 184, &#13;
recante  "Disciplina  dell'adozione  e  dell'affidamento dei minori", &#13;
nonché  al  titolo VIII del libro primo del codice civile), promosso &#13;
con  ordinanza  emessa  il  16 ottobre 2001 dal Tribunale di Vicenza, &#13;
iscritta  al  n. 966  del  registro ordinanze 2001 e pubblicata nella &#13;
Gazzetta   Ufficiale   della  Repubblica  n. 2,  1ª  serie  speciale, &#13;
dell'anno 2002. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio  del 5 giugno 2002 il giudice &#13;
relatore Fernanda Contri. &#13;
    Ritenuto che il Tribunale di Vicenza ha sollevato, in riferimento &#13;
agli   artt. 2,   3,   10  e  11  della  Costituzione,  questione  di &#13;
legittimità costituzionale del combinato disposto degli artt. 35, 27 &#13;
e  28  della  legge  4 maggio 1983, n. 184 (Diritto del minore ad una &#13;
famiglia),  nella  parte  in  cui,  in  caso di adozione di un minore &#13;
straniero, dispongono l'attribuzione automatica all'adottato del solo &#13;
cognome  degli  adottanti, non consentendo al giudice di disporre che &#13;
il minore conservi anche il cognome originario; &#13;
        che il giudice a quo, è investito dell'esame di una domanda, &#13;
presentata dal pubblico ministero ai sensi degli artt. 69, 165, 166 e &#13;
167 del regio decreto 9 luglio 1939, n. 1238 (Ordinamento dello stato &#13;
civile),  con  la  quale  è  stata  promossa d'ufficio la formazione &#13;
dell'atto di nascita di un minore adottato all'estero; &#13;
        che  con successiva domanda integrativa del 21 febbraio 2001, &#13;
il  pubblico  ministero  chiedeva  che il minore potesse mantenere il &#13;
cognome  originario  "in  uno  col  cognome degli adottanti", facendo &#13;
proprie  le motivazioni di una istanza in tal senso presentatagli dai &#13;
genitori adottivi; &#13;
        che   a  fondamento  di  tale  seconda  domanda  il  pubblico &#13;
ministero   faceva   presente   come   il   minore  avesse  percepito &#13;
negativamente  la  perdita  del  suo  cognome  originario, vivendo la &#13;
stessa come una sorta di "amputazione della personalità" e che nello &#13;
stesso nucleo familiare era già inserita la sorella dell'adottato; &#13;
        che  le  ragioni  esposte  dall'istante  venivano  in seguito &#13;
confermate  dall'istruttoria  disposta dal giudice, che accertava che &#13;
la  perdita  del cognome era stata vissuta come una espropriazione da &#13;
parte del minore; &#13;
        che,  ad  avviso  del  Tribunale  di  Vicenza, la domanda del &#13;
pubblico  ministero  non potrebbe, in applicazione delle disposizioni &#13;
impugnate,  trovare  accoglimento,  in  quanto  il  Tribunale  per  i &#13;
minorenni  di  Venezia,  con  decreto  del  1  febbraio  1999,  aveva &#13;
riconosciuto   il   provvedimento   dell'autorità   straniera  quale &#13;
affidamento  preadottivo  del  minore  e successivamente, con decreto &#13;
dell'8 agosto  2000, aveva dichiarato l'adozione del minore ordinando &#13;
la trascrizione dell'atto nei registri dello stato civile; &#13;
        che,  secondo il giudice a quo, poiché l'art. 35 della legge &#13;
n. 184  del  1983 richiama l'art. 27 della stessa legge, deve dedursi &#13;
che  l'adozione  internazionale  produce gli stessi effetti di quella &#13;
nazionale  e  comporta  di  conseguenza  l'automatica attribuzione al &#13;
minore del solo cognome degli adottanti, come ribadito dal successivo &#13;
art. 28; &#13;
        che,  ad  avviso del rimettente, quanto alla attribuzione del &#13;
cognome   al   minore   adottato   all'estero,   va   verificata   la &#13;
compatibilità   con   i  principi  costituzionali  dell'impostazione &#13;
seguita  dal legislatore del 1983, confermata dalla novella del 1998, &#13;
nella parte in cui essa viene "assunta in termini di tale assolutezza &#13;
da  non consentire deroghe neppure in comprovati casi particolari nei &#13;
quali l'interesse del minore deponga in senso contrario"; &#13;
        che,  ricordata  la  giurisprudenza  della  Corte  in tema di &#13;
tutela  del  nome quale diritto garantito dall'art. 2 Cost. (sentenze &#13;
nn. 13  del  1994, 297 del 1996 e 120 del 2001), secondo il giudice a &#13;
quo  la  forzosa  soppressione  del cognome della famiglia di origine &#13;
potrebbe   configurare   una  violazione  del  diritto  all'identità &#13;
personale dell'adottato; &#13;
        che,  sempre  secondo  il  Tribunale di Vicenza, la rigidità &#13;
delle   disposizioni   impugnate  è  irragionevole,  in  riferimento &#13;
all'art. 3   Cost.,   in   quanto   non   tiene   in   considerazione &#13;
l'eventualità  che  in  casi  specifici e comprovati l'interesse del &#13;
minore  possa  essere  più  tutelato dal mantenimento, piuttosto che &#13;
dalla soppressione, del cognome di origine; &#13;
        che,  rileva  ancora  il  rimettente, l'art. 10, terzo comma, &#13;
della  legge  22 maggio  1974,  n. 357  (Ratifica ed esecuzione della &#13;
convenzione  europea  in  materia  di  adozione  di minori, firmata a &#13;
Strasburgo il 24 aprile 1967), prevede espressamente che, come regola &#13;
generale,  l'adottato  possa  assumere  il  cognome  dell'adottante o &#13;
aggiungerlo  al  proprio, secondo un'impostazione più rispettosa dei &#13;
casi concreti; &#13;
        che  il giudice a quo ritiene che tale disposizione "si ponga &#13;
come  parametro  per  valutare  la  correttezza  costituzionale della &#13;
disciplina  legale  denunciata"  in  riferimento  agli  artt. 10 e 11 &#13;
Cost.,  anche  perché  la  legge  n. 476  del  1998,  nel modificare &#13;
l'originaria  disciplina  della  legge  n. 184 del 1983 in materia di &#13;
adozione internazionale, non avrebbe minimamente considerato le norme &#13;
di detta Convenzione del 1967; &#13;
        che   nel   giudizio   di   legittimità   costituzionale  è &#13;
intervenuto il Presidente del Consiglio dei ministri, rappresentato e &#13;
difeso  dall'Avvocatura generale dello Stato, chiedendo alla Corte di &#13;
dichiarare  inammissibile  o,  in  subordine,  infondata la questione &#13;
sollevata dal Tribunale di Vicenza; &#13;
        che   preliminarmente  la  difesa  erariale  rileva  come  il &#13;
riferimento,  quali  parametri di illegittimità costituzionale, agli &#13;
artt. 10  e  11  Cost. risulti non pertinente, non avendo il Paese di &#13;
provenienza  del  minore  sottoscritto  le Convenzioni internazionali &#13;
citate nell'ordinanza di rimessione e non venendo in questione né il &#13;
rispetto  di  norme  internazionali  di  natura  consuetudinaria, né &#13;
alcuna limitazione alla sovranità nazionale; &#13;
        che  l'Avvocatura  osserva che può dubitarsi della rilevanza &#13;
della questione nel procedimento a quo, che concerne la ricostruzione &#13;
e  la  formazione  di un atto di stato civile e non gli effetti della &#13;
già  disposta  adozione, rilevando come il giudice a quo, non si sia &#13;
dato   minimamente   carico  di  esaminare  la  disposizione  di  cui &#13;
all'art. 95,  comma 3, del d.P.R. n. 396 del 2000 (Regolamento per la &#13;
revisione e la semplificazione dell'ordinamento dello stato civile, a &#13;
norma dell'articolo 2, comma 12, della legge 15 maggio 1997, n. 127), &#13;
secondo  cui l'interessato può comunque richiedere il riconoscimento &#13;
del diritto al mantenimento del cognome originariamente attribuitogli &#13;
se  questo  costituisce  ormai un autonomo segno distintivo della sua &#13;
identità personale; &#13;
        che  l'Avvocatura  ritiene  che  le norme censurate non siano &#13;
incompatibili coi principi costituzionali invocati, in quanto dettate &#13;
per  favorire  la  piena  integrazione  del  minore  nel nuovo nucleo &#13;
famigliare,  e  che  la  questione  abbia  carattere manipolativo, in &#13;
quanto volta ad introdurre una scelta discrezionale del giudice. &#13;
    Considerato che il Tribunale di Vicenza dubita della legittimità &#13;
costituzionale  del  combinato disposto degli artt. 35, 27 e 28 della &#13;
legge  4 maggio  1983,  n. 184  (Diritto del minore ad una famiglia), &#13;
nella  parte  in cui, per la adozione di minori stranieri, prevedendo &#13;
l'automatica   attribuzione   all'adottato  del  solo  cognome  degli &#13;
adottanti,  non  consentono  che  il tribunale possa stabilire che il &#13;
minore  possa conservare anche il cognome originario, per violazione: &#13;
dell'art. 2  della Costituzione, essendo la possibilità di mantenere &#13;
il   cognome   originario   un  diritto  inviolabile  della  persona; &#13;
dell'art. 3 Cost., sotto il profilo della ragionevolezza, dal momento &#13;
che  l'automatica  attribuzione  al  minore  del  solo  cognome degli &#13;
adottanti  non  terrebbe  in considerazione i singoli casi specifici; &#13;
degli  artt. 10  e  11  Cost.,  perché le disposizioni impugnate non &#13;
terrebbero  in  alcuna  considerazione  le  norme  della  Convenzione &#13;
europea  in  materia  di  adozione di minori, firmata a Strasburgo il &#13;
24 aprile 1967, ratificata dalla legge 22 maggio 1974, n. 357; &#13;
        che  il  giudice  rimettente,  investito  della  domanda  del &#13;
pubblico  ministero volta alla formazione di un atto di stato civile, &#13;
censura  le disposizioni sull'adozione internazionali dei minorenni - &#13;
delle  quali  non  deve in alcun modo fare applicazione, essendo esse &#13;
già  state  considerate  dal  competente tribunale per i minorenni - &#13;
omettendo  al  contrario  di valutare l'incidenza sul procedimento in &#13;
corso dell'art. 95, comma, 3, del d.P.R. n. 396 del 2000 (Regolamento &#13;
per  la  revisione  e la semplificazione dell'ordinamento dello stato &#13;
civile,  a  norma  dell'art. 2, comma 12, della legge 15 maggio 1997, &#13;
n. 127),  che  consente all'interessato di chiedere il riconoscimento &#13;
del    diritto    al   mantenimento   del   cognome   originariamente &#13;
attribuitogli,  quando  questo  costituisce  ormai  un autonomo segno &#13;
distintivo della sua identità personale; &#13;
        che  la  questione  sollevata dal Tribunale di Vicenza appare &#13;
perciò  manifestamente  inammissibile  per  irrilevanza,  perché le &#13;
disposizioni  impugnate non devono essere applicate dal giudice a quo &#13;
chiamato a decidere in materia diversa da quella dell'adozione, sulla &#13;
quale si è già pronunciato il tribunale minorile competente. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità costituzionale del combinato disposto degli artt. 35, 27 &#13;
e  28  della  legge  4 maggio 1983, n. 184 (Diritto del minore ad una &#13;
famiglia),  nel  testo  modificato  dalla legge 28 marzo 2001, n. 149 &#13;
(Modifiche  alla  legge  4 maggio  1983,  n. 184, recante "Disciplina &#13;
dell'adozione  e dell'affidamento dei minori", nonché al titolo VIII &#13;
del  libro  primo  del codice civile), sollevata, in riferimento agli &#13;
artt. 2,  3, 10 e 11 della Costituzione, dal Tribunale di Vicenza con &#13;
l'ordinanza indicata in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, l'8 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                        Il redattore: Contri &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 16 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
