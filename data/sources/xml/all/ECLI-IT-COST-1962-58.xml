<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1962</anno_pronuncia>
    <numero_pronuncia>58</numero_pronuncia>
    <ecli>ECLI:IT:COST:1962:58</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CAPPI</presidente>
    <relatore_pronuncia>Nicola Jaeger</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/06/1962</data_decisione>
    <data_deposito>14/06/1962</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. GIUSEPPE CAPPI, Presidente - Prof. &#13;
 GASPARE AMBROSINI - Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO &#13;
 GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - &#13;
 Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Dott. ANTONIO MANCA - &#13;
 Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - &#13;
 Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  14  della  &#13;
 legge della Regione siciliana 5 aprile 1952, n.  11, e dell'art. 84, n.  &#13;
 6,  del  T.U.  approvato  con  decreto  del  Presidente  della  Regione  &#13;
 siciliana 9 giugno 1954, n. 9, promosso  con  ordinanza  emessa  il  10  &#13;
 aprile  1959  dalla  Corte  di  appello  di Palermo su ricorso di Amato  &#13;
 Calogero ed altri, iscritta al n. 152 del  Registro  ordinanze  1961  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 245 del 30  &#13;
 settembre 1961 e nella Gazzetta Ufficiale della Regione siciliana n. 51  &#13;
 del 20 settembre 1961.                                                   &#13;
     Vista la dichiarazione di intervento del Presidente  della  Regione  &#13;
 siciliana;                                                               &#13;
     udita  nell'udienza  pubblica  del  30 maggio 1962 la relazione del  &#13;
 Giudice Nicola Jaeger.                                                   &#13;
                           Ritenuto in fatto:                             &#13;
     Con ordinanza in data 10 aprile  1959,  pervenuta,  peraltro,  agli  &#13;
 uffici  della Corte costituzionale soltanto il 18 agosto 1961, la Corte  &#13;
 di  appello  di  Palermo  ha   proposto   questione   di   legittimità  &#13;
 costituzionale  dell'art.  14 della legge della Regione siciliana n. 11  &#13;
 del 5 aprile 1952, e dell'art. 84, n. 6, del T.U. approvato con decreto  &#13;
 9 giugno 1954, n. 9, del Presidente della Regione.                       &#13;
     L'ordinanza è stata emessa  nel  corso  di  un  giudizio  pendente  &#13;
 davanti  a  detta  Corte  di appello in seguito a reclamo, proposto con  &#13;
 atto del 13 marzo 1959  da  sette  consiglieri  comunali  di  Camastra,  &#13;
 dichiarati decaduti dall'ufficio con decisione in data 24 novembre 1958  &#13;
 della   Giunta   provinciale   amministrativa  di  Agrigento,  in  sede  &#13;
 giurisdizionale, motivata in base  alla  pendenza  di  un  giudizio  di  &#13;
 responsabilità  amministrativa  e  contabile  a  carico  di essi, già  &#13;
 Sindaco e Assessori del Comune di Camastra.                              &#13;
     Nel loro ricorso alla Corte di appello,  i  consiglieri  dichiarati  &#13;
 decaduti  avevano sostenuto che la materia dell'elettorato comunale non  &#13;
 appartenesse alla competenza legislativa della Regione siciliana,  onde  &#13;
 avrebbe  dovuto  considerarsi  estesa  senz'altro  a  questa la riforma  &#13;
 adottata con la legge dello Stato 23 marzo 1956, n. 136, il cui art. 6,  &#13;
 modificando  la  norma  anteriore,  cui  si  era  uniformata  la  legge  &#13;
 regionale,  ha  escluso che la ineleggibilità derivante dalla pendenza  &#13;
 di una lite con il Comune si applichi agli amministratori comunali  ove  &#13;
 la  lite  sia connessa con l'esercizio del mandato; in via subordinata,  &#13;
 avevano rilevato che, comunque, la diversità dei  requisiti  richiesti  &#13;
 per  accedere  alle  cariche  comunali  dalla legge statale e da quella  &#13;
 regionale avrebbe reso inapplicabile la seconda,  venuta  in  contrasto  &#13;
 con il principio contenuto nell'art. 51 della Costituzione, a norma del  &#13;
 quale  tutti  i  cittadini possono accedere agli uffici pubblici e alle  &#13;
 cariche elettive in condizioni di eguaglianza.                           &#13;
     La Corte di appello non ha accolto il primo motivo ed ha affermato,  &#13;
 invece, che l'art. 14, lett. o, dello Statuto regionale comprende anche  &#13;
 la materia dell'elettorato comunale; ha escluso, pertanto, che la nuova  &#13;
 disciplina della legge dello Stato del 1956 si sia estesa  di  per  sé  &#13;
 alla   Regione   siciliana   o   abbia   modificato   la   disposizione  &#13;
 corrispondente della legge regionale.                                    &#13;
     Ha  ritenuto,  invece,  non  manifestamente  infondata la questione  &#13;
 della illegittimità costituzionale sopravvenuta della norma  contenuta  &#13;
 nell'art.  14  della  legge regionale 5 aprile 1952, n. 11, e nell'art.  &#13;
 84, n.  6, del T.U. 9 giugno 1954, n. 9, in quanto ne  deriverebbe  una  &#13;
 situazione  di  diseguaglianza  fra  gli  amministratori comunali della  &#13;
 Sicilia e quelli delle altre parti del territorio italiano; e ciò  non  &#13;
 sarebbe  consentito  dalla Costituzione, anche in base all'insegnamento  &#13;
 dato dalla giurisprudenza della Corte costituzionale.                    &#13;
     In quanto alla rilevanza della questione di legittimità, la  Corte  &#13;
 di  appello  ha  constatato che con l'art. 16 di una successiva legge 9  &#13;
 marzo 1959, n. 3, il legislatore regionale aveva introdotto nel proprio  &#13;
 ordinamento una norma identica a quella contenuta nella  legge  statale  &#13;
 in  materia  del 23 marzo 1956; ma non ha ritenuto che per questo fosse  &#13;
 venuta meno la rilevanza della questione sollevata dai ricorrenti.       &#13;
     L'ordinanza  è  stata  notificata  al  Presidente  della   Regione  &#13;
 siciliana  e  al Presidente dell'Assemblea regionale il 31 marzo e il 3  &#13;
 aprile 1961 e pubblicata, per disposizione del Presidente  della  Corte  &#13;
 costituzionale,  nella Gazzetta Ufficiale della Regione siciliana n. 51  &#13;
 del 20 settembre 1961 e nella Gazzetta Ufficiale  della  Repubblica  n.  &#13;
 245 del 30 settembre 1961.                                               &#13;
     È  intervenuto nel giudizio, con atto depositato il 9 giugno 1961,  &#13;
 il Presidente della Regione siciliana, rappresentato e difeso dall'avv.  &#13;
 Vincenzo Gueli. In tale atto la difesa della Regione contesta anzitutto  &#13;
 che sussista,  e  che  trovi  una  congrua  e  sufficiente  motivazione  &#13;
 nell'ordinanza  di rimessione, la rilevanza della questione rispetto al  &#13;
 giudizio principale, il quale avrebbe potuto e dovuto essere  deciso  a  &#13;
 norma  della  più  recente legge regionale, di immediata applicazione.  &#13;
 Nel  merito,  nega  che  la  intervenuta  modificazione  di  una  legge  &#13;
 ordinaria    dello    Stato   possa   determinare   la   illegittimità  &#13;
 costituzionale sopravvenuta di una norma regionale identica alla  norma  &#13;
 statale  anteriore,  e  che,  soprattutto, una divergenza temporanea di  &#13;
 disposizioni,  come  quelle  di  cui  si  discute   in   causa,   possa  &#13;
 considerarsi   tale  da  implicare  una  violazione  del  principio  di  &#13;
 eguaglianza sancito nell'art. 51 della Costituzione.                     &#13;
     Essa conclude, pertanto, perché la Corte  costituzionale  dichiari  &#13;
 inammissibile  o infondata la questione proposta dalla Corte di appello  &#13;
 di Palermo.  Tali conclusioni sono ribadite nella successiva memoria in  &#13;
 data 15 maggio 1962, nella quale si richiama  anche  la  giurisprudenza  &#13;
 della  Corte  di  cassazione sul punto dell'applicabilità immediata di  &#13;
 norme che facciano cessare cause limitative della capacità a ricoprire  &#13;
 uffici pubblici, e si osserva che tale giurisprudenza rende più  grave  &#13;
 il   difetto   di  motivazione  dell'ordinanza  sulla  rilevanza  della  &#13;
 questione. La difesa della Regione ricorda anche alcune sentenze  della  &#13;
 Corte  costituzionale,  e  ne  deduce  che  tali  precedenti  non  sono  &#13;
 invocabili in contrario, ma piuttosto in favore  delle  conclusioni  di  &#13;
 merito già formulate dalla Regione.                                     &#13;
                         Considerato in diritto:                          &#13;
     Dal  testo stesso dell'ordinanza della Corte di appello di Palermo,  &#13;
 con  la  quale  è  stata  proposta  la   questione   di   legittimità  &#13;
 costituzionale, risulta che al momento della pronuncia di essa era già  &#13;
 stata  emanata  la  legge  regionale  9  marzo 1959, n. 3, e che questa  &#13;
 conteneva una norma (art. 16) identica a quella dell'art. 6 della legge  &#13;
 dello Stato 23 marzo 1956, n. 136.                                       &#13;
     La Corte ha però omesso di rilevare la data di pubblicazione della  &#13;
 nuova legge regionale nella Gazzetta Ufficiale della Regione siciliana,  &#13;
 avvenuta nel n. 14 dell'11 marzo 1959, e la disposizione dell'art. 8 di  &#13;
 detta  legge,  a  norma  della  quale questa entrò in vigore il giorno  &#13;
 stesso della pubblicazione.                                              &#13;
     Tutto fa ritenere che, se il giudice del processo principale avesse  &#13;
 tenuto conto di questi  dati,  esso  si  sarebbe  proposto  in  termini  &#13;
 diversi  il problema della rilevanza della questione di legittimità di  &#13;
 una norma, che era venuta meno prima che la  causa  fosse  definita;  e  &#13;
 ciò  anche  in  considerazione  del  principio  accolto dalla Corte di  &#13;
 cassazione, la quale ha non soltanto affermato che le norme  che  fanno  &#13;
 cessare  cause  limitative  della capacità a ricoprire uffici pubblici  &#13;
 sono di immediata attuazione ogni qualvolta  la  ineleggibilità  o  la  &#13;
 decadenza  dalla  carica  per  tali  cause  non sia stata accertata con  &#13;
 sentenza  passata  in  giudicato,  ma  ha  provveduto  in  conseguenza,  &#13;
 riformando  la  sentenza  impugnata a seguito della pubblicazione della  &#13;
 legge nuova (appunto la legge 23 marzo 1956,  n.  136,  sopra  citata),  &#13;
 sopravvenuta dopo la pronuncia della sentenza denunciata per cassazione  &#13;
 (Cass. civ., Sezione Ia, 4 ottobre 1956, n.  3342).                      &#13;
     Di  conseguenza,  si deve ritenere necessaria una nuova valutazione  &#13;
 sul punto della  rilevanza,  rispetto  al  giudizio  principale,  della  &#13;
 questione sottoposta alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  che  gli  atti  siano  restituiti  alla Corte di appello di  &#13;
 Palermo.                                                                 &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 7 giugno 1962.                                &#13;
                                   GIUSEPPE  CAPPI - GASPARE AMBROSINI -  &#13;
                                   MARIO COSATTI  -  FRANCESCO  PANTALEO  &#13;
                                   GABRIELI - GIUSEPPE CASTELLI AVOLIO -  &#13;
                                   ANTONINO  PAPALDO  -  NICOLA JAEGER -  &#13;
                                   GIOVANNI CASSANDRO - ANTONIO MANCA  -  &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   - GIUSEPPE CHIARELLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
