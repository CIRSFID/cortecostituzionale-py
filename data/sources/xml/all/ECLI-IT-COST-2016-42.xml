<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2016</anno_pronuncia>
    <numero_pronuncia>42</numero_pronuncia>
    <ecli>ECLI:IT:COST:2016:42</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CRISCUOLO</presidente>
    <relatore_pronuncia>Nicolò Zanon</relatore_pronuncia>
    <redattore_pronuncia>Nicolò Zanon</redattore_pronuncia>
    <data_decisione>10/02/2016</data_decisione>
    <data_deposito>25/02/2016</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alessandro CRISCUOLO; Giudici : Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 8, commi 4, lettera a), 6, 8 e 10, e 46, commi 1, 2, 3, 4 e 6, del decreto-legge 24 aprile 2014, n. 66 (Misure urgenti per la competitività e la giustizia sociale), convertito, con modificazioni, dall'art. 1, comma 1, della legge 23 giugno 2014, n. 89, promosso dalla Regione autonoma Valle d'Aosta/Vallée d'Aoste con ricorso notificato il 30 luglio-4 agosto 2014, depositato il 31 luglio 2014 e iscritto al n. 56 del registro ricorsi 2014; degli artt. 8, commi 4, 6 e 10, e 46, commi 1, 2, 3, 4 e 6, del d.l. n. 66 del 2014, come convertito, promosso dalla Provincia autonoma di Bolzano con ricorso notificato il 29-31 luglio 2014, depositato il 31 luglio 2014 e iscritto al n. 57 del registro ricorsi 2014; degli artt. 8, commi 4, 6, 7 e 10, 46, commi 1, 2, 3 e 6, e 47, commi 8, 9, 11 e 12, del d.l. n. 66 del 2014, come convertito, promosso dalla Provincia autonoma di Trento con ricorso notificato il 20 agosto 2014, depositato il 26 agosto 2014 e iscritto al n. 65 del registro ricorsi 2014.&#13;
 Visti gli atti di costituzione del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 10 febbraio 2016 il Giudice relatore Nicolò Zanon.&#13;
 Ritenuto che, con i ricorsi iscritti rispettivamente al reg. ric. n. 56, n. 57 e n. 65 del 2014, la Regione autonoma Valle d'Aosta/Vallée d'Aoste e le Province autonome di Bolzano e di Trento hanno promosso, tra le altre, questioni di legittimità costituzionale delle norme contenute negli artt. 8, 46 e 47 del decreto-legge 24 aprile 2014, n. 66 (Misure urgenti per la competitività e la giustizia sociale), convertito, con modificazioni, dall'art. 1, comma 1, della legge 23 giugno 2014, n. 89;&#13;
 che, in particolare, la Regione autonoma Valle d'Aosta/Vallée d'Aoste ha impugnato gli artt. 8, commi 4, lettera a), 6, 8 e 10, e 46, commi 1, 2, 3, 4 e 6, del d.l. n. 66 del 2014, come convertito, nella parte in cui impongono (nella formulazione vigente al momento della proposizione del ricorso), anche alle Regioni a statuto speciale riduzioni di spesa per acquisti di beni e servizi, nonché nella parte in cui aumentano gli importi da computare in riduzione al complesso delle spese finali (nell'ambito della determinazione dell'obiettivo del patto di stabilità in termini di competenza euro compatibile) e quelli relativi al concorso delle autonomie speciali al risanamento della finanza pubblica;&#13;
 che la Regione ricorrente sostiene che il complesso delle disposizioni impugnate inciderebbe unilateralmente in materia riservata alla normativa di attuazione dello Statuto speciale, in violazione del principio pattizio, dell'autonomia finanziaria e organizzativa regionale, del principio di ragionevolezza, del principio di leale collaborazione di cui agli artt. 5 e 120 della Costituzione nonché degli artt. 117, terzo comma, e 119 Cost., in combinato disposto con l'art. 10 della legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte II della Costituzione);&#13;
 che la Provincia autonoma di Bolzano ha impugnato gli artt. 8, commi 4, 6 e 10, e 46, commi 1, 2, 3, 4 e 6, del d.l. n. 66 del 2014, come convertito, nella parte in cui tali disposizioni impongono riduzioni di spesa per acquisti di beni e servizi, nonché nella parte in cui aumentano gli importi da computare in riduzione al complesso delle spese finali (nell'ambito della determinazione dell'obiettivo del patto di stabilità in termini di competenza euro compatibile) e quelli relativi al concorso delle autonomie speciali al risanamento della finanza pubblica;&#13;
 che la Provincia ricorrente sostiene che il complesso delle disposizioni impugnate inciderebbe unilateralmente in materia riservata alla normativa di attuazione dello statuto speciale, in violazione del principio pattizio, dell'autonomia finanziaria e organizzativa provinciale, del principio di ragionevolezza, di leale collaborazione nonché degli artt. 81, 117, terzo comma, 118, 119 e 120 Cost., in combinato disposto con l'art. 1 della legge costituzionale n. 3 del 2001;&#13;
 che la Provincia autonoma di Trento ha impugnato gli artt. 8, commi 4, 6, 7 e 10, 46, commi 1, 2, 3 e 6, e 47, commi 8, 9, 11 e 12, del d.l. n. 66 del 2014, come convertito, nella parte in cui impongono riduzioni di spesa per acquisti di beni e servizi, oltre ad aumentare gli importi da computare in riduzione al complesso delle spese finali (nell'ambito della determinazione dell'obiettivo del patto di stabilità in termini di competenza euro compatibile) e quelli relativi al concorso delle autonomie speciali al risanamento della finanza pubblica;&#13;
 che la Provincia ricorrente sostiene che il complesso delle disposizioni impugnate inciderebbe unilateralmente in materia riservata alla normativa di attuazione dello statuto speciale, in violazione dei principi dell'accordo in materia finanziaria, di ragionevolezza, di leale collaborazione e di autonomia finanziaria e organizzativa provinciale, nonché dell'art. 117, terzo comma, Cost. in combinato disposto con l'art. 10 della legge costituzionale n. 3 del 2001;&#13;
 che con riguardo a tutti i ricorsi, ad eccezione di quello proposto dalla Provincia autonoma di Bolzano, si è costituito il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendone il rigetto, poiché le censure mosse alle norme impugnate sarebbero inammissibili o, comunque, non fondate;&#13;
 che successivamente, a seguito dell'accordo in materia di finanza pubblica raggiunto con il Governo il 15 ottobre 2014, le Province autonome di Trento e di Bolzano hanno rinunciato ai rispettivi ricorsi;&#13;
 che in ragione dell'accordo raggiunto con lo Stato in data 21 luglio 2015, in materia di finanza pubblica, anche la Regione autonoma Valle d'Aosta/Vallée d'Aoste ha rinunciato al ricorso;&#13;
 che tutte dette rinunce sono state accettate dal Presidente del Consiglio dei ministri.&#13;
 Considerato che i ricorsi indicati in epigrafe vanno riuniti, poiché riguardano, in larga misura, le medesime norme, censurate in riferimento a parametri, in buona parte, coincidenti; &#13;
 che, con riguardo alle questioni prima richiamate, le Province autonome di Trento e di Bolzano, nonché la Regione autonoma Valle d'Aosta/Vallée d'Aoste, hanno presentato rinuncia, e che tali rinunce sono state accettate ad opera del Presidente del Consiglio dei ministri; &#13;
 che la rinuncia al ricorso, accettata dalla controparte costituita, determina, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 riuniti i giudizi, &#13;
 dichiara estinti i processi. &#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 febbraio 2016.&#13;
 F.to:&#13;
 Alessandro CRISCUOLO, Presidente&#13;
 Nicolò ZANON, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 25 febbraio 2016.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
