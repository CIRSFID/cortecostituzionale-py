<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>225</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:225</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Brunetto Bucciarelli Ducci</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/07/1984</data_decisione>
    <data_deposito>25/07/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Prof. LIVIO PALADIN - Dott. &#13;
 ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. &#13;
 GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  degli  artt.    151,  &#13;
 secondo  e  terzo comma, e 263 bis del codice di procedura penale; art.  &#13;
 10 del d.P.R. 25  ottobre  1955,  n.    932  (Norme  di  attuazione  di  &#13;
 coordinamento   della   legge  18  giugno  1955,  n.  517,  concernente  &#13;
 modificazioni al codice di procedura  penale)  promosso  con  ordinanza  &#13;
 emessa  il  30 dicembre 1976 dal giudice istruttore presso il Tribunale  &#13;
 di Salerno nel procedimento penale a carico di Petti Luigi iscritta  al  &#13;
 n.  109  del  registro  ordinanze  1977  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 107 dell'anno 1977;                        &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  dell'11 aprile 1984 il Giudice  &#13;
 relatore Brunetto Bucciarelli Ducci.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza del 30 dicembre 1976 il giudice  istruttore  del  &#13;
 Tribunale  di  Salerno,  nel  corso del procedimento penale a carico di  &#13;
 Luigi  Petti  (imputato  di   bancarotta   fraudolenta   e   violazione  &#13;
 dell'obbligo   di   residenza),  sollevava  questione  di  legittimità  &#13;
 costituzionale,  in  relazione  all'art.  24,  secondo   comma,   della  &#13;
 Costituzione,  degli artt. 151, secondo e terzo comma, 263 bis C.p.p. e  &#13;
 10 d.P.R. 25 ottobre 1955, n.  932, nella parte in cui da un  lato  non  &#13;
 prevedono  il  deposito  in  cancelleria del mandato di cattura rimasto  &#13;
 ineseguito e del verbale di vane ricerche ed il correlativo diritto del  &#13;
 difensore all'avviso, dall'altro dispongono la decorrenza nei confronti  &#13;
 del difensore del termine per proporre ricorso dalla data  del  verbale  &#13;
 di  vane  ricerche  e  non  dal  deposito in cancelleria del mandato di  &#13;
 cattura e del verbale stesso.                                            &#13;
     Secondo il giudice a quo tali disposizioni violano  il  diritto  di  &#13;
 difesa,  non consentendo al difensore di un imputato  latitante colpito  &#13;
 da mandato di cattura di aver conoscenza del provvedimento soggetto  ad  &#13;
 impugnazione  ed  estendendo  al  difensore stesso la presunzione iuris  &#13;
 tantum  di  conoscenza  del  provvedimento  fondata  sulla   volontaria  &#13;
 sottrazione  dell'imputato  all'esecuzione  di  esso.   Ne risulterebbe  &#13;
 così impedita - secondo il Tribunale - l'esplicazione  dell'assistenza  &#13;
 tecnica  in  ogni  stato  e  grado  del  procedimento, che è una delle  &#13;
 garanzie essenziali del diritto alla difesa.                             &#13;
     2. - È intervenuto in giudizio il  Presidente  del  Consiglio  dei  &#13;
 ministri,  rappresentato  e  difeso dall'Avvocato Generale dello Stato,  &#13;
 assumendo l'infondatezza della questione sollevata.                      &#13;
     Qualora,  infatti  -  argomenta  l'Avvocatura  -   fosse   previsto  &#13;
 l'obbligo  del  deposito  in cancelleria del mandato di cattura rimasto  &#13;
 ineseguito e del verbale di vane ricerche, si  offenderebbe  l'evidente  &#13;
 interesse  alla  segretezza  sull'emissione del mandato e sulle ragioni  &#13;
 che l'hanno motivato, consentendo all'imputato latitante  di  inquinare  &#13;
 le  prove  e  ponendolo  in  una  situazione  di  ingiustificato favore  &#13;
 rispetto all'imputato che  venga  a  conoscenza  del  mandato  solo  al  &#13;
 momento del suo arresto.  D'altra parte - osserva la difesa dello Stato  &#13;
 - il termine per l'impugnazione del mandato stesso non decorre, in caso  &#13;
 di  latitanza,  dalla  data del verbale di vane ricerche, ma dal giorno  &#13;
 della consegna del mandato all'imputato, come ha ritenuto la Cassazione  &#13;
 (Sez.  IV, 17 marzo 1976, n. 387, riportata più oltre).<diritto>Considerato in diritto</diritto>:                          &#13;
     1. -  Viene  sottoposta  all'esame  della  Corte  la  questione  se  &#13;
 contrastino  o  meno con l'art. 24, secondo comma, della  Costituzione,  &#13;
 gli artt. 151, secondo e terzo comma, 263 bis C.p.p. e  10  d.P.R    25  &#13;
 ottobre  1955,  n. 932, nella parte in cui non prevedono il deposito in  &#13;
 cancelleria del mandato di cattura rimasto ineseguito e del verbale  di  &#13;
 vane  ricerche ed il correlativo diritto del difensore all'avviso ed in  &#13;
 cui dispongono la decorrenza del termine per proporre ricorso da  parte  &#13;
 del  difensore  stesso  contro  il  mandato  di  cattura dalla data del  &#13;
 verbale  di  vane  ricerche,  anziché  da  quella  del   deposito   in  &#13;
 cancelleria  del  mandato di cattura e del suddetto verbale.  Dubita il  &#13;
 giudice istruttore del  Tribunale  di  Salerno  che  tali  disposizioni  &#13;
 ledano  il  diritto  di  difesa,  rendendo  impossibile  l'esplicazione  &#13;
 dell'assistenza tecnica nel caso di mandato di cattura a carico  di  un  &#13;
 imputato latitante.                                                      &#13;
     Tale  lesione  si  determinerebbe  -  si  osserva nell'ordinanza di  &#13;
 rimessione - in quanto la giurisprudenza ordinaria si sarebbe da  tempo  &#13;
 consolidata  in  un'interpretazione  restrittiva delle norme impugnate,  &#13;
 negando al difensore il diritto all'avviso e al deposito del mandato di  &#13;
 cattura e del verbale di vane ricerche e facendo decorrere  il  termine  &#13;
 per  proporre ricorso, in caso di volontaria sottrazione all'esecuzione  &#13;
 del mandato da parte dell'imputato, dalla data del suddetto verbale, di  &#13;
 cui entrambi, imputato e difensore, possono essere del tutto ignari.     &#13;
     2. - La questione, come proposta, non è fondata.                    &#13;
     La lesione lamentata, infatti,  discende  dal  presupposto  che  le  &#13;
 norme  denunciate pongano l'imputato e il difensore nella condizione di  &#13;
 ignorare l'evento che costituisce  il    termine  a  quo  per  proporre  &#13;
 impugnazione  avverso  il  mandato  di  cattura, in quanto la latitanza  &#13;
 stessa dell'imputato comporta l'ipotesi che egli  possa  non  essere  a  &#13;
 conoscenza  della  data  del  verbale  di  vane  ricerche, che registra  &#13;
 appunto  la   mancata   esecuzione   del   mandato   di   cattura   per  &#13;
 l'impossibilità di reperire il destinatario.                            &#13;
     Senonché  è  proprio  tale presupposto giuridico a fare  difetto.  &#13;
 La  giurisprudenza  ordinaria   ha   subito,   infatti,   un'evoluzione  &#13;
 allontanandosi dalla interpretazione restrittiva esposta nell'ordinanza  &#13;
 di  rimessione  ed orientandosi invece, con una tendenza ormai uniforme  &#13;
 da molti anni e tale comunque da costituire allo stato diritto vivente,  &#13;
 nel  senso che il termine per impugnare il mandato di cattura decorre -  &#13;
 ai sensi dell'impugnato art. 10 d.P.R.  n.  932  del  1955  -  non  dal  &#13;
 verbale  di  vane ricerche, ma dalla consegna o dalla notificazione del  &#13;
 mandato di cattura o comunque dal momento in cui  l'imputato  latitante  &#13;
 ne  abbia  avuto  piena  conoscenza.    La  ratio della norma citata si  &#13;
 informa, secondo la Corte di Cassazione, al principio generale  che  il  &#13;
 termine  per  impugnare  un  provvedimento  non  può che decorrere dal  &#13;
 giorno in cui esso viene formalmente conosciuto  dalla  parte,  cui  la  &#13;
 legge riconosce il diritto di impugnarlo.                                &#13;
     La   normativa   vigente,   pertanto,   non  modificata  sul  punto  &#13;
 dall'entrata in vigore della legge 12 agosto 1982,  n.  532  (Tribunale  &#13;
 della  libertà),  garantisce all'imputato latitante e al suo difensore  &#13;
 la conoscenza del momento iniziale della  decorrenza  del  termine  per  &#13;
 impugnare   il  mandato  di  cattura,  ponendolo  nelle  condizioni  di  &#13;
 esercitare quel diritto  alla    difesa  tutelato  dall'art.  24  della  &#13;
 Costituzione.                                                            &#13;
     Questa  Corte, peraltro, ha avuto occasione di affermare più volte  &#13;
 come il  diritto  di  difesa  possa,  quanto  alle  modalità  del  suo  &#13;
 esercizio,  essere  dal  legislatore  diversamente regolato ed adattato  &#13;
 alle speciali esigenze dei diversi procedimenti, purché non  ne  siano  &#13;
 pregiudicati lo scopo e le funzioni (sentenze nn. 159 del 1972 e 80 del  &#13;
 1984).                                                                   &#13;
     Ora  è innegabile che nell'ipotesi della latitanza dell'imputato -  &#13;
 come nel caso della normativa impugnata - la tutela  del  diritto  alla  &#13;
 difesa debba essere contemperata  dall'esigenza, altrettanto meritevole  &#13;
 di  tutela,  di  assicurare  la segretezza delle ricerche dell'imputato  &#13;
 stesso.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata, nei sensi di cui in motivazione, la questione  &#13;
 di legittimità costituzionale degli artt. 151, secondo e terzo  comma,  &#13;
 263  bis  C.p.p.  e  10  d.P.R.  25  ottobre 1955, n. 932, sollevata in  &#13;
 riferimento all'art. 24 della Costituzione dal giudice  istruttore  del  &#13;
 Tribunale di Salerno con l'ordinanza indicata in epigrafe.               &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 13 luglio 1984.                               &#13;
                                   F.to: LEOPOLDO  ELIA  -  ANTONINO  DE  &#13;
                                   STEFANO   -   GUGLIELMO  ROEHRSSEN  -  &#13;
                                   ORONZO REALE -  BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI   -  LIVIO  PALADIN  -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - GIUSEPPE FERRARI  &#13;
                                   - FRANCESCO SAJA - GIOVANNI  CONSO  -  &#13;
                                   ETTORE GALLO - ALDO CORASANITI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
