<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>270</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:270</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/12/1976</data_decisione>
    <data_deposito>29/12/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale dell'art.  13  &#13;
 del r.d.l. 14 aprile 1939, n. 636 (sull'assicurazione obbligatoria  per  &#13;
 l'invalidità e la vecchiaia), convertito nella legge 6 luglio 1939, n.  &#13;
 1272,  sostituito con l'art. 2 della legge 4 aprile 1952, n. 218, e con  &#13;
 l'art. 22, quinto comma, della legge 21 luglio 1965, n.  903,  promosso  &#13;
 con le seguenti ordinanze:                                               &#13;
     1.  -  ordinanza emessa il 16 ottobre 1973 dal tribunale di Genova,  &#13;
 nel  procedimento  civile  vertente  tra  Parodi  Emilio  e  l'Istituto  &#13;
 nazionale  della  previdenza  sociale,  iscritta al n. 137 del registro  &#13;
 ordinanze 1974 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n.  146 del 5 giugno 1974;                                               &#13;
     2.  - ordinanza emessa il 21 maggio 1974 dal giudice del lavoro del  &#13;
 tribunale di Milano,  nel  procedimento  civile  vertente  tra  Vailati  &#13;
 Antonio e l'Istituto nazionale della previdenza sociale, iscritta al n.  &#13;
 380  del  registro ordinanze 1974 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 289 del 6 novembre 1974.                             &#13;
     Udito nella camera di consiglio del  28  ottobre  1976  il  Giudice  &#13;
 relatore Ercole Rocchetti.                                               &#13;
     Ritenuto  che  con ordinanza 16 ottobre 1973, pronunciata nel corso  &#13;
 della causa civile vertente tra Parodi Emilio  e  l'Istituto  nazionale  &#13;
 della previdenza sociale, il tribunale di Genova ha sollevato questione  &#13;
 di  legittimità costituzionale dell'art. 13 del r.d.l. 14 aprile 1939,  &#13;
 n. 636, convertito nella legge 6 luglio 1939, n.  1272, sostituito  con  &#13;
 l'art.  2  della  legge  4 aprile 1952, n.   218, e con l'art. 22 della  &#13;
 legge 21  luglio  1965,  n.  903,  per  violazione  dell'art.  3  della  &#13;
 Costituzione;                                                            &#13;
     che   con   l'ordinanza  21  maggio  1974,  emessa  nel  corso  del  &#13;
 procedimento civile vertente tra Vailati Antonio e l'Istituto nazionale  &#13;
 della previdenza sociale, il giudice del lavoro del tribunale di Milano  &#13;
 ha impugnato la stessa disposizione  di  legge,  con  riferimento  agli  &#13;
 artt. 3, 29, secondo comma, 31, primo comma, e 38, secondo comma, della  &#13;
 Costituzione;                                                            &#13;
     che,  nei  rispettivi giudizi dinanzi alla Corte costituzionale, si  &#13;
 sono costituiti il sig. Emilio Parodi, col  patrocinio  degli  avvocati  &#13;
 Benedetto  Bussi  e Salvatore Marino, e il sig. Antonio Vailati a mezzo  &#13;
 degli avvocati  Angelo  Fumarola  e  Benedetto  Bussi,  i  quali  hanno  &#13;
 concluso  per  la  dichiarazione di illegittimità costituzionale della  &#13;
 norma impugnata;                                                         &#13;
     che, inoltre, in entrambi i giudizi, si  è  costituito  l'Istituto  &#13;
 nazionale  della  previdenza  sociale, col patrocinio degli avvocati G.  &#13;
 Battista Doria e Giulio Abati, il quale  ha  chiesto,  invece,  che  la  &#13;
 dedotta  questione  di  legittimità  costituzionale sia dichiarata non  &#13;
 fondata.                                                                 &#13;
     Considerato che la stessa questione di legittimità  costituzionale  &#13;
 (che  investe  la norma impugnata nella parte in cui, nell'ambito della  &#13;
 disciplina   delle   pensioni   dell'assicurazione   obbligatoria   per  &#13;
 l'invalidità,  la  vecchiaia  e  i superstiti, dispone che, se viene a  &#13;
 morte un pensionato o assicurato e  se  superstite  è  il  marito,  la  &#13;
 pensione  di  riversibilità  è  a questo corrisposta, nel caso in cui  &#13;
 esso sia riconosciuto invalido al  lavoro  ai  sensi  del  primo  comma  &#13;
 dell'art.  10  del r.d.l. n. 636 del 1939) è stata già dichiarata non  &#13;
 fondata, con riferimento agli artt. 3, 29, 37 e 38 della  Costituzione,  &#13;
 con  sentenza  n.  201 del 29 dicembre 1972 e manifestamente infondata,  &#13;
 con riferimento all'art. 3 della Costituzione, con ordinanza n. 50  del  &#13;
 27 febbraio 1974;                                                        &#13;
     che  non sussistono ragioni che inducano a modificare la precedente  &#13;
 decisione;                                                               &#13;
     che gli argomenti svolti nella predetta decisione valgono anche per  &#13;
 quanto riguarda il profilo della illegittimità della norma  denunciata  &#13;
 con  riferimento all'art. 31, primo comma, della Costituzione, il quale  &#13;
 non assume autonomo rilievo rispetto alle altre censure già  esaminate  &#13;
 e disattese dalla Corte.                                                 &#13;
     Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.  &#13;
 87, e l'art. 9, secondo comma, delle Norme integrative  per  i  giudizi  &#13;
 davanti a questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale dell'art. 13 del r.d.l.  14  aprile  1939,  numero  636,  &#13;
 convertito  nella  legge 6 luglio 1939, n. 1272, sostituito dall'art. 2  &#13;
 della legge 4 aprile 1952, n. 218, e dall'art. 22 della legge 21 luglio  &#13;
 1965, n. 903; questione proposta, con le  ordinanze  in  epigrafe,  dal  &#13;
 tribunale  di  Genova e dal giudice del lavoro del tribunale di Milano,  &#13;
 in riferimento agli artt. 3, 29, secondo comma, 31, primo comma, e  38,  &#13;
 secondo comma, della Costituzione.                                       &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 21 dicembre 1976.       &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO - LEOPOLDO ELIA.            &#13;
                                   ARDUINO SALLUSTRI - Cancelliere.</dispositivo>
  </pronuncia_testo>
</pronuncia>
