<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>344</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:344</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/07/1994</data_decisione>
    <data_deposito>25/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, prof. Giuliano VASSALLI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art.  30,  quarto  e    &#13;
 quinto  comma,  della  legge  6  agosto  1990, n. 223 (Disciplina del    &#13;
 sistema radiotelevisivo pubblico e privato), promosso  con  ordinanza    &#13;
 emessa  il  26  ottobre  1993 dal giudice per le indagini preliminari    &#13;
 presso il Tribunale di Milano nel procedimento  penale  a  carico  di    &#13;
 Incerti  Caselli  Patrizia  ed  altri, iscritta al n. 49 del registro    &#13;
 ordinanze 1994 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 9, prima serie speciale, dell'anno 1994;                              &#13;
    Visto  l'atto  di  costituzione di Randazzo Rosa nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
    Udito nell'udienza pubblica del 21 giugno 1994 il Giudice relatore    &#13;
 Enzo Cheli;                                                              &#13;
    Udito l'avvocato Armando Costa  per  Randazzo  Rosa  e  l'Avvocato    &#13;
 dello  Stato  Antonino  Freni  per  il  Presidente  del Consiglio dei    &#13;
 Ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Il giudice per le indagini preliminari presso il Tribunale di    &#13;
 Milano, sciogliendo la riserva formulata su  una  eccezione  avanzata    &#13;
 dal  difensore della parte civile nel corso dell'udienza preliminare,    &#13;
 con ordinanza del 26 ottobre 1993 (R.O. n. 49 del 1994), ha sollevato    &#13;
 questione di legittimità costituzionale dell'art. 30, commi quarto e    &#13;
 quinto, della legge 6 agosto 1990, n.  223  (Disciplina  del  sistema    &#13;
 radiotelevisivo  pubblico  e privato), in relazione agli artt. 3 e 25    &#13;
 della Costituzione.                                                      &#13;
    Il comma quarto dell'art. 30 disciplina la  diffamazione  commessa    &#13;
 "attraverso  trasmissioni  consistenti  nell'attribuzione di un fatto    &#13;
 determinato" e punisce questa ipotesi - applicabile al concessionario    &#13;
 e al delegato al controllo sulla trasmissione - con la pena  prevista    &#13;
 dall'art. 13 della legge sulla stampa (legge 8 febbraio 1948, n. 47).    &#13;
 Il  comma  quinto  dello  stesso  articolo  detta  regole processuali    &#13;
 mediante rinvio all'art. 21 della citata legge sulla stampa e  introduce,  solo  per  la  diffamazione  mediante trasmissioni consistenti    &#13;
 nell'attribuzione di un fatto determinato,  una  deroga  al  criterio    &#13;
 generale  che  ispira la disciplina della competenza territoriale. La    &#13;
 competenza viene, infatti, in questo caso, radicata presso il giudice    &#13;
 del luogo dove risiede la parte offesa e non nel luogo dove il  reato    &#13;
 è stato consumato.                                                      &#13;
    Il  giudice  a  quo,  nel  motivare  in  ordine alla non manifesta    &#13;
 infondatezza della questione, osserva che il criterio adottato  nella    &#13;
 specie  dal  legislatore non rientra neppure tra quelli indicati come    &#13;
 residuali dal codice di procedura penale, venendo, di conseguenza,  a    &#13;
 introdurre  una  deroga  irragionevole al criterio generale, valevole    &#13;
 solo per la diffamazione  aggravata  dall'attribuzione  di  un  fatto    &#13;
 determinato  commessa  con il mezzo radiotelevisivo. Sempre ad avviso    &#13;
 del  giudice  remittente,  i  lavori  parlamentari  non  fornirebbero    &#13;
 elementi  utili  a chiarire se tale deroga risponda ad un particolare    &#13;
 favor per la parte offesa  da  tale  delitto  o  sia  frutto  di  una    &#13;
 dimenticanza rispetto agli altri reati.                                  &#13;
    Viene, di conseguenza, prospettata la violazione dell'art. 3 della    &#13;
 Costituzione, per la disparità di trattamento che la norma impugnata    &#13;
 introdurrebbe  rispetto  a  tutti gli altri reati commessi attraverso    &#13;
 l'uso del mezzo radiotelevisivo, e,  in  particolare,  rispetto  alla    &#13;
 diffamazione semplice.                                                   &#13;
    Il  giudice remittente osserva poi che l'autorità giudiziaria del    &#13;
 locus commissi delicti non può  conoscere  del  reato  in  tutte  le    &#13;
 ipotesi  in  cui l'evento non coincide con la residenza dell'offeso e    &#13;
 questo verrebbe  a  determinare  la  violazione  dell'art.  25  della    &#13;
 Costituzione,  in  base  al  quale  la  disciplina  della  competenza    &#13;
 andrebbe in qualche modo ancorata al luogo di consumazione del reato.    &#13;
    2. - Si è ritualmente costituita la parte offesa  Randazzo  Rosa,    &#13;
 riportandosi all'ordinanza di rimessione.                                &#13;
    Nell'atto  di costituzione la parte privata - dopo aver richiamato    &#13;
 i fatti che hanno dato luogo al giudizio a quo - sviluppa le  censure    &#13;
 relative sia all'art. 3 che all'art. 25 della Costituzione.              &#13;
    3.  -  Nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  che ha concluso per l'inammissibilità e, in via subordinata,    &#13;
 per l'infondatezza della questione.                                      &#13;
    Quanto  all'inammissibilità,  l'Avvocatura  rileva   il   difetto    &#13;
 assoluto  di  motivazione  dell'ordinanza  circa  la  rilevanza della    &#13;
 questione ai fini  della  risoluzione  della  fattispecie  sottoposta    &#13;
 all'esame  del  giudice  a  quo,  difetto  che  non  consentirebbe di    &#13;
 comprendere neanche se, nell'ipotesi di accoglimento,  detto  giudice    &#13;
 sia tenuto o meno a spogliarsi della controversia.                       &#13;
    Nel  merito,  L'Avvocatura ritiene che la scelta discrezionalmente    &#13;
 operata dal legislatore con la norma impugnata non  sia  censurabile,    &#13;
 né   sotto   il   profilo   della   razionalità  né  sotto  quello    &#13;
 dell'eguaglianza.                                                        &#13;
    La difesa dello Stato si sofferma,  in  particolare,  sulla  ratio    &#13;
 della  disposizione, rinvenendola nell'esigenza di porre rimedio alla    &#13;
 sproporzione  di  forze  esistente  tra  chi,  disponendo  del  mezzo    &#13;
 televisivo,  pone  in  essere  condotte  diffamatorie particolarmente    &#13;
 gravi e lesive per il soggetto diffamato,  e  quest'ultimo,  che  ha,    &#13;
 invece, come unico mezzo di reazione la presentazione della querela.     &#13;
    In   questa   situazione  l'eventuale  decisione  favorevole  resa    &#13;
 dall'autorità giudiziaria vicina al luogo di residenza abituale  del    &#13;
 soggetto   offeso   potrebbe,   pertanto,   restituire  a  questi  la    &#13;
 reputazione lesa e colmare la sottolineata sproporzione.<diritto>Considerato in diritto</diritto>1. - Il giudice per le indagini preliminari presso il Tribunale di    &#13;
 Milano dubita della legittimità costituzionale dell'art.  30,  commi    &#13;
 quarto  e  quinto,  della legge 6 agosto 1990, n. 223 (Disciplina del    &#13;
 sistema radiotelevisivo pubblico e privato), dove  si  individua  con    &#13;
 riferimento  al  luogo  di residenza della persona offesa l'autorità    &#13;
 giudiziaria territorialmente competente  a  conoscere  dei  reati  di    &#13;
 diffamazione  consistenti  nell'attribuzione di un fatto determinato,    &#13;
 commessi attraverso trasmissioni radiofoniche e televisive.              &#13;
    La questione viene sollevata in relazione:                            &#13;
       a)  all'art.  3  della  Costituzione,  per  la  disparità   di    &#13;
 trattamento  rispetto  a  tutti  gli  altri reati commessi attraverso    &#13;
 l'uso del mezzo radiotelevisivo  e,  in  particolare,  rispetto  alla    &#13;
 diffamazione semplice;                                                   &#13;
       b) all'art. 25 della Costituzione, dal momento che il principio    &#13;
 del  "giudice  naturale" richiederebbe che la competenza territoriale    &#13;
 sia in qualche modo ancorata al luogo di consumazione del reato.         &#13;
    2. - La questione è inammissibile.                                   &#13;
    Il giudice remittente - così come eccepito dalla  Presidenza  del    &#13;
 Consiglio  dei  Ministri  -  ha  omesso  completamente di motivare in    &#13;
 ordine alla rilevanza della questione ai fini della risoluzione della    &#13;
 concreta fattispecie sottoposta al suo esame, né ha esposto i  fatti    &#13;
 che  hanno  dato  luogo  al  giudizio,  così  da  poter identificare    &#13;
 l'oggetto e i termini dello stesso.                                      &#13;
    Lo svolgimento dei motivi espressi  nell'ordinanza  di  rimessione    &#13;
 non   consente,   d'altro   canto,  di  ricostruire  con  certezza  i    &#13;
 presupposti che renderebbero la questione pregiudiziale  e  rilevante    &#13;
 rispetto al giudizio a quo, stante anche la contraddittorietà che è    &#13;
 dato  rilevare  tra  la  motivazione  ed  il dispositivo della stessa    &#13;
 ordinanza. Mentre da un  lato,  infatti,  in  alcuni  passaggi  della    &#13;
 motivazione,  l'ordinanza sembra orientata a richiedere una pronuncia    &#13;
 diretta a estendere la particolare competenza  territoriale  prevista    &#13;
 dal  quinto comma dell'art. 30 quantomeno alla diffamazione semplice,    &#13;
 dall'altro, il dispositivo della stessa ordinanza si limita,  invece,    &#13;
 a   circoscrivere  la  domanda  alla  sola  caducazione  della  norma    &#13;
 impugnata, così da ricondurre  anche  l'ipotesi  della  diffamazione    &#13;
 aggravata al regime ordinario della competenza.                          &#13;
    L'impossibilità   di   valutare,   ai  fini  del  giudizio  sulla    &#13;
 rilevanza, la connessione tra i fatti dedotti nel processo a quo e la    &#13;
 questione  di  costituzionalità  sollevata   conduce,   dunque,   ad    &#13;
 affermare l'inammissibilità della stessa.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 30, commi quarto e quinto, della legge 6  agosto  1990,  n.    &#13;
 223  (Disciplina  del  sistema  radiotelevisivo  pubblico e privato),    &#13;
 sollevata, in relazione agli artt. 3 e  25  della  Costituzione,  dal    &#13;
 giudice per le indagini preliminari presso il Tribunale di Milano con    &#13;
 l'ordinanza di cui in epigrafe.                                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 luglio 1994.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: CHELI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 25 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
