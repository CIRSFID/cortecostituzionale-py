<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>214</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:214</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>MORTATI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/12/1972</data_decisione>
    <data_deposito>30/12/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. COSTANTINO MORTATI, Presidente - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. &#13;
 ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI - Avv. LEONETTO AMADEI - Prof. GIULIO GIONFRIDA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1 della legge  &#13;
 9  novembre  1955,  n.  1122  (disposizioni  varie  per la previdenza e  &#13;
 assistenza sociale attuate dall'Istituto nazionale  di  previdenza  dei  &#13;
 giornalisti  italiani  "Giovanni  Amendola"),  promosso  con  ordinanza  &#13;
 emessa il 6 aprile  1970  dal  pretore  di  Roma  nel  procedimento  di  &#13;
 esecuzione mobiliare vertente tra l'Esattoria comunale di Roma e Giulia  &#13;
 Mario  Mariano,  iscritta  al  n.  283  del  registro  ordinanze 1970 e  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  267  del  21  &#13;
 ottobre 1970.                                                            &#13;
     Visti  gli  atti  di costituzione dell'Esattoria comunale di Roma e  &#13;
 d'intervento del Presidente del Consiglio dei ministri;                  &#13;
     udito  nell'udienza  pubblica  del  22  novembre  1972  il  Giudice  &#13;
 relatore Ercole Rocchetti;                                               &#13;
     uditi  l'avv.  Giuseppe  Mesiano,  per l'Esattoria, ed il sostituto  &#13;
 avvocato generale dello Stato Michele Savarese, per il  Presidente  del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  del  procedimento  esecutivo  proposto  dalla Esattoria  &#13;
 comunale di Roma nei confronti di Giulia Mario Mariano per il  recupero  &#13;
 di  un  credito  tributario, il pretore di Roma ha ritenuto rilevante e  &#13;
 non  manifestamente  infondata,  con  riferimento  all'art.   3   della  &#13;
 Costituzione,  la  questione di legittimità costituzionale dell'art. 1  &#13;
 della legge 9 novembre 1955, n. 1122, che  dichiara  non  sequestrabili  &#13;
 né  pignorabili le somme corrisposte agli aventi diritto dall'Istituto  &#13;
 nazionale di previdenza dei giornalisti a titolo di  pensioni,  assegni  &#13;
 ed  altre  indennità,  e che perciò restano totalmente esenti da ogni  &#13;
 procedura esecutiva, anche per pagamento di tributi.                     &#13;
     Secondo il  giudice  a  quo,  la  norma  impugnata,  sottraendo  le  &#13;
 pensioni  dei  giornalisti  al  principio,  che  nel nostro ordinamento  &#13;
 avrebbe carattere generale, della pignorabilità, benché limitata, per  &#13;
 crediti d'imposta, degli  stipendi,  pensioni  ed  indennità,  avrebbe  &#13;
 introdotto una disparità di trattamento priva di fondamento razionale,  &#13;
 "avuto  riguardo  ai  criteri della logica e dell'equità e ai principi  &#13;
 giuridici enunciati dagli articoli  2  e  53  della  Costituzione".  Ed  &#13;
 invero,  l'intangibilità  delle  pensioni  dei  giornalisti  non  solo  &#13;
 sarebbe del tutto arbitraria, rispetto al diverso trattamento riservato  &#13;
 alle pensioni di altre  categorie  di  professionisti  e  degli  stessi  &#13;
 pubblici   dipendenti,  ma  costituirebbe  altresì  un  ingiustificato  &#13;
 privilegio, in quanto sottrae una  categoria  di  cittadini  ai  doveri  &#13;
 nascenti dal sistema tributario vigente.                                 &#13;
     L'ordinanza   è   stata   ritualmente   notificata   comunicata  e  &#13;
 pubblicata.                                                              &#13;
     Dinanzi alla Corte si è costituito l'Esattore del Comune  di  Roma  &#13;
 il  quale,  con  deduzioni  depositate  il  5  novembre 1970, ribadisce  &#13;
 sostanzialmente gli argomenti  contenuti  nella  ordinanza  di  rinvio,  &#13;
 deducendo   altresì   l'illegittimità   costituzionale   della  norma  &#13;
 impugnata, in relazione all'art.    53  della  Costituzione,  sotto  il  &#13;
 profilo  che  la  intangibilità delle pensioni dei giornalisti sottrae  &#13;
 tale categoria di contribuenti  all'osservanza  coattiva  del  precetto  &#13;
 costituzionale.                                                          &#13;
     Si  è  costituita  altresì,  in rappresentanza del Presidente del  &#13;
 Consiglio dei ministri, l'Avvocatura generale  dello  Stato,  con  atto  &#13;
 depositato  il  9  novembre  1970,  sostenendo  la  infondatezza  della  &#13;
 questione.                                                               &#13;
     L'Avvocatura  contesta  che  nel  nostro  ordinamento   esista   un  &#13;
 principio    generale   relativo   alla   parziale   pignorabilità   e  &#13;
 sequestrabilità delle pensioni, perché, accanto alle disposizioni che  &#13;
 prevedono la loro pignorabilità nella misura  del  quinto,  esiste  il  &#13;
 principio generale, sancito nell'art. 128 del r.d.l. 4 ottobre 1935, n.  &#13;
 1827,   che   stabilisce   la   incedibilità,   insequestrabilità   e  &#13;
 impignorabilità delle pensioni di previdenza sociale, e  al  quale  il  &#13;
 legislatore   si   è   ispirato   nel   disciplinare   il  trattamento  &#13;
 previdenziale dei  giornalisti.  Il  fatto  poi  che,  per  i  pubblici  &#13;
 dipendenti  e  per  alcune  categorie  di  liberi  professionisti,  sia  &#13;
 prevista una diversa  disciplina,  può  essere  agevolmente  spiegato,  &#13;
 secondo  la difesa dello Stato, con il particolare status di lavoratore  &#13;
 dipendente da riconoscersi ai giornalisti, profondamente diverso sia da  &#13;
 quello dei pubblici impiegati che dei liberi professionisti.             &#13;
     All'udienza le parti costituite hanno concluso in conformità delle  &#13;
 loro precedenti deduzioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Il pretore di Roma, con l'ordinanza in epigrafe, denunzia alla  &#13;
 Corte l'art. 1 della legge 9  novembre  1955,  n.  1122,  che  dichiara  &#13;
 totalmente  insequestrabili  ed impignorabili le somme corrisposte agli  &#13;
 iscritti per  pensioni,  assegni  ed  altre  indennità,  dall'Istituto  &#13;
 nazionale  di previdenza dei giornalisti.  Secondo il giudice a quo, la  &#13;
 norma in tale articolo contenuta sarebbe illegittima nella parte in cui  &#13;
 esonera quelle somme anche  da  procedure  coattive  per  pagamento  di  &#13;
 tributi  ed entro la misura di un quinto di cui all'art. 545 del codice  &#13;
 di procedura civile. La  illegittimità  deriverebbe  dalla  violazione  &#13;
 dell'art.  3,  primo  comma,  della  Costituzione,  per  difformità di  &#13;
 trattamento rispetto a fattispecie analoghe, in quanto le somme erogate  &#13;
 per le medesime causali da casse di previdenza di professionisti, quali  &#13;
 avvocati, dottori commercialisti,  ragionieri,  geometri,  sono  invece  &#13;
 assoggettate  alle  stesse  disposizioni vigenti per i dipendenti delle  &#13;
 pubbliche amministrazioni, e perciò sequestrabili e  pignorabili,  per  &#13;
 crediti  nascenti  da  tributi  ed entro la misura di un quinto (art. 2  &#13;
 d.P.R. 5 gennaio 1950, n. 180).                                          &#13;
     2. - La questione non è fondata.                                    &#13;
     Va innanzi tutto rilevato che, nel  nostro  ordinamento,  non  può  &#13;
 ritenersi esistente, secondo sostiene il giudice a quo, un principio di  &#13;
 carattere  generale  relativo  alla  sequestrabilità  e pignorabilità  &#13;
 degli stipendi e pensioni  per  determinati  crediti,  tra  cui  quelli  &#13;
 relativi  al  pagamento  dei  tributi.   Accanto alle norme citate, che  &#13;
 ammettono la assoggettabilità ad atti coattivi di pensioni da  pagarsi  &#13;
 da  privati  o  da  pubbliche  Amministrazioni,  esistono  le norme che  &#13;
 escludono le  pensioni  di  qualsiasi  importo,  erogate  dall'Istituto  &#13;
 nazionale  della  previdenza  sociale, da ogni azione esecutiva, tranne  &#13;
 che per crediti verso lo stesso Istituto erogante (art.  128  r.d.l.  4  &#13;
 ottobre 1935, n. 1827, e art. 69 legge 30 aprile 1969, n. 153).          &#13;
     La  tesi  della eccezionalità della norma denunziata, che concerne  &#13;
 l'esonero della perseguibilità delle  pensioni  dei  giornalisti,  non  &#13;
 trova  quindi  conforto  nella  realtà  normativa  e  non può perciò  &#13;
 fornire la base di appoggio  alla  eccezione  di  illegittimità  della  &#13;
 stessa norma per violazione del principio di eguaglianza.                &#13;
     3.  - Parimenti è insussistente l'analogia che vi sarebbe, a detta  &#13;
 dell'ordinanza, fra la cassa di previdenza  dei  giornalisti  e  quelle  &#13;
 degli  avvocati,  dei  dottori  commercialisti,  dei  ragionieri  e dei  &#13;
 geometri.                                                                &#13;
     Invero, né i giornalisti sono liberi professionisti, né  la  loro  &#13;
 cassa di previdenza ha gli stessi compiti delle casse che gestiscono la  &#13;
 previdenza a favore dei sopraindicati esercenti professioni liberali.    &#13;
     È vero, peraltro, che dalla legge che disciplina la loro attività  &#13;
 (legge   3  febbraio  1963,  n.  69)  i  giornalisti  sono  qualificati  &#13;
 giornalisti-professionisti, ma tale denominazione è loro conferita  al  &#13;
 solo  fine  di  distinguerli  dai "pubblicisti", per quanto concerne la  &#13;
 professionalità dell'impegno di lavoro  dei  primi,  che  deve  essere  &#13;
 esclusivo  e continuativo, cosa che non occorre invece per quegli altri  &#13;
 che, unitamente all'attività giornalistica, possono  anche  esercitare  &#13;
 altre professioni o impieghi (art. 1, comma quarto, detta legge).        &#13;
     Comunque  sia  poi  in merito a tale qualificazione, certo è che i  &#13;
 giornalisti-professionisti sono lavoratori dipendenti, il cui  rapporto  &#13;
 di lavoro è regolato da contratti collettivi, onde è certo che liberi  &#13;
 professionisti o professionisti, nel senso tradizionale, essi non sono.  &#13;
     4.  -  Ancora meno sussiste poi una analogia tra la struttura e gli  &#13;
 scopi della cassa dei giornalisti e le finalità di quella  dei  liberi  &#13;
 professionisti di cui si è detto, perché la prima, e cioè l'Istituto  &#13;
 nazionale  di  previdenza  dei giornalisti italiani "Giovanni Amendola"  &#13;
 (legge 20 dicembre 1951,  n.  1564),  cui  possono  iscriversi  solo  i  &#13;
 giornalisti  che  hanno  in  atto  un rapporto di lavoro, sostituisce a  &#13;
 tutti gli effetti le corrispondenti forme di previdenza  ed  assistenza  &#13;
 obbligatorie  (art.  1) e cioè non solo quelle attinenti alla pensione  &#13;
 di  vecchiaia  e  invalidità,  ma  anche  quelle  che  concernono   la  &#13;
 disoccupazione  involontaria, la tubercolosi, le malattie e gli assegni  &#13;
 famigliari (art. 3), mentre le ricordate casse di liberi professionisti  &#13;
 hanno compiti ben più limitati e circoscritti.                          &#13;
     In  sostanza,  la  cassa  dei  giornalisti  costituisce  un settore  &#13;
 autonomo del complesso sistema previdenziale predisposto a  tutela  dei  &#13;
 lavoratori  dipendenti  e  i  cui  compiti  sono assolti principalmente  &#13;
 dall'INPS e dall'INAM.                                                   &#13;
     Appare perciò - come del tutto logico e naturale che  la  legge  9  &#13;
 novembre  1955,  n.  1122,  abbia esteso all'Istituto previdenziale dei  &#13;
 giornalisti  tutti  i  benefici,  privilegi  ed  esenzioni   tributarie  &#13;
 previsti per l'Istituto nazionale della previdenza sociale (art. 10), e  &#13;
 quindi  anche la norma impugnata, contenuta nell'art. 1, concernente la  &#13;
 insequestrabilità ed impignorabilità delle pensioni, la quale è  poi  &#13;
 anche  essa  una  disposizione  estensiva  della  normativa prevista in  &#13;
 materia per l'INPS dall'art. 128 del r.d.l. 4 ottobre 1935, n. 1827.     &#13;
     Deve quindi concludersi che la  lamentata  violazione  dell'art.  3  &#13;
 della Costituzione non sussiste.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 1 della legge 9 novembre 1955, n.   1122, avente  ad  oggetto  &#13;
 disposizioni  varie  per  la  previdenza e l'assistenza dei giornalisti  &#13;
 italiani, questione proposta con l'ordinanza in epigrafe in riferimento  &#13;
 all'art.  3, comma primo, della Costituzione.                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 18 dicembre 1972.                             &#13;
                                   COSTANTINO  MORTATI - GIUSEPPE  VERZÌ  &#13;
                                   -  GIOVANNI  BATTISTA   BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO AMADEI - GIULIO GIONFRIDA.    &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
