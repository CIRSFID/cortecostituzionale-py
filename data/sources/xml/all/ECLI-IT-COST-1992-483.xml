<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>483</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:483</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1992</data_decisione>
    <data_deposito>22/12/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Cesare GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  4-bis,  primo    &#13;
 comma,  prima  parte,  della  legge  26  luglio  1975,  n. 354 (Norme    &#13;
 sull'ordinamento  penitenziario  e  sulla  esecuzione  delle   misure    &#13;
 privative e limitative della libertà), introdotto dall'art. 1, primo    &#13;
 comma,  del  decreto-legge  13  maggio  1991,  n.  152 (Provvedimenti    &#13;
 urgenti  in  tema  di  lotta  alla  criminalità  organizzata  e   di    &#13;
 trasparenza   e   buon   andamento   dell'attività  amministrativa),    &#13;
 convertito dalla legge 12 luglio 1991, n. 203, promosso con ordinanza    &#13;
 emessa il 13 febbraio 1992 dal Tribunale di sorveglianza  di  Perugia    &#13;
 nel  procedimento  di  sorveglianza  promosso  da  Colavito Vittorio,    &#13;
 iscritta al n. 372 del registro ordinanze  1992  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  29, prima serie speciale,    &#13;
 dell'anno 1992;                                                          &#13;
    Udito nella camera di consiglio del 19 novembre  1992  il  Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto   che  il  Tribunale  di  sorveglianza  di  Perugia,  con    &#13;
 ordinanza del 13 febbraio 1992, ha sollevato, in riferimento all'art.    &#13;
 24, secondo comma,  della  Costituzione,  questione  di  legittimità    &#13;
 dell'art.  4-  bis,  primo  comma, prima parte, della legge 26 luglio    &#13;
 1975, n. 354, introdotto dall'art. 1, primo comma, del  decreto-legge    &#13;
 13  maggio  1991,  n.  152, convertito dalla legge 12 luglio 1991, n.    &#13;
 203, per la parte in cui prevede, in relazione  alle  istanze  intese    &#13;
 all'ottenimento della riduzione di pena per la liberazione anticipata    &#13;
 presentate  dai  condannati  per  delitti  commessi  per finalità di    &#13;
 terrorismo  o  di  eversione  dell'ordinamento  costituzionale,   per    &#13;
 delitti commessi avvalendosi delle condizioni previste dall'art. 416-bis  del  codice penale ovvero al fine di agevolare l'attività delle    &#13;
 associazioni previste dallo stesso articolo, nonché per i delitti di    &#13;
 cui agli artt. 416- bis e 630 del codice penale e all'articolo 74 del    &#13;
 testo unico delle leggi  in  materia  di  disciplina  delle  sostanze    &#13;
 stupefacenti  e  psicotrope,  prevenzione,  cura e riabilitazione dei    &#13;
 relativi stati  di  tossicodipendenza,  approvato  con  il  d.P.R.  9    &#13;
 ottobre 1990, n. 309, che simili istanze possano trovare accoglimento    &#13;
 "solo  se  sono  stati  acquisiti  elementi  tali  da  far  escludere    &#13;
 l'attualità  di  collegamenti  con  la  criminalità  organizzata  o    &#13;
 eversiva",  previa  richiesta da parte del magistrato di sorveglianza    &#13;
 ai competenti  comitati  provinciali  per  l'ordine  e  la  sicurezza    &#13;
 pubblica; considerato che questa Corte, con ordinanze n. 271 del 1992    &#13;
 e  n.  350  del  1992,  ha  già  dichiarato manifestamente infondate    &#13;
 analoghe questioni sollevate in riferimento agli artt. 3 e 27,  terzo    &#13;
 comma,  della  Costituzione,  in quanto il presupposto interpretativo    &#13;
 alla base delle censure di legittimità, presupposto secondo il quale    &#13;
 l'informativa del comitato provinciale per l'ordine  e  la  sicurezza    &#13;
 pubblica  avrebbe  carattere  vincolante per il giudice, così da non    &#13;
 rendere necessari ulteriori, più approfonditi accertamenti  ove  non    &#13;
 siano  stati  acquisiti  elementi  tali  da escludere l'attualità di    &#13;
 collegamenti con  la  criminalità  organizzata  o  eversiva  risulta    &#13;
 smentito   dalla   giurisprudenza  della  Corte  di  cassazione,  che    &#13;
 qualifica  la  detta  informativa  come  atto  obbligatorio  ma   non    &#13;
 vincolante,  potendo  il giudice, per un verso, trarre da altre fonti    &#13;
 gli  elementi  di  valutazione,  e  per  un  altro verso, dissentire,    &#13;
 purché con appropriate proposizioni interpretative, dal  parere  del    &#13;
 comitato;                                                                &#13;
      che,  peraltro,  dopo la pronuncia dell'ordinanza di rimessione,    &#13;
 è  entrato  in  vigore  il  decreto-legge  8  giugno  1992,  n.  306    &#13;
 (Modifiche   urgenti   al   nuovo   codice   di  procedura  penale  e    &#13;
 provvedimenti di contrasto  alla  criminalità  mafiosa),  convertito    &#13;
 dalla  legge  7 agosto 1992, n. 356, che ha, fra l'altro, "novellato"    &#13;
 la norma denunciata, sia facendo un'eccezione  al  regime  previgente    &#13;
 proprio   per   l'istituto   della  liberazione  anticipata  sia  con    &#13;
 l'innovare il metodo per la concessione dei benefici riguardo a tutte    &#13;
 le "misure alternative alla detenzione previste  dal  capo  VI  della    &#13;
 legge 26 luglio 1975, n. 354";                                           &#13;
      che,   di  conseguenza,  è  necessario  che  gli  atti  vengano    &#13;
 restituiti al giudice a quo perché verifichi se, alla stregua  della    &#13;
 normativa  sopravvenuta, la questione sollevata sia tuttora rilevante    &#13;
 (cfr. ordinanza n. 413 del 1992).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Ordina la restituzione degli atti al Tribunale di  sorveglianza  di    &#13;
 Perugia.                                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 dicembre 1992.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 22 dicembre 1992.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
