<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>141</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:141</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Nicola Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/06/1971</data_decisione>
    <data_deposito>22/06/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  100,  primo  &#13;
 comma,  del  r.d.  16 marzo 1942, n. 267 (legge fallimentare), promosso  &#13;
 con ordinanza emessa il 29 aprile 1970 dal  giudice  del  tribunale  di  &#13;
 Alessandria  delegato  al fallimento di Palazzolo Domenico, iscritta al  &#13;
 n.  188  del  registro  ordinanze  1970  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 163 del 1 luglio 1970.                     &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 18 maggio 1971 il Giudice  relatore  &#13;
 Nicola Reale;                                                            &#13;
     udito  il sostituto avvocato generale dello Stato Giorgio Azzariti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza emessa il 29 aprile 1970 il giudice  delegato  presso  &#13;
 il   tribunale   fallimentare   di  Alessandria,  nel  procedimento  di  &#13;
 impugnazione  promosso   dal   debitore   Palazzolo   Domenico   contro  &#13;
 l'ammissione  di  un  creditore  allo  stato  passivo, ha sollevato, in  &#13;
 riferimento all'art. 24, primo comma, della Costituzione, la  questione  &#13;
 di  legittimità dell'art. 100, primo comma, del r.d. 16 marzo 1942, n.  &#13;
 267 (c.d. legge fallimentare), in quanto non consentirebbe al  debitore  &#13;
 fallito di impugnare i crediti ammessi all'esecuzione concorsuale.       &#13;
     Detto  giudice ha ritenuto rilevante e non manifestamente infondata  &#13;
 la  questione,  sul  presupposto  che  la  norma  denunziata   precluda  &#13;
 effettivamente al debitore la tutela giudiziaria in sede fallimentare.   &#13;
     Costituitasi  in  giudizio  in  rappresentanza  del  Presidente del  &#13;
 Consiglio dei ministri, l'Avvocatura generale dello Stato  ha  eccepito  &#13;
 l'inammissibilità  della  questione,  in  quanto  proposta dal giudice  &#13;
 delegato nelle funzioni  di  giudice  istruttore  nel  procedimento  di  &#13;
 impugnazione  dello  stato  passivo.    Nell'esercizio di tali funzioni  &#13;
 detto giudice non avrebbe potestà decisoria, la quale, invece,  spetta  &#13;
 al collegio, cui compete giudicare circa la concreta applicazione della  &#13;
 norma  impugnata,  anche  sotto  il  profilo  della  legittimazione del  &#13;
 debitore all'impugnazione.                                               &#13;
     Nel  merito  l'Avvocatura  ha  contestato   il   fondamento   della  &#13;
 questione,  osservando  che  dalla  sentenza dichiarativa di fallimento  &#13;
 deriverebbe una diminuzione di capacità giuridica del fallito che, fra  &#13;
 l'altro, importa privazione della amministrazione dei beni (art. 42)  e  &#13;
 della legittimazione processuale nelle controversie relative a rapporti  &#13;
 di  diritto patrimoniale, nelle quali è sostituito dal curatore (artt.  &#13;
 31 e 43 legge fallimentare).   L'esclusione del  diritto  di  impugnare  &#13;
 l'ammissione  al  passivo  di un credito sarebbe manifestazione di tale  &#13;
 limitazione di capacità  del  fallito,  nell'interesse  dei  creditori  &#13;
 concorrenti e della gestione fallimentare.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Nel  giudizio  di legittimità costituzionale dell'art. 100,  &#13;
 primo comma,  del  r.d.  16  marzo  1942,  n.  267  (cosi  detta  legge  &#13;
 fallimentare),   in   relazione   all'art.   24,   primo  comma,  della  &#13;
 Costituzione, l'Avvocatura generale dello Stato  ha  eccepito,  in  via  &#13;
 preliminare,  l'inammissibilità  della  questione,  perché  sollevata  &#13;
 incidentalmente, nel corso di un giudizio di impugnazione  dello  stato  &#13;
 passivo,  dal giudice delegato al fallimento nell'esercizio di funzioni  &#13;
 istruttorie.                                                             &#13;
     Nella  fattispecie  accennata  il  predetto  giudice  non   sarebbe  &#13;
 investito  di  potestà decisoria, spettante, invece, al tribunale, cui  &#13;
 soltanto compete pronunziare circa la concreta applicazione della norma  &#13;
 che esclude la legittimazione del debitore fallito all'impugnazione dei  &#13;
 crediti ammessi.                                                         &#13;
     L'eccezione è fondata.                                              &#13;
     2. - L'art. 100 della  legge  fallimentare,  per  il  caso  che,  a  &#13;
 seguito  del  deposito  dello  stato  passivo,  siano impugnati crediti  &#13;
 ammessi, stabilisce che il giudice delegato fissa con decreto l'udienza  &#13;
 in cui le parti e il curatore devono comparire davanti a lui. Risultato  &#13;
 negativo  il  tentativo  di  componimento  della  vertenza,  lo  stesso  &#13;
 giudice,  ai sensi dell'art. 99 cui fa espresso richiamo l'ultimo comma  &#13;
 dell'art. 100 della legge citata, provvede alla istruzione della  causa  &#13;
 o  delle  cause  di impugnazione, che per la stessa norma vanno riunite  &#13;
 affinché siano esaminate e decise, salvo eccezioni, con unica sentenza  &#13;
 del tribunale. Lo  stesso  giudice,  quindi,  fissa  l'udienza  per  la  &#13;
 discussione   davanti  al  collegio,  a  norma  dell'art.  189  c.p.c.,  &#13;
 espressamente richiamato dall'art. 99 citato.                            &#13;
     Al tribunale, quindi, deve essere rimessa la causa per la pronuncia  &#13;
 sull'impugnazione contro il credito ammesso alla procedura concorsuale,  &#13;
 inclusa, ovviamente, la decisione sulla ricorrenza delle condizioni  di  &#13;
 proponibilità   della   impugnazione   stessa,  fra  le  quali  è  da  &#13;
 comprendere la legittimazione attiva della  parte  esercente  l'analogo  &#13;
 diritto.                                                                 &#13;
     Ne  consegue  che all'organo collegiale, cui spetta identificare le  &#13;
 norme da applicare per la definizione della controversia, è  riservato  &#13;
 il  giudizio  sulla  rilevanza e sulla non manifesta infondatezza della  &#13;
 questione  di  costituzionalità,  che,  riguardo  alla  legittimazione  &#13;
 attiva,  venga  sollevata  ai  sensi  dell'art. 23 della legge 11 marzo  &#13;
 1953, n. 87. Onde, in conformità della costante giurisprudenza seguita  &#13;
 da questa Corte (sentenze nn.   62/1966, 44/1963,  109/1962)  circa  le  &#13;
 questioni  proposte  da  giudici istruttori civili in materia riservata  &#13;
 alla competenza del collegio, deve ritenersi inammissibile la questione  &#13;
 sollevata   nei   termini  sopra  accennati  dal  giudice  delegato  al  &#13;
 fallimento, nell'esercizio delle funzioni istruttorie attribuitegli  ai  &#13;
 fini  del  procedimento  di  cognizione in merito alla contestazione di  &#13;
 pretese creditorie insinuate nel passivo fallimentare.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 dell'art.  100, primo comma, del r.d. 16 marzo 1942, n. 267 (cosi detta  &#13;
 legge fallimentare),  sollevata,  in  riferimento  all'art.  24,  primo  &#13;
 comma, della Costituzione, con l'ordinanza di cui in epigrafe.           &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 16 giugno 1971.                               &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
