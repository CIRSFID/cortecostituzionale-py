<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>74</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:74</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SPAGNOLI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/02/1995</data_decisione>
    <data_deposito>01/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Ugo SPAGNOLI; &#13;
 Giudici: prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente                                               &#13;
                               SENTENZA                                   &#13;
 nel giudizio di legittimità costituzionale degli artt. 10, lett.  d)    &#13;
 del  d.P.R.  29  settembre  1973,  n.  597  (Istituzione e disciplina    &#13;
 dell'imposta sul reddito delle persone fisiche) e 10,  lett.  e)  del    &#13;
 d.P.R.  22  dicembre  1986,  n.  917  (Testo  unico delle imposte sui    &#13;
 redditi)  promosso  con<titolo>ordinanza</titolo>emessa  il  3  marzo  1994  dalla    &#13;
 Commissione  tributaria  di primo grado di Milano su ricorso proposto    &#13;
 da Occhionero Achille contro l'Ufficio II.DD. di Milano  iscritta  al    &#13;
 n.  634  del  registro  ordinanze  1994  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 43,  prima  serie  speciale,  dell'anno    &#13;
 1994;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio dell'8 febbraio  1995  il  Giudice    &#13;
 relatore Renato Granata;                                                 &#13;
    Ritenuto  che la Commissione tributaria di primo grado di Milano -    &#13;
 con ordinanza del 3 marzo 1994, emessa in un giudizio promosso da  un    &#13;
 contribuente  per  contestare  l'esclusa  deducibilità  di  spese di    &#13;
 psicanalisi ai fini dell'IRPEF dovuta per  il  1983  -  ha  sollevato    &#13;
 questione  incidentale di legittimità costituzionale degli artt. 10,    &#13;
 lettera d), d.P.R.  29 settembre 1973 n. 597 e 10, lett.  e),  d.P.R.    &#13;
 22  dicembre  1986 n.   917, nella parte appunto in cui non includono    &#13;
 tra le spese mediche deducibili, ai detti fini, quelle di psiconalisi    &#13;
 e psicoterapia, "sopratutto se sostenute (come nella  specie)  "negli    &#13;
 anni  d'imposta  antecedenti  all'entrata  in  vigore  della legge 18    &#13;
 febbraio 1989, n. 56 istitutiva dell'ordinamento della professione di    &#13;
 psicologo", per asserito  contrasto  con  gli  artt.  3  e  32  della    &#13;
 Costituzione;                                                            &#13;
      che,   nel  giudizio  innanzi  alla  Corte,  è  intervenuto  il    &#13;
 Presidente del Consiglio dei ministri  per  eccepire  l'infondatezza,    &#13;
 sotto ogni profilo, della così proposta impugnativa;                    &#13;
    Considerato  che, per quanto attiene alla attualità, la questione    &#13;
 (a  prescindere  da  ogni  rilievo  di  ammissibilità)  è  comunque    &#13;
 manifestamente  infondata, per la ragione, che nel vigore della legge    &#13;
 n. 56/1989 citata, le spese di psicanalisi sono pienamente ammesse in    &#13;
 deduzione,  come  dalla  stessa  Commissione  rimettente,  del  resto    &#13;
 riconosciuto;                                                            &#13;
      che,   anche  con  riguardo  al  periodo  in  contestazione,  la    &#13;
 questione è del pari manifestamente infondata;                          &#13;
      che, infatti, la non deducibilità delle spese di psicoanalisi e    &#13;
 di psicoterapia sostenute negli anni di  imposta  anteriori  al  1989    &#13;
 costituisce  conseguenza  duplicemente obbligata: per un verso, dalla    &#13;
 distinzione  operata,  a  monte,  dal  legislatore  e  non  messa  in    &#13;
 discussione  dal  giudice  a  quo,  tra  spese per cure generiche non    &#13;
 deducibili (se non in misura limitata ed a determinate condizioni)  e    &#13;
 spese  per  cure  specialistiche integralmente ed incondizionatamente    &#13;
 invece deducibili, e, per altro verso,  dalla  natura  innegabilmente    &#13;
 non  specialistica  delle  prestazioni di psicoanalisi e psicoterapia    &#13;
 rese precedentemente  alla  introduzione,  definizione  e  disciplina    &#13;
 della  professione  di  psicologo e della istituzione del correlativo    &#13;
 albo con esplicita previsione del relativo corso di specializzazione,    &#13;
 attuata appunto solo con la richiamata legge 18 febbraio 1989 n. 56;     &#13;
    Visti gli artt. 26, secondo comma della legge 11 marzo 1953 n.  87    &#13;
 e  9,  secondo  comma,  delle norme integrative per i giudizi innanzi    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  degli  artt.  10,  lett. d), del d.P.R.  29 settembre    &#13;
 1973, n. 597 (Istituzione e disciplina dell'imposta sul reddito delle    &#13;
 persone fisiche) e 10, lett. e), del d.P.R.  22 dicembre 1986, n. 917    &#13;
 (Testo unico delle imposte sui redditi),  sollevata,  in  riferimento    &#13;
 agli artt. 3 e 32 della Costituzione, dalla Commissione tributaria di    &#13;
 primo grado di Milano, con l'ordinanza in epigrafe.                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 febbraio 1995.                             &#13;
                        Il Presidente: SPAGNOLI                           &#13;
                         Il redattore: GRANATA                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 1 marzo 1995.                            &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
