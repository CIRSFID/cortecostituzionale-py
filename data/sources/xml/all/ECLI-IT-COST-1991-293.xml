<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>293</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:293</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GALLO E.</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/06/1991</data_decisione>
    <data_deposito>18/06/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Ettore GALLO; &#13;
 Giudici: dott. Aldo CORASANITI, dott. Francesco GRECO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale degli artt. 418, primo    &#13;
 comma, e 419, quinto e sesto comma, del codice di  procedura  penale,    &#13;
 promossi con le seguenti ordinanze:                                      &#13;
      1)  ordinanza  emessa  il  15  dicembre  1990 dal Giudice per le    &#13;
 indagini preliminari presso il Tribunale di Ancona  nel  procedimento    &#13;
 penale  a  carico  di Carbonari Elio, iscritta al n. 172 del registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 12, prima serie speciale dell'anno 1991;                              &#13;
      2) ordinanza emessa il 14  dicembre  1990  dal  Giudice  per  le    &#13;
 indagini  preliminari  presso il Tribunale di Ancona nel procedimento    &#13;
 penale a carico di Spoletini Rosa, iscritta al n.  193  del  registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 14, prima serie speciale, dell'anno 1991;                             &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  del  5  giugno  1991  il  Giudice    &#13;
 relatore Aldo Corasaniti.                                                &#13;
    Ritenuto  che  il  giudice  per  le indagini preliminari presso il    &#13;
 Tribunale di Ancona, al quale il pubblico ministero  aveva  inoltrato    &#13;
 richiesta  di rinvio a giudizio di Carbonari Elio, ai sensi dell'art.    &#13;
 416 del codice di procedura penale, ritenendo che  nella  fattispecie    &#13;
 sarebbe stato più opportuno, stante l'evidenza della prova, e previo    &#13;
 interrogatorio  dell'imputato  entro  90  giorni  dall'iscrizione nel    &#13;
 registro della notizia del reato, il ricorso, da parte  del  pubblico    &#13;
 ministero,  al giudizio immediato ex art. 453 del codice di procedura    &#13;
 penale, ha sollevato, con ordinanza emessa il 15 dicembre 1990  (R.O.    &#13;
 n.  172/91), questione di legittimità costituzionale, in riferimento    &#13;
 agli artt. 2, 3, 97 e 101, secondo comma, della  Costituzione,  degli    &#13;
 artt.  418,  primo  comma, e 419, commi quinto e sesto, del codice di    &#13;
 procedura penale;                                                        &#13;
      che, in particolare, il giudice a quo sottopone  a  censura:  a)    &#13;
 l'art.  418,  primo  comma,  in  quanto  rende obbligatoria l'udienza    &#13;
 preliminare, inibendo al Giudice per  le  indagini  preliminari  ogni    &#13;
 forma  di  controllo  sulla  scelta  del  rito  da parte del pubblico    &#13;
 ministero, a differenza di  quanto  previsto  dall'art.  455  per  la    &#13;
 richiesta  di giudizio immediato avanzata dal pubblico ministero, che    &#13;
 può essere rigettata dal Giudice per le indagini preliminari;           &#13;
  b) l'art. 419, commi  quinto  e  sesto,  in  quanto  subordinano  il    &#13;
 rifiuto dell'udienza preliminare alla discrezionalità dell'imputato;    &#13;
      che,   ad   avviso   del   giudice   remittente,  le  suindicate    &#13;
 disposizioni   determinano    "la    dequalificazione    dell'udienza    &#13;
 preliminare,  che  cessa  in  tal  modo  di  essere  filtro selettore    &#13;
 processuale", ed il suo "intasamento qualitativo e quantitativo", che    &#13;
 tra l'altro pregiudica il buon andamento dell'"amministrazione  della    &#13;
 giustizia ed organizzazione dei pubblici uffici";                        &#13;
      che  analoghe  questioni  lo  stesso  Giudice  per  le  indagini    &#13;
 preliminari ha sollevato con ordinanza emessa il 14 dicembre 1990 nel    &#13;
 procedimento penale a carico di Spoletini Rosa (R.O. n. 193/91);         &#13;
      che in entrambi i  giudizi  è  intervenuto  il  Presidente  del    &#13;
 Consiglio  dei  ministri  rappresentato  dall'Avvocatura dello Stato,    &#13;
 chiedendo che le questioni siano dichiarate infondate;                   &#13;
    Considerato che la questione  concernente  l'art.  419,  quinto  e    &#13;
 sesto  comma,  sollevata  con  le  ordinanze  n.  172 e n. 193/91, va    &#13;
 dichiarata manifestamente inammissibile, per  difetto  di  rilevanza,    &#13;
 atteso  che  non  risulta  dalle  ordinanze  che nei relativi giudizi    &#13;
 l'imputato avesse rinunciato all'udienza preliminare e  richiesto  il    &#13;
 giudizio immediato;                                                      &#13;
      che  la  questione  concernente  l'art.  418,  primo comma, già    &#13;
 sollevata dallo stesso giudice con  precedenti  ordinanze,  è  stata    &#13;
 dichiarata  manifestamente infondata da questa Corte con ordinanze n.    &#13;
 234 e n. 256 del 1991;                                                   &#13;
      che le nuove ordinanze non prospettano argomenti o motivi nuovi;    &#13;
      che  la  questione   va   pertanto   dichiarata   manifestamente    &#13;
 infondata.                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi:                                                     &#13;
      dichiara   manifestamente   inammissibile   la   questione    di    &#13;
 legittimità  costituzionale,  in  riferimento  agli artt. 2, 3, 97 e    &#13;
 101, secondo comma, della Costituzione, dell'art. 419, quinto e sesto    &#13;
 comma, del codice di procedura penale, sollevata dal giudice  per  le    &#13;
 indagini  preliminari  presso il tribunale di Ancona con le ordinanze    &#13;
 indicate in epigrafe;                                                    &#13;
      dichiara manifestamente infondata la questione  di  legittimità    &#13;
 costituzionale,  in  riferimento  ai  suindicati parametri, dell'art.    &#13;
 418, primo comma, del  codice  di  procedura  penale,  sollevata  dal    &#13;
 medesimo giudice con le ordinanze indicate in epigrafe.                  &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 5 giugno 1991.                                   &#13;
                         Il Presidente: GALLO                             &#13;
                       Il redattore: CORASANITI                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 giugno 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
