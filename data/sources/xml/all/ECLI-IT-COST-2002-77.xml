<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>77</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:77</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>28/02/2002</data_decisione>
    <data_deposito>19/03/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Francesco &#13;
AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 1, commi primo &#13;
e  quarto  (recte:  primo,  quarto  e quinto), della legge 23 ottobre &#13;
1960,  n. 1369  (Divieto  di  intermediazione ed interposizione nelle &#13;
prestazioni di lavoro e nuova disciplina dell'impiego di mano d'opera &#13;
negli  appalti  di opere e di servizi), promosso con ordinanza emessa &#13;
il 19 febbraio 2001  dal Tribunale di Firenze nel procedimento civile &#13;
Sgobbi  Simone  contro  A.I.F.  Gruppo  Securitas  s.r.l.  ed  altri, &#13;
iscritta  al  n. 492  del  registro ordinanze 2001 e pubblicata nella &#13;
Gazzetta   Ufficiale  della  Repubblica  n. 26,  1a  serie  speciale, &#13;
dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera di consiglio del 13 febbraio 2002 il giudice &#13;
relatore Francesco Amirante. &#13;
    Ritenuto  che,  nel  corso  di  un  giudizio  avente  ad  oggetto &#13;
l'accertamento di un rapporto di lavoro subordinato intercorso tra il &#13;
ricorrente  e  la società e gli altri soggetti privati convenuti, la &#13;
condanna  dei convenuti stessi al pagamento di differenze retributive &#13;
e  la  dichiarazione  di nullità del licenziamento orale intimato al &#13;
ricorrente,   il  Tribunale  di  Firenze,  con  ordinanza  emessa  il &#13;
19 febbraio  2001,  ha  sollevato,  in  riferimento agli artt. 3 e 97 &#13;
della   Costituzione,   questione   di   legittimità  costituzionale &#13;
dell'art. 1,  commi  primo  e quarto (recte: primo, quarto e quinto), &#13;
della  legge  23 ottobre 1960, n. 1369 (Divieto di intermediazione ed &#13;
interposizione   nelle  prestazioni  di  lavoro  e  nuova  disciplina &#13;
dell'impiego di mano d'opera negli appalti di opere e servizi), nella &#13;
parte   relativa   all'esclusione  dell'amministrazione  statale  dal &#13;
divieto di intermediazione di manodopera e dalla presunzione circa la &#13;
titolarità  del  rapporto  di  lavoro  in capo al soggetto che abbia &#13;
effettivamente utilizzato le prestazioni di lavoro; &#13;
        che,  secondo  il Tribunale rimettente, la prova testimoniale &#13;
raccolta  dimostrerebbe che il lavoro di movimentazione dei fascicoli &#13;
e  incartamenti  della  Conservatoria  dei  registri  immobiliari  di &#13;
Firenze, del quale il ricorrente (insieme ad altri giovani) era stato &#13;
incaricato dai convenuti che lo retribuivano, in realtà era eseguito &#13;
anche   nell'interesse  dell'amministrazione,  in  quanto  concerneva &#13;
compiti  propri dei dipendenti di questa, tanto che vi era un accordo &#13;
tra  il  Conservatore  e  i  soggetti privati convenuti in giudizio e &#13;
interessati alle visure; &#13;
        che,  sulla  base  di tali circostanze di fatto, il Tribunale &#13;
rimettente  afferma  che  il  contraddittorio  dovrebbe essere esteso &#13;
all'amministrazione  finanziaria  nei  cui  confronti  dovrebbe avere &#13;
applicazione la normativa impugnata se non ne fosse, appunto, esclusa &#13;
l'applicazione nei confronti della amministrazione statale; &#13;
        che,  secondo  il  giudice  a  quo  la suddetta esclusione si &#13;
porrebbe,   in   primo   luogo,   in  contrasto  con  l'art. 3  della &#13;
Costituzione  in quanto essa, del tutto irragionevolmente, renderebbe &#13;
possibile    alla   amministrazione   statale   di   ricorrere   alla &#13;
intermediazione  di  manodopera  per  sottrarsi alla applicazione dei &#13;
principi  di  effettività  del rapporto di pubblico impiego da tempo &#13;
elaborati   dalla   giurisprudenza  in  riferimento  alla  assunzione &#13;
irregolare di personale da parte delle pubbliche amministrazioni; &#13;
        che   sarebbe,   altresì,  violato  il  principio  di  buona &#13;
amministrazione  di  cui  all'art. 97  della  Costituzione  in quanto &#13;
l'immotivato  esonero  della amministrazione statale dalla osservanza &#13;
della  legge  n. 1369 del 1960 esporrebbe soggetti privati al rischio &#13;
di   essere   condannati  a  corrispondere  importi  retributivi  per &#13;
prestazioni  di  lavoro subordinato rese non alle loro dipendenze, ma &#13;
alle  dipendenze di una amministrazione statale per lo svolgimento di &#13;
un pubblico servizio; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  che  ha  concluso  chiedendo  che la questione sia dichiarata &#13;
inammissibile o non fondata. &#13;
    Considerato  che  il giudice rimettente motiva la rilevanza della &#13;
questione  affermando  che  soltanto l'attribuzione della titolarità &#13;
passiva   del  rapporto  alla  amministrazione  finanziaria  potrebbe &#13;
consentire  di  assolvere  i soggetti privati convenuti dalle domande &#13;
proposte nei loro confronti; &#13;
        che, secondo quanto risulta dall'ordinanza di rimessione, non &#13;
soltanto  non  è  parte  l'amministrazione,  ma  nessuna domanda nei &#13;
confronti di questa è stata proposta; &#13;
        che,   conseguentemente,   la   suddetta   motivazione  della &#13;
rilevanza  non  è plausibile in quanto la questione, anziché essere &#13;
proposta  per  la  definizione del giudizio a quo, finisce per essere &#13;
sollevata  in riferimento ad un altro eventuale giudizio, diverso per &#13;
petitum e causa petendi; &#13;
        che, pertanto, la questione è manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità   costituzionale  dell'art. 1,  commi  primo,  quarto  e &#13;
quinto,   della   legge   23 ottobre   1960,   n. 1369   (Divieto  di &#13;
intermediazione ed interposizione nelle prestazioni di lavoro e nuova &#13;
disciplina  dell'impiego  di  manodopera  negli appalti di opere e di &#13;
servizi),   sollevata,   in  riferimento  agli  artt. 3  e  97  della &#13;
Costituzione,  dal  Tribunale di Firenze, con l'ordinanza indicata in &#13;
epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 28 febbraio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                       Il redattore: Amirante &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 19 marzo 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
