<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>294</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:294</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/07/1997</data_decisione>
    <data_deposito>30/07/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nei giudizi di legittimità costituzionale dell'art. 3 della legge 18    &#13;
 ottobre  1995,  n.  427 (Conversione in legge, con modificazioni, del    &#13;
 d.-l. 9 agosto 1995, n. 345, recante disposizioni urgenti in  materia    &#13;
 di  accertamento  con  adesione del contribuente per anni pregressi),    &#13;
 recte:  dell'art.  1, comma 1, lettera b) del d.-l. 9 agosto 1995, n.    &#13;
 345 (Disposizioni urgenti in materia di accertamento con adesione del    &#13;
 contribuente per anni pregressi),  come  modificato  dalla  legge  di    &#13;
 conversione  18  ottobre  1995,  n.  427, promossi con n. 2 ordinanze    &#13;
 emesse il 22 marzo 1996 dalla Commissione tributaria di  primo  grado    &#13;
 di  Torino  sui  ricorsi  proposti  da  Clerici  Elio  in qualità di    &#13;
 rappresentante dello studio legale Clerici contro  l'Ufficio  imposte    &#13;
 dirette  di  Torino e contro l'Ufficio IVA di Torino, iscritte ai nn.    &#13;
 581 e 582 del registro ordinanze 1996  e  pubblicate  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  26, prima serie speciale, dell'anno    &#13;
 1996;                                                                    &#13;
   Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  18 giugno 1997 il giudice    &#13;
 relatore Massimo Vari.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - La Commissione tributaria di primo grado di  Torino,  con  due    &#13;
 ordinanze di identico contenuto (r.o. nn. 581 e 582 del 1996), emesse    &#13;
 il  22  marzo  1996,  ha  sollevato,  in riferimento all'art. 3 della    &#13;
 Costituzione, questione di legittimità  costituzionale  dell'art.  3    &#13;
 della  legge  18  ottobre  1995,  n.  427  (Conversione in legge, con    &#13;
 modificazioni, del d.-l. 9 agosto 1995, n. 345, recante  disposizioni    &#13;
 urgenti  in materia di accertamento con adesione del contribuente per    &#13;
 anni pregressi), recte: dell'art. 1, comma 1, lettera b del  d.-l.  9    &#13;
 agosto 1995, n.  345 (Disposizioni urgenti in materia di accertamento    &#13;
 con  adesione  del  contribuente per anni pregressi), come modificato    &#13;
 dalla legge di conversione 18 ottobre 1995, n. 427,  nella  parte  in    &#13;
 cui   esclude  dalla  definizione  dei  rapporti  tributari,  con  le    &#13;
 modalità   dell'accertamento   con   adesione   del    contribuente,    &#13;
 relativamente  agli anni pregressi (c.d. concordato di massa), coloro    &#13;
 ai quali "entro il  20  maggio  1995  è  stato  notificato  processo    &#13;
 verbale di constatazione con esito positivo ai fini delle imposte sul    &#13;
 reddito  o  dell'imposta  sul  valore aggiunto o notificato avviso di    &#13;
 accertamento".                                                           &#13;
   2. - Il giudice a quo constatato che  l'accertamento  con  adesione    &#13;
 del   contribuente  per  anni  pregressi,  disciplinato  dalla  norma    &#13;
 censurata, viene formulato utilizzando "parametri fissi e immutabili,    &#13;
 predisposti a cura del ministero competente", ritiene che  tale  atto    &#13;
 "più  che  di  un  accertamento,  riveste  le  caratteristiche di un    &#13;
 condono".                                                                &#13;
   Evocando la sentenza della Corte n. 175  del  1986,  il  rimettente    &#13;
 ravvisa,  nella  disposizione in esame, una disparità di trattamento    &#13;
 analoga a quella che ha portato la  Corte  a  dichiarare  illegittimo    &#13;
 l'art.  16 del d.-l. 10 luglio 1982, n. 429 (Norme per la repressione    &#13;
 della evasione in  materia  di  imposte  sui  redditi  e  sul  valore    &#13;
 aggiunto  e  per  agevolare  la definizione delle pendenze in materia    &#13;
 tributaria), come modificato dalla  legge  di  conversione  7  agosto    &#13;
 1982,   n.  516,  nella  parte  in  cui  consentiva  la  notifica  di    &#13;
 accertamenti in rettifica o d'ufficio sino alla data di presentazione    &#13;
 della dichiarazione integrativa, anziché sino alla data  di  entrata    &#13;
 in vigore del d.-l. n. 429 del 1982.                                     &#13;
   Secondo  l'ordinanza  si  tratta  di  una disparità di trattamento    &#13;
 priva di ogni ragionevolezza, che, in contrasto con  l'art.  3  della    &#13;
 Costituzione,  fa dipendere l'ammissione al concordato esclusivamente    &#13;
 dall'epoca in cui viene effettuata la notifica di un  atto  da  parte    &#13;
 dell'amministrazione finanziaria, al cui operato discrezionale viene,    &#13;
 pertanto, condizionata l'applicazione dell'istituto in questione.        &#13;
   3.  -  È  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato e difeso  dall'Avvocatura  generale  dello  Stato,  per    &#13;
 chiedere una pronuncia di non fondatezza della questione proposta.       &#13;
   La difesa erariale osserva, preliminarmente, che tutte le norme che    &#13;
 ammettono  la definizione dell'accertamento con adesione (e anche per    &#13;
 condono) hanno sempre fatto salvi gli effetti degli accertamenti già    &#13;
 intervenuti, nel senso che non fosse possibile definire  un  rapporto    &#13;
 come se l'accertamento non esistesse.                                    &#13;
   È  pur  vero  che  la  richiamata  sentenza  n.  175  del  1986 ha    &#13;
 riconosciuto  illegittima   la   norma   che   consente   all'ufficio    &#13;
 l'accertamento  dopo  che,  con  l'entrata  in vigore della legge, è    &#13;
 maturato il diritto alla definizione,  ma,  sotto  tale  profilo,  la    &#13;
 norma  censurata si adegua perfettamente alla predetta statuizione in    &#13;
 quanto stabilisce che la definizione per adesione non  è  consentita    &#13;
 quando  un  atto  di  accertamento,  inteso in senso ampio, sia stato    &#13;
 eseguito prima di una specifica data (il 20  maggio  1995)  anteriore    &#13;
 all'entrata in vigore della legge.<diritto>Considerato in diritto</diritto>1.  -  Con  due  ordinanze  di  identico  contenuto, la Commissione    &#13;
 tributaria di  primo  grado  di  Torino  ha  sollevato  questione  di    &#13;
 legittimità  costituzionale dell'art. 3 della legge 18 ottobre 1995,    &#13;
 n. 427 (Conversione in legge, con modificazioni, del d.-l.  9  agosto    &#13;
 1995, n. 345, recante disposizioni urgenti in materia di accertamento    &#13;
 con adesione del contribuente per anni pregressi), nella parte in cui    &#13;
 esclude,  dalla  definizione dei rapporti tributari, con le modalità    &#13;
 dell'accertamento con adesione del contribuente,  relativamente  agli    &#13;
 anni  pregressi  (c.d. concordato di massa), coloro ai quali entro il    &#13;
 20 maggio 1995 sia stato notificato processo verbale di constatazione    &#13;
 con esito positivo ai fini delle imposte sul reddito  o  dell'imposta    &#13;
 sul valore aggiunto o notificato avviso di accertamento.                 &#13;
   2.  -  La  disposizione  denunciata  - la quale va più esattamente    &#13;
 intesa come art. 1, comma 1, lettera b) del  d.-l. 9 agosto 1995,  n.    &#13;
 345 (Disposizioni urgenti in materia di accertamento con adesione del    &#13;
 contribuente per anni pregressi), nel testo modificato dalla legge di    &#13;
 conversione  18  ottobre  1995,  n.  427 - contrasterebbe, secondo le    &#13;
 suddette  ordinanze,  con  l'art.  3  della   Costituzione,   facendo    &#13;
 irragionevolmente    dipendere    l'ammissione    del    contribuente    &#13;
 all'accertamento con adesione esclusivamente dall'epoca in cui  viene    &#13;
 effettuata  la  notifica  di  un  atto  emesso  dalla amministrazione    &#13;
 finanziaria,  alla  cui  iniziativa,  pertanto,  viene   condizionata    &#13;
 l'applicazione  del c.d. concordato di massa, equiparabile, ad avviso    &#13;
 del rimettente, ad un provvedimento di condono,  perché  ancorato  a    &#13;
 parametri  fissi  ed  immutabili  predisposti  a  cura  del ministero    &#13;
 competente.                                                              &#13;
   3. - Poiché  le  ordinanze  di  rimessione  hanno  ad  oggetto  la    &#13;
 medesima  questione,  i  relativi  giudizi possono essere riuniti per    &#13;
 essere decisi con unica sentenza.                                        &#13;
   4. - La questione non è fondata.                                      &#13;
   Occorre  premettere  che,  nel precedente giurisprudenziale evocato    &#13;
 dal giudice rimettente e cioè la sentenza n. 175 del 1986, la  Corte    &#13;
 pervenne  alla dichiarazione di illegittimità dell'art. 16 del d.-l.    &#13;
 10 luglio 1982, n. 429, come modificato dalla legge di conversione  7    &#13;
 agosto  1982,  n. 516, perché la disposizione consentiva la notifica    &#13;
 di accertamenti sino alla data di presentazione  della  dichiarazione    &#13;
 integrativa,  anziché  sino  alla  data  di  entrata  in  vigore del    &#13;
 provvedimento  di  clemenza,  determinando  così  una  irragionevole    &#13;
 disparità di trattamento tra contribuenti, alcuni dei quali venivano    &#13;
 ammessi  alla  definizione  automatica,  avendo  l'ufficio  omesso di    &#13;
 notificare  l'accertamento, mentre altri avevano solo la possibilità    &#13;
 di aderire  all'accertamento  loro  notificato,  sia  pure    con  la    &#13;
 riduzione  prevista  dalla  legge; con l'effetto, in conclusione, che    &#13;
 erano gli uffici finanziari a stabilire quali  contribuenti  potevano    &#13;
 beneficiare  della  prima  forma  di definizione agevolata e quali ne    &#13;
 venivano esclusi.                                                        &#13;
   Nel caso qui in esame - senza che occorra  indugiare  sulla  natura    &#13;
 giuridica    dell'istituto   dell'accertamento   con   adesione   del    &#13;
 contribuente per anni pregressi (c.d. concordato di massa),  ritenuto    &#13;
 dall'ordinanza  assimilabile,  in virtù dei criteri di automaticità    &#13;
 che regolano la procedura, ad  un  provvedimento  di  clemenza  -  va    &#13;
 considerato  che  il  termine  del  20  maggio 1995, quale limite per    &#13;
 l'attività di accertamento, preclusivo dell'accesso alla particolare    &#13;
 forma di definizione del rapporto con il fisco, è stato  introdotto,    &#13;
 in  sede  di  conversione  del  d.-l. n. 345 del 1995, dalla legge 18    &#13;
 ottobre 1995, n. 427, che l'ha individuato in una data  già  scaduta    &#13;
 al  momento  di emanazione della legge stessa; e questo, come risulta    &#13;
 dai  lavori  preparatori,  per  evitare  che  la  mancanza  -   nella    &#13;
 disciplina  della  materia, originariamente contenuta nell'art. 3 del    &#13;
 d.-l. 30 settembre 1994,  n.  564  e,  successivamente,  integrata  e    &#13;
 modificata  dal  menzionato  d.-l.  n.  345  del  1995 - di qualsiasi    &#13;
 termine,   per    l'attività    accertativa    dell'amministrazione,    &#13;
 consentisse   alla  stessa,  attraverso  l'attivazione  sine  die  di    &#13;
 procedimenti  finalizzati  all'accertamento,  di  esercitare,  a  sua    &#13;
 discrezione,  un  potere di esclusione dalla definizione del rapporto    &#13;
 tributario con le peculiari modalità sopra  ricordate.  Ne  discende    &#13;
 che  la  disposizione  censurata  tende  a  garantire l'imparzialità    &#13;
 dell'amministrazione nei confronti dei  contribuenti  e,  quindi,  in    &#13;
 definitiva  a realizzare quel principio di parità di trattamento che    &#13;
 questa Corte ha, del resto, ritenuto, in più occasioni,  non  inciso    &#13;
 da  norme  che  condizionino i provvedimenti di definizione agevolata    &#13;
 dei rapporti con il fisco  (fino  ad  una  eventuale  esclusione  dei    &#13;
 medesimi)   ad   atti  amministrativi  già  intervenuti  al  momento    &#13;
 dell'entrata in vigore della legge che li contempla (sentenze nn. 148    &#13;
 del 1967, 96 e 119 del 1980).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  non   fondata   la   questione   di    &#13;
 legittimità  costituzionale  dell'art.  1,  comma 1, lettera b), del    &#13;
 d.-l. 9 agosto 1995, n.  345  (Disposizioni  urgenti  in  materia  di    &#13;
 accertamento  con  adesione  del  contribuente  per  anni pregressi),    &#13;
 convertito, con modificazioni, nella legge 18 ottobre 1995,  n.  427,    &#13;
 sollevata,  in  riferimento  all'art.    3  della Costituzione, dalla    &#13;
 Commissione  tributaria  di  primo  grado di Torino, con le ordinanze    &#13;
 indicate in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1997.                               &#13;
                        Il Presidente: Vassalli                           &#13;
                          Il redattore: Vari                              &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 30 luglio 1997.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
