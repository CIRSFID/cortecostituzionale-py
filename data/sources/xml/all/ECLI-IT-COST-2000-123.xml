<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>123</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:123</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/04/2000</data_decisione>
    <data_deposito>27/04/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 34 del codice    &#13;
 di  procedura penale, promosso con ordinanza emessa il 26 maggio 1999    &#13;
 dal  pretore  di  Vibo  Valentia,  sezione  distaccata di Tropea, nel    &#13;
 procedimento  penale  a  carico  di  V.  G.,  iscritta  al n. 420 del    &#13;
 registro  ordinanze  1999 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 36, prima serie speciale, dell'anno 1999.                  &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 22 marzo 2000 il giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
     Ritenuto  che  il pretore di Vibo Valentia, sezione distaccata di    &#13;
 Tropea,  ha  sollevato,  in  riferimento  agli  artt. 3  e  24  della    &#13;
 Costituzione,  questione  di legittimità costituzionale dell'art. 34    &#13;
 del  codice  di  procedura  penale,  nella  parte  in cui non prevede    &#13;
 l'incompatibilità  alla  funzione  di  giudizio  del  giudice che in    &#13;
 precedenza, in qualità di giudice per le indagini preliminari, abbia    &#13;
 emesso  decreto  di  citazione  a giudizio a seguito di opposizione a    &#13;
 decreto  penale,  anche se tale decreto è stato pronunciato da altro    &#13;
 giudice;                                                                 &#13;
         che   il  rimettente  osserva  che,  seppure  il  decreto  di    &#13;
 citazione  a  giudizio  conseguente  ad  opposizione a decreto penale    &#13;
 possa essere considerato un atto obbligato, sussiste sempre l'obbligo    &#13;
 del giudicante di verificare la sussistenza di una delle cause di non    &#13;
 punibilità previste dall'art. 129 cod. proc. pen.;                      &#13;
         che  quindi  il  giudice,  con  l'emissione  del  decreto  di    &#13;
 citazione  a giudizio, avendo implicitamente ritenuto non sussistente    &#13;
 alcuna causa di non punibilità, avrebbe compiuto una valutazione sul    &#13;
 merito   dell'accusa,   con   conseguente   pregiudizio  per  la  sua    &#13;
 imparzialità;                                                           &#13;
         che tale situazione si porrebbe, ad avviso del giudice a quo,    &#13;
 in   contrasto   con  gli  artt. 3  e  24  Cost.,  "che  garantiscono    &#13;
 l'imparzialità ed indipendenza del giudice";                            &#13;
         che  è  intervenuto nel giudizio il Presidente del Consiglio    &#13;
 dei  Ministri,  rappresentato e difeso dall'Avvocatura generale dello    &#13;
 Stato, chiedendo che la questione sia dichiarata non fondata perché,    &#13;
 nel  caso  in  esame,  "la  valutazione  ex art. 129 cod. proc. pen.,    &#13;
 quand'anche  presente,  verrebbe  comunque  ad  assumere  i connotati    &#13;
 dell'accertamento  "in  negativo", non già della valutazione, seppur    &#13;
 sommaria, "in positivo" sulla responsabilità dell'imputato".            &#13;
     Considerato  che  l'attuale  disciplina  dell'art. 34  cod. proc.    &#13;
 pen. -   come   modificato   dall'art. 171  del  decreto  legislativo    &#13;
 19 febbraio  1998,  n. 51  (che  ha introdotto il comma 2-bis, la cui    &#13;
 efficacia, ai sensi dell'art. 247 dello stesso decreto, sostituito in    &#13;
 parte qua dall'art. 1 della legge 16 giugno 1998, n. 188, decorre dal    &#13;
 2 giugno  1999,  e  quindi  da un momento successivo all'ordinanza di    &#13;
 rimessione),  e poi dall'art. 11 della legge 16 dicembre 1999, n. 479    &#13;
 (che  ha introdotto il comma 2-ter) - prevede l'incompatibilità alla    &#13;
 funzione  di  giudizio del giudice che, nel medesimo procedimento, ha    &#13;
 esercitato funzioni di giudice per le indagini preliminari;              &#13;
         che   pertanto   occorre   restituire  gli  atti  al  giudice    &#13;
 rimettente  affinché  verifichi se la questione di costituzionalità    &#13;
 sia tuttora rilevante.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Ordina  la  restituzione  degli atti al pretore di Vibo Valentia,    &#13;
 sezione distaccata di Tropea.                                            &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 aprile 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 27 aprile 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
