<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>102</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:102</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Leonetto Amadei</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/04/1975</data_decisione>
    <data_deposito>07/05/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTT - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti di legittimità costituzionale dell'art.  670  &#13;
 del codice penale, promossi con le seguenti ordinanze:                   &#13;
     1) ordinanza emessa il 16 marzo 1972 dal pretore di La  Spezia  nel  &#13;
 procedimento  penale a carico di Morelli Mario ed altri, iscritta al n.  &#13;
 296 del registro ordinanze 1972 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n. 254 del 27 settembre 1972;                           &#13;
     2)  ordinanza  emessa il 3 novembre 1972 dal pretore di Pietrasanta  &#13;
 nel procedimento penale a carico di Balloni Eugenio, iscritta al n.  89  &#13;
 del registro ordinanze 1973 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n.  119 del 9 maggio 1973.                                    &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 5 febbraio 1975 il Giudice relatore  &#13;
 Leonetto Amadei;                                                         &#13;
     udito il sostituto avvocato generale dello Stato Giorgio  Azzariti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Nel  corso  del  procedimento  penale  a carico di tal Mario  &#13;
 Morelli ed altri, imputati del reato previsto e punito  dall'art.  670,  &#13;
 primo  e  secondo  comma, del codice penale, il pretore di La Spezia ha  &#13;
 sollevato,  su  istanza  della  difesa,   questione   di   legittimità  &#13;
 costituzionale  del  predetto articolo, in riferimento all'art. 2 della  &#13;
 Costituzione.                                                            &#13;
     La  motivazione  dell'ordinanza  fa  perno  sull'assunto che sia da  &#13;
 escludere che la Costituzione, nel sancire il diritto al lavoro,  abbia  &#13;
 inteso,   di   riflesso,   riconoscere   anche   il   principio   della  &#13;
 obbligatorietà del  lavoro,  rendendolo  coercibile  nei  riguardi  di  &#13;
 coloro  che  lo  rifiutano  per  una  personale  "visione  del  mondo",  &#13;
 espressione di un atteggiamento ideologico di libero dissenso  verso  i  &#13;
 criteri di vita dominanti nella società nella quale sono inseriti.      &#13;
     Sulla  base  di  tale  premessa si renderebbe, pertanto, necessario  &#13;
 accertare se il  mendicare,  sia  esso  esercitato  nel  quadro  di  un  &#13;
 atteggiamento  ideologico  di  rigetto di valori o per far fronte ad un  &#13;
 vero e proprio stato di bisogno, sia tale da ledere "il diritto  altrui  &#13;
 o sia pregiudizievole in se stesso per la pubblica tranquillità".       &#13;
     Invero, nel caso, sarebbe da escludersi tanto il danno patrimoniale  &#13;
 quanto  il  pericolo  per  la  pubblica  tranquillità, risolvendosi al  &#13;
 massimo la richiesta del questuante in un eventuale  senso  di  disagio  &#13;
 per  colui  al  quale  la  richiesta  sia  indirizzata  senza  che ciò  &#13;
 rappresenti turbativa per la stessa tranquillità pubblica.              &#13;
     L'aspetto  di   fondo   della   incostituzionalità   della   norma  &#13;
 poggerebbe,  pertanto,  nella repressione di ogni tipo di attività non  &#13;
 conforme  allo  schema  retributivo   colpendo   normalmente   soggetti  &#13;
 appartenenti  a  "gruppi  sociologicamente  ben definiti ed emarginati:  &#13;
 zingari, beatinks, disoccupati, infermi".                                &#13;
     Di questo si sarebbe resa conto la stessa giurisprudenza escludendo  &#13;
 il reato ogni qualvolta  il  postulante  accompagni  la  richiesta  con  &#13;
 qualche  offerta  simbolica di contro-prestazione (matite, lacci e cose  &#13;
 del genere).                                                             &#13;
     Il reato non sarebbe ipotizzabile neppure quando  si  effettui  nei  &#13;
 modi  e nelle forme specificati nel comma secondo dall'articolo 670 del  &#13;
 codice penale, in quanto  per  essi  soccorrerebbero  altre  previsioni  &#13;
 normative (artt. 610, 612, 640 e 660 cod. pen. ecc.).                    &#13;
     La  questione,  così impostata, si porrebbe, al di fuori di quella  &#13;
 decisa e risolta dalla Corte costituzionale con la sentenza n.  51  del  &#13;
 1959, sollevata in riferimento all'art.  38 della Costituzione.          &#13;
     È  intervenuto il Presidente del Consiglio, rappresentato e difeso  &#13;
 dall'Avvocatura generale dello  Stato.  Questa,  nelle  sue  deduzioni,  &#13;
 sostiene  che  la  norma  impugnata sarebbe pienamente legittima e ciò  &#13;
 anche per effetto della sentenza della Corte richiamata  nell'ordinanza  &#13;
 e  nella  quale  si  preciserebbe, in via di principio generale, che la  &#13;
 norma, "anche nella forma aggravata di accattonaggio vessatorio, tutela  &#13;
 il bene giuridico della tranquillità pubblica,  con  qualche  riflesso  &#13;
 sull'ordine  pubblico".  In  sostanza,  per l'Avvocatura generale dello  &#13;
 Stato, la norma impugnata sarebbe diretta a tutelare i  beni  giuridici  &#13;
 della  pubblica  tranquillità  e dell'ordine pubblico, che ben possono  &#13;
 essere posti in pericolo da coloro che ricorrono a  forme  parassitarie  &#13;
 di vita.                                                                 &#13;
     Se  è  pur vero che ognuno ha diritto, per la nostra Costituzione,  &#13;
 di "esplicare" la propria personalità  astenendosi  dal  lavoro  e  di  &#13;
 praticare  un  sistema di vita diverso dalla generalità dei cittadini,  &#13;
 ciò non toglie che il legislatore  sia  libero  di  predisporre  mezzi  &#13;
 idonei  ad  evitare  che il diritto del singolo contrasti con la tutela  &#13;
 della tranquillità e dell'ordine pubblico. Tale  principio,  affermato  &#13;
 oltretutto  dalla stessa Corte costituzionale con la sentenza n. 12 del  &#13;
 1972,  porterebbe  a  dover  concludere  che  la   repressione   penale  &#13;
 dell'accattonaggio  non  comprimerebbe  affatto  i diritti fondamentali  &#13;
 della personalità.                                                      &#13;
     Per  quanto  attiene  alle  forme aggravanti di accattonaggio (art.  &#13;
 670, secondo comma, codice penale) l'Avvocatura dello Stato osserva che  &#13;
 le norme  penali  richiamate  nell'ordinanza  tutelano  beni  giuridici  &#13;
 diversi da quelli tutelati dalla norma sottoposta all'esame della Corte  &#13;
 e rappresentati, appunto, dalla tranquillità e dall'ordine pubblico.    &#13;
     2. - Altra questione di legittimità costituzionale del primo comma  &#13;
 dell'art.  670 del codice penale, in riferimento agli articoli 3, primo  &#13;
 e secondo comma, e  4,  secondo  comma,  della  Costituzione  è  stata  &#13;
 sollevata d'ufficio dal pretore di Pietrasanta nel corso del giudizio a  &#13;
 carico di tal Eugenio Balloni.                                           &#13;
     Premesso  che  nessuna  legislazione  può favorire la mendicità e  &#13;
 tanto  meno  la  Costituzione  italiana  "fondata  sul  lavoro"  e  sul  &#13;
 riconoscimento non solo del "diritto al lavoro (art. 1 )", ma anche del  &#13;
 "dovere  di ogni cittadino di svolgere, secondo le proprie possibilità  &#13;
 e la propria scelta, un'attività lavorativa (art. 4)",  il  proponente  &#13;
 ritiene,  però, che non si possa ignorare il fatto che alla base delle  &#13;
 cause della mendicità si porrebbe  la  disoccupazione  quale  fenomeno  &#13;
 strutturale  di  un  regime  di libertà economica (art. 41) per cui il  &#13;
 diritto  al  lavoro  rappresenterebbe  un  concetto  astratto  se   non  &#13;
 accompagnato da condizioni atte a renderlo effettivo e possibile.        &#13;
     Di  fatto  tali  condizioni  non sussisterebbero nel nostro sistema  &#13;
 giuridico-sociale, come  non  sussisterebbero  strutture  previdenziali  &#13;
 capaci di garantire, soprattutto con tempestività, adeguate previdenze  &#13;
 a  coloro  che  siano inabili al lavoro e sprovvisti di mezzi necessari  &#13;
 per vivere (art. 38, primo comma, Cost.).                                &#13;
     L'art.   670,   primo   comma,   del   codice   penale,    vietando  &#13;
 aprioristicamente  e  indiscriminatamente  la mendicità, tenderebbe ad  &#13;
 imporre a ognuno un dovere di lavorare che può andare oltre la  scelta  &#13;
 personale   e  le  personali  possibilità  e  terrebbe  conto  solo  a  &#13;
 posteriori (art. 154 del t.u. delle leggi di  p.s.)  di  particolari  e  &#13;
 limitate  condizioni  di  bisogno,  escludendo  dal previsto intervento  &#13;
 assistenziale prefettizio o ministeriale,  oltretutto  non  tempestivo,  &#13;
 gli   inabili   al   lavoro  e  la  intera  categoria  dei  considerati  &#13;
 "marginalizzati" quali i vecchi, gli analfabeti, i malati  cronici  non  &#13;
 gravi,  ecc.,  e  per  i  quali  limitatissime  sono le possibilità di  &#13;
 svolgere una attività lavorativa.                                       &#13;
     Proprio l'insufficienza del sistema assistenziale in atto  dovrebbe  &#13;
 indurre  ad  escludere la punibilità per accattonaggio degli indigenti  &#13;
 inabili al lavoro.                                                       &#13;
     Invero, costituzionalmente inaccettabili sarebbero le varie ragioni  &#13;
 che  comunemente  vengono  addotte  per   giustificare   una   rigorosa  &#13;
 indiscriminata  repressione  dell'accattonaggio.  Ossia  la  tutela del  &#13;
 pubblico decoro, la lotta contro il parassitismo, la moralità pubblica  &#13;
 "intesa come fatto ideologico e razziale", ecc.                          &#13;
     Riferendosi alla sentenza n. 51 del 1959 della Corte costituzionale  &#13;
 e secondo la quale l'art. 670  tutelerebbe  "il  bene  giuridico  e  la  &#13;
 tranquillità  pubblica  con qualche riflesso sull'ordine pubblico", il  &#13;
 proponente rileverebbe sussistere, in ordine alla pena, una  diversità  &#13;
 di  trattamento  tra  il  reato  di  accattonaggio  e  quello  previsto  &#13;
 dall'art. 660 del codice penale in pieno contrasto con il principio  di  &#13;
 eguaglianza.  Infatti  il  legislatore  del 1930 avrebbe trasformato il  &#13;
 reato di mendicità in una ipotesi speciale di molestia,  punendola  in  &#13;
 maniera  del tutto sproporzionata rispetto all'ipotesi comune, violando  &#13;
 così il  principio  di  eguaglianza  inteso  come  ragionevole  e  non  &#13;
 arbitraria proposizione tra le conseguenze giuridiche di due situazioni  &#13;
 equiparabili.                                                            &#13;
     Tale  aspetto  di  incostituzionalità  si accompagnerebbe a quello  &#13;
 desumibile dalle  considerazioni  sviluppate  nella  premessa  per  cui  &#13;
 rimarrebbe  investito  anche  l'art.  4 della Costituzione in quanto la  &#13;
 punizione della mendicità, cosi come è  imposta  dal  codice  penale,  &#13;
 tenderebbe a stabilire un obbligo di lavorare oltre quelle possibilità  &#13;
 che, secondo detto articolo, ne configurano un limite invalicabile.      &#13;
     Non  vi  è  stata  costituzione  di  parte e non è intervenuto il  &#13;
 Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Le  due  ordinanze  pongono  la  questione  di  legittimità  &#13;
 costituzionale  dell'art. 670 del codice penale, in riferimento, quella  &#13;
 del pretore di La Spezia, all'art. 2 della Costituzione, e  quella  del  &#13;
 pretore  di  Pietrasanta,  agli  artt.  3,  primo e secondo comma, e 4,  &#13;
 secondo comma, della stessa Costituzione.                                &#13;
     2. -  La  motivazione  dell'ordinanza  del  pretore  di  La  Spezia  &#13;
 ravviserebbe la violazione dei diritti inviolabili dell'uomo, garantiti  &#13;
 dall'art.  2  della  Costituzione,  nel fatto che l'art. 670 del codice  &#13;
 penale, nella sua formulazione e nella sua portata, si risolverebbe  in  &#13;
 una  imposizione  all'obbligatorietà  del lavoro e, di conseguenza, in  &#13;
 una coercizione  e  in  un  divieto,  penalmente  sanzionato,  di  ogni  &#13;
 atteggiamento  ideologico  di  libero  dissenso verso i criteri di vita  &#13;
 dominanti nella società  nella  quale  il  soggetto  è  inserito,  e,  &#13;
 quindi, di ogni libera scelta di valori e di comportamenti.              &#13;
     In sostanza il dedicarsi alla mendicità rientrerebbe nel quadro di  &#13;
 una  scelta di libertà che non potrebbe essere perseguita penalmente e  &#13;
 non suscettibile, d'altra parte,  di  ledere  i  diritti  altrui  o  di  &#13;
 presentarsi  come  elemento  di pregiudizio e nocumento per la pubblica  &#13;
 tranquillità.                                                           &#13;
     Le  questioni  relative  ai  diritti  dell'uomo  sono  state   già  &#13;
 affrontate e decise da questa Corte in numerose e svariate sentenze.     &#13;
     Un principio generale, ripreso in altre decisioni, è stato fissato  &#13;
 dalla  sentenza n. 11 del 1956. Con tale sentenza la Corte ha stabilito  &#13;
 che l'art. 2 della Costituzione eleva a regola fondamentale, per  tutto  &#13;
 quanto  attiene  ai  rapporti  tra  la  collettività  e  i singoli, il  &#13;
 riconoscimento di quei diritti che formano il patrimonio irretrattabile  &#13;
 della personalità umana e che appartengono all'uomo inteso come essere  &#13;
 libero. È stato anche deciso che l'art. 2 deve essere  necessariamente  &#13;
 ricollegato alle altre norme costituzionali per identificare, anche nei  &#13;
 loro limiti, tali diritti inviolabili.                                   &#13;
     La Corte si è data carico, anche, di fissare i possibili limiti ai  &#13;
 diritti inviolabili dell'uomo, affermando, in altre sue decisioni (cfr.  &#13;
 sent.  n.  75 del 1966 e n. 16 del 1968), che l'art. 2, nel riconoscere  &#13;
 quei  diritti  e  i  doveri  inderogabili  di  solidarietà   politica,  &#13;
 economica  e  sociale,  non  può  escludere che a carico dei cittadini  &#13;
 possano essere disposte quelle restrizioni della sfera  giuridica  rese  &#13;
 necessarie    dalla    tutela    dell'ordine   sociale.   Ancora   più  &#13;
 significativamente con la sentenza n. 168  del  1971  questa  Corte  ha  &#13;
 chiarito  che i diritti primari e fondamentali dell'uomo diventerebbero  &#13;
 illusori per tutti, se ciascuno potesse esercitarli  fuori  dell'ambito  &#13;
 della  legge,  della civile regolamentazione, del costume corrente, per  &#13;
 cui tali diritti debbono venir contemperati  con  le  esigenze  di  una  &#13;
 tollerabile convivenza.                                                  &#13;
     Con  riferimento  alla impostazione data al problema dall'ordinanza  &#13;
 del pretore di La Spezia e sulla  base  degli  orientamenti  di  questa  &#13;
 Corte  in  tema  di  diritti  inviolabili  dell'uomo, devesi senz'altro  &#13;
 affermare  che,  in  linea  di   principio,   la   repressione   penale  &#13;
 dell'accattonaggio  non  comprime  affatto  tali  diritti  e  tantomeno  &#13;
 rappresenta una indiretta coercizione nei riguardi di quei soggetti che  &#13;
 rifiutano di dedicarsi ad  un  lavoro.  Al  cittadino  che  non  svolge  &#13;
 attività  lavorativa  non può riconoscersi, per ciò solo, il diritto  &#13;
 di sollecitare pubblicamente altri a provvedere al suo mantenimento.     &#13;
     La questione pertanto è infondata.                                  &#13;
     3.  -  Conclusioni  parzialmente  difformi  dalle  precedenti  deve  &#13;
 prendere  la  Corte  per  quella  parte  della  questione sollevata dal  &#13;
 pretore di Pietrasanta che, nel riferirsi all'art. 3, secondo comma,  e  &#13;
 all'art.  4,  secondo  comma,  della  Costituzione, si ricollega, nella  &#13;
 motivazione,  all'art.     38  della  stessa   Costituzione.   Infatti,  &#13;
 l'ordinanza  prende  in  esame anche la particolare posizione di coloro  &#13;
 che si dedicano all'accattonaggio in quanto in condizioni fisiche  tali  &#13;
 da  non  poter svolgere una attività lavorativa, sia per il loro stato  &#13;
 precario di salute, sia per la loro età, e non  usufruiscano,  per  la  &#13;
 carenza  delle  strutture  previdenziali  imposte  dall'art.  38  della  &#13;
 Costituzione, di assistenza adeguata, e che, inabili  o  minorati,  non  &#13;
 siano  stati  indirizzati  a  quel  processo di educazione e avviamento  &#13;
 professionale conforme  alle  proprie  possibilità  (art.  4,  secondo  &#13;
 comma, della Costituzione) al quale hanno diritto.                       &#13;
     Se  è pur vero che qualche tendenza giurisprudenziale ha ravvisato  &#13;
 sussistere  causa  di  esclusione  della  punibilità  del   reato   di  &#13;
 accattonaggio  quando  ricorrano le condizioni tipiche volute dall'art.  &#13;
 54 del codice penale, tuttavia ha escluso che lo stato di bisogno possa  &#13;
 confondersi con lo stato di  necessità.  Tale  orientamento  non  può  &#13;
 essere accolto nel suo rigido schematismo in ordine a quelle situazioni  &#13;
 oggettive  e  soggettive  direttamente  riferibili,  come nella specie,  &#13;
 tanto all'art. 4 quanto all'art. 38 della Costituzione  e  che  debbono  &#13;
 essere  tenute  ragionevolmente presenti dall'interprete della norma ai  &#13;
 fini di una decisione che non venga a trovarsi in  conflitto  con  quei  &#13;
 principi  dell'Ordinamento  costituzionale che consacrano veri e propri  &#13;
 diritti primari incomprimibili.                                          &#13;
     Sotto questo profilo ben può rientrare nella sfera di applicazione  &#13;
 dell'art. 54 del codice penale  il  fatto  di  colui  che,  fisicamente  &#13;
 debilitato  e  privo  di chi debba per legge provvedere ai suoi bisogni  &#13;
 essenziali, si induca alla mendicità per non  essere  stato  messo  in  &#13;
 condizione   di   poter  tempestivamente  e  validamente  usufruire  di  &#13;
 quell'assistenza pubblica alla quale  avrebbe  diritto.  Nel  caso,  il  &#13;
 concetto  di  attualità  del  pericolo di un danno grave alla persona,  &#13;
 quale è quello che può essere determinato da uno stato di bisogno non  &#13;
 voluto, si profila come una costante senza soluzione fino a quando  non  &#13;
 siano rimosse le cause che vi hanno dato luogo.                          &#13;
     Solo  in  tali  limiti,  pertanto,  può  ritenersi  non fondata la  &#13;
 questione dell'art. 670  del  codice  penale  in  ordine  agli  aspetti  &#13;
 prospettati dal giudice a quo.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara:                                                            &#13;
     a)   non   fondata  la  questione  di  legittimità  costituzionale  &#13;
 dell'art. 670 del codice penale, sollevata, in riferimento  all'art.  2  &#13;
 della Costituzione, dal pretore di La Spezia, con l'ordinanza di cui in  &#13;
 epigrafe;                                                                &#13;
     b)  non  fondata,  nei sensi di cui in motivazione, la questione di  &#13;
 legittimità costituzionale dell'art. 670 del codice penale, sollevata,  &#13;
 in riferimento agli artt. 3, primo e secondo comma, e 4, secondo comma,  &#13;
 della Costituzione, dal pretore di Pietrasanta.                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 24 aprile 1975.                               &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI - MICHELE ROSSANO.      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
