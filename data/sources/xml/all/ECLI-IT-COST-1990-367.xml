<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>367</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:367</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/1990</data_decisione>
    <data_deposito>24/07/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  degli artt. 9, sesto    &#13;
 (rectius: settimo) comma, e 47 del d.P.R. 29 settembre 1973,  n.  600    &#13;
 (Disposizioni  comuni  in  materia  di accertamento delle imposte sui    &#13;
 redditi)  e  successive  modifiche  e  integrazioni,   promosso   con    &#13;
 ordinanza  emessa  il 13 ottobre 1986 dalla Commissione tributaria di    &#13;
 primo grado di Treviso sul ricorso  proposto  dalla  s.r.l.  "M.d.M."    &#13;
 contro  l'Ufficio  II.DD.  di  Montebelluna,  iscritta  al n. 185 del    &#13;
 registro ordinanze 1990 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 16, prima serie speciale dell'anno 1990;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 26 giugno 1990 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto   che  nel  corso  di  un  giudizio  proposto  contro  il    &#13;
 provvedimento di irrogazione della sanzione prevista dall'art. 47 del    &#13;
 d.P.R. n. 600 del 1973 - per avere un soggetto, sostituto di imposta,    &#13;
 presentato tempestivamente la dichiarazione  prescritta  dall'art.  7    &#13;
 dello  stesso  decreto,  ma  ad ufficio incompetente (Ufficio imposte    &#13;
 dirette di Treviso), e per  essere  tale  dichiarazione  pervenuta  a    &#13;
 quello  competente (Ufficio imposte dirette di Montebelluna) oltre il    &#13;
 termine di  cui  all'art.  9,  ultimo  comma,  del  medesimo  decreto    &#13;
 presidenziale  con  la  conseguenza di essere considerata omessa - la    &#13;
 Commissione tributaria di primo grado di Treviso, con  ordinanza  del    &#13;
 13  ottobre  1986  (pervenuta  a  questa  Corte il 20 marzo 1990), ha    &#13;
 sollevato questione di legittimità  costituzionale  degli  artt.  9,    &#13;
 sesto  comma  (rectius:  settimo  comma) e 47 del d.P.R. 29 settembre    &#13;
 1973, n. 600, per violazione dell'art. 3 della Costituzione;             &#13;
     che   il  giudice  a  quo  ritiene  che  verrebbe  riservata  una    &#13;
 ingiustificata disparità di trattamento a due fatti  sostanzialmente    &#13;
 identici,  poiché, nell'un caso, l'erroneo invio della dichiarazione    &#13;
 dei redditi (modelli 740, 750 e 760) ad ufficio incompetente, pur nel    &#13;
 rispetto  dei  relativi  termini  e dell'obbligo del versamento della    &#13;
 imposta  dovuta,  una  volta  che  la  dichiarazione  sia   pervenuta    &#13;
 all'ufficio competente oltre il mese dalla data di scadenza e sia, di    &#13;
 conseguenza, considerata omessa, è punito  ai  sensi  dell'art.  46,    &#13;
 primo  comma, del d.P.R. n. 600, con una pena pecuniaria da un minimo    &#13;
 di L. 50.000 ad un massimo di L. 500.000, in considerazione del fatto    &#13;
 che  "non  sono  dovute imposte"; nel secondo caso, invece, l'erroneo    &#13;
 invio della dichiarazione  del  sostituto  d'imposta  (modello  770),    &#13;
 sempre ad ufficio incompetente ma previa effettuazione delle ritenute    &#13;
 e versamento all'erario di quanto dovuto, nel caso che  il  documento    &#13;
 fiscale sia pervenuto all'ufficio competente con ritardo superiore al    &#13;
 mese dalla scadenza, è sanzionato ai sensi del successivo  art.  47,    &#13;
 con  la  pena  pecuniaria  da  due  a quattro volte l'ammontare delle    &#13;
 ritenute;                                                                &#13;
      che,  pertanto,  sempre  ad  avviso del giudice a quo, la stessa    &#13;
 infrazione di carattere meramente formale - essendo stati in entrambi    &#13;
 i  casi  assolti  gli  obblighi  tributari  e rispettati i termini di    &#13;
 presentazione delle dichiarazioni, sia pure ad uffici incompetenti  -    &#13;
 verrebbe così punita in modo macroscopicamente diverso;                 &#13;
      che,   nella  stessa  ordinanza,  il  giudice  della  rimessione    &#13;
 denuncia altresì il  sesto  (rectius:  settimo)  comma  dell'art.  9    &#13;
 citato  -  secondo  cui  "le  dichiarazioni  presentate  con  ritardo    &#13;
 superiore al mese si considerano  omesse  a  tutti  gli  effetti,  ma    &#13;
 costituiscono  titolo  per  la  riscossione delle imposte dovute... e    &#13;
 delle ritenute indicate dal sostituti di imposta" - poiché la norma,    &#13;
 in  maniera contradditoria, mentre riconosce per lo Stato l'esistenza    &#13;
 di un titolo per la riscossione delle ritenute,  nello  stesso  tempo    &#13;
 nega  al  contribuente la possibilità di far valere lo stesso titolo    &#13;
 per dimostrare l'avvenuto adempimento dell'obbligo tributario,  così    &#13;
 operando  una  ingiustificata  discriminazione tra Erario e sostituto    &#13;
 d'imposta;                                                               &#13;
      che  non  si  è  costituita  la  parte  privata e che è invece    &#13;
 intervenuto in giudizio il Presidente  del  Consiglio  dei  Ministri,    &#13;
 sostenendo  genericamente  la  inammissibilità  o,  in subordine, la    &#13;
 infondatezza della questione, sulla quale  questa  Corte  si  sarebbe    &#13;
 già più volte pronunciata (da ultimo ord. n. 103 del 1990 e, prima,    &#13;
 ordd. n. 490 del 1987 e n. 212 del 1989);                                &#13;
    Considerato  che, successivamente alla ordinanza di rimessione, è    &#13;
 entrato in vigore il decreto legge 2 marzo 1989,  n.  69  (convertito    &#13;
 con  modificazioni  nella  legge  27  aprile  1989, n. 154), il quale    &#13;
 all'art. 21 nel testo modificato dalla legge di  conversione  dispone    &#13;
 (comma  3), che: "sono considerate valide" (tra l'altro) "lett. b) le    &#13;
 dichiarazioni di cui al titolo I del d.P.R.  29  settembre  1973,  n.    &#13;
 600,  considerate  omesse  perché  pervenute  all'ufficio competente    &#13;
 oltre i termini previsti dalla legge, a condizione  che  siano  state    &#13;
 presentate,  ancorché  ad ufficio incompetente, entro il 31 dicembre    &#13;
 1988" e (comma 4) che "non si applicano le pene pecuniarie  previste:    &#13;
 a) dall'art. 46, primo comma, e dall'art. 47, primo comma, del d.P.R.    &#13;
 29 settembre 1973, n. 600, per le dichiarazioni di cui  al  comma  3,    &#13;
 lett. b)";                                                               &#13;
      che  il  giudizio  a quo verte sulla sanzione applicata ai sensi    &#13;
 dell'art. 47, primo comma, del d.P.R. n. 600 del 1973, per l'avvenuta    &#13;
 presentazione  della  dichiarazione  di  un  sostituto  di imposta ad    &#13;
 ufficio incompetente;                                                    &#13;
      che,  di conseguenza, si rende necessario restituire gli atti al    &#13;
 giudice  della  rimessione  perché  accerti,  alla   stregua   della    &#13;
 sopravvenuta   normativa,   se  la  questione  sollevata  sia  ancora    &#13;
 rilevante nel giudizio principale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Ordina  la  restituzione  degli atti alla Commissione tributaria di    &#13;
 primo grado di Treviso.                                                  &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 luglio 1990.                                &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 24 luglio 1990.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
