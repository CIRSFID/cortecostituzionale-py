<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>366</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:366</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Alfio Finocchiaro</relatore_pronuncia>
    <redattore_pronuncia>Alfio Finocchiaro</redattore_pronuncia>
    <data_decisione>10/12/2003</data_decisione>
    <data_deposito>19/12/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Riccardo CHIEPPA; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 8, comma 1, della legge Provincia di Bolzano 15 aprile 1991, n. 10 (Espropriazione di pubblica utilità per tutte le materie di competenza regionale), come modificato dall'art. 18 della legge Provincia di Bolzano 30 gennaio 1997, n. 1, sollevata in relazione agli artt. 3, 5 e 42 della Costituzione, nonché con riferimento agli artt. 4 e 8 dello Statuto speciale per il Trentino-Alto Adige (Legge cost. 26 febbraio 1948, n. 5) promosso con ordinanza del 27 dicembre 2002 dalla Corte di cassazione nel procedimento civile vertente tra I.P.E.S. della Provincia autonoma di Bolzano ed il Comune di Bolzano ed altri, iscritta al n. 87 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 11, prima serie speciale, dell'anno 2003. &#13;
    Visto l'atto di costituzione della Provincia autonoma di Bolzano; &#13;
    Udito nell'udienza pubblica del 14 ottobre 2003 il Giudice relatore dott. Alfio Finocchiaro; &#13;
    Udito l'avvocato Sergio Panunzio per la Provincia autonoma di Bolzano. &#13;
    Ritenuto che la Corte di cassazione, investita del ricorso proposto dall'Istituto per l'edilizia sociale della Provincia autonoma di Bolzano contro la sentenza della Corte di appello di Bolzano di determinazione dell'indennità per l'espropriazione di un terreno di sua proprietà da parte del Comune di Bolzano, ha sollevato questione di legittimità costituzionale dell'art. 8, comma 1, della legge Provincia di Bolzano 15 aprile 1991, n. 10 (Espropriazione di pubblica utilità per tutte le materie di competenza regionale), come modificato dall'art. 18 della legge Provincia di Bolzano 30 gennaio 1997, n. 1, in riferimento agli artt. 3, 5 e 42 della Costituzione, nonché con riferimento agli artt. 4 e 8 dello Statuto speciale per il Trentino-Alto Adige (Legge cost. 26 febbraio 1948, n. 5); &#13;
    che, secondo la Corte rimettente, la norma provinciale non richiede, ai fini del riconoscimento del carattere edificatorio dei suoli, la ricorrenza delle possibilità legali di edificazione, previste dagli strumenti urbanistici, ma considera sufficienti le possibilità effettive di edificazione ricavabili da un complesso di qualità e caratteristiche obiettive, desumibili da una serie di elementi certi ed inequivoci indicati dalla stessa disposizione legislativa, attestanti comunque la sua concreta attitudine alla edificabilità e, perciò, recependo, in definitiva, il criterio c.d. della edificabilità di fatto, che sarebbe sufficiente a garantire l'applicazione del privilegiato criterio di indennizzo commisurato alla metà della somma tra il prezzo di mercato in una libera contrattazione di compravendita ed il prezzo agricolo, restando applicabile il criterio agricolo tabellare soltanto per le aree site al di fuori dei centri abitati o prive di potenzialità edificatorie; &#13;
    che la norma censurata si pone, per il giudice a quo, in contrasto con l'art. 5-bis, introdotto dalla legge 8 agosto 1992, n. 359, di conversione del decreto legge 11 luglio 1992, n. 333 (Misure urgenti per il risanamento della finanza pubblica), qualificabile con norma fondamentale di riforma economico-sociale, che costituisce un limite all'esercizio delle competenze legislative della Regione Trentino-Alto Adige ed esige generalizzata applicazione in tutto il territorio dello Stato nella parte in cui, ai fini dell'applicazione del sistema di indennizzo delle aree fabbricabili, esige il requisito c.d. della edificabilità legale, ovvero la necessità che il carattere edificatorio sia previsto dallo strumento urbanistico, costituendo l'edificabilità di fatto criterio suppletivo o integrativo in presenza del detto requisito; &#13;
     che nel giudizio si è costituita la Provincia autonoma di Bolzano, chiedendo dichiararsi l'infondatezza della questione sollevata; &#13;
    considerato che il giudice rimettente non dà adeguata motivazione sulla rilevanza della questione nel giudizio a quo, tenuto conto che, nella specie, si trattava di indennizzare l'esproprio di un'area destinata dallo strumento urbanistico a verde pubblico, e tuttavia in parte idonea a recepire nel sottosuolo la costruzione di garages privati, e che dunque non è chiara la ragione per cui egli non ritenga comunque la ricorrenza del requisito dell'edificabilità legale, che secondo l'interpretazione da cui egli stesso muove sarebbe di per sé sufficiente all'applicazione del sistema indennitario delle aree fabbricabili, senza fare autonomo ricorso a quella vocazione edificatoria del terreno desunta da elementi obiettivi, da cui il rimettente fa derivare il dubbio di legittimità costituzionale della legge provinciale; &#13;
    che il difetto di motivazione sulla rilevanza induce a dichiarare la questione manifestamente inammissibile (ordinanze  n. 495 del 2000; n. 83 del 2001; n. 287 del 2002).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
  &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 8, comma 1, della legge Provincia di Bolzano 15 aprile 1991, n. 10 (Espropriazione di pubblica utilità per tutte le materie di competenza regionale), come modificato dall'art. 18 della legge Provincia di Bolzano 30 gennaio 1997, n. 1, sollevata, in relazione agli artt. 3, 5 e 42 della Costituzione, e agli artt. 4 e 8 dello Statuto speciale per il Trentino-Alto Adige (Legge cost. 26 febbraio 1948, n. 5), dalla Corte di cassazione con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 dicembre 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Alfio FINOCCHIARO, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 19 dicembre 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
