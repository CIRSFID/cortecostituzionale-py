<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>420</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:420</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>09/11/2005</data_decisione>
    <data_deposito>14/11/2005</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Annibale MARINI; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'articolo 3 della legge 7 agosto 1990, n. 241 (Nuove norme in materia di procedimento amministrativo e di diritto di accesso ai documenti amministrativi), promossi con tre ordinanze del 22 settembre 2004 dal Tribunale amministrativo regionale per la Puglia, sezione staccata di Lecce, rispettivamente iscritte ai numeri 1010, 1011 e 1013 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 1, prima serie speciale, dell'anno 2005. &#13;
      Visti gli atti di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 12 ottobre 2005 il Giudice relatore Ugo De Siervo. &#13;
    Ritenuto che il Tribunale amministrativo regionale per la Puglia, sezione staccata di Lecce, investito di ricorsi per l'annullamento di provvedimenti di non ammissione alle prove orali per l'esame di abilitazione alla professione di avvocato,  con tre ordinanze di identico contenuto ha sollevato, in riferimento agli articoli 3, 24, 97 e 113 della Costituzione, questione di legittimità costituzionale dell'art. 3 della legge 7 agosto 1990, n. 241 (Nuove norme in materia di procedimento amministrativo e di diritto di accesso ai documenti amministrativi);  &#13;
    che, ad avviso del giudice a quo la censura di difetto di motivazione formulata dai ricorrenti sarebbe infondata alla stregua di un orientamento consolidato della giurisprudenza del giudice di ultima istanza, secondo la quale, in materia di concorsi pubblici l'obbligo motivazionale è adeguatamente assolto mediante attribuzione di un punteggio numerico alla prova di ciascun candidato;  &#13;
    che, secondo il rimettente, tale orientamento avrebbe assunto le caratteristiche di un vero e proprio diritto vivente, nonostante un contrario indirizzo interpretativo della VI sezione del Consiglio di Stato, di cui dà conto lo stesso giudice a quo, il quale precisa tuttavia che si tratterebbe «di precedenti isolati e comunque non univoci»;  &#13;
    che il Tribunale amministrativo dubita pertanto della conformità a Costituzione di tale indirizzo interpretativo, sostenendo che non sarebbe ragionevole (art. 3 della Costituzione) escludere l'obbligo di motivazione per i giudizi d'esame;  &#13;
    che la tutela giurisdizionale (artt. 24 e 113 Cost.) si ridurrebbe al mero riscontro di profili estrinseci e formali, come quelli inerenti alle garanzie relative alla collegialità dell'organo giudicante e alla sua composizione;  &#13;
    che, comunque, il principio di buon andamento e di imparzialità (art. 97 Cost.) richiederebbe la piena trasparenza dell'azione amministrativa;  &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo nel senso della inammissibilità della questione, in quanto identica a quella già dichiarata manifestamente inammissibile da questa Corte con ordinanza n. 466 del 2000; &#13;
    Considerato che il Tribunale amministrativo regionale per la Puglia, sezione staccata di Lecce, ha sollevato, in riferimento agli artt. 3, 24, 97 e 113 della Costituzione, questione di legittimità costituzionale dell'art. 3 della legge 7 agosto 1990, n. 241, alla luce dell'interpretazione di detta disposizione fornita dalla giurisprudenza amministrativa in pronunce, che il rimettente reputa “diritto vivente”, che hanno escluso l'obbligo di esplicita motivazione per i giudizi espressi in sede di valutazione degli esami di abilitazione professionale;  &#13;
    che il Tribunale amministrativo regionale chiede sostanzialmente una pronuncia sulla conformità a Costituzione di tale indirizzo interpretativo, con riguardo ai principî costituzionali di cui alle disposizioni sopra indicate;  &#13;
    che i giudizi, aventi ad oggetto identica norma, vanno riuniti e decisi con unica pronuncia;  &#13;
    che identica questione è già stata ritenuta manifestamente inammissibile da questa Corte, con l'ordinanza n. 466 del 2000, «perché essa non è in realtà diretta a risolvere un dubbio di legittimità costituzionale, ma si traduce piuttosto in un improprio tentativo di ottenere l'avallo di questa Corte a favore di una determinata interpretazione della norma, attività, questa, rimessa al giudice di merito»; &#13;
    che, successivamente, questa Corte, con ordinanza n. 233 del 2001, ha nuovamente dichiarato manifestamente inammissibile la stessa questione, in considerazione del fatto che il rimettente avrebbe voluto «estendere l'obbligo di motivazione ai giudizi espressi in sede di valutazione delle prove d'esame per l'iscrizione all'albo degli avvocati», ma non avrebbe tratto «le conseguenze applicative dell'interpretazione che egli considera conforme ai parametri costituzionali, deducendo l'esistenza della giurisprudenza del Consiglio di Stato, che segue l'interpretazione da lui non condivisa», osservando come «nulla impedisce al rimettente di adottare l'interpretazione da lui ritenuta corretta alla luce dei parametri costituzionali»; &#13;
    che non sussistono ragioni per discostarsi dal richiamato orientamento, tenuto conto che nel frattempo la giurisprudenza amministrativa ha mostrato di fornire un panorama ulteriormente articolato di possibili soluzioni interpretative, spaziando dalla tesi che esclude l'applicabilità del censurato art. 3 alle operazioni di mero giudizio conseguenti a valutazioni tecniche, in quanto attività in tesi non provvedimentali, a quella che invece ritiene applicabile l'obbligo di motivazione previsto dalla disposizione censurata anche ai giudizi valutativi; &#13;
    che all'interno di tale ultimo indirizzo possono poi individuarsi tre diverse posizioni, a seconda che si ritenga l'attribuzione di un punteggio numerico una valida ed idonea espressione motivatoria del giudizio valutativo, ovvero che si escluda tale idoneità, o ancora che si rifiuti una prospettiva aprioristica, per risolvere la questione in relazione alle peculiarità della singola fattispecie, e segnatamente alla relazione intercorrente fra l'estensione dei criteri valutativi prestabiliti dalla commissione esaminatrice ed il carattere più o meno analitico del giudizio sulle prove di esame; &#13;
    che lo stesso giudice rimettente, peraltro, successivamente alla proposizione della questione, ha mostrato di esplorare una ulteriore possibile interpretazione della disposizione censurata, con una serie di ordinanze cautelari nelle quali ha diversamente ricostruito le modalità del sindacato giurisdizionale ex art. 3 della legge n. 241 del 1990 sui giudizi valutativi delle prove scritte relative all'abilitazione alla professione forense, «in ragione dell'indirizzo che consente al giudice amministrativo una valutazione sulle manifestazioni di discrezionalità tecnica, così ridimensionando la funzione di strumento di verifica estrinseca dell'operato della p.a. proprio della motivazione»; &#13;
    che pertanto va confermato il richiamato orientamento di questa Corte, tanto più in presenza delle riportate evoluzioni del panorama giurisprudenziale, che consentono al giudice di adottare una delle (plurime) interpretazioni che ritenga conforme agli invocati parametri costituzionali. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi,  &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 3 della legge 7 agosto 1990, n. 241 (Nuove norme in materia di procedimento amministrativo e di diritto di accesso ai documenti amministrativi), sollevata, in relazione agli artt. 3, 24, 97 e 113 della Costituzione, dal Tribunale amministrativo regionale per la Puglia, sezione staccata di Lecce, con le ordinanze indicate in epigrafe.  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  il 9 novembre 2005.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Ugo DE SIERVO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 14 novembre 2005.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
