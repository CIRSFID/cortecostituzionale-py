<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>261</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:261</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>MEZZANOTTE</presidente>
    <relatore_pronuncia>Paolo Maddalena</relatore_pronuncia>
    <redattore_pronuncia>Paolo Maddalena</redattore_pronuncia>
    <data_decisione>08/07/2004</data_decisione>
    <data_deposito>22/07/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Carlo MEZZANOTTE; Giudici: Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 31, comma 10, della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale dello Stato. Legge finanziaria 2003), promosso con ricorso della Regione Emilia-Romagna, notificato il 1° marzo 2003, depositato in cancelleria il successivo 7 marzo ed iscritto al n. 25 del registro ricorsi 2003.  &#13;
    Visto l'atto di costituzione del Presidente del Consiglio dei ministri;  &#13;
    udito nell'udienza pubblica del 22 giugno 2004 il Giudice relatore Paolo Maddalena; &#13;
    uditi l'avvocato Giandomenico Falcon per la Regione Emilia-Romagna e l'avvocato dello Stato Giancarlo Mandò per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ricorso notificato il 1° marzo 2003, depositato il successivo 7 marzo, la Regione-Emilia Romagna ha sollevato, tra le altre, questione di legittimità costituzionale dell'art. 31, comma 10, della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale dello Stato. Legge finanziaria 2003), in riferimento all'art. 117, terzo e quarto comma, della Costituzione.  &#13;
    La Regione ricorrente ritiene che la norma denunciata, la quale dispone la fissazione delle basi di calcolo dei sovracanoni per la produzione di energia idroelettrica, di cui all'art. 27, comma 10, della legge 28 dicembre 2001, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato. Legge finanziaria 2002), si configuri quale norma di dettaglio in una materia rientrante nella competenza regionale residuale ovvero concorrente (“produzione di energia”). &#13;
    A conferma della propria prospettazione, la ricorrente evidenzia che l'art. 86, comma 1, del decreto legislativo 31 marzo 1998, n. 112 (Conferimento di funzioni e compiti amministrativi dello Stato alle regioni ed agli enti locali in attuazione del Capo I della legge 15 marzo 1997, n. 59), dispone che “alla gestione dei beni del demanio idrico provvedono le regioni e gli enti locali competenti per territorio”. &#13;
    2. - Si è costituito in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che il ricorso venga dichiarato non fondato. &#13;
    3. - Nell'imminenza dell'udienza, sia la Regione Emilia-Romagna, sia l'Avvocatura generale dello Stato hanno presentato memorie. &#13;
    Secondo la ricorrente, la contestata disposizione di rivalutazione dei sovracanoni, contenuta nell'art. 31, comma 10, della legge n. 289 del 2002, non costituirebbe principio fondamentale. &#13;
    A sua volta la difesa erariale sottolinea, invece, che la norma impugnata esprimerebbe la volontà del legislatore statale di fissare un criterio uniforme di determinazione della misura dei sovracanoni, assicurando l'armonico coordinamento del sistema della finanza pubblica.<diritto>Considerato in diritto</diritto>1. - Con ricorso notificato il 1° marzo 2003, depositato il successivo 7 marzo, la Regione Emilia-Romagna ha sollevato, tra le altre, questione di legittimità costituzionale dell'art. 31, comma 10, della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale dello Stato. Legge finanziaria 2003), in riferimento all'art. 117, terzo e quarto comma, della Costituzione, nella parte in cui dispone la fissazione delle basi di calcolo dei sovracanoni per la produzione di energia idroelettrica, di cui all'art. 27, comma 10, della legge 28 dicembre 2001, n. 448 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato. Legge finanziaria 2002).  &#13;
    Secondo la Regione, la disposizione censurata invaderebbe la competenza legislativa regionale residuale ovvero quella concorrente (“produzione di energia”), in quanto essa si configurerebbe quale norma di dettaglio. &#13;
    2. - La questione non è fondata. &#13;
    2.1. - Va in proposito considerato che la disposizione impugnata, concernente la disciplina della fissazione delle basi di calcolo dei sovracanoni per le concessioni di derivazioni di acqua, non attiene alla materia “produzione di energia” (art. 117, terzo comma, della Costituzione) e tanto meno ad ambiti di legislazione residuale (art. 117, quarto comma, della Costituzione). &#13;
    Infatti, come questa Corte ha avuto occasione di affermare (sentenza n. 533 del 2002 e ordinanza n. 21 del 2004), la predetta disciplina dei sovracanoni - qualificabili come prestazioni patrimoniali imposte - attiene alla materia del sistema finanziario e tributario degli enti locali (art. 119, secondo comma, della Costituzione). &#13;
    2.2. - È poi da porre in risalto che, successivamente al ricorso della Regione Emilia-Romagna, questa Corte, con la sentenza n. 37 del 2004, ha dichiarato non fondata la questione di legittimità costituzionale dell'analogo art. 27, comma 10, della legge n. 448 del 2001, nella parte in cui prevede che a decorrere dal 1° gennaio 2002 le basi di calcolo dei sovracanoni dovuti ai Comuni, o ai consorzi obbligatori tra essi costituiti, dai concessionari delle derivazioni d'acqua per produzione di forza motrice “sono fissate rispettivamente in 13 euro e 3,50 euro, fermo restando per gli anni a seguire l'aggiornamento biennale previsto” attraverso decreti del Ministro dei lavori pubblici o di quello delle finanze, dall'art. 3 della legge 22 dicembre 1980, n. 925 (Nuove norme relative ai sovracanoni in tema di concessioni di derivazioni d'acqua per produzione di forza motrice). &#13;
    La disposizione censurata ha modificato le basi di calcolo dei sovracanoni fissando i nuovi importi in 18 euro e 4,50 euro. Si deve dunque ribadire che la norma interviene su una materia già interamente regolata dalla legge dello Stato, la cui competenza, fino all'attuazione dell'art. 119 della Costituzione, resta ferma.  &#13;
    Il legislatore, in sede di attuazione, dovrà coordinare l'insieme della finanza pubblica. A tal fine, dovrà non solo fissare i principi cui i legislatori regionali dovranno attenersi, ma anche determinare le grandi linee dell'intero sistema tributario, e definire gli spazi e i limiti entro i quali potrà esplicarsi la potestà impositiva, rispettivamente, di Stato, Regioni ed enti locali. &#13;
    Sul punto deve concludersi che, in attesa del predetto intervento legislativo, sia da ritenere tuttora spettante al legislatore statale la potestà di dettare norme modificative, anche nel dettaglio, della disciplina dei sovracanoni in questione.</testo>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riservata a separate pronunce la decisione delle ulteriori questioni di legittimità costituzionale proposte dalla Regione Emilia-Romagna nei confronti di altre disposizioni della legge 27 dicembre 2002, n. 289, qui non espressamente esaminate; &#13;
    dichiara non fondata la questione di legittimità costituzionale dell'art. 31, comma 10, della legge 27 dicembre 2002, n. 289 (Disposizioni per la formazione del bilancio annuale dello Stato. Legge finanziaria 2003), sollevata, in riferimento all'art. 117, terzo e quarto comma, della Costituzione, dalla Regione Emilia-Romagna con il ricorso indicato in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8  luglio 2004. &#13;
    F.to: &#13;
    Carlo MEZZANOTTE, Presidente &#13;
    Paolo MADDALENA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 22 luglio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
