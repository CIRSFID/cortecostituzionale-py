<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>125</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:125</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/05/2001</data_decisione>
    <data_deposito>11/05/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 8, primo comma, &#13;
del   d.P.R.  26 aprile  1957,  n. 818  (Norme  di  attuazione  e  di &#13;
coordinamento  della  legge  4 aprile 1952, n. 218, sul riordinamento &#13;
delle  pensioni dell'assicurazione obbligatoria per l'invalidità, la &#13;
vecchiaia ed i superstiti), promosso con ordinanza emessa il 6 agosto &#13;
1999  dal  tribunale  di Bologna nel procedimento civile vertente tra &#13;
Bertucci  Domenico  e  l'Istituto nazionale per la previdenza sociale &#13;
(INPS),  iscritta  al  numero  700  del  registro  ordinanze  1999  e &#13;
pubblicata  nella Gazzetta Ufficiale della Repubblica n. 52, 1ª serie &#13;
speciale, dell'anno 1999. &#13;
    Visto l'atto di costituzione dell'INPS; &#13;
    Udito nell'udienza pubblica del 20 marzo 2001 il giudice relatore &#13;
Annibale Marini; &#13;
    Udito l'avvocato Carlo De Angelis per l'INPS. &#13;
    Ritenuto  che  nel  corso  di  un  giudizio  avente ad oggetto il &#13;
riconoscimento   del   diritto  di  un  artigiano  alla  pensione  di &#13;
anzianità,  il  tribunale  di  Bologna,  in  funzione di giudice del &#13;
lavoro, con ordinanza del 6 agosto 1999, ha sollevato, in riferimento &#13;
agli  artt.3,  primo  comma, e 38, secondo comma, della Costituzione, &#13;
questione di legittimità costituzionale dell'art.8, primo comma, del &#13;
d.P.R. 26 aprile 1957, n. 818 (Norme di attuazione e di coordinamento &#13;
della  legge  4 aprile 1952, n. 218, sul riordinamento delle pensioni &#13;
dell'assicurazione  obbligatoria per l'invalidità, la vecchiaia ed i &#13;
superstiti),   "nella   parte   in   cui  dispone  che  i  contributi &#13;
previdenziali indebitamente versati rimangano acquisiti alle gestioni &#13;
previdenziali   e   siano   computabili   agli  effetti  del  diritto &#13;
dell'assicurato  alle  prestazioni  nel  caso  in cui "l'accertamento &#13;
dell'indebito  versamento  sia  posteriore  di oltre cinque anni alla &#13;
data  in  cui  il versamento è stato effettuato", senza che la norma &#13;
sia  ritenuta  applicabile  ai  contributi pagati alla gestione degli &#13;
artigiani"; &#13;
        che   la   norma  impugnata,  secondo  l'interpretazione  non &#13;
implausibile   del   rimettente,   riguarderebbe  solo  i  lavoratori &#13;
subordinati; &#13;
        che,   tuttavia,   ad  avviso  dello  stesso  rimettente,  le &#13;
fattispecie  del  lavoro autonomo e del lavoro subordinato sarebbero, &#13;
sotto  l'aspetto  della  tutela  previdenziale  di  cui  all'art. 38, &#13;
secondo  comma,  della  Costituzione,  formalmente  e sostanzialmente &#13;
uguali  o  quantomeno  simili  e  renderebbero,  pertanto, lesiva sia &#13;
dell'art. 3,  primo  comma,  che  dell'art. 38,  secondo comma, della &#13;
Costituzione  la  diversità  di disciplina, vigente per le anzidette &#13;
fattispecie, degli indebiti contributivi accertati almeno cinque anni &#13;
dopo l'avvenuto versamento; &#13;
        che  si è costituito in giudizio l'Istituto nazionale per la &#13;
previdenza   sociale  (INPS),  concludendo  per  la  declaratoria  di &#13;
inammissibilità o, in subordine, di infondatezza della questione; &#13;
        che,  secondo  la  difesa della anzidetta parte, la questione &#13;
sarebbe  inammissibile  per difetto di rilevanza richiedendo la norma &#13;
censurata,  ai  fini  della  sua applicabilità, la sussistenza di un &#13;
valido rapporto previdenziale che nella specie farebbe difetto; &#13;
        che,  secondo  la stessa parte, la questione sarebbe comunque &#13;
infondata  in  quanto il principio di eguaglianza non potrebbe essere &#13;
invocato  nel  caso  in  esame  in  considerazione  della  intrinseca &#13;
disomogeneità delle situazioni messe a raffronto dal rimettente. &#13;
    Considerato  che,  secondo  la  giurisprudenza  consolidata della &#13;
Corte  di  cassazione,  l'applicazione  dell'art. 8, primo comma, del &#13;
d.P.R. 26 aprile 1957, n. 818 (Norme di attuazione e di coordinamento &#13;
della  legge  4 aprile 1952, n. 218, sul riordinamento delle pensioni &#13;
dell'assicurazione  obbligatoria per l'invalidità, la vecchiaia ed i &#13;
superstiti),   nella   parte   in   cui   dispone  che  i  contributi &#13;
previdenziali  indebitamente corrisposti, se accertati come tali dopo &#13;
cinque  anni  dal  loro  versamento, rimangono acquisiti alle singole &#13;
gestioni assicurative e sono computabili sia al fine del diritto alla &#13;
prestazione   previdenziale  che  riguardo  alla  misura  di  questa, &#13;
presuppone  la  sussistenza  al  momento  del versamento di un valido &#13;
rapporto assicurativo; &#13;
        che, secondo quanto inequivocamente risulta dall'ordinanza di &#13;
rimessione,  l'anzidetto presupposto non ricorre nella fattispecie in &#13;
esame per il periodo compreso fra il 1 ottobre 1966 ed il 31 dicembre &#13;
1970; &#13;
        che, infatti, relativamente a tale periodo, il ricorrente nel &#13;
giudizio  a  quo,  essendo  stato cancellato in data 29 dicembre 1978 &#13;
dall'elenco  degli  artigiani tenuto presso la Camera di commercio di &#13;
Cosenza, non rivestiva la qualifica di artigiano e risultava, quindi, &#13;
privo  di  un requisito indispensabile per l'attivazione di un valido &#13;
rapporto assicurativo; &#13;
        che,  d'altra  parte,  il  provvedimento di cancellazione del &#13;
ricorrente  dall'elenco  degli artigiani, pur genericamente censurato &#13;
dal  rimettente,  non  risulta  oggetto di una specifica pronunzia di &#13;
illegittimità; &#13;
        che,   pertanto,   la   questione   sollevata  va  dichiarata &#13;
manifestamente  inammissibile  per difetto di rilevanza, non essendo, &#13;
per  quanto  detto,  la  norma  censurata  comunque  applicabile  nel &#13;
giudizio a quo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  dell'art. 8,  primo  comma,  del d.P.R. &#13;
26 aprile  1957, n. 818 (Norme di attuazione e di coordinamento della &#13;
legge   4 aprile  1952,  n. 218,  sul  riordinamento  delle  pensioni &#13;
dell'assicurazione  obbligatoria per l'invalidità, la vecchiaia ed i &#13;
superstiti),  sollevata,  in  riferimento agli artt.3, primo comma, e &#13;
38,  secondo  comma, della Costituzione, dal tribunale di Bologna, in &#13;
funzione digiudice del lavoro, con l'ordinanza di cui in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 7 maggio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                        Il redattore: Marini &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in Cancelleria l'11 maggio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
