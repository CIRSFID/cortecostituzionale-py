<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2019</anno_pronuncia>
    <numero_pronuncia>234</numero_pronuncia>
    <ecli>ECLI:IT:COST:2019:234</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LATTANZI</presidente>
    <relatore_pronuncia>Giovanni Amoroso</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Amoroso</redattore_pronuncia>
    <data_decisione>25/09/2019</data_decisione>
    <data_deposito>13/11/2019</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 74, comma 2, del decreto del Presidente della Repubblica 30 maggio 2002, n. 115, recante «Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia (Testo A)», promosso dal Giudice di pace di Roma, nel procedimento civile vertente tra L. D. S. e Roma Capitale con ordinanza del 3 luglio 2018, iscritta al n. 199 del registro ordinanze 2018 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 4, prima serie speciale, dell'anno 2019.&#13;
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 25 settembre 2019 il Giudice relatore Giovanni Amoroso. &#13;
 Ritenuto che con ordinanza del 3 luglio 2018, il Giudice di pace di Roma ha sollevato questioni di legittimità costituzionale dell'art. 74, comma 2, del decreto del Presidente della Repubblica 30 maggio 2002, n. 115, recante «Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia (Testo A)», per contrasto con l'art. 3, primo comma, della Costituzione, sotto il profilo della violazione dei principi di pari dignità sociale e uguaglianza dei cittadini di fronte alla legge, nonché con l'art. 111, secondo comma, Cost., assumendo la violazione del principio di parità processuale delle parti;&#13;
 che il giudice premette di dover decidere su un ricorso presentato da una parte ammessa in via provvisoria al patrocinio a spese dello Stato, avente a oggetto l'opposizione al verbale di accertamento di violazione del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada);&#13;
 che, con riferimento all'art. 3 Cost., il giudice censura il citato art. 74, comma 2, nella parte in cui - assicurando «il patrocinio nel processo civile, amministrativo, contabile, tributario e negli affari di volontaria giurisdizione, per la difesa del cittadino non abbiente quando le sue ragioni risultino non manifestamente infondate» - non dispone che, nell'ipotesi in cui il legislatore ha previsto l'autodifesa personale, si debba anticipare il solo contributo unificato al richiedente il «gratuito patrocinio»;&#13;
 che in particolare, secondo il rimettente, l'art. 3, primo comma, Cost., risulterebbe violato per plurime ragioni: a) perché coloro che percepiscono un reddito di poco superiore al limite previsto per l'accesso al «gratuito patrocinio» sono costretti a rinunciare a incaricare un avvocato per la propria difesa in quanto la sopportazione dei relativi costi ridurrebbe il loro reddito a livelli corrispondenti allo stato di non abbienza; b) perché fuori dall'ipotesi della condanna del soccombente, «in caso di inammissibilità o responsabilità aggravata accertata in giudizio del richiedente ed affermata dal giudice il difensore pagherebbe le conseguenze per il comportamento del suo assistito vedendo equiparata la sua attività al volontariato»; c) perché nel caso in cui le spese vengano compensate, colui che è stato ammesso al «gratuito patrocinio», non dovendo sopportare esborsi, «non può essere in alcun modo penalizzato al fine di essere disincentivato da azioni infondate»;&#13;
 che la disposizione censurata violerebbe anche l'art. 111, secondo comma, Cost., in quanto nei procedimenti aventi a oggetto sanzioni amministrative le pubbliche amministrazioni resistenti «a causa dell'attuale rigore nel contenimento della spesa pubblica si avvalgono ordinariamente dell'autodifesa con la delega a funzionari che certamente non sono equiparabili ai difensori nominati con conseguente violazione di detto principio di parità delle parti processuali»;&#13;
 che con atto dell'11 febbraio 2019, depositato il giorno successivo, è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni di legittimità costituzionale siano dichiarate inammissibili per difetto di motivazione sulla rilevanza nel giudizio a quo in quanto «dall'ordinanza di rimessione non si evince quale sia l'incidenza della questione sul giudizio di merito, ovvero sulla liquidazione del compenso al difensore»;&#13;
 che comunque, per l'interveniente, le questioni sono infondate, atteso che la disposizione censurata esprime un bilanciamento tra le contrapposte esigenze di garanzia del diritto alla difesa per i non abbienti e di contenimento della spesa pubblica;&#13;
 che - aggiunge l'interveniente - il fatto che i percettori di un reddito di poco superiore al limite previsto dalla legge potrebbero, in ipotesi, incontrare difficoltà nell'accesso al beneficio costituisce implicazione ineludibile di ogni limite fisso e non già indice di irragionevolezza della norma;&#13;
 che, quanto all'asserita violazione dell'art. 111, secondo comma, Cost., l'Avvocatura generale sostiene che il rimettente non ha indicato le ragioni per cui sarebbe leso il principio di parità processuale, non potendosi considerare un dato oggettivamente rilevabile quello della minore competenza dei funzionari rispetto agli altri difensori.&#13;
 Considerato che le sollevate questioni di legittimità costituzionale - aventi ad oggetto il comma 2 dell'art. 74 del d.P.R. 30 maggio 2002, n. 115, recante «Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia (Testo A)» - sono, rispettivamente, manifestamente infondata l'una e manifestamente inammissibile l'altra;&#13;
 che, con riferimento alla dedotta violazione dell'art. 3 della Costituzione, il limite reddituale per l'accesso al beneficio del patrocinio a spese dello Stato è espressione di un bilanciamento rimesso alla discrezionalità del legislatore e coerente con la garanzia costituzionale dell'art. 24, terzo comma, Cost., che, con appositi istituti, assicura ai non abbienti - e quindi non a tutti - i mezzi per agire e difendersi davanti a ogni giurisdizione;&#13;
 che la previsione di una soglia reddituale costituisce un oggettivo criterio selettivo per individuare le persone non abbienti che beneficiano dell'istituto;&#13;
 che la possibilità della difesa personale in alcune controversie non costituisce ragione giustificativa per limitare il beneficio in esame al solo esonero dal pagamento del contributo unificato di cui all'art. 9 del d.P.R. n. 115 del 2002, ben potendo tali controversie richiedere in concreto la competenza professionale della difesa tecnica che assicura l'effettività della tutela giurisdizionale;&#13;
 che pertanto la censura proposta per violazione del principio di eguaglianza è manifestamente infondata;&#13;
 che la non censurata norma secondo cui, nella fattispecie in esame, il Comune resistente può scegliere di difendersi avvalendosi di un proprio funzionario o procedendo alla nomina di un avvocato non pregiudica la tutela giurisdizionale dello stesso Comune (in ordine al quale il rimettente non specifica neppure se si sia costituito in giudizio) e comunque ciò costituisce circostanza non rilevante al fine della tutela giurisdizionale del ricorrente e ininfluente al fine dell'applicazione da parte del giudice a quo della disposizione denunciata;&#13;
 che pertanto la questione sollevata in riferimento all'art. 111, secondo comma, Cost., è manifestamente inammissibile.&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 1) dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 74, comma 2, del decreto del Presidente della Repubblica 30 maggio 2002, n. 115, recante «Testo unico delle disposizioni legislative e regolamentari in materia di spese di giustizia (Testo A)», sollevata, in riferimento all'art. 111, secondo comma, della Costituzione, dal Giudice di pace di Roma con l'ordinanza indicata in epigrafe;&#13;
 2) dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 74, comma 2, del medesimo d.P.R. n. 115 del 2002, sollevata, in riferimento all'art. 3, primo comma, Cost., dal Giudice di pace di Roma con l'ordinanza indicata in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 25 settembre 2019. &#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Giovanni AMOROSO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 13 novembre 2019.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
