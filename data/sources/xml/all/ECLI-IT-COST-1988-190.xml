<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>190</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:190</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1988</data_decisione>
    <data_deposito>18/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi di legittimità costituzionale dell'art. 76 della legge 4    &#13;
 maggio 1983, n. 184 ("Disciplina dell'adozione e dell'affidamento dei    &#13;
 minori"),  promossi  con n. 2 ordinanze emesse il 4 aprile 1986 dalla    &#13;
 Corte d'appello di Milano sui ricorsi proposti da Marfoni Roberto  ed    &#13;
 altra  e  Consorti  Mauro  ed  altra,  iscritte  ai nn. 656 e 657 del    &#13;
 registro ordinanze 1986 e pubblicate nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 55, prima Serie speciale, dell'anno 1986;                  &#13;
    Visto  l'atto  di costituzione di Consorti Mauro ed altra, nonché    &#13;
 gli atti di intervento del Presidente del Consiglio dei ministri;        &#13;
    Udito  nell'udienza  pubblica  del  15  dicembre  1987  il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Uditi  l'avvocato  Bruno  de  Julio  per Consorti Mauro ed altra e    &#13;
 l'Avvocato  dello  Stato  Stefano  Onufrio  per  il  Presidente   del    &#13;
 Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Con due ordinanze del 4 aprile 1986 (R. O. nn. 656 e 657 del    &#13;
 1986) la Corte d'appello di Milano solleva questione di  legittimità    &#13;
 costituzionale,   in   riferimento  all'art.  3  della  Costituzione,    &#13;
 dell'art.  76  della  legge  4  maggio  1983,  n.  184   ("Disciplina    &#13;
 dell'adozione  e dell'affidamento dei minori"), nella parte in cui si    &#13;
 esclude che alle procedure di adozione di minori stranieri  in  corso    &#13;
 al  momento  dell'entrata in vigore della legge n. 184 del 1983 possa    &#13;
 applicarsi la nuova disciplina dell'adozione internazionale.             &#13;
    Dubita  il  giudice  a  quo  che  tale  disposizione determini una    &#13;
 irrazionale disparità di trattamento sulla  base  di  un  mero  dato    &#13;
 cronologico: a) tra minori stranieri da adottare, ammettendo a godere    &#13;
 della nuova maggiore tutela soltanto alcuni  ed  escludendone  altri,    &#13;
 per  i  quali il procedimento sia già iniziato all'entrata in vigore    &#13;
 della legge n. 184 del 1983; b) tra i cittadini  italiani  adottanti,    &#13;
 sottoponendoli  alla predetta discriminazione; c) tra minori italiani    &#13;
 e   minori   stranieri,   escludendo   solo   per    questi    ultimi    &#13;
 l'applicabilità alle procedure in corso della nuova normativa.          &#13;
    I due giudizi de quibus erano stati introdotti rispettivamente dai    &#13;
 coniugi Roberto Marfoni e Lucia Bigaran e dai coniugi Mauro  Consorti    &#13;
 e  Rosamaria  Roncaglio  per ottenere la declaratoria di efficacia in    &#13;
 Italia, con gli effetti dell'adozione speciale, di due  provvedimenti    &#13;
 stranieri;  il  primo dei quali, emesso dal Tribunale per i minorenni    &#13;
 di La Paz (Bolivia) il 7  maggio  1984,  aveva  concesso  ai  coniugi    &#13;
 Marfoni  l'affidamento  preadottivo  del bambino Ruperto Portales; il    &#13;
 secondo, emesso dal Tribunale Terzo della Famiglia di Guatemala il 25    &#13;
 ottobre  1985,  aveva  dichiarato la validità dell'adozione da parte    &#13;
 dei coniugi Consorti dei minori Rosa Antonia Garcia e  Mauro  Antonio    &#13;
 Chavez.                                                                  &#13;
    In  entrambe  le  fattispecie  i ricorrenti erano stati dichiarati    &#13;
 idonei all'adozione speciale con provvedimenti del  Tribunale  per  i    &#13;
 minorenni  di  Milano  per  i  coniugi  Marfoni e del Tribunale per i    &#13;
 minorenni de L'Aquila per i coniugi Consorti.                            &#13;
    Assume   il   giudice   a   quo   che,  conseguendo  la  richiesta    &#13;
 dichiarazione di efficacia in Italia dei due provvedimenti  stranieri    &#13;
 dall'applicazione  della previgente disciplina in materia di adozione    &#13;
 di minori stranieri, diventa rilevante  la  questione  sollevata,  in    &#13;
 quanto  dalla  sua  soluzione discende se debba applicarsi la vecchia    &#13;
 normativa oppure quella introdotta  dalla  legge  n.  184  del  1983,    &#13;
 ritenuta più favorevole.                                                &#13;
    2.  -  È  intervenuto  in  entrambi  i  giudizi il Presidente del    &#13;
 Consiglio dei ministri, rappresentato e difeso dall'Avvocatura  dello    &#13;
 Stato,  eccependo  preliminarmente  l'inammissibilità, per difetto di    &#13;
 rilevanza, della questione sollevata, in quanto nelle  sue  ordinanze    &#13;
 il  giudice  a  quo  si  limita  ad  osservare  che  la  richiesta di    &#13;
 dichiarazione di  efficacia  dei  provvedimenti  stranieri  è  stata    &#13;
 effettuata ai sensi della previgente normativa, senza nulla precisare    &#13;
 in ordine alla accoglibilità o meno  dell'istanza  alla  stregua  di    &#13;
 detta   normativa  e  comunque  senza  nulla  dedurre  in  ordine  al    &#13;
 pregiudizio  concreto  che   deriverebbe   dall'applicabilità   alle    &#13;
 fattispecie dell'ordinamento previgente anziché del nuovo.              &#13;
    Nel  merito  l'Avvocatura  deduce  l'infondatezza della questione,    &#13;
 richiamandosi alla sentenza di questa Corte n. 199 del 1986,  ove  si    &#13;
 afferma che la scelta del legislatore di non fare retroagire l'intera    &#13;
 normativa della  nuova  legge  rappresenta  una  scelta  razionale  e    &#13;
 comunque rientrante nella discrezionalità legislativa, "che incontra    &#13;
 il solo limite dei valori costituzionalmente garantiti". Tra  questi,    &#13;
 la citata sentenza indica quello della tutela del minore straniero in    &#13;
 stato  di  abbandono,  cui  sarebbe   irragionevole   non   estendere    &#13;
 retroattivamente  la  tutela  giurisdizionale  concessa  dall'art. 37    &#13;
 della legge n. 184 del 1983, che prevede nelle ipotesi  di  abbandono    &#13;
 l'applicazione  della  legge italiana. Ma ciò, secondo l'Avvocatura,    &#13;
 non  è  invocabile  nella  specie,  essendo  pacifica  in  causa  la    &#13;
 giurisdizione  del  magistrato  italiano,  anche  alla  stregua della    &#13;
 normativa previgente.                                                    &#13;
    3.  -  Si  sono  costituiti in giudizio i coniugi Mauro Consorti e    &#13;
 Rosamaria Roncaglio, rappresentati e difesi  dagli  avvocati  Giorgio    &#13;
 d'Episcopo  e  Bruno De Julio, eccependo anch'essi in via preliminare    &#13;
 l'irrilevanza della questione e  contestando  l'interpretazione  data    &#13;
 dalla  Corte d'appello di Milano circa la successione nel tempo delle    &#13;
 leggi in materia di adozione di minori stranieri.                        &#13;
    Sottolinea  in  particolare  la  difesa delle parti come l'art. 76    &#13;
 impugnato rappresenti in realtà una salvaguardia degli interessi del    &#13;
 minore  e  degli adottandi rispetto agli "inconvenienti che sarebbero    &#13;
 derivati  dalla  immediata  applicazione  della  nuova  legge,  quali    &#13;
 l'eventuale rimpatrio dei minori, il loro ricovero presso un istituto    &#13;
 in attesa dei provvedimenti di cui all'art. 37".                         &#13;
    Senza  la disposizione dell'art. 76, tra l'altro, il Tribunale per    &#13;
 i minorenni sarebbe stato costretto - secondo le parti - a respingere    &#13;
 tutte  le  istanze dirette ad ottenere l'attribuzione di efficacia ai    &#13;
 provvedimenti stranieri emessi prima della nuova legge.                  &#13;
    Nel  merito  le parti assumono l'infondatezza della questione, non    &#13;
 ravvisando  nella  disposizione  impugnata  alcuna  delle   lamentate    &#13;
 violazioni al dettato costituzionale.<diritto>Considerato in diritto</diritto>1.  - La Corte d'appello di Milano, con due ordinanze del 4 aprile    &#13;
 1986 (R.O. nn. 656 e 657/1986), chiede a  questa  Corte  verifica  di    &#13;
 costituzionalità  dell'art. 76 della legge 4 maggio 1983, n. 184, in    &#13;
 riferimento all'art.  3  della  Costituzione,  "nella  parte  in  cui    &#13;
 esclude  che  la  nuova  normativa  possa  applicarsi  alla procedura    &#13;
 relativa all'adozione di minori stranieri 'in corso' al momento della    &#13;
 entrata in vigore della legge stessa".                                   &#13;
    2. - La questione non è fondata.                                     &#13;
    Questa  Corte,  con  sentenza  n.  199  del 1986, ha statuito: "La    &#13;
 scelta del legislatore, di non fare retroagire  la  intera  normativa    &#13;
 della   legge  n.  184  del  1983,  per  salvaguardare  la  sollecita    &#13;
 definizione o la definitività delle procedure di adozione in corso o    &#13;
 concluse  sotto  l'impero  della precedente legge n. 431 del 1967, è    &#13;
 una scelta razionale e  comunque  rientrante  nella  discrezionalità    &#13;
 legislativa.  Anche  in questo caso la Corte non può che ribadire il    &#13;
 proprio insegnamento (sent. n. 118 del 1957 e sent. n. 36  del  1985)    &#13;
 che 'nel nostro ordinamento il principio della irretroattività della    &#13;
 legge   non   assurge,   nella   sua   assolutezza,    a    principio    &#13;
 costituzionale'",  salva  sempre la statuizione dell'art. 25, secondo    &#13;
 comma, della Costituzione. Pertanto è rimessa alla  valutazione  del    &#13;
 legislatore la scelta tra retroattività e irretroattività in ordine    &#13;
 ai fini che intende raggiungere, con il solo  limite  che  non  siano    &#13;
 contraddetti princìpi e valori costituzionali".                         &#13;
    Nella  specie  tale  contraddizione non sussiste, né si rileva in    &#13;
 concreto pregiudizio derivante  dall'applicabilità  della  normativa    &#13;
 previgente  rispetto  alla  successiva,  né emergono dalle ordinanze    &#13;
 nuovi profili.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata la questione di legittimità costituzionale,    &#13;
 in riferimento all'art. 3  della  Costituzione,  dell'art.  76  della    &#13;
 legge   4   maggio   1983,   n.   184  ("Disciplina  dell'adozione  e    &#13;
 dell'affidamento dei minori"), sollevata  dalla  Corte  d'appello  di    &#13;
 Milano con le ordinanze indicate in epigrafe.                            &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte Costituzionale,    &#13;
 palazzo della Consulta il 10 febbraio 1988.                              &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 febbraio 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
