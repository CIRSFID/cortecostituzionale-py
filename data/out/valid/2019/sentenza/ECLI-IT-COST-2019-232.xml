<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-11-13/232/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-11-13/232"/>
          <FRBRalias value="ECLI:IT:COST:2019:232" name="ECLI"/>
          <FRBRdate date="2019-11-13" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="232"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-11-13/232/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-11-13/232/ita@"/>
          <FRBRdate date="2019-11-13" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-11-13/232/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-11-13/232/ita@.xml"/>
          <FRBRdate date="2019-11-13" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2019-10-08" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2019-11-13" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="LATTANZI" href="/akn/ontology/person/it/LATTANZI" showAs="LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2019</akn4cc:anno>
        <akn4cc:numero>232</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2019:232</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>LATTANZI</akn4cc:presidente>
        <akn4cc:relatore>Giulio Prosperetti</akn4cc:relatore>
        <akn4cc:data_decisione>2019-10-08</akn4cc:data_decisione>
        <akn4cc:data_deposito>2019-11-13</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 6, comma 6, della legge della Regione Sardegna 5 novembre 2018, n. 40 (Disposizioni finanziarie e seconda variazione al bilancio 2018-2020), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 7-16 gennaio 2019, depositato in cancelleria il 15 gennaio 2019, iscritto al n. 2 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 6, prima serie speciale, dell'anno 2019.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione autonoma Sardegna;
 udito nell'udienza pubblica dell'8 ottobre 2019 il Giudice relatore Giulio Prosperetti;
 uditi l'avvocato dello Stato Francesca Morici per il Presidente del Consiglio dei ministri e l'avvocato Mattia Pani per la Regione autonoma Sardegna.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso notificato il 7-16 gennaio 2019 e depositato il 15 gennaio 2019 (reg. ric. n. 2 del 2019), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale dell'art. 6, comma 6, della legge della Regione Sardegna 5 novembre 2018, n. 40 (Disposizioni finanziarie e seconda variazione al bilancio 2018-2020), in riferimento all'art. 3, primo comma, lettera a), della legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna), e agli artt. 3 e 117, secondo comma, lettera l), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione all'<ref href="/akn/it/act/decretoLegislativo/stato/2017-05-25/75/!main#art_23__para_2">art. 23, comma 2, del decreto legislativo 25 maggio 2017, n. 75</ref>, recante «Modifiche e integrazioni al <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main">decreto legislativo 30 marzo 2001, n. 165</ref>, ai sensi degli articoli 16, commi 1, lettera a), e 2, lettere b), c), d) ed e) e 17, comma 1, lettere a), c), e), f), g), h), l) m), n), o), q), r), s) e z), della <ref href="/akn/it/act/legge/stato/2015-08-07/124/!main">legge 7 agosto 2015, n. 124</ref>, in materia di riorganizzazione delle amministrazioni pubbliche».
 2.- Il Presidente del Consiglio rappresenta che la norma impugnata, al fine di omogeneizzare i trattamenti retributivi dei dipendenti dell'Agenzia forestale regionale per lo sviluppo del territorio e l'ambiente della Sardegna (FoReSTAS) con quelli del personale del comparto di contrattazione regionale, ha incrementato le risorse da destinare alla contrattazione collettiva integrativa nel triennio 2016-2018, senza distinguere tra trattamenti di natura fondamentale e accessoria; in tal modo il legislatore regionale, eccedendo la competenza legislativa attribuitagli dall'art. 3, comma 1, lettera a), dello statuto reg. Sardegna e in violazione del principio di uguaglianza, avrebbe invaso la competenza esclusiva del legislatore nazionale in materia di ordinamento civile per la possibile incidenza della norma impugnata sulla contrattazione collettiva e si sarebbe posto in contrasto con l'<ref href="/akn/it/act/decretoLegislativo/stato/2017/75/!main#art_23">art. 23 del d.lgs. n. 75 del 2017</ref>, che impone il contenimento del salario accessorio nei limiti di quello goduto nell'anno 2016.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>- La difesa dello Stato ricorda che la disciplina del lavoro pubblico, ivi compreso il trattamento economico, rientra nella materia dell'ordinamento civile di competenza del legislatore statale e da questo è stata demandata alla contrattazione collettiva, e deduce che, per esigenze di uniformità di tutela su tutto il territorio nazionale, il principio di riserva di contrattazione si impone, quale norma fondamentale di riforma economico-sociale, anche alle Regioni ad autonomia speciale, e quindi alla Regione autonoma Sardegna, pur a fronte di competenze legislative statutarie in materia di stato giuridico ed economico del personale.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>- Il Presidente del Consiglio prosegue rappresentando che, ai sensi del combinato disposto degli <mref><ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_40">artt. 40</ref> e <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_45">45 del decreto legislativo 30 marzo 2001, n. 165</ref></mref> (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), spetta alla contrattazione collettiva integrativa la definizione dei trattamenti economici accessori, purché ciò avvenga nel rispetto dei vincoli di bilancio e dei limiti derivanti dalla legge statale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Tra tali limiti viene in rilievo l'<ref href="/akn/it/act/decretoLegislativo/stato/2017/75/!main#art_23">art. 23 del d.lgs. n. 75 del 2017</ref>, che individua, quale limite alle risorse stanziabili per il trattamento accessorio dei dipendenti pubblici, l'importo stanziato per l'anno 2016.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>5.</num>
            <content>
              <p>- L'incremento delle risorse da destinare alla contrattazione integrativa, apportato dalla legge impugnata, è stato disposto per omogeneizzare i trattamenti economici dei dipendenti di FoReSTAS con quelli del personale del comparto di contrattazione regionale, per il quale è stato sottoscritto il contratto collettivo regionale di lavoro, relativo al triennio 2016-2018, certificato dalla Corte dei Conti.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La difesa dello Stato sottolinea che spetta alla contrattazione collettiva (nella specie quella della Regione ad autonomia speciale) disporre le modalità di utilizzo delle risorse e individuare i limiti finanziari entro cui deve svolgersi la contrattazione integrativa, nel rispetto dei limiti stabiliti dalla stessa contrattazione collettiva di primo livello, dei parametri di virtuosità fissati per le spese del personale dalle vigenti disposizioni e, comunque, nel rispetto degli obiettivi di finanza pubblica e di strumenti analoghi di contenimento della spesa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>D'altro canto, prosegue l'Avvocatura dello Stato, come ribadito dalla Corte dei conti, sezione Lombardia, con il parere 28 marzo 2013, n. 137, lo stanziamento di risorse aggiuntive per la contrattazione decentrata può essere effettuato con legge regionale soltanto se vi sia una clausola di rinvio statale che abiliti espressamente il legislatore regionale ad intervenire.
 6.- In tale complesso sistema di fonti e a fronte del limite imposto all'aumento delle risorse destinate al trattamento accessorio dall'<ref href="/akn/it/act/decretoLegislativo/stato/2017/75/!main#art_23">art. 23 del d.lgs n. 75 del 2017</ref>, la legge regionale oggetto di impugnazione, non prevedendo alcun limite allo stanziamento per i trattamenti accessori e non richiamando i limiti posti dalla normativa statale, avrebbe violato l'art. 117, secondo comma, lettera l), Cost., che attribuisce al legislatore nazionale la competenza esclusiva in materia di «ordinamento civile», nonché l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, per la disparità di trattamento economico che deriverebbe al personale interessato, sia rispetto al restante personale della Regione che al personale di altre Regioni.
 7.- Con atto depositato il 25 febbraio 2019 si è costituita la resistente chiedendo il rigetto delle questioni prospettate; con successiva memoria del 30 aprile 2019 la Regione autonoma Sardegna ha dedotto l'inammissibilità delle questioni per mancata considerazione delle norme statutarie attributive della competenza legislativa primaria in materia di «ordinamento degli uffici e degli enti amministrativi della Regione e stato giuridico ed economico del personale» (art. 3, primo comma, lettera a, dello statuto reg. Sardegna), non essendo condivisibile quanto asserito dalla difesa statale in ordine al fatto che la materia dei trattamenti economici dei pubblici dipendenti è regolata dalla legge statale e dalla contrattazione collettiva e, solo se da esse previsto, dalla contrattazione decentrata e dalla normativa regionale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Così ragionando, secondo la Regione autonoma, si ridurrebbe la competenza primaria statutaria ad una mera potestà legislativa concorrente o residuale; la competenza legislativa statale in materia, invece, andrebbe intesa come mera riserva sui principi e sulle regole fondamentali di diritto civile, mentre il legislatore regionale resterebbe libero, per il resto, di disciplinare i profili privatistici rientranti nelle materie di propria competenza.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Quale ulteriore vizio di inammissibilità, la resistente ha dedotto il difetto di interesse dello Stato a censurare la norma in esame e il difetto di motivazione in ordine al suddetto interesse.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Secondo la Regione autonoma Sardegna non sarebbe chiaro il motivo per cui lo Stato abbia impugnato la norma regionale, poiché essa ha provveduto a dare attuazione all'obiettivo di omogeneizzare i trattamenti economici del personale di FoReSTAS con il restante personale della Regione, delle Agenzie e degli enti regionali strumentali; tale omogeneizzazione, intervenuta dopo e in conformità alla sottoscrizione del contratto collettivo regionale di lavoro relativo al triennio 2016-2018, certificato dalla sezione di controllo della Corte dei conti, garantirebbe, invece, l'uniformità dei trattamenti retributivi nel rispetto del principio di uguaglianza.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Le questioni prospettate sarebbero, quindi, inammissibili per difetto di motivazione, non essendo chiara la ragione della contestazione e non essendo sufficiente la mera enunciazione delle norme costituzionali che si assumono violate, senza l'esplicitazione di un'adeguata motivazione a sostegno dell'impugnazione.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>8.</num>
            <content>
              <p>- Nel merito la resistente ha eccepito l'infondatezza del ricorso poiché la legge regionale si è limitata a intervenire sul trattamento di base dei dipendenti di FoReSTAS, per omogeneizzarlo rispetto a quello degli altri dipendenti regionali, senza alcuna ricaduta sui trattamenti accessori, non essendovi prova che le somme stanziate siano destinate ad incrementarli.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In particolare, la legge impugnata avrebbe dato attuazione alla previsione di cui all'art. 51 della legge della Regione Sardegna 26 aprile 2016, n. 8 (Legge forestale della Sardegna), per effetto della quale le risorse da destinare alla contrattazione collettiva dei dipendenti dell'Agenzia forestale sono determinate con apposita norma regionale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'art. 6, comma 6, della legge reg. Sardegna n. 40 del 2018 avrebbe provveduto a stanziare tali risorse tenendo conto del fatto che al personale di FoReSTAS, ai sensi dell'art. 48, comma 2, della stessa legge reg. Sardegna n. 8 del 2016, si applicava il contratto collettivo nazionale degli operai forestali e impiegati agricoli, ma che con l'inquadramento di tale personale in quello del "Sistema Regione", che include tutti i dipendenti regionali, vi era l'esigenza di stanziare risorse per garantire l'omogeneizzazione dei trattamenti retributivi di base (inquadramento che di fatto è avvenuto con la successiva legge della Regione autonoma Sardegna 19 novembre 2018, n. 43, recante «Norme in materia di inquadramento del personale dell'Agenzia FoReSTAS», non citata dalla Regione).
 9.- Con successiva memoria del 17 settembre 2019 il Presidente del Consiglio ha contestato i motivi di inammissibilità dedotti dalla resistente e ha ribadito le medesime argomentazioni di merito sviluppate nel ricorso, senza replicare in ordine all'inserimento del personale di FoReSTAS nel comparto unico regionale.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Presidente del Consiglio dei ministri ha promosso - in riferimento all'art. 3, primo comma, lettera a), della legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna), e agli artt. 3 e 117, secondo comma, lettera l), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> - questioni di legittimità costituzionale dell'art. 6, comma 6, della legge della Regione Sardegna 5 novembre 2018, n. 40 (Disposizioni finanziarie e seconda variazione al bilancio 2018-2020) che, al fine di omogeneizzare i trattamenti retributivi dei dipendenti dell'Agenzia forestale regionale per lo sviluppo del territorio e l'ambiente della Sardegna (FoReSTAS) con quelli del personale di comparto di contrattazione regionale, ha incrementato «di euro 1.000.000», a decorrere dall'anno 2018, le risorse da destinare alla contrattazione collettiva integrativa del personale di FoReSTAS, nel triennio 2016-2018.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>- Nel ricorso statale si censura l'incremento delle risorse destinate alla contrattazione integrativa dei dipendenti di FoReSTAS poiché, mancando la precisazione in ordine alla destinazione delle somme, alla componente fissa o accessoria della retribuzione, sarebbe possibile un incremento del trattamento accessorio di questo personale, senza il rispetto dei limiti posti dall'<ref href="/akn/it/act/decretoLegislativo/stato/2017-05-25/75/!main#art_23__para_2">art. 23, comma 2, del decreto legislativo 25 maggio 2017, n. 75</ref>, recante «Modifiche e integrazioni al <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main">decreto legislativo 30 marzo 2001, n. 165</ref>, ai sensi degli articoli 16, commi 1, lettera a), e 2, lettere b), c), d) ed e) e 17, comma 1, lettere a), c), e), f), g), h), l) m), n), o), q), r), s) e z), della <ref href="/akn/it/act/legge/stato/2015-08-07/124/!main">legge 7 agosto 2015, n. 124</ref>, in materia di riorganizzazione delle amministrazioni pubbliche», e in violazione del principio di riserva di contrattazione collettiva nel pubblico impiego, previsto dal legislatore statale nell'esercizio della competenza esclusiva in materia di ordinamento civile.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>- Le questioni sono inammissibili.
 4.- Il ricorso avverso una norma regionale che arrechi pregiudizio alle attribuzioni statali, invadendo materie rientranti nelle competenze legislative esclusive dello Stato, deve essere adeguatamente motivato e, a supporto delle censure prospettate, deve chiarire il meccanismo attraverso cui si realizza il preteso vulnus lamentato; quando il vizio sia prospettato in relazione a norme interposte specificamente richiamate è necessario evidenziare la pertinenza e la coerenza di tale richiamo rispetto al parametro evocato.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nel caso in cui difetti un adeguato impianto argomentativo, questa Corte può rilevare, d'ufficio, l'inammissibilità delle censure per genericità e insufficiente motivazione circa l'asserito contrasto con il parametro interposto (ex mult<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/196/!main">is, sentenze n. 196 del 2017</ref>, <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2017/201/!main">ordinanza n. 201 del 2017</ref>).
 5.- Tale è la situazione che si configura nel caso di specie.
 Innanzitutto, il Presidente del Consiglio lamenta l'asserita lesione della competenza del legislatore statale in materia di «ordinamento civile», attribuitagli in via esclusiva dall'art. 117, secondo comma, lettera l), Cost., che sarebbe incisa dalla impugnata norma sarda, la quale incrementa il fondo per l'erogazione dei trattamenti accessori dei dipendenti di FoReSTAS, al fine della omogeneizzazione del loro trattamento con tutto il personale regionale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'invasione della competenza statale nella materia «ordinamento civile» si configura ogni qual volta il legislatore regionale, sostituendosi al contratto collettivo, regoli direttamente un aspetto della retribuzione, poiché, nell'esercizio della propria competenza esclusiva, il legislatore nazionale ha riservato al contratto collettivo l'attribuzione del trattamento economico, fondamentale e accessorio, del personale pubblico (<mref><ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_2__para_3">art. 2, comma 3</ref>, e <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_45">art. 45 del decreto legislativo 30 marzo 2001, n. 165</ref></mref>, recante «Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche»).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La necessità di una disciplina unitaria dei rapporti di lavoro alle dipendenze della pubblica amministrazione si è imposta a seguito della privatizzazione del pubblico impiego e della conseguente esigenza di un trattamento uniforme di tali tipi di rapporti; in tale prospettiva questa Corte ha precisato che «i princípi fissati dalla legge statale in materia costituiscono tipici limiti di diritto privato, fondati sull'esigenza, connessa al precetto costituzionale di eguaglianza, di garantire l'uniformità nel territorio nazionale delle regole fondamentali di diritto che disciplinano i rapporti fra privati e, come tali, si impongono anche alle Regioni a statuto speciale» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2007/189/!main">sentenza n. 189 del 2007</ref>).
 Tra tali principi rientra, per espresso dettato normativo, il principio di riserva di contrattazione collettiva, con la conseguenza che qualunque norma regionale che intenda sostituirsi alla negoziazione delle parti, quale imprescindibile fonte di disciplina del rapporto, comporta un'illegittima intrusione nella sfera di attribuzione del legislatore nazionale.
 6.- Ma, contraddittoriamente, la difesa erariale poggia la propria argomentazione sul mancato richiamo dell'<ref href="/akn/it/act/decretoLegislativo/stato/2017/75/!main#art_23">art. 23 del d.lgs. n. 75 del 2017</ref>, che pone un limite alle somme stanziabili per l'erogazione del trattamento accessorio dei dipendenti regionali, solo presuntivamente superato e, comunque, riferito alla futura conseguenziale negoziazione contrattuale, senza spiegare come ciò avrebbe inciso sull'ordinamento civile.
 Pertanto, non si può non rilevare l'incertezza dei parametri evocati dalla difesa dello Stato con riferimento ad una normativa regionale in piena evoluzione, come dimostrato dalla successiva e pressoché contemporanea legge della Regione autonoma Sardegna 19 novembre 2018, n. 43, recante «Norme in materia di inquadramento del personale dell'Agenzia FoReSTAS», che ha inserito a tutti gli effetti il personale di FoReSTAS nel comparto del personale regionale.
 In conclusione, la motivazione appare perplessa per l'incoerenza della norma interposta rispetto ai parametri evocati e, quindi, va ritenuta l'inammissibilità delle questioni di costituzionalità.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara inammissibili le questioni di legittimità costituzionale dell'art. 6, comma 6, della legge della Regione Sardegna 5 novembre 2018, n. 40 (Disposizioni finanziarie e seconda variazione al bilancio 2018-2020), promosse dal Presidente del Consiglio dei ministri, in riferimento all'art. 3, primo comma, lettera a), della legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna), e agli artt. 3 e 117, secondo comma, lettera l), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione all'<ref href="/akn/it/act/decretoLegislativo/stato/2017-05-25/75/!main#art_23__para_2">art. 23, comma 2, del decreto legislativo 25 maggio 2017, n. 75</ref>, recante «Modifiche e integrazioni al <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main">decreto legislativo 30 marzo 2001, n. 165</ref>, ai sensi degli articoli 16, commi 1, lettera a), e 2, lettere b), c), d) ed e) e 17, comma 1, lettere a), c), e), f), g), h), l) m), n), o), q), r), s) e z), della <ref href="/akn/it/act/legge/stato/2015-08-07/124/!main">legge 7 agosto 2015, n. 124</ref>, in materia di riorganizzazione delle amministrazioni pubbliche», con il ricorso indicato in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'<date refersTo="#decisionDate" date="2019-10-08">8 ottobre 2019</date>.
 F.to:
 <signature><judge refersTo="#giorgioLattanzi" as="#presidente">Giorgio LATTANZI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulioProsperetti" as="#relatore">Giulio PROSPERETTI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2019-11-13">13 novembre 2019</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
