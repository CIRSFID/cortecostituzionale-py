<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>403</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:403</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ONIDA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>13/12/2004</data_decisione>
    <data_deposito>21/12/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 14, comma 5-quinquies, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), inserito dall'art. 13 della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), promossi, nell'ambito di diversi procedimenti penali, dal Tribunale di Milano con ordinanza del 24 febbraio 2003 (iscritta al n. 426 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 27, prima serie speciale, dell'anno 2003), con ordinanza del 26 marzo 2003 (iscritta al n. 427 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 27, prima serie speciale, dell'anno 2003), con due ordinanze del 12 marzo 2003 (iscritte ai numeri 445 e 605 del registro ordinanze 2003 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 28 e n. 35, prima serie speciale, dell'anno 2003), con ordinanza del 3 gennaio 2003 (iscritta al n. 606 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 35, prima serie speciale, dell'anno 2003), con due ordinanze del 7 maggio 2003 (iscritte ai numeri 628 e 882 del registro ordinanze 2003 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 35 e n. 44, prima serie speciale, dell'anno 2003), con ordinanza del 15 luglio 2003 (iscritta al n. 1098 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 51, prima serie speciale, dell'anno 2003), con due ordinanze del 26 settembre 2003 (iscritte ai numeri 285 e 286 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 16, prima serie speciale, dell'anno 2004), con due ordinanze in data 11 novembre 2003 (iscritte ai numeri 639 e 640 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 28, prima serie speciale, dell'anno 2004). &#13;
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 1° dicembre 2004 il Giudice relatore Guido Neppi Modona. &#13;
    Ritenuto che con dodici ordinanze identiche nella parte motiva il Tribunale di Milano ha sollevato, in riferimento agli artt. 3 e 13, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 14, comma 5-quinquies, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), inserito dall'art. 13 della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), nella parte in cui prevede per il reato di cui al comma 5-ter della medesima disposizione l'arresto obbligatorio dell'autore del fatto; &#13;
    che il rimettente procede all'udienza di convalida nei confronti di cittadini stranieri tratti in arresto nella flagranza del reato di cui all'art. 14, comma 5-ter, del decreto legislativo n. 286 del 1998, perché sorpresi nel territorio dello Stato dopo la scadenza del termine entro il quale avrebbero dovuto lasciare il territorio nazionale, come da provvedimento emesso dal questore a norma dell'art. 14, comma 5-bis, dello stesso decreto; &#13;
    che nei giudizi iscritti ai numeri 628 e 882 del registro ordinanze del 2003 è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate inammissibili o infondate. &#13;
    Considerato che, essendo censurato in tutte le ordinanze l'art. 14, comma 5-quinquies, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), inserito dall'art. 13 della legge 30 luglio 2002, n. 189, deve essere disposta la riunione dei relativi giudizi;  &#13;
    che le questioni in esame hanno ad oggetto la previsione dell'arresto obbligatorio in relazione al reato contravvenzionale di cui all'art. 14, comma 5-ter, del decreto legislativo n. 286 del 1998, nel testo precedente alle modifiche apportate dal decreto-legge 14 settembre 2004, n. 241 (Disposizioni urgenti in materia di immigrazione), convertito, con modificazioni, nella legge 12 novembre 2004, n. 271; &#13;
    che, successivamente alle ordinanze di rimessione, questa Corte con sentenza n. 223 del 2004 ha dichiarato costituzionalmente illegittimo l'art. 14, comma 5-quinquies, del decreto legislativo n. 286 del 1998, nella parte in cui stabilisce che per il reato previsto dal comma 5-ter del medesimo articolo è obbligatorio l'arresto dell'autore del fatto; &#13;
    che gli atti devono pertanto essere restituiti al giudice rimettente.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti al Tribunale di Milano. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 dicembre 2004. &#13;
    F.to: &#13;
    Valerio ONIDA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 21 dicembre 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
