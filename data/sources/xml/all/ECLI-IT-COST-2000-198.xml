<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>198</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:198</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GUIZZI</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia>Carlo Mezzanotte</redattore_pronuncia>
    <data_decisione>08/06/2000</data_decisione>
    <data_deposito>16/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Francesco GUIZZI; &#13;
 Giudici: Fernando SANTOSUOSSO giudice, Massimo VARI, Cesare RUPERTO, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Carlo MEZZANOTTE, Guido NEPPI &#13;
 MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
 Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 13, comma 8,    &#13;
 del  decreto  legislativo  25  luglio 1998, n. 286 (Testo unico delle    &#13;
 disposizioni  concernenti  la  disciplina  dell'immigrazione  e norme    &#13;
 sulla  condizione  dello straniero), promosso con ordinanza emessa il    &#13;
 26  novembre  1998  dal Pretore di Modena nel procedimento civile tra    &#13;
 Igbinobaro  Grace  Igbiniken  e  il  Prefetto  di Modena, iscritta al    &#13;
 n. 104  del  registro  ordinanze  1999  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 10,  prima serie speciale, dell'anno    &#13;
 1999;                                                                    &#13;
     Visto  l'atto  d'intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 5 aprile 2000 il giudice    &#13;
 relatore Francesco Guizzi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.   -   Una   cittadina  extracomunitaria  proponeva,  ai  sensi    &#13;
 dell'art. 13  del  decreto  legislativo 25 luglio 1998, n. 286 (Testo    &#13;
 unico  delle disposizioni concernenti la disciplina dell'immigrazione    &#13;
 e norme sulla condizione dello straniero), un reclamo tardivo avverso    &#13;
 il  decreto  prefettizio  di  espulsione,  sostenendo  che  la  copia    &#13;
 notificatale,  contrariamente a quanto previsto da tale disposizione,    &#13;
 non  era  accompagnata  dalla  traduzione  in  una lingua a lei nota.    &#13;
 Circostanza,  questa,  che  le  aveva  impedito  di conoscere nel suo    &#13;
 esatto  contenuto il provvedimento, e di proporre tempestivo reclamo.    &#13;
 L'opponente  chiedeva  quindi  di  essere  rimessa  in  termini, e il    &#13;
 Pretore  di  Modena  ha sollevato, in riferimento agli artt. 24 e 113    &#13;
 della  Costituzione,  questione  di  legittimità del citato art. 13,    &#13;
 nella  parte  in  cui  non  consente il reclamo tardivo, allorché il    &#13;
 destinatario non abbia potuto rispettare il termine di legge per caso    &#13;
 fortuito,   forza   maggiore  o,  come  nella  specie,  per  l'omessa    &#13;
 traduzione  del  provvedimento  stesso  in  una lingua conosciuta dal    &#13;
 destinatario.                                                            &#13;
     In proposito il giudice a quo osserva che i termini perentori, ai    &#13;
 sensi   dell'art. 153  del  codice  di  procedura  civile,  non  sono    &#13;
 prorogabili  e  che  nella  vicenda  al suo esame non è possibile la    &#13;
 rimessione  in  termini, poiché nel sistema del processo civile essa    &#13;
 è  prevista  dall'art. 184-bis  del  codice  di rito soltanto per le    &#13;
 decadenze  incolpevoli  che  si  verificano all'interno della singola    &#13;
 fase processuale.                                                        &#13;
     Nel   motivare   sulla   rilevanza,   il  Pretore  riconosce  che    &#13;
 l'opponente  aveva  diritto alla notifica del provvedimento espulsivo    &#13;
 corredato  dalla traduzione in lingua inglese e sostiene che la norma    &#13;
 oggetto  di censura violerebbe gli artt. 24 e 113 della Costituzione,    &#13;
 poiché  rende  eccessivamente difficoltosa la tutela dei diritti del    &#13;
 cittadino   extracomunitario,   precludendogli  la  possibilità  del    &#13;
 reclamo  anche  quando  il  mancato rispetto del termine non implichi    &#13;
 colpa.                                                                   &#13;
                                                                          &#13;
     2.  -  Si è costituito il Presidente del Consiglio dei Ministri,    &#13;
 con  il  patrocinio dell'Avvocatura dello Stato, la quale ha concluso    &#13;
 per  l'infondatezza  della  questione,  affermando  che il rimettente    &#13;
 avrebbe   potuto   superare   in   via   interpretativa  i  dubbi  di    &#13;
 costituzionalità. La difesa del Governo ricorda che la remissione in    &#13;
 termini  per errore scusabile costituisce "principio generalissimo" e    &#13;
 soggiunge  che  la  decadenza  non  si verifica quando l'atto non sia    &#13;
 compiuto  legittimamente:  nel  caso di specie, la mancata traduzione    &#13;
 avrebbe pertanto impedito il decorso del termine per l'impugnativa.<diritto>Considerato in diritto</diritto>1.   -   Il   Pretore   di   Modena   dubita  della  legittimità    &#13;
 costituzionale  dell'art. 13, comma 8, del decreto legislativo n. 286    &#13;
 del  1998, nella parte in cui non consentirebbe l'opposizione tardiva    &#13;
 avverso  il  decreto prefettizio di espulsione dello straniero quando    &#13;
 questi  non  abbia  avuto  conoscenza,  senza  colpa,  del suo esatto    &#13;
 contenuto.                                                               &#13;
                                                                          &#13;
     2. - La questione non è fondata.                                    &#13;
     In  base alla prospettazione del rimettente, il termine di cinque    &#13;
 giorni,  fissato  per  il  reclamo  dal  citato  art. 13, comma 8, si    &#13;
 consumerebbe  anche  nel caso in cui il decreto di espulsione non sia    &#13;
 portato  a  conoscenza  dell'interessato ovvero gli sia comunicato in    &#13;
 modo  non  intelligibile, perché - come si verifica nella vicenda de    &#13;
 qua - privo di traduzione.                                               &#13;
     Così   interpretata,   la   norma   lederebbe   l'art. 24  della    &#13;
 Costituzione.  Ma  questa lettura della disciplina, dalla quale muove    &#13;
 il rimettente, non è l'unica consentita.                                &#13;
     Al riguardo si deve premettere che l'art. 2, comma 1, del decreto    &#13;
 legislativo n. 286 dispone che "allo straniero comunque presente alla    &#13;
 frontiera  o  nel  territorio dello Stato sono riconosciuti i diritti    &#13;
 fondamentali  della  persona  umana  previsti  dalle norme di diritto    &#13;
 interno, dalle convenzioni internazionali in vigore e dai principi di    &#13;
 diritto   internazionale   generalmente   riconosciuti".  Anche  allo    &#13;
 straniero  deve  quindi  essere  riconosciuto  il pieno esercizio del    &#13;
 diritto di difesa, sancito dall'art. 24 della Costituzione e tutelato    &#13;
 altresì  dal  Patto  internazionale  sui  diritti  civili e politici    &#13;
 stipulato a New York il 19 dicembre 1966, ratificato e reso esecutivo    &#13;
 con  la  legge 25 ottobre 1977, n. 881, ove all'art. 13 si stabilisce    &#13;
 che  "uno  straniero  che  si  trovi legalmente nel territorio di uno    &#13;
 Stato  parte  del  presente  Patto non può esserne espulso se non in    &#13;
 base a una decisione presa in conformità della legge e, salvo che vi    &#13;
 si  oppongano  imperiosi motivi di sicurezza nazionale, deve avere la    &#13;
 possibilità   di  far  valere  le  proprie  ragioni  contro  la  sua    &#13;
 espulsione,  di  sottoporre  il proprio caso all'esame dell'autorità    &#13;
 competente, o di una o più persone specificamente designate da detta    &#13;
 autorità,  e  di  farsi  rappresentare  innanzi ad esse a tal fine".    &#13;
 Principio  analogo  è  poi  ribadito nell'art. 1 del Protocollo n. 7    &#13;
 alla  convenzione  per  la salvaguardia dei diritti dell'uomo e delle    &#13;
 libertà  fondamentali,  adottato  a  Strasburgo il 22 novembre 1984,    &#13;
 ratificato e reso esecutivo con la legge 9 aprile 1990, n. 98.           &#13;
     È  da  considerare,  altresì,  che  il diritto a un riesame del    &#13;
 provvedimento  di  espulsione,  con  piena  garanzia  del  diritto di    &#13;
 difesa,   spetta   non   soltanto   agli  stranieri  che  soggiornano    &#13;
 legittimamente  in  Italia,  ma  anche  a  coloro  che  sono presenti    &#13;
 illegittimamente sul territorio nazionale, come testimonia la lettera    &#13;
 dell'art. 13, comma 8, del decreto legislativo n. 286 del 1998, ov'è    &#13;
 ripresa  la  formula, contenuta nell'art. 2, comma 1, dello straniero    &#13;
 "comunque presente [...] nel territorio dello Stato".                    &#13;
     Il pieno esercizio del diritto di difesa da parte dello straniero    &#13;
 presuppone,  dunque,  che  qualsiasi  atto proveniente dalla pubblica    &#13;
 amministrazione,  diretto  a  incidere sulla sua sfera giuridica, sia    &#13;
 concretamente  conoscibile. Ciò vuol dire, con specifico riferimento    &#13;
 al  decreto di espulsione, che questo deve essere redatto anche nella    &#13;
 lingua  del  destinatario  ovvero,  se  non  sia possibile, in una di    &#13;
 quelle  lingue che - per essere le più diffuse - si possano ritenere    &#13;
 probabilmente  più  accessibili dal destinatario. A tali principi si    &#13;
 è del resto conformato il legislatore, statuendo, all'art. 13, comma    &#13;
 7,  che  "il  decreto  di  espulsione  [...]  nonché ogni altro atto    &#13;
 concernente  l'ingresso, il soggiorno e l'espulsione, sono comunicati    &#13;
 all'interessato   unitamente   all'indicazione   delle  modalità  di    &#13;
 impugnazione  e  ad  una  traduzione in una lingua da lui conosciuta,    &#13;
 ovvero,  ove  non  sia  possibile,  in  lingua  francese,  inglese  o    &#13;
 spagnola".                                                               &#13;
                                                                          &#13;
     3.  -  Lo  straniero  (anche irregolarmente soggiornante) gode di    &#13;
 tutti  i diritti fondamentali della persona umana, fra i quali quello    &#13;
 di  difesa, il cui esercizio effettivo implica che il destinatario di    &#13;
 un   provvedimento,   variamente   restrittivo   della   libertà  di    &#13;
 autodeterminazione, sia messo in grado di comprenderne il contenuto e    &#13;
 il significato.                                                          &#13;
     Ora,  va  ricordato  il principio - che si rinviene nel sistema e    &#13;
 ispira le singole disposizioni positive - secondo cui ogni qual volta    &#13;
 la  legge fissa un termine perentorio, prevedendone la decorrenza dal    &#13;
 compimento di un determinato atto, è necessario che quest'ultimo sia    &#13;
 effettivamente compiuto, non contenga vizi e sia portato a conoscenza    &#13;
 di colui che è onerato dal rispetto di esso.                            &#13;
     La  traduzione del decreto di espulsione è dunque preordinata ad    &#13;
 assicurare  la  sua effettiva conoscibilità; e questa è presupposto    &#13;
 essenziale  per  l'esercizio del diritto di difesa, di cui gode anche    &#13;
 lo straniero irregolarmente presente sul territorio nazionale.           &#13;
                                                                          &#13;
     4.  -  Ciò premesso, è devoluta alla giurisdizione di merito la    &#13;
 valutazione  se  nella vicenda in esame possa considerarsi conseguito    &#13;
 lo  scopo  dell'atto,  che è quello di consentire al destinatario il    &#13;
 pieno   esercizio   del  diritto  di  difesa:  ciò  postula  che  il    &#13;
 provvedimento  di  espulsione  sia materialmente portato a conoscenza    &#13;
 dell'interessato,   o   gli  sia  comunicato  con  modalità  che  ne    &#13;
 garantiscano in concreto la conoscibilità. Sarà il giudice a quo in    &#13;
 particolare,  a  valutare  se l'omessa traduzione impedisca, ai sensi    &#13;
 dell'art. 13,  comma  7,  il decorso del termine perentorio di cinque    &#13;
 giorni per l'impugnazione, o se l'espellendo, malgrado la mancanza di    &#13;
 traduzione,   abbia   comunque   avuto   tempestiva   conoscenza  del    &#13;
 provvedimento,  secondo quanto dimostrato, in ipotesi, dalla pubblica    &#13;
 amministrazione.                                                         &#13;
     Risulta  chiaro da quanto si è detto che l'art. 13, comma 8, non    &#13;
 deve  essere letto necessariamente nel senso proposto dal rimettente.    &#13;
 Infatti,  l'esigenza  primaria di non vanificare il diritto di azione    &#13;
 fa sì che nell'ipotesi di ignoranza senza colpa del provvedimento di    &#13;
 espulsione  -  in  particolare  per  l'inosservanza  dell'obbligo  di    &#13;
 traduzione  dell'atto  -  debba  ritenersi  non  decorso  il termine:    &#13;
 possibilità  interpretativa,  questa,  che non è esclusa dal tenore    &#13;
 letterale  della  disposizione in esame. Onde non vi è lesione degli    &#13;
 artt. 24 e 113 della Costituzione.</testo>
    <dispositivo>per questi motivi                              
                                                                          
                        LA CORTE COSTITUZIONALE                           
     Dichiara  non fondata la questione di legittimità costituzionale    
 dell'art. 13, comma 8, del decreto legislativo 25 luglio 1998, n. 286    
 (Testo   unico   delle   disposizioni   concernenti   la   disciplina    
 dell'immigrazione   e   norme   sulla  condizione  dello  straniero),    
 sollevata, con riferimento agli articoli 24 e 113 della Costituzione,    
 dal Pretore di Modena con l'ordinanza in epigrafe.                       
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    
 Palazzo della Consulta, l'8 giugno 2000.                                 
                       Il Presidente e Redattore: Guizzi                           
     Depositata in cancelleria il 16 giugno 2000.                         
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
