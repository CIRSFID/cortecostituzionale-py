<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>194</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:194</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/04/1993</data_decisione>
    <data_deposito>27/04/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 3, terzo comma,    &#13;
 della legge della Regione Veneto 23 aprile 1990, n. 28  (Nuove  norme    &#13;
 per la tutela dell'ambiente. Modifiche alla legge regionale 16 aprile    &#13;
 1985,  n.  33  "Norme  per  la  tutela  dell'ambiente"), promosso con    &#13;
 ordinanza  emessa  il  6  marzo  1992  dal  Pretore  di  Vicenza  nel    &#13;
 procedimento penale a carico di Gasparini Luciano, iscritta al n. 505    &#13;
 del  registro  ordinanze  1992  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 40, prima serie speciale, dell'anno 1992;            &#13;
    Visto l'atto di intervento della Regione Veneto;                      &#13;
    Udito  nell'udienza  pubblica  del  9  febbraio  1993  il  Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Udito l'avv. Luigi Manzi per la Regione Veneto;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Pretore di Vicenza, nel procedimento penale a carico di    &#13;
 Gasperini Luciano, imputato del reato di cui all'art. 21 della  legge    &#13;
 n.  319  del  1976  per  scarico  di  sostanze  nocive oltre i limiti    &#13;
 tabellari stabiliti dalla detta legge, ha  rilevato  che  l'imputato,    &#13;
 per   sua   ammissione,   aveva,  invece,  effettuato  lo  stoccaggio    &#13;
 provvisorio in azienda di reflui tossico-nocivi, dopo aver presentato    &#13;
 domanda di autorizzazione ai sensi degli artt.  2  e  3  della  legge    &#13;
 regionale  del  Veneto 23 aprile 1990, n. 28, senza ricevere risposta    &#13;
 dall'autorità competente; che l'art. 3, terzo  comma,  della  citata    &#13;
 legge  regionale  prevede che la domanda di autorizzazione si intende    &#13;
 accolta  in  caso  di   silenzio   dell'amministrazione   provinciale    &#13;
 protratto  oltre  i  trenta giorni; e, con ordinanza del 6 marzo 1992    &#13;
 (R.O. n. 505  del  1990),  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale dell'art. 3, terzo comma, citato.                         &#13;
   Ad avviso del giudice remittente, la norma impugnata si porrebbe in    &#13;
 contrasto  con  gli  artt. 3, 25, secondo comma, e 116 (rectius: 117)    &#13;
 della Costituzione disciplinando  in  modo  più  favorevole  materia    &#13;
 sottratta alla competenza regionale, siccome penalmente sanzionata.      &#13;
    2.  -  Nel  giudizio  è  intervenuto  il  Presidente della Giunta    &#13;
 regionale  del  Veneto,  il  quale  ha  preliminarmente  eccepito  la    &#13;
 irrilevanza  della questione nel giudizio a quo, in quanto il giudice    &#13;
 remittente dovrebbe comunque applicare  la  norma  impugnata  siccome    &#13;
 più favorevole. Anche perché la norma censurata sarebbe applicabile    &#13;
 solo  a  seguito  della modifica del fatto contestato che era diverso    &#13;
 (violazione, sanzionata dall'art. 21 della legge n. 319 del 1976, dei    &#13;
 limiti tabellari fissati dalla legge stessa).                            &#13;
    Nel merito ha osservato  che,  siccome  il  provvedimento  che  si    &#13;
 estrinseca   nel   silenzio-assenso   sarebbe   equiparabile   ad  un    &#13;
 provvedimento formale di  autorizzazione,  il  legislatore  regionale    &#13;
 avrebbe   in   ogni   caso   garantito   la  necessità  di  un  atto    &#13;
 autorizzatorio; inoltre, la Regione ha legiferato in esercizio  della    &#13;
 potestà  espressamente  riservatale  dal d.P.R. n. 915 del 1982, che    &#13;
 all'art. 6, lett. f), demanda  alle  Regioni  l'emanazione  di  norme    &#13;
 integrative  e  di  attuazione  per  l'organizzazione  dei servizi di    &#13;
 smaltimento e le procedure di controllo e di autorizzazione.             &#13;
    Infine, ha eccepito la violazione di norme procedurali  in  quanto    &#13;
 l'ordinanza di rimessione è stata notificata in proprio alla Regione    &#13;
 Veneto  anziché  all'Avvocatura  dello  Stato  che  la patrocina per    &#13;
 legge.                                                                   &#13;
    3. - Nell'imminenza  della  udienza  il  Presidente  della  Giunta    &#13;
 regionale  ha  presentato  memoria  con  la  quale  ha  ulteriormente    &#13;
 illustrato gli argomenti esposti nell'atto di intervento.<diritto>Considerato in diritto</diritto>1. - La Corte deve verificare se l'art. 3, terzo comma, della legge    &#13;
 della Regione Veneto del 23 aprile 1990, nella parte in  cui  prevede    &#13;
 che  per lo stoccaggio provvisorio presso il produttore la domanda di    &#13;
 autorizzazione   si   intende   accolta   in   caso    di    silenzio    &#13;
 dell'amministrazione  provinciale  protrattosi oltre i trenta giorni,    &#13;
 violi gli artt. 3, 25, secondo comma,  e  116  (rectius:  117)  della    &#13;
 Costituzione  in  quanto  sarebbe  stata  disciplinata  dalla Regione    &#13;
 materia sottratta alla sua competenza siccome penalmente sanzionata.     &#13;
    2.  -  Deve  essere  preliminarmente  esaminato  il  motivo  della    &#13;
 violazione  delle  norme  processuali  che  regolano la notificazione    &#13;
 dell'atto introduttivo del giudizio costituzionale.                      &#13;
    Il Presidente della Giunta regionale ha rilevato  che  l'ordinanza    &#13;
 di  remissione  della questione di legittimità costituzionale di cui    &#13;
 trattasi è stata irregolarmente  notificata  alla  Regione  anziché    &#13;
 all'Avvocatura   Generale  dello  Stato,  che  per  legge  ne  ha  la    &#13;
 rappresentanza nel giudizio dinanzi alla Corte costituzionale.           &#13;
    L'eccezione non è fondata  in  quanto  la  irregolarità  risulta    &#13;
 sanata dall'avvenuta costituzione della Regione nei giudizi.             &#13;
    3.  -  Va,  quindi,  esaminata  l'eccezione  di  inammissibilità,    &#13;
 sollevata dalla difesa della Regione. Si è  dedotta  la  irrilevanza    &#13;
 della   questione   e,   quindi,   la   sua   inammissibilità  nella    &#13;
 considerazione  che  la  norma   impugnata,   anche   se   dichiarata    &#13;
 costituzionalmente  non  legittima,  deve essere egualmente applicata    &#13;
 nel giudizio a quo in quanto più favorevole all'imputato.               &#13;
    L'eccezione non è fondata.                                           &#13;
    Si ribadisce che (sentt. nn. 146 del 1983, 826 del 1988,  124  del    &#13;
 1990)  le  pronunce  di  legittimità  delle norme penali di favore o    &#13;
 comunque più favorevoli all'imputato influiscono o possono  influire    &#13;
 sul  conseguente  esercizio  della  funzione  giurisdizionale  e  che    &#13;
 l'eventuale accoglimento delle impugnative di siffatte norme viene ad    &#13;
 incidere  sulle  formule  di  proscioglimento  o,  quanto  meno,  sul    &#13;
 dispositivo delle sentenze penali.                                       &#13;
    Inoltre,  la  pronuncia  della  Corte  potrebbe  riflettersi sullo    &#13;
 schema argomentativo della sentenza penale assolutoria  modificandone    &#13;
 la  ratio  decidendi. In tal caso risulterebbe alterato il fondamento    &#13;
 normativo della decisione.                                               &#13;
    Nella  fattispecie,  inoltre,  devesi  rilevare  che   l'eventuale    &#13;
 decisione di illegittimità della legge de qua influisce sulla stessa    &#13;
 imputazione  cioè  nel  fatto da addebitarsi all'imputato e, quindi,    &#13;
 sul reato a lui ascrivibile, che, peraltro, è di natura permanente.     &#13;
    4. - Nel merito la questione è fondata.                              &#13;
    La norma impugnata  prevede,  per  lo  stoccaggio  provvisorio  di    &#13;
 rifiuti  tossici e nocivi, la possibilità dell'autorizzazione tacita    &#13;
 in  luogo  di  quella  espressa.  Introduce,  cioè,  l'istituto  del    &#13;
 silenzio-assenso  in  una  fattispecie  nella quale, attesa la natura    &#13;
 degli interessi protetti e le  finalità  da  raggiungere,  cioè  la    &#13;
 tutela della salute e dell'ambiente, che sono beni costituzionalmente    &#13;
 protetti  (artt.  9  e  32  della  Costituzione)  e  stante l'obbligo    &#13;
 dell'osservanza di direttive comunitarie (nn. 75/442; 76/403; 78/319,    &#13;
 n. 91/156 che modifica la n.  75/442),  sono  indispensabili  per  il    &#13;
 rilascio   dell'autorizzazione   accurate  indagini  ed  accertamenti    &#13;
 tecnici, nonché controlli  specifici  per  la  determinazione  delle    &#13;
 misure   e   degli  accorgimenti  da  osservarsi  per  evitare  danni    &#13;
 facilmente possibili per la  natura  tossica  e  nociva  dei  rifiuti    &#13;
 accumulati.                                                              &#13;
    Non rileva l'avvenuto trasferimento dallo Stato alle Regioni delle    &#13;
 funzioni  relative  allo  smaltimento dei rifiuti con i d.P.R. n. 616    &#13;
 del 1977 (art. 101) e n. 915 del 1982 (art. 6) in  quanto,  dovendosi    &#13;
 osservare  le  direttive  comunitarie  emanate  in  materia, sussiste    &#13;
 l'obbligo dello Stato alla loro attuazione e osservanza (sent. n. 306    &#13;
 del 1992).                                                               &#13;
    Resta assorbita la violazione dell'art. 25 della Costituzione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la illegittimità costituzionale dell'art. 3, terzo comma,    &#13;
 della legge della Regione Veneto 23 aprile 1990, n. 28  (Nuove  norme    &#13;
 per la tutela dell'ambiente. Modifiche alla legge regionale 16 aprile    &#13;
 1985, n. 33 "Norme per la tutela dell'ambiente").                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 aprile 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: GRECO                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 27 aprile 1993.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
