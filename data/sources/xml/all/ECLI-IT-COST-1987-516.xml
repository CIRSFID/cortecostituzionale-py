<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>516</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:516</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/11/1987</data_decisione>
    <data_deposito>10/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 2 e 3 della    &#13;
 legge 13 maggio 1978, n. 180 ("Accertamenti e trattamenti                &#13;
 sanitari  volontari e obbligatori"), promosso con ordinanza emessa il    &#13;
 19 febbraio 1987 dal Pretore di Bracciano nel                            &#13;
 giudizio  di  convalida  relativa  al  ricovero  di Mariani Giuseppe,    &#13;
 iscritta al n. 260 del registro ordinanze  1987  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  28, prima serie speciale,    &#13;
 dell'anno 1987;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  il Pretore di Bracciano, con ordinanza emessa il 19    &#13;
 febbraio 1987, ha sospeso il giudizio di convalida  relativo  ad  una    &#13;
 ordinanza  del  sindaco  del  comune di Anguillara S. per il ricovero    &#13;
 urgente di  persona  affetta  da  "agitazione  mentale  con  note  di    &#13;
 disorientamento  nel  tempo  e  nello  spazio  e  stato di confusione    &#13;
 mentale" ed ha inoltre disposto la trasmissione degli atti alla Corte    &#13;
 costituzionale "per il giudizio di legittimità";                        &#13;
     che   -  per  quanto  possa  comprendersi  dalla  congerie  degli    &#13;
 argomenti, nei quali si dilunga l'ordinanza di rimessione,  argomenti    &#13;
 peraltro  in  massima  parte  non  pertinenti rispetto alla questione    &#13;
 rimessa al giudizio della  Corte,  e  pur  mancando  nel  dispositivo    &#13;
 l'indicazione  delle  norme denunciate e dei parametri costituzionali    &#13;
 invocati - è dato tuttavia desumere dalla  motivazione  che  si  sia    &#13;
 intesa sollevare questione di legittimità costituzionale degli artt.    &#13;
 2 e 3 della legge 13 maggio 1978, n. 180, "per la parte  in  cui  non    &#13;
 impongono  al  sindaco  di  prescrivere  il  minimo  periodo  di cura    &#13;
 consequenziale  all'ordine  di  ricovero   emanato   sulla   diagnosi    &#13;
 formulata   nella  proposta,  omologata  dallo  stesso  sindaco,  per    &#13;
 evidente violazione e contrasto con l'art. 32 della  Costituzione,...    &#13;
 che  garantisce  (oltre  al  ricovero)  la  cura a tutela del diritto    &#13;
 soggettivo  dell'individuo  alla  salute   e   dell'interesse   della    &#13;
 collettività  a  che  i  suoi componenti vengano curati", nonché in    &#13;
 relazione all'art. 3 Cost., "dal momento che,  ricoverando  l'infermo    &#13;
 di  mente  o  psichico  grave, senza la obbligatoria prescrizione del    &#13;
 minimo periodo di cura, si è creata una  disparità  di  trattamento    &#13;
 del  predetto  di  fronte  alla  legge,  nei  confronti  degli  altri    &#13;
 cittadini affetti da  malattie  diverse,  per  i  quali,  oltre  alla    &#13;
 diagnosi, vi è sempre il minimo periodo di prognosi terapeutica";       &#13;
    Considerato  che,  quanto all'asserito contrasto degli artt. 2 e 3    &#13;
 della legge n. 180 del 1978 con l'art. 32  Cost.,  appare  pienamente    &#13;
 soddisfatta  l'esigenza  di  assicurare  all'infermo  un  trattamento    &#13;
 sanitario di durata adeguata alle specifiche necessità  terapeutiche    &#13;
 perché  il  quarto  comma  dell'art.  3  della  citata  legge n. 180    &#13;
 prescrive che "nei casi in cui il trattamento sanitario  obbligatorio    &#13;
 debba  protrarsi  oltre  il settimo giorno, ed in quelli di ulteriore    &#13;
 prolungamento, il sanitario responsabile del servizio psichiatrico...    &#13;
 è  tenuto  a  formulare,  in  tempo  utile, una proposta motivata al    &#13;
 sindaco che ha disposto il ricovero, il quale ne dà comunicazione al    &#13;
 giudice  tutelare,  con  le modalità e per gli adempimenti di cui al    &#13;
 primo  e  al  secondo  comma  del  presente  articolo,  indicando  la    &#13;
 ulteriore durata presumibile del trattamento stesso";                    &#13;
      che,  per quanto riguarda, poi, il riferimento all'art. 3 Cost.,    &#13;
 contenuto nell'ordinanza di  rimessione,  il  menzionato  termine  di    &#13;
 comparazione  assunto per denunciare la disparità di trattamento non    &#13;
 ha alcuna attinenza  con  gli  aspetti  che  riguardano  il  ricovero    &#13;
 obbligatorio  degli  infermi  di  mente,  in  quanto  la prognosi che    &#13;
 accompagna il certificato di ricovero dei soggetti affetti  da  altre    &#13;
 infermità  consiste in una previsione della durata della malattia, e    &#13;
 non ha quindi la funzione di stabilire un periodo minimo di  ricovero    &#13;
 obbligatorio;                                                            &#13;
      che,  pertanto,  la  questione  di  legittimità  costituzionale    &#13;
 sollevata dal  Pretore  di  Bracciano  con  l'ordinanza  indicata  in    &#13;
 epigrafe  è  sotto  entrambi  i  profili  prospettati manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt.  26  della  legge 11 marzo 1953, n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale degli artt. 2 e 3 della legge 13 maggio 1978,  n.  180    &#13;
 ("Accertamenti  e  trattamenti  sanitari  volontari  e obbligatori"),    &#13;
 sollevata in riferimento agli artt. 3  e  32  Cost.  dal  Pretore  di    &#13;
 Bracciano  con ordinanza emessa il 19 febbraio 1987 (reg. ord. n. 260    &#13;
 del 1987).                                                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 novembre 1987.                             &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: CAIANIELLO                           &#13;
    Depositata in cancelleria il 10 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
