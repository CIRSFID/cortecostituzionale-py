<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>41</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:41</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1970</data_decisione>
    <data_deposito>20/03/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale del D.P.R. 2 gennaio  &#13;
 1962, n. 481, nella parte in  cui  rende  obbligatorie  erga  omnes  le  &#13;
 clausole  91,  96  e 97 del contratto collettivo nazionale di lavoro 28  &#13;
 giugno 1958 per i dipendenti delle imprese  commerciali,  promosso  con  &#13;
 ordinanza   emessa  il  19  giugno  1968  dal  pretore  di  Arezzo  nel  &#13;
 procedimento penale a carico di Prati Aldo,  iscritta  al  n.  208  del  &#13;
 registro  ordinanze  1968  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 261 del 12 ottobre 1968.                                   &#13;
     Visto  l'atto  d'intervento  del  Presidente  del   Consiglio   dei  &#13;
 Ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  25  febbraio  1970  il  Giudice  &#13;
 relatore Paolo Rossi;                                                    &#13;
     udito il sostituto avvocato generale dello Stato  Gastone  Dallari,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     In  sede  di  opposizione  a  decreto penale di condanna, tale Aldo  &#13;
 Prati, imputato della contravvenzione prevista  e  punita  dall'art.  8  &#13;
 della  legge  14  luglio  1959, n. 741, per non aver corrisposto, nella  &#13;
 qualità di amministratore delegato della I.M.A.  S.r.l.,  l'indennità  &#13;
 di  anzianità  all'ex impiegato dimissionario Pietro Fabrizi, eccepiva  &#13;
 l'illegittimità costituzionale, per eccesso di delega,  del  d.P.R.  2  &#13;
 gennaio  1962,  n. 481, nella parte in cui aveva reso obbligatorie erga  &#13;
 omnes le clausole 91, 96, 97  del  contratto  collettivo  nazionale  di  &#13;
 lavoro 28 giugno 1958 per i dipendenti da aziende commerciali.           &#13;
     Il  Pretore  procedente  ritenendo  rilevante  e non manifestamente  &#13;
 infondata l'eccezione sollevata, sospendeva  il  giudizio  in  corso  e  &#13;
 rimetteva gli atti a questa Corte, prospettando il dubbio che gli artt.  &#13;
 91,  96,  97  del  citato  contratto  collettivo  -  che  stabiliscono,  &#13;
 rispettivamente, la misura dell'indennità di liquidazione in relazione  &#13;
 alle mansioni esplicate ed all'anzianità di servizio,  il  momento  in  &#13;
 cui deve essere versata, e l'obbligo di corresponsione anche in caso di  &#13;
 dimissioni  volontarie  -  esorbitino  dai limiti stabiliti dalla norma  &#13;
 delegante,  che  fa  riferimento  al  solo  fine  "di assicurare minimi  &#13;
 inderogabili di trattamento economico e normativo"  (art.  1  legge  14  &#13;
 luglio 1959, n. 741).                                                    &#13;
     È intervenuto in giudizio, con atto depositato il 31 ottobre 1968,  &#13;
 il Presidente del Consiglio dei Ministri.                                &#13;
     L'Avvocatura generale dello Stato ha rilevato che il riconoscimento  &#13;
 di   un  diritto  all'indennità  di  anzianità  in  caso  di  recesso  &#13;
 costituisce un istituto  inderogabile  nel  trattamento  economico  dei  &#13;
 lavoratori,  e  come  tale è legittimamente disciplinato dai contratti  &#13;
 collettivi, ed ha concluso negando che le norme impugnate eccedano  dai  &#13;
 ricordati  limiti  stabiliti dalla legge delegante (art. 1 citata legge  &#13;
 741 del 1959).                                                           &#13;
     Con successiva memoria l'Avvocatura ha insistito nelle  conclusioni  &#13;
 prese.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte costituzionale è chiamata a decidere se contrasti o meno  &#13;
 con l'art. 76 della Costituzione, in relazione alla norma delegante che  &#13;
 mira "ad assicurare minimi  inderogabili  di  trattamento  economico  e  &#13;
 normativo"  (art.  1 legge 14 luglio 1959, n. 741), il D.P.R. 2 gennaio  &#13;
 1962, n. 481, nella parte in cui ha reso  obbligatorie  erga  omnes  le  &#13;
 clausole  91,  96,  97  del contratto collettivo nazionale di lavoro 28  &#13;
 giugno 1958, che disciplinano rispettivamente la misura dell'indennità  &#13;
 di anzianità spettante ai dipendenti  delle  imprese  commerciali,  il  &#13;
 momento in cui dev'esser versata, e l'obbligo di corrisponderla in caso  &#13;
 di dimissioni del lavoratore.                                            &#13;
     La questione proposta è infondata.                                  &#13;
     La  legge  di  delegazione  14  luglio 1959, n. 741, autorizzava il  &#13;
 Governo ad estendere erga omnes, con norme delegate, tutte le  clausole  &#13;
 dei  contratti  collettivi  indicati  nell'art.  3  - con esclusione di  &#13;
 quelle contrarie a norme imperative di legge - al fine  di  "assicurare  &#13;
 minimi  inderogabili di trattamento economico e normativo nei confronti  &#13;
 di tutti gli appartenenti ad una medesima categoria" (art. 1).           &#13;
     Per giurisprudenza  costante  della  Corte  costituzionale  rientra  &#13;
 nella   delega   anzidetta   la  disciplina  "della  formazione,  dello  &#13;
 svolgimento ed estinzione del rapporto di  lavoro,  e  dei  correlativi  &#13;
 diritti  e doveri delle parti che in esso intervengono" (sentenza n. 45  &#13;
 del 1966; conformi sentenze n. 129 del 1963 e 26  del  1967).  È  noto  &#13;
 altresì  che  con altre decisioni questa Corte ha delineato la nozione  &#13;
 di trattamento normativo "nel senso più comprensivo di ogni specie  di  &#13;
 pattuizione,  anche  a carattere non economico patrimoniale, necessaria  &#13;
 ad assicurare ai lavoratori un'esistenza  degna  della  persona  umana"  &#13;
 (sentenza n. 12 del 1969).                                               &#13;
     Dimostrata così l'ampia portata della delega contenuta nella legge  &#13;
 di  delegazione  in esame, risulta evidente che la norma impugnata, con  &#13;
 l'estendere erga omnes le clausole 91, 96, 97 del citato  c.c.n.l.,  ha  &#13;
 provveduto  propriamente  ad  integrare  la  disciplina legislativa dei  &#13;
 diritti e dei doveri nascenti dalla cessazione del rapporto di  lavoro,  &#13;
 rimanendo puntualmente nell'ambito della norma delegante, senza violare  &#13;
 affatto  l'art.  76  della Costituzione: in realtà anche quei benefici  &#13;
 che si realizzano solo alla cessazione del rapporto di lavoro rientrano  &#13;
 nel  "trattamento  economico  e  normativo",  di  cui  alla  legge   di  &#13;
 delegazione,  proprio  perché  essi  maturano  gradualmente durante lo  &#13;
 svolgimento  del  rapporto  stesso;  non  a  caso   questa   Corte   ha  &#13;
 riconosciuto   la  natura  retributiva  dell'indennità  di  anzianità  &#13;
 (sentenza n. 75 del 1968).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 del D.P.R. 2  gennaio  1962,  n.  481,  nella  parte  in  cui  ha  reso  &#13;
 obbligatorie erga omnes le clausole 91, 96, 97 del contratto collettivo  &#13;
 nazionale  di  lavoro  28  giugno  1958  per i dipendenti delle imprese  &#13;
 commerciali, sollevata, in relazione agli artt. 1 e 8  della  legge  14  &#13;
 luglio  1959, n. 741, ed in riferimento all'art. 76 della Costituzione,  &#13;
 dal pretore di Arezzo con ordinanza del 19 giugno 1968.                  &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, l'11 marzo 1970.                                 &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
