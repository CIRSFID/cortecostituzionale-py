<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>108</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:108</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/04/1985</data_decisione>
    <data_deposito>17/04/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. &#13;
 GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. &#13;
 GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art.  17, primo  &#13;
 comma, del R.d.l. 3 marzo 1938  n.  680  (Ordinamento  della  Cassa  di  &#13;
 previdenza  per le pensioni agli impiegati degli enti locali), promosso  &#13;
 con ordinanza emessa l'8 marzo 1976 dalla Corte dei Conti  sul  ricorso  &#13;
 proposto  da  Vaccari  Iris,  iscritta al n. 459 del registro ordinanze  &#13;
 1977 e pubblicata nella Gazzetta  Ufficiale  della  Repubblica  n.  334  &#13;
 dell'anno 1977.                                                          &#13;
     Udito  nella  camera  di  consiglio del 20 febbraio 1985 il Giudice  &#13;
 relatore Giuseppe Borzellino.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza emessa l'8 marzo 1976 la Corte dei  conti,  Sez.  &#13;
 III   giurisdizionale,  nel  giudizio  proposto  da  Vaccari  Iris,  ha  &#13;
 sollevato questione  di  legittimità  costituzionale,  in  riferimento  &#13;
 all'art.  36  Cost., dell'art. 17, primo comma, del R.d.l. 3 marzo 1938  &#13;
 n. 680 (convertito nella legge 9 gennaio 1939, n. 41), che  esonera  le  &#13;
 istituzioni  pubbliche  di  assistenza e beneficenza da ogni contributo  &#13;
 per il personale in servizio che appartenga a quelle categorie  per  le  &#13;
 quali  leggi  o  regolamenti  prevedano altro trattamento di quiescenza  &#13;
 obbligatorio o facoltativo che non sia di guerra o privilegiato.         &#13;
     Con decreto 18 luglio 1970 n. 14451 della Dir.  Gen. degli Istituti  &#13;
 di previdenza era stata conferita  alla  signora  Vaccari  Iris,  quale  &#13;
 vedova dell'ex dipendente dell'ECA di Modena, Rossi Ettore, la pensione  &#13;
 indiretta,  con  esclusione  dal  computo  del  servizio  utile  per il  &#13;
 trattamento di quiescenza, del periodo  dal  9  settembre  1944  al  29  &#13;
 giugno  1951,  durante  il  quale il Rossi aveva prestato servizio alle  &#13;
 dipendenze  dell'ECA  contemporaneamente  a   quello   prestato   quale  &#13;
 ufficiale in S.P.E..                                                     &#13;
     Avverso  tale  provvedimento  la interessata proponeva ricorso alla  &#13;
 Corte dei conti deducendo che, ai fini del  trattamento  di  quiescenza  &#13;
 spettantele,   avrebbe  dovuto  essere  computabile  l'intero  servizio  &#13;
 prestato all'ECA dal defunto marito.                                     &#13;
     Nel  corso  del  giudizio,  in  accoglimento   dell'eccezione   del  &#13;
 Procuratore  Generale,  la  detta  Sezione  III  sollevava questione di  &#13;
 legittimità costituzionale  del  precitato  art.  17,  osservando  che  &#13;
 l'esonero   previsto   da   questa  disposizione  comporta  la  mancata  &#13;
 iscrizione del dipendente alla  Cassa  pensioni  enti  locali,  per  il  &#13;
 periodo   in   cui   lo  stesso  ha  prestato  un  simultaneo  servizio  &#13;
 pensionabile  e  la  non  utilizzazione,  ai  fini  pensionistici,  del  &#13;
 servizio reso dallo stesso dipendente agli istituti di beneficenza.      &#13;
     Il  giudice  a quo, dopo aver ricordato che secondo la stessa Corte  &#13;
 Costituzionale (sent. 3 del 1966  e  successive)  la  retribuzione  dei  &#13;
 lavoratori,  ivi compresa quella corrisposta sotto forma di trattamento  &#13;
 di  liquidazione  o  di  quiescenza,  forma  oggetto   di   particolare  &#13;
 protezione  in  riferimento  all'art.  36  Cost.,  ritiene che sussista  &#13;
 pertanto una incompatibilità fra il menzionato art. 17,  primo  comma,  &#13;
 del decreto n. 680 del 1938 e l'indicato parametro costituzionale.       &#13;
     Premesso  che il caso in esame si presenta in maniera assolutamente  &#13;
 identica a quello già risolto dalla Corte Costituzionale con  sentenza  &#13;
 n.  176  del  1975, l'ordinanza osserva altresì, in punto di rilevanza  &#13;
 della  questione   proposta,   che   all'eventuale   dichiarazione   di  &#13;
 incostituzionalità della norma impugnata - rendendosi obbligatorio, da  &#13;
 parte  dell'ECA  di  Modena, l'onere del versamento delle contribuzioni  &#13;
 per il periodo di servizio simultaneo reso dal dipendente dello  Stato,  &#13;
 all'ECA  stesso  (9  settembre  1944-29 giugno 1951) - conseguirebbe la  &#13;
 valutazione in pensione del servizio stesso, così come  preteso  dalla  &#13;
 ricorrente.                                                              &#13;
     Davanti  a questa Corte non si è costituita alcuna delle parti né  &#13;
 è intervenuto il Presidente del Consiglio dei ministri;  pertanto,  ai  &#13;
 sensi  dell'art.  26,  secondo  comma,  della l. 11 marzo 1953, n. 87 e  &#13;
 dell'art. 9, primo comma, delle norme integrative  16  marzo  1956,  la  &#13;
 causa è stata fissata per la decisione in camera di consiglio.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con  l'ordinanza  indicata  in  epigrafe viene sollevata, in  &#13;
 riferimento all'art. 36, primo comma, della Costituzione, la  questione  &#13;
 di legittimità costituzionale dell'art. 17, primo comma, del R.d.l.  3  &#13;
 marzo  1938  n.  680  (Ordinamento  della  Cassa  di  previdenza per le  &#13;
 pensioni agli impiegati degli enti locali).                              &#13;
     2. - La questione è fondata nei limiti in prosieguo precisati.      &#13;
     L'art. 17 del R.d.l. 3 marzo 1938 n.  680  esonera  le  istituzioni  &#13;
 pubbliche  di assistenza e beneficenza da ogni contributo pensionistico  &#13;
 per il personale in servizio che appartenga a quelle categorie  per  le  &#13;
 quali  le leggi prevedano altro trattamento di quiescenza non di guerra  &#13;
 né privilegiato.                                                        &#13;
     Questa Corte, con  sentenza  18  giugno  1975  n.    176,  ha  già  &#13;
 dichiarato  l'illegittimità dell'art. 11 l. n.  1035 del 1939 che, con  &#13;
 formulazione letterale analoga a  quella  della  norma  ora  impugnata,  &#13;
 disponeva  che  a "le Istituzioni pubbliche di assistenza e beneficenza  &#13;
 sono esonerate da  ogni  contributo  per  i  medici  in  servizio  già  &#13;
 provvisti   di  pensione,  che  non  sia  di  guerra  né  privilegiata  &#13;
 ordinaria, o che appartengano a quelle categorie per le quali  leggi  o  &#13;
 regolamenti  prevedano  un  trattamento  di  quiescenza  obbligatorio o  &#13;
 facoltativo".                                                            &#13;
     Le  motivazioni  poste a fondamento di quest'ultima declaratoria di  &#13;
 illegittimità costituzionale, per contrasto della norma impugnata  con  &#13;
 l'art. 36 della Costituzione, ben possono valere anche per l'ipotesi in  &#13;
 esame.  Il  trattamento  preferenziale  che il legislatore ha riservato  &#13;
 agli istituti di assistenza e beneficenza, esonerandoli  dal  pagamento  &#13;
 dei  contributi  assicurativi  per  i  dipendenti  che  si  trovino  in  &#13;
 particolari condizioni, non può riversare, infatti, i propri effetti a  &#13;
 danno dei dipendenti stessi.                                             &#13;
     Una volta riconosciuto che, ai  sensi  del  disposto  dell'art.  36  &#13;
 Cost.,  è  garantita al lavoratore una retribuzione proporzionata alla  &#13;
 quantità e qualità del suo lavoro e che, secondo la giurisprudenza di  &#13;
 questa Corte, la pensione va  considerata  una  forma  di  retribuzione  &#13;
 differita,  ogni  diversa statuizione limitativa degli oneri - quale è  &#13;
 appunto quella oggetto dell'odierno esame - si infrange  a  fronte  dei  &#13;
 principi costituzionali sopraccennati.                                   &#13;
     D'altronde,  anche  per la fattispecie in esame, attesa l'identità  &#13;
 di formulazione delle norme, soccorre quanto ulteriormente  considerato  &#13;
 nella  precedente  citata  sentenza  n. 176, nel senso che la soluzione  &#13;
 normativa (art. 17, comma secondo R.d.l. n. 680) di  far  corrispondere  &#13;
 dal prestatore d'opera - facoltativamente - anche i contributi che sono  &#13;
 a  carico  del  datore di lavoro contrasta con la struttura del sistema  &#13;
 previdenziale e rappresenta, altresì, una  imposizione  incidente  non  &#13;
 ragionevolmente,   all'atto  pratico,  sul  trattamento  economico  del  &#13;
 lavoratore.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 17, primo comma,  &#13;
 del R.d.l. 3 marzo 1938, n. 680 (convertito nella legge 9 gennaio  1939  &#13;
 n.  41)  nella  parte  in  cui  esonera  gli  enti ivi indicati da ogni  &#13;
 contributo per i  personali  in  servizio  che  appartengano  a  quelle  &#13;
 categorie  per le quali leggi o regolamenti prevedano un trattamento di  &#13;
 quiescenza.                                                              &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 16 aprile 1985.         &#13;
                                   F.to:   LEOPOLDO   ELIA  -  GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN - VIRGILIO  &#13;
                                   ANDRIOLI   -   GIUSEPPE   FERRARI   -  &#13;
                                   FRANCESCO  SAJA  -  GIOVANNI  CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE   BORZELLINO   -   FRANCESCO  &#13;
                                   GRECO.                                 &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
