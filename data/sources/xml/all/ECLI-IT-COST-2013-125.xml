<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2013</anno_pronuncia>
    <numero_pronuncia>125</numero_pronuncia>
    <ecli>ECLI:IT:COST:2013:125</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GALLO</presidente>
    <relatore_pronuncia>Giuseppe Tesauro</relatore_pronuncia>
    <redattore_pronuncia>Giuseppe Tesauro</redattore_pronuncia>
    <data_decisione>03/06/2013</data_decisione>
    <data_deposito>05/06/2013</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Franco GALLO; Giudici : Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 9, comma 2, del decreto-legge 31 maggio 2010, n. 78 (Misure urgenti in materia di stabilizzazione finanziaria e di competitività economica), convertito, con modificazioni, dalla legge 30 luglio 2010, n. 122, promosso dal Tribunale amministrativo regionale per il Veneto nel procedimento vertente tra Bruni Bruno ed altri e il Ministero della giustizia ed altri, con ordinanza del 28 luglio 2012, iscritta al n. 220 del registro ordinanze 2012 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 41, prima serie speciale, dell'anno 2012.&#13;
 	Udito nella camera di consiglio del 24 aprile 2013 il Giudice relatore Giuseppe Tesauro.&#13;
 	Ritenuto che il Tribunale amministrativo regionale per il Veneto, con ordinanza del 28 luglio 2012, ha sollevato, in riferimento agli articoli 3, 36, 53, 101 e 104 della Costituzione, questione di legittimità costituzionale dell'articolo 9, comma 2, del decreto-legge 31 maggio 2010, n. 78 (Misure urgenti in materia di stabilizzazione finanziaria e di competitività economica), convertito, con modificazioni, dalla legge 30 luglio 2010, n. 122;&#13;
 	che il rimettente è investito di ricorsi proposti da magistrati ordinari e censura l'art. 9, comma 2, il quale, piuttosto che caratterizzarsi come una riduzione stipendiale (melius, come una riduzione dei trattamenti economici), avrebbe natura tributaria, ricorrendone i due elementi fondamentali dell'imposizione di un sacrificio economico individuale realizzata attraverso un atto autoritativo di carattere ablatorio, nonché della destinazione del gettito scaturente da tale ablazione ad integrare la finanza pubblica;&#13;
 	che, a suo giudizio, la disposizione impugnata violerebbe gli artt. 23 e 53 Cost., in quanto le decurtazioni previste, imponendo un sacrificio economico individuale non "transeunte", in forza di un atto autoritativo di carattere ablatorio, avente la finalità di reperire risorse all'erario, ed avendo quindi natura tributaria, colpirebbe solo una specifica categoria di contribuenti, indipendentemente dalla capacità contributiva complessiva,  così alterando il principio di progressività delle imposte;&#13;
 	che risulterebbe violato anche l'art. 3 Cost., perché determinerebbe un'evidente disparità di trattamento fra contribuenti, nonché gli artt. 101, secondo comma, e 104, primo comma, Cost., in quanto violerebbe il principio per cui il trattamento economico dei magistrati non sarebbe nella libera disponibilità del potere legislativo o del potere esecutivo, trattandosi di un aspetto essenziale per l'attuazione del precetto costituzionale dell'indipendenza della magistratura;&#13;
 	che, infine, la disposizione impugnata si porrebbe in contrasto con l'art. 36 della Costituzione, in quanto violerebbe il principio costituzionale di proporzionalità e adeguatezza della retribuzione, mediante un provvedimento a carattere continuativo e sostanzialmente stabile.&#13;
 	Considerato che il rimettente dubita della legittimità costituzionale dell'articolo 9, comma 2, del decreto-legge 31 maggio 2010, n. 78 (Misure urgenti in materia di stabilizzazione finanziaria e di competitività economica), convertito, con modificazioni, dalla legge 30 luglio 2010, n. 122, in relazione agli interventi normativi che riguardano i magistrati ricorrenti nel giudizio a quo;&#13;
 	che questa Corte, con la sentenza n. 223 del 2012, successiva alla pubblicazione dell'ordinanza di rimessione, ha già dichiarato l'illegittimità costituzionale dell'art. 9, comma 2, del d.l. n. 78 del 2010, nella parte in cui dispone che a decorrere dal 1° gennaio 2011 e sino al 31 dicembre 2013 i trattamenti economici complessivi dei singoli dipendenti, anche di qualifica dirigenziale, previsti dai rispettivi ordinamenti, delle amministrazioni pubbliche, inserite nel conto economico consolidato della pubblica amministrazione, come individuate dall'Istituto nazionale di statistica (ISTAT), ai sensi del comma 3 dell'art. 1 della legge 31 dicembre 2009, n. 196 (Legge di contabilità e finanza pubblica), superiori a 90.000 euro lordi annui siano ridotti del 5% per la parte eccedente il predetto importo fino a 150.000 euro, nonché del 10% per la parte eccedente 150.000 euro; &#13;
 	che, dunque, la questione va dichiarata manifestamente inammissibile, essendo divenuta priva di oggetto. &#13;
 	Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, commi 1 e 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 	dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 9, comma 2, del decreto-legge 31 maggio 2010, n. 78 (Misure urgenti in materia di stabilizzazione finanziaria e di competitività economica), convertito, con modificazioni, dalla legge 30 luglio 2010, n. 122, sollevata, in riferimento agli articoli 3, 36, 53, 101, 104 della Costituzione, dal Tribunale amministrativo regionale per il Veneto, con l'ordinanza indicata in epigrafe.&#13;
 	Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 giugno 2013.&#13;
 F.to:&#13;
 Franco GALLO, Presidente&#13;
 Giuseppe TESAURO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 5 giugno 2013.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
