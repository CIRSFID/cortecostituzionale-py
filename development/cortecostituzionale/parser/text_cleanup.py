import re


def cleanup(text):
    return [p for p in re.split(r"(?<=[.:;])\s+\r\n\s+", text) if p.strip()]
    # return [" ".join(p.split()).strip() for p in re.split(r"\r\n\s{3,}", text)]
