<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1958</anno_pronuncia>
    <numero_pronuncia>71</numero_pronuncia>
    <ecli>ECLI:IT:COST:1958:71</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Gaspare Ambrosini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/11/1958</data_decisione>
    <data_deposito>01/12/1958</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Avv. &#13;
 GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - &#13;
 Prof. ERNESTO BATTAGLINI - Dott. MARIO COSATTI - Prof. FRANCESCO &#13;
 PANTALEO GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO &#13;
 PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. &#13;
 BIAGIO PETROCELLI - Prof. ALDO SANDULLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel   giudizio  di  legittimità  costituzionale  dei  decreti  del  &#13;
 Presidente della Repubblica 18 dicembre 1952, n. 3524,  e  28  dicembre  &#13;
 1952,  n.  4363,  promosso  con  ordinanza 26 ottobre 1957 emessa dalla  &#13;
 Corte di appello  di  Firenze  nel  procedimento  civile  vertente  tra  &#13;
 Sacchini   Settimio  e  l'Ente  per  la  colonizzazione  della  maremma  &#13;
 tosco-laziale, pubblicata nella Gazzetta Ufficiale della Repubblica  n.  &#13;
 21 del 25 gennaio 1958 ed iscritta al n. 4 del Registro ordinanze 1958.  &#13;
     Udita  nell'udienza  pubblica  del 22 ottobre 1958 la relazione del  &#13;
 Giudice Gaspare Ambrosini;                                               &#13;
     udito l'avv.  Paolo Barile per Sacchini Settimio.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con atto 10 novembre 1951 per notar  Maccanti  il  signor  Sacchini  &#13;
 Settimio  acquistava  dal  signor  Massetani  Serafino  quattro  poderi  &#13;
 denominati Scopeto, Lughera, Lavalle e Caprareccia siti a  Ponsano,  in  &#13;
 comune di Volterra, della superficie complessiva di ettari 135.97.20.    &#13;
     Con  decreti  presidenziali  18  dicembre  1952,  n. 3524 (Gazzetta  &#13;
 Ufficiale, 19 gennaio 1953, suppl. ord.) e 28 dicembre  1952,  n.  4363  &#13;
 Gazzetta Ufficiale 24 gennaio 1953, suppl. ord.) parte di tali terreni,  &#13;
 e precisamente ettari 23.03.52, ivi comprese le quattro case coloniche,  &#13;
 veniva espropriata a carico del Massetani, che ne figurava proprietario  &#13;
 al  15  novembre  1949,  e  trasferita  in  proprietà dell'Ente per la  &#13;
 colonizzazione della maremma tosco-laziale.                              &#13;
     Nel corso del  giudizio  per  retrocessione  dei  beni  espropriati  &#13;
 promosso  dal  Sacchini  contro  l'Ente Maremma innanzi al Tribunale di  &#13;
 Pisa con atto di citazione 21 luglio 1955, l'attore sollevò  questione  &#13;
 di  legittimità  costituzionale avverso i citati decreti presidenziali  &#13;
 per violazione degli artt. 76 e 77 della Costituzione  per  i  seguenti  &#13;
 motivi:                                                                  &#13;
     1)   l'intera  proprietà  del  Massetani,  nella  sua  consistenza  &#13;
 catastale al 15 novembre 1949, aveva un reddito dominicale  considerato  &#13;
 al  1  gennaio 1943 inferiore a L. 30.000, e non era quindi soggetta ad  &#13;
 esproprio ai sensi dell'art. 4, comma primo,  della  legge  21  ottobre  &#13;
 1950, n. 841, e dell'annessa tabella di scorporo.                        &#13;
     2)  per  soli  23 ettari di terreno erano state espropriate tutte e  &#13;
 quattro  le  case  coloniche  che  servivano  136  ettari  di   terreni  &#13;
 lasciandone   privi   ben   113,  con  ciò  esulandosi  dal  fine  del  &#13;
 miglioramento fondiario costituente il limite segnato dall'art. 2 della  &#13;
 citata legge n. 841.                                                     &#13;
     L'Ente convenuto contestava la fondatezza della domanda attrice.     &#13;
     Con sentenza 24 maggio-16 luglio 1956  il  Tribunale,  ritenuta  la  &#13;
 manifesta  infondatezza della questione di costituzionalità, rigettava  &#13;
 la domanda del Sacchini.  Contro la predetta sentenza  veniva  proposta  &#13;
 impugnazione  dall'attore  soccombente e la Corte di appello di Firenze  &#13;
 con ordinanza del 26 ottobre 1957 rimetteva alla cognizione della Corte  &#13;
 costituzionale la questione di costituzionalità nei  termini  indicati  &#13;
 dal Sacchini.                                                            &#13;
     Detta  ordinanza, ritualmente notificata alle parti e al Presidente  &#13;
 del Consiglio dei Ministri, comunicata ai Presidenti delle  Camere,  è  &#13;
 stata pubblicata nella Gazzetta Ufficiale del 25 gennaio 1958.           &#13;
     L'ordinanza  della  Corte  di  appello  di  Firenze pone a fuoco la  &#13;
 questione di legittimità  costituzionale  nei  seguenti  termini:  "La  &#13;
 questione si riduce nel vedere se, per la norma dell'art. 4 della legge  &#13;
 21  ottobre 1950, n. 841, il reddito dell'intera proprietà deve essere  &#13;
 determinato dall'applicazione delle tariffe di estimo in  vigore  al  1  &#13;
 gennaio  1943,  con  riferimento  ai  dati  catastali dei terreni al 15  &#13;
 novembre 1949, così come sostiene il Sacchini, oppure con  riferimento  &#13;
 alla  titolarità  dei  beni  al  15  novembre 1949 e ai dati catastali  &#13;
 all'epoca dell'esproprio, così come assume l'Ente Maremma".             &#13;
     Inoltre l'ordinanza stessa ha rimesso alla cognizione  della  Corte  &#13;
 costituzionale  l'altra  eccezione di incostituzionalità sollevata dal  &#13;
 Sacchini per l'espropriazione delle quattro case coloniche.              &#13;
     Costituitosi nel presente giudizio, il Sacchini  nella  sua  difesa  &#13;
 prende  le  mosse  dalla  sentenza n. 81 del 16 maggio 1957 della Corte  &#13;
 costituzionale.                                                          &#13;
     Nell'atto del 17 gennaio 1958 il Sacchini  deduce  l'illegittimità  &#13;
 costituzionale dei due decreti presidenziali per eccesso di delega.      &#13;
     In particolare:                                                      &#13;
     a)  violazione  dell'art.  4  della  legge 1950, n. 841, perché il  &#13;
 reddito dominicale della proprietà secondo il catasto al  15  novembre  &#13;
 1949  e  con  applicazione delle tariffe di estimo vigenti al 1 gennaio  &#13;
 1943 era di L.  4.720,21, cioè inferiore a L. 30.000,  così  che  non  &#13;
 doveva farsi luogo ad espropriazione;                                    &#13;
     b)  violazione dell'art. 2 della legge 1950, n. 841, quanto ai fini  &#13;
 di trasformazione in rapporto alle finalità della riforma fondiaria ed  &#13;
 agraria, perché per i 23 ettari di terreni  trasferiti  in  proprietà  &#13;
 dell'Ente  Maremma  erano  state  espropriate  tutte  e quattro le case  &#13;
 coloniche, lasciandone privi gli altri 113 ettari.                       &#13;
     Nella sua memoria il Sacchini sostiene  che  dalle  sentenze  della  &#13;
 Corte costituzionale 67 e 126, rispettivamente del 14 maggio 1957 e del  &#13;
 28  novembre  1957,  appare  evidente  che  la  Corte costituzionale ha  &#13;
 interpretato l'art. 4 della legge n. 841 nel senso che il reddito della  &#13;
 proprietà, ai fini della determinazione della quota  di  scorporo,  si  &#13;
 ricava dall'applicazione delle tariffe di estimo in vigore al 1 gennaio  &#13;
 1943 con riferimento alla consistenza, cioè al classamento dei terreni  &#13;
 nella situazione stabilizzata al 15 novembre 1949.                       &#13;
     La  realtà  di  fatto, cui fa riferimento la sentenza n. 81 del 16  &#13;
 maggio  1957,  è  quella  del  15   novembre   1949.   "L'entrata   in  &#13;
 conservazione  del  nuovo  catasto  a Volterra nel 1951 è un fatto del  &#13;
 tutto irrilevante, essendo quella del 15 novembre 1949 la data  fissata  &#13;
 dalla   legge   per  il  congelamento  sia  della  estensione  che  del  &#13;
 classamento della proprietà terriera.  L'Ente non doveva adagiarsi sui  &#13;
 dati che il nuovo catasto gli forniva alla data del 1951, e doveva  far  &#13;
 ricorso, se credeva, all'art. 6 della legge n. 841".                     &#13;
     Sul  secondo  motivo  la memoria ribadisce che la espropriazione di  &#13;
 quattro case coloniche per 23 ettari, lasciandone privi gli  altri  113  &#13;
 ettari,  concreta  non  un  vizio di merito, ma un autentico eccesso di  &#13;
 potere che redundat in violazione della norma costituzionale.<diritto>Considerato in diritto</diritto>:                          &#13;
     La questione di legittimità costituzionale  dei  due  decreti  del  &#13;
 Presidente  della  Repubblica 18 dicembre 1952, n.  3524, e 28 dicembre  &#13;
 1952, n. 4363, che dispongono l'esproprio di terreni in  confronto  del  &#13;
 sig.    Sacchini  Settimio  ed  in  favore  dell'Ente Maremma, consiste  &#13;
 nell'interpretazione dell'art. 4, primo comma, della legge  21  ottobre  &#13;
 1950, n. 841.                                                            &#13;
     Questa  Corte  aveva  già, nelle sentenze nn. 65, 67, 81 e 126 del  &#13;
 1957, interpretato l'art. 4 nel senso che, agli effetti dello scorporo,  &#13;
 si debba fare riferimento  alla  data  del  15  novembre  1949  per  la  &#13;
 determinazione  della  consistenza  dei terreni soggetti all'esproprio,  &#13;
 sia rispetto alla loro estensione che alla qualità e classe di essi.    &#13;
     Con la sentenza in pari data  n.  70,  alla  quale  è  sufficiente  &#13;
 rinviare,  la  Corte  ha  ribadito questa interpretazione risolvendo la  &#13;
 questione per una fattispecie del tutto identica a quella che  ha  dato  &#13;
 luogo al presente giudizio.                                              &#13;
     Tanto  premesso,  va  rilevato  che è fuori discussione, in via di  &#13;
 fatto, che i due decreti presidenziali, per i quali è  stata  proposta  &#13;
 la  questione  di legittimità costituzionale, hanno fatto riferimento,  &#13;
 ai fini dell'applicazione della tabella di scorporo annessa alla  legge  &#13;
 stralcio,  ai dati catastali del tempo dell'esproprio e precisamente ai  &#13;
 dati risultanti dal nuovo catasto entrato in conservazione in loco alla  &#13;
 data del 1 settembre 1951, e non ai dati risultanti dal vecchio catasto  &#13;
 alla data del 15 novembre 1949, alla data cioè  indicata  dall'art.  4  &#13;
 per  la  determinazione  della  "consistenza" della proprietà terriera  &#13;
 privata.                                                                 &#13;
     È altresì fuori contestazione che  il  reddito  dominicale  della  &#13;
 proprietà   del  Massetani  Serafino  (dante  causa  del  Sacchini  ed  &#13;
 intestatario della proprietà al 15 novembre 1949) a  questa  data  era  &#13;
 complessivamente  di  L.  4.720,21,  e cioè inferiore alla cifra di L.  &#13;
 30.000 stabilita dalla legge come il minimo per lo scorporo.             &#13;
     Deve quindi concludersi, al lume dell'interpretazione data all'art.  &#13;
 4 della legge stralcio del 1950, n. 841, dalla Corte  nelle  suindicate  &#13;
 sentenze  e  qui  riaffermata,  che  i  due  decreti  presidenziali  in  &#13;
 questione sono viziati da illegittimità costituzionale per eccesso  di  &#13;
 delega.                                                                  &#13;
     Accolto  il  motivo  principale di contestazione della legittimità  &#13;
 costituzionale dei due decreti del  Presidente  della  Repubblica,  non  &#13;
 occorre esaminare il motivo subordinato.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   la   illegittimità   costituzionale   dei  decreti  del  &#13;
 Presidente della Repubblica del 18 dicembre 1952, n.  3524,  e  del  28  &#13;
 dicembre  1952,  n.  4363,  pubblicati  nella  Gazzetta Ufficiale della  &#13;
 Repubblica del 19 gennaio 1953, suppl. ord., e  del  24  gennaio  1953,  &#13;
 suppl.    ord., in relazione all'art. 4 della legge 21 ottobre 1950, n.  &#13;
 841, ed in riferimento agli artt. 76 e 77 della Costituzione.            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 25 novembre 1958.                             &#13;
                                   GAETANO  AZZARITI  - GIUSEPPE CAPPI -  &#13;
                                   TOMASO PERASSI - GASPARE AMBROSINI  -  &#13;
                                   ERNESTO  BATTAGLINI - MARIO COSATTI -  &#13;
                                   FRANCESCO   PANTALEO    GABRIELI    -  &#13;
                                   GIUSEPPE  CASTELLI  AVOLIO - ANTONINO  &#13;
                                   PAPALDO - NICOLA  JAEGER  -  GIOVANNI  &#13;
                                   CASSANDRO  - BIAGIO PETROCELLI - ALDO  &#13;
                                   SANDULLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
