<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>244</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:244</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/07/1983</data_decisione>
    <data_deposito>25/07/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  9,  commi  &#13;
 primo  e  secondo,  della  legge  27  dicembre 1956, n. 1423, nel testo  &#13;
 modificato dall'art. 8 della legge 14 ottobre 1974,  n.  497  (Sanzioni  &#13;
 per  l'inosservanza alle prescrizioni inerenti all'obbligo di soggiorno  &#13;
 ed al divieto di soggiorno) promosso con ordinanza emessa il 14 ottobre  &#13;
 1980 dal Tribunale di Caltanissetta nel procedimento penale a carico di  &#13;
 Notaro Francesco iscritta al n.  164  del  registro  ordinanze  1981  e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 179 del 1981;    &#13;
     visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito, nella camera di consiglio del 22  giugno  1983,  il  Giudice  &#13;
 relatore Alberto Malagugini.                                             &#13;
     Ritenuto  che  con l'ordinanza indicata in epigrafe il Tribunale di  &#13;
 Caltanissetta dubita della  legittimità  costituzionale  dell'art.  9,  &#13;
 primo e secondo comma, della legge 27 dicembre 1956, n. 1423, nel testo  &#13;
 modificato  dall'art.  8 della legge 14 ottobre 1974, n. 497, assumendo  &#13;
 che detta disposizione, in quanto prevede la  pena  dell'arresto  da  6  &#13;
 mesi  a  2  anni  per  la  contravvenzione  agli obblighi inerenti alla  &#13;
 sorveglianza speciale con l'obbligo o il divieto  di  soggiorno  e  non  &#13;
 sancisce l'abrogazione espressa dall'art. 12, primo comma, della stessa  &#13;
 l.  1423  cit. - a termini del quale chi contravvenga alle prescrizioni  &#13;
 dell'obbligo di soggiorno è punito con l'arresto da  tre  mesi  ad  un  &#13;
 anno  -  contrasti  con  l'art.  3  Cost., sotto un duplice profilo: a)  &#13;
 perché    prevede  per  la  medesima  inosservanza   (all'obbligo   di  &#13;
 soggiorno)  di  cui  all'art.  12,  primo comma, una pena più grave di  &#13;
 quella stabilita da tale ultima norma; b) perché  rispetto  a  questa,  &#13;
 prevede  una  pena più grave per l'inosservanza agli obblighi inerenti  &#13;
 al  divieto  di  soggiorno,  pur  essendo  questa  misura  meno   grave  &#13;
 dell'obbligo di soggiorno.                                               &#13;
     Considerato  che  il citato art. 9, secondo comma, - che è la sola  &#13;
 disposizione di cui il  giudice  a  quo  si  occupa  nella  motivazione  &#13;
 dell'ordinanza di rimessione - disciplina, nel nuovo testo, la medesima  &#13;
 materia  di  cui  all'art.  12,  primo  comma, della legge n. 1423/1956  &#13;
 (inosservanza delle prescrizioni inerenti  alla  sorveglianza  speciale  &#13;
 con  obbligo  di  soggiorno) e che pertanto quest'ultima disposizione -  &#13;
 conformemente, del resto, all'avviso espresso dalla Corte di Cassazione  &#13;
 (sez. I,  24  marzo  1980,  Gangitano)  -  deve  ritenersi  tacitamente  &#13;
 abrogata   per   incompatibilità   da  quella  sopravvenuta  (art.  15  &#13;
 preleggi);                                                               &#13;
     che conseguentemente la questione, in quanto basata su  un  erroneo  &#13;
 presupposto interpretativo, deve ritenersi manifestamente infondata.     &#13;
     Visti  gli artt. 26, secondo comma, della legge 11 marzo 1953, n.87  &#13;
 e 9, secondo comma, delle Norme integrative per i  giudizi  innanzi  la  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi  motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale dell'art. 9, secondo  comma,  della  legge  27  dicembre  &#13;
 1956,  n. 1423, nel testo modificato dall'art. 8 della legge 14 ottobre  &#13;
 1974, n. 497, sollevata in riferimento all'art. 3 Cost.  dal  Tribunale  &#13;
 di Caltanissetta con l'ordinanza indicata in epigrafe.                   &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 15 luglio 1983.         &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
