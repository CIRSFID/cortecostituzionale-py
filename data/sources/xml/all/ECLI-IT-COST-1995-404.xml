<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>404</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:404</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/07/1995</data_decisione>
    <data_deposito>26/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 36, lettera f),    &#13;
 del  codice  di procedura penale, promosso con ordinanza emessa il 10    &#13;
 dicembre 1994 dal Giudice  per  le  indagini  preliminari  presso  la    &#13;
 Pretura  di  Salerno  nel  procedimento  penale  a  carico di Viviano    &#13;
 Gaetano, iscritta al n. 44 del registro ordinanze 1995  e  pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 6, prima serie speciale,    &#13;
 dell'anno 1995;                                                          &#13;
    Udito  nella  camera  di  consiglio  del 31 maggio 1995 il Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto che il Giudice per  le  indagini  preliminari  presso  la    &#13;
 Pretura  di  Salerno,  chiamato  a  pronunciarsi  sulla  richiesta di    &#13;
 archiviazione del procedimento penale instaurato a carico di  Gaetano    &#13;
 Viviano,  indagato per il reato di omicidio colposo, ha sollevato, in    &#13;
 riferimento agli artt. 3, 24 e 25 della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale  dell'art.  36,  lettera  f), cod. proc.    &#13;
 pen., nella parte in cui non  prevede  l'obbligo  di  astensione  del    &#13;
 giudice   per   incompatibilità,  ove  questi,  legato  al  pubblico    &#13;
 ministero da vincolo di coniugio, si trovi investito della cognizione    &#13;
 del medesimo fatto che, oggetto di un diverso procedimento, sia stato    &#13;
 esaminato dallo stesso pubblico ministero;                               &#13;
      che  l'ordinanza  di  rimessione,  nel  dar  conto delle vicende    &#13;
 processuali che hanno preceduto l'incidente di  costituzionalità  in    &#13;
 oggetto,  precisa  che  la  richiesta di archiviazione sulla quale il    &#13;
 giudice  a  quo  è  chiamato  a   pronunciarsi   si   riporta   alle    &#13;
 argomentazioni  e  alle  conclusioni  assunte  da  un  altro pubblico    &#13;
 ministero, legato da vincolo  di  coniugio  allo  stesso  rimettente,    &#13;
 nell'ambito  di  un  procedimento  diverso,  perché iscritto in data    &#13;
 diversa nel registro delle notizie di reato, ma avente ad oggetto  il    &#13;
 medesimo  fatto, concernente la morte di Gaetano Vannelli conseguente    &#13;
 all'urto con l'autovettura condotta dall'indagato Gaetano Viviano,  e    &#13;
 concluso con un provvedimento di archiviazione;                          &#13;
      che,   tanto   premesso,   il  giudice  rimettente  ritiene  che    &#13;
 l'impugnato  art.  36,  lettera  f),   cod.   proc.   pen.,   poiché    &#13;
 obbligherebbe  il  giudice ad astenersi soltanto quando il coniuge di    &#13;
 questi eserciti  le  funzioni  di  pubblico  ministero  nello  stesso    &#13;
 procedimento,  non  potrebbe  essere  applicato al caso in esame, nel    &#13;
 quale le  funzioni,  rispettivamente,  di  pubblico  ministero  e  di    &#13;
 giudice,   pur   riguardando  il  medesimo  fatto,  non  si  svolgono    &#13;
 all'interno dello stesso procedimento;                                   &#13;
      che, pertanto, sulla base di tale  interpretazione,  il  Giudice    &#13;
 per  le  indagini  preliminari  presso  la  Pretura  circondariale di    &#13;
 Salerno ravvisa il contrasto dell'art. 36,  lettera  f),  cod.  proc.    &#13;
 pen., con i princip/' di parità sostanziale delle parti processuali,    &#13;
 di   effettività   del   diritto  di  difesa  e  di  indipendenza  e    &#13;
 imparzialità del giudizio, consacrati negli artt. 3, 24 e  25  della    &#13;
 Costituzione;                                                            &#13;
    Considerato  che  l'interpretazione  della  norma  impugnata sulla    &#13;
 quale è basata la questione di legittimità costituzionale in esame,    &#13;
 appare destituita di fondamento, in  quanto  delimita  l'operatività    &#13;
 dell'obbligo    di    astensione    ricondotto    alla   ipotesi   di    &#13;
 incompatibilità prefigurata dall'impugnato art. 36, lettera f), cod.    &#13;
 proc. pen., ricorrendo ad una nozione formalistica  di  procedimento,    &#13;
 estranea   sia   alla   lettera  della  norma,  sia  alla  sua  ratio    &#13;
 ispiratrice,  che   è   quella   di   garantire   la   serenità   e    &#13;
 l'imparzialità  del  giudizio  ogniqualvolta un rapporto qualificato    &#13;
 (in ipotesi, il  rapporto  di  coniugio)  tra  pubblico  ministero  e    &#13;
 giudice,  possa  condizionare la valutazione processuale del medesimo    &#13;
 fatto,  ancorché  questo  costituisca  l'oggetto   di   procedimenti    &#13;
 formalmente  diversi,  così  come  è  avvenuto  nel  caso in esame,    &#13;
 soltanto a causa, peraltro, di una anomala  modalità  di  iscrizione    &#13;
 della notitia criminis;                                                  &#13;
      che,  pertanto,  la  questione di legittimità costituzionale in    &#13;
 oggetto va dichiarata manifestamente infondata;                          &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, della norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 36, lettera  f),  del  codice  di  procedura    &#13;
 penale,  sollevata,  in  riferimento  agli  artt.  3,  24  e 25 della    &#13;
 Costituzione, dal Giudice  per  le  indagini  preliminari  presso  la    &#13;
 Pretura   circondariale  di  Salerno,  con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 luglio 1995.                               &#13;
                Il Presidente e redattore: BALDASSARRE                    &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
