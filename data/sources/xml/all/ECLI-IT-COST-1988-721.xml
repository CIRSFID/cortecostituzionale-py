<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>721</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:721</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/06/1988</data_decisione>
    <data_deposito>23/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  2-ter,    &#13;
 terzo, quarto e sesto comma, della  legge  31  maggio  1965,  n.  575    &#13;
 (Disposizioni  contro la mafia) come modificato e integrato dall'art.    &#13;
 14 della legge 13 settembre 1982, n. 646 (Disposizioni in materia  di    &#13;
 misure  di prevenzione di carattere patrimoniale ed integrazioni alle    &#13;
 leggi 27 dicembre 1956, n. 1423, 10 febbraio 1962, n. 57 e 31  maggio    &#13;
 1965,  n.  575.  Istituzione  di  una  commissione  parlamentare  sul    &#13;
 fenomeno della mafia) promosso con ordinanza emessa  il  29  dicembre    &#13;
 1983  dal  Tribunale  di  Catanzaro  nel  procedimento di prevenzione    &#13;
 instaurato nei confronti di Arena Antonio, iscritta  al  n.  248  del    &#13;
 registro  ordinanze  1984 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 231 dell'anno 1984;                                        &#13;
    Visto   l'atto  d'intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1988 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto che il Tribunale di Catanzaro, con ordinanza emessa il 29    &#13;
 dicembre 1983, ha proposto questione di legittimità  costituzionale,    &#13;
 in  riferimento  agli  artt.  41  e 42 Cost., dell'art. 2-ter, terzo,    &#13;
 quarto e sesto comma, della legge 31 maggio 1965, n. 575, così  come    &#13;
 modificato  ed  integrato dall'art. 14 della legge 13 settembre 1982,    &#13;
 n. 646, nella parte in cui non consente di disporre la  confisca  dei    &#13;
 beni  di  provenienza  illegittima  nelle ipotesi di non applicazione    &#13;
 della misura personale di prevenzione e di  cessazione  della  stessa    &#13;
 per morte del proposto;                                                  &#13;
      che  il  giudice  a  quo  ha  motivato  la  sollevata  questione    &#13;
 attraverso la ricostruzione della natura giuridica e  della  funzione    &#13;
 degli istituti del sequestro e della confisca, nei precedenti storici    &#13;
 della legge 31 maggio 1965, n. 575 e nel sistema della medesima e  la    &#13;
 rilevazione   dell'incoerenza   consistente   nell'impossibilità  di    &#13;
 disporre la confisca  prevista  dalla  predetta  legge  (appunto  per    &#13;
 impedire   che   beni  illecitamente  acquisiti  producano  ulteriori    &#13;
 illeciti o, comunque, rechino  danno  alla  generale  economia  della    &#13;
 collettività)  anche  nel  caso di mancata applicazione della misura    &#13;
 personale di prevenzione  (per  morte  del  proposto  nel  corso  del    &#13;
 procedimento) o nel caso in cui tale misura personale sia cessata per    &#13;
 decorso del termine o per morte del sottoposto;                          &#13;
      che  in  giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  concludendo  per  la  dichiarazione  d'inammissibilità della    &#13;
 proposta questione di legittimità;                                      &#13;
 }tps;7BA    Considerato che, con l'ordinanza di rimessione, si chiede    &#13;
 a questa Corte una pronunzia additiva che estenda la confisca, di cui    &#13;
 al  terzo  comma dell'art. 2- ter della legge 31 maggio 1965, n. 575,    &#13;
 ad ipotesi attualmente non previste;                                     &#13;
      che  tale  intervento di produzione normativa, in particolare in    &#13;
 materia sanzionatoria o, quanto meno, limitativa di diritti,  compete    &#13;
 esclusivamente  al  legislatore  e,  pertanto, esorbita dai poteri di    &#13;
 questa Corte;                                                            &#13;
      che,  pertanto,  la  questione di legittimità costituzionale va    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
      che,   infine,   la   dichiarazione  d'inammissibilità  per  la    &#13;
 motivazione innanzi espressa assorbe ogni  indagine  sulle  ulteriori    &#13;
 ragioni d'inammissibilità eccepite dall'Avvocatura dello Stato;         &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale - sollevata, in riferimento agli artt. 41    &#13;
 e  42  Cost.,  con  ordinanza  del  29 dicembre 1983 del Tribunale di    &#13;
 Catanzaro - dell'art. 2-ter, terzo, quarto e sesto comma, della legge    &#13;
 31  maggio 1965, n. 575, così come modificato ed integrato dall'art.    &#13;
 14 della legge 13 settembre 1982, n. 646.                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 9 giugno 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
