<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1966</anno_pronuncia>
    <numero_pronuncia>92</numero_pronuncia>
    <ecli>ECLI:IT:COST:1966:92</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/06/1966</data_decisione>
    <data_deposito>06/07/1966</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - &#13;
 Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - &#13;
 Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE &#13;
 CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 62, terzo  &#13;
 comma, e 64  della  legge  10  agosto  1950,  n.  648,  concernente  il  &#13;
 riordinamento delle disposizioni sulle pensioni di guerra, promosso con  &#13;
 ordinanza  emessa  il  23  marzo  1965  dalla Corte dei conti - Sezione  &#13;
 seconda giurisdizionale per le pensioni di  guerra  -  sul  ricorso  di  &#13;
 Recchione  Maria  Gabriella,  iscritta al n. 160 del Registro ordinanze  &#13;
 1965 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 216  del  &#13;
 28 agosto 1965.                                                          &#13;
     Udita  nella camera di consiglio del 1 giugno 1966 la relazione del  &#13;
 Giudice Costantino Mortati.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con atto in data 16  gennaio  1957  Recchione  Maria  Gabriella  ha  &#13;
 prodotto  ricorso alla Corte dei conti avverso decreto del Ministro del  &#13;
 tesoro che ebbe a negarle il diritto alla pensione di privilegio da lei  &#13;
 invocato come orfana di donna deceduta per fatto  di  guerra,  in  base  &#13;
 alla considerazione che essa richiedente risultava figlia adulterina di  &#13;
 Recchione Angela.                                                        &#13;
     La competente Sezione della Corte dei conti, nell'esame del ricorso  &#13;
 predetto,  rilevato  come  la  legislazione  pensionistica di guerra ha  &#13;
 espressamente inteso escludere ogni diritto a pensione nei confronti di  &#13;
 figli adulterini, ha  sollevato  d'ufficio  questione  di  legittimità  &#13;
 costituzionale degli artt. 62, terzo comma, e 64, della legge 10 agosto  &#13;
 1950,  n. 648, che tale esclusione sanciscono, nella considerazione che  &#13;
 sembra   non   manifestamente   infondato   il   ritenere   tali  norme  &#13;
 contrastanti, anzitutto con l'art. 38 della  Costituzione  perché  gli  &#13;
 adulterini, al pari di tutti gli altri cittadini, se sforniti dei mezzi  &#13;
 di   sussistenza,  hanno  diritto  all'assistenza  sociale,  e  che  la  &#13;
 normazione  in   materia   di   pensione   di   guerra   ha   carattere  &#13;
 prevalentemente  assistenziale;  in  secondo luogo con l'art. 30, terzo  &#13;
 comma, perché non assicura agli adulterini  (che  pure  sono  compresi  &#13;
 nell'ampia   categoria   ivi  considerata  dei  figli  nati  fuori  del  &#13;
 matrimonio) la richiesta tutela giuridica e sociale, almeno nel caso di  &#13;
 mancanza di membri  della  famiglia  legittima;  infine  con  l'art.  3  &#13;
 perché  la legislazione sulle pensioni di guerra assimila nello stesso  &#13;
 trattamento i figli naturali sicché appare contrario al  principio  di  &#13;
 eguaglianza   differenziare   da   questo   l'altro  disposto  per  gli  &#13;
 adulterini.                                                              &#13;
     Avendo la Corte dei conti ritenuta la questione predetta  rilevante  &#13;
 al  fine  della  risoluzione  del  giudizio  ad  essa deferito, ha, con  &#13;
 ordinanza del 23 marzo 1965, disposta la  sospensione  del  medesimo  e  &#13;
 l'invio degli atti alla Corte costituzionale.                            &#13;
     L'ordinanza,   debitamente   notificata   e  comunicata,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  numero  216  del  &#13;
 1965.                                                                    &#13;
     Poiché non vi è stata costituzione di parti la Corte ha deciso in  &#13;
 camera di consiglio ai sensi dell'art. 26 della legge n. 87 del 1953.<diritto>Considerato in diritto</diritto>.                          &#13;
     La questione proposta dall'ordinanza della Corte dei conti relativa  &#13;
 alla compatibilità con gli artt. 38, primo comma, 30, terzo comma, e 3  &#13;
 della  Costituzione delle norme sulle pensioni di guerra, che escludono  &#13;
 dal diritto a conseguire tale pensione i figli adulterini  di  genitori  &#13;
 morti a causa di eventi bellici, non è fondata.                         &#13;
     Deve    anzitutto    escludersi    che   ricorra   il   motivo   di  &#13;
 incostituzionalità che si vorrebbe dedurre dall'art. 38.   Infatti  il  &#13;
 diritto   al   mantenimento   ed  all'assistenza  da  questo  garantito  &#13;
 costituisce il contenuto di una pretesa attribuita ad ogni cittadino il  &#13;
 quale risulti inabile al lavoro  e  sfornito  dei  necessari  mezzi  di  &#13;
 sussistenza,   ed   è  da  soddisfare  con  appositi  istituti  e  con  &#13;
 determinate modalità, mentre il diritto alla pensione di  guerra,  pur  &#13;
 riannodandosi  al  primo perché, come esso, espressione di un'esigenza  &#13;
 di solidarietà sociale, trova il suo  specifico  titolo  negli  eventi  &#13;
 connessi allo stato di guerra e nelle particolari situazioni da questo,  &#13;
 direttamente  o indirettamente, determinate, ed è regolato con criteri  &#13;
 diversi da quelli che presiedono all'attività meramente assistenziale.  &#13;
     Neppure rilevabile è  da  ritenere  il  contrasto  che  si  assume  &#13;
 rispetto all'art. 30, terzo comma, della Costituzione.                   &#13;
     La   disparità   di   trattamento,  fatta  dalla  legge  ai  figli  &#13;
 adulterini, è stata determinata,  nell'intento  del  legislatore,  dal  &#13;
 timore  di  lesioni  dei diritti della famiglia legittima.  Infatti nel  &#13;
 concorso tra legittimi e adulterini il riconoscimento  del  diritto  di  &#13;
 questi  ultimi  avrebbe  rischiato  di ridurre la misura della pensione  &#13;
 percepibile dai primi; preoccupazione,  questa,  che,  comunque  la  si  &#13;
 valuti,  risponde allo stesso dettato dell'art. 30, comma terzo, ultima  &#13;
 parte.                                                                   &#13;
     Per analogo motivo è da ritenere infondata la censura  argomentata  &#13;
 dall'art.  3  della Costituzione nella parte in cui contiene il divieto  &#13;
 di ogni distinzione fatta derivare da "condizioni personali".  Come  la  &#13;
 stessa  ordinanza  ricorda,  la  costante giurisprudenza della Corte ha  &#13;
 interpretato  tale disposizione nel senso che essa non escluda (ed anzi  &#13;
 importi) trattamenti differenziati quando essi  risultino  giustificati  &#13;
 da situazioni oggettivamente diverse.                                    &#13;
     Ora  non  è  dubbio che la condizione dei figli adulterini e degli  &#13;
 incestuosi, secondo le disposizioni vigenti che  riflettono  la  comune  &#13;
 coscienza  sociale,  non  è  equiparabile  a  quella degli altri figli  &#13;
 naturali, e pertanto la esclusione del diritto a pensione disposta  nei  &#13;
 confronti  dei  medesimi,  riflettendo  il  sistema cui è informata la  &#13;
 legislazione, non può considerarsi arbitraria.  Il che è  sufficiente  &#13;
 a far ritenere non contrastanti con l'art. 3 le norme denunciate.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 proposta dalla Sezione speciale per le pensioni di guerra  della  Corte  &#13;
 dei  conti,  relativa  agli  artt. 62, terzo comma, e 64 della legge 10  &#13;
 agosto 1950, n. 648, in relazione agli artt. 38, primo comma, 30, terzo  &#13;
 comma, 3, primo comma, della Costituzione.                               &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 22 giugno 1966.         &#13;
                                   GASPARE  AMBROSINI  - NICOLA JAEGER -  &#13;
                                   GIOVANNI    CASSANDRO    -     BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
