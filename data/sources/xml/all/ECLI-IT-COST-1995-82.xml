<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>82</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:82</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/02/1995</data_decisione>
    <data_deposito>06/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 2043, 2051, e    &#13;
 1227,  primo  comma, del codice civile, promosso con ordinanza emessa    &#13;
 il 7 aprile  1994  dal  Pretore  di  Lecce  nel  procedimento  civile    &#13;
 vertente  tra  Gigante  Anna,  in  proprio e nella qualità di legale    &#13;
 rappresentante della figlia minore Melcarne Sara, e l'Amministrazione    &#13;
 provinciale di Lecce, iscritta al n. 353 del registro ordinanze  1994    &#13;
 e  pubblicata  nella Gazzetta ufficiale della Repubblica n. 26, prima    &#13;
 serie speciale, dell'anno 1994;                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 25 gennaio 1995 il Giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
    Ritenuto che con ordinanza emessa il 7 aprile 1994 il  Pretore  di    &#13;
 Lecce  ha  sollevato,  in  riferimento  agli  artt.  3, 24 e 97 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art. 2043    &#13;
 c.c., "ove interpretato nel senso che l'inerzia  colposa  della  p.a.    &#13;
 atta  a creare o non rimuovere situazioni di pericolo non è causa di    &#13;
 responsabilità della stessa, nel caso in cui non si sia in  presenza    &#13;
 di  una  situazione di pericolo insidiosa"; dell'art. 2051 c.c., "ove    &#13;
 interpretato nel senso che non sia applicabile anche alla P.A. per  i    &#13;
 beni demaniali soggetti ad uso ordinario, generale e diretto da parte    &#13;
 dei   cittadini";   e   dell'art.   1227,  primo  comma,  c.c.,  "ove    &#13;
 interpretato nel senso di escludere, in presenza  di  un'insidia,  un    &#13;
 accertamento   del   concorso   di   colpa   del  danneggiato  e  del    &#13;
 responsabile";                                                           &#13;
      che,   secondo   il    giudice    a    quo,    l'interpretazione    &#13;
 giurisprudenzialeche esclude la responsabilità della p.a. in caso di    &#13;
 insidia   visibile   e   prevedibile  potrebbe  fornire  un  supporto    &#13;
 all'inerzia di  quest'ultima  in  violazione  del  precetto  di  buon    &#13;
 andamento;                                                               &#13;
      che,  inoltre,  non  sarebbe  richiesta  alla  p.a.  neppure  la    &#13;
 dimostrazione che all'origine del pericolo vi siano state circostanze    &#13;
 non  tempestivamente  evitabili  e/o  segnalabili,  con   conseguenti    &#13;
 difficoltà   processuali   per  il  danneggiato,  tenuto  a  provare    &#13;
 l'imprevedibilità dell'insidia, mentre, per  converso,  quest'ultimo    &#13;
 non   sarebbe   mai   riconosciuto   come   concorrente  nella  colpa    &#13;
 eventualmente accertata a carico della p.a.;                             &#13;
      che, infine, ad avviso del Pretore remittente, i proprietari  di    &#13;
 strade private sarebbero gravati da una più marcata responsabilità,    &#13;
 in quanto chiamati a rispondere a titolo di custodia;                    &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura dello Stato, che  ha  concluso    &#13;
 per l'inammissibilità o comunque per l'infondatezza della questione.    &#13;
    Considerato  che  il  giudice  a  quo  dubita  della  legittimità    &#13;
 costituzionale,    non     delle     denunziate     norme,     bensì    &#13;
 dell'interpretazione che di esse verrebbe data in giurisprudenza;        &#13;
      che  tale  interpretazione egli non fa propria ma, al contrario,    &#13;
 la considera come effetto di deviazione da un  corretto  procedimento    &#13;
 ermeneutico,  comportante  la  "pratica  .. disapplicazione di alcune    &#13;
 norme fondamentali sulla disciplina dell'illecito extracontrattuale",    &#13;
 e non manca poi di osservare - ricordando quanto questa Corte ha più    &#13;
 volte avuto occasione di precisare -  che  "tra  due  interpretazioni    &#13;
 d'un  testo  di  legge,  l'una conforme e l'altra contrastante con la    &#13;
 Costituzione, deve sempre preferirsi la prima";                          &#13;
      che pertanto la prospettazione, essenzialmente ipotetica, appare    &#13;
 articolata come un quesito interpretativo e  legata  ad  una  lettura    &#13;
 incompleta della giurisprudenza;                                         &#13;
      che   il   giudizio  di  legittimità  costituzionale  non  può    &#13;
 conseguentemente essere ammesso;                                         &#13;
    Visti gli artt. 26 della legge 11 marzo 1953,  n.  87  e  9  delle    &#13;
 norme integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente inammissibile la questione di legittimità    &#13;
 costituzionale degli artt. 2043, 2051 e 1227, primo comma, del codice    &#13;
 civile, sollevata, in  riferimento  agli  artt.  3,  24  e  97  della    &#13;
 Costituzione,  dal  Pretore  di  Lecce  con  l'ordinanza  di  cui  in    &#13;
 epigrafe.                                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 23 febbraio 1995.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: RUPERTO                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 6 marzo 1995.                            &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
