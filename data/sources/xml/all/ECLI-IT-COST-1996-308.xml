<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>308</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:308</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/07/1996</data_decisione>
    <data_deposito>24/07/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, &#13;
 prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, &#13;
 prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 27 e 29 della    &#13;
 legge 23 luglio 1991, n. 223 (Norme in materia di cassa integrazione,    &#13;
 mobilità, trattamenti di  disoccupazione,  attuazione  di  direttive    &#13;
 della  comunità  europea, avviamento al lavoro ed altre disposizioni    &#13;
 in materia di mercato del lavoro), promosso con ordinanza emessa  l'8    &#13;
 gennaio  1996 dal pretore di Livorno nel procedimento civile vertente    &#13;
 tra Lorenzini Anna Franca ed altre e INPS  iscritta  al  n.  315  del    &#13;
 registro  ordinanze  1996 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 15, prima serie speciale, dell'anno 1996;                  &#13;
   Visto l'atto di costituzione dell'INPS;                                &#13;
   Udito nella camera di consiglio  del  10  luglio  1996  il  giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
   Ritenuto  che  nel  corso  di  un  procedimento  civile promosso da    &#13;
 Lorenzini Anna Franca ed altre nei confronti dell'INPS, il pretore di    &#13;
 Livorno, con ordinanza emessa l'8  gennaio  1996,  ha  sollevato,  in    &#13;
 riferimento  agli  artt.  3, 37 e 38 della Costituzione, questione di    &#13;
 legittimità costituzionale degli artt. 27 e 29 della legge 23 luglio    &#13;
 1991, n.  223 (Norme in materia  di  cassa  integrazione,  mobilità,    &#13;
 trattamenti   di   disoccupazione,   attuazione  di  direttive  della    &#13;
 Comunità europea, avviamento al  lavoro  ed  altre  disposizioni  in    &#13;
 materia  di mercato del lavoro), nella parte in cui non prevedono che    &#13;
 alle  lavoratrici  sia  attribuito   un   accredito   di   anzianità    &#13;
 contributiva  fino al raggiungimento del sessantesimo anno di età o,    &#13;
 quantomeno, pari ad anni dieci;                                          &#13;
     che a parere del giudice a quo le norme impugnate introdurrebbero    &#13;
 una irragionevole disparità di trattamento tra le lavoratrici  ed  i    &#13;
 lavoratori del settore siderurgico, nonché, all'interno della stessa    &#13;
 categoria   di   lavoratrici,   tra   quelle   che   beneficiano  del    &#13;
 prepensionamento  nel  momento  attuale  e  quelle   che   ne   hanno    &#13;
 beneficiato prima dell'introduzione della normativa censurata, per le    &#13;
 quali  la  Corte  costituzionale, con la sentenza n. 134 del 1991, ha    &#13;
 riconosciuto lo stesso trattamento spettante agli uomini;                &#13;
     che  nel  giudizio  avanti  alla  Corte  costituzionale   si   è    &#13;
 costituito  l'INPS,  rilevando  che identica questione risulta essere    &#13;
 stata già decisa con le sentenze nn. 354 del 1994 e 64  del  1996  e    &#13;
 rimettendosi, pertanto, alla decisione della Corte;                      &#13;
   Considerato  che  questa  Corte,  con  sentenza  n. 64 del 1996, ha    &#13;
 deciso   un'analoga   questione   di   legittimità   costituzionale,    &#13;
 interpretando  l'art.  8 del d.-l. 16 maggio 1994, n. 299, convertito    &#13;
 in legge, con modificazioni, dall'art. 1, primo comma, della legge 19    &#13;
 luglio 1994, n. 451, nel senso  che  esso,  prevedendo  un  piano  di    &#13;
 pensionamento  anticipato  dei  lavoratori  siderurgici  di  età non    &#13;
 inferiore a cinquant'anni se uomini  ed  a  quarantasette  se  donne,    &#13;
 attribuisce  ai dipendenti medesimi una maggiorazione dell'anzianità    &#13;
 contributiva fissata, per entrambi i sessi,  nella  misura  di  dieci    &#13;
 anni;                                                                    &#13;
     che   in   seguito   identica   questione   è  stata  dichiarata    &#13;
 manifestamente infondata con la ordinanza n. 192 del 1996;               &#13;
     che non essendo stati prospettati ulteriori  e  nuovi  motivi  di    &#13;
 censura   la   questione   relativa   alle  lavoratrici  del  settore    &#13;
 siderurgico deve essere dichiarata manifestamente infondata;             &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e  9, secondo comma, delle norme integrative per i giudizi avanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  degli  artt.  27  e 29 della legge 23 luglio 1991, n.    &#13;
 223 (Norme in materia di cassa integrazione,  mobilità,  trattamenti    &#13;
 di  disoccupazione,  attuazione di direttive della comunità europea,    &#13;
 avviamento al lavoro ed altre disposizioni in materia di mercato  del    &#13;
 lavoro),    sollevata,  in  riferimento  agli  artt. 3, 37 e 38 della    &#13;
 Costituzione, dal pretore di Livorno con  l'ordinanza  indicata    in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1996.                               &#13;
                          Il Presidente: Ferri                            &#13;
                       Il redattore: Santosuosso                          &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 24 luglio 1996.                           &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
