<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>148</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:148</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Leonetto Amadei</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/06/1973</data_decisione>
    <data_deposito>18/07/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Dott. &#13;
 LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. &#13;
 ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Prof. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  216 del  &#13;
 codice penale, promosso con ordinanza  emessa  il  2  agosto  1972  dal  &#13;
 giudice  di  sorveglianza del tribunale di Firenze nel procedimento per  &#13;
 misure di sicurezza a carico di Cosimi Ludovico, iscritta al n. 317 del  &#13;
 registro ordinanze 1972 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 279 del 25 ottobre 1972.                                   &#13;
     Udito  nella  camera  di  consiglio  del  14 giugno 1973 il Giudice  &#13;
 relatore Leonetto Amadei.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Cosimi Ludovico, detenuto in espiazione di pena nella casa  di  &#13;
 reclusione  di  Firenze  e  prossimo  alla scarcerazione, inoltrava, al  &#13;
 giudice di sorveglianza, istanza di revoca della misura di sicurezza di  &#13;
 assegnazione a casa di lavoro, inflittagli con la sentenza di condanna,  &#13;
 adducendo la sopravvenuta invalidità al lavoro.                         &#13;
     Il  giudice  di  sorveglianza,  dato  atto   che,   effettivamente,  &#13;
 l'inabilità al lavoro era sopravvenuta durante lo stato di detenzione,  &#13;
 e  affermata  la  propria  competenza  a valutare, ex articolo 635 cod.  &#13;
 proc. pen., le  possibili  conseguenze  dell'evento  invalidante  sulla  &#13;
 esecuzione della misura di sicurezza, sollevava, con ordinanza datata 2  &#13;
 agosto  1972,  questione  di  legittimità costituzionale dell'art. 216  &#13;
 cod.  pen., in riferimento agli artt. 3, primo e secondo  comma,  e  38  &#13;
 della  Costituzione,  nella  parte  in  cui  impone o rende facoltativa  &#13;
 l'applicazione della casa di lavoro senza tener conto delle  condizioni  &#13;
 fisiche del soggetto.                                                    &#13;
     2.  -  L'ordinanza  esamina  l'asserito contrasto dell'articolo 216  &#13;
 cod. pen. con i  richiamati  precetti  costituzionali  in  una  diffusa  &#13;
 motivazione,  che,  nei suoi aspetti essenziali, può riassumersi nella  &#13;
 seguente proposizione:   non può  ritenersi  legittima  l'applicazione  &#13;
 della  misura  di  sicurezza  della  casa  di  lavoro ad un soggetto in  &#13;
 condizioni di capacità lavorativa notevolmente ridotta, avuto riguardo  &#13;
 alla funzione che il lavoro è destinato ad  assumere  nella  struttura  &#13;
 della   misura   stessa   e  alla  particolare  tutela  prevista  dalla  &#13;
 Costituzione a favore degli inabili al lavoro.                           &#13;
     3. - Con diretto  riferimento  all'art.  38,  secondo  comma  della  &#13;
 Costituzione,  il  proponente  osserva che, in forza delle disposizioni  &#13;
 normative  e   regolamentari,   la   prestazione   lavorativa   imposta  &#13;
 all'internato  non  differisce,  nella sostanza, da quel tipo di lavoro  &#13;
 subordinato in senso tecnico, destinato ad inserirsi nel  vasto  quadro  &#13;
 delle  previdenze  poste,  a tutela dei lavoratori, dalla Costituzione.  &#13;
 Tale tutela, pertanto, verrebbe disattesa con la sottoposizione,  senza  &#13;
 limitazione alcuna, ad un'attività lavorativa obbligatoria di soggetti  &#13;
 fisicamente non idonei ad essa.                                          &#13;
     4.  -  L'asserito  contrasto della norma con l'art. 3, primo comma,  &#13;
 della Costituzione, viene presentato sotto due  distinti  profili,  uno  &#13;
 specifico e l'altro generico.                                            &#13;
     Sotto  l'aspetto  specifico  sarebbe  illegittimo accomunare ad una  &#13;
 stessa disciplina giuridica tanto  soggetti  invalidi  quanto  soggetti  &#13;
 validi  al  lavoro.  Mentre  per  i  secondi  si  avrebbe  una concreta  &#13;
 assegnazione  ad  un  lavoro  pienamente  retribuito  e  in  grado   di  &#13;
 corrispondere al loro reinserimento nella società per i primi, invece,  &#13;
 l'assoggettamento  ad uno sforzo fisico inaccettabile ed insostenibile,  &#13;
 determinerebbe  un  maggiore  stato  di  frizione  tra  il  soggetto  e  &#13;
 l'organizzazione  sociale,  con un inevitabile risultato in opposizione  &#13;
 ai fini.                                                                 &#13;
     Sotto l'aspetto generico la violazione del principio  d'uguaglianza  &#13;
 sarebbe  sostanzialmente  determinata  dagli stessi motivi posti a base  &#13;
 della violazione dell'art. 38 della Costituzione.  Infatti,  si  assume  &#13;
 che  gli  invalidi,  destinatari  della  norma di cui all'art. 216 cod.  &#13;
 pen., verrebbero a  trovarsi,  a  parità  di  condizioni  fisiche,  in  &#13;
 situazione  giuridica  diversa  con  gli altri invalidi che destinatari  &#13;
 della norma non lo sono; mentre, per  il  precetto  costituzionale,  la  &#13;
 invalidità  dovrebbe determinare effetti comuni a tutti, incensurati o  &#13;
 no, delinquenti abituali o no.                                           &#13;
     5. - Il conflitto tra la norma penale e il secondo comma  dell'art.  &#13;
 3  della  Costituzione  è  prospettato  nel  senso  che  la  misura di  &#13;
 sicurezza, così come  è  strumentalizzata  dal  codice  vigente,  non  &#13;
 sarebbe  in  grado  di  assolvere  alla  sua  funzione.  Lo  Stato, per  &#13;
 legittimarla, dovrebbe rimuovere gli ostacoli  di  ordine  economico  e  &#13;
 sociale   che,   limitando   di   fatto  l'uguaglianza  dei  cittadini,  &#13;
 impediscono il pieno sviluppo della personalità  umana  e  determinano  &#13;
 quei  fenomeni di conflittualità sociale che stanno, spesso, alla base  &#13;
 di una condotta antigiuridica.                                           &#13;
     Nessuna parte si è costituita in questa sede.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  giudice  di  sorveglianza  di  Firenze   denuncia,   con  &#13;
 riferimento  agli  artt. 3, primo e secondo comma, e 38, secondo comma,  &#13;
 della Costituzione, la illegittimità costituzionale dell'art. 216  del  &#13;
 codice  penale,  nella  parte  in  cui  impone,  o  rende  facoltativa,  &#13;
 l'applicazione della misura di sicurezza della  casa  di  lavoro  senza  &#13;
 alcuna  limitazione  attinente alle condizioni di invalidità al lavoro  &#13;
 del soggetto alla stessa misura sottoposto.                              &#13;
     2.  -  Il  contrasto  tra  la  norma  impugnata  e  l'art. 38 della  &#13;
 Costituzione è prospettato, sia pure con più ampia motivazione, negli  &#13;
 stessi termini delle due ordinanze che dettero luogo alla  sentenza  n.  &#13;
 167  del  1972  di  questa  Corte,  che  ha  dichiarato  non fondata la  &#13;
 sollevata  questione  di  legittimità  costituzionale.  Sotto   questo  &#13;
 aspetto,   pertanto,  la  proposta  questione  deve  essere  dichiarata  &#13;
 manifestamente infondata.                                                &#13;
     3. -  È  altresì  infondata  la  questione  nel  suo  riferimento  &#13;
 all'art. 3, primo e secondo comma, della Costituzione.                   &#13;
     L'art. 216 c.p. non viola il principio di eguaglianza, né sotto il  &#13;
 profilo   generico,   né   sotto   il   profilo   specifico   indicati  &#13;
 nell'ordinanza.                                                          &#13;
     Si  afferma,  a  riguardo,  che  la  violazione  del  principio  di  &#13;
 eguaglianza,  in  senso  generico,  si  avrebbe  con l'assoggettare gli  &#13;
 invalidi al lavoro, destinatari della norma  di  cui  all'articolo  216  &#13;
 c.p.,  ad  una  disciplina  giuridica di coercizione in antitesi con la  &#13;
 posizione di tutela costituzionale propria dei lavoratori invalidi e in  &#13;
 quanto tali. L'assunto generalizza il concetto di eguaglianza  oltre  i  &#13;
 limiti  costituzionali che ad esso sono propri. Vale osservare che, nel  &#13;
 raffronto prospettato, non è ravvisabile in alcun modo  una  identità  &#13;
 di  situazioni  giuridiche  per effetto della comune condizione fisica,  &#13;
 che, nel caso, è estrinseca all'essenza del principio di  eguaglianza.  &#13;
 V'è  una  situazione  soggettiva,  tipica,  assorbente  di ogni altra,  &#13;
 nell'invalido al lavoro destinatario  della  norma  impugnata:  la  sua  &#13;
 condizione  di  socialmente  pericoloso a causa e per effetto della sua  &#13;
 condotta antigiuridica. Lo stato di invalidità  potrà  esercitare  il  &#13;
 suo  peso ad altri fini, ma non certamente sulle conseguenze proprie di  &#13;
 ogni comportamento penalmente illecito.                                  &#13;
     Il  principio  di  eguaglianza  non  è   neppure   violato   sotto  &#13;
 l'ulteriore    particolare    profilo    secondo   il   quale   sarebbe  &#13;
 costituzionalmente illegittimo sottoporre ad uno stesso  trattamento  -  &#13;
 casa  di  lavoro  -  tanto  soggetti  validi, quanto soggetti invalidi,  &#13;
 nonostante l'identica condizione giuridica di socialmente pericolosi.    &#13;
     In più sentenze, questa Corte, ha preso  in  esame  il  fondamento  &#13;
 giuridico  delle  misure  di  sicurezza e, per ultimo, nella richiamata  &#13;
 sentenza n. 167 del 1972. Ha, la Corte, affermato che presupposto delle  &#13;
 misure di sicurezza è la pericolosità sociale del soggetto  al  quale  &#13;
 vengono  applicate.  Da  tale  presupposto non è possibile discostarsi  &#13;
 nello  stabilire  la  legittimità  costituzionale  delle   norme   che  &#13;
 regolano,  nella  loro  diversa articolazione, tali misure, contemplate  &#13;
 dall'art. 25 della Costituzione.                                         &#13;
     Le eventuali  disarmonie  del  sistema  in  atto,  sulle  quali  si  &#13;
 impernia    e   si   diffonde   la   motivazione   ampiamente   critica  &#13;
 dell'ordinanza,  non  assumono  rilevanza  ai  fini  del  giudizio   di  &#13;
 costituzionalità  della  norma contestata e nei limiti e negli aspetti  &#13;
 in cui è contestata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara:                                                            &#13;
     a)  la  manifesta  infondatezza  della  questione  di  legittimità  &#13;
 costituzionale   dell'art.   216   del  codice  penale,  sollevata,  in  &#13;
 riferimento  all'art.  38  della  Costituzione,  con   l'ordinanza   in  &#13;
 epigrafe;                                                                &#13;
     b)  non  fondata  la questione di legittimità costituzionale dello  &#13;
 stesso  art.  216  del  codice  penale,  sollevata   nella   richiamata  &#13;
 ordinanza,  in  riferimento  all'art.  3,  primo e secondo comma, della  &#13;
 Costituzione.                                                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 28 giugno 1973.         &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
