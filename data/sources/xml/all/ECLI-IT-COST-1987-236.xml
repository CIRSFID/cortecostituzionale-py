<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>236</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:236</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ANDRIOLI</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/06/1987</data_decisione>
    <data_deposito>23/06/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Virgilio ANDRIOLI; &#13;
 Giudici: dott. Francesco SAJA, prof. Giovanni CONSO, prof. Ettore &#13;
 GALLO, dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  13, quinto    &#13;
 comma, lett. b della legge 27 luglio 1978, n. 392  "Disciplina  delle    &#13;
 locazioni di immobili urbani", promossi con le seguenti ordinanze:       &#13;
      1)  ordinanza  emessa l'8 luglio 1980 dal Pretore di Bologna nel    &#13;
 procedimento civile vertente tra Opera Pia Asili Infantili e Zaccaria    &#13;
 Renato  iscritta  al  n. 654 del registro ordinanze 1980 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 311 dell'anno 1980;         &#13;
     2) ordinanza emessa il 31 luglio 1980 dal Giudice conciliatore di    &#13;
 Grugliasco nel procedimento civile vertente tra Soc.  San  Giorgio  e    &#13;
 Santini  Antonio  ed  altra iscritta al n. 792 del registro ordinanze    &#13;
 1980 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  20    &#13;
 dell'anno 1981;                                                          &#13;
      3)  ordinanza emessa il 16 marzo 1982 dal Pretore di Bologna nel    &#13;
 procedimento civile vertente tra Nanetti Igino e Serra Lino  iscritta    &#13;
 al  n.  313  del  registro ordinanze 1982 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 283 dell'anno 1982;                        &#13;
    Udito  nella  camera  di  consiglio  del  6 maggio 1987 il Giudice    &#13;
 relatore Vincenzo Caianiello.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  tre  ordinanze,  il Pretore di Bologna (Reg. ord. nn. 654 del    &#13;
 1980 e 313 del 1982) e il giudice conciliatore  di  Grugliasco  (Reg.    &#13;
 ord.   n.  792  del  1980)  sollevavano,  nel  corso  di  altrettanti    &#13;
 procedimenti promossi ex art. 45 della legge 27 luglio 1978, n.  392,    &#13;
 questione incidentale di legittimità costituzionale del quinto comma    &#13;
 dell'art. 13 della legge n. 392 del 1978, laddove vengono previsti  i    &#13;
 coefficienti  di maggiorazione per gli immobili di piccole dimensioni    &#13;
 ad uso di abitazione ai fini della determinazione dell'equo canone.      &#13;
    Nei  procedimenti sottoposti all'esame del Pretore di Bologna (nn.    &#13;
 654/80 e 313/82) si era dibattuto della determinazione del canone  di    &#13;
 appartamenti  rispettivamente  di  mq  68,60 e di mq 69,870; poiché,    &#13;
 applicando il  coefficiente  previsto  dalla  lett.  b)  della  norma    &#13;
 denunciata  (1,10), risulta un canone superiore a quello previsto per    &#13;
 immobile della estensione di mq 70,01, i giudici a quo ravvisano  una    &#13;
 penalizzazione  in  danno di chi conduce in locazione un appartamento    &#13;
 di più ridotte dimensioni, lamentando in particolare  l'applicazione    &#13;
 del  coefficiente  correttivo  anche  a  casi  in  cui  la superficie    &#13;
 convenzionale oltrepassa il limite massimo al di là del quale non vi    &#13;
 sarebbe nessuna correzione maggiorativa.                                 &#13;
    Per  contro,  nel  procedimento  sottoposto  all'esame del giudice    &#13;
 conciliatore di Grugliasco, si lamenta che un immobile di mq  71,350,    &#13;
 sia  soggetto  ad  un  canone  inferiore  a quello che, in virtù del    &#13;
 criterio di cui alla lett. e) della stessa norma, è previsto per gli    &#13;
 immobili di mq 70.                                                       &#13;
    Tutti  i  giudici  remittenti ravvisano una violazione dell'art. 3    &#13;
 Cost., in  quanto  verrebbe  leso  il  principio  di  ragionevolezza,    &#13;
 ravvisandosi  nelle diverse fattispecie una disparità di trattamento    &#13;
 in casi analoghi, non giustificata.                                      &#13;
    Il  Pretore di Bologna (ord. n. 654/80) stimola anche la Corte, ex    &#13;
 art. 27 legge 11 marzo 1953, n.  87,  in  caso  di  accoglimento,  ad    &#13;
 estendere  la  pronuncia  di  incostituzionalità  all'art. 13, comma    &#13;
 quinto, lett. c), ben potendosi ravvisare i medesimi incovenienti  in    &#13;
 relazione   agli  immobili  compresi  tra  quelli  aventi  superficie    &#13;
 inferiore a mq 46, rispetto  a  quelli  che  superano  di  poco  tali    &#13;
 dimensioni.                                                              &#13;
    L'ordinanza    veniva   ritualmente   notificata,   comunicata   e    &#13;
 pubblicata; non si aveva costituzione di  parti  né  intervento  del    &#13;
 Presidente del Consiglio dei Ministri.<diritto>Considerato in diritto</diritto>1.  - Le tre ordinanze del Pretore di Bologna, (nn. 654 del 1980 e    &#13;
 313 del 1982 Reg. ord.) e del giudice conciliatore di Grugliasco  (n.    &#13;
 792  del  1980  Reg.  ord.) concernono tutte l'art. 13 della legge 27    &#13;
 luglio 1978, n. 392;  i  relativi  giudizi  possono  pertanto  essere    &#13;
 riuniti e decisi con unica sentenza.                                     &#13;
    2.   -   Il   Pretore   di   Bologna   lamenta   che,  in  ragione    &#13;
 dell'applicazione del coefficiente maggiorativo previsto dalla  lett.    &#13;
 b) della norma denunciata e applicabile agli immobili compresi tra mq    &#13;
 46 e mq 70 (1,10), appartamenti compresi nella fascia  immediatamente    &#13;
 inferiore a mq 70,01 siano soggetti ad un canone superiore rispetto a    &#13;
 quelli compresi nella fascia immediatamente superiore ai  mq  70.  Si    &#13;
 ravvisa  in  ciò  una  irrazionale  penalizzazione  in  danno di chi    &#13;
 conduce in locazione un appartamento di più ridotte  dimensioni,  in    &#13;
 particolare  evidenziando  ipotesi  di  applicazione del coefficiente    &#13;
 correttivo anche a casi in cui la superficie convenzionale oltrepassa    &#13;
 il  limite  massimo  al  di  là  del  quale  non  vi sarebbe nessuna    &#13;
 correzione maggiorativa, con violazione dell'art. 3 Cost.                &#13;
    3. - La questione è fondata nei limiti di cui appresso.              &#13;
    In  proposito  questa  Corte  premette  che  in  sé  e per sé la    &#13;
 previsione legislativa di applicazione  di  coefficienti  correttivi,    &#13;
 per  rendere  più omogeneo il computo del canone di locazione per le    &#13;
 piccole unità immobiliari, non è irragionevole in  quanto  risponde    &#13;
 alla  esigenza,  evidentemente  ritenuta  valida  dal legislatore nel    &#13;
 proprio discrezionale apprezzamento, di  perequare  fra  loro  unità    &#13;
 immobiliari  aventi  caratteristiche  economiche  similari nonostante    &#13;
 modeste differenze di estensione.                                        &#13;
    L'applicazione di tali coefficienti diviene irragionevole le volte    &#13;
 in cui in base a tale applicazione accada che unità  di  consistenza    &#13;
 minore  risultino  soggette ad un canone più alto rispetto ad unità    &#13;
 abitative di dimensioni superiori.                                       &#13;
    Così  avviene  nel  caso  denunciato ove, tra le unità abitative    &#13;
 comprese tra i 46 ed i  70  mq,  quelle  a  ridosso  della  superfice    &#13;
 massima   di   riferimento   dei   70   mq,  finiscono,  per  effetto    &#13;
 dell'applicazione del  coefficiente,  con  il  superare  importo  del    &#13;
 canone di locazione delle abitazioni a partire da mq 70,01.              &#13;
    Il  sistema adottato diventa illogico a partire dalla superfice di    &#13;
 mq 63, 65 che, applicato il coefficiente di 1,10, dà  luogo  ad  una    &#13;
 superfice  convenzionale  di  70,01, venendo assoggettata a un canone    &#13;
 superiore a quello di mq. 70.                                            &#13;
    Di   conseguenza,  per  restituire  razionalità  al  sistema  dei    &#13;
 coefficienti, basta dichiarare l'illegittimità costituzionale  della    &#13;
 lettera  b),  del  comma  quinto,  dell'art. 13 della legge 27 luglio    &#13;
 1978, n. 392 nella parte in cui consente, mediante l'applicazione dei    &#13;
 coefficienti  maggiorativi,  che  il  canone  relativo ad immobili di    &#13;
 dimensioni inferiori ai 70,01 mq  possa  essere  maggiore  di  quello    &#13;
 previsto  per  gli  immobili compresi nella fascia superiore anziché    &#13;
 equiparato a quello per immobili di mq 70.                               &#13;
    4.  -  Poiché  peraltro  lo stesso inconveniente può verificarsi    &#13;
 anche in applicazione del coefficiente previsto dalla lett.  c),  del    &#13;
 quinto   comma,  art.  13  della  legge  n.  392  del  1978  per  gli    &#13;
 appartamenti di dimensioni inferiori a mq 46 (1,20), la Corte ritiene    &#13;
 ai  sensi dell'art. 27 della legge 11 marzo 1953, n. 87, estendere la    &#13;
 pronuncia di illegittimità costituzionale anche a tale  norma  nella    &#13;
 parte  in  cui  consente,  mediante  l'applicazione  dei coefficienti    &#13;
 maggiorativi, che  il  canone  relativo  ad  immobili  di  dimensioni    &#13;
 inferiori  ai  46  mq  possa  essere  maggiore di quello previsto per    &#13;
 immobili compresi  nella  fascia  superiore,  anziché  equiparato  a    &#13;
 quello previsto per immobili di mq 46.                                   &#13;
    5. - È opportuno chiarire che il riferimento alle superfici reali    &#13;
 formulato     differenziatamente     nella      dichiarazione      di    &#13;
 incostituzionalità   afferente  alla  lett.  b)  rispetto  a  quella    &#13;
 concernente la lett. c) della stessa norma è reso  necessario  dalla    &#13;
 differente  formulazione che il legislatore ha adottato relativamente    &#13;
 alle lettere b) e c) dell'articolo in esame, riferendosi la lett.  b)    &#13;
 a "unità immobiliare di superficie compresa tra mq 46 e mq 70", e la    &#13;
 lett. c) a "unità immobiliare di superficie inferiore a mq 46".         &#13;
    6.  -  Il  giudice  conciliatore  di  Grugliasco,  dal  canto suo,    &#13;
 evidenzia  l'ipotesi  in  cui  un  immobile  di  dimensioni  di  poco    &#13;
 superiore a mq 70,01 è soggetto ad un canone che risulta inferiore a    &#13;
 quello che, in virtù del criterio di cui alla lett. b) dello  stesso    &#13;
 art. 13, è previsto per gli immobili di mq 70.                          &#13;
    In  ragione  della  pronunciata  illegittimità  costituzionale in    &#13;
 parte qua, di cui più sopra, l'inconveniente in questione non potrà    &#13;
 però più verificarsi.                                                  &#13;
    Di   conseguenza   la  questione  di  legittimità  costituzionale    &#13;
 sollevata  dal  giudice  conciliatore  di  Grugliasco   deve   essere    &#13;
 dichiarata non fondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     a)  Dichiara  l'illegittimità  costituzionale  della  lett.  b),    &#13;
 dell'art. 13,  quinto  comma,  della  l.  27  luglio  1978,  n.  392,    &#13;
 "Disciplina  delle locazioni di immobili urbani", nella parte in cui,    &#13;
 mediante l'applicazione dei coefficienti maggiorativi,  consente  che    &#13;
 il  canone  relativo  ad immobili di dimensioni inferiori ai 70,01 mq    &#13;
 possa essere maggiore di quello previsto per immobili compresi  nella    &#13;
 fascia  superiore, anziché equiparato a quello previsto per immobili    &#13;
 di mq 70.                                                                &#13;
     b)  Dichiara, ai sensi dell'art. 27 della legge 11 marzo 1953, n.    &#13;
 87, la illegittimità costituzionale della lett.  c),  dell'art.  13,    &#13;
 quinto  comma,  della  l. 27 luglio 1978, n. 392, nella parte in cui,    &#13;
 mediante l'applicazione dei coefficienti                                 &#13;
  maggiorativi,  consente  che  il  canone  relativo  ad  immobili  di    &#13;
 dimensioni inferiori  ai  46  mq  possa  essere  maggiore  di  quello    &#13;
 previsto  per  immobili  compresi  nella  fascia  superiore  anziché    &#13;
 equiparato a quello previsto per immobili di mq 46.                      &#13;
     c)  Dichiara  conseguentemente  non  fondata, nei sensi di cui in    &#13;
 motivazione, la questione di legittimità costituzionale della  lett.    &#13;
 b),  dell'art.  13, quinto comma, della legge 27 luglio 1978, n. 392,    &#13;
 sollevata dal giudice conciliatore di Grugliasco con  l'ordinanza  n.    &#13;
 792 Reg. 1980.                                                           &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 giugno 1987.                               &#13;
                           Il Presidente: ANDRIOLI                        &#13;
                           Il Redattore: CAIANIELLO                       &#13;
    Depositata in cancelleria il 23 giugno 1987.                          &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
