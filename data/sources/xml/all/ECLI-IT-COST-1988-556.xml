<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>556</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:556</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/05/1988</data_decisione>
    <data_deposito>19/05/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 22 della legge    &#13;
 della Regione Sardegna 23 ottobre 1978, n.  62  ("I  controlli  sugli    &#13;
 enti  locali"),  promosso  con ordinanza emessa il 24 aprile 1985 dal    &#13;
 T.A.R. per la Sardegna sui ricorsi riuniti proposti da Rossi  Antonio    &#13;
 ed  altri  contro  il Comune di Bosa ed altro, iscritta al n. 148 del    &#13;
 registro ordinanze 1986 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 28/1ª s.s. dell'anno 1986;                                 &#13;
    Visti  gli atti di costituzione del Comune di Bosa e della Regione    &#13;
 Sardegna;                                                                &#13;
    Udito  nell'udienza pubblica del 22 marzo 1988 il Giudice relatore    &#13;
 Vincenzo Caianiello;                                                     &#13;
    Udito l'avv. Sergio Panunzio per la Regione Sardegna.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  TAR per la Sardegna, adito per l'annullamento di alcune    &#13;
 delibere di adozione del piano  regolatore  generale  del  Comune  di    &#13;
 Bosa, con ordinanza in data 24 aprile 1985, ha sollevato questione di    &#13;
 legittimità costituzionale  dell'art.  22  legge  reg.  Sardegna  23    &#13;
 ottobre  1978 n. 62 che, comminando la decadenza delle delibere degli    &#13;
 enti locali non pubblicate negli appositi  albi  entro  dieci  giorni    &#13;
 dalla  loro adozione, si porrebbe in contrasto con gli artt. 3, 4 e 5    &#13;
 dello  Statuto  Sardo  che  non  attribuiscono  alla  regione  alcuna    &#13;
 competenza legislativa in materia di ordinamento degli enti locali.      &#13;
    Ad  avviso  del  giudice  remittente,  la  disposizione impugnata,    &#13;
 disciplinando un "adempimento parallelo ma concettualmente  distinto"    &#13;
 da  quelli  che  la  legge regionale può legittimamente stabilire in    &#13;
 tema di controllo sugli atti degli enti territoriali minori, ai sensi    &#13;
 dell'art.  3  lett. a) e 48 (rectius 46) dello Statuto, va inquadrata    &#13;
 nell'ambito della materia attinente all'ordinamento e  alle  funzioni    &#13;
 comunali,   da   un  lato  sottratta  ad  ogni  sorta  di  competenza    &#13;
 legislativa  regionale,  e  dall'altro,  espressamente  riservata  al    &#13;
 legislatore statale dall'art. 128 della Costituzione.                    &#13;
    Osserva  inoltre  il  Tribunale  che  la  decadenza delle delibere    &#13;
 comunali, non pubblicate entro un  termine  così  breve  dalla  loro    &#13;
 adozione,   comporta   una   sostanziale  limitazione  dell'autonomia    &#13;
 comunale, tanto più grave ove  si  consideri  che  la  pubblicazione    &#13;
 nell'albo,   avendo  una  funzione  esclusivamente  divulgativa,  non    &#13;
 dovrebbe incidere sulle determinazioni sostanziali dell'ente e che il    &#13;
 pregiudizio  derivante  dalla  decadenza, attesa la mutevolezza degli    &#13;
 equilibri interni e della volontà politica  dell'organo  consiliare,    &#13;
 non  sempre  può  essere  evitato  mediante  la  semplice riadozione    &#13;
 dell'atto.                                                               &#13;
    2.  -  Si è costituito il Comune di Bosa chiedendo l'accoglimento    &#13;
 della questione sulla base dell'art. 4 e dell'art. 46  dello  Statuto    &#13;
 Sardo,   che   attribuisce  alla  regione  una  potestà  legislativa    &#13;
 concorrente in materia di controllo sugli atti degli enti locali.        &#13;
    Ad  avviso  del Comune, infatti, poiché la disposizione impugnata    &#13;
 va ricompresa nell'ambito di tale  potestà  legislativa,  del  tutto    &#13;
 inconferente dovrebbe ritenersi il richiamo operato dal giudice a quo    &#13;
 agli  artt.  3  e  5  dello  Statuto,  nonché  all'art.  128   della    &#13;
 Costituzione,  mentre un esame della questione alla luce dell'art. 46    &#13;
 dello Statuto sarebbe possibile tenendo conto del riferimento  che  a    &#13;
 tale   articolo   si   fa  nella  motivazione  del  provvedimento  di    &#13;
 rimessione.                                                              &#13;
    Osserva  poi  nel  merito  la  parte  che  anche quando si volesse    &#13;
 inquadrare  la  disposizione  censurata  nell'ambito  della   materia    &#13;
 attinente al controllo sugli enti locali, la fissazione di un termine    &#13;
 così ridotto per la pubblicazione delle delibere nell'albo comunale,    &#13;
 violerebbe  un  principio fondamentale della legislazione statale che    &#13;
 garantisce comunque, per la stesura e pubblicazione  delle  delibere,    &#13;
 un congruo termine.                                                      &#13;
    Anche  la Regione autonoma Sardegna si è costituita confutando le    &#13;
 tesi sostenute  nell'ordinanza  di  rimessione  e  chiedendo  che  la    &#13;
 questione sia dichiarata infondata.                                      &#13;
    Ritiene   in  particolare  la  regione  che  la  disciplina  della    &#13;
 pubblicazione delle delibere comunali rientrerebbe nella materia  dei    &#13;
 controlli   sugli  enti  locali  come  risulta  dal  suo  inserimento    &#13;
 nell'art. 60 ultimo  comma  della  legge  statale  n.  62  del  1953,    &#13;
 riguardante  appunto  i  controlli  sulle province sui comuni e sugli    &#13;
 altri enti locali.                                                       &#13;
    D'altra  parte, la fissazione di un limite di tempo massimo per la    &#13;
 pubblicazione degli atti  degli  enti  locali  risponderebbe  ad  una    &#13;
 fondamentale  esigenza di garanzia dell'effettività ed efficenza del    &#13;
 meccanismo di controllo, collocandosi armonicamente  all'interno  del    &#13;
 vigente  sistema  legislativo,  senza perciò violare alcun principio    &#13;
 generale della normativa statale in materia.<diritto>Considerato in diritto</diritto>1.  -  Oggetto  della  questione di legittimità costituzionale è    &#13;
 l'art. 22 della  legge  regionale  della  Sardegna,  che  commina  la    &#13;
 decadenza delle deliberazioni degli enti locali territoriali, nonché    &#13;
 dei loro Consorzi e Comprensori, non pubblicate  entro  dieci  giorni    &#13;
 dalla adozione.                                                          &#13;
   Ad  avviso  del  giudice  a  quo  tale  disposizione si porrebbe in    &#13;
 contrasto con gli artt. 3, 4 e 5 dello  Statuto  regionale,  che  non    &#13;
 attribuiscono  alcuna  competenza legislativa alla regione in materia    &#13;
 di ordinamento degli enti locali.                                        &#13;
    2. - La questione è fondata, sia pure solo parzialmente.             &#13;
    La norma denunciata, fatte salve le disposizioni di legge speciali    &#13;
 che prevedono termini e periodi diversi di pubblicazione,  stabilisce    &#13;
 che  le  deliberazioni  degli  enti  in  parola sono pubblicate negli    &#13;
 appositi albi entro dieci giorni dalla adozione e per  la  durata  di    &#13;
 quindici giorni, a pena di decadenza.                                    &#13;
    Osserva  la  Corte  che  tale  norma,  per  la  parte che concerne    &#13;
 l'obbligo della pubblicazione delle deliberazioni degli  enti  locali    &#13;
 entro  un certo termine, non può considerarsi illegittima, in quanto    &#13;
 la pubblicazione, nel dare notizia ai cittadini  delle  deliberazioni    &#13;
 stesse,  consente ad essi di partecipare, seppur indirettamente, alla    &#13;
 funzione di controllo, mediante la  proposizione  di  opposizioni  ai    &#13;
 competenti  organi  preposti  a questa funzione che, anche attraverso    &#13;
 tale strumento partecipativo, sono posti in grado  di  esercitare  in    &#13;
 modo  più  ampio  e  completo.  Nei  sensi anzidetti trattasi di una    &#13;
 previsione fra l'altro già  esistente  nella  legislazione  statale,    &#13;
 perché  difatti l'art. 163 del regolamento di esecuzione della legge    &#13;
 comunale e provinciale, approvato con r.d. 12 febbraio 1911  n.  297,    &#13;
 stabiliva  che  il  certificato  della  eseguita  pubblicazione delle    &#13;
 deliberazioni comunali e provinciali, "deve far  menzione  se  siansi    &#13;
 prodotte opposizioni".                                                   &#13;
    In  quanto  diretta  a  disciplinare i termini della pubblicazione    &#13;
 che, per quel che si è detto, assume rilievo anche  con  riferimento    &#13;
 al  controllo  sulle  deliberazioni,  la  disposizione rientra dunque    &#13;
 nella  competenza  della  regione  cui  spetta,  nella  materia   dei    &#13;
 controlli,  potestà  legislativa ai sensi dell'art. 46 dello Statuto    &#13;
 regionale Sardegna.                                                      &#13;
    A  diverse  conclusioni  devesi  invece  pervenire  per  quel  che    &#13;
 riguarda la  comminatoria  di  decadenza  prevista  dall'articolo  in    &#13;
 esame,  nella  ipotesi  in cui non si provveda alla pubblicazione nei    &#13;
 termini e per la durata indicata. Quella della decadenza  è  difatti    &#13;
 una previsione che non attiene alla materia di controlli, bensì alla    &#13;
 disciplina della efficacia delle deliberazioni, in quanto il  mancato    &#13;
 adempimento  delle  formalità  di  pubblicazione  determina appunto,    &#13;
 secondo  la  norma  denunciata,  la  decadenza  degli  effetti  delle    &#13;
 deliberazioni stesse.                                                    &#13;
    Sotto  quest'ultimo  aspetto la norma esula dalla competenza della    &#13;
 Regione, cui  non  spetta  la  potestà  legislativa  in  materia  di    &#13;
 ordinamento   degli   enti  locali,  e,  quindi,  limitatamente  alla    &#13;
 previsione della decadenza, deve essere dichiarata costituzionalmente    &#13;
 illegittima.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale dell'art. 22 della legge    &#13;
 regionale della Sardegna 23 ottobre 1978 n. 62  ("I  controlli  sugli    &#13;
 Enti   locali")  nella  parte  in  cui  prevede  la  decadenza  delle    &#13;
 deliberazioni dei  Comuni,  Province,  Comunità  montane,  organismi    &#13;
 comprensoriali e Consorzi che non siano pubblicate entro dieci giorni    &#13;
 dalla loro adozione e per la durata di quindici giorni.                  &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 maggio 1988.                                &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 maggio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
