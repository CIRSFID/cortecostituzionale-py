<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>575</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:575</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/12/1987</data_decisione>
    <data_deposito>23/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 9 della legge    &#13;
 20 febbraio 1958, n. 55 (Estensione del trattamento di riversibilità    &#13;
 ed  altre  provvidenze  in  favore  dei pensionati dell'assicurazione    &#13;
 obbligatoria per la  invalidità,  la  vecchiaia  ed  i  superstiti),    &#13;
 promosso  con  ordinanza emessa il 7 maggio 1981 dal Pretore di Udine    &#13;
 nel procedimento civile vertente tra Nigris  Federico  e  l'I.N.P.S.,    &#13;
 iscritta  al  n.  446  del registro ordinanze 1981 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica n. 290 dell'anno 1981;               &#13;
    Visti  l'atto  di  costituzione  dell'I.N.P.S.  nonché  l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nell'udienza  pubblica  del  10  novembre  1987  il Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Uditi  l'avv.  Fabrizio  Ausenda per l'I.N.P.S. e l'Avvocato dello    &#13;
 Stato Paolo d'Amico per il Presidente del Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con ordinanza emessa il 7 maggio 1981 il Pretore di Udine ha    &#13;
 sollevato  questione  incidentale  di   legittimità   costituzionale    &#13;
 dell'art. 9 l. 20 febbraio 1958 n. 55 "nella parte in cui equipara al    &#13;
 periodo  di  servizio  militare,  considerato  utile  ai  fini  della    &#13;
 pensione  e  della  misura di essa, quello della militarizzazione del    &#13;
 lavoratore dipendente dello Stato o da Ente pubblico e non quello del    &#13;
 dipendente (militarizzato) di un privato imprenditore", per contrasto    &#13;
 con l'art. 3 Cost.; nonché "quello di lavoro coatto o di  cattività    &#13;
 degli  ex  internati civili in Germania e non altri periodi consimili    &#13;
 in Paesi diversi", in riferimento anche all'art. 35 Cost.                &#13;
    Il  giudizio  a  quo risulta promosso, nei confronti dell'INPS, da    &#13;
 Nigris Federico per il riconoscimento,  ai  fini  pensionistici,  del    &#13;
 periodo trascorso quale dipendente privato militarizzato, nonché del    &#13;
 periodo di cattività trascorso in Albania.                              &#13;
    Secondo il Pretore, la norma impugnata sarebbe "priva di qualsiasi    &#13;
 razionalità e quindi dispregiativa del  principio  di  uguaglianza",    &#13;
 avendo  previsto  l'equiparazione  al  servizio militare soltanto dei    &#13;
 periodi trascorsi da militarizzati dipendenti dello Stato e  di  Enti    &#13;
 pubblici  e non anche dei periodi relativi a militarizzati dipendenti    &#13;
 privati; così come risultano utili i periodi di lavoro coatto  o  di    &#13;
 cattività  degli  ex internati civili in Germania e non anche quelli    &#13;
 trascorsi in altri territori.                                            &#13;
    Tanto più - osserva il rimettente in ordine al secondo punto "che    &#13;
 l'art. 35 Cost. dichiara che la Repubblica tutela il lavoro in  tutte    &#13;
 le  sue forme ed applicazioni compresa quindi quella cui un cittadino    &#13;
 sia costretto da forze occupanti una parte del  territorio  nazionale    &#13;
 qual'era in quel tempo l'Albania".                                       &#13;
    2. - Nel presente giudizio si è costituito l'INPS, secondo cui il    &#13;
 r.d.l. 30 marzo  1943  n.  123  (Disciplina  della  militarizzazione)    &#13;
 distinse  i militarizzati "di diritto", cioè coloro facenti parte di    &#13;
 personale  distaccato  dalle  mansioni  normali  e  che  in  caso  di    &#13;
 applicazione  della  legge  di guerra venivano assegnati a comandi, o    &#13;
 reparti o servizi delle forze armate (art. 1), dai  militarizzati  su    &#13;
 ordine  specifico  della competente Autorità militare in costanza di    &#13;
 rapporto di lavoro soggetto all'obbligo delle  assicurazioni  sociali    &#13;
 (art. 2).                                                                &#13;
    Infondata  sarebbe  quindi la prima questione di costituzionalità    &#13;
 dato che questi ultimi militarizzati  non  potrebbero  in  ogni  caso    &#13;
 essere   assimilati   ai  primi  ai  fini  dell'accreditamento  della    &#13;
 contribuzione figurativa.                                                &#13;
    Sul  secondo  profilo  relativo  ai periodi di cattività in Paesi    &#13;
 diversi  dalla  Germania,  si  deduce  una  "diversità  obiettiva  e    &#13;
 razionalmente  apprezzabile  tra  la  situazione  degli  internati in    &#13;
 Germania  e  quella  degli  internati  in  altri   Paesi;   ciò   in    &#13;
 considerazione delle dimensioni storiche dell'evento e dei più gravi    &#13;
 disagi  e  maltrattamenti  riservati  ai  prigionieri  italiani   dai    &#13;
 tedeschi, a seguito dei noti eventi bellici".                            &#13;
    Permarrebbe,   comunque,   solo   una  "lievissima  diversità  di    &#13;
 trattamento", dal momento che la  l.  21  dicembre  1974  n.  702  ha    &#13;
 riconosciuto agli internati in altri Paesi alleati, nemici o neutrali    &#13;
 la facoltà di riscattare i relativi  periodi  di  prigionia  con  un    &#13;
 minimo   onere   economico   (contributi   base   non  integrati  né    &#13;
 rivalutati).                                                             &#13;
    3.  -  In giudizio è intervenuta, per il Presidente del Consiglio    &#13;
 dei   ministri,   l'Avvocatura   generale   dello   Stato   svolgendo    &#13;
 argomentazioni identiche a quelle rese dall'INPS.<diritto>Considerato in diritto</diritto>1.  - L'art. 7 della legge 20 febbraio 1958, n. 55 (Estensione del    &#13;
 trattamento di riversibilità ed  altre  provvidenze  in  favore  dei    &#13;
 pensionati  dell'assicurazione  obbligatoria  per  la invalidità, la    &#13;
 vecchiaia ed i superstiti) accorda ai titolari di pensioni  a  carico    &#13;
 dell'assicurazione  medesima,  che abbiano prestato servizio militare    &#13;
 nel  periodo  bellico,  "un  supplemento  di   pensione",   calcolato    &#13;
 figurativamente  come se nel periodo alle armi fosse stato versato un    &#13;
 contributo corrispondente alla prima classe di contribuzioni indicate    &#13;
 nelle inerenti tabelle.                                                  &#13;
    Il  successivo  art. 9 considera periodi di servizio militare, nei    &#13;
 sensi e per i fini anzidescritti, anche quelli  -  tra  gli  altri  -    &#13;
 prestati   in   qualità   di   "militarizzati"   da   dipendenti  di    &#13;
 Amministrazioni dello Stato o di Enti pubblici, con esclusione  così    &#13;
 -  di quegli altri soggetti pure militarizzati ma quali prestatori di    &#13;
 lavoro alle dipendenze di privati.                                       &#13;
    2.  -  Il  giudice rimettente (Pretore di Udine) ravvisa contrasto    &#13;
 dell'enunciata  norma  -  ex  art.  3  Cost.  -  per   ingiustificata    &#13;
 disparità  di trattamento - in ordine al beneficio del supplemento -    &#13;
 fra dipendenti pubblici e privati.                                       &#13;
    In adverso, oppongono l'INPS e l'Avvocatura dello Stato che le due    &#13;
 situazioni  non  sarebbero   comparabili,   mancando   identità   di    &#13;
 condizioni.                                                              &#13;
    Per  la  legislazione  del  tempo  (r.d.l.  30 marzo 1943, n. 123:    &#13;
 Disciplina della militarizzazione) lo status in esame  sarebbe  stato    &#13;
 assunto   dal   lavoratore   privato   solo  per  atto  discrezionale    &#13;
 dell'autorità  amministrativa   militare   (art.   2),   mentre   la    &#13;
 militarizzazione  del personale civile della pubblica amministrazione    &#13;
 si  sarebbe  costituita  di  diritto,  in  base   ai   documenti   di    &#13;
 mobilitazione  e  con  distacco  dalle  mansioni  normali:  da qui la    &#13;
 diversità di posizione di stato e di conseguente attività in regime    &#13;
 di militarizzazione.                                                     &#13;
    L'eccezione non ha pregio.                                            &#13;
    La  disposizione inerente al beneficio pensionistico supplementare    &#13;
 non ha discriminato  quanto  alla  fonte  -  di  diritto  ovvero  per    &#13;
 specifico  provvedimento  -  ai  fini di acquisto della condizione di    &#13;
 militarizzato. Secondo, poi, la disciplina del tempo la posizione "di    &#13;
 diritto"   (art.  1  del  r.d.l.  n.  123)  concerneva  non  soltanto    &#13;
 "dipendenti  delle  amministrazioni  dello  Stato",   ma,   altresì,    &#13;
 "qualsiasi  cittadino"  assegnato  alle  forze armate operanti: ed è    &#13;
 solo  così  che  vi  si  possono  considerare  ricompresi  anche   i    &#13;
 dipendenti  da altri Enti pubblici, ma evidentemente non solo questi.    &#13;
    Ma anche da ciò prescindendo, la norma specifica per l'assunzione    &#13;
 della condizione di militarizzato su ordine dell'Autorità  avvinceva    &#13;
 in  identità  di situazione e di soggezione alla legge penale e alla    &#13;
 disciplina militare tanto dipendenti da Amministrazioni dello Stato e    &#13;
 da  altri Enti pubblici, quanto appartenenti a stabilimenti ausiliari    &#13;
 "ed altre aziende private" (art. 2, comma secondo, nn. 1 e 2).           &#13;
    E  la sopravvenuta norma di legge (n. 55 del 1958) sui benefici in    &#13;
 discussione al fine di evitare indebiti cumuli pone, per  suo  conto,    &#13;
 su  di  un  identico  piano  le  due categorie (art. 10, primo comma,    &#13;
 lettere a e d).                                                          &#13;
    Sicché,  la  disposizione  comportante  una  discriminazione  tra    &#13;
 soggetti  in  capo  ai  quali   ricadeva,   al   tempo   della   loro    &#13;
 militarizzazione,  l'identità  di  stato  e di conseguenti doveri è    &#13;
 palesemente irrazionale, nella parte in cui non ammette al  beneficio    &#13;
 di cui trattasi anche i dipendenti privati.                              &#13;
    3.   -  Il  Pretore  rimettente  dubita  pure  della  legittimità    &#13;
 costituzionale del predetto art. 9 legge n. 55, nella  parte  in  cui    &#13;
 l'attribuzione  dei  benefici  supplementari  spetta per i periodi di    &#13;
 lavoro coatto  o  di  cattività  ai  soli  ex  internati  civili  in    &#13;
 Germania,  con  esclusione  di  quelli trascorsi in altri territori o    &#13;
 teatri di guerra.                                                        &#13;
    Anche qui il rimettente ravvisa un contrasto con l'art. 3 ed anche    &#13;
 col successivo art. 35 Cost., apparendogli ingiustificata - a parità    &#13;
 di  tutela  -  la  discriminazione  territoriale  circa  il  luogo di    &#13;
 internamento.                                                            &#13;
    È  da  considerare,  tuttavia,  che il legislatore ha limitato il    &#13;
 supplemento pensionistico ai deportati  in  Germania,  avendo  inteso    &#13;
 riconoscere   i   connotati  della  particolare  afflittività  della    &#13;
 detenzione in suolo tedesco. Peraltro, non ha inteso  lasciare  privo    &#13;
 di benefici ogni altro periodo di internamento nei territori alleati,    &#13;
 nemici o neutrali:  ha  concesso,  infatti,  a  coloro  che  avessero    &#13;
 subìto   cattività   del   genere   la  facoltà  di  riscatto  dei    &#13;
 corrispondenti periodi con un onere contributivo minimo, ragguagliato    &#13;
 cioè  alle  norme  e  ai criteri in vigore all'epoca cui il riscatto    &#13;
 stesso va riferito  (art.  2  legge  21  dicembre  1974,  n.  702  in    &#13;
 relazione all'art. 6 legge 28 marzo 1968, n. 341): trattasi di scelte    &#13;
 discrezionali,  nell'area  previdenziale,  che  al  solo  legislatore    &#13;
 competono.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara l'illegittimità costituzionale dell'art. 9, primo comma,    &#13;
 legge 28 febbraio  1958,  n.  55  -  Estensione  del  trattamento  di    &#13;
 riversibilità   ed   altre  provvidenze  in  favore  dei  pensionati    &#13;
 dell'assicurazione obbligatoria per la invalidità, la vecchiaia ed i    &#13;
 superstiti  -  nella  parte  in cui non prevede che, agli effetti del    &#13;
 precedente art. 7, siano considerati  periodi  di  servizio  militare    &#13;
 anche  quelli  prestati  come  militarizzati da dipendenti di aziende    &#13;
 private;                                                                 &#13;
    Dichiara inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 9, primo comma, predetto,  nella  parte  concernente,  agli    &#13;
 effetti  del  precedente art. 7, la valutazione dei periodi di lavoro    &#13;
 coatto o di cattività limitatamente  agli  ex  internati  civili  in    &#13;
 Germania,  sollevata dal Pretore di Udine - in riferimento agli artt.    &#13;
 3 e 35 Cost. - con l'ordinanza in epigrafe.                              &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, l'11 dicembre 1987.        &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: BORZELLINO                           &#13;
    Depositata in cancelleria il 23 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
