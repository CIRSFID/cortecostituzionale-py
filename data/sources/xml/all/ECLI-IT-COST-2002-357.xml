<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>357</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:357</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia>Carlo Mezzanotte</redattore_pronuncia>
    <data_decisione>10/07/2002</data_decisione>
    <data_deposito>17/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'articolo 1033 del &#13;
codice civile, promosso con ordinanza emessa il 14 ottobre 1997 dalla &#13;
Corte  d'appello di Milano, iscritta al n. 926 del registro ordinanze &#13;
2001 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 46, 1ª &#13;
serie speciale, dell'anno 2001. &#13;
    Udito  nella  camera  di  consiglio dell'8 maggio 2002 il giudice &#13;
relatore Carlo Mezzanotte. &#13;
    Ritenuto  che, con ordinanza in data 14 ottobre 1997, pervenuta a &#13;
questa  Corte  l'8 novembre 2001, la Corte d'appello di Milano, quale &#13;
giudice  di  rinvio,  ha  sollevato,  in riferimento agli articoli 3, &#13;
primo  comma,  e  42, secondo comma, della Costituzione, questione di &#13;
legittimità  costituzionale  dell'articolo  1033  del codice civile, &#13;
nella  parte  in  cui "non prevede anche l'obbligo di dare passaggio, &#13;
analogo  a  quello  dovuto  alle condotte di acque, a tubi o ad altri &#13;
condotti per la fornitura di gas metano"; &#13;
        che  il  remittente  premette che la Corte di cassazione, con &#13;
sentenza  n. 11130 del 1992, nel cassare, con rinvio, la sentenza con &#13;
la  quale  era  stata  accolta  la domanda delle attrici ed era stata &#13;
costituita una servitù coattiva di "metanodotto" in favore del fondo &#13;
delle stesse, ha affermato il seguente principio di diritto: "qualora &#13;
non  ricorrano le specifiche figure di servitù coattive previste dal &#13;
codice civile, negli artt. da 1033 a 1057, ovvero da leggi speciali - &#13;
e,  nella  specie,  invocandosi  una  servitù  di "metanodotto", non &#13;
legislativamente  prevista,  si rientrava in tale ipotesi, - non può &#13;
essere  invocata  la  disciplina dell'art. 1032 cod. civ. e seguenti, &#13;
trattandosi di disposizioni speciali, non estensibili all'infuori dei &#13;
casi espressamente considerati"; &#13;
        che,  ad  avviso  del  giudice  a  quo  nella interpretazione &#13;
imposta   dalla   Corte   di   cassazione,   l'art. 1033   cod.  civ. &#13;
contrasterebbe,  in  primo luogo, con l'art. 3 della Costituzione, in &#13;
quanto,   essendo   identici   i  "bisogni  della  vita"  soddisfatti &#13;
dall'acqua  e  dall'energia termica in genere, essendo le esigenze di &#13;
fruizione dell'una e dell'altra egualmente diffuse nella popolazione, &#13;
sussistendo identità di interesse pubblico tra la fruizione in massa &#13;
dell'acqua   corrente   proveniente  dal  pubblico  acquedotto  e  la &#13;
fruizione del gas metano (energia pulita e meno costosa) attinto alla &#13;
rete  pubblica  (meglio  controllabile  e  più idonea, rispetto agli &#13;
impianti  autonomi,  a  garantire  l'incolumità  dei  singoli),  non &#13;
essendo  dissimili  le  opere necessarie alla conduzione dell'acqua e &#13;
del   gas  metano,  e  non  potendosi  ormai  ravvisare  una maggiore &#13;
pericolosità  delle condutture del gas rispetto a quelle dell'acqua, &#13;
attesa l'avanzata tecnologia e le specifiche prescrizioni legislative &#13;
di  sicurezza delle condutture del metano e dei relativi impianti, il &#13;
fatto    che    siano    diversamente   tutelate   le   esigenze   di &#13;
approvvigionamento   dell'acqua  e  del  metano  sarebbe  lesivo  del &#13;
principio di eguaglianza; &#13;
        che  la  medesima  disposizione  contrasterebbe  altresì con &#13;
l'art. 42,  secondo  comma,  della Costituzione, giacché limiterebbe &#13;
diversamente   il  diritto  di  proprietà  dei  singoli,  rendendolo &#13;
coercibile  a fini di utilità sociale solo nel caso dell'acqua e non &#13;
anche nel caso del metano. &#13;
    Considerato  che  il  remittente  sollecita  una pronuncia con la &#13;
quale  l'ambito di operatività dell'articolo 1033 del codice civile, &#13;
che  prevede  la  costituzione coattiva della servitù di acquedotto, &#13;
sia  esteso a comprendere la possibilità di costituire coattivamente &#13;
la servitù di metanodotto; &#13;
        che  tale  richiesta  è formulata sulla base del rilievo che &#13;
l'energia  termica  costituirebbe  oggi un bisogno della vita al pari &#13;
dell'acqua  e della ritenuta insussistenza di qualsivoglia componente &#13;
di maggior  pericolosità nel trasporto attraverso condutture del gas &#13;
metano rispetto al trasporto dell'acqua; &#13;
        che  le  situazioni  poste  a raffronto dal giudice a quo non &#13;
possono  essere  ritenute  a  tal  punto  omogenee  da imporre, quale &#13;
soluzione    costituzionalmente    obbligata,    l'estensione   della &#13;
possibilità   di  costituire  coattivamente  anche  la  servitù  di &#13;
gasdotto; &#13;
        che,  infatti,  pur  tenendo  conto,  come  invita  a fare il &#13;
remittente,  del  fatto  che la distribuzione del gas metano tende ad &#13;
intensificarsi sempre più, anche per la molteplicità degli impieghi &#13;
di  cui  tale  fonte di energia è suscettibile, vale ad escludere la &#13;
prospettata  identità  di  situazioni  il  rilievo  che  le utilità &#13;
conseguibili dall'impiego del metano, a differenza di quelle connesse &#13;
alla  utilizzazione  dell'acqua,  possono  essere acquisite anche con &#13;
altre   fonti   di  energia;  sicché  non  appare  irragionevole  la &#13;
valutazione  che  il legislatore ha compiuto allorché ha previsto la &#13;
costituzione  coattiva  della  servitù  di acquedotto e non anche di &#13;
metanodotto; &#13;
        che  alla  affermazione  di  un  diritto alla costituzione di &#13;
servitù   coattiva   di   metanodotto   non  può  certo  pervenirsi &#13;
considerando  questa  quale  soluzione  necessitata  derivante  dalla &#13;
scelta del legislatore di favorire la diffusione del gas metano; &#13;
        che, se tale indirizzo legislativo non può essere negato, ed &#13;
è anzi ravvisabile in molteplici atti normativi (v., in particolare, &#13;
legge  29 settembre  1964,  n. 847;  legge  28 novembre 1980, n. 784, &#13;
art. 11;   decreto-legge  31 agosto  1987,  n. 364,  convertito,  con &#13;
modificazioni,  dalla  legge 29 ottobre 1987, n. 205, art. 3; decreto &#13;
legislativo  23 maggio  2000,  n. 164),  da  esso  non  è  possibile &#13;
desumere  anche  la  scelta di un modello coercitivo nella disciplina &#13;
dei  rapporti  tra  fondi  vicini  che  solo  il legislatore potrebbe &#13;
introdurre   (come   fece   a   coronamento   di   un   programma  di &#13;
elettrificazione  generalizzato  del  Paese)  e  che  non può essere &#13;
assunto  da  questa Corte come costituzionalmente vincolato proprio a &#13;
causa  dell'esistenza  di  fonti di energia alternative, di modalità &#13;
tecniche  di  approvvigionamento del gas metano diverse dal trasporto &#13;
attraverso  condutture  e,  infine, della possibilità di giungere al &#13;
medesimo risultato mediante atti di esercizio dell'autonomia privata; &#13;
        che,   pertanto,   la   questione   deve   essere  dichiarata &#13;
manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'articolo  1033  del codice civile, &#13;
sollevata,  in  riferimento  agli articoli 3 e 42 della Costituzione, &#13;
dalla   Corte  d'appello  di  Milano,  con  l'ordinanza  indicata  in &#13;
epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                      Il redattore: Mezzanotte &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 17 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
