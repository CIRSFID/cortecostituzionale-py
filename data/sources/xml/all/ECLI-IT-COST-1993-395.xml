<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>395</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:395</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/11/1993</data_decisione>
    <data_deposito>16/11/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 2 e 4,  commi    &#13;
 primo  e quarto, della legge 26 settembre 1985, n. 482 (Modificazioni    &#13;
 del trattamento tributario delle indennità di fine  rapporto  e  dei    &#13;
 capitali  corrisposti  in  dipendenza  di  contratti di assicurazione    &#13;
 sulla vita), promosso con ordinanza emessa il 19 gennaio  1993  dalla    &#13;
 Corte  di cassazione sul ricorso proposto dall'Inadel contro Carrieri    &#13;
 Domenico, iscritta al n. 219 del registro ordinanze 1993 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  20,  prima   serie    &#13;
 speciale, dell'anno 1993;                                                &#13;
    Udito  nella  camera  di  consiglio del 20 ottobre 1993 il Giudice    &#13;
 relatore Prof. Gabriele Pescatore;                                       &#13;
    Ritenuto che la Corte di  cassazione,  con  ordinanza  19  gennaio    &#13;
 1993,  ha  sollevato  questione  di  legittimità  costituzionale, in    &#13;
 riferimento agli artt. 3 e 53 della Costituzione, degli artt. 2 e  4,    &#13;
 commi primo e quarto, della legge                                        &#13;
 26 settembre 1985, n. 482;                                               &#13;
      che  tale  questione  è stata proposta nel corso di un giudizio    &#13;
 promosso nell'ottobre 1983 da un  dipendente  comunale,  collocato  a    &#13;
 riposo  nel  luglio  1978,  il  quale  chiedeva la restituzione della    &#13;
 ritenuta Irpef fatta  dall'Inadel  sulla  sua  indennità  premio  di    &#13;
 servizio;                                                                &#13;
      che  il  giudice  a  quo  ha esposto in proposito che, a seguito    &#13;
 della sentenza n. 178 del 1986, di  questa  Corte,  l'art.  4,  comma    &#13;
 3-ter, del decreto-legge 14 marzo 1988, n. 154 (rectius d.l. 14 marzo    &#13;
 1988,  n.  70,  nel  testo di cui alla legge di conversione 13 maggio    &#13;
 1988,  n.  154),  ha  disposto  la  detrazione   dall'imponibile   da    &#13;
 assoggettare  ad  Irpef,  di  un  importo  pari  al  rapporto  tra  i    &#13;
 contributi versati dal lavoratore e quelli versati  dall'ente  datore    &#13;
 di  lavoro, riguardo a tutte le indennità di fine rapporto, comunque    &#13;
 denominate, alla cui formazione concorrano  contributi  previdenziali    &#13;
 posti a carico dei lavoratori;                                           &#13;
      che, in seguito, l'art. 6, comma primo bis, del decreto-legge 30    &#13;
 maggio  1988,  n.  173, convertito, con modificazioni, nella legge 26    &#13;
 maggio 1988, n. 291, ha disposto l'applicazione retroattiva  di  tale    &#13;
 disposizione,  con  effetto  dal  17  luglio 1986 e, successivamente,    &#13;
 l'art. 2 del decreto-legge 2  marzo  1989,  n.  69,  convertito,  con    &#13;
 modificazioni nell'art. 2- bis della legge 27 aprile 1989, n. 154, ha    &#13;
 stabilito  che  le disposizioni di cui al comma terzo ter dell'art. 4    &#13;
 del decreto-legge 2 marzo 1988, n. 70, convertito  con  modificazioni    &#13;
 dalla  legge 13 maggio 1988, n. 154, si applicano alle indennità ivi    &#13;
 indicate corrisposte successivamente alla data di entrata  in  vigore    &#13;
 della  legge  26  settembre 1985, n. 482, nonché a quelle indennità    &#13;
 per le quali trovano applicazione gli artt. 4 e 5 della stessa  legge    &#13;
 n.  482  del  1985,  ancorché non sia stata presentata l'istanza ivi    &#13;
 prevista, così disponendo - con il richiamo all'art. 5  della  legge    &#13;
 n.  482  del  1985  - l'applicazione retroattiva della riliquidazione    &#13;
 dell'imposta per tutte le indennità, comunque denominate,  percepite    &#13;
 a decorrere dal primo gennaio 1980;                                      &#13;
      che secondo il giudice a quo le norme impugnate violerebbero gli    &#13;
 artt.  3  e  53 della Costituzione, nella parte in cui non prevedono,    &#13;
 per le indennità premio di servizio erogate dall'Inadel nel  periodo    &#13;
 di tempo intercorrente tra il 1 gennaio 1974 e il 1 gennaio 1980, che    &#13;
 dall'imponibile  da  assoggettare  ad imposta vada detratta una somma    &#13;
 pari  alla  percentuale  dell'indennità  di   premio   di   servizio    &#13;
 corrispondente  al  rapporto  esistente  alla data del collocamento a    &#13;
 riposo, tra il contributo posto a carico dell'iscritto  e  l'aliquota    &#13;
 complessiva   del   contributo   previdenziale  obbligatorio  versato    &#13;
 dall'Inadel;                                                             &#13;
    Considerato che nel giudizio a  quo  si  verteva  su  un  rimborso    &#13;
 d'imposta,  cui  si  applica il termine di decadenza di diciotto mesi    &#13;
 previsto dall'art. 38, secondo comma, del d.P.R. 29  settembre  1973,    &#13;
 n. 602;                                                                  &#13;
      che  tale  termine  di decadenza è stato ritenuto legittimo con    &#13;
 sentenza n. 494 del 1991 di questa Corte, che ha anche  affermato  la    &#13;
 legittimità   del   limite   temporale   (1  gennaio  1980)  per  la    &#13;
 riliquidazione dell'Irpef sulle indennità di fine  rapporto  erogate    &#13;
 prima  dell'entrata  in vigore della legge 26 settembre 1985, n. 482,    &#13;
 ove non sia stata presentata istanza di  rimborso  entro  l'anzidetto    &#13;
 termine;                                                                 &#13;
      che   nell'ordinanza  di  rimessione  manca  ogni  accenno  alla    &#13;
 tempestività della domanda in relazione a tale termine di decadenza;    &#13;
      che,  pertanto,  la  rilevanza  della   questione   non   appare    &#13;
 adeguatamente motivata;                                                  &#13;
    Visto l'art. 23, comma secondo, della legge 11 marzo 1953, n. 87;     &#13;
    Visti  gli  artt.  26  della  legge 11 marzo 1953, n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale degli artt. 2 e 4, commi primo e quarto,    &#13;
 della legge 26 settembre 1985, n. 482 (Modificazioni del  trattamento    &#13;
 tributario   delle   indennità  di  fine  rapporto  e  dei  capitali    &#13;
 corrisposti in dipendenza di contratti di assicurazione sulla  vita),    &#13;
 sollevata, in riferimento agli artt. 3 e 53 della Costituzione, dalla    &#13;
 Corte di cassazione, con l'ordinanza indicata in epigrafe.               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 3 novembre 1993.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: PESCATORE                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 16 novembre 1993.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
