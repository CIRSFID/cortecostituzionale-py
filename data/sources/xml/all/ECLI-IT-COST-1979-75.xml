<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>75</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:75</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/1979</data_decisione>
    <data_deposito>16/07/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. &#13;
 BRUNETTO BUCCTARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO &#13;
 PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 20 del d.l. 3  &#13;
 febbraio  1970,  n. 7, in relaz.  agli artt. 8 e 9 legge 3 maggio 1967,  &#13;
 n. 317 promosso con ordinanza emessa il 17 dicembre   1973 dal  pretore  &#13;
 di  Petilia  Policastro,  nel procedimento civile vertente tra Talarico  &#13;
 Simone e  l'Ispettorato provinciale del lavoro di  Catanzaro,  iscritta  &#13;
 al  n.  495  del  registro ordinanze   1975 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n.  332 del 17 dicembre 1975.                 &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri nonché l'atto di  costituzione del Ministero del lavoro;       &#13;
     udito  nell'udienza  pubblica del 3 maggio 1979 il Giudice relatore  &#13;
 Alberto Malagugini;                                                      &#13;
     udito il vice avvocato generale dello Stato Giovanni Albisinni  per  &#13;
 il  Presidente  del  Consiglio    dei  ministri  e per il Ministero del  &#13;
 lavoro.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso di un procedimento di opposizione ad ordinanza  5  aprile  &#13;
 1973 dell'Ispettorato  provinciale del lavoro di Catanzaro, concernente  &#13;
 il  pagamento  di  una sanzione pecuniaria   amministrativa a sensi del  &#13;
 d.l. 3 febbraio 1970, n. 7 (convertito con legge 11 marzo 1970, n. 83),  &#13;
 il pretore di  Petilia  Policastro  con  ordinanza  17  dicembre  1973,  &#13;
 accogliendo  un'istanza    dell'opponente Talarico Simone, ha sollevato  &#13;
 questione di legittimità costituzionale del citato  art. 20  del  d.l.  &#13;
 n.  7/1970, per ritenuto contrasto con gli artt. 102, 113 e 24, secondo  &#13;
 comma,  della Costituzione.                                              &#13;
     Il  d.l.  n.  70/1970  ("norme  in  materia   di   collocamento   e  &#13;
 accertamento dei lavoratori  agricoli") all'art. 20 prevede le sanzioni  &#13;
 per  la  violazione  delle  disposizioni in esso contenute; i primi tre  &#13;
 commi prevedono sanzioni  penali  (ammenda),  gli  altri  una  sanzione  &#13;
 pecuniaria  amministrativa.  Nella specie, viene in questione il quarto  &#13;
 comma, in base al quale "I  datori  di    lavoro  che  non  assumono  i  &#13;
 lavoratori  per  il  tramite della sezione dell'Ufficio del lavoro sono  &#13;
 assoggettati alla sanzione amministrativa del pagamento   di una  somma  &#13;
 da lire 50.000 a lire 200.000 per ogni lavoratore assunto".              &#13;
     Il  pretore  prende  le mosse dalla sentenza n. 32/1970 della Corte  &#13;
 costituzionale, e riconosce  la legittimità di principio di  leggi  di  &#13;
 depenalizzazione  e  di un conseguente procedimento del  tipo di quello  &#13;
 introdotto dalla legge n. 317/67. Rileva tuttavia che, nella specie, è  &#13;
 questione  di una violazione che, fino al d.l. n. 7/70, era punita come  &#13;
 contravvenzione dalla legge 21  aprile 1949, n. 264 (artt. 13 e  27)  e  &#13;
 successivamente,  a  suo  giudizio,  è tornata ad essere   punita come  &#13;
 contravvenzione dall'art.   33 della  legge  20  maggio  1970,  n.  300  &#13;
 (statuto  dei  lavoratori).  Rileva,  inoltre,  che  il d.l. n. 7/70, a  &#13;
 differenza della legge  n.  317/67,  non  reca    alcuna  dichiarazione  &#13;
 espressa  sulla  depenalizzazione di violazioni in esso contemplate. Da  &#13;
 ciò la conclusione "che il legislatore, nell'assoggettare  a  sanzione  &#13;
 amministrativa  una  violazione  di  legge  già  esistente  nel nostro  &#13;
 ordinamento e per tradizione considerata sempre  quale illocito  penale  &#13;
 stabilendo per essa la procedura di cui agli artt. 8 e 9 sopra citati e  &#13;
 senza  espressamente  dichiararne  la  depenalizzazione,  abbia violato  &#13;
 quanto  disposto  dagli  articoli  102,  113,  24  della  Costituzione,  &#13;
 trasferendo  ad  un  organo  amministrativo  il potere di   giudicare e  &#13;
 punire  una  violazione  di  legge  che  non   poteva,   senza   idoneo  &#13;
 provvedimento      legislativo,  essere  sottratta  al  giudice  penale  &#13;
 ordinario,  con  conseguente  soppressione  del  diritto  alla  difesa,  &#13;
 garantito e disciplinato dalle norme del processo penale".               &#13;
     L'ordinanza di rimessione è stata emessa in data 17 dicembre 1973;  &#13;
 il   fascicolo  di  causa  è    pervenuto  alla  Corte  costituzionale  &#13;
 incompleto, il 26 maggio 1975, ed ha potuto essere  registrato soltanto  &#13;
 in data 29 ottobre 1975.                                                 &#13;
     Nel procedimento davanti alla Corte costituzionale  è  intervenuta  &#13;
 l'Avvocatura   di  Stato    sostenendo  l'erroneità  del  surriportato  &#13;
 ragionamento del giudice a quo. "Non vi è dubbio  infatti -  argomenta  &#13;
 l'Avvocatura di Stato - che l'idoneo provvedimento  legislativo  esiste  &#13;
 ed  è    costituito  appunto  dal decreto-legge 3 febbraio 1970, n. 7,  &#13;
 convertito nella legge 11 marzo   1970, n. 83.  In  tale  provvedimento  &#13;
 legislativo  non  era certamente necessaria una espressa  dichiarazione  &#13;
 circa la intenzione del legislatore  di  "depenalizzare"  un  illecito,  &#13;
 degradandolo  a  sanzione  amministrativa,  risultando ciò chiaramente  &#13;
 dalla qualificazione di "sanzione amministrativa" che, nell'art. 20 del  &#13;
 d.l. 3 febbraio  1970, n. 7, viene data  alla  sanzione  comminata  per  &#13;
 l'illecito previsto".<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  pretore  di Petilia Policastro dubita della legittimità  &#13;
 costituzionale "dell'art. 20 d.l. 3  febbraio 1970, n. 7, in  relazione  &#13;
 agli  artt.  8 e 9 legge 3 maggio 1967, n. 317, sotto il profilo  della  &#13;
 violazione degli artt.  102, 113, 24 della Costituzione".                &#13;
     Il d.l. 3 febbraio 1970, n. 7 è  stato  convertito  con  legge  11  &#13;
 marzo  1970,  n.  83,  portante    modifiche, non rilevanti ai fini del  &#13;
 presente  giudizio  incidentale,  anche  agli  artt.  10   e   20.   La  &#13;
 disposizione  in  esame  è, dunque, quella dell'art. 20, quarto comma,  &#13;
 del d.l. 3 febbraio 1970,  n. 7, nel testo risultante per effetto della  &#13;
 modificazione introdotta con la succitata legge di conversione.          &#13;
     Il giudice a  quo  muove  dal  riconoscimento  che  "non  esistendo  &#13;
 distinzione qualitativa tra  illecito amministrativo ed illecito penale  &#13;
 contravvenzionale,   la   qualificazione   di   un  fatto  e  la    sua  &#13;
 trasposizione dall'una all'altra categoria dipende esclusivamente dalla  &#13;
 discrezionale    valutazione  del  legislatore".  Ciò non ostante egli  &#13;
 dubita della legittimità costituzionale dell'art.  20,  quarto  comma,  &#13;
 del d.l. n. 7 del 1970, perché con esso il legislatore ha  trasformato  &#13;
 in  illecito  amministrativo una fattispecie in precedenza disciplinata  &#13;
 quale  illecito      penale   "senza   espressamente   dichiararne   la  &#13;
 depenalizzazione".  Risulterebbero  perciò violati,  sempre secondo il  &#13;
 giudice a quo, gli artt.  102,  113,  24  della  Costituzione,  perché  &#13;
 sarebbe    stato  trasferito  "ad un organo amministrativo il potere di  &#13;
 giudicare e punire una violazione di    legge  che  non  poteva,  senza  &#13;
 idoneo  provvedimento  legislativo,  essere sottratta al giudice penale  &#13;
 ordinario,  con  conseguente  soppressione  del  diritto  alla  difesa,  &#13;
 garantito e disciplinato  dalle norme del processo penale".              &#13;
     2.  -  La  semplice  enunciazione  delle argomentazioni addotte dal  &#13;
 giudice a quo dimostra la   manifesta infondatezza della  questione  di  &#13;
 costituzionalità  da  lui  sollevata.  Appare  perciò  superflua ogni  &#13;
 ulteriore motivazione  che  potrebbe  soltanto  richiamare  principi  e  &#13;
 concetti  elementari  a  cominciare da quello per cui una legge (o atto  &#13;
 avente forza di legge) è    il  provvedimento  legislativo  pienamente  &#13;
 idoneo  a  produrre  gli  effetti  risultanti  dal  testo    normativo,  &#13;
 inequivocabilmente voluti dal legislatore ordinario, nei  limiti  della  &#13;
 propria indiscutibile e indiscussa competenza.                           &#13;
     E  poiché  questo  è  il pretesto del quale il pretore di Petilia  &#13;
 Policastro si avvale per riproporre  motivi di incostituzionalità, con  &#13;
 riguardo agli artt. 8 e 9 della legge n. 317 del 1967, già  dichiarati  &#13;
 non fondati da questa Corte con la sentenza n. 32 del  1970  -  che  il  &#13;
 giudice  a  quo    mostra  di conoscere, ma di voler disattendere -  la  &#13;
 questione va dichiarata manifestamente  infondata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara manifestamente  infondata  la  questione  di  legittimità  &#13;
 costituzionale  dell'art.  20 d.l. 3  febbraio 1970, n. 7, in relazione  &#13;
 agli artt. 8 e 9 della legge 3 maggio 1967, n. 317, sotto il    profilo  &#13;
 della  violazione degli artt. 102, 113 e 24 Cost. sollevata dal pretore  &#13;
 di Petilia  Policastro con l'ordinanza indicata in epigrafe.             &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, l'll luglio 1979.                                &#13;
                                   F.to:   LEONETTO   AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA -  GUIDO  ASTUTI  -  MICHELE  &#13;
                                   ROSSANO  -  LEOPOLDO ELIA - GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO  REALE    BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
