<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>386</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:386</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/11/1998</data_decisione>
    <data_deposito>27/11/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 3  della  legge    &#13;
 30  luglio  1990,  n.  217  (Istituzione del patrocinio a spese dello    &#13;
 Stato per i non abbienti), promosso con ordinanza emessa il 12 luglio    &#13;
 1997 dal Tribunale di  Catania,  iscritta  al  n.  189  del  registro    &#13;
 ordinanze 1998 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 13, prima serie speciale, dell'anno 1998.                             &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 30 settembre  1998  il  giudice    &#13;
 relatore Carlo Mezzanotte.                                               &#13;
   Ritenuto  che,  con  ordinanza  del 12 luglio 1997, il Tribunale di    &#13;
 Catania, chiamato a pronunciarsi su alcune istanze di  ammissione  al    &#13;
 patrocinio  a  spese  dello  Stato, ha sollevato, in riferimento agli    &#13;
 artt.  3  e  24  della  Costituzione,   questione   di   legittimità    &#13;
 costituzionale  dell'art.  3  della  legge  30  luglio  1990,  n. 217    &#13;
 (Istituzione del patrocinio a spese dello Stato per i non  abbienti),    &#13;
 nella  parte  in  cui  fissa  come  condizione  per  l'ammissione  al    &#13;
 beneficio  la  titolarità  di  un  reddito  risultante   dall'ultima    &#13;
 dichiarazione non superiore ad un determinato limite, senza prevedere    &#13;
 la  possibilità  di  accertamenti  in  ordine  alle reali condizioni    &#13;
 economiche e patrimoniali dell'istante e senza che sia consentito  al    &#13;
 giudice  verificare se i suoi redditi siano alimentati da proventi di    &#13;
 attività illecite desumibili anche dal suo tenore di vita;              &#13;
     che,  ad  avviso  del  remittente,  la   disposizione   censurata    &#13;
 violerebbe  in  primo luogo l'art. 3 della Costituzione che impone al    &#13;
 legislatore il rispetto del canone della ragionevolezza,  in  quanto,    &#13;
 prevedendo   soltanto   controlli  formali  sulla  posizione  fiscale    &#13;
 dell'istante, determinerebbe la possibilità  che  siano  ammesse  al    &#13;
 patrocinio  a  spese  dello  Stato  persone  che  traggono redditi da    &#13;
 attività illecite;                                                      &#13;
     che l'art. 3 della  Costituzione  sarebbe  altresì  violato  per    &#13;
 l'ingiustificato  eguale trattamento riservato a categorie di persone    &#13;
 che si trovano in condizioni economiche diverse, e cioè  a  soggetti    &#13;
 che versano in situazione di effettiva impossidenza e a soggetti che,    &#13;
 traendo   i   propri  mezzi  economici  da  traffici  illeciti,  solo    &#13;
 formalmente risultano impossidenti;                                      &#13;
     che, sotto un ulteriore  profilo,  il  principio  di  eguaglianza    &#13;
 sarebbe  violato  per il trattamento deteriore riservato ai cittadini    &#13;
 abbienti, i quali, traendo i loro proventi da attività lecite,  sono    &#13;
 assoggettati  all'obbligo  della  dichiarazione  dei  redditi  e  non    &#13;
 possono fruire del  beneficio,  al  raffronto  con  i  cittadini  che    &#13;
 abbiano  redditi provenienti da attività illecita che possono invece    &#13;
 essere ammessi al patrocinio a spese dello Stato;                        &#13;
     che violato,  infine,  sarebbe  l'art.  24,  terzo  comma,  della    &#13;
 Costituzione, secondo il quale i mezzi per agire e difendersi davanti    &#13;
 a  ogni  giurisdizione  dovrebbero  essere,  con  appositi  istituti,    &#13;
 assicurati esclusivamente a coloro che versino in una  situazione  di    &#13;
 non  abbienza  effettiva  e  non  anche  a  coloro  che si limitino a    &#13;
 dichiarare di essere non abbienti;                                       &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo che la questione sia dichiarata manifestamente infondata.      &#13;
   Considerato che questa Corte, con la sentenza n. 144 del 1992 e con    &#13;
 la  successiva  ordinanza n. 244 del 1998, ha rilevato e ribadito che    &#13;
 la snella procedura di ammissione al beneficio prevista dall'art.   6    &#13;
 della  legge  n.  217  del  1990,  benché non lasci spazio ad alcuna    &#13;
 verifica o controllo preventivi da parte del giudice  competente,  è    &#13;
 da  ritenere pienamente attuativa del dettato costituzionale, poiché    &#13;
 la garanzia del patrocinio dei non abbienti deve essere assicurata in    &#13;
 tempi brevi, incompatibili con controlli e indagini  di  una  qualche    &#13;
 durata sull'effettivo reddito dell'istante;                              &#13;
     che  il pericolo che, a causa della limitatezza dell'accertamento    &#13;
 che il giudice è chiamato  a  compiere  in  sede  di  ammissione  al    &#13;
 patrocinio  a  spese  dello  Stato,  possano  prodursi  le situazioni    &#13;
 denunciate dal remittente è scongiurato dall'art. 6, comma 3,  della    &#13;
 medesima  legge,  in  forza del quale l'istanza dell'interessato e il    &#13;
 decreto  di  ammissione,  unitamente  alle   dichiarazioni   e   alla    &#13;
 documentazione  allegate,  devono  essere trasmessi all'Intendente di    &#13;
 finanza  perché  possa  verificare  l'esattezza  dell'ammontare  del    &#13;
 reddito  attestato  dall'imputato  e disporre eventualmente controlli    &#13;
 anche a mezzo della Guardia di finanza;                                  &#13;
     che,  diversamente  da  quanto  mostra  di  ritenere  il  giudice    &#13;
 remittente, in sede di successivo accertamento assumono rilievo anche    &#13;
 i  redditi che provengano da attività illecite (ordinanza n. 244 del    &#13;
 1998), poiché anche con riferimento a questi l'Intendente di finanza    &#13;
 può proporre al giudice la revoca o  la  modifica  del  beneficio  e    &#13;
 provocare  gli  effetti recuperatori stabiliti dall'art. 11 in favore    &#13;
 dello Stato e l'applicazione delle sanzioni penali previste dall'art.    &#13;
 5, comma 7, a carico dell'indebito beneficiario;                         &#13;
     che, pertanto, la questione deve essere dichiarata manifestamente    &#13;
 infondata.                                                               &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art.  3  della  legge  30  luglio  1990,  n.  217    &#13;
 (Istituzione  del patrocinio a spese dello Stato per i non abbienti),    &#13;
 sollevata, in riferimento agli artt. 3 e 24 della  Costituzione,  dal    &#13;
 Tribunale di Catania con l'ordinanza indicata in epigrafe.               &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 novembre 1998.                             &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Mezzanotte                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 27 novembre 1998.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
