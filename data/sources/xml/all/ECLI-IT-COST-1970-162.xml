<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>162</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:162</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/11/1970</data_decisione>
    <data_deposito>18/11/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. &#13;
 ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'articolo unico del  &#13;
 d.P.R. 14 luglio 1960, n.  1032, nella parte in cui rende efficaci erga  &#13;
 omnes gli artt. 34 e 62 del c.c.n.l. 24 lugllo  1959  per  gli  addetti  &#13;
 all'edilizia,  e  dell'articolo  unico del d.P.R 9 maggio 1961, n. 715,  &#13;
 nella parte in cui rende efficace erga omnes l'art.  11  del  c.c.l.  1  &#13;
 settembre  1959  per  gli  operai  edili  ed  affini della provincia di  &#13;
 Genova, promosso con ordinanza emessa l'8 aprile 1970  dal  pretore  di  &#13;
 Genova nel procedimento penale a carico di Mazzucchelli Mario, iscritta  &#13;
 al  n.    154  del  registro ordinanze 1970 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 136 del 3 giugno 1970.                     &#13;
     Udito nella camera di consiglio del  28  ottobre  1970  il  Giudice  &#13;
 relatore Costantino Mortati.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  del  procedimento  penale  contro  Mazzucchelli  Mario,  &#13;
 opponente avverso il decreto penale col quale era  stato  condannato  a  &#13;
 lire  410.000 di ammenda perché, nella sua qualità di titolare di una  &#13;
 impresa edile, aveva violato l'art. 34 del c.c.n.l. 24 luglio 1959  per  &#13;
 gli  addetti  all'edilizia,  in  relazione  all'art.  11  del  c.c.l. 1  &#13;
 settembre 1959, integrativo del predetto per la  provincia  di  Genova,  &#13;
 per  avere  omesso  di  accantonare in favore di 41 dipendenti le quote  &#13;
 stabilite per ferie, festività e gratifica natalizia,  il  pretore  di  &#13;
 Genova,  con  ordinanza in data 8 aprile 1970 ha sollevato questione di  &#13;
 costituzionalità del d.P.R. 14 luglio 1960, n. 1032,  nella  parte  in  &#13;
 cui  rende  obbligatori  erga  omnes  gli artt. 34 e 62 del c.c.n.l. 24  &#13;
 luglio 1959 per gli addetti all'edilizia e specificamente del d.P.R.  9  &#13;
 maggio  1961,  n. 715, nella parte in cui rende obbligatorio erga omnes  &#13;
 l'art. 11 del c.c.l. per la provincia di Genova del 1  settembre  1959,  &#13;
 in riferimento agli artt. 76 e 77, primo comma, della Costituzione.      &#13;
     Ricordata  la  giurisprudenza della Corte costituzionale in materia  &#13;
 di casse edili e  rilevato  come  la  norma  del  contratto  collettivo  &#13;
 concernente  la  provincia  di  Genova  non sia stata finora dichiarata  &#13;
 incostituzionale, pur essendo affetta da vizi  in  tutto  e  per  tutto  &#13;
 analoghi   a  quelli  cui  si  riferivano  le  precedenti  pronunce  di  &#13;
 accoglimento, il pretore ha disposto la sospensione del giudizio  e  la  &#13;
 trasmissione  degli  atti  a  questa  Corte.  L'ordinanza, notificata e  &#13;
 comunicata a termini di  legge,  è  stata  pubblicata  nella  Gazzetta  &#13;
 Ufficiale  n.  136  del  3  giugno  1970.  Nessuna  delle  parti  si è  &#13;
 costituita nel giudizio così promosso.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'articolo unico del d.P.R. n. 1032 del 1960, nella  parte  in  cui  &#13;
 rende  efficaci  erga omnes gli artt. 34 e 62 del c.c.n.l. suddetto, è  &#13;
 stato già dichiarato illegittimo con la sentenza di  questa  Corte  n.  &#13;
 129  del 1963 e pertanto la questione, nella corrispondente parte, deve  &#13;
 essere dichiarata manifestamente infondata.                              &#13;
     Deve  invece  essere  decisa  per  la  prima  volta  la   questione  &#13;
 concernente  l'articolo  unico del d.P.R. n.  715 del 1961, nella parte  &#13;
 in cui rende efficace erga omnes l'art.  11  del  c.c.l.  suddetto,  la  &#13;
 quale, pur presentando elementi comuni rispetto a quella risolta con la  &#13;
 sentenza n. 129 del 1963 (e confermata nelle successive nn. 31, 59, 78,  &#13;
 79  e  97  del  1964, n. 100 del 1965, n. 48 del 1966, nn.  41 e 73 del  &#13;
 1967, nn. 33 e 34 del 1969, n. 71 del 1970), riguarda una norma che  ha  &#13;
 una portata distinta da quelle espressamente dichiarate illegittime con  &#13;
 tali pronunce, anche se è analoga ad esse per il suo contenuto.         &#13;
     Infatti  con  la  sentenza  n.  41  del  1967  è  stata dichiarata  &#13;
 l'illegittimità costituzionale  del  d.P.R.  n.    715  del  1961,  ma  &#13;
 soltanto  nella  parte in cui rendeva efficace erga omnes l'art. 11 del  &#13;
 c.c.l. 2 ottobre 1959, che prevedeva la costituzione della Cassa  edile  &#13;
 spezzina  ed  il  versamento  di contributi a suo favore, mentre con la  &#13;
 sentenza n. 71 del 1970 lo stesso d.P.R. è stato colpito  nella  parte  &#13;
 in  cui  rendeva efficace erga omnes l'art. 12 del c.c.l.  le settembre  &#13;
 1959, che prevede la costituzione  della  Cassa  edile  genovese  e  il  &#13;
 versamento di contributi a favore di essa.                               &#13;
     Viene  invece ora in considerazione l'art. 11 dello stesso c.c.l. e  &#13;
 precisamente la disposizione contenuta nel quarto  comma  di  esso,  in  &#13;
 virtù  della  quale  le  imprese  sono tassativamente tenute a versare  &#13;
 l'importo della  percentuale  del  22,30  della  retribuzione  globale,  &#13;
 dovuta  ai lavoratori per ferie, gratifica natalizia e festività, alla  &#13;
 Cassa edile genovese; in proposito non vi è che da ripetere quanto  fu  &#13;
 detto  nelle  precedenti  sentenze  sopra  ricordate  e  cioè  che  le  &#13;
 disposizioni  degli  accordi  o  contratti  collettivi  relative   agli  &#13;
 obblighi  derivanti  per  gli  addetti alle industrie edilizie e affini  &#13;
 dalla costituzione delle casse edili non corrispondono  alle  finalità  &#13;
 per l'adempimento delle quali è stato attribuito il potere legislativo  &#13;
 delegato  ai  sensi della legge 14 luglio 1959, n. 741, e pertanto tali  &#13;
 obblighi non  possono  essere  imposti  anche  nei  confronti  dei  non  &#13;
 iscritti alle associazioni che li hanno stipulati.                       &#13;
     È  chiaro  tuttavia  che  l'illegittimità della norma che dispone  &#13;
 l'accantonamento presso la Cassa edile delle somme  dovute  per  ferie,  &#13;
 gratifica  natalizia,  ecc.,  non  si estende affatto alla norma che fa  &#13;
 obbligo al datore di lavoro di accantonare  le  percentuali  stabilite,  &#13;
 accantonamento  che  potrà  avvenire  in  una  qualsiasi  delle  forme  &#13;
 consentite.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale dell'articolo unico del d.P.R. 14 luglio 1960, n.  1032,  &#13;
 già  dichiarato  costituzionalmente illegittimo con la sentenza n. 129  &#13;
 del 4 luglio 1963 nella parte in cui rendeva efficaci  erga  omnes  gli  &#13;
 artt. 34 e 62 del c.c.n.l. 24 luglio 1959 per gli addetti all'edilizia;  &#13;
     dichiara  l'illegittimità  costituzionale  dell'articolo unico del  &#13;
 d.P.R. 9 maggio 1961, n. 715, nella parte  in  cui  rende  obbligatorio  &#13;
 erga omnes il quarto comma dell'art. 11 del c.c.l. 1 settembre 1959 per  &#13;
 gli operai edili ed affini della provincia di Genova.                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 novembre 1970.       &#13;
                                   GIUSEPPE BRANCA - MICHELE FRAGALI   -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
