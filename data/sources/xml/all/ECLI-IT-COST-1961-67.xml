<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1961</anno_pronuncia>
    <numero_pronuncia>67</numero_pronuncia>
    <ecli>ECLI:IT:COST:1961:67</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CAPPI</presidente>
    <relatore_pronuncia>Michele Fragali</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/12/1961</data_decisione>
    <data_deposito>22/12/1961</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. GIUSEPPE CAPPI, Presidente - Prof. &#13;
 GASPARE AMBROSINI - Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO &#13;
 GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - &#13;
 Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO &#13;
 PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1 e 3 della  &#13;
 legge  13  dicembre  1956, n. 1409, promosso con ordinanza emessa il 22  &#13;
 giugno 1960 dal Tribunale di Ravenna nel procedimento penale  a  carico  &#13;
 di  Penso  Mario  e  Baracchini  Walter, iscritta al n. 85 del Registro  &#13;
 ordinanze 1960 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 267 del 29 ottobre 1960.                                              &#13;
     Vista  la  dichiarazione di intervento del Presidente del Consiglio  &#13;
 dei Ministri;                                                            &#13;
     udita nell'udienza pubblica del 18 ottobre 1961  la  relazione  del  &#13;
 Giudice Michele Fragali;                                                 &#13;
     udito  il sostituto avvocato generale dello Stato Franco Chiarotti,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza 22 giugno 1960, il Tribunale di Ravenna, sospeso  &#13;
 il procedimento penale pendente contro Penso Mario e Baracchini Walter,  &#13;
 ha rimesso a questa Corte il giudizio sulla questione  di  legittimità  &#13;
 costituzionale,  sollevata  dalla  difesa del Penso, relativamente agli  &#13;
 artt. 1 e 3 della legge 13 dicembre 1956, n. 1409,  per  contrasto  con  &#13;
 l'art.  10,  primo  comma,  della Costituzione. L'art. 1 della suddetta  &#13;
 legge impone, ai  capitani  di  navi  nazionali  di  stazza  netta  non  &#13;
 superiore  alle duecento tonnellate che trasportino tabacchi, l'obbligo  &#13;
 di redigere il manifesto del carico prescritto  dalla  legge  doganale,  &#13;
 anche  fuori  della  zona  di  vigilanza doganale marittima, e l'art. 3  &#13;
 punisce l'inosservanza di tale obbligo mediante l'estensione delle pene  &#13;
 stabilite per il contrabbando  dei  tabacchi  esteri;  l'ordinanza  del  &#13;
 Tribunale  ha  elevato il dubbio sulla legittimità costituzionale, sia  &#13;
 dell'obbligo  fatto  da  detta   legge,   sia   dell'estensione   delle  &#13;
 disposizioni  stabilite  per  il  contrabbando del tabacco, nel caso di  &#13;
 mancanza del manifesto del carico, quando la  nave  si  trovi  in  mare  &#13;
 aperto.                                                                  &#13;
     L'ordinanza  è  stata  notificata  al Presidente del Consiglio dei  &#13;
 Ministri il 26 agosto 1960, al Presidente del Senato il 17 luglio  1960  &#13;
 e al Presidente della Camera dei Deputati il 18 dello stesso luglio.     &#13;
     È  stata  pubblicata nella Gazzetta Ufficiale della Repubblica del  &#13;
 29 ottobre 1960, n. 267.                                                 &#13;
     2. - Intervenuto in giudizio, con atto depositato il  15  settembre  &#13;
 1960,  il  Presidente  del  Consiglio  dei  Ministri ha fatto rilevare,  &#13;
 anzitutto, che l'ordinanza di rimessione non specifica quali  siano  le  &#13;
 norme   del  diritto  internazionale,  generalmente  riconosciute,  che  &#13;
 contrastano  con  le  disposizioni  impugnate.  Nello  stesso  atto  di  &#13;
 intervento  e nella memoria del 25 settembre 1961, volendo supporre che  &#13;
 l'ordinanza si sia riferita ad una norma di diritto internazionale  che  &#13;
 imponga  allo  Stato  di  ispirare  la propria legislazione soltanto al  &#13;
 principio della territorialità della norma penale, il  Presidente  del  &#13;
 Consiglio ha osservato:                                                  &#13;
     a)  che  non esiste una norma vincolante di quel contenuto, potendo  &#13;
 trovare applicazione in materia anche il principio  dell'universalità,  &#13;
 per  cui  la  legge  penale  dovrebbe applicarsi a chiunque commetta il  &#13;
 reato, dovunque si trovi e qualunque sia lo Stato al quale  appartiene,  &#13;
 o  la  regola  della  personalità,  per  la  quale  la legge penale si  &#13;
 applicherebbe soltanto ai cittadini dello  Stato  che  ha  disposto  la  &#13;
 sanzione,  dovunque  si  trovino, in modo che, per ogni soggetto, abbia  &#13;
 valore soltanto la legge dello  Stato  cui  appartiene  o,  infine,  il  &#13;
 principio  della  difesa,  che vuole applicata soltanto la legge penale  &#13;
 dello Stato cui appartiene il soggetto passivo del reato, ovunque  esso  &#13;
 si trovi e da chiunque offeso;                                           &#13;
     b)  il  principio  della  territorialità  è  assunto  dallo Stato  &#13;
 italiano con i temperamenti risultanti  da  una  parziale  accettazione  &#13;
 delle altre regole, in modo che non urta contro alcuna norma di diritto  &#13;
 internazionale  generalmente  riconosciuta, la previsione di una figura  &#13;
 criminosa costituita da condotta posta in essere e  da  evento  che  si  &#13;
 determini al di fuori del territorio dello Stato;                        &#13;
     c)  il  principio  della  territorialità,  comunque,  non è stato  &#13;
 violato dalla  legge  impugnata,  perché,  nella  specie,  l'azione  e  &#13;
 l'omissione  considerate,  consistenti  nel trasporto di tabacchi senza  &#13;
 manifesto di carico, sono punite da quella legge  in  quanto  poste  in  &#13;
 essere su nave nazionale, che l'art. 5, secondo comma, Cod.  penale, in  &#13;
 conformità dell'ordinamento internazionale, considera territorio dello  &#13;
 Stato;                                                                   &#13;
     d)  la  migliore dottrina internazionalistica afferma la regola per  &#13;
 cui la nave privata resta sottoposta alla  potestà  di  governo  dello  &#13;
 Stato  della  bandiera  financo  ove  si  trovi  in  acque territoriali  &#13;
 straniere;                                                               &#13;
     e) nella specie si avrebbe, tutt'al più,  un  reato  commesso  dal  &#13;
 cittadino  in  territorio  estero,  per  il  quale  la  legge impugnata  &#13;
 stabilisce il vigore della legge penale italiana, e al  quale,  quindi,  &#13;
 è applicabile l'art. 7, n. 5, del Codice penale.                        &#13;
     3.  -  Non  si  sono costituite le parti private e, alla udienza di  &#13;
 discussione, l'Avvocatura dello Stato ha ribadito  le  deduzioni  sopra  &#13;
 esposte.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Rileva l'Avvocatura dello Stato che l'ordinanza del Tribunale  &#13;
 di Ravenna non indica esplicitamente quali siano le  norme  di  diritto  &#13;
 internazionale  generalmente  riconosciute, che sarebbero state violate  &#13;
 dagli artt. 1 e 3 della legge 13 dicembre 1956, n. 1409.                 &#13;
     Tuttavia,  si  desume,  per  implicito,  dal  suo   contesto,   che  &#13;
 l'ordinanza  ha  presupposto  esistente  una regola la cui applicazione  &#13;
 escluderebbe la competenza  legislativa  dello  Stato,  in  materia  di  &#13;
 polizia  amministrativa  (in  particolare  di polizia finanziaria) e in  &#13;
 materia penale, riguardo alla  nave  nazionale  che  navighi  nell'alto  &#13;
 mare.  Infatti, essa dubita che possa "imporsi obbligo del manifesto di  &#13;
 carico  per  trasporti  di  tabacchi  fuori  della  zona  di  vigilanza  &#13;
 doganale"  e  che  "la  mancanza  di detto documento, quando la nave si  &#13;
 trovi in mare aperto, costituisca reato di contrabbando di tabacco": si  &#13;
 invoca, quindi, il principio di territorialità della potestà  statale  &#13;
 e  della legge penale, che si ritiene costituisca una regola di diritto  &#13;
 internazionale generalmente riconosciuta, garantita dalla  Costituzione  &#13;
 a norma del suo art. 10.                                                 &#13;
     Senonché questo principio non è geralmente inteso nel significato  &#13;
 restrittivo avuto presente dal Tribunale di Ravenna.                     &#13;
     2. - Infatti riceve unanime consenso l'affermazione che la potestà  &#13;
 dello  Stato sui propri cittadini segue costoro anche nei trasferimenti  &#13;
 all'estero, con il solo tramite segnato da una  analoga  potestà  che,  &#13;
 sugli stranieri spetta allo Stato della nuova residenza, secondo il suo  &#13;
 diritto  interno e il diritto internazionale. E, in correlazione, si è  &#13;
 rilevato che  non  esiste  alcuna  regola  internazionale  generalmente  &#13;
 riconosciuta, la quale circoscriva la competenza dello Stato in materia  &#13;
 penale  all'azione  compiuta  nel suo territorio, se questa lede i suoi  &#13;
 interess.                                                                &#13;
     Tali principi si sono applicati alla nave, che è oggetto specifico  &#13;
 del dubbio prospettato dal Tribunale di Ravenna, nel senso che lo Stato  &#13;
 di immatricolazione irradia la propria potestà  sulla  medesima  anche  &#13;
 fuori  dal  limite  delle acque territoriali, qualunque sia il luogo in  &#13;
 cui essa navighi o sosti,  salva  la  potestà  dello  Stato  straniero  &#13;
 quando  la  nave  ne percorra le acque o vi trattenga.  Il collegamento  &#13;
 con lo Stato d'iscrizione svolge, quindi, la sua efficacia anche quando  &#13;
 la nave si trovi in alto mare; e l'alto mare, infatti, secondo un'altra  &#13;
 norma generalmente riconosciuta, è aperto al  libero  e  pari  uso  di  &#13;
 tutti  i  membri della comunità internazionale, in modo che ogni Stato  &#13;
 vi può  estendere  l'esercizio  della  propria  potesti  nel  rispetto  &#13;
 dell'analoga  libertà per gli altri Stati. Il predetto collegamento è  &#13;
 tanto intenso da ritenere che la nave sia parte  del  territorio  dello  &#13;
 Stato  in  cui  è  immatricolata,  volendosi affermare che questo vi i  &#13;
 valere la propria autorità come sul proprio territorio  (art.  4  Cod.  &#13;
 penale,  e  art. 4 Cod. nav.); e se, in tempi recenti, si è contestato  &#13;
 il  valore  dogmatico  di  tale  assimilazione,  non  si  è  oppugnata  &#13;
 l'esistenza della norma che l'assimilazione intende esprimere. La quale  &#13;
 spiega  il  suo significato e ha la sua giustificazione soltanto quando  &#13;
 la nave viene a trovarsi fuori del mare sul quale impera esclusivamente  &#13;
 lo Stato di cui essa ha la nazionalità;  essendo  ovvio  che,  ove  in  &#13;
 questo  mare  la  nave  sosti,  non  vi  è  ragione  di  ricercarne la  &#13;
 condizione giuridica, né v'è motivo di indagare sul  trattamento  dei  &#13;
 fatti  e  degli  atti  che  vi  si  compiono  a  bordo, perché il mare  &#13;
 territoriale è una continuazione della terraferma. E quella  norma  ha  &#13;
 un'efficacia  tanto  estesa, non soltanto da includere la possibilità,  &#13;
 da parte  dello  Stato,  di  esplicare  una  propria  potestà  per  il  &#13;
 regolamento dell'attività che si svolge a bordo della nave nazionale e  &#13;
 per  la determinazione degli effetti di questa attività, ma, altresì,  &#13;
 da legittimare una competenza punitiva dello Stato per i reati  che  si  &#13;
 consumano sulla nave stessa.                                             &#13;
     3.  - Poteva, pertanto, la legge 13 dicembre 1956, n. 1409, come ha  &#13;
 fatto, non soltanto imporre l'obbligo del manifesto di  carico  per  le  &#13;
 navi  nazionali  di  stazza netta non superiore alle 200 tonnellate che  &#13;
 trasportino tabacchi  fuori  della  zona  di  vigilanza  doganale,  ma,  &#13;
 altresì,  comminare sanzioni penali per la infrazione a tale dovere, a  &#13;
 tutela dell'interesse finanziario dello Stato.                           &#13;
     L'opportunità delle norme impugnate,  che  sembra  contestata  dal  &#13;
 Tribunale  di  Ravenna,  non  è  suscettibile  di  formare  oggetto di  &#13;
 controllo da parte di questa Corte.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     respinta l'eccezione pregiudiziale  dedotta  dall'Avvocatura  dello  &#13;
 Stato:                                                                   &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 1 e 3 della legge 13 dicembre 1956, n. 1409, in riferimento  &#13;
 all'art. 10 della Costituzione.                                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 5 dicembre 1961.                              &#13;
                                   GIUSEPPE  CAPPI - GASPARE AMBROSINI -  &#13;
                                   MARIO COSATTI  -  FRANCESCO  PANTALEO  &#13;
                                   GABRIELI - GIUSEPPE CASTELLI AVOLIO -  &#13;
                                   ANTONINO  PAPALDO  -  NICOLA JAEGER -  &#13;
                                   GIOVANNI    CASSANDRO    -     BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI GIUSEPPE  BRANCA  -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
