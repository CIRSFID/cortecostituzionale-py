<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>25</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:25</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Edoardo Volterra</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/02/1974</data_decisione>
    <data_deposito>13/02/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ- Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2237, secondo  &#13;
 e  terzo comma, del codice civile, e dell'art. 10, secondo comma, della  &#13;
 legge 2 marzo 1949, n. 144 (Approvazione della  tariffa  degli  onorari  &#13;
 per  le prestazioni professionali dei geometri), promosso con ordinanza  &#13;
 emessa il 17 dicembre 1969 dal pretore di Postiglione nel  procedimento  &#13;
 civile vertente tra Viggiano Nicola e Carleo Nicola, iscritta al n. 159  &#13;
 del registro ordinanze 1971 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 151 del 16 giugno 1971.                                    &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  21  novembre  1973  il  Giudice  &#13;
 relatore Edoardo Volterra;                                               &#13;
     udito  il sostituto avvocato generale dello Stato Michele Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Nel corso del procedimento civile vertente tra Viggiano Nicola  &#13;
 ed il geometra Carleo Nicola, il pretore di Postiglione  con  ordinanza  &#13;
 emessa  il  17  dicembre  1969  ha  sollevato questione di legittimità  &#13;
 costituzionale degli artt. 2237, secondo  e  terzo  comma,  del  codice  &#13;
 civile  e  10,  secondo  comma,  della  legge  2  marzo  1949,  n. 144,  &#13;
 limitatamente alla parte in cui dispongono che il  professionista  può  &#13;
 recedere  dal  contratto  solo  per  giusta causa ed in modo da evitare  &#13;
 pregiudizio al cliente.                                                  &#13;
     Ritenuta la rilevanza della questione - trattandosi di  opposizione  &#13;
 a  decreto  ingiuntivo  fondata  su  eccezione  di avvenuto recesso del  &#13;
 contratto senza giusti motivi da parte del geometra Carleo - il giudice  &#13;
 a quo d'ufficio ha rilevato il contrasto  delle  norme  denunciate  con  &#13;
 l'art.  3  della  Costituzione, per la disparità di trattamento tra le  &#13;
 parti nel rapporto di prestazione d'opera intellettuale.                 &#13;
     Infatti  l'esercizio  del  potere  di recesso del professionista è  &#13;
 subordinato alla sussistenza di una "giusta causa", cioè di un  evento  &#13;
 che  renda  impossibile  la  prosecuzione  del  rapporto, richiedendosi  &#13;
 inoltre che il  recesso  stesso  sia  esercitato  in  modo  da  evitare  &#13;
 pregiudizio  al cliente, laddove questi può anticipatamente porre fine  &#13;
 ad nutum al rapporto anche per mero capriccio e,  quindi,  prescindendo  &#13;
 dalla  esistenza  o  meno  di una giusta causa e di danni eventualmente  &#13;
 cagionati  all'altra  parte,  col  semplice  rimborso,  in  favore  del  &#13;
 prestatore  d'opera, delle spese sostenute e col pagamento del compenso  &#13;
 per l'opera svolta.                                                      &#13;
     2. - L'ordinanza è stata  regolarmente  notificata,  comunicata  e  &#13;
 pubblicata nella Gazzetta Ufficiale.                                     &#13;
     È  intervenuto dinanzi alla Corte costituzionale il Presidente del  &#13;
 Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale  &#13;
 dello Stato, per chiedere che la questione  proposta  venga  dichiarata  &#13;
 infondata.                                                               &#13;
     Secondo   l'Avvocatura   il   contratto   di   prestazione  d'opera  &#13;
 intellettuale  riposa  sul  carattere  assolutamente   fiduciario   del  &#13;
 rapporto  tra  cliente  e professionista, sicché è sembrato del tutto  &#13;
 conseguenziale a tale concreta esigenza il fatto di consentire al primo  &#13;
 di risolvere  unilateralmente  il  rapporto,  anche  senza  obbligo  di  &#13;
 giustificazione alcuna, quando detto rapporto fiduciario venga comunque  &#13;
 a cessare.                                                               &#13;
     Garantendo inoltre al professionista, così come le norme impugnate  &#13;
 in  effetti contemplano, l'integrale rimborso delle spese eventualmente  &#13;
 da lui sostenute ed il pagamento  del  compenso  per  l'opera  comunque  &#13;
 prestata,   si   è   venuta  praticamente  a  coprire  ogni  eventuale  &#13;
 possibilità di pregiudizio per gli interessi del medesimo, nel caso di  &#13;
 revoca ad nutum del mandato conferitogli, dal  momento  che,  oltre  ai  &#13;
 diritti  patrimoniali  di  cui  sopra,  non sembra configurabile alcuna  &#13;
 altra pretesa giuridica del prestatore d'opera intellettuale meritevole  &#13;
 di tutela, tanto meno quella di un qualunque diritto alla  prosecuzione  &#13;
 del  rapporto  nonostante  la  dichiarata  cessazione della fiducia del  &#13;
 cliente nei suoi confronti.                                              &#13;
     Considerazioni analoghe non possono valere, invece,  per  ciò  che  &#13;
 concerne  la  posizione  contrattuale del professionista:   egli ha, è  &#13;
 vero, il  diritto  di  rifiutare,  quando  lo  ritenga,  l'accettazione  &#13;
 dell'incarico   ed   è  appunto  in  quella  sede,  nella  fase  cioè  &#13;
 preliminare alla  conclusione  del  contratto,  che  l'indubbio  potere  &#13;
 discrezionale  di  scelta,  che  anche  al  professionista  deve essere  &#13;
 riconosciuto, ha modo di potersi attuare.                                &#13;
     Ma, quando tale scelta sia  stata  positivamente  operata  mediante  &#13;
 l'accettazione  dell'incarico, è evidente allora l'opportunità che la  &#13;
 ipotesi della  susseguente  rinuncia  venga  ad  essere  ancorata  alla  &#13;
 sussistenza di valide ed obiettive ragioni, piuttosto che alla semplice  &#13;
 volontà  discrezionale del professionista, sia perché nei riguardi di  &#13;
 quest'ultimo  non   appaiono   invocabili   quei   motivi   di   ordine  &#13;
 essenzialmente  fiduciario  che  presiedono  per  contro alla scelta da  &#13;
 parte del cliente, sia soprattutto a causa dell'assai grave e  talvolta  &#13;
 irreparabile pregiudizio agli interessi del committente che l'eventuale  &#13;
 unilaterale  abbandono del mandato da parte del professionista potrebbe  &#13;
 in effetti determinare.                                                  &#13;
     Se  dunque  non  è  contestabile  la  difformità  di  trattamento  &#13;
 riservato  dalle  norme  impugnate  alle  due  summenzionate  categorie  &#13;
 contrattuali,  sarebbe  tuttavia  da  escludere che tale difformità si  &#13;
 risolva in una violazione del precetto costituzionale  di  eguaglianza,  &#13;
 rispondendo  la  stessa  all'esigenza  di  disciplinare in modo diverso  &#13;
 situazioni giuridiche diverse.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. -  Il  giudice  a  quo  solleva  la  questione  di  legittimità  &#13;
 costituzionale  riguardo  all'art.  2237,  secondo  e  terzo comma, del  &#13;
 codice civile e all'art. 10, secondo comma, della legge 2  marzo  1949,  &#13;
 n.      144,   limitatamente  alla  parte  in  cui  dispongono  che  il  &#13;
 professionista può recedere dal contratto solo per giusta causa  e  in  &#13;
 modo   da   evitare  pregiudizio  al  cliente.  Queste  norme,  secondo  &#13;
 l'ordinanza, contrasterebbero con l'art. 3 della Costituzione in quanto  &#13;
 creerebbero una disparità di situazione del professionista rispetto al  &#13;
 cliente, il quale, invece, in base al disposto dei  medesimi  articoli,  &#13;
 potrebbe  porre  fine  ad  nutum  al  rapporto,  "anche",  come afferma  &#13;
 l'ordinanza, "per mero capriccio", prescindendo dall'esistenza  o  meno  &#13;
 di una giusta causa e di danni eventualmente cagionati all'altra parte,  &#13;
 col  semplice  rimborso,  in favore del prestatore d'opera, delle spese  &#13;
 sostenute e col pagamento del compenso per l'opera svolta.               &#13;
     2. - La questione è infondata. Le disposizioni denunciate non sono  &#13;
 in contrasto con il principio di  uguaglianza  consacrato  nell'art.  3  &#13;
 della  Costituzione in quanto regolano razionalmente situazioni diverse  &#13;
 che necessariamente  sorgono  dalla  natura  stessa  del  contratto  di  &#13;
 prestazione d'opera intellettuale e sono a questo consequenziali.        &#13;
     Come  è  unanimemente  riconosciuto  e confermato dalla dottrina e  &#13;
 dalla giurisprudenza, tale contratto si basa sul  carattere  fiduciario  &#13;
 del  rapporto  fra  cliente e professionista. La prestazione che questo  &#13;
 ultimo è tenuto a  fornire  non  è  fungibile  e  dipende  dalla  sua  &#13;
 capacità  personale:    pertanto  è  proprio  della natura stessa del  &#13;
 contratto che al committente, il quale dubita  che  il  prestatore  dia  &#13;
 sufficiente affidamento a che la sua opera possa realizzarsi e pertanto  &#13;
 che  si  raggiunga  lo  scopo  prefisso  dal rapporto obbligatorio, sia  &#13;
 riconosciuta la facoltà di recedere unilateralmente dal  rapporto  con  &#13;
 effetto ex nunc.                                                         &#13;
     Il  diritto  di recesso unilateralmente riconosciuto al cliente non  &#13;
 crea una disparità di situazione nei confronti del prestatore d'opera,  &#13;
 ma si sostanzia in una posizione contrattuale  derivante  razionalmente  &#13;
 dalla struttura stessa del rapporto contrattuale e dalla diversa natura  &#13;
 delle relative prestazioni.                                              &#13;
     Data  la  natura  del  contratto,  è  del  tutto  razionale che il  &#13;
 prestatore d'opera non abbia diritto alla  prosecuzione  del  rapporto,  &#13;
 una  volta  che il committente abbia revocato l'incarico, così come è  &#13;
 razionale  che  il  medesimo  prestatore  d'opera  non  possa  recedere  &#13;
 discrezionalmente  dal  contratto se non per giusta causa ed in modo da  &#13;
 evitare  ogni  pregiudizio  al  cliente.  Egli,  infatti,  nella   fase  &#13;
 preliminare del contratto è libero di accettare o rifiutare l'incarico  &#13;
 offertogli  per  la fiducia che il cliente ha riposto in lui. Una volta  &#13;
 però che il prestatore d'opera abbia operato questo diritto di  scelta  &#13;
 e  si  sia  obbligato  a  compiere  l'attività  commessagli,  non può  &#13;
 sottrarsi a tale obbligo se non per validi ed obbiettivi motivi e  può  &#13;
 recedere solo se non arreca pregiudizio agli interessi del committente.  &#13;
     Non  può  pertanto  ravvisarsi  alcun contrasto dell'art. 2237 del  &#13;
 codice civile e dell'art. 10, secondo comma, della legge 2 marzo  1949,  &#13;
 n.  144,  con l'art. 3 della Costituzione in quanto la disparità delle  &#13;
 situazioni e del trattamento del committente e del  prestatore  d'opera  &#13;
 contemplate nell'articolo impugnato è del tutto razionale e risponde a  &#13;
 imprescindibili esigenze oggettive.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 2237 del codice civile e dell'art. 10, secondo  comma,  della  &#13;
 legge  2  marzo  1949, n. 144 (Approvazione della tariffa degli onorari  &#13;
 per le prestazioni professionali dei geometri), in riferimento all'art.  &#13;
 3  della  Costituzione,  sollevata  dall'ordinanza   del   pretore   di  &#13;
 Postiglione.                                                             &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 5 febbraio 1974.                              &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - LUIGI OGGIONI - ANGELO DE  MARCO  -  &#13;
                                   ERCOLE  ROCCHETTI  - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
