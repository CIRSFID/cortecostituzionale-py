<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>297</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:297</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/06/1995</data_decisione>
    <data_deposito>05/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art.  15,  comma  1,    &#13;
 lettera  e), della legge 19 marzo 1990, n. 55 (Nuove disposizioni per    &#13;
 la prevenzione della delinquenza di tipo mafioso  e  di  altre  gravi    &#13;
 forme  di  manifestazione  di pericolosità sociale), come sostituito    &#13;
 dall'art. 1 della legge 18 gennaio 1992, n. 16 (Norme in  materia  di    &#13;
 elezioni  e nomine presso le regioni e gli enti locali), promosso con    &#13;
 ordinanza emessa il 10  ottobre  1994  dal  Tribunale  di  Patti  nel    &#13;
 procedimento  civile  vertente  tra  Milio Luciano e Sindoni Vincenzo    &#13;
 Roberto, iscritta al n. 709 del registro ordinanze 1994 e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  49,  prima  serie    &#13;
 speciale, dell'anno 1994;                                                &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 31 maggio 1995 il Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto che, con ordinanza del 10 ottobre 1994, il  Tribunale  di    &#13;
 Patti ha sollevato questione di legittimità costituzionale dell'art.    &#13;
 15,  comma  1,  lettera  e),  della  legge 19 marzo 1990, n. 55, come    &#13;
 sostituito dall'art. 1 della legge 18 gennaio  1992,  n.  16,  "nella    &#13;
 parte   in   cui   riferisce   la   incandidabilità  e,  quindi,  la    &#13;
 ineleggibilità  anche  alla  condotta  di  detenzione  di   sostanza    &#13;
 stupefacente  come  regolamentata a seguito del d.P.R. 5 giugno 1993,    &#13;
 n. 171";                                                                 &#13;
      che il giudice remittente premette che il ricorrente ha  chiesto    &#13;
 l'annullamento  dell'elezione  a Sindaco del Comune di Capo d'Orlando    &#13;
 di Sindoni Vincenzo Roberto, rinviato  a  giudizio  con  decreto  del    &#13;
 giudice  per le indagini preliminari presso il Tribunale di Patti del    &#13;
 18 febbraio 1993 quale imputato del reato di detenzione di  g.  0,286    &#13;
 di  cocaina,  previsto  dall'art.  73, comma 1, del d.P.R. n. 309 del    &#13;
 1990;                                                                    &#13;
      che il giudice a quo rileva che, a seguito del d.P.R.  5  giugno    &#13;
 1993,  n. 171 - emesso in forza del risultato positivo del referendum    &#13;
 abrogativo  del  18/19  aprile  1993  -,  la  fattispecie  di   reato    &#13;
 contestata  al  Sindoni continua ad assumere rilevanza penale solo in    &#13;
 presenza  di  destinazione  a  terzi   della   sostanza   (è   stata    &#13;
 praticamente  depenalizzata  la  detenzione  per  uso personale): per    &#13;
 altro verso, però, essendo inibito al giudice dell'azione elettorale    &#13;
 l'accertamento  anche  incidentale  della   ipotesi   di   cui   alla    &#13;
 contestazione,    dovrebbe   pervenirsi   ad   una   statuizione   di    &#13;
 ineleggibilità per fatti la cui rilevanza penale è ormai  più  che    &#13;
 dubbia  anche  sulla  scorta  della  sola  formulazione  del  capo di    &#13;
 imputazione;                                                             &#13;
      che, pertanto, si appalesa di  dubbia  legittimità  l'art.  15,    &#13;
 comma 1, lett. e), della legge citata, nella parte in cui sancisce la    &#13;
 incandidabilità,  con conseguente nullità della elezione, di coloro    &#13;
 che siano stati solo rinviati a giudizio per il reato di cui all'art.    &#13;
 73  del  d.P.R.  n.  309  del  1990,  ben  potendosi  configurare una    &#13;
 situazione sostanziale di detenzione  per  uso  personale  -  perciò    &#13;
 depenalizzata - non accertabile dal giudice dell'azione elettorale;      &#13;
      che,  in particolare, ad avviso del remittente, la norma si pone    &#13;
 in contrasto: a)  con  l'art.  3  della  Costituzione,  venendosi  ad    &#13;
 equiparare,  in  difetto  di  un  effettivo  potere  di  valutazione,    &#13;
 posizioni  molto  diverse,  quali  quella  dello  spacciatore  e  del    &#13;
 detentore  ad  uso  personale,  il  quale  abbia  subito  l'esercizio    &#13;
 dell'azione penale sulla scorta della normativa previgente (e  quindi    &#13;
 una  condotta penalmente rilevante ed una che non lo è più); b) con    &#13;
 l'art.  51  della  Costituzione,  in  quanto  nel  caso   di   specie    &#13;
 l'applicazione  della  norma porterebbe a statuire la ineleggibilità    &#13;
 anche in ipotesi di assenza di una preclusione  legislativa,  assenza    &#13;
 che  sarebbe  certamente  tale  ove  il  giudice penale ritenesse (in    &#13;
 mancanza di elementi indiziari a supporto della destinazione a terzi)    &#13;
 l'avvenuta depenalizzazione del fatto;                                   &#13;
      che è intervenuto in giudizio il Presidente del  Consiglio  dei    &#13;
 Ministri, concludendo per l'infondatezza della questione;                &#13;
    Considerato  che  il  Tribunale di Patti dubita della legittimità    &#13;
 costituzionale, in riferimento agli artt. 3 e 51 della  Costituzione,    &#13;
 dell'art.  15, comma 1, lettera e), della legge 19 marzo 1990, n. 55,    &#13;
 come sostituito dall'art. 1 della legge 18 gennaio 1992, n. 16, nella    &#13;
 parte in  cui  sancisce  la  "non  candidabilità"  (con  conseguente    &#13;
 nullità dell'eventuale elezione) ad una serie di cariche elettive di    &#13;
 coloro  che  siano  stati  rinviati  a  giudizio  per  il  delitto di    &#13;
 detenzione di sostanza stupefacente (previsto dall'art. 73 del  testo    &#13;
 unico  approvato  con  d.P.R.  9  ottobre  1990, n. 309), pur potendo    &#13;
 trattarsi, nella fattispecie, di  detenzione  per  uso  personale,  e    &#13;
 cioè  di  una condotta che, successivamente al rinvio a giudizio, è    &#13;
 stata depenalizzata ad opera del d.P.R. 5 giugno 1993, n. 171, ma  il    &#13;
 cui accertamento è riservato al giudice penale;                         &#13;
      che  tale  disciplina  viola,  ad  avviso  del  remittente,  gli    &#13;
 indicati  parametri  costituzionali,  in   quanto   irragionevolmente    &#13;
 equipara,  in  materia  di elettorato passivo, due posizioni - quella    &#13;
 del detentore per uso personale e quella dello spacciatore -  di  cui    &#13;
 la  prima  non  è  più penalmente rilevante e potrebbe, in ipotesi,    &#13;
 essere quella ricorrente nel caso di specie;                             &#13;
      che,  rispetto  alla  decisione   in   ordine   alla   questione    &#13;
 particolare   sollevata   dal  remittente,  si  rivela  pregiudiziale    &#13;
 valutare  la  legittimità  costituzionale  della  norma   la   quale    &#13;
 stabilisce  in generale la "non candidabilità" a cariche elettive di    &#13;
 coloro per i quali sia stato disposto  il  giudizio  per  determinati    &#13;
 delitti;                                                                 &#13;
      che   il  dubbio  di  legittimità  costituzionale  della  norma    &#13;
 anzidetta va posto in riferimento all'art. 27, secondo  comma,  della    &#13;
 Costituzione,  secondo  cui  l'imputato  non è considerato colpevole    &#13;
 sino alla condanna definitiva, nonché agli artt. 2, 3  e  51,  primo    &#13;
 comma,   della  Costituzione,  in  base  ai  quali  è  demandata  al    &#13;
 legislatore la disciplina, nel rispetto del principio di  eguaglianza    &#13;
 e di ragionevolezza, del diritto fondamentale di accesso alle cariche    &#13;
 elettive;                                                                &#13;
     che,  in conclusione, è necessario sollevare incidentalmente, in    &#13;
 riferimento agli artt. 2, 3, 27, secondo comma, e  51,  primo  comma,    &#13;
 della  Costituzione,  la  questione  di  legittimità  costituzionale    &#13;
 dell'art. 15, comma 1, lettera e), della legge n. 55 del  1990,  come    &#13;
 sostituito dall'art. 1 della legge n. 16 del 1992, nella parte in cui    &#13;
 prevede la "non candidabilità" alle elezioni regionali, provinciali,    &#13;
 comunali  e  circoscrizionali  di coloro per i quali, in relazione ai    &#13;
 delitti indicati nella precedente lettera a), è  stato  disposto  il    &#13;
 giudizio,  ovvero  che  sono stati presentati o citati a comparire in    &#13;
 udienza per il giudizio.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dispone  la  trattazione  innanzi  a   sé   della   questione   di    &#13;
 legittimità  costituzionale dell'art. 15, comma 1, lettera e), della    &#13;
 legge 19 marzo 1990, n. 55 (Nuove  disposizioni  per  la  prevenzione    &#13;
 della  delinquenza  di  tipo  mafioso  e  di  altre  gravi  forme  di    &#13;
 manifestazione di pericolosità sociale), come sostituito dall'art. 1    &#13;
 della legge 18 gennaio 1992, n. 16 (Norme in materia  di  elezioni  e    &#13;
 nomine  presso  le  regioni  e  gli  enti locali), nella parte in cui    &#13;
 prevede la "non candidabilità" alle elezioni regionali, provinciali,    &#13;
 comunali e circoscrizionali di coloro per i quali,  in  relazione  ai    &#13;
 delitti  indicati  nella  precedente lettera a), è stato disposto il    &#13;
 giudizio, ovvero che sono stati presentati o citati  a  comparire  in    &#13;
 udienza  per il giudizio, in riferimento agli artt. 2, 3, 27, secondo    &#13;
 comma, e 51, primo comma, della Costituzione;                            &#13;
    Ordina il rinvio del presente  giudizio,  per  poter  trattare  la    &#13;
 questione relativa congiuntamente a quella di cui al capo precedente;    &#13;
    Ordina che la cancelleria provveda agli adempimenti di legge;         &#13;
    Ordina  che  la  presente  ordinanza sia pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica.                                              &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 26 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 5 luglio 1995.                           &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
