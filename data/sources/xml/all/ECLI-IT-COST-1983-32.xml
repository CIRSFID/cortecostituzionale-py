<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>32</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:32</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Arnaldo Maccarone</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/01/1983</data_decisione>
    <data_deposito>22/02/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - &#13;
 Prof. VIRGILIO ANDRIOLI &#13;
 - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO &#13;
 - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti di legittimità costituzionale degli artt. 7,  &#13;
 8, 92 e 98 del d.P.R. 29 settembre 1973,  n.  602  (Disposizioni  sulla  &#13;
 riscossione  delle  imposte  sul  reddito) e dell'art. 10, n. 11, della  &#13;
 legge delega 9 ottobre 1971, n. 825, promossi con le  ordinanze  emesse  &#13;
 il  20  settembre  1979  dalla  Commissione  tributaria di 2   grado di  &#13;
 Cagliari, il 21 maggio e il 12 marzo 1979 dalla Commissione  tributaria  &#13;
 di  1    grado  di  Busto Arsizio, il 5 novembre 1979 dalla Commissione  &#13;
 tributaria di 2   grado di  Alessandria,  il  16  febbraio  1979  dalla  &#13;
 Commissione tributaria di 2  grado di Gorizia (quattro ordinanze), il 5  &#13;
 marzo  1980  dalla  Commissione tributaria di 2  grado di Milano, il 26  &#13;
 maggio 1980 dalla Commissione tributaria di 1  grado di Torino,  il  12  &#13;
 gennaio  1979 dalla Commissione tributaria di 1 grado di Grosseto e l'8  &#13;
 aprile 1981 ed il 14 maggio 1980  dalla  Commissione  tributaria  di  2  &#13;
 grado  di  Milano,  rispettivamente  iscritte ai nn. 98, 108, 109, 119,  &#13;
 136, 137, 138, 147 e 766 del registro ordinanze 1980, ai nn. 248,289  e  &#13;
 545  del  registro  ordinanze  1981 ed al n. 141 del registro ordinanze  &#13;
 1982 e pubblicate nella Gazzetta Ufficiale della  Repubblica  nn.  105,  &#13;
 118,  124,131  e 357 del 1980, nn. 255,262 e 318 del 1981 e n.  220 del  &#13;
 1982.                                                                    &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito   nell'udienza  pubblica  dell'1  dicembre  1982  il  Giudice  &#13;
 relatore Arnaldo Maccarone;                                              &#13;
     udito l'avvocato dello Stato Giorgio D'Amato per il Presidente  del  &#13;
 Consiglio dei ministri;                                                  &#13;
     ritenuto   che,   con  l'ordinanza  del  20  settembre  1979  della  &#13;
 Commissione tributaria di secondo grado di Cagliari, è stata sollevata  &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3 e  &#13;
 76 Cost., dell'art. 92 d.P.R. 29 settembre 1973 n.  602, secondo cui è  &#13;
 posta, a carico di chi non  esegua  entro  le  prescritte  scadenze  il  &#13;
 versamento  dei tributi dovuti, una sopratassa proporzionale ai tributi  &#13;
 stessi;                                                                  &#13;
     che  con  le  ordinanze  del  12  marzo  e  21  maggio  1979  della  &#13;
 Commissione tributaria di primo grado di Busto Arsizio, del 16 febbraio  &#13;
 1979 della Commissione tributaria di secondo grado di  Gorizia,  del  5  &#13;
 marzo 1980 e dell'8 aprile 1981 della Commissione tributaria di secondo  &#13;
 grado  di  Milano,  del  26 maggio 1980 della Commissione tributaria di  &#13;
 primo  grado  di  Torino,  sono  state  sollevate  analoghe  questioni,  &#13;
 estendendo  le  impugnazioni anche in riferimento all'art. 77 Cost. per  &#13;
 quanto riguarda le Commissioni di Milano e Torino e limitando invece il  &#13;
 riferimento al solo art. 76 Cost. per quanto riguarda la Commissione di  &#13;
 Gorizia ed al solo art. 3 Cost. per quanto riguarda la  Commissione  di  &#13;
 Busto Arsizio;                                                           &#13;
     che,  con  le  ordinanze  del  5  novembre  1979  della Commissione  &#13;
 tributaria di secondo grado di Alessandria e del 14 maggio  1980  della  &#13;
 Commissione tributaria di secondo grado di Milano le censure, formulate  &#13;
 sempre  in  riferimento  agli  artt.   3, 76 e 77 Cost., ed in funzione  &#13;
 dell'applicazione della sopratassa prevista dall'art.  92,  sono  state  &#13;
 estese anche all'art. 7 d.P.R. n. 602 del 1973 che stabilisce i termini  &#13;
 per  i  versamenti  dei  tributi  in conto corrente postale, all'art. 8  &#13;
 successivo,  che  stabilisce  i  termini  per  i   versamenti   diretti  &#13;
 all'Erario  ed  all'art. 98 che stabilisce le modalità di applicazione  &#13;
 della sopratassa;                                                        &#13;
     che,  con  l'ordinanza  del  12  gennaio  1979  della   Commissione  &#13;
 tributaria  di  primo  grado  di  Grosseto,  sempre  in un procedimento  &#13;
 concernente  l'applicazione  della  menzionata  sopratassa,  è   stata  &#13;
 sollevata  con  riferimento  agli  artt.  76  e  23 Cost., questione di  &#13;
 legittimità costituzionale dell'art. 10, n. 11 della legge di delega 9  &#13;
 ottobre 1971 n. 825, in base alla quale fu emanato il  ripetuto  d.P.R.  &#13;
 n. 602 del 1973;                                                         &#13;
     considerato  che i giudizi vengono riuniti stante la identità o la  &#13;
 stretta connessione delle sollevate questioni;                           &#13;
     che, in virtù dell'art. 9 della legge 22 dicembre 1980 n. 882,  le  &#13;
 sanzioni  amministrative previste dall'art. 92 d.P.R. 29 settembre 1973  &#13;
 n. 602 non si applicano ai contribuenti ed ai sostituti di imposta  che  &#13;
 hanno  provveduto  entro il 31 agosto 1980 al pagamento delle imposte o  &#13;
 ritenute dovute;                                                         &#13;
     che tale termine è stato ulteriormente prorogato al  30  settembre  &#13;
 1982  con l'art. 23 del D.L. 10 luglio 1982 n. 429, convertito nella l.  &#13;
 7 agosto 1982 n. 516;                                                    &#13;
     che i giudizi a quibus vertono tutti su sanzioni applicate ai sensi  &#13;
 dell'art. 92 del cit. d.P.R. n. 602 del  1973  per  la  violazione  dei  &#13;
 termini di pagamento dei tributi dovuti;                                 &#13;
     che,  di  conseguenza,  si  rende  necessario, come richiesto anche  &#13;
 dall'Avvocatura  Generale  dello  stato,  restituire  gli   atti   alle  &#13;
 Commissioni  tributarie  sopra indicate perché accertino, alla stregua  &#13;
 della sopravvenuta normativa, se le questioni sollevate  siano  tuttora  &#13;
 rilevanti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione  degli  atti  alle  Commissioni tributarie  &#13;
 indicate in epigrafe.                                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 28 gennaio 1983.                              &#13;
                                   F.to:  LEOPOLDO  ELIA  -  ANTONINO DE  &#13;
                                   STEFANO  -  GUGLIELMO   ROEHRSSEN   -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
