<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>431</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:431</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/09/1995</data_decisione>
    <data_deposito>12/09/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale del combinato disposto    &#13;
 dell'art. 37, primo  comma,  del  codice  penale  militare  di  pace,    &#13;
 unitamente  agli  artt.  223 e 224, nonché all'art. 222 dello stesso    &#13;
 codice, promossi con due ordinanze emesse il 22 e il 14 febbraio 1995    &#13;
 dal Tribunale militare di Padova nei procedimenti penali a carico  di    &#13;
 D'Ambra  Giuseppe  e  di Catanese Walter, iscritte rispettivamente ai    &#13;
 nn. 231 e 239 del registro ordinanze 1995 e pubblicate nella Gazzetta    &#13;
 Ufficiale della  Repubblica  nn.  18  e  19,  prima  serie  speciale,    &#13;
 dell'anno 1995;                                                          &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  12  luglio  1995  il  Giudice    &#13;
 relatore Francesco Guizzi.                                               &#13;
    Ritenuto  che  con  due ordinanze, emesse nel corso di altrettanti    &#13;
 procedimenti, il Tribunale militare di Padova  ha  denunciato  l'art.    &#13;
 37, primo comma, del codice penale militare di pace in relazione alle    &#13;
 disposizioni incriminatrici speciali di seguito indicate:                &#13;
      gli  artt.  223  e 224, per violazione degli artt. 3 e 103 della    &#13;
 Costituzione, nel corso del procedimento a carico di Catanese Walter,    &#13;
 soldato in congedo, già effettivo,  imputato  di  lesione  personale    &#13;
 grave  per  aver  colpito  con  un  pugno  al  volto  un  pari grado,    &#13;
 cagionandogli varie fratture e l'indebolimento  dell'organo  estetico    &#13;
 (ordinanza del 14 febbraio 1995);                                        &#13;
      l'art.  222, per lesione degli artt. 3 e 103 della Costituzione,    &#13;
 nel processo a carico del soldato D'Ambra Giuseppe imputato del reato    &#13;
 di percosse (ordinanza del 22 febbraio 1995);                            &#13;
      che le  due  ordinanze  sollevano  la  questione  del  combinato    &#13;
 disposto dell'art. 37, primo comma, in relazione agli artt. 223 e 224    &#13;
 nonché  all'art. 222, nella parte in cui esso si applica anche al di    &#13;
 fuori delle condizioni stabilite  dall'art.  5,  terzo  comma,  della    &#13;
 legge  11  luglio  1978,  n. 382 (Norme di principio sulla disciplina    &#13;
 militare);                                                               &#13;
      che secondo i giudici a quibus i reati contenuti nel  libro  II,    &#13;
 titolo  IV,  capo  III,  quali le percosse e le lesioni personali, si    &#13;
 applicano per il solo fatto che i soggetti  (attivo  e  passivo)  del    &#13;
 reato rivestano la qualità di militare;                                 &#13;
      che  le  figure criminose disegnate dalle disposizioni impugnate    &#13;
 potrebbero, dunque, definirsi "reato militare" ai  sensi  del  citato    &#13;
 art.  37,  primo  comma,  sì  che la nozione di esso peccherebbe per    &#13;
 difetto, e per eccesso;                                                  &#13;
      che detto art. 37 non ricomprenderebbe  nel  suo  ambito  né  i    &#13;
 fatti  lesivi  di interessi militari - vi esulerebbero la corruzione,    &#13;
 l'abuso d'ufficio, la concussione, il peculato d'uso e l'omicidio tra    &#13;
 pari grado - né i fatti estranei alla tutela  di  quegli  interessi,    &#13;
 onde  la  violazione  degli  artt.  3  e  103,  ultimo  comma,  della    &#13;
 Costituzione;                                                            &#13;
      che  sotto  quest'ultimo  riguardo  il  reato  militare potrebbe    &#13;
 essere definito "come violazione di legge penale posta  a  tutela  di    &#13;
 valori   afferenti   all'istituzione   militare,   esplicitamente   o    &#13;
 implicitamente garantiti dalla Carta costituzionale" (sentenza n.  81    &#13;
 del 1980);                                                               &#13;
      che,  al  contrario, sotto il primo profilo, quello dell'art. 3,    &#13;
 la   lesione   si   paleserebbe   per   l'effetto   derivante   dalla    &#13;
 qualificazione   formale   di  reato  militare,  con  un  trattamento    &#13;
 sostanziale  complessivo  (pene   principali   e   accessorie,   loro    &#13;
 esecuzione, scriminanti, attenuanti e aggravanti) del tutto peculiare    &#13;
 e  diverso rispetto a quello conseguente alla contestazione del reato    &#13;
 comune, stante la radicale diversità della  disciplina  processuale,    &#13;
 caratterizzata dalle forme del processo militare e dalla richiesta di    &#13;
 procedimento da parte del comandante di Corpo;                           &#13;
      che  vi  sarebbero, tuttavia, fatti non idonei a interferire con    &#13;
 beni o interessi militari e che, ciononostante, verrebbero sottoposti    &#13;
 alla speciale disciplina del codice penale militare;                     &#13;
      che tali sarebbero i fatti di lesione personale, come quello  in    &#13;
 esame,  commessi  per  ragioni e in situazioni estranee al servizio e    &#13;
 alla  disciplina  militare  (e,  cioè,  fatti  lesivi  di  interessi    &#13;
 "comuni"  sottratti alla loro naturale disciplina per essere, invece,    &#13;
 assoggettati a quella militare);                                         &#13;
      che  l'identico  trattamento   sanzionatorio   sarebbe   inoltre    &#13;
 comminato  per  tutti  i  fatti, quanto all'interesse leso, anche fra    &#13;
 loro eterogenei;                                                         &#13;
      che  si  paleserebbe,   in   tal   modo,   l'incostituzionalità    &#13;
 dell'impugnato  art.  37,  primo comma, in relazione all'art. 3 della    &#13;
 Costituzione, in una lesione riscontrabile pure per  tutte  le  altre    &#13;
 incriminazioni denunciate;                                               &#13;
      che  tali  disposizioni  ricomprenderebbero, invero, fatti senza    &#13;
 alcuna afferenza con le esigenze  del  servizio  e  della  disciplina    &#13;
 militare,  assoggettando il fatto "comune", o delle percosse, o delle    &#13;
 lesioni personali alle conseguenze derivanti dall'ascrizione al genus    &#13;
 del reato militare (dall'attenuante dell'ottima condotta militare  ex    &#13;
 art. 48, ultimo comma, all'aggravante del grado rivestito ex art. 47,    &#13;
 n.  2;  dalla perseguibilità sottratta alla parte lesa e affidata al    &#13;
 comandante di corpo, alla sottoposizione alla giurisdizione  militare    &#13;
 ex art. 263);                                                            &#13;
      che,  in  tal  modo,  ne discenderebbe una irrazionalità, ancor    &#13;
 più evidente con riferimento alla disposizione di  cui  all'art.  5,    &#13;
 terzo  comma,  della legge n. 382 del 1978, sul modello dell'art. 199    &#13;
 del codice penale militare,  ove  si  statuiscono  le  condizioni  di    &#13;
 applicabilità  della normativa sull'insubordinazione e sull'abuso di    &#13;
 autorità (riferimento che potrebbe restringere l'applicazione  della    &#13;
 disposizione impugnata soltanto quando i militari "svolgono attività    &#13;
 di  servizio;  si  trovano in luoghi militari o comunque destinati al    &#13;
 servizio; indossano l'uniforme; e si qualificano  come  militari,  in    &#13;
 relazione  a compiti di servizio, o si rivolgono ad altri militari in    &#13;
 divisa o vi si qualificano come tali");                                  &#13;
      che il Tribunale rimettente ha perciò sollevato, in riferimento    &#13;
 all'art. 3 della Costituzione, anche questione  di  costituzionalità    &#13;
 degli  artt. 223, 224 e 222 del codice penale militare di pace, nella    &#13;
 parte in cui si applicano  altresì  al  di  fuori  delle  condizioni    &#13;
 stabilite dall'art. 5, terzo comma, della legge n. 382 del 1978;         &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 concludendo per l'infondatezza;                                          &#13;
      che  ad  avviso  dell'Avvocatura  rientrerebbe  nella  sfera  di    &#13;
 discrezionalità riservata al legislatore prevedere (o meno) cause di    &#13;
 inapplicabilità di  determinate  fattispecie,  purché  non  vi  sia    &#13;
 violazione del criterio di ragionevolezza;                               &#13;
      che  tale censura non sarebbe però invocabile, nella ipotesi in    &#13;
 esame, non soltanto per la diversa  "obiettività  giuridica"  fra  i    &#13;
 valori posti a confronto, ma anche perché i reati militari e i reati    &#13;
 comuni   fra  loro  comparati  riceverebbero  differente  trattamento    &#13;
 sanzionatorio, a volte più grave  delle  corrispondenti  fattispecie    &#13;
 comuni.                                                                  &#13;
    Considerato  che  le  stesse  questioni, sulla base delle medesime    &#13;
 considerazioni, hanno già formato  oggetto  di  esame  da  parte  di    &#13;
 questa  Corte,  che le ha dichiarate inammissibili con la sentenza n.    &#13;
 298 del 1995;                                                            &#13;
      che le ordinanze di  rimessione  non  prospettano,  a  sostegno,    &#13;
 nuove o diverse argomentazioni;                                          &#13;
      che,  pertanto, previa trattazione congiunta dei due giudizi per    &#13;
 l'identità   delle   questioni,   va   dichiarata    la    manifesta    &#13;
 inammissibilità delle stesse;                                           &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la manifesta inammissibilità della    &#13;
 questione  di  legittimità  costituzionale  del  combinato  disposto    &#13;
 dell'art.  37,  primo  comma,  del  codice  penale  militare di pace,    &#13;
 unitamente agli artt. 223 e 224, nonché all'art.  222  dello  stesso    &#13;
 codice, sollevata dal Tribunale militare di Padova, in relazione agli    &#13;
 artt.  3  e  103  della  Costituzione,  con  le ordinanze indicate in    &#13;
 epigrafe.                                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 6 settembre 1995.                             &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 12 settembre 1995.                       &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
