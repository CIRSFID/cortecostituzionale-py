<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>397</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:397</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/12/1996</data_decisione>
    <data_deposito>16/12/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSK Y, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 27 della legge    &#13;
 23 luglio 1991, n. 223  (Norme  in  materia  di  cassa  integrazione,    &#13;
 mobilità,  trattamenti di disoccupazione, attuazione delle direttive    &#13;
 della Comunità europea, avviamento al lavoro ed  altre  disposizioni    &#13;
 in  materia  di mercato del lavoro), promosso con ordinanza emessa il    &#13;
 27 aprile 1996 dal pretore di Pisa  sul  ricorso  proposto  da  Conti    &#13;
 Gabriella  contro  l'INPS,  iscritta al n. 775 del registro ordinanze    &#13;
 1996 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  36,    &#13;
 prima serie speciale, dell'anno 1996;                                    &#13;
   Visti gli atti di costituzione di Conti Gabriella e dell'INPS;         &#13;
   Udito  nella  camera  di  consiglio del 27 novembre 1996 il giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
   Ritenuto che nel corso di un giudizio promosso da  Conti  Gabriella    &#13;
 nei  confronti dell'INPS, il pretore di Pisa, con ordinanza emessa il    &#13;
 27 aprile 1996, ha sollevato, in riferimento agli artt. 3 e 37  della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art.  27    &#13;
 della legge 23 luglio  1991,  n.  223  (Norme  in  materia  di  cassa    &#13;
 integrazione,  mobilità,  trattamenti  di disoccupazione, attuazione    &#13;
 delle direttive della Comunità  europea,  avviamento  al  lavoro  ed    &#13;
 altre  disposizioni in materia di mercato del lavoro), nella parte in    &#13;
 cui non prevede, nell'ipotesi di prepensionamento  delle  lavoratrici    &#13;
 del  settore  siderurgico,  che  queste  possano  godere dello stesso    &#13;
 accredito di anzianità contributiva stabilito per i lavoratori dello    &#13;
 stesso settore;                                                          &#13;
     che, a parere del giudice a quo,  il  mancato  riconoscimento  in    &#13;
 favore  della ricorrente dello stesso accredito contributivo previsto    &#13;
 per  i  lavoratori  maschi  dello  stesso  settore  si   risolverebbe    &#13;
 nell'omessa  considerazione  da  parte  dell'INPS  di quelle pronunce    &#13;
 della Corte costituzionale che, con riguardo a casi analoghi, avevano    &#13;
 riconosciuto alla lavoratrice il diritto  a  conseguire  la  medesima    &#13;
 anzianità contributiva prevista per i lavoratori;                       &#13;
     che   nel  giudizio  davanti  alla  Corte  costituzionale  si  è    &#13;
 costituita la parte privata chiedendo l'emissione di un provvedimento    &#13;
 che non si discosti da quanto già in precedenza affermato da  questa    &#13;
 Corte;                                                                   &#13;
     che  nel  giudizio  si  è  anche costituito l'INPS rilevando che    &#13;
 identica questione risulta essere stata già decisa  con  le  recenti    &#13;
 ordinanze  nn.  192  e  308  del 1996 e concludendo, pertanto, per la    &#13;
 declaratoria di manifesta infondatezza della questione;                  &#13;
   Considerato che questa Corte, con sentenza  n.  64  del  1996,  nel    &#13;
 decidere   analoga   questione  di  legittimità  costituzionale,  ha    &#13;
 ritenuto che l'art. 8 del d.-l. 16 maggio 1994, n. 299, convertito in    &#13;
 legge, con modificazioni, dall'art. 1 della legge 19 luglio 1994,  n.    &#13;
 451,   prevedente   un   piano  di  prepensionamento  dei  lavoratori    &#13;
 siderurgici di età non inferiore  a  cinquant'anni  se  uomini  e  a    &#13;
 quarantasette  se  donne, deve essere interpretato nel senso che esso    &#13;
 attribuisce ai dipendenti medesimi una maggiorazione  dell'anzianità    &#13;
 contributiva  fissata,  per  entrambi  i sessi, nella misura di dieci    &#13;
 anni;                                                                    &#13;
     che  successivamente  identica  questione  è  stata   dichiarata    &#13;
 manifestamente infondata, da ultimo, con ordinanza n. 308 del 1996;      &#13;
     che  non  essendo  stati  prospettati ulteriori e nuovi motivi di    &#13;
 censura,  la  questione  relativa  alle   lavoratrici   del   settore    &#13;
 siderurgico deve essere dichiarata manifestamente infondata;             &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i  giudizi  avanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 27 della legge 23 luglio 1991, n. 223 (Norme    &#13;
 in  materia  di  cassa  integrazione,   mobilità,   trattamenti   di    &#13;
 disoccupazione,  attuazione  delle direttive della Comunità europea,    &#13;
 avviamento al lavoro ed altre disposizioni in materia di mercato  del    &#13;
 lavoro),   sollevata,   in  riferimento  agli  artt.  3  e  37  della    &#13;
 Costituzione,  dal  pretore  di  Pisa  con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 dicembre 1996.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 16 dicembre 1996.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
