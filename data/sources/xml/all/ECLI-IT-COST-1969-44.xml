<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>44</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:44</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Enzo Capolozza</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/03/1969</data_decisione>
    <data_deposito>21/03/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ- Dott. GIOVANNI &#13;
 BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi riuniti di legittimità costituzionale dell'art.  150,  &#13;
 secondo comma, del decreto del Presidente della Repubblica  29  gennaio  &#13;
 1958, n. 645 (T.U. delle leggi sulle imposte dirette), promossi con due  &#13;
 ordinanze  emesse il 5 aprile 1967 dalla Commissione distrettuale delle  &#13;
 imposte di Napoli sui ricorsi della società S.A.R.A. contro  l'Ufficio  &#13;
 delle  imposte  di  Napoli,  iscritte  ai  nn.  151  e 152 del Registro  &#13;
 ordinanze 1967 e pubblicate nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 221 del 2 settembre 1967.                                             &#13;
     Visti   gli   atti   di  costituzione  della  società  S.A.R.A.  e  &#13;
 d'intervento del Presidente del Consiglio dei Ministri;                  &#13;
     udita nell'udienza pubblica del 26 febbraio 1969 la  relazione  del  &#13;
 Giudice Enzo Capalozza;                                                  &#13;
     udito   il   sostituto   avvocato  generale  dello  Stato  Giovanni  &#13;
 Albisinni, per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     La Società acquisti, rivendite, amministrazioni S.A.R.A., con sede  &#13;
 in Napoli, in data 15 gennaio 1965, inoltrò  ricorso  alla  Commissione  &#13;
 distrettuale  delle  imposte  di  quella  città  contro l'accertamento  &#13;
 dell'ufficio finanziario che, in sede di rettifica della  denunzia  per  &#13;
 l'esercizio  del  1960, ai fini dell'imposta sulle società, aveva, fra  &#13;
 l'altro, elevato  a  L.  30  milioni  534.696  il  reddito  imponibile,  &#13;
 comprendendovi  quello  dei  fabbricati,  nella  somma risultante dalla  &#13;
 relativa imposta, ai sensi degli artt.  148, lett. c,  e  150,  secondo  &#13;
 comma,  del decreto del Presidente della Repubblica 29 gennaio 1958, n.  &#13;
 645 (T.U.  delle leggi sulle imposte dirette).                           &#13;
     Nel ricorso si sollevava questione di  legittimità  costituzionale  &#13;
 delle  norme  su  citate,  in  riferimento  agli  artt.  53 e 113 della  &#13;
 Costituzione, e l'adita Commissione, con ordinanza del 5  aprile  1967,  &#13;
 la  riteneva rilevante e non manifestamente infondata, limitatamente al  &#13;
 secondo  comma  dell'art.  150  in  riferimento   all'art.   53   della  &#13;
 Costituzione.                                                            &#13;
     Un'ordinanza  di identico contenuto è  stata, in pari data, emessa  &#13;
 dalla Commissione distrettuale di  Napoli  su  ricorso  della  medesima  &#13;
 Società  contro  l'accertamento  relativo  all'esercizio  del 1961 nel  &#13;
 quale, in sede di rettifica, l'ufficio  finanziario  aveva  elevato  il  &#13;
 reddito imponibile a L. 30.517.755.                                      &#13;
     Le  ordinanze,  ritualmente  notificate  e  comunicate,  sono state  &#13;
 pubblicate nella Gazzetta Ufficiale n. 221 del 2 settembre 1967.         &#13;
     Nel primo giudizio innanzi a  questa  Corte  si  è  costituita  la  &#13;
 società  S.A.R.A.  con  deduzioni depositate il 19 settembre 1967 e ha  &#13;
 chiesto la dichiarazione d'illegittimità della norma denunziata.        &#13;
     In entrambi i giudizi è  intervenuto il Presidente  del  Consiglio  &#13;
 dei  Ministri,  rappresentato  e  difeso dall'Avvocatura generale dello  &#13;
 Stato, con atto depositato il 21 settembre 1967 ed ha  chiesto,  a  sua  &#13;
 volta, che la questione sia dichiarata infondata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Le due ordinanze, di identico contenuto, propongono la stessa  &#13;
 questione  di  legittimità  costituzionale,  e  pertanto  i   relativi  &#13;
 giudizi, congiuntamente discussi nella pubblica udienza, possono essere  &#13;
 riuniti e decisi con un unica sentenza.                                  &#13;
     2.  -  La  Commissione  distrettuale  delle  imposte  di  Napoli ha  &#13;
 promosso giudizio di legittimità costituzionale della norma  contenuta  &#13;
 nell'art.   150,  secondo  comma,  del  decreto  del  Presidente  della  &#13;
 Repubblica 29 gennaio 1958, n. 645  (T.U.  delle  leggi  sulle  imposte  &#13;
 dirette),  in  riferimento  agli artt. 53 e 113 della Costituzione, nel  &#13;
 presupposto, accolto dalla giurisprudenza di questa Corte (sentenze nn.  &#13;
 12, 41 e 42 del 1957, n. 132 del 1963 e n.   103 del 1964),  di  essere  &#13;
 legittimata a proporre questioni di legittimità costituzionale.         &#13;
     Con  recente  sentenza n. 10 del 30 gennaio 1969, questa Corte - in  &#13;
 conformità alla sua precedente decisione n. 6  dello  stesso  anno  in  &#13;
 tema  di  commissioni comunali per i tributi locali - ha escluso che le  &#13;
 commissioni per i tributi  erariali  possano  essere  configurate  come  &#13;
 organi giurisdizionali.                                                  &#13;
     Per  quanto,  in particolare, concerne le commissioni distrettuali,  &#13;
 la  citata  sentenza  n.  10  del  1969  ne  ha  desunto  il  carattere  &#13;
 amministrativo   della   disciplina   relativa  alla  composizione  del  &#13;
 collegio, nonché ai poteri ad esso attribuiti.                          &#13;
     3. - La  questione  sollevata  deve,  pertanto,  essere  dichiarata  &#13;
 inammissibile  per difetto dei presupposti richiesti dal l'art. 1 della  &#13;
 legge costituzionale n. 1 del 1948.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 dell'art.   150,  secondo  comma,  del  decreto  del  Presidente  della  &#13;
 Repubblica 29 gennaio  1958,  n.  645  (T.U.  delle  imposte  dirette),  &#13;
 sollevata, in riferimento agli artt. 53 e 113 della Costituzione, dalla  &#13;
 Commissione  distrettuale  delle  imposte  di  Napoli  con le ordinanze  &#13;
 indicate in epigrafe.                                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 13 marzo 1969.                                &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ- GIOVANNI BATTISTA BENEDETTI -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE.</dispositivo>
  </pronuncia_testo>
</pronuncia>
