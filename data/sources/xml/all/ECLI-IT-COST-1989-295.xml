<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>295</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:295</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/05/1989</data_decisione>
    <data_deposito>25/05/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 648 del codice    &#13;
 di procedura civile, promosso con ordinanza emessa il 17 ottobre 1988    &#13;
 dal  Pretore  di La Spezia nel procedimento civile vertente tra Paita    &#13;
 Dante e l'Impresa ICEA, iscritta al n. 12 del registro ordinanze 1989    &#13;
 e  pubblicata  nella  Gazzetta  Ufficiale della Repubblica n. 5 prima    &#13;
 serie speciale dell'anno 1989;                                           &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 12 aprile 1989 il Giudice    &#13;
 relatore Aldo Corasaniti;                                                &#13;
    Ritenuto  che  nel  corso  di  un  procedimento  avente ad oggetto    &#13;
 l'opposizione al decreto ingiuntivo,  il  Pretore  di  La  Spezia,  a    &#13;
 seguito   della   istanza   di   provvisoria  esecutorietà  avanzata    &#13;
 dall'opposto Umberto Calvosa sul rilievo che l'opposizione medesima -    &#13;
 promossa  da  Dante  Paita,  che tra l'altro eccepiva l'inadempimento    &#13;
 della controparte - non era fondata su prova  scritta,  ha  sollevato    &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3    &#13;
 e 24 della Costituzione, dell'art. 648 (primo comma) c.p.c.;             &#13;
      che,  ad  avviso  del  giudice  a  quo  la  norma censurata, non    &#13;
 richiedendo, al fine della concessione della  provvisoria  esecuzione    &#13;
 del  decreto  ingiuntivo, in caso di opposizione non fondata su prova    &#13;
 scritta,  la  sussistenza  del  pericolo  nel  ritardo,  si  pone  in    &#13;
 contrasto  con  gli artt. 24 e 3, primo comma, della Costituzione, in    &#13;
 quanto oblitera  il  giusto  equilibrio  -  osservato  perfino  nella    &#13;
 disciplina  dell'ipotesi  di esecuzione provvisoria della sentenza di    &#13;
 primo grado (art. 282 c.p.c.) - fra diritto di  agire  e  diritto  di    &#13;
 difendersi in giudizio;                                                  &#13;
      che, sempre ad avviso del giudice a quo, la norma censurata, con    &#13;
 il  sancire  una  attenuazione   dell'onere   probatorio   a   favore    &#13;
 dell'istante  opposto,  si  pone  in  contrasto con gli artt. 24 e 3,    &#13;
 secondo (recte primo) comma, in quanto siffatta attenuazione  non  ha    &#13;
 più ragion d'essere in presenza dell'opposizione, sia questa fondata    &#13;
 su prova scritta o no;                                                   &#13;
      che  è intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 ministri, rappresentato e difeso dall'Avvocatura dello Stato, che  ha    &#13;
 eccepito  l'inammissibilità  -  per  non  avere spiegato l'autorità    &#13;
 remittente  perché,  nel  giudizio  a  quo,  avrebbe  dovuto  essere    &#13;
 necessariamente  concessa  l'esecuzione  provvisoria del decreto - e,    &#13;
 nel merito, l'infondatezza della questione;                              &#13;
    Considerato,  quanto  al  primo  profilo,  che la discrezionalità    &#13;
 attribuita al giudice istruttore dalla norma impugnata ai fini  della    &#13;
 concessione  della  provvisoria esecuzione in caso di opposizione non    &#13;
 fondata su prova scritta, deve ovviamente essere esercitata, come  in    &#13;
 ogni ipotesi di misura avente (anche) natura cautelare, attraverso la    &#13;
 congiunta valutazione del fumus boni juris e del periculum  in  mora,    &#13;
 elemento  che,  dunque,  è  richiesto  nell'ipotesi di cui si tratta    &#13;
 (cfr. sentenza n. 137 del 1984 in motivazione);                          &#13;
      che,  quanto  al  secondo profilo, la denunciata attenuazione di    &#13;
 onere  probatorio,  sia  pure  ai  fini   della   concessione   della    &#13;
 provvisoria  esecuzione,  non  sussiste,  perché  in  ogni  caso  la    &#13;
 valutazione del fumus boni juris va operata anche nei confronti della    &#13;
 prova  dedotta  dall'istante  opposto a base della domanda di decreto    &#13;
 ingiuntivo (cfr. sent. n. 137 del 1984 in motivazione);                  &#13;
      che   pertanto   la   questione   va  dichiarata  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale, in riferimento agli artt. 3 e 24 della  Costituzione,    &#13;
 dell'art.  648  del codice di procedura civile, sollevata dal Pretore    &#13;
 di La Spezia con l'ordinanza indicata in epigrafe.                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 17 maggio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CORASANITI                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 25 maggio 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
