<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>110</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:110</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia>Massimo Vari</redattore_pronuncia>
    <data_decisione>10/04/2002</data_decisione>
    <data_deposito>12/04/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Valerio ONIDA, Carlo &#13;
MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità costituzionale degli artt. 1 e 5 della &#13;
legge   25 giugno   1999,   n. 205   (Delega   al   Governo   per  la &#13;
depenalizzazione  dei  reati  minori  e modifiche al sistema penale e &#13;
tributario),  e  19  del decreto legislativo 30 dicembre 1999, n. 507 &#13;
(Depenalizzazione   dei   reati   minori   e   riforma   del  sistema &#13;
sanzionatorio,  ai  sensi  dell'art. 1  della  legge  25 giugno 1999, &#13;
n. 205),  promossi,  con  ordinanze  emesse l'8 e il 1 marzo 2001, il &#13;
5 giugno  e  il  3 luglio  2001  (n. 5  ordinanze),  dal tribunale di &#13;
Firenze,  rispettivamente iscritte ai numeri 489, 490, 730, 781, 782, &#13;
783,  784  e  785  del  registro  ordinanze  2001  e pubblicate nella &#13;
Gazzetta  Ufficiale  della  Repubblica  numeri 25,  39 e 40, 1ª serie &#13;
speciale, dell'anno 2001; &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 30 gennaio 2002 il giudice &#13;
relatore Massimo Vari. &#13;
    Ritenuto  che,  con  otto ordinanze, emesse in data 1 marzo 2001, &#13;
8 marzo  2001,  5 giugno  2001  e  3 luglio 2001 (n. 5 ordinanze), il &#13;
tribunale  di  Firenze ha sollevato, con riferimento all'art. 3 della &#13;
Costituzione,  questione di legittimità costituzionale degli artt. 1 &#13;
e  5  della  legge  25 giugno  1999, n. 205 (Delega al Governo per la &#13;
depenalizzazione  dei  reati  minori  e modifiche al sistema penale e &#13;
tributario), e dell'art. 19 del decreto legislativo 30 dicembre 1999, &#13;
n. 507  (Depenalizzazione  dei  reati  minori  e  riforma del sistema &#13;
sanzionatorio,  ai  sensi  dell'art. 1  della  legge  25 giugno 1999, &#13;
n. 205); &#13;
        che le disposizioni sopra indicate sono denunciate, da talune &#13;
delle  ordinanze  in  epigrafe,  nella  parte in cui non prevedono la &#13;
depenalizzazione  del reato previsto e punito dall'art. 186, comma 2, &#13;
del  decreto  legislativo  30 aprile 1992, n. 285 (Nuovo codice della &#13;
strada), limitatamente al comportamento di chi, conseguita la patente &#13;
di  guida,  conduce un veicolo in stato di alterazione dovuto all'uso &#13;
di  alcool  (r.o.  numeri 489,  490  del 2001) o in stato di ebbrezza &#13;
(r.o. nn. 730, 781, 784, 785 del 2001); &#13;
        che  le  medesime  disposizioni  vengono,  al  tempo  stesso, &#13;
censurate,  da  talune altre delle ordinanze in epigrafe, nella parte &#13;
in  cui non prevedono la depenalizzazione del reato previsto e punito &#13;
dall'art. 187,  comma  4,  del  codice  della  strada,  in  relazione &#13;
all'art. 186,   comma   2,  del  medesimo  codice,  limitatamente  al &#13;
comportamento  di  chi,  conseguita  la  patente di guida, conduce un &#13;
veicolo   in   stato   di  alterazione  dovuto  all'uso  di  sostanze &#13;
stupefacenti (r.o. numeri 782 e 783 del 2001); &#13;
        che, ad avviso del giudice a quo la scelta del legislatore di &#13;
depenalizzare  la  contravvenzione  di  guida  senza  patente, di cui &#13;
all'art. 116  del codice della strada, ma non le fattispecie di reato &#13;
previste  dagli artt. 186 e 187 del medesimo codice, appare del tutto &#13;
illogica  in  quanto  la  condotta  di  chi guida un'automobile senza &#13;
patente,   e   quindi  senza  esperienza,  è  più  pericolosa,  per &#13;
l'incolumità pubblica, di quella di chi, avendo dimostrato di essere &#13;
in   grado   di   condurre   un   autoveicolo  superando  l'esame  di &#13;
abilitazione,  viene  trovato in uno stato di momentanea alterazione, &#13;
della  cui  effettiva  incidenza negativa sulla capacità di guida la &#13;
legge non richiede l'accertamento; &#13;
        che,  pertanto, le norme denunciate contrasterebbero, secondo &#13;
il   giudice  a  quo  con  l'art. 3  della  Costituzione,  in  quanto &#13;
determinerebbero  una ingiustificata disparità di trattamento tra la &#13;
condotta  di  chi  guida  un  veicolo  senza  aver  mai conseguito la &#13;
patente, oggi punita con la sanzione amministrativa, e la condotta di &#13;
chi,  avendo  conseguito la patente stessa, guida un veicolo in stato &#13;
di temporanea, presunta, alterazione dovuta al consumo di alcool o di &#13;
sostanze stupefacenti; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato, &#13;
concludendo per la manifesta infondatezza della questione. &#13;
    Considerato   che  i  giudizi,  in  quanto  propongono  questioni &#13;
identiche,   o   quantomeno   analoghe,  vanno  riuniti,  per  essere &#13;
congiuntamente decisi; &#13;
        che  la  questione di legittimità costituzionale delle norme &#13;
denunciate - nella parte in cui non prevedono la depenalizzazione del &#13;
reato  previsto  e  punito  dall'art. 187,  comma 4, del codice della &#13;
strada,  in  relazione  all'art. 186,  comma  2, del medesimo codice, &#13;
limitatamente  al  comportamento  di  chi,  conseguita  la patente di &#13;
guida,  conduce  un veicolo in stato di alterazione dovuto all'uso di &#13;
sostanze  stupefacenti  -  è  stata  già  dichiarata manifestamente &#13;
infondata, da questa Corte, con ordinanza n. 144 del 2001; &#13;
        che,  nella predetta ordinanza, la Corte - dopo aver premesso &#13;
che   rientra   nella   discrezionalità  legislativa  il  potere  di &#13;
configurare  le  ipotesi  criminose  e  di depenalizzare fatti dianzi &#13;
costituenti  reato,  e,  al  tempo  stesso,  che  uno  scrutinio  che &#13;
direttamente   investa  il  merito  delle  scelte  sanzionatorie  del &#13;
legislatore  è  possibile  solo ove l'opzione normativa contrasti in &#13;
modo  manifesto  con il canone della ragionevolezza - ha ritenuto che &#13;
la  scelta del legislatore di non estendere la depenalizzazione anche &#13;
alla  fattispecie  di guida in stato di alterazione dovuto all'uso di &#13;
sostanze  stupefacenti non può ritenersi irragionevole solo a motivo &#13;
delle   valutazioni   espresse   dal   rimettente   in   ordine  alla &#13;
asserita maggiore  pericolosità  della  condotta  di chi guida senza &#13;
aver  conseguito la prescritta abilitazione rispetto a quella di chi, &#13;
pur  avendo  regolarmente  conseguito la patente, viene sorpreso alla &#13;
guida in stato di alterazione dovuto all'uso di stupefacenti; &#13;
        che  il  rimettente  non prospetta profili di censura nuovi o &#13;
diversi da quelli già esaminati dalla Corte nella citata ordinanza; &#13;
        che  le  argomentazioni  sviluppate  nella predetta ordinanza &#13;
n. 144 del 2001 appaiono estensibili, avuto riguardo alle prospettate &#13;
ragioni   di   censura,   anche   alla   questione   di  legittimità &#13;
costituzionale  delle  norme  denunciate,  nella  parte  in  cui  non &#13;
prevedono   la   depenalizzazione   del   reato   previsto  e  punito &#13;
dall'art. 186,  comma  2,  del  codice della strada, limitatamente al &#13;
comportamento  di  chi,  conseguita  la  patente di guida, conduce un &#13;
veicolo in stato di alterazione dovuto all'uso di alcool; &#13;
        che,   pertanto,   entrambe  le  questioni  devono  reputarsi &#13;
manifestamente infondate. &#13;
    Visti  gli  art. 26,  secondo  comma,  della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    Dichiara   la   manifesta   infondatezza   delle   questioni   di &#13;
legittimità  costituzionale  degli artt. 1 e 5 della legge 25 giugno &#13;
1999,  n. 205  (Delega  al  Governo per la depenalizzazione dei reati &#13;
minori  e  modifiche  al sistema penale e tributario), e dell'art. 19 &#13;
del  decreto  legislativo  30 dicembre 1999, n. 507 (Depenalizzazione &#13;
dei  reati  minori  e  riforma  del  sistema  sanzionatorio, ai sensi &#13;
dell'art. 1  della  legge  25 giugno  1999,  n. 205),  sollevate, con &#13;
riferimento  all'art. 3 della Costituzione, dal Tribunale di Firenze, &#13;
con le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 aprile 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Vari &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 12 aprile 2002. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
