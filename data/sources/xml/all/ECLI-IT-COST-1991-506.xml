<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>506</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:506</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1991</data_decisione>
    <data_deposito>30/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 62 della legge    &#13;
 della   Regione   Calabria   17   dicembre   1981,   n.   21   (Norme    &#13;
 sull'amministrazione del patrimonio e sulla contabilità delle Unità    &#13;
 Sanitarie  Locali)  promosso  con ordinanza emessa il 5 febbraio 1991    &#13;
 dalla Corte d'Appello di Milano nel procedimento civile vertente  tra    &#13;
 U.S.L.  n.  7  di  Rossano  e  S.p.a.  Upjohn  iscritta al n. 473 del    &#13;
 registro ordinanze 1991 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 28, prima serie speciale, dell'anno 1991;                  &#13;
    Udito  nella  camera  di  consiglio del 4 dicembre 1991 il Giudice    &#13;
 relatore Luigi Mengoni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di  un  giudizio  di  impugnazione  proposto  dalla    &#13;
 U.S.L.  di  Rossano  avverso  la  sentenza  di  primo grado che aveva    &#13;
 condannato l'appellante a corrispondere alla società  Upjohn,  oltre    &#13;
 agli interessi legali, un ulteriore importo a titolo di maggior danno    &#13;
 per  svalutazione  monetaria  ex  art.  1224,  secondo  comma, Codice    &#13;
 civile, la Corte d'appello di Milano, con ordinanza  del  5  febbraio    &#13;
 1991, ha sollevato questione di legittimità costituzionale dell'art.    &#13;
 62  della legge della Regione Calabria 17 dicembre 1981, n. 21, nella    &#13;
 parte in cui, nel disciplinare gli interessi  moratori  dovuti  dalle    &#13;
 USL  di  quella  Regione, dispone che "tutti gli interessi da ritardo    &#13;
 sono interessi di mora comprensivi  del  risarcimento  del  danno  ai    &#13;
 sensi dell'art. 1224, secondo comma, Codice civile".                     &#13;
    Il  giudice  remittente  dubita della conformità di tale norma al    &#13;
 dettato costituzionale in quanto:                                        &#13;
       a) l'art.  117,  primo  comma,  della  Costituzione,  che  pure    &#13;
 attribuisce   alle   regioni   potestà  legislativa  in  materia  di    &#13;
 assistenza sanitaria ed ospedaliera, non può essere interpretato  in    &#13;
 senso  così  lato  da  farvi  rientrare  anche  la  disciplina delle    &#13;
 obbligazioni   nascenti   da   rapporti   contrattuali   di    natura    &#13;
 privatistica,  instaurati  con  terzi  dalle unità sanitarie locali,    &#13;
 quasi fosse possibile creare in ogni regione delle aree  territoriali    &#13;
 di jus singulare;                                                        &#13;
       b) una disposizione così concepita non appare conciliabile col    &#13;
 principio  di  uguaglianza  di  cui  all'art.  3  della Costituzione,    &#13;
 essendo  irrazionale  l'assoggettamento  di  coloro   che   stipulano    &#13;
 identici  contratti  (di  diritto privato) con varie unità sanitarie    &#13;
 locali a una diversa disciplina, in tema di interessi debitori  e  di    &#13;
 eventuale maggior danno conseguente al ritardo nel pagamento dei loro    &#13;
 crediti,  sol perché i rispettivi debitori sono dislocati in regioni    &#13;
 diverse.<diritto>Considerato in diritto</diritto>1.  -  La Corte d'appello di Milano reputa non conforme agli artt.    &#13;
 117 e 3 della Costituzione l'art. 62, quarto comma, della legge della    &#13;
 Regione Calabria 17 dicembre 1981, n. 21, il quale, con riguardo agli    &#13;
 impegni di spesa delle unità sanitarie locali,  dispone  che  "tutti    &#13;
 gli  interessi  da  ritardo  sono  interessi  di mora comprensivi del    &#13;
 risarcimento del danno ai sensi dell'art. 1224,  secondo  comma,  del    &#13;
 codice civile".                                                          &#13;
    2. - La questione è fondata.                                         &#13;
    La  norma  impugnata deroga all'art. 1224 Codice civile escludendo    &#13;
 l'applicabilità del secondo comma alle obbligazioni pecuniarie delle    &#13;
 U.S.L. situate nel territorio della Regione Calabria. Ai creditori di    &#13;
 queste unità sanitarie è negato  il  diritto  al  risarcimento  del    &#13;
 maggior  danno,  oltre gli interessi legali di mora, che essi provino    &#13;
 di avere sofferto a causa del ritardo  del  pagamento.  Né  varrebbe    &#13;
 osservare  che,  almeno  per il ritardo successivo al centottantesimo    &#13;
 giorno dalla scadenza del debito, il  maggior  danno  è  corrisposto    &#13;
 forfettariamente  sotto  forma  di aumento degli interessi di mora al    &#13;
 tasso degli interessi bancari. L'art.  1224,  secondo  comma,  Codice    &#13;
 civile  esclude  il  risarcimento del maggior danno effettivo solo se    &#13;
 gli interessi moratori in misura superiore a quella legale sono stati    &#13;
 convenuti dalle parti del contratto.                                     &#13;
    Questa Corte ha ripetutamente affermato che, pur nelle materie  in    &#13;
 cui  è  ad  esse riconosciuta competenza legislativa, le Regioni non    &#13;
 hanno il potere di modificare la disciplina  dei  diritti  soggettivi    &#13;
 per  quanto  riguarda  i  profili  civilistici  dei  rapporti  da cui    &#13;
 derivano, cioè i modi  di  acquisto  e  di  estinzione,  i  modi  di    &#13;
 accertamento,  le  regole sull'adempimento delle obbligazioni e della    &#13;
 responsabilità    per    inadempimento,    la    disciplina    della    &#13;
 responsabilità  extra  contrattuale,  ecc.  (cfr.  sent.  n. 391 del    &#13;
 1989): "la regolamentazione  di  siffatti  rapporti  appartiene  alla    &#13;
 competenza  istituzionale  dello  Stato, giacché ad essa sottostanno    &#13;
 esigenze di unità e di eguaglianza che possono essere  salvaguardate    &#13;
 solo    se    esclusivamente    all'ente   esponenziale   dell'intera    &#13;
 collettività nazionale è riconosciuto il potere di  emanare  misure    &#13;
 in proposito" (sent. n. 154 del 1972).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   l'illegittimità  costituzionale  dell'art.  62,  quarto    &#13;
 comma, della legge della Regione Calabria 17  dicembre  1981,  n.  21    &#13;
 (Norme sull'amministrazione del patrimonio e sulla contabilità delle    &#13;
 Unità Sanitarie Locali).                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 dicembre 1991.                             &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 30 dicembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
