<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-11-25/246/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-11-25/246"/>
          <FRBRalias value="ECLI:IT:COST:2020:246" name="ECLI"/>
          <FRBRdate date="2020-11-25" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="246"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-11-25/246/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-11-25/246/ita@"/>
          <FRBRdate date="2020-11-25" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-11-25/246/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-11-25/246/ita@.xml"/>
          <FRBRdate date="2020-11-25" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-11-03" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-11-25" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="MORELLI" href="/akn/ontology/person/it/MORELLI" showAs="MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="filomenaPerrone" href="/akn/ontology/person/it/filomenaPerrone" showAs="Filomena PERRONE"/>
        <TLCPerson eId="bile" href="/akn/ontology/person/it/bile" showAs="bile"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario Morelli"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>246</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:246</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>MORELLI</akn4cc:presidente>
        <akn4cc:relatore>Augusto Antonio Barbera</akn4cc:relatore>
        <akn4cc:data_decisione>2020-11-03</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-11-25</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>; Giudici : <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 83, comma 4-sexies, della legge della Regione Veneto 13 aprile 2001, n. 11 (Conferimento di funzioni e compiti amministrativi alle autonomie locali in attuazione del <ref href="/akn/it/act/decretoLegislativo/stato/1998-03-31/112/!main">decreto legislativo 31 marzo 1998, n. 112</ref>), come aggiunto dall'art. 10, comma 1, della legge della Regione Veneto 14 dicembre 2018, n. 43 (Collegato alla legge di stabilità regionale 2019), promosso dal Tribunale amministrativo regionale per il Veneto, nel procedimento vertente tra la Telecom Italia spa e la Regione Veneto e altri, con ordinanza del 17 giugno 2019, iscritta al n. 172 del registro ordinanze 2019 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 43, prima serie speciale, dell'anno 2019.
 Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Telecom Italia spa e della Regione Veneto, nonché l'atto di intervento della Open Fiber spa;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica del 3 novembre 2020 il Giudice relatore Augusto Antonio Barbera;
 uditi l'avvocato Nico Moravia per la Open Fiber spa, gli avvocati Francesco Caliandro per la Telecom Italia spa e Andrea Manzi per la Regione Veneto, in collegamento da remoto, ai sensi del punto 1), del decreto del Presidente della Corte del 30 ottobre 2020;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>deliberato nella camera di consiglio del 3 novembre 2020.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 17 giugno 2019 (r.o. n. 172 del 2019), il Tribunale amministrativo regionale per il Veneto ha sollevato questioni di legittimità costituzionale dell'art. 83, comma 4-sexies, della legge della Regione Veneto 13 aprile 2001, n. 11 (Conferimento di funzioni e compiti amministrativi alle autonomie locali in attuazione del <ref href="/akn/it/act/decretoLegislativo/stato/1998-03-31/112/!main">decreto legislativo 31 marzo 1998, n. 112</ref>), aggiunto al testo originario dall'art. 10, comma 1, della legge della Regione Veneto 14 dicembre 2018, n. 43 (Collegato alla legge di stabilità regionale 2019), in riferimento agli artt. 3, 117, secondo comma, lettera e), e terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 La norma censurata dispone che in caso di occupazione di beni del demanio idrico per l'installazione e la fornitura di reti e per l'esercizio dei servizi di comunicazione elettronica, così come per l'installazione e gestione di sottoservizi e di impianti di sostegno di servizi fuori suolo, il soggetto richiedente sia tenuto al pagamento dei canoni nella misura stabilita dalla Giunta regionale, oltre al versamento degli altri oneri previsti dalla normativa vigente in materia.
 2.- Il giudizio principale è stato promosso da Telecom Italia spa, operatore di comunicazione elettronica autorizzato ai sensi dell'art. 25 del decreto legislativo 1° agosto 2003, n. 259 (Codice delle comunicazioni elettroniche).
 Detta società ha impugnato la nota n. prot. 100127 del 12 marzo 2019, con la quale la Regione Veneto - Genio Civile di Verona - le aveva comunicato che la sua istanza di rinnovo di una concessione idraulica, avente ad oggetto il fiancheggiamento telefonico del fiume Adige nel territorio del Comune di Bussolengo, sarebbe stata evasa solo all'esito del pagamento dell'importo ivi meglio specificato, a titolo di canone per l'occupazione di un tratto di bene demaniale, sulla base della menzionata disposizione regionale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nel ricorso, Telecom Italia spa ha dedotto, fra l'altro, l'illegittimità costituzionale della norma regionale presupposta.
 3.- In ordine alla rilevanza delle questioni, il rimettente osserva che l'unica questione dibattuta nel giudizio è l'applicabilità della norma censurata, che condiziona il rinnovo della concessione all'assolvimento di un onere economico; donde la possibilità di dar corso all'incidente di costituzionalità anche nella fase cautelare del giudizio principale, come in effetti accaduto.
 4.- Con riferimento alla non manifesta infondatezza, il giudice a quo rileva anzitutto che la materia delle telecomunicazioni riceve specifica disciplina dal menzionato codice delle comunicazioni elettroniche, destinato a prevalere, in quanto testo normativo successivo e speciale, sulle disposizioni contenute nel <ref href="/akn/it/act/decretoLegislativo/stato/1998-03-31/112/!main">decreto legislativo 31 marzo 1998, n. 112</ref> (Conferimento di funzioni e compiti amministrativi dello Stato alle regioni ed agli enti locali, in attuazione del capo I della <ref href="/akn/it/act/legge/stato/1997-03-15/59/!main">legge 15 marzo 1997, n. 59</ref>), che ha delegato alle Regioni l'esercizio delle funzioni di polizia idraulica, abilitandole ad imporre ai privati il pagamento di un canone per l'occupazione di aree demaniali.
 4.1.- In particolare, l'art. 93, comma 1, del citato cod. comunicazioni elettroniche stabilisce una riserva di legge in materia, non consentendo alle pubbliche amministrazioni di imporre agli operatori della comunicazione elettronica oneri finanziari, reali o contributi al di fuori di quelli espressamente contemplati dal comma 2 (come interpretato autenticamente, e con efficacia retroattiva, dall'<ref href="/akn/it/act/decretoLegislativo/stato/2016-02-15/33/!main#art_12__para_3">art. 12, comma 3, del decreto legislativo 15 febbraio 2016, n. 33</ref> recante "Attuazione della <ref href="/akn/it/act/direttivaUe/eu/2014/61/!main">direttiva 2014/61</ref>/UE del Parlamento europeo e del Consiglio, del 15 maggio 2014, recante misure volte a ridurre i costi dell'installazione di reti di comunicazione elettronica ad alta velocità"); si tratta, nello specifico, delle spese necessarie alla sistemazione delle aree pubbliche coinvolte dagli interventi di installazione e manutenzione, dei costi di ripristino delle aree medesime, della tassa per l'occupazione di spazi ed aree pubbliche (TOSAP) o del canone previsto al medesimo fine (COSAP).
 4.2.- Il rimettente osserva che detta previsione, secondo il costante orientamento della giurisprudenza costituzionale (sono richiamate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/47/!main">sentenze n. 47 del 2015</ref>, n. 272 del 2010, n. 450 del 2006 e n. 336 del 2005), esprime un principio fondamentale della materia «ordinamento della comunicazione», poiché assicura a tutti gli operatori del settore un trattamento uniforme e non discriminatorio; su tali basi, la stessa giurisprudenza attribuisce all'art. 93 del cod. comunicazioni elettroniche anche una finalità di tutela della concorrenza, con l'obiettivo di garantire parità di trattamento e di non ostacolare l'ingresso di nuovi soggetti nel settore economico di riferimento.
 4.3.- Ciò posto, il rimettente assume che la norma censurata si pone, anzitutto, in contrasto con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, in quanto l'imposizione, da parte della Regione Veneto, di un onere finanziario diverso da quelli tassativamente previsti dalla citata legge statale per l'occupazione del demanio idrico comporta una disparità di trattamento rispetto ad altre Regioni, nelle quali tale onere non è richiesto.
 Il rimettente denunzia, inoltre, una violazione dell'art. 117, secondo comma, lettera e), Cost., poiché l'intervento del legislatore regionale invade la competenza esclusiva dello Stato nella materia «tutela della concorrenza»; assume, infine, anche la violazione dell'art. 117, terzo comma, Cost., poiché la norma censurata contrasta con un principio fondamentale stabilito con legge dello Stato nella materia «ordinamento della comunicazione», di competenza legislativa concorrente.
 5.- Con atto depositato il 23 ottobre 2019 si è costituita in giudizio Telecom Italia spa, ricorrente nel giudizio principale.
 5.1.- La società ha dapprima ricostruito i termini fattuali della vicenda, caratterizzata da un pregresso contenzioso originato dalla pretesa della Regione Veneto di ottenere, in relazione a diversi impianti di telecomunicazione realizzati previa concessione di aree del demanio idrico, il pagamento di canoni, il cui fondamento era rinvenibile nelle previsioni del <ref href="/akn/it/act/decretoLegislativo/stato/1998/112/!main">d.lgs. n. 112 del 1998</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Detto contenzioso era stato definito con il rigetto delle pretese della Regione Veneto, in conformità all'orientamento ormai consolidato della giurisprudenza della Corte di cassazione e del Consiglio di Stato, secondo cui l'art. 93, comma 2, del cod. comunicazioni elettroniche si pone in rapporto di specialità rispetto alla disciplina dei canoni demaniali ricavabile dalle disposizioni del <ref href="/akn/it/act/decretoLegislativo/stato/1998/112/!main">d.lgs. n. 112 del 1998</ref>.
 5.2.- La Regione Veneto aveva allora adottato la disposizione censurata, con la quale era previsto il versamento di un canone, nella misura determinata dalla Giunta Regionale, a carico del richiedente la concessione per l'occupazione del demanio idrico ai fini dell'esercizio dei servizi di comunicazione; ed in tale contesto aveva preso avvio il giudizio principale, originato dalla pretesa della Regione di subordinare al pagamento di detto canone il rinnovo della concessione di un bene del demanio idrico regionale.
 5.3.- Poste tali premesse, Telecom Italia spa ha concluso per l'accoglimento delle questioni sollevate, riportandosi alla giurisprudenza richiamata nella stessa ordinanza di rimessione.
 6.- Con memoria depositata il 6 novembre 2019 si è costituita in giudizio la Regione Veneto.
 6.1.- Pur dichiaratamente consapevole dell'orientamento della giurisprudenza costituzionale, la Regione ha sostenuto che la riserva di legge contenuta nell'art. 93, comma 2, del citato cod. comunicazioni elettroniche non varrebbe ad escludere la possibilità di interventi normativi delle Regioni, in quanto la disciplina del demanio idrico interseca diverse materie, alcune delle quali (come le materie «governo del territorio» e «ordinamento della comunicazione») sono affidate alla competenza legislativa concorrente; in tal senso, ha pertanto sostenuto che una limitazione della potestà legislativa regionale che giungesse ad escludere la possibilità di imporre canoni demaniali sarebbe «inammissibile, anche e soprattutto alla luce delle indicazioni della Carta».
 6.2.- Inoltre, avuto riguardo alla natura dello stesso art. 93, ha rilevato che la riconduzione ad unum della disciplina delle comunicazioni, in attuazione della <ref href="/akn/it/act/direttivaUe/eu/2002/21/!main">Direttiva 2002/21</ref>/CE del Parlamento Europeo e del Consiglio, del 7 marzo 2002, che istituisce un quadro normativo comune per le reti ed i servizi di comunicazione elettronica, non ha comportato una nuova articolazione del riparto delle competenze legislative stabilito dalla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, poiché le Regioni dispongono della competenza ad attuare ed eseguire gli atti normativi dell'Unione Europea nelle materie ad esse attribuite, come previsto dall'art. 117, quinto comma, Cost.
 Secondo la Regione, pertanto, la norma censurata atterrebbe alle materie «governo del territorio» ed «ordinamento della comunicazione», rispetto alle quali non può prospettarsi l'adottabilità, da parte del legislatore statale, di principi fondamentali talmente pervasivi da «comprimere in modo pressoché assoluto ogni competenza delle Regioni, pena la violazione dei principi di sussidiarietà, differenziazione e adeguatezza».
 6.3.- Su tali basi, la Regione ha sostenuto che l'art. 93 del cod. comunicazioni elettroniche farebbe comunque salva l'applicazione di leggi regionali che stabiliscono canoni e oneri per l'impianto di reti o l'esercizio di servizi di comunicazione; ed ha chiesto alla Corte di sollevare dinanzi a se stessa questione di legittimità di tale norma (nonché dell'<ref href="/akn/it/act/decretoLegislativo/stato/2016/33/!main#art_12__para_3">art. 12, comma 3, del d.lgs. n. 33 del 2016</ref>, norma di interpretazione autentica con efficacia retroattiva), laddove interpretata come affermativa di una riserva di legge statale in relazione ad attività che «implichino l'occupazione di beni demaniali affidati alla gestione delle Regioni».
 6.4.- Nel merito, ha poi concluso per il rigetto delle questioni, assumendo che le disposizioni censurate sarebbero conformi «sia al diritto dell'Unione Europea, sia all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_23">art. 23 della Costituzione</ref> [...], sia al riparto di competenze fissato nell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_117">art. 117 della Costituzione</ref>, sia, infine e in particolare, all'esigenza di tutelare la concorrenza»; ed a tale ultimo riguardo ha sostenuto che le eventuali differenziazioni relative all'obbligo del pagamento di canoni sarebbero «giustificate dall'attenzione che ogni ente rivolge alle peculiarità del proprio territorio», e che - in ogni caso - il canone demaniale sarebbe conforme ai requisiti di trasparenza, obiettiva giustificabilità, proporzionalità e non discriminazione fissati dal citato cod. comunicazioni elettroniche.
 7.- È infine intervenuta, con memoria depositata il 12 novembre 2019, Open Fiber spa, anch'essa operatrice autorizzata per le telecomunicazioni, deducendo la propria legittimazione ad intervenire in base a due distinte circostanze.
 Da un lato, infatti, detta società ha allegato di aver promosso un giudizio dal contenuto identico a quello del giudizio principale, poi sospeso dal TAR in attesa della definizione della presente questione di legittimità, ed ha sostenuto che da tale circostanza doveva desumersi l'esistenza di un suo interesse diretto ed immediato rispetto al rapporto sostanziale dedotto in giudizio; dall'altro lato, ha documentato di essere comunque intervenuta ad adiuvandum nel giudizio principale, seppur con atto successivo al deposito dell'ordinanza di rimessione.
 Nel merito, ha concluso per l'accoglimento della questione.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>8.</num>
            <content>
              <p>- Telecom Italia spa e la Regione Veneto hanno depositato memorie in prossimità dell'udienza, insistendo nelle conclusioni già formulate.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Tribunale amministrativo regionale per il Veneto dubita della legittimità costituzionale dell'art. 83, comma 4-sexies, della legge della Regione Veneto 13 aprile 2001, n. 11 (Conferimento di funzioni e compiti amministrativi alle autonomie locali in attuazione del <ref href="/akn/it/act/decretoLegislativo/stato/1998-03-31/112/!main">decreto legislativo 31 marzo 1998, n. 112</ref>), aggiunto al testo originario dall'art. 10, comma 1, della legge della Regione Veneto 14 dicembre 2018, n. 43 (Collegato alla legge di stabilità regionale 2019), in riferimento agli artt. 3, 117, secondo comma, lettera e), e terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 Tale norma prevede che, in caso di occupazione di beni del demanio idrico per l'installazione e la fornitura di reti e per l'esercizio dei servizi di comunicazione elettronica, così come per l'installazione e gestione di sottoservizi e di impianti di sostegno di servizi fuori suolo, il soggetto richiedente sia tenuto al pagamento dei canoni nella misura stabilita dalla Giunta regionale, oltre al versamento degli altri oneri previsti dalla normativa vigente in materia.
 1.1.- Secondo il rimettente, la norma censurata derogherebbe all'art. 93, comma 1, del decreto legislativo 1° agosto 2003, n. 259 (Codice delle comunicazioni elettroniche), a mente del quale «Le Pubbliche amministrazioni, le Regioni, le Province ed i Comuni non possono imporre per l'impianto di reti o per l'esercizio dei servizi di comunicazione elettronica, oneri e canoni che non siano stabiliti per legge»; e siffatta deroga, riguardando costi ed oneri non contemplati in altre Regioni, determinerebbe una disparità di trattamento tra operatori economici la cui attività è distribuita sul territorio nazionale, con conseguente violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>
 1.2.- La stessa imposizione, inoltre, darebbe luogo ad un'alterazione del sistema concorrenziale del mercato nazionale, con conseguente invasione della competenza legislativa esclusiva dello Stato nella materia «tutela della concorrenza»; e poiché, in base al costante orientamento della giurisprudenza costituzionale, l'art. 93 del già citato cod. comunicazioni elettroniche esprime un principio fondamentale nella materia «ordinamento della comunicazione», di competenza legislativa concorrente, la norma censurata, nel porsi in contrasto con tale principio, violerebbe altresì l'art. 117, terzo comma, Cost..
 2.- Esercitando il proprio potere di decidere l'ordine delle questioni da affrontare (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/258/!main">sentenze n. 258 del 2019</ref> e n. 148 del 2018), questa Corte ritiene di esaminare prioritariamente quest'ultima censura.
 2.1.- Occorre anzitutto rilevare che, con la disposizione censurata, la Regione Veneto non ha esercitato la propria competenza legislativa nella materia «governo del territorio».
 La norma, infatti, non concerne la progettazione tecnica, la realizzazione o l'allocazione degli impianti di produzione o trasmissione delle comunicazioni, aspetti certamente destinati ad interessare l'assetto urbanistico e le peculiarità territoriali dell'area su cui tali attività ricadono e, come tali, rientranti nella competenza legislativa delle Regioni, sia pure tenute «ad uniformarsi agli standard stabiliti dal gestore della rete di trasmissione nazionale» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2004/7/!main">sentenza n. 7 del 2004</ref>; in senso conforme, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2005/336/!main">sentenza n. 336 del 2005</ref>).
 Essa è, piuttosto, destinata a regolare l'esercizio, da parte della Regione, delle funzioni amministrative conferitele per effetto del <ref href="/akn/it/act/decretoLegislativo/stato/1998-03-31/112/!main">decreto legislativo 31 marzo 1998, n. 112</ref> (Conferimento di funzioni e compiti amministrativi dello Stato alle regioni ed agli enti locali, in attuazione del capo I della <ref href="/akn/it/act/legge/stato/1997-03-15/59/!main">legge 15 marzo 1997, n. 59</ref>), ed in particolare di quelle in materia di risorse idriche, mediante la disciplina del relativo rapporto concessorio.
 2.2.- In tale specifico ambito, la disposizione censurata prevede il pagamento di canoni a carico degli operatori della comunicazione, in relazione a tutte le ipotesi nelle quali l'esercizio della relativa attività renda necessaria l'occupazione di beni del demanio idrico; il canone è, infatti, previsto «per l'installazione e fornitura di reti e per l'esercizio dei servizi di comunicazione elettronica, così come per la installazione e gestione di sottoservizi e di impianti di sostegno di servizi fuori suolo».
 Detta disposizione va, pertanto, ricondotta alla materia «ordinamento della comunicazione», laddove disciplina l'imposizione di oneri pecuniari.
 2.3.- Nell'ambito di quest'ultima materia, la disciplina del settore della comunicazione elettronica persegue il duplice e concorrente obiettivo della libertà nella fornitura del relativo servizio, in quanto di preminente interesse generale, e della tutela del diritto di iniziativa economica degli operatori, da svolgersi in regime di concorrenza proprio al fine di garantire il più ampio accesso all'uso dei mezzi di comunicazione elettronica.
 Siffatti obiettivi hanno caratterizzato l'intervento del legislatore statale nel settore delle telecomunicazioni, avvenuto con il citato cod. comunicazioni elettroniche; tale intervento, fra l'altro, ha attuato una liberalizzazione del mercato con le finalità (espressamente rappresentate nelle Direttive 2002/19/CE, 2002/20/CE, 2002/21/CE e 2002/22/CE del Parlamento Europeo e del Consiglio, del 7 marzo 2002) di garantire agli imprenditori l'accesso al settore con criteri di obiettività, trasparenza, non discriminazione e proporzionalità, nonché di consentire agli utenti finali la fornitura del servizio universale, senza distorsioni della concorrenza.
 2.4.- Su tali basi, questa Corte ha da tempo affermato che l'art. 93 del cod. comunicazioni elettroniche costituisce espressione di un principio fondamentale della materia, «in quanto persegue la finalità di garantire a tutti gli operatori un trattamento uniforme e non discriminatorio, attraverso la previsione del divieto di porre a carico degli stessi oneri o canoni» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2005/336/!main">sentenza n. 336 del 2005</ref>; in senso conforme, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/47/!main">sentenze n. 47 del 2015</ref>, n. 272 del 2010, n. 450 del 2006).
 La norma censurata si pone, infatti, in netto contrasto con tale principio, poiché impone agli operatori delle comunicazioni una prestazione pecuniaria che rientra nell'ambito di quelle colpite dal divieto.
 2.5.- In ordine alla finalità perseguita dall'art. 93 del citato cod. comunicazioni elettroniche, questa Corte ha inoltre precisato che, in mancanza di tale divieto, ogni singola Regione «potrebbe liberamente prevedere obblighi "pecuniari" a carico dei soggetti operanti sul proprio territorio, con il rischio, appunto, di una ingiustificata discriminazione rispetto ad operatori di altre Regioni, per i quali, in ipotesi, tali obblighi potrebbero non essere imposti» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/272/!main">sentenza n. 272 del 2010</ref>).
 Si deve, perciò, escludere che, come invece ritenuto dalla Regione resistente, la riserva di legge contenuta nell'art. 93 consenta anche un intervento del legislatore regionale.
 Se così non fosse, del resto, sarebbe contraddetta la stessa ratio legis, come individuata, con la decisione poc'anzi citata, nella finalità di «evitare che ogni Regione possa liberamente prevedere obblighi "pecuniari" a carico dei soggetti operanti sul proprio territorio».</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>- La questione sollevata in relazione all'art. 117, terzo comma, Cost., è dunque fondata. Vengono assorbiti i restanti profili.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'art. 83, comma 4-sexies, della legge della Regione Veneto 13 aprile 2001, n. 11 (Conferimento di funzioni e compiti amministrativi alle autonomie locali in attuazione del <ref href="/akn/it/act/decretoLegislativo/stato/1998-03-31/112/!main">decreto legislativo 31 marzo 1998, n. 112</ref>).
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-11-03">3 novembre 2020</date>.
 F.to:
 <signature><judge refersTo="#marioRosarioMorelli" as="#presidente">Mario Rosario MORELLI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#augustoAntonioBarbera" as="#relatore">Augusto Antonio BARBERA</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#filomenaPerrone" as="#cancelliere">Filomena PERRONE</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-11-25">25 novembre 2020</date>.
 Il Cancelliere
 F.to: Filomena PERRONE
 Allegato:Ordinanza letta all'udienza del <date refersTo="" date="2020-11-03">3 novembre 2020</date>Rilevato che nel giudizio n. 172 r.o. del 2019, promosso con ordinanza del Tribunale amministrativo per il Veneto del <date refersTo="" date="2019-06-17">17 giugno 2019</date>, è intervenuta la Open Fiber spa, la quale, esponendo di aver impugnato innanzi al Tribunale amministrativo regionale per il Veneto un atto dal contenuto identico a quello oggetto del giudizio principale, e rappresentando altresì che il relativo giudizio è stato sospeso dallo stesso Tribunale in attesa della definizione della presente questione di legittimità costituzionale, assume, per tale ragione, di essere legittimata ad intervenire, in quanto portatrice di interesse immediato e diretto al rapporto sostanziale dedotto in giudizio;che, inoltre, la stessa Open Fiber spa afferma di essere intervenuta ad adiuvandum nel giudizio principale con atto notificato in data <date refersTo="" date="2019-11-08">8 novembre 2019</date>.Considerato, quanto a tale ultimo profilo, che nel giudizio di legittimità costituzionale in via incidentale possono costituirsi i soggetti che erano parti del giudizio a quo al momento dell'ordinanza di rimessione (ex plurimis, tra le tante, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/13/!main">sentenze n. 13 del 2019</ref>, n. 217 e n. 180 del 2018; ordinanze allegate alle <mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/158/!main">sentenze n. 158</ref>, n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/119/!main">119</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/30/!main">30 del 2020</ref></mref>, n. 237, n. 221, n. 159, n. 141 e n. 98 del 2019, n. 217, n. 194, n. 180 e n. 77 del 2018, n. 29 del 2017, n. 286, n. 243 e n. 84 del 2016; <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/202/!main">ordinanze n. 202</ref>, n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/111/!main">111</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/37/!main">37 del 2020</ref></mref>), mentre nel caso di specie il deposito dell'atto di intervento è successivo all'emissione di quest'ultima;che, per costante giurisprudenza di questa Corte, l'intervento di soggetti estranei al giudizio principale (art. 4 delle Norme integrative per i giudizi davanti alla Corte costituzionale) è ammissi<signature><judge refersTo="#bile">bile</judge></signature> soltanto per i terzi titolari di un interesse qualificato, inerente in modo diretto e immediato al rapporto sostanziale dedotto in giudizio e non semplicemente regolato, al pari di ogni altro, dalla norma oggetto di censura (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/158/!main">is, sentenza n. 158 del 2018</ref>, con allegata ordinanza letta all'udienza del <date refersTo="" date="2020-06-10">10 giugno 2020</date>; <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/206/!main">sentenza n. 206 del 2019</ref>, con allegata ordinanza letta all'udienza del <date refersTo="" date="2019-06-04">4 giugno 2019</date>);che, pertanto, l'incidenza sulla posizione soggettiva dell'interveniente deve derivare non già, come per tutte le altre situazioni sostanziali disciplinate dalla disposizione denunciata, dalla pronuncia della Corte sulla legittimità costituzionale della legge stessa, ma dall'immediato effetto che detta pronuncia produce sul rapporto sostanziale oggetto del giudizio a quo;che, nel caso in esame, la Open Fiber spa non è titolare di un interesse direttamente riconduci<signature><judge refersTo="#bile">bile</judge></signature> all'oggetto del giudizio principale, bensì di un interesse riflesso all'accoglimento della questione, in quanto assoggettata, come ogni altro operatore di comunicazione elettronica autorizzato, alla norma regionale censurata;che, inoltre, per costante giurisprudenza di questa Corte, ai fini dell'ammissibilità dell'intervento, non rileva che il giudizio di cui è parte l'interveniente sia stato sospeso in attesa dell'esito dell'incidente di costituzionalità scaturito da altro indipendente giudizio, poiché, ove si ritenesse altrimenti, verrebbe sostanzialmente soppresso il carattere incidentale del giudizio di legittimità costituzionale e non sarebbe consentito alla Corte di verificare la rilevanza della questione (<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/202/!main">ordinanza n. 202 del 2020</ref>);che, pertanto, l'intervento della Open Fiber spa deve essere dichiarato inammissi<signature><judge refersTo="#bile">bile</judge></signature>.PER QUESTI MOTIVILA CORTE COSTITUZIONALEdichiara inammissi<signature><judge refersTo="#bile">bile</judge></signature> l'intervento della società Open Fiber spa.F.to: <signature><judge refersTo="#marioRosarioMorelli" as="#presidente">Mario Rosario Morelli</judge>, <role refersTo="#presidente">Presidente</role></signature></block>
    </conclusions>
  </judgment>
</akomaNtoso>
