<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>49</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:49</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1986</data_decisione>
    <data_deposito>12/03/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO - Prof. RENATO &#13;
 DELL'ANDRO - Prof. GABRIELE PESCATORE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1 della legge  &#13;
 14 marzo 1961 n. 132 promosso con l'ordinanza emessa il 29 ottobre 1976  &#13;
 dalla Corte dei conti su ricorso proposto da Granello Daniele  iscritta  &#13;
 al  n.  581  del  registro  ordinanze  1981 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 5 dell'anno 1982;                          &#13;
     udito nella camera di consiglio del  22  gennaio  1986  il  Giudice  &#13;
 relatore Giuseppe Borzellino.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza  emessa  il  29  ottobre  1976 (pervenuta alla Corte  &#13;
 costituzionale il 27 agosto 1981) la Corte dei conti  -  terza  sezione  &#13;
 giurisdizionale  -,  sul  ricorso  proposto  da  Granello  Daniele  per  &#13;
 ottenere la concessione della pensione di riversibilità  quale  vedovo  &#13;
 di  pensionata  ex regime austro-ungarico, ha sollevato, su istanza del  &#13;
 Procuratore generale,  "la  questione  di  legittimità  costituzionale  &#13;
 dell'art.   1 della legge 14 marzo 1961 n. 132, in relazione all'art. 3  &#13;
 della   Costituzione,   in   quanto   esclude   dal   beneficio   della  &#13;
 riversibilità  delle  pensioni  i vedovi di pensionate già dipendenti  &#13;
 dal cessato regime austro-urgarico".                                     &#13;
     Il giudice a quo premette che l'impugnato art. 1 della legge n. 132  &#13;
 del 1961 ammette la riversibilità delle  pensioni  di  dipendenti  del  &#13;
 cessato  regime  austro-ungarico soltanto a favore degli orfani e delle  &#13;
 vedove, restando quindi escluse dal beneficio  le  altre  categorie  di  &#13;
 soggetti  invece previste dalla legge 15 febbraio 1958 n. 46 (genitori,  &#13;
 collaterali, vedovo).                                                    &#13;
     Vengono richiamate le  argomentazioni  esposte  in  una  precedente  &#13;
 ordinanza  di  rimessione  emessa  dalla  stessa sezione il 18 dicembre  &#13;
 1974, con la quale la stessa norma (art. 11. n. 132 del 1961) è  stata  &#13;
 sospettata  di  incostituzionalità  per l'esclusione dal beneficio dei  &#13;
 collaterali. E quanto alla omessa previsione del vedovo tra i  soggetti  &#13;
 beneficiari  della pensione di riversibilità, "nessun valido motivo di  &#13;
 tecnica  o  politica  legislativa   né   tanto   meno   etico-sociale"  &#13;
 giustificherebbe  siffatta  esclusione  che  crea  "una  disparità  di  &#13;
 trattamento tra cittadini italiani  congiunti  di  pensionati,  il  cui  &#13;
 onere  pensionistico  fu  assolto  integralmente  a  carico dello Stato  &#13;
 italiano  riproponendo  a  distanza di decenni una differenziazione che  &#13;
 risalirebbe all'origine dei  diritti  propri  dei  pensionati  e  che  &#13;
 invece   fu   già  sostanzialmente  superata  dalle  norme  del  primo  &#13;
 dopoguerra".<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La questione sollevata dalla Corte dei conti  con  l'ordinanza  &#13;
 in  epigrafe  verte  sull'art.  1  l. 14 marzo 1961 n.  132 (Estensione  &#13;
 delle norme sulla riversibilità delle pensioni, contenute nella  legge  &#13;
 15  febbraio  1958,  n.  46,  alla  vedova ed orfani di pensionati già  &#13;
 appartenenti all'Amministrazione austro-ungarica o all'ex Stato  libero  &#13;
 di  Fiume)  nella  parte  in  cui  esclude  dalla  riversibilità delle  &#13;
 pensioni - in contrasto con l'art. 3 Cost. -  i  vedovi  di  pensionate  &#13;
 già  dipendenti  del  cessato  regime  austro-ungarico e dell'ex Stato  &#13;
 libero di Fiume.                                                         &#13;
     2. - La questione è fondata.                                        &#13;
     In   termini   generali   la   giurisprudenza   costituzionale   ha  &#13;
 ripetutamente affermato la parità di trattamento tra uomini e donne in  &#13;
 tema di trattamenti pensionistici (sent. 30 gennaio 1980 n. 6, sent. 30  &#13;
 gennaio  1980 n. 9, sent. 26 maggio 1981 n. 75, sent. 18 luglio 1984 n.  &#13;
 214).                                                                    &#13;
     E quanto al punto specifico di  causa,  che  riveste,  per  le  sue  &#13;
 origini, connotazioni del tutto peculiari questa Corte, con la sentenza  &#13;
 n.  76 del 1979, emessa sulla base dell'ordinanza di rinvio della Corte  &#13;
 dei  conti  indicata  in  narrativa  ebbe  già  ad   occuparsi   della  &#13;
 legittimità  della norma impugnata, nella parte in cui escludeva dalla  &#13;
 riversibilità i collaterali, secondo  le  condizioni  previste  per  i  &#13;
 dipendenti civili e militari dello Stato.                                &#13;
     Si  riconobbe  ivi,  dichiarandosi in conseguenza la illegittimità  &#13;
 del disposto nei limiti offerti,  la  sussistenza  di  un  criterio  di  &#13;
 "puntuale  parallelismo"  della  normativa  pensionistica  relativa  ai  &#13;
 dipendenti   dell'ex   Amministrazione   austroungarica   con    quella  &#13;
 concernente gli impiegati dello Stato.                                   &#13;
     E  dunque,  alla  luce  di  tale  puntuale principio, certamente da  &#13;
 confermarsi, non può non  riconoscersi  -  alla  medesima  stregua  di  &#13;
 quanto  previsto dall'art. 81 del testo unico 29 dicembre 1973, n. 1092  &#13;
 sul trattamento di quiescenza dei dipendenti dello  Stato  -  anche  il  &#13;
 diritto  del  vedovo alla riversibilità della pensione, liquidata alla  &#13;
 moglie in base alle specifiche  norme  concernenti  il  cessato  regime  &#13;
 austro-ungarico.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 1 della legge 14  &#13;
 marzo  1961  n.  132 (Estensione delle norme sulla riversibilità delle  &#13;
 pensioni, contenute nella legge 15 febbraio 1958 n. 46, alle vedove  ed  &#13;
 orfani  di  pensionati  già  appartenenti'  all'Amministrazione austro  &#13;
 ungarica o all'ex Stato libero di Fiume), nella parte  in  cui  esclude  &#13;
 dal   beneficio  della  riversibilità  della  pensione  il  vedovo  di  &#13;
 pensionata già dipendente del cessato regime austro-ungarico o dell'ex  &#13;
 Stato libero di Fiume.                                                   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 7 marzo 1986.                                 &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   GIUSEPPE   FERRARI    -  &#13;
                                   FRANCESCO  SAJA  -  GIOVANNI  CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   -   RENATO   DELL'ANDRO   -  GABRIELE  &#13;
                                   PESCATORE.                             &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
