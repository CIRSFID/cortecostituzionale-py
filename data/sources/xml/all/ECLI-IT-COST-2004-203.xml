<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>203</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:203</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ZAGREBELSKY</presidente>
    <relatore_pronuncia>Alfonso Quaranta</relatore_pronuncia>
    <redattore_pronuncia>Alfonso Quaranta</redattore_pronuncia>
    <data_decisione>24/06/2004</data_decisione>
    <data_deposito>28/06/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 9, comma 2, della legge della Regione Abruzzo 5 agosto 2003, n. 11 (Norme in materia di Comunità montane), promosso con ricorso del Presidente del Consiglio dei ministri, notificato il 23 ottobre 2003, depositato in cancelleria il 30 successivo ed iscritto al n. 77 del registro ricorsi 2003. &#13;
    Udito nell'udienza pubblica del 25 maggio 2004 il Giudice relatore Alfonso Quaranta;  &#13;
    udito l'avvocato dello Stato Massimo Salvatorelli per il Presidente del Consiglio dei ministri. &#13;
    Ritenuto che il Presidente del Consiglio dei ministri, con ricorso notificato il 23 ottobre 2003 e depositato il successivo 30 ottobre, ha sollevato questione di legittimità costituzionale in via principale dell'art. 9, comma 2, della legge della Regione Abruzzo 5 agosto 2003, n. 11 (Norme in materia di Comunità montane); &#13;
    che la disposizione impugnata prevede l'esercizio di un potere sostitutivo da parte del difensore civico regionale, ai sensi dell'art. 136 del decreto legislativo 18 agosto 2000, n. 267 (Testo unico delle leggi sull'ordinamento degli enti locali), nell'ipotesi in cui i consigli dei Comuni membri delle Comunità montane non provvedano ad eleggere i propri rappresentanti in seno alla Comunità montana stessa, nella prima seduta successiva al loro insediamento e, comunque, non oltre il quarantacinquesimo giorno dallo stesso;  &#13;
    che la difesa erariale sostiene che il predetto intervento del difensore civico regionale avverrebbe non per il compimento - come consentito dal citato art. 136 del d.lgs. n. 267 del 2000 - di «atti obbligatori per legge» di natura amministrativa, bensì per lo svolgimento di una attività di natura «politico-istituzionale», quale quella relativa alla rappresentanza elettiva dei Consigli comunali; &#13;
    che la suddetta attività atterrebbe alle «funzioni istituzionali proprie dei Comuni», come indicate dall'art. 42, comma 1 (recte: 2), lettera m), del d.lgs. n. 267 del 2000, che assegna al Consiglio comunale la potestà di «nomina dei rappresentanti del consiglio presso enti, aziende ed istituzioni ad esso espressamente riservata dalla legge»; &#13;
    che la disposizione censurata inciderebbe, inoltre, sulle modalità di elezione compiutamente prefissate dall'art. 27, comma 2, dello stesso d.lgs. n. 267 del 2000 e non suscettibili di integrazioni e modificazioni da parte del legislatore regionale; &#13;
    che per i motivi sin qui esposti l'art. 9, comma 2, della legge della Regione Abruzzo n. 11 del 2003 violerebbe, secondo la difesa erariale: a) l'art. 114 della Costituzione, «per lesione del principio di equiordinazione tra Stato, Regioni ed Enti locali e delle prerogative istituzionali dei Comuni»; b) l'art. 117, secondo comma, lettera p), della Costituzione, «in quanto non spetta alla Regione ed esula dalla sua competenza legislativa la regolamentazione, sia pure in via sostitutiva, della materia regolata dall'art. 27, comma 2, del d.lgs. n. 267 del 2000, che rientra, invece, nella competenza esclusiva dello Stato in materia di organi di governo e funzioni fondamentali di Comuni, Province e Città metropolitane»; &#13;
    che in data 4 maggio 2004 l'Avvocatura generale dello Stato ha depositato una memoria, con la quale ha ribadito, ampliandole, le argomentazioni già svolte;  &#13;
    che in data 22 maggio 2004 la difesa erariale ha depositato una nota con la quale ha sottolineato che la Regione Abruzzo ha abrogato la disposizione impugnata con l'art. 1, comma 36, della legge della stessa Regione 19 novembre 2003, n. 20 (Modifiche ed integrazioni alla legge regionale 17 aprile 2003, n. 7 - Legge finanziaria regionale 2003) e che la disposizione impugnata risulta non aver avuto concreta applicazione nel periodo di vigenza; &#13;
    che il ricorrente ha, pertanto, ritenuto che sia venuto meno l'interesse alla declaratoria di incostituzionalità, ed ha chiesto che sia dichiarata cessata la materia del contendere; &#13;
    che la Regione Abruzzo, non costituitasi in giudizio, ha trasmesso, in data 20 maggio 2004, presso la cancelleria della Corte, una nota del Difensore civico della stessa Regione dell'11 maggio 2004, attestante la mancata attuazione della disposizione impugnata; &#13;
    che nel corso dell'udienza pubblica l'Avvocatura generale dello Stato ha nuovamente concluso perché venga dichiarata la cessazione della materia del contendere.  &#13;
    Considerato che l'art. 1, comma 36, della legge della Regione Abruzzo 19 novembre 2003, n. 20  ha abrogato la disposizione impugnata; &#13;
    che, inoltre, tale disposizione non ha ricevuto attuazione durante il periodo della sua vigenza, come risulta dall'attestazione, in data 11 maggio 2004, del Difensore civico della Regione Abruzzo, condivisa dallo stesso ricorrente che - sia nella memoria depositata in data 22 maggio 2004 che nel corso dell'udienza pubblica - ha chiesto che venga dichiarata cessata la materia del contendere;  &#13;
    che, in siffatta situazione, viene meno la necessità di una pronuncia da parte di questa Corte (ordinanze n. 137 del 2004, n. 15 del 2003 e n. 443 del 2002) e deve, pertanto, essere dichiarata la cessazione della materia del contendere.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara cessata la materia del contendere in ordine alla questione di legittimità costituzionale dell'art. 9, comma 2, della legge della Regione Abruzzo 5 agosto 2003, n. 11 (Norme in materia di Comunità montane), sollevata, in riferimento agli artt. 114 e 117, secondo comma, lettera p), della Costituzione, dal Presidente del Consiglio dei ministri con il ricorso indicato in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 giugno 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Alfonso QUARANTA, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 28 giugno 2004. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
