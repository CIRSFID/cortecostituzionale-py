<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>56</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:56</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/02/1999</data_decisione>
    <data_deposito>04/03/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi di legittimità costituzionale degli artt. 513, comma 2,    &#13;
 e 514, del codice di procedura penale, come modificati dalla legge  7    &#13;
 agosto  1997,  n.  267  (Modifica  delle  disposizioni  del codice di    &#13;
 procedura penale in tema di valutazione delle  prove),  promossi  con    &#13;
 ordinanze  emesse  il  19 dicembre 1997 dal Tribunale di Milano, il 6    &#13;
 febbraio 1998 dal Tribunale di Torino, il 22 ed il 21 aprile 1998 dal    &#13;
 Tribunale di Udine, il 22 aprile 1998 dal Tribunale  di  Roma,  il  7    &#13;
 maggio  1998  dal  Tribunale  di  Avezzano,  il  20  maggio 1998, dal    &#13;
 Tribunale di Pescara e l'11  giugno  1998  dal  pretore  di  Pescara,    &#13;
 rispettivamente  iscritte  ai  nn. 93, 265, 428, 429, 461, 494, 602 e    &#13;
 630 del registro ordinanze 1998 e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica nn. 9, 16, 25, 26, 28, 37, 38, prima serie speciale,    &#13;
 dell'anno 1998.                                                          &#13;
   Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del 27 gennaio 1999 il giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto che il Tribunale di Milano  (r.o.  n.  93  del  1998),  il    &#13;
 Tribunale  militare di Torino (r.o. n. 265 del 1998), il Tribunale di    &#13;
 Udine (r.o. nn. 428 e 429 del 1998), il Tribunale per i minorenni  di    &#13;
 Roma  (r.o.  n.  461 del 1998), il Tribunale di Avezzano (r.o. n. 494    &#13;
 del 1998), il Tribunale di Pescara  (r.o.  n.  602  del  1998)  e  il    &#13;
 pretore  di  Pescara  (r.o.  n.  630  del  1998)  hanno sollevato, in    &#13;
 riferimento agli artt. 3, 24, 25, 97,  101,  102,  111  e  112  della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art. 513,    &#13;
 comma  2, del codice di procedura penale, come modificato dalla legge    &#13;
 7 agosto 1997, n. 267 (Modifica  delle  disposizioni  del  codice  di    &#13;
 procedura  penale in tema di valutazione delle prove), nella parte in    &#13;
 cui subordina all'accordo delle parti l'utilizzabilità ai fini della    &#13;
 decisione  delle  dichiarazioni  rese  nella  fase   delle   indagini    &#13;
 preliminari  dall'imputato in procedimento connesso che si avvalga in    &#13;
 dibattimento della facoltà di non rispondere;                           &#13;
     che, in particolare, il Tribunale di Avezzano impugna, unitamente    &#13;
 all'art. 513, comma 2, cod. proc. pen., anche l'art. 514 dello stesso    &#13;
 codice;                                                                  &#13;
     che  tutte  le  questioni  sono  state  sollevate  nel  corso  di    &#13;
 dibattimenti  nei  quali  alcuni  imputati  in procedimenti connessi,    &#13;
 citati per la prima volta dopo l'entrata in vigore della legge n. 267    &#13;
 del 1997, si erano avvalsi della facoltà di  non  rispondere,  e  le    &#13;
 parti  non  avevano  prestato  il  consenso  alla utilizzazione delle    &#13;
 dichiarazioni rese in precedenza;                                        &#13;
     che, secondo i rimettenti, l'art. 513, comma 2, cod.  proc.  pen.    &#13;
 sarebbe   in  contrasto  con  l'art.  3  della  Costituzione  per  la    &#13;
 irragionevole   diversità   della    disciplina    riservata    alle    &#13;
 dichiarazioni rese nel corso delle indagini preliminari dall'imputato    &#13;
 in  procedimento  connesso  che  in  dibattimento  si  avvalga  della    &#13;
 facoltà di non rispondere rispetto:  a quella dettata per le  stesse    &#13;
 dichiarazioni  quando  per  fatti o circostanze imprevedibili non sia    &#13;
 possibile ottenere la presenza del soggetto citato ai sensi dell'art.    &#13;
 210 cod. proc. pen. (r.o. n. 265 del 1998, r.o. n. 461 del 1998, r.o.    &#13;
 n.  602  del  1998);  alla  disciplina  riservata  alle dichiarazioni    &#13;
 testimoniali rese nel corso delle indagini preliminari (r.o.  n.  494    &#13;
 del  1998; r.o. n. 602 del 1998); alla disciplina dettata nel comma 1    &#13;
 dell'art. 513 cod. proc.  pen.,  secondo  cui  le  dichiarazioni  del    &#13;
 coimputato  che  rifiuta in dibattimento di sottoporsi all'esame sono    &#13;
 utilizzabili nei confronti dell'imputato consenziente (r.o.   n.  494    &#13;
 del 1998);                                                               &#13;
     che,  secondo  i giudici a quibus l'art. 513, comma 2, cod. proc.    &#13;
 pen. determina altresì  disparità  di  trattamento  tra  l'imputato    &#13;
 raggiunto  da  dichiarazioni  accusatorie  rese  da  un  imputato  in    &#13;
 procedimento connesso divenute irripetibili ai  sensi  dell'art.  512    &#13;
 cod.  proc.    pen.,  come  tali  utilizzabili  per  la  decisione, e    &#13;
 l'imputato attinto dalle dichiarazioni di un imputato in procedimento    &#13;
 connesso, irripetibili a seguito dell'esercizio della facoltà di non    &#13;
 rispondere e quindi inutilizzabili ai fini della decisione (r.o.  nn.    &#13;
 428 e 429 del 1998);                                                     &#13;
     che  i rimettenti lamentano inoltre che l'art. 513, comma 2, cod.    &#13;
 proc.  pen.,  vietando   in   mancanza   dell'accordo   delle   parti    &#13;
 l'acquisizione  delle  dichiarazioni legittimamente assunte prima del    &#13;
 dibattimento,  deroghi  irragionevolmente   al   principio   di   non    &#13;
 dispersione  della  prova  e impedisca al giudice la piena conoscenza    &#13;
 dei fatti del giudizio, così sacrificando l'esercizio della funzione    &#13;
 giurisdizionale, il cui fine è quello della ricerca  della  verità,    &#13;
 con  conseguente  lesione  anche  del  principio dell'obbligatorietà    &#13;
 dell'azione penale, in contrasto con gli artt. 3,  25,  101,  secondo    &#13;
 comma,  della  Costituzione (r.o.   n. 93 del 1998), con gli artt. 3,    &#13;
 24, secondo comma, 25, secondo comma, 101, secondo comma, 102,  primo    &#13;
 comma, e 111 della Costituzione (r.o. n. 265 del 1998), con gli artt.    &#13;
 3,  101  e 112 della Costituzione (r.o. nn. 428 e 429 del 1998, nelle    &#13;
 quali il principio di non dispersione viene ricondotto agli artt.  2,    &#13;
 3  e  25,  secondo  comma,  Cost.),  con  gli  artt.  3  e  25  della    &#13;
 Costituzione (r.o. n. 461 del  1998),  con  gli  artt.  101,  secondo    &#13;
 comma,  e  111  della  Costituzione  (r.o. n. 494 del 1998, che evoca    &#13;
 anche l'art. 24 della Costituzione  per  violazione  del  diritto  di    &#13;
 difesa  della  parte  civile  e del coimputato che abbiano in ipotesi    &#13;
 interesse alla  utilizzazione  di  dichiarazioni  favorevoli)  e  con    &#13;
 l'art. 3 della Costituzione (r.o. n. 630 del 1998);                      &#13;
     che,  infine, a giudizio dei rimettenti l'art. 513, comma 2, cod.    &#13;
 proc. pen., subordinando alla volontà delle parti  l'ingresso  delle    &#13;
 dichiarazioni rese in precedenza da imputati in procedimenti connessi    &#13;
 fra  il materiale probatorio sottoposto alla valutazione del giudice,    &#13;
 introduce  un  principio  dispositivo  in  materia   probatoria,   in    &#13;
 contrasto   con  i  principi  di  uguaglianza,  legalità,  esercizio    &#13;
 dell'azione   penale,   funzione   conoscitiva   del    processo    e    &#13;
 indefettibilità della giurisdizione, con violazione degli artt. 101,    &#13;
 secondo  comma,  e  112 della Costituzione (r.o. n. 93 del 1998, r.o.    &#13;
 nn. 428 e 429 del 1998; r.o. n. 461 del 1998; r.o. n. 602  del  1998,    &#13;
 secondo  cui  sarebbe violato anche l'art.  97 Cost.), degli artt. 3,    &#13;
 25, secondo comma, 102 e 112 della  Costituzione  (r.o.  n.  265  del    &#13;
 1998);                                                                   &#13;
     che  nei  giudizi  promossi  con le ordinanze iscritte ai nn. 93,    &#13;
 265, 428, 429, 461, 494 e 630 del r.o. del  1998  è  intervenuto  il    &#13;
 Presidente   del  Consiglio  dei  Ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura generale  dello  Stato,  riportandosi  integralmente,    &#13;
 stante   l'analogia   delle  questioni,  al  contenuto  dell'atto  di    &#13;
 intervento relativo ai giudizi di costituzionalità promossi  con  le    &#13;
 ordinanze  iscritte  ai  nn. 776 e 787 del r.o. del 1997, già decisi    &#13;
 con sentenza n. 361 del 1998, nonché, per  i  giudizi  promossi  con    &#13;
 ordinanze  iscritte  ai  nn. 265 e 494 del r.o. del 1998, all'atto di    &#13;
 intervento relativo alla questione  sollevata  con  ordinanza  del  1    &#13;
 dicembre  1997  dal  Tribunale  di  Lecco,  fissata  per la camera di    &#13;
 consiglio del 10 febbraio 1999, (r.o. n. 112 del 1998).                  &#13;
   Considerato che tutte le  ordinanze  di  rimessione,  muovendo  dal    &#13;
 quadro  normativo risultante dalle modifiche introdotte dalla legge 7    &#13;
 agosto  1997,  n.  267,  sottopongono  a   censura   il   regime   di    &#13;
 inutilizzabilità  ai  fini della decisione, in mancanza dell'accordo    &#13;
 delle parti, delle  dichiarazioni  rese  nella  fase  delle  indagini    &#13;
 preliminari  dall'imputato in procedimento connesso che si avvalga in    &#13;
 dibattimento della facoltà di non rispondere;                           &#13;
     che i giudizi, attesa la sostanziale identità  delle  questioni,    &#13;
 vanno riuniti;                                                           &#13;
     che,  successivamente  alla  emissione  delle  ordinanze,  questa    &#13;
 Corte, con sentenza n. 361 del 1998, ha inciso  sul  predetto  quadro    &#13;
 normativo, dichiarando la illegittimità costituzionale, tra l'altro,    &#13;
 dell'art.    513,  comma  2,  ultimo periodo, del codice di procedura    &#13;
 penale "nella parte in cui non prevede che,  qualora  il  dichiarante    &#13;
 rifiuti  o comunque ometta in tutto o in parte di rispondere su fatti    &#13;
 concernenti la  responsabilità  di  altri  già  oggetto  delle  sue    &#13;
 precedenti  dichiarazioni,  in mancanza dell'accordo delle parti alla    &#13;
 lettura si applica l'art.   500, commi  2-bis  e  4,  del  codice  di    &#13;
 procedura penale";                                                       &#13;
     che  pertanto  occorre  restituire gli atti ai giudici rimettenti    &#13;
 affinché  verifichino  se,  alla   luce   della   nuova   disciplina    &#13;
 applicabile  a  seguito  della sentenza n. 361 del 1998, le questioni    &#13;
 sollevate siano tuttora rilevanti;                                       &#13;
     che  per   quanto   concerne   la   questione   di   legittimità    &#13;
 costituzionale   dell'art.   514   cod.   proc.  pen.  sollevata,  in    &#13;
 riferimento agli artt.   3, 24, 101,  secondo  comma,  e  111,  primo    &#13;
 comma,  della Costituzione dal Tribunale di Avezzano, con la sentenza    &#13;
 richiamata questa Corte ha dichiarato l'inammissibilità  di  analoga    &#13;
 questione  sul  presupposto che "l'art. 514 non ha autonomo contenuto    &#13;
 normativo rispetto alle  regole  di  utilizzazione  probatoria  delle    &#13;
 dichiarazioni rese in precedenza";                                       &#13;
     che   pertanto   la   questione   va   dichiarata  manifestamente    &#13;
 inammissibile.                                                           &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi:                                                     &#13;
     dichiara  la  manifesta  inammissibilità  della   questione   di    &#13;
 legittimità  costituzionale  dell'art.  514  del codice di procedura    &#13;
 penale, sollevata, in riferimento agli  artt.  3,  24,  101,  secondo    &#13;
 comma,  e  111,  primo  comma,  della  Costituzione  dal Tribunale di    &#13;
 Avezzano con l'ordinanza in epigrafe;                                    &#13;
     ordina  la  restituzione  degli  atti  al Tribunale di Milano, al    &#13;
 Tribunale militare di Torino, al Tribunale di Udine, al Tribunale per    &#13;
 i minorenni di Roma,  al  Tribunale  di  Avezzano,  al  Tribunale  di    &#13;
 Pescara  e  al  pretore  di  Pescara  in  relazione alla questione di    &#13;
 legittimità costituzionale dell'art. 513, comma  2,  del  codice  di    &#13;
 procedura penale.                                                        &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 febbraio 1999.                             &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 4 marzo 1999.                             &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
