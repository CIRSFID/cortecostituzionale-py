<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>971</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:971</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/10/1988</data_decisione>
    <data_deposito>14/10/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 236 delle norme    &#13;
 della Regione siciliana di cui  al  d.l.p.  29  ottobre  1955,  n.  6    &#13;
 (Ordinamento   amministrativo   degli   enti   locali  nella  Regione    &#13;
 siciliana) a) e art. 85  lett.  a)  d.P.R.  10  gennaio  1957,  n.  3    &#13;
 (Statuto  degli impiegati civili dello Stato), promosso con ordinanza    &#13;
 emessa l'8 novembre 1985 dal TAR per la Sicilia Sez. di  Catania  sul    &#13;
 ricorso proposto da Iuvara Vincenzo contro Comune di Ispica, iscritta    &#13;
 al n. 248 del registro ordinanze 1988  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  23, prima serie speciale, dell'anno    &#13;
 1988;                                                                    &#13;
    Udito  nella  camera  di  consiglio del 12 ottobre 1988 il Giudice    &#13;
 relatore Giuseppe Borzellino;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Con ordinanza dell'8 novembre 1985 pervenuta addì 18 maggio    &#13;
 1988 (R.O. n. 248/88) il  Tribunale  amministrativo  regionale  della    &#13;
 Sicilia  - Sezione di Catania - rimetteva a questa Corte la questione    &#13;
 di legittimità costituzionale dell'art. 236 in allegato  al  decreto    &#13;
 legislativo  presidenziale (della Regione siciliana) 29 ottobre 1955,    &#13;
 n. 6 (Ordinamento amministrativo  degli  enti  locali  nella  Regione    &#13;
 siciliana)  e  dell'art.  85  lett.  a)  d.P.R. 10 gennaio 1957, n. 3    &#13;
 (Statuto degli impiegati civili dello Stato).                            &#13;
    In  punto  di  fatto,  Iuvara  Vincenzo,  dipendente del Comune di    &#13;
 Ispica,  destituito  "di   diritto",   in   forza   delle   precitate    &#13;
 disposizioni,  senza cioè procedimento disciplinare di sorta poiché    &#13;
 già irrevocabilmente condannato in  sede  penale  per  il  reato  di    &#13;
 peculato  (art.  314  c.p.),  aveva  ricorso avverso il provvedimento    &#13;
 deliberato dalla Giunta municipale.                                      &#13;
    2.   -   Il   Collegio   remittente   sospetta  di  illegittimità    &#13;
 costituzionale la normativa indicata  che  inciderebbe  sul  disposto    &#13;
 dell'art.  3 Cost., per la irragionevolezza di una disciplina rigida,    &#13;
 che contrasta col "principio generale di graduazione  della  sanzione    &#13;
 alla   gravità  del  fatto-reato".  Risulterebbero  incisi  anche  i    &#13;
 successivi  artt.  4  e  35,  poiché  il  provvedimento  produrrebbe    &#13;
 senz'altro  "l'effetto  della  perdita  del lavoro", nonché, ancora,    &#13;
 l'art.  97,  impedendosi  -  si  assume  -  "l'azione  amministrativa    &#13;
 adeguata".<diritto>Considerato in diritto</diritto>1.1 - L'art. 85 lett. a) del d.P.R. 10 gennaio 1957, n. 3 (Statuto    &#13;
 degli impiegati civili  dello  Stato),  direttamente  applicabile  ai    &#13;
 dipendenti  degli enti locali della Sicilia per effetto dell'art. 236    &#13;
 dell'ordinamento  amministrativo  degli  enti  locali  nella  Regione    &#13;
 siciliana  (d.l.p.  29  ottobre  1955,  n. 6) dispone che l'impiegato    &#13;
 incorre nella destituzione, escluso il procedimento  disciplinare,  a    &#13;
 seguito  di  condanna per taluni delitti specificamente elencati, fra    &#13;
 cui il peculato, così come dedotto in fattispecie.                      &#13;
    1.2   -   Il   Collegio   remittente   dubita  della  legittimità    &#13;
 costituzionale di tale  normativa  per  la  rigidità  della  massima    &#13;
 sanzione  espulsiva,  senza  cioè  che  attraverso  il  procedimento    &#13;
 disciplinare sia possibile  operare,  nella  misura  della  sanzione,    &#13;
 alcuna  graduazione riferita al caso concreto: in tal modo verrebbero    &#13;
 a esser vulnerati, oltre la tutela del lavoro (artt. 4 e  35)  e  del    &#13;
 buon  andamento  amministrativo (art. 97), i principi fondamentali di    &#13;
 ragionevolezza chiaramente desumibili dall'art. 3 Cost.                  &#13;
    2.1 - La questione è fondata.                                        &#13;
    La  Corte  ha  già  avuto  modo  di  considerare,  per  identiche    &#13;
 fattispecie, come  l'ordinamento  appaia  vieppiù  orientato,  oggi,    &#13;
 verso  la  esclusione  di  sanzioni  rigide,  avulse da un confacente    &#13;
 rapporto di adeguatezza col caso concreto ed ha osservato esser  ciò    &#13;
 largamente  tendenziale - in adempimento del principio di eguaglianza    &#13;
 - nell'area punitiva penale e con identica incidenza anche nel  campo    &#13;
 disciplinare amministrativo (sent. n. 270 del 1986).                     &#13;
    La  necessità  di  razionalizzare  il sistema, in atto stemperato    &#13;
 nell'indistinto poiché diverse e difformi in parte le corrispondenti    &#13;
 norme  contenute  nei  vari  ordinamenti  per  i pubblici dipendenti,    &#13;
 rivelava, tuttavia, che  i  rimedi  esaustivi  andavano  assunti  dal    &#13;
 Parlamento,    dovendosi   operare   scelte   globali   a   fini   di    &#13;
 omogeneizzazione, in punto, dell'intero comparto pubblico.               &#13;
    2.2  -  Nuovamente  investita  della questione la Corte deve tener    &#13;
 conto che, in conformità  alle  premesse  affermazioni,  un  recente    &#13;
 disegno  di  legge, volto a modificare talune norme del codice penale    &#13;
 in materia di circostanze attenuanti e  di  sospensione  condizionale    &#13;
 della  pena, contiene disposizioni in ordine all'oggetto dell'odierna    &#13;
 fattispecie, diretta a rendere inoperante, infatti,  la  destituzione    &#13;
 di  diritto  limitatamente  ai  casi di sospensione condizionale. Non    &#13;
 rileva qui esame di sorta sui limiti subiettivi  cui  il  legislatore    &#13;
 intenderebbe  circoscrivere  -  ma comunque mantenere - la menzionata    &#13;
 sanzione rigida; va  favorevolmente  considerato,  tuttavia,  che  si    &#13;
 intende comunque perseguire, nella sede legislativa, la riferibilità    &#13;
 univoca a tutti i pubblici dipendenti.                                   &#13;
    Sicché  appare  di certo tendenzialmente concretato quell'intento    &#13;
 di adeguamento delle scelte ai criteri di omogeneizzazione  emergenti    &#13;
 dalla  legge-quadro  sul  pubblico impiego (29 marzo 1983, n. 93) che    &#13;
 operato da un ramo del Parlamento (il disegno  ha  ottenuto  il  voto    &#13;
 della  Camera  e trovasi ora presso il Senato: doc. n. 1239) consente    &#13;
 ora alla Corte - che ne  aveva  chiaramente  avvertita  la  pressante    &#13;
 esigenza - di dispiegare, senz'ulteriori remore, la propria verifica.    &#13;
    3.  -  L'indispensabile gradualità sanzionatoria, ivi compresa la    &#13;
 misura massima destitutoria, importa - adunque - che  le  valutazioni    &#13;
 relative siano ricondotte, ognora, alla naturale sede di valutazione:    &#13;
 il procedimento disciplinare, in difetto di che ogni  relativa  norma    &#13;
 risulta  incoerente,  per  il  suo  automatismo,  e  conseguentemente    &#13;
 irrazionale ex art. 3 Cost.                                              &#13;
   Assorbita    ogni   altra   questione,   va   dichiarata   pertanto    &#13;
 l'illegittimità costituzionale dell'art.  85  lett.  a)  d.P.R.   10    &#13;
 gennaio  1957,  n.  3 e dell'art. 236 delle norme per gli enti locali    &#13;
 nella Regione siciliana di cui al d.l.p. 29 ottobre 1955 n. 6,  nella    &#13;
 parte  in  cui  in  luogo  del  mero provvedimento di destituzione di    &#13;
 diritto non prevedono l'esperimento del procedimento disciplinare.       &#13;
    In  conseguenza  di  quanto  sin qui considerato e in applicazione    &#13;
 dell'art. 27 della legge 11 marzo 1953 n.  87  va  dichiarata,  negli    &#13;
 stessi  termini, l'illegittimità costituzionale dell'art. 247 r.d. 3    &#13;
 marzo 1934, n. 383, nel testo sostituito con legge 27 giugno 1942, n.    &#13;
 851;  dell'art.  66  lett.  a)  d.P.R.  15  dicembre  1969,  n. 1229;    &#13;
 dell'art. 1, secondo comma, della legge 13 maggio 1975,  n.  157  (in    &#13;
 relazione  all'art. 85, lett. a), d.P.R. n. 3 del 1957); dell'art. 57    &#13;
 lett. a) d.P.R. 20 dicembre 1979, n. 761; dell'art. 8 lett. a) d.P.R.    &#13;
 25 ottobre 1981, n. 737, tutti specificati in dispositivo.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  dell'art.  85  lett. a)    &#13;
 d.P.R. 10 gennaio 1957, n. 3 (Statuto degli  impiegati  civili  dello    &#13;
 Stato)  e dell'art. 236 delle norme della regione siciliana di cui al    &#13;
 d.l.p. 29 ottobre 1955, n. 6 (Ordinamento amministrativo  degli  enti    &#13;
 locali  nella  Regione sicilian a), nella parte in cui non prevedono,    &#13;
 in luogo del provvedimento di destituzione di diritto,  l'apertura  e    &#13;
 lo svolgimento del procedimento disciplinare;                            &#13;
    Dichiara,  in applicazione dell'art. 27 della l. 11 marzo 1953, n.    &#13;
 87,  e  negli  stessi   termini   di   cui   al   precedente   punto,    &#13;
 l'illegittimità costituzionale degli articoli:                          &#13;
      247   r.d.   3  marzo  1934,  n.  383  (T.u.  legge  comunale  e    &#13;
 provinciale), nel testo sostituito con legge 27 giugno 1942, n. 851;     &#13;
      66  lett. a) d.P.R. 15 dicembre 1959, n. 1229 (Ordinamento degli    &#13;
 ufficiali giudiziari e degli aiutanti ufficiali giudiziari);             &#13;
      1,  comma  secondo,  l. 13 maggio 1975, n. 157 (estensione delle    &#13;
 norme dello Statuto degli impiegati civili dello  Stato  agli  operai    &#13;
 dello Stato);                                                            &#13;
      57 lett. a) d.P.R. 20 dicembre 1979, n. 761 (Stato giuridico del    &#13;
 personale delle unità sanitarie locali);                                &#13;
      8 lett. a) d.P.R. 25 ottobre 1981, n. 737 (Sanzioni disciplinari    &#13;
 per  il  personale  dell'Amministrazione  di  pubblica  sicurezza   e    &#13;
 regolamentazione dei relativi procedimenti).                             &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 ottobre 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: BORZELLINO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 14 ottobre 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
