<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1980</anno_pronuncia>
    <numero_pronuncia>183</numero_pronuncia>
    <ecli>ECLI:IT:COST:1980:183</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/12/1980</data_decisione>
    <data_deposito>22/12/1980</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. &#13;
 ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCA RONE - &#13;
 Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 177 bis dei  &#13;
 codice di procedura penale, promosso con ordinanza emessa il  10  marzo  &#13;
 1976  dal  tribunale  di Milano sull'istanza di incidente di esecuzione  &#13;
 proposta dal Lazic Milomir, iscritta al n. 481 del  registro  ordinanze  &#13;
 1976  e pubblicata nella Gazzetta Ufficiale della Repubblica n. 246 del  &#13;
 15 settembre 1976.                                                       &#13;
     Udito nella camera di consiglio del  30  ottobre  1980  il  Giudice  &#13;
 relatore Alberto Malagugini.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il tribunale di Milano, in sede di incidente di esecuzione promosso  &#13;
 da  Lazic  Milomir  avverso  ordine di carcerazione emesso a seguito di  &#13;
 sentenza di condanna in contumacia, con  ordinanza  in  data  10  marzo  &#13;
 1976,  ha  osservato che all'imputato il decreto di citazione era stato  &#13;
 notificato col rito degli irreperibili;  che  il  domicilio  all'estero  &#13;
 dell'imputato stesso era noto al giudice del dibattimento; che tuttavia  &#13;
 l'imputato,  già  sottoposto a custodia preventiva, né all'atto della  &#13;
 scarcerazione per libertà provvisoria, né successivamente, era  stato  &#13;
 invitato  a  dichiarare od eleggere domicilio per le notificazioni, non  &#13;
 essendo previsto un simile adempimento dall'art. 177 bis c.p.p.  Questa  &#13;
 disciplina, secondo il tribunale, sarebbe sospetta di contrasto con gli  &#13;
 artt.  3 e 24 Cost. (e la questione sarebbe rilevante nell'incidente di  &#13;
 esecuzione, concernendo la validità del giudizio e quindi  del  titolo  &#13;
 esecutivo).                                                              &#13;
     L'argomentazione muove dalla sentenza n.  31 del 1965, con la quale  &#13;
 la  Corte  costituzionale  ha dichiarato illegittimo il citato art. 177  &#13;
 bis  c.p.p.,  nei  limiti  in  cui   consente   che   il   decreto   di  &#13;
 irreperibilità  sia emesso prima di un congruo termine per la elezione  &#13;
 di domicilio. La previsione di un tale termine - osserva il  giudice  a  &#13;
 quo   -   "ha   come   ovvio   presupposto   la   conoscenza  da  parte  &#13;
 dell'interessato dell'esistenza della facoltà medesima".                &#13;
     Ciò  non  si verifica nel caso dell'imputato che dimori all'estero  &#13;
 in pendenza di un procedimento penale a suo carico, di cui  abbia  già  &#13;
 avuto  comunicazione:  allo  stesso le norme previste dall'art. 177 bis  &#13;
 c.p.p. riservano un trattamento differenziato rispetto all'imputato che  &#13;
 sì trovi all'estero prima dello inizio del procedimento a  suo  carico  &#13;
 (nell'ovvio  presupposto  per  entrambi  i casi, che tale domicilio sia  &#13;
 noto).                                                                   &#13;
     A  quest'ultimo,  infatti,  l'organo   procedente   è   tenuto   a  &#13;
 trasmettere  non  solo  avviso  di  procedimento ma altresì "invito" a  &#13;
 dichiarare o eleggere domicilio per la notificazione degli atti.         &#13;
     Né l'art. 177 bis c.p.p., né  altre  norme,  peraltro,  prevedono  &#13;
 simile  invito  all'imputato che dimori all'estero successivamente, sì  &#13;
 che l'art. 177 bis c.p.p., come formulato,  consente  un  indebolimento  &#13;
 del  diritto di difesa di quest'ultimo, e una disparità di trattamento  &#13;
 rispetto ai primo.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il   tribunale   di   Milano   dubita   della   legittimità  &#13;
 costituzionale  dell'art.  177 bis del codice di procedura penale nella  &#13;
 parte in cui non prevede  che  l'imputato  dimorante  allo  estero  (ad  &#13;
 indirizzo  conosciuto) e che in qualunque modo sia già informato della  &#13;
 pendenza  a  suo  carico  di  un  procedimento  penale   sia   avvisato  &#13;
 dall'autorità procedente anche della facoltà di dichiarare o eleggere  &#13;
 domicilio ai fini delle notificazioni.                                   &#13;
     La  questione  è  stata  sollevata  nel  corso  di un incidente di  &#13;
 esecuzione proposto  da  imputato  che,  già  detenuto  in  attesa  di  &#13;
 giudizio  e  rimesso  in  libertà  provvisoria, era stato giudicato in  &#13;
 contumacia, dopo che il decreto di citazione gli era  stato  notificato  &#13;
 nei modi previsti dall'art. 170 cod. proc. pen.                          &#13;
     La questione non è fondata.                                         &#13;
     2.  -  Il  vigente  sistema  processuale  penale  è informato alla  &#13;
 esigenza di garantire la possibilità di instaurare il  contraddittorio  &#13;
 con   le   parti   interessate,  in  particolare  con  l'imputato,  per  &#13;
 l'esercizio del diritto di difesa.                                       &#13;
     Essenziale, a tale fine, è che la  notizia  del  procedimento  sia  &#13;
 portata   nella   sfera  di  conoscibilità  dell'imputato  stesso  per  &#13;
 l'esercizio delle facoltà che, in ragione di tale  sua  qualità,  gli  &#13;
 sono riconosciute dall'ordinamento.                                      &#13;
     Specificatamente,   con  riferimento  alla  fattispecie  in  esame,  &#13;
 l'imputato detenuto che debba essere scarcerato per causa  diversa  dal  &#13;
 proscioglimento  definitivo, nell'atto della scarcerazione ha l'obbligo  &#13;
 di dichiarare od eleggere domicilio per le notificazioni; dichiarazione  &#13;
 che, ricevuta dal direttore dello stabilimento carcerario, deve  essere  &#13;
 immediatamente  comunicata all'autorità giudiziaria che ha disposto la  &#13;
 scarcerazione (art. 171, terzo comma, c.p.p.).                           &#13;
     Del pari, il giudice, nel primo atto del  procedimento  in  cui  è  &#13;
 presente  l'imputato,  oltre  ad  invitarlo  a  scegliere un difensore,  &#13;
 ovvero,  in  difetto,  a  nominargliene  uno  d'ufficio,  lo  invita  a  &#13;
 dichiarare  o  eleggere  il  domicilio  per  le  notificazioni  a norma  &#13;
 dell'art.  171 c.p.p. (art. 304 c.p.p.).                                 &#13;
     Infine, nel caso di  imputato  dimorante  all'estero  ad  indirizzo  &#13;
 conosciuto,   l'art.  177  bis,  primo  comma,  c.p.p.  impone  l'invio  &#13;
 dell'avviso di procedimento con il contestuale invito a  dichiarare  od  &#13;
 eleggere domicilio nel territorio nazionale.                             &#13;
     3. - Evidente è la specificità della fattispecie disciplinata dal  &#13;
 primo comma dell'art. 177 bis c.p.p., che impone l'invio dell'avviso di  &#13;
 procedimento  con  il  contestuale  invito  a  dichiarare  od  eleggere  &#13;
 domicilio nel territorio nazionale all'imputato dimorante all'estero ad  &#13;
 indirizzo conosciuto, nel presupposto che costui non sia  già  entrato  &#13;
 in contatto con l'autorità giudiziaria procedente a suo carico.         &#13;
     Il secondo comma del medesimo art. 177 bis c.p.p. disciplina (oltre  &#13;
 a  quella, che qui non interessa, dell'imputato dimorante all'estero ad  &#13;
 indirizzo sconosciuto) l'ipotesi dell'imputato dimorante all'estero  ad  &#13;
 indirizzo  noto  al  quale  sia  stata  inviata  e risulti pervenuta la  &#13;
 raccomandata  contenente  l'avviso  di  procedimento   e   l'invito   a  &#13;
 dichiarare  od  eleggere  domicilio  nel  territorio  nazionale  per le  &#13;
 notificazioni e che non abbia aderito  all'invito  O,  pur  aderendovi,  &#13;
 abbia reso dichiarazioni insufficienti o inidonee. Soltanto all'inerzia  &#13;
 o  all'incuria  dell'imputato  che  abbia  ricevuto  avviso  ed  invito  &#13;
 consegue il ricorso all'art. 170 c.p.p. (in modo assolutamente identico  &#13;
 a quanto previsto dall'art. 171, quinto comma, c.p.p., per l'ipotesi in  &#13;
 cui manchino o  siano  insufficienti  o  inidonee  la  dichiarazione  o  &#13;
 l'elezione  di  domicilio, nel qual caso le notificazioni sono eseguite  &#13;
 mediante  deposito  nella   cancelleria   o   segreteria   dell'ufficio  &#13;
 giudiziario procedente e con immediato avviso al difensore).             &#13;
     Sotto  il  profilo  denunziato, di una ingiustificata disparità di  &#13;
 trattamento, per ciò che attiene all'esercizio del diritto di  difesa,  &#13;
 tra  l'imputato  dimorante  all'estero  ad indirizzo noto che non abbia  &#13;
 avuto previa conoscenza del procedimento pendente a suo carico e quello  &#13;
 dell'imputato anch'esso dimorante allo estero  ad  indirizzo  noto  che  &#13;
 tale  conoscenza,  invece,  abbia  avuto,  in uno dei modi previsti dal  &#13;
 codice di rito, la questione non è,  quindi,  fondata.  Evidente,  è,  &#13;
 infatti,  la differenza tra le situazioni considerate, mentre va tenuto  &#13;
 presente, altresì, che,  in  entrambi  i  casi,  il  decreto  previsto  &#13;
 dall'art.   170  viene  emesso  soltanto  quando  l'imputato,  posto  a  &#13;
 conoscenza del procedimento a suo carico, ha omesso di avvalersi  o  si  &#13;
 è   avvalso  in  modo  insufficiente  o  inidoneo  della  facoltà  di  &#13;
 dichiarare od eleggere domicilio nel territorio nazionale.               &#13;
     Affermata la legittimità costituzionale  della  norma  denunziata,  &#13;
 non   spetta,   invece,  a  questa  Corte,  giudice  delle  leggi,  né  &#13;
 individuare  le  violazioni   di   norme   processuali,   eventualmente  &#13;
 verificatesi  nel  procedimento  a  quo, né indicare se e quali rimedi  &#13;
 siano adottabili per ovviare ad esse.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  177  bis  del  codice  di  procedura  penale  sollevata  dal  &#13;
 tribunale  di  Milano,  in  riferimento  agli  artt.  3  e   24   della  &#13;
 Costituzione con l'ordinanza indicata in epigrafe.                       &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 16 dicembre 1980.       &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA   -   EDOARDO   VOLTERRA  -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   - LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
