<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>59</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:59</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>10/02/2003</data_decisione>
    <data_deposito>28/02/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per ammissibilità di conflitto di attribuzione tra poteri dello Stato sorto a seguito della delibera del Senato della Repubblica del 23 giugno 1999 relativa alla insindacabilità del comportamento del sen. Donato Manfroi promosso dalla Corte d'Appello di Venezia sez. 2° penale con ricorso depositato il 7 maggio 2002 ed iscritto al n. 220 del registro ammissibilità conflitti. &#13;
    Udito nella camera di consiglio del 15 gennaio 2003 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che con ricorso del 26 aprile 2002, depositato nella cancelleria della Corte il 7 maggio 2002, la Corte di appello di Venezia - nel corso del  procedimento penale promosso nei confronti del senatore Donato Manfroi, condannato in primo grado perché ritenuto responsabile del reato di cui all'art. 340 cod. pen., per avere, in qualità di sindaco del Comune di Cencenighe Agordino, nei giorni 1-2 marzo 1996, interrotto i servizi comunali, chiudendo d'autorità l'accesso al pubblico al palazzo in cui hanno sede gli uffici e ordinando al personale dipendente di non rispondere alle telefonate e di staccare l'apparato fax - ha sollevato conflitto di attribuzioni tra poteri dello Stato nei confronti del Senato della Repubblica, in relazione alla deliberazione adottata dalla Assemblea il 23 giugno 1999 (doc. IV-quater, n. 42), con la quale non è stata approvata la proposta della Giunta delle elezioni e delle immunità parlamentari di dichiarare che il fatto per il quale è in corso l'indicato procedimento penale non concerne opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e non ricade, pertanto, nell'ipotesi di cui all'art. 68, primo comma, della Costituzione; &#13;
    che, a parere della autorità ricorrente, la condotta oggetto della imputazione esulerebbe totalmente dall'esercizio delle funzioni parlamentari - così come peraltro evidenziato nella Relazione della Giunta, la cui proposta è stata disattesa dalla Assemblea - posto che, pur manifestandosi la natura politica di «un gesto di protesta», tale condotta era stata posta in essere dal senatore Manfroi nella sua qualità di sindaco del Comune di Cencenighe Agordino e non già nell'esercizio del mandato parlamentare, rispetto al quale difettava qualsiasi collegamento, con correlativa inapplicabilità della garanzia prevista dall'art. 68, primo comma, della Costituzione; &#13;
    che, pertanto, non reputandosi conforme all'ordinamento costituzionale la deliberazione assunta al riguardo dal Senato della Repubblica, la Corte di appello di Venezia solleva conflitto di attribuzione in ordine «al corretto uso del potere di decidere sulla sussistenza dei presupposti di applicabilità dell'art. 68, primo comma, della Costituzione», come esercitato dal Senato con l'anzidetta delibera, della quale viene pertanto richiesto l'annullamento. &#13;
    Considerato che, in questa fase, occorre deliberare esclusivamente se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzioni tra poteri dello Stato, impregiudicata ogni definitiva decisione anche in ordine all'ammissibilità (art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87); &#13;
    che, quanto al requisito soggettivo, la Corte di appello di Venezia è legittimata a sollevare conflitto, essendo competente a dichiarare definitivamente, per il procedimento del quale è investita, la volontà del potere cui appartiene, in ragione dell'esercizio delle funzioni giurisdizionali svolte in posizione di indipendenza costituzionalmente garantita; &#13;
    che, parimenti, il Senato della Repubblica, che ha deliberato nel senso della insindacabilità della condotta posta in essere da un proprio membro, è legittimato ad essere parte del conflitto, essendo competente a dichiarare definitivamente la volontà del potere che rappresenta; &#13;
    che, per quanto attiene al profilo oggettivo del conflitto, la Corte ricorrente denuncia la lesione della propria sfera di attribuzioni, garantita da norme costituzionali, in conseguenza della deliberazione - ritenuta illegittima - con la quale il Senato della Repubblica ha qualificato come insindacabile la condotta del parlamentare, cui si riferisce il giudizio, in quanto compresa nell'esercizio delle funzioni parlamentari (art. 68, primo comma, della Costituzione); &#13;
    che, pertanto, esiste la materia di un conflitto la cui risoluzione spetta alla competenza della Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il ricorso per conflitto di attribuzione proposto dalla Corte di appello di Venezia nei confronti del Senato della Repubblica con l'atto introduttivo indicato in epigrafe; &#13;
    dispone: &#13;
che la cancelleria della Corte dia immediata comunicazione della presente ordinanza alla Corte di appello di Venezia ricorrente; &#13;
che l'atto introduttivo e la presente ordinanza siano, a cura del ricorrente, notificati al Senato della Repubblica entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova della eseguita notificazione, presso la cancelleria della Corte entro il termine di venti giorni dalla notificazione stessa, a norma dell'art. 26, terzo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 febbraio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Giovanni Maria FLICK, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 28 febbraio 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
