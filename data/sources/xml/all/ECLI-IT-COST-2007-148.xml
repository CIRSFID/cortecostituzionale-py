<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>148</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:148</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Franco Gallo</relatore_pronuncia>
    <redattore_pronuncia>Franco Gallo</redattore_pronuncia>
    <data_decisione>18/04/2007</data_decisione>
    <data_deposito>27/04/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, nota II-bis, comma 4, ultimo periodo, della parte I della tariffa allegata al decreto del Presidente della Repubblica 26 aprile 1986, n. 131 (Approvazione del testo unico delle disposizioni concernenti l'imposta di registro), promosso con ordinanza depositata il 21 febbraio 2006 dalla Commissione tributaria provinciale di Udine nel giudizio vertente tra Elvio Galasso e l'Agenzia delle Entrate - Ufficio di Cervignano del Friuli, iscritta al n. 543 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 49, prima serie speciale, dell'anno 2006. 
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; 
    udito nella camera di consiglio del  21 marzo 2007 il Giudice relatore Franco Gallo.  
    Ritenuto che, con ordinanza depositata il 21 febbraio 2006, la Commissione tributaria provinciale di Udine – nel corso di un giudizio promosso da un contribuente nei confronti dell'Agenzia delle entrate, avverso un avviso di liquidazione con il quale erano state revocate le agevolazioni fiscali concesse per l'acquisto di una unità immobiliare abitativa – ha sollevato, in riferimento agli artt. 3 e 35, primo e quarto comma, della Costituzione, questione di legittimità costituzionale dell'ultimo periodo del comma 4 della nota II-bis dell'art. 1 della parte I della tariffa allegata al d.P.R. 26 aprile 1986, n. 131 (Approvazione del testo unico delle disposizioni concernenti l'imposta di registro), «nella parte in cui non prevede che, al fine di evitare la decadenza dalle agevolazioni fiscali concesse per il precedente acquisto, l'acquisto di altro immobile si possa perfezionare con atti traslativi a titolo oneroso della proprietà di case di abitazione non di lusso e con atti traslativi o costitutivi della nuda proprietà, dell'usufrutto, dell'uso e dell'abitazione relativi alle stesse e nella parte in cui, invece, prevede l'obbligo di adibire a propria abitazione principale l'altro immobile acquistato»; 
    che, in punto di fatto, il giudice rimettente riferisce che l'Agenzia delle entrate aveva revocato dette agevolazioni, perché il contribuente non aveva adibito a propria abitazione il fabbricato acquistato; 
    che, in punto di diritto, la Commissione tributaria provinciale osserva che la disciplina delle agevolazioni per l'acquisto della prima casa, introdotta con la legge 22 aprile 1982, n. 168 (Misure fiscali per lo sviluppo dell'edilizia abitativa), è stata nel tempo ripetutamente modificata; 
    che il giudice rimettente rileva, in particolare, che la norma denunciata, quale modificata dall'art. 3, comma 131, della legge 28 dicembre 1995, n. 549 (Misure di razionalizzazione della finanza pubblica), si applica agli atti traslativi a titolo oneroso della proprietà di case di abitazione non di lusso e agli atti traslativi costitutivi della nuda proprietà, dell'usufrutto, dell'uso e dell'abitazione relativi alle stesse e che la decadenza da detta agevolazione si verifica nel caso in cui l'immobile acquistato sia alienato entro cinque anni dal suo acquisto e non sia acquistato entro un anno un altro immobile da destinare ad abitazione principale; 
    che, secondo il rimettente, tale disciplina, distinguendo tra l'ipotesi di “primo acquisto” (avente ad oggetto – sempre per il giudice a quo – «il trasferimento della proprietà o della nuda proprietà, o la costituzione dell'usufrutto, dell'uso o dell'abitazione sull'immobile acquistato», senza che la legge richieda la destinazione dell'immobile ad abitazione principale) e l'ipotesi di “riacquisto” (avente ad oggetto l'acquisto di un altro immobile da destinare ad abitazione principale), genererebbe una serie di conseguenze paradossali: a) «l'acquirente della nuda proprietà di un immobile, che rivenda il suo diritto nei cinque anni dall'acquisto e, che, riacquisti, entro un anno dalla alienazione, un diritto di nuda proprietà su altro immobile, decade dall'agevolazione sul precedente acquisto, perché non può adempiere all'obbligo di destinare a propria abitazione principale il nuovo immobile, di cui ha acquistato solo la nuda proprietà»; b) decade dall'agevolazione anche «l'acquirente che effettui il riacquisto non nel comune dove ha la propria residenza, ma nel comune dove svolge la  
 
 
 
 
 
  propria attività e non vuole o non può trasferire in quel comune la propria residenza nei diciotto mesi successivi al riacquisto»; c) ugualmente, «decade l'acquirente trasferito all'estero per ragioni di lavoro che riacquisti l'immobile nel comune dove vi è la sede o il luogo di esercizio dell'attività del proprio datore di lavoro e non vuole o non può trasferire in quel comune la propria residenza»; d) «decade l'acquirente cittadino italiano emigrato all'estero, che riacquisti l'immobile in un qualunque comune del territorio italiano e che non vuole o non può rientrare in Italia e fissare in quel comune la propria residenza»; 
    che, su queste premesse, la norma censurata si porrebbe, per il giudice a quo, «in contrasto con il principio di eguaglianza previsto dall'art. 3 della Costituzione, perché vi è una ingiustificata disparità di trattamento tra il contribuente che effettua il "primo acquisto" e quello che effettua il "riacquisto" a fronte di situazioni identiche»; 
    che la norma contrasterebbe, inoltre, con il primo comma dell'art. 35 della Costituzione, «perché rende più disagevole il lavoro, nei limiti in cui impedisce il “riacquisto” dell'immobile che non si identifichi anche con l'abitazione principale», e con il quarto comma dello stesso articolo, «perché pregiudica la libertà di emigrazione, nei limiti in cui impedisce il “riacquisto” di un immobile senza il rientro in Italia»; 
    che il giudice rimettente afferma, infine, senza ulteriore motivazione, che la questione ha «rilevanza nel giudizio in corso»; 
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata manifestamente infondata; 
    che, quanto alla disparità di trattamento denunciata dal rimettente, la difesa erariale osserva che: a) il giudice a quo non ha considerato che la norma censurata può essere interpretata nel senso che la decadenza dalle agevolazioni fiscali è impedita dall'acquisto «di uno qualsiasi dei diritti indicati dal comma 1 (proprietà, nuda proprietà, usufrutto, uso, abitazione)»; b) la previsione della necessità che il secondo immobile acquistato venga adibito ad abitazione principale del contribuente non è irragionevole, «in quanto persegue lo scopo di escludere dal  
 
 
 
 
 
 godimento dei benefici fiscali le operazioni di compravendita di immobili […] aventi una finalità meramente speculativa»; 
    che, quanto alle censure sollevate con riferimento all'art. 35 Cost., la stessa difesa erariale sostiene che «la disciplina fiscale dell'acquisto non è ispirata ad esigenze di tutela del lavoro e della libertà di emigrazione, e non è assolutamente idonea a menomare l'esercizio dei relativi diritti». 
    Considerato che la Commissione tributaria provinciale di Udine dubita, in riferimento agli artt. 3 e 35, primo e quarto comma, della Costituzione, della legittimità costituzionale dell'ultimo periodo del comma 4 della nota II-bis dell'art. 1 della parte I della tariffa allegata al d.P.R. 26 aprile 1986, n. 131 (Approvazione del testo unico delle disposizioni concernenti l'imposta di registro); 
    che la disposizione censurata prevede che, nel caso in cui il contribuente che ha usufruito delle agevolazioni fiscali concesse per l'acquisto di una casa di abitazione non di lusso trasferisca l'immobile prima del decorso del termine di cinque anni dall'acquisto, egli possa evitare la decadenza dalle agevolazioni prevista dalla stessa legge per detto trasferimento infraquinquennale, ove proceda, entro un anno dall'alienazione dell'immobile, all'acquisto di altro immobile da adibire a propria abitazione principale; 
    che il rimettente censura la norma «nella parte in cui non prevede che, al fine di evitare la decadenza dalle agevolazioni fiscali concesse per il precedente acquisto, l'acquisto di altro immobile si possa perfezionare con atti traslativi a titolo oneroso della proprietà di case di abitazione non di lusso e con atti traslativi o costitutivi della nuda proprietà, dell'usufrutto, dell'uso e dell'abitazione relativi alle stesse e nella parte in cui, invece, prevede l'obbligo di adibire a propria abitazione principale l'altro immobile acquistato»; 
    che, nel descrivere la fattispecie sottoposta al suo esame, il rimettente si limita a riferire che l'Agenzia delle entrate, con l'avviso di liquidazione impugnato nel giudizio principale, aveva revocato dette agevolazioni, perché il contribuente non aveva adibito a propria abitazione il fabbricato acquistato, come previsto dalla norma censurata quale condizione per evitare la decadenza dalle agevolazioni; 
    che l'ordinanza di rimessione non precisa né il tempo dell'acquisto e della vendita dell'immobile che ha fruito originariamente delle agevolazioni, né quello dell'acquisto del nuovo immobile, né – ancora – se la vendita e gli acquisti immobiliari riguardino la proprietà o altro diritto reale, ma si limita ad affermare genericamente che la questione ha «rilevanza nel giudizio in corso»; 
    che tali carenze di descrizione della fattispecie, impedendo alla Corte di svolgere la necessaria verifica circa l'applicabilità della norma impugnata nel giudizio a quo e, quindi, circa l'incidenza della richiesta pronuncia sulla situazione soggettiva fatta valere, si risolvono in un'insufficiente motivazione sulla rilevanza della questione; 
    che, pertanto, la questione è manifestamente inammissibile, restando conseguentemente precluso l'esame del merito.  
    Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi  &#13;
LA CORTE COSTITUZIONALE  &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'ultimo periodo del comma 4 della nota II-bis dell'art. 1 della parte I della tariffa allegata al d.P.R. 26 aprile 1986, n. 131 (Approvazione del testo unico delle disposizioni concernenti l'imposta di registro), sollevata, in riferimento agli articoli 3 e 35, primo e quarto comma, della Costituzione, dalla Commissione tributaria provinciale di Udine con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 18 aprile 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Franco GALLO, Redattore  &#13;
Gabriella MELATTI, Cancelliere  &#13;
Depositata in Cancelleria il 27 aprile 2007.  &#13;
Il Cancelliere  &#13;
F.to: MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
