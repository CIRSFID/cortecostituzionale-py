<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>28</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:28</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/1982</data_decisione>
    <data_deposito>11/02/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - &#13;
 Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  137,  commi  &#13;
 primo e terzo, cod. proc. pen. (Uso della lingua italiana) promosso con  &#13;
 ordinanza   emessa  l'8  marzo  1977  dal  Tribunale  di  Trieste,  nel  &#13;
 procedimento penale a carico di Pahor Samo,  iscritta  al  n.  260  del  &#13;
 registro  ordinanze  1977  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 183 del 6 luglio 1977.                                     &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 21 ottobre 1981 il Giudice relatore  &#13;
 Michele Rossano;                                                         &#13;
     udito l'avvocato dello Stato Franco Chiarotti per il Presidente del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  sentenza  in  data  13  dicembre  1974  il  Pretore di Trieste  &#13;
 condannò Pahor Samo,  nato  a  Trbovlye  (Jugoslavia)  e  residente  a  &#13;
 Trieste,  quale colpevole del reato punito dall'articolo 651 del codice  &#13;
 penale, per aver rifiutato di dare indicazioni sulla propria  identità  &#13;
 personale a due vigili urbani.                                           &#13;
     Avverso   la  sentenza  il  Pahor  propose  appello,  invocando  la  &#13;
 sussistenza della causa di giustificazione dell'esercizio del  diritto,  &#13;
 fondato  sull'articolo  5 dello Statuto speciale allegato al Memorandum  &#13;
 del 5 ottobre 1954 tra Italia e Jugoslavia.    Egli  deduceva  di  aver  &#13;
 titolo  ad  esigere che il vigile urbano gli formulasse la richiesta di  &#13;
 indicazioni  sulla  sua  identità   personale   in   lingua   slovena,  &#13;
 direttamente  o per il tramite di un interprete, e che lo stesso vigile  &#13;
 rispondesse in lingua slovena, direttamente o  per  il  tramite  di  un  &#13;
 interprete, alle richieste di chiarimento sulla intimazione espressa in  &#13;
 lingua italiana.                                                         &#13;
     Durante il dibattimento di appello davanti al Tribunale di Trieste,  &#13;
 all'udienza  del  28  ottobre  1976, venne nominato un interprete della  &#13;
 Corte d'appello di Trieste, sul presupposto che il Pahor non conoscesse  &#13;
 la lingua italiana. All'udienza dell'8 marzo 1977 il Pahor, invitato ad  &#13;
 indicare le proprie  discolpe  a  mezzo  di  interprete,  contestò  la  &#13;
 validità  del  procedimento, poiché la citazione a giudizio era stata  &#13;
 formulata in lingua italiana, senza la traduzione in lingua slovena.     &#13;
     Il  pubblico   ministero   chiese   al   Tribunale   di   procedere  &#13;
 all'immediato  giudizio del Pahor per il reato di cui all'articolo 137,  &#13;
 terzo comma, del codice di procedura penale, per essersi  rifiutato  di  &#13;
 esprimersi in lingua italiana, pur conoscendola.                         &#13;
     Con  ordinanza  pronunciata nella stessa udienza dell'8 marzo 1977,  &#13;
 il  Tribunale  di  Trieste  ha  sollevato  d'ufficio  le  questioni  di  &#13;
 legittimità  costituzionale  dell'articolo  137, prima e ultima parte,  &#13;
 del codice di procedura penale, in riferimento  agli  articoli  3  e  6  &#13;
 della Costituzione e alla legge costituzionale 31 gennaio 1963, n. 1.    &#13;
     Il primo comma dell'articolo impugnato prescrive che tutti gli atti  &#13;
 del  procedimento  penale  devono  essere compiuti in lingua italiana a  &#13;
 pena di nullità; il secondo comma punisce il rifiuto di esprimersi  in  &#13;
 lingua  italiana  da  parte di persona che la conosca, nonché la falsa  &#13;
 attestazione di ignorarla. Ad avviso del tribunale, tali norme sono  in  &#13;
 contrasto  con  l'articolo  3 della Costituzione, in quanto determinano  &#13;
 una ingiustificata disparità di trattamento tra gli appartenenti  alla  &#13;
 minoranza  di  lingua slovena nella Regione Friuli-Venezia Giulia e gli  &#13;
 appartenenti  alla  minoranza   di   lingua   tedesca   nella   Regione  &#13;
 Trentino-Alto Adige, ai quali è consentito da specifiche previsioni di  &#13;
 usare  la propria lingua nel processo penale. Le stesse norme sarebbero  &#13;
 in contrasto altresì con l'articolo 6 della Costituzione, in relazione  &#13;
 all'articolo 3  della  legge  costituzionale  31  gennaio  1963,  n.  1  &#13;
 (Statuto  della  Regione  Friuli-Venezia  Giulia),  perché  violano il  &#13;
 principio di tutela della minoranza di  lingua  slovena  nella  Regione  &#13;
 Friuli- Venezia Giulia.                                                  &#13;
     Nel  giudizio  davanti a questa Corte non si è costituita la parte  &#13;
 privata.  È  intervenuto  invece  il  Presidente  del  Consiglio   dei  &#13;
 ministri,  rappresentato  e  difeso dall'Avvocato Generale dello Stato,  &#13;
 con atto depositato il 22 luglio 1977, chiedendo che  le  questioni  di  &#13;
 legittimità  costituzionale  vengano  dichiarate infondate, sulla base  &#13;
 dei motivi di seguito esposti. Il Tribunale di Trieste  ha  esattamente  &#13;
 premesso  che  il  Memorandum  d'intesa di Londra del 1954 non è stato  &#13;
 recepito nella sua integralità  e  con  i  suoi  allegati  nel  nostro  &#13;
 ordinamento  giuridico.  Peraltro sono state adottate numerose misure a  &#13;
 favore della minoranza slovena del Friuli-Venezia Giulia,  anche  prima  &#13;
 del Memorandum, con atti legislativi statali, con ordinanze del Governo  &#13;
 Militare  Alleato,  rimaste  poi in vigore, con decreti del Commissario  &#13;
 Generale del Governo per il Territorio di Trieste,  con  semplici  atti  &#13;
 amministrativi, con le varie norme emanate dalla Regione Friuli-Venezia  &#13;
 Giulia.                                                                  &#13;
     Tutte   queste   misure   -   sostiene   l'Avvocatura   -  tutelano  &#13;
 adeguatamente la minoranza slovena e possono ritenersi applicative  del  &#13;
 suddetto  Memorandum  o  adottate  proprio  in  relazione  agli impegni  &#13;
 assunti con il Memorandum  stesso.    Le  medesime  misure,  in  quanto  &#13;
 riferibili  al  citato  atto  internazionale,  sono  state mantenute in  &#13;
 vigore anche dopo la ratifica degli accordi  italo-jugoslavi  di  Osimo  &#13;
 (legge  14  marzo  1977, n. 73) in base al disposto dell'articolo 3 del  &#13;
 trattato.                                                                &#13;
     Non  è  fondato  quindi  -  si  aggiunge  -  l'addotto   contrasto  &#13;
 dell'articolo 137 del codice di procedura penale con gli articoli 3 e 6  &#13;
 della  Costituzione  e  con  l'articolo  3  dello Statuto della Regione  &#13;
 Friuli-Venezia Giulia. Tale contrasto viene posto in relazione  con  le  &#13;
 norme  emanate per gli appartenenti al gruppo tedesco del Trentino-Alto  &#13;
 Adige, il quale presenta caratteristiche sostanziali che non  ricorrono  &#13;
 per  la  minoranza  slovena.  La  prima ha infatti consistenza numerica  &#13;
 assai superiore,  con  larghi  addensamenti  in  alcune  località.  Il  &#13;
 secondo  ha  per contro una consistenza globale molto ridotta e risulta  &#13;
 disperso sul territorio della Regione.  La differenza tra i due  gruppi  &#13;
 giustifica  un  diverso  regime  di  tutela,  anche per quanto concerne  &#13;
 l'equo rapporto che deve esistere tra  funzionamento  e  organizzazione  &#13;
 dei pubblici uffici locali (amministrativi e giudiziari) e le effettive  &#13;
 esigenze delle minoranze nel rapporto con gli uffici stessi.             &#13;
     Inoltre  -  si  conclude - l'appartenente al gruppo sloveno risulta  &#13;
 sufficientemente tutelato agli effetti della difesa  in  giudizio  dato  &#13;
 che  può esprimersi nella propria lingua, qualora non abbia conoscenza  &#13;
 della lingua italiana. Va sottolineato che, in applicazione della legge  &#13;
 19 luglio 1967, n. 568 sul  conferimento  dell'incarico  di  traduttore  &#13;
 interprete  presso  gli  uffici  giudiziari,  sono  stati  nominati sei  &#13;
 interpreti e traduttori per il distretto di Corte d'appello di Trieste,  &#13;
 di cui cinque per la lingua slovena e uno per la lingua serbo- croata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Il Tribunale di  Trieste  solleva  questione  di  legittimità  &#13;
 costituzionale  dei commi primo e terzo dell'articolo 137 del codice di  &#13;
 procedura penale per contrasto con l'articolo 3 della  Costituzione  in  &#13;
 quanto  determinano  ingiustificata  disparità  di trattamento tra gli  &#13;
 appartenenti  alle  minoranze   di   lingua   slovena   nella   Regione  &#13;
 Friuli-Venezia  Giulia e gli appartenenti alle minoranze alloglotte del  &#13;
 Trentino-Alto Adige e della Valle d'Aosta, ai quali è  consentito,  in  &#13;
 base  a  specifiche  normative  l'uso  della  lingua madre nel processo  &#13;
 penale. Le disposizioni citate  contrasterebbero  inoltre,  secondo  il  &#13;
 Tribunale  di  Trieste, anche con l'articolo 6 della Costituzione e con  &#13;
 l'articolo 3 della legge costituzionale 31 gennaio 1963, n. 1  (Statuto  &#13;
 della Regione Friuli-Venezia Giulia).                                    &#13;
     Le  questioni  non sono superate per il sopravvenire della legge 14  &#13;
 marzo 1977, n. 73, che autorizza la ratifica  e  dà  piena  ed  intera  &#13;
 esecuzione al trattato tra l'Italia e la Jugoslavia firmato ad Osimo il  &#13;
 10  novembre  1975, in quanto manca a tutt'oggi una normativa, che, sia  &#13;
 pure  limitatamente  all'uso  della  lingua  slovena,   dia   specifica  &#13;
 attuazione  al  contenuto  dell'articolo  8  di quel trattato.   Questa  &#13;
 situazione  di  carenza,  di  cui è doveroso sottolineare la gravità,  &#13;
 rende dunque necessaria la pronuncia di questa Corte.                    &#13;
     Peraltro, in termini di rilevanza, le questioni vanno  circoscritte  &#13;
 all'ambito  spaziale  in cui sono state sollevate e cioè al territorio  &#13;
 di Trieste, prescindendosi dalle soluzioni adottabili per le  minoranze  &#13;
 slovene  insediate  nelle  altre  parti  della  Regione  Friuli-Venezia  &#13;
 Giulia.                                                                  &#13;
     2.  -  Le  questioni  proposte  debbono  entrambe  dichiararsi  non  &#13;
 fondate.                                                                 &#13;
     Diversi  tuttavia  sono i motivi della duplice dichiarazione di non  &#13;
 fondatezza. Infatti, a proposito del primo comma dell'articolo 137  del  &#13;
 codice  di  procedura  penale  si  deve  ricordare  che la Costituzione  &#13;
 conferma per implicito che il nostro sistema riconosce l'italiano  come  &#13;
 unica  lingua  ufficiale,  da usare obbligatoriamente, salvo le deroghe  &#13;
 disposte a tutela dei  gruppi  linguistici  minoritari,  da  parte  dei  &#13;
 pubblici   uffici  nell'esercizio  delle  loro  attribuzioni.  Ciò  è  &#13;
 confermato testualmente dell'articolo 84 dello  Statuto  della  Regione  &#13;
 Trentino-Alto  Adige  (ora  99  del Testo unico approvato con d.P.R. 31  &#13;
 agosto 1972, n. 670) e dall'articolo 38 dello Statuto speciale  per  la  &#13;
 Valle  d'Aosta. Pertanto, nessun contrasto può ravvisarsi tra il primo  &#13;
 comma dell'articolo 137 del codice di procedura penale ed  i  parametri  &#13;
 costituzionali invocati.                                                 &#13;
     Per   la   seconda   questione   di   legittimità  costituzionale,  &#13;
 concernente il terzo comma dell'articolo 137 del  codice  di  procedura  &#13;
 penale  precisato,  è  da dire che, di per se stessi, né l'articolo 3  &#13;
 né l'articolo 6 della Costituzione  possono  garantire  una  specifica  &#13;
 tutela  agli  appartenenti  a  singole minoranze linguistiche. Anzi, è  &#13;
 chiara nell'ordinamento la  tendenza  a  dare  attuazione  ai  principi  &#13;
 dell'articolo   6   della  Costituzione  secondo  regimi  articolati  e  &#13;
 peculiari,  dettati  in   relazione   alle   differenziate   situazioni  &#13;
 ambientali.  Perciò,  anche  per  la  Regione  Friuli-Venezia  Giulia,  &#13;
 malgrado l'esplicito riferimento della X Disposizione transitoria della  &#13;
 Costituzione alla tutela delle minoranze linguistiche e dell'articolo 3  &#13;
 dello  Statuto  speciale  della  Regione   alla   "salvaguardia   delle  &#13;
 rispettive  caratteristiche  etniche  e  culturali"  dei diversi gruppi  &#13;
 linguistici  di  appartenenza,  resta  fermo  che  le  norme  di  grado  &#13;
 costituzionale,  richiamate  come  parametro,  hanno  natura  di  norma  &#13;
 direttiva e dall'applicazione differita. D'altra  parte  al  Memorandum  &#13;
 d'intesa  fra  i Governi d'Italia, del Regno Unito, degli Stati Uniti e  &#13;
 della Jugoslavia, concernente il territorio libero di Trieste  (siglato  &#13;
 a  Londra  il  5 ottobre 1954), che pure conteneva all'articolo 5 dello  &#13;
 Statuto speciale (allegato secondo) ampie garanzie in tema di uso della  &#13;
 lingua materna per le minoranze etniche italiana e slovena, non fu  mai  &#13;
 data  piena  ed  intera  esecuzione all'interno del nostro ordinamento,  &#13;
 risultando tale Memorandum attuato in modo parziale e  prevalentemente,  &#13;
 se non esclusivamente, a mezzo di provvedimenti amministrativi.          &#13;
     Ma   ciò   non  esime  l'interprete  dall'accertare  se  le  norme  &#13;
 legislative   vigenti   bastino   comunque   a   conferire    immediata  &#13;
 operatività,  in  tema  di uso della lingua nel territorio di Trieste,  &#13;
 alle norme costituzionali evocate ed in  particolare  agli  articoli  6  &#13;
 della  Costituzione e 3 dello Statuto regionale.  È sufficiente, a tal  &#13;
 fine, ricordare le leggi statali 19 luglio 1961, n. 1012, e 22 dicembre  &#13;
 1973, n. 932, contenenti la disciplina per la istituzione di scuole con  &#13;
 lingua di insegnamento slovena nelle province  di  Trieste  e  Gorizia;  &#13;
 nonché  l'articolo  34  del  d.P.R.  31  maggio  1974,  n. 416 (con la  &#13;
 significativa rubrica "tutela delle minoranze...") e l'articolo 8 della  &#13;
 legge 14 gennaio 1975, n. 1, sull'ordinamento dei  consigli  scolastici  &#13;
 nelle  province  di  Trieste e di Gorizia in ordine alle scuole statali  &#13;
 con lingua di insegnamento slovena. Vanno pure menzionate la  legge  31  &#13;
 ottobre  1966, n. 935, che ha abrogato il divieto di dare nomi slavi ai  &#13;
 bambini; la legge 14 aprile 1956, n.  308,  che  ha  approvato  e  reso  &#13;
 esecutiva  la  convenzione  fra  Presidenza  del Consiglio e RAI-TV per  &#13;
 l'estensione al territorio triestino del servizio radiotelevisivo,  con  &#13;
 l'esplicita  previsione  di  trasmissione  di  notiziari e programmi in  &#13;
 lingua slovena  per  mezzo  della  stazione  triestina.  (L'impegno  è  &#13;
 ripetuto  nella  legge  di riforma 14 aprile 1975, n. 103, prevedendosi  &#13;
 anzi la stipulazione  di  una  apposita  convenzione  per  trasmissioni  &#13;
 televisive  in  lingua  slovena).  Un  preciso  riferimento a partiti o  &#13;
 gruppi politici  "espressi"  dalla  minoranza  di  lingua  slovena  del  &#13;
 Friuli-Venezia  Giulia è poi contenuto nell'art.  2, comma nono, della  &#13;
 legge 24  gennaio  1979,  n.  18,  per  l'elezione  dei  rappresentanti  &#13;
 dell'Italia  al  Parlamento  europeo.  Né mancano specifici ordini del  &#13;
 Governo Militare Alleato mai abrogati, provvedimenti del  Commissariato  &#13;
 generale  del  Governo  italiano per il territorio di Trieste e recenti  &#13;
 leggi regionali che valorizzano particolari aspetti  della  vita  della  &#13;
 minoranza slovena.                                                       &#13;
     Questo complesso di atti ha un contenuto normativo che corrisponde,  &#13;
 sia  pure  per  parti,  a  quello che avrebbero potuto avere uno o più  &#13;
 provvedimenti formalmente diretti a dare attuazione agli articoli dello  &#13;
 Statuto speciale allegato  al  Memorandum  d'intesa;  anzi,  ad  avviso  &#13;
 dell'Avvocatura   dello   Stato,   tali  misure  "possono  o  ritenersi  &#13;
 applicative del suddetto Memorandum" o adottate "proprio  in  relazione  &#13;
 agli  impegni"con  esso  assunti.  Ma  ciò che conta è che tali norme  &#13;
 danno riconoscimento alla minoranza slovena  o  meglio  qualificano  la  &#13;
 popolazione di lingua slovena nel territorio di Trieste come "minoranza  &#13;
 riconosciuta",  il  che  concretizza l'ulteriore operatività normativa  &#13;
 dell'articolo 6 della Costituzione  e  dell'articolo  3  dello  Statuto  &#13;
 regionale,  quanto  meno per il territorio triestino. Se ormai si è in  &#13;
 presenza, al di là di ogni dubbio, di  una  "minoranza  riconosciuta",  &#13;
 con  tale  situazione  è  incompatibile,  prima ancora logicamente che  &#13;
 giuridicamente, qualsiasi sanzione  che  colpisca  l'uso  della  lingua  &#13;
 materna  da  parte  degli appartenenti alla minoranza stessa. È questa  &#13;
 infatti l'operatività  minima,  che,  in  tema  di  trattamento  delle  &#13;
 minoranze  linguistiche,  deriva  dal  fatto ricognitivo di una singola  &#13;
 minoranza. E ciò a  prescindere  dalla  circostanza,  che  perde  ogni  &#13;
 rilievo,  della  conoscenza  o  meno  della  lingua  ufficiale da parte  &#13;
 dell'appartenente alla  minoranza,  sicché  questi,  ove  lo  volesse,  &#13;
 potrebbe   servirsi,   "nell'uso   pubblico",  della  lingua  italiana:  &#13;
 altrimenti nessun  trattamento  particolare  riceverebbe  sotto  questo  &#13;
 aspetto  lo  sloveno,  pretendendosi  da  lui  lo  stesso comportamento  &#13;
 richiesto a tutte le  persone,  cittadine  e  straniere,  che  sappiano  &#13;
 esprimersi  in  lingua  italiana  (art. 137, secondo comma, cod.  proc.  &#13;
 pen.). Questa  tutela  "minima",  anche  nei  rapporti  con  le  locali  &#13;
 autorità  giurisdizionali,  consente  già  ora agli appartenenti alla  &#13;
 minoranza slovena di usare la lingua materna  e  di  ricevere  risposte  &#13;
 dalle   autorità   in  tale  lingua:    nelle  comunicazioni  verbali,  &#13;
 direttamente o per il tramite di un interprete;  nella  corrispondenza,  &#13;
 con  il testo italiano accompagnato da traduzione in lingua slovena. Si  &#13;
 può  del  resto  ricordare  l'applicazione  fornita nel Friuli-Venezia  &#13;
 Giulia alla  legge  19  luglio  1967,  n.  568,  contenente  norme  sul  &#13;
 conferimento  dell'incarico  di  traduttore  e di interprete presso gli  &#13;
 uffici giudiziari.                                                       &#13;
     Peraltro  l'osservanza   dei   precetti   dell'articolo   6   della  &#13;
 Costituzione  e  dell'articolo 3 dello Statuto regionale - in relazione  &#13;
 all'articolo 3 della Costituzione  -  non  richiede  affatto  che  alla  &#13;
 minoranza  slovena  della  Provincia  di  Trieste debba necessariamente  &#13;
 applicarsi una normativa simile a quella adottata per il  Trentino-Aldo  &#13;
 Adige o per la Valle d'Aosta: restando rimesso al legislatore italiano,  &#13;
 nella  propria  discrezionalità,  di scegliere i modi e le forme della  &#13;
 tutela da garantire alla minoranza linguistica slovena.                  &#13;
     Circa il terzo comma dell'articolo  137  del  codice  di  procedura  &#13;
 penale  si  deve  dunque  concludere per la sua non applicabilità agli  &#13;
 appartenenti alla minoranza slovena nel territorio di Trieste in quanto  &#13;
 minoranza riconosciuta: sia che si voglia ravvisare in ciò un caso  di  &#13;
 esclusione  della  punibilità  da  esercizio del diritto (art. 51 cod.  &#13;
 pen.), sia che si propenda per una delimitazione ex ante,  nella  sfera  &#13;
 soggettiva, della operatività della norma penale.                       &#13;
     Pertanto,  così  interpretata,  la  disposizione  del  terzo comma  &#13;
 dell'articolo 137 del  codice  di  procedura  penale  si  sottrae  alle  &#13;
 proposte censure di costituzionalità.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata:                                                &#13;
     a)  la  questione di legittimità costituzionale dell'articolo 137,  &#13;
 primo comma, del codice di procedura penale, sollevata con  l'ordinanza  &#13;
 in epigrafe dal Tribunale di Trieste in riferimento agli articoli 3 e 6  &#13;
 della   Costituzione   e   3   dello  Statuto  speciale  della  Regione  &#13;
 Friuli-Venezia Giulia;                                                   &#13;
     b) nei sensi di cui in motivazione, la  questione  di  legittimità  &#13;
 costituzionale  dell'articolo 137, terzo comma, del codice di procedura  &#13;
 penale, sollevata con l'ordinanza in epigrafe dello stesso Tribunale in  &#13;
 riferimento agli articoli 3 e 6 della Costituzione e  3  dello  Statuto  &#13;
 speciale della Regione Friuli-Venezia Giulia.                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 20 gennaio 1982.                              &#13;
                                   F.to:   LEOPOLDO   ELIA   -   EDOARDO  &#13;
                                   VOLTERRA - MICHELE ROSSANO - ANTONINO  &#13;
                                   DE  STEFANO  -  GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO REALE -  BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI.                      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
