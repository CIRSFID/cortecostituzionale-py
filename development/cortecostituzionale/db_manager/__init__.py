import os
import sqlite3

from cortecostituzionale.settings import DB_PATH


def get_conn():
    conn = sqlite3.connect(DB_PATH)
    conn.row_factory = sqlite3.Row
    return conn


def init_db(conn=None):
    print("Initialising db...", end="", flush=True)
    conn = conn or get_conn()
    with conn:
        conn.execute(
            """
            CREATE TABLE IF NOT EXISTS downloaded_archives (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                filename TEXT NOT NULL,
                extracted BOOL NOT NULL DEFAULT 0,
                sha256_hexdigest TEXT NOT NULL,
                UNIQUE (filename, sha256_hexdigest)
            )
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS downloaded_archives_ix on downloaded_archives(filename)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS downloaded_digest_ix on downloaded_archives(sha256_hexdigest)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS downloaded_filename_digest_ix on downloaded_archives(filename, sha256_hexdigest)
            """
        )

        conn.execute(
            """
            CREATE TABLE IF NOT EXISTS pronunce (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                ecli TEXT NOT NULL UNIQUE,
                anno INTEGER NOT NULL,
                numero INTEGER NOT NULL,
                tipo CHAR(1) NOT NULL,
                presidente TEXT NOT NULL,
                relatore TEXT NOT NULL,
                data_decisione DATETIME NOT NULL,
                data_deposito DATETIME NOT NULL,
                tipo_procedimento TEXT,
                dispositivo TEXT,
                parsed BOOL NOT NULL DEFAULT 0,
                valid BOOL DEFAULT null,
                parser_version TEXT DEFAULT NULL
            )
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_ecli_ix on pronunce(ecli)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_anno_ix on pronunce(anno)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_numero_ix on pronunce(numero)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_tipo_ix on pronunce(tipo)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_presidente_ix on pronunce(presidente)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_relatore_ix on pronunce(relatore)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_data_deposito_ix on pronunce(data_deposito)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_data_decisione_ix on pronunce(data_decisione)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS tipo_procedimento_ix on pronunce(tipo_procedimento)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS dispositivo_ix on pronunce(dispositivo)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_data_parsed_ix on pronunce(parsed)
            """
        )
        conn.execute(
            """
            CREATE INDEX IF NOT EXISTS pronunce_data_valid_ix on pronunce(valid)
            """
        )

    print("Done.")


if not os.path.isfile(DB_PATH):
    init_db()


if __name__ == '__main__':
    init_db()
