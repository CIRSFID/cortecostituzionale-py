<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>63</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:63</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/03/2001</data_decisione>
    <data_deposito>13/03/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
CONTRI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni &#13;
Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 425 del codice &#13;
di  procedura penale, promosso con ordinanza emessa il 14 aprile 2000 &#13;
dal giudice per l'udienza preliminare presso il Tribunale di Imperia, &#13;
nel  procedimento  penale  a  carico  di  Nazzareno  Sirci  ed altri, &#13;
iscritta  al  n. 403  del  registro ordinanze 2000 e pubblicata nella &#13;
Gazzetta   Ufficiale  della  Repubblica,  1a  serie  speciale,  n. 29 &#13;
dell'anno 2000. &#13;
    Visto  l'atto  d'intervento  del  Presidente  del  Consiglio  dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 24 gennaio 2001 il giudice &#13;
relatore Franco Bile. &#13;
    Ritenuto  che,  con  ordinanza del 14 aprile 2000, pervenuta alla &#13;
Corte  il  7 giugno 2000, il giudice per l'udienza preliminare presso &#13;
il  Tribunale di Imperia ha sollevato, in riferimento agli articoli 3 &#13;
e  24  della  Costituzione,  questione di legittimità costituzionale &#13;
dell'art. 425  del codice di procedura penale, nella parte in cui non &#13;
prevede  che  il  giudice,  nel  pronunciare  sentenza di non luogo a &#13;
procedere,  possa  condannare  l'imputato  al  pagamento  delle spese &#13;
processuali  in  favore  della  parte  civile  - salvo che ritenga di &#13;
disporne  la  compensazione  totale  o  parziale  per giusti motivi - &#13;
qualora  la  mancata  decisione  sull'azione  civile  in sede penale, &#13;
conseguente  alla  pronuncia  di detta sentenza, non si ricolleghi ad &#13;
una   determinazione   del   danneggiato   o   non  sia  al  medesimo &#13;
addebitabile; &#13;
        che  la  questione  è  stata  sollevata,  in sede di udienza &#13;
preliminare,  in  un  procedimento  penale  per  un  reato  di  falsa &#13;
testimonianza,   nel  quale  il  pubblico  ministero,  a  seguito  di &#13;
ritrattazione  da parte degli imputati ex art. 376 del codice penale, &#13;
aveva  chiesto  la pronuncia di sentenza di non luogo a procedere con &#13;
declaratoria  dell'intervenuta  causa  di  non punibilità, la difesa &#13;
degli  imputati  si  era associata a tale richiesta e la parte civile &#13;
aveva  chiesto  invece,  in via principale, il rinvio a giudizio (nel &#13;
presupposto   dell'incompletezza   della  ritrattazione)  ed  in  via &#13;
subordinata,  per  il  caso  di pronuncia della suddetta sentenza, la &#13;
condanna  degli  imputati  al risarcimento del danno e alla rifusione &#13;
delle spese di costituzione di parte civile; &#13;
        che  il  rimettente  -  premesso  di  dovere, a seguito della &#13;
ritrattazione, pronunciare sentenza di non luogo a procedere ai sensi &#13;
dell'art. 425  cod. proc. pen. - esclude di potersi pronunciare sulle &#13;
questioni  civili,  in  quanto  la possibilità che il giudice penale &#13;
decida  su  di  esse è ristretta al caso in cui pronunci sentenza di &#13;
condanna in sede dibattimentale o in sede di giudizio abbreviato, ove &#13;
tale  rito  sia  stato  accettato  dalla  parte  privata,  laddove la &#13;
sentenza ex art. 425 cod. proc. pen. non sarebbe sentenza di condanna &#13;
(anche  perché  revocabile  ex  art. 434  cod.  proc. pen.), sarebbe &#13;
inoppugnabile   per  la  parte  civile  e  non  spiegherebbe  effetti &#13;
sull'azione civile; &#13;
        che  siffatta  conclusione  sarebbe, del resto, rafforzata da &#13;
quanto  questa Corte ha ritenuto con la sentenza n. 443 del 1990, che &#13;
dichiarò  infondata  la  questione  di  legittimità  costituzionale &#13;
dell'art. 444  cod.  proc. pen., nella parte in cui non prevedeva che &#13;
il giudice, nell'applicare una pena su richiesta delle parti, potesse &#13;
decidere sull'azione civile per le restituzioni ed i danni; &#13;
        che,  viceversa,  secondo il rimettente, la medesima sentenza &#13;
n. 443   del   1990   -   laddove   ha   dichiarato  l'illegittimità &#13;
costituzionale  dello stesso art. 444 cod. proc. pen., nella parte in &#13;
cui  non  prevedeva che il giudice penale, nell'applicare una pena su &#13;
richiesta  delle  parti, potesse condannare l'imputato alla rifusione &#13;
delle  spese processuali in favore della parte civile - comporterebbe &#13;
la  conclusione  che  anche  in  sede  di pronuncia della sentenza ex &#13;
art. 425   cod.   proc.   pen.  sarebbe  giustificabile  la  condanna &#13;
dell'imputato  alla  rifusione delle spese giudiziali sostenute dalla &#13;
parte civile; &#13;
        che,  infatti,  anche nel caso della pronuncia della sentenza &#13;
ex  art.  425  cod.  proc.  pen., varrebbero le affermazioni fatte da &#13;
questa  Corte  nella  citata  sentenza,  sia  quanto alla mancanza di &#13;
necessario  collegamento fra la condanna nelle spese processuali e la &#13;
sentenza  di  condanna  per  responsabilità  civile,  sia  quanto al &#13;
rilievo  che, laddove la mancata decisione sull'azione civile in sede &#13;
penale  non sia ricollegata né ad una determinazione del danneggiato &#13;
(come  nella  mancata  accettazione  del  giudizio abbreviato) né "a &#13;
qualcosa   a   lui   comunque  addebitabile",  sarebbe  "evidente  il &#13;
pregiudizio  nel  lasciare  a  carico  della  parte  civile "le spese &#13;
incontrate  per iniziative o attività rivelatesi determinanti per le &#13;
decisioni dell'imputato"; &#13;
        che  tali affermazioni - ad avviso del rimettente - sarebbero &#13;
adeguate  anche  in  relazione  al  caso  di  specie,  in  quanto  il &#13;
"pregiudizio  della  parte  civile ...  pare  assolutamente  evidente &#13;
laddove  si  rilevi  che  proprio  l'attività  della parte civile ha &#13;
condotto  gli  imputati  alla  ritrattazione  solo pochi giorni prima &#13;
della data fissata per l'udienza preliminare"; &#13;
        che, sulla base di tali argomenti, il rimettente ha sollevato &#13;
l'indicata questione di legittimità costituzionale; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  tramite l'Avvocatura generale dello Stato, sostenendo &#13;
genericamente  l'inammissibilità  e  comunque  l'infondatezza  della &#13;
questione. &#13;
    Considerato   che   il  rimettente  ha  motivato  la  prospettata &#13;
questione  esclusivamente  con  riferimento  al tertium comparationis &#13;
costituito   dall'ipotesi   della   pronuncia   della   sentenza   di &#13;
applicazione   della   pena   su   richiesta  delle  parti  ai  sensi &#13;
dell'art. 444  cod.  proc.  pen.,  nella  disciplina risultante dalla &#13;
sentenza  di  questa Corte n. 443 del 1990, testualmente riproponendo &#13;
le motivazioni sulla base delle quali l'articolo citato fu dichiarato &#13;
illegittimo  nella  parte  in cui non prevedeva la possibilità della &#13;
condanna nelle spese processuali a favore della parte civile; &#13;
        che, pertanto, l'ordinanza di rimessione, ancorché deduca la &#13;
violazione sia dell'art. 3 che dell'art. 24 della Costituzione, è in &#13;
realtà  motivata  con esclusivo riferimento al primo, nel senso che, &#13;
secondo il rimettente, per la sentenza di cui all'art. 425 cod. proc. &#13;
pen.  ricorrerebbero  le  medesime ragioni per le quali questa Corte, &#13;
con    la    citata   decisione,   ha   dichiarato   l'illegittimità &#13;
costituzionale dell'art. 444 del codice di procedura penale; &#13;
        che,  dunque,  la  Corte  deve  limitarsi  ad accertare se la &#13;
mancata  previsione  da  parte  dell'art. 425 del codice di procedura &#13;
penale  della  possibilità della condanna nelle spese processuali in &#13;
favore  della  parte  civile  determini  una lesione del principio di &#13;
eguaglianza,  in  quanto  realizzi  un  trattamento  differenziato di &#13;
situazioni identiche o talmente simili da richiederne uno eguale; &#13;
        che,  sotto  tale profilo, il tertium comparationis invocato, &#13;
cioè  l'ipotesi  della  sentenza applicativa della pena su richiesta &#13;
delle  parti,  presenta  evidenti  diversità rispetto a quella della &#13;
sentenza  ex  art. 425  cod. proc. pen., in quanto - mentre la prima, &#13;
pur  non  potendo essere identificata come vera e propria sentenza di &#13;
condanna,  è  tuttavia  a  questa  "equiparata",  salvo che la legge &#13;
disponga  diversamente,  dall'ultimo inciso del primo comma dell'art. &#13;
445  cod.  proc. pen. -- viceversa la sentenza ex art. 425 cod. proc. &#13;
pen. in  nessun  caso - nonostante la diversità delle fattispecie in &#13;
relazione  alle  quali può essere pronunciata - è equiparata ad una &#13;
sentenza di condanna, come del resto riconosce lo stesso rimettente; &#13;
        che,  pertanto,  risulta  invocato  un  tertium comparationis &#13;
inidoneo,  non  diversamente  da  quanto  accadde  nel caso deciso da &#13;
questa  Corte  con  l'ordinanza  n. 73 del 1993, nel quale il tertium &#13;
scrutinato  dalla  sentenza  n. 443  del  1990  era  stato invocato a &#13;
proposito  dell'art. 162 del codice penale, per la mancata previsione &#13;
della  condanna nelle spese processuali in favore della parte civile, &#13;
in  ipotesi  di  sentenza di non doversi procedere per estinzione del &#13;
reato per oblazione; &#13;
        che,  del resto, analogamente a quanto allora ritenuto, anche &#13;
nella fattispecie della sentenza di non luogo a procedere ex art. 425 &#13;
del codice di procedura penale il danneggiato dal reato, costituitosi &#13;
parte  civile,  ben  può  far valere la pretesa alla rifusione delle &#13;
spese   processuali  sostenute  in  relazione  allo  svolgimento  del &#13;
procedimento   conclusosi   con  tale  sentenza,  nell'ambito  di  un &#13;
successivo  giudizio  di  risarcimento  del  danno  avanti al giudice &#13;
civile, configurandosi l'esborso per le spese come una componente del &#13;
danno da reato; &#13;
        che,  conclusivamente,  la  questione  deve essere dichiarata &#13;
manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87  e  9,  secondo  comma,  delle  norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art. 425  del  codice di procedura &#13;
penale,  sollevata,  in  riferimento  agli  articoli  3  e  24  della &#13;
Costituzione,   dal  giudice  per  l'udienza  preliminare  presso  il &#13;
Tribunale di Imperia, con l'ordinanza indicata in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 marzo 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Bile &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 13 marzo 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
