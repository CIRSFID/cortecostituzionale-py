<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>92</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:92</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1994</data_decisione>
    <data_deposito>15/03/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 5 della legge    &#13;
 16  febbraio  1987,  n.  81  (Delega  legislativa  al  Governo  della    &#13;
 Repubblica  per  l'emanazione del nuovo codice di procedura penale) e    &#13;
 dell'art. 72, primo e secondo comma, del R.D. 30 gennaio 1941, n.  12    &#13;
 (ordinamento giudiziario), come sostituito dall'art. 22 del d.P.R. 22    &#13;
 settembre  1988,  n.  449 (Approvazione delle norme per l'adeguamento    &#13;
 dell'ordinamento giudiziario al nuovo processo penale ed a  quello  a    &#13;
 carico  di  imputati  minorenni),  promosso con ordinanza emessa l'11    &#13;
 gennaio  1993  dal  Pretore  di  Torino  -  sezione   distaccata   di    &#13;
 Moncalieri,  nel  procedimento  penale a carico di Mulassano Mauro ed    &#13;
 altro, iscritta al n. 406 del registro ordinanze  1993  e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  30,  prima  serie    &#13;
 speciale, dell'anno 1993;                                                &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 26 gennaio 1994 il Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto  che  il  Pretore  di  Torino  -  sezione  distaccata  di    &#13;
 Moncalieri,   ha   sollevato,   in   riferimento  all'art.  76  della    &#13;
 Costituzione, questione di legittimità  costituzionale  dell'art.  5    &#13;
 della  legge  16  febbraio  1987 n. 81, nonché dell'art. 72, primo e    &#13;
 secondo comma, del R.D.  30  gennaio  1941  n.  12  (come  sostituito    &#13;
 dall'art.  22  del  d.P.R. 22 settembre 1988 n. 449), "nella parte in    &#13;
 cui prevede  che  le  funzioni  di  pubblico  ministero  nell'udienza    &#13;
 dibattimentale  pretorile possano essere svolte, per delega specifica    &#13;
 e nominativa del Procuratore della Repubblica presso la  Pretura,  da    &#13;
 uditori giudiziari";                                                     &#13;
      che,  ad  avviso  del  giudice  a  quo,  la norma costituzionale    &#13;
 risulterebbe violata in quanto nell'art. 5 della legge n. 81 del 1987    &#13;
 non vi sarebbe traccia di principi e criteri direttivi; condizione  -    &#13;
 questa   -   fondamentale   per   una  legittima  delega  al  Governo    &#13;
 dell'esercizio della funzione legislativa;                               &#13;
      che è intervenuta  nel  giudizio  l'Avvocatura  generale  dello    &#13;
 Stato,  in  rappresentanza del Presidente del Consiglio dei ministri,    &#13;
 che ha concluso per l'infondatezza della questione;                      &#13;
    Considerato che questione identica, sollevata dallo stesso Pretore    &#13;
 è stata già esaminata da questa Corte e  dichiarata  manifestamente    &#13;
 infondata con ordinanza n. 468 del 1993;                                 &#13;
      che  in  detta decisione si è rilevato come sia sufficiente che    &#13;
 la nuova  disciplina  si  collochi  all'interno  del  nuovo  processo    &#13;
 penale, ne attui le finalità e costituisca il coerente sviluppo e la    &#13;
 concreta  attuazione  dei  criteri  e  dei  principi ispiratori della    &#13;
 riforma (sentt. n. 68 e n. 181 del 1991),  e  che,  inoltre,  possono    &#13;
 utilmente  dedursi  principi  e  criteri  direttivi dagli artt. 2 e 3    &#13;
 della stessa legge n. 81 del 1987 e, in particolare, dalle  direttive    &#13;
 che  hanno  per  oggetto  l'ordinamento  giudiziario.  E  cioè dalla    &#13;
 direttiva n. 68,  la  quale  prevede  che  le  funzioni  di  pubblico    &#13;
 ministero  in  udienza  siano esercitate con piena autonomia, e dalla    &#13;
 direttiva n. 103, secondo cui il processo  davanti  al  pretore  deve    &#13;
 svolgersi  con criteri di massima semplificazione e, tra l'altro, con    &#13;
 la garanzia della distinzione delle funzioni di pubblico ministero  e    &#13;
 di giudice;                                                              &#13;
      che  pertanto  anche  la  presente  questione, non essendo stati    &#13;
 dedotti  argomenti  ulteriori  rispetto  a   quelli   esaminati,   va    &#13;
 dichiarata manifestamente infondata;                                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  degli  artt.  5,  della  legge 16 febbraio 1987 n. 81    &#13;
 (Delega legislativa al Governo della Repubblica per l'emanazione  del    &#13;
 nuovo  codice  di procedura penale), e 72, primo e secondo comma, del    &#13;
 R.D. 30 gennaio 1941 n. 12 (Ordinamento giudiziario), come sostituito    &#13;
 dall'art. 22 del d.P.R. 22 settembre 1988 n. 449 (Approvazione  delle    &#13;
 norme   per   l'adeguamento  dell'ordinamento  giudiziario  al  nuovo    &#13;
 processo  penale  ed  a  quello  a  carico  di  imputati  minorenni),    &#13;
 sollevata  in  riferimento all'art. 76 della Costituzione dal Pretore    &#13;
 di Torino, sezione  distaccata  di  Moncalieri,  con  l'ordinanza  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 7 marzo 1994.                                    &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 15 marzo 1994.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
