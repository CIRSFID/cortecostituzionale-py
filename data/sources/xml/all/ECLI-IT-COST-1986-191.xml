<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>191</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:191</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/06/1986</data_decisione>
    <data_deposito>14/07/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI &#13;
 - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - &#13;
 Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO &#13;
 GRECO - Prof. GABRIELE PESCATORE - Avv. UGO SPAGNOLI - Prof. FRANCESCO &#13;
 PAOLO CASAVOLA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale della legge approvata  &#13;
 il 7 luglio 1977  e  riapprovata  il  18  ottobre  1977  dal  Consiglio  &#13;
 regionale   del  Trentino  -  Alto  Adige,  recante  "Modificazioni  ed  &#13;
 integrazioni alla legge regionale 29 dicembre 1975, n. 14,  concernente  &#13;
 disciplina  delle tasse regionali e delle soprattasse provinciali sulle  &#13;
 concessioni non governative"; promosso con ricorso del  Presidente  del  &#13;
 Consiglio  dei  ministri,  notificato il 9 novembre 1977, depositato in  &#13;
 cancelleria il 19 successivo ed iscritto al n.  33 del registro ricorsi  &#13;
 1977.                                                                    &#13;
     Visto l'atto di costituzione della Regione Trentino - Alto Adige;    &#13;
     udito nell'udienza pubblica del 20 maggio 1986 il Giudice  relatore  &#13;
 Antonio La Pergola; _                                                    &#13;
     uditi l'avvocato dello Stato Giorgio Azzariti, per il ricorrente, e  &#13;
 l'avv. Sergio Panunzio, per la Regione.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Il  Presidente  del  Consiglio dei ministri impugna il testo  &#13;
 della Regione Trentino - Alto Adige riapprovato dal Consiglio regionale  &#13;
 il 18 ottobre 1977, e recante "Modificazioni ed integrazioni alla legge  &#13;
 regionale 29 dicembre 1975, n. 14, concernente disciplina  delle  tasse  &#13;
 regionali   e  delle  soprattasse  provinciali  sulle  concessioni  non  &#13;
 governative".                                                            &#13;
     Rileva  anzitutto  l'Avvocatura dello Stato che il disegno di legge  &#13;
 impugnato  prevede  che  siano  sottoposte  a  tasse  regionali   sulle  &#13;
 concessioni  non governative le licenze di cui agli artt. 111 e 115 del  &#13;
 Testo Unico delle leggi  di  pubblica  sicurezza,  ora  rilasciate  dai  &#13;
 Presidenti  delle  giunte  provinciali  sulla  base  dell'art. 20 dello  &#13;
 statuto speciale di autonomia e relative alle arti tipografiche ed alle  &#13;
 agenzie pubbliche e di affari. Tali provvedimenti sono stati soggetti a  &#13;
 tassa sulle concessioni governative dai nn.   62  -  64  della  tariffa  &#13;
 allegata al d.P.R. 26 ottobre 1972, n.  641.                             &#13;
     Già  in  sede  di rinvio del disegno in questione il Governo aveva  &#13;
 osservato che le tasse in oggetto non  possono  colpire  gli  atti  dei  &#13;
 Presidenti  delle  Province  autonome,  che  in  base all'art. 20 dello  &#13;
 statuto ineriscano ad attività statali decentrate.                      &#13;
     L'Avvocatura rileva ancora  che  l'art.  1  del  disegno  impugnato  &#13;
 prevede  che  i  ricorsi  gerarchici  alla  Giunta  regionale contro le  &#13;
 decisioni adottate dal capo dell'ispettorato  generale  delle  finanze,  &#13;
 devono intendersi accolti in caso di silenzio della Giunta stessa.       &#13;
     Ad avviso del Governo era stato già rilevato in sede di rinvio che  &#13;
 non  è  ammissibile  l'applicazione alla fattispecie dell'istituto del  &#13;
 silenzo - accoglimento: il perseguimento di interessi pubblici non può  &#13;
 trovare pregiudizio per causa dell'attribuzione di rilievo positivo  al  &#13;
 comportamento inerte dell'amministrazione.                               &#13;
     L'Avvocatura  ricorda  che la prima questione ora sollevata davanti  &#13;
 alla Corte era già stata posta dal Governo  in  sede  di  approvazione  &#13;
 della  legge  regionale n.   14 del 1975. In quell'occasione la Regione  &#13;
 aveva tentato di sottoporre a imposta regionale le licenze in discorso;  &#13;
 ma il Governo rilevava tuttavia che l'art.  20  dello  statuto  non  ha  &#13;
 previsto  -  in materia - alcun trasferimento di competenza dallo Stato  &#13;
 alle Province.                                                           &#13;
     Ora, la Regione obietta che si tratta di due funzioni, le quali, in  &#13;
 quanto  attribuite  ai  Presidenti  delle  Province,  devono  ritenersi  &#13;
 trasferite  a  queste  ultime  e  così  esercitabili  anche  dai  loro  &#13;
 Presidenti,  anziché,  come  di  solito  avviene,  dalle  giunte.   Ci  &#13;
 troveremmo  allora di fronte a concessioni che, prima governative, sono  &#13;
 divenute provinciali. Replica  in  proposito  l'Avvocatura  che  questa  &#13;
 Corte,  con  la  sentenza  n.  14 del 1956, ha escluso che la norma del  &#13;
 previgente statuto, in tutto analoga a quella  attualmente  in  vigore,  &#13;
 abbia operato alcun "decentramento istituzionale" per quel che concerne  &#13;
 l'attuale  controversia.  Le norme di attuazione dello statuto speciale  &#13;
 approvate con il d.P.R. n. 686 del  1973  attribuiscono  al  Presidente  &#13;
 della  Provincia,  quale  autorità  locale  di pubblica sicurezza, una  &#13;
 figura giuridica non diversa da quella rivestita dal Sindaco, quando è  &#13;
 considerato come organo dello Stato.                                     &#13;
     Dovendo le licenze in  oggetto  ancora  ritenersi  governative,  il  &#13;
 disegno  legislativo  impugnato offenderebbe l'art. 73 dello statuto di  &#13;
 autonomia.                                                               &#13;
     Tale ultima norma, nell'attribuire  alla  Regione  la  facoltà  di  &#13;
 istituire   tributi   propri,   pone   infatti   il  limite  costituito  &#13;
 dall'armonia "con i principi del sistema tributario dello  Stato";  fra  &#13;
 tali  principi,  secondo  l'Avvocatura  dello  Stato, vi sarebbe quello  &#13;
 ricavabile dagli artt. 1 - 3 della legge n. 281 del 1970 e dallo stesso  &#13;
 d.P.R. 26 ottobre 1972, n. 641, in base al quale va  riconosciuta  allo  &#13;
 Stato  la potestà impositiva, con riguardo alle concessioni di propria  &#13;
 competenza,  potestà  riaffermata  dall'art.  77 lett. b) dello stesso  &#13;
 statuto.                                                                 &#13;
     Del pari decisiva,  ai  fini  del  rigetto  della  tesi  regionale,  &#13;
 sarebbe  la  considerazione  che  gli  stessi  atti che la Regione vuol  &#13;
 sottoporre a tassa di  concessione,  sono  già  soggetti  a  tassa  di  &#13;
 concessione  governativa,  sulla  base dei n. 62, 63 e 64 della tariffa  &#13;
 allegata al d.P.R. n. 641 del 1972.                                      &#13;
     Il Presidente del  Consiglio,  per  tramite  dell'Avvocatura  dello  &#13;
 Stato,  insiste  altresì sull'illegittimità della previsione da parte  &#13;
 del  disegno  di  legge  impugnato  del  silenzio  -  accoglimento,  in  &#13;
 relazione  ai  ricorsi  presentati  alla  Giunta  regionale,  contro le  &#13;
 decisioni dell'ispettorato generale delle imposte.                       &#13;
     Anche se, osserva l'Avvocatura, vanno aumentando i casi di silenzio  &#13;
 - accoglimento previsti dal legislatore, essi rappresentano pur  sempre  &#13;
 deroghe   al   principio   generale  operante  nella  nostra  giustizia  &#13;
 amministrativa, che attribuisce normalmente al silenzio della  pubblica  &#13;
 amministrazione  il  significato di rigetto delle istanze o dei ricorsi  &#13;
 del privato.                                                             &#13;
     All'inerzia dell'amministrazione  può  talora  attribuirsi  valore  &#13;
 equivalente ad un provvedimento di visto o di approvazione ma non si è  &#13;
 mai  riconosciuto  al  suddetto  silenzio  valore  di  accoglimento del  &#13;
 ricorso del privato contro un atto amministrativo e  quindi  valore  di  &#13;
 annullamento dell'atto impugnato.                                        &#13;
     L'Avvocatura conclude che gli artt. 6 del d.P.R. 24 novembre 1,971,  &#13;
 n.  1199 e 11 del d.P.R. 26 ottobre 1972, n. 641, rappresentano appunto  &#13;
 la  manifestazione  ed   applicazione   "di   un   principio   generale  &#13;
 dell'ordinamento e del sistema di giustizia tributaria".                 &#13;
     Per   i  suddetti  motivi  si  chiede  che  questa  Corte  dichiari  &#13;
 incostituzionale il disegno di legge regionale impugnato  in  relazione  &#13;
 alla violazione degli artt. 73 e 77 dello statuto speciale del Trentino  &#13;
 - Alto Adige.                                                            &#13;
     2.  -  Si costituisce nel presente giudizio di costituzionalità la  &#13;
 Regione Trentino - Alto Adige, la  cui  difesa  osserva  anzitutto  che  &#13;
 l'esercizio   dei   poteri  amministrativi  in  oggetto  è  demandato,  &#13;
 nell'a'mbito della Regione, ai Presidenti delle due Province  autonome,  &#13;
 e  ciò  sulla  base  dell'art.  20  dello statuto di autonomia e delle  &#13;
 relative norme di attuazione.                                            &#13;
     Alla tesi del Presidente del  Consiglio  secondo  cui  le  suddette  &#13;
 disposizioni  non avrebbero operato alcun "trasferimento istituzionale"  &#13;
 in materia di pubblica sicurezza alle Province di Trento e  Bolzano  è  &#13;
 opposto  che se le attribuzioni in oggetto costituiscano espressione di  &#13;
 una competenza  propria,  o  invece  di  una  competenza  delegata,  è  &#13;
 questione del tutto irrilevante; in ogni caso ci troveremmo di fronte a  &#13;
 funzioni  esercitate  da  un  organo  della Provincia ed in quanto tali  &#13;
 provinciali.                                                             &#13;
     Pertanto, continua la difesa della Regione, deve escludersi che  il  &#13;
 disegno impugnato violi il principio secondo il quale deve riconoscersi  &#13;
 allo  Stato la potestà impositiva con riferimento alle "concessioni di  &#13;
 propria competenza", in quanto le licenze in oggetto sono di competenza  &#13;
 delle Province autonome.                                                 &#13;
     Comunque, soggiunge la  difesa  regionale,  la  competenza  vantata  &#13;
 dalle  Province  non  deriva  da  una delega' non essendo l'onere delle  &#13;
 relative spese a carico dello Stato (come previsto per il caso  opposto  &#13;
 dall'art. 16, terzo comma, dello statuto). Tutte le spese relative alle  &#13;
 funzioni  in oggetto sono viceversa a carico delle Province.  Il che si  &#13;
 assume debba comportare la legittimità dell'istituzione di un  tributo  &#13;
 da  parte  della  Regione, nel rispetto del principio di corrispondenza  &#13;
 tra titolarità delle funzioni e potestà impositiva.                    &#13;
     Inoltre non sarebbe conferente  l'argomento  secondo  il  quale  le  &#13;
 licenze  in  questione  sarebbero già sottoposte ad un tributo statale  &#13;
 sulla base di quanto previsto dalla tabella allegata al d.P.R.  n.  641  &#13;
 del 1972.                                                                &#13;
     Infatti,   il   disegno  di  legge  impugnato  tenderebbe  solo  ad  &#13;
 introdurre, nell'a'mbito regionale, forme  di  imposizione  sostitutive  &#13;
 della   tassa   di  concessione  governativa,  fine  questo  pienamente  &#13;
 legittimo,  in  considerazione  dell'assetto  delle  competenze   delle  &#13;
 Province nella materia in oggetto.                                       &#13;
     Quanto, poi, al secondo motivo di impugnazione, la difesa regionale  &#13;
 osserva che la disciplina dettata dal legislatore statale in materia di  &#13;
 silenzio  -  rigetto  e di contenzioso amministrativo nel settore delle  &#13;
 tasse   sulle   concessioni   governative   non   può    rappresentare  &#13;
 l'espressione  di  un  principio  fondamentale  dell'ordinamento, ma il  &#13;
 risultato della scelta di  una  determinata  soluzione  normativa,  che  &#13;
 ammetterebbe   ampie   possibilità  di  deroga.  Al  riguardo  vengono  &#13;
 richiamate varie norme legislative.                                      &#13;
     Per i suddetti motivi la Regione confida nel rigetto del ricorso.    &#13;
     3. - All'udienza del 20 maggio 1986 tanto l'Avvocatura dello  Stato  &#13;
 quanto  la  difesa  della Regione Trentino - Alto Adige hanno insistito  &#13;
 sulle rispettive tesi.                                                   &#13;
     Inoltre, la difesa della Regione Trentino -  Alto  Adige  ha  fatto  &#13;
 notare  come  il  quadro  della  normativa  concernente l'oggetto della  &#13;
 controversia sia parzialmente mutato in base all'art.  8  del  d.1.  10  &#13;
 novembre  1978,  n.  702,  convertito  con  modificazioni nella legge 8  &#13;
 gennaio 1979, n. 3 e al decreto del Ministro delle Finanze 29  novembre  &#13;
 1978 (pubblicato nella G. U. 14 dicembre 1978, n. 348).                  &#13;
     Dalla  predetta  normativa,  applicativa  dell'art.  19  del d.P.R.  &#13;
 n.616/77, si ricava che la tassa di cui al n. 62 della tabella allegata  &#13;
 al d.P.R. n. 641/72 è divenuta, al pari di altre, un tributo comunale.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La presente questione, promossa dal Presidente del  Consiglio,  &#13;
 concerne  il  testo della legge riapprovata dal Consiglio regionale del  &#13;
 Trentino - Alto Adige il 18 ottobre 1977, dopo il rinvio  disposto  dal  &#13;
 Governo.                                                                 &#13;
     Il  testo  normativo  impugnato  reca integrazioni e modifiche alla  &#13;
 legge regionale del 29 dicembre 1975, n.  14, concernente la disciplina  &#13;
 delle tasse regionali e delle soprattasse provinciali sulle concessioni  &#13;
 non governative.  Nel ricorso dello Stato si deduce un  duplice  ordine  &#13;
 di censure.                                                              &#13;
     a)   In  primo  luogo,  la  Regione  avrebbe  ecceduto  le  proprie  &#13;
 competenze con il sottoporre alle tasse regionali sulle concessioni non  &#13;
 governative le licenze previste agli artt. 111 e 115  del  Testo  Unico  &#13;
 delle  leggi  di  pubblica  sicurezza  (R.D.  n.  773  del  1931). Tali  &#13;
 provvedimenti sono, rispettivamente, la licenza per l'arte  tipografica  &#13;
 e  quella  per  l'apertura e conduzione delle agenzie (*) di affari. Ai  &#13;
 sensi dell'art. 20 dello statuto del Trentino - Alto Adige (e dell'art.  &#13;
 3 delle norme di attuazione poste nel d.P.R. n.   686/73), il  relativo  &#13;
 rilascio  compete  ai  Presidenti delle giunte provinciali (di Trento e  &#13;
 Bolzano),  i  quali esercitano, infatti, le attribuzioni spettanti alle  &#13;
 autorità di pubblica sicurezza in varie materie,  incluse  le  due  in  &#13;
 considerazione.  L'Avvocatura  dello Stato ritiene che, alla stregua di  &#13;
 una precedente pronunzia della Corte (14/56), resa in  merito  all'art.  &#13;
 16  del  previgente  testo  dello  statuto speciale (cfr. art. 47 delle  &#13;
 norme di attuazione del d.P.R.  n.    574/51),  di  identico  contenuto  &#13;
 rispetto  all'art.  20 dell'attuale statuto, non si debba ravvisare nel  &#13;
 caso in esame alcun istituzionale decentramento delle  attribuzioni  di  &#13;
 pubblica sicurezza a favore della Provincia: il Presidente della Giunta  &#13;
 provinciale  emanerebbe  le  licenze  sopra  menzionate  in  una  veste  &#13;
 giuridica, che, sostanzialmente, non differisce da quella  assunta  dal  &#13;
 Sindaco,  quale  autorità  locale  di  pubblica  sicurezza. Del resto,  &#13;
 soggiunge l'Avvocatura, la vigente legislazione dello Stato  include  i  &#13;
 provvedimenti che vengono in rilievo nella specie fra gli atti soggetti  &#13;
 alle tasse sulle concessioni governative (cfr. le rispettive voci della  &#13;
 tabella  annessa  al  d.P.R.  n.  641/72:  Disciplina delle tasse sulle  &#13;
 concessioni  governative).  Il  che  comproverebbe   che   difetta   il  &#13;
 presupposto  giustificativo  delle norme regionali dedotte in giudizio.  &#13;
 Di qui il necessario risultato della violazione degli  artt.  73  e  77  &#13;
 dello  statuto  speciale  per  il Trentino - Alto Adige, dai quali trae  &#13;
 fondamento la potestà legislativa ed impositiva della Regione, il  cui  &#13;
 esercizio è qui contestato.                                             &#13;
     b)   L'altro   rilievo  formulato  nel  ricorso  investe,  come  il  &#13;
 corrispondente motivo di rinvio  in  seguito  alla  prima  approvazione  &#13;
 della legge, l'art. 1 del testo censurato. Ivi è posta la disposizione  &#13;
 che  sostituisce  il  terzo comma dell'art. 11 della legge regionale n.  &#13;
 14/75.  Quest'ultima  disposizione   concerne   i   ricorsi   (inerenti  &#13;
 all'applicazione    delle   tasse   in   questione,   prodotti   avanti  &#13;
 all'ispettorato generale delle finanze e del  patrimonio)  e  contempla  &#13;
 che,  decorso  il  termine  di  novanta  giorni dalla presentazione del  &#13;
 ricorso, senza che sia  stata  comunicata  al  ricorrente  la  relativa  &#13;
 decisione,  il ricorso s'intende respinto. La modifica introdotta dalla  &#13;
 norma impugnata sta in ciò, che,  decorso  il  termine  anzidetto,  il  &#13;
 ricorso  deve  invece  ritenersi  accolto.  Ad  avviso dell'Avvocatura,  &#13;
 sarebbe stata così configurata un'ingiustificata deroga  -  e  perciò  &#13;
 un'infrazione  -  al  principio  generale  vigente  nel  sistema  della  &#13;
 giustizia    amministrativa,    che,    di    regola,    al    silenzio  &#13;
 dell'amministrazione  conferisce l'opposto significato:  quello, cioè,  &#13;
 del diniego di accoglimento dell'istanza ovvero di rigetto del  ricorso  &#13;
 del privato.                                                             &#13;
     2.  -  Quanto alla censura sopra richiamata sub 1 a) s'impongono le  &#13;
 seguenti conclusioni. Essa è fondata, per  le  ragioni  dedotte  dallo  &#13;
 Stato,  con  riguardo  alla  licenza  che  attiene  alle agenzie (*) di  &#13;
 affari.  Si  tratta  di  un  atto  ancora  soggetto  alla  tassa  sulle  &#13;
 concessioni  governative  con  d.P.R.  n. 641/72,(**) 64, della tabella  &#13;
 allegata, ed emesso  dal  Presidente  della  Giunta  provinciale  quale  &#13;
 autorità  locale di pubblica sicurezza, senza che le relative funzioni  &#13;
 siano istituzionalmente passate in capo  all'ente  autonomo.    Diverso  &#13;
 avviso  va  accolto  con  riferimento  alla licenza concernente le arti  &#13;
 tipografiche. A proposito di quest'ultimo provvedimento, è infatti  da  &#13;
 osservare  che la competenza ad emanarlo è stata trasferita ai comuni,  &#13;
 in forza dell'art. 19 del d.P.R. n.  616/77,  n.  11.  La  funzione  di  &#13;
 polizia  amministrativa,  che si concreta nel rilascio della licenza in  &#13;
 questione, è attribuita al comune, come le  altre  elencate  nell'art.  &#13;
 19,  nel  quadro  del  seguente regime: il consiglio comunale determina  &#13;
 procedure e competenze in ordine all'esercizio di  detta  funzione;  il  &#13;
 Ministro  dell'Interno  può  impartire, per il tramite del Commissario  &#13;
 del Governo, direttive vincolanti ai sindaci, sempre con riguardo  alla  &#13;
 funzione  attribuita  al  comune;  per esigenze di pubblica sicurezza i  &#13;
 provvedimenti, incluso quello qui  considerato,  sono  adottati  previa  &#13;
 comunicazione al prefetto e devono essere sospesi, annullati o revocati  &#13;
 per  motivata  richiesta  dello stesso; il relativo diniego è efficace  &#13;
 solo se il prefetto esprime parere conforme.                             &#13;
     Ora,  non  occorre  indagare  se  così  si   abbia   decentramento  &#13;
 istituzionale  della funzione e di quale tipo o grado esso sia. Ai fini  &#13;
 fiscali, che rilevano per  l'attuale  giudizio  è,  infatti,  decisiva  &#13;
 quest'altra  considerazione.  A  norma dell'art. 8 del d.1. 10 novembre  &#13;
 1978, n. 702, convertito nella legge 8 gennaio 1979, n. 3, gli  atti  e  &#13;
 provvedimenti  emessi dai comuni nell'esercizio delle proprie funzioni,  &#13;
 comprese quelle attribuite dal d.P.R. n.   616/77 e per  le  quali  sia  &#13;
 dovuta  la  tassa  sulle  concessioni governative, sono assoggettati, a  &#13;
 decorrere dal 1 gennaio  1979,  a  tassa  sulle  concessioni  comunali.  &#13;
 L'individuazione  degli  atti  soggetti a tale ultima tassa è rimessa,  &#13;
 nel quinto comma dello stesso articolo,  ad  un  decreto  del  Ministro  &#13;
 delle  Finanze, da emanarsi secondo le modalità ivi prescritte; quello  &#13;
 adottato nella specie (d.m. 29 novembre 1978) prevede, infatti,  al  n.  &#13;
 16  dell'annessa  tabella,  fra  gli  atti  gravati  dalla  tassa sulle  &#13;
 concessioni comunali, la licenza per l'esercizio dell'arte tipografica.  &#13;
 La licenza prevista nell'art. 111 del T. U. di  pubblica  sicurezza  è  &#13;
 dunque  esclusa  dai  provvedimenti  soggetti a tassa sulle concessioni  &#13;
 governative nell'àmbito in cui opera il regime fiscale  dettato  dalla  &#13;
 legge  statale  in conformità ed attuazione dell'art. 19 del d.P.R. n.  &#13;
 616. Non si vede, allora, perché la Regione del Trentino - Alto  Adige  &#13;
 non  possa  adottare  il  trattamento  tributario  disposto dalla norma  &#13;
 impugnata; il contestato onere fiscale è fatto gravare su un atto  che  &#13;
 non  figura  più  tra  le  concessioni  governative;  ed esso è stato  &#13;
 imposto, occorre concludere, sul razionale e legittimo presupposto  che  &#13;
 la  licenza  rilasciata  dal  comune  equivale, per quanto concerne  la  &#13;
 presente controversia, a quella  rimessa  al  Presidente  della  Giunta  &#13;
 provinciale.                                                             &#13;
     3.  -  Il  secondo  motivo  di ricorso dello Stato è fondato.   Il  &#13;
 principio al quale la legge regionale doveva  nella  specie  adeguarsi,  &#13;
 secondo  il  primo  comma  dell'art.  73  dello statuto, discende, come  &#13;
 osserva l'Avvocatura, dall'art. 6 del d.P.R. n. 1199/71 e dall'art.  11  &#13;
 del  d.P.R.  641/72.  L'una  e  l'altra  delle  anzidette  disposizioni  &#13;
 riguardano, ciascuna nel proprio àmbito, puntualmente  il  settore  in  &#13;
 cui  la  Regione  ha preteso di esercitare la sua potestà impositiva e  &#13;
 legislativa. L'art. 6 del d.P.R. n. 1199/71 statuisce,  in  materia  di  &#13;
 ricorsi amministrativi, che, decorso il termine di novanta giorni dalla  &#13;
 data  di  presentazione  del  ricorso  senza  che  l'organo adito abbia  &#13;
 comunicato la decisione, il ricorso si intende  respinto  a  tutti  gli  &#13;
 effetti.  Analogo  criterio  è sancito, con specifico riferimento alla  &#13;
 disciplina delle tasse sulle concessioni governative, nel  terzo  comma  &#13;
 dell'art.  11 del d.P.R. n. 641/1972. La normativa invocata in giudizio  &#13;
 dallo  Stato  costituisce,  secondo  statuto,  un  limite  della  sfera  &#13;
 attribuita  al  legislatore  del  Trentino  - Alto Adige; limite che il  &#13;
 censurato regime delle tasse sulle concessioni regionali ha mancato  di  &#13;
 rispettare.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1) dichiara l'illegittimità costituzionale dell'art. 3 della legge  &#13;
 della   Regione  Trentino  -  Alto  Adige,  riapprovata  dal  Consiglio  &#13;
 regionale il 18 ottobre 1977 e recante "Modificazioni  ed  integrazioni  &#13;
 alla  legge  regionale 29 dicembre 1975, n. 14, concernente: Disciplina  &#13;
 delle tasse regionali e delle soprattasse provinciali sulle concessioni  &#13;
 non governative", nella parte in cui comprende - ad integrazione  della  &#13;
 tabella allegata alla legge regionale n.  14 del 1975 - fra le tasse di  &#13;
 spettanza regionale, al numero 67 (*), quella per la licenza prescritta  &#13;
 dall'art.  115  del  Testo  Unico  di  pubblica  sicurezza per aprire o  &#13;
 condurre agenzie (**) di affari;                                         &#13;
     2) dichiara l'illegittimità costituzionale dell'art. 1 della legge  &#13;
 della  Regione  Trentino  -  Alto  Adige,  riapprovata  dal   Consiglio  &#13;
 regionale il 18 ottobre 1977 di cui al n. 1);                            &#13;
     3) dichiara non fondata la questione di legittimità costituzionale  &#13;
 dell'art.  3  della legge della Regione Trentino - Alto Adige di cui al  &#13;
 n. 1), nella parte in cui - ad integrazione della tabella allegata alla  &#13;
 legge regionale n.  14 del 1975 - comprende fra le tasse regionali,  al  &#13;
 numero  66,  quella  relativa  alla  licenza per l'esercizio delle arti  &#13;
 tipografiche di cui all'art. 111 del Testo Unico di pubblica sicurezza,  &#13;
 sollevata dal Presidente del Consiglio  dei  ministri,  in  riferimento  &#13;
 agli artt. 73 e 77 dello statuto speciale per il Trentino - Alto Adige,  &#13;
 con il ricorso in epigrafe.                                              &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 30 giugno 1986.                               &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO  GRECO - GABRIELE PESCATORE  &#13;
                                   -  UGO  SPAGNOLI  -  FRANCESCO  PAOLO  &#13;
                                   CASAVOLA.                              &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
