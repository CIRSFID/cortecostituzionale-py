<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>71</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:71</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GUIZZI</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/03/2000</data_decisione>
    <data_deposito>17/03/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Francesco GUIZZI; &#13;
 Giudici: Cesare MIRABELLI, Fernando SANTOSUOSSO, Massimo VARI, Cesare &#13;
 RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale &#13;
 MARINI, Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>Nel  giudizio  di  legittimità  costituzionale della legge della    &#13;
 Regione Sardegna, riapprovata il 27 febbraio 1997, recante "Norme per    &#13;
 l'esercizio   delle   competenze  in  materia  di  tutela  paesistica    &#13;
 trasferite  alla  Regione  autonoma  della  Sardegna con l'art. 6 del    &#13;
 d.P.R.  22 maggio 1975, n. 480 e delegate con l'art. 57 del d.P.R. 19    &#13;
 giugno  1979,  n. 348",  promosso  con  ricorso  del  Presidente  del    &#13;
 Consiglio  dei  Ministri,  notificato il 20 marzo 1997, depositato in    &#13;
 cancelleria  il  27  successivo  ed  iscritto  al  n. 29 del registro    &#13;
 ricorsi 1997.                                                            &#13;
     Visto l'atto di costituzione della Regione Sardegna;                 &#13;
     Udito  nell'udienza  pubblica  dell'8  febbraio  2000  il giudice    &#13;
 relatore Carlo Mezzanotte;                                               &#13;
     Uditi l'avvocato dello Stato Pier Giorgio Ferri per il Presidente    &#13;
 del  Consiglio  dei  Ministri  e  l'avvocato  Sergio  Panunzio per la    &#13;
 Regione Sardegna.                                                        &#13;
     Ritenuto   che   il   Presidente   del  Consiglio  dei  Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura  generale  dello Stato, con    &#13;
 ricorso  notificato  il  20  marzo  1997  e  depositato  il  27 marzo    &#13;
 successivo,  ha  impugnato  in via principale la delibera legislativa    &#13;
 della  Regione  autonoma  della  Sardegna,  approvata  dal  Consiglio    &#13;
 regionale  il 30 maggio 1996, riapprovata a maggioranza assoluta, con    &#13;
 modificazioni,  il  27  febbraio  1997  (Norme  per l'esercizio delle    &#13;
 competenze  in  materia  di tutela paesistica trasferite alla Regione    &#13;
 autonoma  della  Sardegna  con  l'art. 6  del  d.P.R. 22 maggio 1975,    &#13;
 n. 480, e delegate con l'art. 57 del d.P.R. 19 giugno 1979, n. 348);     &#13;
         che,   ad  avviso  del  ricorrente,  la  delibera  impugnata,    &#13;
 sostituendo  l'Assessore  regionale  della  pubblica istruzione nelle    &#13;
 competenze  che nella legislazione statale spettano al Ministro, e la    &#13;
 Giunta   regionale  in  quelle  che  spettano  al  Governo  (art. 2),    &#13;
 attribuendo  all'Assessore  poteri  sostitutivi rispetto a quelli del    &#13;
 Sindaco  (art. 9), ed una potestà di riesame delle autorizzazioni da    &#13;
 questo  rilasciate (art. 10), ed eliminando, in definitiva, il potere    &#13;
 statale  di  annullamento  d'ufficio  delle autorizzazioni regionali,    &#13;
 posto  a  estrema  salvaguardia  del  paesaggio,  violerebbe le norme    &#13;
 fondamentali  di  riforma  economico-sociale  contenute nella legge 8    &#13;
 agosto  1985,  n. 431  (Conversione  in legge, con modificazioni, del    &#13;
 decreto-legge  27  giugno  1985, n. 312, recante disposizioni urgenti    &#13;
 per  la  tutela  delle  zone di particolare interesse ambientale), il    &#13;
 principio  di  leale  collaborazione e quello di buon andamento della    &#13;
 pubblica amministrazione;                                                &#13;
         che  si  è  costituita in giudizio la Regione autonoma della    &#13;
 Sardegna,  eccependo l'inammissibilità e comunque l'infondatezza del    &#13;
 ricorso;                                                                 &#13;
         che nel ricorso l'Avvocatura generale dello Stato ha riferito    &#13;
 che  il  Governo,  nei  confronti della medesima delibera legislativa    &#13;
 riapprovata,  ha disposto, contestualmente all'impugnazione innanzi a    &#13;
 questa  Corte,  un nuovo rinvio all'esame del Consiglio regionale, ai    &#13;
 sensi  dell'art. 33,  primo  comma,  della  legge  costituzionale  26    &#13;
 febbraio  1948,  n. 3,  recante  lo  statuto  speciale  della Regione    &#13;
 Sardegna;                                                                &#13;
         che   in  data  13  novembre  1998  la  difesa  regionale  ha    &#13;
 depositato copia del bollettino ufficiale della Regione del 21 agosto    &#13;
 1998 nel quale è stata pubblicata la legge regionale 12 agosto 1998,    &#13;
 n. 28  (Norme  per  l'esercizio delle competenze in materia di tutela    &#13;
 paesistica  trasferite  alla  Regione  autonoma  della  Sardegna  con    &#13;
 l'art. 6  del d.P.R. 22 maggio 1975, n. 480, e delegate con l'art. 57    &#13;
 del d.P.R. 19 giugno 1979, n. 348);                                      &#13;
         che in data 20 gennaio 2000 l'Avvocatura generale dello Stato    &#13;
 ha  depositato  atto nel quale, dopo aver premesso che è venuto meno    &#13;
 l'interesse  a  una  pronuncia  di  questa  Corte a seguito del visto    &#13;
 deciso  dal  Consiglio  dei  ministri  nella seduta del 6 agosto 1998    &#13;
 sulla   nuova   delibera  legislativa  regionale,  ha  dichiarato  di    &#13;
 rinunciare al ricorso;                                                   &#13;
         che  la  Regione  autonoma  della Sardegna in data 27 gennaio    &#13;
 2000 ha depositato atto di accettazione della rinuncia al ricorso.       &#13;
     Considerato   che   la   rinuncia   al   ricorso,  seguita  dalla    &#13;
 accettazione della controparte, comporta, ai sensi dell'art. 25 delle    &#13;
 norme  integrative per i giudizi innanzi a questa Corte, l'estinzione    &#13;
 del processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara estinto il processo.                                        &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 6 marzo 2000.                                 &#13;
                         Il Presidente: Guizzi                            &#13;
                       Il redattore: Mezzanotte                           &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 17 marzo 2000.                          &#13;
               Il direttore della cancelleria: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
