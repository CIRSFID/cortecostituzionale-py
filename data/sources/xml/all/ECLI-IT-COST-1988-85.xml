<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>85</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:85</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1988</data_decisione>
    <data_deposito>26/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale della legge 23 aprile    &#13;
 1981, n. 154,  recante:  "Norme  in  materia  di  ineleggibilità  ed    &#13;
 incopatibilità  alle  cariche di consigliere regionale, provinciale,    &#13;
 comunale e circoscrizionale ed in materia di  incompatibilità  degli    &#13;
 addetti  al  servizio  sanitario nazionale", promosso con ricorso del    &#13;
 Presidente della Giunta regionale della Sardegna,  notificato  il  27    &#13;
 maggio  1981,  depositato  in cancelleria il 5 giugno (successivo) ed    &#13;
 iscritto al n. 26 del registro ricorsi 1981.                             &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza  pubblica  del  10  novembre  1987  il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Uditi  l'avv.  Sergio  Panunzio  per  la Regione Sardegna e l'avv.    &#13;
 Sergio Laporta dello  Stato  per  il  Presidente  del  Consiglio  dei    &#13;
 ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Con ricorso notificato il 27 maggio 1981 la Regione Sardegna    &#13;
 ha sollevato -  nell'ipotesi  che  fosse  ritenuta  applicabile  alla    &#13;
 Regione stessa - questione di legittimità costituzionale della legge    &#13;
 statale 23 aprile 1981, n. 154 ("Norme in materia di  ineleggibilità    &#13;
 ed   incompatibilità   alle   cariche   di   consigliere  regionale,    &#13;
 provinciale, circoscrizionale ed in materia di incompatibilità degli    &#13;
 addetti al servizio sanitario nazionale") nel suo complesso e, in via    &#13;
 subordinata,  degli  artt.  1  e  4  per  addotto  contrasto  con  le    &#13;
 disposizioni  dello Statuto speciale di autonomia approvato con legge    &#13;
 costituzionale  26  febbraio  1948,  n.  3  e  modificato  con  legge    &#13;
 costituzionale 23 febbraio 1972, n. 1.                                   &#13;
    2.  -  Premesso  che  la  citata legge statale non dovrebbe essere    &#13;
 ritenuta applicabile alla Regione Sardegna, posto che non ha abrogato    &#13;
 il  d.P.R.  12  dicembre  1948,  n. 1462, emanato in attuazione degli    &#13;
 artt. 17 e 55 dello Statuto, recante "norme per la prima elezione del    &#13;
 Consiglio  regionale  della  Sardegna"  e  dettante  una  specifica e    &#13;
 compiuta disciplina delle cause di  ineleggibilità,  si  afferma  in    &#13;
 ricorso che ove, peraltro, la legge n. 154 del 1981 fosse applicabile    &#13;
 anche alla Regione sarda, essa sarebbe incostituzionale in quanto non    &#13;
 specificamente emessa per la Sardegna, in contrasto col "significato"    &#13;
 da attribuire all'art. 17 dello Statuto che,  dopo  aver  contemplato    &#13;
 tre  casi  di  incompatibilità,  prevede,  all'ultimo comma, che gli    &#13;
 altri casi di ineleggibilità e di  incompatibilità  sono  stabiliti    &#13;
 con legge dello Stato.                                                   &#13;
    La  ritenuta  necessità  di  una  specifica norma statale sarebbe    &#13;
 avallata da un triplice ordine di considerazioni:                        &#13;
      a) dalla specialità dell'autonomia della Sardegna, le cui forme    &#13;
 e condizioni particolari sono garantite dall'art. 116 Cost.;             &#13;
      b)   dall'orientamento  costantemente  seguito  dal  legislatore    &#13;
 statale che - eccezion fatta per il Trentino  Alto  Adige  e  per  la    &#13;
 Sicilia,  dove  i  casi  di  ineleggibilità ed incompatibilità sono    &#13;
 disciplinati con leggi regionali - ha sempre dettato norme elettorali    &#13;
 di  carattere  generale  per  le  sole  regioni  a statuto ordinario,    &#13;
 apprestando specifiche disposizioni per la Sardegna, Valle d'Aosta  e    &#13;
 Friuli Venezia Giulia;                                                   &#13;
      c)  dal  richiamo  operato  dal  d.P.R.  n.  1462  del 1948 alle    &#13;
 disposizioni  per  l'elezione  della  Camera  dei  deputati  e  dalla    &#13;
 conseguente inammissibilità dell'assimilazione dei consiglieri delle    &#13;
 regioni a statuto speciale ai consiglieri comunali e provinciali.        &#13;
    3.  -  In ogni caso - si afferma in ricorso - l'art. 4 della legge    &#13;
 in parola, stabilendo che  la  carica  di  consigliere  regionale  è    &#13;
 incompatibile  con quella di sindaco di qualunque comune compreso nel    &#13;
 territorio della Regione, sarebbe in palese contrasto  con  la  norma    &#13;
 statutaria di cui all'art. 17, secondo comma, legge costituzionale n.    &#13;
 3 del 1948, la quale, invece, detta  che  "l'ufficio  di  consigliere    &#13;
 regionale  è  incompatibile con quello... di un sindaco di un comune    &#13;
 con popolazione superiore a diecimila abitanti".  Né  -  si  afferma    &#13;
 testualmente  -  potrebbe "obiettarsi che l'ultimo comma dell'art. 17    &#13;
 demanda alla legge  dello  Stato  di  stabilire  gli  altri  casi  di    &#13;
 incompatibilità,   perché  proprio  questa  precisa  dizione  fissa    &#13;
 l'assoluta   inderogabilità    del    caso    di    incompatibilità    &#13;
 esplicitamente previsto dalla norma statutaria".                         &#13;
    4.  -  Evidentemente  -  conclude la Regione Sardegna - appare del    &#13;
 pari l'incostituzionalità dell'art. 1 della legge n.  154  del  1981    &#13;
 laddove  stabilisce  che  "sono eleggibili a consigliere regionale...    &#13;
 gli elettori di un qualsiasi  comune  della  Repubblica  che  abbiano    &#13;
 compiuto..."  in  riferimento all'art. 17, primo comma, dello Statuto    &#13;
 speciale d'autonomia, secondo il quale "è elettore ed eleggibile  al    &#13;
 consiglio  regionale  chi  è  iscritto  nelle liste elettorali della    &#13;
 Regione".                                                                &#13;
    5.  - l'Avvocatura dello Stato, costituitasi per il Presidente del    &#13;
 Consiglio dei ministri, ha chiesto che il ricorso venga rigettato.       &#13;
    Premesso   che  l'art.  122  Cost.  distingue  tra  le  norme  che    &#13;
 disciplinano  il  sistema  di  elezione  da  quelle   sull'elettorato    &#13;
 passivo,  che  la Corte costituzionale, con sentenze n. 105 del 1957,    &#13;
 n. 60 del 1966 e n.  108  del  1968,  ha  statuito  debba  avere  una    &#13;
 disciplina  uniforme  per tutti i cittadini e per tutto il territorio    &#13;
 nazionale a mente dell'art. 51 Cost., si osserva  sostanzialmente  in    &#13;
 atto  di  intervento  che nessun dubbio può sussistere sulla diretta    &#13;
 applicabilità della legge impugnata alla Sardegna, attesa la riserva    &#13;
 di  legge  statale  espressamente  posta  dalla norma statutaria ed i    &#13;
 limiti anche temporali fissati dall'art.  55,  secondo  comma,  dello    &#13;
 Statuto, alla efficacia del d.P.R. n. 1462 del 1948, concernente solo    &#13;
 la prima elezione del Consiglio regionale e  contenente  un  richiamo    &#13;
 alle   disposizioni   per   l'elezione   della  Camera  dei  deputati    &#13;
 esclusivamente  ai  fini  della  disciplina  delle   elezioni   (oggi    &#13;
 direttamente regolate dalla legge regione sarda 23 marzo 1961, n. 4),    &#13;
 non già dei casi di ineleggibilità ed incompatibilità.                &#13;
    Nel  merito  l'Avvocatura  rileva  che  la  legge  impugnata si è    &#13;
 limitata,  con  l'art.  4,   a   fissare   un   ulteriore   caso   di    &#13;
 incompatibilità  che  si aggiunge ai tre già previsti dall'art. 17,    &#13;
 secondo comma, dello Statuto; e con l'art. 1 a stabilire  una  regola    &#13;
 che  non  è  ovviamente applicabile nella Regione Sardegna attesa la    &#13;
 diversa  disposizione  statutaria,  di  rango  costituzionale  e   di    &#13;
 carattere  speciale,  che  stabilisce  che è eleggibile al Consiglio    &#13;
 regionale chi è iscritto nelle liste elettorali  della  Regione.  Ma    &#13;
 ciò  ovviamente  non comporta la illegittimità costituzionale della    &#13;
 norma statale, che trova piena  applicazione  in  tutto  il  restante    &#13;
 territorio della Repubblica.<diritto>Considerato in diritto</diritto>1. - La questione di legittimità costituzionale, sollevata in via    &#13;
 principale  dal  Presidente  della  regione  Sardegna  va  dichiarata    &#13;
 inammissibile, stante l'erroneità del presupposto da cui essa muove,    &#13;
 consistente nella ritenuta applicabilità  anche  alle  elezioni  dei    &#13;
 consiglieri   regionali   sardi   dei   casi  di  ineleggibilità  ed    &#13;
 incompatibilità disciplinati dalla legge statale 23 aprile  1981  n.    &#13;
 154.                                                                     &#13;
    2.  -  Invero,  il  rapporto  tra  lo statuto regionale sardo e la    &#13;
 citata legge n. 154 del 1981 si configura nella parte che  interessa,    &#13;
 non   soltanto   in   termini   di   sovraordinazione   della   norma    &#13;
 costituzionale rispetto a quella ordinaria, ma anche  quale  rapporto    &#13;
 fra legge speciale e legge ordinaria: la disciplina dei casi suddetti    &#13;
 trova cioè la sua speciale regolamentazione nello statuto  regionale    &#13;
 e  nelle  relative  norme  di  attuazione,  con la conseguenza che la    &#13;
 disciplina dei medesimi casi, dettata in via generale con legge dello    &#13;
 Stato,  non può configurarsi, per tale ragione, come automaticamente    &#13;
 derogatoria o integrativa di quella speciale. Ciò  dicasi  non  solo    &#13;
 con  riferimento  ai  casi di ineleggibilità di cui all'art. 1 della    &#13;
 legge n. 154 del 1981, ma altresì con particolare riguardo a  quelli    &#13;
 di   incompatibilità  sanciti  dal  successivo  art.  4,  dovendosi,    &#13;
 relativamente ad essi e per le testé esposte ragioni, osservare  che    &#13;
 se  tale  norma  avesse  inteso  riferirsi  alla  regione ricorrente,    &#13;
 muovendosi nello spazio proprio della  riserva  di  cui  all'art.  17    &#13;
 dello  Statuto di tale regione, avrebbe dovuto in tal senso contenere    &#13;
 una espressa previsione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara inammissibile la questione di legittimità costituzionale    &#13;
 della legge 23 aprile 1981 n. 154, in toto e degli artt. 1 e 4  della    &#13;
 medesima  legge,  sollevata,  in riferimento agli artt. 17 e 55 della    &#13;
 legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale regione    &#13;
 Sardegna),  dal  Presidente  della  regione  Sardegna  col ricorso in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
