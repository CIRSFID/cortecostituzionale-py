<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>31</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:31</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Antonino Papaldo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/03/1967</data_decisione>
    <data_deposito>22/03/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. &#13;
 LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt.  108, 117 e  &#13;
 118 del D.P.R. 12 febbraio 1965, n. 162, promosso con ordinanza  emessa  &#13;
 il  31  ottobre  1966  dal Pretore di Pistoia nel procedimento penale a  &#13;
 carico di Livi Rodolfo, iscritta al n. 226 del Registro ordinanze  1966  &#13;
 e  pubblicata  nella  Gazzetta  Ufficiale della Repubblica n. 12 del 14  &#13;
 gennaio 1967.                                                            &#13;
     Udita nella camera di consiglio del 16 marzo 1967 la relazione  del  &#13;
 Giudice Antonino Papaldo.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza emessa il 31 ottobre 1966 dal Pretore di Pistoia nel  &#13;
 procedimento penale a carico di Livi  Rodolfo  è  stata  sollevata  la  &#13;
 questione di legittimità costituzionale degli artt. 108, 117 e 118 del  &#13;
 D.P.R.  12  febbraio  1965,  n.  162,  in riferimento all'art. 77 della  &#13;
 Costituzione, in quanto con tale legge delegata  sono  state  comminate  &#13;
 pene accessorie non previste dalla legge delega 9 ottobre 1964, n. 991,  &#13;
 e  si  è  fatto  richiamo  al  decreto - legge del 15 ottobre 1925, n.  &#13;
 2033, e ad altre disposizioni quando nella legge  delega  si  prevedeva  &#13;
 l'emanazione  di una legge ordinaria che disciplinasse organicamente la  &#13;
 produzione dei mosti, vini ed aceti, cioè in  modo  completo  e  senza  &#13;
 riferimento a precedenti norme regolanti la stessa materia.              &#13;
     L'ordinanza   è   stata   ritualmente   notificata,  comunicata  e  &#13;
 pubblicata, ma nessuno si è costituito in questa sede.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il fatto che la legge delegata  abbia  dichiarato  di  tener  ferme  &#13;
 alcune  disposizioni  della  legge  sulla  repressione  delle frodi nei  &#13;
 prodotti agrari e del relativo regolamento nonché le disposizioni  del  &#13;
 testo  unico  delle  leggi  sanitarie,  non  costituisce  vizio  di non  &#13;
 conformità alla delega. Questa ha demandato al Governo di disciplinare  &#13;
 organicamente la  materia.  Ma  non  è  esatto  che  per  disciplinare  &#13;
 organicamente  una materia la legge delegata debba comprendere un corpo  &#13;
 di norme completo senza alcun riferimento a norme precedenti.            &#13;
     La  Corte,  pur  dovendo  ripetere  che  in  tema  di  giudizio  di  &#13;
 conformità  alle deleghe legislative non si possono delineare principi  &#13;
 generali valevoli per tutti i casi, ritiene  che  la  legge  in  esame,  &#13;
 imponendo  la organicità della disciplina, non importava il divieto di  &#13;
 fare riferimento a norme precedenti.                                     &#13;
     Disciplinare in via organica non significa necessariamente  dettare  &#13;
 un corpo di norme nuove ed autonome. Può bene essere organico un corpo  &#13;
 di norme che richiamino in parte norme precedenti.                       &#13;
     Nella  specie questa considerazione è tanto più valida se si tien  &#13;
 presente che il tener ferme norme del testo unico delle leggi sanitarie  &#13;
 e della legge sulle frodi nei prodotti agrari non  appare  contrastante  &#13;
 con  un  criterio  di  organicità,  trattandosi  di  norme  contenenti  &#13;
 principi generali sui poteri  dell'Amministrazione  sanitaria  e  degli  &#13;
 altri  organi  che  operano  nel  campo  della  tutela  igienica  degli  &#13;
 alimenti.                                                                &#13;
     Per quanto si riferisce alla censura relativa alla comminazione  di  &#13;
 pene  accessorie  non  previste  dalla  legge delega, la questione deve  &#13;
 essere  dichiarata  manifestamente   infondata,   non   essendo   state  &#13;
 prospettate  nuove  o diverse argomentazioni rispetto alla pronuncia di  &#13;
 questa Corte emanata sulla stessa questione con sentenza n.  14  del  1  &#13;
 febbraio 1967.                                                           &#13;
     La sostanziale infondatezza delle questioni dispensa dall'esaminare  &#13;
 se  sia esatto il richiamo dell'art. 77 della Costituzione che si legge  &#13;
 nell'ordinanza di rinvio.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 degli  artt.  117  e  118  del  D.  P.  R.  12  febbraio  1965, n. 162,  &#13;
 concernente "Norme per la repressione delle frodi nella preparazione  e  &#13;
 nel  commercio  dei  mosti,  vini  ed aceti" e manifestamente infondata  &#13;
 quella dell'art. 108 dello stesso decreto, sollevate con l'ordinanza di  &#13;
 cui in epigrafe.                                                         &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 16 marzo 1967           &#13;
                                   GASPARE  AMBROSINI - ANTONINO PAPALDO  &#13;
                                   - NICOLA JAEGER - GIOVANNI  CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
