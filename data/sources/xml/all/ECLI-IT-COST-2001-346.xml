<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>346</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:346</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/11/2001</data_decisione>
    <data_deposito>06/11/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, &#13;
Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale della legge regionale &#13;
Emilia-Romagna   24  aprile  1995,  n. 52  (Integrazioni  alla  legge &#13;
regionale  25 gennaio 1983, n. 6 "Diritto allo studio"), promosso con &#13;
ordinanza  emessa  il  20  dicembre 1999 dal Tribunale amministrativo &#13;
regionale  dell'Emilia-Romagna  sul  ricorso  proposto  dal  Comitato &#13;
bolognese   "Scuola  e  Costituzione"  ed  altri  contro  la  Regione &#13;
Emilia-Romagna,  iscritta  al  n. 491  del  registro ordinanze 2000 e &#13;
pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 39, prima &#13;
serie speciale, dell'anno 2000. &#13;
    Visti  gli  atti di costituzione del Comitato bolognese "Scuola e &#13;
Costituzione"  ed altri, della regione Emilia-Romagna, nonché l'atto &#13;
di  intervento  della  Federazione  italiana scuole materne (FISM) ed &#13;
altri; &#13;
    Udito  nell'udienza  pubblica  del  10  luglio  2001  il  giudice &#13;
relatore Franco Bile; &#13;
    Uditi  gli avvocati Massimo Luciani, Federico Sorrentino, Corrado &#13;
Mauceri,  Maria  Virgilio e Sergio Panunzio per il Comitato bolognese &#13;
"Scuola  e Costituzione" ed altri, Giandomenico Falcon per la Regione &#13;
Emilia-  Romagna,  Michele  Scudiero,  Mauro  Giovannelli  e Giuseppe &#13;
Totaro per la Federazione italiana scuole materne ed altri. &#13;
    Ritenuto  che con ordinanza in data 20 dicembre 1999 il Tribunale &#13;
amministrativo   regionale   dell'Emilia-Romagna   ha   sollevato  la &#13;
questione  di  legittimità  costituzionale della legge della Regione &#13;
Emilia-Romagna   24 aprile   1995,  n. 52  (Integrazioni  alla  legge &#13;
regionale   25   gennaio  1983,  n. 6  "Diritto  allo  studio"),  per &#13;
violazione degli artt. 33, primo, secondo e terzo comma, e 117, primo &#13;
comma, della Costituzione; &#13;
        che  l'ordinanza  è  stata  emessa  nel corso di un giudizio &#13;
promosso  avanti  al  Tribunale amministrativo regionale dal Comitato &#13;
bolognese  "Scuola e Costituzione", dalla Chiesa Evangelica Metodista &#13;
di  Bologna,  dalla Chiesa Cristiana Avventista del Settimo Giorno di &#13;
Bologna   e   dalla   Comunità  Ebraica  di  Bologna,  per  ottenere &#13;
l'annullamento    della   deliberazione   del   Consiglio   regionale &#13;
dell'Emilia-Romagna  in  data  28  settembre 1995, n. 97, concernente &#13;
l'approvazione  dei  criteri per l'assegnazione ai comuni, per l'anno &#13;
1995,   dei   contributi   previsti  dalla  legge  citata,  in  vista &#13;
dell'attivazione  di  convenzioni per la qualificazione e il sostegno &#13;
delle  scuole dell'infanzia gestite da soggetti privati senza fini di &#13;
lucro; &#13;
        che, come risulta dall'ordinanza, il Tribunale amministrativo &#13;
regionale,  con  sentenza  in  data  1  aprile 1997, aveva dichiarato &#13;
inammissibile  il  secondo  ed  il  terzo motivo del ricorso ed aveva &#13;
accolto   il   primo   motivo  (riconoscendo  l'illegittimità  della &#13;
delibera,  per  violazione  della legge regionale, nella parte in cui &#13;
prevedeva la ripartizione dei fondi anche in favore di comuni che non &#13;
avessero  stipulato  le ricordate convenzioni) e contestualmente, con &#13;
separata  ordinanza,  aveva  proposto  la  questione  di legittimità &#13;
costituzionale  della  medesima  legge  regionale in riferimento agli &#13;
artt. 33,   secondo   e  terzo  comma,  e  117,  primo  comma,  della &#13;
Costituzione; &#13;
        che  la  Corte costituzionale, con ordinanza n. 67 del 1998 - &#13;
ritenuto  ammissibile  l'intervento  della FISM (Federazione italiana &#13;
scuole   materne)   -   ha  dichiarato  la  questione  manifestamente &#13;
inammissibile,  rilevando  che con la sentenza di accoglimento di una &#13;
parte del ricorso, per violazione della legge impugnata, il giudice a &#13;
quo  aveva  già  fatto  applicazione di essa; che, pertanto, ai fini &#13;
della motivazione sulla rilevanza, egli avrebbe dovuto "dar conto del &#13;
fatto  che  non  si  fosse  ormai  esaurito  il suo potere decisorio, &#13;
rimanendo   come   unico   oggetto   del  giudizio  le  questioni  di &#13;
legittimità   costituzionale  sollevate  dai  ricorrenti  in  logica &#13;
subordinazione  all'ipotesi  che  l'impugnata delibera fosse ritenuta &#13;
conforme  a  legge";  che,  invece, il rimettente si era "limitato ad &#13;
affermare  in modo apodittico la rilevanza della sollevata questione, &#13;
il cui accoglimento peraltro avrebbe reso inutiliter data la sentenza &#13;
ch'egli aveva già pronunciato"; &#13;
        che  -  sulla  base  di  queste  premesse  - il giudice a quo &#13;
ripropone  la  questione  di legittimità costituzionale della citata &#13;
legge regionale, in riferimento agli artt. 33, primo, secondo e terzo &#13;
comma,  e 117, primo comma, della Costituzione, ritenendola rilevante &#13;
e non manifestamente infondata; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
tramite  l'Avvocatura  generale dello Stato, il quale con una memoria &#13;
ha  chiesto  che la questione sia dichiarata inammissibile o comunque &#13;
infondata,   e   poi,   con   la  memoria  depositata  nell'imminenza &#13;
dell'udienza, ha dichiarato di "revocare" l'intervento; &#13;
        che si sono costituite congiuntamente le parti ricorrenti del &#13;
giudizio a quo, chiedendo l'accoglimento della questione; &#13;
        che  si  è  costituita la Regione Emilia-Romagna, sostenendo &#13;
l'inammissibilità e l'infondatezza della questione; &#13;
        che  ha  spiegato  intervento  la  FISM (Federazione italiana &#13;
scuole  materne), già intervenuta nel giudizio deciso dall'ordinanza &#13;
n. 67  del  1998, rilevando che la legge regionale impugnata è stata &#13;
abrogata,  e  sostenendo  l'inammissibilità  e  l'infondatezza della &#13;
questione. &#13;
    Considerato  che l'ordinanza n. 67 del 1998 di questa Corte - con &#13;
cui  la  questione  proposta  dal  rimettente, ed oggi riproposta, è &#13;
stata   dichiarata   manifestamente   inammissibile   -  ha  ritenuto &#13;
ammissibile  l'intervento  già allora spiegato dalla FISM, del quale &#13;
persistono le condizioni di ammissibilità; &#13;
        che  la presente questione di legittimità costituzionale è, &#13;
come  quella  decisa  dall'ordinanza  n. 67  del 1998, manifestamente &#13;
inammissibile,  anche a prescindere dal totale difetto di motivazione &#13;
dell'ordinanza  di  rimessione  (emessa  in  data  20  dicembre 1999) &#13;
relativamente  all'incidenza  sul giudizio pendente della legge della &#13;
Regione  Emilia-Romagna  25 maggio 1999, n. 10 (Diritto allo studio e &#13;
all'apprendimento  per  tutta  la  vita  e qualificazione del sistema &#13;
formativo  integrato),  il cui art. 16, terzo comma, ha espressamente &#13;
abrogato  la  legge  regionale  n. 52  del  1995,  e relativamente al &#13;
significato   da  attribuire  all'art. 17,  che  in  via  transitoria &#13;
assoggetta  ancora  alla  legge  del  1995  i  soli  "procedimenti di &#13;
erogazione dei benefici di natura finanziaria in corso"; &#13;
        che,  infatti,  il remittente afferma anzitutto che - essendo &#13;
stata  la  delibera  regionale impugnata sia nella sua interezza, sia &#13;
(col  primo  motivo) in riferimento ad un profilo parziale - "oggetto &#13;
largamente   prevalente   del   thema   decidendum"   è  "l'asserita &#13;
illegittimità  derivata  dell'impugnata  delibera per illegittimità &#13;
costituzionale  della  legge  regionale  citata"  onde la sentenza di &#13;
accoglimento del primo motivo non ha esaurito il potere decisorio del &#13;
giudice,   avendo   "definito   soltanto   una  parte  secondaria  (e &#13;
sostanzialmente marginale) dell'oggetto del contendere"; &#13;
        che   l'argomentazione   nulla   di  decisivo  aggiunge  alla &#13;
motivazione  del  precedente  provvedimento  di  rimessione, circa il &#13;
quale  questa Corte, con l'ordinanza n. 67 del 1998, ha già rilevato &#13;
che  il  Tribunale  amministrativo  regionale  -  proprio  in ragione &#13;
dell'accoglimento  del  primo motivo del ricorso - aveva applicato la &#13;
legge  regionale  in esame, utilizzandola come parametro per valutare &#13;
(e in concreto escludere) la legittimità della delibera impugnata, e &#13;
che  non  risultava  come,  a  seguito  di  tale applicazione, non si &#13;
dovesse  considerare  esaurito  il  correlativo  potere decisorio del &#13;
giudice,  con  la  conseguenza  -  ora  come allora - della manifesta &#13;
inammissibilità,     per    irrilevanza,    della    questione    di &#13;
costituzionalità; &#13;
        che, infine, le restanti considerazioni svolte dal remittente &#13;
si  risolvono  in  meri  rilievi critici alla precedente ordinanza di &#13;
questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale della legge della Regione Emilia-Romagna &#13;
24  aprile  1995, n. 52 (Integrazioni alla legge regionale 25 gennaio &#13;
1983,   n. 6   "Diritto   allo   studio"),  sollevata  dal  Tribunale &#13;
amministrativo  regionale  dell'Emilia-Romagna,  in  riferimento agli &#13;
articoli  33, primo, secondo e terzo comma, e 117, primo comma, della &#13;
Costituzione, con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 novembre 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Bile &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 6 novembre 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
