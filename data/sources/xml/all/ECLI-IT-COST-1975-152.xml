<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>152</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:152</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Nicola Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/06/1975</data_decisione>
    <data_deposito>19/06/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI- Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI- Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti di legittimità costituzionale dell'art.  35,  &#13;
 primo e secondo comma, della legge 20 maggio 1970, n. 300 (Statuto  dei  &#13;
 lavoratori), promossi con le seguenti ordinanze:                         &#13;
     1)  ordinanza  emessa il 27 dicembre 1972 dal pretore di Torino nel  &#13;
 procedimento civile vertente tra Tavilla Antonio e la società Fonti di  &#13;
 Baceno, iscritta al n. 244 del registro  ordinanze  1973  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 198 del 1 agosto 1973;      &#13;
     2)  ordinanza  emessa il 26 giugno 1973 dal pretore di Portoferraio  &#13;
 nel procedimento civile vertente tra  Bicecci  Roberto  e  la  società  &#13;
 cooperativa  Consorzio  agrario  provinciale di Livorno, iscritta al n.  &#13;
 293 del registro ordinanze 1973 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n.223 del 29 agosto 1973;                               &#13;
     3)  ordinanza emessa il 15 giugno 1973 dal pretore di Treviglio nel  &#13;
 procedimento civile vertente tra Carminati  Giuseppina  e  la  società  &#13;
 Crouse  Hinds of Europe, iscritta al n. 449 del registro ordinanze 1973  &#13;
 e pubblicata nella Gazzetta Ufficiale della Repubblica  n.  48  del  20  &#13;
 febbraio 1974.                                                           &#13;
     Visti  gli  atti  d'intervento  del  Presidente  del  Consiglio dei  &#13;
 ministri e di costituzione di Bicecci Roberto;                           &#13;
     udito nell'udienza pubblica del 23 aprile 1975 il Giudice  relatore  &#13;
 Nicola Reale;                                                            &#13;
     udito il sostituto avvocato generale dello Stato Renato Carafa, per  &#13;
 il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  - Con ordinanza emessa il 27 dicembre 1972 il pretore di Torino  &#13;
 ha sollevato, in riferimento agli artt. 3, 4 e 35, comma  primo,  della  &#13;
 Costituzione,  questioni  di  legittimità costituzionale dell'art. 35,  &#13;
 primo e secondo comma, della legge 20 maggio 1970, n. 300 (Statuto  dei  &#13;
 lavoratori) nella parte in cui disciplina l'applicabilità alle imprese  &#13;
 agricole,  commerciali  ed  industriali  dell'art.  18  concernente  la  &#13;
 reintegrazione nel posto di lavoro del dipendente il cui  licenziamento  &#13;
 sia stato annullato, dichiarato nullo o inefficace.                      &#13;
     È intervenuto in giudizio il Presidente del Consiglio dei ministri  &#13;
 deducendo, a mezzo dell'Avvocatura generale dello Stato, l'infondatezza  &#13;
 delle proposte questioni di legittimità costituzionale.                 &#13;
     2.  -  La legittimità costituzionale del predetto art. 35 è stata  &#13;
 posta in dubbio in  termini  sostanzialmente  identici,  sia  pure  con  &#13;
 riferimento  soltanto  all'art. 3 Cost., dal pretore di Treviglio e dal  &#13;
 pretore di Portoferraio con ordinanze del 15 e del 26 giugno 1973.       &#13;
     Nel secondo giudizio vi è stata costituzione di una delle parti in  &#13;
 causa che ha concluso per l'accoglimento dell'eccezione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Le tre ordinanze  indicate  in  epigrafe  sollevano  questioni  &#13;
 analoghe;  si  ravvisa  pertanto opportuna la riunione dei giudizi onde  &#13;
 dar luogo a decisione con unica sentenza.                                &#13;
     2. - L'art. 35 della legge 20 maggio  1970,  n.  300  (Statuto  dei  &#13;
 lavoratori)  statuisce nel primo comma che le disposizioni dell'art. 18  &#13;
 di quella stessa legge, le quali prevedono la reintegrazione nel  posto  &#13;
 di  lavoro  del  dipendente  il  cui licenziamento sia stato annullato,  &#13;
 dichiarato nullo o inefficace ai sensi degli artt. 2 e  seguenti  della  &#13;
 legge   15   luglio  1966,  n.  604,  si  applicano  a  ciascuna  sede,  &#13;
 stabilimento,  filiale,  ufficio  o   reparto   autonomo   di   imprese  &#13;
 industriali e commerciali che occupi più di quindici dipendenti e, per  &#13;
 quanto attiene alle imprese agricole, a quelle che, nel loro complesso,  &#13;
 occupino più di cinque dipendenti. Le norme in questione si applicano,  &#13;
 inoltre,   giusta  quanto  disposto  dal  secondo  comma  del  predetto  &#13;
 articolo, anche alle imprese industriali o commerciali che nello stesso  &#13;
 comune occupino più di quindici dipendenti e a  quelle  agricole  che,  &#13;
 nel medesimo ambito territoriale abbiano più di cinque dipendenti, pur  &#13;
 quando  ciascuna  unità  produttiva,  singolarmente  considerata,  non  &#13;
 raggiunga tale limite.                                                   &#13;
     La Corte è chiamata a decidere se tale disciplina contrasti con il  &#13;
 principio di uguaglianza ed, in particolare, con gli artt. 3, 4  e  35,  &#13;
 comma  primo,  della  Costituzione e quindi ad accertare se il criterio  &#13;
 numerico adottato sia tale da determinare  un'irragionevole  disparità  &#13;
 di trattamento nella tutela del diritto alla conservazione del posto di  &#13;
 lavoro:                                                                  &#13;
     a)  tra i dipendenti di imprese commerciali ed industriali e quelli  &#13;
 di imprese agricole in considerazione del fatto che, per queste ultime,  &#13;
 il limite numerico risulta  fissato  in  cinque  anziché  in  quindici  &#13;
 unità;                                                                  &#13;
     b) tra i dipendenti di imprese commerciali o industriali, a seconda  &#13;
 che  essi  risultino  addetti  a  sedi,  stabilimenti, uffici o reparti  &#13;
 autonomi che occupino o meno, singolarmente o nell'ambito  territoriale  &#13;
 di uno stesso comune, più di quindici dipendenti.                       &#13;
     3. - Le questioni non sono fondate.                                  &#13;
     La  ragione  del  diverso  limite  numerico adottato per le imprese  &#13;
 agricole rispetto a  quelle  industriali  va  infatti  ricercata,  come  &#13;
 riconosciuto  anche nei lavori preparatori della legge, nelle sensibili  &#13;
 differenze, organizzative e strutturali, che nella realtà economica  e  &#13;
 sociale del nostro Paese separano le une dalle altre.                    &#13;
     Per  quanto  concerne  la  seconda questione, cui sopra si è fatto  &#13;
 cenno, va considerato che questa Corte, nel dichiarare non fondate,  in  &#13;
 riferimento  all'art.  3  Cost.,  analoghe  questioni  di  legittimità  &#13;
 costituzionale concernenti la normativa in esame, ha già affermato che  &#13;
 essa  trova  il  suo  fondamento,  oltre   che   nel   criterio   della  &#13;
 fiduciarietà del rapporto di lavoro e nell'opportunità di non gravare  &#13;
 di   oneri   eccessivi  le  imprese  di  modeste  dimensioni,  anche  e  &#13;
 soprattutto  nell'esigenza  di  salvaguardare  le  funzionalità  delle  &#13;
 unità  produttive  (intese  quali  articolazioni di una più complessa  &#13;
 organizzazione imprenditoriale, fornite di autonomia così dal punto di  &#13;
 vista economico-strutturale come da quello funzionale o  del  risultato  &#13;
 produttivo) ed in ispecie, di quelle con un minor numero di dipendenti,  &#13;
 nelle  quali  la  reintegrazione  nel  medesimo ambiente del dipendente  &#13;
 licenziato  potrebbe  determinare  il  verificarsi  di  situazioni   di  &#13;
 tensione  nelle quotidiane relazioni umane e di lavoro (sent. n. 55 del  &#13;
 1974).                                                                   &#13;
     Tali considerazioni restano  pienamente  valide  e  fanno  ritenere  &#13;
 infondate,  anche  in riferimento agli artt. 4 e 35, comma primo, della  &#13;
 Costituzione, le questioni sollevate con le ordinanze in  epigrafe,  le  &#13;
 quali  del  resto  sono  tutte  di  data  anteriore alla sentenza sopra  &#13;
 ricordata e non contengono argomenti che possano indurre questa Corte a  &#13;
 mutare la propria giurisprudenza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondate in riferimento agli artt. 3,  4  e  35,  primo  &#13;
 comma,  della Costituzione, le questioni di legittimità costituzionale  &#13;
 dell'art. 35, primo e secondo comma, della legge 20 maggio 1970, n. 300  &#13;
 (Statuto dei lavoratori) sollevate dal pretore di Torino,  dal  pretore  &#13;
 di  Treviglio  e  dal  pretore  di  Portoferraio  con  le  ordinanze in  &#13;
 epigrafe.                                                                &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 6 giugno 1975.                                &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO  AMADEI - GIULIO GIONFRIDA -  &#13;
                                   EDOARDO VOLTERRA  -  GUIDO  ASTUTI  -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
