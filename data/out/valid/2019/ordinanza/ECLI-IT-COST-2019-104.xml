<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-04-24/104/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-04-24/104"/>
          <FRBRalias value="ECLI:IT:COST:2019:104" name="ECLI"/>
          <FRBRdate date="2019-04-24" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="104"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-04-24/104/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-04-24/104/ita@"/>
          <FRBRdate date="2019-04-24" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-04-24/104/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-04-24/104/ita@.xml"/>
          <FRBRdate date="2019-04-24" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2019-03-20" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2019-04-24" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="LATTANZI" href="/akn/ontology/person/it/LATTANZI" showAs="LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="filomenaPerrone" href="/akn/ontology/person/it/filomenaPerrone" showAs="Filomena PERRONE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2019</akn4cc:anno>
        <akn4cc:numero>104</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2019:104</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>LATTANZI</akn4cc:presidente>
        <akn4cc:relatore>Giovanni Amoroso</akn4cc:relatore>
        <akn4cc:data_decisione>2019-03-20</akn4cc:data_decisione>
        <akn4cc:data_deposito>2019-04-24</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/1982-11-20/890/!main#art_14">art. 14 della legge 20 novembre 1982, n. 890</ref> (Notificazione di atti a mezzo posta e di comunicazioni a mezzo posta connesse con la notificazione di atti giudiziari), e dell'<ref href="/akn/it/act/legge/stato/2006-12-27/296/!main#art_1__para_161">art. 1, comma 161, della legge 27 dicembre 2006, n. 296</ref>, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2007)», promosso dalla Commissione tributaria regionale della Campania nel giudizio vertente tra l'Agenzia delle entrate-Riscossione - Napoli, subentrata a Equitalia Servizi Riscossione spa, e Andrea Mignone, con ordinanza del 10 novembre 2017, iscritta al n. 95 del registro ordinanze 2018 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 27, prima serie speciale, dell'anno 2018.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 6 marzo 2019 il Giudice relatore Giovanni Amoroso.
 Ritenuto che con ordinanza del 10 novembre 2017, la Commissione tributaria regionale della Campania (di seguito CTR) ha sollevato, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">24</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_23">23</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_111">111</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_11">11 della Costituzione</ref></mref>, quest'ultimo in relazione all'art. 6 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con <ref href="/akn/it/act/legge/stato/1955-08-04/848/!main">legge 4 agosto 1955, n. 848</ref>, questioni di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/1982-11-20/890/!main#art_14">art. 14 della legge 20 novembre 1982, n. 890</ref> (Notificazioni di atti a mezzo posta e di comunicazioni a mezzo posta connesse con la notificazione di atti giudiziari), e dell'<ref href="/akn/it/act/legge/stato/2006-12-27/296/!main#art_1__para_161">art. 1, comma 161, della legge 27 dicembre 2006, n. 296</ref>, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2007)», «nella parte in cui, ammettendo la notificazione diretta degli atti impositivi e dei ruoli - da parte degli Uffici Finanziari Erariali e Locali nonché degli Enti di riscossione - a mezzo servizio postale di raccomandata con ricevuta di ritorno, escludono a tale forma di notifica la applicazione delle modalità di cui alla <ref href="/akn/it/act/legge/stato/1982/890/!main">L. 890/1982</ref>»;
 che la CTR deve decidere sull'appello proposto da Equitalia Servizi di Riscossione spa (quale incorporante di Equitalia Sud spa) nei confronti della sentenza della Commissione tributaria provinciale di Napoli che ha accolto il ricorso proposto dal contribuente Andrea Mignone avverso un preavviso di fermo, emesso da Equitalia Sud spa e notificato a mezzo posta raccomandata, sul rilievo che la notifica delle sottostanti cartelle di pagamento, riferite a tributi erariali e imposte comunali, risultava viziata in quanto non supportata dalla prova dell'avviso di ricevimento postale della comunicazione di avvenuta notificazione (cosiddetta CAN);
 che la CTR rileva l'errore del giudice di primo grado il quale ha ritenuto la nullità della notifica benché dagli atti di causa emergesse che tutte le cartelle erano state notificate «a mezzo posta raccomandata A.R. diretta, cioè senza l'intermediazione di ufficiale notificatore», sia quelle relative a tributi erariali (notifica effettuata ai sensi dell'<ref href="/akn/it/act/legge/stato/1982/890/!main#art_14">art. 14 della legge n. 890 del 1982</ref>), sia quelle concernenti tributi locali (notifica effettuata ai sensi dell'<ref href="/akn/it/act/legge/stato/2006/296/!main#art_1__para_161">art. 1, comma 161, della legge n. 296 del 2006</ref>);
 che, ad avviso del giudice a quo, la giurisprudenza di legittimità, con orientamento consolidato, afferma che, in tema di notificazioni a mezzo posta, la disciplina relativa alla raccomandata con avviso di ricevimento, mediante la quale può essere notificato, ai sensi dell'<ref href="/akn/it/act/legge/stato/1982/890/!main#art_14">art. 14 della legge n. 890 del 1982</ref>, l'avviso di accertamento o liquidazione senza intermediazione dell'ufficiale giudiziario, è quella dettata dalle disposizioni sul servizio postale ordinario per la consegna dei plichi raccomandati, in quanto le disposizioni di cui alla legge citata attengono esclusivamente alla notifica eseguita dall'ufficiale giudiziario ex <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_140">art. 140 del codice di procedura civile</ref>;
 che conseguentemente non deve essere redatta alcuna relata di notifica o annotazione specifica sull'avviso di ricevimento in ordine alla persona cui è stato consegnato il plico e l'atto pervenuto all'indirizzo del destinatario deve ritenersi ritualmente consegnato a quest'ultimo, stante la presunzione di conoscenza di cui all'<ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_1335">art. 1335 del codice civile</ref>, superabile solo se il medesimo dia prova di essersi trovato nell'impossibilità senza sua colpa di prenderne cognizione;
 che l'applicazione di tali principi comporterebbe l'accoglimento dell'appello;
 che tuttavia la CTR dubita della legittimità costituzionale delle disposizioni censurate, là dove «prevedono una forma di notificazione degli atti impositivi senza le garanzie nella fase di consegna del plico previste dalla <ref href="/akn/it/act/legge/stato/1982/890/!main">L. 890/1982</ref> per le notificazioni a mezzo posta effettuate dall'Ufficiale giudiziario, dal messo comunale o speciale» e, in particolare, senza la CAN prescritta (alla data dell'ordinanza di rimessione) dall'<ref href="/akn/it/act/legge/stato/1982/890/!main#art_7">art. 7 della legge n. 890 del 1982</ref>;
 che la CTR si diffonde sulla ratio della disciplina delle notificazioni, evidenziando la differenza tra la comunicazione degli atti unilaterali, regolata dagli <mref><ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_1334">artt. 1334</ref> e <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_1335">1335 cod. civ.</ref></mref> e finalizzata a garantire la mera conoscibilità, e la procedura della notificazione, volta, invece, ad assicurare una conoscenza effettiva dell'atto;
 che le norme censurate sarebbero irragionevoli e, dunque, in conflitto con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, in quanto darebbero luogo a «una sostanziale elusione dell'obbligo di notifica» là dove prevedono una «mera comunicazione, elevando a forma di notificazione sul piano solo nominalistico, presunzioni semplici di conoscibilità che non corrispondono alla prima che è presunzione legale iuris et de iure di conoscenza»;
 che, inoltre, sarebbe violato l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">art. 24 Cost.</ref>, in combinato disposto con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, in quanto l'attenuazione delle garanzie di conoscenza dell'atto in danno del contribuente si risolverebbe in un'irragionevole lesione del diritto di difesa;
 che la CTR, poi, assume che le disposizioni impugnate violerebbero anche «l'art. 6 CEDU applicabile direttamente ex <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_11">art. 11 Cost.</ref>, non garantendo al soggetto passivo una conoscenza dell'atto sfavorevole con negazione della possibilità di adeguata e tempestiva difesa considerando le decadenze e preclusioni peraltro fissate in termini assai brevi (di regola 60 giorni), il tutto altresì rimettendo a sostanziale discrezione dell'Ufficio Impositore, cioè dell'Autorità, la scelta se adottare o meno un procedimento più garantista, o meglio una vera notificazione che conduce alla legale certa conoscenza ovvero una comunicazione che al massimo conduce ad una mera astratta conoscibilità»;
 che le disposizioni censurate darebbero luogo, altresì, a una violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_111">art. 111 Cost.</ref> «perché rendendo non certa la conoscenza legale al destinatario dell'atto sostanziale impugnabile determinano una lesione del contraddittorio, quale esplicazione della possibilità effettiva di agire e contrastare nel processo» la pretesa avanzata dall'amministrazione, nonché dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref>, permettendo alla pubblica amministrazione «di non organizzare i propri uffici e le proprie attività in modo da consentire la certa legale conoscenza degli atti sfavorevoli al cittadino stesso»;
 che con atto depositato il 24 luglio 2018 è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate inammissibili e, comunque, infondate;
 che, in particolare, l'Avvocatura generale pone in rilievo che la Corte costituzionale, con la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/175/!main">sentenza n. 175 del 2018</ref>, ha già dichiarato non fondata la questione di legittimità costituzionale dell'art. 26, primo comma, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1973/602/!main">d.P.R. n. 602 del 1973</ref>, nella parte in cui facoltizza l'agente della riscossione alla notifica diretta e semplificata delle cartelle esattoriali, senza intermediario, mediante invio di raccomandata con avviso di ricevimento.
 Considerato che con motivazione sintetica, ma non implausibile, il collegio rimettente ha dato conto delle ragioni che inducono a fare applicazione delle disposizioni censurate;
 che questa Corte (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/175/!main">sentenza n. 175 del 2018</ref>) ha già esaminato analoga questione di costituzionalità riguardante parimenti la modalità di notificazione «diretta» delle cartelle di pagamento con riferimento a quella effettuata dagli ufficiali della riscossione ai sensi dell'art. 26, primo comma, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1973-09-29/602/!main">decreto del Presidente della Repubblica 29 settembre 1973, n. 602</ref> (Disposizioni sulla riscossione delle imposte sul reddito);
 che con tale pronuncia questa Corte - richiamando i consolidati principi secondo cui «il regime differenziato della riscossione coattiva delle imposte risponde all'esigenza, di rilievo costituzionale, di assicurare con regolarità le risorse necessarie alla finanza pubblica» e «la disciplina speciale della riscossione coattiva delle imposte non pagate risponde all'esigenza della pronta realizzazione del credito fiscale a garanzia del regolare svolgimento della vita finanziaria dello Stato» (rispettivamente, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/90/!main">sentenze n. 90 del 2018</ref> e n. 281 del 2011) - ha dichiarato non fondate, nei sensi di cui in motivazione, le questioni di legittimità costituzionale dell'art. 26, primo comma, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1973-09-29/602/!main">decreto del Presidente della Repubblica 29 settembre 1973, n. 602</ref>, sollevate, in riferimento agli artt. 3, primo comma, 24, primo e secondo comma, e 111, primo e secondo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;
 che, come rilevato nella medesima pronuncia, nella fattispecie della notificazione "diretta", vi è un sufficiente livello di conoscibilità - ossia di possibilità che si raggiunga, per il notificatario, l'effettiva conoscenza dell'atto - «stante l'avvenuta consegna del plico (oltre che allo stesso destinatario, anche alternativamente) a chi sia legittimato a riceverlo, sicché il "limite inderogabile" della discrezionalità del legislatore non è superato e non è compromesso il diritto di difesa del destinatario della notifica»;
 che analoghe considerazioni possono svolgersi con riferimento sia alla notifica diretta ad opera degli uffici finanziari, prevista dall'<ref href="/akn/it/act/legge/stato/1982/890/!main#art_14">art. 14 della legge n. 890 del 1982</ref>, sia a quella contemplata dall'<ref href="/akn/it/act/legge/stato/2006/296/!main#art_1__para_161">art. 1, comma 161, della legge n. 296 del 2006</ref> per i tributi locali;
 che l'indicazione degli ulteriori parametri da parte della CTR rimettente non offre elementi per una diversa valutazione delle questioni, che sono pertanto, sotto ogni profilo, manifestamente infondate.
 che peraltro - come già evidenziato nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/175/!main">sentenza n. 175 del 2018</ref> - la mancanza, in concreto, di «effettiva conoscenza» dell'atto, per causa non imputabile, può legittimare il destinatario a richiedere la rimessione in termini ai sensi dell'art. 153, secondo comma, del <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main">codice di procedura civile</ref>;
 che l'<ref href="/akn/it/act/legge/stato/2000-07-27/212/!main#art_6">art. 6 della legge 27 luglio 2000, n. 212</ref> (Disposizioni in materia di statuto dei diritti del contribuente) legittima un'applicazione estensiva dell'istituto della rimessione in termini, sì da tutelare il contribuente che non abbia avuto «effettiva conoscenza» dell'atto restituendolo nel termine di decadenza, di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/1992-12-31/546/!main#art_19">art. 19 del decreto legislativo 31 dicembre 1992, n. 546</ref> (Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'<ref href="/akn/it/act/legge/stato/1991-12-30/413/!main#art_30">art. 30 della legge 30 dicembre 1991, n. 413</ref>), per impugnare l'atto;
 che è rimesso al prudente apprezzamento del giudice della controversia valutare ogni comprovato elemento presuntivo (<ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_2729">art. 2729 del codice civile</ref>), offerto dal destinatario della notifica "diretta" della cartella di pagamento - il quale, pur essendo integrata un'ipotesi di conoscenza legale in ragione del rispetto delle formalità (tanto più che semplificate) di cui alle disposizioni censurate, assuma di non aver avuto conoscenza effettiva dell'atto per causa a lui non imputabile - al fine di accogliere, o no, la richiesta di rimessione in termini.
 Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/1982-11-20/890/!main#art_14">art. 14 della legge 20 novembre 1982, n. 890</ref> (Notificazioni di atti a mezzo posta e di comunicazioni a mezzo posta connesse con la notificazione di atti giudiziari) e dell'<ref href="/akn/it/act/legge/stato/2006-12-27/296/!main#art_1__para_161">art. 1, comma 161, della legge 27 dicembre 2006, n. 296</ref>, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2007)» sollevate, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">24</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_23">23</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_111">111</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_11">11 della Costituzione</ref></mref>, quest'ultimo in relazione all'art. 6 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con <ref href="/akn/it/act/legge/stato/1955-08-04/848/!main">legge 4 agosto 1955, n. 848</ref>, dalla Commissione tributaria regionale della Campania con l'ordinanza indicata in epigrafe.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2019-03-20">20 marzo 2019</date>.
 F.to:
 <signature><judge refersTo="#giorgioLattanzi" as="#presidente">Giorgio LATTANZI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giovanniAmoroso" as="#relatore">Giovanni AMOROSO</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#filomenaPerrone" as="#cancelliere">Filomena PERRONE</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2019-04-24">24 aprile 2019</date>.
 Il Cancelliere
 F.to: Filomena PERRONE</block>
    </conclusions>
  </judgment>
</akomaNtoso>
