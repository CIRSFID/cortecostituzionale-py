<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>43</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:43</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1997</data_decisione>
    <data_deposito>20/02/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio di legittimità costituzionale dell'articolo 8, secondo    &#13;
 e terzo comma, della legge 15 dicembre 1972, n.  772  (Norme  per  il    &#13;
 riconoscimento  dell'obiezione  di coscienza), promosso con ordinanza    &#13;
 emessa il 27 maggio 1996 dalla Corte  costituzionale  nel  corso  del    &#13;
 giudizio di legittimità costituzionale, sollevato dal giudice per le    &#13;
 indagini  preliminari  presso  il  tribunale  militare  di  Roma  nel    &#13;
 procedimento penale a carico di Rombi Gennaro, iscritta al n. 614 del    &#13;
 registro ordinanze 1996 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 27, prima serie speciale, dell'anno 1996;                  &#13;
   Visto l'atto di costituzione di Rombi Gennaro;                         &#13;
   Udito  nell'udienza  pubblica  del  10  dicembre  1996  il  giudice    &#13;
 relatore Gustavo Zagrebelsky;                                            &#13;
   Uditi  gli  avvocati  Mauro  Mellini  e Roberto Lorenzini per Rombi    &#13;
 Gennaro.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. -  Nel corso dell'udienza preliminare relativa a un procedimento    &#13;
 penale per il reato di rifiuto del servizio militare di cui  all'art.    &#13;
 8, secondo comma, della legge 15 dicembre 1972, n. 772 a carico di un    &#13;
 cittadino,  già in precedenza condannato con sentenza definitiva per    &#13;
 il medesimo titolo di reato  con  applicazione  del  beneficio  della    &#13;
 sospensione  condizionale  dell'esecuzione  della  pena, e nuovamente    &#13;
 indagato, nel giudizio a  quo,  in  relazione    all'inosservanza  di    &#13;
 ulteriore  atto  di  chiamata  alle  armi emesso dopo il passaggio in    &#13;
 giudicato  della  prima  sentenza,  il  giudice   per   le   indagini    &#13;
 preliminari  presso  il  tribunale militare di Roma ha sollevato, con    &#13;
 ordinanza del 19 giugno  1995  (r.o.  529  del  1995),  questione  di    &#13;
 legittimità  costituzionale del combinato disposto degli articoli 8,    &#13;
 secondo e terzo comma, della legge n.  772 del 1972, e 163 e seguenti    &#13;
 del codice penale,  in  riferimento  all'art.  3  della  Costituzione    &#13;
 nonché al principio della finalità rieducativa della pena (art. 27,    &#13;
 terzo comma, della Costituzione).                                        &#13;
   2.  -  Respinte  come manifestamente infondate diverse e più ampie    &#13;
 questioni   di    costituzionalità    prospettate    dalla    difesa    &#13;
 dell'indagato,  il  giudice  a  quo osserva che il sistema normativo,    &#13;
 qualora vi sia stata la concessione  della  sospensione  condizionale    &#13;
 della  pena  nel  giudizio  per  il primo reato, delinea una sequenza    &#13;
 (nuova chiamata alle armi, nuovo reato in caso  di  reiterazione  del    &#13;
 rifiuto,  revoca  del  beneficio sospensivo, espiazione della pena e,    &#13;
 infine, esonero ex art. 8, terzo  comma)  che  il  rimettente  reputa    &#13;
 complessivamente coerente e sostanzialmente conforme a Costituzione.     &#13;
   Ma  un  elemento  di  possibile  incostituzionalità è ravvisabile    &#13;
 nella  sola  ipotesi  in  cui,  nel  primo  giudizio,  il   beneficio    &#13;
 sospensivo sia stato concesso d'ufficio.                                 &#13;
   In   questo   caso,   infatti,  data  la  mancata  richiesta  della    &#13;
 sospensione condizionale da parte dell'interessato,  la  reiterazione    &#13;
 del  reato  di  rifiuto  sarebbe  ascrivibile, più che alla volontà    &#13;
 dell'obiettore  rimasto  fermo   nei   propri   convincimenti,   alla    &#13;
 determinazione del giudice.                                              &#13;
   Al  riguardo,  il  rimettente  muove  dal  particolare ed esclusivo    &#13;
 rilievo che, nella disciplina in argomento, assume la  manifestazione    &#13;
 di  volontà  del  soggetto,  della cui mera dichiarazione di rifiuto    &#13;
 l'ordinamento prende atto e  alla  cui  eventuale  domanda  di  segno    &#13;
 contrario  (art.    8,  commi  quarto  e  quinto)  viene  addirittura    &#13;
 attribuita rilevanza quale presupposto di una causa di estinzione del    &#13;
 reato. In simile quadro, qualora la  sospensione  condizionale  venga    &#13;
 concessa  su  richiesta dell'interessato, è ragionevole - afferma il    &#13;
 giudice a  quo  -  che  l'ordinamento  sanzioni  l'ulteriore  rifiuto    &#13;
 secondo il descritto regime.                                             &#13;
   Verso diversa conclusione orienta però il caso in cui il beneficio    &#13;
 sospensivo  sia  applicato  d'ufficio.  In  simile ipotesi, appare al    &#13;
 rimettente di dubbia ragionevolezza un meccanismo che,  per  condurre    &#13;
 all'esonero,  comporta  un  prolungamento della pena da espiare senza    &#13;
 che questo trovi fondamento in qualche "comportamento intermedio         &#13;
  ... tra i momenti di rifiuto su cui si fondano  i  reati  ascritti".    &#13;
 Neppure è idonea a dissolvere il dubbio la possibilità di impugnare    &#13;
 la  sentenza  di  condanna  per  vedersi  tolto  il "beneficio" della    &#13;
 sospensione, che si rivela in realtà svantaggioso  sul  piano  della    &#13;
 reiterazione  dei  reati;  una  possibilità, questa, che implica una    &#13;
 pretesa "eccessiva", ad avviso del giudice a quo.                        &#13;
   La reiterazione del rifiuto, dunque, deriverebbe  da  un  beneficio    &#13;
 non  richiesto  e il conseguente incremento del periodo di espiazione    &#13;
 della pena necessario ai fini dell'esonero  risulterebbe,  in  questo    &#13;
 senso, non giustificato in riferimento al parametro di ragionevolezza    &#13;
 e  al principio della finalità rieducativa della pena. Pur ribadendo    &#13;
 la  mancanza  di   collegamento   funzionale   tra   la   sospensione    &#13;
 condizionale e l'esonero dal servizio militare, il rimettente ritiene    &#13;
 che  la  specifica  situazione descritta richieda un'integrazione dei    &#13;
 principi ricavabili dalla giurisprudenza costituzionale: di  qui,  la    &#13;
 proposizione della questione di legittimità costituzionale dell'art.    &#13;
 8,  secondo  e terzo comma, della legge n. 772 del 1972, in combinato    &#13;
 disposto con gli artt. 163 e seguenti del codice penale, "nella parte    &#13;
 in cui prevede che, a  fronte  della  concessione  di  ufficio  della    &#13;
 sospensione  condizionale  della  pena,  l'esonero  consegua soltanto    &#13;
 all'espiazione della pena inflitta per il secondo reato". Quanto alla    &#13;
 rilevanza di detta questione,  il  rimettente  osserva  che  dal  suo    &#13;
 eventuale  accoglimento  deriverebbero  effetti  sul  secondo atto di    &#13;
 chiamata alle armi adottato nei confronti dell'imputato, atto che  è    &#13;
 il  presupposto  dell'ulteriore  reato  di rifiuto per il quale è in    &#13;
 corso il procedimento penale.                                            &#13;
   3. - Nel giudizio così promosso si è  costituito  Gennaro  Rombi,    &#13;
 indagato nel giudizio a quo. Nell'atto di costituzione, il patrocinio    &#13;
 della  parte  privata  ha sviluppato articolate deduzioni, di portata    &#13;
 esplicitamente più ampia rispetto  al  quesito  circoscritto  -  dal    &#13;
 giudice  rimettente - alla sola ipotesi della precedente condanna con    &#13;
 pena  condizionalmente  sospesa  d'ufficio.  In  particolare,   viene    &#13;
 censurata  l'interpretazione  del  rimettente secondo cui il reato di    &#13;
 rifiuto del servizio previsto dall'art. 8, secondo comma, della legge    &#13;
 n. 772 del 1972 sarebbe un reato ripetibile; si  tratterebbe  invece,    &#13;
 nonostante la formulazione normativa e in mancanza di una convincente    &#13;
 presa  di posizione della giurisprudenza di legittimità, di un reato    &#13;
 istantaneo,  che  si  proietta  su  un  unico  oggetto,  il  servizio    &#13;
 militare,  e  che assumerebbe effetti permanenti, data l'unicità del    &#13;
 servizio di leva. La prima condanna, quindi, non potrebbe far cessare    &#13;
 la consumazione di un reato già in sé esaurito né  far  "rivivere"    &#13;
 l'obbligo   di   prestazione  del  servizio;  d'altra  parte  sarebbe    &#13;
 un'improprietà   l'espressione,    utilizzata    dal    legislatore,    &#13;
 dell'"esonero"  dal  servizio  a pena espiata.  Qualora si aderisse a    &#13;
 questa  configurazione,  la  questione  sollevata   dovrebbe   essere    &#13;
 dichiarata  inammissibile  per  difetto di rilevanza:  se il reato è    &#13;
 irripetibile, infatti, l'intera censura perde consistenza.               &#13;
   Valorizzando la ratio della legge n. 772 del  1972,  rispetto  alla    &#13;
 situazione  normativa  preesistente,  in  cui si configurava la serie    &#13;
 delle condanne a catena, il reato di rifiuto totale del  servizio,  e    &#13;
 la    collegata    previsione   dell'esonero   da   esso,   implicano    &#13;
 necessariamente, secondo la parte privata, la  scelta  interpretativa    &#13;
 nel  senso  dell'unicità del fatto e quindi della impossibilità - o    &#13;
 illegittimità - di nuove ulteriori chiamate dopo il primo rifiuto.      &#13;
   Sarebbe poi inesatta la tesi  secondo  la  quale  solo  l'effettiva    &#13;
 espiazione   della   pena  esonera  dalla  prestazione  del  servizio    &#13;
 militare; nella nozione di "espiazione",  ai  fini  che  interessano,    &#13;
 dovrebbe  farsi  rientrare  ogni  modalità  con  cui  si  perseguono    &#13;
 finalità  rieducative,  e  tra  esse  quindi  anche  la  sospensione    &#13;
 condizionale;  si  dovrebbe  quindi  affermare   che   l'esonero   è    &#13;
 collegabile,    oltre    che   all'espiazione   in   senso   stretto,    &#13;
 all'estinzione del reato o della pena conseguiti per  altra  via.  Ne    &#13;
 sarebbe  riprova  l'esonero  conseguente  - per incontestata prassi -    &#13;
 all'applicazione dello speciale affidamento in prova  regolato  dalla    &#13;
 legge  29  aprile 1983, n. 167, ai condannati per reati di obiezione.    &#13;
 In via di  principio,  dunque,  unico  presupposto  per  darsi  luogo    &#13;
 all'esonero è quello della "constatazione a posteriori che, malgrado    &#13;
 la  reazione  dell'ordinamento,  abbia  operato  un  ... tentativo di    &#13;
 rieducazione", cosicché, persistendo l'obiezione  di  coscienza,  la    &#13;
 sanzione penale non può più raggiungere effetti rieducativi.           &#13;
   Sarebbe,  infine,  impropria  la notazione dell'ordinanza di rinvio    &#13;
 secondo la quale le  finalità  rieducative  risulterebbero  estranee    &#13;
 all'istituto   dell'esonero,   perché   quest'ultimo,   per   essere    &#13;
 ragionevole  e  legittimo,  deve  presupporre  la  constatazione  del    &#13;
 fallimento di un tentativo di rieducazione, che segna il limite entro    &#13;
 cui contenere la tutela dell'interesse pubblico.                         &#13;
   Tutte le esposte argomentazioni, osserva il patrocinio della parte,    &#13;
 non  sono  state valorizzate dal giudice a quo, neppure sul piano dei    &#13;
 presupposti amministrativi  del  fatto-reato,  avendo  il  rimettente    &#13;
 dedotto  il solo specifico elemento della concessione d'ufficio della    &#13;
 sospensione  condizionale  della  pena,  tale   da   determinare   un    &#13;
 irragionevole  svantaggio derivante non da scelta dell'interessato ma    &#13;
 dalla determinazione del giudice. Un profilo, si osserva ancora,  che    &#13;
 comunque  non  potrebbe  essere  così  circoscritto,  perché  anche    &#13;
 nell'ipotesi di richiesta  di  parte  la  concessione  del  beneficio    &#13;
 risiede  pur  sempre  nella determinazione discrezionale del giudice;    &#13;
 "sempre e comunque",  quindi,  sussiste  l'incostituzionalità  delle    &#13;
 norme  impugnate,  nella  parte  in  cui  prevedono che l'esonero dal    &#13;
 servizio militare consegua solo all'espiazione  della  pena  inflitta    &#13;
 per  il  secondo  reato, a fronte della concessione della sospensione    &#13;
 condizionale della pena nel primo giudizio.                              &#13;
   4. - È intervenuto in giudizio il  Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato.   L'interveniente, individuata  la  sostanza  della  questione    &#13;
 nella asserita presumibile volontà dell'obiettore di espiare la pena    &#13;
 onde  ottenere  l'esonero  dal servizio militare, rileva che, anche a    &#13;
 tralasciare  i  dubbi  sulla  esattezza  di  una  tale  ricostruzione    &#13;
 induttiva  della  volontà dell'imputato, questi dispone comunque del    &#13;
 mezzo processuale dell'impugnazione della sentenza per rimuovere, ove    &#13;
 lo volesse, gli effetti  "deteriori"  -  per  questo  profilo  -  del    &#13;
 beneficio,  essendo  correntemente  riconosciuto  che  la sospensione    &#13;
 condizionale non può tradursi  in  un  pregiudizio  per  l'imputato.    &#13;
 L'interveniente   ha   quindi   concluso   per  una  declaratoria  di    &#13;
 inammissibilità o di infondatezza della questione.                      &#13;
   5. - La parte privata ha successivamente depositato una memoria  di    &#13;
 replica  all'atto di intervento dell'Avvocatura erariale, nella quale    &#13;
 si afferma l'assurdità del rimedio  proposto  (l'impugnazione  della    &#13;
 prima  sentenza)  al fine di ovviare all'inconveniente dedotto con la    &#13;
 questione.  L'improprietà  del  rimedio  suggerito   riconfermerebbe    &#13;
 l'irragionevolezza insita nella normativa impugnata.                     &#13;
   6.   -  Nel  corso  del  giudizio  in  via  incidentale  instaurato    &#13;
 dall'ordinanza di rimessione del giudice per le indagini  preliminari    &#13;
 presso  il  tribunale militare di Roma, la Corte costituzionale - con    &#13;
 ordinanza n. 183 del  27  maggio  1996  (r.o.  614  del  1996)  -  ha    &#13;
 sollevato  dinanzi  a se stessa, in riferimento agli artt. 2, 3, 19 e    &#13;
 21  della  Costituzione,  questione  di  legittimità  costituzionale    &#13;
 dell'art.  8,  secondo  e  terzo  comma, della legge n. 772 del 1972,    &#13;
 nella  parte  in  cui  consente  la   ripetuta   sottoponibilità   a    &#13;
 procedimento penale del medesimo soggetto già condannato per i fatti    &#13;
 ivi previsti.                                                            &#13;
   Nell'ordinanza,  si  osserva che, rispetto alla specifica questione    &#13;
 dedotta dal giudice di merito,  incentrata  sul  trattamento  di  chi    &#13;
 ripeta  il  reato  dopo  una  prima  condanna a pena condizionalmente    &#13;
 sospesa, assume valore pregiudiziale la valutazione di conformità  a    &#13;
 Costituzione  della  più  generale  possibilità  di moltiplicazione    &#13;
 delle condanne e della conseguente sommatoria di pene  nei  confronti    &#13;
 di  chi rifiuti e persista nel rifiuto della prestazione militare, in    &#13;
 tutti i casi in cui, per un  motivo  previsto  dall'ordinamento  (nel    &#13;
 caso  dedotto:    per  la concessione del beneficio della sospensione    &#13;
 condizionale con la prima sentenza), alla condanna irrogata per prima    &#13;
 non faccia seguito l'espiazione della pena con essa inflitta.            &#13;
   L'anzidetta possibilità rivela profili di contrasto:  a)  sia  con    &#13;
 l'art.  3  della  Costituzione,  quanto al principio di razionalità,    &#13;
 perché essa contraddice la ratio del terzo comma dell'art.  8  della    &#13;
 legge  n. 772 del 1972, rivolto ad evitare il fenomeno della "spirale    &#13;
 delle  condanne"  che  ha   ripetutamente   originato   pronunce   di    &#13;
 incostituzionalità  della  disciplina  in  parola,  a iniziare dalla    &#13;
 sentenza n. 409 del 1989; b) sia con gli artt. 2, 3, 19  e  21  della    &#13;
 Costituzione, che apprestano una protezione costituzionale ai diritti    &#13;
 della  coscienza  (sentenza n. 467 del 1991), perché la comminatoria    &#13;
 di plurime condanne e pene può  comportare  una  coercizione  morale    &#13;
 permanente,   in   vista  di  un  mutamento  coatto  della  coscienza    &#13;
 individuale.                                                             &#13;
   7. - Anche nel giudizio così instaurato si è costituita la  parte    &#13;
 privata  Gennaro Rombi, il cui patrocinio, nell'atto di costituzione,    &#13;
 ha ripetuto le  osservazioni  già  svolte  nel  precedente  atto  di    &#13;
 costituzione  relativo  al giudizio di costituzionalità promosso dal    &#13;
 giudice penale militare.                                                 &#13;
   8. - La questione è stata esaminata nell'udienza pubblica  del  10    &#13;
 dicembre   1996;   la   parte  privata  ha  insistito  nelle  proprie    &#13;
 conclusioni.<diritto>Considerato in diritto</diritto>1. -  Chiamata a pronunciarsi, con riferimento agli artt.  3  e  27    &#13;
 della  Costituzione,  sulla  legittimità costituzionale dell'art. 8,    &#13;
 secondo e terzo comma, della legge 15 dicembre 1972,  n.  772  (Norme    &#13;
 per  il  riconoscimento  dell'obiezione  di  coscienza), in combinato    &#13;
 disposto con gli artt. 163 e seguenti del codice penale, nella  parte    &#13;
 in   cui   prevede  che,  in  caso  di  precedente  condanna  a  pena    &#13;
 condizionalmente sospesa, in assenza di richiesta dell'imputato,  per    &#13;
 il  reato  di rifiuto totale del servizio militare dovuto a motivi di    &#13;
 coscienza,  l'esonero  dal  servizio   militare   consegua   soltanto    &#13;
 all'espiazione della pena inflitta per un ulteriore medesimo reato di    &#13;
 rifiuto, questa Corte ha ritenuto di sollevare d'ufficio innanzi a se    &#13;
 medesima  la più ampia questione, logicamente pregiudiziale a quella    &#13;
 specifica  ora  indicata,  della  legittimità  costituzionale  dello    &#13;
 stesso  art.  8, secondo e terzo comma, della legge 15 dicembre 1972,    &#13;
 n. 772,  nella  parte  in  cui  non  esclude  la  possibilità  della    &#13;
 ripetizione  di  condanne a carico del medesimo soggetto che persiste    &#13;
 nel rifiuto totale del servizio militare.                                &#13;
   Tale mancata esclusione, che  può  verificarsi  nelle  particolari    &#13;
 ipotesi  di  cui  si  dirà,  è  apparsa  a  questa  Corte di dubbia    &#13;
 legittimità costituzionale per due ordini di ragioni:  innanzitutto,    &#13;
 per violazione dell'art. 3 della Costituzione, sotto il profilo della    &#13;
 razionalità  della  disciplina,  in quanto contrastante con la ratio    &#13;
 dell'art. 8, terzo comma, della legge n. 772 del  1972,  all'evidenza    &#13;
 rivolto  a  evitare  quella  "spirale  delle condanne" che in diverse    &#13;
 circostanze, a iniziare dalla sentenza di questa  Corte  n.  409  del    &#13;
 1989,  è  stata  ritenuta illegittima; inoltre, per violazione degli    &#13;
 artt. 2,  3,  19  e  21  della  Costituzione  i  quali,  secondo  una    &#13;
 giurisprudenza  costituzionale  che  si  è affermata a partire dalla    &#13;
 sentenza n. 467 del 1991, apprestano una protezione costituzionale ai    &#13;
 cosiddetti "diritti della coscienza", in quanto  la  comminazione  di    &#13;
 plurime  condanne  e pene, nel caso di persistenza nell'atteggiamento    &#13;
 di rifiuto, può condurre a una permanente pressione morale in  vista    &#13;
 di un mutamento coatto dei contenuti della coscienza individuale.        &#13;
   2. - La questione è fondata.                                          &#13;
   3.  -  Occorre innanzitutto chiarire che il legislatore, con l'art.    &#13;
 8, secondo comma, della legge n. 772 del 1972, avendo  previsto  come    &#13;
 reato  il fatto del cosiddetto obiettore totale - cioè di colui che,    &#13;
 fuori dei  casi  di  ammissione  ai  benefici  previsti  dalla  legge    &#13;
 medesima,   adducendo  i  motivi  di  coscienza  da  questa  indicati    &#13;
 nell'art. 1, rifiuta, in  tempo  di  pace,  prima  di  assumerlo,  il    &#13;
 servizio  militare  di  leva  -  ha  poi preso in considerazione, per    &#13;
 escluderla,  l'eventualità di incriminazioni,  processi  e  condanne    &#13;
 plurimi  di  quanti tengano fermo il proprio atteggiamento di rifiuto    &#13;
 totale,  di  fronte  al  servizio  militare   e   alle   obbligazioni    &#13;
 alternative  a  esso.  A  tal  fine,  con il terzo comma del medesimo    &#13;
 articolo ha previsto che l'espiazione  della  pena  inflitta  con  la    &#13;
 prima  sentenza  di  condanna  esonera dalla prestazione del servizio    &#13;
 militare di leva.                                                        &#13;
   In tal modo,  attraverso  il  sistema  dell'esonero  amministrativo    &#13;
 conseguente  all'espiazione  della  pena, si raggiunge normalmente il    &#13;
 risultato  voluto,  poiché  l'Amministrazione  militare   non   può    &#13;
 disporre  una  nuova  chiamata  al servizio e, in assenza di questa -    &#13;
 indipendentemente da ogni questione circa la configurazione del reato    &#13;
 previsto dal  secondo  comma  come  reato  suscettibile  di  ripetute    &#13;
 commissioni -, viene a mancare il presupposto della ripetizione.         &#13;
   Senonché  l'anzidetta  sequenza,  che  dalla  condanna, attraverso    &#13;
 l'espiazione della pena, conduce all'esonero, può  interrompersi  le    &#13;
 volte  in  cui, per un motivo previsto dall'ordinamento, l'espiazione    &#13;
 totale o parziale della  pena  non  ha  luogo.  Un'evenienza,  quella    &#13;
 descritta,  che  in  concreto si realizza per lo più a seguito della    &#13;
 concessione della sospensione condizionale della esecuzione di  pena,    &#13;
 dato il particolare modo di operare di tale beneficio (estin-tivo del    &#13;
 reato  al  termine  del  periodo  prescritto, ma, anteriormente, solo    &#13;
 impeditivo dell'espiazione), ma che può ugualmente derivare,  da  un    &#13;
 punto di vista concettuale e indipendentemente da specifiche prese di    &#13;
 posizione della giurisprudenza, da particolari prassi amministrative,    &#13;
 o  da puntuali statuizioni legislative (ad esempio l'art. 1, comma 2,    &#13;
 del  d.P.R. 12 aprile 1990, n. 75, o l'art. 6, comma 5, del d.P.R. 16    &#13;
 dicembre 1986, n. 865) come conseguenza dell'applicazione di  diversi    &#13;
 istituti,   di   portata  generale  (come,  per  ipotesi,  l'amnistia    &#13;
 impropria o l'indulto) o individuale (come la grazia e la liberazione    &#13;
 condizionale); istituti diversi tra loro per  funzione  e  meccanismo    &#13;
 operativo,  ma  unificabili, ai fini che qui interessano, per il loro    &#13;
 effetto di mancata espiazione dell'intera pena. In codesti casi,  non    &#13;
 realizzandosi  la condizione dell'esonero prevista dall'art. 8, terzo    &#13;
 comma, si è ritenuto (come in effetti l'Amministrazione militare  ha    &#13;
 ritenuto,  in  relazione  a un caso di sospensione condizionale della    &#13;
 pena, da cui ha preso origine il processo penale che ha dato luogo al    &#13;
 giudizio incidentale sul quale si è infine  venuta  a  innestare  la    &#13;
 questione  ora  in  esame)  che  sia necessario procedere a una nuova    &#13;
 chiamata, secondo i ritmi ordinari ai quali obbedisce il reclutamento    &#13;
 (si veda la circolare del Ministero della difesa LEV/A.49/UDG del  16    &#13;
 luglio  1992,  punto  3). Di fronte alla nuova chiamata, il rinnovato    &#13;
 atteggiamento  di  rifiuto  innescherà  un  nuovo  processo  che  si    &#13;
 concluderà  presumibilmente  con  una nuova condanna e con la revoca    &#13;
 della sospensione condizionale precedentemente  concessa.  Cosicché,    &#13;
 sia pure eccezionalmente, eventi quali quelli ora indicati, impedendo    &#13;
 l'esecuzione  della  pena  e,  con ciò, l'operatività dell'esonero,    &#13;
 porranno   l'obiettore   nella   condizione    di    subire    quella    &#13;
 moltiplicazione  di conseguenze penali che l'art.  8, terzo comma, ha    &#13;
 in generale inteso escludere.                                            &#13;
   4. - La disciplina che si è descritta, in riferimento alle ipotesi    &#13;
 particolari anzidette, appare insuperabilmente contraddittoria.          &#13;
   4.1. - In primo luogo, essa determina un pervertimento della natura    &#13;
 di quelli che,  nei  confronti  della  generalità  dei  destinatari,    &#13;
 valgono  normalmente  come  benefici.  Istituti  come  la sospensione    &#13;
 condizionale della  pena,  l'amnistia,  l'indulto,  la  grazia  e  la    &#13;
 liberazione   condizionale   rappresentano,   per   l'obiettore   che    &#13;
 coerentemente persiste nel proprio  atteggiamento  di  coscienza,  la    &#13;
 premessa  per  la  moltiplicazione  delle condanne e la sommatoria di    &#13;
 pene, laddove, in mancanza di essi, si avrebbe una  sola  condanna  e    &#13;
 una sola pena, espiata la quale si darebbe luogo all'esonero.            &#13;
   Né  varrebbe  osservare  che  tale mutazione del beneficio nel suo    &#13;
 contrario non si verificherebbe se il soggetto che la prima volta  ha    &#13;
 rifiutato   il  servizio  militare  non  lo  rifiutasse  la  seconda,    &#13;
 cosicché  sarebbe  conseguenza  di  mero  fatto,   derivante   dalla    &#13;
 circostanza,  irrilevante  per  il diritto, che egli persista nel suo    &#13;
 atteggiamento di obiezione.  La persistenza è tanto poco irrilevante    &#13;
 che lo stesso  art.  8,  terzo  comma,  l'ha  presa  a  base  di  una    &#13;
 disciplina  che  -  sia  pure  imperfettamente - mira per l'appunto a    &#13;
 sterilizzarne le conseguenze ulteriori rispetto alla prima condanna e    &#13;
 a evitare quella spirale delle  condanne  che  la  giurisprudenza  di    &#13;
 questa Corte ha più volte censurato.                                    &#13;
   4.2.  - Quest'ultima considerazione introduce un secondo aspetto di    &#13;
 irrazionalità, rilevabile sotto il profilo  dell'incongruità  della    &#13;
 disciplina   impugnata  rispetto  alla  sua  ratio.  Il  terzo  comma    &#13;
 dell'art. 8, che prevede il sistema dell'esonero  amministrativo,  è    &#13;
 inequivocabilmente dettato nell'intento di evitare che l'integrazione    &#13;
 della  fattispecie di reato prevista dal secondo comma possa avvenire    &#13;
 più d'una volta, nell'ambito  della  vicenda  personale  di  ciascun    &#13;
 obiettore.  Ciò, secondo la giurisprudenza di questa Corte (sentenza    &#13;
 n.  343  del  1993),  costituisce  garanzia di proporzionalità della    &#13;
 pena, nel bilanciamento tra la protezione del dovere di difesa  della    &#13;
 Patria   e  la  garanzia  della  libertà  personale.  Ma,  nei  casi    &#13;
 particolari che  sono  qui  in  questione,  la  legge  manca  il  suo    &#13;
 obiettivo  e  tradisce la sua ratio. E, anche in questo, può vedersi    &#13;
 un aspetto della sua irrazionalità.                                     &#13;
   È ben vero, tuttavia, che  l'intento  legislativo  di  evitare  la    &#13;
 moltiplicazione  dei  processi,  delle  condanne  e delle pene non è    &#13;
 incondizionato. Ciò che preme al legislatore è che un'espiazione di    &#13;
 pena vi sia ed è a tale condizione che  si  prevede  l'esonero,  dal    &#13;
 quale  deriverà  l'impossibilità di una nuova commissione del reato    &#13;
 di rifiuto, previsto dal secondo comma dell'art. 8. Ed è altrettanto    &#13;
 vero che nei casi di sospensione o estinzione della pena l'espiazione    &#13;
 manca, in tutto o in parte. Perciò - si potrebbe concludere - quella    &#13;
 ratio  non  avrebbe  ragione  di  essere  invocata  per   argomentare    &#13;
 l'irrazionalità  della  legge:  anzi,  taluno  potrebbe al contrario    &#13;
 concludere  ch'essa,  mancando  l'espiazione  della  pena,  non  solo    &#13;
 giustifica ma addirittura esige l'iterazione della condanna.             &#13;
   Senonché,  si  deve  considerare  che  gli eventi da cui deriva la    &#13;
 mancata espiazione della pena dipendono da logiche  e  obbediscono  a    &#13;
 interessi  istituzionali  obiettivi,  tanto  da poter essere posti in    &#13;
 essere, tutti, per iniziativa unilaterale delle autorità competenti.    &#13;
 La ratio che esprime la  disciplina  dell'art.  8,  secondo  e  terzo    &#13;
 comma,  deve  pertanto essere ricostruita in modo tale da tener conto    &#13;
 dell'esistenza dei casi di estinzione e sospensione della pena.  E in    &#13;
 tale ricostruzione complessiva  l'elemento  della  previa  espiazione    &#13;
 della  pena  appare recessivo, di fronte alla duplice esigenza di non    &#13;
 impedire la normale applicazione degli  istituti  che  comportano  la    &#13;
 sospensione  o  l'estinzione  della pena e ugualmente di escludere la    &#13;
 moltiplicazione delle condanne.                                          &#13;
    In altri termini, per tener ferma come fondamentale l'esigenza  di    &#13;
 non consentire la catena delle condanne, la Corte, non potendo negare    &#13;
 in  generale  l'applicabilità  degli  istituti  della  sospensione e    &#13;
 dell'estinzione della pena al reato  previsto  dall'art.  8,  secondo    &#13;
 comma, deve invece negare l'assolutezza della previa espiazione della    &#13;
 pena,  come elemento condizionante la ragione d'essere delle norme in    &#13;
 esame.                                                                   &#13;
   È   rispetto   alla   ratio   così   ricostruita,    alla    luce    &#13;
 dell'operatività   degli   istituti  generali  della  sospensione  e    &#13;
 dell'estinzione della pena, che  gli  effetti  prodotti  dalle  norme    &#13;
 impugnate appaiono, nelle ipotesi in esame,  contraddittori.             &#13;
   5. - L'incostituzionalità della normativa impugnata, al di là dei    &#13;
 profili   di   irrazionalità  interna  al  sistema  legislativo  ora    &#13;
 esaminati, risulta altresì dalla violazione degli artt. 2, 3,  19  e    &#13;
 21,  primo comma, della Costituzione i quali, come riconosciuto dalla    &#13;
 giurisprudenza di questa Corte (sentenze nn. 196 del 1987 e  467  del    &#13;
 1991),  contengono un insieme di elementi normativi convergenti nella    &#13;
 configurazione unitaria di un principio di protezione dei  cosiddetti    &#13;
 diritti della coscienza.                                                 &#13;
   Tale   protezione,   tuttavia,  non  può  ritenersi  illimitata  e    &#13;
 incondizionata.   Spetta innanzitutto  al  legislatore  stabilire  il    &#13;
 punto  di  equilibrio  tra  la  coscienza  individuale  e le facoltà    &#13;
 ch'essa reclama, da un lato, e i complessivi, inderogabili doveri  di    &#13;
 solidarietà  politica, economica e sociale che la Costituzione (art.    &#13;
 2)   impone,  dall'altro,  affinché  l'ordinato  vivere  comune  sia    &#13;
 salvaguardato e i pesi  conseguenti  siano  equamente  ripartiti  tra    &#13;
 tutti, senza privilegi.                                                  &#13;
   Si  può pensare, nell'ambito di tali valutazioni d'insieme, che un    &#13;
 atto compiuto per ragioni di coscienza riconosciute dal  legislatore,    &#13;
 possa essere considerato illecito e incorrere in sanzione, tanto più    &#13;
 che,  nella  specie,  in  mancanza  di essa, si finirebbe per rendere    &#13;
 facoltativi sia il servizio militare  che  i  servizi  alternativi  a    &#13;
 esso,  previsti  dalla  legge.  Ma,  una volta che all'elemento delle    &#13;
 determinazioni di coscienza  si  dia  un  rilievo,  sia  pure  in  un    &#13;
 contesto  sanzionatorio,  non  si  può  non seguire la logica che ne    &#13;
 deriva.                                                                  &#13;
   Si potrà esigere l'espiazione di una pena - in mancanza  di  altre    &#13;
 più  valide  alternative  -  come  corrispettivo della sottrazione a    &#13;
 doveri costituzionalmente imposti (nella specie,  secondo  l'art.  52    &#13;
 della   Costituzione,  la  difesa  della  Patria  nelle  sue  diverse    &#13;
 possibili modalità: sentenze nn. 53 del 1967, 164 del 1985 e 470 del    &#13;
 1989).  Ma, una volta che all'elemento della coscienza si sia dato un    &#13;
 valore caratterizzante  la  disciplina  positiva,  non  si  può  poi    &#13;
 disconoscerlo  e  predisporre misure di pressione rivolte a provocare    &#13;
 il mutamento delle convinzioni e dei comportamenti secondo coscienza.    &#13;
 Quando,  secondo  valutazioni  rientranti   nell'ambito   della   sua    &#13;
 discrezionalità,   il  legislatore  ritenga  che  l'ordinato  vivere    &#13;
 sociale  non  consenta  di  riconoscere  ai  singoli  il  diritto  di    &#13;
 sottrarsi  unilateralmente  e incondizionatamente all'adempimento dei    &#13;
 doveri  di  solidarietà,  il  rilievo  ch'esso  comunque  dia   alle    &#13;
 determinazioni  di  coscienza, se è compatibile con la previsione di    &#13;
 una  sanzione  nella  quale  l'obiettore  decida  di  incorrere,  per    &#13;
 fedeltà   e   coerenza   ai  propri  convincimenti,  non  è  invece    &#13;
 ragionevolmente compatibile con la pressione morale che  si  dispiega    &#13;
 nel  tempo,  attraverso  la comminazione reiterata di sanzioni per il    &#13;
 caso di perseveranza nel medesimo atteggiamento di coscienza.            &#13;
   Tra la previsione di una prima  e  unica  sanzione  e  la  ripetuta    &#13;
 comminazione   di  sanzioni  corre  infatti  un'incolmabile  distanza    &#13;
 qualitativa. Solo la prima è compatibile con il riconoscimento della    &#13;
 signori'a individuale sulla propria  coscienza,  la  quale  può  non    &#13;
 essere  disgiunta da un onere, previsto dall'ordinamento; la seconda,    &#13;
 invece, introducendo una pressione morale  continuativa  orientata  a    &#13;
 ottenere  o  il  mutamento  dei  contenuti  della coscienza ovvero un    &#13;
 comportamento  esteriore   contrastante   con   essa,   finisce   per    &#13;
 disconoscere tale signori'a. Tale è la ragione che ha indotto questa    &#13;
 Corte,  già  nella  sentenza  n.  409  del  1989, a eliminare non la    &#13;
 previsione di una condanna ma la possibilità di quella che è  stata    &#13;
 denominata  la  "spirale  delle  condanne",  in  materia  di servizio    &#13;
 militare, quando  siano  coinvolte  questioni  di  coscienza  cui  il    &#13;
 legislatore  abbia  dato  rilievo.  Ed  è la stessa ragione che l'ha    &#13;
 indotta, nella sentenza n. 467 del 1991, a parlare con enfasi, ancora    &#13;
 per censurare  le  norme  che  lo  rendevano  possibile,  di  effetto    &#13;
 devastante  sulla  coscienza  derivante  dalla  ripetuta e perdurante    &#13;
 minaccia di sanzione.                                                    &#13;
   6.  -  L'eliminazione  dell'incostituzionalità   della   normativa    &#13;
 impugnata comporta che se ne dichiari l'illegittimità nella parte in    &#13;
 cui  non  esclude  la  possibilità  di  comminazione, irrogazione ed    &#13;
 esecuzione di più di una condanna per il  medesimo  fatto  di  reato    &#13;
 previsto  dall'art.   8, secondo comma, della legge 15 dicembre 1972,    &#13;
 n. 772. Appartiene all'ambito delle determinazioni interpretative  e,    &#13;
 eventualmente,   delle   scelte   legislative   stabilire  come  tale    &#13;
 esclusione, fin da ora imposta dal rispetto della Costituzione, possa    &#13;
 articolarsi a partire dalla disciplina positiva:  se  attraverso  una    &#13;
 riconsiderazione  della  disciplina  amministrativa della chiamata di    &#13;
 leva, oppure della disciplina penale sostanziale o processuale  della    &#13;
 materia esaminata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  dell'art.  8, secondo e    &#13;
 terzo comma, della legge 15 dicembre  1972,  n.  772  (Norme  per  il    &#13;
 riconoscimento  dell'obiezione  di coscienza), nella parte in cui non    &#13;
 esclude la possibilità di più di una condanna per il reato di  chi,    &#13;
 al  di  fuori dei casi di ammissione ai benefici previsti dalla legge    &#13;
 suddetta, rifiuta, in tempo di pace, prima di assumerlo, il  servizio    &#13;
 militare di leva, adducendo i motivi di cui all'art. 1 della medesima    &#13;
 legge.                                                                   &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 10 febbraio 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 10 febbraio 1997.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
