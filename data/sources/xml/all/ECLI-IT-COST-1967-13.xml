<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>13</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:13</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Antonino Papaldo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/02/1967</data_decisione>
    <data_deposito>09/02/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIOVANNI BATTISTA BENEDETTI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale del D.P.R.  12 febbraio  &#13;
 1965, n. 162, recante "Norme  per  la  repressione  delle  frodi  nella  &#13;
 preparazione  e  nel  commercio dei mosti, vini ed aceti", in relazione  &#13;
 all'art.  1 della legge 9 ottobre 1964, n. 991, promosso con  ordinanza  &#13;
 emessa  il 15 luglio 1966 dal Pretore di Latina nel procedimento penale  &#13;
 a carico di Manzo Francesco, iscritta al n. 181 del Registro  ordinanze  &#13;
 1966 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  258 del  &#13;
 15 ottobre 1966.                                                         &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri:                                                                &#13;
     udita nell'udienza pubblica del 18 gennaio 1967  la  relazione  del  &#13;
 Giudice Antonino Papaldo;                                                &#13;
     udito  il  sostituto  avvocato generale dello Stato Piero Peronaci,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel procedimento penale a carico di Francesco Manzo,  imputato  del  &#13;
 reato  di  cui  agli  artt.  35, secondo comma, 102 e 108 del D.P.R. 12  &#13;
 febbraio 1965, n. 162, il Pretore di Latina, con  ordinanza  15  luglio  &#13;
 1966,  ritualmente notificata ai Presidenti dei due rami del Parlamento  &#13;
 e al Presidente del Consiglio dei Ministri e pubblicata nella  Gazzetta  &#13;
 Ufficiale  della  Repubblica n.   258 del 15 ottobre 1966, riteneva non  &#13;
 manifestamente infondata la questione  di  legittimità  costituzionale  &#13;
 del  suddetto decreto delegato in relazione agli artt. 73, terzo comma,  &#13;
 e 76 della Costituzione, nonché della legge delega 9 ottobre 1964,  n.  &#13;
 991, in relazione all'art. 76 della Costituzione.                        &#13;
     Secondo  l'ordinanza, la legge delega, promulgata il 9 ottobre 1964  &#13;
 e  pubblicata  il  28  dello  stesso  mese,  si  dovrebbe   considerare  &#13;
 pubblicata  in  ritardo con violazione del precetto contenuto nell'art.  &#13;
 73, comma terzo, della Costituzione, secondo il quale  "le  leggi  sono  &#13;
 pubblicate  subito  dopo  la  promulgazione". E poiché la legge delega  &#13;
 fissa all'art. 1 il termine di tre mesi dalla sua entrata in vigore per  &#13;
 l'emanazione  della legge delegata, è da ritenere che, per effetto del  &#13;
 ritardo arbitrariamente  interposto  nella  pubblicazione  della  legge  &#13;
 delega,  l'emanazione della legge delegata sarebbe al di là dei limiti  &#13;
 temporali stabiliti dalla legge di delegazione.                          &#13;
     Inoltre,  poiché  a  norma  dell'art.   76   della   Costituzione,  &#13;
 l'esercizio   della  funzione  legislativa  può  essere  dalle  Camere  &#13;
 delegato al  Governo  "soltanto  per  tempo  limitato",  e  poiché  il  &#13;
 suddetto  requisito  di  validità  della  legge  di  delegazione  può  &#13;
 ritenersi sussistente solo in quanto, per la delimitazione del tempo di  &#13;
 esercizio della delega, si faccia riferimento a  momenti  e  fatti  non  &#13;
 suscettibili  di  risultare  influenzati dalla facoltà del Governo, la  &#13;
 legge delega sarebbe altresì in contrasto sia  con  l'art.  73,  comma  &#13;
 terzo,  della Costituzione sia con le norme poste col R.D. 24 settembre  &#13;
 1931, n. 1256, per il fatto che il termine iniziale per l'esercizio del  &#13;
 potere legislativo delegato sia stato stabilito  con  riferimento  alla  &#13;
 data di pubblicazione della legge delega.                                &#13;
     Con  memoria  depositata  il  4  novembre  1966  si  è  costituita  &#13;
 l'Avvocatura dello Stato, in rappresentanza e difesa del Presidente del  &#13;
 Consiglio dei Ministri.                                                  &#13;
     Con riguardo al primo profilo  di  illegittimità  l'Avvocatura  ha  &#13;
 richiamato la sentenza n. 163 del 19 dicembre 1963, con la quale questa  &#13;
 Corte  dichiarò  infondata  la  stessa  questione, statuendo che si ha  &#13;
 valida  prefissione  del  termine  iniziale  di  esercizio  del  potere  &#13;
 delegato  quando  il dies quo sia fatto coincicidere, come nell'attuale  &#13;
 fattispecie,  con  la  data  di  entrata  in  vigore  della  legge   di  &#13;
 delegazione,  sempre  che  in  tali  casi  siasi  adempiuto al precetto  &#13;
 dell'art. 73 della Costituzione in  ordine  alla  pronta  pubblicazione  &#13;
 della legge.                                                             &#13;
     Quanto  alla  censura  relativa  all'arbitrario ritardo che sarebbe  &#13;
 stato  frapposto  alla  pubblicazione  della  legge   di   delegazione,  &#13;
 l'Avvocatura  ha  eccepito  che,  attese  le  complesse  operazioni  di  &#13;
 pubblicazione  delle  leggi,  non   sarebbe   arbitrario   l'intervallo  &#13;
 intercorso  tra  la  data  di  promulgazione  (9  ottobre)  e quella di  &#13;
 pubblicazione  della  legge  delega  (28  ottobre).  E   pertanto   non  &#13;
 esisterebbe,  nella  specie,  alcuna  violazione  delle  invocate norme  &#13;
 costituzionali.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'ordinanza parte sostanzialmente dal principio enunciato da questa  &#13;
 Corte nella sentenza del 6 - 19 dicembre 1963, n. 163, secondo  cui  si  &#13;
 ha   valida   prefissione  del  termine  iniziale  di  esercizio  della  &#13;
 delegazione legislativa anche quando la data di  decorrenza  sia  fatta  &#13;
 coincidere  con quella dell'entrata in vigore della legge delega. Ma il  &#13;
 Pretore rileva che l'arbitrario ritardo nella pubblicazione della legge  &#13;
 delega - ritardo che si sarebbe verificato in violazione  dell'art.  73  &#13;
 della  Costituzione  e delle norme poste con il R.D. 24 settembre 1931,  &#13;
 n. 1256, sulla promulgazione e pubblicazione delle leggi - avrebbe reso  &#13;
 possibile l'emanazione delle  norme  delegate  al  di  là  dei  limiti  &#13;
 stabiliti dalla legge di delegazione.                                    &#13;
     Il rilievo non è fondato.                                           &#13;
     Vero  è  che  nella  predetta  sentenza la Corte ha affermato che,  &#13;
 allorquando si adotti il criterio di far  decorrere  il  termine  dalla  &#13;
 data di entrata in vigore della legge delega, deve esigersi un rigoroso  &#13;
 adempimento  dell'obbligo,  imposto dall'art. 73 della Costituzione, di  &#13;
 procedere alle  operazioni  necessarie  a  rendere  efficace  la  legge  &#13;
 medesima subito dopo che sia intervenuta la promulgazione.               &#13;
     Ma  è  chiaro  che  deve  trattarsi  di  un ritardo arbitrario; un  &#13;
 ritardo,  cioè,  che  abbia  per  effetto  l'emanazione  del   decreto  &#13;
 legislativo  al  di  là  dei  limiti  di  tempo  stabiliti dalla legge  &#13;
 delegante.                                                               &#13;
     Ora, nella specie, è da escludere che ci sia stato un ritardo  tra  &#13;
 la  promulgazione  (9  ottobre)  e  la pubblicazione (28 ottobre) della  &#13;
 legge  delega;  comunque,  anche  se  si  potesse   ritenere   che   la  &#13;
 pubblicazione  dopo una ventina di giorni non corrisponda rigorosamente  &#13;
 alla norma contenuta nell'art. 73, terzo comma, della Costituzione  (il  &#13;
 testo unico delle norme sulla promulgazione e pubblicazione delle leggi  &#13;
 è  fuori causa in questa sede), è certo che nel caso attuale non può  &#13;
 parlarsi di ritardo arbitrario agli effetti dell'osservanza dei termini  &#13;
 a norma dell'art.  76 della Costituzione.                                &#13;
     L'adempimento del  precetto  contenuto  nell'art.  73  non  è  qui  &#13;
 prospettabile   sotto   l'angolo   visuale   della   legittimità   del  &#13;
 conferimento della delega, in quanto la legge di delegazione sia  stata  &#13;
 pubblicata  in  ritardo,  bensì  sotto  quello  della legittimità del  &#13;
 decreto legislativo, in quanto emanato oltre il termine. Ma,  a  questi  &#13;
 effetti, non basta che esista un qualsiasi ritardo nella pubblicazione:  &#13;
 occorre che il ritardo sia arbitrario, che, cioè, sia tale da indicare  &#13;
 uno spostamento dell'inizio del termine di esercizio della delega.       &#13;
     Ora, nella specie, mentre nulla fa ritenere che l'intervallo tra la  &#13;
 data  di promulgazione e quella di pubblicazione della legge delega sia  &#13;
 stato  preordinato  all'effetto  di  eludere  il  termine  fissato  per  &#13;
 l'esercizio  della  delega,  non  può dirsi che l'intervallo sia stato  &#13;
 tanto lungo da costituire, anche  senza  ed  oltre  l'intenzione  degli  &#13;
 organi governativi, causa di prolungamento del termine stesso,</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 del  D.P.R.  12  febbraio  1965,  n.  162,  contenente  norme  per   la  &#13;
 repressione  delle  frodi nella preparazione e nel commercio dei mosti,  &#13;
 vini ed aceti, in relazione all'art. 1 della legge 9 ottobre  1964,  n.  &#13;
 991, ed in riferimento agli artt. 73 e 76 della Costituzione.            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 1 febbraio 1967.                              &#13;
                                       GASPARE  AMBROSINI   -   ANTONINO  &#13;
                                   PAPALDO  -  NICOLA  JAEGER - GIOVANNI  &#13;
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  &#13;
                                   ANTONIO   MANCA  -  ALDO  SANDULLI  -  &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -    GIOVANNI    BATTISTA  &#13;
                                   BENEDETTI - FRANCESCO PAOLO BONIFACIO  &#13;
                                   - LUIGI OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
