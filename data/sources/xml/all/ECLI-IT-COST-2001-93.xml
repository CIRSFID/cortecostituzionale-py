<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>93</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:93</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/03/2001</data_decisione>
    <data_deposito>30/03/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY,Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, &#13;
Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco &#13;
BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'articolo 4, comma &#13;
10,  del  decreto  legislativo  24  febbraio 1998, n. 58 (Testo unico &#13;
delle  disposizioni  in  materia  di  intermediazione finanziaria, ai &#13;
sensi  degli  artt. 8  e  21  della  legge  6  febbraio 1996, n. 52), &#13;
promosso  con  ordinanza  emessa  il 21 gennaio 2000 dal Consiglio di &#13;
Stato,  iscritta  al  n. 497 del registro ordinanze 2000 e pubblicata &#13;
nella  Gazzetta  Ufficiale  della Repubblica 1ª serie speciale, n. 39 &#13;
dell'anno 2000. &#13;
    Visti   gli   atti  di  costituzione  delle  parti  del  giudizio &#13;
principale, nonché l'atto di intervento del Presidente del Consiglio &#13;
dei ministri; &#13;
    Udito  nella  camera di consiglio del 21 febbraio 2001 il giudice &#13;
relatore Carlo Mezzanotte. &#13;
    Ritenuto che, con ordinanza in data 21 gennaio 2000, il Consiglio &#13;
di  Stato  ha  sollevato, in riferimento agli articoli 3, 24, 76 e 97 &#13;
della   Costituzione,   questione   di   legittimità  costituzionale &#13;
dell'articolo  4, comma 10, del decreto legislativo 24 febbraio 1998, &#13;
n. 58  (Testo  unico delle disposizioni in materia di intermediazione &#13;
finanziaria, ai sensi degli artt. 8 e 21 della legge 6 febbraio 1996, &#13;
n. 52),  nella  parte  in  cui  "ha  inteso precludere l'accesso alle &#13;
notizie,  alle  informazioni  ed  ai dati in possesso della CONSOB in &#13;
ragione  della  sua  attività  di vigilanza, definendoli "coperti da &#13;
segreto d'ufficio "; &#13;
        che  in  particolare, ad avviso del giudice a quo, l'articolo &#13;
4,  comma  10,  del decreto legislativo n. 58 del 1998 contrasterebbe &#13;
con  l'articolo  3  della  Costituzione,  sia  sotto il profilo della &#13;
disparità  di trattamento tra soggetti che siano sottoposti a poteri &#13;
di  vigilanza  e  di  controllo  in diversi ambiti professionali, sia &#13;
sotto   il   profilo   del   mancato   rispetto   del   canone  della &#13;
ragionevolezza,   tenuto   conto   degli  interessi  che  vengono  in &#13;
considerazione  e del principio per il quale ad un soggetto coinvolto &#13;
in  un  qualsiasi  procedimento,  a  maggior  ragione  se  di  natura &#13;
disciplinare,  vanno  rilasciati gli atti che riguardino direttamente &#13;
la  sua  sfera  giuridica, anche quando si tratti di attività svolta &#13;
dall'autorità amministrativa nell'esercizio di poteri di vigilanza e &#13;
di controllo; &#13;
        che,   prosegue  il  remittente,  la  disposizione  censurata &#13;
violerebbe  l'articolo  24 della Costituzione, poiché la preclusione &#13;
dell'accesso  agli atti della Commissione nazionale per le società e &#13;
la  borsa  (CONSOB)  potrebbe  negativamente  incidere  sulle  scelte &#13;
processuali  dei soggetti interessati, i quali non sarebbero posti in &#13;
condizione  di  valutare  se gli atti ed i comportamenti della CONSOB &#13;
siano  conformi  alla  legge,  anche  nel  caso in cui il diniego sia &#13;
motivato  da  ragioni  di  interesse  pubblico, e sarebbe altresì in &#13;
contrasto  con  i  principî  di buon andamento ed imparzialità della &#13;
pubblica amministrazione; &#13;
        che,  infine,  secondo  il  Consiglio di Stato, l'articolo 4, &#13;
comma   10,   del  decreto  legislativo  n. 58  del  1998  violerebbe &#13;
l'articolo  76 della Costituzione, in quanto l'articolo 1 della legge &#13;
di delegazione 6 febbraio 1996, n. 52 (Disposizioni per l'adempimento &#13;
di  obblighi  derivanti  dall'appartenenza dell'Italia alle Comunità &#13;
europee  - Legge comunitaria 1994) prevede che "ove ricorrano deleghe &#13;
al  Governo  per l'emanazione di decreti legislativi recanti le norme &#13;
occorrenti  per  dare  attuazione  alle  direttive comunitarie, tra i &#13;
principî  e i criteri generali dovranno sempre essere previsti quelli &#13;
della   piena   trasparenza   e   della   imparzialità   dell'azione &#13;
amministrativa,  al  fine  di  garantire  il  diritto di accesso alla &#13;
documentazione e ad una corretta informazione dei cittadini, nonché, &#13;
nei modi opportuni, i diritti dei consumatori e degli utenti"; &#13;
        che  si  è  costituita  in  giudizio la parte ricorrente nel &#13;
processo  principale,  e ha chiesto l'accoglimento della questione di &#13;
legittimità costituzionale; &#13;
        che si è costituita altresì la Commissione nazionale per le &#13;
società  e  la  borsa,  in persona del Presidente pro-tempore, ed è &#13;
intervenuto  il  Presidente  del  Consiglio  dei  ministri,  entrambi &#13;
rappresentati  e difesi dall'Avvocatura generale dello Stato, e hanno &#13;
chiesto che la questione sia dichiarata inammissibile o infondata. &#13;
    Considerato  che,  successivamente  all'ordinanza  di rimessione, &#13;
questa  Corte,  con  la  sentenza  n. 460 del 2000, ha dichiarato non &#13;
fondata,   nei   sensi   di  cui  in  motivazione,  la  questione  di &#13;
legittimità  costituzionale  dell'articolo  4, comma 10, del decreto &#13;
legislativo  24  febbraio 1998, n. 58 (Testo unico delle disposizioni &#13;
in  materia  di intermediazione finanziaria, ai sensi degli artt. 8 e &#13;
21  della legge 6 febbraio 1996, n. 52), proposta in riferimento agli &#13;
articoli  2, 3, 11, 21, 24, 97, primo comma, e 98, primo comma, della &#13;
Costituzione; &#13;
        che  nella  citata  pronuncia, si è chiarito che la sfera di &#13;
applicazione  dell'articolo 4, comma 10, quale che ne sia l'effettiva &#13;
estensione,  con certezza non comprende gli atti, le notizie e i dati &#13;
in   possesso  della  CONSOB  in  relazione  alla  sua  attività  di &#13;
vigilanza,  posti  a  fondamento  di  un  procedimento  disciplinare, &#13;
sicché  questi,  nei  confronti  dell'interessato,  non sono affatto &#13;
segreti  e  sono  invece  pienamente  accessibili,  non  soltanto nel &#13;
giudizio  di  opposizione  alla sanzione disciplinare, ma anche nello &#13;
speciale  procedimento  di  accesso  regolato  dall'articolo 25 della &#13;
legge   7   agosto   1990,   n. 241,   strumento   esperibile   anche &#13;
dall'incolpato    nei   procedimenti   disciplinari   per   orientare &#13;
preventivamente l'azione amministrativa onde evitarne deviazioni; &#13;
        che,  in  relazione  al  diritto  di accesso nei procedimenti &#13;
disciplinari,  l'interpretazione offerta dalla citata sentenza lascia &#13;
indenne   l'articolo   4,   comma  10,  dalla  denunciata  violazione &#13;
dell'articolo  76  della Costituzione per contrasto con l'articolo 1, &#13;
comma  1,  della  legge 6 febbraio 1996, n. 52, che, tra i principî e &#13;
criteri   generali  ai  quali  il  Governo  avrebbe  dovuto  ispirare &#13;
l'esercizio  della delega, includeva quello della piena trasparenza e &#13;
della  imparzialità dell'azione amministrativa, al fine di garantire &#13;
il diritto di accesso alla documentazione; &#13;
        che,   pertanto,   la   questione   deve   essere  dichiarata &#13;
manifestamente infondata in riferimento a tutti i parametri evocati. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'articolo  4, comma 10, del decreto &#13;
legislativo  24  febbraio 1998, n. 58 (Testo unico delle disposizioni &#13;
in  materia  di intermediazione finanziaria, ai sensi degli artt. 8 e &#13;
21  della  legge  6  febbraio 1996, n. 52), sollevata, in riferimento &#13;
agli  articoli  3,  24,  76 e 97 della Costituzione, dal Consiglio di &#13;
Stato con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 21 marzo 2001. &#13;
                       Il Presidente: Ruperto &#13;
                      Il redattore: Mezzanotte &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 30 marzo 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
