<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>73</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:73</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/04/1982</data_decisione>
    <data_deposito>16/04/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, 4,  5  e  &#13;
 13  della legge 22 luglio 1966, n. 607 (Norme in materia di enfiteusi e  &#13;
 prestazioni fondiarie perpetue) promosso con ordinanza,  emessa  il  18  &#13;
 luglio  1975  dal  Tribunale di Reggio Calabria nel procedimento civile  &#13;
 vertente tra Placanica Antonio e Mallamaci Consolato ed altri, iscritta  &#13;
 al n. 627 del registro  ordinanze  1975  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 58 del 3 marzo 1976.                       &#13;
     Udito  nella  camera  di  consiglio del 25 febbraio 1982 il Giudice  &#13;
 relatore Virgilio Andrioli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ricorso, depositato il  9  marzo  1970  nella  cancelleria  &#13;
 della   Pretura   di  Gallina,  Mallamaci  Consolato,  Legato  Carmelo,  &#13;
 Mallamaci Filippo, Carmela e Angela, Verduci Carmela Titina e  Carmelo,  &#13;
 coenfiteuti  di  un fondo sito in Motta S. Giovanni in forza di atto 25  &#13;
 aprile 1850 per notar Oliva, ne chiesero l'affrancazione ai sensi della  &#13;
 l. 22 luglio 1966 n. 607 nei confronti di Placanica Antonio,  il  quale  &#13;
 oppose  tra  l'altro  che  il rogito Oliva non era a lui opponibile per  &#13;
 difetto di trascrizione, ma l'adito Pretore, con  ordinanza  depositata  &#13;
 il 10 novembre 1970, dispose l'affrancazione in favore dei ricorrenti.   &#13;
     2.  -  La  Sezione  specializzata  agraria  del Tribunale di Reggio  &#13;
 Calabria, avanti la quale il Placanica non  solo  aveva  insistito  nel  &#13;
 contestare  la sussistenza di un rapporto di enfiteusi per essere stati  &#13;
 i miglioramenti eseguiti dal Placanica medesimo, ma  aveva  chiesto  in  &#13;
 via  riconvenzionale  la condanna dei ricorrenti alla corresponsione di  &#13;
 indennità per aver essi fruito dei miglioramenti, a) con sentenza  non  &#13;
 definitiva  18  luglio  1975  depositata  il  15 ottobre (di cui non si  &#13;
 rinviene copia nel fascicolo trasmesso) confermò il  provvedimento  di  &#13;
 affrancazione  e  dispose  la  prosecuzione  del  giudizio  per  quanto  &#13;
 concerne la domanda riconvenzionale di indennità per  i  miglioramenti  &#13;
 che  il  medesimo  concedente Placanica aveva addotto di aver recato al  &#13;
 fondo  affrancato,  e  b)  con  separata  ordinanza  18   luglio   1975  &#13;
 (notificata  il 22 ottobre e comunicata il 13 novembre 1975, pubblicata  &#13;
 nella G.U. n. 58 del 3 marzo 1976 e iscritta al n.  627  R.O.  1975)  -  &#13;
 presa  in  esame  la  diversa  disciplina positiva riservata in tema di  &#13;
 indennità per miglioramenti al  proprietario  concedente  rispetto  al  &#13;
 trattamento  stabilito  per  il  mezzadro,  il colono e il possessore -  &#13;
 giudicò rilevante e  non  manifestamente  infondata  la  questione  di  &#13;
 legittimità costituzionale degli artt.  1, 4, 5, 13, l. 22 luglio 1966  &#13;
 n.  607 in riferimento all'art. 3 Cost., nonché, sotto l'aspetto della  &#13;
 menomazione della iniziativa economica privata e della mancata garanzia  &#13;
 della proprietà privata, in riferimento agli. artt.41 e 42 Cost..       &#13;
     3. - Avanti la Corte nessuna delle parti si è  costituita  né  ha  &#13;
 spiegato intervento il Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     4.   -  La  questione  è  inammissibile  perché  il  proprietario  &#13;
 concedente (quale è da ritenersi allo stato  il  Placanica  a  seguito  &#13;
 della  sentenza  non  definitiva  del  Tribunale),  che  assume di aver  &#13;
 apportato miglioramenti al fondo, non rientra in alcuna  delle  quattro  &#13;
 categorie  giuridiche  la  cui discrepanza di trattamento viene assunta  &#13;
 dal giudice a quo a motivo di violazione del principio di  uguaglianza:  &#13;
 non dell'enfiteuta perché trattasi di concedente, non dell'affittuario  &#13;
 perché  il  concedente non ha la gestione del bene produttivo, né del  &#13;
 mezzadro perché non ne ha la cogestione, e  non  ne  gode  infine  del  &#13;
 possesso perché questo compete all'enfiteuta. Sorte non diversa merita  &#13;
 l'assunzione,  che  da parte del giudice a quo si è fatta, degli artt.  &#13;
 42 e 41 a parametri di costituzionalità  perché  per  un  verso  alla  &#13;
 proprietà si sovrappone il rapporto enfiteutico e per altro verso (non  &#13;
 al   concedente   ma)   all'enfiteuta  spetta  l'iniziativa  economica.  &#13;
 Concedente che può, se ed  in  quanto  ne  sussistano  i  presupposti,  &#13;
 esercitare l'azione di indebito arricchimento.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 degli artt. 1, 4, 5 e 13 l.  22  luglio  1966  n.  607,  sollevata,  in  &#13;
 riferimento  agli  artt. 3, 41 e 42 Cost., con ordinanza 18 luglio 1975  &#13;
 dalla sezione specializzata agraria del Tribunale di Reggio Calabria.    &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 2 aprile 1982.          &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   -  ANTONINO  DE  STEFANO  - GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELI DUCCI - ALBERTO MALAGUGINI  &#13;
                                   -  LIVIO PALADIN - ANTONIO LA PERGOLA  &#13;
                                   -  VIRGILIO   ANDRIOLI   -   GIUSEPPE  &#13;
                                   FERRARI  -  FRANCESCO SAJA - GIOVANNI  &#13;
                                   CONSO.                                 &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
