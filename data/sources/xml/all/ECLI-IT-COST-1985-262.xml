<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>262</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:262</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/11/1985</data_decisione>
    <data_deposito>08/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 10 del r.d.  8  &#13;
 gennaio  1931,  n.  148  ("Coordinamento  delle  norme sulla disciplina  &#13;
 giuridica dei rapporti collettivi di lavoro con quelle sul  trattamento  &#13;
 giuridico-economico  del  personale  delle ferrovie, tranvie e linee di  &#13;
 navigazione interna in regime di concessione"),  nel  testo  risultante  &#13;
 dopo  la  sentenza  della Corte costituzionale n. 93 del 1979, promossi  &#13;
 con ordinanze emesse il 18 aprile, il 13 maggio 1980 e  il  19  gennaio  &#13;
 1981 dal Pretore di Roma (n. 4 ord.), il 28 gennaio 1981 dal Pretore di  &#13;
 Firenze  (n.  2  ord.),  il  14  e  il  15 dicembre 1981 dal Pretore di  &#13;
 Brescia, il 25 marzo 1982 dal Pretore di Roma e il 20 aprile  1983  dal  &#13;
 Pretore  di  Ferrara, iscritte rispettivamente ai nn. 429, 452, 468 del  &#13;
 registro ordinanze 1980, ai nn. 206, 322, 323  del  registro  ordinanze  &#13;
 1981, ai nn.  148, 149, 485 del registro ordinanze 1982 e al n. 499 del  &#13;
 registro  ordinanze  1983  e  pubblicate nella Gazzetta Ufficiale della  &#13;
 Repubblica nn. 228, 235 dell'anno 1980, nn. 193,  269  dell'anno  1981,  &#13;
 nn. 206, 220 e 344 dell'anno 1982 e n. 322 dell'anno 1983.               &#13;
     Visti  gli  atti  di costituzione dell'ATAC e dell'ATAF nonché gli  &#13;
 atti di intervento del Presidente del Consiglio dei ministri;            &#13;
     udito nella camera di consiglio  del  9  ottobre  1985  il  Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto  che  con  quattro  ordinanze  in data 18 aprile 1980 e 19  &#13;
 gennaio 1981 - emesse in una serie di controversie di  lavoro  promosse  &#13;
 da  addetti  a pubblici servizi di trasporto, nel corso delle quali era  &#13;
 stata eccepita dall'azienda  convenuta  l'improcedibilità  dell'azione  &#13;
 giudiziaria  per  omesso  o  tardivo  esperimento  del  previo  reclamo  &#13;
 gerarchico ai sensi dell'art. 10 del r.d. 8  gennaio  1931,  n.  148  e  &#13;
 successive   modifiche   l'adito  Pretore  di  Roma  ha  denunciato  di  &#13;
 illegittimità, in riferimento agli artt. 3 e  24  Cost.,  il  predetto  &#13;
 art.  10  r.d. cit., nel testo risultante dalla sentenza della Corte n.  &#13;
 93/1979, "in quanto non prevede che il giudice possa, con la  pronuncia  &#13;
 di  improcedibilità, rimettere in termini le parti, con la contestuale  &#13;
 sospensione del processo in corso";                                      &#13;
     che,  secondo  il  giudice  a  quo,  la  richiamata  decisione già  &#13;
 dichiarativa della illegittimità dell'art. 10, secondo e terzo  comma,  &#13;
 r.d. 148 cit. "nella parte in cui dispone (va) l'improponibilità e non  &#13;
 l'improcedibilità dell'azione giudiziaria in caso di mancata o tardiva  &#13;
 presentazione del reclamo gerarchico nelle controversie (come quelle di  &#13;
 specie) aventi ad oggetto il riconoscimento della qualifica "o "di ogni  &#13;
 altro  diritto non esclusivamente patrimoniale" inerente al rapporto di  &#13;
 lavoro degli autoferrotranvieri -  sarebbe  priva  di  effetti  pratici  &#13;
 (onde  appunto  l'immanente  contrasto  dell'impugnato  art. 10 con gli  &#13;
 artt. 3 e  24  Cost.),  una  volta  che  l'attuata  sostituzione  della  &#13;
 sanzione  della  "improponibilità" con quella della "improcedibilità"  &#13;
 non  si  completerebbe  con  la  previsione  dell'applicabilità  degli  &#13;
 istituti  conseguenziali  della  improcedibilità  stessa: istituti che  &#13;
 l'interprete non sarebbe autorizzato a desumere per analogia  dall'art.  &#13;
 443  c.p.c.  (dettato  per  un  diverso  tipo  di controversia), né il  &#13;
 legislatore, da parte sua, avrebbe provveduto ad introdurre;             &#13;
     che innanzi  alla  Corte,  nei  giudizi  relativi  alle  prime  due  &#13;
 ordinanze,  si è costituita l'ATAC e, nel giudizio relativo alla terza  &#13;
 ordinanza, è intervenuto il Presidente  del  Consiglio  dei  ministri:  &#13;
 entrambi,  se pur con diversa motivazione, concludendo per la manifesta  &#13;
 infondatezza della questione;                                            &#13;
     che lo stesso Pretore di Roma con ordinanza 13  maggio  1980  e  25  &#13;
 marzo  1982,  il  Pretore  di  Firenze con due ordinanze del 28 gennaio  &#13;
 1981, il Pretore di Brescia con ordinanze 14 e 15 dicembre 1981  ed  il  &#13;
 Pretore  di  Ferrara, con un'ultima ordinanza del 20 aprile 1983, hanno  &#13;
 impugnato il predetto art. 10, sempre nel testo già  emendato  con  la  &#13;
 sentenza  n.  93  del  1979,  in  relazione  all'ultimo profilo (che si  &#13;
 ritiene non intaccato dalla citata pronuncia)  della  "improponibilità  &#13;
 dell'azione  giudiziaria  promossa  oltre il termine di sessanta giorni  &#13;
 dalla definizione del reclamo gerarchico o dal termine di trenta giorni  &#13;
 per detta definizione";                                                  &#13;
     che si assumono, anche in questo caso, violati gli  artt.  3  e  24  &#13;
 Cost. per l'ingiustificata disparità di trattamento (sul terreno della  &#13;
 difesa  dei propri diritti) che si determinerebbe, per un verso, tra la  &#13;
 generalità  dei  lavoratori   subordinati   e   la   categoria   degli  &#13;
 autoferrotranvieri,  in  quanto  "le  finalità  di  pubblico interesse  &#13;
 inerenti alla natura del servizio  non  richiedono  di  necessità  una  &#13;
 compressione  del  diritto  dei  lavoratori  del  settore in termini di  &#13;
 decadenza dello stesso"; e, per altro verso, tra gli autoferrotranvieri  &#13;
 che abbiano proposto tardivamente il reclamo gerarchico (per  i  quali,  &#13;
 dopo   la   sentenza   n.   93/1979,   la   sanzione  è  quella  della  &#13;
 improcedibilità   e   non   più   della   improponibilità)   e   gli  &#13;
 autoferrotranvieri   che   abbiano   proposto   tardivamente   l'azione  &#13;
 giudiziaria, i quali incorrerebbero invece nella decadenza;              &#13;
     che nei giudizi relativi alle ordinanze del Pretore di  Firenze  si  &#13;
 è  costituita  l'Azienda  convenuta  (ATAF),  che  ha  concluso per il  &#13;
 rigetto dell'impugnativa; e nei medesimi giudizi ed in quelli  relativi  &#13;
 alle  ordinanze  dei  Pretori di Roma e Ferrara è anche intervenuto il  &#13;
 Presidente  del  Consiglio  dei   ministri,   che   ha   concluso   per  &#13;
 l'infondatezza  della impugnativa, escludendo che possano equipararsi -  &#13;
 tanto sul piano giuridico formale quanto su quello pratico  -  l'omesso  &#13;
 esperimento  del rimedio gerarchico e la tardiva adizione dell'AGO dopo  &#13;
 l'esito di tale rimedio; e sottolineando che comunque "la previsione di  &#13;
 termini per l'accesso alla tutela giurisdizionale  rientra  nell'ambito  &#13;
 della potestà - latamente discrezionale - del legislatore ordinario di  &#13;
 determinare le modalità di esercizio del diritto di difesa".            &#13;
     Considerato   che   le   dieci  ordinanze  sollevano  questioni  di  &#13;
 legittimità  analoghe  e  connesse,   che   possono   perciò   venire  &#13;
 unitariamente decise;                                                    &#13;
     che  entrambe  le  riassunte questioni sono state, per altro, negli  &#13;
 identici termini, già esaminate dalla Corte che, con sentenza  n.  136  &#13;
 del  corrente  anno,  ha  escluso  la fondatezza dell'una e dell'altra,  &#13;
 richiamando l'ormai consolidata giurisprudenza della Cassazione che  (a  &#13;
 seguito  della  citata  sentenza  n.  93/79)  interpreta l'art. 10 r.d.  &#13;
 148/1931 nei precisi termini in cui vorrebbero ricondurlo  i  richiesti  &#13;
 interventi correttivi; e cioè appunto nel senso dell'applicabilità in  &#13;
 via  analogica dell'art. 443 c.p.c., in caso di tardività del reclamo,  &#13;
 e   della   esclusione   della   decadenza   dall'azione   nell'ipotesi  &#13;
 dell'inerzia  dell'interessato  contro il rigetto o il silenzio-rigetto  &#13;
 del reclamo: onde deve  ribadirsi  che  non  sussistono  le  denunciate  &#13;
 violazioni degli artt. 3 e 24 Cost..                                     &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e  9, comma secondo, delle Norme integrative per i giudizi davanti alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la manifesta infondatezza delle questioni di  legittimità  &#13;
 costituzionale dell'art. 10, secondo e terzo comma, del regio decreto 8  &#13;
 gennaio  1931,  n. 148 ("Coordinamento delle norme sulla disciplina dei  &#13;
 rapporti collettivi di lavoro con  quelle  sul  trattamento  giuridico-economico  del personale delle ferrovie, tranvie e linee di navigazione  &#13;
 interna in regime  di  concessione"),  nel  testo  risultante  dopo  la  &#13;
 sentenza  della  Corte  costituzionale n. 93 del 1979, sollevate con le  &#13;
 ordinanze  in  epigrafe,  in  riferimento  agli  artt.  3  e  24  della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 novembre 1985.        &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   RENATO DELL'ANDRO.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
