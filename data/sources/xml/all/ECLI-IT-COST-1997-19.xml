<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:19</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/01/1997</data_decisione>
    <data_deposito>10/02/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  ammissibilità, ai sensi dell'art. 2, primo comma,    &#13;
 della legge costituzionale 11 marzo 1953, n. 1,  della  richiesta  di    &#13;
 referendum   popolare   per   l'abrogazione  dell'art.  4,  comma  2,    &#13;
 limitatamente alle parole "non"  e  "se  non  previa  intesa  con  il    &#13;
 Governo  e  nell'ambito degli indirizzi e degli atti di coordinamento    &#13;
 di cui  al  comma  precedente",  del  decreto  del  Presidente  della    &#13;
 Repubblica  24  luglio  1977,  n. 616 (Attuazione della delega di cui    &#13;
 all'art. 1 della legge 22 luglio 1975, n. 382), iscritto al n. 87 del    &#13;
 registro referendum;                                                     &#13;
   Vista  l'ordinanza  del  26-27 novembre 1996 con la quale l'Ufficio    &#13;
 centrale  per  il  referendum  presso  la  Corte  di  cassazione   ha    &#13;
 dichiarato legittima la richiesta;                                       &#13;
    Udito  nella  camera  di  consiglio dell'8 gennaio 1997 il giudice    &#13;
 relatore Carlo Mezzanotte;                                               &#13;
    Uditi gli avvocati Stefano Grassi e Beniamino Caravita di  Toritto    &#13;
 per  i delegati dei Consigli regionali della Lombardia, del Piemonte,    &#13;
 della Valle d'Aosta, della Calabria, del Veneto, della Puglia e della    &#13;
 Toscana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. -  L'Ufficio centrale per il referendum,  costituito  presso  la    &#13;
 Corte  di  cassazione, in applicazione della legge 25 maggio 1970, n.    &#13;
 352,  e  successive  modificazioni,  ha  esaminato  la  richiesta  di    &#13;
 referendum  popolare, presentata dai Consigli regionali delle Regioni    &#13;
 Valle d'Aosta,  Toscana,  Veneto,  Lombardia,  Piemonte,  Calabria  e    &#13;
 Puglia,  sul  seguente quesito: Volete voi che sia abrogato l'art. 4,    &#13;
 secondo comma, limitatamente alle  parole  "non"  e  "se  non  previa    &#13;
 intesa  con  il Governo e nell'ambito degli indirizzi e degli atti di    &#13;
 coordinamento di cui al comma precedente" del d.P.R. 24 luglio  1977,    &#13;
 n.  616  (Attuazione  della  delega  di cui all'art. 1 della legge 22    &#13;
 luglio 1975, n. 382)?".                                                  &#13;
   2. - Con ordinanza depositata in data 27 novembre  1996,  l'Ufficio    &#13;
 centrale  per  il  referendum  ha  dichiarato  la  legittimità della    &#13;
 richiesta, stabilendo come denominazione del  referendum:  Abolizione    &#13;
 dei  limiti  statali  alle  attività  promozionali  all'estero delle    &#13;
 Regioni.                                                                 &#13;
   3.  -  Ricevuta  la   comunicazione   dell'ordinanza   dall'Ufficio    &#13;
 centrale,  il  Presidente  di  questa Corte ha fissato il giudizio di    &#13;
 ammissibilità  della  richiesta  referendaria  per  la   camera   di    &#13;
 consiglio  dell'8  gennaio 1997, disponendo altresì le comunicazioni    &#13;
 previste dall'art. 33, secondo comma, della legge n. 352 del 1970.       &#13;
   4. - Nell'imminenza della  camera  di  consiglio,  i  delegati  dei    &#13;
 Consigli  regionali  delle  Regioni  promotrici  del referendum hanno    &#13;
 depositato una memoria, con la quale insistono perché sia dichiarata    &#13;
 l'ammissibilità  della  richiesta  e  chiariscono  che   la   stessa    &#13;
 mirerebbe  ad  attribuire  alle  Regioni  la possibilità di svolgere    &#13;
 attività  promozionali   all'estero   nelle   materie   di   propria    &#13;
 competenza, senza limitazioni di provenienza governativa.                &#13;
   La  richiesta  di  referendum,  secondo  i  delegati  dei  Consigli    &#13;
 regionali, non solo  rappresenterebbe  il  risultato  dell'evoluzione    &#13;
 dell'ordinamento  nazionale e comunitario, ma non porrebbe nemmeno in    &#13;
 discussione il potere estero dello Stato, in  quanto  si  riferirebbe    &#13;
 esclusivamente  allo  svolgimento  di  attività  che costituirebbero    &#13;
 espressione  piena  della  capacità   giuridica   e   dell'autonomia    &#13;
 regionale.                                                               &#13;
   In   relazione   agli   altri   temi  generali  dell'ammissibilità    &#13;
 referendaria, le Regioni promotrici rilevano che il quesito,  pur  se    &#13;
 di  tipo  "parziale",  non  avrebbe  natura  manipolativa  e, essendo    &#13;
 ispirato ad una matrice razionalmente unitaria,  rispetterebbe  anche    &#13;
 le  condizioni di omogeneità, chiarezza, coerenza e univocità poste    &#13;
 dalla giurisprudenza costituzionale.                                     &#13;
   Né,   ad   avviso   dei   delegati   dei    Consigli    regionali,    &#13;
 all'inammissibilità del referendum condurrebbe la considerazione che    &#13;
 l'esito  favorevole della consultazione referendaria darebbe luogo ad    &#13;
 una situazione normativa incostituzionale.  E  ciò  sia  perché,  a    &#13;
 fronte   di   attività   promozionali   delle   Regioni   all'estero    &#13;
 interferenti con la sfera dei rapporti internazionali riservata  allo    &#13;
 Stato,  sarebbe  pur  sempre esperibile il conflitto di attribuzione,    &#13;
 sia perché, secondo la consolidata giurisprudenza di  questa  Corte,    &#13;
 nessun  rilievo  potrebbe avere in sede di giudizio di ammissibilità    &#13;
 delle richieste di referendum abrogativo la eventuale  illegittimità    &#13;
 costituzionale delle norme legislative residue.<diritto>Considerato in diritto</diritto>1.  -    La  richiesta  di referendum abrogativo sulla quale questa    &#13;
 Corte è chiamata a pronunciarsi investe l'art. 4, secondo comma, del    &#13;
 d.P.R. 24 luglio 1977, n. 616,  secondo  il  quale  le  Regioni  "non    &#13;
 possono  svolgere  all'estero  attività  promozionali  relative alle    &#13;
 materie di loro competenza se non previa  intesa  con  il  Governo  e    &#13;
 nell'ambito degli indirizzi e degli atti di coordinamento" menzionati    &#13;
 nel  primo comma dello stesso articolo, che ne affida l'adozione allo    &#13;
 Stato.  A  causa  del  modo  in  cui   è   formulato   (si   propone    &#13;
 l'eliminazione  delle parole "non" e "se non previa intesa etc."), la    &#13;
 ratio che il quesito referendario obiettivamente incorpora  consiste,    &#13;
 come   del   resto   risulta  anche  dalla  denominazione  impostagli    &#13;
 dall'Ufficio centrale per il referendum della  Corte  di  cassazione,    &#13;
 nella  rimozione  di  ogni  limite statale all'attività promozionale    &#13;
 all'estero  delle  Regioni,  così  che  queste  possano  liberamente    &#13;
 autodeterminarsi  senza dover soggiacere all'onere di intese previe o    &#13;
 di osservare indirizzi governativi procedimentali o di contenuto.        &#13;
   2. - La richiesta, nel suo oggettivo ed  evidente  significato,  è    &#13;
 inammissibile.                                                           &#13;
   Lo   svolgimento   all'estero   di  attività  promozionale  spetta    &#13;
 indubbiamente alle Regioni come attività loro propria,  comprendente    &#13;
 ogni  comportamento  legato  da  un  nesso  di  strumentalità con le    &#13;
 materie di competenza regionale,  diretto  allo  sviluppo  economico,    &#13;
 sociale  e  culturale  del  proprio  territorio  (sentenza n. 179 del    &#13;
 1987). E tuttavia, contrariamente a quanto  sostenuto  dalle  Regioni    &#13;
 promotrici,  si  tratta  di  attività  non  solo  contigua, ma anche    &#13;
 potenzialmente interferente con la  politica  estera  riservata  allo    &#13;
 Stato;  essa  postula  pertanto  strumenti giuridici di coordinamento    &#13;
 onde evitare che si determinino riflessi negativi sugli indirizzi  di    &#13;
 politica internazionale assunti dal Parlamento e dal Governo.            &#13;
   3.  -  Questa  Corte  ha  più  volte  ricordato come il peculiare,    &#13;
 reciproco atteggiarsi delle competenze statali e regionali in materia    &#13;
 internazionale ed il loro inevitabile interferire chiamino  in  causa    &#13;
 il  principio di leale cooperazione (sentenze n. 425 del 1995, n. 212    &#13;
 del 1994, n. 250 del 1993), il quale impone,  quale  vero  e  proprio    &#13;
 vincolo  costituzionale,  in  primo luogo la preventiva conoscenza da    &#13;
 parte dello Stato delle attività che le singole Regioni intendano di    &#13;
 volta  in  volta  promuovere  (quindi,  un  dovere  di   informazione    &#13;
 preventivo  in  capo alle Regioni:   sentenze n. 425 del 1995, n. 204    &#13;
 del 1993, n. 472 del 1992); e, secondariamente,  quale  indefettibile    &#13;
 strumento  di  tutela  dell'esclusività  degli  indirizzi statali di    &#13;
 politica internazionale, che vanno salvaguardati ovviamente prima che    &#13;
 l'attività delle Regioni venga  intrapresa,  la  possibilità  dello    &#13;
 Stato   di  opporre  tempestivamente  il  proprio  motivato  diniego,    &#13;
 peraltro sindacabile da questa Corte in sede di  conflitto  (sentenza    &#13;
 n. 204 del 1993).                                                        &#13;
   L'ipotesi,  avanzata  dalle  Regioni promotrici, che lo Stato debba    &#13;
 far valere solo successivamente e in via repressiva  la  contrarietà    &#13;
 dell'attività    regionale   ai   propri   indirizzi   di   politica    &#13;
 internazionale equivarrebbe a vanificare il principio  costituzionale    &#13;
 di   leale   cooperazione,   sul  quale  si  fondano  sia  l'esigenza    &#13;
 dell'obbligo di informazione preventiva da parte delle  Regioni,  sia    &#13;
 la  possibilità  di  un  preventivo  motivato diniego da parte dello    &#13;
 Stato.                                                                   &#13;
   Poiché la ratio ispiratrice del quesito non è la sostituzione  di    &#13;
 un  modello  di  coordinamento  con  altro diverso ed equivalente dal    &#13;
 punto  di  vista  della  concretizzazione  del  principio  di   leale    &#13;
 cooperazione,  bensì  l'eliminazione  in  radice  di  ogni  forma di    &#13;
 coordinamento  fra  Stato  e  Regioni   in   materia   di   attività    &#13;
 promozionali   all'estero,   si  deve  concludere  che  la  richiesta    &#13;
 referendaria, per il tramite dell'abrogazione delle  parole  "non"  e    &#13;
 "se  non previa intesa con il Governo e nell'ambito degli indirizzi e    &#13;
 degli atti di coordinamento di cui  al  comma  precedente"  contenute    &#13;
 nell'art.  4,  secondo  comma,  del d.P.R. 24 luglio 1977, n. 616, è    &#13;
 tesa a colpire inammissibilmente il principio costituzionale di leale    &#13;
 cooperazione che trova il suo diretto fondamento  nell'art.  5  della    &#13;
 Costituzione.                                                            &#13;
   In   conclusione,  è  qui  operante  il  limite  "gerarchico"  del    &#13;
 referendum abrogativo, reso  esplicito  da  questa  Corte  fin  dalla    &#13;
 sentenza n.  16 del 1978.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile  la richiesta di referendum popolare per la    &#13;
 abrogazione, nelle parti indicate in epigrafe, del d.P.R.  24  luglio    &#13;
 1977,  n.  616 (Attuazione della delega di cui all'art. 1 della legge    &#13;
 22  luglio  1975,  n.  382),  richiesta  dichiarata  legittima,   con    &#13;
 ordinanza  del  26-27  novembre  1996,  dall'Ufficio  centrale per il    &#13;
 referendum costituito presso la Corte di cassazione.                     &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 30 gennaio 1997.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Mezzanotte                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 10 febbraio 1997.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
