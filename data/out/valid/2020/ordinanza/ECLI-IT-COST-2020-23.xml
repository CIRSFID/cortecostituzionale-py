<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-14/23/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-14/23"/>
          <FRBRalias value="ECLI:IT:COST:2020:23" name="ECLI"/>
          <FRBRdate date="2020-02-14" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="23"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-14/23/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-14/23/ita@"/>
          <FRBRdate date="2020-02-14" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-14/23/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-14/23/ita@.xml"/>
          <FRBRdate date="2020-02-14" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-01-16" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-02-14" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>23</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:23</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Giulio Prosperetti</akn4cc:relatore>
        <akn4cc:data_decisione>2020-01-16</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-02-14</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei giudizi di legittimità costituzionale dell'art. 1, comma 2, della legge della Regione Basilicata 28 febbraio 2018, n. 3 (Interventi in materia di continuità assistenziale), degli artt. 1 e 2, nonché dell'intero testo, della legge della Regione Basilicata 27 giugno 2018, n. 10 (Disposizioni in materia sanitaria), e dell'art. 15 della legge della Regione Basilicata 20 agosto 2018, n. 18 (Prima variazione al bilancio di previsione pluriennale 2018/2020), promossi dal Presidente del Consiglio dei ministri con ricorsi notificati il 27 aprile-3 maggio, il 27-30 agosto e il 19-25 ottobre 2018, rispettivamente depositati in cancelleria il 4 maggio, il 31 agosto e il 23 ottobre 2018, iscritti ai nn. 35, 56 e 74 del registro ricorsi 2018 e pubblicati nella Gazzetta Ufficiale della Repubblica nn. 23, 40 e 48, prima serie speciale, dell'anno 2018.
 Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Basilicata;
 udito nella camera di consiglio del 15 gennaio 2020 il Giudice relatore Giulio Prosperetti;
 deliberato nella camera di consiglio del 16 gennaio 2020.
 Ritenuto che, con ricorso notificato il 27 aprile-3 maggio 2018, depositato il 4 maggio 2018 e iscritto al reg. ric. n. 35 del 2018, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale dell'art. 1, comma 2, della legge della Regione Basilicata 28 febbraio 2018, n. 3 (Interventi in materia di continuità assistenziale), in riferimento agli artt. 117, secondo comma, lettera l), e 3 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;
 che, secondo il ricorrente, la disposizione regionale impugnata, nel riconoscere ai medici di continuità assistenziale un compenso orario forfettario per attività ambulatoriali differibili in modo difforme da quanto previsto dall'art. 67 dell'Accordo collettivo nazionale per la disciplina dei rapporti con i medici di medicina generale del 29 luglio 2009, «eccede dalle competenze regionali, e contrasta con l'art. 8, comma l, prima parte, del <ref href="/akn/it/act/decretoLegislativo/stato/1992/502/!main">decreto legislativo n. 502 del 1992</ref>, secondo il quale il rapporto tra il servizio sanitario regionale e i medici e i pediatri è disciplinato da apposite convenzioni di durata triennale, conformi agli accordi collettivi nazionali»;
 che, ad avviso del ricorrente, in tal modo la disposizione impugnata lede la competenza riservata allo Stato dall'art. 117, secondo comma, lettera l), Cost. in materia di «ordinamento civile», alla quale è riconducibile la contrattazione collettiva, e, al contempo, il principio costituzionale di eguaglianza di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, «incidendo sull'esigenza di garantire l'uniformità nel territorio nazionale delle regole fondamentali di diritto che disciplinano i rapporti in questione»;
 che la Regione Basilicata non si è costituita in giudizio;
 che con ricorso notificato il 27-30 agosto 2018, depositato il 31 agosto 2018 e iscritto al reg. ric. n. 56 del 2018, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale degli artt. 1 e 2 della legge della Regione Basilicata 27 giugno 2018, n. 10 (Disposizioni in materia sanitaria), in riferimento agli artt. 117, secondo comma, lettera l), e 3 Cost.;
 che, secondo il ricorrente, l'art. 1 della legge regionale impugnata, nel disciplinare elementi della retribuzione dei medici di continuità assistenziale in modo difforme da quanto previsto dall'art. 67 dell'Accordo collettivo nazionale di settore del 29 luglio 2009, lede la competenza riservata allo Stato dall'art. 117, secondo comma, lettera l), Cost. in materia di «ordinamento civile», alla quale è riconducibile la contrattazione collettiva, nonché l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, in quanto incide sull'esigenza di garantire l'uniformità nel territorio nazionale delle regole fondamentali di diritto che disciplinano i rapporti in questione;
 che dalla illegittimità costituzionale dell'art. l, secondo il ricorrente, consegue l'incostituzionalità dell'art. 2 riguardante le «Procedure per il recupero dei crediti» e dell'intero testo della legge in esame, in quanto composta di soli tre articoli tra loro inscindibilmente connessi;
 che la Regione Basilicata si è costituita in giudizio con atto depositato il 5 ottobre 2018, deducendo l'infondatezza del ricorso e, comunque, chiedendo di dichiarare la cessazione della materia del contendere per effetto delle disposizioni recate dall'art. 15 della legge della Regione Basilicata 20 agosto 2018 n. 18 (Prima variazione al bilancio di previsione pluriennale 2018/2020), sostitutivo dell'impugnato art. 1 della legge reg. Basilicata n. 10 del 2018;
 che, con ricorso notificato il 19-25 ottobre 2018, depositato il 23 ottobre 2018 e iscritto al reg. ric. n. 74 del 2018, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale del citato art. 15 legge reg. Basilicata n. 18 del 2018, in riferimento agli artt. 117, secondo comma, lettera l), e 3 Cost.;
 che, secondo il ricorrente, la disposizione impugnata, nel modificare l'art. 1 della legge reg. Basilicata n. 10 del 2018, già oggetto del ricorso n. 56 del 2018, non consente di ritenere superati i rilievi di incostituzionalità precedentemente riscontrati relativamente alla disposizione modificata, presentando gli stessi profili di criticità già rilevati nei confronti di quest'ultima;
 che, pertanto, la disposizione impugnata lede a sua volta la competenza riservata allo Stato dall'art. 117, secondo comma, lettera l), Cost. in materia di «ordinamento civile» e, al contempo, il principio costituzionale di eguaglianza di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>;
 che la Regione Basilicata si è costituita in giudizio con atto depositato il 15 novembre 2018, chiedendo di dichiarare infondato il ricorso;
 che nel corso dei giudizi è intervenuta l'abrogazione, ad opera dell'art. 15 della legge della Regione Basilicata 13 marzo 2019, n. 4 (Ulteriori disposizioni urgenti in vari settori d'intervento della Regione Basilicata), delle disposizioni rispettivamente oggetto dei ricorsi in esame;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, per effetto della predetta abrogazione delle disposizioni regionali impugnate e del conseguente venir meno delle ragioni che avevano indotto alle rispettive impugnative, il Presidente del Consiglio dei ministri ha dichiarato di rinunciare ai ricorsi con atti depositati, quanto ai ricorsi n. 35 e n. 56 del 2018, in data 11 giugno 2019 e, quanto al ricorso n. 74 del 2018, in data 19 giugno 2019, in conformità delle delibere adottate dal Consiglio dei ministri, quanto ai primi due ricorsi, in data 30 maggio 2019 e, quanto al terzo ricorso, in data 11 giugno 2019;
 che la Regione Basilicata ha accettato le rinunce ai ricorsi con atti depositati il 17 luglio 2019.
 Considerato che i ricorsi promuovono questioni analoghe in riferimento a parametri coincidenti e che pertanto i relativi giudizi vanno riuniti per essere decisi con un unico provvedimento;
 che è intervenuta la rinuncia ai ricorsi da parte del Presidente del Consiglio dei ministri, previa conforme deliberazione del Consiglio dei ministri;
 che tutte le rinunce sono state accettate dalla Regione Basilicata;
 che questa Corte ha chiarito (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/37/!main">sentenza n. 37 del 2016</ref> e <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2017/78/!main">ordinanza n. 78 del 2017</ref>) che il requisito della «previa deliberazione» della Giunta regionale è previsto dall'<ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_32__para_2">art. 32, comma 2, della legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale) solo come presupposto dell'iniziativa della Regione contro una legge statale;
 che tali decisioni si fondano sul principio, di portata generale, per cui le disposizioni che prevedono nullità, preclusioni, inammissibilità e decadenze processuali si intendono assoggettate ad un regime di stretta legalità;
 che, in ragione della sua generalità, il predetto principio è applicabile a tutti gli atti per cui le Norme integrative per i giudizi davanti alla Corte costituzionale e la citata <ref href="/akn/it/act/legge/stato/1953/87/!main">legge n. 87 del 1953</ref> non prescrivono formalità e quindi anche all'accettazione della rinuncia dell'impugnativa proposta dallo Stato;
 che, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'intervenuta rinuncia al ricorso determina l'estinzione del processo: quanto al ricorso n. 35 del 2018, in mancanza della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Basilicata (ex plurim<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2019/243/!main">is, ordinanze n. 243 del 2019</ref> e n. 4 del 2019); quanto ai ricorsi n. 56 e n. 74 del 2018, per effetto dell'intervenuta accettazione da parte della resistente delle rispettive rinunce.
 Visti l'art. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e gli artt. 9, comma 2, e 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 riuniti i giudizi,
 dichiara estinti i processi.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-01-16">16 gennaio 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulioProsperetti" as="#relatore">Giulio PROSPERETTI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-02-14">14 febbraio 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
