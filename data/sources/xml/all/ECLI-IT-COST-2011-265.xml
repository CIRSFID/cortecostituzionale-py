<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2011</anno_pronuncia>
    <numero_pronuncia>265</numero_pronuncia>
    <ecli>ECLI:IT:COST:2011:265</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>QUARANTA</presidente>
    <relatore_pronuncia>Alfonso Quaranta</relatore_pronuncia>
    <redattore_pronuncia>Alfonso Quaranta</redattore_pronuncia>
    <data_decisione>05/10/2011</data_decisione>
    <data_deposito>12/10/2011</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alfonso QUARANTA; Giudici : Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 223, comma 3, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso dal Giudice di pace di Castiglione del Lago nel procedimento vertente tra A. D. e l'U.T.G. di Perugia, con ordinanza del 10 febbraio 2010, iscritta al n. 349 del registro ordinanze 2010 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 46, prima serie speciale, dell'anno 2010.&#13;
 Visto l'atto di intervento del Presidente del Consiglio dei ministri.&#13;
 Udito nella camera di consiglio del 21 settembre 2011 il Giudice relatore Alfonso Quaranta.</epigrafe>
    <testo>Ritenuto che il Giudice di pace di Castiglione del Lago, con l'ordinanza indicata in epigrafe, ha sollevato - in riferimento agli articoli 13, 24, 25 e 27 della Costituzione - questione di legittimità costituzionale dell'articolo 223, comma 3, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), «per due differenti profili»;&#13;
 che, infatti, la norma è censurata «nella parte in cui non prevede che il provvedimento cautelare» della sospensione della patente «cessi comunque il proprio effetto con il superamento favorevole della visita medica obbligatoria» (così come stabilito, invece, dall'art. 187, comma 6, del codice della strada), nonché «nella parte in cui prevede che il Prefetto possa irrogare una sanzione superiore nel suo massimo al minimo previsto dagli artt. 186 e 187» dello stesso codice;&#13;
 che il giudice a quo premette di essere chiamato a decidere del ricorso proposto da un soggetto sanzionato a norma dell'art. 186, commi 2 e 7, del codice della strada, per essersi rifiutato - in occasione di un sinistro stradale in cui era rimasto coinvolto il 7 luglio 2008 - di sottoporsi agli esami volti ad accertare l'assunzione di sostanze alcoliche o psicotrope;&#13;
 che oggetto del giudizio principale è, in particolare, la richiesta di annullamento, previa sospensione, del provvedimento prefettizio di sospensione della patente, adottato il 22 luglio 2008, a norma dell'art. 223, comma 3, del codice della strada;&#13;
 che tanto premesso in punto di fatto, il rimettente - non senza rammentare che la sospensione cautelare de qua, secondo l'univoca giurisprudenza, anche costituzionale (è citata l'ordinanza n. 344 del 2004), non risulta cumulabile con quella irrogata dal giudice penale, a titolo di sanzione accessoria per i reati di cui agli artt. 186 e 187 del codice della strada, sicché «il periodo di sospensione della patente sarà solo quello stabilito dalla sentenza penale, da cui verrà detratto il periodo di sospensione cautelare già scontato» - lamenta, per un verso, che l'autorità prefettizia, allorché «dispone il provvedimento cautelare» ne stabilisce la durata «senza prevederne la cessazione automatica al momento del superamento della visita» medica che «certifica la idoneità del soggetto alla guida dell'autoveicolo»; &#13;
 che l'evenienza da ultimo indicata, a dire del remittente, fa venire meno il «presupposto della misura cautelare, cioè le esigenze di salvaguardia della pubblica incolumità»;&#13;
 che, per altro verso, il giudice a quo si duole del fatto che il Prefetto «è autorizzato ad irrogare un provvedimento cautelare» - che si porrebbe, in realtà, come «un anticipo della sanzione accessoria» - in una misura «pari o superiore al massimo che il giudice penale potrà comminare dopo un procedimento completamente istruito»;&#13;
 che, pertanto, secondo il Giudice di pace rimettente, la norma censurata, da un lato, verrebbe a sottrarre «il trasgressore al suo giudice naturale», privandolo «delle garanzie costituzionali in ordine ad una pena accessoria che, nella odierna vita quotidiana, raffigura un provvedimento di sostanziale limitazione della libertà personale»; &#13;
 che, dall'altro, essa porrebbe il soggetto imputato dei reati di cui agli artt. 186 e 187 del codice della strada, «assolto o condannato ad una sanzione accessoria più limitata nel tempo rispetto alla misura cautelare disposta dal Prefetto» e «completamente scontata al momento della sentenza penale», nella necessità di tutelare le proprie ragioni solo con «la richiesta di risarcimento dei danni allo Stato»;&#13;
 che su tali basi il giudice a quo ha dedotto la violazione degli artt. 13, 24, 25 e 27 della Costituzione;&#13;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto - non senza rilevare, peraltro, che l'art. 223 del codice della strada è stato sostituito dall'art. 43, comma 4, della legge 27 luglio 2010, n. 120 (Disposizioni in materia di sicurezza stradale), sicché, per effetto di tale modifica, l'agente o organo accertatore procede, per tutte le ipotesi di reato che prevedono la sanzione accessoria della sospensione della patente (e non più soltanto per quelle previste dai commi 2 e 3 dell'art. 222 del medesimo codice), al ritiro immediato della patente - che la questione venga dichiarata, per più ragioni, inammissibile o, in subordine, non fondata.&#13;
 Considerato che il Giudice di pace di Castiglione del Lago, con l'ordinanza  indicata in epigrafe, ha sollevato - in riferimento agli articoli 13, 24, 25 e 27 della Costituzione - questione di legittimità costituzionale dell'articolo 223, comma 3, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), «per due differenti profili», in particolare censurandolo «nella parte in cui non prevede che il provvedimento cautelare» della sospensione della patente «cessi comunque il proprio effetto con il superamento favorevole della visita medica obbligatoria» (così come stabilito, invece, dall'art. 187, comma 6, del medesimo codice), nonché «nella parte in cui prevede che il Prefetto possa irrogare una sanzione superiore nel suo massimo al minimo previsto dagli artt. 186 e 187» dello stesso codice;&#13;
 che a prescindere dall'intervenuta modifica, successivamente all'adozione dell'ordinanza di rimessione, della norma censurata, la questione in esame - come rilevato dall'Avvocatura generale dello Stato - risulta sotto più profili manifestamente inammissibile;&#13;
 che, peraltro, il dubbio sul carattere ancipite del petitum dell'ordinanza di rimessione - atteso che il giudice a quo ha chiesto, ad un tempo, un intervento "additivo" ed uno "ablatorio" sul testo della norma censurata, senza che sia dato comprendere se le due richieste siano in un rapporto di alternatività irrisolta o di subordinazione - già sembrerebbe comportare l'inammissibilità della questione sollevata (ex multis, sentenza n. 355 del 2010, ordinanze numeri 230 e 98 del 2009);&#13;
 che a tale esito conduce, comunque, la constatazione che l'ordinanza di rimessione difetta di una motivazione non meramente assertiva in ordine alle ragioni del contrasto della norma censurata con gli artt. 13, 24, 25 e 27 Cost. e dunque in ordine alla sua non manifesta infondatezza (da ultimo, ordinanze numeri 203 e 180 del 2011).&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 223, comma 3, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata - in riferimento agli articoli 13, 24, 25 e 27 della Costituzione - dal Giudice di pace di Castiglione del Lago con l'ordinanza indicata in epigrafe. &#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 ottobre 2011.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente e Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 12 ottobre 2011.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
