<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>176</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:176</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>19/05/2003</data_decisione>
    <data_deposito>23/05/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'articolo 8, comma 5, della legge della Regione Lazio 12 settembre 1994, n. 39 (Individuazione delle strutture organizzative degli Istituti per il diritto allo studio universitario - IIDiSU del Lazio e determinazione dell'organico del ruolo del personale degli istituti), promossi con due ordinanze del 19 febbraio 2001 e due ordinanze del 5 marzo 2001, dal TAR del Lazio, rispettivamente iscritte ai nn. 127, 128, 129 e 130 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 13, prima serie speciale, dell'anno 2002. &#13;
      Visti gli atti di costituzione di Castaldi Luciano, di Minchella Angelino e della Regione Lazio; &#13;
      udito nella camera di consiglio del 26 febbraio 2003 il Giudice relatore Ugo De Siervo. &#13;
    Ritenuto che, con quattro distinte ordinanze di contenuto pressoché identico, emesse in data 19 febbraio e 5 marzo 2002 e pervenute tutte il 1° marzo 2002, nell'ambito di altrettanti processi, il Tribunale amministrativo regionale del Lazio ha sollevato - in riferimento agli artt. 3, 36 primo comma, e 97, primo comma, della Costituzione - questione di legittimità costituzionale dell'art. 8, comma 5, della legge della Regione Lazio 12 settembre 1994, n. 39 (Individuazione delle strutture organizzative degli Istituti per il diritto allo studio universitario - IIDiSU del Lazio e determinazione dell'organico del ruolo del personale degli istituti); &#13;
    che la questione è sorta nell'ambito di procedimenti nei quali i ricorrenti, tutti dipendenti dell'Istituto per il diritto allo studio universitario (IIDiSU), e destinatari della legge regionale n. 39 del 1994, hanno chiesto il riconoscimento del diritto a percepire i benefici economici derivanti dal loro reinquadramento nei ruoli della Regione Lazio, ai sensi della citata legge, a decorrere dal 1° febbraio 1981, con interessi e rivalutazione monetaria, previo eventuale annullamento delle deliberazioni della Giunta regionale, emesse tutte il 19 aprile 1995, nella parte in cui fissano la decorrenza del loro inquadramento economico dal 5 ottobre 1994, data di entrata in vigore della legge n. 39 del 1994, anziché dal 1° febbraio 1981; &#13;
    che ad avviso del remittente, all'accoglimento di tale istanza sarebbe di ostacolo l'art. 8, comma 5, della legge regionale censurata che prevede espressamente che, mentre gli effetti giuridici del reinquadramento dei dipendenti decorrono dal 1° febbraio 1981, gli effetti economici decorrono invece dalla data di entrata in vigore della legge n. 39 del 1994, e cioè dal 5 ottobre 1994; &#13;
    che il giudice a quo, dopo avere ricostruito le vicende normative che hanno interessato i ricorrenti, ritiene di dover sollevare la questione di legittimità costituzionale dell'art. 8, comma 5, della legge regionale n. 39 del 1994 in quanto tale norma contrasterebbe con l'art. 3 della Costituzione ed il principio di parità dei cittadini "essendo diversamente trattate nella disposizione in parola situazioni riferite a dipendenti regionali, nella sostanza, uguali e permanendo comunque il contrasto tra le esigenze di perequazione” poste a base della legge regionale, e il rimedio da essa previsto che invece procrastina la situazione di disuguaglianza del trattamento economico di dipendenti dello stesso Ente; &#13;
    che la norma censurata violerebbe inoltre l'art. 36 della Costituzione, in quanto la maggiore retribuzione, proporzionale alla maggiore professionalità riconosciuta, non verrebbe fatta decorrere dalla data del reinquadramento giuridico (e del conseguente riconoscimento di tale maggiore professionalità), ma da un momento successivo, con ciò concretamente incidendo sul diritto del lavoratore a percepire una retribuzione proporzionata alla qualità e quantità del suo lavoro; &#13;
    che infine l'art. 8, comma 5, della legge regionale n. 39 del 1994 contrasterebbe con l'art. 97 della Costituzione in quanto la situazione discriminatoria che tale legge lascerebbe sussistere sarebbe sintomatica di una non corretta ed imparziale amministrazione; &#13;
    che, in punto di rilevanza, osserva il rimettente, le richieste dei ricorrenti non potrebbero essere accolte, non potendo il TAR disapplicare provvedimenti legislativi; &#13;
    che sono intervenuti taluni dei ricorrenti nei giudizi a quibus i quali hanno chiesto l'accoglimento della questione sollevata dal TAR Lazio; &#13;
    che, inoltre, è intervenuta la Regione Lazio che ha chiesto di dichiarare non fondata la questione prospettata dal giudice amministrativo, in quanto il diverso trattamento spettante ai dipendenti del ruolo IIDiSU corrisponderebbe ad una oggettiva diversità delle loro condizioni rispetto a quelle degli altri dipendenti regionali, e comunque rientrerebbe nella discrezionalità del legislatore individuare le modalità di perequazione del trattamento del personale, tenuto conto anche delle concrete disponibilità finanziarie; &#13;
    che ad avviso della difesa erariale,  neppure vi sarebbe violazione del principio della "retribuzione sufficiente” sancito dall'art. 36 della Costituzione, non potendosi stabilire un raffronto tra prestazioni svolte da impiegati regionali per amministrazioni distinte; &#13;
    che, infine, inesistente sarebbe anche la violazione dell'art. 97 della Costituzione, non essendovi un'irragionevole discriminazione tra categorie di personale tale da turbare il buon andamento della amministrazione della Regione. &#13;
    Considerato che l'identità delle questioni sollevate rende possibile procedere alla riunione dei giudizi; &#13;
    che il giudice a quo dubita della legittimità costituzionale, in riferimento agli artt. 3, 36, primo comma, e 97, primo comma, della Costituzione, dell'art. 8, comma 5, della legge della Regione Lazio 12 settembre 1994, n. 39 (Individuazione delle strutture organizzative degli Istituti per il diritto allo studio universitario - IIDiSU del Lazio e determinazione dell'organico del ruolo del personale degli istituti), nella parte in cui dispone che il reinquadramento nei ruoli regionali del personale dell'Istituto per il diritto alla studio universitario, decorre agli effetti giuridici dal 1° febbraio 1981 e agli effetti economici dal 5 ottobre 1994; &#13;
    che questa Corte, con ordinanza n. 241 del 2002, si è già pronunciata sulla medesima questione, prospettata dallo stesso rimettente, dichiarandone la manifesta infondatezza; &#13;
    che le ordinanze di rimessione, emesse in data anteriore alla decisione sopra richiamata, non contengono diverse ed ulteriori argomentazioni che possano condurre questa Corte a differenti conclusioni; &#13;
    che pertanto le questioni devono essere dichiarate manifestamente infondate. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 8, comma 5, della legge della Regione Lazio 12 settembre 1994, n. 39 (Individuazione delle strutture organizzative degli Istituti per il diritto allo studio universitario - IIDiSU del Lazio e determinazione dell'organico del ruolo del personale degli istituti), sollevate, in riferimento agli artt. 3, 36, primo comma, e 97, primo comma,  della Costituzione, dal Tribunale amministrativo regionale del Lazio con le ordinanze indicate in epigrafe. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  il 19 maggio 2003. &#13;
F.to: &#13;
Riccardo CHIEPPA, Presidente &#13;
Ugo DE SIERVO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 23 maggio 2003. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
