<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>71</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:71</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/04/1981</data_decisione>
    <data_deposito>26/05/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Avv. ORONZO REALE &#13;
 - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - &#13;
 Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale dell'art. 270  &#13;
 R.D.23 maggio 1924,  n.  827  (Regolamento  per  l'amministrazione  del  &#13;
 patrimonio e per la contabilità generale dello Stato) e dell'art.  429  &#13;
 cod.  proc.  civ., nel testo modificato dalla  legge 11 agosto 1973, n.  &#13;
 533, promossi con le seguenti  ordinanze:                                &#13;
     1. - Ordinanza emessa il 28 maggio 1975 dalla Corte di  appello  di  &#13;
 Caltanissetta nel procedimento civile  vertente tra Salerno Rosolino ed  &#13;
 il  Comune di Caltanissetta,  iscritta al n. 350 del registro ordinanze  &#13;
 1975 e pubblicata nella  Gazzetta Ufficiale della Repubblica n. 268 del  &#13;
 1975;                                                                    &#13;
     2. - Ordinanza emessa il 23 luglio 1976 dal  Tribunale  di  Catania  &#13;
 nel   procedimento   civile  vertente  tra     l'Assessorato  Regionale  &#13;
 dell'Agricoltura e delle Foreste della   Sicilia e  Di  Marzo  Nicolò,  &#13;
 iscritta  al  n.  707  del registro ordinanze   1976 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n.  17 del 1977;                     &#13;
     3. - Ordinanza emessa il 20 dicembre 1976 dal Pretore di Palmi  nel  &#13;
 procedimento civile vertente tra Arduca  Antonino ed altri e l'Ospedale  &#13;
 Civile  di  Palmi,  iscritta  al n. 111   del registro ordinanze 1977 e  &#13;
 pubblicata nella Gazzetta  Ufficiale della Repubblica n. 107 del 1977.   &#13;
     Visti gli atti di intervento del  Presidente  del  Consiglio    dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  10  dicembre  1980  il  Giudice  &#13;
 relatore Antonio La Pergola;                                             &#13;
     udito l'avvocato dello  Stato  Giuseppe  Angelini  Rota,  per    il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con  ordinanza emessa il 28 maggio 1975 nel  procedimento di  &#13;
 lavoro vertente  fra  il  Comune  di    Caltanissetta  e  tale  Salerno  &#13;
 Rosolino,  la  Corte  di  appello di   Caltanissetta ha denunziato come  &#13;
 costituzionalmente illegittimo,    in  quanto  rilevante  ai  fini  del  &#13;
 decidere,  il corpo di norme  contenute nel Regolamento di contabilità  &#13;
 generale dello  Stato (R.D. 23 maggio 1924, n. 827)  e  in  particolare  &#13;
 l'art.  270 censurando d'altra parte la mancata estensione alla  specie  &#13;
 delle  disposizioni introdotte nel c.p.c. con la legge  11 agosto 1973,  &#13;
 n. 533, e in particolare l'art. 429, terzo  comma,  c.p.c.,  nel  nuovo  &#13;
 testo.                                                                   &#13;
     Ai  sensi, appunto, della norma per ultimo citata il  Salerno aveva  &#13;
 chiesto alla Corte di appello la  rivalutazione monetaria  del  credito  &#13;
 da  lui  vantato nei  confronti del Comune, già accertato con sentenza  &#13;
 non  definitiva dello stesso giudice a quo.  L'ente  convenuto    aveva  &#13;
 opposto,   ex   art.   270  del  citato  Regolamento  di  contabilità,  &#13;
 l'infondatezza della pretesa, deducendo che i   crediti  pecuniari  nei  &#13;
 confronti  dello  Stato  e  degli enti pubblici non economici diventano  &#13;
 liquidi ed esigibili solo con l'emissione del relativo titolo di  spesa  &#13;
 da  parte  dell'amministrazione:  prima  di quel momento non vi sarebbe  &#13;
 mora  dell'ente  debitore  né  dunque  obbligo  di  corrispondere  gli  &#13;
 interessi,   restando   con   ciò   esclusa  ogni     possibilità  di  &#13;
 rivalutazione monetaria del credito. Tale principio, si soggiunge,  non  &#13;
 trova applicazione nel solo  caso in cui il debito dell'amministrazione  &#13;
 sia  stato    accertato  con sentenza passata in giudicato, e così non  &#13;
 possa più contestarsi che il credito della  controparte  sia    certo,  &#13;
 liquido  ed  esigibile.  Dove  invece,  esso si applica -   e così nel  &#13;
 nostro caso - si avrebbe la violazione di più di   un  precetto  della  &#13;
 Carta fondamentale: in primo luogo,  dell'art.  3. La norma che abilita  &#13;
 lo  Stato  o l'ente pubblico   non economico a scegliere, liberamente e  &#13;
 senza  controllo, il momento in cui il proprio credito pecuniario    è  &#13;
 reso liquido ed esigibile, deroga, si dice, all'art. 1282  del c.c., in  &#13;
 forza del quale il credito liquido ed esigibile  di una somma di denaro  &#13;
 produce  interessi  di  pieno  diritto.  Ora,  siffatta deroga, fondata  &#13;
 esclusivamente  sulla    qualità  del  debitore,   implicherebbe   una  &#13;
 ingiustificata  disparità di trattamento dei creditori, secondo che il  &#13;
 loro    rapporto  intercorra  con  lo  Stato,  o  con gli enti pubblici  &#13;
 previsti da detto Regolamento di contabilità, ovvero   intercorra  con  &#13;
 altri  enti  o  soggetti. Quando, poi, si tratti  di crediti di lavoro,  &#13;
 oltre alla  denunziata  disuguaglianza    di  disciplina,  verrebbe  in  &#13;
 rilievo l'inosservanza degli artt.  35 e 36 Cost. Si assume infatti che  &#13;
 siano  inestensibili  alla   specie le garanzie di ordine sostanziale e  &#13;
 processuale introdotte con la legge  n.  533  del  1973  a  favore  dei  &#13;
 dipendenti  di  persona  giuridica o fisica diversa dallo Stato o dagli  &#13;
 enti  pubblici non economici. Anche per questa  considerazione, dunque,  &#13;
 si imporrebbe  il  diniego  degli    interessi  corrispettivi  e  della  &#13;
 rivalutazione  monetaria  dei    crediti di lavoro, con il risultato di  &#13;
 privare i dipendenti  dello Stato e degli anzidetti enti  pubblici  non  &#13;
 economici      della   giusta   retribuzione,  loro  costituzionalmente  &#13;
 garantita.  All'ente  debitore  sarebbe  d'altra  parte  concesso    il  &#13;
 vantaggio  di  fruire ingiustificatamente della somma di  denaro dovuta  &#13;
 al creditore, spesso per lungo tempo e  senza spese, e di assolvere poi  &#13;
 i propri debiti con moneta  che ha valore svilito.                       &#13;
     L'Avvocatura  dello  Stato,  intervenuta  in  rappresentanza    del  &#13;
 Presidente  del  Consiglio,  deduce  che  il  citato    Regolamento  di  &#13;
 contabilità,  nel  disciplinare     compiutamente   l'ordinazione   ed  &#13;
 erogazione  della  spesa   pubblica deroga legittimamente il regime del  &#13;
 codice  civile, come è posto non solo dal citato art.  1282  ma  anche  &#13;
 dall'art.  1224,  ai sensi del quale "nelle obbligazioni  che hanno per  &#13;
 oggetto una somma di denaro sono dovuti   dal  giorno  della  mora  gli  &#13;
 interessi  legali". La normativa  censurata, si soggiunge, come vincola  &#13;
 l'ente pubblico   all'osservanza di un certo  procedimento,  così  non  &#13;
 può    non  vincolare  i creditori dell'ente medesimo; essa   diverge,  &#13;
 certo, dalle regole che governano i rapporti  scaturenti dall'autonomia  &#13;
 privata ma non costituisce per questo alcuna prerogativa  od  esenzione  &#13;
 soggettiva    dell'ente  pubblico  dai  normali  obblighi  gravanti sul  &#13;
 debitore,  che  risponde  alla   oggettiva   necessità   di   regolare  &#13;
 l'attività    svolta    dall'ente    pubblico   nell'interesse   della  &#13;
 collettività, ed in conformità del principio, sancito   dall'art.  97  &#13;
 della   Carta   fondamentale,   del   buon   andamento  della  pubblica  &#13;
 amministrazione. Il  Regolamento di contabilità - viene anzi dedotto -  &#13;
 non  allarga, bensì limita l'autonomia degli enti pubblici, in   guisa  &#13;
 da  ridurre  il  rapporto obbligatorio sotto la   disciplina alla quale  &#13;
 ente pubblico e soggetto privato   soggiacciono  ad  egual  titolo.  La  &#13;
 deroga  dai  principi  del codice civile, che discende dalla normazione  &#13;
 censurata, troverebbe allora un'idonea  giustificazione  costituzionale  &#13;
 proprio  nell'adempimento  del  richiamato  precetto dell'art.   97. Ne  &#13;
 seguirebbe che non sussiste la lamentata    violazione  del  canone  di  &#13;
 eguaglianza;  e  d'altra parte non   sarebbero nemmeno offesi gli artt.  &#13;
 35 e 36 Cost., che   tutelano  il  lavoro  e  il  diritto  alla  giusta  &#13;
 retribuzione.  Le norme  impugnate, si assume, incidono sui sottostanti  &#13;
 rapporti  solo    relativamente  a  situazioni  accessorie  e  sono  in  &#13;
 definitiva    volte  a  garantire  l'adempimento  del  debito  da parte  &#13;
 dell'ente pubblico; e così si conclude che la posizione del creditore,  &#13;
 tanto più dove questi è lavoratore subordinato,   non  è  vulnerata,  &#13;
 sibbene sostanzialmente rafforzata.                                      &#13;
     2.  -  Con  ordinanza emessa il 20 dicembre del 1976 il  Pretore di  &#13;
 Palmi, quale giudice del lavoro, nel  procedimento civile vertente  tra  &#13;
 Arduca  Antonino  ed  altri    e l'ospedale civile di Palmi, sollevava,  &#13;
 ritenendola   rilevante e non manifestamente  infondata,  questione  di  &#13;
 legittimità costituzionale:                                             &#13;
     a)  dell'art.429,  terzo  comma, codice di procedura civile,  nella  &#13;
 parte in cui la norma censurata non deroga, per i   crediti di  lavoro,  &#13;
 al  principio  stabilito  dall'art.  270  R.D.   23 maggio 1924, n. 827  &#13;
 (erroneamente indicato come   R.D. n. 747 del  1923),  e  non  consente  &#13;
 pertanto  la  rivalutazione monetaria di detti crediti dal giorno della  &#13;
 maturazione del relativo diritto;                                        &#13;
     b) dello stesso articolo 270 del R.D. 827 del 1924.                  &#13;
     Ottanta  dipendenti  avevano  adito  il  giudice  a   quo   perché  &#13;
 l'ospedale civile di Palmi venisse condannato al  pagamento delle somme  &#13;
 loro   dovute   a   titolo   di      retribuzione,   e  già  liquidate  &#13;
 dall'amministrazione.  Essi    chiedevano,  peraltro,  la  condanna  al  &#13;
 pagamento degli  interessi legali e al risarcimento del danno derivante  &#13;
 dalla  svalutazione monetaria.                                           &#13;
     L'ospedale  eccepiva,  in  rito,  il  difetto  di giurisdizione del  &#13;
 giudice ordinario e, nel merito, opponeva che gli interessi  richiesti,  &#13;
 se  configurati  come corrispettivi, non dovevano  ritenersi dovuti per  &#13;
 la mancata emissione del relativo   ordine di pagamento,  e  se  invece  &#13;
 configurati  come    moratori, dovevano ritenersi decorsi dalla data di  &#13;
 ciascuna delibera di liquidazione, invece che dal dies a  quo  indicato  &#13;
 dagli  attori;  opponeva,  inoltre, la   impossibilità di applicare ai  &#13;
 rapporti di lavoro aventi per  parti gli enti pubblici non economici il  &#13;
 disposto  dell'art.  429 c.p.c., terzo comma, nuovo testo, che concerne  &#13;
 la   rivalutazione dei  crediti.  Il  Pretore  di  Palmi  riteneva  con  &#13;
 sentenza  non  definitiva  la  propria giurisdizione, trattandosi nella  &#13;
 specie di crediti di lavoro già riconosciuti  dall'amministrazione,  e  &#13;
 tuttavia   condannava   l'ospedale   al   pagamento  del  solo  credito  &#13;
 principale. Si deduce infatti nell'ordinanza di rinvio  che  il  citato  &#13;
 art.  429, terzo  comma, c.p.c., risulta, in forza dell'art. 409, nuovo  &#13;
 testo,    c.p.c.,  inapplicabile  alle  controversie  devolute  in  via  &#13;
 generale  al  giudice amministrativo, nulla importando che  in deroga a  &#13;
 tale regola sussista nella specie la  competenza del giudice ordinario.  &#13;
 Inoltre, l'art. 270 del  R.D. n.  827 del 1924, come  è  inteso  dalla  &#13;
 prevalente    giurisprudenza,  stabilirebbe che i debiti della pubblica  &#13;
 amministrazione - e, così, secondo il Consiglio di Stato,  degli  enti  &#13;
 pubblici  ospedalieri  - diventano esigibili solo  dopo l'emissione del  &#13;
 mandato  di  pagamento  con  la      conseguenza   di   precludere   la  &#13;
 rivalutabilità dei crediti  verso l'ente debitore.                      &#13;
     Vero  è  -  osserva  il  giudice  a  quo  -  che  qui  vengono  in  &#13;
 considerazione  interessi  moratori,  mentre  la    giurisprudenza  del  &#13;
 Consiglio  di  Stato  avrebbe  accolto  la    suddetta  interpretazione  &#13;
 dell'art. 270 del Regolamento di   contabilità  con  riferimento  agli  &#13;
 interessi  corrispettivi. Ma   non è men vero che altra giurisprudenza  &#13;
 vorrebbe  estendere il principio in questione agli interessi  moratori;  &#13;
 e  del  resto,  la  stessa  distinzione tra interessi corrispettivi   e  &#13;
 moratori sarebbe ormai revocata  in  dubbio  dalla  migliore  dottrina,  &#13;
 dovendosi  ravvisare  una funzione   risarcitoria anche negli interessi  &#13;
 corrispettivi.  A  parte  ciò,     l'art.  270  del   Regolamento   di  &#13;
 contabilità derogherebbe al  principio sancito nell'art. 1219, Cpv. n.  &#13;
 3,  del codice   civile, ai sensi del quale la costituzione in mora non  &#13;
 è   necessaria  quando  è  scaduto  il  termine  per    l'adempimento  &#13;
 dell'obbligazione.  Il Pretore di Palmi denunzia quindi, in riferimento  &#13;
 agli  artt.  3,  35  e  36  Cost.,  un'ingiustificata   disparità   di  &#13;
 trattamento  a  carico  dei   lavoratori dipendenti dallo Stato e dagli  &#13;
 anzidetti enti   pubblici. La deroga  agli  artt.  1219  e  1282  c.c.,  &#13;
 disposta      esclusivamente   in  considerazione  della  qualità  del  &#13;
 debitore, implicherebbe, si deduce, un'illegittima  discriminazione tra  &#13;
 i creditori, secondo che il loro  rapporto intercorra con enti soggetti  &#13;
 alla disciplina di cui all'art. 270 del decreto  n.  827  del  1924,  o  &#13;
 viceversa   intercorra con altri soggetti, e la discriminazione sarebbe  &#13;
 poi tanto più grave ed evidente in quanto essa afferisce a  crediti di  &#13;
 lavoro. Nell'atto di intervento l'Avvocatura  dello Stato  richiama  le  &#13;
 deduzioni  esposte  in riferimento alla   medesima questione, sollevata  &#13;
 dalla Corte d'appello di Caltanissetta.                                  &#13;
     3. - Il Tribunale di Catania, con ordinanza  emessa  il  23  luglio  &#13;
 1976,  nel  procedimento  civile vertente tra   l'Assessorato Regionale  &#13;
 dell'Agricoltura e Foreste e Di Marzo Nicolò, si è posto d'ufficio il  &#13;
 problema  della   legittimità   costituzionale   dell'art.   270   del  &#13;
 Regolamento  di  contabilità generale  dello Stato, approvato con R.D.  &#13;
 23 maggio 1924, n. 827. Tale norma prescrive che le spese  dello  Stato  &#13;
 (e  dell'ente    pubblico  non economico) passino attraverso distinte e  &#13;
 successive  fasi:  impegno, liquidazione, ordinazione e  pagamento.  Il  &#13;
 procedimento così configurato, prosegue il giudice a quo,  si conclude  &#13;
 con  l'emissione  del  titolo  di  spesa, spesso a notevole distanza di  &#13;
 tempo dall'ordinazione. La normativa testé descritta divergerebbe  dal  &#13;
 principio   sancito   nell'art.  1282  del     codice  civile,  essendo  &#13;
 giurisprudenza  pacifica  che gli interessi moratori decorrano a favore  &#13;
 dei creditori della P.A. solo  dall'emissione del mandato di pagamento.  &#13;
 La questione sarebbe ictu    oculi  rilevante.    Nel  caso  sottoposto  &#13;
 all'esame  del  Tribunale di  Catania vengono infatti in considerazione  &#13;
 interessi    maturati  sul  canone  locatizio  dovuto   al   Di   Marzo  &#13;
 dall'Assessorato    appellante,  a  far  tempo dalla scadenza e fino al  &#13;
 giorno del  pagamento del canone stesso;  interessi  moratori,  dunque,  &#13;
 che  la  P.A.  dovrebbe  corrispondere  se  il  citato  art.  270 fosse  &#13;
 dichiarato incostituzionale, e non nella ipotesi opposta.                &#13;
     Si deduce, poi, che la normativa censurata confligga con i seguenti  &#13;
 precetti della Costituzione:                                             &#13;
     a) l'art. 3, per la illegittima situazione di privilegio  riservata  &#13;
 alla  P.A.  -  arbitra  di  effettuare  il  pagamento  "in  esito  alla  &#13;
 situazione generale di cassa e alla effettiva  disponibilità di fondi"  &#13;
 - rispetto ai creditori privati.   Siffatta deroga  del  comune  regime  &#13;
 degli  interessi  sarebbe ingiustificata  nel nostro caso, in cui viene  &#13;
 in rilievo un  rapporto  costituito  jure  privatorum.  La  natura  del  &#13;
 diritto  di credito non  muta, si deduce, per il fatto che debitore sia  &#13;
 un  ente  pubblico,  invece  che  un   soggetto   privato,   ed   esige  &#13;
 quell'eguaglianza   di   trattamento,   della   quale  si  denunzia  la  &#13;
 violazione.                                                              &#13;
     b) Gli artt. 24 e 113. Al privato spetterebbe un diritto  perfetto,  &#13;
 e  tuttavia  privo di tutela, in quanto esigibile  solo con l'emissione  &#13;
 del mandato di pagamento. Poiché non si  può escludere l'eventualità  &#13;
 che  la  P.A.  manchi  addirittura     di  emettere  il   mandato,   si  &#13;
 subordinerebbe per questa via  la realizzazione del diritto del privato  &#13;
 ad   una  condizione    sospensiva,  che  si  atteggia  come  meramente  &#13;
 potestativa.  In ogni caso, al creditore sarebbe precluso di  agire  in  &#13;
 executivis,  finché  il  suo  diritto  rimanga  inesigibile secondo la  &#13;
 censurata norma di contabilità. La tutela della situazione  giuridica,  &#13;
 pienamente   garantita   al   creditore      dagli   invocati  precetti  &#13;
 costituzionali, verrebbe così  esclusa nel processo  di  esecuzione  e  &#13;
 "di fatto vanificata".                                                   &#13;
     c)  L'art.  97.  L'osservanza del principio di buon  andamento e di  &#13;
 imparzialità dell'amministrazione,   sancito in detto  articolo  della  &#13;
 Carta  fondamentale,   sarebbe compromessa dal ritardo nel pagamento da  &#13;
 parte  degli enti pubblici e per converso garantita se la P.A.    fosse  &#13;
 tenuta  a  corrispondere  gli  interessi  moratori, giacché, in questo  &#13;
 caso, l'ente pubblico sarebbe indotto    ad  adempiere  puntualmente  i  &#13;
 propri debiti.                                                           &#13;
     Si osserva, infine, che il Regolamento di contabilità non  è atto  &#13;
 amministrativo,  ma  atto  avente  forza di legge. In   particolare, si  &#13;
 annette all'art. 270, che forma oggetto  di    specifica  impugnazione,  &#13;
 l'efficacia,  caratteristica  della    norma  speciale,  di regolare il  &#13;
 presente caso in deroga al  generale disposto dell'art. 1282 del codice  &#13;
 civile.  Spetterebbe, quindi, alla Corte di conoscere  della  questione  &#13;
 di costituzionalità che concerne la norma  impugnata; al giudice a quo  &#13;
 sarebbe  solo  concesso di  delibarne, per parte sua, la rilevanza e la  &#13;
 non manifesta  infondatezza.                                             &#13;
     Si  è  costituito  in  giudizio  il  Presidente   del   Consiglio,  &#13;
 rappresentato  e  difeso  dall'Avvocatura  generale  dello   Stato, per  &#13;
 dedurre l'infondatezza della questione.   Nell'atto  di  intervento  si  &#13;
 svolgono le stesse deduzioni  sopra riferite con riferimento alle altre  &#13;
 ordinanze  di   rimessione. Si aggiunge poi che non sussiste il preteso  &#13;
 contrasto  con  gli  artt.  24  e 113 della Costituzione. Nella specie,  &#13;
 assume l'Avvocatura, non si tratta di una norma processuale che  limiti  &#13;
 il  diritto di difesa o di azione bensì  di una norma sostanziale, che  &#13;
 afferisce al contenuto del  singolo diritto, regolarmente assistito, in  &#13;
 tali limiti di  sostanza, "dalle normali garanzie giurisdizionali".      &#13;
     Il procedimento discusso all'udienza del 13 febbraio   1980  veniva  &#13;
 rinviato  a  nuovo  ruolo  con ordinanza n. 145   del 1980 e ridiscusso  &#13;
 all'udienza del 10 dicembre 1980.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - I giudizi promossi con le ordinanze in epigrafe  possono, data  &#13;
 la sostanziale identità della questione,  essere riuniti e decisi  con  &#13;
 unica sentenza.                                                          &#13;
     2.   -   Viene  sotto  vario  riguardo  dedotta  la  illegittimità  &#13;
 costituzionale dell'art. 429, terzo comma, nuovo testo,  del codice  di  &#13;
 procedura  civile  nonché  dell'art. 270 R.D.  23 maggio 1924, n.  827  &#13;
 (Regolamento  per     l'amministrazione  del  patrimonio   e   per   la  &#13;
 contabilità  generale dello Stato). La prima delle citate disposizioni  &#13;
 è impugnata dalla Corte di appello di Caltanissetta e dal  Pretore  di  &#13;
 Palmi;  la  seconda,  oltre che dai suddetti  giudici, dal Tribunale di  &#13;
 Catania. La questione è così  prospettata alla Corte:                  &#13;
     a) l'art. 429, terzo comma, c.p.c., censurato in riferimento   agli  &#13;
 artt.  3,  35,  primo  comma, e 36 Cost., dispone: "il  giudice, quando  &#13;
 pronuncia sentenza di condanna al   pagamento di somme  di  denaro  per  &#13;
 crediti di lavoro,  deve determinare, oltre agli interessi nella misura  &#13;
 legale,    il maggior danno eventualmente subito dal lavoratore per  la  &#13;
 diminuzione di valore del suo credito condannando al   pagamento  della  &#13;
 somma  relativa  con  decorrenza  dal    giorno  della  maturazione del  &#13;
 diritto". La lesione dei suddetti precetti costituzionali sussisterebbe  &#13;
 in quanto si assume, per un duplice ordine di  considerazioni,  che  la  &#13;
 disposizione  in esame sia inestensibile alla specie. Si deduce, per un  &#13;
 verso, che  le  controversie  dalle  quali  trae  origine  il  presente  &#13;
 giudizio riguardino crediti di lavoro nei confronti di enti pubblici: i  &#13;
 quali ultimi sono, in virtù delle  espresse statuizioni dell'art. 409,  &#13;
 cpv.  nn.  4 e 5,  del codice di procedura civile, sottratti all'intera  &#13;
 disciplina  delle controversie individuali di lavoro  in  detto  codice  &#13;
 introdotta  con legge 11 agosto 1973, n. 533, e così anche al disposto  &#13;
 dell'art.  429 c.p.c. Per altro verso, l'estensione al  caso  in  esame  &#13;
 della  norma or ora citata sarebbe preclusa ex art. 270 del Regolamento  &#13;
 di contabilità generale dello Stato.  Quest'ultima  disposizione,  che  &#13;
 prevede  distinte e successive fasi  del procedimento di ordinazione ed  &#13;
 erogazione  della  spesa  pubblica,  sarebbe  intesa  dalla  prevalente  &#13;
 giurisprudenza  nel  senso che il debito della pubblica amministrazione  &#13;
 diviene esigibile solo con l'emissione del  mandato  di  pagamento,  al  &#13;
 termine dell'anzidetta serie procedimentale. Si assume dunque che, tale  &#13;
 essendo la norma regolatrice della specie, nessun limite sia posto alla  &#13;
 sua  applicazione  dall'art.  429,  terzo  comma,  c.p.c.  La censurata  &#13;
 disposizione del  Regolamento  di  contabilità  derogherebbe,  d'altra  &#13;
 parte,  alla  disciplina  dettata  dal  codice civile con riguardo agli  &#13;
 interessi nelle   obbligazioni pecuniarie (art. 1282,  primo  comma)  e  &#13;
 alla costituzione del  debitore in mora (art. 1219 cpv. n. 3). Con ciò  &#13;
 resterebbe  escluso  che l'ente pubblico sia tenuto alla corresponsione  &#13;
 degli interessi moratori e del maggior danno derivante  al    creditore  &#13;
 dalla  svalutazione monetaria; l'ente pubblico sarebbe allora assistito  &#13;
 dall'ingiustificato  privilegio  di  determinare  discrezionalmente  il  &#13;
 momento  in  cui rendere esigibile il proprio  debito: ne discenderebbe  &#13;
 la denunziata disparità di trattamento tra la pubblica amministrazione  &#13;
 e   gli   altri   debitori    e,    correlativamente,    un'illegittima  &#13;
 discriminazione  tra  i  titolari  dei crediti di lavoro, secondo se il  &#13;
 loro rapporto intercorra con lo Stato (comunque, con un ente pubblico a  &#13;
 cui si applichi il Regolamento di contabilità) o  invece  con  diverso  &#13;
 soggetto.  Di  qui l'ulteriore conseguenza che - trattandosi nel nostro  &#13;
 caso    di  crediti  vantati  da  dipendenti   di   enti   pubblici   -  &#13;
 risulterebbero    lesi i principi stabiliti negli artt. 35 e 36 Cost. a  &#13;
 tutela del lavoro e della giusta retribuzione dei lavoratori.            &#13;
     b) Forma oggetto del giudizio promosso dal Tribunale  di Catania il  &#13;
 solo art. 270 del Regolamento di  contabilità.   La specie  sottoposta  &#13;
 all'esame di quel giudice riguarda,  infatti,  un rapporto di locazione  &#13;
 fra  l'Assessorato Regionale   dell'Agricoltura e foreste e un privato.  &#13;
 La disposizione che si   censura   precluderebbe,  com'è  interpretata  &#13;
 dalla  giurisprudenza,    l'insorgenza  della  mora,  e  il conseguente  &#13;
 riconoscimento   degli interessi  maturati  sul  canone  locatizio  dal  &#13;
 giorno  della    scadenza  a  quello in cui l'Assessorato appellante ha  &#13;
 provveduto   a  effettuare  il  pagamento.  Fin  qui  la  questione  è  &#13;
 proposta, al  pari che nelle altre ordinanze di rinvio, in relazione al  &#13;
 principio  costituzionale  di  eguaglianza.  Si  denunzia  altresì  la  &#13;
 violazione degli artt. 24 e 113 Cost., deducendo che, se alla  pubblica  &#13;
 amministrazione  spetta  di  stabilire    discrezionalmente  quando - o  &#13;
 addirittura se - emettere il  titolo di spesa, si  viene  a  vanificare  &#13;
 quella  tutela, della  quale dovrebbe invece godere il pieno e perfetto  &#13;
 diritto    soggettivo  del  creditore.  Si  lamenta,  infine,  che   la  &#13;
 denunziata  deroga  delle  norme  relative  alla   corresponsione degli  &#13;
 interessi moratori pregiudichi il  puntuale adempimento degli  obblighi  &#13;
 pecuniari  da  parte    degli  enti  pubblici  ai  quali  si applica il  &#13;
 Regolamento di  contabilità, e vulneri per questo verso il  principio,  &#13;
 consacrato  nell'art.  97  Cost.,  del  buon andamento della   pubblica  &#13;
 amministrazione.                                                         &#13;
     3. - Giova alla  corretta  disamina  del  presente  caso    fermare  &#13;
 anzitutto  l'attenzione  sulla  censura  che concerne   l'articolo 429,  &#13;
 terzo comma, c.p.c. Come risulta dal    dispositivo  dell'ordinanza  di  &#13;
 remissione  del  Pretore di  Palmi, detta disposizione è denunziata in  &#13;
 quanto essa  non derogherebbe al principio  sopra  richiamato,  che  si  &#13;
 assume  sancito  nell'art. 270 del Regolamento di  contabilità, con la  &#13;
 conseguenza di impedire, riguardo ai   dipendenti dello Stato  e  degli  &#13;
 enti  pubblici  non    economici,  la  decorrenza  degli interessi e la  &#13;
 rivalutazione  degli stessi crediti dal giorno  della  maturazione  del  &#13;
 diritto.  Ora,  se  da  un  canto  l'art. 429, terzo comma, c.p.c.   è  &#13;
 impugnato, nei termini testé riferiti, in relazione all'art.  270  del  &#13;
 Regolamento  di  contabilità,  dall'altro  si  deduce -   dallo stesso  &#13;
 Pretore di Palmi, e con un'autonoma  ed    ulteriore  censura  -  anche  &#13;
 l'illegittimità costituzionale di  quest'ultima disposizione.           &#13;
     Il punto esige un cenno di chiarimento. L'art. 429 c.p.c.  potrebbe  &#13;
 derogare  alla  citata  disposizione,  e  comunque  al    regime  della  &#13;
 contabilità pubblica  -  sul  presupposto,  è    appena  il  caso  di  &#13;
 avvertire,  che  si  tratti  di  norme incidenti   sulla disciplina dei  &#13;
 rapporti di lavoro dei dipendenti dello   Stato e degli  enti  pubblici  &#13;
 non  economici  -  solo  se  tali    rapporti  ricadessero sotto la sua  &#13;
 previsione: laddove si  assume dal Pretore di  Palmi  (e,  analogamente  &#13;
 dalla  Corte    d'appello  di  Caltanissetta)  che  essi  ne  rimangono  &#13;
 necessariamente  esclusi.  La  lesione  del  principio di eguaglianza -  &#13;
 asserita in base alla discriminazione che   opererebbe nel  trattamento  &#13;
 dei  crediti  di  lavoro  -  viene    così a prospettarsi sul semplice  &#13;
 assunto che l'art. 429,  terzo comma, c.p.c. sia inestensibile al  caso  &#13;
 di     specie:     indipendentemente,     dunque,     dalla     dedotta  &#13;
 incostituzionalità dell'art. 270 del Regolamento di  contabilità.  Ma  &#13;
 va  subito  osservato che, sotto il profilo  ora in esame, la questione  &#13;
 non è fondata. In questo senso    la  Corte  si  è  già  pronunciata  &#13;
 (sentenza  n.  43  del  1977)  in    altro  giudizio,  in cui la stessa  &#13;
 disposizione dell'art. 429,  terzo comma, veniva denunziata, sempre  in  &#13;
 riferimento    all'art.  3  Cost.,  sostanzialmente per i motivi qui in  &#13;
 esame:   "Le ragioni giustificatrici della norma  in  questione"  -  è  &#13;
 detto nella pronunzia testé citata, e va ora ripetuto - "non  rilevano  &#13;
 negli  stessi  termini  modi  e  misura in cui   ricorrono per gli enti  &#13;
 pubblici economici. Ciò basta per  constatare che le situazioni  poste  &#13;
 a  raffronto  sono  diverse    e  che  quindi  non  sussiste  l'assunta  &#13;
 illegittimità  costituzionale della norma nella parte in  cui  prevede  &#13;
 la    disciplina  sopradetta solo per i dipendenti di cui all'art.  409  &#13;
 del codice di procedura civile". Ciò detto si deve  escludere anche la  &#13;
 lamentata violazione degli artt. 35 e  36 Cost., dedotta  sull'assunto,  &#13;
 di  cui  si è appena veduta  l'infondatezza, che la mancata estensione  &#13;
 al nostro caso   dell'art. 429, terzo comma,  c.p.c.  abbia  offeso  il  &#13;
 principio  costituzionale di eguaglianza.                                &#13;
     4. - Resta da considerare la denunzia dell'art. 270 del Regolamento  &#13;
 di  contabilità.  Nell'ordinanza  di  rinvio   emessa dal Tribunale di  &#13;
 Catania si afferma - e nelle altre  due evidentemente si  presuppone  -  &#13;
 che  tale  Regolamento    sia  atto dotato della forza di legge, quindi  &#13;
 impugnabile in  questa sede.                                             &#13;
     La Corte è di contrario avviso. Il Regolamento è posto  con regio  &#13;
 decreto in conformità della norma di legge  che    ne  costituisce  il  &#13;
 fondamento, l'art. 88 del R.D. 18  novembre 1923, n.2440.  Quest'ultimo  &#13;
 atto  legislativo  è,   a sua volta, un decreto emesso in virtù della  &#13;
 delega   concessa al  governo  con  legge  3  dicembre  1922,  n.  1601  &#13;
 ("Delegazione  di  pieni  poteri al Governo del re per il riordinamento  &#13;
 del sistema tributario e della pubblica  amministrazione").  L'atto  in  &#13;
 cui  è  contenuta  la  norma    censurata  soddisfa, fuor di dubbio, i  &#13;
 requisiti prescritti  per la formazione dei regolamenti dalla legge che  &#13;
 governava la materia: precisamente, il decreto emana,  come doveva, dal  &#13;
 re, in forza della menzionata delega,  che esso espressamente richiama,  &#13;
 e su proposta del Ministro delle  finanze,  sentiti  il  Consiglio  dei  &#13;
 ministri,  la   Corte dei conti e il Consiglio di Stato. La compresenza  &#13;
 nella  specie di questi elementi formali consente quindi di stabilire -  &#13;
 in  via di esclusione, e alla stregua dei criteri predeterminati  dalle  &#13;
 norme  vigenti  -  che  l'atto  in esame non poteva avere base  diversa  &#13;
 dalla potestà regolamentare, qui appositamente  attribuita al governo.  &#13;
 Tale risultato  s'impone,  del  resto,    anche  in  considerazione  di  &#13;
 precedenti pronunzie rese in  analoghi casi dalla Corte, e specialmente  &#13;
 di quel che si è  affermato con sentenza n. 118 del 1968: "In presenza  &#13;
 di  una  qualificazione data dalla legge, nel senso che il  governo era  &#13;
 legittimato ad emanare un regolamento, è   necessario  che  concorrano  &#13;
 elementi  obiettivi,  certi  ed    inequivoci  per  dimostrare  che, al  &#13;
 contrario, si trattava di  una vera e propria  delega  legislativa:  il  &#13;
 che  è  da   affermarsi specialmente in riferimento ad un  ordinamento  &#13;
 nel quale, a differenza di quello attuale, la  diversa forza degli atti  &#13;
 normativi  non  dava  luogo  ad  una   semplice ripartizione fra organi  &#13;
 diversi della competenza   a sindacarne  i  vizi  sostanziali,  ma  era  &#13;
 rilevante  al  fine    della  configurabilità  stessa  di un controllo  &#13;
 giurisdizionale, notoriamente escluso per  gli  atti  legislativi".  Si  &#13;
 deve  aggiungere  che  la  qualificazione   dell'atto come regolamento,  &#13;
 privo del valore della legge,   è nella specie  pienamente  suffragata  &#13;
 anche  dal  preciso   tenore della norma (il citato art. 88 del R.D. n.  &#13;
 2440 del  1923), dalla quale esso trae fondamento. Il governo    veniva  &#13;
 abilitato,  nelle  forme  sopra viste, soltanto "a  modificare le norme  &#13;
 regolamentari vigenti per l'amministrazione del  patrimonio  e  per  la  &#13;
 contabilità   generale dello Stato, con facoltà di emanare ogni altra  &#13;
 disposizione di complemento, di coordinamento e  di    attuazione".  Si  &#13;
 voleva  dunque  circoscrivere l'esercizio  della potestà regolamentare  &#13;
 rigorosamente nei limiti di  una normazione secondaria e  complementare  &#13;
 rispetto  alla    legge.  Nel caso in esame, infatti, il regolamento è  &#13;
 subordinato allo stesso atto legislativo abilitante, che  appresta, dal  &#13;
 canto suo, un'organica disciplina  della  contabilità  generale  dello  &#13;
 Stato.                                                                   &#13;
     Non  varrebbe,  d'altra  parte,  nemmeno  osservare  che la   norma  &#13;
 impugnata è stata costantemente considerata, nel   diritto vivente  ad  &#13;
 opera della giurisprudenza, come un  precetto idoneo a derogare, quando  &#13;
 si tratti di debiti  pecuniari dello Stato e degli anzidetti altri enti  &#13;
 pubblici,    il  regime  posto nel codice   civile in tema di interessi  &#13;
 moratori. A tacer d'altro, qui   soccorre il rilievo che  la  Corte  di  &#13;
 cassazione  ha con varie  decisioni ultimamente negato il fondamento di  &#13;
 una simile  deroga: nel prospettare la presente questione  si  sarebbe,  &#13;
 quindi, in ogni caso denunziata una norma, ormai spoglia  del contenuto  &#13;
 o   dell'efficacia   precettiva,  che  si    vorrebbero  far  scaturire  &#13;
 dall'interpretazione  giurisprudenziale.  Siamo invece, e  sicuramente,  &#13;
 di  fronte    ad  una disposizione, la quale, comunque interpretata, è  &#13;
 pur sempre prodotta mediante atto regolamentare: e con    questa  fonte  &#13;
 non  possono  crearsi norme provviste dello  stesso valore della legge.  &#13;
 Nessun rilievo ha, infine, la    circostanza  che  nelle  ordinanze  di  &#13;
 rinvio  si  fa  riferimento    non solo all'art. 270 del Regolamento di  &#13;
 contabilità, ma  al contesto, o al sistema, della  disciplina  in  cui  &#13;
 figura  questa singola disposizione, per dedurne - oltre, o  piuttosto,  &#13;
 che  una  norma - un qualche principio. sul quale  possa esercitarsi il  &#13;
 sindacato della Corte. Con ciò non si  è, invero,  ancora  dimostrato  &#13;
 che il corpo normativo dal  quale un tale principio, andrebbe enucleato  &#13;
 trascende  la  sfera del regolamento; né, dunque, si è dimostrato che  &#13;
 la  disposizione oggetto di puntuale censura  trova  nell'ordine  delle  &#13;
 fonti idonea e sicura collocazione sul piano della  legge formale, o di  &#13;
 altro  atto  a  questa  equiparato.  Il che,   in conclusione, comprova  &#13;
 l'inammissibilità della  questione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a)   dichiara   inammissibile   la   questione   di    legittimità  &#13;
 costituzionale  dell'art.  270  del  R.D.  23  maggio  1924,  n.    827  &#13;
 (Regolamento  per  l'amministrazione  del  patrimonio      e   per   la  &#13;
 contabilità generale dello Stato), sollevata dalla  Corte d'appello di  &#13;
 Caltanissetta,  dal  Pretore  di  Palmi e   dal Tribunale di Catania in  &#13;
 riferimento agli artt. 24, 97 e  113 Cost.;                              &#13;
     b)    dichiara   non   fondata   la   questione   di   legittimità  &#13;
 costituzionale  dell'art.  429,  terzo  comma,  c.p.c.  in    relazione  &#13;
 all'art.  270  del R.D.  23 maggio 1924, n. 827,  sollevata dalla Corte  &#13;
 d'appello di Caltanissetta e dal  Pretore di Palmi in riferimento  agli  &#13;
 artt. 3, 35, primo  comma, e 36 Cost.                                    &#13;
     Così  deciso  in  Roma,  nella  sede della Corte   costituzionale,  &#13;
 Palazzo della Consulta, 13 aprile 1981.                                  &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA   -   EDOARDO   VOLTERRA  -  &#13;
                                   MICHELE  ROSSANO     -  ANTONINO   DE  &#13;
                                   STEFANO  -  LEOPOLDO  ELIA -   ORONZO  &#13;
                                   REALE - BRUNETTO BUCCIARELLI DUCCI  -  &#13;
                                   ALBERTO  MALAGUGINI - LIVIO PALADIN -  &#13;
                                   ARNALDO  MACCARONE   -   ANTONIO   LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI.                      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
