<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>146</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:146</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>09/04/2003</data_decisione>
    <data_deposito>24/04/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, promossi, nell'ambito di diversi procedimenti penali, dal Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro con ordinanze del 19 marzo, del 21 maggio e del 23 aprile 2002, iscritte rispettivamente al n. 304, al n. 384 e al n. 435 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 26, n. 36 e n. 40, prima serie speciale, dell'anno 2002. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 26 marzo 2003 il Giudice relatore Guido Neppi Modona. &#13;
    Ritenuto che con tre ordinanze di identico tenore, in data 19 marzo, 23 aprile e 21 maggio 2002, il Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro ha sollevato, in riferimento agli artt. 3 e 111, secondo, quarto e quinto comma, della Costituzione, questione di legittimità dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, nella parte in cui richiede il consenso dell'imputato minorenne per la definizione del procedimento nell'udienza preliminare anche nell'ipotesi di sentenza di non luogo a procedere pienamente liberatoria o per improcedibilità; &#13;
    che ad avviso del Tribunale rimettente la norma censurata, subordinando al consenso dell'imputato la definizione del processo minorile nell'udienza preliminare, ha inteso evidentemente dare attuazione al principio sancito dal quinto comma dell'art. 111 Cost., ma attraverso una scelta «alquanto singolare» ha previsto tale regola anche in relazione agli esiti favorevoli all'imputato, senza tenere conto che il suddetto principio costituzionale «postula una sorta di disponibilità del diritto al dibattimento in vista esclusivamente di una probabile pronuncia di colpevolezza» e dovrebbe perciò garantire all'imputato minorenne la possibilità di rinunciare alla definizione anticipata del processo solamente «nella prospettiva di una pronuncia dibattimentale […] più favorevole»; &#13;
    che una interpretazione letterale della disposizione in esame condurrebbe a risultati evidentemente irrazionali, in quanto è intrinsecamente irragionevole prevedere il consenso del minorenne quale condizione per la pronuncia di una sentenza di non luogo a procedere «a carattere pienamente assolutorio» o di improcedibilità, mentre il consenso non è richiesto per la definizione del procedimento con una pronuncia di condanna a norma del comma 2 dello stesso art. 32 del d.P.R. n. 448 del 1988; &#13;
    che sarebbe perciò «indispensabile un intervento» della Corte costituzionale volto ad affermare che «il diritto dell'imputato al contraddittorio dibattimentale dovrebbe incontrare il limite invalicabile rappresentato dal rispetto del principio del favor innocentiae» ed a consentire al giudice dell'udienza preliminare «di emettere una sentenza di non luogo a procedere nelle ipotesi di proscioglimento c.d. "pieno", poiché il dovere di declaratoria immediata costituisce un principio generale immanente al sistema processuale penale»; &#13;
    che la norma censurata sarebbe inoltre illogica e  inconciliabile con i principi che presidiano il sistema processuale minorile, nel quale «si considera "fisiologica" la rinuncia alla pretesa punitiva nell'ottica dell'educazione del minore»; &#13;
    che la differenza di disciplina intercorrente tra la nuova udienza preliminare minorile e l'udienza preliminare ordinaria renderebbe inoltre evidente la disparità di trattamento dei minorenni rispetto agli imputati adulti; &#13;
    che nel giudizio instaurato con ordinanza iscritta al n. 304 del r.o. del 2002 è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che si è riportato all'atto di intervento prodotto nel giudizio relativo alla ordinanza iscritta al n. 566 del r.o. del 2001, decisa con la sentenza n. 195 del 2002. &#13;
    Considerato che con tre ordinanze di uguale tenore il Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro dubita, in riferimento agli artt. 3 e 111, secondo, quarto e quinto comma della Costituzione, della legittimità costituzionale dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, nella parte in cui richiede il consenso dell'imputato minorenne per la definizione del procedimento nell'udienza preliminare anche nell'ipotesi di sentenza di non luogo a procedere pienamente liberatoria o per improcedibilità;  &#13;
    che, stante l'identità delle tre ordinanze, va disposta la riunione dei relativi giudizi; &#13;
    che con sentenza n. 195 del 2002, depositata il 16 maggio 2002, questa Corte ha già accolto la medesima questione, dichiarando costituzionalmente illegittima la norma impugnata «nella parte in cui, in mancanza del consenso dell'imputato, preclude al giudice di pronunciare sentenza di non luogo a procedere che non presuppone un accertamento di responsabilità»; &#13;
    che le questioni vanno pertanto dichiarate manifestamente inammissibili. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 32, comma 1, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), come modificato dall'art. 22 della legge 1° marzo 2001, n. 63, sollevate, in riferimento agli artt. 3 e 111, secondo, quarto e quinto comma, della Costituzione, dal Giudice dell'udienza preliminare del Tribunale per i minorenni di Catanzaro, con le ordinanze in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 aprile 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 24 aprile 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
