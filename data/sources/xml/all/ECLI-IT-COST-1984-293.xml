<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>293</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:293</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1984</data_decisione>
    <data_deposito>19/12/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale degli artt.  41 e 367  &#13;
 del codice di procedura civile promosso con l'ordinanza  emessa  il  25  &#13;
 gennaio  1979  dal  Pretore  di  Roma  sul  ricorso proposto da Pucello  &#13;
 Giuliana ed altri contro istituto Poligrafico dello Stato, iscritta  al  &#13;
 n.  252  del  registro  ordinanze  1979  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 154 dell'anno 1979.                        &#13;
     Visti gli atti di costituzione di Pucello Giuliana ed altri nonché  &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
     udito nell'udienza pubblica del 16 ottobre 1984 il Giudice relatore  &#13;
 Aldo Corasaniti;                                                         &#13;
     uditi l'avvocato Roberto Muggia, per Pucello Giuliana  ed  altri  e  &#13;
 l'avvocato dello Stato Giorgio Azzariti per il Presidente del Consiglio  &#13;
 dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso di un procedimento civile promosso da Pucello Giuliana e  &#13;
 da altri  nei  confronti  dell'Istituto  Poligrafico  dello  Stato  per  &#13;
 ottenere   sentenza   di  condanna  dell'ente  alla  corresponsione  di  &#13;
 differenze retributive, il Pretore  di  Roma,  essendo  stato  proposto  &#13;
 dall'Istituto  convenuto  regolamento  preventivo  di giurisdizione, ha  &#13;
 sollevato su iniziativa dei ricorrenti, con ordinanza  del  25  gennaio  &#13;
 1979   (reg.   ord.   n.  252  del  1979),  questione  di  legittimità  &#13;
 costituzionale degli artt. 41 e 367 c.p.c., in riferimento agli artt. 3  &#13;
 e 24 Cost..                                                              &#13;
     Il giudice a quo ha rilevato  che  la  sospensione  necessaria  del  &#13;
 processo  a  seguito  della  proposizione  di regolamento preventivo di  &#13;
 giurisdizione - verificatasi, nella specie, nel corso  dell'istruttoria  &#13;
 disposta  dallo stesso giudice proprio al fine di acquisire elementi di  &#13;
 fatto  indispensabili  per   valutare   la   natura   pubblicistica   o  &#13;
 privatistica  dell'istituto  convenuto  -  si  pone in contrasto con il  &#13;
 principio costituzionale della parità delle parti nel processo  e  con  &#13;
 la garanzia costituzionale del diritto di difesa.                        &#13;
     Ha,   al  riguardo,  considerato  che  l'istituto  del  regolamento  &#13;
 preventivo, nella sua attuale  disciplina,  consente  alla  parte,  che  &#13;
 intende  proporlo,  di  scegliere  il  momento  processuale  a lei più  &#13;
 favorevole sotto  il  profilo  istruttorio;  donde  la  violazione  del  &#13;
 principio di parità e, quindi, dell'art. 3 Cost..                       &#13;
     Con  riferimento, poi, al profilo di incostituzionalità ex art. 24  &#13;
 Cost., il giudice a quo - premesso che con sentenza di questa Corte  n.  &#13;
 73  del  1973  era  stata  dichiarata  infondata  analoga questione sul  &#13;
 duplice rilievo della  possibilità  per  le  parti  di  fornire  prove  &#13;
 documentali  nel  giudizio  dinanzi  alla  Corte  di Cassazione e della  &#13;
 natura non vincolante  della  decisione  resa  dalla  stessa  Corte  in  &#13;
 relazione  al  merito della causa - ha osservato che la pronunzia sulla  &#13;
 giurisdizione può pregiudicare  il  merito  e  che  non  è  possibile  &#13;
 distinguere,   nell'ambito   delle  attività  istruttorie,  tra  prove  &#13;
 documentali e altri tipi di prove, riferendo di fatto solo  alle  prime  &#13;
 la garanzia costituzionale di cui all'art. 24 Cost..                     &#13;
     Nel  giudizio  si  sono  costituiti, chiedendo che la questione sia  &#13;
 dichiarata fondata, i ricorrenti, i quali hanno depositato memorie.      &#13;
     È  intervenuto  in  giudizio  il  Presidente  del  Consiglio   dei  &#13;
 ministri,  chiedendo  che  la questione sia dichiarata inammissibile o,  &#13;
 comunque, infondata.                                                     &#13;
     L'interventore ha sottolineato che  la  questione  ha  per  oggetto  &#13;
 norme  procedurali relative ai poteri giurisdizionali del giudice a quo  &#13;
 in pendenza del regolamento di giurisdizione,  nello  specifico  ambito  &#13;
 dell'attività istruttoria esperibile ai fini (non già della decisione  &#13;
 del  merito, bensì) proprio dell'accertamento della sussistenza o meno  &#13;
 della  giurisdizione   ordinaria,   poteri   giurisdizionali   la   cui  &#13;
 delimitazione  è  ormai  devoluta  alla  Corte  di  Cassazione:  donde  &#13;
 l'inammissibilità della proposta questione di costituzionalità.        &#13;
     L'interventore  ha  sostenuto  che,  comunque,  tale  questione  è  &#13;
 infondata  in  considerazione,  per  un verso, della discrezionalità -  &#13;
 entro i limiti di ragionevolezza - da riconoscere al legislatore  nella  &#13;
 regolamentazione dei modi della tutela giurisdizionale, per altro verso  &#13;
 del  potere  delle  parti  di  fornire  alla  Corte di Cassazione - che  &#13;
 comunque  è  chiamata  a  valutare  i  presupposti  di   fatto   della  &#13;
 giurisdizione - nuove prove documentali.                                 &#13;
     Con  successiva memoria ha insistito pregiudizialmente nel chiedere  &#13;
 che sia dichiarata l'inammissibilità  della  questione,  rilevando  in  &#13;
 particolare  che  le Sezioni Unite hanno medio tempore, con la sentenza  &#13;
 n. 3826 del 1979, deciso irretrattabilmente sulla  giurisdizione,  alla  &#13;
 cui   verifica   la   questione   di  legittimità  costituzionale  era  &#13;
 incidentale, negando la giurisdizione del giudice adito.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Come  si  desume  dalla  narrativa,  il  giudice  a  quo   -  &#13;
 discutendosi  se  la  giurisdizione  spettasse al giudice ordinario o a  &#13;
 quello amministrativo, ed essendo stato proposto regolamento preventivo  &#13;
 - ha sospettato di illegittimità costituzionale il combinato  disposto  &#13;
 degli artt. 41 e 367 c.p.c. sotto il particolare profilo dell'incidenza  &#13;
 che esso spiega sulla prova in tema di giurisdizione.                    &#13;
     Ha  sottolineato  al riguardo come la normativa denunciata imponga,  &#13;
 per il caso di intervenuta proposizione del  regolamento preventivo  di  &#13;
 giurisdizione,  la  sospensione  di un'attività istruttoria in punto a  &#13;
 giurisdizione diretta, come quella del giudice a quo,  all'accertamento  &#13;
 dei  fatti  rilevanti  senza  limitazione  di mezzi di prova, e devolva  &#13;
 correlativamente  il  giudizio  alle  Sezioni  Unite  della  Corte   di  &#13;
 Cassazione,  costrette  dalla  peculiarità  del rito a valutare i soli  &#13;
 documenti  già  acquisiti  (art.  372  c.p.c.)  o  al  più   (secondo  &#13;
 un'interpretazione meno rigorosa della detta disposizione) ad acquisire  &#13;
 soltanto nuovi documenti (con esclusione di altri mezzi di prova).       &#13;
     L'illegittimità    del    congelamento    o    della   limitazione  &#13;
 dell'attività istruttoria che vengono in tal modo  a  determinarsi  è  &#13;
 prospettata con riferimento a due parametri:                             &#13;
     a) all'art. 3 Cost. in quanto, dandosi alla parte, alla cui tesi in  &#13;
 punto  di  giurisdizione  siano favorevoli i risultati istruttori in un  &#13;
 dato momento, il potere di  determinare  unilateralmente,  mediante  la  &#13;
 proposizione   del  regolamento  preventivo,  gli  effetti  suindicati,  &#13;
 sarebbe violato il principio della parità delle parti nel processo (la  &#13;
 regola del combattimento ad armi pari);                                  &#13;
     b)  all'art.  24   Cost.   in   quanto   gli   effetti   suindicati  &#13;
 costituirebbero  comunque  violazione del diritto di difesa considerato  &#13;
 come diritto alla prova.                                                 &#13;
     Va tuttavia rilevato che i due profili di illegittimità danno vita  &#13;
 a una  questione  sostanzialmente  unica.    Invero,  a  parte  che  la  &#13;
 violazione  del  principio  del  combattimento  giudiziale ad armi pari  &#13;
 ferisce l'art. 24 Cost. non meno che l'art. 3 Cost., il vizio normativo  &#13;
 denunciato consiste, al di là della  prospettazione,  non  già  nella  &#13;
 possibilità   che   il  congelamento  o  la  limitazione  della  prova  &#13;
 intervengano su iniziativa della parte che in un dato momento la reputi  &#13;
 a  sé  più  favorevole  e  malgrado  l'altra  parte,   bensì   nella  &#13;
 possibilità  stessa  che  il  congelamento  o  la  limitazione  in sé  &#13;
 considerati intervengano.                                                &#13;
     2. - L'eccezione di  inammissibilità  sollevata  dall'interventore  &#13;
 nella  memoria  (sopravvenuta  decisione  del regolamento preventivo da  &#13;
 parte  della  Corte  regolatrice  nel  senso  della   negazione   della  &#13;
 giurisdizione  del  giudice  a  quo)  -  a  parte ogni dubbio sulla sua  &#13;
 fondatezza in relazione al carattere del  dedotto  ostacolo  preclusivo  &#13;
 (irrilevanza  successiva)  - rimane comunque assorbita da una diversa e  &#13;
 preliminare ragione di inammissibilità della questione stessa.          &#13;
     Si tratta di  una  ragione  di  inammissibilità,  che  (sia  o  no  &#13;
 adombrata,  come  potrebbe ritenersi, nell'atto di intervento) la Corte  &#13;
 ritiene di dovere rilevare d'ufficio.                                    &#13;
     L'illegittimità prospettata risiede  non  tanto  nella  inibizione  &#13;
 della  attività  istruttoria  nei  confronti del giudice a quo e nella  &#13;
 rimessione  di  tale  attività  ad   altro   giudice,   quanto   nelle  &#13;
 peculiarità  probatorie  del  procedimento davanti al giudice ad quem,  &#13;
 cioè alla Corte di Cassazione,  o  meglio  nella  loro  estensione  al  &#13;
 giudizio  della detta Corte sulla giurisdizione (solo indirettamente le  &#13;
 denunciate carenze del procedimento  davanti  alla  Cassazione  possono  &#13;
 riflettersi  sulla  legittimità  dell'intero  istituto del regolamento  &#13;
 preventivo).  Sicché unicamente la Cassazione, - di fronte a cui  quel  &#13;
 procedimento,  con  quelle  peculiarità, si svolge, ed ai fini del cui  &#13;
 (solo) giudizio l'eliminazione del vizio è pertanto rilevante  -  può  &#13;
 postularla,  e  non  anche  il  giudice  a  quo.    Né, per giungere a  &#13;
 conclusione opposta, varrebbero gli argomenti adducibili a favore della  &#13;
 rilevanza per il giudice adito  ex  art.  700  c.p.c.  delle  questioni  &#13;
 relative  alla  legittimità  della privazione nei suoi confronti di un  &#13;
 potere   di   decidere   su   richieste  di  provvedimenti  di  urgenza  &#13;
 anticipatori non devoluto medio tempore ad altro giudice.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   inammissibile    la    questione    di    illegittimità  &#13;
 costituzionale,  in riferimento agli artt. 3 e 24 Cost., degli artt. 41  &#13;
 e 367 c.p.c. sollevata dal Pretore di Roma con ordinanza del 25 gennaio  &#13;
 1979.                                                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 14 dicembre 1984.                             &#13;
                                   F.to:   LEOPOLDO   ELIA  -  GUGLIELMO  &#13;
                                   ROEHRSSEN  -   BRUNETTO   BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN  -  ANTONIO  LA   PERGOLA   -  &#13;
                                   VIRGILIO  ANDRIOLI - GIUSEPPE FERRARI  &#13;
                                   - FRANCESCO SAJA - GIOVANNI  CONSO  -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO.                   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
