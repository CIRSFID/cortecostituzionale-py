<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>503</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:503</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/11/1987</data_decisione>
    <data_deposito>10/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 152, secondo    &#13;
 comma, del r.d. 18 giugno 1931, n. 773 ("Testo unico delle  leggi  di    &#13;
 pubblica  sicurezza"),  promosso  con ordinanza emessa il 28 febbraio    &#13;
 1985 dal  Pretore  di  Legnano,  iscritta  al  n.  256  del  registro    &#13;
 ordinanze 1985 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 173- bis dell'anno 1985;                                              &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 28 ottobre 1987 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso di un procedimento penale a carico di un    &#13;
 cittadino straniero, imputato del reato di cui all'art. 152 del  r.d.    &#13;
 18  giugno  1931  n.  773  ("Testo  unico  delle  leggi  di  pubblica    &#13;
 sicurezza"),  il  Pretore  di  Legnano  ha  sollevato  questione   di    &#13;
 legittimità  costituzionale  del  citato art. 152, secondo comma, in    &#13;
 riferimento agli artt. 2 e 97 Cost.:                                     &#13;
      che  la  norma  impugnata  viene  censurata  nella parte in cui,    &#13;
 consentendo al prefetto di emettere un  provvedimento  di  espulsione    &#13;
 immediatamente esecutivo "senza instaurare con il destinatario nessun    &#13;
 contradditorio", si porrebbe in contrasto sia  con  il  principio  di    &#13;
 imparzialità  (art. 97 Cost.) che con il generale riconoscimento dei    &#13;
 diritti inviolabili dell'uomo (art. 2 Cost.);                            &#13;
      che  l'Avvocatura  Generale dello Stato è intervenuta chiedendo    &#13;
 che venisse dichiarata la non fondatezza della questione.                &#13;
    Considerato  che,  in relazione alla presunta violazione dell'art.    &#13;
 2, questa Corte con le pronunce n. 75 del 1966 e  168  del  1971,  ha    &#13;
 già  avuto  modo  di  affermare  che  il  riconoscimento dei diritti    &#13;
 inviolabili dell'uomo non esclude che a carico  dei  cittadini  siano    &#13;
 poste  quelle restrizioni della sfera giuridica rese necessarie dalla    &#13;
 tutela dell'ordine sociale o dell'ordine pubblico;                       &#13;
      che  analoghe  considerazioni  valgono  a  maggior  ragione  nei    &#13;
 confronti dello straniero il  quale,  secondo  quanto  già  ritenuto    &#13;
 nelle  sentenze  n.  104 del 1969 e 244 del 1974, gode sul territorio    &#13;
 della Repubblica di una tutela meno intensa di quella riconosciuta al    &#13;
 cittadino;                                                               &#13;
      che, in particolare, secondo le ricordate pronunce, lo straniero    &#13;
 non ha di regola un diritto acquisito  di  ingresso  e  di  soggiorno    &#13;
 nello  Stato  e  pertanto  le  relative  libertà  ben possono essere    &#13;
 limitate a tutela di particolari  interessi  pubblici,  quale  quello    &#13;
 attinente alla sicurezza intesa come ordinato vivere civile;             &#13;
      che  quindi  la  questione  sollevata  in riferimento all'art. 2    &#13;
 Cost. va dichiarata manifestamente infondata;                            &#13;
      che  il  principio del cosiddetto giusto procedimento - al quale    &#13;
 il giudice remittente sembra riferirsi quando censura la facoltà  di    &#13;
 espulsione  in assenza di qualsiasi contradditorio - non è assistito    &#13;
 in assoluto da garanzia costituzionale  come  questa  Corte  ha  già    &#13;
 ritenuto  in  precedenti  occasioni (sent. n. 13 del 1962 e n. 23 del    &#13;
 1978);                                                                   &#13;
      che,   pertanto,   anche   in  relazione  a  tale  profilo,  non    &#13;
 sussistendo violazione dell'art. 97 Cost., la questione va dichiarata    &#13;
 manifestamente infondata;                                                &#13;
    Visti  gli  artt.  26  legge  11  marzo 1953 n. 87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 152, secondo comma, r.d. 18 giugno  1931  n.    &#13;
 773  ("Testo unico delle leggi di pubblica sicurezza"), sollevata, in    &#13;
 riferimento agli artt. 2 e 97  Cost.,  dal  Pretore  di  Legnano  con    &#13;
 ordinanza n. 256 del 1985.                                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 25 novembre 1987.                             &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: CAIANIELLO                           &#13;
    Depositata in cancelleria il 10 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
