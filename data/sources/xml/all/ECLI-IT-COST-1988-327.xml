<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>327</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:327</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/03/1988</data_decisione>
    <data_deposito>17/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 16 della legge    &#13;
 27 luglio 1978,  n.  392  (Disciplina  delle  locazioni  di  immobili    &#13;
 urbani),  promosso  con  ordinanza  emessa  il  16  marzo  1982 dalla    &#13;
 Commissione tributaria di primo grado di Genova, iscritta al  n.  361    &#13;
 del  registro  ordinanze  1984  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 259 dell'anno 1984;                                  &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 24 febbraio 1988 il Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto  che  la  Società  Entomologica  Italiana,  con  sede in    &#13;
 Genova, proponeva ricorso alla Commissione tributaria di primo  grado    &#13;
 della stessa città avverso la registrazione di variazione negli atti    &#13;
 del N.C.E.U. emessa dall'Ufficio tecnico  erariale,  il  quale  aveva    &#13;
 rilevato  il  frazionamento  di  una unità immobiliare di proprietà    &#13;
 della ricorrente - classificata in categoria A/1, classe 2ª - in  due    &#13;
 distinte  unità  ed  aveva a queste attribuito la categoria A/2 e la    &#13;
 classe 3ª;                                                               &#13;
      che  la  ricorrente  deduceva  che, da un lato, il declassamento    &#13;
 della categoria catastale da  A/1  a  A/2  determinava,  per  effetto    &#13;
 dell'applicazione  dell'art.  16  della  legge 27 luglio 1978, n. 392    &#13;
 ("Disciplina delle locazioni  di  immobili  urbani"),  una  rilevante    &#13;
 diminuzione  del  reddito  reale  (pari  alla  somma  dei  canoni  di    &#13;
 locazione delle due unità immobiliari frazionate,  che  risulterebbe    &#13;
 inferiore  al  canone  dell'unica unità originaria); dall'altro, per    &#13;
 converso,  l'attribuzione  della  classe  3ª  ai   due   appartamenti    &#13;
 comportava  un  notevole  aumento  del  reddito imponibile (pari alla    &#13;
 somma delle rendite catastali, che risulterebbe  superiore  a  quella    &#13;
 dell'appartamento originario);                                           &#13;
      che  la adita Commissione tributaria, con ordinanza del 16 marzo    &#13;
 1982 (pervenuta alla Corte il 22 marzo 1984), ha sollevato  questione    &#13;
 di  legittimità  costituzionale  del  citato  art. 16 della legge 27    &#13;
 luglio 1978, n. 392, deducendo che la norma (peraltro inderogabile ai    &#13;
 sensi dell'art. 79 della stessa legge) violerebbe:                       &#13;
       a) l'art. 3 Cost., per disparità di trattamento tributario tra    &#13;
 chi utilizza l'immobile direttamente quale  titolare  di  un  diritto    &#13;
 reale  di  godimento su di esso e chi lo utilizza mediante locazione,    &#13;
 in quanto nel primo caso il reddito imponibile è  ritenuto  ex  lege    &#13;
 coincidente  con  quello  reale  quantificato nella rendita catastale    &#13;
 rivalutata, mentre, nel secondo, il reddito imponibile  coincide  con    &#13;
 il canone locatizio in concreto percepito, ove superiore alla rendita    &#13;
 catastale;                                                               &#13;
       b)   l'art.  53  Cost.,  per  violazione  del  principio  della    &#13;
 capacità contributiva, in quanto  può  verificarsi,  come  avvenuto    &#13;
 nella  fattispecie, che alla diminuzione del reddito reale (canone di    &#13;
 locazione) corrisponda un aumento del reddito imponibile;                &#13;
      che,  in  conclusione, il giudice a quo censura l'art. 16 citato    &#13;
 "in quanto, in relazione alla tipologia,  fa  riferimento  alla  sola    &#13;
 categoria  catastale  e non anche alla classe nell'ambito di ciascuna    &#13;
 categoria, o piuttosto alla rendita catastale  dell'immobile",  così    &#13;
 adottando, a suo avviso, un criterio lacunoso, ambiguo e determinante    &#13;
 risultati contraddittori;                                                &#13;
      che  il  Presidente  del  Consiglio dei ministri, intervenuto in    &#13;
 giudizio, chiede che la questione sia  dichiarata  inammissibile  per    &#13;
 irrilevanza o, comunque, infondata;                                      &#13;
   Considerato  che  questa  Corte  ha  costantemente affermato che il    &#13;
 nesso di pregiudizialità necessario per la rilevanza della questione    &#13;
 di  legittimità  costituzionale  deve  consistere  in un rapporto di    &#13;
 rigorosa strumentalità tra la risoluzione della questione  stessa  e    &#13;
 la  decisione  del  giudizio  a  quo  e  che il dubbio deve, perciò,    &#13;
 investire una norma dalla  cui  applicazione  il  giudice  remittente    &#13;
 dimostri  di  non  poter  prescindere  (da  ult.,  ordd. nn. 595/87 e    &#13;
 167/88);                                                                 &#13;
      che nella fattispecie, come ha esattamente rilevato l'Avvocatura    &#13;
 dello Stato, il  giudice  a  quo  non  potrà  né  dovrà  mai  fare    &#13;
 applicazione  della norma censurata, in quanto il giudizio sottoposto    &#13;
 al  suo   esame   concerne   esclusivamente   la   legittimità   del    &#13;
 provvedimento  dell'u.t.e.  in  ordine  alla  "qualificazione" e alla    &#13;
 "classificazione" delle unità immobiliari in questione e non certo i    &#13;
 riflessi  che  detto  provvedimento  potrà  comportare, in base alla    &#13;
 norma impugnata, sulla determinazione del canone di locazione;           &#13;
      che,  peraltro, non può non rilevarsi che la censura investe la    &#13;
 scelta effettuata dal legislatore  nell'ambito  della  sua  sfera  di    &#13;
 discrezionalità  tecnica,  e che tale scelta, secondo la consolidata    &#13;
 giurisprudenza di questa Corte (da ult., ordd.  nn.  386  e  573  del    &#13;
 1987),  non  è  sindacabile  in  questa  sede,  salvo che non ne sia    &#13;
 evidente l'arbitrarietà e  irrazionalità,  il  che  certamente  non    &#13;
 accade nel caso di specie;                                               &#13;
    Visti  gli  artt.  26  della  legge 11 marzo 1953, n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi dinanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 16 della legge 27 luglio  1978,    &#13;
 n. 392 (Disciplina delle locazioni di immobili urbani), sollevata, in    &#13;
 riferimento agli artt. 3 e 53 Cost., dalla Commissione tributaria  di    &#13;
 primo grado di Genova con l'ordinanza in epigrafe.                       &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 10 marzo 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 17 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
