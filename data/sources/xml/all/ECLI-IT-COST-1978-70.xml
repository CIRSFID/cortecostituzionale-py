<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1978</anno_pronuncia>
    <numero_pronuncia>70</numero_pronuncia>
    <ecli>ECLI:IT:COST:1978:70</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/06/1978</data_decisione>
    <data_deposito>02/06/1978</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN &#13;
 - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti sull'ammissibilità,  ai  sensi  dell'art.  75,  &#13;
 secondo  comma,  della  Costituzione,  delle  richieste  di  referendum  &#13;
 popolare per l'abrogazione:                                              &#13;
     1. - della legge 22 maggio 1975, n. 152,  recante  "Disposizioni  a  &#13;
 tutela dell'ordine pubblico", come modificata, nell'art. 5, dall'art. 2  &#13;
 della  legge  8 agosto 1977, n. 533: "Disposizioni in materia di ordine  &#13;
 pubblico" (n. 11 reg. ref.);                                             &#13;
     2. - della legge 2 maggio 1974, n. 195, "Contributo dello Stato  al  &#13;
 finanziamento  dei  partiti  politici", come modificata, nell'art.   3,  &#13;
 terzo comma, lett. b, dall'articolo unico della legge 16 gennaio  1978,  &#13;
 n.  11, "Modifiche alla legge 2 maggio 1974, n.  195, concernente norme  &#13;
 sul contributo dello Stato al finanziamento dei partiti  politici"  (n.  &#13;
 12 reg. ref.).                                                           &#13;
     Udito  nella  camera  di  consiglio  del  1  giugno 1978 il Giudice  &#13;
 relatore Livio Paladin.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza del 25 maggio 1978, l'Ufficio  centrale  per  il  &#13;
 referendum,  costituito  presso la Corte di cassazione, ha disposto che  &#13;
 le operazioni  relative  alla  richiesta  di  referendum  popolare  per  &#13;
 l'abrogazione  della  legge  22  maggio  1975,  n. 152, si svolgano sul  &#13;
 seguente quesito: "Volete voi che sia abrogata la legge 22 maggio 1975,  &#13;
 n. 152, recante "Disposizioni  a  tutela  dell'ordine  pubblico",  come  &#13;
 modificata, nell'art. 5, dall'art. 2 della legge 8 agosto 1977, n. 533,  &#13;
 "Disposizioni in materia di ordine pubblico"?                            &#13;
     L'Ufficio  centrale  ha  infatti  rilevato  che  questa Corte - con  &#13;
 sentenza n. 68 del 17 maggio 1978 - aveva  dichiarato  l'illegittimità  &#13;
 dell'art.  39 della legge n. 352 del 1970, "limitatamente alla parte in  &#13;
 cui non prevede  che  se  l'abrogazione  degli  atti  o  delle  singole  &#13;
 disposizioni cui si riferisce il referendum venga accompagnata da altra  &#13;
 disciplina  della  stessa  materia,  senza  modificare  né  i principi  &#13;
 ispiratori della complessiva disciplina preesistente  né  i  contenuti  &#13;
 normativi  essenziali  dei  singoli precetti, il referendum si effettui  &#13;
 sulle  nuove  disposizioni  legislative";  e   conseguentemente   aveva  &#13;
 annullato  -  con  la  sentenza  n. 69 del 23 maggio 1978 - l'ordinanza  &#13;
 emessa dall'Ufficio centrale in data 6 dicembre 1977,  nella  parte  in  &#13;
 cui  modificava il quesito referendario relativo alla legge n.  152 del  &#13;
 1975, eccettuandone l'art. 5.                                            &#13;
     Su  questa  base,  l'Ufficio centrale ha constatato che la modifica  &#13;
 introdotta dall'art. 2 della legge n. 533 del 1977, quanto all'art.   5  &#13;
 della legge n. 152 del 1975, "esaurendosi nell'ampliamento della figura  &#13;
 criminosa  con  esclusione  di  un  caso particolare, nell'inasprimento  &#13;
 della  sanzione  e  nella  previsione   dell'arresto   facoltativo   in  &#13;
 flagranza, pur se innova anche nella sostanza la disciplina precedente,  &#13;
 tuttavia   è   sempre   riconducibile   ai  principi  complessivamente  &#13;
 ispiratori  di  questa";  e  pertanto  ha  concluso  che  le   relative  &#13;
 operazioni referendarie si dovranno estendere alla nuova disposizione.   &#13;
     2. - Con altra ordinanza emessa in pari data, l'Ufficio centrale ha  &#13;
 disposto  che  le  operazioni  relative  alla  richiesta  di referendum  &#13;
 popolare per l'abrogazione della  legge  2  maggio  1974,  n.  195,  si  &#13;
 svolgano  sul seguente quesito: "Volete voi che sia abrogata la legge 2  &#13;
 maggio 1974, n. 195,  "Contributo  dello  Stato  al  finanziamento  dei  &#13;
 partiti  politici",  come modificata, nell'art. 3, terzo comma, lettera  &#13;
 b, dall'articolo unico della legge 16 gennaio 1978, n.  11,  "Modifiche  &#13;
 alla  legge  2  maggio  1974,  n. 195, concernente norme sul contributo  &#13;
 dello Stato al finanziamento dei partiti politici"?                      &#13;
     Preso atto della ricordata sentenza  n.  68  del  17  maggio  1978,  &#13;
 l'Ufficio  centrale  ha ritenuto "che la introdotta modifica, attenendo  &#13;
 alla sola individuazione dei destinatari del finanziamento,  opera  una  &#13;
 innovazione   di   dettaglio,   costituente   una   ulteriore  puntuale  &#13;
 applicazione dei  principi  informatori  della  complessiva  disciplina  &#13;
 preesistente"; ed anche in tal caso ha perciò modificato l'oggetto del  &#13;
 quesito, estendendo le operazioni referendarie alla nuova disposizione.  &#13;
     3.  -  Entrambe  le  ordinanze sono state quindi trasmesse a questa  &#13;
 Corte,  per   la   verifica   dell'esistenza   di   eventuali   ragioni  &#13;
 d'inammissibilità, in ordine ai.quesiti referendari così modificati.   &#13;
     Ricevuta  comunicazione  delle ordinanze, il Presidente della Corte  &#13;
 ha fissato per le conseguenti deliberazioni il giorno  1  giugno  1978,  &#13;
 dandone a sua volta comunicazione ai presentatori delle richieste ed al  &#13;
 Presidente  del  Consiglio dei ministri, ai sensi dell'art. 33, secondo  &#13;
 comma, della legge n. 352 del 1970; né gli uni  né  l'altro  si  sono  &#13;
 avvalsi della facoltà di depositare memorie, di cui all'art. 33, terzo  &#13;
 comma, della legge citata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Ciò  premesso, la Corte ritiene ammissibile la richiesta di  &#13;
 referendum per l'abrogazione della  legge  22  maggio  1975,  n.    152  &#13;
 ("Disposizioni   a  tutela  dell'ordine  pubblico"),  anche  in  ordine  &#13;
 all'art. 5 della legge stessa, come modificato dall'art. 2 della  legge  &#13;
 8 agosto 1977, n. 533 ("Disposizioni in materia di ordine pubblico").    &#13;
     Valgono  in  tal  senso,  infatti,  le stesse considerazioni che la  &#13;
 Corte ha già svolto nella sentenza n. 16  del  7  febbraio  1978,  per  &#13;
 desumere l'ammissibilità della richiesta referendaria concernente, nel  &#13;
 loro  complesso,  tutte  le residue disposizioni della legge n. 152 del  &#13;
 1975.                                                                    &#13;
     2.   -   Del    pari,    non    sussistono    specifiche    ragioni  &#13;
 d'inammissibilità,   per   effetto  dell'estensione  delle  operazioni  &#13;
 referendarie concernenti la legge 2 maggio 1974,  n.  195  ("Contributo  &#13;
 dello Stato al finanziamento dei partiti politici"), all'articolo unico  &#13;
 della  legge  16  gennaio  1978,  n. 11 ("Modifiche alla legge 2 maggio  &#13;
 1974,  n.  195,  concernente  norme  sul  contributo  dello  Stato   al  &#13;
 finanziamento dei partiti politici").                                    &#13;
     Quanto  invece  all'ammissibilità del referendum per l'abrogazione  &#13;
 dell'intera legge n. 195 del 1974, la Corte  si  è  già  espressa  in  &#13;
 senso affermativo con la ricordata sentenza n. 16 di quest'anno.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   ammissibili,   ai   sensi   delle   indicate   ordinanze  &#13;
 dell'Ufficio centrale per il referendum presso la Corte di  cassazione,  &#13;
 le richieste di referendum:                                              &#13;
     1)     per  l'abrogazione  della  legge  22  maggio  1975,  n.  152  &#13;
 ("Disposizioni  a  tutela  dell'ordine  pubblico"),   come   modificata  &#13;
 nell'art.   5,   dall'art.   2  della  legge  8  agosto  1977,  n.  533  &#13;
 ("Disposizioni in materia di ordine pubblico");                          &#13;
     2)  per  l'abrogazione  della  legge  2  maggio  1974,  n.  195  ("  &#13;
 Contributo  dello  Stato  al finanziamento dei partiti politici"), come  &#13;
 modificata, nell'art. 3, terzo  comma,  lett.  b,  dall'articolo  unico  &#13;
 della  legge  16  gennaio  1978,  n. 11 ("Modifiche alla legge 2 maggio  &#13;
 1974,  n.  195,  concernente  norme  sul  contributo  dello  Stato   al  &#13;
 finanziamento dei partiti politici").                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 1 giugno 1978.          &#13;
                                   F.to:  LEONETTO  AMADEI   -   EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO  -  ANTONINO  DE  STEFANO   -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI   LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE.           &#13;
                                   GIOVANNI VTTALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
