<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>130</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:130</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia>Franco Bile</redattore_pronuncia>
    <data_decisione>23/03/2006</data_decisione>
    <data_deposito>28/03/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2045 del codice civile, promosso con ordinanza del 30 maggio 2005 dal Tribunale di Cosenza nel procedimento civile vertente tra Claudia Leporace e Vincenzo Iacoianni ed altri, iscritta al n. 501 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 41, prima serie speciale, dell'anno 2005. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio dell'8 marzo 2006 il Giudice relatore Franco Bile.  &#13;
    Ritenuto che il Tribunale di Cosenza, in composizione monocratica, con ordinanza emessa il 30 maggio 2005, ha sollevato questione di legittimità costituzionale dell'art. 2045 del codice civile in riferimento all'art. 3 della Costituzione; &#13;
    che l'ordinanza è stata resa nel corso di un giudizio civile, che l'attrice, trasportata nell'autovettura condotta dal marito, aveva proposto nei confronti di costui e della società assicuratrice per la responsabilità civile, per ottenere il risarcimento del danno subìto a seguito di una frenata, dopo che nel giudizio stesso erano state chiamate in causa la ASL di Cosenza e la Regione Calabria; &#13;
    che il rimettente – premesso che la frenata era stata resa necessaria da un evento imprevedibile (l'improvviso attraversamento della strada da parte di un cane) – afferma che «nel giudizio sussiste, alla stregua dello svolgimento dei fatti, la prova liberatoria di cui al primo comma dell'art. 2054 cod. civ.» e che «la domanda, pertanto, deve essere rigettata»; &#13;
    che peraltro – ritenuta implicita nella domanda di risarcimento quella di corresponsione di un equo indennizzo, quando risulti che il convenuto abbia agito in stato di necessità – il rimettente rileva che l'art. 2045 cod. civ. non può trovare applicazione nella specie «in quanto la condotta del danneggiante non era volta a prevenire “un grave danno alla persona”, ma ad evitare di travolgere e ferire un animale»; &#13;
    che, tuttavia, a suo giudizio, l'esclusione di una simile ipotesi dallo “stato di necessità” conduce a risultati paradossali ed assurdi – con conseguente «contrasto con il principio di uguaglianza e con quello di ragionevolezza» –, in quanto «la norma consente di attribuire un indennizzo al danneggiato solo qualora la condotta necessitata abbia consentito di evitare un danno grave ad un soggetto umano, mentre non prevede alcun ristoro indennitario nel caso in cui la condotta non riprovevole del danneggiato abbia avuto di mira la salvaguardia di un essere animato diverso dall'uomo ovvero di un interesse di rango meno elevato»; &#13;
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per la manifesta infondatezza della questione. &#13;
    Considerato che il rimettente – dopo essere giunto alla conclusione che «nel giudizio sussiste, alla stregua dello svolgimento dei fatti, la prova liberatoria di cui al primo comma dell'art. 2054 cod. civ.» e che «la domanda, pertanto, deve essere rigettata» – ritiene che nella domanda di risarcimento del danno è implicita quella di corresponsione di un equo indennizzo, ove risulti che il convenuto abbia agito in stato di necessità; &#13;
    che peraltro egli non chiarisce come, in termini logico-giuridici, la ritenuta imprevedibilità dell'improvviso attraversamento della strada da parte di un cane (che porta, secondo la premessa, all'esclusione della responsabilità del conducente per i danni derivati dalla frenata) possa coesistere con l'applicabilità nella stessa fattispecie della norma impugnata, la quale viceversa presuppone pur sempre una responsabilità dell'agente, almeno in termini di imputabilità della condotta lesiva, derivante dalla libera determinazione di violare una norma giuridica; &#13;
    che una tale impostazione della questione dimostra l'evidente errore prospettico da cui muove il rimettente, là dove riferisce gli asseriti vizi di incostituzionalità della norma alla mancata inclusione, nell'ambito dei presupposti per la sua operatività, della condotta del danneggiante che miri ad «evitare di travolgere e ferire un animale», senza verificare se tale norma fosse in concreto applicabile, ossia se la manovra necessitata del conducente fosse idonea a salvare sé e la persona trasportata dal pericolo attuale di un danno di maggiore gravità derivante dalle possibili conseguenze dell'investimento dell'animale; &#13;
    che dall'erroneità della premessa interpretativa discende la manifesta infondatezza della sollevata questione. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 2045 del codice civile, sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale di Cosenza, in composizione monocratica, con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 marzo 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Franco BILE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 28 marzo 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
