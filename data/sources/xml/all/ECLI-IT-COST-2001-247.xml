<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>247</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:247</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/07/2001</data_decisione>
    <data_deposito>12/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY,Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI,Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  per  conflitto  di attribuzione tra poteri dello Stato &#13;
sorto  a  seguito  della  delibera  della  Camera  dei  deputati  del &#13;
21 luglio   1998,   relativa  alla  insindacabilità  delle  opinioni &#13;
espresse  dall'on. Vittorio  Sgarbi  nei  confronti del dott. Gennaro &#13;
Costagliola,  promosso  con ricorso della Corte di appello di Napoli, &#13;
sezione  prima  civile,  notificato il 5 dicembre 2000, depositato in &#13;
Cancelleria  il  12 gennaio  2001  ed  iscritto  al n. 3 del registro &#13;
conflitti 2001. &#13;
    Visto l'atto di costituzione della Camera dei deputati; &#13;
    Udito  nella  camera  di  consiglio del 26 aprile 2001 il giudice &#13;
relatore Massimo Vari.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un procedimento civile instaurato dai congiunti &#13;
del  magistrato  Gennaro  Costagliola  contro  il  deputato  Vittorio &#13;
Sgarbi,  per ottenere la condanna di quest'ultimo al risarcimento dei &#13;
danni  per  la  lesione  della reputazione e dell'identità personale &#13;
derivata   al   magistrato  dalle  frasi  pronunciate  dall'anzidetto &#13;
deputato,  nel  corso  di alcune trasmissioni televisive, la Corte di &#13;
appello  di Napoli, sezione prima civile, con ordinanza del 6 ottobre &#13;
1999,  ha  sollevato  conflitto  di  attribuzione nei confronti della &#13;
Camera  dei  deputati,  in  riferimento  alla  deliberazione adottata &#13;
dall'assemblea   il   21 luglio   1998,   che  ha  affermato  che  le &#13;
dichiarazioni  per  le  quali  è pendente il menzionato procedimento &#13;
civile   concernono  opinioni  espresse  dal  membro  del  Parlamento &#13;
nell'esercizio  delle sue funzioni, con conseguente insindacabilità, &#13;
a norma dell'art. 68, primo comma, della Costituzione. &#13;
    In  punto di fatto, la Corte di appello rammenta che il tribunale &#13;
di  Napoli,  con  sentenza  del  10 novembre  1997,  ha condannato il &#13;
deputato   Sgarbi   al  risarcimento  del  danno,  ritenendo  che  le &#13;
dichiarazioni  dal  medesimo pronunciate nel corso delle trasmissioni &#13;
televisive  "Sgarbi  quotidiani", trasmesse dalla rete Canale 5, e in &#13;
un'intervista  rilasciata al quotidiano "La Repubblica", contenessero &#13;
affermazioni  aventi  carattere  diffamatorio  e  lesivo dell'onore e &#13;
della  memoria  del defunto magistrato dott. Gennaro Costagliola. Nel &#13;
corso   del   giudizio   di  appello,  l'on. Sgarbi  ha  prodotto  la &#13;
deliberazione  di insindacabilità adottata dalla Camera dei Deputati &#13;
il 21 luglio 1998. &#13;
    Il  giudice  ricorrente,  escludendo  che  possa  ravvisarsi  una &#13;
connessione   tra  le  dichiarazioni  pronunciate  dall'on. Sgarbi  e &#13;
l'esercizio  della  sua  attività  di  parlamentare,  ritiene che la &#13;
Camera  dei  deputati  abbia "erroneamente valutato le condizioni e i &#13;
presupposti richiesti dall'art. 68, primo comma, della Costituzione", &#13;
e  chiede,  quindi,  che  venga dichiarato che non spetta alla stessa &#13;
Camera  affermare  l'insindacabilità,  ai  sensi dell'art. 68, primo &#13;
comma,  della  Costituzione,  delle  opinioni  espresse  dal deputato &#13;
Vittorio  Sgarbi,  secondo  quanto  deliberato  dall'assemblea  nella &#13;
seduta del 21 luglio 1998, con conseguente annullamento di tale atto. &#13;
    2. - Il  conflitto,  con ordinanza n. 469 del 3 novembre del 2000 &#13;
di   questa   Corte,   è   stato  dichiarato  ammissibile  ai  sensi &#13;
dell'art. 37 della legge 11 marzo 1953, n. 87. &#13;
    La  Corte  di  appello di Napoli ha notificato in data 5 dicembre &#13;
2000 il ricorso e l'ordinanza alla Camera dei deputati, depositandoli &#13;
poi,  con  la  prova  dell'avvenuta  notificazione, nella cancelleria &#13;
della Corte costituzionale in data 12 gennaio 2001. &#13;
    3. - Si   è   costituita  la  Camera  dei  deputati,  eccependo, &#13;
preliminarmente,  l'inammissibilità  del  conflitto, per esser stato &#13;
introdotto con la forma dell'ordinanza e non con ricorso. &#13;
    In   subordine   e  nel  merito,  la  memoria  conclude  per  una &#13;
declaratoria di spettanza alla stessa Camera del potere di dichiarare &#13;
l'insindacabilità,   ai   sensi  dell'art. 68,  primo  comma,  della &#13;
Costituzione,   delle  opinioni  espresse  dall'on. Vittorio  Sgarbi, &#13;
svolgendo  ampie argomentazioni sulla riconducibilità delle opinioni &#13;
medesime all'esercizio delle sue funzioni di parlamentare. &#13;
    Nella  memoria  illustrativa successivamente depositata la Camera &#13;
dei  deputati,  pur insistendo, in via subordinata, nelle conclusioni &#13;
già  rassegnate,  ha,  tuttavia, rilevato la tardività del deposito &#13;
del  ricorso  e  dell'ordinanza  ammissiva  del conflitto rispetto al &#13;
termine   perentorio  fissato  dall'art. 26,  comma  3,  delle  norme &#13;
integrative   per   i  giudizi  davanti  alla  Corte  costituzionale, &#13;
eccependo,  così, l'improcedibilità del conflitto stesso, alla luce &#13;
del  consolidato  orientamento  in  tal  senso  della  giurisprudenza &#13;
costituzionale.<diritto>Considerato in diritto</diritto>1. - La  Corte  di  appello  di  Napoli, sezione prima civile, ha &#13;
sollevato  conflitto  di  attribuzione nei confronti della Camera dei &#13;
deputati in riferimento alla deliberazione adottata dall'assemblea il &#13;
21 luglio  1998, che ha ritenuto che le dichiarazioni pronunciate dal &#13;
deputato  Vittorio  Sgarbi,  per  le  quali  è pendente procedimento &#13;
civile  innanzi alla medesima Corte d'appello, costituiscono opinioni &#13;
espresse   nell'esercizio   delle   sue   funzioni,  con  conseguente &#13;
insindacabilità,   a   norma   dell'art. 68,   primo   comma,  della &#13;
Costituzione. &#13;
    Ad avviso del giudice ricorrente, la predetta deliberazione della &#13;
Camera    sarebbe    lesiva   delle   attribuzioni   riservate   alla &#13;
giurisdizione,  in  quanto  fondata  su  un'erronea  valutazione  dei &#13;
presupposti  richiesti  dal  menzionato  art. 68  della Costituzione, &#13;
giacché   non  sussisterebbe  alcun  collegamento  tra  le  opinioni &#13;
espresse  dal  deputato  Sgarbi  e  la  sua funzione di parlamentare, &#13;
essendo state pronunciate al di fuori della sede parlamentare e in un &#13;
ambito completamente estraneo al dibattito politico. &#13;
    2. - Il  ricorso,  unitamente all'ordinanza n. 469 del 3 novembre &#13;
del  2000, con la quale questa Corte lo ha dichiarato ammissibile, è &#13;
stato  notificato alla Camera dei deputati, a cura del ricorrente, in &#13;
data 5 dicembre 2000. &#13;
    Il   ricorso   e   l'ordinanza,   con   la   prova  dell'avvenuta &#13;
notificazione,  sono  stati, quindi, depositati presso la cancelleria &#13;
della Corte costituzionale in data 12 gennaio 2001. &#13;
    3. - La  Camera dei deputati dopo aver sostenuto, nel costituirsi &#13;
in  giudizio,  l'inammissibilità  del  conflitto e, comunque, la sua &#13;
infondatezza,  ha,  con successiva memoria, evidenziato la tardività &#13;
del  deposito  del  ricorso  e  della  ordinanza  di  ammissibilità, &#13;
eccependo, quindi, l'improcedibilità del conflitto stesso. &#13;
    4. - L'eccezione di improcedibilità è fondata. &#13;
    Deve,  infatti,  rammentarsi  che  il  giudizio  per conflitto di &#13;
attribuzione  è articolato in due fasi autonome e distinte, entrambe &#13;
rimesse all'iniziativa della parte interessata; la prima è destinata &#13;
a  concludersi  con una sommaria delibazione sulla ammissibilità del &#13;
conflitto, mentre la seconda con una decisione definitiva sul merito, &#13;
oltre che sull'ammissibilità. &#13;
    All'esito   della   prima  fase,  il  ricorrente  ha  l'onere  di &#13;
provvedere,  nei  termini  previsti,  non solo alla notificazione del &#13;
ricorso   e  dell'ordinanza  che  ammette  il  conflitto,  ma  anche, &#13;
successivamente  alla  notificazione,  al  loro  deposito,  presso la &#13;
cancelleria  della  Corte,  nel termine - fissato dall'art. 26, terzo &#13;
comma,  delle  norme  integrative  per  i  giudizi dinanzi alla Corte &#13;
stessa - di venti giorni dall'ultima notificazione. &#13;
    Secondo    il    costante   orientamento   della   giurisprudenza &#13;
costituzionale,   il   ricordato   deposito   nel   termine  indicato &#13;
costituisce  un  necessario  adempimento affinché si possa dar luogo &#13;
alla  seconda fase del conflitto, relativa alla decisione sul merito; &#13;
tale  termine  va,  inoltre,  ritenuto  perentorio, in quanto da esso &#13;
decorre  l'intera  catena  degli  ulteriori  termini  fissati  per la &#13;
prosecuzione  del  giudizio  (tra  le altre, sentenze n. 203, n. 50 e &#13;
n. 35 del 1999). &#13;
    Nel caso all'esame, il ricorso e l'ordinanza, pur tempestivamente &#13;
notificati  in  data  5 dicembre 2000, risultano depositati presso la &#13;
cancelleria   di   questa   Corte  il  12  gennaio  2001  e,  dunque, &#13;
successivamente alla scadenza del termine di venti giorni. &#13;
    Sicché,  il  mancato  rispetto dell'anzidetto termine perentorio &#13;
per   il   deposito   non  consente  di  procedere  allo  svolgimento &#13;
dell'ulteriore fase del giudizio.</testo>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  improcedibile  il  conflitto di attribuzione tra poteri &#13;
dello  Stato proposto dalla Corte di appello di Napoli, sezione prima &#13;
civile,  nei  confronti  della  Camera  dei  deputati, con il ricorso &#13;
indicato in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Vari &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 12 luglio 2001. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
