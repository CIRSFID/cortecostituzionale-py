<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>509</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:509</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1990</data_decisione>
    <data_deposito>26/10/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. &#13;
 Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  47, primo    &#13;
 comma, della legge 26 luglio 1975,  n.  354  (Norme  sull'ordinamento    &#13;
 penitenziario  e sulla esecuzione delle misure privative e limitative    &#13;
 della libertà), così come introdotto e modificato  dalla  legge  10    &#13;
 ottobre  1986  n. 663, promosso con ordinanza emessa il 27 marzo 1990    &#13;
 dal  Tribunale  di  Sorveglianza  di  Brescia  nel  procedimento   di    &#13;
 sorveglianza  nei confronti di Trimboli Rocco, iscritta al n. 400 del    &#13;
 registro ordinanze 1990 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 26, prima serie speciale, dell'anno 1990;                  &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  Camera  di  consiglio del 10 ottobre 1990 il Giudice    &#13;
 relatore Ettore Gallo;                                                   &#13;
    Ritenuto  che,  con  ordinanza  27  marzo  1990,  il  Tribunale di    &#13;
 Sorveglianza  di  Brescia   sollevava   questione   di   legittimità    &#13;
 costituzionale  dell'art. 47, primo comma, della legge 26 luglio 1975    &#13;
 n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle    &#13;
 misure  privative e limitative della libertà), così come introdotto    &#13;
 e modificato dalla legge 10 ottobre 1986 n. 663, con riferimento agli    &#13;
 artt. 3 e 27 della Costituzione;                                         &#13;
      che nell'ordinanza espone il Tribunale che la questione riguarda    &#13;
 un condannato con unica sentenza alla pena di anni 3 e  mesi  sei  di    &#13;
 reclusione,  di  cui  anni  tre  inflitti  per il reato più grave di    &#13;
 concussione, e i residui sei mesi per  la  continuazione  relativa  a    &#13;
 reati di lieve entità di peculato e falso;                              &#13;
      che  di  tale  pena  l'interessato  ha già scontato in custodia    &#13;
 cautelare anni 1 e giorni 17, sicché la  pena  residua  è  di  gran    &#13;
 lunga inferiore ad anni tre di reclusione;                               &#13;
      che,  a  fronte  della  richiesta  di  affidamento  in  prova al    &#13;
 servizio sociale, il Tribunale ha, però, rilevato che la sentenza n.    &#13;
 386 del 1989 di questa Corte avrebbe bensì escluso dal computo della    &#13;
 "pena inflitta", ai fini dell'istituto in esame,  la  parte  di  pena    &#13;
 espiata,  purché,  però,  si  tratti  di pene irrogate con sentenze    &#13;
 diverse;                                                                 &#13;
      che,  nella  specie,  invece la pena unica è stata inflitta con    &#13;
 unica sentenza in quanto  è  stato  applicato  il  disposto  di  cui    &#13;
 all'art.  81,  secondo  comma, codice penale, sicché il Tribunale si    &#13;
 domanda  se,  trattandosi  di  reato   continuato,   il   privilegium    &#13;
 favorabile  che  lo  contraddistingue  non  debba  dispiegare  i suoi    &#13;
 effetti anche in sede esecutiva  e  perciò  tenersi  conto  soltanto    &#13;
 della pena da espiare ancorché inflitta con unica sentenza;             &#13;
      che  conseguentemente,  al  fine  di vedere risolto quest'ultimo    &#13;
 quesito,  il  Tribunale  solleva   la   questione   di   legittimità    &#13;
 costituzionale innanzi a questa Corte;                                   &#13;
    Considerato che la questione viene sollevata in forma perplessa e,    &#13;
 comunque - come bene ha osservato l'Avvocatura Generale riguarderebbe    &#13;
 la  mera  interpretazione  dell'art. 81, secondo comma, cod. pen. che    &#13;
 spetta  innanzitutto   al   giudice   di   merito   (cui,   peraltro,    &#13;
 soccorrerebbe  l'utile  indicazione  di  cui all'art. 671 cod.  proc.    &#13;
 pen.);                                                                   &#13;
      che,  tuttavia,  non può la Corte esimersi dal rilevare che, in    &#13;
 realtà, il problema è inesistente  in  quanto  frutto  di  equivoco    &#13;
 giacché,  se è vero che la sentenza n. 386 del 1989 di questa Corte    &#13;
 ha risolto una questione concernente un cumulo di pene  derivanti  da    &#13;
 più  sentenze,  è pur vero, però, che già nelle prime righe della    &#13;
 motivazione in  diritto  (par.  2)  la  sentenza  faceva  riferimento    &#13;
 all'art.  76,  primo  comma,  cod. pen. quale fonte del "cumulo delle    &#13;
 pene inflitte con una o più sentenze di condanna";                      &#13;
      che,  d'altra  parte,  non  poteva essere diversamente in quanto    &#13;
 l'art. 76, primo comma, codice penale, così come l'art. 73 che  pure    &#13;
 si  riferisce  a pene della stessa specie, sono correlati all'art. 71    &#13;
 (in testa al Capo) che disciplina proprio la condanna per più  reati    &#13;
 con  unica  sentenza:  sicché è proprio e soltanto questa l'ipotesi    &#13;
 cui gli artt. 73  e  76  cod.  pen.  si  coordinano  per  contemplare    &#13;
 l'istituto della pena unica;                                             &#13;
      che,  in  effetti,  l'estensione  del  principio di cui ai detti    &#13;
 articoli (e in particolare del primo comma dell'art. 76 cod. pen.  su    &#13;
 cui  si  svolge la citata sentenza della Corte) anche ai casi di pene    &#13;
 irrogate con più sentenze di condanna, dipende dal disposto  di  cui    &#13;
 al  successivo art. 80 cod.pen., sicché la previsione principale del    &#13;
 legislatore è proprio quella concernente l'ipotesi che preoccupa  il    &#13;
 Tribunale  rimettente, e il contenuto della decisione di questa Corte    &#13;
 a fortiori è perciò ad essa riferibile;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Visti  ed  applicati  gli  artt.  26, secondo comma, della legge 11    &#13;
 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per  i    &#13;
 giudizi davanti alla Corte Costituzionale;                               &#13;
    Dichiara la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 47, primo comma, della legge 26 luglio  1975    &#13;
 n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle    &#13;
 misure privative e limitative della libertà), così come  introdotto    &#13;
 e  modificato  dalla  legge  10  ottobre  1986  n. 663, sollevata dal    &#13;
 Tribunale di Sorveglianza di Brescia, con ordinanza 27 marzo 1990, in    &#13;
 riferimento agli artt. 3 e 27 della Costituzione.                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1990.                              &#13;
                          Il Presidente: CONSO                            &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 ottobre 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
