<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>1080</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:1080</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/11/1988</data_decisione>
    <data_deposito>06/12/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, 3, 58 e 65    &#13;
 della legge 27 luglio 1978, n. 392 ("Disciplina  delle  locazioni  di    &#13;
 immobili  urbani"),  e  dell'art. 657 del codice di procedura civile,    &#13;
 promosso con ordinanza emessa il 18  febbraio  1988  dal  Pretore  di    &#13;
 Cortina    d'Ampezzo    nel    procedimento   civile   vertente   tra    &#13;
 l'Amministrazione provinciale di Venezia e Barbato Elia, iscritta  al    &#13;
 n.  151  del  registro  ordinanze  1988  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 18,  prima  Serie  speciale,  dell'anno    &#13;
 1988;                                                                    &#13;
    Visti gli atti di costituzione dell'Amministrazione provinciale di    &#13;
 Venezia e di Barbato Elia nonché l'atto di intervento del Presidente    &#13;
 del Consiglio dei ministri;                                              &#13;
    Udito  nella  camera  di  consiglio del 9 novembre 1988 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
   Ritenuto   che   nel   corso  di  un  giudizio  avente  ad  oggetto    &#13;
 l'intimazione di licenza per finita locazione e contestuale citazione    &#13;
 per   la  convalida,  promosso  dall'Amministrazione  provinciale  di    &#13;
 Venezia, nei confronti dell' ex custode di una colonia alpina, per il    &#13;
 rilascio  di  un  immobile  ad  essa adiacente, il Pretore di Cortina    &#13;
 d'Ampezzo, con ordinanza emessa il 18 febbraio 1988, ha sollevato, in    &#13;
 relazione  all'art. 97, primo comma, della Costituzione, questione di    &#13;
 legittimità costituzionale degli artt. 1, 3, 58 e 65 della legge  27    &#13;
 luglio  1978,  n.  392,  e  657 del codice di procedura civile, nella    &#13;
 parte in cui consentono il recesso dalla locazione alla scadenza  del    &#13;
 contratto  senza  prevedere  una  giusta  causa  anche  allorché  il    &#13;
 locatore sia la pubblica Amministrazione;                                &#13;
      che  il  giudice  a quo ritiene che il principio d'imparzialità    &#13;
 debba garantire tutti i soggetti  che  vengono  in  contatto  con  la    &#13;
 pubblica  Amministrazione,  onde  quest'ultima,  al  fine  di rendere    &#13;
 possibile il controllo  del  rispetto  di  detto  principio,  sarebbe    &#13;
 tenuta a motivare tutti i provvedimenti;                                 &#13;
      che  si  sono  costituite  entrambe  le  parti,  rispettivamente    &#13;
 riservando il convenuto le  proprie  conclusioni  ad  una  successiva    &#13;
 memoria  ed  insistendo per la declaratoria d'inammissibilità, o, in    &#13;
 subordine, d'infondatezza, l'Amministrazione intimante, la  quale  ha    &#13;
 altresì depositato memoria nell'imminenza della camera di consiglio;    &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, la quale ha  concluso  per    &#13;
 la declaratoria di inammissibilità ovvero di manifesta infondatezza;    &#13;
    Considerato  che  questa  Corte ha già rilevato (sent. n. 252 del    &#13;
 1983) come la formulazione dell'art.  657  del  codice  di  procedura    &#13;
 civile, ove la si legga in termini di omessa previsione di una giusta    &#13;
 causa per la risoluzione - alla scadenza - della  locazione,  risulti    &#13;
 del  tutto  coerente  con  l'inesistenza sul piano sostanziale di una    &#13;
 corrispondente situazione soggettiva;                                    &#13;
      che  in particolare, nella citata decisione, è stata esclusa la    &#13;
 fondatezza della tesi che vorrebbe trasformare la proprietà  privata    &#13;
 in   una   funzione  pubblica,  osservandosi  come  sia  proprio  del    &#13;
 "legislatore ordinario il compito  di  introdurre,  a  seguito  delle    &#13;
 opportune  valutazioni  e  dei  necessari  bilanciamenti  dei diversi    &#13;
 interessi, quei limiti che ne assicurano la funzione sociale";           &#13;
      che,  a riguardo, la scelta per una durata minima, ma pur sempre    &#13;
 definita, della locazione, si  inserisce  nella  organica  disciplina    &#13;
 positiva  che  regola  tale rapporto, correlandosi con il complessivo    &#13;
 quadro normativo;                                                        &#13;
      che,    quindi,    sarebbe    irrazionalmente    discriminatoria    &#13;
 l'imposizione di un  contratto  a  tempo  indeterminato  soltanto  al    &#13;
 locatore-pubblica  Amministrazione,  mentre la prospettata previsione    &#13;
 di  un  obbligo  di  motivazione  anche  degli  atti  compiuti   iure    &#13;
 privatorum,  dalla  stessa, verrebbe altresì a contrastare sia con i    &#13;
 criteri che regolano l'azione di quest'ultima, sia con  il  principio    &#13;
 generale dell'irrilevanza dei motivi nel negozio giuridico;              &#13;
      che,   pertanto,   la   proposta   questione  è  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, delle norme integrative per i  giudizi  davanti  alla  Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 1, 3, 58 e 65 della legge 27 luglio  1978,    &#13;
 n. 392 ("Disciplina delle locazioni di immobili urbani"), e dell'art.    &#13;
 657 del codice di procedura civile, sollevata, in relazione  all'art.    &#13;
 97, primo comma, della Costituzione, dal Pretore di Cortina d'Ampezzo    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
    Così  deciso  in  Roma,  in  camera di consiglio nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 novembre 1988.       &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 6 dicembre 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
