<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>238</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:238</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ANDRIOLI</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/06/1987</data_decisione>
    <data_deposito>23/06/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Virgilio ANDRIOLI; &#13;
 Giudici: dott. Francesco SAJA, prof. Giovanni CONSO, prof. Ettore &#13;
 GALLO, dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 2, comma 1, n.    &#13;
 3, del d.P.R. 5 gennaio 1950, n. 180 (t.u. delle leggi concernenti il    &#13;
 sequestro,  il  pignoramento  e  la cessione degli stipendi, salari e    &#13;
 pensioni dei dipendenti delle pubbliche Amministrazioni) in relazione    &#13;
 all'art.  545,  comma  4,  cod.  proc.  civ. promossi con le seguenti    &#13;
 ordinanze:                                                               &#13;
      1)  ordinanza  emessa  il  24 maggio 1983 dal Pretore di Catania    &#13;
 nella  procedura  esecutiva  promossa  da  Marletta  Placido   contro    &#13;
 Bartolotta  Rosa  ed  altra iscritta al n. 917 del registro ordinanze    &#13;
 1983 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  67    &#13;
 dell'anno 1984;                                                          &#13;
      2) ordinanza emessa il 10 maggio 1983 dal Pretore di Catania nel    &#13;
 procedimento civile vertente tra Banca di Credito S.  Giuliano  e  La    &#13;
 Rosa  Francesco  ed  altra  iscritta al n. 918 del registro ordinanze    &#13;
 1983 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  67    &#13;
 dell'anno 1984;                                                          &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  4 giugno 1987 il giudice    &#13;
 relatore Virgilio Andrioli;                                              &#13;
                              Ritenuto che                                &#13;
   1.  - Con due ordinanze, emesse l'una il 24 maggio 1983 (comunicata    &#13;
 l'11 giugno e notificata il 20 settembre successivi; pubblicata nella    &#13;
 Gazzetta  Ufficiale  n. 67 del 7 marzo 1984 e iscritta al n. 917 reg.    &#13;
 ord. 1983) nel processo di espropriazione presso terzi  tra  Marletta    &#13;
 Placido,  Bartolotta  Rosa  e  Ferrovia  Circumetnea, e l'altra il 10    &#13;
 maggio 1983 (comunicata il 19 maggio e notificata  il  20  settembre;    &#13;
 pubblicata nella Gazzetta Ufficiale n. 67 del 7 marzo 1984 e iscritta    &#13;
 al n. 918 reg. ord. 1983) nel processo di espropriazione presso terzi    &#13;
 tra  Banca  di  credito  S.  Giuliano, e Cammarata Salvinia e La Rosa    &#13;
 Francesco,  il  Pretore  di  Catania  dichiarò   rilevante   e   non    &#13;
 manifestamente  infondata la questione di legittimità costituzionale    &#13;
 dell'art. 2 co. 1  d.P.R.  5  gennaio  1950,  n.  180,  in  relazione    &#13;
 all'art.  545  co.  4  c.p.c.  nella  parte  in  cui  non  prevede la    &#13;
 pignorabilità  e   sequestrabilità   degli   stipendi,   salari   e    &#13;
 retribuzioni  equivalenti,  nonché  delle  pensioni,  indennità che    &#13;
 tengono luogo  di  pensione  e  degli  altri  assegni  di  quiescenza    &#13;
 corrisposti dallo Stato e dagli altri enti, aziende ed imprese di cui    &#13;
 all'art. 1 del citato d.P.R., fino alla concorrenza di un quinto  per    &#13;
 ogni  credito  vantato nei confronti del personale, per contrasto con    &#13;
 l'art. 3 Cost.;                                                          &#13;
    2.  -  Avanti la Corte nessuna delle parti si costituì ma spiegò    &#13;
 in ambo gli incidenti intervento  il  Presidente  del  Consiglio  dei    &#13;
 ministri, per il quale l'Avvocatura generale dello Stato argomentò  e    &#13;
 concluse con atti depositati  il  27  marzo  1984  per  la  manifesta    &#13;
 infondatezza ovvero infondatezza della proposta questione;               &#13;
    3.  -  Nell'adunanza  del  4 giugno 1987 in camera di consiglio il    &#13;
 giudice Andrioli ha svolto relazione congiunta sui due incidenti;        &#13;
                             Considerato che                              &#13;
    4.  -  Con  sent.  31  marzo  1987,  n.  89 la Corte ha dichiarato    &#13;
 l'illegittimità costituzionale dell'art. 2  co.  1  n.  3  d.P.R.  5    &#13;
 gennaio  1950,  n. 180 (t.u. delle leggi concernenti il sequestro, il    &#13;
 pignoramento e la cessione degli  stipendi,  salari  e  pensioni  dei    &#13;
 dipendenti  delle  pubbliche  Amministrazioni) nella parte in cui, in    &#13;
 contrasto con l'art. 545 co. 5 c.p.c., non prevede la  pignorabilità    &#13;
 e   la   sequestrabilità   degli   stipendi  salari  e  retribuzioni    &#13;
 corrisposti da altri enti diversi dallo Stato, aziende ed imprese  di    &#13;
 cui  all'art.  dello stesso d.P.R. fino alla concorrenza di un quinto    &#13;
 per ogni credito vantato nei confronti del personale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 riuniti  i  due  incidenti  sollevati  dal  Pretore  di  Catania  con    &#13;
 ordinanze 24 maggio 1983 (n. 917 reg. ord. 1983) e 10 maggio 1983 (n.    &#13;
 918   reg.   ord.  1983),  dichiara  inammissibile  la  questione  di    &#13;
 costituzionalità dell'art. 2 co. 1 n. 3 d.P.R. 5  gennaio  1950,  n.    &#13;
 180 nella parte in cui, in contrasto con l'art. 545 co. 4 c.p.c., non    &#13;
 prevede la pignorabilità e la sequestrabilità di stipendi, salari e    &#13;
 retribuzioni  corrisposti  da  altri  enti  diversi  dallo  Stato, da    &#13;
 aziende ed enti di cui all'art.  1  dello  stesso  d.P.R.  fino  alla    &#13;
 concorrenza  di  un quinto per ogni credito vantato nei confronti del    &#13;
 personale, dichiarata fondata con sent. n. 89 del 1987.                  &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 giugno 1987.                               &#13;
                       Il Presidente: ANDRIOLI                            &#13;
                       Il Redattore: ANDRIOLI                             &#13;
    Depositata in cancelleria il 23 giugno 1987.                          &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
