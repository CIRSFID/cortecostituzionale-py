<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>203</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:203</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/2000</data_decisione>
    <data_deposito>16/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, Cesare &#13;
 RUPERTO, Riccardo CHIEPPA, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
 CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, &#13;
 Franco BILE, Giovanni Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 7 del decreto    &#13;
 legislativo  9  luglio  1997,  n. 237  (Modifica  della disciplina in    &#13;
 materia  di  servizi  autonomi  di  cassa  degli  uffici finanziari),    &#13;
 promosso  con  ordinanza  emessa  il  19  gennaio 1999 dal pretore di    &#13;
 Trento  nel procedimento civile vertente tra la Provincia Autonoma di    &#13;
 Bolzano  e  l'Amministrazione  delle  Finanze  ed  altre, iscritta al    &#13;
 n. 127  del  registro  ordinanze  1999  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 11,  prima serie speciale, dell'anno    &#13;
 1999;                                                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio del 10 maggio 2000 il giudice    &#13;
 relatore Piero Alberto Capotosti.                                        &#13;
     Ritenuto  che  il  pretore  di Trento, con ordinanza emessa il 19    &#13;
 gennaio  1999,  in un giudizio di opposizione all'esecuzione promossa    &#13;
 dal  concessionario  del  servizio  di  riscossione  per il pagamento    &#13;
 dell'indennizzo  per  l'occupazione  abusiva di un bene demaniale, ha    &#13;
 sollevato  questione  di  legittimità costituzionale dell'art. 7 del    &#13;
 decreto  legislativo 9 luglio 1997, n. 237 (Modifica della disciplina    &#13;
 in  materia  di  servizi  autonomi di cassa degli uffici finanziari),    &#13;
 nella parte in cui, rinviando all'art. 67 del d.P.R. 28 gennaio 1988,    &#13;
 n. 43,  che  a  sua  volta richiama l'art. 11 del d.P.R. 29 settembre    &#13;
 1973,   n. 602,   e  dunque  l'intera  normativa  in  vigore  per  la    &#13;
 riscossione  delle imposte dirette, impedisce al debitore, in caso di    &#13;
 contestazione  dell'esistenza o dell'entità del credito, di proporre    &#13;
 opposizione    all'esecuzione   davanti   all'autorità   giudiziaria    &#13;
 ordinaria  e di ottenere dalla stessa la sospensione dell'esecuzione,    &#13;
 in riferimento agli artt. 3 e 24 della Costituzione;                     &#13;
         che,  ad  avviso  del giudice rimettente, la norma impugnata,    &#13;
 rendendo  applicabile  alla  riscossione coattiva dell'indennizzo per    &#13;
 l'occupazione   abusiva   di   beni  demaniali,  che  non  ha  natura    &#13;
 tributaria,  l'art. 54,  primo e secondo comma, del d.P.R. n. 602 del    &#13;
 1973,  che  in  riferimento  alla  riscossione coattiva delle entrate    &#13;
 tributarie  esclude la proponibilità dell'opposizione all'esecuzione    &#13;
 e  la  possibilità  di ottenere dal giudice ordinario la sospensione    &#13;
 della    procedura    esecutiva,   determinerebbe   una   limitazione    &#13;
 ingiustificata  e  discriminatoria  del  diritto di difesa, aggravata    &#13;
 dalla mancata previsione di un sistema di gradualità nell'iscrizione    &#13;
 a  ruolo,  analogo  a quello applicabile in caso di contestazione dei    &#13;
 crediti tributari;                                                       &#13;
         che  tale  disparità  di trattamento sarebbe resa ancor più    &#13;
 evidente  dalla  circostanza  che,  in  mancanza  di  una  disciplina    &#13;
 legislativa   delle  modalità  di  liquidazione  dell'indennità  in    &#13;
 questione,   l'iscrizione   a   ruolo   ha   luogo  a  seguito  della    &#13;
 determinazione unilaterale ed autoritativa del credito da parte della    &#13;
 pubblica amministrazione;                                                &#13;
         che,  nel  giudizio  dinanzi  alla  Corte  costituzionale, è    &#13;
 intervenuto  il  Presidente  del  Consiglio dei Ministri, il quale ha    &#13;
 eccepito  l'infondatezza  della  questione,  sostenendo che il rinvio    &#13;
 contenuto nella norma impugnata riguarda soltanto le norme del d.P.R.    &#13;
 n. 602  del  1973  che  disciplinano  la  formazione  dei  ruoli e la    &#13;
 procedura  esecutiva,  e  non  si  estende  a quelle disposizioni che    &#13;
 trovano  giustificazione  esclusivamente  nella natura tributaria del    &#13;
 credito.                                                                 &#13;
     Considerato che la questione di legittimità costituzionale ha ad    &#13;
 oggetto l'art. 7 del decreto legislativo n. 237 del 1997, nella parte    &#13;
 in  cui,  prevedendo che alla riscossione coattiva delle somme dovute    &#13;
 per   l'utilizzazione,  anche  senza  titolo,  di  beni  demaniali  e    &#13;
 patrimoniali  dello  Stato  si  applicano  le  disposizioni contenute    &#13;
 nell'art. 67  del  d.P.R. n. 43 del 1988, che a sua volta richiama le    &#13;
 disposizioni relative alla riscossione dei tributi, rende applicabile    &#13;
 l'art. 54  del  d.P.R.  n. 602  del  1973,  il  quale  prevede che le    &#13;
 opposizioni  regolate  dagli  articoli  da  615  a  618 del codice di    &#13;
 procedura  civile  non  sono  ammesse  ed  attribuisce  il  potere di    &#13;
 sospendere l'esecuzione in via esclusiva all'intendente di finanza;      &#13;
         che,  successivamente  alla  proposizione  della questione di    &#13;
 legittimità costituzionale, il decreto legislativo 26 febbraio 1999,    &#13;
 n. 46  ha  riordinato la disciplina della riscossione mediante ruolo,    &#13;
 disponendo  che  si effettua con tale sistema la riscossione coattiva    &#13;
 delle  entrate  dello  Stato, anche diverse dalle imposte sui redditi    &#13;
 (art. 17),  e  sostituendo  l'intero  Titolo II del d.P.R. n. 602 del    &#13;
 1973,  avente  ad  oggetto  la  riscossione  coattiva, e quindi anche    &#13;
 l'art. 54;                                                               &#13;
         che  gli  artt. 57 e 60 del d.P.R. n. 602 del 1973, nel testo    &#13;
 novellato   dall'art. 16   del  d.lgs.  n. 46  del  1999,  confermano    &#13;
 l'improponibilità   delle  opposizioni  regolate  dall'art. 615  del    &#13;
 codice di procedura civile, fatta eccezione per quelle concernenti la    &#13;
 pignorabilità  dei  beni, e delle opposizioni regolate dall'art. 617    &#13;
 del  codice  di procedura civile relative alla regolarità formale ed    &#13;
 alla  notificazione  del  titolo esecutivo, prevedendo inoltre che il    &#13;
 giudice  dell'esecuzione  non  può sospendere il processo esecutivo,    &#13;
 salvo che ricorrano gravi motivi e vi sia fondato pericolo di grave e    &#13;
 irreparabile danno;                                                      &#13;
         che,  in  particolare,  l'art. 29  del  d.lgs. n. 46 del 1999    &#13;
 prevede  che "per le entrate () non tributarie, il giudice competente    &#13;
 a  conoscere  le controversie concernenti il ruolo può sospendere la    &#13;
 riscossione  se ricorrono gravi motivi", disponendo altresì che alle    &#13;
 medesime  entrate  "non  si  applica  la  disposizione  del  comma  1    &#13;
 dell'articolo  57  del  decreto  del  Presidente  della Repubblica 29    &#13;
 settembre 1973, n. 602, come sostituito dall'articolo 16 del presente    &#13;
 decreto  e  le  opposizioni  all'esecuzione ed agli atti esecutivi si    &#13;
 propongono  nelle forme ordinarie", ed aggiungendo che "ad esecuzione    &#13;
 iniziata  il  giudice può sospendere la riscossione solo in presenza    &#13;
 dei  presupposti  di cui all'art. 60 del decreto del Presidente della    &#13;
 Repubblica  29  settembre 1973, n. 602, come sostituito dall'articolo    &#13;
 16 del presente decreto";                                                &#13;
         che   all'indennizzo   per   l'occupazione  abusiva  di  beni    &#13;
 demaniali  è  quindi  applicabile, in parte qua il predetto art. 29,    &#13;
 trattandosi di entrata non avente natura tributaria;                     &#13;
         che,  in  seguito,  il  decreto  legislativo  13 aprile 1999,    &#13;
 n. 112 ha riordinato il servizio nazionale della riscossione mediante    &#13;
 ruolo,   disponendo  espressamente,  all'art. 68,  l'abrogazione  del    &#13;
 d.P.R. n. 43 del 1988;                                                   &#13;
         che  le  norme  sopravvenute  hanno  determinato un mutamento    &#13;
 complessivo  del  quadro normativo di riferimento, tale da imporre il    &#13;
 riesame  della  perdurante  rilevanza della questione di legittimità    &#13;
 costituzionale  da  parte del giudice a quo (cfr. ordinanze nn. 439 e    &#13;
 441 del 1999).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Ordina la restituzione degli atti al pretore di Trento.              &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 giugno 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 16 giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
