<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>285</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:285</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/05/1989</data_decisione>
    <data_deposito>25/05/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. &#13;
 Luigi MENGONI, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  degli artt. 1, primo    &#13;
 comma e 5, ultimo comma, della legge 7 maggio 1981, n. 180 (Modifiche    &#13;
 all'ordinamento  giudiziario militare di pace) promosso con ordinanza    &#13;
 emessa il 10 giugno 1988 dal Procuratore militare della Repubblica di    &#13;
 Cagliari  nel  procedimento  penale  a  carico  di Calvanese Luciano,    &#13;
 iscritta al n. 401 del registro ordinanze  1988  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  38, prima serie speciale,    &#13;
 dell'anno 1988;                                                          &#13;
    Visto   l'atto  d'intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio dell'11 gennaio 1989 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che con ordinanza 10 giugno 1988 il Procuratore militare    &#13;
 della Repubblica di Cagliari, nel corso di  un'istruttoria  sommaria,    &#13;
 ha  sollevato,  in  riferimento  all'art.  108, secondo comma, Cost.,    &#13;
 questione di legittimità costituzionale degli artt. 1, primo comma e    &#13;
 5,  ultimo  comma,  della  legge  7  maggio  1981,  n. 180 (Modifiche    &#13;
 all'ordinamento giudiziario militare di  pace)  nella  parte  in  cui    &#13;
 consentono  che  un  magistrato  militare  con  funzioni  di pubblico    &#13;
 ministero possa iniziare ed  esercitare  l'azione  penale  e  privare    &#13;
 l'imputato  della libertà personale pur essendo privo, a causa della    &#13;
 mancata  istituzione  dell'organo  d'autogoverno  della  magistratura    &#13;
 militare,  della  garanzia  d'indipendenza  che  gli  dovrebbe essere    &#13;
 assicurata per legge;                                                    &#13;
      che,  in  particolare,  il  Procuratore  militare  remittente  -    &#13;
 ricordato che analoga questione di legittimità costituzionale,  già    &#13;
 sollevata  in  altri procedimenti, è stata dichiarata manifestamente    &#13;
 infondata dal Tribunale militare di  Cagliari  ed  irrilevante  dalla    &#13;
 Corte   militare   d'appello,  sezione  distaccata  di  Verona  -  ha    &#13;
 osservato:                                                               &#13;
       1)  che,  a  suo  parere, la sentenza n. 266 del 1988 di questa    &#13;
 Corte ha accertato che i magistrati militari sono rimasti privi delle    &#13;
 garanzie d'indipendenza richieste dalla Costituzione;                    &#13;
       2)  che,  alla stregua di questa interpretazione della suddetta    &#13;
 sentenza, è  da  ravvisarsi  l'illegittimità  costituzionale  delle    &#13;
 norme impugnate, nella parte in cui consentono che possano continuare    &#13;
 a svolgere attività  giurisdizionale  organi  privi  della  garanzia    &#13;
 d'indipendenza;                                                          &#13;
       3)  che,  invero,  non  potrebbe  ritenersi  che  i  magistrati    &#13;
 militari, dichiarati dalla precitata sentenza  privi  delle  garanzie    &#13;
 d'indipendenza,  siano  invece  indipendenti sol perché in fatto non    &#13;
 dipendono da nessuno;                                                    &#13;
       4)  che, infine, nemmeno potrebbe replicarsi che al legislatore    &#13;
 debba essere concesso un tempo ragionevole e congruo per istituire un    &#13;
 organo  d'autogoverno della magistratura militare, avendo la predetta    &#13;
 sentenza ritenuto non  più  tollerabile  che,  a  quasi  sette  anni    &#13;
 dall'entrata  in  vigore  della  legge n. 180 del 1981, non sia stata    &#13;
 data completa attuazione all'art. 108 Cost.;                             &#13;
    Considerato  che  il pubblico ministero, in quanto privo di poteri    &#13;
 decisori (e, in  particolare,  del  potere  d'emettere  provvedimenti    &#13;
 restrittivi della libertà personale) non è legittimato a promuovere    &#13;
 giudizi di legittimità costituzionale (cfr. sentenze di questa Corte    &#13;
 nn.  40, 41 e 42 del 1963 ed ordinanze n. 186 del 1971, n. 5 del 1979    &#13;
 e n. 163 del 1981);                                                      &#13;
      che,   pertanto,   la   proposta   questione   di   legittimità    &#13;
 costituzionale va dichiarata manifestamente inammissibile per difetto    &#13;
 di   legittimazione  del  procuratore  militare  della  Repubblica  a    &#13;
 sollevarla;                                                              &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt. 1, primo comma, e  5,  ultimo    &#13;
 comma,  della  legge 7 maggio 1981, n. 180 (Modifiche all'ordinamento    &#13;
 giudiziario militare di pace) in riferimento  all'art.  108,  secondo    &#13;
 comma,  Cost., sollevata dal Procuratore militare della Repubblica di    &#13;
 Cagliari con l'ordinanza indicata in epigrafe.                           &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 17 maggio 1989.         &#13;
                          Il Presidente: CONSO                            &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 25 maggio 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
