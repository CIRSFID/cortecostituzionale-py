<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>29</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:29</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/1982</data_decisione>
    <data_deposito>11/02/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - &#13;
 Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 14 e 182  &#13;
 del codice penale militare di pace (estranei alle  forze  armate  dello  &#13;
 Stato.  Attività  sediziosa)  e dell'art. 266, n. 1, del codice penale  &#13;
 (istigazione di  militari  a  disobbedire  alle  leggi),  promosso  con  &#13;
 ordinanza  emessa il 1 dicembre 1976 dal tribunale di Ascoli Piceno nei  &#13;
 due procedimenti penali riuniti a carico di Zazzetta Giustino ed  altri  &#13;
 e  di  Abbate  Francesco  ed  altri,  iscritta  al  n. 380 del registro  &#13;
 ordinanze 1977 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 279 del 12 ottobre 1977.                                              &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  24  novembre  1981  il  Giudice  &#13;
 relatore Michele Rossano;                                                &#13;
     udito l'avvocato dello Stato Franco Chiarotti per il Presidente del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel   corso   dei  due  procedimenti  penali  riuniti  -  a  carico  &#13;
 rispettivamente di Zazzetta Giustino  ed  altri  quattro  imputati  dei  &#13;
 reati  di  cui  agli  artt.  81 cpv. cod. pen., 14 (estranei alle forze  &#13;
 armate dello Stato) e 182 (attività sediziosa) c.p.m.p.,  266,  n.  1,  &#13;
 cod.  pen.  (istigazione  di  militari  a disobbedire alle leggi); e di  &#13;
 Abbate Francesco ed altri imputati del reato di  cui  agli  artt.  595,  &#13;
 110,  112,  n.  1,  cod.  pen.  e  13  legge  8  febbraio  1948,  n. 47  &#13;
 (disposizioni sulla stampa)  -  il  tribunale  di  Ascoli  Piceno,  con  &#13;
 ordinanza   pronunciata   all'udienza  1  dicembre  1976,  ha  ritenuto  &#13;
 rilevante ai fini della decisione e  non  manifestamente  infondata  la  &#13;
 questione di legittimità costituzionale degli artt. 14 e 182 c.p.m.p.,  &#13;
 sollevata  dal  P.M.  in  riferimento agli artt. 3, 21, 52, cpv., della  &#13;
 Costituzione; e la questione di legittimità  costituzionale  dell'art.  &#13;
 266  cod.  pen., sollevata dai difensori degli imputati, in riferimento  &#13;
 all'art. 21, comma primo, della Costituzione.                            &#13;
     L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n. 279 del  &#13;
 12 ottobre 1977.                                                         &#13;
     Nel giudizio davanti a questa Corte non si sono costituite le parti  &#13;
 private.                                                                 &#13;
     È   intervenuto   il   Presidente   del  Consiglio  dei  ministri,  &#13;
 rappresentato e difeso dall'Avvocato generale  dello  Stato,  con  atto  &#13;
 depositato   il   16   giugno  1977,  chiedendo  che  le  questioni  di  &#13;
 legittimità costituzionale siano dichiarate non fondate.<diritto>Considerato in diritto</diritto>:                          &#13;
     Le  questioni  come  proposte   nell'ordinanza   di   rinvio   sono  &#13;
 inammissibili.                                                           &#13;
     La  specificazione  della  fattispecie  concreta,  che  consenta di  &#13;
 determinare l'oggetto del giudizio di costituzionalità, è  essenziale  &#13;
 nei  giudizi incidentali di legittimità costituzionale, dovendo questa  &#13;
 Corte giudicare se la norma, della cui legittimità  costituzionale  si  &#13;
 dubita,   sia  applicabile  dal  giudice  per  la  tutela  del  diritto  &#13;
 soggettivo e dell'interesse legittimo o sia invece  estranea  al  thema  &#13;
 decidendum (sentenze nn. 45 e 60 del 1972; n. 44 del 1975; nn. 49 e 134  &#13;
 del 1980; nn. 119, 178 e 180 del 1981).                                  &#13;
     L'ordinanza   del  tribunale  di  Ascoli  Piceno  omette  qualsiasi  &#13;
 precisazione in proposito e fa  riferimento,  per  quanto  concerne  le  &#13;
 questioni di legittimità costituzionale degli artt. 14 e 182 c.p.m.p.,  &#13;
 alla  requisitoria  del  pubblico  ministero da ritenere "integralmente  &#13;
 riportata e trascritta".  Anche a prescindere  da  ogni  considerazione  &#13;
 sulla  motivazione "per relationem" la stessa requisitoria del P.M.  si  &#13;
 limita, senza in alcun modo riferirsi  alla  fattispecie  concreta,  ad  &#13;
 enunciare i motivi per i quali ritiene, in astratto, gli artt. 14 e 182  &#13;
 in contrasto con gli artt. 3, 21, 52 cpv. della Costituzione.            &#13;
     Qualsiasi precisazione della fattispecie concreta manca anche nella  &#13;
 parte dell'ordinanza di rinvio concernente la questione di legittimità  &#13;
 costituzionale  dell'art.  266  cod. pen.   in riferimento all'art. 21,  &#13;
 comma primo, della Costituzione, sollevata dai difensori degli imputati  &#13;
 all'udienza 1 dicembre 1975 senza  neppure  indicare  gli  elementi  di  &#13;
 fatto.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibili le questioni di legittimità costituzionale  &#13;
 degli artt. 14 e 182 c.p.m.p., in riferimento agli artt. 3, 21, 52 cpv.  &#13;
 della Costituzione, e dell'art. 266 cod. pen., in riferimento  all'art.  &#13;
 21,  comma  primo, della Costituzione, proposte dal tribunale di Ascoli  &#13;
 Piceno con l'ordinanza indicata in epigrafe.                             &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 20 gennaio 1982.                              &#13;
                                   F.to:   LEOPOLDO   ELIA   -   EDOARDO  &#13;
                                   VOLTERRA - MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO -  GUGLIELMO  ROEHRSSEN  -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO SAJA.     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
