<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>306</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:306</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GALLO E.</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/06/1991</data_decisione>
    <data_deposito>26/06/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Ettore GALLO; &#13;
 Giudici: dott. Aldo CORASANITI, dott. Francesco GRECO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art.  418,  primo  e    &#13;
 secondo comma, del codice di procedura penale, promosso con ordinanza    &#13;
 emessa  il  15  ottobre  1990 dal Giudice per le indagini preliminari    &#13;
 presso il Tribunale  di  Ancona  nel  processo  penale  a  carico  di    &#13;
 Lucchetta Giancarlo, iscritta al n. 195 del registro ordinanze 1991 e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 14, prima    &#13;
 serie speciale, dell'anno 1991;                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  5 giugno 1991 il Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto che il Giudice per  le  indagini  preliminari  presso  il    &#13;
 Tribunale  di  Ancona,  cui  il Pubblico ministero aveva richiesto la    &#13;
 "fissazione dell'udienza preliminare", premesso che: la richiesta "si    &#13;
 fonda sulla lettera dell'art. 418"  del  nuovo  codice  di  procedura    &#13;
 penale,  e  che,  quindi,  il  giudice  è  tenuto,  per  un verso, a    &#13;
 procedere alla fissazione della detta udienza entro due giorni  dalla    &#13;
 richiesta  (primo  comma)  e, per un altro verso, a stabilire la data    &#13;
 dell'udienza entro un termine non superiore  a  trenta  giorni  dalla    &#13;
 richiesta   stessa;   che  "i  detti  termini  sembrano  univocamente    &#13;
 perentori  per  la  stessa  tassativa   dizione   legislativa   ('non    &#13;
 superiore'),  quindi  a  pena  di decadenza, suscettibili di relativa    &#13;
 eccezione nell'ipotesi di mancata osservanza degli stessi  (art.  173    &#13;
 stesso Cod.) e non passibili di proroga, non consentendolo il dettato    &#13;
 della legge"; e che "la detta perentorietà ed esiguità dei termini"    &#13;
 vulnera  gli artt. 2 e 97 della Costituzione, "norma quest'ultima che    &#13;
 tramite l'organizzazione dei pubblici uffici secondo disposizioni  di    &#13;
 legge  assicura  il buon andamento della pubblica amministrazione, in    &#13;
 quanto al contrario la detta esiguità è fonte di grande disservizio    &#13;
 costringendo", da un lato, "la Cancelleria dell'Ufficio del G.I.P.  e    &#13;
 gli  Ufficiali  Giudiziari  nonché  talvolta  la polizia giudiziaria    &#13;
 (art. 148,  2°  co.  nuovo  C.P.P.)  ad  un  eccesso  di  lavoro"  e,    &#13;
 dall'altro  lato,  "il Giudice competente ad un 'intasamento' del suo    &#13;
 ruolo d'udienza" e ciò a scapito  delle  "esigenze  qualitative  del    &#13;
 lavoro",  con  la  frequente  necessità  di  "rinvii  dell'udienza",    &#13;
 cosicché  il  rispetto  dei  trenta  giorni  "diviene  a  tal  punto    &#13;
 meramente  formale", quando, invece, "una interpretazione del termine    &#13;
 in senso meramente ordinatorio" si adeguerebbe al principio  di  buon    &#13;
 andamento;  tutto  ciò  premesso,  ha,  con ordinanza del 15 ottobre    &#13;
 1990,   sollevato,   in   riferimento   agli    indicati    parametri    &#13;
 costituzionali,  questioni  di  legittimità  dell'art.  418, primo e    &#13;
 secondo  comma,  del  codice  di  procedura  penale  -  questioni che    &#13;
 investono, "ad avviso dello Scrivente, la funzionalità genetica  del    &#13;
 processo   penale   giunto   allo  stadio  dell'udienza  preliminare,    &#13;
 eccezione ovviamente reiterabile e reiterata in tutti i  processi  in    &#13;
 itinere  ragion  per  cui, sempre ad avviso di chi scrive, si pone il    &#13;
 problema  dell'urgenza  della  trattazione  del  presente  instaurato    &#13;
 procedimento" - "laddove stabilisce termini perentori per l'emissione    &#13;
 del  decreto di citazione a giudizio e per la fissazione dell'udienza    &#13;
 preliminare";                                                            &#13;
      e che nel giudizio è intervenuto il  Presidente  del  Consiglio    &#13;
 dei  ministri,  rappresentato e difeso dall'Avvocatura Generale dello    &#13;
 Stato, chiedendo che le questioni siano dichiarate  non  fondate,  in    &#13;
 quanto  con  esse  vengono  denunciate  soltanto  "probabili  carenze    &#13;
 organizzative nell'ambito degli uffici giudiziari";                      &#13;
    Considerato che questa Corte, con ordinanza n.  243  del  1991  ha    &#13;
 già  dichiarato  la  manifesta  inammissibilità  della questione di    &#13;
 legittimità costituzionale dell'art. 418, secondo comma, del  codice    &#13;
 di  procedura  penale, sollevata dallo stesso Giudice per le indagini    &#13;
 preliminari presso il Tribunale di Ancona, per essersi il  giudice  a    &#13;
 quo  limitato  a prospettare genericamente la eccessiva brevità "del    &#13;
 termine   stabilito   dalla   norma   denunciata,   richiedendo   una    &#13;
 inammissibile  pronuncia  additiva  che  provveda  a rideterminare la    &#13;
 durata, così sostituendosi  integralmente  nelle  scelte  che  vanno    &#13;
 riservate al legislatore";                                               &#13;
      e  che  la  ratio  decidendi  di tale pronuncia è riferibile, a    &#13;
 fortiori, alla previsione dell'art. 418, primo comma, del  codice  di    &#13;
 procedura  penale,  considerato lo stretto rapporto ravvisabile tra i    &#13;
 due termini l'uno dei quali diviene rilevante ai fini del computo del    &#13;
 termine "finale" contemplato nell'art. 418, secondo comma;               &#13;
      che,  di  conseguenza,  entrambe  le  questioni  devono   essere    &#13;
 dichiarate manifestamente inammissibili;                                 &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   delle   questioni   di    &#13;
 legittimità costituzionale dell'art. 418, primo e secondo comma, del    &#13;
 codice  di procedura penale, sollevate, in riferimento agli artt. 2 e    &#13;
 97 della Costituzione, dal Giudice per le indagini preliminari presso    &#13;
 il Tribunale di Ancona con ordinanza del 15 ottobre 1990.                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, 17 giugno 1991.                                  &#13;
                         Il Presidente: GALLO                             &#13;
                        Il redattore: VASSALLI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 giugno 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
