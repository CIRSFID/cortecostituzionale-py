<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>249</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:249</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/02/1988</data_decisione>
    <data_deposito>03/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 79, ottavo    &#13;
 comma, e 80, dodicesimo comma, del d.P.R.  15  giugno  1959,  n.  393    &#13;
 (Testo   unico   delle  norme  sulla  circolazione  stradale),  quali    &#13;
 sostituiti dagli artt. 1 e 2 della  legge  14  gennaio  1974,  n.  62    &#13;
 (Sostituzione  degli  articoli 79, 80, 86, 124 e 127 e modifiche agli    &#13;
 articoli 81, 87, 88, 138 e 141 del  testo  unico  delle  norme  sulla    &#13;
 circolazione  stradale  approvato  con  decreto  del Presidente della    &#13;
 Repubblica 15 giugno 1959, n. 393,  anche  in  relazione  alle  norme    &#13;
 previste  dal  regolamento C.E.E. n. 543 del 25 marzo 1969), promosso    &#13;
 con ordinanza emessa il 15 marzo 1985 dal Tribunale  di  Sondrio  nel    &#13;
 procedimento  penale  a carico di Gianera Giacomo, iscritta al n. 472    &#13;
 del registro ordinanze 1985 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 291- bis dell'anno 1985.                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 16 dicembre 1987 il Giudice    &#13;
 relatore Giovanni Conso;                                                 &#13;
    Ritenuto  che  il Tribunale di Sondrio, con ordinanza del 15 marzo    &#13;
 1985, emessa nel corso del procedimento penale a  carico  di  Gianera    &#13;
 Giacomo,  appellante avverso sentenza del Pretore di Chiavenna che lo    &#13;
 aveva condannato per il reato di incauto affidamento di autoveicolo a    &#13;
 persona  minore  degli  anni  diciotto  ("sfornita,  naturalmente, di    &#13;
 patente di guida"), ha sollevato, in riferimento,  all'art.  3  della    &#13;
 Costituzione, questione di legittimità degli artt. 79, ottavo comma,    &#13;
 e 80, undicesimo comma, del d.P.R. 15  giugno  1959,  n.  393,  quali    &#13;
 sostituiti  dagli  artt.1  e  2  della legge 14 febbraio 1974, n. 62,    &#13;
 nella parte in cui  puniscono  l'incauto  affidamento  di  veicolo  a    &#13;
 persona  minore  degli  anni diciotto (e, quindi, non in possesso dei    &#13;
 requisiti  per  conseguire  la  patente  di  guida)  meno  gravemente    &#13;
 dell'incauto  affidamento  di  veicolo  a persona che, pur non avendo    &#13;
 conseguito la patente di guida, sia in  possesso  dei  requisiti  per    &#13;
 ottenerla,   dando   luogo   ad   una  inaccettabile  "disparità  di    &#13;
 trattamento punitivo in relazione ad una ipotesi  più  grave  punita    &#13;
 con pena inferiore";                                                     &#13;
      e  che  nel  giudizio è intervenuto il Presidente del Consiglio    &#13;
 dei ministri, rappresentato e difeso dall'Avvocatura  Generale  dello    &#13;
 Stato,  chiedendo  che  la  questione  sia dichiarata inammissibile o    &#13;
 infondata;                                                               &#13;
    Considerato  che  il giudice a quo, con il dolersi del trattamento    &#13;
 sanzionatorio previsto in  ordine  alla  contravvenzione  di  incauto    &#13;
 affidamento  di  autoveicolo  a  persona  minore  degli anni diciotto    &#13;
 ("naturalmente" sfornita  di  patente  di  guida),  ha,  in  realtà,    &#13;
 denunciato  il  solo art.79, ottavo comma, del d.P.R. 15 giugno 1959,    &#13;
 n.393, quale sostituito dall'art.1 della legge 14 gennaio 1974, n.62,    &#13;
 coinvolgendo  l'art.  80  dello stesso d.P.R. 15 giugno 1959, n. 392,    &#13;
 quale sostituito dall'art. 2 della legge  14  gennaio  1974,  n.  62,    &#13;
 unicamente  come  tertium  comparationis della dedotta illegittimità    &#13;
 della norma censurata;                                                   &#13;
      e  che  la proposta questione tende a conseguire un aggravamento    &#13;
 della  pena  prevista  per  il  reato  di  incauto   affidamento   di    &#13;
 autoveicolo  a  persona  minore  degli  anni diciotto ("naturalmente"    &#13;
 sfornita di patente di guida), in modo, quanto meno,  di  conformarla    &#13;
 alla  misura della pena comminata per il reato di incauto affidamento    &#13;
 di autoveicolo a persona che, pur non essendo abilitata  alla  guida,    &#13;
 sia  in possesso dei requisiti prescritti dalla legge per ottenere la    &#13;
 relativa abilitazione;                                                   &#13;
      che,  peraltro,  il tipo di decisione richiesto va al di là dei    &#13;
 poteri spettanti a questa Corte, essendole precluso, dal fondamentale    &#13;
 ed  inderogabile  principio  di  stretta  legalità dei reati e delle    &#13;
 pene, qualsiasi intervento che comporti l'aggravamento  di  una  pena    &#13;
 prevista in astratto (v. sentenze n. 42 del 1977 e n. 108 del 1981);     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 79, ottavo comma,  del  decreto    &#13;
 del  Presidente  della  Repubblica 15 giugno 1959, n.393 (Testo unico    &#13;
 delle norme sulla circolazione stradale), quale sostituito dall'art.1    &#13;
 della  legge  14  gennaio 1974, n.62 (Sostituzione degli articoli 79,    &#13;
 80, 86, 124 e 127 e modifiche agli articoli 81, 87, 88, 138 e 141 del    &#13;
 testo  unico  delle  norme  sulla circolazione stradale approvato con    &#13;
 decreto del Presidente della Repubblica 15 giugno 1959, n. 393, anche    &#13;
 in relazione alle norme previste dal regolamento C.E.E. n. 543 del 25    &#13;
 marzo 1969), questione sollevata, in  riferimento  all'art.  3  della    &#13;
 Costituzione,  dal  Tribunale  di  Sondrio con ordinanza del 15 marzo    &#13;
 1985.                                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 febbraio 1988.                             &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: CONSO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 3 marzo 1988.                            &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
