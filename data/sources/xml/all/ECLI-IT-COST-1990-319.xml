<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>319</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:319</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/06/1990</data_decisione>
    <data_deposito>05/07/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 21, ultimo    &#13;
 comma,  della  legge  24  dicembre  1969,   n.   990   (Assicurazione    &#13;
 obbligatoria    della    responsabilità   civile   derivante   dalla    &#13;
 circolazione  dei  veicoli  a  motore  e  dei  natanti),  nel   testo    &#13;
 modificato  dal  decreto-legge  23 dicembre 1976, n. 857, convertito,    &#13;
 con modificazioni, nella legge 26 febbraio 1977, n. 39, promosso  con    &#13;
 ordinanza  emessa  il 31 ottobre 1989 dalla Corte d'appello di Trento    &#13;
 nei  procedimenti  civili  riuniti  vertenti  tra  la  s.p.a.  UNIASS    &#13;
 Assicurazioni  in  nome  dell'I.N.A.  -  Fondo di garanzia ed altri e    &#13;
 l'I.N.A.I.L. ed altri, iscritta al n. 677 del registro ordinanze 1989    &#13;
 e  pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 3, prima    &#13;
 serie speciale, dell'anno 1990;                                          &#13;
    Udito  nella  camera  di  consiglio  del  21 marzo 1990 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto che la Corte d'appello di Trento, con ordinanza emessa il    &#13;
 31  ottobre  1989,  ha  sollevato,  in  relazione  all'art.  3  della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art. 21,    &#13;
 ultimo comma, della  legge  24  dicembre  1969,  n.  990  (nel  testo    &#13;
 modificato  dal  decreto-legge  23 dicembre 1976, n. 857, convertito,    &#13;
 con modificazioni, nella legge 26 febbraio 1977, n. 39);                 &#13;
      che il giudice a quo espone quanto segue: 1) con sentenza penale    &#13;
 divenuta irrevocabile, era stata accertata la                            &#13;
 responsabilità  di Zonta Renzo in relazione ad un incidente stradale    &#13;
 che aveva cagionato la morte di  due  persone;  2)  il  Tribunale  di    &#13;
 Trento,  adito  in  rivalsa  dall'I.N.A.I.L. aveva accolto la domanda    &#13;
 condannando  al  rimborso  delle   somme   erogate   il   commissario    &#13;
 liquidatore  della  compagnia  assicuratrice  del danneggiante (posta    &#13;
 medio tempore in liquidazione  coatta  amministrativa)  e  la  s.p.a.    &#13;
 UNIASS  Assicurazioni  (in  nome  e  per conto dell'I.N.A. - Fondo di    &#13;
 garanzia); 3) quest'ultima aveva  appellato  la  decisione  assumendo    &#13;
 l'estinzione  dell'obbligazione  verso  l'I.N.A.I.L.  per effetto dei    &#13;
 versamenti effettuati a titolo  di  provvisionale  agli  eredi  delle    &#13;
 vittime, i quali, a loro volta, avevano proposto separato gravame;       &#13;
      che,  ammontando  all'epoca  del  sinistro il massimale previsto    &#13;
 dalla Tabella A allegata alla  legge  n.  990  del  1969  a  quaranta    &#13;
 milioni  di  lire,  il  giudice  remittente dubita della legittimità    &#13;
 costituzionale della previsione normativa che affida la rivalutazione    &#13;
 delle   somme   alla   periodica   revisione   tramite  provvedimenti    &#13;
 legislativi;                                                             &#13;
      che,   secondo   la  Corte  d'appello,  la  situazione  parrebbe    &#13;
 deteriore rispetto a quella delle vittime  d'incidenti  cagionati  da    &#13;
 veicolo non identificato cui si riferisce la sentenza n. 560 del 1987    &#13;
 di questa Corte (dichiarativa  dell'illegittimità  del  primo  comma    &#13;
 della  norma  impugnata),  laddove  identica  sarebbe  l'esigenza  di    &#13;
 garantire una reale copertura del rischio, dovendosi tener conto  del    &#13;
 massimale vigente al momento del sinistro.                               &#13;
    Considerato  che  questa Corte, nella sentenza n. 560 del 1987, ha    &#13;
 nettamente distinto l'ipotesi del sinistro  cagionato  da  veicolo  o    &#13;
 natante  non identificato dalle altre due previsioni d'intervento del    &#13;
 Fondo di garanzia per le vittime della strada (veicolo o natante  non    &#13;
 assicurato   nonché   stato   di  liquidazione  coatta  dell'impresa    &#13;
 assicuratrice);                                                          &#13;
      che  proprio il meccanismo di adeguamento dei minimi di garanzia    &#13;
 attuato, per i casi da  ultimo  citati,  attraverso  l'emanazione  di    &#13;
 specifici  provvedimenti,  succedutisi  nel  tempo,  ha consentito di    &#13;
 porre in risalto l'irrazionalità dell'omessa collocazione nel flusso    &#13;
 temporale  del  risarcimento di cui alla prima ipotesi e di pervenire    &#13;
 alla conseguente declaratoria d'illegittimità costituzionale;           &#13;
      che,  quindi,  per  la  ritenuta  congruità  di  tale sistema -    &#13;
 consistente  in  periodiche  ponderazioni  dei  valori  monetari  del    &#13;
 risarcimento  (cfr.  da ultimo il d.P.R. 9 febbraio 1990, in Gazzetta    &#13;
 Ufficiale n. 141 del 19 giugno 1990, che aumenta i limiti  de  quibus    &#13;
 in  conformità  alla  direttiva  C.E.E. del 30 dicembre 1983) - deve    &#13;
 ritenersi  manifestamente  infondata  la   questione   sollevata   in    &#13;
 riferimento  all'art.  21, ultimo comma, della legge n. 990 del 1969,    &#13;
 nella parte in cui richiama la lettera c), dell'art. 19 della  stessa    &#13;
 legge;                                                                   &#13;
      che,  a  sua  volta, la questione concernente il medesimo comma,    &#13;
 nella parte in cui richiama la lettera b) di tale legge  risulta  del    &#13;
 tutto  irrilevante  nel giudizio a quo, onde la prospettata questione    &#13;
 è manifestamente inammissibile;                                         &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 21, ultimo comma, della  legge  24  dicembre    &#13;
 1969, n. 990 (Assicurazione obbligatoria della responsabilità civile    &#13;
 derivante dalla circolazione dei veicoli a  motore  e  dei  natanti),    &#13;
 nella  parte  in  cui  richiama  l'art.  19, lettera c), della stessa    &#13;
 legge, sollevata, in riferimento all'art. 3 della Costituzione, dalla    &#13;
 Corte d'appello di Trento con l'ordinanza in epigrafe;                   &#13;
    Dichiara   la   manifesta   inammissibilità  della  questione  di    &#13;
 legittimità costituzionale dell'art. 21, ultimo comma,  della  legge    &#13;
 medesima,  nella  parte  in cui richiama l'art. 19, lettera b), della    &#13;
 legge n. 990 del 1969, sollevata, in  riferimento  all'art.  3  della    &#13;
 Costituzione, con la stessa ordinanza in epigrafe.                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 giugno 1990.                               &#13;
                          Il Presidente: CONSO                            &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 5 luglio 1990.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
