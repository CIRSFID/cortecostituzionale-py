<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>240</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:240</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/06/2000</data_decisione>
    <data_deposito>23/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, Cesare &#13;
 RUPERTO, Riccardo CHIEPPA, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
 CONTRI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni &#13;
 Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 665, commi 4 e    &#13;
 4-bis  del  codice di procedura penale, promosso con ordinanza emessa    &#13;
 il  26  ottobre  1999  dal  Tribunale  di  Gela  nel  procedimento di    &#13;
 esecuzione  nei  confronti  di M. A., iscritta al n. 672 del registro    &#13;
 ordinanze 1999 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 50, prima serie speciale, dell'anno 1999.                             &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri.                                                                &#13;
     Udito  nella  camera  di  consiglio del 10 maggio 2000 il giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
     Ritenuto  che  il  Tribunale  di Gela ha sollevato in qualità di    &#13;
 giudice  dell'esecuzione,  in  riferimento agli articoli 3 e 24 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art. 665,    &#13;
 commi  4  e  4-bis del codice di procedura penale, nella parte in cui    &#13;
 non  prevede  che, se l'esecuzione concerne più provvedimenti emessi    &#13;
 da  giudici diversi, in composizione monocratica e collegiale, sia in    &#13;
 ogni caso competente il giudice in composizione collegiale;              &#13;
         che il rimettente premette:                                      &#13;
           di  essere investito della richiesta del pubblico ministero    &#13;
 e  della  difesa  di revoca di una sentenza penale di condanna per il    &#13;
 delitto  di oltraggio a pubblico ufficiale del pretore di Caltagirone    &#13;
 (divenuta irrevocabile il 17 marzo 1999) per sopravvenuta abrogazione    &#13;
 della  norma  incriminatrice,  disposta  dall'art. 18, comma 1, della    &#13;
 legge 25 giugno 1999, n. 205;                                            &#13;
         che  la  richiesta di revoca "appare manifestamente infondata    &#13;
 per  difetto  delle  condizioni  di legge" e pertanto suscettibile di    &#13;
 essere  decisa  de  plano ai sensi dell'art. 666, comma 2, cod. proc.    &#13;
 pen.;                                                                    &#13;
         che  nei  confronti  della persona condannata il Tribunale di    &#13;
 Gela  ha  emesso  una sentenza di condanna divenuta irrevocabile il 4    &#13;
 dicembre  1993,  e  altre  sentenze  di  condanna  sono  state emesse    &#13;
 successivamente  dai  Pretori  di  Trapani e di Siracusa, nonché dal    &#13;
 pretore  di Caltagirone, la cui sentenza è divenuta irrevocabile per    &#13;
 ultima;                                                                  &#13;
         che  in tale situazione, concernente l'esecuzione di sentenze    &#13;
 emesse  sia  dal  tribunale che dal pretore, il pubblico ministero ha    &#13;
 ritenuto   che   la  competenza  appartenga  al  Tribunale  di  Gela,    &#13;
 prevalendo  la competenza del tribunale su quella del pretore a norma    &#13;
 dell'art. 665,   comma  4,  cod.  proc.  pen.,  e  quindi  anche  con    &#13;
 riferimento  alla  sentenza  di  condanna  pronunciata dal pretore di    &#13;
 Caltagirone;                                                             &#13;
         che,   peraltro,   la   disposizione  invocata  dal  pubblico    &#13;
 ministero  non è più in vigore, essendo stata abrogata e sostituita    &#13;
 dall'art. 206   del  decreto  legislativo  19  febbraio  1998,  n. 51    &#13;
 (entrato  in  vigore il 20 marzo 1998 e divenuto efficace il 2 giugno    &#13;
 1999), che ha riformulato il testo dell'art. 665, comma 4, cod. proc.    &#13;
 pen., inserendo anche il comma 4-bis;                                    &#13;
         che  il  decreto  legislativo  n. 51  del  1998  ha soppresso    &#13;
 l'ufficio  giudiziario  del pretore, attribuendo le relative funzioni    &#13;
 al tribunale, per cui le sentenze di condanna pronunciate dal pretore    &#13;
 vanno ora riferite al tribunale in composizione monocratica;             &#13;
         che  non  sono  applicabili  le  norme anteriormente vigenti,    &#13;
 comprese   quelle   relative  alla  competenza  e  alla  composizione    &#13;
 dell'organo giudicante, in quanto il procedimento è stato instaurato    &#13;
 dopo l'entrata in efficacia del decreto legislativo n. 51 del 1998;      &#13;
         che,  ad  avviso  del  rimettente,  a  seguito  della novella    &#13;
 dell'art. 665   cod.   proc.   pen. e,   segnatamente,   della  nuova    &#13;
 formulazione   del  comma  4,  in  caso  di  pluralità  di  condanne    &#13;
 pronunciate  da  giudici  diversi la competenza spetterebbe sempre al    &#13;
 giudice  che  ha  emesso  il  provvedimento divenuto irrevocabile per    &#13;
 ultimo,  salva la vis attractiva che il giudice ordinario continua ad    &#13;
 esercitare nei confronti del giudice speciale;                           &#13;
         che,  in  particolare,  la  deroga in favore del tribunale in    &#13;
 composizione  collegiale, prevista dal comma 4-bis riguarderebbe solo    &#13;
 l'ipotesi  di  più  provvedimenti  emessi dallo stesso tribunale, in    &#13;
 composizione monocratica e collegiale, in quanto solo all'interno del    &#13;
 medesimo  tribunale  sussisterebbe  quel  rapporto  di "attribuzione"    &#13;
 degli  affari  a  cui  fa  riferimento  il nuovo comma introdotto dal    &#13;
 decreto legislativo n. 51 del 1998;                                      &#13;
         che  secondo  il  giudice  a quo la nuova disciplina, in base    &#13;
 alla  quale  la competenza spetterebbe al Tribunale di Caltagirone in    &#13;
 composizione monocratica, subentrato al soppresso ufficio del pretore    &#13;
 che  ha  emesso il provvedimento divenuto irrevocabile per ultimo, si    &#13;
 pone   in   contrasto   con   l'art. 3   della  Costituzione:  quando    &#13;
 l'esecuzione   concerne   più   provvedimenti  emessi  dallo  stesso    &#13;
 tribunale  in  composizione  monocratica  e  collegiale,  alle  parti    &#13;
 dovrebbe  essere  sempre  assicurata  "la garanzia della composizione    &#13;
 collegiale   del   giudice",   anche  se  il  provvedimento  divenuto    &#13;
 irrevocabile per ultimo è stato emesso dal tribunale in composizione    &#13;
 monocratica,  mentre  tale garanzia risulta negata se l'esecuzione si    &#13;
 riferisce  a  più provvedimenti emessi da giudici diversi, qualunque    &#13;
 sia la loro composizione;                                                &#13;
         che  la  norma  censurata  sarebbe  dunque  irragionevole, in    &#13;
 quanto  la  composizione collegiale del giudice ha natura di garanzia    &#13;
 processuale,    "sempre    assicurata   dal   legislatore   ordinario    &#13;
 ogniqualvolta   vengano   in  rilievo  la  gravità  del  trattamento    &#13;
 sanzionatorio  ovvero la complessità o delicatezza delle fattispecie    &#13;
 incriminatrici";                                                         &#13;
         che  risulterebbe violato anche l'art. 24 della Costituzione,    &#13;
 a  cagione  della  "maggiore capacità di ascolto e di giudizio delle    &#13;
 ragioni  delle  parti"  assicurata  dalla composizione collegiale del    &#13;
 tribunale;                                                               &#13;
         che  nel  giudizio è intervenuto il Presidente del Consiglio    &#13;
 dei  Ministri, rappresentato e difeso dalla Avvocatura generale dello    &#13;
 Stato,  chiedendo  che  la  questione  venga dichiarata infondata, in    &#13;
 quanto   la   nuova  formulazione  del  comma  4  e  il  comma  4-bis    &#13;
 dell'art. 665 cod. proc. pen. non hanno sostanzialmente modificato la    &#13;
 precedente  disciplina,  ma  si sono limitati a rimodulare le vecchie    &#13;
 regole   in   funzione   dell'introduzione   del  giudice  unico:  in    &#13;
 particolare,  ad  avviso  dell'Avvocatura, il criterio cronologico è    &#13;
 destinato   ad   operare   in   via  generale  quando  si  tratta  di    &#13;
 provvedimenti emessi da giudici diversi (comma 4), mentre il criterio    &#13;
 della  collegialità,  a  cui  fa riferimento il comma 4-bis, "appare    &#13;
 funzionale   alla   determinazione   della   competenza   tra  organo    &#13;
 monocratico  e collegiale a prescindere dal fatto che siano diversi i    &#13;
 giudici  che  hanno emesso il provvedimento e quindi anche e non solo    &#13;
 quando non lo siano".                                                    &#13;
     Considerato  che  il rimettente - in base alla interpretazione da    &#13;
 lui riservata al comma 4-bis dell'art. 665 cod. proc. pen., nel senso    &#13;
 che  la  competenza spetta al giudice in composizione collegiale solo    &#13;
 nel  caso  in cui l'esecuzione concerna più provvedimenti emessi dal    &#13;
 medesimo  tribunale  in  composizione  monocratica  e collegiale - in    &#13;
 sostanza   lamenta  che  non  sia  stata  riprodotta,  opportunamente    &#13;
 adattata  ai  rapporti  tra  giudice  in  composizione  monocratica e    &#13;
 collegiale,  la  formulazione  dell'originario  comma 4 dell'art. 665    &#13;
 cod. proc. pen., che in caso di più provvedimenti emessi dal pretore    &#13;
 e  da  altro  giudice  ordinario  prevedeva  sempre  la competenza di    &#13;
 quest'ultimo;                                                            &#13;
         che,  a  prescindere  da qualsiasi valutazione sull'esattezza    &#13;
 dell'interpretazione  seguita  dal  rimettente circa la portata della    &#13;
 norma  censurata  -  sulla  quale, peraltro, non si è ancora formato    &#13;
 alcun  indirizzo  giurisprudenziale -, non è dato riscontrare alcuna    &#13;
 violazione  dei parametri costituzionali evocati dal giudice a quo in    &#13;
 quanto  l'attribuzione  della  competenza  rientra  nella sfera delle    &#13;
 scelte   affidate   alla   discrezionalità   del   legislatore,  non    &#13;
 suscettibili    di    censura    sul   terreno   della   legittimità    &#13;
 costituzionale,  sempre  che  siano  state  esercitate  sulla base di    &#13;
 criteri non irragionevoli;                                               &#13;
         che    tale    principio   ha   costantemente   ispirato   la    &#13;
 giurisprudenza   di  questa  Corte  in  tema  di  attribuzione  della    &#13;
 competenza  all'organo  giudicante in composizione monocratica (anche    &#13;
 con  specifico  riferimento  alla  soppressa  figura  del  pretore) o    &#13;
 collegiale (cfr., tra le tante, sentenze n. 460 del 1994 e n. 268 del    &#13;
 1986, ordinanze nn. 423 e 139 del 1997, n. 257 del 1995);                &#13;
         che pertanto la questione di legittimità costituzionale deve    &#13;
 essere dichiarata manifestamente infondata.                              &#13;
     Visti  gli articoli 26, secondo comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 665,  commi  4  e  4-bis, del    &#13;
 codice di procedura penale, sollevata, in riferimento agli articoli 3    &#13;
 e  24  della  Costituzione, dal Tribunale di Gela, con l'ordinanza in    &#13;
 epigrafe.                                                                &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 giugno 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Modona                             &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 23 giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
