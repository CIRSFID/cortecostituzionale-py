<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>120</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:120</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/06/1971</data_decisione>
    <data_deposito>09/06/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale degli  artt.  1,  &#13;
 da 9 a 17, 26, 27, 29, 33, 51, 52 e 53 della legge 12 febbraio 1968, n.  &#13;
 132 (enti ospedalieri e assistenza ospedaliera), promossi:               &#13;
     1)  dalla  Regione  della  Lombardia,  con ricorso notificato il 27  &#13;
 agosto 1970, depositato in cancelleria il  5  settembre  successivo  ed  &#13;
 iscritto al n. 12 del registro ricorsi 1970;                             &#13;
     2) dalla Regione degli Abruzzi, con ricorso notificato il 2 ottobre  &#13;
 1970,  depositato  in cancelleria il 10 successivo ed iscritto al n. 19  &#13;
 del registro ricorsi 1970.                                               &#13;
     Visti gli atti di costituzione del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 13 gennaio 1971 il Giudice relatore  &#13;
 Giovanni Battista Benedetti;                                             &#13;
     uditi l'avv. Enrico Allorio, per la Regione della Lombardia, l'avv.  &#13;
 Pietro  Tranquilli-Leali, per la Regione degli Abruzzi, ed il sostituto  &#13;
 avvocato generale dello Stato Michele Savarese, per il  Presidente  del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - La Regione della Lombardia e quella degli Abruzzi, con ricorsi  &#13;
 notificati  al Presidente del Consiglio dei ministri rispettivamente il  &#13;
 27 agosto 1970 e il  2  ottobre  1970,  hanno  impugnato  la  legge  12  &#13;
 febbraio  1968,  n.  132,  contenente  norme  sugli  enti ospedalieri e  &#13;
 l'assistenza ospedaliera.                                                &#13;
     In particolare la Regione lombarda ha  denunciato  l'illegittimità  &#13;
 costituzionale dei seguenti articoli:                                    &#13;
     a) art. 1, per violazione dell'art. 118 della Costituzione, perché  &#13;
 confisca   a   beneficio   del   Ministro   della   sanità  competenze  &#13;
 amministrative nella materia di assistenza sanitaria ed ospedaliera che  &#13;
 l'art. 117 riserva alle Regioni;                                         &#13;
     b)  artt.  9 a 17, perché organizzano l'amministrazione degli enti  &#13;
 ospedalieri, la  vigilanza  sugli  stessi  e  sugli  istituti  ed  enti  &#13;
 ecclesiastici che esercitano l'assistenza ospedaliera, con disposizioni  &#13;
 che tolgono ogni potere regionale di intervento;                         &#13;
     c) artt. 26, 27 e 29, perché consentono allo Stato di procedere ad  &#13;
 una   programmazione   di   settore   in   una  materia  di  competenza  &#13;
 amministrativa e legislativa regionale;                                  &#13;
     d) art. 33, perché consente la concessione, da parte del  Ministro  &#13;
 della  sanità,  di contributi e sussidi agli enti e integrazione degli  &#13;
 assegni ai medici interni, mentre in base agli artt. 118  e  119  della  &#13;
 Costituzione  gli  interventi  predetti  dovrebbero  essere  effettuati  &#13;
 attraverso le Regioni;                                                   &#13;
     e) artt. 51, 52 e 53, perché concernono le case di cura privata  e  &#13;
 permettono unicamente all'amministrazione centrale dello Stato, insieme  &#13;
 a quella periferica, ogni iniziativa ed ogni controllo.                  &#13;
     La  Regione  abruzzese  ha impugnato invece gli artt. 1, da 9 a 17,  &#13;
 26, 27, 29, 33, 51, 52 e 53, come lesivi della  competenza  legislativa  &#13;
 ed   amministrativa  della  Regione  ed  inoltre  della  sua  autonomia  &#13;
 finanziaria.                                                             &#13;
     Viene rilevato dalle due Regioni che allo Stato poteva competere di  &#13;
 dettare norme direttive, non disposizioni così analitiche come  quelle  &#13;
 emanate,  che  rendono esclusivo l'intervento dello Stato e negano ogni  &#13;
 potere alle Regioni.                                                     &#13;
     2. - Il Presidente del Consiglio dei ministri, intervenuto  innanzi  &#13;
 alla   Corte,  ha  eccepito  l'inammissibilità  dei  ricorsi,  perché  &#13;
 notificati entro i trenta giorni dalla data d'insediamento della  prima  &#13;
 giunta  regionale,  ma  a  distanza di anni dalla data di pubblicazione  &#13;
 della legge impugnata: le ragioni  dell'eccezione  sono  quelle  stesse  &#13;
 esposte  dalla  medesima  parte  a proposito del ricorso proposto dalla  &#13;
 Regione lombarda avverso la legge 16 maggio 1970,  n.  281,  contenente  &#13;
 provvedimenti  finanziari  per  l'attuazione  delle  Regioni  a statuto  &#13;
 ordinario, deciso con la sentenza 25 febbraio 1971, n. 39.               &#13;
     Nel merito, il Presidente del Consiglio rileva  che  non  tutta  la  &#13;
 materia  sanitaria  può considerarsi trasferibile alle Regioni: l'art.  &#13;
 32 della Costituzione affida alla "Repubblica" la tutela della  salute,  &#13;
 e  la  legge  suddetta è di natura programmatica ancorché di settore,  &#13;
 cosicché vale per essa ciò che la Corte ha deciso nella  sentenza  24  &#13;
 gennaio 1964, n. 4, a proposito della legge sugli acquedotti. L'art. 32  &#13;
 della  Costituzione dovrà necessariamente passare attraverso strumenti  &#13;
 e fasi di programmazione  a  carattere  nazionale,  coordinati  con  le  &#13;
 competenze  delle  Regioni:  l'obiettivo  di fondo è l'istituzione del  &#13;
 servizio sanitario nazionale, ma la legge impugnata non sembra ignorare  &#13;
 o comprimere, fuori dalle necessità  di  coordinamento,  le  autonomie  &#13;
 regionali.  L'intervento  degli  organi statali previsto nell'art. 1 si  &#13;
 spiega alla luce della natura degli istituti ivi considerati di livello  &#13;
 universitario,  mentre  la  disciplina  delle  case  di  cura  private,  &#13;
 riguardando  le  iniziative  privatistiche nel settore sanitario, esula  &#13;
 dalla competenza regionale e non può non essere riservata allo Stato.   &#13;
     3. - La Regione lombarda e il Presidente del Consiglio dei ministri  &#13;
 hanno presentato memorie.                                                &#13;
     La Regione lombarda, a proposito dell'eccezione di  intempestività  &#13;
 del  suo ricorso, oppone le medesime argomentazioni da essa prospettate  &#13;
 nella causa decisa con la predetta sentenza 25 febbraio  1971,  n.  39.  &#13;
 Nel  merito  ribadisce  che  la  legge  impugnata non può ritenersi di  &#13;
 programma, dato che contiene minuziose disposizioni  che  non  lasciano  &#13;
 margine alcuno alle competenze regionali e addirittura le confisca.      &#13;
     La  Presidenza  del  Consiglio  fa  presente che, per evitare vuoti  &#13;
 legislativi, l'intervento dello Stato non poteva essere che completo  e  &#13;
 minuto.  Allorquando le Regioni saranno poste in grado di esercitare la  &#13;
 potestà legislativa che  loro  spetta  nella  materia,  si  porrà  il  &#13;
 problema  della  coesistenza  della  legge  con  quelle  regionali: una  &#13;
 corretta dialettica del rapporto  fra  la  potestà  statale  e  quella  &#13;
 regionale  potrà  consentire  la discriminazione fra norme e principi,  &#13;
 mentre oggi la Regione non può pretendere che l'ordinamento  si  tenga  &#13;
 sgombro da una disciplina in atto necessitata.                           &#13;
     4.  -  All'udienza  del  13  gennaio  1971 le parti hanno oralmente  &#13;
 svolto e illustrato le rispettive tesi difensive.<diritto>Considerato in diritto</diritto>.                          &#13;
     Le questioni da decidere sono sostanzialmente eguali a quelle sulle  &#13;
 quali questa Corte si è pronunciata con sentenza in pari data n. 119.   &#13;
     Anche  i  ricorsi  in  esame   devono   ritenersi   tempestivamente  &#13;
 notificati,  poiché,  come  già  statuito  nella sentenza 25 febbraio  &#13;
 1971, n. 39, i termini per la proposizione dei ricorsi per  le  Regioni  &#13;
 di  nuova  istituzione  debbono  computarsi a far data dal giorno della  &#13;
 formazione delle rispettive giunte.                                      &#13;
     Tuttavia tali ricorsi sono inammissibili  per  altro  verso.  Nella  &#13;
 citata  sentenza  n.  119  la  Corte  ha  precisato che alle Regioni è  &#13;
 impedito  di  sollevare  questioni  d'invasione  della  sfera  di  loro  &#13;
 competenza finché non siano maturati i presupposti richiesti dall'art.  &#13;
 17  della legge 16 maggio 1970, n. 281, e cioè fino a quando non siano  &#13;
 stati emanati i decreti relativi al trasferimento  alle  Regioni  delle  &#13;
 funzioni  loro  attribuite  e  del  relativo personale dipendente dallo  &#13;
 Stato o, in mancanza, finché non sia decorso un  biennio  dall'entrata  &#13;
 in vigore della predetta legge.                                          &#13;
     Nella  specie  tali  presupposti  non  si  sono ancora verificati e  &#13;
 pertanto solo quando sarà stato rimosso  l'impedimento  costituzionale  &#13;
 che ne deriva potranno porsi in concreto questioni di menomazione delle  &#13;
 competenze  regionali. Rimosso tale impedimento, la legge impugnata non  &#13;
 potrà impedire l'esercizio della competenza regionale  e  in  essa  si  &#13;
 potranno  rinvenire  i  principi fondamentali che pongono limiti a tale  &#13;
 esercizio, ex art. 117 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 degli  artt.  1, 9, 10, 11, 12, 13, 14, 15, 16, 17, 26, 27, 29, 33, 51,  &#13;
 52 e 53 della legge 12 febbraio 1968, n. 132, recante norme sugli  enti  &#13;
 ospedalieri e sull'assistenza ospedaliera, proposta dalla Regione della  &#13;
 Lombardia e da quella degli Abruzzi, con ricorsi rispettivamente del 27  &#13;
 agosto e 2 ottobre 1970, in riferimento agli artt.5, 115, 117, 118, 119,  &#13;
 23, 125 e 130 della Costituzione.                                        &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 4 giugno 1971.                                &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
