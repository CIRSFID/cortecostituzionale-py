<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>89</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:89</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1995</data_decisione>
    <data_deposito>17/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 271 del  codice    &#13;
 penale e dell'art. 28 della legge 20 maggio 1970, n. 300 (Norme sulla    &#13;
 tutela  della  libertà  e  dignità  dei  lavoratori, della libertà    &#13;
 sindacale e dell'attività sindacale nei luoghi di lavoro e norme sul    &#13;
 collocamento), promosso con ordinanza emessa l'11  gennaio  1994  dal    &#13;
 Tribunale  di  Torino  nel  procedimento  civile  vertente tra AMIAT,    &#13;
 Azienda municipale igiene ambientale torinese  e  FALIA,  Federazione    &#13;
 autonomista   lavoratori   igiene   ambientale   -   SALP,  Sindacato    &#13;
 autonomista lavoratori piemontesi, iscritta al  n.  97  del  registro    &#13;
 ordinanze 1994 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 12, prima serie speciale, dell'anno 1994;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 22 febbraio  1995  il  Giudice    &#13;
 relatore Cesare Ruperto.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - In una controversia ex art. 28 della legge 20 maggio 1970, n.    &#13;
 300  (Norme  sulla  tutela  della libertà e dignità dei lavoratori,    &#13;
 della libertà sindacale e dell'attività  sindacale  nei  luoghi  di    &#13;
 lavoro  e  norme  sul  collocamento),  promossa dall'organismo locale    &#13;
 (FALIA, Federazione autonomista lavoratori igiene ambientale -  SALP,    &#13;
 Sindacato   autonomista   lavoratori   piemontesi)  dell'associazione    &#13;
 sindacale   CONFEDERSAL   (Confederazione    sindacati    autonomisti    &#13;
 lavoratori),    il    Pretore   aveva   ritenuto   che   quest'ultima    &#13;
 organizzazione possedesse il  requisito  di  associazione  a  livello    &#13;
 nazionale in quanto, seppure presente soltanto in sei regioni, mirava    &#13;
 statutariamente  a  diffondersi  su  tutto il territorio, nell'ottica    &#13;
 "della  auspicata  federalizzazione   dello   Stato   italiano"   (la    &#13;
 Confederazione  si  poneva  infatti  come insieme di tre Federazioni:    &#13;
 nord, centro e sud), "irrilevante essendo che questa" fosse  prevista    &#13;
 "nell'interesse della collettività dei popoli italiani e di tutte le    &#13;
 risorse delle rispettive nazioni".                                       &#13;
    Nel  corso  del  giudizio di appello avverso la sentenza pretorile    &#13;
 che,  su  tale  premessa,  aveva  ravvisato  l'antisindacabilità  del    &#13;
 comportamento della azienda, il Tribunale di Torino ha sollevato, con    &#13;
 ordinanza   emessa  l'11  gennaio  1994,  questione  di  legittimità    &#13;
 costituzionale  degli artt. 271 del codice penale e 28 della legge n.    &#13;
 300 del 1970, in riferimento agli artt. 2, 3, 18, 21, 24,  35  e  39,    &#13;
 primo comma, della Costituzione.                                         &#13;
    Premette il giudice a quo che la sommatoria di nazioni distinte di    &#13;
 cui  allo statuto della CONFEDERSAL non può considerarsi equivalente    &#13;
 al concetto di nazione unitaria, non riducibile ad  un  mero  àmbito    &#13;
 territoriale, di cui al citato art. 28. A sostegno di tale assunto il    &#13;
 Tribunale  compie un excursus dell'uso che del concetto di nazione è    &#13;
 stato fatto dal Costituente, concludendo nel senso che la norma dello    &#13;
 statuto dei lavoratori postula associazioni che svolgono  funzioni  a    &#13;
 beneficio  del  sistema  nel  suo  complesso e non dei soli iscritti,    &#13;
 così incentivando le più  ampie  forme  possibili  di  aggregazione    &#13;
 sindacale.                                                               &#13;
    Tuttavia  -  prosegue  il  giudice  remittente  -  l'art. 39 della    &#13;
 Costituzione  non  richiede  il  "requisito  nazionale";  la   stessa    &#13;
 formulazione   della   norma,   inoltre,  indicherebbe  che  vi  sono    &#13;
 interessi-base (qual è quello alla libertà sindacale  nell'azienda)    &#13;
 insiti  nel  diritto  al  lavoro,  la  cui garanzia sarebbe possibile    &#13;
 soltanto attraverso la tutela immediata dell'organizzazione sindacale    &#13;
 (intesa  come  comunità  dei  lavoratori).  In  tal  senso  potrebbe    &#13;
 dubitarsi  della  legittimità  costituzionale, in relazione all'art.    &#13;
 39, primo comma, della Costituzione, dell'art. 28  citato,  là  dove    &#13;
 esclude  la legittimazione delle associazioni sindacali che non siano    &#13;
 nazionali "ancorché esistenti come sindacati di fatto".                 &#13;
    Nel merito della controversia il giudice  a  quo  rileva  come  la    &#13;
 CONFEDERSAL  non  possa  essere  qualificata  nazionale,  aggiungendo    &#13;
 altresì che essa appare anzi essere antinazionale ex art.  271  c.p.    &#13;
 e,  come  tale, non meritevole di tutela ai sensi dell'art. 1322 c.c.    &#13;
 L'art. 271 c.p., che tutela il sentimento  nazionale  ed  opera  come    &#13;
 limite  esterno  dell'autonomia  negoziale, non potrebbe considerarsi    &#13;
 investito   dalla   declaratoria   d'illegittimità    costituzionale    &#13;
 dell'art.  272,  secondo comma, c.p., di cui alla sentenza n. 168 del    &#13;
 1986. Di conseguenza parrebbe necessario un  intervento  della  Corte    &#13;
 sulla  norma  onde rimuovere detto limite, intervento reso necessario    &#13;
 anche dal fatto che "l'esistenza di organismi sindacali  di  distinte    &#13;
 nazionalità  nell'ambito  dello  Stato costituisce un quid novum nel    &#13;
 vigente ordinamento".                                                    &#13;
    2. - È intervenuto il  Presidente  del  Consiglio  dei  ministri,    &#13;
 rappresentato  e  difeso dall'Avvocatura dello Stato, che ha concluso    &#13;
 nel  senso  dell'inammissibilità  per  irrilevanza  della  questione    &#13;
 relativa  all'art. 271 c.p. e dell'infondatezza delle censure rivolte    &#13;
 all'art. 28 della legge n. 300 del 1970.                                 &#13;
    Sotto il primo profilo l'Autorità intervenuta  osserva  che,  ove    &#13;
 dalla     norma     penale    potesse    argomentarsi    l'illiceità    &#13;
 dell'associazione, ciò si  rifletterebbe  sulla  legittimità  della    &#13;
 stessa,  a prescindere dal connotato definitorio "nazionale" adottato    &#13;
 nell'art. 28.                                                            &#13;
    Rileva altresì l'Avvocatura come il carattere nazionale  riguardi    &#13;
 la   dimensione   organizzativa   della   presenza   sul   territorio    &#13;
 dell'organizzazione e non già la coerenza del  suo  statuto  con  la    &#13;
 concezione  politica  di  nazione intesa in senso unitario. Elemento,    &#13;
 questo, d'incerta  verificabilità  ed  inconferente  al  fine  della    &#13;
 delimitazione  della  legittimazione,  siccome non rilevante quanto a    &#13;
 rappresentatività    e   quindi   non   sintomatico   dell'idoneità    &#13;
 dell'azione sindacale.<diritto>Considerato in diritto</diritto>1. - Il Tribunale di Torino dubita, in riferimento agli  artt.  2,    &#13;
 3,  18,  21,  24,  35  e  39,  primo comma, della Costituzione, della    &#13;
 legittimità costituzionale degli artt. 271 c.p. e 28 della legge  n.    &#13;
 300  del 1970. La prima norma è censurata in quanto, con l'affermare    &#13;
 il disvalore dell'antinazionalità vietando le  associazioni  che  si    &#13;
 propongono  o  che  svolgono  un'attività  diretta a distruggere o a    &#13;
 deprimere il  sentimento  nazionale,  viene  a  limitare  l'autonomia    &#13;
 associativa  e  a  privare  del  requisito  della  nazionalità dette    &#13;
 associazioni,  non   consentendo   inoltre   che   siano   costituite    &#13;
 associazioni  sindacali  di  distinte nazionalità nell'a'mbito dello    &#13;
 Stato. La norma dello statuto dei  lavoratori  è  invece  sospettata    &#13;
 d'illegittimità  costituzionale  nella  parte  in  cui  consente  la    &#13;
 legittimazione attiva  esclusivamente  agli  organismi  locali  delle    &#13;
 associazioni sindacali nazionali.                                        &#13;
    2.  -  La  questione concernente l'art. 271 c.p. è manifestamente    &#13;
 inammissibile  per  l'evidente  irrilevanza  nel  giudizio  in  corso    &#13;
 dinanzi  al  giudice  remittente, il quale è chiamato a pronunciarsi    &#13;
 sull'appello avverso la sentenza del Pretore in tema di comportamento    &#13;
 antisindacale. La  totale  estraneità  al  thema  decidendum,  della    &#13;
 citata norma incriminatrice, non consente di estrapolare dalla stessa    &#13;
 criteri  e  definizioni utilizzabili a fini diversi da quelli penali.    &#13;
 Il sindacato di legittimità non  può  quindi  essere  ammesso;  sul    &#13;
 punto  l'ordinanza  di  rimessione  appare,  non  a  caso,  del tutto    &#13;
 carente, sia con riguardo alla motivazione circa la rilevanza, sia in    &#13;
 ordine al giudizio di non manifesta infondatezza.                        &#13;
    3. - Analoga carenza, a prima vista, sembra ravvisabile anche  con    &#13;
 riferimento  alla  seconda  delle descritte censure. Risulta tuttavia    &#13;
 sufficientemente comprensibile il senso di  queste,  proponendosi  il    &#13;
 Tribunale  di  ottenere  dalla  Corte  una decisione ampliativa delle    &#13;
 ipotesi di legittimazione ad agire ex  art.  28  con  riferimento  al    &#13;
 requisito   della   dimensione   nazionale.   E   dunque  va  esclusa    &#13;
 l'inammissibilità della sollevata questione; la  quale  è  peraltro    &#13;
 infondata.                                                               &#13;
    3.1. - Il procedimento di repressione della condotta antisindacale    &#13;
 si  aggiunge alle tutele già assicurate alle associazioni sindacali,    &#13;
 e  rappresenta   un   mezzo   ulteriore   per   garantire   in   modo    &#13;
 particolarmente  rapido ed efficace i diritti del sindacato. Il fatto    &#13;
 che il legislatore abbia riservato la relativa azione  a  determinati    &#13;
 soggetti  collettivi,  risulta  coerente  con  la  razionalità delle    &#13;
 scelte  poste  a  base  di  criteri  per  individuare   la   maggiore    &#13;
 rappresentatività  degli stessi, più volte scrutinate positivamente    &#13;
 da  questa  Corte.  In  particolare  la  concezione  che  assume   la    &#13;
 dimensione organizzativa nazionale come indice di adeguato livello di    &#13;
 rappresentatività  (cfr.  la sentenza n. 54 del 1974 e, soprattutto,    &#13;
 la sentenza n. 334 del 1988)  è  apparsa  idonea  a  "consentire  la    &#13;
 selezione, tra i tanti possibili, dell'interesse collettivo rilevante    &#13;
 da  porre a base del conflitto con la parte imprenditoriale". Più in    &#13;
 generale e con riferimento all'unica norma costituzionale sulla quale    &#13;
 si sofferma in proposito il giudice a quo , questa Corte ha osservato    &#13;
 come l'opzione nel senso di  un  livello  rappresentativo  nazionale,    &#13;
 oltre  a corrispondere al ruolo tradizionalmente svolto dal movimento    &#13;
 sindacale  italiano, si uniformi al principio solidaristico nel quale    &#13;
 va inserito anche l'invocato art. 39 della Costituzione.                 &#13;
    Ciò, naturalmente, non esclude che lo stesso legislatore possa in    &#13;
 futuro dettare nuove  regole  idonee  a  realizzare  diversamente  "i    &#13;
 principi  di libertà e pluralismo sindacale additati dal primo comma    &#13;
 dell'art. 39  della  Costituzione",  anche  prevedendo  strumenti  di    &#13;
 verifica   dell'effettiva   rappresentatività   delle   associazioni    &#13;
 (sentenza n. 30 del 1990). Ma  il  controllo  di  compatibilità  tra    &#13;
 l'indice  della  dimensione  organizzativa  nazionale  e  la  realtà    &#13;
 sociale esistente non  può  concludersi,  allo  stato,  che  con  la    &#13;
 conferma della non contrarietà del modello statutario al disegno del    &#13;
 Costituente.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità  costituzionale  dell'art.  271   del   codice   penale,    &#13;
 sollevata, in riferimento agli artt. 2, 3, 18, 21, 24, 35 e 39, primo    &#13;
 comma, della Costituzione, dal Tribunale di Torino con l'ordinanza di    &#13;
 cui in epigrafe;                                                         &#13;
    Dichiara  non  fondata la questione di legittimità costituzionale    &#13;
 dell'art. 28 della legge 20 maggio 1970, n. 300 (Norme  sulla  tutela    &#13;
 della  libertà e dignità dei lavoratori, della libertà sindacale e    &#13;
 dell'attività  sindacale  nei  luoghi  di   lavoro   e   norme   sul    &#13;
 collocamento),  sollevata,  in riferimento alle medesime norme, dallo    &#13;
 stesso Tribunale di Torino con l'ordinanza citata.                       &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1995.                                  &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: RUPERTO                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 17 marzo 1995.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
