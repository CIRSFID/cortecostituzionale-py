import json
import os
import sys

from cortecostituzionale.visualizer.constitution_reader import get_constitution_article_version_dict, date_dict_to_number
from cortecostituzionale.visualizer.pronunciation_reader import get_pronunciation_meta_dict

CONSTITUTION_DOC_PATH = "/Users/biagio/desktop/corte_cost_paper/visualizer/cost.json"
PRONUNCIATION_DOC_PATH = "/Users/biagio/repos/cirsfid/cortecostituzionale-py/data/out"  # sys.argv[2]
PLOT_DIR = "plot"

if not os.path.isdir(PLOT_DIR):
    os.mkdir(PLOT_DIR)

if PRONUNCIATION_DOC_PATH.endswith('.json'):
    with open(PRONUNCIATION_DOC_PATH, 'r') as f:
        pronunciation_meta_dict = json.load(f)
else:
    pronunciation_meta_dict = get_pronunciation_meta_dict(PRONUNCIATION_DOC_PATH)
constitution_article_version_dict = get_constitution_article_version_dict(CONSTITUTION_DOC_PATH)


# print(constitution_article_version_dict)

def get_version(article_id, date_dict):
    if article_id not in constitution_article_version_dict:
        return None
    date_numer = date_dict_to_number(date_dict)
    for i, version in enumerate(constitution_article_version_dict[article_id]):
        if date_numer < date_dict_to_number(version['end']):
            return i


article_version_dict = {}
method_set = set()
year_reference_per_article = {}
for k, v in pronunciation_meta_dict.items():
    year = v["decision_date"]['year']
    method = v['pronunciation_method']
    method_set.add(method)
    for reference, count in v['constitution_article_reference_counter'].items():
        article_id = reference.split('_')[1]
        paragraph_id = reference.split('para_')[-1] if 'para' in reference else None
        version = get_version(article_id, v["decision_date"])
        if version is None:
            continue
        # update article_version_dict
        if article_id not in article_version_dict:
            article_version_dict[article_id] = set()
        article_version_dict[article_id].add(version)
        # update year_reference_per_article
        if article_id not in year_reference_per_article:
            year_reference_per_article[article_id] = {}
        year_reference_dict = year_reference_per_article[article_id]
        if year not in year_reference_dict:
            year_reference_dict[year] = {}
        reference_dict = year_reference_dict[year]
        if method not in reference_dict:
            reference_dict[method] = {}
        version_dict = reference_dict[method]
        if version not in version_dict:
            version_dict[version] = 0
        version_dict[version] += count
# print(year_reference_per_article)

#################################################################################
######################## Stacked barplot with matplotlib ########################
#################################################################################
# https://python-graph-gallery.com/12-stacked-barplot-with-matplotlib/

# libraries
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec

BAR_WIDTH = 1
for article, year_reference_dict in year_reference_per_article.items():
    year_groups_dict = {
        year: [
            [
                method_dict[method].get(version, 0)
                for version in article_version_dict[article]
            ] if method in method_dict else
            [
                0
                for version in article_version_dict[article]
            ]
            for method in method_set
        ]
        for year, method_dict in year_reference_dict.items()
    }

    version_list = list(article_version_dict[article])
    year_list, groups_list = zip(*sorted(year_groups_dict.items(), key=lambda x: int(x[0])))
    # Values of each group
    bars = zip(*groups_list)
    # bars = map(lambda x: list(map(sum, x)), bars)
    bars = list(bars)
    methods = list(method_set)

    # The position of the bars on the x-axis
    r = list(range(len(year_list)))
    ncols = 1
    nrows = len(bars)

    # Panel Bar Chart
    plt.title(article)
    figure = Figure(figsize=(20 * ncols, 7 * nrows))
    canvas = FigureCanvas(figure)
    grid = GridSpec(ncols=ncols, nrows=nrows)
    axes = []
    cmap = sns.color_palette("cubehelix", len(version_list))
    for i, b in enumerate(bars):
        ax = figure.add_subplot(grid[i // ncols, i % ncols])
        ax.set_title(methods[i])
        ax.set_ylabel('References')
        ax.set_xlabel('Year')
        ax.set_xticks(r)
        ax.set_xticklabels(year_list, fontweight='bold', rotation=90)
        ax.grid(True)
        axes.append(ax)
        # Stacked Bar Chart
        versions = list(zip(*b))
        for i, v in enumerate(versions):
            if i == 0:
                ax.bar(r, v, width=BAR_WIDTH, color=cmap[i], edgecolor='white')
            else:  # Create green bars (middle), on top of the firs ones
                bottom = np.sum(versions[:i], axis=0).tolist()
                ax.bar(r, v, bottom=bottom, width=BAR_WIDTH, color=cmap[i], edgecolor='white')
        ax.legend(
            (Line2D([0], [0], color=c, lw=4) for c in cmap),
            version_list
        )
    # save figure
    figure.savefig(os.path.join(PLOT_DIR, f"article_{article}.png"), bbox_inches='tight')
