<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>101</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:101</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VARI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>08/04/2002</data_decisione>
    <data_deposito>10/04/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Massimo VARI; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 34, comma 2, &#13;
del   codice   di  procedura  penale,  promosso,  nell'ambito  di  un &#13;
procedimento  penale, dal Tribunale di Biella con ordinanza emessa il &#13;
giorno  11 ottobre  2000,  iscritta  al n. 599 del registro ordinanze &#13;
2001 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 33, 1a &#13;
serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera di consiglio del 27 febbraio 2002 il giudice &#13;
relatore Guido Neppi Modona. &#13;
    Ritenuto  che il Tribunale di Biella ha sollevato, in riferimento &#13;
agli  artt. 3, 24 e 111, secondo comma, della Costituzione, questione &#13;
di  legittimità  costituzionale dell'art. 34, comma 2, del codice di &#13;
procedura  penale,  nella parte in cui non prevede l'incompatibilità &#13;
alla  funzione  di  giudizio  del  giudice del dibattimento che abbia &#13;
rigettato  la  richiesta  di giudizio abbreviato subordinata, a norma &#13;
dell'art. 438,   comma  5,  cod.  proc.  pen.,  ad  una  integrazione &#13;
probatoria; &#13;
        che  il  rimettente  premette  che,  prima  dell'apertura del &#13;
dibattimento,  la  difesa  degli  imputati  aveva formulato, ai sensi &#13;
degli  artt. 438, comma 5, e 555, comma 2, cod. proc. pen., richiesta &#13;
di  giudizio  abbreviato  subordinata  alla  ammissione  di una nuova &#13;
perizia contabile; &#13;
        che tale richiesta era stata respinta, in quanto la prova era &#13;
stata ritenuta non necessaria ai fini della decisione e incompatibile &#13;
con le finalità di economia processuale del rito; &#13;
        che   il   giudice   a   quo   rileva   che,   avendo  dovuto &#13;
necessariamente   esaminare  gli  atti  del  fascicolo  del  pubblico &#13;
ministero  per  effettuare  tale valutazione, è venuto a trovarsi in &#13;
una   situazione   del   tutto  analoga  a  quella  del  giudice  del &#13;
dibattimento   che,  a  seguito  del  rigetto  di  una  richiesta  di &#13;
applicazione  della  pena, è incompatibile alla funzione di giudizio &#13;
per   effetto   della   sentenza   n. 186   del   1992   della  Corte &#13;
costituzionale; &#13;
        che,  ad  avviso  del  rimettente,  in entrambe le ipotesi il &#13;
giudice   deve   infatti  "procedere  approfonditamente  ad  esame  e &#13;
valutazione degli atti del fascicolo del P.M." ed "entrare nel merito &#13;
del   processo"  nell'un  caso  per  accertare  se  la  richiesta  di &#13;
integrazione  probatoria  sia  necessaria  ai  fini  della decisione, &#13;
nell'altro per valutare la fondatezza della richiesta di applicazione &#13;
della pena; &#13;
        che   la  mancata  previsione  della  incompatibilità  nella &#13;
ipotesi  considerata  si  porrebbe  in  contrasto  con l'art. 3 della &#13;
Costituzione   per   la   diversità   di   trattamento  cui  vengono &#13;
assoggettate  due  situazioni del tutto analoghe e con gli artt. 24 e &#13;
111,  secondo  comma, della Costituzione per violazione del principio &#13;
secondo  cui  il  processo  si  svolge  davanti  a un giudice terzo e &#13;
imparziale; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo  che  la questione venga dichiarata inammissibile e &#13;
comunque  infondata,  in  base  al  rilievo  che l'incompatibilità a &#13;
partecipare  al giudizio è determinata da una preventiva valutazione &#13;
in  ordine  alla  responsabilità  dell'imputato,  e  non  dalla mera &#13;
conoscenza degli atti. &#13;
    Considerato  che  il rimettente, giudice del dibattimento che nel &#13;
corso  degli  atti  preliminari  ha respinto la richiesta di giudizio &#13;
abbreviato  subordinata  ad  una  integrazione probatoria, ritiene di &#13;
trovarsi  in una situazione di incompatibilità assimilabile a quella &#13;
individuata  da  questa  Corte  con  la  sentenza  n. 186 del 1992 in &#13;
relazione  alla  ipotesi  del  giudice  del dibattimento che abbia in &#13;
precedenza  respinto  la  richiesta  di  applicazione  della  pena, e &#13;
pertanto  denuncia  il contrasto dell'art. 34, comma 2, del codice di &#13;
procedura  penale  con  gli  artt. 3,  24 e 111, secondo comma, della &#13;
Costituzione,  per  il diverso trattamento riservato a casi del tutto &#13;
analoghi   e  per  la  violazione  dei  principi  della  terzietà  e &#13;
imparzialità del giudice; &#13;
        che  nel richiamarsi alla sentenza n. 186 del 1992 il giudice &#13;
a  quo omette di considerare che già in quella occasione la Corte ha &#13;
messo  in  rilievo  la  differenza  tra il rigetto della richiesta di &#13;
applicazione  della  pena,  che "comporta quanto meno una valutazione &#13;
negativa   circa   l'esistenza   delle   condizioni  legittimanti  il &#13;
proscioglimento  ex  art. 129  cod. proc. pen." e determina quindi un &#13;
pregiudizio  per  l'imparzialità del giudice, e la valutazione circa &#13;
la  definibilità  del  giudizio allo stato degli atti (condizione di &#13;
ammissibilità  del  rito  abbreviato  alla  stregua della disciplina &#13;
allora  vigente),  che  si  sostanzia  in  "una  decisione  di natura &#13;
meramente  processuale,  per  ciò  stesso inidonea a dar luogo ad un &#13;
"pre-giudizio" rispetto alla decisione di merito"; &#13;
        che  tali  rilievi  valgono  a maggior ragione in ordine alla &#13;
nuova  disciplina  del  giudizio  abbreviato  introdotta  dalla legge &#13;
16 dicembre  1999, n. 479, in base alla quale il giudice, a fronte di &#13;
una  richiesta di giudizio abbreviato subordinata ad una integrazione &#13;
probatoria,  è  chiamato  a  stabilire  soltanto  se  la  prova  sia &#13;
necessaria  ai fini della decisione e compatibile con le finalità di &#13;
economia  processuale  proprie  del  procedimento, operando così una &#13;
valutazione  che  non implica alcun giudizio di merito in ordine alla &#13;
responsabilità dell'imputato; &#13;
        che,  quanto  alla  conoscenza  degli  atti del fascicolo del &#13;
pubblico  ministero, nella quale il rimettente individua la causa del &#13;
pregiudizio   per   l'imparzialità  del  giudice,  questa  Corte  ha &#13;
ripetutamente  affermato  che  tale conoscenza è ininfluente ai fini &#13;
della   incompatibilità   con   la  funzione  di  giudizio  ove  non &#13;
accompagnata  da  una  valutazione  contenutistica,  di  merito,  sui &#13;
risultati  delle  indagini  preliminari  (v. tra le tante, oltre alla &#13;
menzionata sentenza n. 186 del 1992, sentenze n. 131 del 1996, n. 455 &#13;
del 1994, n. 502 del 1991, nonché ordinanza n. 152 del 1999); &#13;
        che,  infine,  in merito alla censura mossa alla disposizione &#13;
impugnata  sotto  il  profilo  della  violazione  dei  principi della &#13;
terzietà e imparzialità del giudice, è già stato osservato che la &#13;
nuova  formulazione  dell'art. 111  della  Costituzione  "non  innova &#13;
sostanzialmente  rispetto  ai  principi  già  desumibili dagli a suo &#13;
tempo  invocati  artt. 24  e 3 della Costituzione, quali interpretati &#13;
dalla  giurisprudenza di questa Corte" (ordinanza n. 112 del 2001; v. &#13;
inoltre sentenza n. 283 del 2000 e ordinanza n. 167 del 2001); &#13;
        che   la  questione  va  pertanto  dichiarata  manifestamente &#13;
infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art. 34,  comma  2,  del codice di &#13;
procedura  penale,  sollevata, in riferimento agli artt. 3, 24 e 111, &#13;
secondo  comma,  della  Costituzione,  dal  Tribunale  di Biella, con &#13;
l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, l'8 aprile 2002. &#13;
                         Il Presidente: Vari &#13;
                     Il redattore: Neppi Modona &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 10 aprile 2002. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
