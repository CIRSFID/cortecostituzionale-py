<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1962</anno_pronuncia>
    <numero_pronuncia>90</numero_pronuncia>
    <ecli>ECLI:IT:COST:1962:90</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Biagio Petrocelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/11/1962</data_decisione>
    <data_deposito>22/11/1962</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Dott. &#13;
 MARIO COSATTI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO &#13;
 - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO &#13;
 PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE &#13;
 BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. &#13;
 GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 125 del D.L.  &#13;
 del Presidente della Regione siciliana 29 ottobre 1955, n. 6,  promosso  &#13;
 con  ordinanza  emessa  il  26  giugno  1961  dal Pretore di Lipari nel  &#13;
 procedimento penale a carico di Li Donni Giuseppe, iscritta al  n.  136  &#13;
 del Registro ordinanze 1961 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica  n.  232  del  16  settembre 1961 e nella Gazzetta Ufficiale  &#13;
 della Regione siciliana n. 47 del 31 agosto 1961.                        &#13;
     Visto l'atto di intervento del Presidente della Regione siciliana;   &#13;
     udita nell'udienza pubblica del 17 ottobre 1962  la  relazione  del  &#13;
 Giudice Biagio Petrocelli;                                               &#13;
     udito  l'avv.  Salvatore Pugliatti, per il Presidente della Regione  &#13;
 siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel procedimento penale a carico di Li Donni Giuseppe, imputato  di  &#13;
 contravvenzione  all'art.  2  del  regolamento  sulla  pescheria del 18  &#13;
 agosto 1945 del Comune di Lipari, per omesso conferimento di  un  pesce  &#13;
 spada  al  centro ittico, punibile ai sensi dell'art. 125 del D. L. del  &#13;
 Presidente della Regione siciliana 29 ottobre 1955, n. 6, il Pretore di  &#13;
 Lipari ha sollevato di ufficio,  con  ordinanza  del  26  giugno  1961,  &#13;
 questione  di  legittimità costituzionale del menzionato art. 125, per  &#13;
 violazione degli artt. 3, 5 e 25, secondo comma, della Costituzione,  e  &#13;
 per  essere in contrasto col principio più volte affermato dalla Corte  &#13;
 costituzionale, che la competenza a legiferare nella materia penale  è  &#13;
 devoluta esclusivamente allo Stato.                                      &#13;
     L'ordinanza,  regolarmente notificata e comunicata all'imputato, al  &#13;
 P. M. presso il Tribunale di Messina e ai  Presidenti  della  Giunta  e  &#13;
 dell'Assemblea  della  Regione  siciliana,  è  stata  pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica del 16 settembre 1961,  n.  232,  e  &#13;
 nella  Gazzetta   Ufficiale della Regione siciliana del 31 agosto 1961,  &#13;
 n. 47.                                                                   &#13;
     Con atto di intervento e  deduzioni  del  25  agosto  1961,  si  è  &#13;
 costituito   in   giudizio   il  Presidente  della  Regione  siciliana,  &#13;
 rappresentato e difeso dall'avv.  Salvatore Pugliatti.                   &#13;
     Nelle sue deduzioni la Regione siciliana, pur riconoscendo  in  via  &#13;
 generale  il  principio, affermato da questa Corte nelle sue precedenti  &#13;
 decisioni, che il potere normativo penale spetti soltanto  allo  Stato,  &#13;
 ritiene  che  la  Corte  stessa  non  si  sia  proposto ex pro fesso il  &#13;
 problema dei limiti entro i quali questo, che qualifica monopolio dello  &#13;
 Stato, dovrebbe essere mantenuto.                                        &#13;
     Una norma primaria sprovvista di sanzione sarebbe priva di forza  e  &#13;
 di  contenuto  pratico, specialmente quando, per la sua natura e per il  &#13;
 tipo di interessi (pubblici) che deve realizzare, ha bisogno di  essere  &#13;
 rafforzata   da   sanzioni   peculiari,   come  quelle  penali,  almeno  &#13;
 contravvenzionali. La negazione  della  potestà  penale  alla  Regione  &#13;
 condurrebbe  "al  paradossale  risultato  che  la  potestà legislativa  &#13;
 regionale apparirebbe massimamente  inefficiente  nelle  materie  nelle  &#13;
 quali,   istituzionalmente,   dovrebbe   conseguire   il   massimo   di  &#13;
 efficienza".  Il  problema,  pertanto,  dovrebbe  essere  esaminato  in  &#13;
 rapporto  alla  competenza  esclusiva della Regione per alcune materie.  &#13;
 L'attribuzione senza limiti della potestà normativa penale allo  Stato  &#13;
 significherebbe  attribuzione  allo stesso, sia pure limitatamente alle  &#13;
 sanzioni  penali,  di  una  competenza  legislativa  in   materie   che  &#13;
 istituzionalmente appartengono alla competenza esclusiva della Regione,  &#13;
 con  la  conseguenza  della  evidente  contraddizione  di  una potestà  &#13;
 normativa concorrente  là  dove  dovrebbe  riscontrarsi  una  potestà  &#13;
 normativa  esclusiva.  Né, d'altra parte, di tale potestà concorrente  &#13;
 sarebbe  possibile  l'esercizio, non essendo concepibile che la Regione  &#13;
 possa emanare norme primarie e chiedere, poi, allo  Stato  di  disporre  &#13;
 sanzioni  penali.  In  definitiva,  l'affermazione di una tale potestà  &#13;
 concorrente importerebbe la soppressione  di  ogni  potestà  normativa  &#13;
 sanzionatoria  per  le  materie riservate alla Regione.  Infatti, da un  &#13;
 lato, questa, priva della potestà  penale,  non  potrebbe  emanare  le  &#13;
 norme  sanzionatorie,  e  dall'altro  lo Stato, apparentemente titolare  &#13;
 della  indicata  potestà,  non  sarebbe  in  grado   di   esercitarla,  &#13;
 considerato  che  rispetto  alla stessa materia la potestà legislativa  &#13;
 spetta esclusivamente alla Regione. A questa, in definitiva, secondo la  &#13;
 difesa, non si può non  riconoscere  "la  potestà  di  emanare  norme  &#13;
 sanzionatorie,  anche  penali,  nelle  materie  nelle quali ha potestà  &#13;
 esclusiva rispetto alle norme primarie". Una soluzione della  questione  &#13;
 così  come  ora è prospettata non troverebbe ostacolo negli argomenti  &#13;
 finora addotti a sostegno della esclusiva appartenenza allo  Stato  del  &#13;
 potere normativo penale.                                                 &#13;
     La  difesa della Regione osserva, infine, che gli artt. 3 e 5 della  &#13;
 Costituzione,  richiamati  nell'ordinanza   del   Pretore,   non   sono  &#13;
 pertinenti. Circa, poi, l'art. 25 della Costituzione, anche richiamato,  &#13;
 e  sul  quale la Corte costituzionale ha fondata la sua decisione n. 21  &#13;
 del 1957, si afferma che è discutibile  la  sostenuta  interpretazione  &#13;
 restrittiva del termine "legge", nello stesso articolo contenuto.        &#13;
     In  data  9  ottobre è stata presentata dalla difesa della Regione  &#13;
 una memoria fuori termine, nella quale si insiste sugli argomenti  già  &#13;
 enunciati  e sulla necessità di ammettere che la Regione possa emanare  &#13;
 norme sanzionatorie contravvenzionali.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La difesa della Regione ha ritenuto nella  presente  causa  di  &#13;
 poter  proporre come nuovo, nella materia in questione, il problema dei  &#13;
 limiti di cui immancabilmente, a suo giudizio, dovrebbe essere  oggetto  &#13;
 il  cosiddetto monopolio statale della potestà normativa penale. Ma il  &#13;
 problema  è  da  considerarsi  risoluto  nei  termini   stessi   delle  &#13;
 precedenti  decisioni di questa Corte. Respinta la configurazione di un  &#13;
 diritto penale speciale accanto al diritto penale  generale  o  comune,  &#13;
 unico  essendo  il  diritto  penale  in base alla natura delle sanzioni  &#13;
 imposte dalla legge e quali che  siano  gli  interessi  con  tal  mezzo  &#13;
 tutelati,  la  Corte  ha  fissato  due  punti  fondamentali:  l'uno che  &#13;
 stabilisce la riserva assoluta di legge in materia penale; l'altro, che  &#13;
 del primo costituisce il razionale fondamento, riguarda la  natura  dei  &#13;
 beni dell'uomo su cui incide la sanzione penale.                         &#13;
     Nella  sentenza  n.  21 del 1957, la disposizione del comma secondo  &#13;
 dell'art. 25 della Costituzione, in virtù  della  quale  nessuno  può  &#13;
 essere  punito  se  non in forza di una legge che sia entrata in vigore  &#13;
 prima del fatto commesso, è stata interpretata nel senso che per legge  &#13;
 debba intendersi, in tale disposizione, soltanto la legge dello  Stato;  &#13;
 ed ora questa Corte non ha che da rimettersi a questa interpretazione e  &#13;
 alla motivazione che allora ne fu data.                                  &#13;
     Nella  stessa  sentenza  la esclusiva appartenenza allo Stato della  &#13;
 potestà normativa penale trovò come  suo  fondamento  la  particolare  &#13;
 natura  delle  restrizioni  della  sfera giuridica che vengono inflitte  &#13;
 mediante la pena.  La quale incide sui beni ed  attributi  fondamentali  &#13;
 della  persona  umana,  in  primo  luogo la libertà personale; onde la  &#13;
 necessità che tali restrizioni  siano  da  stabilire  in  base  a  una  &#13;
 generale  e,  si  potrebbe aggiungere, superiore valutazione dei beni e  &#13;
 degli  interessi  dell'uomo  e  della  vita  sociale, quale può essere  &#13;
 compiuta soltanto dal legislatore statale.                               &#13;
     Stabiliti questi due punti, la impossibilità di porre limiti  alla  &#13;
 potestà  penale  dello  Stato  a  favore  di altri enti appare in modo  &#13;
 evidente. Ogni limite, infatti, non potrebbe non costituire  violazione  &#13;
 della  norma costituzionale che soltanto la legge dello Stato riconosce  &#13;
 come fonte della norma penale; e sarebbe in  pari  tempo  negazione  di  &#13;
 quei  supremi  interessi  umani  la  cui restrizione a mezzo della pena  &#13;
 spetta soltanto allo Stato di valutare. Né varrebbe obbiettare che una  &#13;
 eventuale potestà normativa penale  della  Regione  avrebbe  carattere  &#13;
 limitato,  in quanto riferibile soltanto ai particolari interessi della  &#13;
 Regione  stessa  e  come  presidio  delle  sole  norme  giuridiche  che  &#13;
 statutariamente essa ha il potere di emettere. È più che evidente che  &#13;
 soltanto entro questi limiti è ipoteticamente concepibile una potestà  &#13;
 penale  della  Regione; e, pertanto, è soltanto relativamente a questa  &#13;
 ridotta  possibilità  che  viene  riaffermato   il   principio   della  &#13;
 esclusività  del  potere  statale, non ponendosi nemmeno come astratta  &#13;
 ipotesi la idea di un potere penale della Regione che vada al di là di  &#13;
 quei limiti.                                                             &#13;
     Si  assume  che  le  norme   della   Regione   richiederebbero   il  &#13;
 rafforzamento  di  sanzioni penali "almeno contravvenzionali". Ora, che  &#13;
 lo Stato, nel disporre la tutela dell'attività degli altri enti che lo  &#13;
 compongono a mezzo di sanzioni penali, riduca  in  modo  espresso  tale  &#13;
 intervento  a  fattispecie  soltanto  contravvenzionali,  è  cosa  che  &#13;
 rientra nel  normale  esercizio  della  potestà  legislativa  e  nella  &#13;
 determinazione  di  limiti  che per varie ragioni questa stabilisce. Ma  &#13;
 allorché si tende, come nella presente controversia e come nelle altre  &#13;
 precedenti sullo  stesso  oggetto,  a  dedurre  dai  principi,  e  come  &#13;
 implicita  nell'ordinamento,  l'esistenza  di una potestà penale delle  &#13;
 Regioni,  la  limitazione  alle   fattispecie   contravvenzionali   con  &#13;
 esclusione  di  quelle  dei  delitti  non  troverebbe alcun fondamento;  &#13;
 palesandosi in tal modo in tutta la sua estensione l'entità del potere  &#13;
 che alla Regione si vorrebbe rivendicare.                                &#13;
     2. - Di fronte alle considerazioni che precedono nemmeno può  aver  &#13;
 valore   l'obbiezione   che,   in   modo  particolare,  riferisce  alla  &#13;
 legislazione regionale esclusiva la necessità di una estensione  della  &#13;
 potestà  penale  alla  Regione.  Con  l'attribuzione di una competenza  &#13;
 legislativa esclusiva si ha una riserva piena di  competenza  a  favore  &#13;
 della   Regione   per   talune  materie,  nel  senso  che  è  devoluta  &#13;
 esclusivamente  alla  Regione  la  concreta  disciplina  dei   rapporti  &#13;
 inerenti a queste materie; ma non nel senso che, per le stesse materie,  &#13;
 debba  ritenersi  in  sua potestà ogni specie e forma di tutela, anche  &#13;
 quella che è dalla Costituzione attribuita esclusivamente allo  Stato.  &#13;
 La  sanzione  penale  può  costituire  il  più  energico  ed efficace  &#13;
 presidio del  comando  giuridico,  ma  non  per  questo  può  di  esso  &#13;
 considerarsi  un  attributo  essenziale,  in  forza del quale, entro il  &#13;
 potere esclusivo  della  Regione  di  regolare  legislativamente  certi  &#13;
 rapporti,  debba  necessariamente  ritenersi inclusa anche una autonoma  &#13;
 disponibilità della tutela penale. Altre sanzioni, come  questa  Corte  &#13;
 ha  già  rilevato,  possono efficacemente tutelare le norme giuridiche  &#13;
 regionali. La eventuale ritenuta insufficienza di tali  sanzioni  e  la  &#13;
 conseguente  necessità  di  una  tutela penale è problema di politica  &#13;
 legislativa estraneo alle decisioni di questa Corte; e  che,  comunque,  &#13;
 si risolve invocando l'intervento delle norme penali dello Stato, senza  &#13;
 che  ciò  possa  minimamente  influire sul carattere esclusivo, per le  &#13;
 materie per cui è stabilito, della legislazione regionale,  né  tanto  &#13;
 meno  importare,  come  si  assume,  l'inserimento  di  una  normazione  &#13;
 concorrente dello Stato in quella che  deve  rimanere  esclusiva  della  &#13;
 Regione.                                                                 &#13;
     D'altra  parte, non è nemmeno valida l'osservazione di una pratica  &#13;
 impossibilità dell'intervento dello Stato in considerazione del  fatto  &#13;
 che la Regione, emanate le norme primarie, dovrebbe poi richiedere allo  &#13;
 Stato  l'emanazione  delle  norme  sanzionatorie. Analogamente a quanto  &#13;
 avviene per la tutela dell'attività  di  altri  enti,  lo  Stato  può  &#13;
 fornire  alla  legislazione  regionale  una o più norme penali, che ne  &#13;
 costituiscano, opportunamente  articolate,  il  preventivo  e  generale  &#13;
 presidio;  del  che  la  Corte  non  ha  motivo  di  non riconoscere la  &#13;
 opportunità.                                                            &#13;
     Si deve,  pertanto,  dichiarare  la  illegittimità  costituzionale  &#13;
 della  norma impugnata. Spetterà poi al giudice di merito lo stabilire  &#13;
 se  al  fatto  che  diede  luogo  al  procedimento  penale  non   possa  &#13;
 eventualmente  applicarsi  una  vigente  norma  penale dell'ordinamento  &#13;
 statale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la illegittimità costituzionale dell'art. 125 del  D.Leg.  &#13;
 del  Presidente  della  Regione siciliana del 29 ottobre 1955, n. 6, in  &#13;
 riferimento all'art. 25, secondo comma, della Costituzione.              &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 12 novembre 1962.                             &#13;
                                   GASPARE  AMBROSINI  - MARIO COSATTI -  &#13;
                                   GIUSEPPE CASTELLI AVOLIO  -  ANTONINO  &#13;
                                   PAPALDO  -  NICOLA  JAEGER - GIOVANNI  &#13;
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  &#13;
                                   ANTONIO   MANCA  -  ALDO  SANDULLI  -  &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI - GIUSEPPE  VERZÌ.</dispositivo>
  </pronuncia_testo>
</pronuncia>
