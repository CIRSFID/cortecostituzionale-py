<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>370</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:370</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/07/1999</data_decisione>
    <data_deposito>28/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, &#13;
 prof. Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. &#13;
 Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 10,  lett.  h),    &#13;
 del  d.P.R.  29  settembre  1973,  n.  597  (Istituzione e disciplina    &#13;
 dell'imposta  sul  reddito  delle  persone  fisiche),  promosso   con    &#13;
 ordinanza  emessa  il  10  novembre 1997 dalla Commissione tributaria    &#13;
 regionale di Milano sul ricorso proposto dall'Ufficio  delle  imposte    &#13;
 dirette  di Milano contro Andreani Alessandro, iscritta al n. 645 del    &#13;
 registro delle ordinanze 1998 e pubblicata della  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 38, prima serie speciale, dell'anno 1998.            &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio  del  23  giugno  1999  il  giudice    &#13;
 relatore Fernanda Contri;                                                &#13;
   Ritenuto  che  la  Commissione  tributaria regionale di Milano, con    &#13;
 ordinanza del 10 novembre 1997, ha  sollevato,  in  riferimento  agli    &#13;
 artt.   3  e  53,  primo  comma,  della  Costituzione,  questione  di    &#13;
 legittimità costituzionale dell'art. 10, lettera h), del  d.P.R.  29    &#13;
 settembre  1973,  n.  597  (Istituzione e disciplina dell'imposta sul    &#13;
 reddito delle persone  fisiche),  nella  parte  in  cui  consente  la    &#13;
 deduzione dal reddito imponibile degli importi per assegni alimentari    &#13;
 corrisposti  alle  persone  indicate nell'art. 433 del codice civile,    &#13;
 solo se essi risultino da provvedimenti  dell'autorità  giudiziaria,    &#13;
 con   esclusione  di  quelli  prestati  spontaneamente  dal  soggetto    &#13;
 obbligato;                                                               &#13;
     che  la  controversia  davanti  al  giudice  a  quo  concerne  la    &#13;
 deducibilità  dal  reddito  imponibile  delle  rette  pagate  da  un    &#13;
 contribuente per il ricovero della madre - soggetto fiscalmente a suo    &#13;
 carico  -  in  una  casa  di  riposo  e  qualificate   dal   medesimo    &#13;
 contribuente come assegni alimentari;                                    &#13;
     che  il  rimettente,  ritenuta  l'applicabilità alla fattispecie    &#13;
 della norma impugnata, osserva  come  essa,  nella  sua  formulazione    &#13;
 letterale,  non  ammetta una interpretazione estensiva, favorevole al    &#13;
 contribuente;                                                            &#13;
     che, secondo il giudice  a  quo  la  scelta  del  legislatore  di    &#13;
 consentire  la  deduzione nei limiti anzidetti sarebbe irragionevole,    &#13;
 trovando gli obblighi alimentari la  loro  causa  direttamente  nella    &#13;
 legge,  ed  avendo il provvedimento dell'autorità giudiziaria valore    &#13;
 di  mero  accertamento  dell'obbligo,  con  la  conseguenza   di   un    &#13;
 trattamento  deteriore  per  chi  osserva  spontaneamente  il proprio    &#13;
 obbligo rispetto a chi vi si sottopone solo a seguito dell'intervento    &#13;
 del giudice;                                                             &#13;
     che, ad avviso del giudice a quo la disposizione impugnata  viola    &#13;
 il principio di eguaglianza stabilito dall'art. 3 della Costituzione,    &#13;
 alla   luce   del  quale  deve  essere  inteso  anche  l'obbligo  dei    &#13;
 contribuenti di concorrere alle spese pubbliche in ragione della loro    &#13;
 capacità contributiva, di cui all'art. 53 della Costituzione;           &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  chiedendo  alla  Corte  di  dichiarare  inammissibile,  o  in    &#13;
 subordine   infondata,   la  questione  sollevata  dalla  Commissione    &#13;
 tributaria regionale di Milano;                                          &#13;
     che,  ad  avviso  dell'Avvocatura,  la  fattispecie  oggetto  del    &#13;
 giudizio davanti al giudice a quo trova la sua  regolamentazione  non    &#13;
 nella norma impugnata ma nell'art. 15, n. 3), del d.P.R. 29 settembre    &#13;
 1973,  n.  597,  il  quale  disciplina le detrazioni di imposta per i    &#13;
 contribuenti con carichi di famiglia, consentendo la detrazione di un    &#13;
 importo fisso per ciascuna delle persone di  cui  all'art.  433  cod.    &#13;
 civ.  che  convivano col contribuente, o che percepiscano assegni non    &#13;
 risultanti   da   provvedimenti   dell'autorità   giudiziaria,   con    &#13;
 conseguente irrilevanza della questione nel giudizio in corso davanti    &#13;
 al giudice tributario;                                                   &#13;
     che,  sempre  secondo l'Avvocatura, la questione sarebbe comunque    &#13;
 infondata,  dal  momento  che  gli  assegni  alimentari  e  le  spese    &#13;
 sostenute  per  il mantenimento dei familiari non sono, in assenza di    &#13;
 provvedimento  del  giudice,  fiscalmente  irrilevanti,  ma   trovano    &#13;
 riconoscimento nel regime delle detrazioni di imposta.                   &#13;
   Considerato  che  la  questione  sottoposta  all'esame  della Corte    &#13;
 risulta ammissibile, posto che nel giudizio a quo si  controverte  in    &#13;
 ordine  alla  deduzione dal reddito imponibile, ai fini della imposta    &#13;
 sul  reddito  delle  persone  fisiche,   di   oneri   sostenuti   dal    &#13;
 contribuente per il mantenimento di una delle persone di cui all'art.    &#13;
 433 cod.  civ;                                                           &#13;
     che,  in  generale,  riguardo  al  sistema  delle  deduzioni  dal    &#13;
 reddito,  la  Corte  ha  avuto  occasione   di   chiarire   che   "la    &#13;
 detraibilità non è secondo Costituzione necessariamente generale ed    &#13;
 illimitata,  ma va concretata e commisurata dal legislatore ordinario    &#13;
 secondo un criterio che concili le esigenze finanziarie  dello  Stato    &#13;
 con quelle del cittadino" (sentenza n. 134 del 1982) e che "spetta al    &#13;
 legislatore, secondo le sue valutazioni discrezionali, di individuare    &#13;
 gli  oneri  deducibili considerando il necessario collegamento con la    &#13;
 produzione del reddito, il nesso di proporzionalità con  il  gettito    &#13;
 generale  dei tributi, nonché l'esigenza fondamentale di adottare le    &#13;
 opportune cautele contro le evasioni di imposta" (sentenza n. 143 del    &#13;
 1982; v. anche le sentenze nn. 108 del 1983  e  239  del  1993  e  le    &#13;
 ordinanze nn. 948 del 1988 e 556 del 1987);                              &#13;
     che la deduzione dal reddito imponibile degli assegni alimentari,    &#13;
 limitata  alla  misura  risultante  da  provvedimento  dell'autorità    &#13;
 giudiziaria, corrisponde ad una scelta del  legislatore  ispirata  ad    &#13;
 esigenze  di  certezza  nella  individuazione degli oneri detraibili,    &#13;
 altrimenti  lasciata  alla   volontà   del   contribuente   o   alla    &#13;
 discrezionalità dell'amministrazione finanziaria;                       &#13;
     che  il  sistema,  all'art. 15, primo comma, n. 3), del d.P.R. 29    &#13;
 settembre 1973, n. 597, già prevede  detrazioni  di  imposta  per  i    &#13;
 familiari a carico del contribuente;                                     &#13;
     che  pertanto  la  questione  di  legittimità  costituzionale è    &#13;
 manifestamente infondata.                                                &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 10, lettera  h),  del  d.P.R.  29  settembre    &#13;
 1973, n. 597 (Istituzione e disciplina dell'imposta sul reddito delle    &#13;
 persone  fisiche),  sollevata, in riferimento agli artt. 3 e 53 della    &#13;
 Costituzione, dalla Commissione tributaria regionale  di  Milano  con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 luglio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 28 luglio 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
