<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>141</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:141</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1989</data_decisione>
    <data_deposito>21/03/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  29, terzo    &#13;
 comma, della  legge  4  aprile  1952,  n.  218  (Riordinamento  della    &#13;
 pensione   dell'assicurazione   obbligatoria  per  l'invalidità,  la    &#13;
 vecchiaia ed i superstiti),  promosso  con  ordinanza  emessa  il  20    &#13;
 luglio  1988  dal  Giudice  conciliatore  di Ferrara nel procedimento    &#13;
 civile vertente tra Squarzoni Ida e l'I.N.P.S., iscritta  al  n.  638    &#13;
 del  registro  ordinanze  1988  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 47, prima serie speciale dell'anno 1988;             &#13;
    Visto  l'atto  di  costituzione  dell'I.N.P.S.,  nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nell'udienza  pubblica  del  21  febbraio  1989  il Giudice    &#13;
 relatore Aldo Corasaniti;                                                &#13;
    Udito l'Avvocato dello Stato Luigi Siconolfi per il Presidente del    &#13;
 Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il Giudice Conciliatore di Ferrara, con ordinanza emessa il    &#13;
 20  luglio  1988  nel  procedimento  civile  tra  Squarzoni  Rita   e    &#13;
 l'I.N.P.S., ha sollevato questione di legittimità costituzionale, in    &#13;
 riferimento agli artt. 3 e 47 della Costituzione, dell'art. 29, comma    &#13;
 terzo,  della  legge  4  aprile  1952,  n.  218  (Riordinamento delle    &#13;
 pensioni  dell'assicurazione  obbligatoria  per   l'invalidità,   la    &#13;
 vecchiaia  ed  i superstiti), nella parte in cui non prevede, in tema    &#13;
 di assicurazione facoltativa  presso  l'I.N.P.S.,  una  rivalutazione    &#13;
 costante  nel  tempo  per  ogni  anno  successivo  al  1947  sino  al    &#13;
 pensionamento e anche posteriormente.                                    &#13;
    Rileva  il giudice a quo che il cittadino ha la facoltà di essere    &#13;
 iscritto al regime di assicurazione  facoltativa  presso  l'I.N.P.S.,    &#13;
 nel  qual  caso  deve  provvedere ad effettuare versamenti nel tempo.    &#13;
 Mentre  per  l'assicurazione  obbligatoria   è   stato   per   legge    &#13;
 predisposto  un  congegno  di scala mobile per le pensioni in essere,    &#13;
 nonché per la rivalutazione delle contribuzioni ai fini del  calcolo    &#13;
 della  pensione  originaria, per l'assicurazione facoltativa nulla di    &#13;
 ciò è stato stabilito successivamente alla legge n. 218  del  1952.    &#13;
 Di  conseguenza,  persone  che  hanno  per anni versato contribuzioni    &#13;
 volontarie  si  vedono  ridurre  in   maniera   radicale   il   rateo    &#13;
 pensionistico,  che  in  alcuni  casi  può  arrivare  addirittura ad    &#13;
 azzerarsi. La qual cosa sembra: a) creare disparità  di  trattamento    &#13;
 tra  assicurati  obbligatoriamente  e assicurati facoltativamente, in    &#13;
 violazione dell'art. 3 della  Costituzione;  b)  esser  contraria  al    &#13;
 disposto  dell'art.  47  della  Costituzione,  perché il trattamento    &#13;
 riservato a chi opta per l'assicurazione volontaria non incoraggia il    &#13;
 risparmio.                                                               &#13;
    2. - L'I.N.P.S., costituitosi, ha dedotto l'inammissibilità della    &#13;
 questione e, in subordine, l'infondatezza.                               &#13;
    3.  -  È  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, che  ha  concluso  per  la    &#13;
 declaratoria di inammissibilità della questione.<diritto>Considerato in diritto</diritto>1.  - Il Giudice conciliatore di Ferrara, nel corso di un giudizio    &#13;
 tendente ad ottenere, nei confronti dell'I.N.P.S.,  la  rivalutazione    &#13;
 dei  contributi  versati  ai  fini dell'assicurazione facoltativa per    &#13;
 l'invalidità, la vecchiaia ed i superstiti, ha  sollevato  questione    &#13;
 di  legittimità  costituzionale,  in  riferimento  agli artt. 3 e 47    &#13;
 della Costituzione, dell'art. 29, terzo comma, della legge  4  aprile    &#13;
 1952,   n.   218  (Riordinamento  delle  pensione  dell'assicurazione    &#13;
 obbligatoria per l'invalidità, la vecchiaia ed i superstiti),  nella    &#13;
 parte in cui non prevede la rivalutazione delle contribuzioni versate    &#13;
 dal 1° gennaio 1948 in poi.                                              &#13;
    Da  tale omissione conseguirebbe, ad avviso del giudice a quo : a)    &#13;
 una  ingiustificata  discriminazione  sfavorevole  degli   assicurati    &#13;
 facoltativamente  rispetto agli assicurati obbligatoriamente, i quali    &#13;
 ultimi soltanto beneficiano di meccanismi di rivalutazione, idonei ad    &#13;
 evitare che la pensione, per effetto della svalutazione monetaria, si    &#13;
 riduca ad entità irrisoria; b)  la  dissuasione  dal  risparmio,  in    &#13;
 vista della costituzione di forme volontarie di previdenza.              &#13;
    2.  -  Osserva  la  Corte  che,  quando  venne  istituita,  con il    &#13;
 decreto-legge luogotenenziale 21 aprile 1919, n. 603, l'assicurazione    &#13;
 obbligatoria  contro  l'invalidità  e  la vecchiaia per i lavoratori    &#13;
 dipendenti, i limiti di  applicazione  della  detta  assicurazione  -    &#13;
 dalla  quale  non  erano  coperti i lavoratori dipendenti con redditi    &#13;
 eccedenti determinati livelli (art.  2),  nonché  la  totalità  dei    &#13;
 lavoratori  autonomi (artigiani, commercianti, casalinghe, pescatori,    &#13;
 coltivatori diretti, coloni e mezzadri, professionisti)  -  indussero    &#13;
 il    legislatore   a   conservare   opportunamente   l'assicurazione    &#13;
 facoltativa, già costituente l'unica forma assicurativa.                &#13;
    Non  vi  è  dubbio,  quindi, che, sin dal momento in cui ha avuto    &#13;
 inizio  la  coesistenza   delle   due   forme   assicurative,   anche    &#13;
 l'assicurazione  facoltativa  era  inspirata  a fini di previdenza in    &#13;
 quanto svolgeva la funzione di  consentire  la  costituzione  di  una    &#13;
 pensione,  con  versamenti  volontari,  a  favore  dei lavoratori non    &#13;
 ricompresi nell'obbligo assicurativo, nonché quella di consentire ai    &#13;
 lavoratori  appartenenti  alle  categorie  soggette  all'obbligo,  di    &#13;
 aumentare, con versamenti volontari, la propria pensione (art. 30 del    &#13;
 decreto-legge luotenenziale n. 603 del 1919). Tanto più che anche la    &#13;
 detta assicurazione facoltativa  riguardava  lavoratori  appartenenti    &#13;
 alle  categorie  meno  abbienti  (art. 30 citato). Siffatti connotati    &#13;
 l'assicurazione facoltativa ha mantenuto anche in seguito (cfr.  art.    &#13;
 85  del regio decreto-legge 4 ottobre 1935, n. 1827), e li conservava    &#13;
 all'atto dell'emanazione della disposizione impugnata (art. 29  della    &#13;
 legge  n.  218  del  1952),  giacché  la prima delle due funzioni di    &#13;
 tutela  previdenziale  dianzi  richiamate  si  è   invero   alquanto    &#13;
 attenuata  solo  in seguito, per effetto della progressiva estensione    &#13;
 dell'assicurazione obbligatoria a  sempre  più  vaste  categorie  di    &#13;
 lavoratori  (cfr.,  tra  le altre, la legge 26 ottobre 1957, n. 1047,    &#13;
 relativa ai coltivatori diretti, coloni e mezzadri, la legge 4 luglio    &#13;
 1959,  n.  463,  relativa agli artigiani, la legge 22 luglio 1966, n.    &#13;
 613, relativa ai commercianti). E non può trascurarsi di considerare    &#13;
 che  l'eventuale  adozione  di  sistemi  facoltativi  integrativi  di    &#13;
 previdenza è materia  ora  oggetto  di  numerosi  progetti  in  sede    &#13;
 legislativa.                                                             &#13;
    Ciò  premesso, è da notare che la norma impugnata ha considerato    &#13;
 il fenomeno della svalutazione monetaria fino  alla  data  della  sua    &#13;
 entrata  in  vigore,  anche se ha limitato la disposta rivalutazione,    &#13;
 secondo dati  parametri,  ai  contributi  versati  nell'assicurazione    &#13;
 facoltativa a tutto l'anno 1947.                                         &#13;
    Orbene,  la  mancata  considerazione,  anche  per  il  futuro, del    &#13;
 fenomeno della svalutazione, già in atto, nell'ambito di un  sistema    &#13;
 correlante  le  prestazioni all'importo dei contributi, tanto più in    &#13;
 una normativa che di tale fenomeno si dà carico per il passato, e la    &#13;
 conseguente  mancata adozione di un congegno di rivalutazione per gli    &#13;
 anni successivi all'entrata in vigore della legge  stessa,  inficiano    &#13;
 la  norma  impugnata  di  irragionevolezza. Invero, avuto riguardo al    &#13;
 fine  previdenziale  perseguito   dalla   assicurazione   facoltativa    &#13;
 (omogeneo,  del  resto,  rispetto  a  quello perseguito, sia pure con    &#13;
 mezzi diversi, dall'assicurazione obbligatoria), l'omissione  oggetto    &#13;
 di  censura  rende  la  norma stessa non rispondente al fine medesimo    &#13;
 sotto il profilo dell'effettività, in esso naturalmente implicito ed    &#13;
 attuato per le categorie degli assicurati obbligatoriamente.             &#13;
    Va pertanto dichiarata l'illegittimità costituzionale della norma    &#13;
 impugnata in parte qua.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara l'illegittimità costituzionale dell'art. 29, comma terzo,    &#13;
 della legge 4 aprile  1952,  n.  218  (Riordinamento  delle  pensioni    &#13;
 dell'assicurazione  obbligatoria per l'invalidità, la vecchiaia ed i    &#13;
 superstiti),  nella  parte  in  cui  non  prevede  un  meccanismo  di    &#13;
 adeguamento  dell'importo  nominale dei contributi versati dal giorno    &#13;
 della sua entrata in vigore in poi.                                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1989.                                  &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CORASANITI                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 marzo 1989.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
