<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>94</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:94</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/05/1971</data_decisione>
    <data_deposito>11/03/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof VINCENZO MICHELE &#13;
 TRIMARCHI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2  bis  della  &#13;
 legge   28   settembre   1966,   n.  749  (conversione  in  legge,  con  &#13;
 modificazioni, del d.l. 30 luglio 1966, n. 590, recante provvedimenti a  &#13;
 favore della città di Agrigento in conseguenza del  movimento  franoso  &#13;
 verificatosi  il  19  luglio 1966), promosso con ordinanza emessa il 20  &#13;
 marzo 1970 dal pretore di Agrigento nel procedimento penale a carico di  &#13;
 Ferlisi Salvatore  e  Giuseppe,  iscritta  al  n.    175  del  registro  &#13;
 ordinanze  1970  e pubblicata nella Gazzetta Ufficiale della Repubblica  &#13;
 n. 150 del 17 giugno 1970.                                               &#13;
     Visto  l'atto  d'intervento  del  Presidente  del   Consiglio   dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica del 10 marzo 1971 il Giudice relatore  &#13;
 Paolo Rossi;                                                             &#13;
     udito  il  sostituto   avvocato   generale   dello   Stato   Franco  &#13;
 Casamassima, per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di un procedimento penale a carico dei fratelli Ferlisi  &#13;
 imputati di varie contravvenzioni  per  aver  iniziato,  sprovvisti  di  &#13;
 licenza,  la  costruzione  di  un capannone industriale nella Valle dei  &#13;
 Templi di Agrigento, in zona ricadente  nel  perimetro  delimitato  dal  &#13;
 decreto  ministeriale  16  maggio  1968,  il  pretore  di  Agrigento ha  &#13;
 sollevato questione di  legittimità  costituzionale  dell'art.  2  bis  &#13;
 della  legge  28  settembre  1966,  n.  749,  in base al quale è stato  &#13;
 emanato il predetto decreto ministeriale.                                &#13;
     La norma impugnata, dichiarando la Valle dei  Templi  di  Agrigento  &#13;
 "zona  archeologica  d'interesse nazionale", ed attribuendo al Ministro  &#13;
 per la pubblica istruzione, di concerto con quello dei lavori pubblici,  &#13;
 il compito di determinare, "con proprio  decreto,  il  perimetro  della  &#13;
 zona,  le  prescrizioni  d'uso,  ed  i  vincoli  di  inedificabilità",  &#13;
 violerebbe la riserva di legge stabilita dall'art. 42,  secondo  comma,  &#13;
 della  Costituzione,  per aver demandato alla pubblica Amministrazione,  &#13;
 senza prestabilire adeguati criteri direttivi, il potere di  delimitare  &#13;
 la  zona  e  di determinare i modi di godimento del diritto dominicale,  &#13;
 con possibilità di inibire totalmente lo jus aedificandi.               &#13;
     In secondo luogo, prosegue  l'ordinanza  di  remissione,  la  norma  &#13;
 impugnata  violerebbe  l'art.  14  (lett.  n)  dello  Statuto regionale  &#13;
 siciliano  -  secondo  cui  rientrano  nella   competenza   legislativa  &#13;
 esclusiva  della  regione  la  tutela  del paesaggio e la conservazione  &#13;
 delle antichità, e delle opere artistiche -  per  aver  attribuito  al  &#13;
 Ministro   per   la  pubblica  istruzione  le  corrispondenti  funzioni  &#13;
 amministrative, spettanti invece al Presidente della Regione.            &#13;
     Si è costituito in questa sede il  Presidente  del  Consiglio  dei  &#13;
 ministri,  rappresentato e difeso dall'Avvocatura generale dello Stato,  &#13;
 con  atto  di  intervento  depositato  il  7  luglio  1970,   chiedendo  &#13;
 dichiararsi l'infondatezza della questione sollevata.                    &#13;
     In   ordine  al  primo  profilo  di  illegittimità  costituzionale  &#13;
 prospettato dal giudice di merito, la difesa dello Stato  premette  che  &#13;
 secondo  i  principi  generali  (cfr.  artt. 11, 12 e 21 legge 1 giugno  &#13;
 1939, n. 1089), l'interesse storico o artistico, di un  immobile  o  di  &#13;
 una  intera  zona viene di solito riconosciuto e dichiarato mediante un  &#13;
 mero provvedimento amministrativo, cui consegue  l'assoggettamento  del  &#13;
 bene   allo   speciale   status  stabilito  ex  lege,  con  l'ulteriore  &#13;
 conseguenza che alla  stessa  Amministrazione  compete  l'adozione  dei  &#13;
 provvedimenti   esecutivi  richiesti  dalla  natura  del  bene  stesso;  &#13;
 soggiunge quindi l'Avvocatura che la particolarità  ravvisabile  nella  &#13;
 specie  -  riconoscimento  della  natura archeologica del bene mediante  &#13;
 atto legislativo, e adozione dei provvedimenti conseguenziali con  atto  &#13;
 amministrativo  -  non  rileva  ai  fini  del profilo di illegittimità  &#13;
 costituzionale denunciato.                                               &#13;
     Comunque passando ad esaminare il complesso dei  poteri  spettanti,  &#13;
 in subiecta materia, alla pubblica Amministrazione, concretantisi nella  &#13;
 determinazione del perimetro della Valle dei Templi, delle prescrizioni  &#13;
 d'uso  e dei vincoli di inedificabilità, la difesa dello Stato osserva  &#13;
 che tali attività rientrano istituzionalmente nei compiti  del  potere  &#13;
 esecutivo,  costituendo  esercizio  della discrezionalità tecnica.  La  &#13;
 stessa Corte costituzionale controllando, sempre  in  riferimento  alla  &#13;
 riserva  di  legge  di cui all'art. 42, secondo comma, della Carta, gli  &#13;
 analoghi poteri, spettanti ai Comuni ex art. 7 della legge urbanistica,  &#13;
 per la ripartizione in zone edificabili o meno del territorio comunale,  &#13;
 ha  riconosciuto  che  siffatta  discrezionalità,  non  illimitata,  e  &#13;
 soggetta  a  sindacato  giurisdizionale, non contrasta con il principio  &#13;
 costituzionale invocato.                                                 &#13;
     Per  quanto  attiene   al   secondo   profilo   di   illegittimità  &#13;
 costituzionale   denunciato  (pretesa  violazione  dell'art.  14  dello  &#13;
 Statuto), la difesa dello Stato rileva che la questione prospettata  è  &#13;
 stata già recentemente dichiarata infondata dalla Corte costituzionale  &#13;
 con  la sentenza n. 74 del 1969, né sono stati addotti nuovi argomenti  &#13;
 dal  pretore  di  Agrigento,  che  sembra  aver  ignorato  la  predetta  &#13;
 decisione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  La  Corte  costituzionale è chiamata a decidere le seguenti  &#13;
 questioni:                                                               &#13;
      a) se l'art. 2 bis della legge 28  settembre  1966,  n.    749,  -  &#13;
 disponendo  che  "la  Valle  dei Templi di Agrigento è dichiarata zona  &#13;
 archeologica di interesse nazionale" e che "il Ministro per la pubblica  &#13;
 istruzione,  di  concerto  con  il  Ministro  per  i  lavori  pubblici,  &#13;
 determina,   con   proprio   decreto,   il  perimetro  della  zona,  le  &#13;
 prescrizioni d'uso, i vincoli di inedificabilità" - contrasti  o  meno  &#13;
 con  la  riserva  di  legge  di  cui  all'art. 42, secondo comma, della  &#13;
 Costituzione, per non  aver  specificato  i  criteri  cui  la  pubblica  &#13;
 Amministrazione avrebbe dovuto adeguarsi nel dare esecuzione al dettato  &#13;
 legislativo;                                                             &#13;
     b)  se  la  norma  predetta,  provvedendo  in siffatta maniera alla  &#13;
 tutela del complesso archeologico della Valle dei Templi, non contrasti  &#13;
 con  l'art.  14  dello  Statuto  regionale,  secondo  cui   l'Assemblea  &#13;
 regionale   siciliana  ha  la  legislazione  esclusiva  in  materia  di  &#13;
 "conservazione delle antichità e delle opere artistiche".               &#13;
     2. - Secondo la costante giurisprudenza di questa Corte la  riserva  &#13;
 relativa di legge, invocata anche nella specie, consente al legislatore  &#13;
 di attribuire alla pubblica Amministrazione il potere di incidere sulla  &#13;
 concreta  disciplina del godimento degli immobili "qualora, nella legge  &#13;
 ordinaria, siano contenuti  elementi  e  criteri  idonei  a  delimitare  &#13;
 chiaramente  la  discrezionalità dell'Amministrazione" (sentenza n. 38  &#13;
 del 1966).                                                               &#13;
     Di tale principio la Corte fece applicazione in relazione agli ampi  &#13;
 poteri conferiti ai Comuni dall'art. 7 della legge urbanistica, secondo  &#13;
 cui,  com'è  noto,  mediante  l'emanazione  dei  piani  regolatori  il  &#13;
 territorio comunale viene distinto in zone più o meno edificabili, con  &#13;
 rilevanti   conseguenze  per  il  diritto  del  proprietario.  In  tale  &#13;
 occasione fu riconosciuto che l'imposizione di vincoli  di  zona  sulle  &#13;
 aree    altrimenti   fabbricabili   non   costituisce   esercizio   "di  &#13;
 discrezionalità  indiscriminata  ed   incontrollabile",   "bensì   di  &#13;
 discrezionalità  tecnica",  rimanendo  pertanto esclusa la prospettata  &#13;
 violazione della riserva relativa di legge.                              &#13;
     Imedesimi criteri consentono a fortiori di escludere,  anche  nella  &#13;
 fattispecie  ora  in  esame,  il vizio di illegittimità costituzionale  &#13;
 denunziato.                                                              &#13;
     Invero i poteri attribuiti dalla norma impugnata al Ministro per la  &#13;
 pubblica istruzione, concernenti la delimitazione del  perimetro  della  &#13;
 Valle  dei  Templi,  le prescrizioni d'uso dei terreni, ed i vincoli di  &#13;
 inedificabilità, involgono apprezzamenti  e  valutazioni  strettamente  &#13;
 connessi  con  discipline  tecniche,  e  sono stati conferiti all'unico  &#13;
 evidente fine di salvaguardare l'interesse archeologico  nazionale  del  &#13;
 comprensorio.      Sono  stati  così  previsti  il  divieto  di  usare  &#13;
 particolari  mezzi  meccanici  per  il  dissodamento  del   terreno   e  &#13;
 l'imposizione  di  limitazioni  edificatorie  variamente configurate in  &#13;
 relazione alla distanza dei terreni  dai  monumenti  archeologici  allo  &#13;
 scopo di non danneggiarne la prospettiva e la visione d'assieme.         &#13;
     Deve  quindi  riconoscersi  che  la  circoscritta  discrezionalità  &#13;
 conferita  alla  pubblica  Amministrazione  dalla  norma  in  esame  è  &#13;
 sufficientemente  definita  ed  ha natura tecnica.  Pertanto l'asserita  &#13;
 violazione  dell'art.  42,  secondo  comma,  della  Costituzione,   non  &#13;
 sussiste                                                                 &#13;
     3.  -  È  chiara  poi  la  manifesta  infondatezza  della  seconda  &#13;
 questione in esame. Invero questa Corte, con  la  sentenza  n.  74  del  &#13;
 1969,  ha  già escluso l'illegittimità costituzionale dell'articolo 2  &#13;
 bis della legge 28 settembre 1966, n. 749, a suo tempo  denunciato  per  &#13;
 asserita  violazione  dell'art.  14  dello Statuto regionale siciliano,  &#13;
 questione ora riproposta - in termini del tutto generici  -  senza  che  &#13;
 siano addotti nuovi motivi.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 2 bis della legge 28 settembre 1966, n. 749  (conversione  in  &#13;
 legge,  con  modificazioni,  del  d.l.  30 luglio 1966, n. 590, recante  &#13;
 provvedimenti a favore della città di  Agrigento  in  conseguenza  del  &#13;
 movimento  franoso  verificatosi  il  19  luglio  1966), sollevata, con  &#13;
 l'ordinanza in epigrafe indicata, in riferimento all'art.  42,  secondo  &#13;
 comma, della Costituzione;                                               &#13;
     dichiara  manifestamente  infondata  la  questione  di legittimità  &#13;
 costituzionale dell'art. 2 bis della legge 28 settembre 1966,  n.  749,  &#13;
 sollevata  in riferimento all'art. 14 dello Statuto regionale siciliano  &#13;
 e già dichiarata non fondata con sentenza n. 74 del 27 marzo 1969       &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 5 maggio 1971                                 &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI  -  NICOLA REALE -  &#13;
                                   PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
