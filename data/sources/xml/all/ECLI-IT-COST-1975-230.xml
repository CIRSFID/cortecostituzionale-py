<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>230</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:230</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Vincenzo Trimarchi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/10/1975</data_decisione>
    <data_deposito>10/10/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONIO DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale dell'art.   2  &#13;
 della legge 9 ottobre 1971, n. 825; degli artt.  15, 16, 17, 19,  20  e  &#13;
 30  del  d.P.R.  26  ottobre  1972,  n.  636; dell'art. 4 del d.P.R. 29  &#13;
 settembre 1973, n. 597; e degli artt. 1, 46, 56  e  57  del  d.P.R.  29  &#13;
 settembre  1973, n. 600 (cumulo dei redditi familiari), promossi con le  &#13;
 seguenti ordinanze:                                                      &#13;
     1) ordinanza emessa il 15 aprile  1975  dal  pretore  di  Roma  nel  &#13;
 procedimento  civile  vertente  tra  Capaccioli Mario e Garzia Erminia,  &#13;
 iscritta al n. 136 del  registro  ordinanze  1975  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 114 del 30 aprile 1975;           &#13;
     2)  ordinanza  emessa  il  2 aprile 1975 dal pretore di Voghera nel  &#13;
 procedimento civile vertente tra Odorisio  Roberto  e  Morini  Mirella,  &#13;
 iscritta  al  n.  160  del  registro  ordinanze 1975 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 140 del 28 maggio 1975;           &#13;
     3) ordinanza emessa il 29 aprile 1975 dal pretore  di  Livorno  nel  &#13;
 procedimento  civile vertente tra Scappatura Giuseppe e Lombardi Maria,  &#13;
 iscritta al n. 253 del  registro  ordinanze  1975  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 202 del 30 luglio 1975;           &#13;
     4)  ordinanza  emessa  il  16 aprile 1975 dal pretore di Milano nel  &#13;
 procedimento civile vertente tra Pomarici Ferdinando e Farciglia  Maria  &#13;
 Rosaria,  iscritta  al  n. 275 del registro ordinanze 1975 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 202 del 30 luglio 1975;     &#13;
     5) ordinanza emessa il 26 aprile 1975  dal  pretore  di  Arona  nel  &#13;
 procedimento civile vertente tra Aprile Michele e Rossi Piera, iscritta  &#13;
 al  n.  289  del  registro  ordinanze  1975 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 202 del 30 luglio 1975;                    &#13;
     6) ordinanza emessa il 24 aprile 1975 dal pretore  di  Firenze  nel  &#13;
 procedimento  civile  vertente  tra  Calefato  Claudio  e Ottavi Paola,  &#13;
 iscritta al n. 314 del  registro  ordinanze  1975  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 202 del 30 luglio 1975.           &#13;
     Visti  gli  atti  d'intervento  del  Presidente  del  Consiglio dei  &#13;
 ministri e di costituzione di Garzia Erminia e di Ottavi Paola;          &#13;
     udito nell'udienza pubblica dell'8 ottobre 1975 il Giudice relatore  &#13;
 Vincenzo Michele Trimarchi;                                              &#13;
     uditi l'avv. Franco Gaetano Scoca, per Garzia Erminia, l'avv. Paolo  &#13;
 Barile,  per  Ottavi  Paola,  ed  il vice avvocato generale dello Stato  &#13;
 Giovanni Albisinni, per il Presidente del Consiglio dei ministri.        &#13;
     Ritenuto che con le ordinanze indicate in  epigrafe  i  pretori  di  &#13;
 Roma,  Voghera,  Livorno,  Milano,  Arona  e  Firenze  hanno  sollevato  &#13;
 questioni di legittimità costituzionale relative a norme della legge 9  &#13;
 ottobre 1971, n. 825 (contenente delega legislativa  al  Governo  della  &#13;
 Repubblica per la riforma tributaria), del d.P.R. 29 settembre 1973, n.  &#13;
 597  (relativo  alla  istituzione  ed  alla disciplina dell'imposta sul  &#13;
 reddito delle persone fisiche), del d.P.R. 29 settembre  1973,  n.  600  &#13;
 (disposizioni  comuni  in  materia  di  accertamento  delle imposte sui  &#13;
 redditi) e del d.P.R. 26 ottobre 1972, n. 636  (sulla  revisione  della  &#13;
 disciplina del contenzioso tributario);                                  &#13;
     che, considerate unitariamente, le questioni concernono:             &#13;
     a)  gli artt. 4, lettera a, del d.P.R. n. 597 e 1, comma terzo, del  &#13;
 d.P.R. n. 600 in relazione all'art. 2 della legge n. 825 del  1971,  in  &#13;
 riferimento agli artt. 76 e 77 della Costituzione;                       &#13;
     b)  i  detti artt. 4, lettera a, e 1, comma terzo, nonché il detto  &#13;
 art. 2 della legge n. 825, in riferimento agli artt. 3, 29, 31, 53,  4,  &#13;
 35 e 37 della Costituzione;                                              &#13;
     c)  gli  stessi artt. 4, lettera a, e 1, comma terzo (ed il secondo  &#13;
 anche in conseguenza del primo), in riferimento  agli  artt.3,  4,  13,  &#13;
 15,24,27,29,31,36, 37 e 53 della Costituzione;                           &#13;
     d)  gli stessi artt. 4, lettera a, 1, comma terzo, e 2, nonché gli  &#13;
 artt. 46, 56 e 57 del d.P.R. n. 600 del 1973, in  riferimento  all'art.  &#13;
 27 della Costituzione;                                                   &#13;
     e)  i  detti  artt.  2, 4, lettera a, e 1, comma terzo, nonché gli  &#13;
 artt. 15, 16, 17,  19,  20  e  30  del  d.P.R.  n.  636  del  1972,  in  &#13;
 riferimento all'art. 24 della Costituzione;                              &#13;
     f)  il  detto  art. 4, lettera a, nella parte in cui, ai fini della  &#13;
 determinazione del reddito complessivo,  imputa  al  soggetto  passivo,  &#13;
 oltre ai redditi propri, quelli della moglie, in riferimento agli artt.  &#13;
 2, 3, 4, 13, 15, 24, 27, 29, 31, 35, 37, 53 e 76 della Costituzione;     &#13;
     che  l'Avvocatura  generale  dello  Stato,  in  rappresentanza  del  &#13;
 Presidente del Consiglio dei ministri, nei  sei  gidizi,  con  distinti  &#13;
 atti  di  costituzione, ha chiesto che le questioni siano dichiarate in  &#13;
 tutto o in parte inammissibili e comunque non fondate, e con la memoria  &#13;
 ha altresì avanzato istanza di rinvio della trattazione dei giudizi;    &#13;
     che l'avv. Scoca, per la Garzia, ha chiesto che sia  dichiarata  la  &#13;
 illegittimità costituzionale dell'art. 4, lettera a, del d.P.R. n. 597  &#13;
 del  1973 anche in relazione all'art. 2, comma primo, n. 3, della legge  &#13;
 n. 825 del 1971, e dell'art. 1, comma terzo,  del  d.P.R.  n.  600  del  &#13;
 1973;                                                                    &#13;
     che  l'avv.  Barile,  per la Ottavi, ha chiesto la dichiarazione di  &#13;
 illegittimità costituzionale degli artt. 1, 2  e  4,  lettera  a,  del  &#13;
 d.P.R. n. 597 del 1973;                                                  &#13;
     che  all'udienza  dell'8  ottobre  1975,  il vice avvocato generale  &#13;
 dello Stato Giovanni Albisinni ha rinnovato l'istanza di  rinvio  della  &#13;
 trattazione  dei  giudizi, l'avv. Barile si è opposto all'accoglimento  &#13;
 dell'istanza  e   che   questa   Corte   ha   respinto   l'istanza;   e  &#13;
 successivamente  il  Presidente  del  Consiglio dei ministri e le parti  &#13;
 private, a mezzo dei loro rappresentanti e difensori, hanno  svolto  le  &#13;
 rispettive ragioni ed insistito nelle relative conclusioni.              &#13;
     Considerato  che  i  sei  giudizi,  stante  l'identità,  totale  o  &#13;
 parziale, delle questioni che ne  costituiscono  gli  oggetti,  possono  &#13;
 essere riuniti;                                                          &#13;
     che  l'eccezione, avanzata dall'Avvocatura generale dello Stato, di  &#13;
 inammissibilità di tutte le dette questioni in  quanto  sollevate  nel  &#13;
 corso  di giudizi in ciascuno dei quali, in considerazione dell'oggetto  &#13;
 della domanda, il pretore avrebbe  dovuto  dichiarare  improponibile  o  &#13;
 inammissibile  l'esercizio  dell'azione  ex  art.  700  del  codice  di  &#13;
 procedura civile, non  appare  fondata,  perché  ogni  valutazione  in  &#13;
 ordine all'azione fatta valere spetta al giudice di merito;              &#13;
     che del pari non è fondata l'altra eccezione proposta dalla stessa  &#13;
 Avvocatura  generale  dello  Stato,  secondo  cui  le  dette  questioni  &#13;
 sarebbero inammissibili sotto  il  profilo  che  la  risoluzione  delle  &#13;
 stesse  non avrebbe carattere pregiudiziale nei confronti della chiesta  &#13;
 decisione, stante che il pretore si sarebbe potuto e dovuto limitare  a  &#13;
 dare atto del rifiuto della moglie resistente di aderire alla richiesta  &#13;
 di  dati  e  documenti  relativi  al proprio reddito; e ciò perché il  &#13;
 petitum in ciascuno dei detti giudizi è consistito in una  domanda  di  &#13;
 accertamento  di  codesto  rifiuto  da parte della moglie e di condanna  &#13;
 della stessa a provvedere nel senso invocato dal ricorrente;             &#13;
     che tra le norme denunciate è quella  di  cui  all'art.  1,  comma  &#13;
 terzo,  del  d.P.R.  n.  600  del  1973, secondo la quale ogni soggetto  &#13;
 passivo deve dichiarare annualmente, ed in unico  contesto,  i  redditi  &#13;
 propri ed a lui imputabili;                                              &#13;
     che di detta norma è prospettato, tra l'altro, il contrasto con il  &#13;
 principio di eguaglianza, per ciò che nel caso, come quello di specie,  &#13;
 in  cui  oggetto  di  imputazione  sono i redditi della moglie, solo il  &#13;
 marito, e non anche  la  moglie,  sarebbe  soggetto  all'obbligo  della  &#13;
 dichiarazione;                                                           &#13;
     che la valutazione della dedotta disparità di trattamento e quindi  &#13;
 la  decisione  in  ordine alla questione di legittimità costituzionale  &#13;
 non possono aversi se non unitamente e in relazione alla considerazione  &#13;
 del contenuto e della portata dell'art. 2, comma primo, del  d.P.R.  n.  &#13;
 597 del 1973, e cioè della soggettività passiva di imposta;            &#13;
     che  a  proposito  di  quest'ultima norma (non denunciata da alcuna  &#13;
 delle ordinanze), statuendo essa  che  "soggetti  passivi  dell'imposta  &#13;
 sono le persone fisiche, residenti e non residenti nel territorio dello  &#13;
 Stato,  ad  eccezione di quelle i cui redditi sono imputati ad altri ai  &#13;
 sensi dell'art. 4" dello stesso decreto n. 597,  si  può,  per  altro,  &#13;
 dubitare  circa  la  conformità di essa norma agli artt.  3 e 29, e 24  &#13;
 della Costituzione,  perché,  nell'ipotesi  in  cui  soggetto  passivo  &#13;
 dell'imposta  sia  un  marito e siano a questo imputati i redditi della  &#13;
 moglie, è previsto un trattamento differenziato, nonostante la parità  &#13;
 morale e giuridica dei coniugi, e senza che la disparità  si  presenti  &#13;
 razionalmente giustificata, o funzionalizzata alla garanzia dell'unità  &#13;
 familiare,  e tale trattamento differenziato si pone o si risolve anche  &#13;
 sul terreno della tutela in giudizio dei diritti e degli interessi;      &#13;
     che tale questione di legittimità costituzionale ha rilevanza  per  &#13;
 la  decisione  della  questione  concernente l'art. 1, comma terzo, del  &#13;
 d.P.R.  n.  600  sopra  ricordata  ed  è,  per   quanto   detto,   non  &#13;
 manifestamente infondata;                                                &#13;
     che  ricorrono,  pertanto,  gli estremi perché la Corte, riservata  &#13;
 ogni pronuncia in ordine a tutte le questioni  di  cui  alle  ordinanze  &#13;
 indicate   in   epigrafe,   sollevi  davanti  a  sé  la  questione  di  &#13;
 legittimità costituzionale ora  specificata,  ai  sensi  dell'art.  23  &#13;
 della legge 11 marzo 1953, n. 87.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     solleva  davanti  a sé la questione di legittimità costituzionale  &#13;
 dell'art. 2, comma primo, del d.P.R. 29  settembre  1973,  n.  597,  in  &#13;
 riferimento agli artt. 3, 29 e 24 della Costituzione;                    &#13;
     sospende i giudizi in corso e come sopra riuniti;                    &#13;
     ordina  che  a  cura  della  cancelleria  la presente ordinanza sia  &#13;
 notificata alle parti in  causa  e  al  Presidente  del  Consiglio  dei  &#13;
 ministri   e   sia  comunicata  ai  Presidenti  delle  due  Camere  del  &#13;
 Parlamento;                                                              &#13;
     ordina che gli atti dei giudizi in atto pendenti  siano  restituiti  &#13;
 alla cancelleria.                                                        &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 10 ottobre 1975.                              &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   -   NICOLA  REALE  -  PAOLO  ROSSI  -  &#13;
                                   LEONETTO AMADEI - GIULIO GIONFRIDA  -  &#13;
                                   EDOARDO  VOLTERRA  -  GUIDO  ASTUTI -  &#13;
                                   MICHELE ROSSANO - ANTONIO DE STEFANO.  &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
