<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>178</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:178</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia>Massimo Vari</redattore_pronuncia>
    <data_decisione>06/05/2002</data_decisione>
    <data_deposito>10/05/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
FLICK, Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art. 2, comma 4, &#13;
della legge 3 maggio 1999, n. 124 (Disposizioni urgenti in materia di &#13;
personale  scolastico), promossi con tre ordinanze emesse il 6 luglio &#13;
2000  dal  Tribunale  amministrativo  regionale della Puglia, sezione &#13;
staccata di Lecce, rispettivamente iscritte ai nn. 113, 114 e 115 del &#13;
registro  ordinanze  2001 e pubblicate nella Gazzetta Ufficiale della &#13;
Repubblica n. 8, 1ª serie speciale, dell'anno 2001. &#13;
    Visti  gli atti di costituzione di Quarta Maria Rosaria ed altra, &#13;
di  Ciardo  Antonella  ed  altre  e  di Padalino Lucia Paola ed altre &#13;
nonché  gli  atti  di  intervento  del  Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nell'udienza  pubblica  del  26 febbraio  2002  il giudice &#13;
relatore Massimo Vari; &#13;
    Uditi  l'avvocato  Franco  Carrozzo  per  Quarta Maria Rosaria ed &#13;
altra,  Ciardo  Antonella  ed altre e Padalino Lucia Paola ed altre e &#13;
l'Avvocato  dello  Stato  Gabriella  Palmieri  per  il Presidente del &#13;
Consiglio dei ministri. &#13;
    Ritenuto  che il Tribunale amministrativo regionale della Puglia, &#13;
sezione  staccata di Lecce, con tre ordinanze di analogo tenore (R.O. &#13;
nn. 113  a  115  del  2001)  -  emesse nel corso di giudizi aventi ad &#13;
oggetto  l'annullamento  di  provvedimenti  di  esclusione, di talune &#13;
insegnanti,   dalla   sessione   riservata   per   il   conseguimento &#13;
dell'idoneità   all'insegnamento   nella   scuola  elementare  -  ha &#13;
sollevato  questione  di  legittimità  costituzionale  dell'art.  2, &#13;
comma 4,  della  legge 3 maggio 1999, n. 124 (Disposizioni urgenti in &#13;
materia   di  personale  scolastico);  norma  che  -  ai  fini  della &#13;
maturazione  del  prescritto  periodo  di insegnamento occorrente per &#13;
l'ammissione  alla  predetta  sessione  - prende in considerazione il &#13;
solo  servizio  di insegnamento prestato nelle scuole statali, ovvero &#13;
negli   istituti   e  scuole  secondarie  legalmente  riconosciuti  o &#13;
pareggiati,  ovvero  nelle  scuole materne autorizzate e nelle scuole &#13;
elementari   parificate;   senza   menzionare  le  scuole  elementari &#13;
autorizzate, nelle quali avevano prestato servizio le ricorrenti; &#13;
        che  il giudice a quo, nell'escludere che la disposizione, in &#13;
ragione   del  carattere  eccezionale  e  derogatorio,  sia  tale  da &#13;
consentire un'"interpretazione estensiva", atta a ricomprendere anche &#13;
le  fattispecie  al  suo  esame, ritiene che la stessa violi l'art. 3 &#13;
della  Costituzione,  a  causa  della  ingiustificata  disparità  di &#13;
trattamento  fra  coloroche  hanno prestato servizio nella scuola non &#13;
statale  parificata e coloro che hanno prestato servizio nella scuola &#13;
elementare  autorizzata,  reputando,  al  tempo  stesso, inciso anche &#13;
l'art. 97  della  Costituzione,  in riferimento al principio di buona &#13;
amministrazione; &#13;
        che si sono costituite, innanzi alla Corte, le ricorrenti nel &#13;
giudizio a quo, chiedendo che la questione sia dichiarata fondata; &#13;
        che è intervenuto, altresì, il Presidente del Consiglio dei &#13;
ministri,  chiedendo  che la questione venga dichiarata inammissibile &#13;
e, comunque, infondata. &#13;
    Considerato che i giudizi in epigrafe, analoghi per oggetto e per &#13;
profili  proposti,  vanno  riuniti  per  essere  decisi  con un'unica &#13;
pronunzia; &#13;
        che,  quanto  alla questione prospettata, risulta evidente la &#13;
differenza  tra  le  scuole elementari parificate e quelle elementari &#13;
autorizzate,  dal  momento  che,  pur  essendo  entrambe dette scuole &#13;
annoverabili  tra gli istituti non statali, solo le scuole elementari &#13;
parificate  sono  rette  da  un regime concessorio che le assimila ad &#13;
ogni effetto legale (art. 344 del decreto legislativo 16 aprile 1994, &#13;
n. 297)  -  e,  segnatamente, per i profili pubblicistici concernenti &#13;
l'adozione dei programmi delle attività didattiche e il rilascio dei &#13;
titoli  di  studio  -  alle scuole elementari statali, tanto che sono &#13;
tenute  ad  adottare  lo  stesso  "ordinamento" (art. 346 del decreto &#13;
legislativo 16 aprile 1994, n. 297); &#13;
        che   la   norma   censurata   rappresenta,   comunque,   una &#13;
disposizione  transitoria,  valevole  - come risulta anche dai lavori &#13;
preparatori  della  legge n. 124 del 1999 - ai limitati e contingenti &#13;
fini di integrazione delle graduatorie permanenti di cui all'art. 401 &#13;
del  decreto  legislativo  16 aprile  1994, n. 297, in ragione di una &#13;
discrezionale   scelta   legislativa   di   provvista   di  personale &#13;
insegnante,  volta a dar rilievo al possesso di esperienze didattiche &#13;
acquisite  in  ambiti  interni  o  quanto  meno assimilabili a quelli &#13;
statali; &#13;
        che,  per  i  motivi anzidetti, la disposizione censurata non &#13;
comporta violazione dei parametri costituzionali evocati, non potendo &#13;
considerarsi   irragionevole,   né   contraria   al  buon  andamento &#13;
dell'amministrazione,   la   scelta   del   legislatore  di  valutare &#13;
diversamente  il  servizio  effettuato  dai  docenti presso le scuole &#13;
elementari  parificate rispetto a quello reso nelle scuole elementari &#13;
autorizzate; &#13;
        che,  pertanto,  la  questione  deve ritenersi manifestamente &#13;
infondata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi; &#13;
    Dichiara  manifestamente  infondata  la questione di legittimità &#13;
costituzionale  dell'art. 2,  comma  4,  della  legge  3 maggio 1999, &#13;
n. 124  (Disposizioni  urgenti  in  materia di personale scolastico), &#13;
sollevata,  in  riferimento agli artt. 3 e 97 della Costituzione, con &#13;
le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 6 maggio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Vari &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 10 maggio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
