<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>70</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:70</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia>Franco Bile</redattore_pronuncia>
    <data_decisione>28/02/2002</data_decisione>
    <data_deposito>19/03/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Valerio ONIDA, Carlo &#13;
MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 1, comma 2, &#13;
lettera  c),  del  decreto-legge  19 settembre  1992,  n. 384 (Misure &#13;
urgenti  in  materia di previdenza, di sanità e di pubblico impiego, &#13;
nonché  disposizioni fiscali), convertito in legge 14 novembre 1992, &#13;
n. 438,  promosso  con  ordinanza  emessa  il  26 novembre  1999  dal &#13;
Tribunale  di Bolzano nel procedimento civile vertente tra l'Istituto &#13;
nazionale  di  previdenza  sociale  (I.N.P.S.) e Giampaolo Racchetti, &#13;
iscritta  al  n. 600  del  registro ordinanze 2000 e pubblicata nella &#13;
Gazzetta   Ufficiale  della  Repubblica  n. 44,  1a  serie  speciale, &#13;
dell'anno 2000. &#13;
    Visti  l'atto  di  costituzione  dell'I.N.P.S., nonché l'atto di &#13;
intervento del Presidente del Consiglio dei ministri; &#13;
    Udito  nell'udienza  pubblica  del  29 gennaio  2002  il  giudice &#13;
relatore Franco Bile; &#13;
    Uditi  l'avvocato  Alessandro  Riccio per l'I.N.P.S. e l'avvocato &#13;
dello  Stato  Giorgio  D'Amato  per  il  Presidente del Consiglio dei &#13;
ministri. &#13;
    Ritenuto  che,  con  ordinanza  emessa  il  26 novembre  1999, il &#13;
Tribunale  di  Bolzano nel giudizio di appello promosso dall'Istituto &#13;
Nazionale della Previdenza Sociale contro Giampaolo Racchetti, per la &#13;
riforma  della  sentenza  n. 523 del 1996 del pretore di Bolzano - ha &#13;
dichiarato  rilevante  e non manifestamente infondata, in riferimento &#13;
agli  artt. 3  e  38 della Costituzione, la questione di legittimità &#13;
costituzionale   dell'art. 1,   comma  2,  lettera  c),  della  legge &#13;
14 dicembre  1992, n. 438 (rectius: art. 1 decreto-legge 19 settembre &#13;
1992,  n. 384, "Misure urgenti in materia di previdenza, di sanità e &#13;
di  pubblico  impiego,  nonché  disposizioni fiscali", convertito in &#13;
legge 14 novembre 1992, n. 438), nella parte in cui non estende anche &#13;
ai   lavoratori   autonomi   la   deroga   alla   sospensione   della &#13;
corresponsione  del  trattamento  pensionistico  di anzianità per il &#13;
periodo  compreso  tra l'entrata in vigore del medesimo decreto-legge &#13;
ed  il  31 dicembre  1993, prevista per i lavoratori dipendenti per i &#13;
quali sia intervenuta l'estinzione del rapporto di lavoro; &#13;
        che,  nella  specie,  la  domanda  di pensione di anzianità, &#13;
presentata  all'I.N.P.S.  dal lavoratore autonomo (artigiano) in data &#13;
29 luglio  1993,  era  stata  accolta  tenendo  conto,  ai fini della &#13;
decorrenza,  del  citato  periodo  di sospensione, in quanto, secondo &#13;
l'Istituto,  la  deroga  alla sospensione posta dall'art. 1, comma 2, &#13;
lettera  c)  del  citato  decreto-legge, riguardava i soli lavoratori &#13;
dipendenti  per  i quali anteriormente alla data di entrata in vigore &#13;
del  decreto-legge  fosse  intervenuta  l'estinzione  del rapporto di &#13;
lavoro  o  fosse  iniziato  il  periodo  di  preavviso  connesso alla &#13;
risoluzione del rapporto; &#13;
        che  -  secondo  il  tribunale  rimettente - l'esclusione dei &#13;
lavoratori   autonomi  dal  beneficio  della  deroga  al  blocco  dei &#13;
trattamenti pensionistici di anzianità configurerebbe un'irrazionale &#13;
discriminazione di trattamento in violazione dell'art. 3 Cost; &#13;
        che sarebbe altresì violato l'art. 38, secondo comma, Cost., &#13;
poiché  i  lavoratori autonomi verrebbero privati, quantomeno per un &#13;
determinato   periodo,   della   tutela  economica  che  il  rapporto &#13;
assicurativo   aveva  loro  garantito  al  momento  della  cessazione &#13;
dell'attività  di lavoro e della successiva prosecuzione volontaria, &#13;
privandoli  -  con norma successiva, non prevedibile al momento della &#13;
loro scelta - dei mezzi adeguati, già ad essi garantiti dalla legge; &#13;
        che  si  è  costituito  l'I.N.P.S. sostenendo l'infondatezza &#13;
della questione; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato, &#13;
chiedendo  che  la sollevata questione di legittimità costituzionale &#13;
sia dichiarata inammissibile o comunque infondata. &#13;
    Considerato  che  - come già rilevato da questa Corte (ordinanza &#13;
n. 18  del 2001) - il temporaneo blocco delle pensioni di anzianità, &#13;
posto  dal  menzionato decreto-legge n. 384 del 1992, si inserisce in &#13;
un   processo   di   radicale  riconsiderazione  del  trattamento  di &#13;
anzianità,  dettato  inizialmente dalla necessità di un contingente &#13;
intervento  di  ripristino degli equilibri finanziari, e poi sfociato &#13;
nella  riforma  pensionistica  introdotta  dalla legge 8 agosto 1995, &#13;
n. 335    (Riforma   del   sistema   pensionistico   obbligatorio   e &#13;
complementare),   mirante   ad   incidere   stabilmente  sulla  spesa &#13;
previdenziale; &#13;
        che  in  questo  contesto le specifiche e limitate deroghe al &#13;
blocco  suddetto, previste dalla disposizione censurata, hanno natura &#13;
eccezionale  e  non  sono  suscettibili di estensione, atteso che "la &#13;
scelta  di escludere determinate categorie di lavoratori dalle misure &#13;
di  blocco  rientra  a  pieno  titolo nella sfera di discrezionalità &#13;
politica riservata al legislatore" (ordinanza citata); &#13;
        che  inoltre  le  rimarchevoli  differenze  esistenti  tra la &#13;
fattispecie  del  lavoro autonomo e quella del lavoro subordinato non &#13;
consentono   di   riconoscere   nella   disciplina  previdenziale  di &#13;
quest'ultimo  un  idoneo  tertium comparationis (ordinanza n. 133 del &#13;
2001; sentenza n. 416 del 1999); &#13;
        che  infine  la  garanzia  dell'art. 38 della Costituzione è &#13;
legata  allo  stato  di bisogno del lavoratore e quindi riguarda, tra &#13;
gli  altri,  i  trattamenti  pensionistici  che trovano la loro causa &#13;
nella  cessazione dell'attività lavorativa per ragioni di età e non &#13;
anche  quelli - quali le pensioni di anzianità nel regime precedente &#13;
alla  menzionata  riforma  -  il  cui presupposto consista nella sola &#13;
maturazione  di  una  determinata  anzianità  contributiva (sentenza &#13;
n. 416 del 1999, cit.); &#13;
        che,   pertanto,   la   questione   di  costituzionalità  è &#13;
manifestamente infondata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art. 1,  comma  2, lettera c), del &#13;
decreto-legge 19 settembre 1992, n. 384 (Misure urgenti in materia di &#13;
previdenza,  di  sanità  e di pubblico impiego, nonché disposizioni &#13;
fiscali), convertito in legge 14 novembre 1992, n. 438, sollevata, in &#13;
riferimento  agli  artt. 3  e 38 della Costituzione, dal Tribunale di &#13;
Bolzano con l'ordinanza indicata in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 28 febbraio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Bile &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 19 marzo 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
