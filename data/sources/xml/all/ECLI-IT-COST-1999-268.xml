<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>268</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:268</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/06/1999</data_decisione>
    <data_deposito>23/06/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità costituzionale dell'art. 1, comma 230,    &#13;
 della legge 23 dicembre 1996, n.  662  (Misure  di  razionalizzazione    &#13;
 della  finanza pubblica), così come richiamato dall'art. 4, comma 6,    &#13;
 del decreto legge 28  marzo  1997,  n.  79  (Misure  urgenti  per  il    &#13;
 riequilibrio  della finanza pubblica), convertito, con modificazioni,    &#13;
 dalla legge 28 maggio 1997, n. 140, promossi con 2  ordinanze  emesse    &#13;
 il  10  dicembre  1998  ed  il 18 gennaio 1999 dal pretore di Milano,    &#13;
 sezione distaccata di Legnano, iscritte ai nn. 137 e 179 del registro    &#13;
 ordinanze 1999 e pubblicate nella Gazzetta Ufficiale  nn.  11  e  13,    &#13;
 prima serie speciale, dell'anno 1999.                                    &#13;
   Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio  del  25  maggio  1999  il  giudice    &#13;
 relatore Valerio Onida.                                                  &#13;
   Ritenuto  che,  con due ordinanze di identico tenore, il pretore di    &#13;
 Milano, sezione distaccata di  Legnano,  ha  sollevato  questione  di    &#13;
 legittimità   costituzionale,   in   riferimento  all'art.  3  della    &#13;
 Costituzione, dell'art. 1, comma 230, della legge 23  dicembre  1996,    &#13;
 n.  662  (Misure  di razionalizzazione della finanza pubblica), così    &#13;
 come richiamato dall'art. 4, comma 6,  del  decreto  legge  28  marzo    &#13;
 1997,  n.  79  (Misure  urgenti  per  il  riequilibrio  della finanza    &#13;
 pubblica), convertito, con modificazioni, dalla legge 28 maggio 1997,    &#13;
 n. 140, nella parte in cui non prevede che, oltre i provvedimenti  di    &#13;
 esecuzione,  i  provvedimenti di merito in corso in qualsiasi stato e    &#13;
 grado siano "sospesi per effetto della domanda di regolarizzazione  e    &#13;
 subordinatamente  al  puntuale pagamento delle somme determinate agli    &#13;
 effetti del presente articolo alle scadenze dallo stesso previste";      &#13;
     che il remittente premette che l'art. 4 del d-.l. n. 79 del  1997    &#13;
 prevede  la  regolarizzazione  delle  posizioni  debitorie relative a    &#13;
 omissioni o ritardi nel  versamento  di  contributi  previdenziali  e    &#13;
 assistenziali,   anche   attraverso   il  pagamento  in  trenta  rate    &#13;
 bimestrali, e richiama,  estendendone  la  portata,  la  disposizione    &#13;
 dell'art. 1, comma 230, della legge 23 dicembre 1996, n. 662, secondo    &#13;
 la  quale  la  regolarizzazione  estingue  i  reati  connessi  con la    &#13;
 denuncia e con i versamenti contributivi, e sono sospesi, per effetto    &#13;
 della domanda di  regolarizzazione  e  subordinatamente  al  puntuale    &#13;
 pagamento   delle   somme  dovute  alle  scadenze  previste,  solo  i    &#13;
 provvedimenti di esecuzione in corso, in qualsiasi fase e grado;         &#13;
     che, ad avviso del giudice a quo, tale disciplina, non prevedendo    &#13;
 la sospensione dei procedimenti penali  in  corso,  come  era  invece    &#13;
 previsto  dall'art. 3, comma 9, del d.-l. n. 103 del 1991, convertito    &#13;
 dalla legge n. 166 del 1991, non consentirebbe al debitore, che  pure    &#13;
 è  ammesso  alla  regolarizzazione mediante il pagamento rateale, di    &#13;
 ottenere, fino al completo pagamento, il beneficio  della  estinzione    &#13;
 del  reato,  realizzandosi  così  una  ingiustificata  disparità di    &#13;
 trattamento, in sede giurisdizionale, fra chi è in grado di  versare    &#13;
 in  unica  soluzione  la  somma  dovuta, e chi invece, per le proprie    &#13;
 condizioni economiche, deve avvalersi del pagamento rateale;             &#13;
     che è intervenuto il  Presidente  del  Consiglio  dei  Ministri,    &#13;
 chiedendo  che  la  questione sia dichiarata inammissibile e comunque    &#13;
 manifestamente infondata.                                                &#13;
   Considerato che le due ordinanze sollevano identica questione, onde    &#13;
 i relativi giudizi possono essere riuniti per essere decisi con unica    &#13;
 pronunzia;                                                               &#13;
     che analoga questione,  in  riferimento  allo  stesso  parametro,    &#13;
 sollevata  con riguardo all'art. 1, comma 230, della legge n. 662 del    &#13;
 1996, è stata dichiarata da questa  Corte  manifestamente  infondata    &#13;
 con  l'ordinanza n. 142 del 1999, in cui si è chiarito, fra l'altro,    &#13;
 che la disciplina denunciata, pur dando luogo ad una  disarmonia  fra    &#13;
 normativa amministrativa e normativa penale connessa, non si appalesa    &#13;
 costituzionalmente  illegittima,  poiché  il debitore che fa ricorso    &#13;
 alla regolarizzazione mediante il pagamento rateale non può  vantare    &#13;
 una   pretesa   costituzionalmente   protetta  a  vedere  sospeso  il    &#13;
 procedimento penale in attesa del completamento del  pagamento  delle    &#13;
 somme  dovute;  e  che la disparità fra chi è in grado di pagare in    &#13;
 unica soluzione e  chi  ricorre  alla  rateizzazione  è  insita  nel    &#13;
 meccanismo  normativo  che  subordina  la  estinzione  del  reato  al    &#13;
 pagamento  di  somme,  senza  che  ciò  costituisca,  di  per   sé,    &#13;
 violazione   della  Costituzione,  né  dà  luogo  ad  irragionevoli    &#13;
 disparità di trattamento, ancorché  il  mancato  coordinamento  fra    &#13;
 normativa   amministrativa   e   normativa   penale   possa  incidere    &#13;
 sull'efficacia dell'incentivo alla regolarizzazione  predisposto  dal    &#13;
 legislatore;                                                             &#13;
     che  le  ordinanze in esame non recano argomenti nuovi o comunque    &#13;
 tali da indurre questa Corte a modificare il proprio orientamento;       &#13;
     che  pertanto  anche  la  questione  qui  sollevata  deve  essere    &#13;
 dichiarata manifestamente infondata.                                     &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la  manifesta  infondatezza   della    &#13;
 questione  di  legittimità  costituzionale  dell'art.  1, comma 230,    &#13;
 della legge 23 dicembre 1996, n.  662  (Misure  di  razionalizzazione    &#13;
 della  finanza  pubblica),  come richiamato dall'art. 4, comma 6, del    &#13;
 decreto  legge  28  marzo  1997,  n.  79  (Misure  urgenti   per   il    &#13;
 riequilibrio  della finanza pubblica), convertito, con modificazioni,    &#13;
 dalla legge  28  maggio  1997,  n.  140,  sollevata,  in  riferimento    &#13;
 all'art.  3  della  Costituzione,  dal  pretore  di  Milano,  sezione    &#13;
 distaccata di Legnano, con le ordinanze in epigrafe.                     &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'11 giugno 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                          Il redattore: Onida                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 giugno 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
