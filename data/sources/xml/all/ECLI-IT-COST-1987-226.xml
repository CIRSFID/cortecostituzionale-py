<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>226</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:226</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ANDRIOLI</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/06/1987</data_decisione>
    <data_deposito>17/06/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Virgilio ANDRIOLI; &#13;
 Giudici: prof. Giuseppe FERRARI, dott. Francesco SAJA, prof. Giovanni &#13;
 CONSO, dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2 del d.P.R. 30    &#13;
 giugno  1965,  n.  1124  ("Testo   unico   delle   disposizioni   per    &#13;
 l'assicurazione  obbligatoria  contro  gli  infortuni sul lavoro e le    &#13;
 malattie professionali"), promosso con ordinanza emessa il 25  luglio    &#13;
 1980  dal  Pretore  di  Bologna  nel procedimento civile vertente tra    &#13;
 Vicinelli Gianfranco e l'I.N.A.I.L., iscritta al n. 716 del  registro    &#13;
 ordinanze 1980 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 325 dell'anno 1980;                                                   &#13;
    Visti  gli  atti  di  costituzione  dell'I.N.A.I.L. e di Vicinelli    &#13;
 Gianfranco,  nonché  gli  atti  di  intervento  del  Presidente  del    &#13;
 Consiglio dei ministri;                                                  &#13;
    Udito nell'udienza pubblica del 19 maggio 1987 il Giudice relatore    &#13;
 Francesco Paolo Casavola;                                                &#13;
    Uditi  l'avvocato  Enrico  Ruffini  per  l'I.N.A.I.L. e l'Avvocato    &#13;
 dello Stato Pier Giorgio Ferri per il Presidente  del  Consiglio  dei    &#13;
 ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il Pretore di Bologna, con ordinanza del 25 luglio 1980, in    &#13;
 riferimento agli articoli 2, 3, secondo comma, 4,  primo  comma,  35,    &#13;
 primo comma, 38, secondo comma, della Costituzione, solleva questione    &#13;
 di legittimità costituzionale dell'articolo 2 del d.P.R.  30  giugno    &#13;
 1965,  n.  1124  ("Testo unico delle disposizioni per l'assicurazione    &#13;
 contro gli infortuni sul lavoro e le malattie professionali"),  nella    &#13;
 parte  in  cui  esclude  che  l'infezione  malarica  possa costituire    &#13;
 infortunio sul lavoro. Il procedimento ha  origine  da  causa  civile    &#13;
 avente   ad   oggetto  il  ricorso  di  Vicinelli  Gianfranco  contro    &#13;
 l'I.N.A.I.L., per ottenere pensione  di  invalidità  per  gli  esiti    &#13;
 permanenti  -  splenomegalia  con  fibrosi  -  derivanti da infezione    &#13;
 malarica contratta in luogo di lavoro in Ghana.                          &#13;
    2.  -  Per  il Presidente del Consiglio dei ministri, l'Avvocatura    &#13;
 generale dello Stato assume la infondatezza della questione proposta,    &#13;
 sostenendo che non sarebbero censurabili sul piano della legittimità    &#13;
 costituzionale le scelte  di  merito  del  legislatore  nel  definire    &#13;
 presupposti  e  condizioni  di  applicazione del sistema garantistico    &#13;
 degli infortuni. La esclusione della malaria dal novero  delle  cause    &#13;
 di  infortunio  troverebbe  giustificazione nel carattere anomalo del    &#13;
 processo causativo  dell'evento  dannoso  rispetto  alla  fattispecie    &#13;
 legale  dell'infortunio  per  causa  violenta in occasione di lavoro.    &#13;
 Compenserebbe tale esclusione il trattamento  disposto,  in  caso  di    &#13;
 morte da febbre perniciosa, dall'art. 329 del Testo unico delle leggi    &#13;
 sanitarie, sostituito da quello stabilito dall'art. 1 della legge  11    &#13;
 marzo 1953, n. 160.                                                      &#13;
    3. - Per quanto riguarda le parti private, costituite in giudizio,    &#13;
 l'I.N.A.I.L.  sostiene  l'infondatezza  della  questione,  mentre  il    &#13;
 Vicinelli   Gianfranco  argomenta,  in  adesione  alla  ordinanza  di    &#13;
 rimessione, sia nel senso della inattualità della normativa rispetto    &#13;
 alla  evoluzione  della  malaria,  sia assumendo che la puntura della    &#13;
 zanzara costituisce  causa  violenta  verificabile  in  occasione  di    &#13;
 lavoro,  per  il  rischio  cui  il  lavoratore è esposto in ambiente    &#13;
 infestato.<diritto>Considerato in diritto</diritto>1.  -  Il  Pretore  di  Bologna, con ordinanza del 25 luglio 1980,    &#13;
 ritiene che l'art. 2 del d.P.R. 30  giugno  1965,  n.  1124,  ("Testo    &#13;
 unico delle disposizioni per l'assicurazione contro gli infortuni sul    &#13;
 lavoro e le malattie professionali"), nella parte in cui esclude  che    &#13;
 l'evento  dannoso  derivante da infezione malarica sia compreso tra i    &#13;
 casi di infortunio sul lavoro, e richiama disposizioni speciali,  che    &#13;
 limitano,  in  materia  ogni  tutela  al  caso  di  morte  da  febbre    &#13;
 perniciosa,  "non  solo  appare  manifestamente   irragionevole,   ma    &#13;
 contrastante,  da un lato, con i generali principi costituzionali che    &#13;
 riconoscono l'inviolabile diritto alla  salute  (art.  2  Cost.),  la    &#13;
 parità  sostanziale  fra  i  cittadini  (art.  3, secondo comma), la    &#13;
 tutela della posizione di lavoro (art. 4, primo comma) e, dall'altro,    &#13;
 con  quelli più specifici che garantiscono il lavoro in tutte le sue    &#13;
 forme e applicazioni (art.  35,  primo  comma)  e  che  delineano  la    &#13;
 struttura del sistema previdenziale (art. 38, secondo comma)".           &#13;
    2. - La questione è fondata.                                         &#13;
    La norma impugnata recita: "L'assicurazione comprende tutti i casi    &#13;
 di infortunio avvenuti per causa violenta in occasione di lavoro,  da    &#13;
 cui  sia  derivata  la  morte  o  un'inabilità permanente al lavoro,    &#13;
 assoluta o parziale, ovvero  un'inabilità  temporanea  assoluta  che    &#13;
 importi l'astensione dal lavoro per più di tre giorni.                  &#13;
    "Agli  effetti del presente decreto, è considerata infortunio sul    &#13;
 lavoro l'infezione carbonchiosa. Non è invece compreso tra i casi di    &#13;
 infortunio   sul  lavoro  l'evento  dannoso  derivante  da  infezione    &#13;
 malarica, il quale è regolato da disposizioni speciali".                &#13;
    La   esclusione   della   malaria   dall'elenco   delle  patologie    &#13;
 riconducibili a infortuni sul  lavoro  proviene  dall'art.  16  della    &#13;
 legge 22 giugno 1933, n. 851, dall'art. 2 del r.d. 17 agosto 1935, n.    &#13;
 1765, ed è ribadita nel primo comma dell'art. 329  del  Testo  unico    &#13;
 delle leggi sanitarie (r.d. 27 luglio 1934, n. 1265), il quale ultimo    &#13;
 recita:  "L'infezione  malarica  non  è  compresa  fra  i  casi   di    &#13;
 infortunio  per  causa  violenta  in  occasione  di  lavoro, che sono    &#13;
 preveduti dalle vigenti disposizioni sugli infortuni degli operai sul    &#13;
 lavoro  e  sulla  assicurazione obbligatoria contro gli infortuni sul    &#13;
 lavoro in agricoltura".                                                  &#13;
    Negli  anni cui risalgono queste norme la malaria infestava intere    &#13;
 regioni del Paese e costituiva un rischio generico di malattia  e  di    &#13;
 morte  per  le  popolazioni, non un rischio specifico dei lavoratori.    &#13;
 Era  pertanto  allora  giustificato  che  la  malattia  da  infezione    &#13;
 malarica  fosse esclusa dal sistema assicurativo contro gli infortuni    &#13;
 sul lavoro, e che si provvedesse invece con una sovvenzione, ai sensi    &#13;
 dell'art.  329,  secondo comma, assegnata ai discendenti, ascendenti,    &#13;
 coniugi,  fratelli  o  sorelle  dell'operaio  deceduto   per   febbre    &#13;
 perniciosa.  Tale norma, peraltro, è stata sostituita dalla legge 11    &#13;
 marzo  1953,  n.  160,  che  dispone  in  luogo   della   sovvenzione    &#13;
 l'estensione  del  trattamento  stabilito  per  i  casi  di morte per    &#13;
 infortunio sul lavoro in agricoltura,  ai  sensi  dell'art.  3  della    &#13;
 legge 20 febbraio 1950, n. 64.                                           &#13;
    Nel     secondo     dopoguerra,    mediante    l'uso    del    DDT    &#13;
 (diclorodifeniltricloroetano), la infestazione malarica,  che  durava    &#13;
 da  molti  secoli,  è  stata  in  pochi  anni debellata. Non ha oggi    &#13;
 perciò più  alcuna  ragionevolezza  un  regime  giuridico,  che  in    &#13;
 materia  di  infezione  malarica  continui  a  postulare  un  rischio    &#13;
 generico per gli abitanti, e ad escludere  un  rischio  specifico  in    &#13;
 occasione  di lavoro in circoscritto ambiente infesto, e che provveda    &#13;
 alla tutela in caso di morte, e non anche in caso di danno.              &#13;
    3.  -  Questa  Corte ritiene che il regime vigente sopradescritto,    &#13;
 comprensivo non solo della norma impugnata, ma  anche  dell'art.  16,    &#13;
 primo comma, della legge 22 giugno 1933, n. 851, dell'art. 329, comma    &#13;
 primo, del r.d. 27 luglio 1934, n. 1265, nonché dell'art. 2, secondo    &#13;
 comma,  parte  seconda,  del  r.d.  17  agosto 1935, n. 1765, leda il    &#13;
 valore  costituzionale   della   salute,   esplicitamente   garantito    &#13;
 dall'articolo  32  della  Costituzione,  e  che  come  diritto  umano    &#13;
 fondamentale e  inviolabile,  non  può,  senza  alcuna  apprezzabile    &#13;
 ratio,   tollerare   limitazioni   o  esclusioni  del  corrispondente    &#13;
 inderogabile   dovere   di   solidarietà,   secondo   il    precetto    &#13;
 dell'articolo 2 della Costituzione. Inoltre, quando il lavoratore, in    &#13;
 occasione di lavoro, soggiacendo alla causa violenta,  che  configura    &#13;
 l'infortunio,   quale   è   la   repentina   puntura   dell'anofele,    &#13;
 inoculatrice dei parassiti patogeni, contrae la malattia con esito di    &#13;
 danno  grave e permanente, invalidante le sue capacità di lavoro, il    &#13;
 diritto previsto dall'art.  38,  comma  secondo,  della  Costituzione    &#13;
 risulta  violato,  se  il  regime assicurativo, continui ad escludere    &#13;
 dalle ipotesi di infortunio l'infezione malarica, nella presentazione    &#13;
 epidemiologica  attuale,  non  in  quella della prima metà di questo    &#13;
 secolo.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    a)  dichiara  la illegittimità costituzionale dell'articolo 2 del    &#13;
 d.P.R. 30 giugno 1965, n. 1124 ("Testo unico delle  disposizioni  per    &#13;
 l'assicurazione  contro  gli  infortuni  sul  lavoro  e  le  malattie    &#13;
 professionali") nella parte in  cui  non  comprende  tra  i  casi  di    &#13;
 infortunio   sul  lavoro  l'evento  dannoso  derivante  da  infezione    &#13;
 malarica, regolato da disposizioni speciali;                             &#13;
    b)  dichiara  -  in applicazione dell'art. 27 della legge 11 marzo    &#13;
 1953, n. 87 - la illegittimità costituzionale  dell'art.  16,  primo    &#13;
 comma,   della  legge  22  giugno  1933,  n.  851  ("Coordinamento  e    &#13;
 integrazione delle norme dirette a diminuire le cause                    &#13;
  della  malaria"),  dell'art.  329,  comma  primo, del r.d. 27 luglio    &#13;
 1934, n. 1265 ("Approvazione del testo unico delle leggi  sanitarie")    &#13;
 e dell'art. 2, secondo comma, parte seconda, del r.d. 17 agosto 1935,    &#13;
 n.  1765  ("Disposizioni  per  l'assicurazione   obbligatoria   degli    &#13;
 infortuni sul lavoro e delle malattie professionali").                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 giugno 1987.                                &#13;
                           Il Presidente: ANDRIOLI                        &#13;
                           Il Redattore: CASAVOLA                         &#13;
    Depositata in cancelleria il 17 giugno 1987.                          &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
