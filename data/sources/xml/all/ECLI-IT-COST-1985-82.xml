<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>82</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:82</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ROEHRSSEN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/03/1985</data_decisione>
    <data_deposito>20/03/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GUGLIELMO ROEHRSSEN, Presidente - Avv. &#13;
 ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi riuniti di legittimità costituzionale degli artt. 12,  &#13;
 primo comma, e 24 della legge della  Provincia  di  Bolzano  20  agosto  &#13;
 1972, n. 15 (Legge di riforma dell'edilizia abitativa), promossi con n.  &#13;
 9  ordinanze  emesse  il 31 gennaio, 6 marzo, 21 febbraio, 27 marzo, 17  &#13;
 aprile e 13 marzo 1984 dalla Corte d'Appello di Trento, iscritte ai nn.  &#13;
 379, 406, 422, 494, 549, 565, 576, 851 e  871  del  registro  ordinanze  &#13;
 1984  e  pubblicate  nella Gazzetta Ufficiale della Repubblica nn. 259,  &#13;
 266, 273, 287 e 294 dell'anno 1984 e n. 7 bis dell'anno 1985.            &#13;
     Udito nella camera di consiglio del 26  febbraio  1985  il  Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
     Ritenuto che:                                                        &#13;
     1.1  - la Corte d'Appello di Trento, con le nove ordinanze indicate  &#13;
 in epigrafe, ha  sollevato  questione  di  legittimità  costituzionale  &#13;
 dell'art.  12,  commi  primo  e  terzo,  della legge della Provincia di  &#13;
 Bolzano 20 agosto 1972, n. 15  e  successive  modificazioni  (Legge  di  &#13;
 riforma  dell'edilizia  abitativa),  in  riferimento  agli artt. 3 e 42  &#13;
 Cost.;                                                                   &#13;
     1.2 - il giudice remittente, premesso che  nei  giudizi  a  quibus,  &#13;
 tutti  concernenti  opposizione  alla determinazione dell'indennità di  &#13;
 esproprio,  vengono  in  considerazione  aree   dotate   di   vocazione  &#13;
 edificatoria  (donde la rilevanza della questione), deduce che la norma  &#13;
 censurata, imponendo, nel  dettare  i  criteri  per  la  determinazione  &#13;
 dell'indennità,  di  considerare  i terreni espropriati come agricoli,  &#13;
 prescinderebbe dalle effettive caratteristiche del bene  oblato,  così  &#13;
 offendendo  sia  la previsione costituzionale del "serio ristoro" (art.  &#13;
 42 Cost.), che lo stesso principio di eguaglianza (art.  3  Cost.),  in  &#13;
 relazione   alla   disparità   di  trattamento  conseguente  a  valori  &#13;
 indennitari che possono essere i più vari e i meno logici;              &#13;
     2.1  -  all'infuori  dell'ordinanza  n.  406/84,  il giudice a quo,  &#13;
 solleva, in via  subordinata  all'accoglimento  della  principale,  una  &#13;
 seconda  questione  di legittimità costituzionale, denunciando, sempre  &#13;
 in riferimento agli artt. 3 e 42 Cost., l'art. 24, primo  comma,  della  &#13;
 medesima  legge provinciale n. 15 del 1972, il quale, nell'ambito della  &#13;
 normativa concernente le aree  di  edilizia  agevolata  nelle  zone  di  &#13;
 espansione,  disciplina  l'acquisizione  di  dette  aree  da  parte del  &#13;
 comune;                                                                  &#13;
     2.2 - la Corte remittente rileva che, se il valore urbanistico  dei  &#13;
 beni  ablati  tornasse  ad avere rilevanza ai fini della determinazione  &#13;
 della indennità, non potrebbe non tenersi conto del fatto  che,  nelle  &#13;
 zone  di espansione, il terreno della comunione coatta che residua allo  &#13;
 stralcio di quello destinato all'edilizia  abitativa  agevolata  ed  è  &#13;
 restituito ai proprietari, viene perciò stesso ad acquistare un pregio  &#13;
 più   elevato,   in   quanto   affrancato  dal  rischio  di  ulteriori  &#13;
 espropriazioni e destinato all'edilizia residenziale  privata;  con  la  &#13;
 conseguente  illegittimità costituzionale della norma censurata, nella  &#13;
 parte in cui  non  detrae  dall'indennizzo,  a  mo'  di  compensazione,  &#13;
 l'ingiustificata locupletazione conseguita dall'espropriato mediante la  &#13;
 restituzione del suolo non utilizzato per fini pubblici;                 &#13;
     3. - in nessuno dei giudizi instaurati con le ordinanze in esame si  &#13;
 sono  costituite  parti  private,  né è intervenuto il Presidente del  &#13;
 Consiglio dei ministri;                                                  &#13;
     4. - i giudizi possono, data l'identità  delle  questioni,  essere  &#13;
 riuniti e congiuntamente decisi;                                         &#13;
     considerato  che  le  medesime questioni sono state già esaminate,  &#13;
 sotto gli stessi profili, dalla Corte costituzionale che, con  sentenza  &#13;
 n. 231 del 1984, ha dichiarato:                                          &#13;
     a)   l'illegittimità  costituzionale  -  limitatamente  al  regime  &#13;
 dell'indennità di esproprio previsto per le aree comprese  nel  centro  &#13;
 edificato   o   altrimenti   provviste,  in  relazione  alle  oggettive  &#13;
 caratteristiche  del  bene  ablato,  dell'attitudine   edificatoria   -  &#13;
 dell'art.    12, primo comma, della legge della Provincia di Bolzano n.  &#13;
 15 del 1972 e successive modificazioni, al quale andava circoscritta la  &#13;
 questione sollevata - allora, come nelle ordinanze  in  esame  -  dalla  &#13;
 stessa  Corte  d'Appello di Trento, anche nei confronti del terzo comma  &#13;
 dell'art. 12;                                                            &#13;
     b) l'inammissibilità della questione relativa all'art.  24,  primo  &#13;
 comma,  della  stessa  legge  provinciale,  in  quanto  le  conseguenze  &#13;
 ipotizzate dal giudice a quo si  sarebbero  verificate  solo  se  e  in  &#13;
 quanto  il  legislatore  altoatesino,  nel  ridefinire,  in conseguenza  &#13;
 dell'illegittimità  costituzionale  allora  pronunziata,   il   regime  &#13;
 indennitario,  non  avesse  tenuto  conto del trattamento di favore che  &#13;
 nelle ordinanze di remissione si assumeva riservato  ai  terreni  e  ai  &#13;
 soggetti espropriati nelle zone di espansione;                           &#13;
     che non vi sono motivi per discostarsi da tali decisioni, in quanto  &#13;
 il  giudice a quo prospetta le questioni con argomentazioni identiche a  &#13;
 quelle contenute nelle ordinanze di cui ai giudizi decisi con la citata  &#13;
 sentenza n. 231/84.                                                      &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e 9, secondo comma, delle Norme integrative per i giudizi dinanzi  alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a)   dichiara   la   manifesta   infondatezza  della  questione  di  &#13;
 legittimità costituzionale dell'art.  12,  primo  comma,  della  legge  &#13;
 della  provincia  di  Bolzano  20 agosto 1972, n. 15 ("Legge di riforma  &#13;
 dell'edilizia abitativa") e  successive  modificazioni,  sollevata,  in  &#13;
 riferimento  agli  artt.  3 e 42 Cost., dalla Corte d'Appello di Trento  &#13;
 con tutte le ordinanze indicate in epigrafe;                             &#13;
     b)  dichiara  la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità  costituzionale  dell'art.  24,  primo  comma, della legge  &#13;
 della Provincia di Bolzano 20 agosto 1972, n. 15, sollevata dalla Corte  &#13;
 d'Appello di Trento, in riferimento agli artt. 3 e  42  Cost.,  con  le  &#13;
 ordinanze 379, 422, 494, 549, 565, 576, 851 e 871 del 1984.              &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 19 marzo 1985.          &#13;
                                   F.to: GUGLIELMO  ROEHRSSEN  -  ORONZO  &#13;
                                   REALE  -  BRUNETTO  BUCCIARELLI DUCCI  &#13;
                                   ALBERTO MALAGUGINI - LIVIO PALADIN  -  &#13;
                                   ANTONIO   LA   PERGOLA   -   VIRGILIO  &#13;
                                   ANDRIOLI   -   GIUSEPPE   FERRARI   -  &#13;
                                   FRANCESCO  SAJA  -  GIOVANNI  CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE   BORZELLINO   -   FRANCESCO  &#13;
                                   GRECO.                                 &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
