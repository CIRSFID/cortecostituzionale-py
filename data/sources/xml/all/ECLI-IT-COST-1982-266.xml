<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>266</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:266</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/12/1982</data_decisione>
    <data_deposito>31/12/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - &#13;
 Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - &#13;
 Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale degli artt.  519 e 544  &#13;
 cod. pen. (Violenza carnale - causa speciale di estinzione  del  reato)  &#13;
 promosso con ordinanza emessa l'11 maggio 1981 dal Tribunale di Ravenna  &#13;
 nel  procedimento  penale a carico di Leone Taormina Vincenzo, iscritta  &#13;
 al n. 474 del registro  ordinanze  1981  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 297 del 28 ottobre 1981.                   &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del  2  dicembre  1982  il  Giudice  &#13;
 relatore Ettore Gallo.                                                   &#13;
     Ritenuto che, con ord. 11 maggio 1981, il Tribunale di Ravenna, nel  &#13;
 corso  di  un  procedimento  penale  a  carico  di  tale Leone Taormina  &#13;
 Vincenzo, imputato del delitto di cui agli artt. 81 cpv., 519  cpv.  n.  &#13;
 1  cod.  pen.,  ha  sollevato  questione di legittimità costituzionale  &#13;
 dell'art. 544 cod. pen. in relazione all'art.  3, primo comma, Cost.;    &#13;
     che,  per  l'esattezza,  nel  dispositivo  si  fa  anche   menzione  &#13;
 dell'art.  519 cod. pen., ma in realtà tutta l'ordinanza è unicamente  &#13;
 incentrata sull'asserita incompatibilità dell'art. 544, per  cui  deve  &#13;
 ritenersi  che  il richiamo all'altro articolo sia stato fatto soltanto  &#13;
 per la relazione, nel senso che la situazione contemplata nell'art. 544  &#13;
 era da intendersi, per la specie, riferita alla fattispecie prevista  e  &#13;
 punita nell'art. 519 cpv. n. 1 cod.  pen.;                               &#13;
     che,  in  effetti,  l'imputato,  confesso  di  avere avuto rapporti  &#13;
 carnali colla parte offesa,  minore  degli  anni  quattordici  (di  cui  &#13;
 asseriva avere ignorato l'età), aveva però presentato seria profferta  &#13;
 di  matrimonio,  che  la  minore  aveva  rifiutato  in  quanto il Leone  &#13;
 Taormina non consentiva che (e frattanto e  dopo  il  matrimonio)  ella  &#13;
 continuasse  ad  avere  rapporti intimi con altro uomo di cui si diceva  &#13;
 innamorata;                                                              &#13;
     che,  a  quel  punto,  il  Tribunale  aveva  ritenuto sussistere il  &#13;
 denunciato contrasto sia sotto il  profilo  della  ragionevolezza,  sia  &#13;
 sotto  quello  della eguaglianza, in quanto, per il primo, il principio  &#13;
 del c.d. "matrimonio riparatore" è rifiutato dalla  coscienza  sociale  &#13;
 come criterio inadeguato ed arcaico di composizione dei conflitti, vuoi  &#13;
 interpersonali vuoi - e tanto meno - della pretesa punitiva dello Stato  &#13;
 e,  per  il secondo, in quanto verrebbe a determinarsi grave disparità  &#13;
 fra chi va impunito  per  aver  ottenuto,  senza  convinzione  e  senza  &#13;
 ravvedimenti,  di  contrarre  matrimonio,  e  chi,  a causa del rifiuto  &#13;
 spesso irragionevole dell'offesa, non ottiene la celebrazione del  rito  &#13;
 nonostante ogni più sincero pentimento e il più serio proposito;       &#13;
     che il Presidente del Consiglio dei ministri ha spiegato intervento  &#13;
 tramite  l'Avvocatura  generale  dello  Stato,  la  quale ha chiesto la  &#13;
 restituzione degli atti al  giudice  a  quo,  essendo  stato  frattanto  &#13;
 abrogato  il  denunziato  art.  544  cod.  pen. in virtù della legge 5  &#13;
 agosto 1981 n. 442.                                                      &#13;
     Considerato però che, trattandosi di disposizione più  favorevole  &#13;
 al  reo,  essa  va  comunque  applicata - se applicabile - ai sensi del  &#13;
 disposto di cui al terzo comma dell'art. 2 cod. pen. sicché non è  il  &#13;
 caso  di  riproporre al Tribunale di Ravenna il riesame della posizione  &#13;
 in relazione allo jus superveniens;                                      &#13;
     che,  invece,  appare  manifestamente  inammissibile  per  assoluta  &#13;
 irrilevanza  il  prospettato  profilo d'incostituzionalità riferito al  &#13;
 principio  di  ragionevolezza,  giacché  non  si  è  in  alcun   modo  &#13;
 verificata  nella  specie la situazione oggettiva configurante la causa  &#13;
 speciale estintiva come delineata dal legislatore;                       &#13;
     che appare poi manifestamente infondato  l'alternativo  prospettato  &#13;
 profilo di vera e propria lesione del principio di uguaglianza, perché  &#13;
 la  legge,  attribuendo  efficacia estintiva al matrimonio che consegue  &#13;
 all'illecito rapporto carnale di cui  all'art.  519  cod.  pen.,  aveva  &#13;
 inteso  soltanto  di  evitare  che  la  condanna penale costituisse una  &#13;
 perenne causa di grave difficoltà all'incontro dei novelli coniugi  su  &#13;
 di un piano di distensione e di comprensione;                            &#13;
     che,   perciò,  è  da  escludere  che  il  legislatore  si  fosse  &#13;
 ripromesso d'istituire un premio all'operoso ravvedimento del reo;  per  &#13;
 cui,   senza   l'effettiva  celebrazione  del  matrimonio,  l'eventuale  &#13;
 pentimento   potrà   essere   preso   in   considerazione   ai    fini  &#13;
 dell'attenuante, ma resta una situazione soggettiva estranea alla ratio  &#13;
 dell'area    estintiva,    e    percio   insuscettibile   di   proporsi  &#13;
 comparativamente nella prospettiva dell'art. 3 Cost.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara manifestamente inammissibile la questione di  legittimità  &#13;
 costituzionale  dell'art.  544 cod. pen. in relazione all'art. 3 Cost.,  &#13;
 sollevata coll'ordinanza in epigrafe dal Tribunale di Ravenna sotto  il  &#13;
 profilo della ragionevolezza                                             &#13;
     Dichiara  manifestamente infondata la medesima questione, sollevata  &#13;
 colla stessa ordinanza dalla detta  autorità  giudiziaria,  sempre  in  &#13;
 relazione  al citato parametro costituzionale, sotto lo stretto profilo  &#13;
 del principio di uguaglianza.                                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 22 dicembre 1982.       &#13;
                                   F.to:  LEOPOLDO  ELIA  -  ANTONINO DE  &#13;
                                   STEFANO  -  GUGLIELMO   ROEHRSSEN   -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALDIN   -   ANTONIO   LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ETTORE GALLO.                          &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
