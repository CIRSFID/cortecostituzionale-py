<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-05-02/82/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-05-02/82"/>
          <FRBRalias value="ECLI:IT:COST:2023:82" name="ECLI"/>
          <FRBRdate date="2023-05-02" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="82"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-05-02/82/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-05-02/82/ita@"/>
          <FRBRdate date="2023-05-02" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-05-02/82/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-05-02/82/ita@.xml"/>
          <FRBRdate date="2023-05-02" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2023-02-21" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2023-05-02" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>non fondatezza</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="SCIARRA" href="/akn/ontology/person/it/SCIARRA" showAs="SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2023</akn4cc:anno>
        <akn4cc:numero>82</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2023:82</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>SCIARRA</akn4cc:presidente>
        <akn4cc:relatore>Giulio Prosperetti</akn4cc:relatore>
        <akn4cc:data_decisione>2023-02-21</akn4cc:data_decisione>
        <akn4cc:data_deposito>2023-05-02</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>non fondatezza</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>; Giudici : <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 6 della legge della Regione Abruzzo 11 marzo 2022, n. 4 (Interventi a favore del mototurismo), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 17-19 maggio 2022, depositato in cancelleria il 19 maggio 2022, iscritto al n. 32 del registro ricorsi 2022 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2022.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Abruzzo;
 udito nell'udienza pubblica del 21 febbraio 2023 il Giudice relatore Giulio Prosperetti;
 uditi l'avvocato dello Stato Fabrizio Fedeli per il Presidente del Consiglio dei ministri e l'avvocato Alessia Frattale per la Regione Abruzzo;
 deliberato nella camera di consiglio del 21 febbraio 2023.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso depositato il 19 maggio 2022 (reg. ric. n. 32 del 2022), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale dell'art. 6 della legge della Regione Abruzzo 11 marzo 2022, n. 4 (Interventi a favore del mototurismo), in riferimento agli artt. 81, terzo comma, e 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione agli <mref><ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_17__para_1">artt. 17, commi 1</ref> e <ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_17__para_3">3</ref>, e <ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_17__para_19">19 della legge 31 dicembre 2009, n. 196</ref></mref> (Legge di contabilità e finanza pubblica).
 2.- Il ricorrente rappresenta che la legge reg. Abruzzo n. 4 del 2022 reca una specifica disciplina per promuovere il turismo motociclistico e, a tal fine, ha previsto una serie di interventi senza quantificarne gli oneri e senza prevedere adeguata copertura finanziaria.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In particolare, secondo il Presidente del Consiglio dei ministri, comporterebbero nuove spese le disposizioni di cui agli artt. 1, 2, 3 e 5 della legge reg. Abruzzo n. 4 del 2022 che prevedono, rispettivamente, l'istituzione e la redazione di una «Rete degli itinerari mototuristici d'Abruzzo» e il relativo «Piano di gestione» (artt. 1 e 2); la realizzazione di interventi in materia di programmazione turistica regionale che comprenda la promozione della rete degli itinerari mototuristici; l'organizzazione di eventi, quali motoraduni e motoraid; la diversificazione delle offerte ricettive; il coordinamento degli itinerari mototuristici regionali abruzzesi con quelli nazionali e internazionali; la promozione degli itinerari mototuristici abruzzesi (art. 3); l'istituzione dell'accompagnatore mototuristico, iscritto in un apposito elenco regionale di valore puramente informativo che viene pubblicato sul sito internet istituzionale e sul portale turistico regionale (art. 5).
 In relazione alle suddette attività il successivo art. 6, comma 1, oggetto di impugnazione, prevede la clausola di invarianza finanziaria, per cui l'amministrazione farà fronte ai relativi oneri con le risorse esistenti a legislazione vigente.
 3.- Le censure del ricorrente si appuntano sul fatto che la clausola di invarianza finanziaria della spesa non è stata supportata dalla relazione tecnica e dagli allegati richiesti dall'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_17">art. 17 della legge n. 196 del 2009</ref> per dimostrare la sufficienza delle risorse esistenti e, pertanto, si risolverebbe in una mera clausola di stile, in contrasto con l'art. 81, terzo comma, Cost., che impone la quantificazione e la copertura della spesa, e con l'art. 117, terzo comma, Cost. che, tra i principi di coordinamento della finanza pubblica, annovera la necessità di redigere la relazione tecnica delle leggi di spesa.
 4.- Con lo stesso ricorso è impugnato anche il comma 2 dell'art. 6 della legge reg. Abruzzo n. 4 del 2022 che prevede un apposito e nuovo capitolo di bilancio per il futuro compimento delle attività previste dall'art. 4 della medesima legge regionale, senza quantificare e coprire gli oneri conseguenti.
 Il suddetto art. 4 prevede interventi a favore delle persone affette da disabilità e per la mobilità sostenibile e la guida con prudenza; in particolare, alla Regione è affidato il compito di promuovere e sostenere tali attività anche mediante la concessione di finanziamenti specifici per l'abbattimento delle barriere architettoniche e la fruibilità della rete mototuristica con speciali tipologie di moto dedicate, quali motocarrozzette o sidecar, nonché per la promozione del mototurismo con mezzi ecologici e per il suo svolgimento in sicurezza, attraverso una campagna di sensibilizzazione attuata con cartellonistica stradale.
 5.- L'art. 6, comma 2, della legge reg. Abruzzo n. 4 del 2022 stabilisce che gli interventi, previsti dal precedente art. 4 della stessa legge, saranno finanziati a decorrere dall'anno 2023, ma non quantifica la relativa spesa, rinviando alle singole leggi di bilancio annuali.
 Secondo la difesa statale, tale modalità di copertura della spesa sarebbe in contrasto con l'art. 81, terzo comma, Cost., in relazione all'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_19__para_1">art. 19, comma 1, della legge n. 196 del 2009</ref>, che impone la quantificazione degli oneri e l'indicazione della copertura per ciascuno degli esercizi del bilancio pluriennale di previsione 2022-2024.
 6.- Si è costituita in giudizio la Regione Abruzzo eccependo la non fondatezza del ricorso e chiedendone il rigetto.
 In riferimento alle attività previste dagli artt. 1, 2 e 3 della legge reg. Abruzzo n. 4 del 2022, la difesa regionale ha dedotto che esse possono essere svolte senza incremento alcuno della spesa, facendo ricorso alle risorse destinate alla programmazione e pianificazione turistica per la quale la legislazione vigente prevede appositi stanziamenti di risorse.
 6.1.- In particolare, la Regione evidenzia che le attività di cui all'art. 1, comma 3, e all'art. 2, comma 2, della legge reg. Abruzzo n. 4 del 2022 si traducono in una ricognizione e valutazione degli itinerari turistici, per consigliare quelli da effettuare in moto, e, per loro natura, rientrano nell'attività di pianificazione turistica realizzabile con le risorse umane, strumentali e finanziarie a ciò destinate sulla base della legislazione vigente.
 6.2.- Parimenti senza oneri, facendo ricorso ad intese e ad altre forme di coordinamento, potrebbero attuarsi le attività di cui all'art. 3 della legge regionale impugnata per la realizzazione di una rete degli itinerari di mototurismo e per la promozione di eventi dedicati, comprese le attività di diversificazione dell'offerta delle strutture ricettive, anche tramite apposita segnaletica, e il coordinamento con itinerari di altre regioni e con quelli internazionali.
 Secondo la difesa della resistente, sarà la Giunta regionale, in sede di programmazione turistica e nei limiti delle risorse disponibili, a promuovere gli interventi soprarichiamati, coordinandoli con quelli finanziati sulla base dei fondi europei o con altre azioni finalizzate al raggiungimento degli obiettivi previsti.
 6.3.- Con riferimento all'art. 5 della legge reg. Abruzzo n. 4 del 2022, la Regione osserva che l'integrazione del repertorio regionale con il profilo professionale dell'accompagnatore mototuristico integra un'attività ordinaria che la legislazione di settore già demanda alle strutture amministrative competenti in materia di formazione professionale.
 6.4.- In sintesi, dunque, la natura stessa delle attività previste dagli artt. 1, 2, 3 e 5 della legge reg. Abruzzo n. 4 del 2022 sarebbe idonea a suffragare la clausola di invarianza finanziaria, rendendo irrilevante la mancanza di apposita relazione tecnico-finanziaria per dimostrare l'assenza di oneri aggiuntivi.
 7.- In merito all'art. 6, comma 2, della legge reg. Abruzzo n. 4 del 2022, la difesa regionale osserva che gli impegni di spesa derivanti dall'art. 4 della legge regionale impugnata integrano spese continuative di carattere facoltativo che non generano un obbligo immediato di copertura finanziaria della spesa, ma consentono di individuarla di volta in volta nell'ambito del bilancio dei singoli esercizi, giusto quanto stabilito dall'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_38__para_1">art. 38, comma 1, del decreto legislativo 23 giugno 2011, n. 118</ref> (Disposizioni in materia di armonizzazione dei sistemi contabili e degli schemi di bilancio delle Regioni, degli enti locali e dei loro organismi, a norma degli <mref><ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_1">articoli 1</ref> e <ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_2">2 della legge 5 maggio 2009, n. 42</ref></mref>), che si applica alle regioni quale norma speciale e prevalente rispetto agli <mref><ref href="/akn/it/act/legge/stato/2009/196/!main#art_17">artt. 17</ref> e <ref href="/akn/it/act/legge/stato/2009/196/!main#art_19">19 della legge n. 196 del 2009</ref></mref> indicati nel ricorso del Presidente del Consiglio dei ministri.
 Pertanto, troverebbe applicazione il richiamato <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_38__para_1">art. 38, comma 1, del d.lgs. n. 118 del 2011</ref> e la norma regionale impugnata non sarebbe in contrasto né con l'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_19">art. 19 della legge n. 196 del 2009</ref> per la mancata quantificazione della spesa, essendo essa discrezionale, né con l'art. 17 della stessa <ref href="/akn/it/act/legge/stato/2009/196/!main">legge n. 196 del 2009</ref> per l'assenza della relazione tecnico-finanziaria e della copertura finanziaria, non essendo la spesa quantificabile.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con il ricorso indicato in epigrafe (reg. ric. n. 32 del 2022), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questioni di legittimità costituzionale dell'art. 6 della legge reg. Abruzzo n. 4 del 2022 che prevedono, rispettivamente, la clausola di invarianza finanziaria della spesa per il compimento di attività volte a promuovere e sostenere il mototurismo regionale (indicate dai precedenti artt. 1, 2, 3 e 5) e l'istituzione di un apposito stanziamento di bilancio per finanziare, a decorrere dall'anno 2023, gli interventi (di cui al precedente art. 4), per l'attività mototuristica con mezzi ecologici e con mezzi idonei ai soggetti diversamente abili, nonché gli interventi necessari ad una campagna di sensibilizzazione al mototurismo in sicurezza, il tutto anche mediante la concessione di finanziamenti specifici.
 2.- Con riferimento al comma 1 dell'art. 6 della legge reg. Abruzzo n. 4 del 2022, le censure del ricorrente concernono la clausola di invarianza finanziaria della spesa che non è supportata dalla relazione tecnica e dagli allegati richiesti dall'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_17">art. 17 della legge n. 196 del 2009</ref>, al fine di dimostrare la possibilità di adempiere alle funzioni attribuite dagli artt. 1, 2, 3 e 5 della legge reg. Abruzzo n. 4 del 2022 con le risorse finanziarie, umane e strumentali già previste a legislazione vigente.
 3.- Quanto al comma 2 dell'art. 6 impugnato, il Presidente del Consiglio dei ministri censura la mancata quantificazione e copertura degli oneri derivanti dagli interventi di cui all'art. 4 della legge regionale impugnata - in particolare gli interventi per la diffusione del mototurismo con mezzi ecologici e con mezzi idonei al trasporto dei disabili e gli interventi per il mototurismo in sicurezza - rinviando, invece, per l'individuazione delle risorse, agli stanziamenti di spesa iscritti sul bilancio regionale.
 L'illegittimità costituzionale di entrambe le disposizioni è dedotta in riferimento agli artt. 81, terzo comma, e 117, terzo comma, Cost., in relazione all'<mref><ref href="/akn/it/act/legge/stato/2009/196/!main#art_17__para_1">art. 17, commi 1</ref> e <ref href="/akn/it/act/legge/stato/2009/196/!main#art_17__para_3">3, della legge n. 196 del 2009</ref></mref>, che prescrive la redazione di relazioni tecniche per dimostrare la copertura della spesa o la sua invarianza, e all'art. 19 della medesima <ref href="/akn/it/act/legge/stato/2009/196/!main">legge n. 196 del 2009</ref>, che estende la quantificazione e la copertura delle spese pluriennali prevista per le leggi statali anche alle disposizioni delle leggi regionali.
 4.- La questione relativa all'art. 6, comma 1, della legge reg. Abruzzo n. 4 del 2022 è fondata.
 4.1.- Il principio di copertura finanziaria della spesa e il correlato obbligo di quantificazione di cui all'art. 81, terzo comma, Cost. impongono un preciso vincolo al legislatore che viene declinato nella redazione della relazione tecnica, disciplinata dall'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_17__para_3">art. 17, comma 3, della legge n. 196 del 2009</ref>, in base al quale tutti gli atti normativi sono accompagnati, appunto, dalla suddetta relazione, «predisposta dalle amministrazioni competenti e verificata dal Ministero dell'economia e delle finanze, sulla quantificazione delle entrate e degli oneri recati da ciascuna disposizione, nonché delle relative coperture, con la specificazione, per la spesa corrente e per le minori entrate, degli oneri annuali fino alla completa attuazione delle norme e, per le spese in conto capitale, della modulazione relativa agli anni compresi nel bilancio pluriennale e dell'onere complessivo in relazione agli obiettivi fisici previsti».
 Il successivo comma 6-bis dello stesso <ref href="/akn/it/act/legge/stato/2009/196/!main#art_17">art. 17 della legge n. 196 del 2009</ref> si occupa della clausola di neutralità finanziaria, prevedendo che in tali casi «la relazione tecnica riporta la valutazione degli effetti derivanti dalle disposizioni medesime, i dati e gli elementi idonei a suffragare l'ipotesi di invarianza degli effetti sui saldi di finanza pubblica, attraverso l'indicazione dell'entità delle risorse già esistenti nel bilancio e delle relative unità gestionali, utilizzabili per le finalità indicate dalle disposizioni medesime anche attraverso la loro riprogrammazione. In ogni caso, la clausola di neutralità finanziaria non può essere prevista nel caso di spese di natura obbligatoria».
 L'art. 19 della medesima <ref href="/akn/it/act/legge/stato/2009/196/!main">legge n. 196 del 2009</ref> estende, poi, tali precetti a tutte le regioni e alle Province autonome di Trento e di Bolzano.
 Pertanto, il legislatore regionale è tenuto alla redazione della relazione tecnica anche nel caso in cui la norma non necessiti di nuove coperture rispetto alle disponibilità già esistenti a bilancio, dovendo in questa ipotesi comunque indicare l'entità di tali risorse per rendere attendibile la loro idoneità e sufficienza rispetto agli adempimenti previsti.
 5.- Questa Corte ha più volte precisato che la clausola di invarianza finanziaria non può tradursi in una mera clausola di stile e che, «[o]ve la nuova spesa si ritenga sostenibile senza ricorrere alla individuazione di ulteriori risorse, per effetto di una più efficiente e sinergica utilizzazione delle somme allocate nella stessa partita di bilancio per promiscue finalità, la pretesa autosufficienza non può comunque essere affermata apoditticamente, ma va corredata da adeguata dimostrazione economica e contabile» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2012/115/!main">sentenza n. 115 del 2012</ref>), consistente nell'esatta quantificazione delle risorse disponibili e della loro eventuale eccedenza utilizzabile per la nuova o maggiore spesa, i cui oneri devono essere specificamente quantificati per dimostrare l'attendibilità della copertura.
 6.- Pertanto, la Regione avrebbe dovuto redigere apposita relazione tecnica sulla possibilità di realizzare il disposto legislativo attraverso le normali dotazioni degli uffici, e, quindi, non può essere accolta la prospettazione difensiva per cui l'illegittimità costituzionale sarebbe esclusa dal fatto che la promozione dell'attività mototuristica avrebbe un mero carattere di indirizzo non comportante spese aggiuntive.
 Peraltro, tale affermazione non trova conferma nella stessa legge regionale oggetto di impugnazione e, in particolare, nel disposto dell'art. 3, comma 2, laddove si prevede che i suddetti interventi «sono coordinati con quelli finanziati sulla base di fondi europei».
 La necessaria relazione tecnica avrebbe dovuto specificare quanto di pertinenza della normale dotazione degli uffici e quanto, invece, avrebbe dovuto essere imputato ai fondi europei.
 Va, pertanto, dichiarata l'illegittimità costituzionale dell'art. 6, comma 1, della legge reg. Abruzzo n. 4 del 2022, per violazione dell'art. 81, terzo comma, Cost., in relazione all'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_17">art. 17 della legge n. 196 del 2009</ref>.
 7.- Diversa questione è quella promossa dal ricorrente in ordine al comma 2 dell'art. 6 della legge reg. Abruzzo n. 4 del 2022 che, per le modalità di finanziamento delle attività del precedente art. 4, non provvede contestualmente alla copertura dei relativi oneri rinviando, invece, agli stanziamenti di spesa iscritti sul bilancio annuale regionale.
 L'art. 4 è dedicato a interventi in favore dei disabili, nonché per la mobilità sostenibile e la guida con prudenza, e attribuisce alla Regione un compito di promozione e sostegno in tali campi, da realizzarsi anche mediante finanziamenti specifici sia per l'abbattimento delle barriere architettoniche e la fruibilità della rete mototuristica con speciali tipologie di moto dedicate, quali motocarrozzette o sidecar, sia per la promozione del mototurismo con mezzi ecologici e per il suo svolgimento in sicurezza, attraverso una campagna di sensibilizzazione attuata con cartellonistica stradale.
 L'illegittimità costituzionale dell'art. 6, comma 2, della legge regionale impugnata è dedotta in riferimento all'art. 81, terzo comma, Cost., in relazione all'<ref href="/akn/it/act/legge/stato/2009/196/!main#art_19">art. 19 della legge n. 196 del 2009</ref> che prescrive la quantificazione e la copertura delle spese pluriennali.
 8.- La questione non è fondata.
 9.- La Regione fa corretto riferimento all'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_38">art. 38 del d.lgs. n. 118 del 2011</ref> che, per le spese pluriennali continuative, prevede che le leggi regionali quantificano l'onere annuale previsto per ciascuno degli esercizi finanziari compresi nel bilancio di previsione e indicano l'onere a regime solo in caso di spese obbligatorie, rinviando, per quelle facoltative, alla legge di bilancio dei singoli esercizi.
 Il rinvio alla successiva legge di bilancio non concerne soltanto il quantum della spesa, ma al legislatore regionale è rimesso anche l'an della realizzazione delle attività.
 10.- Gli interventi previsti dall'art. 4 della legge reg. Abruzzo n. 4 del 2022 non sono, infatti, di immediata applicazione e la norma attribuisce alla Regione un compito di promozione e sostegno di tali attività, demandando alla Giunta regionale di disciplinare la concessione dei finanziamenti specifici per favorirne il compimento.
 A tal fine il comma 2 dell'art. 6 della legge regionale impugnata, nell'individuare un capitolo di bilancio su cui appostare le risorse per sostenere gli oneri dei suddetti interventi, richiama espressamente l'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_38">art. 38 del d.lgs. n. 118 del 2011</ref> e precisa che l'autorizzazione di spesa è consentita solo nei limiti degli stanziamenti annualmente iscritti sul bilancio regionale, con ciò rendendo evidente il carattere eventuale dell'attività, in relazione alle necessarie risorse disponibili.
 11.- In applicazione dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_38">art. 38 del d.lgs. n. 118 del 2011</ref>, dunque, la legge regionale impugnata rinvia l'obbligo di copertura finanziaria a decorrere dal 2023 con l'adozione della legge di bilancio, quale momento in cui sono compiute le scelte allocative delle risorse.
 In questo senso, il comma 3 dell'art. 6 della legge reg. Abruzzo n. 4 del 2022 dispone che: «[l]'autorizzazione alla spesa di cui alla presente legge è consentita solo nei limiti degli stanziamenti di spesa annualmente iscritti sul bilancio regionale»; e ciò in attuazione del canone costituzionale dell'art. 81, terzo comma, Cost. di cui il ricordato art. 38 costituisce disposizione specificativa (in questi termini <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021/226/!main">sentenza n. 226 del 2021</ref>).
 L'omessa quantificazione degli oneri e il rinvio del finanziamento degli interventi al 2023 trova, dunque, fondamento nella natura non obbligatoria della spesa prevista dalla disposizione impugnata, «restando comunque fermo che qualunque sua attuazione dovrà essere preceduta da idonea disposizione di legge regionale recante adeguata quantificazione e relativa copertura» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2023/48/!main">sentenza n. 48 del 2023</ref>; nello stesso senso, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2023/57/!main">sentenza n. 57 del 2023</ref>).
 Pertanto, va dichiarata non fondata la questione di legittimità costituzionale, promossa in riferimento agli artt. 81, terzo comma, e 117, terzo comma, Cost., dell'art. 6, comma 2, della legge reg. Abruzzo n. 4 del 2022, stante il carattere eventuale della spesa la cui quantificazione spetterà alle leggi di bilancio annuali.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 1) dichiara l'illegittimità costituzionale dell'art. 6, comma 1, della legge della Regione Abruzzo 11 marzo 2022, n. 4 (Interventi a favore del mototurismo);
 2) dichiara non fondata la questione di legittimità costituzionale dell'art. 6, comma 2, della legge reg. Abruzzo n. 4 del 2022, promossa, in riferimento agli artt. 81, terzo comma, e 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione agli <mref><ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_17__para_1">artt. 17, commi 1</ref> e <ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_17__para_3">3</ref>, e <ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_17__para_19">19 della legge 31 dicembre 2009, n. 196</ref></mref> (Legge di contabilità e finanza pubblica), dal Presidente del Consiglio dei ministri, con il ricorso indicato in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2023-02-21">21 febbraio 2023</date>.
 F.to:
 <signature><judge refersTo="#silvanaSciarra" as="#presidente">Silvana SCIARRA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulioProsperetti" as="#relatore">Giulio PROSPERETTI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2023-05-02">2 maggio 2023</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
