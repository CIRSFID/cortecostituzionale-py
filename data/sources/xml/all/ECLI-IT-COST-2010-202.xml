<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2010</anno_pronuncia>
    <numero_pronuncia>202</numero_pronuncia>
    <ecli>ECLI:IT:COST:2010:202</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMIRANTE</presidente>
    <relatore_pronuncia>Paolo Grossi</relatore_pronuncia>
    <redattore_pronuncia>Paolo Grossi</redattore_pronuncia>
    <data_decisione>07/06/2010</data_decisione>
    <data_deposito>10/06/2010</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Francesco AMIRANTE; Giudici : Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 3, comma 5, lettera b), 4, comma 4, lettera b) e 20 della legge della Regione Lazio 11 agosto 2009, n. 21 (Misure straordinarie per il settore edilizio ed interventi per l'edilizia residenziale sociale), promosso con ricorso del Presidente del Consiglio dei ministri notificato il 20 ottobre 2009, depositato in cancelleria il 27 ottobre 2009 ed iscritto al n. 100 del registro ricorsi 2009.&#13;
          Visto l'atto di costituzione della Regione Lazio;  &#13;
          udito nell'udienza pubblica del 28 aprile 2010 il Giudice relatore Paolo Grossi;&#13;
          uditi l'avvocato dello Stato Maurizio Borgo per il Presidente del Consiglio dei ministri e l'avvocato Massimo Luciani per la Regione Lazio.</epigrafe>
    <testo>Ritenuto che, con ricorso notificato il 20 ottobre 2009 e depositato il successivo 27 ottobre, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato l'art. 3, comma 5, lettera b), l'art. 4, comma 4, lettera b) e l'art. 20 della legge della Regione Lazio 11 agosto 2009, n. 21 (Misure straordinarie per il settore edilizio ed interventi per l'edilizia residenziale sociale), nella parte in cui [all'art. 3, comma 5, lettera b) ed all'art. 4, comma 4, lettera b)] «si prevede che la realizzazione degli interventi di ampliamento, demolizione e ricostruzione sia subordinata alla predisposizione del fascicolo del fabbricato secondo quanto previsto dalla l.r. n. 31/2002 (Istituzione del fascicolo del fabbricato) e dal relativo regolamento regionale di attuazione n. 6/2005, ovvero dagli specifici regolamenti comunali, qualora adottati», e nella parte in cui [all'art. 20] «si prevede la redazione del fascicolo del fabbricato, da allegare al quadro tecnico-economico finale dell'intervento, a cura dei beneficiari del finanziamento regionale per l'edilizia residenziale pubblica, ivi compresa l'edilizia agevolata-convenzionale»;&#13;
 che, secondo il ricorrente, le norme impugnate - nell'accollare ai privati una serie di accertamenti, nonché l'acquisizione e la conservazione di informazioni e documenti (compiti, questi ultimi, attribuiti alla pubblica amministrazione nell'esercizio della propria funzione di vigilanza) - si pongono in contrasto: a) con l'art. 3 della Costituzione, per irragionevolezza, e con l'art. 97 Cost., per violazione del principio di efficienza e buon andamento della pubblica amministrazione (come già affermato da questa Corte nella sentenza n. 315 del 2003); b) con gli artt. 23, 41 e 42 Cost., trattandosi di «prestazioni imposte» che, in quanto incidono sulla libertà di iniziativa economica e sul diritto di proprietà, devono trovare la loro fonte nella disciplina statale; c) con l'art. 117, secondo comma, lettera l), Cost. per violazione della competenza legislativa esclusiva dello Stato in materia di ordinamento civile; d) in subordine, con l'art. 117, terzo comma, Cost. per lesione della competenza statale sui principi fondamentali in materia di governo del territorio;&#13;
 che si è costituita la Regione Lazio, la quale ha concluso per la declaratoria di inammissibilità o, comunque. di non fondatezza delle sollevate censure con riferimento a tutti i parametri evocati.&#13;
 Considerato preliminarmente che la Regione Lazio risulta essersi costituita in giudizio, in persona del Vicepresidente e f.f. di Presidente della Giunta regionale, sulla base della determinazione del Segretario Generale del Consiglio regionale n. 449 del 10 novembre 2009 e sulla base della determinazione del Direttore del Dipartimento Territorio n. B5991 del 19 novembre 2009;&#13;
 che, in particolare, nell'atto di determinazione del Segretario Generale del Consiglio regionale, la legittimazione dell'organo emittente è stata fondata sulle prerogative del Presidente del Consiglio regionale, delineate negli artt. 21 e 24 della legge regionale statutaria 11 novembre 2004, n. 1 (Nuovo Statuto della Regione Lazio), tra le quali non è prevista la competenza a deliberare sui giudizi innanzi alla Corte costituzionale;&#13;
 che la norma cui occorre fare riferimento è l'art. 32, comma 2, della legge 11 marzo 1953, n. 87, cui si adegua l'art. 41, comma 4, del vigente Statuto della Regione Lazio, prevedendo, tra l'altro, che il Presidente della Regione, previa deliberazione della Giunta regionale, «promuove l'impugnazione delle leggi dello Stato e delle altre Regioni e propone ricorso per conflitti di attribuzione dinanzi alla Corte costituzionale» (ordinanza n. 126 del 2010);&#13;
 che, in tale competenza ad autorizzare la promozione dei giudizi di costituzionalità, deve ritenersi compresa anche la deliberazione di costituirsi in tali giudizi, data la natura politica della valutazione che i due atti richiedono;&#13;
 che, pertanto, la costituzione della Regione Lazio deve ritenersi inammissibile;&#13;
 che, peraltro, successivamente alla proposizione del ricorso, la Regione Lazio, con l'art. 1 della legge regionale 3 febbraio 2010, n. 1 (recante «Modifiche alla legge regionale 11 agosto 2009, n. 21 - Misure straordinarie per il settore edilizio ed interventi per l'edilizia residenziale sociale - e successive modifiche»), è intervenuta su tutte le disposizioni impugnate mediante, da un lato, la sostituzione del comma 5 dell'art. 3 con altra norma che (per consentire la realizzazione dei menzionati interventi edilizi di ampliamento, demolizione e ricostruzione) non prevede più la predisposizione del fascicolo del fabbricato, e, dall'altro lato, la abrogazione espressa degli artt. 4, comma 4, lettera b), e 20 della legge regionale n. 21 del 2009;&#13;
 che, proprio in considerazione di ciò, il ricorrente - con atto notificato alla Regione Lazio nella persona del Presidente della Giunta regionale pro tempore il 20 aprile 2010 e depositato presso la cancelleria di questa Corte il successivo 26 aprile - ha rinunciato al ricorso, affermando che tali modifiche hanno fatto venir meno le motivazioni del ricorso;&#13;
 che, in mancanza di costituzione in giudizio della parte convenuta, la rinuncia al ricorso determina, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (da ultimo, ordinanze n. 148 e n. 137 del 2010).</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara estinto il processo. &#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 giugno 2010.&#13;
 F.to:&#13;
 Francesco AMIRANTE, Presidente&#13;
 Paolo GROSSI, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 10 giugno 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
