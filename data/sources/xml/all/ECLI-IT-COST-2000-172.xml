<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>172</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:172</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/05/2000</data_decisione>
    <data_deposito>01/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 11, comma 5,    &#13;
 della  legge  30  dicembre 1991, n. 413 (Disposizioni per ampliare le    &#13;
 basi   imponibili,   per   razionalizzare,  facilitare  e  potenziare    &#13;
 l'attività   d'accertamento;   disposizioni   per  la  rivalutazione    &#13;
 obbligatoria  dei  beni immobili delle imprese, nonché per riformare    &#13;
 il  contenzioso e per la definizione agevolata dei rapporti tributari    &#13;
 pendenti; delega al Presidente della Repubblica per la concessione di    &#13;
 amnistia  per  reati  tributari; istituzione dei centri di assistenza    &#13;
 fiscale  e  del  conto  fiscale), promosso con ordinanza emessa il 25    &#13;
 settembre  1998  dalla  commissione tributaria provinciale di Foggia,    &#13;
 sui  ricorsi riuniti proposti da Di Iasio Raffaele ed altri contro la    &#13;
 direzione  regionale per le entrate della Puglia - sezione di Foggia,    &#13;
 iscritta  al  n. 164  del  registro ordinanze 1999 e pubblicata nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica - prima serie speciale - n. 12    &#13;
 dell'anno 1999;                                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 22 marzo 2000 il giudice    &#13;
 relatore Massimo Vari;                                                   &#13;
     Ritenuto  che,  con  ordinanza del 25 settembre 1998 (iscritta al    &#13;
 r.o.  n. 164  del  1999),  la  commissione  tributaria provinciale di    &#13;
 Foggia  ha  sollevato  -  nel corso di un giudizio proposto da taluni    &#13;
 contribuenti,  per  la  restituzione  della ritenuta IRPEF effettuata    &#13;
 sulle  somme  loro  corrisposte  nel  1997, a titolo di indennità di    &#13;
 esproprio  questione  di  legittimità  costituzionale  dell'art. 11,    &#13;
 comma  5,  della  legge  30  dicembre  1991, n. 413 (Disposizioni per    &#13;
 ampliare   le  basi  imponibili,  per  razionalizzare,  facilitare  e    &#13;
 potenziare    l'attività   d'accertamento;   disposizioni   per   la    &#13;
 rivalutazione  obbligatoria  dei beni immobili delle imprese, nonché    &#13;
 per  riformare  il  contenzioso  e  per  la definizione agevolata dei    &#13;
 rapporti  tributari  pendenti;  delega al Presidente della Repubblica    &#13;
 per  la  concessione di amnistia per reati tributari; istituzione dei    &#13;
 centri  di  assistenza  fiscale e del conto fiscale), "nella parte in    &#13;
 cui   non  prevede  l'assoggettamento  a  tassazione  soltanto  delle    &#13;
 indennità   riscosse   in   virtù   di  atti,  anche  volontari,  o    &#13;
 provvedimenti emessi successivamente al 31 dicembre 1988";               &#13;
                                                                          &#13;
         che  il  rimettente  -  nel denunciare violazione dell'art. 3    &#13;
 della  Costituzione  -  ritiene  che  la censurata disposizione debba    &#13;
 essere intesa nel senso "che le somme percepite a titolo di esproprio    &#13;
 e  di  cessione  volontaria nel corso del procedimento espropriativo,    &#13;
 nonché  a  seguito  di  occupazioni  acquisitive, a far tempo dal 1°    &#13;
 gennaio  1992,  cioè  dalla  data  di entrata in vigore della legge,    &#13;
 siano  assoggettate a tassazione... senza che abbia alcuna incidenza,    &#13;
 per l'insorgenza dell'obbligo contributivo, la data del provvedimento    &#13;
 o dell'atto che ha generato la corresponsione delle somme";              &#13;
         che,  ad  avviso  del  giudice  a  quo,  ne  conseguirebbe un    &#13;
 trattamento  privilegiato  per i contribuenti di cui al comma 9 della    &#13;
 disposizione  censurata - e cioè per coloro che vengono assoggettati    &#13;
 retroattivamente  a  tassazione  per il triennio 1989-1991 - rispetto    &#13;
 alla  generalità  dei  contribuenti  ricadenti  nella previsione del    &#13;
 comma  5,  "atteso  che  soltanto in favore dei primi, e senza alcuna    &#13;
 apprezzabile   giustificazione   razionale,   la   tassazione   viene    &#13;
 circoscritta  alle  somme  percepite  in  conseguenza  di  atti anche    &#13;
 volontari  o provvedimenti emessi successivamente al 31 dicembre 1988    &#13;
 e fino alla data di entrata in vigore della legge";                      &#13;
         che  è intervenuto il Presidente del Consiglio dei Ministri,    &#13;
 rappresentato   e   difeso   dall'avvocatura  generale  dello  Stato,    &#13;
 chiedendo che la sollevata questione venga dichiarata non fondata;       &#13;
                                                                          &#13;
     Considerato   che   il   rimettente,   nell'assumere  che,  nella    &#13;
 fattispecie al suo esame, non avrebbe alcun rilievo, per l'insorgenza    &#13;
 dell'obbligo tributario, la data del provvedimento o dell'atto che ha    &#13;
 generato il diritto alla corresponsione delle somme, pone, a premessa    &#13;
 della  denunciata  disparità  di trattamento, una scelta ermeneutica    &#13;
 che richiedeva di essere adeguatamente argomentata e motivata, attesa    &#13;
 l'esistenza di orientamenti giurisprudenziali tutt'altro che univoci,    &#13;
 circa  i  presupposti  di  applicabilità dell'art. 11 della legge 30    &#13;
 dicembre 1991, n. 413, segnatamente con riferimento alla necessità o    &#13;
 meno  di  tener  conto,  nelle  varie fattispecie di tassazione delle    &#13;
 plusvalenze,  della  data  del  titolo posto a fondamento del diritto    &#13;
 all'indennità;                                                          &#13;
                                                                          &#13;
         che,  avendo  il  giudice  a quo l'obbligo di dar conto della    &#13;
 rilevanza   e   della  non  manifesta  infondatezza  della  sollevata    &#13;
 questione,  la evidenziata carenza di motivazione preclude alla Corte    &#13;
 la  necessaria  verifica  sulla sussistenza di entrambe le condizioni    &#13;
 occorrenti    per    dare   valido   ingresso   allo   scrutinio   di    &#13;
 costituzionalità;                                                       &#13;
         che,   pertanto,   la   questione   deve   essere  dichiarata    &#13;
 manifestamente inammissibile;                                            &#13;
                                                                          &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale  dell'art. 11,  comma  5, della legge 30    &#13;
 dicembre  1991, n. 413 (Disposizioni per ampliare le basi imponibili,    &#13;
 per    razionalizzare,    facilitare    e    potenziare   l'attività    &#13;
 d'accertamento;  disposizioni  per  la rivalutazione obbligatoria dei    &#13;
 beni  immobili  delle imprese, nonché per riformare il contenzioso e    &#13;
 per  la definizione agevolata dei rapporti tributari pendenti; delega    &#13;
 al  Presidente  della  Repubblica  per la concessione di amnistia per    &#13;
 reati  tributari;  istituzione dei centri di assistenza fiscale e del    &#13;
 conto   fiscale),   sollevata,   in   riferimento   all'art. 3  della    &#13;
 Costituzione, dalla commissione tributaria provinciale di Foggia, con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 25 maggio 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                          Il redattore: Vari                              &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 1° giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
