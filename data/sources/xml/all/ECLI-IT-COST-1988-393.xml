<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>393</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:393</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/03/1988</data_decisione>
    <data_deposito>31/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 42 del r.d. 16    &#13;
 luglio 1905, n. 646 (Approvazione del testo  unico  delle  leggi  sul    &#13;
 credito fondiario), promosso con ordinanza emessa il 4 marzo 1986 dal    &#13;
 Tribunale di Alessandria, iscritta al n. 487 del  registro  ordinanze    &#13;
 1986  e  pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n.    &#13;
 39/prima serie speciale dell'anno 1986;                                  &#13;
    Visti  l'atto  di  costituzione dell'Istituto Bancario S. Paolo di    &#13;
 Torino nonché l'atto di intervento del Presidente del Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 24 febbraio 1988 il giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto che con ordinanza emessa il 4 marzo 1986 nel procedimento    &#13;
 civile di opposizione all'esecuzione in corso tra il fallimento della    &#13;
 s.n.c.  Cantele  &amp; C. di Cantele Carlo e l'Istituto Bancario S. Paolo    &#13;
 di Torino ed altri, il  Tribunale  di  Alessandria  ha  sollevato  la    &#13;
 questione  di  legittimità  costituzionale  dell'art. 42 del r.d. 16    &#13;
 luglio 1905, n. 646, in relazione  all'art.  3,  primo  comma,  della    &#13;
 Costituzione;                                                            &#13;
      che,  ad  avviso  del  giudice  a quo, la norma impugnata, nella    &#13;
 parte  in  cui  consente  agli  istituti  di  credito  fondiario   di    &#13;
 proseguire nella azione esecutiva nonostante l'avvenuta dichiarazione    &#13;
 di fallimento del debitore, violerebbe il principio  di  eguaglianza,    &#13;
 di cui al cit. art. 3 Cost., perché sottrarrebbe ingiustificatamente    &#13;
 l'istituto  di  credito  fondiario  procedente   in   via   esecutiva    &#13;
 all'applicazione dell'art. 52 della legge fallimentare;                  &#13;
      che  il  giudice  a  quo, pur essendo a conoscenza che la stessa    &#13;
 questione è stata dichiarata non fondata con  sentenza  n.  211  del    &#13;
 1976,  chiede  che  la Corte la riesamini insistendo, in particolare,    &#13;
 sull'irragionevolezza del  trattamento  privilegiato  riservato  agli    &#13;
 istituti  di  credito  fondiario  di  fronte ad un fatto eccezionale,    &#13;
 quale il fallimento;                                                     &#13;
      che  avanti  la  Corte  si  è costituito l'Istituto Bancario S.    &#13;
 Paolo di Torino, chiedendo che la questione sia dichiarata infondata;    &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 analogamente chiedendo che la questione, sulla base della sentenza n.    &#13;
 211 del 1976, sia dichiarata manifestamente infondata;                   &#13;
    Considerato  che l'argomentazione della ordinanza introduttiva del    &#13;
 presente  giudizio,  sostanzialmente  ripetitiva   di   quella   già    &#13;
 esaminata  con la predetta sentenza, non è affatto idonea ad indurre    &#13;
 la Corte a discostarsi dalla precedente pronuncia, in  cui  è  stato    &#13;
 posto  in  luce come la destinazione delle disponibilità finanziarie    &#13;
 degli istituti in questione all'esigenza primaria  di  assicurare  il    &#13;
 buon  funzionamento  del credito fondiario giustifichi gli strumenti,    &#13;
 eventualmente speciali, di sollecita  e  sicura  soddisfazione  delle    &#13;
 loro posizioni creditorie;                                               &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953,  n. 87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 42 r.d. 16 luglio 1905, n. 646 sollevata, in    &#13;
 riferimento   all'art.  3,  primo  comma,  Cost.,  dal  Tribunale  di    &#13;
 Alessandria con l'ordinanza indicata in epigrafe.                        &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 marzo 1988.          &#13;
                     Il Presidente: SAJA                                  &#13;
                     Il Redattore: SAJA                                   &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 31 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
