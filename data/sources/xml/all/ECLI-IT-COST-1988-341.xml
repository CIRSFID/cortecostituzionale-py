<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>341</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:341</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1988</data_decisione>
    <data_deposito>24/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di  legittimità  costituzionale dell'art. 30,    &#13;
 terzo comma, della legge della Provincia di Bolzano 21 agosto 1978 n.    &#13;
 46  (Provvedimenti concernenti gli invalidi civili, i ciechi civili e    &#13;
 i sordomuti), come modificato dall'art. 2 (recte: art. 1) della legge    &#13;
 della  Provincia  di  Bolzano 1° agosto 1980, n. 29, promossi con due    &#13;
 ordinanze emesse dal Pretore di Bolzano il 12 febbraio 1982 e  il  18    &#13;
 dicembre 1985, iscritte al n. 281 del registro ordinanze 1982 e al n.    &#13;
 186 del registro ordinanze 1986 e pubblicate nella Gazzetta Ufficiale    &#13;
 della  Repubblica  n.  269 dell'anno 1982 e n. 28, 1ª serie speciale,    &#13;
 dell'anno 1986;                                                          &#13;
    Visti  gli  atti  di  costituzione  di Mattana Angelo ed altri, di    &#13;
 Bravi Goliardo e della Provincia Autonoma di Bolzano;                    &#13;
    Udito  nella  camera  di  consiglio  del  25  novembre  il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto   che,  nel  procedimento  promosso  da  Mattana  Angelo,    &#13;
 Antonio, Rosa e Maria Lucia contro la Provincia Autonoma di  Bolzano,    &#13;
 avente   ad   oggetto   il  pagamento  dei  ratei  di  indennità  di    &#13;
 accompagnamento  richiesta  dalla  loro  de  cuius  Maria  Lazzarotto    &#13;
 Mattana  e  negata  dalla  Giunta provinciale per non essere stata la    &#13;
 defunta sottoposta al prescritto accertamento sanitario,  il  Pretore    &#13;
 di  Bolzano  ha  sollevato  questione  di legittimità costituzionale    &#13;
 dell'art. 30, terzo comma, della legge della Provincia di Bolzano  1°    &#13;
 agosto  1980  n.  29  (recte:  dell'art. 30, terzo comma, della legge    &#13;
 provinciale  di  Bolzano  21  agosto  1978  n.  46,  come  modificato    &#13;
 dall'art. 1 della legge della stessa provincia 1° agosto 1980 n. 29),    &#13;
 in riferimento all'art. 3 Cost., per la disparità di trattamento che    &#13;
 esso  determina  tra  gli  eredi  del  minorato  che  ha  ottenuto il    &#13;
 riconoscimento di tale sua condizione prima della morte e quelli  del    &#13;
 minorato che non lo ha ottenuto;                                         &#13;
      che  i  ricorrenti,  costituitisi  nel giudizio, hanno insistito    &#13;
 sulla fondatezza della questione, dipendendo, a  loro  avviso,  detta    &#13;
 discriminazione da una circostanza meramente casuale, non sorretta da    &#13;
 alcuna giustificazione ragionevole, quale è  la  maggiore  o  minore    &#13;
 tempestività dell'accertamento medico;                                  &#13;
      che  la  Provincia  Autonoma  ha  eccepito  l'irrilevanza  della    &#13;
 questione   in   quanto    la    norma    richiede    l'effettuazione    &#13;
 dell'accertamento  sanitario,  non avvenuto nella specie per la morte    &#13;
 dell'interessata  verificatasi  poco  dopo  la  presentazione   della    &#13;
 domanda;  e  nel  merito  ha sostenuto l'infondatezza della questione    &#13;
 stessa per difetto di omogeneità delle situazioni poste a  confronto    &#13;
 e  perché  la stessa legge provinciale n. 29 del 1980 pone, all'art.    &#13;
 2,  secondo  comma,  un  termine  ragionevole   di   sei   mesi   per    &#13;
 l'espletamento  dell'accertamento  tecnico,  secondo  una valutazione    &#13;
 discrezionale rimessa al legislatore;                                    &#13;
      che  nel  procedimento  proposto  da  Bravi Goliardo, Giovanni e    &#13;
 Luisa, quali eredi di Prearo Annamaria  Bravi,  contro  la  Provincia    &#13;
 Autonoma  di Bolzano, a seguito del rigetto della domanda proposta in    &#13;
 sede amministrativa,  per  ottenere  il  pagamento  dei  ratei  della    &#13;
 pensione  o  dell'assegno di invalidità spettante alla loro de cuius    &#13;
 dal 1° gennaio 1982 al 30 aprile 1983, lo  stesso  Pretore  (R.O.  n.    &#13;
 186/86)   ha   sollevato  questione  di  legittimità  costituzionale    &#13;
 dell'art. 30, terzo comma, della legge provinciale n.  46  del  1978,    &#13;
 come  modificato  dall'art. 2 (recte: art. 1) della legge provinciale    &#13;
 n. 29 del 1980 in riferimento all'art. 24 Cost., in quanto  la  norma    &#13;
 censurata non consente agli eredi della persona asseritamente inabile    &#13;
 la possibilità di proseguire l'azione per il riconoscimento del loro    &#13;
 diritto  qualora  la  morte  di  detta  persona  intervenga  dopo  la    &#13;
 decisione negativa della Commissione sanitaria  provinciale  e  prima    &#13;
 del definitivo riconoscimento dello stato di invalidità;                &#13;
      che  i  ricorrenti,  costituitisi  nel giudizio, hanno insistito    &#13;
 sulla fondatezza della questione rilevando che nella  specie  vi  era    &#13;
 già   stato   l'accertamento  medico-legale,  che  però  non  aveva    &#13;
 riconosciuto la sussistenza di una malattia di grado invalidante;        &#13;
      che  la  Provincia  Autonoma  di Bolzano ha, anzitutto, eccepito    &#13;
 l'irrilevanza della questione in quanto la signora Prearo  Bravi  era    &#13;
 deceduta  dopo  che  l'accertamento  medico-legale  aveva avuto esito    &#13;
 negativo; e, nel  merito,  la  infondatezza  della  stessa  questione    &#13;
 essendo già intervenuto l'accertamento stesso con tale esito;           &#13;
    Considerato che le due questioni, siccome connesse, possono essere    &#13;
 riunite e decise con un'unica ordinanza;                                 &#13;
      che  non sussiste la violazione dell'art. 3 Cost. denunciata con    &#13;
 l'ordinanza  del  12  febbraio  1982  (R.O.   n.   281/82),   essendo    &#13;
 nettamente diverse le situazioni poste a raffronto;                      &#13;
      che   non   sussiste   nemmeno   la   violazione   del  precetto    &#13;
 costituzionale (art. 24 Cost.) denunciata con  la  seconda  ordinanza    &#13;
 (R.O.  n.  186/86)  in quanto la stessa norma impugnata consente agli    &#13;
 eredi di ottenere l'accertamento degli  ulteriori  requisiti  per  la    &#13;
 concessione  della pensione anche dopo la morte dell'inabile ed anche    &#13;
 dopo che l'accertamento medico-legale  abbia  avuto  esito  positivo;    &#13;
 mentre,  nel  caso  in  cui l'accertamento abbia avuto esito negativo    &#13;
 occorre,  prima  dell'inizio  dell'azione  giudiziaria,  proporre  il    &#13;
 ricorso  amministrativo  ai  sensi dell'art. 443 cod. proc. civ. e la    &#13;
 sua proposizione sospende il giudizio;  sicché  in  tale  situazione    &#13;
 deve  ritenersi  assicurata  la tutela giudiziale del preteso diritto    &#13;
 fatto valere;                                                            &#13;
      che,   pertanto,  le  questioni  sollevate  sono  manifestamente    &#13;
 infondate;                                                               &#13;
    Visti gli artt. 26, secondo comma, legge 11 marzo 1953, n. 87 e 9,    &#13;
 secondo comma, delle Norme integrative per  i  giudizi  davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riunisce i giudizi e dichiara manifestamente infondate le questioni    &#13;
 di legittimità costituzionale dell'art. 30, terzo comma, della legge    &#13;
 della  Provincia  Autonoma  di  Bolzano  21  agosto  1978  n. 46 come    &#13;
 modificato dall'art. 2 (recte:  art.  1)  della  legge  della  stessa    &#13;
 provincia  1° agosto 1980 n. 29, sollevate dal Pretore di Bolzano, in    &#13;
 riferimento agli artt. 3 e 24 Cost., con le ordinanze in epigrafe.       &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte Costituzionale, Palazzo della Consulta, l'11 marzo 1988.           &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 24 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
