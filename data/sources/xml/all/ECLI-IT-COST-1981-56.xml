<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>56</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:56</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/03/1981</data_decisione>
    <data_deposito>07/04/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULTO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi riuniti di legittimità costituzionale della legge  18  &#13;
 dicembre 1973, n. 877  (Nuove  norme  per  la  tutela  del    lavoro  a  &#13;
 domicilio), promossi con ordinanze emesse dalla  Corte di cassazione il  &#13;
 14  luglio  1977,  dal  Pretore di   Sansepolcro il 20 febbraio e il 13  &#13;
 marzo 1978, dalla  Corte di cassazione il 4 maggio 1978 e  dai  Pretori  &#13;
 di:   Arezzo il 20 marzo 1978, Pistoia il 30 ottobre 1978,  Poppi il 15  &#13;
 novembre 1978, Varallo il 6 novembre 1978,  Pistoia il 21  marzo  1979,  &#13;
 Pieve  di Cadore il 21 luglio  1979, Treviglio il 20 giugno 1979, Pieve  &#13;
 di Cadore il 19  settembre 1979, Monsummano Terme il 5 novembre   1979,  &#13;
 Pistoia  il  7  novembre  1979,  Monsummano  Terme   il 5 ottobre 1979,  &#13;
 Isernia il 12 dicembre 1979,  rispettivamente iscritte al  n.  551  del  &#13;
 registro  ordinanze    1977,  ai  nn.  254, 255, 443, 667, 674, 678 del  &#13;
 registro ordinanze 1978, ai nn.  292,  438,  650,  788,  852,  961  del  &#13;
 registro  ordinanze  1979  ed ai nn. 5, 53 e 79 del registro  ordinanze  &#13;
 1980 e pubblicate nella Gazzetta Ufficiale   della Repubblica  nn.  39,  &#13;
 222  e 347 del 1978, nn. 59, 168,  210 e 310 del 1979 e nn. 8, 22, 57 e  &#13;
 71 del 1980.                                                             &#13;
     Visti l'atto di costituzione di  Soldini  Gustavo  e  gli  atti  di  &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
     udito  nell'udienza  pubblica  del  15  ottobre  1980  il   Giudice  &#13;
 relatore Livio Paladin;                                                  &#13;
     uditi l'avvocato Aldo Aranguren per Soldini  Gustavo  e  l'avvocato  &#13;
 dello  Stato  Giorgio  Azzariti,  per il Presidente   del Consiglio dei  &#13;
 ministri.                                                                &#13;
     Ritenuto che le sedici ordinanze indicate in epigrafe  hanno  tutte  &#13;
 impugnato  - nei medesimi termini - la legge  18 dicembre 1973, n.  877  &#13;
 ("Nuove norme per la tutela   del lavoro  a  domicilio"),  per  pretesa  &#13;
 violazione  degli  artt.    70,  72  e 73 Cost.; che nelle ordinanze in  &#13;
 questione si   contesta la corrispondenza fra  il  testo  dell'art.  1,  &#13;
 primo    comma,  della  legge predetta, quale era stato approvato dalla  &#13;
 Camera dei deputati, e quello successivamente  approvato dal Senato  (e  &#13;
 promulgato  dal  Presidente  della  Repubblica):  data  la sostituzione  &#13;
 della particella "e" alla particella "o", operata nella parte    finale  &#13;
 dell'espressione "utilizzando materie prime o accessorie e attrezzature  &#13;
 proprie o dello stesso  imprenditore", senza però che sul punto vi sia  &#13;
 stata  una   nuova deliberazione della Camera; e che di conseguenza  si  &#13;
 desume  -  da  tutti  i  giudici  a  quibus  -   l'illegittimità   del  &#13;
 procedimento  legislativo  di approvazione e  promulgazione della legge  &#13;
 in esame;                                                                &#13;
     ritenuto che in tutti i giudizi è intervenuto il Presidente    del  &#13;
 Consiglio  dei  ministri, chiedendo che la Corte  dichiari infondata la  &#13;
 proposta questione, in quanto  all'indiscutibile diversità formale dei  &#13;
 due testi - quello   approvato dalla  Camera  e  quello  approvato  dal  &#13;
 Senato  -    non  corrisponderebbe  una  diversità sostanziale; che in  &#13;
 entrambe  le  Camere   -   stando   ai   lavori   preparatori   addotti  &#13;
 dall'Avvocatura dello Stato il problema della definizione  del lavoro a  &#13;
 domicilio  sarebbe  stato  affrontato  da un   medesimo angolo visuale,  &#13;
 nonostante la ricordata   sostituzione della  disgiuntiva  "o"  con  la  &#13;
 congiuntiva  "e";    che lo stesso Senato avrebbe infatti operato nella  &#13;
 convinzione che l'art. 1  "contenesse  la  medesima  disposizione  già  &#13;
 approvata dalla Camera dei deputati"; e  che la formulazione definitiva  &#13;
 dell'art.  1,  primo comma,  ben potrebbe significare che vi è "lavoro  &#13;
 subordinato  a    domicilio  se  sia  utilizzato  macchinario   (ovvero  &#13;
 materiale)  del  lavoratore  ed  anche  se sia utilizzato   macchinario  &#13;
 dell'imprenditore"; sicché  spetterebbe  alla    Corte  far  prevalere  &#13;
 "sulla constatazione meramente  formale della insignificante diversità  &#13;
 del  testo legislativo  approvato" - per effetto di un errore materiale  &#13;
 - "la  considerazione sostanziale della identità della norma    voluta  &#13;
 ed approvata dai due rami del Parlamento";                               &#13;
     ritenuto, altresì, che nel primo dei due giudizi pendenti  dinanzi  &#13;
 al  Pretore  di  Sansepolcro  (reg. ord. n. 254/1978),   è intervenuto  &#13;
 l'imputato  Gustavo  Soldini,  chiedendo  per    contro  l'accoglimento  &#13;
 dell'impugnativa  promossa  dal    giudice  a  quo;  che  nell'atto  di  &#13;
 intervento (e in una  successiva memoria) si nega che la  promulgazione  &#13;
 abbia  potuto sanare il vizio in esame ed anzi si osserva che il  vizio  &#13;
 stesso  avrebbe effetti tanto più gravi, in quanto la  definizione del  &#13;
 lavoro a domicilio  costituirebbe  la  "chiave  di  volta"  dell'intera  &#13;
 legge n. 877 del 1973.                                                   &#13;
     Considerato  che  nei  procedimenti  - sia penali sia civili -  nel  &#13;
 corso dei quali è stata messa in dubbio la legittimità costituzionale  &#13;
 della legge 18 dicembre 1973, n. 877, si  tratta anzitutto di applicare  &#13;
 l'art. 1, primo comma,  della    legge  stessa,  per  stabilire  se  le  &#13;
 controversie   in  esame    vadano  o  meno  assoggettate  all'apposita  &#13;
 disciplina del  lavoro a domicilio: sicché la  proposta  questione  si  &#13;
 appalesa  con  certezza rilevante - alla data di emissione  delle varie  &#13;
 ordinanze di rinvio -  limitatamente  a  questa  sola  parte  dell'atto  &#13;
 legislativo impugnato; che, d'altro  canto, il vizio denunciato sarebbe  &#13;
 precisamente    imputabile alla discordanza del testo dell'art. 1, già  &#13;
 approvato dalla Camera dei deputati, rispetto al testo   approvato  dal  &#13;
 Senato e quindi promulgato dal Presidente  della Repubblica; che, nella  &#13;
 prospettazione  delle  ordinanze  di rinvio e degli atti di intervento,  &#13;
 tale    discordanza  andrebbe  attribuita  ad  un   errore   materiale,  &#13;
 verificatosi  nella  trascrizione  dell'art. 1, dopo  l'approvazione da  &#13;
 parte  della  Camera  dei  deputati  e  prima    della   corrispondente  &#13;
 approvazione da parte dell'altro  ramo del Parlamento;                   &#13;
     considerato, però, che nel corso del presente giudizio è  entrata  &#13;
 in   vigore   la   legge   16  dicembre  1980,  n.  858     (intitolata  &#13;
 "Interpretazione autentica e modificazione  dell'art. 1 della legge  18  &#13;
 dicembre  1973, n. 877, recante  nuove norme per la tutela del lavoro a  &#13;
 domicilio"):  l'art.  1 della quale ha riaffermato - con effetto "dalla  &#13;
 data di  entrata in vigore della precedente legge 18 dicembre  1973, n.  &#13;
 877", secondo l'espressa disposizione dell'art. 3 - che "è  lavoratore  &#13;
 a domicilio chiunque, con vincolo di subordinazione, esegue nel proprio  &#13;
 domicilio o in locale di cui abbia disponibilità ... lavoro retribuito  &#13;
 per  conto  di  uno  o  più  imprenditori, utilizzando materie prime o  &#13;
 accessorie e attrezzature proprie e dello   stesso imprenditore,  anche  &#13;
 se fornite per il tramite di terzi"  (mentre l'art. 2 reinserisce nella  &#13;
 parte  finale della definizione del lavoro a domicilio - ma con effetto  &#13;
 per il solo avvenire -  la disgiuntiva "o" in luogo  della  congiuntiva  &#13;
 "e");  e  che,  pertanto,  si  rende  necessario restituire gli atti ai  &#13;
 giudici a quibus, affinché accertino se  la  sollevata  questione  sia  &#13;
 tuttora rilevante.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la restituzione degli atti alla terza sezione penale  della  &#13;
 Corte di cassazione ed ai  pretori  di  Sansepolcro,  di    Arezzo,  di  &#13;
 Pistoia,  di  Varallo,  di Poppi, di Treviglio, di  Pieve di Cadore, di  &#13;
 Monsummano Terme, di Isernia.                                            &#13;
     Così deciso in Roma,  nella  sede  della  Corte    costituzionale,  &#13;
 Palazzo della Consulta, il 25 marzo 1981.                                &#13;
     F.to:  LEONETTO  AMADEI  -  GIULIO  GIONFRIDA  - EDOARDO VOLTERRA -  &#13;
 MICHELE ROSSANO -   ANTONINO DE STEFANO -  LEOPOLDO  ELIA  -  GUGLIELMO  &#13;
 ROEHRSSEN  -  ORONZO  REALE  -  BRUNETTO  BUCCIARELLI  DUCCI  - ALBERTO  &#13;
 MALAGUGINI - LIVIO PALADIN - ARNALDO MACCARONE - ANTONIO LA  PERGOLA  -  &#13;
 VIRGILIO ANDRIOLI.                                                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
