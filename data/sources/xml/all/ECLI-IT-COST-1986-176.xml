<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>176</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:176</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/06/1986</data_decisione>
    <data_deposito>07/07/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI &#13;
 - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO Dott. FRANCESCO GRECO - Prof. GABRIELE &#13;
 PESCATORE - Avv. UGO SPAGNOLI - Prof. FRANCESCO PAOLO CASAVOLA, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  11,  comma  &#13;
 primo,  della  legge  15  luglio  1966  n. 604 (Norme sui licenziamenti  &#13;
 individuali), promosso con ordinanza emessa  il  27  marzo  1984  dalla  &#13;
 Corte  di  Cassazione  sul  ricorso  proposto  da  Nalin  Adolfo contro  &#13;
 Insirilli Angelo, iscritta al n. 538  del  registro  ordinanze  1985  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n.  297  bis  &#13;
 dell'anno 1985;                                                          &#13;
     udito nella camera di consiglio  del  23  aprile  1986  il  Giudice  &#13;
 relatore Francesco Greco.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  lettera  raccomandata  dell'1  agosto  1981  il  Centro  Studi  &#13;
 "Aleardo Aleardi"  di  Verona  comunicava  il  licenziamento  al  Prof.  &#13;
 Insirilli  Angelo  e successivamente, su richiesta dell'interessato, ne  &#13;
 indicava, per iscritto, il motivo nell'avvenuto compimento, da parte di  &#13;
 quest'ultimo, del sessantacinquesimo anno di età.                       &#13;
     Impugnato giudizialmente il licenziamento, l'Insirilli ne  otteneva  &#13;
 l'annullamento  ad  opera del Pretore, che lo riteneva ingiustificato e  &#13;
 conseguentemente condannava il suddetto centro studi a  reintegrare  il  &#13;
 lavoratore  nel  posto  di  lavoro  ed  a  risarcirgli il danno. Queste  &#13;
 statuizioni venivano confermate dal Tribunale di Verona,  in  grado  di  &#13;
 appello.                                                                 &#13;
     Nel   susseguente   giudizio  di  Cassazione,  l'adita  Corte,  con  &#13;
 ordinanza emessa il 27  marzo  1984  (R.O.  n.  538/85),  sollevava  la  &#13;
 questione  di  legittimità costituzionale dell'art.   11, primo comma,  &#13;
 della legge 15 luglio 1966 n. 604, nel testo emendato con  la  sentenza  &#13;
 di  questa  Corte  14  luglio  1971  n.  174  e  nella parte in cui, in  &#13;
 contrasto con gli artt. 3, 4 e 38  Cost.,  esclude,  nei  riguardi  dei  &#13;
 prestatori  di  lavoro  ultrasessantacinquenni,  non  pensionati né in  &#13;
 possesso dei requisiti di legge per  avere  diritto  alla  pensione  di  &#13;
 vecchiaia,  l'applicabilità  del divieto di licenziamento senza giusta  &#13;
 causa o giustificato motivo.                                             &#13;
     Osservava in particolare che:                                        &#13;
     a) contrariamente a quanto opinato dal giudice di appello,  con  la  &#13;
 menzionata  sentenza n. 174/71 questa Corte non ha esteso alla suddetta  &#13;
 categoria  di  lavoratori   anziani   il   divieto   di   licenziamento  &#13;
 ingiustificato,  ma  soltanto  le  garanzie  di cui agli artt. 2 (forma  &#13;
 scritta del licenziamento e comunicazione, se richiesta, dei motivi)  e  &#13;
 5  (assoggettamento  del  datore  di lavoro all'onere della prova della  &#13;
 giusta causa o del giustificato motivo) della legge n. 604/66:  e  ciò  &#13;
 al  fine  di  una  più  puntuale  tutela  di  tali lavoratori rispetto  &#13;
 all'eventualità di licenziamenti  determinati  da  motivi  illeciti  e  &#13;
 colpiti  da  nullità  ai  sensi  dell'art.  4  della  medesima  legge,  &#13;
 applicabile, a differenza del cennato  divieto  di  cui  ai  precedenti  &#13;
 artt. 1 e 3, anche a detti lavoratori;                                   &#13;
     b)  l'effetto  di  questa  parziale  declaratoria di illegittimità  &#13;
 costituzionale implica, dunque, soltanto che, da un lato, il lavoratore  &#13;
 ultrasessantacinquenne  ha  diritto  alla  osservanza  della  ricordata  &#13;
 disciplina  formale  del  licenziamento  e, dall'altro lato, in caso di  &#13;
 impugnazione giudiziale di questo,  proposta  per  asserita  violazione  &#13;
 dell'art. 4 della legge n. 604 del 1966, il datore di lavoro è onerato  &#13;
 della  prova  indiretta  contraria  e  cioè  della dimostrazione della  &#13;
 effettiva sussistenza della giusta  causa  o  del  giustificato  motivo  &#13;
 eventualmente  addotto:  fermo restando, poi, che in caso di fallimento  &#13;
 di tale Frova non sarebbe dato ricavarne de iure la  conseguenza  della  &#13;
 sussistenza  di un motivo illecito ex art. 4 cit., la cui dimostrazione  &#13;
 è onere, invero e pur sempre,  del  lavoratore,  secondo  i  princìpi  &#13;
 generali,  non  diversamente  dal  caso  in  cui  il  datore  di lavoro  &#13;
 nessun'altra  giustificazione  adduca  se  non   quella   dell'avvenuto  &#13;
 superamento del sessantacinquesimo anno;                                 &#13;
     c)  di qui la rilevanza della proposta questione: nella fattispecie  &#13;
 l'impugnazione del licenziamento, concernente  il  solo  aspetto  della  &#13;
 carenza  di  giusta  causa  o  di  giustificato motivo, dovrebbe essere  &#13;
 respinta alla stregua degli esposti princìpi, pacifico essendo, per il  &#13;
 resto, che l'impugnante aveva superato l'età limite per l'applicazione  &#13;
 delle garanzie di cui agli artt. 1 e 3 della legge n. 604/66,  che  era  &#13;
 impiegato   alle  dipendenze  di  un  datore  di  lavoro  con  più  di  &#13;
 trentacinque dipendenti e che non vi era questione di nullità ex  art.  &#13;
 4  stessa  legge:  solo l'accoglimento della pregiudiziale questione di  &#13;
 costituzionalità potrebbe, perciò, condurre ad un diverso esito della  &#13;
 lite;                                                                    &#13;
     d) la non manifesta infondatezza della questione discende in  primo  &#13;
 luogo  dal  rilievo  che la norma censurata crea, soltanto alla stregua  &#13;
 dell'età  e,  pertanto,  irragionevolmente  una  discriminazione   fra  &#13;
 lavoratori  che  versano nella comune condizione di carenza del diritto  &#13;
 al trattamento pensionistico di vecchiaia: soltanto per taluni di essi,  &#13;
 invero, ed a cagione del solo superamento del sessantacinquesimo  anno,  &#13;
 resta  esclusa  la  applicabilità  del  divieto di licenziamento senza  &#13;
 giusta causa o giustificato  motivo,  accompagnandosi  poi  a  siffatta  &#13;
 violazione  del  principio  di  uguaglianza anche quella del diritto al  &#13;
 lavoro sancito nell'art. 4 Cost., come ineluttabile  conseguenza  della  &#13;
 carenza  di  tutela della stabilità del posto di lavoro, implicata dal  &#13;
 superamento suddetto;                                                    &#13;
     e)   mentre   l'esclusione  di  questa  tutela  nei  confronti  dei  &#13;
 lavoratori aventi diritto alla pensione di vecchiaia  risponde  ad  una  &#13;
 ragionevole  scelta  del  legislatore,  poiché riguarda coloro che, in  &#13;
 caso di  licenziamento,  non  rimangono  "senza  retribuzione  e  senza  &#13;
 trattamento  di  quiescenza"  (sent.  n. 174/ 71 cit.), non altrettanto  &#13;
 può dirsi per i lavoratori ultrasessantacinquenni  privi  di  identico  &#13;
 diritto,  poiché  qui  è  proprio  il difetto di tale presupposto che  &#13;
 induce a negare la ragionevolezza  della  estensione,  anche  a  questi  &#13;
 ultimi, della medesima esclusione;                                       &#13;
     f)  questa  stessa  Corte,  inoltre,  ha  rilevato  che la semplice  &#13;
 maggior probabilità che tali lavoratori siano  inidonei  al  lavoro  a  &#13;
 causa dell'età avanzata non può essere assunta a valida e sufficiente  &#13;
 giustificazione di un trattamento differenziato in loro pregiudizio. Ed  &#13;
 a  tale  considerazione va aggiunta l'altra che detta probabilità, ove  &#13;
 concretamente avveratasi, non precluderebbe al  datore  di  lavoro,  in  &#13;
 ipotesi tenuto all'osservanza del divieto de quo anche nei confronti di  &#13;
 lavoratori ultrasessantacinquenni, la possibilità di avviare opportuni  &#13;
 accertamenti  al  riguardo,  nelle  forme  dell'art.  5  della legge n.  &#13;
 300/70, al fine di comminare un licenziamento per giustificato motivo;   &#13;
     g)  infine,  l'avere  accomunato  nell'identica  esclusione   della  &#13;
 operatività   del  divieto  di  licenziamento  senza  giusta  causa  o  &#13;
 giustificato motivo sia i lavoratori con diritto a pensione che  quelli  &#13;
 ultrasessantacinquenni   privi   di  tale  diritto  comporta  ulteriori  &#13;
 discriminazioni  in  danno  dei  secondi  perché  comporta  per   loro  &#13;
 l'impossibilità  di  conseguire il suddetto trattamento di quiescenza,  &#13;
 con la correlata violazione anche dell'art. 38 Cost..                    &#13;
     L'ordinanza,  ritualmente  notificata  e   comunicata,   è   stata  &#13;
 pubblicata con la Gazzetta Ufficiale del 18 dicembre 1985 n. 297 bis.    &#13;
     Nessuno si è costituito.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.   -   La   Corte   di   Cassazione   dubita  della  legittimità  &#13;
 costituzionale dell'art. 11, primo comma, della legge 15 luglio 1966 n.  &#13;
 604, nel testo emendato con la sentenza di questa Corte del  15  luglio  &#13;
 1971  n.  174, nella parte in cui esclude l'applicazione del divieto di  &#13;
 licenziamento senza giusta causa o giustificato motivo,  ai  lavoratori  &#13;
 che  hanno  compiuto  il  sessantacinquesimo  anno  di età e non siano  &#13;
 pensionati o non in possesso dei requisiti di legge per  avere  diritto  &#13;
 alla pensione di vecchiaia.                                              &#13;
     Ha  osservato che, con la detta sentenza n. 174/71, questa Corte ha  &#13;
 esteso a tali lavoratori solo le garanzie di cui agli artt. 2 (cioè la  &#13;
 necessità della forma scritta del licenziamento e la comunicazione, se  &#13;
 richiesta, dei motivi) e 5 (onere della prova della giusta causa o  del  &#13;
 giustificato  motivo  a carico del datore di lavoro) della citata legge  &#13;
 n.  604/66, al fine di una loro più puntuale tutela,  nell'ipotesi  di  &#13;
 licenziamenti  determinati  da  motivi  illeciti  o da ragioni di credo  &#13;
 politico o fede religiosa, anziché il divieto  più  generale  di  cui  &#13;
 agli artt. 1 (necessità di giusta causa a fondamento del licenziamento  &#13;
 del  prestatore  di  lavoro  ai  sensi dell'art. 2119 cod. civ. o di un  &#13;
 giustificato  motivo)  e  3  (sussistenza  di  un  giustificato  motivo  &#13;
 determinato  da  un  notevole inadempimento degli obblighi contrattuali  &#13;
 del prestatore di  lavoro  ovvero  da  ragioni  inerenti  all'attività  &#13;
 produttiva,  all'organizzazione  del lavoro o al regolare funzionamento  &#13;
 di essa).                                                                &#13;
     Ed  ha  riferito il sollevato dubbio di legittimità costituzionale  &#13;
 agli artt. 3, 4, 38 Cost. rilevando: a)  che  sussiste  discriminazione  &#13;
 ingiustificata ed irrazionale tra lavoratori ultrasessantacinquenni che  &#13;
 hanno  diritto  alla pensione di vecchiaia e lavoratori che non l'hanno  &#13;
 ancora conseguito e sono perciò soggetti a licenziamento senza  giusta  &#13;
 causa o giustificato motivo solo per avere raggiunto la detta età;      &#13;
     b) che si verifica violazione del diritto al lavoro perché i detti  &#13;
 lavoratori  subiscono la perdita delle garanzie di stabilità del posto  &#13;
 di lavoro solo per la suddetta ragione;                                  &#13;
     c) che la sopravvenuta carenza di  stabilità  si  risolve  in  una  &#13;
 remora  all'acquisizione  del  diritto  al trattamento di quiescenza da  &#13;
 parte di soggetti che ne sono ancora privi.                              &#13;
     La Corte remittente ha aggiunto che la scelta  del  legislatore  è  &#13;
 ragionevole  quando  cade  su coloro che, in caso di licenziamento, non  &#13;
 restano senza retribuzione e senza trattamento di quiescenza  (conforme  &#13;
 sent. Corte Cost. n. 174/71) ma non lo è quando cade su lavoratori che  &#13;
 sono  licenziati solo per avere raggiunto i 65 anni di età, tanto più  &#13;
 che non è precluso al datore di lavoro di fare accertare, nelle  forme  &#13;
 di  cui  all'art.  5  legge  n. 300 del 1970, la sussistenza della loro  &#13;
 inidoneità alla continuazione della prestazione di lavoro, conseguente  &#13;
 al raggiungimento della detta età e concretante una giusta causa o  un  &#13;
 giustificato  motivo di recesso, mentre agli stessi lavoratori dovrebbe  &#13;
 essere garantito, in ogni caso, il  conseguimento  del  trattamento  di  &#13;
 quiescenza.                                                              &#13;
     2. - La questione è fondata.                                        &#13;
     Questa  Corte,  nella sentenza n. 174/71 con la quale ha dichiarato  &#13;
 la parziale illegittimità costituzionale della  norma,  ora  di  nuovo  &#13;
 denunciata,     in    una    fattispecie    relativa    a    lavoratori  &#13;
 ultrasessantacinquenni, licenziati per  motivi  politici,  religiosi  o  &#13;
 sindacali,  senza  la  comunicazione  scritta del licenziamento e senza  &#13;
 l'indicazione dei detti motivi, necessaria ai fini della  contestazione  &#13;
 (artt.  2  e  5  della legge n. 604/66), ha formulato dei princìpi che  &#13;
 fondano anche la decisione  della  questione,  ora  sottoposta  al  suo  &#13;
 esame,  la  quale  concerne  la  più  generale  ipotesi  di lavoratori  &#13;
 licenziati solo per avere raggiunto il sessantacinquesimo anno  d'età,  &#13;
 senza  avere  diritto  a  pensione  o  avere  conseguito  le condizioni  &#13;
 necessarie,  previste  dal  legislatore  per  il  conseguimento   della  &#13;
 pensione  di  vecchiaia  e  l'applicabilità  ad essi degli artt. 1 e 3  &#13;
 della legge n. 604/66.                                                   &#13;
     La Corte ritenne allora, ed ora non ha ragione di non ribadire, che  &#13;
 non potevano essere  considerati  sullo  stesso  piano  lavoratori  che  &#13;
 fossero  in  possesso  dei  requisiti  di  legge per avere diritto alla  &#13;
 pensione di vecchiaia e lavoratori che  avevano  comunque  superato  il  &#13;
 sessantacinquesimo anno d'età senza diritto a pensione e che sarebbero  &#13;
 esposti alla perdita della retribuzione senza trattamento di quiescenza  &#13;
 per vecchiaia; che, oltre a sussistere disparità di trattamento tra le  &#13;
 due  categorie di lavoratori, non risultava attuata in concreto, per la  &#13;
 seconda categoria, la tutela del diritto  al  lavoro  nei  modi  e  nei  &#13;
 limiti costituzionalmente garantiti.                                     &#13;
     Aggiunse  che  non  ricorrevano  particolari ragioni perché a quei  &#13;
 lavoratori venisse negato o non egualmente riconosciuto  il  diritto  a  &#13;
 determinate  garanzie;  che  non  poteva  essere  desunta  a  valida  e  &#13;
 sufficiente ragione del trattamento differenziato la semplice  maggiore  &#13;
 probabilità  che, in quanto anziani, quei lavoratori non si trovassero  &#13;
 nelle  migliori  condizioni  per il normale dispiegamento delle energie  &#13;
 fisiche e psichiche in favore  del  datore  di  lavoro  e  che  questi,  &#13;
 correlativamente, attraverso la loro collaborazione, non conseguisse il  &#13;
 regolare adempimento delle obbligazioni contrattuali e di legge, oppure  &#13;
 il normale apporto all'esercizio dell'impresa.                           &#13;
     3. - Le suddette ragioni trovano oggi un maggior fondamento perché  &#13;
 la  durata  della  vita  media  si è prolungata, mentre l'introduzione  &#13;
 nelle fabbriche e, in genere,  nelle  imprese,  di  macchine  di  varie  &#13;
 specie ha reso il lavoro meno usurante.                                  &#13;
     Sicché  devesi ritenere che non possono essere negate, per il solo  &#13;
 fatto dell'età, quelle cautele e quelle garanzie che sono informate al  &#13;
 rispetto della personalità umana e che costituiscono  altresì  indici  &#13;
 del valore spettante al lavoro nella moderna società.                   &#13;
     Pertanto,      anche      al      lavoratore     anziano     (cioè  &#13;
 ultrasessantacinquenne) va  riconosciuta  la  medesima  tutela  che  è  &#13;
 accordata agli altri lavoratori. Per essi non opera il recesso ad nutum  &#13;
 del datore di lavoro solo per il raggiungimento della detta età, ma il  &#13;
 loro  licenziamento  deve  trovare  ragione in una giusta causa o in un  &#13;
 giustificato motivo, dati gli artt. 1 e 3 della legge n.  604/66.        &#13;
     Inoltre, essi devono ricevere dal datore di lavoro la comunicazione  &#13;
 scritta del licenziamento con l'indicazione dei  motivi,  se  richiesta  &#13;
 entro il termine di legge, mentre l'onere della prova della sussistenza  &#13;
 del  motivo  è  a  carico  del  datore di lavoro (artt. 2 e 5 legge n.  &#13;
 604/66).                                                                 &#13;
     L'inidoneità  fisica,  conseguente  all'età,  che  impedisca   la  &#13;
 continuazione  della prestazione della sua opera a favore del datore di  &#13;
 lavoro in adempimento delle obbligazioni contrattuali o di legge  o  il  &#13;
 normale  apporto  all'esercizio  dell'impresa può concretare un giusto  &#13;
 motivo del licenziamento del lavoratore ma essa non  può  essere  solo  &#13;
 presunta  per  l'età  (di  oltre 65 anni) ma deve essere provata ed il  &#13;
 datore di lavoro può anche farla accertare con i mezzi e le  modalità  &#13;
 di cui all'art. 5 della legge n. 300/70.                                 &#13;
     La  Corte ribadisce anche (sent. n. 174/71) che la norma denunciata  &#13;
 risulta in contrasto con l'art. 4 Cost. ove si consideri che la  tutela  &#13;
 del  diritto  al  lavoro  è  strettamente  connessa all'attuazione del  &#13;
 principio di uguaglianza innanzi affermata.                              &#13;
     E conferma poi l'interpretazione del detto art.  4  Cost.,  secondo  &#13;
 cui  al  cittadino  non  sono  garantiti il diritto al conseguimento di  &#13;
 un'occupazione ed il diritto alla conservazione del posto di lavoro ma,  &#13;
 ove siano previsti i casi, i tempi ed  i  modi  dei  licenziamenti,  la  &#13;
 disciplina,  per  essere  conforme  alla Costituzione deve rispecchiare  &#13;
 l'esigenza di un trattamento giuridico eguale per situazioni eguali ed,  &#13;
 in relazione ad esse, può essere diversificato  solo  in  presenza  di  &#13;
 giustificate ragioni.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 11, primo comma,  &#13;
 della  legge  15 luglio 1966 n. 604 (contenente norme sui licenziamenti  &#13;
 individuali) nella parte in cui esclude l'applicabilità degli artt.  1  &#13;
 e  3 della stessa legge nei riguardi di prestatori di lavoro che, senza  &#13;
 essere pensionati o in  possesso  dei  requisiti  di  legge  per  avere  &#13;
 diritto    alla    pensione   di   vecchiaia,   abbiano   superato   il  &#13;
 sessantacinquesimo anno di età.                                         &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 giugno 1986.         &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - GIOVANNI  CONSO  -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   - GABRIELE PESCATORE - UGO SPAGNOLI -  &#13;
                                   FRANCESCO PAOLO CASAVOLA.              &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
