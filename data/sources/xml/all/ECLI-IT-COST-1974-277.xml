<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>277</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:277</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Angelo de Marco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/12/1974</data_decisione>
    <data_deposito>11/12/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  9,  primo  &#13;
 comma,  del  decreto legislativo luogotenenziale 10 maggio 1916, n. 497  &#13;
 (Semplificazione alla procedura  per  la  liquidazione  delle  pensioni  &#13;
 privilegiate  di  guerra), promosso con ordinanza emessa il 21 febbraio  &#13;
 1972 dalla Corte dei conti - sezione IV pensioni militari - sul ricorso  &#13;
 di Grasso Francesco, iscritta al n. 379 del registro ordinanze  1972  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n. 21 del 24  &#13;
 gennaio 1973.                                                            &#13;
     Visto l'atto di costituzione di Grasso Francesco;                    &#13;
     udito nell'udienza pubblica del 6 novembre 1974 il Giudice relatore  &#13;
 Angelo De Marco;                                                         &#13;
     udito l'avv. Antonio Amitrano, per Grasso Francesco.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con istanza in data 2 aprile 1959, Francesco Grasso - già  soldato  &#13;
 di  leva  della  classe  1926, inviato in congedo illimitato in data 21  &#13;
 settembre 1948 per un'infermità dichiarata dalla competente  autorità  &#13;
 sanitaria  militare  preesistente  alla  chiamata alle armi - asserendo  &#13;
 che, invece, tale infermità era stata contratta in  servizio  militare  &#13;
 ed  a  causa  del  servizio  stesso,  chiedeva che gli venisse concesso  &#13;
 trattamento pensionistico privilegiato ordinario.                        &#13;
     Con decreto 18 dicembre 1959,  il  Ministero  della  difesaesercito  &#13;
 respingeva  la  predetta istanza perché proposta dopo oltre dieci anni  &#13;
 dalla cessazione del servizio e, quindi, colpita dalla prescrizione  di  &#13;
 cui all'art. 2946 del codice civile.                                     &#13;
     Avverso  tale  decreto  il  Grasso proponeva ricorso alla Corte dei  &#13;
 conti, assumendo  che  il  corso  della  eccepita  prescrizione  doveva  &#13;
 ritenersi  interrotto  per effetto dell'art.  25 della legge 9 novembre  &#13;
 1961, n. 1240, e dell'art. 16 della legge 10 agosto 1950, n. 648.        &#13;
     Il   Procuratore   generale   della  Corte  dei  conti,  nelle  sue  &#13;
 conclusioni scritte chiedeva  la  relezione  del  ricorso  per  essersi  &#13;
 verificata la decadenza comminata dall'art. 9, primo comma, del decreto  &#13;
 legislativo  luogotenenziale  10  maggio 1916, numero 497, da ritenersi  &#13;
 pregiudiziale alla eccezione di prescrizione.                            &#13;
     All'udienza di trattazione del ricorso, il patrono  del  ricorrente  &#13;
 sollevava  questione  di  legittimità  costituzionale,  in riferimento  &#13;
 all'art. 3 della Costituzione, di detto art. 9, in relazione agli artt.  &#13;
 88 e 89 della legge 18 marzo 1968, n.  313, essendo  ingiustificata  la  &#13;
 disparità  di  trattamento  di  un  militare infermo rispetto a quello  &#13;
 adottato nei riguardi degli infortunati civili per fatto di  guerra  ed  &#13;
 il P.M. si associava.                                                    &#13;
     La Corte adita (Sez. IV) con ordinanza 21 febbraio 1972 ha ritenuto  &#13;
 la  questione,  come  sopra prospettata, rilevante e non manifestamente  &#13;
 infondata,  sospendendo  il  giudizio  davanti  ad  essa   pendente   e  &#13;
 rimettendo gli atti a questa Corte.                                      &#13;
     Secondo l'ordinanza, la disparità di trattamento tra i richiedenti  &#13;
 pensioni  privilegiate  di  guerra  (o  di  altri  eventi assimilati) e  &#13;
 richiedenti  pensioni  privilegiate  ordinarie   (anche   se   militari  &#13;
 mobilitati  in tempo di guerra, ma non appartenenti a reparti operanti,  &#13;
 pur se al seguito di essi) consiste in questo: mentre per  i  primi  la  &#13;
 "constatazione"   non   deve  comprendere  anche  l'accertamento  della  &#13;
 dipendenza  da  causa  di  servizio,  per  i  secondi,   invece,   deve  &#13;
 comprenderla.                                                            &#13;
     Al  riguardo  si  fa  presente  che nel senso di cui si è detto è  &#13;
 costante la giurisprudenza della  Corte  dei  conti,  anche  a  sezioni  &#13;
 unite,  e che la stessa Corte ha pure costantemente ritenuta infondata,  &#13;
 in quanto l'ha ritenuta giustificata dalle difficoltà che, in tempo di  &#13;
 guerra e nella zona di  operazioni,  incontrerebbe  l'osservanza  delle  &#13;
 norme  riguardanti gli accertamenti della dipendenza dell'infermità da  &#13;
 causa di servizio.                                                       &#13;
     Ma si oppone che questa giustificazione non sembra accettabile e si  &#13;
 risolve in una interpretazione di assai dubbia esattezza del denunziato  &#13;
 art. 9, la cui dizione è tale da  farlo  ritenere  applicabile,  nella  &#13;
 forma  più  ampia  sopra specificata, anche alle pensioni privilegiate  &#13;
 ordinarie.                                                               &#13;
     A conferma di tale tesi si fa, infine, presente che, in  forza  del  &#13;
 richiamo  contenuto nell'art. 1 della legge 17 ottobre 1967, n. 974, ai  &#13;
 congiunti dei militari caduti per causa  di  servizio  o  deceduti  per  &#13;
 infermità contratta o aggravata per causa di servizio si applicano gli  &#13;
 artt.  88 e 89 della legge n.  313 del 1968, che pur riconoscendo fermo  &#13;
 l'onere di chiedere la constatazione da dipendenza da causa di servizio  &#13;
 della infermità che trasse a morte il militare, ne è molto  agevolata  &#13;
 la  dimostrazione  che  può  desumersi  anche  dalla  cartella clinica  &#13;
 redatta in occasione di un di lui ricovero in ospedale militare.         &#13;
     Si è costitutito nel  presente  giudizio  il  Grasso,  il  di  cui  &#13;
 patrocinio,  con  memoria  depositata  il  13  febbraio  1973, oltre ad  &#13;
 insistere sulle considerazioni già  fatte  proprie  dall'ordinanza  di  &#13;
 rinvio,  chiede  che  la  dichiarazione  di  illegittimità,  sempre in  &#13;
 riferimento all'art. 3 della  Costituzione,  venga  estesa  al  secondo  &#13;
 comma  dell'art.  9 del d.l.g. lgt. n. 497 del 1916, in forza del quale  &#13;
 la stessa decadenza di cui al primo comma è preveduta per coloro  che,  &#13;
 avendo riportato una ferita o una lesione o una infermità riconosciuta  &#13;
 durante  l'attività  di  servizio come dipendente dal servizio stesso,  &#13;
 senza che essa dia luogo ad alcun trattamento  di  quiescenza,  non  ne  &#13;
 facciano  nel  termine dei cinque anni constatare l'aggravamento ove si  &#13;
 manifesti.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Anzitutto si deve rilevare che la  questione  di  legittimità  &#13;
 costituzionale  del  secondo  comma dell'art. 9 del decreto legislativo  &#13;
 luogotenenziale 10 maggio 1916, n.   497,  in  riferimento  all'art.  3  &#13;
 della Costituzione, sollevata dal patrocinio della parte privata con la  &#13;
 memoria   di   cui   in  narrativa  è  inammissibile  per  difetto  di  &#13;
 legittimazione a proporla, in quanto  non  compresa  nell'ordinanza  di  &#13;
 rinvio.                                                                  &#13;
     2.  -  A  differenza  di  tutte  le disposizioni che, in materia di  &#13;
 pensioni privilegiate ordinarie, sia civili sia militari, a  cominciare  &#13;
 dal r.d. 21 febbraio 1895, n. 70, fino al t.u.  approvato con d.P.R. 29  &#13;
 dicembre 1973, n. 1092, prescrivono il tempestivo rigoroso accertamento  &#13;
 della  dipendenza  da  causa  di servizio dell'evento dannoso sul quale  &#13;
 poggia la relativa domanda, l'art.9, comma primo, del d.lgt. lo  maggio  &#13;
 1916, n.497, si limita a disporre:  "Chiunque ritenga di aver contratto  &#13;
 una  infermità  a  causa  di  servizio e lasci trascorrere cinque anni  &#13;
 dalla   cessazione   del   servizio   medesimo   senza   chiedere   "la  &#13;
 constatazione" decade dal diritto alla pensione privilegiata".           &#13;
     Con una costante e consolidata giurisprudenza la Corte dei conti ha  &#13;
 interpretato  tale  norma nel senso che per le pensioni privilegiate di  &#13;
 guerra bastasse a legittimare la richiesta del relativo trattamento  la  &#13;
 sola  constatazione  da  parte  dei  competenti  organi  sanitari della  &#13;
 esistenza dell'evento dannoso (ferite, lesioni, infermità) e non anche  &#13;
 la dipendenza da causa di servizio, giustificando la diversa disciplina  &#13;
 delle pensioni privilegiate ordinarie, con la difficoltà che, in tempo  &#13;
 di guerra e nelle zone di operazioni, incontrerebbe l'osservanza  delle  &#13;
 disposizioni  riguardanti gli accertamenti della dipendenza da causa di  &#13;
 servizio.                                                                &#13;
     Con l'ordinanza di rinvio la IV  sezione  della  Corte  dei  conti,  &#13;
 riesaminata  detta  giurisprudenza,  ha  ritenuto  la  sopra  riportata  &#13;
 giustificazione non accettabile e, pertanto, ha proposto  questione  di  &#13;
 legittimità   costituzionale   -   in   riferimento  al  principio  di  &#13;
 eguaglianza - del citato articolo 9 nella parte  in  cui,  interpretato  &#13;
 restrittivamente,   esso   non   trova   applicazione   nelle  pensioni  &#13;
 privilegiate ordinarie.                                                  &#13;
     3. - Così chiaritine i  termini,  la  proposta  questione  risulta  &#13;
 infondata.                                                               &#13;
     Come  più  volte  questa  Corte  ha  avuto occasione di affermare,  &#13;
 perché sussista violazione del principio di eguaglianza occorre che  a  &#13;
 situazioni del tutto identiche, senza alcuna razionale giustificazione,  &#13;
 corrispondano  differenti  discipline, mentre ha riconosciuta legittima  &#13;
 la corrispondenza di tali discipline a situazioni non  identiche  ma  a  &#13;
 loro volta differenziate.                                                &#13;
     Nella   specie   un   criterio  discriminatore  tra  le  situazioni  &#13;
 diversamente disciplinate è quel doloroso evento che è la guerra.      &#13;
     Non a caso il d.lgt. n. 497 del 1916 è stato emanato nel corso  di  &#13;
 una  sanguinosa  guerra durante la quale si ebbero ingentissime perdite  &#13;
 in morti  e  feriti  e  quando  gli  ospedali  militari  nonostante  la  &#13;
 requisizione  anche  di  numerosi  ospedali  civili  e l'adattamento ad  &#13;
 ospedale perfino di scuole e collegi furono appena  sufficienti  a  far  &#13;
 fronte alle  sempre più pressanti esigenze che la situazione imponeva.  &#13;
 Ben lungi dall'apparire inaccettabile e di assai dubbia  esattezza,  la  &#13;
 giustificazione  data a fondamento della giurisprudenza della Corte dei  &#13;
 conti,  che  con  l'ordinanza  di  rimessione  si  vorrebbe  repudiare,  &#13;
 risulta,   quindi,  non  solo  logica  e  razionale,  ma,  soprattutto,  &#13;
 corrispondente ad una saggia ed obiettiva valutazione di situazioni che  &#13;
 è augurabile non abbiano più a verificarsi.                            &#13;
     Mentre tempo e mezzi non erano del tutto sufficienti  a  provvedere  &#13;
 per  la cura di feriti e di malati, sempre in maggior numero affluenti,  &#13;
 non sarebbe stato logico distrarre gli  organi  sanitari  militari  dai  &#13;
 compiti  essenziali  che  tale  situazione  imponeva,  per  attendere a  &#13;
 pratiche burocratiche per giunta  non  sempre  di  facile  e  sollecita  &#13;
 definizione.                                                             &#13;
     Ma  vi  è  di  più: salvo i rarissimi casi di autolesionismo, non  &#13;
 facilmente  occultabili,  è  più  che  giustificata  ed  evidente  la  &#13;
 presunzione che le ferite o infermità per le quali sia stato, in tempo  &#13;
 di  guerra,  ricoverato  in  ospedale chi vi partecipa, dipendano dalla  &#13;
 guerra stessa.                                                           &#13;
     Addirittura sporadici sono, invece, i casi  di  malattie  o  ferite  &#13;
 riportate  in  servizio civile o militare non di guerra da coloro che a  &#13;
 tali servizi siano addetti, la cui dipendenza  da  questa  causa  possa  &#13;
 essere  presunta,  mentre,  per  contro,  è  spesso di assai difficile  &#13;
 accertamento anche a breve distanza dall'evento dannoso.                 &#13;
     Né argomenti in contrario possono essere desunti dagli artt. 88  e  &#13;
 89  della legge 18 marzo 1968, n. 313, come pure dalla legge 17 ottobre  &#13;
 1967, n.  974,  o  da  quella  28  maggio  1973,  n.  296,  perché  si  &#13;
 riferiscono  sempre  a ferite, lesioni o infermità dipendenti da causa  &#13;
 di guerra o da eventi  che  il  legislatore,  nella  sua  insindacabile  &#13;
 discrezionalità,  che,  del  resto  risulta  razionalmente  usata,  ha  &#13;
 ritenuto, per la peculiarità delle vicende sociali e  politiche  nelle  &#13;
 quali si sono verificati, di assimilare alla guerra.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 9 del decreto legislativo luogotenenziale 10 maggio 1916,  n.  &#13;
 497  (Semplificazione alla procedura per la liquidazione delle pensioni  &#13;
 privilegiate di guerra), sollevata, in  riferimento  all'art.  3  della  &#13;
 Costituzione, con l'ordinanza di cui in epigrafe.                        &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 5 dicembre 1974.                              &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - LUIGI OGGIONI - ANGELO DE  MARCO  -  &#13;
                                   ERCOLE  ROCCHETTI  ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  -  EDOARDO  VOLTERRA GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
