<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>120</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:120</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/05/1997</data_decisione>
    <data_deposito>06/05/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, &#13;
 avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero &#13;
 Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale  dell'art.  2,  comma  1,    &#13;
 lettera  a) della legge della regione Lombardia del 17 febbraio 1986,    &#13;
 n.  5  (Disciplina  per  l'autorizzazione  e   la   vigilanza   sulle    &#13;
 istituzioni  sanitarie  di  carattere  privato che svolgono attività    &#13;
 ambulatoriale, nonché per il trasporto  di  infermi),  promosso  con    &#13;
 ordinanza  emessa  il  13  dicembre  1995  dal pretore di Mantova nel    &#13;
 procedimento civile vertente tra Mazzali Alberto ed altri e  USLL  47    &#13;
 di  Mantova  iscritta  al  n.  472  del  registro  ordinanze  1996  e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  22,  prima    &#13;
 serie speciale, dell'anno 1996;                                          &#13;
   Visto l'atto di intervento della regione Lombardia;                    &#13;
   Udito  nella  camera  di  consiglio del 26 febbraio 1997 il giudice    &#13;
 relatore Francesco Guizzi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. -  Il pretore di Mantova, adito  da  Mazzali  Alberto  e  altri,    &#13;
 medici-chirurghi,   avverso   ordinanze-ingiunzioni   che   comminano    &#13;
 sanzioni amministrative per l'attivazione di ambulatori medici  senza    &#13;
 autorizzazione,   ha  sollevato,  in  riferimento  all'art.  3  della    &#13;
 Costituzione, questione di legittimità costituzionale  dell'art.  2,    &#13;
 comma  1, lettera a), della legge della regione Lombardia 17 febbraio    &#13;
 1986, n. 5 (Disciplina per  l'autorizzazione  e  la  vigilanza  sulle    &#13;
 istituzioni  sanitarie  di  carattere  privato che svolgono attività    &#13;
 ambulatoriale, nonché per il trasporto di infermi), nella  parte  in    &#13;
 cui   assimila   situazioni   diverse,   e  cioè  gli  istituti  con    &#13;
 organizzazione propria che presentano le stesse caratteristiche delle    &#13;
 case e degli istituti di cura, e lo studio privato del singolo medico    &#13;
 con targa pubblicitaria e, in ipotesi, con un solo dipendente.           &#13;
   Detta assimilazione sarebbe irragionevole: se scopo della norma  è    &#13;
 infatti  quello di consentire il controllo dell'autorità preposta ai    &#13;
 servizi sanitari gestiti dai privati, la definizione  di  ambulatorio    &#13;
 va  circoscritta  alle  strutture  organizzate  che  siano  dotate di    &#13;
 attrezzature idonee per gli accertamenti diagnostici e gli interventi    &#13;
 terapeutici.  Di qui, la violazione del principio di eguaglianza.        &#13;
   2. - È intervenuto il  presidente  della  regione  Lombardia,  nel    &#13;
 senso   dell'inammissibilità  e,  in  subordine,  dell'infondatezza.    &#13;
 Innanzitutto, perché il rimettente non descrive  le  caratteristiche    &#13;
 degli  studi professionali dei ricorrenti, precludendo in tal modo al    &#13;
 giudice costituzionale il  controllo  della  rilevanza,  per  cui  la    &#13;
 questione  sarebbe  inammissibile  per  insufficiente indicazione dei    &#13;
 fatti di causa. Ulteriore ragione di inammissibilità  è,  poi,  nel    &#13;
 fatto   che  l'ordinanza  non  precisa  il  "senso"  della  richiesta    &#13;
 declaratoria di illegittimità costituzionale, ed è indeterminato il    &#13;
 thema   decidendum;   né   si   individuano   ragioni   a   sostegno    &#13;
 dell'introduzione  di  scelte  non  costituzionalmente  obbligate  e,    &#13;
 quindi, riservate alla discrezionalità del legislatore.                 &#13;
   Nel merito, la questione sarebbe comunque infondata. Il legislatore    &#13;
 regionale ha sottoposto a controllo  tutte  le  strutture  aperte  al    &#13;
 pubblico,  movendo  da  un dato oggettivo, e cioè la possibilità di    &#13;
 accesso  indifferenziato.   Coerentemente,   si   è   dato   rilievo    &#13;
 all'elemento  della  pubblicità  che  mira ad attirare clienti nella    &#13;
 struttura  sanitaria,  favorendo  così  l'accesso  a   un   pubblico    &#13;
 indifferenziato.  Altrettanto  può  dirsi  per  l'ulteriore elemento    &#13;
 della presenza di dipendenti, che si giustifica in nome dell'esigenza    &#13;
 di mantenere rapporti con il pubblico.                                   &#13;
   L'ampiezza della fattispecie descritta dalla disposizione censurata    &#13;
 - prosegue la difesa della regione - è funzionale alla tutela  degli    &#13;
 utenti  della  struttura  sanitaria, quale che sia la sua importanza.    &#13;
 Ma anche aderendo alla prospettazione dell'ordinanza, le  conclusioni    &#13;
 non  sarebbero  diverse.  Se  l'intento  del  legislatore regionale -    &#13;
 invero  costituzionalmente  apprezzabile  -  fosse  stato  quello  di    &#13;
 esercitare un controllo sui servizi sanitari gestiti dai privati, non    &#13;
 si  vede  per  quale  motivo  vi si dovrebbero sottrarre le strutture    &#13;
 carenti di attrezzature idonee per gli accertamenti e gli  interventi    &#13;
 terapeutici,  giacché  si  impone  il  massimo  rigore per qualunque    &#13;
 attività di assistenza sanitaria.                                       &#13;
   Argomenti,  questi,  che  la  Regione  Lombardia ha ribadito in una    &#13;
 memoria depositata nell'imminenza della camera di consiglio,  ove  si    &#13;
 richiamano  le  sentenze  di  questa  Corte nn. 35 e 17 del 1997, con    &#13;
 riguardo al valore costituzionale, essenziale, della salute.<diritto>Considerato in diritto</diritto>1. -  Il pretore di Mantova ha sollevato, in  riferimento  all'art.    &#13;
 3   della  Costituzione,  questione  di  legittimità  costituzionale    &#13;
 dell'art.  2, comma 1, lettera a) della legge della regione Lombardia    &#13;
 17  febbraio  1986,  n.  5  (Disciplina  per  l'autorizzazione  e  la    &#13;
 vigilanza  sulle  istituzioni  sanitarie  di  carattere  privato  che    &#13;
 svolgono  attività  ambulatoriale,  nonché  per  il  trasporto   di    &#13;
 infermi),  nella parte in cui assimila indiscriminatamente situazioni    &#13;
 diverse, e cioè gli istituti aventi individualità e  organizzazione    &#13;
 propria  che  presentano le stesse caratteristiche delle case e degli    &#13;
 istituti di cura, e lo studio privato del  singolo  medico  il  quale    &#13;
 abbia, in ipotesi, un solo dipendente e la targa pubblicitaria. Detta    &#13;
 assimilazione   sarebbe  irragionevole  e  fonte  di  disparità;  e,    &#13;
 infatti, se la norma tende a controllare i servizi  sanitari  gestiti    &#13;
 dai  privati,  la  definizione  di  ambulatorio  va circoscritta alle    &#13;
 strutture organizzate, dotate di attrezzature  per  gli  accertamenti    &#13;
 diagnostici e le terapie prescritte.                                     &#13;
   2.  -  Vanno disattese le eccezioni di inammissibilità mosse dalla    &#13;
 regione Lombardia, secondo cui l'ordinanza di rimessione non indica i    &#13;
 fatti di causa, è indeterminata  nel  thema  decidendum,  e  inoltre    &#13;
 richiede   alla   Corte   una   pronuncia   lesiva   dell'ambito   di    &#13;
 discrezionalità riservato al legislatore.                               &#13;
   Nella  parte  iniziale,  relativa  alle   sanzioni   amministrative    &#13;
 comminate  ai  ricorrenti  per  aver attivato ambulatori medici senza    &#13;
 l'autorizzazione, prevista dalla  legge  regionale  n.  5  del  1986,    &#13;
 l'ordinanza  motiva  in modo per vero succinto, ma sufficiente, circa    &#13;
 la rilevanza  della  questione.  Né  può  dirsi  incerto  il  thema    &#13;
 decidendum: il dubbio di legittimità, avanzato alla luce dell'art. 3    &#13;
 della  Costituzione,  concerne  l'assimilazione  -  nell'ultima parte    &#13;
 dell'art. 2, comma 1, lettera a) - di  fattispecie  che  si  assumono    &#13;
 diverse.  E  non vale altresì, quale ragione di inammissibilità, la    &#13;
 garanzia della discrezionalità legislativa,  dal  momento  che  dopo    &#13;
 un'eventuale pronuncia di illegittimità costituzionale si renderebbe    &#13;
 comunque   necessario   un   successivo  intervento  del  legislatore    &#13;
 regionale volto a dare razionale sistemazione alla materia, secondo i    &#13;
 principi affermati da questa Corte.                                      &#13;
   3. - Passando dunque al merito, la questione è infondata.             &#13;
   Il giudice a  quo  si  duole  dell'assimilazione  -  a  suo  avviso    &#13;
 ingiustificata  -  di  situazioni  diverse: quella degli istituti con    &#13;
 individualità e organizzazione propria e autonoma,  e  quella  dello    &#13;
 studio  privato  del singolo medico; onde la violazione del principio    &#13;
 di eguaglianza.                                                          &#13;
   Va considerato, innanzitutto, che la regione è titolare dei poteri    &#13;
 di vigilanza e  di  autorizzazione  sulle  istituzioni  sanitarie  di    &#13;
 carattere privato, fra i quali rientra, certo, quello di autorizzarne    &#13;
 la  pubblicità  (in particolare, v. la sentenza n. 461 del 1992). La    &#13;
 definizione  delle  istituzioni  sanitarie   private   che   svolgono    &#13;
 attività ambulatoriali, di cui all'art. 2 della legge regionale n. 5    &#13;
 del  1986,  è  strumentale al meccanismo autorizzatorio disciplinato    &#13;
 dall'art. 3 che vale, infatti, per  le  strutture  individuate  dalla    &#13;
 legge.                                                                   &#13;
   La  questione  di  legittimità  costituzionale pone un problema di    &#13;
 bilanciamento fra diverse istanze: vi è l'esigenza di tutelare  beni    &#13;
 pubblici  essenziali,  riconducibili  al  diritto  fondamentale della    &#13;
 salute,  con  il  riconoscimento  di  poteri  di   vigilanza   e   di    &#13;
 autorizzazione  affidati  a  soggetti  pubblici;  ma occorre che tali    &#13;
 poteri siano conformati in modo ragionevole, al fine di non  limitare    &#13;
 -  senza  che  vi sia effettiva necessità - l'iniziativa privata nel    &#13;
 settore dell'assistenza sanitaria.   Ora, non si  può  dire  che  la    &#13;
 scelta  operata  dal  legislatore  regionale  sia fonte di disparità    &#13;
 ingiustificate  o  sia,  comunque,  irragionevole.    La  lettera  a)    &#13;
 dell'art.  2, qui in esame, si fonda essenzialmente sulla sussistenza    &#13;
 di  una  "autonomia  organizzativa"  della  struttura  e  sulla   sua    &#13;
 "apertura  al  pubblico":  deve  trattarsi,  cioè,  di  "istituzioni    &#13;
 sanitarie", secondo quanto precisato nella parte  iniziale  dell'art.    &#13;
 2,  comma  1,  e nell'ultima parte della lettera a), che eccettua gli    &#13;
 studi privati sprovvisti di organizzazione propria.                      &#13;
   Il sistema qui ricostruito si sottrae, dunque, alle censure  mosse,    &#13;
 e la questione va dichiarata infondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 2, comma 1, lettera a), della legge della regione Lombardia    &#13;
 17  febbraio  1986,  n.  5  (Disciplina  per  l'autorizzazione  e  la    &#13;
 vigilanza  sulle  istituzioni  sanitarie  di  carattere  privato  che    &#13;
 svolgono  attività  ambulatoriale,  nonché  per  il  trasporto   di    &#13;
 infermi),  sollevata,  in  riferimento all'art. 3 della Costituzione,    &#13;
 dal pretore di Mantova con l'ordinanza indicata in epigrafe.             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 5 maggio 1997.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Guizzi                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 6 maggio 1997.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
