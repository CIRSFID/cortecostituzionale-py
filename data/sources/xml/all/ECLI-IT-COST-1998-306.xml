<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>306</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:306</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/07/1998</data_decisione>
    <data_deposito>22/07/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'art.  204,  comma  1,    &#13;
 del  decreto  legislativo  30 aprile 1992, n. 285 (Nuovo codice della    &#13;
 strada), promossi con ordinanze  emesse  l'11  febbraio  1997  (n.  5    &#13;
 ordinanze)  dal  pretore di Roma sezione distaccata di Castelnuovo di    &#13;
 Porto iscritte ai nn. 849, 850, 851, 852 e 853 del registro ordinanze    &#13;
 1997 e pubblicate nella Gazzetta Ufficiale della  Repubblica  n.  50,    &#13;
 prima serie speciale, dell'anno 1997.                                    &#13;
   Visti  gli  atti  d'intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di  consiglio  del  7  aprile  1998  il  giudice    &#13;
 relatore Fernando Santosuosso.                                           &#13;
   Ritenuto  che  nel  corso  di  diversi  giudizi  di  opposizione ad    &#13;
 ordinanze-ingiunzioni  prefettizie  con  le  quali  si  intimava   il    &#13;
 pagamento  di somme di denaro a titolo di sanzione amministrativa per    &#13;
 violazione di norme del codice della strada, il  pretore  di  Roma  -    &#13;
 sezione distaccata di Castelnuovo di Porto, dopo aver emesso sentenze    &#13;
 di  rigetto  non  definitive  (che  non statuivano sulla richiesta di    &#13;
 riduzione della sanzione, formulata, in via subordinata, da  tutti  i    &#13;
 ricorrenti),  con  cinque  ordinanze  emesse  l'11  febbraio 1997, ha    &#13;
 sollevato  -  in  riferimento  all'art.    24  della  Costituzione  -    &#13;
 questione  di legittimità costituzionale dell'art. 204, comma 1, del    &#13;
 decreto legislativo 30 aprile 1992,  n.    285  (Nuovo  codice  della    &#13;
 strada),  nella  parte  in  cui  prevede  che il prefetto, se ritiene    &#13;
 fondato l'accertamento,  ingiunge  il  pagamento  di  una  somma  non    &#13;
 inferiore al doppio del minimo edittale per ogni singola violazione;     &#13;
     che  il pretore di Roma ritiene di dover riproporre la questione,    &#13;
 già decisa dalla Corte costituzionale nel senso  della  infondatezza    &#13;
 (e  poi,  in epoca successiva alle ordinanze di rimessione, nel senso    &#13;
 della manifesta infondatezza), dal  momento  che  non  sarebbe  stato    &#13;
 considerato   che   il   potere   del   giudice  dell'opposizione  di    &#13;
 rideterminare la sanzione amministrativa  può  operare  soltanto  in    &#13;
 quanto  gli  sia  permesso  di  effettuare  una valutazione di merito    &#13;
 sull'applicazione della sanzione stessa: se, cioè, "abbia il  potere    &#13;
 di prendere cognizione non soltanto degli aspetti di legittimità del    &#13;
 provvedimento  amministrativo  sanzionatorio,  ma anche di quelli che    &#13;
 attengono alla  discrezionalità,  per  cui  soltanto  a  seguito  di    &#13;
 annullamento   dell'atto   si   possa   sostituire  altra  e  diversa    &#13;
 valutazione del fatto";                                                  &#13;
     che  tuttavia  la  Corte  di  cassazione, con indirizzo costante,    &#13;
 avrebbe più volte ribadito che la cognizione del  giudice  ordinario    &#13;
 deve limitarsi ad un sindacato di legittimità, relativo agli aspetti    &#13;
 attinenti  alla  motivazione  del  provvedimento  e  non  al concreto    &#13;
 esercizio della potestà sanzionatoria dell'amministrazione:  perciò    &#13;
 il  giudice  ordinario  non  potrebbe  mai,  "in concreto", applicare    &#13;
 l'art. 23 della legge n.  689  del  1981,  rideterminando  in  melius    &#13;
 l'entità   della   sanzione,   poiché   dovrebbe  "ripercorrere  il    &#13;
 procedimento     amministrativo     sanzionatorio,      sostituendosi    &#13;
 all'autorità   irrogante   nella   quantificazione   concreta  della    &#13;
 sanzione, ciò che palesemente gli è precluso";                         &#13;
     che  da  ciò  deriverebbe  il  contrasto  con  l'art.  24  della    &#13;
 Costituzione,  in  quanto  la  disposizione impugnata si connoterebbe    &#13;
 come  un  "deterrente"  alla  proposizione   del   ricorso   in   via    &#13;
 amministrativa,  in considerazione dell'impossibilità per il giudice    &#13;
 di valutare "in concreto", in caso di rigetto del ricorso stesso,  la    &#13;
 congruità  della  sanzione  irrogata,  che  risulta  automaticamente    &#13;
 maggiorata rispetto a quella non contestata;                             &#13;
     che nei giudizi davanti alla Corte costituzionale è  intervenuto    &#13;
 il  Presidente  del  Consiglio  dei  Ministri, rappresentato e difeso    &#13;
 dall'Avvocatura  generale  dello  Stato,  che  ha  concluso  per   la    &#13;
 manifesta  infondatezza della questione, essendo già stata decisa in    &#13;
 passato nel senso dell'infondatezza e della manifesta infondatezza.      &#13;
   Considerato che, essendo stata  sollevata  in  tutti  i  giudizi  a    &#13;
 quibus  la  medesima  questione,  i  relativi giudizi di legittimità    &#13;
 costituzionale devono essere riuniti;                                    &#13;
     che identica questione è già stata più volte rimessa a  questa    &#13;
 Corte  e  decisa,  da  ultimo,  con l'ordinanza n. 324 del 1997 e, in    &#13;
 precedenza, con l'ordinanza n. 268 del 1996, la sentenza n.  366  del    &#13;
 1994,  le  ordinanze  n.  67  e  n.  350  del 1994, nelle quali si è    &#13;
 rilevato che: a) la misura della sanzione può  ben  essere  modulata    &#13;
 dal  legislatore  in  modo  da  perseguire  finalità  deflattive del    &#13;
 contenzioso amministrativo; b) è,  comunque,  sempre  esperibile  il    &#13;
 ricorso  giurisdizionale,  in  cui  il giudice, anche quando respinge    &#13;
 l'opposizione, non è vincolato da alcun limite per la determinazione    &#13;
 della  sanzione,  che  ben   può   essere   fissata   nella   misura    &#13;
 corrispondente  a  quella  "ridotta"  di  cui all'art. 202 del codice    &#13;
 della strada;                                                            &#13;
     che  negli  stessi  sensi  appare  orientata  la   più   recente    &#13;
 giurisprudenza  della  Corte  di  cassazione,  secondo  la  quale "il    &#13;
 procedimento amministrativo di determinazione della sanzione ... può    &#13;
 essere  interamente  sostituito  dal  giudice  dell'opposizione,  cui    &#13;
 spetta  il  potere  di  determinare autonomamente, prescindendo cioè    &#13;
 dalle   valutazioni   compiute   dall'amministrazione   ed   espresse    &#13;
 nell'ordinanza-ingiunzione,  l'entità  della  sanzione dovuta per la    &#13;
 concreta violazione" (v. Cass., 2 febbraio 1996, n.  911);               &#13;
     che, non essendo stati addotti profili nuovi, la  questione  deve    &#13;
 essere dichiarata manifestamente infondata.                              &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i  giudizi  avanti    &#13;
 la Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale dell'art. 204, comma 1,  del    &#13;
 decreto  legislativo  30  aprile  1992,  n.  285  (Nuovo codice della    &#13;
 strada), sollevata in riferimento all'art. 24 della Costituzione  dal    &#13;
 pretore  di Roma - sezione distaccata di Castelnuovo di Porto, con le    &#13;
 ordinanze indicate in epigrafe.                                          &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 9 luglio 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 22 luglio 1998.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
