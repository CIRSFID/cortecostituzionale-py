<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>92</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:92</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/01/1991</data_decisione>
    <data_deposito>16/02/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, dott. Francesco &#13;
 GRECO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco &#13;
 Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  464, primo    &#13;
 comma, del codice di  procedura  penale,  promossi  con  le  seguenti    &#13;
 ordinanze:                                                               &#13;
      1)  ordinanza  emessa  il 14 maggio 1990 dal Tribunale di Varese    &#13;
 nel procedimento a carico di Tallachini Vittorio, iscritta al n.  516    &#13;
 del  registro  ordinanze  1990  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 34, prima serie speciale, dell'anno 1990;            &#13;
      2)  ordinanza  emessa  il 24 maggio 1990 dal Tribunale di Varese    &#13;
 nel procedimento a carico di Iannece Nicolina, iscritta al n. 559 del    &#13;
 registro  ordinanze  1990 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 38, prima serie speciale, dell'anno 1990;                  &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 12 dicembre 1990 il Giudice    &#13;
 relatore Giovanni Conso;                                                 &#13;
    Ritenuto  che  il  Tribunale  di  Varese,  con  due  ordinanze dal    &#13;
 contenuto sostanzialmente identico emesse il 14 maggio 1990 ed il  30    &#13;
 maggio 1990, ha sollevato, in riferimento agli artt. 3, 24, 101 e 111    &#13;
 della Costituzione, questione di legittimità  dell'art.  464,  primo    &#13;
 comma,  del  codice  di  procedura penale, laddove prescrive che, "in    &#13;
 assenza del consenso del p. m. nel termine  fissato  dal  G.i.p.,  il    &#13;
 giudice per le indagini preliminari dispone il giudizio immediato";      &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  Generale  dello    &#13;
 Stato,  chiedendo che la questione sia dichiarata, in via principale,    &#13;
 inammissibile e, in subordine, non fondata;                              &#13;
    Considerato che le ordinanze sollevano un'identica questione e che    &#13;
 i relativi giudizi vanno, quindi, riuniti;                               &#13;
      che,  questa  Corte, con sentenza n. 81 del 1991, ha dichiarato,    &#13;
 in applicazione dell'art. 27  della  legge  11  marzo  1953,  n.  87,    &#13;
 l'illegittimità  costituzionale  dell'art.  464,  primo  comma,  del    &#13;
 codice di procedura penale, proprio nella parte in  cui  non  prevede    &#13;
 che  il  pubblico  ministero,  in  caso  di  dissenso,  sia tenuto ad    &#13;
 enunciarne le ragioni e  nella  parte  in  cui  non  prevede  che  il    &#13;
 giudice,  quando,  a dibattimento concluso, ritiene ingiustificato il    &#13;
 dissenso del pubblico  ministero,  possa  applicare  all'imputato  la    &#13;
 riduzione  di  pena  contemplata  dall'art. 442, secondo comma, dello    &#13;
 stesso codice;                                                           &#13;
      che,  di  conseguenza,  la  questione  qui  proposta deve essere    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 464, primo comma, del codice di    &#13;
 procedura  penale, già dichiarato costituzionalmente illegittimo con    &#13;
 sentenza n. 81 del 1991, nella  parte  in  cui  non  prevede  che  il    &#13;
 pubblico  ministero, in caso di dissenso, sia tenuto ad enunciarne le    &#13;
 ragioni e nella parte in cui non prevede che il  giudice,  quando,  a    &#13;
 dibattimento   concluso,   ritiene  ingiustificato  il  dissenso  del    &#13;
 pubblico ministero, possa applicare all'imputato la riduzione di pena    &#13;
 contemplata  dall'art.  442,  secondo  comma,  dello  stesso  codice,    &#13;
 questione sollevata dal Tribunale  di  Varese  con  le  ordinanze  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 28 gennaio 1991.                              &#13;
                    Il Presidente e redattore: CONSO                      &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 16 febbraio 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
