<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>13</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:13</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/01/1985</data_decisione>
    <data_deposito>23/01/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - AVV. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - AVV. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO &#13;
 LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO &#13;
 CORASANTTI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt.  27, 28, 29  &#13;
 e 31 legge 16 giugno 1927, n. 1766; art. 1 legge  10  luglio  1930,  n.  &#13;
 1078  (Definizione  delle  controversie  in  materia  di  usi  civici),  &#13;
 promosso con l'ordinanza  emessa  il  7  maggio  1980  dal  pretore  di  &#13;
 Montefiascone  nel  procedimento  civile vertente tra Ercolani Maria ed  &#13;
 altri e Università Agraria  di  Tarquinia,  iscritta  al  n.  805  del  &#13;
 registro  ordinanze  1980  e  pubblicata nella Gazzella Ufficiale della  &#13;
 Repubblica n. 34 dell'anno 1981.                                         &#13;
     Visti  gli  atti  di  costituzione  dell'Università   Agraria   di  &#13;
 Tarquinia, di Enzo Ercolani ed altri e di Maria Ercolani nonché l'atto  &#13;
 di intervento del Presidente del Consiglio dei ministri.                 &#13;
     Udito  nella  camera  di  consiglio  del 4 dicembre 1984 il Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto che  il  Pretore  di  Montefiascone,  con  l'ordinanza  in  &#13;
 epigrafe   (emessa   in   un  giudizio  di  opposizione  all'esecuzione  &#13;
 dell'obbligo di rilascio di un terreno, avente titolo nella sentenza 22  &#13;
 giugno 1968 del Commissario agli usi civici di Roma), ha denunciato gli  &#13;
 artt. 27 e 28 l. 16 giugno 1927, n.  1766 e 1 l.  10  luglio  1930,  n.  &#13;
 1078  (secondo  cui i commissari per gli usi civici vanno "nominati con  &#13;
 decreto reale" - ora del Ministero dell'agricoltura  -  e  "scelti  fra  &#13;
 magistrati" ordinari, previa collocazione fuori ruolo), prospettando il  &#13;
 contrasto  di  detta  normativa  con  gli artt. 102, 103, 104, 105 e VI  &#13;
 disp.  trans. della Costituzione, in base  alla  considerazione  che  -  &#13;
 dopo   l'entrata  in  vigore  della  legge  (195/1958)  istitutiva  del  &#13;
 Consiglio superiore della magistratura (cui la Costituzione riserva  le  &#13;
 assunzioni   e   gli  altri  provvedimenti  relativi  allo  status  dei  &#13;
 magistrati) - il legislatore avrebbe dovuto  prevedere  una  "ratifica"  &#13;
 dello  stesso  Consiglio  per  quei  magistrati, ai quali l'incarico di  &#13;
 giudice degli usi civici era stato già conferito da altro organo;       &#13;
     che  lo  stesso  Pretore ha anche impugnato gli artt. 29 e 31 della  &#13;
 predetta l. 1766/1927, in riferimento agli artt. 3, 24, 41, 42, 43, 44,  &#13;
 101, 102, 104, 108 e VI disp. trans. Cost., argomentando che,  nel  suo  &#13;
 complesso,  la  disciplina  degli  usi  civici  sarebbe "anacronistica,  &#13;
 superata ed in contrasto  con  la  moderna  politica  economico-sociale  &#13;
 della  Nazione,  che protegge il coltivatore diretto" e "l'accorpamento  &#13;
 delle proprietà";                                                       &#13;
     che innanzi alla Corte si sono costituite  le  parti  opponenti  ed  &#13;
 opposte  del  giudizio  a quo, che hanno concluso, rispettivamente, per  &#13;
 l'accoglimento   e   per   la   reiezione   dell'impugnativa,   ed   è  &#13;
 altresi'intervenuto  il  Presidente  del  Consiglio  dei  ministri, per  &#13;
 chiedere alla Corte di  dichiarare  l'inammissibilità  della  prima  e  &#13;
 l'infondatezza della seconda questione.                                  &#13;
     Considerato  che  la  ratifica ex nunc, di cui è stata prospettata  &#13;
 l'esigenza, in relazione a nomine commissariali effettuate (come  nella  &#13;
 specie) da organo diverso dal C.S.M., sarebbe assolutamente ininfluente  &#13;
 nel  giudizio  di esecuzione a quo, che trae titolo da una sentenza del  &#13;
 Commissario agli usi civici di Roma la quale è - per ammissione  dello  &#13;
 stesso  Pretore  -  ormai  assistita  dalla  forza  del giudicato, onde  &#13;
 sarebbe insuscettibile di essere in alcun modo raggiunta dagli  effetti  &#13;
 di  una  eventuale  pronuncia  della  Corte  sulla  illegittimità  del  &#13;
 giudice; e che, per tale motivo, la prima questione  è  manifestamente  &#13;
 inammissibile;                                                           &#13;
     che  la  seconda  questione  - motivata con riferimento (pressoché  &#13;
 esclusivo) agli artt. 42 e 44 Cost. (al di là della  enunciazione  del  &#13;
 tutto  apodittica  di  una serie ulteriore di altri parametri) è a sua  &#13;
 volta  manifestamente  infondata,  poiché  il  formulato  giudizio  di  &#13;
 anacronismo  della  disciplina degli usi civici (là dove non travalica  &#13;
 in  una  non  consentita  valutazione  di  politica   legislativa)   è  &#13;
 contraddetto  e  smentito  dalla  successiva  normativa (artt. 1, terzo  &#13;
 comma, e 4, lett. l del d.P.R. 15 gennaio 1972, n. 11; artt.  66  e  71  &#13;
 lett.  i  del  d.P.R.    24 luglio 1977, n. 616) che ha trasferito alle  &#13;
 Regioni le funzioni amministrative e confermato allo Stato le  funzioni  &#13;
 giurisdizionali  nella  predetta  materia  degli  usi civici; e perché  &#13;
 comunque la questione  di  costituzionalità  della  giurisdizione  del  &#13;
 Commissario  per  gli  usi  civici,  in riferimento agli artt. 108 e 25  &#13;
 Cost., è stata già dichiarata non fondata con sentenza della Corte n.  &#13;
 73/1970.                                                                 &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e 9 delle norme integrative per i giudizi davanti alla Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1. - dichiara la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità  costituzionale  degli  artt.  27  e 28 della l. 16 giugno  &#13;
 1927, n. 1766 (sul riordinamento degli usi civici) e dell'art. 1  della  &#13;
 l.    10  luglio 1930, n. 1078 (sulla definizione delle controversie in  &#13;
 materia di usi civici), in riferimento agli artt. 102, 103, 104, 105  e  &#13;
 VI   disp.   trans.   della  Costituzione,  sollevata  dal  Pretore  di  &#13;
 Montefiascone con l'ordinanza in epigrafe.                               &#13;
     2.  -  dichiara  la  manifesta  infondatezza  della  questione   di  &#13;
 legittimità  costituzionale  degli  artt.  29  e  31 della predetta l.  &#13;
 1766/1927, in riferimento agli artt. 3, 24, 41, 42, 43, 44,  101,  102,  &#13;
 104,  105,  108 e VI disp. trans. Cost., sollevata dallo stesso Pretore  &#13;
 con l'ordinanza indicata.                                                &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 18 gennaio 1985.        &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI   -   GIUSEPPE  BORZELLINO  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
