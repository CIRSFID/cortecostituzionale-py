<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1959</anno_pronuncia>
    <numero_pronuncia>45</numero_pronuncia>
    <ecli>ECLI:IT:COST:1959:45</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Antonio Manca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1959</data_decisione>
    <data_deposito>15/07/1959</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Avv. &#13;
 GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - Prof. &#13;
 ERNESTO BATTAGLINI - Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO &#13;
 GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - &#13;
 Prof. NICOLA JAEGER - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - &#13;
 Prof. ALDO SANDULLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  del  decreto   del  &#13;
 Presidente  della  Repubblica  19  novembre 1952, n. 2290, promosso con  &#13;
 ordinanza emessa il 12 febbraio 1958  dalla  Corte  di  Cassazione  nel  &#13;
 procedimento  civile  vertente fra Cianciotta Angelantonio e l'Ente per  &#13;
 la riforma fondiaria in  Puglia  e  Lucania,  iscritta  al  n.  27  del  &#13;
 Registro ordinanze del 1958 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 174 del 19 luglio 1958.                                    &#13;
     Udita  nell'udienza  pubblica  del  27 maggio 1959 la relazione del  &#13;
 Giudice Antonio Manca;                                                   &#13;
     uditi l'avv. Virgilio Andrioli per il  Cianciotta  e  il  sostituto  &#13;
 avvocato generale dello Stato Francesco Agrò per l'Ente di riforma.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Risulta  dall'ordinanza  emessa  il 12 febbraio 1958 dalla Corte di  &#13;
 cassazione,   nel   procedimento   civile   vertente   fra   Cianciotta  &#13;
 Angelantonio  e  l'Ente  per  la riforma fondiaria in Puglia e Lucania,  &#13;
 che, con decreto del Presidente della Repubblica del 19 novembre  1952,  &#13;
 n.  2290  (pubblicato  nella  Gazzetta Ufficiale del 20 dicembre 1952),  &#13;
 veniva espropriata nei confronti del Cianciotta, nella misura di ettari  &#13;
 19,45,91, una parte della masseria "Sacromonte" di  complessivi  ettari  &#13;
 168,63,38,  posseduta  dal  Cianciotta, a titolo di enfiteusi perpetua,  &#13;
 concessa dall'Opera pia S. Croce e S. Lucia di Altamura.                 &#13;
     Nel giudizio davanti al  tribunale  di  Bari  il  Cianciotta  aveva  &#13;
 chiesto  il  risarcimento  dei  danni  per l'illegittima espropriazione  &#13;
 disposta col decreto suddetto, in quanto il relativo  procedimento  era  &#13;
 stato  diretto  contro  l'enfiteuta e non nei confronti dell'Opera pia,  &#13;
 proprietaria del fondo. La  quale,  come  ente  pubblico,  non  sarebbe  &#13;
 soggetta  alle  espropriazioni  prevedute  dalle  leggi  sulla  riforma  &#13;
 agraria e fondiaria.                                                     &#13;
     Il Tribunale, dopo avere disposto l'integrazione del  giudizio  con  &#13;
 l'intervento  dell'Opera  pia,  con sentenza 30 maggio 1954 respinse le  &#13;
 istanze del Cianciotta e dell'Opera pia; e tale sentenza fu  confermata  &#13;
 dalla Corte di appello di Bari, con sentenza 22 febbraio 1955.           &#13;
     Su  ricorso  del  Cianciotta, la Corte di cassazione, ritenendo non  &#13;
 manifestamente infondata e rilevante, ai  fini  della  decisione  della  &#13;
 causa, l'eccezione dedotta dal Cianciotta e dall'Opera pia, ha disposto  &#13;
 la  trasmissione degli atti a questa Corte, sospendendo di decidere sul  &#13;
 ricorso.                                                                 &#13;
     Nell'ordinanza la  questione  di  legittimità  costituzionale  del  &#13;
 decreto di scorporo, in relazione agli articoli 2 della legge 12 maggio  &#13;
 1950,  n.  230, e 4 della legge 21 ottobre 1950, n. 841, è prospettata  &#13;
 sotto tre aspetti, nell'ordine seguente:                                 &#13;
     1) se soggetto passivo dell'espropriazione, per l'attuazione  della  &#13;
 riforma  fondiaria,  di  terreni  concessi  in  enfiteusi  sia soltanto  &#13;
 l'enfiteuta, oppure anche il concedente;                                 &#13;
     2) se, per la determinazione della quota oggetto  di  scorporo,  si  &#13;
 debba  tener conto dell'intero reddito derivante dal fondo enfiteutico,  &#13;
 e se questo debba computarsi per il calcolo complessivo del reddito del  &#13;
 concedente e dell'enfiteuta o  di  entrambi,  ovvero  se  debba  essere  &#13;
 ripartito in ragione della quota a ciascuno appartenente;                &#13;
     3)  se  siano soggetti a scorporo i beni di un ente pubblico, nella  &#13;
 specie di un'Opera pia.                                                  &#13;
     Dell'ordinanza sono state effettuate le prescritte notificazioni  e  &#13;
 comunicazioni.                                                           &#13;
     Si  sono  costituiti  in  questa  sede il Cianciotta, rappresentato  &#13;
 dagli avvocati Virgilio Andrioli e  Antonio  Borracci,  depositando  le  &#13;
 deduzioni  il  9  luglio 1958, nonché l'Ente di riforma, rappresentato  &#13;
 dall'Avvocatura generale dello Stato, che  ha  depositato,  pure  il  9  &#13;
 luglio 1958, le deduzioni.                                               &#13;
     L'Avvocatura dello Stato, a sostegno della legittimità del decreto  &#13;
 di  scorporo,  nelle  deduzioni  prospetta  la tesi (e vi insiste anche  &#13;
 nella memoria depositata il 12 maggio 1959) che, nel  caso  di  terreni  &#13;
 concessi in enfiteusi, soggetto passivo dell'esproprio, in applicazione  &#13;
 della  legge  sulla  riforma  fondiaria  21 ottobre 1950, n. 841 (legge  &#13;
 stralcio), non sarebbe il proprietario-concedente, bensì  l'enfiteuta,  &#13;
 che,  a  tal  fine,  sarebbe  equiparato al proprietario. In tal senso,  &#13;
 secondo  l'Avvocatura  dovrebbe  essere  interpretata  la  disposizione  &#13;
 contenuta nel terzo comma dell'art. 4 della predetta legge, nella quale  &#13;
 si  stabilisce che "le norme dei commi precedenti si applicano anche ai  &#13;
 beni costituiti in enfiteusi"; disposizione che deve  essere  collegata  &#13;
 con  quella  dell'art. 2, secondo comma, della legge 12 maggio 1950, n.  &#13;
 230 (legge Sila), che considererebbe l'enfiteuta come vero proprietario  &#13;
 del fondo. E questa interpretazione, si aggiunge,  troverebbe  conferma  &#13;
 nell'art.  5 della legge 18 maggio 1951, n. 333, che impone soltanto ai  &#13;
 proprietari  e  agli  enfiteuti,  i  cui  terreni  sono   soggetti   ad  &#13;
 espropriazione,  speciali  obblighi  rispetto  ai terreni stessi. Se si  &#13;
 dovesse ritenere invece che soggetto passivo dello  scorporo  fosse  il  &#13;
 proprietario-conce-dente  e  che  l'enfiteuta assumesse la posizione di  &#13;
 terzo, la ricordata disposizione dell'art. 4 della legge n. 841 sarebbe  &#13;
 superflua; perché da un lato la situazione  dei  proprietari  rispetto  &#13;
 alla  legge fondiaria sarebbe disciplinata dalle disposizioni contenute  &#13;
 nei due primi commi dell'art. 4, e i diritti dei terzi  sull'indennità  &#13;
 sarebbero  regolati  dall'art.  9  della  legge Sila, applicabile anche  &#13;
 nell'ambito della legge stralcio.  Osserva ancora l'Avvocatura che, nei  &#13;
 rapporti con l'enfiteuta, si dovrebbe adottare,  in  difetto  di  norme  &#13;
 contrarie,  lo stesso sistema della legge generale sulle espropriazioni  &#13;
 del  25  giugno  1865,  n.  2359.  La  quale,  in varie disposizioni, e  &#13;
 particolarmente in quelle contenute negli articoli 27 e  52,  considera  &#13;
 l'enfiteuta,    e   non   il   concedente,   quale   soggetto   passivo  &#13;
 dell'espropriazione.                                                     &#13;
     Da  tali  rilievi  la  difesa  dell'Ente  di   riforma   trae   due  &#13;
 conseguenze.  La  prima che, poiché spetterebbe all'enfiteuta tutto il  &#13;
 reddito dominicale del fondo e  poiché  in  base  a  tale  reddito  si  &#13;
 dovrebbe  determinare la quota di scorporo, non sarebbe ammissibile sia  &#13;
 una ripartizione dell'onere di esproprio, dato che il concedente non è  &#13;
 possessore del terreno, sia una ripartizione  del  reddito  in  ragione  &#13;
 della  quota percepita rispettivamente dall'enfiteuta e dal concedente,  &#13;
 costituendo il canone un diritto di credito anche se con effetti reali.  &#13;
 La seconda che  sarebbe  superfluo  l'esame  dell'altro  aspetto  della  &#13;
 questione  prospettata nell'ordinanza, relativo cioè alla possibilità  &#13;
 di applicare le leggi di riforma ai beni degli enti  pubblici;  essendo  &#13;
 comunque da rilevare che i terreni espropriati non formerebbero oggetto  &#13;
 di  "proprietà pubblica", e che i diritti dell'Opera pia potrebbero in  &#13;
 ogni caso farsi valere sull'indennità.                                  &#13;
     La difesa del Cianciotta alle argomentazioni dell'Avvocatura, nelle  &#13;
 deduzioni e  più  ampiamente  nella  memoria,  oppone  anzitutto  che,  &#13;
 essendosi   nella   specie   espropriata   una   parte  della  masseria  &#13;
 "Sacromonte",  della  quale  proprietaria  è  un'Opera  pia,  dovrebbe  &#13;
 esaminarsi  il  problema  se  i  terreni appartenenti ad Enti pubblici,  &#13;
 anche quando siano concessi in enfiteusi, possano essere colpiti  dallo  &#13;
 scorporo,  in  base  alle  leggi di riforma, e trasferiti in proprietà  &#13;
 agli Enti  incaricati  di  applicare  le  leggi  stesse.  Problema  che  &#13;
 sussisterebbe  anche  se si ritenesse che l'espropriazione debba essere  &#13;
 diretta contro l'enfiteuta e non contro il concedente,  perché,  anche  &#13;
 in  tale ipotesi, sia la determinazione delle quote di scorporo, sia il  &#13;
 trasferimento in proprietà  agli  enti  di  riforma,  comprenderebbero  &#13;
 anche  il  diritto  del proprietario-concedente. E siccome, a quanto si  &#13;
 sostiene, oggetto delle leggi Sila e stralcio sarebbero soltanto i beni  &#13;
 appartenenti a privati (persone fisiche o società),  si  avrebbe  già  &#13;
 fondato  motivo  per  ritenere  l'illegittimità di tutto il decreto di  &#13;
 scorporo di cui si discute. Osserva inoltre la  difesa  del  Cianciotta  &#13;
 che  non  correttamente  sarebbero richiamate le disposizioni contenute  &#13;
 negli articoli 27, 44 e 52 della legge generale di  espropriazione  del  &#13;
 1865,  che  prendono  in  considerazione  l'enfiteusi.  Poiché diverso  &#13;
 sarebbe il sistema delle leggi di riforma fondiaria, che non riguardano  &#13;
 i beni in sé considerati prescindendo dai diritti di cui sono oggetto,  &#13;
 ma  stabiliscono  che,  per  determinare  la  quota   di   terreno   da  &#13;
 espropriare,  è  necessario  prima  individuare  il  proprietario,  la  &#13;
 consistenza della proprietà terriera di cui è  titolare,  nonché  il  &#13;
 reddito  dominicale  complessivo  e medio per ettaro. Secondo la difesa  &#13;
 del Cianciotta l'interpretazione del terzo comma  dell'art.    4  della  &#13;
 legge  stralcio sarebbe nel senso che, se si tratta di beni concessi in  &#13;
 enfiteusi,  si  dovrebbe  procedere  a  due  commassazioni,  l'una  nei  &#13;
 riguardi  del  proprietario-concedente  avente  come oggetto il reddito  &#13;
 dominicale diretto, l'altra nei confronti  dell'enfiteuta,  avente  per  &#13;
 base   il   reddito   percepito   dall'utilista.  Da  ciò  deriverebbe  &#13;
 l'illegittimità del decreto sotto  altro  aspetto,  in  quanto  cioè,  &#13;
 applicando  le norme della legge del 1865, e non tenendo conto che, nel  &#13;
 fondo  coesistevano  il   diritto   del   concedente   e   il   diritto  &#13;
 dell'enfiteuta, nella quota di scorporo si sarebbero imputati (come non  &#13;
 sarebbe  contestato) a quest'ultimo tutto il reddito dominicale e tutta  &#13;
 la superficie della  masseria,  considerando  il  Cianciotta  non  come  &#13;
 enfiteuta,  ma  come  pieno  proprietario  del  fondo  stesso.  Con  la  &#13;
 conseguenza,  nel  caso  concreto,  che  si  sarebbe   illegittimamente  &#13;
 aumentata in danno dell'enfiteuta la quota di scorporo, ponendo inoltre  &#13;
 a  carico  di  costui  la corresponsione a favore del concedente di una  &#13;
 parte dell'indennità corrispondente al diretto dominio.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'art. 4 della legge 21 ottobre  1950,  n.  841  (contenente  norme  &#13;
 sull'espropriazione,   bonifica,  trasformazione  ed  assegnazione  dei  &#13;
 terreni ai contadini, cosiddetta legge stralcio), stabilisce, nel primo  &#13;
 e nel secondo comma, le condizioni in base alle  quali,  nei  territori  &#13;
 suscettibili  di  trasformazione  fondiaria  e  agraria,  la proprietà  &#13;
 terriera privata è soggetta ad una percentuale di scorporo. Nel  terzo  &#13;
 comma  dispone che "le norme dei commi precedenti si applicano anche ai  &#13;
 beni costituiti in enfiteusi". E poiché, con il decreto del Presidente  &#13;
 della Repubblica del 19 novembre 1952 (emanato  in  applicazione  della  &#13;
 ricordata  legge  21  ottobre 1950), è stata espropriata nei confronti  &#13;
 dell'enfiteuta  Angelantonio  Cianciotta  una  parte   della   masseria  &#13;
 "Sacromonte",  di  proprietà  dell'Opera  pia  S.  Croce e S. Lucia di  &#13;
 Altamura, concessa in enfiteusi perpetua al Cianciotta,  nell'ordinanza  &#13;
 della Corte di cassazione, come primo motivo di incostituzionalità del  &#13;
 detto  decreto,  per  eccesso  di delega, si prospetta la questione (ad  &#13;
 avviso di questa Corte fondamentale nell'attuale controversia),  se  il  &#13;
 procedimento  di  scorporo,  in  base alla legge stralcio, debba essere  &#13;
 diretto soltanto contro l'enfiteuta, come hanno ritenuto il Tribunale e  &#13;
 la Corte di appello e come sostiene l'Avvocatura  dello  Stato,  ovvero  &#13;
 anche  nei  confronti  del  proprietario-  concedente, come sostiene la  &#13;
 difesa del Cianciotta. La quale  osserva,  in  proposito,  che,  quando  &#13;
 l'espropriazione  riguarda terreni in enfiteusi, al fine di determinare  &#13;
 la percentuale  di  scorporo  si  dovrebbe  procedere  ad  una  duplice  &#13;
 commassazione,  "l'una  che abbia per soggetto passivo il concedente, e  &#13;
 per oggetto la superficie ed il reddito commisurato al dominio diretto,  &#13;
 e l'altra che abbia per soggetto passivo l'enfiteuta, sulla base  della  &#13;
 superficie  e  del  reddito  commisurato  al  dominio  utile". Aggiunge  &#13;
 inoltre a chiarimento  che,  per  "determinare  la  quota  di  scorporo  &#13;
 sarebbe   indispensabile   accertare  chi  è  proprietario  e  chi  è  &#13;
 enfiteuta, perché il duplice accertamento  rappresenterebbe  il  primo  &#13;
 insostituibile  passo,  non  compiendo  il  quale  sarebbe  impossibile  &#13;
 determinare la superficie e il reddito dominicale  del  proprietario  e  &#13;
 dell'enfiteuta e quindi dimensionare le rispettive quote di scorporo".   &#13;
     Accogliendo  tale tesi peraltro si verrebbero a porre, in contrasto  &#13;
 col sistema, sullo stesso piano e rispetto  allo  stesso  bene  oggetto  &#13;
 dell'espropriazione   il   diritto   del   concedente,  titolare  della  &#13;
 proprietà,  e  il  diritto  dell'enfiteuta,  cui,  secondo  l'opinione  &#13;
 dominante, spetta sull'immobile un diritto reale parziario, sia pure il  &#13;
 più  ampio  e  comprensivo.  E  ne  deriverebbe  inoltre,  come logico  &#13;
 corollario, che, in dipendenza della duplice determinazione delle quote  &#13;
 di scorporo, si dovrebbe procedere anche ad una duplice  determinazione  &#13;
 dell'indennità. Il che sarebbe contrario al principio fondamentale che  &#13;
 regola  la  materia,  secondo  il  quale  la  indennità  stessa  viene  &#13;
 calcolata con criterio unitario  rispetto  al  bene  è  spropriato;  e  &#13;
 soltanto  su  di  essa, che lo surroga, si possono far valere i diritti  &#13;
 dei terzi, ai sensi dell'art. 52,  secondo  comma,  della  legge  sulle  &#13;
 espropriazioni  del  25 giugno 1865, n. 2359, e dell'art. 9 della legge  &#13;
 12 maggio 1950, n.  230  (cosiddetta  legge  Sila),  applicabile  anche  &#13;
 nell'ambito della legge stralcio.                                        &#13;
     Questa  Corte ritiene invece fondata la tesi sostenuta dall'Ente di  &#13;
 riforma, nel senso che soggetto passivo del  procedimento  di  scorporo  &#13;
 sia  l'enfiteuta  Sebbene  infatti  la  disposizione  del  terzo  comma  &#13;
 dell'art. 4 della legge 21 ottobre 1950, nella formulazione  letterale,  &#13;
 si  riferisca  obiettivamente al bene enfiteutico, deve essere tuttavia  &#13;
 interpretata in correlazione col sistema nel quale il  terzo  comma  è  &#13;
 inserito.   È,   infatti,  da  tenere  presente  che  questo  richiama  &#13;
 espressamente anche le norme del primo e del secondo comma dello stesso  &#13;
 art. 4; quelle norme cioè che caratterizzano il sistema adottato dalla  &#13;
 legge stralcio per le espropriazioni nei comprensori di  riforma.    Il  &#13;
 quale  sistema,  come  è stato già chiarito con le sentenze di questa  &#13;
 Corte n. 8 e n. 10 del 1959, a differenza della  legge  del  25  giugno  &#13;
 1865,   n.   2359,   sulle  espropriazioni,  non  riguarda  i  beni  da  &#13;
 assoggettare   allo   scorporo   indipendentemente    dall'accertamento  &#13;
 dell'effettivo titolare, ma li prende in considerazione in quanto fanno  &#13;
 parte di tutto il compendio terriero a lui appartenente. Sulla base poi  &#13;
 del  reddito  complessivo  dominicale  e  di  quello  medio  per ettaro  &#13;
 spettante al titolare dei beni, calcolato secondo le tariffe di  estimo  &#13;
 in  vigore  al  1  gennaio  1943, determina, nelle tabelle annesse alla  &#13;
 legge, la percentuale di scorporo, da attuarsi in concreto  nelle  zone  &#13;
 di terreno individuate dagli enti incaricati delle espropriazioni.       &#13;
     Dato  lo stretto collegamento fra i primi tre commi dell'art. 4, se  &#13;
 ne deve desumere che, anche quando si tratta di fondo enfiteutico, tale  &#13;
 bene, ai fini dello scorporo in attuazione della legge di riforma,  non  &#13;
 è  preso  in  considerazione  nella  sua  consistenza  obiettiva, come  &#13;
 parrebbe in base alla  formulazione letterale del terzo comma dell'art.  &#13;
 4, bensì come fonte di produzione del reddito terriero,  da  riferirsi  &#13;
 al  soggetto,  cioè all'enfiteuta, che, essendo nel possesso del fondo  &#13;
 (come è pacifico nella specie) tale  reddito  direttamente  percepisce  &#13;
 (art.  1561 del Cod. civ. del 1865, e art. 959 Cod. civ. vigente). Ora,  &#13;
 se  si  considera che, secondo la legge stralcio, come si è accennato,  &#13;
 la quota di scorporo è determinata  da  una  percentuale  del  reddito  &#13;
 complessivo  e  medio  dominicale  terriero,  è  coerente  col sistema  &#13;
 ritenere  che  il  reddito  del  fondo   enfiteutico,   inerente   alla  &#13;
 produttività  del  fondo  medesimo,  non  potendo  essere  imputato al  &#13;
 proprietario che non lo percepisce, costituisce  uno  dei  coefficienti  &#13;
 per  il  calcolo  complessivo  della  consistenza patrimoniale terriera  &#13;
 dell'enfiteuta. Sul quale, del resto, grava anche l'imposta sul reddito  &#13;
 dominicale, ai sensi degli articoli 49  e  50  del  testo  unico  sulle  &#13;
 imposte  dirette (approvato con decreto del 29 gennaio 1958, n. 645), i  &#13;
 quali, su questo punto, hanno riprodotto disposizioni già  in  vigore.  &#13;
 Ne  discende  perciò  che, nel procedimento espropriativo in base alla  &#13;
 legge  di  riforma,  l'enfiteuta  viene   ad   essere   parificato   al  &#13;
 proprietario, e che, per coerente ragione, devono riferirsi allo stesso  &#13;
 enfiteuta   tutte  le  altre  disposizioni  della  legge  stralcio  che  &#13;
 presuppongono l'espropriazione o da essa dipendono.                      &#13;
     In base a tali criteri appunto,  secondo  quanto  si  rileva  dalle  &#13;
 deduzioni   delle   parti,   è  stata  attuata  la  legge  di  riforma  &#13;
 dell'ottobre 1950, dagli organi competenti, col parere  conforme  della  &#13;
 Commissione parlamentare.                                                &#13;
     La quale parificazione trova anche conferma nell'art. 5 della legge  &#13;
 18  maggio  1951, n. 333, contenente norme interpretative e integrative  &#13;
 della precedente legge stralcio del 21  ottobre  1950.  Tale  articolo,  &#13;
 infatti,  dispone  "che  i proprietari e gli enfiteuti, i cui beni sono  &#13;
 soggetti ad espropriazione, rispondono della conservazione dei  terreni  &#13;
 medesimi,  con  le relative piantagioni, costruzioni ed impianti, dalla  &#13;
 data di entrata in vigore della legge sino al momento della consegna di  &#13;
 essi  all'ente  espropriante".  E   assoggetta   poi,   nel   caso   di  &#13;
 inosservanza,  l'uno  e  l'altro  alle  sanzioni  penali  stabilite dal  &#13;
 successivo art. 6.                                                       &#13;
     L'interpretazione anzidetta, d'altra  parte,  aderisce  anche  alle  &#13;
 finalità  comuni alle leggi di riforma fondiaria ed agraria. Le quali,  &#13;
 come risulta ampiamente chiarito nelle relazioni che  le  accompagnano,  &#13;
 sono  state  emanate  con  il  duplice  scopo  sociale ed economico, di  &#13;
 "promuovere  un'equa  distribuzione  della  proprietà  terriera  e  di  &#13;
 determinare  un'intensa  trasformazione della terra, in conseguenza del  &#13;
 passaggio della proprietà agli agricoltori coltivatori". Finalità  il  &#13;
 cui  conseguimento  avrebbe  potuto  incontrare difficoltà se gli enti  &#13;
 incaricati della riforma, nei  comprensori  determinati  in  base  alla  &#13;
 legge  stralcio,  riguardo  ai  beni  in  enfiteusi,  si fossero dovuti  &#13;
 rivolgere al  proprietario-  concedente,  il  cui  reddito  complessivo  &#13;
 (rappresentato   soltanto   dal   canone,  determinato,  nella  specie,  &#13;
 nell'annua somma di  lire  10.445,  come  risulta  dalla  sentenza  del  &#13;
 Tribunale),  non  avesse  raggiunto la misura Stabilita dalla legge per  &#13;
 legittimare la procedura di scorporo.                                    &#13;
     In sostanza il terzo comma dell'art. 4 della legge  stralcio,  come  &#13;
 pure  rileva  l'Avvocatura  dello  Stato,  per  le finalità proprie di  &#13;
 questa legge, e nei limiti  da  questa  stabiliti,  richiama  la  norma  &#13;
 contenuta  nel  secondo  comma  dell'art.  27 della legge del 25 giugno  &#13;
 1865, n. 2359, sulle espropriazioni  per  pubblica  utilità,  dove  si  &#13;
 stabilisce  che,  quando si tratti di beni enfiteutici, l'indennità è  &#13;
 accettata e pattuita dall'enfiteuta  che  si  trova  nel  possesso  del  &#13;
 fondo.                                                                   &#13;
     Né  ciò  è  incompatibile,  contrariamente  a  quanto obietta la  &#13;
 difesa del Cianciotta, col sistema fondamentale della  legge  stralcio,  &#13;
 la  quale  richiede  accertamenti  subiettivi per la determinazione del  &#13;
 reddito complessivo e medio del fondo.   L'applicazione  infatti  della  &#13;
 ricordata  disposizione  dell'art.  27 non esclude che gli accertamenti  &#13;
 anzidetti, riguardo al fondo enfiteutico, siano compiuti nei  confronti  &#13;
 dell'enfiteuta,  come  si  è in precedenza chiarito. Ed è altresì da  &#13;
 notare  che  pure  alle  disposizioni  della  legge  del  1865  occorre  &#13;
 riferirsi,  in  mancanza di altre norme, per stabilire quale situazione  &#13;
 derivi al proprietario-concedente in conseguenza dell'espropriazione  a  &#13;
 carico  dell'enfiteuta. Ora, dispone l'art. 52, secondo comma, di detta  &#13;
 legge che "pronunziata  l'espropriazione,  tutti  i  diritti  (compreso  &#13;
 quello  di  diretto  dominio)  si possono far valere non più sul fondo  &#13;
 espropriato,  ma  sull'indennità  che  lo  rappresenta".  Ne  consegue  &#13;
 pertanto  che,  nella specie l'Opera pia proprietaria-concedente assume  &#13;
 la posizione di  terzo  nel  procedimento  espropriativo  svoltosi  nei  &#13;
 riguardi  dell'enfiteuta  Cianciotta;  e  che  i diritti dell'Ente sono  &#13;
 trasferiti ad ogni effetto sull'indennità, in base  all'art.  9  della  &#13;
 legge Sila, applicabile come si è detto, anche nell'ambito della legge  &#13;
 stralcio.                                                                &#13;
     Tutte  le considerazioni finora esposte portano quindi a concludere  &#13;
 da un lato che, dei  tre  aspetti  sotto  i  quali  è  prospettata  la  &#13;
 questione   di   incostituzionalità   nell'ordinanza  della  Corte  di  &#13;
 cassazione, non può  ritenersi  fondato  il  primo,  che  riguarda  il  &#13;
 soggetto  passivo  dell'esproprio,  e dall'altro che resta superato, ai  &#13;
 fini  dell'attuale  controversia,  il  terzo  aspetto,   che   concerne  &#13;
 l'ammissibilità  o  meno  della  procedura  di scorporo, in attuazione  &#13;
 della legge stralcio, nei confronti degli enti pubblici.                 &#13;
     Occorre invece  esaminare  il  secondo  aspetto,  che  riguarda  il  &#13;
 calcolo  del  reddito  ai  fini  della  determinazione  della  quota di  &#13;
 Scorporo.                                                                &#13;
     Deduce la difesa del Cianciotta (e  non  è  contestato)  che,  nel  &#13;
 calcolo  anzidetto, sia stato illegittimamente tenuto presente tutto il  &#13;
 reddito del fondo costituito in enfiteusi, nel quale,  si  assume,  sia  &#13;
 stata  compresa  anche  la  parte  inerente  ai dominio diretto, con un  &#13;
 aumento della quota di  scorporo  in  pregiudizio  dell'enfiteuta;  sul  &#13;
 quale    graverebbe    inoltre   il   prelievo   di   una   percentuale  &#13;
 dell'indennità, corrispondente al diretto dominio.  Pure sotto  questo  &#13;
 profilo peraltro, la questione non può ritenersi fondata.               &#13;
     Se  infatti,  come  si  è in precedenza osservato, la procedura di  &#13;
 esproprio, in attuazione della legge stralcio, ha come soggetto passivo  &#13;
 esclusivamente l'enfiteuta, essendo questo parificato al proprietario a  &#13;
 tutti gli effetti preveduti dalla legge di riforma,  e  se  l'enfiteuta  &#13;
 percepisce  tutti  i  frutti  del  fondo,  come  nella  specie  non  è  &#13;
 contestato, è logico ritenere che del loro ammontare si dovesse  tener  &#13;
 conto ai fini del calcolo complessivo del reddito dominicale. Nel quale  &#13;
 calcolo  non  può essere compreso il canone (nella specie stabilito in  &#13;
 una somma di denaro), che  nel  sistema  previsto  dal  Codice  civile,  &#13;
 costituisce  oggetto  di  una  delle  obbligazioni dell'enfiteuta, come  &#13;
 corrispettivo del godimento del fondo (art. 960, primo comma).           &#13;
     Si deve  pertanto  concludere  che  la  questione  di  legittimità  &#13;
 costituzionale  del  decreto  del  Presidente  della  Repubblica del 19  &#13;
 novembre  1952,  n.  2290,  proposta  nell'ordinanza  della  Corte   di  &#13;
 cassazione non può ritenersi fondata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   non  fondata  la  questione,  proposta  dalla  Corte  di  &#13;
 cassazione con ordinanza  del  12  febbraio  1958,  sulla  legittimità  &#13;
 costituzionale  del decreto del Presidente della Repubblica 19 novembre  &#13;
 1952, n. 2290, in relazione all'art. 4 della legge 21 ottobre 1950,  n.  &#13;
 841, contenente norme per l'espropriazione, bonifica, trasformazione ed  &#13;
 assegnazione  dei terreni ai contadini, e con riferimento agli articoli  &#13;
 76 e 77 della Costituzione.                                              &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, l'8 luglio 1959.                                 &#13;
                                   GAETANO  AZZARITI  - GIUSEPPE CAPPI -  &#13;
                                   TOMASO PERASSI - GASPARE AMBROSINI  -  &#13;
                                   ERNESTO  BATTAGLINI - MARIO COSATTI -  &#13;
                                   FRANCESCO   PANTALEO    GABRIELI    -  &#13;
                                   GIUSEPPE  CASTELLI  AVOLIO - ANTONINO  &#13;
                                   PAPALDO  -  NICOLA  JAEGER  -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
