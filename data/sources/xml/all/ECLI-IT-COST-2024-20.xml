<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2024</anno_pronuncia>
    <numero_pronuncia>20</numero_pronuncia>
    <ecli>ECLI:IT:COST:2024:20</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BARBERA</presidente>
    <relatore_pronuncia>Giovanni Pitruzzella</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Pitruzzella</redattore_pronuncia>
    <data_decisione>24/01/2024</data_decisione>
    <data_deposito>19/02/2024</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>cessata materia del contendere</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta da:&#13;
 Presidente: Augusto Antonio BARBERA; Giudici : Franco MODUGNO, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI, Giovanni PITRUZZELLA, Antonella SCIARRONE ALIBRANDI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 36, comma 1-bis, del decreto-legge 4 maggio 2023, n. 48 (Misure urgenti per l'inclusione sociale e l'accesso al mondo del lavoro), convertito, con modificazioni, nella legge 3 luglio 2023, n. 85, promosso dalla Regione Campania con ricorso notificato il 1° settembre 2023, depositato in cancelleria l'8 settembre 2023, iscritto al n. 27 del registro ricorsi 2023 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 40, prima serie speciale, dell'anno 2023, la cui trattazione è stata fissata per l'adunanza in camera di consiglio del 23 gennaio 2024.&#13;
 Visto l'atto di costituzione del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 24 gennaio 2024 il Giudice relatore Giovanni Pitruzzella;&#13;
 deliberato nella camera di consiglio del 24 gennaio 2024.&#13;
 Ritenuto che, con ricorso depositato l'8 settembre 2023 (iscritto al n. 27 del registro ricorsi 2023), la Regione Campania ha impugnato l'art. 36, comma 1-bis, del decreto-legge 4 maggio 2023, n. 48 (Misure urgenti per l'inclusione sociale e l'accesso al mondo del lavoro), convertito, con modificazioni, nella legge 3 luglio 2023, n. 85, per violazione degli artt. 117, commi terzo e quarto, 118 e 119 della Costituzione, nonché del principio di leale collaborazione di cui agli artt. 5 e 120 Cost.;&#13;
 che la disposizione impugnata stabilisce quanto segue: «Al fine di incrementare la sicurezza del trasporto marittimo è istituito, nello stato di previsione del Ministero delle infrastrutture e dei trasporti, un fondo con una dotazione di 1 milione di euro per l'anno 2023 e di 2 milioni di euro per ciascuno degli anni dal 2024 al 2026, destinato all'erogazione di contributi alle imprese armatoriali per la formazione iniziale del personale impiegato sulle navi, con particolare riferimento alle figure professionali mancanti di sezioni di coperta, macchine, cucina e camera. Con decreto del Ministro delle infrastrutture e dei trasporti, di concerto con il Ministro dell'economia e delle finanze, sono definiti le modalità di presentazione delle domande per l'accesso al contributo, i criteri per la selezione delle stesse, le spese ammissibili, le modalità di erogazione del contributo, le modalità di verifica, controllo e rendicontazione delle spese, nonché le cause di decadenza e revoca. I contributi di cui al primo periodo sono assegnati alle imprese armatoriali con decreto del Ministro delle infrastrutture e dei trasporti sulla base delle attività di formazione rendicontate, ivi compresi gli oneri per l'acquisizione delle relative certificazioni, qualora si proceda all'assunzione di almeno il 60 per cento del personale formato. I corsi di formazione sono svolti avvalendosi dei centri di addestramento autorizzati dal Comando generale del Corpo delle capitanerie di porto. Agli oneri derivanti dall'attuazione del presente comma, pari a 1 milione di euro per l'anno 2023 e a 2 milioni di euro annui per ciascuno degli anni dal 2024 al 2026, si provvede mediante corrispondente riduzione dell'autorizzazione di spesa di cui all'articolo 3, comma 33, della legge 24 dicembre 2007, n. 244»;&#13;
 che la ricorrente censura, in primo luogo, il secondo periodo del citato art. 36, comma 1-bis, «nella parte in cui - con riferimento al Fondo istituito per l'erogazione di contributi alle imprese armatoriali per la formazione iniziale del personale impiegato sulle navi [...] - demanda la definizione delle modalità di presentazione delle domande per l'accesso al contributo, i criteri per la selezione delle stesse, le spese ammissibili, le modalità di erogazione del contributo, le modalità di verifica, controllo e rendicontazione delle spese, nonché le cause di decadenza e revoca ad un decreto del Ministro delle Infrastrutture e dei Trasporti, di concerto con il Ministro dell'Economia e delle Finanze, senza prevedere alcuna forma di coinvolgimento delle Regioni»;&#13;
 che la ricorrente censura, inoltre, il terzo e il quarto periodo del citato art. 36, comma 1-bis, «nella parte in cui stabilisce esso stesso - invece di prevederne la definizione previo coinvolgimento delle Regioni - una disciplina attuativa di dettaglio»;&#13;
 che, secondo la ricorrente, l'art. 36, comma 1-bis, ricadrebbe nelle materie del trasporto pubblico locale, della formazione professionale, del lavoro, delle grandi reti di trasporto e di navigazione e della tutela e sicurezza del lavoro, tutte rientranti nella competenza legislativa regionale ai sensi dell'art. 117, commi terzo e quarto, Cost.;&#13;
 che, inoltre, la giurisprudenza di questa Corte avrebbe escluso la legittimità costituzionale di finanziamenti statali a destinazione vincolata, anche a favore di soggetti privati, in materie regionali;&#13;
 che, sia nei casi di intreccio di competenze statali e regionali, sia nei casi di chiamata in sussidiarietà ai sensi dell'art. 118, primo comma, Cost., l'istituzione di un fondo statale a destinazione vincolata dovrebbe essere accompagnata dalla previsione di un raccordo con le regioni nella fase di attuazione della norma legislativa;&#13;
 che, con atto depositato il 10 ottobre 2023, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, si è costituito nel presente giudizio;&#13;
 che il resistente eccepisce l'inammissibilità del ricorso per incompleta ricostruzione del quadro normativo e ne afferma, comunque, la non fondatezza;&#13;
 che, secondo il resistente, la definizione degli standard di addestramento (e le relative certificazioni) del personale imbarcato sarebbe riconducibile alla materia della sicurezza della navigazione marittima e, pertanto, rientrerebbe nella competenza legislativa esclusiva statale ai sensi dell'art. 117, secondo comma, lettera h), Cost.&#13;
 Considerato che, con atto depositato il 18 gennaio 2024, la Regione Campania ha rinunciato al ricorso, sulla base della delibera della Giunta regionale 17 gennaio 2024, n. 22;&#13;
 che da tale delibera risulta che la rinuncia è volta a «non pregiudicare le aspettative» delle imprese armatoriali campane destinatarie di atti di concessione del contributo «e le finalità sottese all'intervento»;&#13;
 che non è pervenuta l'accettazione della rinuncia da parte del Presidente del Consiglio dei ministri;&#13;
 che, in presenza di una rinuncia non accettata dal resistente costituito, questa Corte dichiara la cessazione della materia del contendere, qualora non risulti un interesse del resistente alla decisione (ex multis, sentenze n. 229, n. 221, n. 187 e n. 118 del 2021);&#13;
 che, dunque, va dichiarata la cessazione della materia del contendere in ordine alle questioni promosse dalla Regione Campania sull'art. 36, comma 1-bis, del d.l. n. 48 del 2023, come convertito.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara cessata la materia del contendere in ordine alle questioni di legittimità costituzionale dell'art. 36, comma 1-bis, del decreto-legge 4 maggio 2023, n. 48 (Misure urgenti per l'inclusione sociale e l'accesso al mondo del lavoro), convertito, con modificazioni, nella legge 3 luglio 2023, n. 85, promosse, in riferimento agli artt. 117, commi terzo e quarto, 118 e 119 della Costituzione, nonché al principio di leale collaborazione di cui agli artt. 5 e 120 Cost., dalla Regione Campania con il ricorso indicato in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 gennaio 2024.&#13;
 F.to:&#13;
 Augusto Antonio BARBERA, Presidente&#13;
 Giovanni PITRUZZELLA, Redattore&#13;
 Roberto MILANA, Direttore della Cancelleria&#13;
 Depositata in Cancelleria il 19 febbraio 2024&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
