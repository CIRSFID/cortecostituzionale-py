<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>255</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:255</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/06/1995</data_decisione>
    <data_deposito>16/06/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 54, 420 e  484    &#13;
 del  codice  di  procedura  penale, promossi con tre ordinanze emesse    &#13;
 l'11 novembre 1994 dal giudice per le indagini preliminari presso  il    &#13;
 Tribunale  di  Cagliari  nei  procedimenti  penali  a carico di Cuccu    &#13;
 Sandro ed altri, Loddo Aristide ed altra e Parodo Angelo iscritte  ai    &#13;
 nn.  788,  789 del registro ordinanze 1994 e 6 del registro ordinanze    &#13;
 1995 e pubblicate nella Gazzetta Ufficiale della Repubblica nn.  3  e    &#13;
 4, prima serie speciale, dell'anno 1995;                                 &#13;
    Visti  gli  atti  di  intervento  del Presidente dei Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  17  maggio  1995  il  Giudice    &#13;
 relatore Francesco Guizzi;                                               &#13;
    Ritenuto che con tre decreti del 16 giugno 1994, a norma dell'art.    &#13;
 412, comma 1, del codice di procedura penale, il procuratore generale    &#13;
 presso  la  Corte d'appello di Cagliari aveva avocato, per il mancato    &#13;
 esercizio dell'azione  penale  nei  termini  di  legge,  le  indagini    &#13;
 preliminari  relative ad altrettanti procedimenti di cui era titolare    &#13;
 il procuratore della Repubblica presso il tribunale di quella città;    &#13;
      che a seguito del deposito della richiesta di rinvio a  giudizio    &#13;
 degli   indagati  nella  cancelleria  del  giudice  per  le  indagini    &#13;
 preliminari, e alla ricezione dell'avviso di fissazione  dell'udienza    &#13;
 preliminare (ai sensi dell'art. 419, comma 2, del codice di procedura    &#13;
 penale),  il procuratore generale, con tre distinti provvedimenti del    &#13;
 26 agosto 1994, aveva delegato il procuratore della Repubblica presso    &#13;
 il Tribunale di Cagliari "a partecipare all'udienza, a richiedere  il    &#13;
 rinvio   a  giudizio  degli  imputati  e  a  sostenere  l'accusa  nel    &#13;
 procedimento di primo grado", eventualmente a propria volta delegando    &#13;
 "un magistrato del suo ufficio";                                         &#13;
      che  all'udienza  preliminare,  alla  presenza   del   sostituto    &#13;
 procuratore   della  Repubblica  presso  il  Tribunale  di  Cagliari,    &#13;
 intervenuto in luogo del procuratore generale, assente nonostante  la    &#13;
 comunicazione di un primo rinvio, il giudice dell'udienza preliminare    &#13;
 -  contestata  la legittimità delle deleghe conferite - ha sollevato    &#13;
 per ciascuno dei processi, in relazione agli artt. 24, secondo comma,    &#13;
 e 112 della Costituzione, questione di costituzionalità degli  artt.    &#13;
 54,  420 e 484 del codice di procedura penale, "laddove non prevedono    &#13;
 un mezzo per porre rimedio alla mancata partecipazione all'udienza di    &#13;
 un pubblico ministero legittimato ad processum";                         &#13;
      che nella legislazione vigente non  esisterebbe  un  potere  del    &#13;
 procuratore  generale  presso  la  Corte  d'appello  di  delegare  un    &#13;
 magistrato di altro ufficio di procura  a  svolgere  le  funzioni  di    &#13;
 pubblico  ministero nelle indagini preliminari e nei giudizi di primo    &#13;
 grado, costituendo eccezioni alla regola le ipotesi  di  sostituzione    &#13;
 previste dagli artt. 570 del codice di rito e 72 del regio decreto 30    &#13;
 gennaio  1941,  n.  12,  in  materia di ordinamento giudiziario, come    &#13;
 novellato dall'art. 22 del d.P.R. 22 settembre 1988, n. 449;             &#13;
      che l'indebito esercizio del preteso potere di delega  anche  in    &#13;
 un  caso  non  consentito  espressamente  dalla legge dovrebbe essere    &#13;
 rilevato, ai sensi dell'art. 420 del codice di procedura  penale,  in    &#13;
 sede  di verifica della costituzione delle parti, là dove il giudice    &#13;
 dell'udienza preliminare  -  rilevata  la  presenza  di  un  pubblico    &#13;
 ministero   privo   di   legittimazione  -  non  potrebbe  proseguire    &#13;
 l'attività processuale per il difetto di  costituzione  della  parte    &#13;
 pubblica;                                                                &#13;
      che  una  tale  difficoltà non sarebbe risolta sia dall'art. 74    &#13;
 del regio decreto n. 12 del 1941, che si limiterebbe a  enunciare  il    &#13;
 principio  generale  della  partecipazione  (necessaria) del pubblico    &#13;
 ministero alle  udienze  penali,  sia  dall'art.  54  del  codice  di    &#13;
 procedura  penale,  che  regolerebbe soltanto i contrasti negativi di    &#13;
 tipo "orizzontale" tra pubblici ministeri,  mentre  nella  specie  si    &#13;
 verserebbe in un caso di contrasto "verticale";                          &#13;
      che  non  potrebbe  essere  utilizzato altro strumento, quale la    &#13;
 declaratoria di nullità dell'udienza preliminare (come richiesto dal    &#13;
 pubblico   ministero   intervenuto),   perché   ciò   facendo    si    &#13;
 verificherebbe  una  regressione  del procedimento a quello stadio in    &#13;
 cui finirebbe  per  essere  riprodotto  l'atto  nullo,  dal  titolare    &#13;
 dell'ufficio,   convinto  della  sua  regolarità  (onde  il  circolo    &#13;
 vizioso);                                                                &#13;
      che per l'inesistenza di uno strumento risolutore del  contrasto    &#13;
 "verticale"   tra   pubblici   ministeri   si  configurerebbero  come    &#13;
 costituzionalmente illegittimi gli artt. 54, 420 e 484 del codice  di    &#13;
 procedura  penale, non prevedendo alcun "mezzo per porre rimedio alla    &#13;
 mancata  partecipazione  all'udienza  preliminare  di   un   pubblico    &#13;
 ministero legittimato ad processum";                                     &#13;
      che    vi    sarebbe   lesione   del   diritto   di   difesa   e    &#13;
 dell'obbligatorietà dell'azione penale (art. 24,  secondo  comma,  e    &#13;
 112  della  Costituzione), con il conseguente rischio di prescrizione    &#13;
 del reato;                                                               &#13;
      che tale  anomalia  sarebbe  superabile  conferendo  al  giudice    &#13;
 dell'udienza  preliminare  il  potere  di  rilevare, e denunciare, il    &#13;
 conflitto di tipo "verticale" alla Corte di cassazione (competente ai    &#13;
 sensi  dell'art.  32  del  codice  di   procedura   penale),   ovvero    &#13;
 attribuendo  a uno degli uffici contendenti il potere di sollevare il    &#13;
 conflitto;                                                               &#13;
      che la questione sarebbe rilevante per la parte che investe  gli    &#13;
 artt.  54  e  420  del codice di procedura penale (che attengono alle    &#13;
 fasi sino all'udienza preliminare) e soltanto in "via  secondaria  ed    &#13;
 eventuale" per la parte concernente l'art. 484 relativo alla fase del    &#13;
 giudizio;                                                                &#13;
      che  in  tutti  e tre i giudizi è intervenuto il Presidente del    &#13;
 Consiglio, rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  concludendo  per  l'inammissibilità  e,  in  subordine,  per    &#13;
 l'infondatezza della questione;                                          &#13;
      che ad  avviso  dell'Avvocatura  il  giudice  del  Tribunale  di    &#13;
 Cagliari   avrebbe   preso   in   esame  una  situazione  "del  tutto    &#13;
 patologica",   insuscettibile   di   apprezzamento   in    sede    di    &#13;
 costituzionalità (ordinanza n. 182 del 1992);                           &#13;
      che ove si seguisse l'impostazione del rimettente si giungerebbe    &#13;
 a  sospettare  di illegittimità costituzionale tutte le disposizioni    &#13;
 del codice di rito riguardanti la presenza del pubblico ministero  in    &#13;
 udienza  sulla  base dell'eventualità di una sua mancata presenza in    &#13;
 essa;                                                                    &#13;
      che, invece, tali situazioni - ipotesi estreme anche  sul  piano    &#13;
 disciplinare  -  imporrebbero al giudice il differimento dell'udienza    &#13;
 per la mancata presenza del pubblico ministero;                          &#13;
      che,  nel  caso  di  specie,  l'avvenuta  avocazione  ai   sensi    &#13;
 dell'art.  51,  comma 2, del codice di procedura penale, radicherebbe    &#13;
 in capo al  procuratore  generale  presso  la  Corte  di  appello  la    &#13;
 competenza già attribuita al procuratore della Repubblica;              &#13;
      che  la  mancata  presenza dell'avocante all'udienza preliminare    &#13;
 avrebbe dovuto comportare un nuovo differimento  e  la  comunicazione    &#13;
 dell'assenza alle autorità competenti sul piano disciplinare;           &#13;
      che,  in  via  alternativa, si potrebbe prospettare la soluzione    &#13;
 indicata (con riferimento al  codice  previgente)  da  una  pronuncia    &#13;
 della  Cassazione,  secondo  la  quale il sostituto procuratore della    &#13;
 Repubblica,  impropriamente  delegato  in   un   processo,   dovrebbe    &#13;
 proseguire  l'attività  di  accusa  nel  giudizio senza incorrere in    &#13;
 un'ipotesi di nullità, mentre l'irregolarità, commessa in  sede  di    &#13;
 delega, avrebbe rilievo disciplinare;                                    &#13;
    Considerato  che le questioni sollevate sono identiche e pertanto,    &#13;
 previa la loro riunione, vanno trattate congiuntamente;                  &#13;
      che, a differenza  dell'altra  disposizione  denunciata  (l'art.    &#13;
 420),  l'impugnativa  degli  artt.  54  e 484 del codice di procedura    &#13;
 penale va dichiarata inammissibile per difetto della  rilevanza,  non    &#13;
 riguardando  la  fase  dell'udienza preliminare nella quale, e per la    &#13;
 quale, si controverte;                                                   &#13;
      che, pertanto, la questione residua attiene, in riferimento agli    &#13;
 artt. 24, secondo comma, e 112 della Costituzione, all'art.  420  del    &#13;
 codice  di  rito,  in  quanto non prevede "un mezzo per porre rimedio    &#13;
 alla mancata partecipazione  all'udienza  di  un  pubblico  ministero    &#13;
 legittimato ad processum " (recte: "ad causam"), in base a una delega    &#13;
 illegittima  conferita  al  procuratore  della  Repubblica  presso il    &#13;
 tribunale dal  procuratore  generale  presso  la  Corte  d'appello  a    &#13;
 seguito di avocazione ex art. 412, comma 1;                              &#13;
      che  l'assenza reiterata del Procuratore generale all'udienza (e    &#13;
 la  presenza  di  un  sostituto  procuratore  della  Repubblica,  non    &#13;
 legittimato) avrebbe determinato una grave situazione processuale non    &#13;
 risolvibile con le attuali previsioni del codice di rito;                &#13;
      che  a  differenza  della  difesa  dell'imputato,  la quale vede    &#13;
 innestarsi una disciplina  pubblicistica  sull'istituto  privatistico    &#13;
 della  rappresentanza,  l'organizzazione  degli uffici della pubblica    &#13;
 accusa è regolata  dalla  "legge  di  ordinamento  giudiziario",  di    &#13;
 esclusiva  competenza  statale,  nel  cui ambito trovano soluzione le    &#13;
 questioni  che,  come  quella  in  esame,  devono  restare  confinate    &#13;
 "all'interno  di  quelle  situazioni patologiche" estranee al sistema    &#13;
 processuale "e, quindi, insuscettibili  di  apprezzamento  in  questa    &#13;
 sede" (ordinanza n. 182 del 1992);                                       &#13;
      che,   pertanto,   la  questione  va  dichiarata  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti gli artt. 96, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi:                                                     &#13;
       a)   dichiara   inammissibile   la  questione  di  legittimità    &#13;
 costituzionale degli artt. 54 e 484 del codice di  procedura  penale,    &#13;
 sollevata,  in  riferimento agli artt. 24, secondo comma, e 112 della    &#13;
 Costituzione, dal giudice per le indagini preliminari  del  Tribunale    &#13;
 di Cagliari con le ordinanze in epigrafe;                                &#13;
       b)  dichiara  la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 420  del  codice  di  procedura    &#13;
 penale  sollevata, in riferimento agli artt. 24, secondo comma, e 112    &#13;
 della Costituzione, dal medesimo giudice con le indicate ordinanze.      &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 13 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 16 giugno 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
