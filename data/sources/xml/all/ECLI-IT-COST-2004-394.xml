<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>394</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:394</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ONIDA</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia>Franco Bile</redattore_pronuncia>
    <data_decisione>13/12/2004</data_decisione>
    <data_deposito>17/12/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 4 febbraio 2004 relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dall'on. Vittorio Sgarbi nei confronti della dott.ssa Ilda Boccassini, promosso dal Tribunale di Brescia, seconda sezione penale, con ricorso depositato il 31 maggio 2004 ed iscritto al n. 264 del registro ammissibilità conflitti. &#13;
    Udito nella camera di consiglio del 17 novembre 2004 il Giudice relatore Franco Bile.     &#13;
    Ritenuto che, nel corso di un processo penale a carico del deputato Vittorio Sgarbi – imputato del delitto di diffamazione commesso nel corso della puntata del 1° aprile 1999 del programma televisivo Sgarbi quotidiani, diffuso dall'emittente Canale 5, in danno del magistrato Ilda Boccassini, sostituto procuratore della Repubblica presso il Tribunale di Milano – il Tribunale di Brescia, in composizione monocratica, seconda sezione penale, con ricorso del 20 aprile 2004, ha proposto conflitto di attribuzioni tra poteri dello Stato avverso la delibera adottata dalla Camera dei deputati il 4 febbraio 2004, con la quale è stato dichiarato che i fatti contestati al deputato medesimo concernono opinioni espresse da questo nell'esercizio delle sue funzioni, ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    che il Tribunale ricorrente ricorda che in tale occasione il deputato Sgarbi, conduttore del menzionato programma televisivo – riferendosi alla vicenda relativa al “caso Sharifa” e richiamando il contenuto di una propria intervista rilasciata a “Il Giornale”, in relazione alla quale Ilda Boccassini aveva esercitato azione risarcitoria nei suoi confronti – aveva, tra l'altro, affermato: «Se il mio assistente di studio vede Sharifa con un bambino crede che siano mamma e figlio, la Boccassini invece ha pensato che Sharifa fosse una mercante di minori. Il sospetto prima di tutto. Non se se avete capito: è un problema di alterazione dello sguardo. Si, i magistrati hanno una percezione diversa della realtà»; &#13;
    che, affermata la propria legittimazione a far valere mediante conflitto di attribuzione la menomazione della sua sfera di attribuzioni derivante dall'adozione di una deliberazione d'insindacabilità in mancanza di qualsivoglia nesso funzionale tra le opinioni espresse e la funzione parlamentare, il Tribunale osserva come la prerogativa costituzionale tuteli l'indipendente svolgimento delle attività proprie del parlamentare e quelle ad esse strettamente connesse, e non costituisca in suo favore una posizione di privilegio della quale possa avvalersi allorché svolga attività politica o eserciti comunque il diritto alla libera manifestazione del proprio pensiero, non essendovi ragione alcuna perché in tale veste egli non operi su un piano di parità con ogni altra persona e nel rispetto dei limiti sanciti dall'ordinamento giuridico; &#13;
    che il Tribunale ritiene che la mera inerenza a temi giudiziari dell'argomento trattato e l'attualità del dibattito sulla giustizia, promosso dallo stesso deputato Sgarbi, sono circostanze che, da sole, non possono supportare la tesi della insindacabilità delle opinioni da lui espresse ai sensi dell'art. 68 Cost.: e, seppure la vicenda afferente al caso giudiziario “Sharifa” avesse già formato oggetto di interpellanze parlamentari, nessuna di quelle messe a disposizione nel processo dalla difesa dell'imputato risulta avere quale firmatario l'imputato medesimo, e comunque esse hanno ad oggetto la formulazione di mere doglianze circa il corretto esercizio da parte del pubblico ministero Boccassini dei suoi compiti, con conseguente sollecitazione del ministro competente all'esercizio dei suoi poteri disciplinari; &#13;
    che, dunque, il Tribunale di Brescia, sospeso il giudizio, «propone conflitto di attribuzione in ordine al corretto uso del potere di decidere sulla sussistenza dei presupposti di applicabilità dell'art. 68, comma 1, Cost., come esercitato dalla Camera dei Deputati con la delibera adottata in data 4 febbraio 2004», e «chiede l'annullamento di detta deliberazione della Camera dei Deputati per violazione dell'art. 68 della Costituzione». &#13;
    Considerato che in questa fase la Corte è chiamata, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a deliberare preliminarmente, senza contraddittorio, se il ricorso sia ammissibile in quanto esista la materia di un conflitto la cui risoluzione spetti alla sua competenza, in riferimento ai requisiti soggettivo e oggettivo indicati nel primo comma dello stesso art. 37; &#13;
    che, sotto l'aspetto soggettivo, il Tribunale di Brescia, in composizione monocratica, è legittimato a sollevare conflitto di attribuzione tra poteri dello Stato, quale organo competente a dichiarare definitivamente – nel procedimento penale del quale esso è investito – la volontà del potere cui appartiene, in ragione dell'esercizio di funzioni giurisdizionali svolte in posizione di piena indipendenza, costituzionalmente garantita; &#13;
    che anche la Camera dei deputati, che ha adottato la deliberazione di insindacabilità delle opinioni espresse da un proprio membro, è legittimata ad essere parte del conflitto costituzionale, essendo competente a dichiarare definitivamente la volontà del potere che essa impersona, in relazione all'applicabilità della prerogativa stessa; &#13;
    che, per quanto riguarda il profilo oggettivo del conflitto, il Tribunale di Brescia lamenta la lesione delle proprie attribuzioni, costituzionalmente garantite, in conseguenza dell'adozione, da parte della Camera dei deputati, di una deliberazione che ha affermato – in modo da esso ritenuto arbitrario, perché non corrispondente ai criteri stabiliti dalla Costituzione – l'insindacabilità delle opinioni espresse da un parlamentare, a norma dell'art. 68, primo comma, della Costituzione; &#13;
    che, pertanto, esiste la materia di un conflitto costituzionale di attribuzione, la cui risoluzione spetta alla competenza di questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara ammissibile, a norma dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dal Tribunale di Brescia, in composizione monocratica, seconda sezione penale, nei confronti della Camera dei deputati, con l'atto introduttivo indicato in epigrafe; &#13;
    dispone: &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al ricorrente Tribunale di Brescia, in composizione monocratica, seconda sezione penale; &#13;
    b) che, a cura del ricorrente, l'atto introduttivo e la presente ordinanza siano notificati alla Camera dei deputati, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati nella cancelleria di questa Corte entro il termine di venti giorni dalla notificazione, a norma dell'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 dicembre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Franco BILE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 17 dicembre 2004. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
