<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>100</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:100</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>26/03/2003</data_decisione>
    <data_deposito>28/03/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 448, comma 1, del codice di procedura penale, promosso, nell'ambito di un procedimento penale, dal Tribunale di Bergamo con ordinanza del 7 dicembre 2001, iscritta al n. 160 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2002. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 15 gennaio 2003 il Giudice relatore Guido Neppi Modona. &#13;
  &#13;
    Ritenuto che con ordinanza del 7 dicembre 2001 il Tribunale di Bergamo ha sollevato, in riferimento agli artt. 3 (non enunciato formalmente), 111 (secondo e quarto comma) e 112 della Costituzione, questione di legittimità costituzionale dell'art. 448, comma 1, secondo periodo, del codice di procedura penale, nella parte in cui «consente al solo imputato di chiedere (e ottenere) nonostante il dissenso del pubblico ministero l'applicazione della pena nella fase preliminare al dibattimento»;  &#13;
    che il rimettente premette che prima del compimento delle formalità di apertura del dibattimento alcuni imputati avevano reiterato la richiesta di applicazione della pena già formulata, negli stessi termini, davanti al giudice dell'udienza preliminare, in relazione alla quale il pubblico ministero aveva espresso il proprio dissenso; &#13;
    che ad avviso del giudice a quo il comma 1 dell'art. 448 cod. proc. pen., come modificato dall'art. 34, comma 1, della legge 19 dicembre 1999, n. 479 - consentendo all'imputato di rinnovare, prima della dichiarazione di apertura del dibattimento, la richiesta di applicazione della pena precedentemente formulata e non accolta per dissenso del pubblico ministero, e prevedendo che il giudice, valutata la fondatezza della richiesta, pronunci immediatamente sentenza - impone al giudice stesso di applicare all'imputato la pena da lui indicata anche senza il consenso del pubblico ministero; &#13;
    che tale disciplina snaturerebbe l'istituto del patteggiamento, caratterizzato, nell'originario "impianto" del codice del 1988, dall'accordo fra le parti, in quanto la richiesta di patteggiamento e il consenso costituiscono, così come precisato dalla giurisprudenza di legittimità, «due manifestazioni di volontà unilaterali convergenti, rivolte al giudice, provenienti dall'imputato e dal pubblico ministero»; &#13;
    che prima della riforma introdotta dalla legge n. 479 del 1999 la giurisprudenza riteneva pacificamente che il giudizio sulle ragioni del dissenso del pubblico ministero potesse essere compiutamente formulato soltanto all'esito dell'istruzione dibattimentale e che quindi il giudice potesse valutare la fondatezza della richiesta dell'imputato di applicazione della pena solo dopo la chiusura del dibattimento;  &#13;
    che la disciplina censurata travolgerebbe i principi che connotano l'istituto del patteggiamento e si porrebbe in contrasto con l'art. 111, secondo e quarto comma, Cost., in quanto consentirebbe all'imputato di chiedere e di ottenere una pena non concordata con il pubblico ministero e imporrebbe al giudice di applicare tale pena non a seguito del giudizio, e quindi del contraddittorio tra le parti, ma "immediatamente" e sulla base dei soli atti raccolti nel fascicolo del pubblico ministero;  &#13;
    che nel patteggiamento, «istituto che più tipicamente esprime la condizione di parità delle parti», verrebbe così «irragionevolmente introdotta una norma di disparità, lesiva altresì del principio costituzionale della formazione della prova nel dibattimento»; &#13;
    che l'applicazione in limine litis della pena richiesta dall'imputato, anche senza il consenso del pubblico ministero, determinerebbe inoltre «un vero e proprio esautoramento della parte pubblica dall'esercizio dell'azione penale», violando il principio di cui all'art. 112 Cost., che investe non solo il «momento iniziale e genetico», ma tutto lo «sviluppo e concreto esercizio» dell'azione penale; &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata infondata. &#13;
    Considerato che il rimettente dubita, in riferimento agli artt. 3, 111, secondo e quarto comma, e 112 della Costituzione, della legittimità costituzionale dell'art. 448, comma 1, secondo periodo, del codice di procedura penale, come modificato dall'art. 34, comma 1, della legge 19 dicembre 1999, n. 479, in quanto, nel consentire all'imputato di rinnovare, prima della dichiarazione di apertura del dibattimento, la richiesta di applicazione della pena già rivolta al giudice dell'udienza preliminare e non accolta a causa del dissenso del pubblico ministero, imporrebbe al giudice del dibattimento, valutata la fondatezza della richiesta, di pronunciare immediatamente sentenza di applicazione della pena anche in mancanza del consenso del pubblico ministero; &#13;
    che questa Corte ha avuto recentemente occasione di affermare, in relazione ad una questione in cui la disciplina censurata era assunta quale tertium comparationis (ordinanza n. 426 del 2001, depositata in data successiva all'ordinanza di rimessione), che l'art. 448, comma 1, cod. proc. pen., ove interpretato nel senso che al giudice del dibattimento è riconosciuto il potere di accogliere la richiesta di patteggiamento in limine litis anche in assenza del consenso del pubblico ministero, «si porrebbe in contrasto con la struttura negoziale che caratterizza l'istituto dell'applicazione della pena, in quanto verrebbe ad espropriare il pubblico ministero del suo potere di concorrere, in condizioni di parità con l'imputato, alla scelta del rito, e sacrificherebbe l'esercizio del suo diritto alla prova in dibattimento»;  &#13;
    che è infatti conforme all'essenza dell'istituto che «il potere di pronunciare sentenza di applicazione della pena malgrado il dissenso del pubblico ministero possa essere esercitato, ex art. 448, comma 1, quarto periodo, cod. proc. pen., solo dopo la chiusura del dibattimento, quando il giudice è posto in grado di valutare, in esito alle risultanze dell'istruzione dibattimentale, se le ragioni del dissenso del pubblico ministero erano giustificate»; &#13;
    che questa Corte ha inoltre rilevato che tale interpretazione logico-sistematica non è in contrasto con il tenore letterale della disposizione censurata, che «si limita a prevedere la facoltà dell'imputato di rinnovare la richiesta di applicazione della pena prima dell'apertura del dibattimento, mediante una formulazione che non esclude che la richiesta debba essere corredata del consenso del pubblico ministero, secondo quanto disposto in via generale dall'art. 444 cod. proc. pen., cui rinvia il primo periodo del comma 1 dell'art. 448»; &#13;
    che la questione, sollevata sulla base di un erroneo presupposto interpretativo, deve pertanto essere dichiarata manifestamente infondata.  &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 448, comma 1, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 111, secondo e quarto comma, e 112 della Costituzione, dal Tribunale di Bergamo, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 marzo 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 28 marzo 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
