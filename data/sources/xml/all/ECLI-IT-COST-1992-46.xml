<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>46</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:46</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1992</data_decisione>
    <data_deposito>05/02/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 85, 87, primo    &#13;
 comma,  89,  commi  terzo  e  quinto,  90, commi secondo e terzo, del    &#13;
 d.P.R. 29 gennaio 1958, n. 645 (Approvazione del  testo  unico  delle    &#13;
 leggi  sulle  imposte  dirette)  promosso  con ordinanza emessa il 28    &#13;
 novembre 1990  dalla  Commissione  tributaria  di  secondo  grado  di    &#13;
 Pescara  sul  ricorso proposto da Giosia Iachetti contro l'Intendenza    &#13;
 di Finanza di Pescara, iscritta al n. 540 del registro ordinanze 1991    &#13;
 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  34,  prima    &#13;
 serie speciale, dell'anno 1991;                                          &#13;
    Udito  nella  camera  di consiglio del 18 dicembre 1991 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto che la Commissione tributaria di secondo grado di Pescara    &#13;
 - nel corso di un giudizio promosso da un pensionato dello Stato, che    &#13;
 chiedeva la restituzione delle somme trattenutegli sull'indennità di    &#13;
 buonuscita Enpas  a  titolo  d'imposta  di  ricchezza  mobile  -  con    &#13;
 ordinanza  28  novembre  1990  ha sollevato questione di legittimità    &#13;
 costituzionale degli artt. 85, 87, primo comma, 89,  terzo  e  quinto    &#13;
 comma, 90, secondo e terzo comma, del d.P.R. 29 gennaio 1958, n. 645;    &#13;
      che,  secondo  il  giudice a quo, detti articoli, esentando - di    &#13;
 regola - le indennità di fine rapporto dall'imposta di ricchezza mobile soltanto se di ammontare non superiore ad un  milione  di  lire,    &#13;
 contrasterebbero  con  gli  artt.  3 e 53 Cost., per la disparità di    &#13;
 trattamento che ne deriverebbe, in relazione  alla  totale  esenzione    &#13;
 prevista:  a) dall'art. 124 del R.D.L. 4 ottobre 1935, n. 1827, conv.    &#13;
 nella legge 6 aprile  1936,  n.  1155,  per  le  indennità  di  fine    &#13;
 rapporto  erogate  dall'Inps;  b) dall'art. 35 della legge 11 gennaio    &#13;
 1943, n. 138, per i  premi  di  fine  servizio  e  le  indennità  di    &#13;
 anzianità erogate dall'Inam; c) dall'art. 2 del D.L. 16 luglio 1947,    &#13;
 n.  708,  conv.  nella  legge  29  novembre  1952,  n.  2388,  per le    &#13;
 indennità di fine rapporto erogate ai lavoratori  dello  spettacolo;    &#13;
 d)  dall'articolo  unico  della  legge  4  maggio  1951,  n.  497, in    &#13;
 relazione alle liquidazioni del  personale  dipendente  dall'Istituto    &#13;
 nazionale di previdenza dei pubblici trasporti; e) dall'art. 10 della    &#13;
 legge  9  novembre 1955, n. 1122, in relazione a quelle del personale    &#13;
 dipendente dall'Istituto di previdenza dei giornalisti  italiani  "G.    &#13;
 Amendola";  f) dalla legge 2 aprile 1958, n. 377, per le liquidazioni    &#13;
 del personale dipendente dalle esattorie e ricevitorie delle  imposte    &#13;
 dirette;                                                                 &#13;
    Considerato  che  l'art.  124 del R.D.L. 4 ottobre 1935, n. 1827 -    &#13;
 contrariamente a quanto immotivatamente affermato dal giudice a quo -    &#13;
 nell'esentare  dall'imposta  di  ricchezza   mobile   i   trattamenti    &#13;
 previdenziali  ivi  previsti,  non  ha  ad oggetto indennità di fine    &#13;
 rapporto (o altre a queste  equiparabili),  come  risulta  dalla  sua    &#13;
 lettera,  nonché  dalla  interpretazione sistematica derivante dalla    &#13;
 inserzione di tale articolo in un testo legislativo che  non  prevede    &#13;
 l'erogazione  di  indennità connesse alla cessazione del rapporto di    &#13;
 impiego;                                                                 &#13;
      che l'art. 35 della l. 11 gennaio 1943, n.  138,  l'art.  2  del    &#13;
 d.l. 16 luglio 1947, n. 708, l'articolo unico della l. 4 maggio 1951,    &#13;
 n. 497, l'art. 10 della l. 9 novembre 1955, n. 1122 e l'art. 76 della    &#13;
 l.  2 aprile 1958, n. 377, richiamano le esenzioni previste dall'art.    &#13;
 124 del R.D.L. 4 ottobre 1935, n. 1827 e quindi, a  loro  volta,  non    &#13;
 esentano  dall'imposta  di  R.M.  indennità  di buonuscita o altre a    &#13;
 questa equiparabili;                                                     &#13;
      che, di conseguenza, il giudice a quo lamenta una differenza  di    &#13;
 trattamento tributario che palesemente non sussiste;                     &#13;
    Visti gli artt. 26, secondo comma, della l. 11 marzo 1953, n. 87 e    &#13;
 9,  secondo comma, delle Norme integrative per i giudizi davanti alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  degli  artt.  85,  87, comma primo, 89, commi terzo e    &#13;
 quinto, 90 commi secondo e terzo, del d.P.R. 29 gennaio 1958, n.  645    &#13;
 (Approvazione  del  testo  unico  delle leggi sulle imposte dirette),    &#13;
 sollevata in riferimento agli artt. 3 e 53 della  Costituzione  dalla    &#13;
 Commissione  tributaria  di secondo grado di Pescara, con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 22 gennaio 1992.                              &#13;
                       Il Presidente: BORZELLINO                          &#13;
                        Il redattore: PESCATORE                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 5 febbraio 1992.                         &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
