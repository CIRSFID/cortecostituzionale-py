<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>10</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:10</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Vincenzo Trimarchi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/01/1974</data_decisione>
    <data_deposito>23/01/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente, &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 19 del r.d.l.  &#13;
 21   febbraio   1938,   n.   246  (Disciplina  degli  abbonamenti  alle  &#13;
 radioaudizioni), modificato dall'art. 1 del d.l.C.P.S. 5 ottobre  1947,  &#13;
 n.   1208   (Aumento  delle  sanzioni  pecuniarie  comminate  da  leggi  &#13;
 tributarie e finanziarie), promosso con ordinanza emessa il 17 febbraio  &#13;
 1971 dal tribunale di Catania nel procedimento penale a  carico  di  Di  &#13;
 Grazia  Salvatore,  iscritta  al  n.  144 del registro ordinanze 1971 e  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  119  del  12  &#13;
 maggio 1971.                                                             &#13;
     Udito  nella  camera  di  consiglio dell'8 novembre 1973 il Giudice  &#13;
 relatore Vincenzo Michele Trimarchi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Salvatore Di Grazia, imputato della  contravvenzione  di  cui  agli  &#13;
 artt.  1  e  2  del  r.d.l.  21 febbraio 1938, n. 246 (disciplina degli  &#13;
 abbonamenti alle radioaudizioni)  e  successive  modificazioni,  punita  &#13;
 dall'art.   19  dello  stesso  r.d.,  modificato  dall'articolo  1  del  &#13;
 d.l.C.P.S. 5 ottobre 1947, n. 1208,  perché  deteneva  un  apparecchio  &#13;
 televisivo  per  uso  privato  senza  avere  corrisposto  il  canone di  &#13;
 abbonamento per il periodo dal 1   gennaio  1969  al  30  giugno  1970,  &#13;
 veniva  citato  a  comparire  davanti  al tribunale di Catania. In sede  &#13;
 dibattimentale l'imputato eccepiva  l'incostituzionalità  delle  dette  &#13;
 norme;  ed  il  pubblico  ministero  che  in  un  primo  momento si era  &#13;
 dichiarato favorevole, concludeva per l'infondatezza dell'eccezione.     &#13;
     Il  tribunale,  con  ordinanza  del  17  febbraio  1971,  sollevava  &#13;
 questione  di  legittimità  costituzionale dell'art. 19 del r.d.l.  n.  &#13;
 246 del 1938, come modificato dall'art. 1 del d.l.C.P.S.  n.  1208  del  &#13;
 1947,  denunciandone  il  contrasto  con  l'art.  3 della Costituzione.  &#13;
 Osservava che lo Stato, riservatisi  in  via  esclusiva  i  servizi  di  &#13;
 telecomunicazioni, aveva dato in concessione quelli di radiotelevisione  &#13;
 alla  RAI;  e  che  questa si presentava come una società privata, che  &#13;
 esercitava in concessione un pubblico servizio. E  richiamava,  quindi,  &#13;
 la  giurisprudenza  di  questa  Corte con la quale l'indicato regime è  &#13;
 stato  riconosciuto  legittimo  sotto  il  profilo  costituzionale,  in  &#13;
 riferimento agli artt. 3 e 43 della Costituzione.                        &#13;
     Rilevava, però, l'esistenza di una  disparità  di  trattamento  a  &#13;
 favore   della   RAI   e   nei  confronti  di  altre  società  private  &#13;
 concessionarie di pubblici servizi quali la SIP, l'ENEL  e  l'ENI,  per  &#13;
 ciò    che  alla  prima  e  non  anche  alle  altre,  a garanzia della  &#13;
 riscossione dei canoni di utenza, è assicurata una tutela  penale  per  &#13;
 cui  l'abbonato  "viene  perseguito  non  solo  con  le  normali  forme  &#13;
 amministrative e civili ma anche mediante il promovimento di  un'azione  &#13;
 penale, costituendo reato finanziario il mancato pagamento del canone".  &#13;
 Mentre  tutte  le indicate società concessionarie di pubblico servizio  &#13;
 trovano nella  legge  la  disciplina  dell'organizzazione  e  struttura  &#13;
 interne  e dei rapporti con i terzi, ed in provvedimenti amministrativi  &#13;
 la determinazione dei canoni di utenza, solo  la  RAI  gode  di  quella  &#13;
 particolare normativa di favore.                                         &#13;
     L'ordinanza   emessa  in  udienza  veniva  notificata  al  pubblico  &#13;
 ministero, al Di Grazia ed al Presidente del Consiglio dei  ministri  e  &#13;
 regolarmente  comunicata e pubblicata (nella Gazzetta Ufficiale n.  119  &#13;
 del 12 maggio 1971).                                                     &#13;
     Davanti a questa Corte non si costituiva nessuna delle parti e  non  &#13;
 spiegava intervento il Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  tribunale  di  Catania ritiene che violi il principio di  &#13;
 eguaglianza l'art. 19 del r.d.l. 21 febbraio 1938, n.  246  (disciplina  &#13;
 degli  abbonamenti  alle  radioaudizioni)  modificato  dall'art.  1 del  &#13;
 d.l.C.P.S. 5 ottobre 1947, n. 1208 (aumento delle  sanzioni  pecuniarie  &#13;
 comminate da leggi tributarie e finanziarie), che prevede come reato la  &#13;
 mancata  corresponsione  del  canone  di  abbonamento  da  parte di chi  &#13;
 detenga uno o più apparecchi radioriceventi o impianto aereo od  altro  &#13;
 dispositivo  comunque  atto  od  adattabile  alla  radioricezione, e la  &#13;
 punisce con la pena  dell'ammenda  da  lire  duecentocinquanta  a  lire  &#13;
 cinquemila.                                                              &#13;
     La  norma  denunciata  assicura  alla  RAI  una  tutela  penale nei  &#13;
 confronti dell'abbonato, e di una  tutela  analoga  invece  non  godono  &#13;
 altre "società private" come la SIP, l'ENEL e l'ENI che sono anch'esse  &#13;
 titolari di concessioni di pubblico servizio.                            &#13;
     2.  -  Con  la  sentenza  n.  81 del 1963, questa Corte, chiamata a  &#13;
 pronunciarsi  sulla  questione  di  legittimità   costituzionale,   in  &#13;
 riferimento (tra l'altro) all'art. 3 della Costituzione, degli artt. 1,  &#13;
 2 e 13 del citato r.d.l. n. 246 del 1938 che, secondo il giudice a quo,  &#13;
 stante l'esistenza di sanzioni penali per le inadempienze solo a carico  &#13;
 dei  radioutenti,  avrebbero dato vita ad una ingiustificata disparità  &#13;
 di trattamento di costoro e della  RAI,  ha  ritenuto  non  fondata  la  &#13;
 questione, sembrando ad essa Corte evidente che "le posizioni della RAI  &#13;
 e  degli  utenti  si  presentano  in  modo  diverso" e che nel rapporto  &#13;
 "domina l'elemento giuspubblicistico,  che  attribuisce  alla  RAI  una  &#13;
 situazione  giuridica, quale concessionaria di un servizio di interesse  &#13;
 pubblico, diversa da quella degli altri soggetti del rapporto,  privati  &#13;
 utenti del servizio stesso".                                             &#13;
     Ora la questione è sollevata attraverso la messa a raffronto da un  &#13;
 canto  del  trattamento  giuridico  della RAI e dall'altro di quello di  &#13;
 date  altre  società  private  titolari  di  concessioni  di  pubblico  &#13;
 servizio (quali, per il giudice a quo, la SIP, l'ENEL e l'ENI) e sempre  &#13;
 in  relazione  alla  tutela penale accordata nei confronti degli utenti  &#13;
 inadempienti.                                                            &#13;
     La questione così proposta non è fondata.                          &#13;
     3.  -  Anche  se,  come  ricorda  il  tribunale  di  Catania, è da  &#13;
 ravvisare nei confronti di società private titolari di concessioni  di  &#13;
 pubblico  servizio,  quali  la  RAI  e  la SIP, l'esistenza di sfere di  &#13;
 situazioni giuridiche analoghe per la fonte, e cioè  anche  se  è  da  &#13;
 rilevare  che codeste due società trovano in leggi la disciplina delle  &#13;
 loro organizzazioni e strutture interne, e dei loro  rapporti  con  gli  &#13;
 utenti, e che i canoni di utenza radiotelevisivi e quelli di utenza del  &#13;
 servizio   telefonico   urbano   ed   interurbano  sono  stabiliti  con  &#13;
 provvedimenti ministeriali,  non  si  può    non  constatare  come  la  &#13;
 specifica  disciplina  dei rapporti delle due società con i rispettivi  &#13;
 utenti, sul punto in esame, sia giustificatamente diversa.               &#13;
     Va subito precisato che non è in discussione la conformità o meno  &#13;
 al principio di eguaglianza della tutela penale accordata alla RAI  nei  &#13;
 confronti  degli utenti, in sé e per sé considerata, questione la cui  &#13;
 fondatezza, per altro,  sarebbe  implicitamente  esclusa  nella  citata  &#13;
 sentenza n. 81 del 1963 di questa Corte.                                 &#13;
     Nelle    due    ipotesi    messe    a    raffronto,    procedendosi  &#13;
 all'individuazione  degli  interessi  presi   in   considerazione   dal  &#13;
 legislatore,  è  facile  accorgersi  che  le  situazioni  di  fatto  e  &#13;
 giuridiche a cui sul piano della logica si riportano rispettivamente la  &#13;
 presenza e l'assenza della tutela penale de qua, non sono eguali.        &#13;
     Ed infatti:                                                          &#13;
     a) nel caso della RAI, all'utente basta la detenzione  (acquisibile  &#13;
 liberamente  dal  mercato)  di  un  apparecchio  atto o adattabile alla  &#13;
 ricezione delle emissioni perché il godimento  del  servizio  pubblico  &#13;
 possa  aver  ed  abbia  luogo,  e  di contro per l'esercente sussistono  &#13;
 difficoltà di controllo; e nel caso della  SIP,  invece,  allo  stesso  &#13;
 fine  occorrono  l'installazione  presso  il singolo utente di apposito  &#13;
 impianto ad opera della società stessa ed il collegamento di esso  con  &#13;
 la  rete  urbana, ed ogni prestazione dell'esercente è controllabile e  &#13;
 quantitativamente determinabile;                                         &#13;
     b) nel primo caso, il diritto  al  canone  di  abbonamento  non  è  &#13;
 assistito   da   efficaci   forme   di   autotutela   né   di   fronte  &#13;
 all'inadempimento dell'utente, la società  può  in  fatto  sospendere  &#13;
 singolarmente  il  servizio, e nel secondo caso, invece, la società da  &#13;
 sé di fronte al  mancato  adempimento  da  parte  dell'utente,  ha  il  &#13;
 diritto  di  sospendere il servizio telefonico, di provvedere al ritiro  &#13;
 del materiale installato presso  l'utente  stesso  e  di  risolvere  il  &#13;
 contratto di abbonamento, oltre che quello di rivalersi del suo credito  &#13;
 sulle  somme  anticipate  dall'abbonato per comunicazioni interurbane o  &#13;
 per qualsiasi titolo;                                                    &#13;
     c) non sono infine di scarso rilievo  la  differenza  che  sussiste  &#13;
 nella  strutturazione  del  procedimento  di  riscossione del canone di  &#13;
 abbonamento, stante che  quello  per  le  radiodiffusioni  deve  essere  &#13;
 versato  direttamente ad uffici statali, ed in generale il fatto che il  &#13;
 rapporto  tra  la  RAI  ed  i  radioutenti  è  regolato  da   principi  &#13;
 pubblicistici.                                                           &#13;
     La   tutela   penale   in   favore   della  RAI  appare,  pertanto,  &#13;
 sufficientemente giustificata, e  data  la  rilevata  differenza  delle  &#13;
 situazioni  prese in considerazione dal legislatore, non è irrazionale  &#13;
 la mancata estensione  di  quella  tutela  all'altra  società  privata  &#13;
 concessionaria di pubblico servizio.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 19 del r.d.l. 21 febbraio  1938,  n.  246  (disciplina  degli  &#13;
 abbonamenti  alle radioaudizioni) modificato dall'art. 1 del d.l.C.P.S.  &#13;
 5 ottobre 1947, n. 1208 (aumento delle sanzioni pecuniarie comminate da  &#13;
 leggi tributarie e finanziarie), questione  sollevata,  in  riferimento  &#13;
 all'art. 3 della Costituzione, dal tribunale di Catania con l'ordinanza  &#13;
 indicata in epigrafe.                                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'11 gennaio 1974.         &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ-  LUIGI  OGGIONI  -  ANGELO DE  &#13;
                                   MARCO  -  ERCOLE  ROCCHETTI  -   ENZO  &#13;
                                   CAPALOZZA    -    VINCENZO    MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI.                        &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
