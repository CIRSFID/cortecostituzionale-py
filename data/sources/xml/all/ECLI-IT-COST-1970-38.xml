<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>38</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:38</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Luigi Oggioni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1970</data_decisione>
    <data_deposito>20/03/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  degli artt.   545,  &#13;
 quarto comma, e 553  del  codice  di  procedura  civile,  promosso  con  &#13;
 ordinanza  emessa  il  21  giugno  1968  dal  pretore  di  Pinerolo nel  &#13;
 procedimento di esecuzione vertente tra Mauro Mario e Socratini Barreca  &#13;
 Michelina, iscritta al n. 184 del registro ordinanze 1968 e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 261 del 12 ottobre 1968.    &#13;
     Udito  nella  camera  di  consiglio del 10 febbraio 1970 il Giudice  &#13;
 relatore Luigi Oggioni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza del 21 giugno 1968 emessa nel corso del  procedimento  &#13;
 di   esecuzione  promosso  da  Mauro  Mario  contro  Socratini  Barreca  &#13;
 Michelina,  il  pretore  di  Pinerolo   ha   sollevato   questione   di  &#13;
 legittimità  costituzionale dell'art. 545, quarto comma, del codice di  &#13;
 procedura civile in relazione al terzo  comma  dello  stesso  articolo,  &#13;
 nella  parte  in cui, in ordine ai crediti non alimentari "assoggetta a  &#13;
 pignoramento la quota fissa di un  quinto  dello  stipendio  dovuto  da  &#13;
 privati,  anziché  quella  quota  variabile  che  il  pretore  ritenga  &#13;
 pignorabile, come invece è consentito dal terzo  comma  citato  per  i  &#13;
 crediti alimentari".                                                     &#13;
     Secondo  il pretore, la norma denunziata violerebbe il principio di  &#13;
 eguaglianza dell'art. 3 della Costituzione perché potrebbe incidere in  &#13;
 misura più sensibile sulle retribuzioni meno alte e prossime al limite  &#13;
 minimo, che non su quelle di maggior importo; dando  luogo,  in  questo  &#13;
 caso,  ad  una  ingiusta  sperequazione  a  danno  dei  lavoratori meno  &#13;
 abbienti:  senza alcuna possibilità di riduzione discrezionale a quota  &#13;
 graduata e variabile.                                                    &#13;
     Il  giudice  a  quo  ha  sollevato  contestualmente  questione   di  &#13;
 legittimità  costituzionale  dell'art.  553  del  codice  di procedura  &#13;
 civile "considerato in collegamento col citato quarto  comma  dell'art.  &#13;
 545  dello  stesso  codice, nella parte in cui, imponendo al pretore di  &#13;
 assegnare, in pagamento del credito, la somma  pignorata  nella  misura  &#13;
 richiesta,  senza  possibilità di graduazione, sarebbe suscettibile di  &#13;
 ridurre la retribuzione del debitore al di sotto del minimo (salariale)  &#13;
 necessario per garantire a lui ed alla sua famiglia un'esistenza libera  &#13;
 e  dignitosa"  violando  così  il  principio  stabilito  in  proposito  &#13;
 dall'art.   36 della Costituzione, che sarebbe applicabile anche a quei  &#13;
 casi di insufficienza  della  retribuzione  derivante  da  decurtazioni  &#13;
 dovute a cause estranee al rapporto di lavoro.                           &#13;
     Nessuna delle parti si è costituita in giudizio.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  La  prima  questione  sollevata con l'ordinanza di rinvio in  &#13;
 riferimento all'art. 3 della  Costituzione,  riguarda  l'articolo  545,  &#13;
 quarto  comma,  del codice di procedura civile, in quanto, riconoscendo  &#13;
 pignorabili, dai creditori in genere, nella misura fissa di un  quinto,  &#13;
 gli  stipendi  ed  i  salari dovuti all'esecutato, la norma verrebbe ad  &#13;
 incidere in misura più sensibile sulle basse retribuzioni in confronto  &#13;
 a quelle di maggiore importo.                                            &#13;
     La questione non è fondata.                                         &#13;
     Questa  Corte,  con  la  sentenza  n.  20  del  1968,  si  è  già  &#13;
 pronunciata  nel  senso che il limite di un quinto pignorabile è stato  &#13;
 adottato dal legislatore in base ad un potere razionalmente  esercitato  &#13;
 anche  secondo  gli  insegnamenti  della  scienza  economica. Tra i due  &#13;
 criteri, in astratto ammissibili, della proporzionalità rispetto  alla  &#13;
 misura  della  retribuzione ovvero della progressività, per cui chi ha  &#13;
 una retribuzione più bassa viene  colpito,  in  proporzione,  con  una  &#13;
 minore  incidenza  di  misura,  è stato preferito il secondo criterio,  &#13;
 che, secondo il suespresso giudizio della Corte, risulta immune da ogni  &#13;
 indizio di incostituzionalità.                                          &#13;
     Nell'ordinanza di rinvio non sono addotti, su questo punto, nuovi e  &#13;
 diversi argomenti, validi per addivenire a differente decisione.         &#13;
     L'ordinanza prospetta, tuttavia, sotto ulteriore profilo, l'ipotesi  &#13;
 di incostituzionalità dell'art.  545,  quarto  comma,  del  codice  di  &#13;
 procedura  civile  se  confrontato  col  terzo comma (pignorabilità in  &#13;
 misura variabile per i crediti alimentari).                              &#13;
     La disparità di trattamento, a seconda della natura  del  credito,  &#13;
 si  risolverebbe, stando all'ordinanza, in violazione dell'art. 3 della  &#13;
 Costituzione,  in  quanto  non  sarebbe  escluso  che,  in  particolari  &#13;
 circostanze,  un credito per alimenti, che pur meriterebbe in ogni caso  &#13;
 maggior tutela, possa essere garantito con  pignoramento  inferiore  al  &#13;
 quinto,  mentre  un  credito non alimentare sarebbe, sempre e comunque,  &#13;
 meglio tutelato mediante pignoramento mai inferiore al quinto.           &#13;
     L'ordinanza, così ponendo  la  questione,  non  considera  che  il  &#13;
 particolare trattamento disposto per i crediti alimentari dipende dalla  &#13;
 peculiare  natura  dell'obbligo  corrispondente,  condizionato  sia  al  &#13;
 bisogno dell'alimentando, sia alla capacità economica  dell'obbligato,  &#13;
 quale residua, dopo soddisfatti i suoi propri debiti.                    &#13;
     La  variabilità  dell'obbligo  giustifica  la  variabilità  della  &#13;
 misura esecutiva per effettuarne l'adempimento.   Nemmeno sotto  questo  &#13;
 profilo sussiste alcuna irrazionale differenziazione di trattamento.     &#13;
     2.  -  L'ordinanza,  nel  quadro  generale della stessa materia, ha  &#13;
 sollevato anche una particolare questione riguardante l'art.  553  cod.  &#13;
 proc.  civ.  (assegnazione di crediti) come connesso e conseguenziale a  &#13;
 quanto sopra esaminato, inserendo anche al riguardo  l'ipotesi  che  il  &#13;
 sistema  di  legge  possa  dar  luogo  a  contrasto con l'art. 36 della  &#13;
 Costituzione.                                                            &#13;
     La Corte osserva, in primo luogo, che, essendo la  norma  dell'art.  &#13;
 553  complementare  della  precedente di cui all'art. 545, valgono, per  &#13;
 escluderne l'incostituzionalità, le stesse considerazioni suesposte.    &#13;
     In secondo luogo osserva che il principio dettato dall'art. 36 vale  &#13;
 per regolare il rapporto di lavoro nell'ambito  suo  proprio  attinente  &#13;
 alla  sua  conclusione  ed attuazione e non si estende alle conseguenze  &#13;
 contingenti ed eventuali che possano essere occasionate da  eventi  che  &#13;
 da quel rapporto prescindano.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 545, quarto comma, e 553 del  codice  di  procedura  civile  &#13;
 sollevata,  con l'ordinanza di cui in epigrafe, dal pretore di Pinerolo  &#13;
 in riferimento agli artt. 3 e 36 della Costituzione.                     &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, l' 11 marzo 1970.          &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
