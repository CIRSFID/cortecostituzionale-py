<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2009</anno_pronuncia>
    <numero_pronuncia>52</numero_pronuncia>
    <ecli>ECLI:IT:COST:2009:52</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMIRANTE</presidente>
    <relatore_pronuncia>Paolo Maria Napolitano</relatore_pronuncia>
    <redattore_pronuncia>Paolo Maria Napolitano</redattore_pronuncia>
    <data_decisione>11/02/2009</data_decisione>
    <data_deposito>18/02/2009</data_deposito>
    <tipo_procedimento>GIUDIZIO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>improcedibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 15 febbraio 2006 (doc. IV-ter, n. 17) relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Raffaele Iannuzzi nei confronti di Giancarlo Caselli e Gioacchino Natoli, promosso con ricorso del Giudice per le indagini preliminari del Tribunale di Milano, notificato il 17 gennaio 2007, depositato in cancelleria il 10 ottobre 2008 ed iscritto al n. 15 del registro conflitti tra poteri dello Stato 2006, fase di merito. &#13;
    Visto l'atto di costituzione del Senato della Repubblica; &#13;
      udito nella camera di consiglio del 28 gennaio 2009 il Giudice relatore Paolo Maria Napolitano. &#13;
    Ritenuto che, con atto del 7 luglio 2006, il Giudice per le indagini preliminari del Tribunale di Milano, nel corso di un procedimento penale a carico del senatore Raffaele Iannuzzi – imputato del reato di diffamazione aggravata per avere offeso la reputazione di Giancarlo Caselli e Gioacchino Natoli, negli articoli di stampa pubblicati a sua firma, rispettivamente, sul quotidiano «Il Giornale» del 17 novembre 2004, con il titolo «Ecco come i pentiti dovevano uccidere Canale», e sul settimanale «Panorama» in data 25 novembre 2004, con il titolo «A Palermo giustizia è (quasi) fatta» – ha sollevato conflitto di attribuzione tra poteri dello Stato nei confronti del Senato della Repubblica in relazione alla deliberazione adottata nella seduta del 15 febbraio 2006 (Doc. IV-ter, n. 17) con la quale l'Assemblea, approvando la proposta della Giunta delle elezioni e delle immunità parlamentari, ha dichiarato che i fatti per i quali è in corso il procedimento penale di cui sopra concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, con conseguente insindacabilità ai sensi dell'art. 68, primo comma, della Costituzione;  &#13;
    che il ricorrente è dell'avviso che la deliberazione del Senato della Repubblica, la quale si basa sulla asserita sussistenza del «nesso funzionale» tra il contenuto degli articoli di stampa ed alcuni atti tipici della funzione parlamentare compiuti dal senatore Iannuzzi, consistenti in particolare nella presentazione di taluni disegni di legge aventi ad oggetto le problematiche connesse alla «gestione di coloro che collaborano con la giustizia», sia in contrasto con i canoni interpretativi stabiliti dalla giurisprudenza della Corte, atteso che essa non conterrebbe alcun elemento concreto da cui poter desumere la sussistenza di una corrispondenza sostanziale tra il contenuto degli articoli in questione e le opinioni già espresse dal senatore Iannuzzi in specifici atti parlamentari, non essendo sufficiente una mera comunanza di tematiche e un generico riferimento alla rilevanza dei fatti pubblici;  &#13;
    che, pertanto, il citato Giudice per le indagini preliminari ha promosso il giudizio per conflitto di attribuzione nei confronti del Senato della Repubblica in quanto, a suo avviso, la condotta addebitabile al senatore Iannuzzi, astrattamente idonea, nella sua specificità e gravità, ad integrare un illecito, esula dall'esercizio delle funzioni parlamentari, non presentando alcun legame con atti parlamentari, neppure nell'accezione più ampia;  &#13;
    che il conflitto è stato dichiarato ammissibile da questa Corte con ordinanza n. 420 del 2006, depositata il 14 dicembre 2006 e notificata al Senato della Repubblica, a cura del ricorrente, il 17 gennaio 2007; &#13;
    che il ricorrente ha provveduto al prescritto deposito degli atti presso la cancelleria di questa Corte solamente il 10 ottobre 2008; &#13;
    che, con atto depositato il 5 febbraio 2007, si è, nel frattempo, costituito in giudizio il Senato della Repubblica, chiedendo che il  ricorso per conflitto sia dichiarato inammissibile, improcedibile e comunque infondato;  &#13;
    che, nel merito, la difesa del Senato sostiene che il conflitto sarebbe infondato, in quanto gli atti parlamentari riferibili al senatore, menzionati nella relazione della Giunta delle elezioni e delle immunità parlamentari del Senato (vale a dire Disegno di legge n. 2292: «Istituzione di una commissione Parlamentare di inchiesta sulla gestione di coloro che collaborano con la giustizia», depositato sin dal 25 giugno 2003, e Documento XXII, n. 25: «Proposta di inchiesta parlamentare del Senato sulla gestione di coloro che collaborano con la giustizia, depositato il 19 gennaio 2004»), dimostrerebbero l'esistenza del nesso funzionale tra le dichiarazioni oggetto del procedimento penale e l'attività parlamentare svolta dal senatore Raffaele Iannuzzi.   &#13;
    Considerato che il ricorso introduttivo è stato notificato al Senato della Repubblica, unitamente all'ordinanza che lo ha dichiarato ammissibile, in data 17 gennaio 2007 e che gli atti sono stati depositati presso la cancelleria di questa Corte, con plico spedito a mezzo del servizio postale, il 10 ottobre 2008, ossia oltre il termine di venti giorni dalla notificazione, previsto dall'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale;  &#13;
    che, in conformità alla costante giurisprudenza di questa Corte (si vedano, da ultimo, le ordinanze n. 430 del 2008 e n. 253 del 2007), tale deposito deve considerarsi tardivo, essendo il predetto termine perentorio;  &#13;
    che, pertanto, il giudizio deve essere dichiarato improcedibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE  &#13;
    dichiara improcedibile il giudizio per conflitto di attribuzione tra poteri dello Stato promosso dal Giudice dell'udienza preliminare presso il Tribunale di Milano nei confronti del Senato della Repubblica, con il ricorso indicato in epigrafe.  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, palazzo della Consulta, l'11 febbraio 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Paolo Maria NAPOLITANO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 febbraio 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
