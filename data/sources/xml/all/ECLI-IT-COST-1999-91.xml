<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>91</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:91</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/03/1999</data_decisione>
    <data_deposito>23/03/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi di legittimità costituzionale: a) dell'art. 1, comma 4,    &#13;
 del d.-l. legge 21 ottobre 1996, n. 536 (Misure per  il  contenimento    &#13;
 della spesa farmaceutica e la rideterminazione del tetto di spesa per    &#13;
 l'anno  1996),  convertito  nella  legge 23 dicembre 1996, n. 648; b)    &#13;
 degli artt. 2, ultimo inciso, 3, commi 3 e 4, 4, e 5,  comma  3,  del    &#13;
 d.-l.  17  febbraio  1998,  n. 23 (Disposizioni urgenti in materia di    &#13;
 sperimentazioni cliniche  in  campo  oncologico  e  altre  misure  in    &#13;
 materia  sanitaria),  convertito,  con  modificazioni,  nella legge 8    &#13;
 aprile 1998, n. 94; c) del combinato disposto degli artt. 3, comma 4,    &#13;
 del decreto-legge n. 23 del 1998 e 1, comma 4, del  decreto-legge  n.    &#13;
 536  del  1996;  d)  del preambolo apposto al decreto-legge n. 23 del    &#13;
 1998; giudizi promossi con ordinanze emesse il 10 febbraio  1998  dal    &#13;
 pretore  di  Rieti, il 3 marzo 1998 (cinque ordinanze) dal pretore di    &#13;
 Catania, il 27 febbraio 1998 dal pretore di Modica, il 4  marzo  1998    &#13;
 (due  ordinanze)  dal  pretore  di  Caltagirone, il 16 marzo 1998 dal    &#13;
 pretore di Torino, il 14 marzo 1998 dal pretore di Catania  e  il  15    &#13;
 aprile  1998  dal  pretore  di  Lecce,  sezione distaccata di Maglie,    &#13;
 iscritte rispettivamente ai nn. 204, 279, 280, 281,  282,  283,  302,    &#13;
 338,  339,  346,  380  e 438 del registro ordinanze 1998 e pubblicate    &#13;
 nella Gazzetta Ufficiale della Repubblica, prima serie speciale,  nn.    &#13;
 14, 17, 18, 20, 21, 23 e 25 dell'anno 1998;                              &#13;
   Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del  13  gennaio  1999  il  giudice    &#13;
 relatore Francesco Guizzi;                                               &#13;
   Ritenuto  che  in  un  procedimento  cautelare  promosso  ai  sensi    &#13;
 dell'art.  700 del codice di procedura civile il  pretore  di  Rieti,    &#13;
 con  ordinanza  del  10  febbraio  1998, ha sollevato, in riferimento    &#13;
 all'art.   32   della   Costituzione,   questione   di   legittimità    &#13;
 costituzionale  dell'art.   1, comma 4, del d.-l. 21 ottobre 1996, n.    &#13;
 536 (Misure  per  il  contenimento  della  spesa  farmaceutica  e  la    &#13;
 rideterminazione  del  tetto  di  spesa  per l'anno 1996), convertito    &#13;
 nella legge 23 dicembre 1996, n. 648, perché detta norma, negando al    &#13;
 malato  indigente l'erogazione gratuita di farmaci innovativi durante    &#13;
 la sperimentazione, vulnererebbe il bene primario della salute;          &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,   rappresentato   e  difeso  dall'Avvocatura  dello  Stato,    &#13;
 ricordando  che  il   Governo,   successivamente   all'ordinanza   di    &#13;
 rimessione,   ha   adottato   il   d.-l.  17  febbraio  1998,  n.  23    &#13;
 (Disposizioni urgenti in materia di sperimentazioni cliniche in campo    &#13;
 oncologico e altre misure  in  materia  sanitaria),  convertito,  con    &#13;
 modificazioni,  nella  legge  8 aprile 1998, n. 94; motivo per cui si    &#13;
 dovrebbero restituire gli atti al giudice a quo per  un  nuovo  esame    &#13;
 della rilevanza;                                                         &#13;
     che  il  Tribunale  di  Catania, con cinque ordinanze emesse il 3    &#13;
 marzo 1998, ha sollevato, in riferimento agli artt. 3, 32,  70  e  77    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art.  2, comma 1, ultimo inciso, del citato decreto-legge n.  23    &#13;
 del  1998,  denunciando una irragionevole discriminazione a danno dei    &#13;
 farmaci del "multitrattamento Di Bella";                                 &#13;
     che secondo il Collegio  rimettente  vi  sarebbe,  altresì,  una    &#13;
 disparità  ingiustificata  fra i malati terminali selezionati per la    &#13;
 sperimentazione, per i quali la somministrazione è gratuita,  e  gli    &#13;
 altri che non vi partecipano;                                            &#13;
     che  ne  deriverebbe  la violazione degli articoli 3, 32, 70 e 77    &#13;
 della Costituzione;                                                      &#13;
     che è intervenuto il  Presidente  del  Consiglio  dei  Ministri,    &#13;
 rappresentato   e  difeso  dall'Avvocatura  dello  Stato,  nel  senso    &#13;
 dell'inammissibilità e, comunque, dell'infondatezza;                    &#13;
     che, investito di un ricorso ai sensi dell'art. 700 del codice di    &#13;
 procedura civile, il pretore di Modica, con ordinanza  emessa  il  27    &#13;
 febbraio  1998, ha sollevato, in riferimento agli articoli 3, 24 e 32    &#13;
 della  Costituzione,   questione   di   legittimità   costituzionale    &#13;
 dell'art.  3,  comma  4, e dell'art. 4 del citato decreto-legge n. 23    &#13;
 del   1998,   perché   tali   disposizioni   determinerebbero    una    &#13;
 inammissibile  disparità  di trattamento fra i pazienti, comprimendo    &#13;
 il diritto alla salute del cittadino e impedendo, al contempo, che la    &#13;
 tutela di esso possa  attuarsi  attraverso  il  ricorso  agli  organi    &#13;
 giurisdizionali;                                                         &#13;
     che  analoghe  censure  sono mosse dal pretore di Caltagirone, il    &#13;
 quale solleva anch'egli, con due ordinanze, questione di legittimità    &#13;
 costituzionale degli artt. 3, comma 4, e 4 del  decreto-legge  n.  23    &#13;
 del 1998;                                                                &#13;
     che  è  intervenuto  anche  in  tali  giudizi  il Presidente del    &#13;
 Consiglio dei Ministri, rappresentato e difeso  dall'Avvocatura dello    &#13;
 Stato, osservando come la questione concernente l'art.  3,  comma  4,    &#13;
 risulti  inammissibile,  alla  luce della sentenza di questa Corte n.    &#13;
 185 del 26 maggio 1998,  mentre  la  censura  mossa  all'art.  4  del    &#13;
 decreto-legge citato sarebbe manifestamente infondata;                   &#13;
     che  il  pretore di Torino, con ordinanza emessa il 16 marzo 1998    &#13;
 in un procedimento instaurato ai sensi dell'art. 700  del  codice  di    &#13;
 procedura    civile,   ha   sollevato   questione   di   legittimità    &#13;
 costituzionale dell'art. 1, comma 4, del  decreto-legge  n.  536  del    &#13;
 1996, degli artt.  3, commi 3 e 4, e 5, comma 3, del decreto-legge n.    &#13;
 23  del  1998, perché lesivi del "diritto alla libertà di cura" che    &#13;
 sarebbe espressione del diritto alla salute, con  lesione,  altresì,    &#13;
 dell'art. 3 della Costituzione;                                          &#13;
     che  ha  spiegato  intervento  il  Presidente  del  Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura   dello   Stato,    &#13;
 chiedendo  l'inammissibilità  della  questione concernente l'art. 3,    &#13;
 comma 4, del decreto-legge n.  23  del  1998  (conseguentemente  alla    &#13;
 declaratoria  di  illegittimità  disposta  da  questa  Corte  con la    &#13;
 sentenza n. 185 del 1998), e  l'infondatezza  di  quella  riguardante    &#13;
 l'art. 3, comma 3, e l'art. 5, comma 3, tenuto conto, in particolare,    &#13;
 che tali disposizioni sono state modificate in sede di conversione;      &#13;
     che, investito di un ricorso ai sensi dell'art. 700 del codice di    &#13;
 procedura  civile,  il pretore di Catania, con ordinanza emessa il 14    &#13;
 marzo 1998, ha sollevato, in riferimento agli  artt.  3  e  32  della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art.  2,    &#13;
 comma 1, ultima proposizione, del decreto-legge n. 23 del 1998;          &#13;
     che il Presidente del Consiglio  dei  Ministri,  rappresentato  e    &#13;
 difeso  dall'Avvocatura  dello  Stato, è intervenuto nel senso della    &#13;
 inammissibilità, alla luce della sentenza di questa Corte n. 185 del    &#13;
 1998, cui il legislatore ha dato attuazione con il  decreto-legge  16    &#13;
 giugno  1998,  n.  186, convertito, con modificazioni, nella legge 30    &#13;
 luglio 1998, n. 257;                                                     &#13;
     che,  investito  di  diciannove  ricorsi  presentati   ai   sensi    &#13;
 dell'art.    700 del codice di procedura civile, il pretore di Lecce,    &#13;
 sezione distaccata di Maglie, con ordinanza emessa il 15 aprile 1998,    &#13;
 ha sollevato,  in  riferimento  agli  artt.  2,  3,  24  e  32  della    &#13;
 Costituzione, questione di legittimità costituzionale degli artt. 2,    &#13;
 3,  4 e 5 del decreto-legge n. 23 del 1998, denunciando, altresì, il    &#13;
 contrasto  fra  le  finalità  del  decreto,  come   presentate   nel    &#13;
 preambolo, e i citati parametri costituzionali;                          &#13;
     che  secondo  il  giudice  a  quo  l'irrazionale  limitazione del    &#13;
 diritto  dei  medici  alla  libera  prescrizione  dei    farmaci   si    &#13;
 tradurrebbe  in  una lesione dell'art. 32 della Costituzione, posto a    &#13;
 salvaguardia anche della libertà di cura, con l'unico  limite  della    &#13;
 "non dannosità" dei farmaci;                                            &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato e difeso dall'Avvocatura dello Stato, nel  senso  della    &#13;
 inammissibilità, alla luce della sentenza di questa Corte n. 185 del    &#13;
 1998;                                                                    &#13;
     che  per  l'Avvocatura  dello  Stato  la  questione relativa agli    &#13;
 obblighi dei medici e dei farmacisti sarebbe irrilevante e,  in  ogni    &#13;
 caso, infondata;                                                         &#13;
   Considerato    che le ordinanze indicate vanno riunite e decise con    &#13;
 unica pronuncia, stante la connessione delle materie trattate;           &#13;
     che esse denunciano le disposizioni originarie del  decreto-legge    &#13;
 n. 23 del 17 febbraio 1998;                                              &#13;
     che  successivamente  al  loro  deposito  questa  Corte,  con  la    &#13;
 sentenza  n.   185   del   1998,   ha   dichiarato   l'illegittimità    &#13;
 costituzionale   dell'art.    2,  comma  1,  ultima  proposizione,  e    &#13;
 dell'art. 3, comma 4, del decreto-legge n. 23 del  1998,  convertito,    &#13;
 con modificazioni, nella legge n. 94 del 1998;                           &#13;
     che  il Governo ha emanato, per dare attuazione a detta sentenza,    &#13;
 il d.-l. 16 giugno 1998, n. 186, convertito, con modificazioni, nella    &#13;
 legge 30 luglio 1998, n. 257;                                            &#13;
     che il Ministro della sanità, con ordinanza del 20 novembre 1998    &#13;
 (Proseguimento  del  "multitrattamento  Di  Bella") ha disposto che i    &#13;
 pazienti i quali, sotto la responsabilità del medico curante,  hanno    &#13;
 usufruito del  multitrattamento in esame, possono proseguirlo qualora    &#13;
 la  malattia  risulti  stabile,  previa  effettuazione  di  controlli    &#13;
 clinico-strumentali   (v.   l'art.   1,   comma   3,   dell'ordinanza    &#13;
 ministeriale);                                                           &#13;
     che  gli  atti vanno quindi restituiti ai giudici rimettenti, per    &#13;
 una nuova valutazione della rilevanza della questione, alla luce  del    &#13;
 ius superveniens.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  ordina  la restituzione degli atti ai giudici    &#13;
 rimettenti indicati in epigrafe.                                         &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 12 marzo 1999.                                &#13;
                         Il Presidente: Granata                           &#13;
                          Il redattore: Guizzi                            &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 23 marzo 1999.                            &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
