<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>134</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:134</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Chiarelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/07/1963</data_decisione>
    <data_deposito>13/07/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA &#13;
 JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. &#13;
 ANTONIO MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 23 del R.  D.  &#13;
 5  giugno  1939,  n.  1016 (T.   U. delle norme per la protezione della  &#13;
 selvaggina e per l'esercizio  della  caccia),  promosso  con  ordinanza  &#13;
 emessa il 10 aprile 1962 dal Consiglio di Stato in s. g. - Sezione VI -  &#13;
 su   ricorso   di   Gentile   Mauro   ed   altri  contro  il  Ministero  &#13;
 dell'agricoltura e foreste, iscritta al n. 194 del  Registro  ordinanze  &#13;
 1962  e pubblicata nella Gazzetta Ufficiale della Repubblica n. 320 del  &#13;
 15 dicembre 1962.                                                        &#13;
     Visti  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri  e  gli  atti  di  costituzione  in giudizio dei Gentile e del  &#13;
 Ministero dell'agricoltura;                                              &#13;
     udita nell'udienza pubblica dell'8 maggio  1963  la  relazione  del  &#13;
 Giudice Giuseppe Chiarelli;                                              &#13;
     uditi  l'avv.    Massimo  Bisogni,  per  i Gentile, ed il sostituto  &#13;
 avvocato generale dello Stato  Elio  Vitucci,  per  il  Presidente  del  &#13;
 Consiglio dei Ministri e per il Ministero dell'agricoltura.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     I  signori  Mauro,  Pio,  Guido  e  Roberto Gentile, con ricorso 10  &#13;
 ottobre 1960 al Consiglio di Stato in s. g., impugnavano il decreto del  &#13;
 Ministro  dell'agricoltura  e  foreste,   pubblicato   nella   Gazzetta  &#13;
 Ufficiale  n.  218  del  6 settembre 1960, n. 3424, col quale, ai sensi  &#13;
 dell'art. 23 del T.U. delle norme per la protezione della selvaggina  e  &#13;
 per  l'esercizio  della  caccia,  si vietava la caccia nella zona della  &#13;
 Provincia di Arezzo denominata Pieve a Maiano fino al 30 giugno 1966.    &#13;
     I  ricorrenti,  col  primo   motivo,   sollevavano   questione   di  &#13;
 legittimità  costituzionale del detto art. 23 per contrasto con l'art.  &#13;
 76 della Costituzione e, col secondo, per contrasto con  l'articolo  57  &#13;
 della Costituzione.                                                      &#13;
     Il  Consiglio  di Stato, mentre disattendeva il secondo motivo, con  &#13;
 ordinanza 10 aprile 1962 sospendeva  di  giudicare  sul  primo  motivo,  &#13;
 rimettendo gli atti a questa Corte.                                      &#13;
     Nell'ordinanza,  ritenuta  la rilevanza della questione, si osserva  &#13;
 che essa non appare manifestamente infondata, in quanto il citato  art.  &#13;
 23  attribuisce  al  Ministro  un  potere  che equivale, quanto ai suoi  &#13;
 effetti, al potere di rendere  inoperanti  norme  legislative,  dettate  &#13;
 dalla  stessa  legge  sulla  caccia a garanzia dei cittadini, ossia, in  &#13;
 sostanza, di derogare ad essi  e  quindi  di  esercitare  una  funzione  &#13;
 legislativa  al  di  fuori  della  ipotesi  prevista dall'art. 76 della  &#13;
 Costituzione.                                                            &#13;
     L'ordinanza veniva notificata alle parti in causa e  al  Presidente  &#13;
 del Consiglio dei Ministri, comunicata ai Presidenti delle due Camere e  &#13;
 pubblicata nella Gazzetta Ufficiale del 15 dicembre 1962, n. 320.        &#13;
     Si  sono costituiti in giudizio i fratelli Gentile, rappresentati e  &#13;
 difesi dall'avv.  Massimo Bisogni, nonché il Presidente del  Consiglio  &#13;
 dei   Ministri   e  il  Ministero  dell'agricoltura  e  delle  foreste,  &#13;
 rappresentati e difesi dall'Avvocatura generale dello Stato.             &#13;
     Nelle loro deduzioni, depositate il 28 dicembre  1962,  i  fratelli  &#13;
 Gentile  sostengono  l'incostituzionalità  dell'art. 23 del T.U. sulla  &#13;
 caccia, in riferimento all'art. 76 della  Costituzione,  in  quanto  la  &#13;
 delega  in  esso  contenuta  è data al Ministro invece che al Governo;  &#13;
 faculta il Ministro a revocare o modificare  le  norme  di  legge  e  a  &#13;
 utilizzare  la  delega per casi singoli; non contiene la determinazione  &#13;
 dei principi e dei criteri direttivi; non è  limitata  nel  tempo  né  &#13;
 definita quanto all'oggetto.                                             &#13;
     L'Avvocatura generale dello Stato, nelle deduzioni per il Ministero  &#13;
 dell'agricoltura  e  nell'atto  di  intervento  per  il  Presidente del  &#13;
 Consiglio, esclude che nell'art. 23 possa  ravvisarsi  una  delegazione  &#13;
 legislativa,  in  quanto  le norme che in base ad esso possono emanarsi  &#13;
 non hanno valore di legge.  Esse sono una manifestazione del potere  di  &#13;
 ordinanza e costituiscono una fonte primaria di secondo grado, prevista  &#13;
 da  una  legge ordinaria e non dalla Costituzione, la quale, però, non  &#13;
 ne esclude l'ammissibilità.   Rientrando in quelle  che  un'autorevole  &#13;
 dottrina qualifica ordinanze "libere", l'atto col quale vengono emanate  &#13;
 ha  natura  meramente  amministrativa,  e  non  è  assimilabile  ad un  &#13;
 provvedimento legislativo.                                               &#13;
     La difesa del Ministero osserva  inoltre  che  non  è  contemplata  &#13;
 dalla  Costituzione  una garanzia costituzionale del diritto di caccia;  &#13;
 che l'attività venatoria è positivamente disciplinata come  interesse  &#13;
 legittimo;  che  la  vigente  disciplina  legislativa  di  essa, mentre  &#13;
 prevede    una    stagione     venatoria     ordinaria,     attribuisce  &#13;
 all'Amministrazione   la   possibilità   di   fronteggiare  situazioni  &#13;
 particolari  mediante  l'esercizio  del  potere   di   ordinanza,   che  &#13;
 istituzionalmente le appartiene.                                         &#13;
     In  via  gradata l'Avvocatura osserva che a non diverse conclusioni  &#13;
 si giungerebbe considerando l'atto del  Ministro  come  un  regolamento  &#13;
 delegato,  in quanto si riconoscerebbe sempre ad esso la natura di atto  &#13;
 amministrativo.                                                          &#13;
     L'Avvocatura conclude perché sia dichiarata infondata la sollevata  &#13;
 questione di legittimità costituzionale.                                &#13;
     In una memoria depositata il 24 aprile 1963 la difesa dei  fratelli  &#13;
 Gentile insiste sulla tesi che l'art. 23 del T.U. sulla caccia contiene  &#13;
 una   delega  legislativa,  mancante  però  dei  requisiti  prescritti  &#13;
 dall'art. 76 della  Costituzione.    Si  richiama,  quindi,  ad  alcuni  &#13;
 principi  dell'ordinamento  giuridico,  tratti  dalla Costituzione, dai  &#13;
 Codici civile e penale e dalla stessa legge sulla caccia, sui quali  si  &#13;
 baserebbe  il  diritto  soggettivo di caccia, che trova nella legge una  &#13;
 diretta regolamentazione, la quale escluderebbe  che  l'art.  23  abbia  &#13;
 potuto  attribuire alla pubblica Amministrazione un potere di ordinanza  &#13;
 libera; potere, peraltro, non previsto dalla Costituzione.  Questa  non  &#13;
 consente  neanche  - si afferma infine nella memoria, a proposito della  &#13;
 tesi  subordinata  dell'Avvocatura   -   l'esercizio   della   potestà  &#13;
 regolamentare  in  materia  interamente  disciplinata  dalla legge, non  &#13;
 potendo il legislatore fare le norme e poi declassarle.  Insiste quindi  &#13;
 nella richiesta dichiarazione d'incostituzionalità dell'art. 23  della  &#13;
 legge sulla caccia.                                                      &#13;
     Nella  discussione orale le difese della Presidenza del Consiglio e  &#13;
 dei fratelli Gentile hanno ribadito le loro tesi.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'art. 23 del R.D. 5 giugno 1939, n. 1016, non contiene, a giudizio  &#13;
 di questa Corte, una delegazione legislativa.                            &#13;
     Esso prevede l'emanazione, da parte del Ministro  dell'agricoltura,  &#13;
 di   un   provvedimento  che,  in  considerazione  del  verificarsi  di  &#13;
 determinati  interessi   pubblici   concreti,   ponga   temporaneamente  &#13;
 particolari  restrizioni  all'esercizio  della caccia, disciplinato, in  &#13;
 linea generale, dalla legge stessa.                                      &#13;
     È noto che, nel  nostro  sistema  legislativo,  l'esercizio  della  &#13;
 caccia  è  sottoposto  ad  autorizzazione  amministrativa  (licenza di  &#13;
 caccia) ed è limitato ad alcuni  periodi  dell'anno,  stabiliti  dalla  &#13;
 legge;  la  stessa legge però soggiunge, nell'articolo in discussione,  &#13;
 che il Ministro per l'agricoltura e  per  le  foreste,  "nell'interesse  &#13;
 della  protezione  di una o più specie di selvaggina, può restringere  &#13;
 il periodo di caccia o di uccellagione o vietare le  medesime,  sia  in  &#13;
 modo  generale  e  assoluto, sia per talune forme di caccia o specie di  &#13;
 selvaggina o per determinate località".                                 &#13;
     Con  tale   disposizione   viene   attribuita   al   Ministro   per  &#13;
 l'agricoltura  una  competenza,  la  quale  trova  la sua ragione nella  &#13;
 necessità che, ove si verifichino particolari circostanze che  pongano  &#13;
 in  pericolo  una o più specie di selvaggina, si possa tempestivamente  &#13;
 provvedere ad assicurare la protezione di esse, mediante quelle  misure  &#13;
 limitatrici  dell'esercizio  della  caccia  che  siano  richieste dalle  &#13;
 predette circostanze, debitamente accertate e valutate  dalla  pubblica  &#13;
 Amministrazione.                                                         &#13;
     Il  potere  in  tal modo conferito al Ministro si manifesta in atti  &#13;
 soggetti al regime degli atti amministrativi quanto  alla  causa,  alla  &#13;
 forma,  all'efficacia,  al  sindacato di legittimità.   Restando così  &#13;
 escluso che l'art. 23 contenga una  delegazione  a  emanare  atti  "che  &#13;
 abbiano  valore  di  legge  ordinaria"  (art.  77,  primo  comma, della  &#13;
 Costituzione), la disposizione in esso  contenuta  non  cade  sotto  la  &#13;
 norma dell'art. 76 della Costituzione.                                   &#13;
     Né  l'attribuzione  della  sopra indicata competenza alla pubblica  &#13;
 Amministrazione trova ostacolo nella riserva legislativa,  giacché  la  &#13;
 materia della caccia non forma oggetto, nel nostro ordinamento, di tale  &#13;
 riserva,  ed  il fatto che la legge ordinaria abbia regolato la materia  &#13;
 con maggiore o  minore  ampiezza  non  significa  che  abbia  con  ciò  &#13;
 costituito una riserva di legge.                                         &#13;
     Dalle  esposte  considerazioni  tuttavia  non deriva che dei poteri  &#13;
 conferitigli il Ministro possa fare un uso illimitato e insindacabile.   &#13;
     Questa  Corte  ha  già  avuto  occasione  di  rilevare  come   sia  &#13;
 ammissibile   che   la   legge   ordinaria   attribuisca  all'autorità  &#13;
 amministrativa l'emanazione di atti anche normativa, purché indichi  i  &#13;
 criteri  idonei  a  delimitare la discrezionalità dell'organo a cui il  &#13;
 potere viene attribuito (sentenza n. 26 del  1961  e,  precedentemente,  &#13;
 sentenza n.  103 del 1957).                                              &#13;
     Nel  caso  presente,  i  limiti  del  potere attribuito al Ministro  &#13;
 risultano dal complesso della legge sulla caccia, la quale per  ipotesi  &#13;
 e  fini  diversi da quelli considerati nell'art. 23 prevede altre forme  &#13;
 di pubblici interventi (come la costituzione di zone di ripopolamento),  &#13;
 e risultano dalla stessa formulazione del  detto  articolo.    Infatti,  &#13;
 l'ampia   enunciazione   del   possibile  contenuto  del  provvedimento  &#13;
 ministeriale ("può restringere il periodo di caccia " o vietarla;  "in  &#13;
 modo  generale  e assoluto", o "per talune forme di caccia, e specie di  &#13;
 selvaggina,  o  per  determinate  località"),  più  che  indicare  la  &#13;
 vastità del potere attribuito al Ministro, indica la necessità che il  &#13;
 provvedimento da adottare corrisponda alla precisa individuazione delle  &#13;
 esigenze   da  soddisfarete  sia  conforme  a  queste  il  contenuto  e  &#13;
 l'estensione delle imposte limitazioni.                                  &#13;
     L'emanazione del provvedimento dovrà,  pertanto,  trovare  il  suo  &#13;
 fondamento  nel  fatto  che  si siano verificate delle circostanze, non  &#13;
 specificamente previste dal legislatore,  le  quali  abbiano  provocato  &#13;
 l'insorgere  di  un  interesse  a  una  particolare  protezione  di una  &#13;
 determinata specie, o di più determinate specie, di selvaggina, e, con  &#13;
 tale interesse, abbiano determinato la necessità  di  un  sollecito  e  &#13;
 adeguato intervento.                                                     &#13;
     Palese   sarebbe,  invece,  l'illegittimità  dell'uso  del  potere  &#13;
 previsto dall'art. 23, se fosse diretto alla  tutela  di  un  interesse  &#13;
 diverso  da  quello  innanzi  indicato,  come,  ad  es., l'interesse al  &#13;
 ripopolamento di una zona o l'interesse turistico di una località.      &#13;
     Inoltre, la discrezionalità  degli  apprezzamenti  della  pubblica  &#13;
 Amministrazione   non   è   sottratta   ai   comuni  mezzi  di  tutela  &#13;
 giurisdizionale contro gli atti amministrativi.  Da ciò  consegue  che  &#13;
 dovrà  risultare  dal contesto del provvedimento ministeriale, perché  &#13;
 sia reso possibile l'esperimento di tali mezzi, lo specifico  interesse  &#13;
 all'attuazione  di  particolari  misure di protezione della selvaggina,  &#13;
 debitamente e concretamente valutato dall'Amministrazione.               &#13;
     Discende, infine, dal  carattere  della  competenza  attribuita  al  &#13;
 Ministro  e  dalla  funzione assegnata al suo intervento, la necessaria  &#13;
 temporaneità del provvedimento,  la  cui  durata  dovrà  congruamente  &#13;
 corrispondere alle peculiarità delle condizioni che ne hanno provocato  &#13;
 l'emanazione.                                                            &#13;
     È    indagine  riservata  al giudice della legittimità degli atti  &#13;
 amministrativi stabilire se nei singoli casi il  Ministro  abbia  fatto  &#13;
 retto  uso  del  potere  attribuitogli e si è mantenuto entro i limiti  &#13;
 sopra indicati.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  23 del T.U. delle norme per la protezione della selvaggina e  &#13;
 per l'esercizio della caccia, approvato con  R.D.  5  giugno  1939,  n.  &#13;
 1016, in riferimento all'art. 76 della Costituzione.                     &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 4 luglio 1963.                                &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO - ANTONINO  PAPALDO  -  NICOLA  &#13;
                                   JAEGER  - GIOVANNI CASSANDRO - BIAGIO  &#13;
                                   PETROCELLI -  ANTONIO  MANCA  -  ALDO  &#13;
                                   SANDULLI  - GIUSEPPE BRANCA - MICHELE  &#13;
                                   FRAGALI  -   COSTANTINO   MORTATI   -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE  VERZÌ.</dispositivo>
  </pronuncia_testo>
</pronuncia>
