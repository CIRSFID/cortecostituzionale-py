<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>785</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:785</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/06/1988</data_decisione>
    <data_deposito>07/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 4, lett. e),    &#13;
 della tariffa all. A al d.P.R. 26 ottobre  1972  n.  634  (Disciplina    &#13;
 dell'imposta  di registro), promosso con ordinanza emessa il 3 luglio    &#13;
 1987 dalla Commissione tributaria  di  secondo  grado  di  Udine  sul    &#13;
 ricorso  proposto  dalla  s.p.a. Furlanis Costruzioni Generali contro    &#13;
 l'Ufficio del registro di Udine, iscritta  al  n.  828  del  registro    &#13;
 ordinanze 1987 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 54, 1ª serie speciale dell'anno 1987;                                 &#13;
    Udito  nella  camera  di  consiglio  del  9 giugno 1988 il Giudice    &#13;
 relatore Francesco Saja.                                                 &#13;
    Ritenuto  che nel corso di un procedimento iniziato dalla Furlanis    &#13;
 Costruzioni  Generali  s.p.a.  ed  avente  per  oggetto  il  rimborso    &#13;
 dell'imposta di registro corrisposta, ai sensi dell'art. 4, lett. e),    &#13;
 della  tariffa  all.  A  al  d.P.R.  26  ottobre  1972  n.  634,  per    &#13;
 l'emissione  di prestiti obbligazionari, la Commissione tributaria di    &#13;
 secondo grado di Udine sollevava, con ordinanza emessa  il  3  luglio    &#13;
 1987,  questione  di  legittimità  costituzionale del citato art. 4,    &#13;
 lett. e), in riferimento agli artt. 11 e 76 Cost.;                       &#13;
      che,  ad  avviso  del giudice a quo, la norma censurata sembrava    &#13;
 violare l'art. 11 Cost. perché in contrasto con la direttiva  C.E.E.    &#13;
 n.  335  del  17  luglio 1969 (il cui art. 11 escludeva ogni forma di    &#13;
 imposizione  tributaria  sui  prestiti  contratti  sotto   forma   di    &#13;
 obbligazioni),  nonché  l'art.  76  Cost.  per  eccesso dalla delega    &#13;
 contenuta nell'art. 7  l.  9  ottobre  1971  n.  825  (che  stabiliva    &#13;
 l'adeguamento   dell'imposta   di   registro  alla  citata  direttiva    &#13;
 comunitaria).                                                            &#13;
    Considerato  che  a disciplinare la materia de qua è intervenuto,    &#13;
 in epoca anteriore alla ordinanza di rimessione, il d.P.R. 26  aprile    &#13;
 1986  n.  131,  di  approvazione  del  testo  unico  sull'imposta  di    &#13;
 registro, il quale, proprio  in  osservanza  della  citata  direttiva    &#13;
 comunitaria,   ha   eliminato   la   tassazione   dell'emissione   di    &#13;
 obbligazioni (v. art. 4 della tariffa allegata);                         &#13;
      che l'art. 79 dello stesso testo unico stabilisce l'applicazione    &#13;
 retroattiva di detta  norma  modificativa,  disponendo  che  essa  ha    &#13;
 effetto  anche  per gli atti anteriori se vi è controversia pendente    &#13;
 alla data di entrata in vigore dello  stesso  d.P.R.,  ossia  dal  1°    &#13;
 luglio 1986;                                                             &#13;
      che,   pertanto,  il  giudice  rimettente  avrebbe  dovuto  fare    &#13;
 applicazione di tale nuova normativa la quale, del resto,  disciplina    &#13;
 la  materia  superando i dubbi di costituzionalità prospettati nella    &#13;
 ordinanza;                                                               &#13;
      che   in   conclusione   la  questione  deve  essere  dichiarata    &#13;
 manifestamente inammissibile.                                            &#13;
    Visti  gli  artt.  26  legge  11  marzo 1953 n. 87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente inammissibile la questione di legittimità    &#13;
 costituzionale dell'art. 4, lett. e), della tariffa all. A al  d.P.R.    &#13;
 26  ottobre  1972  n.  634  (Disciplina  dell'imposta  di  registro),    &#13;
 sollevata, in riferimento agli artt. 11 e 76 Cost., dalla Commissione    &#13;
 tributaria  di  secondo  grado  di  Udine con l'ordinanza indicata in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 giugno 1988.                               &#13;
                          Il Presidente: SAJA                             &#13;
                           Il redattore: SAJA                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 7 luglio 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
