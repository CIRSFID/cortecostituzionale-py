<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>538</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:538</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/11/1989</data_decisione>
    <data_deposito>11/12/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. &#13;
 Luigi MENGONI, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art. 4, n. 7 del    &#13;
 decreto-legge 10 luglio  1982,  n.  429  (Norme  per  la  repressione    &#13;
 dell'evasione in materia di imposte sui redditi e sul valore aggiunto    &#13;
 e per agevolare la definizione delle pendenze in materia  tributaria)    &#13;
 convertito  in  legge  7  agosto 1982, n. 516, promossi con ordinanze    &#13;
 emesse il 10 febbraio, il 14 marzo e l'11 aprile 1989  dal  Tribunale    &#13;
 di  Forlì;  il  25  gennaio  1989  dal  Tribunale  di Isernia; il 17    &#13;
 febbraio 1989 dal Giudice istruttore presso il Tribunale di  Trieste;    &#13;
 il 1° febbraio 1989 (n. 2 ordd.) e l'8 febbraio 1989 dal Tribunale di    &#13;
 Pinerolo; il 24 gennaio, il 26 gennaio  e  il  30  gennaio  1989  dal    &#13;
 Tribunale  di  Modena,  iscritte ai nn. 238, 246, 254, 266, 267, 268,    &#13;
 269, 273, 287, 288 e 289 del registro  ordinanze  1989  e  pubblicate    &#13;
 nella  Gazzetta Ufficiale della Repubblica nn. 20, 22, 23 e 24, prima    &#13;
 serie speciale, dell'anno 1989;                                          &#13;
    Visti  gli  atti  d'intervento  del  Presidente  del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 4 ottobre 1989 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che  il  Tribunale  di Forlì, con tre ordinanze emesse,    &#13;
 rispettivamente, il 10 febbraio, il 14 marzo e l'11 aprile  del  1989    &#13;
 (Reg.  ord.  nn.  238,  246 e 273/1989); il Tribunale di Isernia, con    &#13;
 ordinanza 25 febbraio 1989 (Reg. ord. n. 254/1989); il  Tribunale  di    &#13;
 Pinerolo,  con  due  ordinanze  del 1° febbraio 1989 e con una dell'8    &#13;
 febbraio 1989 (Reg. ord. nn. 267, 268 e 269/1989);  il  Tribunale  di    &#13;
 Modena,  con tre ordinanze emesse, rispettivamente, nei giorni 24, 26    &#13;
 e 30 gennaio 1989 (Reg. ord. nn. 287,  288  e  289/1989)  nonché  il    &#13;
 Giudice  istruttore  presso il Tribunale di Trieste, con ordinanza 17    &#13;
 febbraio 1989 (Reg. ord. n. 266/1989) hanno sollevato, in riferimento    &#13;
 agli  artt.  3  e 25, secondo comma, Cost., questione di legittimità    &#13;
 costituzionale dell'art. 4, primo comma, n. 7 del decreto-  legge  10    &#13;
 luglio  1982, n. 429, come convertito in legge 7 agosto 1982, n. 516,    &#13;
 nella parte in  cui  prevede  come  elemento  costitutivo  del  reato    &#13;
 l'alterazione in misura rilevante del risultato della dichiarazione;     &#13;
      che  in  tutti  i  giudizi  è  intervenuto  il  Presidente  del    &#13;
 Consiglio  dei  ministri,  rappresentato  e  difeso   dall'Avvocatura    &#13;
 generale  dello  Stato  ed  ha  concluso  per  l'inammissibilità  o,    &#13;
 comunque, per l'infondatezza della questione;                            &#13;
    Considerato   che,   in  ragione  dell'identità  delle  questioni    &#13;
 sollevate, i relativi giudizi possono essere riuniti;                    &#13;
      che, con sentenza n. 247 del 1989, questa Corte ha dichiarato la    &#13;
 non fondatezza, in riferimento agli artt.  3  e  25,  secondo  comma,    &#13;
 Cost.,  della  questione  di legittimità costituzionale dell'art. 4,    &#13;
 primo comma, n. 7 del decreto-legge 10  luglio  1982,  n.  429,  come    &#13;
 convertito in legge 7 agosto 1982, n. 516;                               &#13;
      che le ordinanze di rimessione non prospettano argomenti nuovi o    &#13;
 diversi rispetto a quelli già esaminati dalla Corte  con  la  citata    &#13;
 decisione;                                                               &#13;
      che,   pertanto,   la   sollevata   questione   di  legittimità    &#13;
 costituzionale va dichiarata manifestamente infondata;                   &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 4, primo comma, n. 7  del  decreto-legge  10    &#13;
 luglio  1982,  n.  429  (Norme  per  la  repressione dell'evasione in    &#13;
 materia di imposte sui redditi e sul valore aggiunto e per  agevolare    &#13;
 la  definizione delle pendenze in materia tributaria) come convertito    &#13;
 in legge 7 agosto 1982, n. 516, sollevata, in riferimento agli  artt.    &#13;
 3  e  25,  secondo  comma,  Cost.,  dal  Tribunale di Forlì, con tre    &#13;
 ordinanze emesse, rispettivamente, il 10 febbraio, il 14 marzo e l'11    &#13;
 aprile  1989  (Reg.  ord.  nn. 238, 246 e 273/1989); dal Tribunale di    &#13;
 Isernia, con ordinanza 25 febbraio 1989 (Reg. ord. n. 254/1989);  dal    &#13;
 Tribunale  di  Pinerolo  con due ordinanze del 1° febbraio 1989 e con    &#13;
 una dell'8 febbraio 1989 (Reg. ord. nn. 267,  268  e  269/1989);  dal    &#13;
 Tribunale  di  Modena, con tre ordinanze emesse, rispettivamente, nei    &#13;
 giorni 24, 26 e 30 gennaio 1989 (Reg. ord. nn. 287, 288  e  289/1989)    &#13;
 nonché  dal  Giudice  istruttore presso il Tribunale di Trieste, con    &#13;
 ordinanza 17 febbraio 1989 (Reg. ord. n. 266/1989).                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 30 novembre 1989.                             &#13;
                          Il Presidente: CONSO                            &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria l'11 dicembre 1989.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
