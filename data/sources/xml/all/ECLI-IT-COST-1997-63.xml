<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>63</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:63</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/03/1997</data_decisione>
    <data_deposito>14/03/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, avv. Fernanda CONTRI, &#13;
 prof. Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel   giudizio  di  legittimità  costituzionale  dell'art.  669-ter,    &#13;
 secondo comma, del codice di procedura civile, promosso con ordinanza    &#13;
 emessa  il  22  febbraio  1996  dal  giudice  di  pace  di  Fano  nel    &#13;
 procedimento civile vertente tra Condominio "Flaminio R. 3" di Fano e    &#13;
 Calamandrei  Daniela  ed  altro,  iscritta  al  n.  450  del registro    &#13;
 ordinanze 1996 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 21, prima serie speciale, dell'anno 1996;                             &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio del 12 febbraio 1997 il giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
   Ritenuto che  nel  corso  di  un  procedimento  in  cui  era  stato    &#13;
 richiesto  ante  causam un provvedimento d'urgenza il giudice di pace    &#13;
 di Fano, con ordinanza emessa il 22 febbraio 1996, ha sollevato -  in    &#13;
 riferimento agli artt. 107, terzo comma, 101, 106, secondo comma, 3 e    &#13;
 97,  primo  comma,  della  Costituzione  -  questione di legittimità    &#13;
 costituzionale  dell'art.  669-ter,  secondo  comma,  del  codice  di    &#13;
 procedura civile, nella parte in cui prescrive che "se competente per    &#13;
 la  causa  di  merito è il giudice di pace, la domanda si propone al    &#13;
 pretore";                                                                &#13;
     che, a giudizio  del  rimettente,  con  la  recente  riforma  del    &#13;
 processo  sarebbe stato introdotto un principio generale, secondo cui    &#13;
 alla competenza di merito  corrisponde  la  titolarità  dei  "poteri    &#13;
 d'urgenza",  così palesandosi l'irragionevolezza dell'esclusione del    &#13;
 giudice di pace dalla competenza cautelare;                              &#13;
     che  quest'ultima,  inoltre,   determinerebbe   una   surrettizia    &#13;
 gerarchia   tra   magistrati   onorari   e  togati,  nonché  ritardi    &#13;
 procedurali   ostativi   al   buon   andamento   dell'amministrazione    &#13;
 giudiziaria;                                                             &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura dello Stato,  che  ha  chiesto    &#13;
 dichiararsi    la    manifesta    infondatezza    della    questione,    &#13;
 preliminarmente sottolineando la natura discrezionale della scelta di    &#13;
 escludere il giudice di pace dalla competenza cautelare;                 &#13;
     che  l'autorità  intervenuta  ha poi osservato, nel merito, come    &#13;
 l'art. 106 della Costituzione,  se  consente  la  nomina  di  giudici    &#13;
 onorari,  non impone affatto l'attribuzione ad essi di ogni funzione,    &#13;
 in particolare di quella  cautelare,  implicante  un  accentuato  uso    &#13;
 dell'imperium  in  ragione  del  correlativo  potere di attuazione di    &#13;
 provvedimenti;                                                           &#13;
    Considerato  che  il  legislatore,  nell'esercizio  della  propria    &#13;
 discrezionalità,  ha,  col  nuovo  procedimento  cautelare uniforme,    &#13;
 introdotto un modulo processuale unitario, in cui:                       &#13;
      a) stabilisce una  correlazione  necessaria  tra  la  denunciata    &#13;
 norma  e  il successivo art. 669-quater dettato per la corrispondente    &#13;
 ipotesi di competenza cautelare in corso di causa;                       &#13;
     b) ripartisce le competenze in modo da  escludere  sempre  quella    &#13;
 del giudice di pace;                                                     &#13;
     c)     prevede     altresì     un     complesso     di    poteri    &#13;
 d'attuazione-esecuzione delle misure cautelari  (art.  669-duodecies)    &#13;
 ed  un sistema di ipotesi di reclamabilità (art. 669-terdecies), non    &#13;
 conciliabili con l'invocata estensione di competenza;                    &#13;
     che con detta esclusione il legislatore  non  ha  travalicato  il    &#13;
 limite  di  ragionevolezza  imposto  al  suo  potere di conformare il    &#13;
 processo, tanto più in quanto il  giudice  di  pace  decide  secondo    &#13;
 equità  il  merito  delle  cause  il  cui valore non eccede lire due    &#13;
 milioni  (art.    113,  secondo  comma),   attività,   questa,   ben    &#13;
 difficilmente conciliabile con l'apprezzamento del fumus boni iuris;     &#13;
     che,  d'altronde, trattandosi appunto di normativa concernente il    &#13;
 modo di esercizio della funzione giurisdizionale, non può venire  in    &#13;
 considerazione   il   principio  di  buon  andamento  della  pubblica    &#13;
 amministrazione;                                                         &#13;
     che  palesemente  estranei  alla  ripartizione  della  competenza    &#13;
 appaiono   infine  gli  altri  parametri  evocati  nell'ordinanza  di    &#13;
 rimessione, relativi tutti  all'"ordinamento  giurisdizionale"  della    &#13;
 Magistratura, e più in particolare al reclutamento e allo status dei    &#13;
 giudici;                                                                 &#13;
       che, pertanto, la questione risulta manifestamente infondata;      &#13;
   Visti gli art. 26, secondo comma, della legge 11 marzo 1953, n.  87    &#13;
 e  9,  secondo  comma,  delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale   dell'art.  669-ter,  secondo  comma,  sollevata,  in    &#13;
 riferimento agli artt. 3, 97, primo comma, 101, 106, secondo comma, e    &#13;
 107, terzo comma, della Costituzione, dal giudice di pace di Fano con    &#13;
 l'ordinanza di cui in epigrafe.                                          &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 10 marzo 1997.                                &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 14 marzo 1997.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
