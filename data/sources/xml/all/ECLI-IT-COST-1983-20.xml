<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>20</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:20</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1983</data_decisione>
    <data_deposito>01/02/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN &#13;
 - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  69,  u.c.,  &#13;
 cod.    pen. (Concorso di circostanze aggravanti e attenuanti) promosso  &#13;
 con ordinanza emessa il 26 gennaio  1979  dal  Pretore  di  Salò,  nel  &#13;
 procedimento  penale  a  carico di Garatti Giuseppe, iscritta al n. 810  &#13;
 del registro ordinanze 1979 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 15 del 16 gennaio 1980.                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio del 21 dicembre 1982 il Giudice  &#13;
 relatore Ettore Gallo.                                                   &#13;
     Ritenuto che, con ord.  26  gennaio  1979,  il  Pretore  di  Salò,  &#13;
 sull'eccezione  avanzata  dal  difensore  nel  corso di un procedimento  &#13;
 penale  contro  tale   Garatti   Giuseppe,   sollevava   questione   di  &#13;
 legittimità  costituzionale  dell'art. 69 u.c. cod. pen.  in relazione  &#13;
 all'art. 3 Cost.,                                                        &#13;
     che, ad avviso del Pretore,  il  legislatore  avrebbe  graduato  le  &#13;
 pene,  nei  riguardi di reati appartenenti ad una stessa categoria, sia  &#13;
 attraverso la configurazione di circostanze aggravanti, sia  attraverso  &#13;
 il mutamento del nomen iuris.                                            &#13;
     che,  però  -  sempre ad avviso del primo giudice - l'art. 69 cod.  &#13;
 pen. consente il giudizio di bilanciamento esclusivamente  in  rapporto  &#13;
 alle  vere  e  proprie  circostanze del reato, in senso tecnico, ma non  &#13;
 anche con riguardo a quelle che il legislatore avrebbe  assunto  "quali  &#13;
 elementi  costitutivi  di  uno stesso reato base", con ciò violando il  &#13;
 principio di uguaglianza.                                                &#13;
     che la parte privata non  si  è  costituita,  mentre  ha  spiegato  &#13;
 intervento  il  Presidente del Consiglio dei ministri il quale, tramite  &#13;
 l'Avvocatura Generale dello Stato, ha  chiesto  che  la  questione  sia  &#13;
 dichiarata infondata.                                                    &#13;
     Considerato   che,  pur  essendovi  effettivamente  sul  punto  uno  &#13;
 specifico  principio  contrario  già  espresso  da  questa  Corte  con  &#13;
 riguardo  al  ben  diverso  interesse tutelato dalla fattispecie di cui  &#13;
 all'art. 341 rispetto a quello di cui all'art. 594 cod. pen.,  per  cui  &#13;
 sembra  azzardato  parlare  di reati appartenenti alla stessa categoria  &#13;
 (sent. n. 109 del 2 luglio 1968), tuttavia deve darsi  precedenza  alla  &#13;
 questione di rilevanza, che il Pretore non si è nemmeno proposto, e in  &#13;
 ordine  alla  quale  non è dato, perciò, alcun cenno alla fattispecie  &#13;
 concreta, rispetto a cui  il  primo  giudice  si  limita  ad  affermare  &#13;
 apoditticamente la pregiudizialità della sollevata questione,           &#13;
     che,  per  tal modo, in conformità alla costante giurisprudenza di  &#13;
 questa Corte, resta insoddisfatta la prescrizione dell'art. 23, secondo  &#13;
 comma, l. 11 marzo 1953 n. 87 (sentenze 108, 109 e 158/82),              &#13;
     che conseguentemente la questione va dichiarata  inammissibile  per  &#13;
 difetto di motivazione sulla rilevanza.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  manifestamente inammissibile la questione di legittimità  &#13;
 costituzionale sollevata, con ord. 26  gennaio  1979,  dal  Pretore  di  &#13;
 Salò  nei confronti dell'art. 69 u.c. cod. pen., in relazione all'art.  &#13;
 3 Cost.                                                                  &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 gennaio 1983.        &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   -  ANTONINO  DE  STEFANO  - GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
