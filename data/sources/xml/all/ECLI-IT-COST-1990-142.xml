<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>142</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:142</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1990</data_decisione>
    <data_deposito>26/03/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 1 e 3 della    &#13;
 legge 21 febbraio  1989,  n.  61  (Misure  urgenti  per  fronteggiare    &#13;
 l'eccezionale  carenza  di  disponibilità  abitative),  promosso con    &#13;
 ordinanza emessa il 10 aprile 1989 dal Pretore  di  Torre  Annunziata    &#13;
 nel  procedimento  civile  vertente tra Panariello Santo e Castellano    &#13;
 Alfonso, iscritta al n. 465 del registro ordinanze 1989 e  pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 42, prima serie speciale    &#13;
 dell'anno 1989;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 31 gennaio 1990 il Giudice    &#13;
 relatore Aldo Corasaniti;                                                &#13;
    Ritenuto  che,  in sede di opposizione all'esecuzione per rilascio    &#13;
 di un immobile locato  per  uso  abitativo,  esecuzione  promossa  da    &#13;
 Alfonso  Castellano  sulla  base  di  sentenza che aveva accertato la    &#13;
 legittimità  del  recesso  per  necessità,  il  Pretore  di   Torre    &#13;
 Annunziata,  con ordinanza emessa il 10 aprile 1989, su eccezione del    &#13;
 conduttore-opponente,  ha   sollevato   questione   di   legittimità    &#13;
 costituzionale,  in riferimento agli artt. 3 e 24 della Costituzione,    &#13;
 del combinato disposto  degli  artt.  1  e  3  del  decreto-legge  30    &#13;
 dicembre  1988, n. 551 (Misure urgenti per fronteggiare l'eccezionale    &#13;
 carenza di disponibilità abitative), convertito, con  modificazioni,    &#13;
 nella legge 21 febbraio 1989, n. 61;                                     &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri, rappresentato dall'Avvocatura generale dello Stato, che  ha    &#13;
 concluso  per  l'inammissibilità  ovvero  per  l'infondatezza  della    &#13;
 questione;                                                               &#13;
    Considerato  che  il  decreto-legge  n.  551  del 1988, da un lato    &#13;
 sospende  l'esecuzione  dei  provvedimenti  (sentenze,  convalide  di    &#13;
 licenza  o  di  sfratto,  ordinanze  di  rilascio di cui all'art. 665    &#13;
 c.p.c.), indicati dall'art. 1, primo comma, che abbiano accertato  la    &#13;
 cessazione  del contratto alla scadenza (salvi i casi di cui all'art.    &#13;
 2), e dall'altro dispone (art. 3, primo comma) che l'assistenza della    &#13;
 forza pubblica sia concessa secondo criteri stabiliti dal prefetto in    &#13;
 relazione alle indicazioni della commissione di cui all'art. 4 e  che    &#13;
 (art.  3,  secondo  comma),  nell'ambito di tali criteri, sia data la    &#13;
 priorità, oltre che ai titoli a esecuzione non sospesa - fra i quali    &#13;
 devono  annoverarsi le sentenze che abbiano accertato la legittimità    &#13;
 del recesso per necessità del locatore  di  cui  all'art.  59  della    &#13;
 legge  27  luglio  1978, n. 392 - anche ad alcuni titoli a esecuzione    &#13;
 sospesa, purché il locatore che  agisce  in  base  ad  uno  di  essi    &#13;
 dichiari,  nelle  forme  previste,  di  avere  urgente  necessità di    &#13;
 adibire l'immobile locato ad uso abitativo proprio, del coniuge,  del    &#13;
 genitore o dei figli;                                                    &#13;
     che ad avviso del giudice a quo la normativa denunciata determina    &#13;
 ingiustificata discriminazione  del  conduttore  e  lesione  del  suo    &#13;
 diritto  di  difesa,  in  quanto,  mentre consente al locatore di far    &#13;
 valere,  al  fine  di  ottenere  la  priorità  nell'esecuzione,  una    &#13;
 situazione  di  necessità,  peraltro non contemplata dal titolo, non    &#13;
 consente  al  conduttore  di  far  valere,   al   fine   di   opporsi    &#13;
 all'esecuzione  di  sentenza  fondata  sulla  necessità del locatore    &#13;
 (art. 59 legge n. 392 del  1978),  il  successivo  venir  meno  della    &#13;
 necessità;                                                              &#13;
      che  la  mancata previsione da parte della normativa impugnata -    &#13;
 normativa concernente l'ipotesi che  il  locatore  faccia  valere  la    &#13;
 propria  necessità  come  causa di priorità, nell'ottenimento della    &#13;
 forza pubblica ai fini dell'esecuzione del proprio  titolo,  rispetto    &#13;
 ad  altri  locatori  richiedenti  del  pari  la  detta  assistenza  -    &#13;
 dell'ipotesi che il conduttore esecutato intenda far  valere  la  non    &#13;
 persistenza  della necessità del locatore, accertata nel giudizio di    &#13;
 cognizione  fra  le  dette  due  parti,  per  opporsi  all'esecuzione    &#13;
 promossa dal locatore, non può ritenersi in violazione dei parametri    &#13;
 suindicati;                                                              &#13;
      che,  invero,  tra  le due ipotesi non vi è alcuna omogeneità,    &#13;
 trattandosi, nel primo caso, di un  conflitto  di  interessi  tra  il    &#13;
 locatore   richiedente   la  forza  pubblica  e  gli  altri  locatori    &#13;
 richiedenti,  i  soli  controinteressati  e  suscettivi   di   essere    &#13;
 danneggiati  direttamente da una scorretta risoluzione del conflitto,    &#13;
 e nel secondo caso, di una controversia fra  conduttore  esecutato  e    &#13;
 locatore esecutante;                                                     &#13;
      che, d'altra parte, la normativa impugnata non ignora l'esigenza    &#13;
 del conduttore di far valere  la  non  persistenza  della  necessità    &#13;
 addotta  in  sede  di  richiesta  della  forza pubblica dal locatore,    &#13;
 giacché, se questi  non  utilizzi  l'immobile  recuperato  entro  un    &#13;
 termine,  riconosce  al  conduttore  stesso,  in  relazione  al danno    &#13;
 indiretto da lui riportato per effetto della priorità male acquisita    &#13;
 dal  locatore nei confronti degli altri locatori, il risarcimento del    &#13;
 danno  e  il  rimborso  delle  spese  (quarto   comma   dell'art.   3    &#13;
 decreto-legge n. 551 del 1988);                                          &#13;
      che, comunque, il conduttore, il quale intenda far valere - come    &#13;
 nel caso - la non persistenza della necessità addotta  dal  locatore    &#13;
 nei  suoi  confronti  nel  giudizio  di cognizione, anche se non può    &#13;
 farne motivo di opposizione all'esecuzione, non è privo neanche esso    &#13;
 di   tutela,   giacché,  se  il  locatore  non  utilizzi  l'immobile    &#13;
 recuperato entro  un  termine,  fruisce  del  più  incisivo  rimedio    &#13;
 apprestato  dall'art.  60  della  legge n. 392 del 1978, vale a dire,    &#13;
 oltre al rimborso delle spese, il ripristinamento della locazione  in    &#13;
 alternativa al mero risarcimento del danno;                              &#13;
      che,   pertanto,   la  questione  va  dichiarata  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 1 e 3 del decreto-legge 30 dicembre  1988,    &#13;
 n.  551  (Misure  urgenti  per  fronteggiare l'eccezionale carenza di    &#13;
 disponibilità abitative), convertito, con modificazioni, nella legge    &#13;
 21  febbraio 1989, n. 61, sollevata, in riferimento agli artt. 3 e 24    &#13;
 della Costituzione, dal Pretore di Torre Annunziata  con  l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 7 marzo 1990.                                 &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CORASANITI                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 marzo 1990.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
