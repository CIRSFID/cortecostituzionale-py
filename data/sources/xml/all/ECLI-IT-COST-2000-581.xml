<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>581</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:581</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/12/2000</data_decisione>
    <data_deposito>29/12/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
 Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 26 della legge    &#13;
 27  dicembre  1977,  n. 968  (Principi generali e disposizioni per la    &#13;
 protezione  e  la  tutela  della fauna e la disciplina della caccia),    &#13;
 nonché  degli artt. 16 e 18, secondo comma della legge della Regione    &#13;
 Emilia-Romagna 15 febbraio 1994, n. 8 (Disposizioni per la protezione    &#13;
 della  fauna  selvatica  e per l'esercizio dell'attività venatoria),    &#13;
 promosso  con  ordinanza emessa il 23 luglio 1999 dal giudice di pace    &#13;
 di  Bologna  nel  procedimento  civile  vertente  tra  Boni Anna e la    &#13;
 Provincia di Bologna, iscritta al n. 41 del registro ordinanze 2000 e    &#13;
 pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 8, 1ª serie    &#13;
 speciale, dell'anno 2000.                                                &#13;
     Visti  gli  atti di intervento della Regione Emilia-Romagna e del    &#13;
 Presidente del Consiglio dei ministri;                                   &#13;
     Udito  nella  camera di consiglio del 29 novembre 2000 il giudice    &#13;
 relatore Fernando Santosuosso.                                           &#13;
     Ritenuto  che  nel corso di un giudizio di risarcimento del danno    &#13;
 causato da un cinghiale nei confronti di un'autovettura il giudice di    &#13;
 pace  di  Bologna,  con  ordinanza del 23 luglio 1999 (pervenuta alla    &#13;
 cancelleria  di  questa  Corte in data 24 gennaio 2000), ha sollevato    &#13;
 questione  di legittimità costituzionale dell'art. 26 della legge 27    &#13;
 dicembre  1977,  n. 968  (Principi  generali  e  disposizioni  per la    &#13;
 protezione  e  la  tutela  della fauna e la disciplina della caccia),    &#13;
 nonché degli artt. 16 e 18, secondo comma, della legge della Regione    &#13;
 Emilia-Romagna 15 febbraio 1994, n. 8 (Disposizioni per la protezione    &#13;
 della fauna selvatica e per l'esercizio dell'attività venatoria), in    &#13;
 riferimento agli artt. 3, primo comma, 32, primo comma, e 42, secondo    &#13;
 comma, della Costituzione;                                               &#13;
         che,  ad  avviso  del  rimettente,  nella  giurisprudenza  di    &#13;
 legittimità  si  afferma il principio secondo cui in caso di danni a    &#13;
 persone  e/o  a cose causati da animali selvatici non sia applicabile    &#13;
 l'art.   2052  cod.  civ.  e  il  conseguente  regime  di  inversione    &#13;
 dell'onere  della  prova,  bensì  l'art.  2043  cod.  civ.,  il  che    &#13;
 comporterebbe l'applicazione della regola generale per cui incombe al    &#13;
 danneggiato   di  provare,  tra  l'altro,  la  colpa  della  pubblica    &#13;
 amministrazione;                                                         &#13;
         che,  pertanto,  mentre  il  conduttore di attività agricola    &#13;
 sarebbe  esentato  dal  provare  la  responsabilità  della  pubblica    &#13;
 amministrazione  per  ottenere  il risarcimento del danno prodotto da    &#13;
 animali  selvatici,  il  singolo  danneggiato  dagli  stessi animali,    &#13;
 invece, dovrebbe fornire tale prova;                                     &#13;
         che    questa    diversa    disciplina   sarebbe   priva   di    &#13;
 ragionevolezza,   atteso   che   il   presente   momento  storico  è    &#13;
 caratterizzato  da  una  massiccia  immissione  di animali selvatici,    &#13;
 specie  nei  parchi  naturali,  con  forte  incremento  di  incidenti    &#13;
 stradali e danni a terzi;                                                &#13;
         che  la  normativa  sopra  citata,  nella  parte  in cui "non    &#13;
 prevede  la  risarcibilità  dei danni causati da animali selvatici a    &#13;
 persone e/o a cose, alla stessa stregua della risarcibilità prevista    &#13;
 e  disciplinata  per i danni alla produzione agricola", violerebbe il    &#13;
 principio  di  uguaglianza  (art.  3, primo comma, Cost.), nonché la    &#13;
 tutela  della  salute  (art.  32,  primo comma, Cost.) e quella della    &#13;
 proprietà privata (art. 42, secondo comma, Cost.);                      &#13;
         che  nel  presente giudizio di legittimità costituzionale è    &#13;
 intervenuto il Presidente del Consiglio dei ministri, rappresentato e    &#13;
 difeso  dall'Avvocatura  generale  dello  Stato,  che ha concluso per    &#13;
 l'inammissibilità   o,   in   subordine,  per  l'infondatezza  della    &#13;
 questione;                                                               &#13;
         che  è  intervenuto il Presidente della Giunta regionale pro    &#13;
 tempore  della  Regione Emilia-Romagna, chiedendo analogamente che la    &#13;
 questione sia dichiarata inammissibile e infondata.                      &#13;
     Considerato  che,  in  ordine  alle eccezioni di inammissibilità    &#13;
 sollevate   dalla   difesa  erariale  e  dalla  Regione  intervenuta,    &#13;
 principalmente  appuntate sulla genericità ed indeterminatezza della    &#13;
 prospettazione  del  giudice a quo questa Corte ritiene che le stesse    &#13;
 siano  prive  di fondamento, in quanto dal contesto della motivazione    &#13;
 del provvedimento di rimessione è possibile evincere le disposizioni    &#13;
 che  il  giudice a quo intende censurare, individuandole nell'art. 26    &#13;
 della   legge   n. 968  del  1977  (come  sostituiti  dalle  omologhe    &#13;
 disposizioni della legge 11 febbraio 1992, n. 157 dettante "Norme per    &#13;
 la  protezione  della  fauna  selvatica  omeoterma  e per il prelievo    &#13;
 venatorio")  e  negli artt. 16 e 18, secondo comma, della legge della    &#13;
 Regione   Emilia-Romagna  15  febbraio  1994,  n. 8  (nel  suo  testo    &#13;
 originario),  essendo  questo  il  complesso di norme che regolano il    &#13;
 regime  di  accertamento  e di ristoro dei pregiudizi alla produzione    &#13;
 agricola derivanti dagli animali selvatici;                              &#13;
         che,  pertanto,  occorre  esaminare nel merito la prospettata    &#13;
 violazione, da parte della normativa così individuata, del principio    &#13;
 di  uguaglianza  ex  art.  3  della Costituzione nel non prevedere la    &#13;
 risarcibilità dei danni causati da animali selvatici a persone e/o a    &#13;
 cose  alla  stessa  stregua  (e  in  particolare con lo stesso regime    &#13;
 probatorio)  di quello previsto dalle disposizioni citate per i danni    &#13;
 alla  produzione agricola, vulnus che sarebbe per il rimettente ancor    &#13;
 più  evidente  alla  luce  dell'interpretazione giurisprudenziale di    &#13;
 legittimità  che ritiene applicabile alla specie il regime ordinario    &#13;
 di  responsabilità  aquiliana ex art. 2043 cod. civ. anziché quello    &#13;
 di cui all'art. 2052 dello stesso codice;                                &#13;
         che,  ad avviso di questa Corte, l'esigenza di una parità di    &#13;
 trattamento  tra  le  situazioni di fatto che si assumono analoghe di    &#13;
 chi  patisce  un  danno alla produzione agricola e di chi invece vede    &#13;
 danneggiata  la propria persona o i propri beni dalla fauna selvatica    &#13;
 non  sussiste,  atteso  che  non solo sono differenti le predette due    &#13;
 fattispecie,  ma  la  ratio  della normativa denunciata risiede nella    &#13;
 specificità  della  protezione  offerta in relazione ai danni subiti    &#13;
 dalle   produzioni   agricole  a  causa  della  fauna  selvatica;  il    &#13;
 legislatore   ha   cioè   inteso  approntare  una  peculiare  tutela    &#13;
 all'agricoltura  indennizzando gli effetti negativi ad essa derivanti    &#13;
 dalla  presenza  di  quegli  animali  sul  territorio,  presenza  che    &#13;
 nell'attuale  contesto  storico  sociale  è  ritenuta  meritevole di    &#13;
 protezione nel quadro di un armonico equilibrio ambientale;              &#13;
         che,  considerata  la natura speciale della indennizzabilità    &#13;
 prevista  dalle  disposizioni  censurate,  queste  norme  non possono    &#13;
 essere  estese  oltre  i  casi  espressamente  previsti;  né la loro    &#13;
 irragionevolezza  può  essere  ricollegata all'interpretazione della    &#13;
 giurisprudenza  di legittimità sull'art. 2052 cod. civ., secondo cui    &#13;
 questa   disposizione  è  applicabile  solo  in  presenza  di  danni    &#13;
 provocati  da  animali  domestici con esclusione di quelli selvatici,    &#13;
 per  i  quali  si applica invece l'art. 2043 cod. civ; giurisprudenza    &#13;
 che  trova  la  sua giustificazione nella diversità delle situazioni    &#13;
 poste  a  raffronto,  atteso  lo  "stato  di  naturale  libertà  che    &#13;
 caratterizza la fauna selvatica";                                        &#13;
         che,  in ordine alle ulteriori censure relative agli artt. 32    &#13;
 e  42 della Costituzione, ritiene questa Corte che la possibilità di    &#13;
 tutela  generale  ex  art. 2043 cod. civ. escluda la configurabilità    &#13;
 dei lamentati vizi;                                                      &#13;
         che,  pertanto,  la questione sollevata deve ritenersi, sotto    &#13;
 ogni profilo, manifestamente infondata.                                  &#13;
     Visti  gli  artt.  26,  secondo comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale  dell'art.  26  della  legge 11 febbraio    &#13;
 1992, n. 157 (Norme per la protezione della fauna selvatica omeoterma    &#13;
 e  per  il  prelievo  venatorio) nonché degli artt. 16 e 18, secondo    &#13;
 comma,  della  legge  della  Regione Emilia-Romagna 15 febbraio 1994,    &#13;
 n. 8  (Disposizioni  per  la  protezione  della fauna selvatica e per    &#13;
 l'esercizio  dell'attività venatoria) sollevata, in riferimento agli    &#13;
 artt.  3,  32,  primo comma, e 42, secondo comma, della Costituzione,    &#13;
 dal giudice di pace di Bologna, con l'ordinanza di cui in epigrafe.      &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 dicembre 2000.                             &#13;
                Il Presidente e redattore: Santosuosso                    &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 29 dicembre 2000.                       &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
