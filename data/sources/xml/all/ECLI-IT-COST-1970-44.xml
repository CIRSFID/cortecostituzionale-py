<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>44</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:44</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/03/1970</data_decisione>
    <data_deposito>23/03/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI Giudici</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt.  56,  comma  &#13;
 primo,  e  58,  comma  secondo,  della  legge  7  gennaio  1929,  n. 4,  &#13;
 contenente norme generali  sulla  repressione  delle  violazioni  delle  &#13;
 leggi  finanziarie, promosso con ordinanza emessa il 28 maggio 1968 dal  &#13;
 tribunale di  Locri  nel  procedimento  civile  vertente  tra  Gioffré  &#13;
 Domenico  e  l'amministrazione  finanziaria dello Stato, iscritta al n.  &#13;
 142 del registro ordinanze 1968 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n.  222 del 31 agosto 1968.                             &#13;
     Visti  gli  atti  di  costituzione dell'amministrazione finanziaria  &#13;
 dello Stato e d'intervento del Presidente del Consiglio dei Ministri;    &#13;
     udito nell'udienza pubblica del 28 gennaio 1970 il Giudice relatore  &#13;
 Giovanni Battista Benedetti;                                             &#13;
     udito il sostituto avvocato generale dello Stato  Francesco  Agrò,  &#13;
 per  l'amministrazione  finanziaria dello Stato e per il Presidente del  &#13;
 Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza  emessa  il  28  maggio 1968 nel procedimento civile  &#13;
 vertente tra Gioffré Domenico e  l'amministrazione  finanziaria  dello  &#13;
 Stato  il  tribunale di Locri ha sollevato la questione di legittimità  &#13;
 costituzionale,  in  riferimento  agli  artt.  3,  24  e  i  13   della  &#13;
 Costituzione,  degli  artt.  56  e 58 della legge 7 gennaio 1929, n. 4,  &#13;
 contenente norme generali  sulla  repressione  delle  violazioni  delle  &#13;
 leggi finanziarie.                                                       &#13;
     Si   afferma   nell'ordinanza   che  tali  disposizioni  -  la  cui  &#13;
 applicazione ha luogo anche per le violazioni delle norme  della  legge  &#13;
 19   giugno   1940,   n.  762,  sull'imposta  generale  sull'entrata  -  &#13;
 attribuendo  efficacia  esecutiva  all'ordinanza   dell'intendente   di  &#13;
 finanza  e  al decreto del Ministro delle finanze contenenti condanna a  &#13;
 pena  pecuniaria,  limitano  enormemente  i  diritti  di   tutela   del  &#13;
 cittadino.                                                               &#13;
     L'attribuzione   di   siffatta  efficacia  consentirebbe,  infatti,  &#13;
 all'amministrazione  finanziaria  di   soddisfare   la   sua   pretesa,  &#13;
 procedendo  ad  esecuzione  sui  beni  del  preteso  debitore, prima di  &#13;
 qualsiasi accertamento,  ad  opera  dell'autorità  giudiziaria,  sulla  &#13;
 effettiva esistenza e sulla fondatezza della pretesa stessa.             &#13;
     Dal  che  conseguirebbe,  ad  avviso  del  tribunale,  una evidente  &#13;
 condizione di soggezione e disparità di trattamento per  il  cittadino  &#13;
 nonché   una   limitazione   dei  suoi  diritti  di  difesa  e  tutela  &#13;
 giurisdizionale  che  dovrebbero  condurre  ad  una  dichiarazione   di  &#13;
 incostituzionalità  degli  artt.  56 e 58 della legge n. 4 del 1929 in  &#13;
 riferimento agli artt. 3, 24 e 113 della Costituzione.  Nei  confronti,  &#13;
 cioè,  quegli  stessi  precetti  che  giustificarono  la  pronuncia di  &#13;
 incostituzionalità dell'art. 52  della  legge  n.  762  del  1940  che  &#13;
 subordinava  al  previo  pagamento  dell'imposta  generale sull'entrata  &#13;
 l'esperibilità del gravame dinanzi  all'autorità  giudiziaria  contro  &#13;
 l'ordinanza  definitiva dell'intendente di finanza e contro il decreto,  &#13;
 anch'esso definitivo, del Ministro delle finanze (sent. 79/1961).        &#13;
     Nel giudizio dinanzi a questa Corte la  parte  privata  non  si  è  &#13;
 costituita.  L'amministrazione finanziaria dello Stato ed il Presidente  &#13;
 del  Consiglio  dei   Ministri,   entrambi   rappresentati   e   difesi  &#13;
 dall'Avvocatura   generale   dello  Stato,  hanno,  invece,  depositato  &#13;
 rispettivamente  deduzioni  costitutive  e  atto   di   intervento   in  &#13;
 cancelleria l'8 luglio e il 20 settembre 1968.                           &#13;
     Nei  propri  scritti difensivi l'Avvocatura rileva che il tribunale  &#13;
 di Locri muove dalla premessa dell'avvenuta abolizione della regola del  &#13;
 solve et  repete  per  sostenere  l'incostituzionalità  del  principio  &#13;
 dell'esecutorietà   del   provvedimento   amministrativo   in  materia  &#13;
 tributaria. Ha dimenticato  però  che  nelle  stesse  sentenze  citate  &#13;
 nell'ordinanza  di rinvio (21 e 79 del 1961) la Corte costituzionale ha  &#13;
 avuto modo di precisare che dall'abolizione della regola  suddetta  non  &#13;
 viene in alcun modo intaccato il principio dell'esecutorietà dell'atto  &#13;
 amministrativo;  e che nella successiva sentenza n. 86 del 1962, in cui  &#13;
 venne in esame l'art. 145, comma terzo, della  legge  di  registro,  la  &#13;
 Corte,  nel  ribadire l'impossibilità di una assimilazione giuridica o  &#13;
 un  collegamento  logico  tra  i  due  principi,   ha   affermato   che  &#13;
 l'esecutorietà   dell'atto   non   impedisce   né  limita  la  tutela  &#13;
 giurisdizionale, ma esclude soltanto che l'autorità giudiziaria  possa  &#13;
 sospendere  l'effetto  dell'atto  amministrativo:  il  che  è permesso  &#13;
 dall'art. 113 della Costituzione.                                        &#13;
     In  particolare,  venendo alle singole censure l'Avvocatura osserva  &#13;
 che non sussiste violazione del principio di uguaglianza non  potendosi  &#13;
 disconoscere  la  peculiarità  del  rapporto contribuente - Fisco; che  &#13;
 neppure è a parlarsi di violazione degli  artt.  24  e  113  dato  che  &#13;
 l'esecuzione  iniziata  dall'amministrazione  non  vieta al debitore di  &#13;
 agire in giudizio e di tutelare i propri diritti e interessi.            &#13;
     Conclude pertanto chiedendo che  la  Corte  voglia  dichiarare  non  &#13;
 fondata la proposta questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Gli artt. 56, comma primo, e 58, comma secondo, della legge 7  &#13;
 gennaio 1929, n. 4, contenente norme generali sulla  repressione  delle  &#13;
 violazioni  delle  leggi finanziarie, attribuiscono efficacia di titolo  &#13;
 esecutivo rispettivamente all'ordinanza, non impugnata in  termine  dal  &#13;
 trasgressore,   con   la   quale   l'intendente  di  finanza  determina  &#13;
 l'ammontare della pena pecuniaria  e  al  decreto  del  Ministro  delle  &#13;
 finanze  che  stabilisce  in  misura  diversa,  rispetto  all'ordinanza  &#13;
 intendentizia, l'ammontare  di  detta  pena.  Il  tribunale  di  Locri,  &#13;
 muovendo  dalla premessa che l'attribuzione di siffatta efficacia rende  &#13;
 coercibile  l'obbligo  di  pagare  consentendo  all'amministrazione  di  &#13;
 realizzare  la  sua  pretesa  sui beni del contribuente senza il previo  &#13;
 accertamento della fondatezza  della  stessa  ad  opera  dell'autorità  &#13;
 giudiziaria,   ha  ritenuto  che  le  indicate  disposizioni  siano  in  &#13;
 contrasto con gli artt. 3, 24 e 113 della Costituzione  e  che  debbano  &#13;
 essere  dichiarate  incostituzionali  così  come è stato fatto per la  &#13;
 disposizione contenuta nell'art. 52 della legge 19 giugno 1940, n. 762,  &#13;
 che  subordinava  al   previo   pagamento   dell'imposta   sull'entrata  &#13;
 l'esperibilità  dell'azione  giudiziaria contro l'ordinanza definitiva  &#13;
 dell'intendente  e  contro  il  decreto  del  Ministro  delle   finanze  &#13;
 (sentenza n. 79/1961).                                                   &#13;
     2.  -  La  questione  non è fondata. Le norme denunziate non hanno  &#13;
 alcuna relazione con la regola del solve et repete che la Corte ha più  &#13;
 volte  dichiarato  contrastante  con  gli  artt.  3,  24  e  113  della  &#13;
 Costituzione;    ma    costituiscono    applicazione    del   principio  &#13;
 dell'esecutorietà dell'atto amministrativo nel campo tributario la cui  &#13;
 legittimità, in riferimento agli stessi  precetti  costituzionali,  è  &#13;
 stata affermata in diverse occasioni (sentenze 21 del 1961, 86 e 87 del  &#13;
 1962).                                                                   &#13;
     Il  principio  dell'esecutorietà è di fondamentale importanza nel  &#13;
 nostro ordinamento poiché garantisce  il  regolare  svolgimento  della  &#13;
 vita finanziaria dello Stato assicurandogli la pronta disponibilità di  &#13;
 quei mezzi economici che sono indispensabili per assolvere i compiti di  &#13;
 pubblico interesse (sentenza 13 del 1970). Gli scopi ai quali il regime  &#13;
 giuridico  dell'esecutorietà  è  preordinato si realizzano, peraltro,  &#13;
 senza   violazione   dei   precetti   costituzionali   che   assicurano  &#13;
 l'eguaglianza  dei  cittadini dinanzi alla legge e l'intangibilità del  &#13;
 loro diritto di agire in giudizio per la tutela dei diritti e interessi  &#13;
 legittimi contro tutti gli atti lesivi della pubblica amministrazione.   &#13;
     Nei confronti del principio  dell'esecutorietà,  che  consente  la  &#13;
 riscossione,  anche  in  via  coattiva,  di  una  entrata senza che sia  &#13;
 necessario il previo accertamento della legittimità  dell'imposizione,  &#13;
 tutti  i  contribuenti  si  trovano in identica situazione; mentre, per  &#13;
 quanto riguarda il rapporto fisco  -  contribuente,  la  diversità  di  &#13;
 trattamento  riservato  al  primo  trova giustificazione, come è stato  &#13;
 già rilevato, nell'esigenza di  garantire  allo  Stato  la  percezione  &#13;
 delle entrate necessarie al perseguimento dei suoi fini pubblici.        &#13;
     In ordine poi ai diritti di tutela e difesa giuridica delle proprie  &#13;
 ragioni   è   di   tutta   evidenza   che   l'esecutorietà  dell'atto  &#13;
 amministrativo  non  esclude  la  proponibilità  dell'azione   davanti  &#13;
 all'autorità  giudiziaria,  non  preclude  cioè  al  contribuente  la  &#13;
 possibilità di esperire quei gravami diretti ad  ottenere  l'eventuale  &#13;
 accertamento  giudiziale d'illegittimità della pretesa tributaria, né  &#13;
 peraltro sottopone tali azioni ad alcun onere preliminare.               &#13;
     Queste le ragioni poste dalla  Corte  nelle  ricordate  sentenze  a  &#13;
 sostegno    della    legittimità    costituzionale    del    principio  &#13;
 dell'esecutorietà dell'atto amministrativo, ragioni che si  appalesano  &#13;
 valide  e  decisive  anche  rispetto  alle  norme ora in esame che tale  &#13;
 principio riaffermano in materia di applicazione di pene pecuniarie per  &#13;
 violazioni di leggi finanziarie.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 degli artt. 56, comma primo, e 58, comma secondo, della legge 7 gennaio  &#13;
 1929,   n.   4,  contenente  norme  generali  sulla  repressione  delle  &#13;
 violazioni delle leggi finanziarie, sollevata, con l'ordinanza  di  cui  &#13;
 in epigrafe, in riferimento agli artt. 3, 24 e 113 della Costituzione.   &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 12 marzo 1970.                                &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
