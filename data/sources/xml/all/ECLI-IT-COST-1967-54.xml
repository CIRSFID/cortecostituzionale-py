<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>54</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:54</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Nicola Jaeger</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/04/1967</data_decisione>
    <data_deposito>05/05/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. GIUSEPPE BRANCA &#13;
 - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE &#13;
 CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'articolo unico del  &#13;
 D.P.R. 14 luglio 1960, n. 1032, nella parte in cui  rende  obbligatorio  &#13;
 erga  omnes  l'art.  30 del contratto collettivo nazionale di lavoro 24  &#13;
 luglio 1959, promosso con ordinanza  emessa  il  26  ottobre  1965  dal  &#13;
 Pretore di Cavalese nel procedimento penale a carico di Rossi Domenico,  &#13;
 iscritta  al  n.  213  del  Registro  ordinanze 1965 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 326 del 31 dicembre 1965.         &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udita  nell'udienza  pubblica  del 1 febbraio 1967 la relazione del  &#13;
 Giudice Nicola Jaeger;                                                   &#13;
     udito il sostituto avvocato generale dello Stato Giorgio  Azzariti,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  verbale  in  data  20  agosto 1965 l'ispettore Pietro Candela,  &#13;
 addetto   all'Ispettorato   del   lavoro   di   Trento,   elevava   una  &#13;
 contravvenzione  a  certo  Domenico  Rossi,  titolare  di  una  impresa  &#13;
 edilizia di Pozza di Fassa, avendo accertato che tale imprenditore  non  &#13;
 aveva   corrisposto   a   diversi   lavoratori  da  lui  dipendenti  le  &#13;
 retribuzioni loro spettanti entro il termine di quindici  giorni  dalla  &#13;
 scadenza;  per  uno  di  essi,  Giovanni  Battista Pederiva, il ritardo  &#13;
 sarebbe stato - a quanto si legge nel verbale - addirittura di circa  9  &#13;
 mesi, essendo cessato il rapporto di lavoro fin dal 30 novembre 1964.    &#13;
     L'ispettore  riferiva  di  essersi  recato presso il cantiere della  &#13;
 ditta e di avere accertato che essa è soggetta alla  osservanza  delle  &#13;
 disposizioni   sul  trattamento  economico  e  normativo  degli  operai  &#13;
 dipendenti dalle imprese esercenti attività  edilizia,  contenute  nel  &#13;
 contratto collettivo nazionale di lavoro stipulato il 24 luglio 1959 ed  &#13;
 esteso  erga omnes con D.P.R. 14 luglio 1960, n. 1032.  La ditta stessa  &#13;
 infatti esercita attività di costruzioni edili o non risulta  iscritta  &#13;
 all'albo  delle  imprese artigiane. Si fa presente inoltre che, a norma  &#13;
 dell'art. 30 del  contratto  collettivo  citato,  essa  avrebbe  dovuto  &#13;
 provvedere  a  liquidare  le  retribuzioni  non oltre i quindici giorni  &#13;
 dalla scadenza dei periodi di paga cui esse si riferivano.               &#13;
     Dal  verbale  risulta  infine  che  la  ditta   stessa,   diffidata  &#13;
 formalmente  a  liquidare  le  somme  dovute  e  ad esibire la relativa  &#13;
 documentazione, non dette alcun riscontro alla diffida ricevuta.         &#13;
     Con ordinanza emessa il 26 ottobre  1965  il  Pretore  di  Cavalese  &#13;
 sospendeva   il  giudizio,  sollevando  d'ufficio  la  questione  della  &#13;
 legittimità costituzionale dell'articolo unico del  D.P.R.  14  luglio  &#13;
 1960, n. 1032, per la parte in cui rende obbligatorio erga omnes l'art.  &#13;
 30  del  contratto  collettivo  nazionale  di lavoro 24 luglio 1959, in  &#13;
 relazione all'art. 76 della Costituzione.                                &#13;
     Si osserva nella ordinanza che  il  fatto  attribuito  all'imputato  &#13;
 consiste  nel  non  avere  pagato  ai  suoi  dipendenti la retribuzione  &#13;
 pattuita  entro  il  termine  previsto  dall'art.  30   del   contratto  &#13;
 collettivo suddetto; che per una persona non iscritta alla associazione  &#13;
 dei  datori  di  lavoro che stipulò con la categoria dei lavoratori il  &#13;
 contratto collettivo, il ritardo nel pagamento o il  mancato  pagamento  &#13;
 della  retribuzione sembra costituire inadempimento di una obbligazione  &#13;
 civile  prevista  dall'art.  2099  del  Codice  civile,  piuttosto  che  &#13;
 violazione  dei  minimi inderogabili del trattamento economico pattuito  &#13;
 con i suoi dipendenti; che il prestatore di lavoro può far  valere  il  &#13;
 suo  diritto  alla retribuzione citando in giudizio l'obbligato davanti  &#13;
 al competente giudice civile; che, pertanto, il termine entro il  quale  &#13;
 deve  essere  soddisfatta  l'obbligazione non sembra corrispondere alla  &#13;
 specifica finalità che la legge intendeva raggiungere  ed  assicurare,  &#13;
 così  che la norma in questione avrebbe ecceduto i limiti della delega  &#13;
 contenuta nell'art. 1 della legge n. 741  del  1959,  nella  parte  che  &#13;
 rende  obbligatorio  erga  omnes  l'art.  30  del  contratto collettivo  &#13;
 nazionale di lavoro.                                                     &#13;
     L'ordinanza  è  stata  regolarmente   comunicata,   notificata   e  &#13;
 pubblicata nella Gazzetta Ufficiale (n. 326 del 31 dicembre 1965).       &#13;
     È  intervenuto  in  giudizio  solo il Presidente del Consiglio dei  &#13;
 Ministri, rappresentato e difeso dall'Avvocatura Generale dello  Stato,  &#13;
 che  ha sostenuto decisamente la tesi della evidente infondatezza della  &#13;
 questione proposta, richiamando in particolare la giurisprudenza  della  &#13;
 Corte costituzionale sull'argomento.                                     &#13;
     Alla  udienza  del 1 febbraio 1967 è intervenuto il rappresentante  &#13;
 della Avvocatura generale dello Stato, che ha ribadito gli argomenti  e  &#13;
 le conclusioni esposti nell'atto di intervento.<diritto>Considerato in diritto</diritto>:                          &#13;
     La   Corte   non  ritiene  fondata  la  questione  di  legittimità  &#13;
 costituzionale sollevata dal Pretore di Cavalese, né può  considerare  &#13;
 validi  gli  argomenti addotti nella motivazione dell'ordinanza, con la  &#13;
 quale la questione di legittimità è stata proposta.                    &#13;
     La menzione del fatto  che  l'imputato  Domenico  Rossi  non  fosse  &#13;
 iscritto   all'Associazione   costruttori   edili   non  è  rilevante,  &#13;
 trattandosi  di  contratto  collettivo  esteso  erga  omnes,  né  può  &#13;
 giustificare  -  non  soltanto  in sede di processo civile, ma anche ed  &#13;
 ancor più in sede penale - l'enorme ritardo (circa nove  mesi  in  uno  &#13;
 dei  casi  riferiti  nella  esposizione  del  fatto)  nel pagamento dei  &#13;
 salari, che soprattutto nei riguardi di semplici operai hanno  precipuo  &#13;
 carattere  alimentare  e  perciò devono essere corrisposti entro breve  &#13;
 tempo dalla prestazione del lavoro (precisamente entro quindici  giorni  &#13;
 dalla  scadenza,  a  norma del contratto collettivo nazionale di lavoro  &#13;
 esteso erga omnes con il D.P.R. n. 1032 del 1960).                       &#13;
     Né  la  Corte  può considerare accettabile la tesi, esposta nella  &#13;
 ordinanza di rimessione, che la disposizione in esame  non  corrisponda  &#13;
 "alla  specifica  finalità  che  la  legge  intendeva  raggiungere  ed  &#13;
 assicurare, così che la norma in questione avrebbe ecceduto  i  limiti  &#13;
 della  delega",  essendo noto ed ovvio che i contratti collettivi hanno  &#13;
 sempre avuto, fin dalla loro origine, anzitutto il fine di garantire ai  &#13;
 lavoratori subordinati un trattamento  economico  minimo,  non  esposto  &#13;
 alle  falcidie,  che avrebbero facilmente potuto essere provocate dalla  &#13;
 concorrenza fra gli stessi lavoratori.                                   &#13;
     Si può convenire con la  tesi  sostenuta  nell'ordinanza,  che  il  &#13;
 mancato  o ritardato pagamento della retribuzione costituisca - anche -  &#13;
 inadempimento di una obbligazione civile, prevista dall'art.  2099  del  &#13;
 Codice  civile;  ma  si  deve  subito  aggiungere che il legislatore ha  &#13;
 ritenuto opportuno, ed anzi necessario, rafforzare le garanzie a favore  &#13;
 della parte più debole e bisognosa, introducendo,  accanto  all'azione  &#13;
 civile - che richiede spesso molto tempo e non poche spese prima che si  &#13;
 pervenga alla sentenza definitiva ed al possesso del titolo esecutivo -  &#13;
 la  previsione  di  una  sanzione  penale,  più  pronta  ed assai più  &#13;
 efficace. Né  occorre  dire  che  la  previsione  e  la  scelta  della  &#13;
 sanzione,  anche  penale,  nonché  del  tipo  e  della misura di essa,  &#13;
 rientra pienamente nei poteri del legislatore.                           &#13;
     D'altra parte, la tesi che l'esercizio dell'azione civile da  parte  &#13;
 di  un  lavoratore subordinato, al fine di ottenere il pagamento di una  &#13;
 retribuzione non percepita da molti mesi, e che - come si è  osservato  &#13;
 -  ha  carattere  alimentare,  sia  la  soluzione  più  adeguata  alle  &#13;
 situazioni previste, non sembra davvero convalidata dalla esperienza, a  &#13;
 tutti nota, della lentezza e della durata dei  procedimenti,  nei  loro  &#13;
 diversi  gradi, né potrebbe comunque essere presa in considerazione in  &#13;
 questa sede.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'articolo  unico del D.P.R. 14 luglio 1960, n. 1032, nella parte in  &#13;
 cui rende obbligatorio erga omnes l'art. 30  del  contratto  collettivo  &#13;
 nazionale  di lavoro 24 luglio 1959, in relazione all'articolo 76 della  &#13;
 Costituzione.                                                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 27 aprile 1967.                               &#13;
                                   GASPARE  AMBROSINI - ANTONINO PAPALDO  &#13;
                                   - NICOLA JAEGER - GIOVANNI  CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
