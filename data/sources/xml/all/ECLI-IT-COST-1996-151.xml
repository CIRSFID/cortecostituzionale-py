<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>151</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:151</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/05/1996</data_decisione>
    <data_deposito>08/05/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI: &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, &#13;
 prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, &#13;
 prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale dell' art. 3, secondo    &#13;
 comma, del decreto-legge  10  luglio  1982,  n.  429  (Norme  per  la    &#13;
 repressione  della  evasione  in materia di imposte sui redditi e sul    &#13;
 valore aggiunto e per agevolare  la  definizione  delle  pendenze  in    &#13;
 materia  tributaria),  convertito,  con  modificazioni, nella legge 7    &#13;
 agosto 1982, n. 516, promosso con l'ordinanza emessa  il  25  ottobre    &#13;
 1995  dal  giudice per le indagini preliminari presso il tribunale di    &#13;
 Mondovì nel procedimento penale a carico di Iannuzzi Luigi, iscritta    &#13;
 al n. 897 del registro ordinanze 1995  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  1,  prima serie speciale, dell'anno    &#13;
 1996;                                                                    &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  26  marzo 1996 il giudice    &#13;
 relatore Renato Granata;                                                 &#13;
   Ritenuto  che  in  un  procedimento  penale  nel  quale  l'imputato    &#13;
 Iannuzzi Luigi era chiamato a rispondere del reato di cui all'art. 3,    &#13;
 comma  secondo,  della  legge  7 agosto 1982, n. 516 - recte: art. 3,    &#13;
 secondo comma, del decreto-legge 10 luglio 1982, n. 429 (Norme  sulla    &#13;
 repressione  della  evasione  in materia di imposte sui redditi e sul    &#13;
 valore aggiunto e per agevolare  la  definizione  delle  pendenze  in    &#13;
 materia  tributaria),  convertito,  con  modificazioni, nella legge 7    &#13;
 agosto 1982, n. 516 - per avere  omesso  di  annotare,  nell'apposito    &#13;
 registro stampati, due blocchetti di ricevute fiscali, il giudice per    &#13;
 le indagini preliminari presso il tribunale di Mondovì ha sollevato,    &#13;
 con   ordinanza  del  25  ottobre  1995,  questione  di  legittimità    &#13;
 costituzionale, in riferimento all'art. 3 della  Costituzione,  della    &#13;
 norma  suddetta, che prevede come reato contravvenzionale il fatto di    &#13;
 chi stampa, fornisce, acquista o detiene stampati per la compilazione    &#13;
 dei documenti di accompagnamento dei beni viaggianti o delle ricevute    &#13;
 fiscali senza provvedere alle prescritte annotazioni;                    &#13;
     che, a seguito delle  modifiche  all'art.  1,  sesto  comma,  del    &#13;
 decreto-legge  n.  429  del  1982,  cit. introdotte dall'art. 1 della    &#13;
 legge 15 maggio 1991, n. 154, l'omessa tenuta del registro di  carico    &#13;
 e scarico delle bolle di accompagnamento non ha più valenza penale e    &#13;
 che  quindi - osserva il giudice rimettente - continuerebbe ad essere    &#13;
 punito chi si rende responsabile  di  un  fatto  meno  grave  (omessa    &#13;
 annotazione  nel  registro), mentre non è più punibile chi commette    &#13;
 un fatto più grave (omessa  tenuta  del  registro)  con  conseguente    &#13;
 vulnus del principio di ragionevolezza;                                  &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura generale dello Stato chiedendo    &#13;
 che la questione sia dichiarata inammissibile  od  infondata  perché    &#13;
 già decisa da questa Corte con ordinanza n.464 del 1995.                &#13;
   Considerato  che  questa  Corte  ha  già  ritenuto  manifestamente    &#13;
 infondata la medesima questione di  costituzionalità  (ordinanza  n.    &#13;
 464 del 1995, cit.);                                                     &#13;
     che  il  giudice  rimettente  non  introduce argomenti nuovi, né    &#13;
 prospetta profili diversi di censura;                                    &#13;
     che quindi la questione è manifestamente infondata.                 &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art.  3,  secondo  comma,  del  decreto-legge  10    &#13;
 luglio  1982,  n.  429  (Norme  per  la repressione della evasione in    &#13;
 materia di imposte sui redditi e sul valore aggiunto e per  agevolare    &#13;
 la definizione delle pendenze in materia tributaria), convertito, con    &#13;
 modificazioni,  nella  legge  7  agosto  1982,  n.516,  sollevata, in    &#13;
 riferimento all'art.   3  della  Costituzione,  dal  giudice  per  le    &#13;
 indagini  preliminari  del  tribunale  di  Mondovì  con  l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 2 maggio 1996.                                &#13;
                         Il Presidente: Ferri                             &#13;
                         Il redattore: Granata                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria l'8 maggio 1996.                             &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
