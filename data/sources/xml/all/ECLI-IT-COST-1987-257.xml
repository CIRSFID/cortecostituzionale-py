<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>257</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:257</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ANDRIOLI</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/07/1987</data_decisione>
    <data_deposito>13/07/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Virgilio ANDRIOLI; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, &#13;
 prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 16, secondo e    &#13;
 quarto comma, e 17, primo comma, della legge della Regione Marche  19    &#13;
 ottobre  1981,  n.  30  ("Attuazione  del  diritto  allo studio nelle    &#13;
 università  aventi  sede  nella  regione  Marche"),   promosso   con    &#13;
 ordinanza  emessa  il  14  ottobre  1982  dal  Pretore  di Urbino nel    &#13;
 procedimento civile vertente tra Mosconi Vincenzo  e  Ente  regionale    &#13;
 per  il  diritto  allo  studio  universitario, iscritta al n. 809 del    &#13;
 registro ordinanze 1982 e pubblicata nella Gazzetta Ufficiale n.  114    &#13;
 dell'anno 1983.                                                          &#13;
    Visti  gli  atti di costituzione della Regione Marche e di Mosconi    &#13;
 Vincenzo;                                                                &#13;
    Udito nell'udienza pubblica del 16 giugno 1987 il Giudice relatore    &#13;
 Francesco Paolo Casavola;                                                &#13;
    Udito l'avvocato Sergio Panunzio per la Regione Marche.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Il 23 agosto 1982, Mosconi Vincenzo, direttore amministrativo    &#13;
 dell'Ente regionale per il diritto allo studio universitario E.R.S.U.    &#13;
 -  (già  Opera  universitaria  della  Libera Università di Urbino),    &#13;
 deposita presso il Pretore di Urbino,  in  funzione  di  Giudice  del    &#13;
 lavoro,  ricorso  contro  l'E.R.S.U.  per  ottenerne  il  trattamento    &#13;
 economico previsto dall'art. 11 della legge 19 ottobre 1981,  n.  30,    &#13;
 della Regione Marche, riconosciutogli con delibere 27 aprile 1982, n.    &#13;
 6 (ma, rectius, 7 aprile), e 4 giugno 1982, n. 53, del  Consiglio  di    &#13;
 amministrazione  dell'Ente,  entrambe peraltro annullate dalla Giunta    &#13;
 regionale in sede di controllo.                                          &#13;
    Il  ricorrente  solleva eccezione di illegittimità costituzionale    &#13;
 dell'art. 16, quarto comma, della legge 19 ottobre 1981, n. 30, della    &#13;
 Regione   Marche,  che  dispone  il  subentro  degli  E.R.S.U.  nella    &#13;
 proprietà dei beni  mobili  ed  immobili  e  nella  titolarità  dei    &#13;
 rapporti attivi e passivi delle Opere delle Università delle Marche,    &#13;
 inclusa fra  queste  la  Libera  Università  di  Urbino,  mentre  il    &#13;
 Ministero  della  pubblica  istruzione,  con D.M. 31 ottobre 1979, in    &#13;
 attuazione della normativa di cui all'art.  44,  secondo  comma,  del    &#13;
 d.P.R. 24 luglio 1977, n. 616, trasferisce alla Regione Marche i beni    &#13;
 e il personale delle sole Università statali di Ancona,  Camerino  e    &#13;
 Macerata.                                                                &#13;
    Il   Pretore  di  Urbino,  con  ordinanza  del  14  ottobre  1982,    &#13;
 riconosciuta la non manifesta infondatezza  dell'eccezione,  solleva,    &#13;
 in  riferimento  agli  artt. 3 e 117 della Costituzione, questione di    &#13;
 legittimità costituzionale degli artt. 16, secondo e quarto comma, e    &#13;
 17,  primo  comma,  della  citata  legge  della Regione Marche del 19    &#13;
 ottobre 1981, n. 30.                                                     &#13;
    2. - La Regione Marche, intervenuta in giudizio, confuta l'assunto    &#13;
 del giudice remittente, in particolare per quanto riguarda l'art. 44,    &#13;
 primo comma, del d.P.R. n. 616 del 1977, che trasferisce alle Regioni    &#13;
 le funzioni dello Stato, mentre con il secondo comma trasferisce  gli    &#13;
 enti  pubblici  (nella  specie  le Opere universitarie) diversi dallo    &#13;
 Stato. Quanto al  D.M.  31  ottobre  1979,  esso  è  sostanzialmente    &#13;
 integrato  e modificato dal d.l. 31 ottobre 1979, n. 536, che prevede    &#13;
 ai  fini  del  trasferimento  dei  beni  e  dei  rapporti   giuridici    &#13;
 accertamento ad opera di apposita commissione.                           &#13;
    3.  -  La  parte  ricorrente  sostiene,  invece, che il pluralismo    &#13;
 culturale,  garantito   come   valore   costituzionale   nel   nostro    &#13;
 ordinamento,  fonda  una lettura delle norme richiamate nel senso che    &#13;
 esse conservino articolazione  e  differenziazione  alle  istituzioni    &#13;
 universitarie   e   al   loro  supporto  assistenziale,  riconoscendo    &#13;
 duplicità di regime per quelle statali trasferite alle regioni e per    &#13;
 quelle libere.<diritto>Considerato in diritto</diritto>Con  ordinanza del 14 ottobre 1982, il Pretore di Urbino, adito in    &#13;
 funzione di Giudice del lavoro, per conoscere del ricorso di  Mosconi    &#13;
 Vincenzo   contro   l'Ente  regionale  per  il  diritto  allo  studio    &#13;
 universitario (E.R.S.U.), per  ottenerne  il  competente  trattamento    &#13;
 economico,  su  eccezione  del  ricorrente, solleva in relazione agli    &#13;
 artt.  3  e  117  della  Costituzione,  questione   di   legittimità    &#13;
 costituzionale  della  legge  regionale 19 ottobre 1981, n. 30, nella    &#13;
 parte (art. 16, quarto comma) in cui dispone che "gli ERSU subentrano    &#13;
 nella  proprietà dei beni mobili ed immobili e nella titolarità dei    &#13;
 rapporti attivi  e  passivi  delle  opere"  delle  Università  delle    &#13;
 Marche, includendovi la Libera Università di Urbino.                    &#13;
    La  questione di legittimità costituzionale è estesa dal giudice    &#13;
 remittente anche al secondo comma dello stesso art.  16  e  al  primo    &#13;
 comma  del  successivo  art.  17,  che  rispettivamente dispongono lo    &#13;
 scioglimento delle Opere universitarie e il  trasferimento  del  loro    &#13;
 personale alla Regione.                                                  &#13;
    Ritiene  il  giudice  remittente che "le questioni sollevate hanno    &#13;
 nella causa una rilevanza assorbente e preliminare rispetto  ad  ogni    &#13;
 altra,   investendo  la  legittimazione  a  contraddire  la  domanda.    &#13;
 Infatti, se venissero accolte, il rapporto  controverso  non  farebbe    &#13;
 più  capo  all'E.R.S.U.,  ma all'Opera dell'Università libera, (che    &#13;
 tornerebbe così a funzionare, analogamente a quanto avvenuto per  le    &#13;
 istituzioni  pubbliche  di  assitenza  e  beneficenza a seguito della    &#13;
 sentenza 30 luglio 1981 n. 173 della Corte Costituzionale)".             &#13;
    Giudica  questa  Corte  che l'asserita rilevanza è smentita dalla    &#13;
 formulazione del petitum. Nei confronti dell'Ente regionale, la parte    &#13;
 attrice   tende   infatti  a  far  valere,  in  linea  oggettivamente    &#13;
 principale,  una  pretesa  di  contenuto  patrimoniale,   qualificata    &#13;
 proprio  dal  ricorso al Pretore in funzione di Giudice del lavoro, e    &#13;
 derivante da un rapporto d'impiego.                                      &#13;
    Il   difetto   di   rilevanza  della  questione  consegue  perciò    &#13;
 all'oggetto della domanda, volto all'accertamento di un  credito  per    &#13;
 retribuzioni  con  richiesta  di condanna al pagamento delle stesse a    &#13;
 carico  del  convenuto   Ente,   preventivamente   individuato   come    &#13;
 legittimato passivo attraverso la scelta del Giudice e del rito.         &#13;
    La   questione,   così   come   è   proposta  nella  specie,  è    &#13;
 inammissibile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 16, secondo e quarto comma, e dell'art.  17,  primo  comma,    &#13;
 della  legge  della Regione Marche 19 ottobre 1981, n. 30, sollevata,    &#13;
 in riferimento agli artt. 3 e 117 della Costituzione, dal Pretore  di    &#13;
 Urbino con ordinanza del 14 ottobre 1982.                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 2 luglio 1987.          &#13;
                      Il Presidente: ANDRIOLI                             &#13;
                       Il Redattore: CASAVOLA                             &#13;
    Depositata in cancelleria il 13 luglio 1987.                          &#13;
                        Il cancelliere: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
