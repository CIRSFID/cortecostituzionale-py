<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>283</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:283</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>03/07/2006</data_decisione>
    <data_deposito>07/07/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 13, commi 2, lettere a) e b), 3 e 7, 13-bis e 14, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promossi con ordinanze del 16 aprile 2005 e del 18 luglio 2005 dai Giudici di pace di Milano e di Catanzaro, iscritte ai nn. 354 e 526 del registro ordinanze 2005 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 29 e n. 44, prima serie speciale, dell'anno 2005. &#13;
      Visti gli atti di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 7 giugno 2006 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che il Giudice di pace di Milano, nel corso di un procedimento avente ad oggetto la opposizione al decreto di espulsione emesso nei confronti di un cittadino extracomunitario, con ordinanza del 16 aprile 2005, ha sollevato, in relazione all'art. 24 della Costituzione, questione di legittimità costituzionale dell'art. 13, comma 3, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui prevede che il provvedimento prefettizio di espulsione è immediatamente esecutivo, anche se sottoposto a impugnativa da parte dell'interessato; &#13;
    che il rimettente, in punto di rilevanza, evidenzia che, non avendo lo straniero ricorrente presenziato all'udienza fissata per la comparizione delle parti, la sua assenza è «presumibilmente determinata dalla già avvenuta esecuzione dell'espulsione o dal timore di incorrere nella sanzione penale prevista dall'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998»; &#13;
    che, quanto alla non manifesta infondatezza, il giudice a quo osserva che la disciplina relativa al decreto di espulsione nel prevedere, da un lato, l'immediata esecutività del citato decreto, al quale può seguire l'ordine di allontanamento dal territorio dello Stato e, dall'altro, il termine fino ad ottanta giorni per la decisione sull'opposizione avverso di esso, non garantisce  il diritto di difesa dello straniero, al quale viene di fatto preclusa la possibilità di presenziare all'udienza di convalida del decreto di espulsione, se non assumendosi il rischio di incorrere nelle conseguenze dell'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998; &#13;
    che il contrasto della norma impugnata con i parametri costituzionali evocati sarebbe, secondo il giudice a quo, ancor più evidente in considerazione della previsione contenuta nell'art. 17 del d.lgs. n. 286 del 1998, che consente allo straniero di rientrare in Italia «per il tempo strettamente necessario per l'esercizio del diritto di difesa, al solo fine di partecipare al giudizio o al compimento di atti per i quali è necessaria la sua presenza», qualora egli sia parte offesa ovvero persona sottoposta ad indagini nell'ambito di un procedimento penale; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente inammissibile ovvero infondata; &#13;
    che il Giudice di pace di Catanzaro, nel corso di un procedimento avente ad oggetto l'opposizione al decreto di espulsione di un cittadino extracomunitario e al contestuale ordine impartitogli dal questore di lasciare il territorio dello Stato, con ordinanza del 18 luglio 2005, ha sollevato, in relazione agli  artt. 3, 13, commi primo, secondo e terzo, e 24 della Costituzione, questione di legittimità costituzionale degli artt. 13, commi 2, lettere a) e b), e 7, 13-bis e 14, comma 5-bis, del d.lgs. n. 286 del 1998, nella parte in cui prevedono che il decreto di espulsione è «immediatamente esecutivo anche se sottoposto a gravame o impugnativa», che il consequenziale ordine del questore di lasciare il territorio dello Stato entro il termine di cinque giorni non è soggetto a convalida e che gli atti del procedimento di espulsione debbono essere tradotti in una lingua conosciuta allo straniero ovvero, ove non sia possibile, in francese, inglese e spagnolo; &#13;
    che, secondo il rimettente, la mancata previsione del potere, in capo al giudice, di sospendere il decreto di espulsione sarebbe in contrasto con l'art 13 della Costituzione, in quanto tale atto inciderebbe sulla libertà personale dello straniero senza alcun controllo preventivo di legittimità; &#13;
    che, a parere del giudice a quo, l'art. 14, comma 5-bis, del d.lgs. n. 286 del 1998 sarebbe in contrasto con i parametri costituzionali evocati, non prevedendo, diversamente da quanto stabilito per l'accompagnamento alla frontiera, alcuna convalida per il provvedimento di allontanamento dal territorio dello Stato; &#13;
    che il rimettente ritiene le norme sopra indicate in contrasto, altresì, con l'art. 24 della Costituzione, in quanto precludono allo straniero la possibilità di «essere ascoltato dal giudice»; &#13;
    che, secondo il giudice a quo, l'art. 13, comma 7, del d.lgs. n. 286 del 1998 nel prevedere la traduzione degli atti relativi al procedimento di espulsione in una lingua conosciuta al destinatario ovvero in francese, inglese e spagnolo, violerebbe il principio della conoscibilità degli atti giuridici; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile e/o infondata; &#13;
    che la difesa erariale osserva che l'ordinanza di rimessione, del tutto priva della descrizione della fattispecie oggetto del giudizio a quo, non contiene alcuna motivazione in ordine alla rilevanza della questione, pur avendo il rimettente dato atto di aver sospeso l'efficacia del decreto di espulsione; &#13;
    che, a parere dell'Avvocatura, la questione sarebbe, comunque, infondata avendo la Corte, con la sentenza n. 161 del 2000, dichiarato non fondata analoga questione riferita all'art. 11 della legge 6 marzo 1998, n. 40 (Disciplina dell'immigrazione e norme sulla condizione dello straniero), in quanto la mancata previsione di una tutela cautelare, nel caso di impugnativa del decreto prefettizio di espulsione dello straniero, si giustificava con la previsione normativa di un termine breve per la decisione del ricorso; &#13;
    che, secondo l'Avvocatura, il richiamo all'art. 13 della Costituzione sarebbe inconferente, poiché le norme impugnate non incidono sulla libertà personale dei destinatari; &#13;
    che, quanto alla questione relativa all'art. 13, comma 7, la difesa erariale osserva che la Corte, con la sentenza n. 257 del 2004, ne ha dichiarato l'infondatezza, evidenziando come la norma risponda a criteri di ragionevolezza. &#13;
    Considerato che le ordinanze di rimessione propongono analoghe questioni, onde i relativi giudizi vanno riuniti per essere unitamente definiti; &#13;
    che entrambe le ordinanze difettano della descrizione delle fattispecie concrete oggetto dei giudizi a quibus, con l'ulteriore limite che la questione sollevata dal Giudice di pace di Milano è, in ordine al requisito della rilevanza, del tutto ipotetica, essendosi il rimettente limitato ad affermare che la mancata comparizione in udienza dello straniero è «presumibilmente determinata dalla già avvenuta esecuzione dell'espulsione o dal timore di incorrere nella sanzione penale prevista dall'art. 14, comma 5-ter, del d.lgs. n. 286 del 1998»; &#13;
    che le questioni così sollevate devono essere, pertanto, dichiarate manifestamente inammissibili. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale degli artt. 13, commi 2, lettere a) e b), 3 e 7,  13-bis e 14, comma 5-bis, del decreto legislativo 25 luglio 1998 n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), sollevate, in riferimento agli artt. 3, 13, commi primo, secondo e terzo, e 24 della Costituzione, dal Giudici di pace di Milano e di Catanzaro con le ordinanze indicate in epigrafe. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 luglio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 7 luglio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
