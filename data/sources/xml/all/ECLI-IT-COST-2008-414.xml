<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>414</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:414</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FLICK</presidente>
    <relatore_pronuncia>Giuseppe Tesauro</relatore_pronuncia>
    <redattore_pronuncia>Giuseppe Tesauro</redattore_pronuncia>
    <data_decisione>03/12/2008</data_decisione>
    <data_deposito>17/12/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, comma 547, della legge 23 dicembre 2005, n. 266 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2006), promosso con ordinanza del 12 febbraio 2008 dal Tribunale di Lanciano, nel procedimento penale a carico di M. A. ed altro, iscritta al n. 164 del registro ordinanze 2008 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2008. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 5 novembre 2008 il Giudice relatore Giuseppe Tesauro. &#13;
    Ritenuto che il Tribunale di Lanciano, con ordinanza del 12 febbraio 2008, ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 1, comma 547, della legge 23 dicembre 2005, n. 266 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2006), nella parte in cui stabilisce che per le violazioni di cui all'art. 110, comma 9, del regio decreto 18 giugno 1931, n. 773 (Approvazione del testo unico delle leggi di pubblica sicurezza), e successive modificazioni, commesse in data antecedente all'entrata in vigore della citata legge, si applicano le disposizioni vigenti al tempo delle violazioni stesse, e «pertanto le sanzioni penali previste nella formulazione originaria della norma»; &#13;
    che il rimettente rileva che l'art. 1, comma 543, della legge n. 266 del 2005, sostituendo il comma 9 dell'art. 110 del r.d. n. 773 del 1931, ha trasformato in illecito amministrativo le fattispecie in materia di installazione in esercizi pubblici di apparecchi automatici da gioco non conformi alle disposizioni di legge già configurate come reato contravvenzionale, ivi compresa quella contestata agli imputati nel procedimento principale; &#13;
    che, peraltro, ai sensi dell'art. 1, comma 547, della legge n. 266 del 2005, alle violazioni anteriormente commesse continuano ad applicarsi le disposizioni previgenti; &#13;
    che, ad avviso del giudice a quo, tale disciplina transitoria viola l'art. 3 della Costituzione, in quanto, introducendo una deroga al principio di retroattività della lex mitior, determina una ingiustificata disparità di trattamento tra coloro che abbiano posto in essere una delle violazioni di cui all'art. 110, comma 9, del r.d. n. 773 del 1931 anteriormente al 1° gennaio 2006 e coloro che commettono il medesimo fatto illecito successivamente a tale data, in danno dei primi, i quali rimangono soggetti a sanzione penale; &#13;
    che, al riguardo, il rimettente richiama la giurisprudenza della Corte costituzionale, secondo la quale deroghe al principio di retroattività della legge penale favorevole, sancito dall'art. 2 del codice penale, possono essere disposte dal legislatore in presenza di sufficienti ragioni giustificative (citata la sentenza n. 393 del 2006), negando che nella specie ne ricorra alcuna; &#13;
    che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo di dichiarare la questione inammissibile o manifestamente infondata.  &#13;
    Considerato che il Tribunale di Lanciano dubita, in riferimento all'art. 3 della Costituzione, della legittimità costituzionale dell'art. 1, comma 547, della legge 23 dicembre 2005, n. 266 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2006), nella parte in cui stabilisce che per le violazioni di cui all'art. 110, comma 9, del regio decreto 18 giugno 1931, n. 773 (Approvazione del testo unico delle leggi di pubblica sicurezza), commesse in data antecedente all'entrata in vigore della citata legge, si applicano le disposizioni vigenti al tempo delle violazioni stesse, e «pertanto le sanzioni penali previste nella formulazione originaria della norma»; &#13;
    che questa Corte, con la sentenza n. 215 del 2008, successiva alla pubblicazione dell'ordinanza di rimessione, ha già dichiarato l'illegittimità costituzionale dell'art. 1, comma 547, della legge n. 266 del 2005, proprio nella parte in cui stabiliva che, per le violazioni di cui all'art. 110, comma 9, del r.d. n. 773 del 1931, e successive modificazioni, commesse in data antecedente all'entrata in vigore della citata legge, si applicassero le sanzioni penali previste al tempo delle violazioni stesse; &#13;
    che, dunque, la questione va dichiarata manifestamente inammissibile, essendo venuta meno la deroga al principio di non ultrattività della legge penale, cui si riferisce la censura del rimettente;  &#13;
    che la pronuncia di manifesta inammissibilità deve essere preferita alla restituzione degli atti, nonostante la sentenza n. 215 del 2008 sia sopravvenuta all'ordinanza di rimessione, in quanto l'efficacia ex tunc della detta pronuncia di illegittimità preclude ogni valutazione in ordine alla perdurante rilevanza della questione, divenuta priva di oggetto (ordinanze n. 269 del 2008, n. 290 e n. 34 del 2002, n. 575 del 2000, n. 525 e n. 233 del 1995, n. 171 del 1992). &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 1, comma 547, della legge 23 dicembre 2005, n. 266 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato – legge finanziaria 2006), sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale di Lanciano con l'ordinanza indicata in epigrafe.  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 dicembre 2008.  &#13;
F.to:  &#13;
Giovanni Maria FLICK, Presidente  &#13;
Giuseppe TESAURO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 17 dicembre 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
