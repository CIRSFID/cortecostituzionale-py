<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1978</anno_pronuncia>
    <numero_pronuncia>67</numero_pronuncia>
    <ecli>ECLI:IT:COST:1978:67</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Guido Astuti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/04/1978</data_decisione>
    <data_deposito>10/05/1978</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. LEONETTO AMADEI - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. &#13;
 LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. &#13;
 BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO &#13;
 PALADIN - Dott. ARNALDO MACCARONE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale degli  artt.  3,  &#13;
 6,  11,  14  e  15  del  d.P.R.  26  ottobre  1972, n. 643 (Istituzione  &#13;
 dell'imposta  comunale  sull'incremento  di  valore  degli   immobili),  &#13;
 promossi  con ordinanze 11 marzo 1977, 20 dicembre 1976 e 2 aprile 1977  &#13;
 della Commissione tributaria di 1 grado di Bergamo,  10  novembre  1976  &#13;
 della  Commissione  tributaria  di  1  grado di Messina, 20 giugno 1977  &#13;
 della Commissione tributaria di 1 grado di Busto Arsizio  e  14  giugno  &#13;
 1977  della  Commissione tributaria di 1 grado di Lanciano, iscritte ai  &#13;
 nn.   469, 470, 471, 482, 483 e  514  del  registro  ordinanze  1977  e  &#13;
 pubblicate  nella  Gazzetta  Ufficiale  della  Repubblica  nn. 334, 340  &#13;
 dell'anno 1977 e n. 11 dell'anno 1978.                                   &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del  26 aprile 1978 il Giudice  &#13;
 relatore Guido Astuti.                                                   &#13;
     Ritenuto che con le  ordinanze  elencate  in  epigrafe  sono  state  &#13;
 sollevate,  in riferimento all'art. 53 Cost., questioni di legittimità  &#13;
 costituzionale degli artt. 3, 6, 11 e 14 del d.P.R. 26 ottobre 1972, n.  &#13;
 643 (Istituzione dell'imposta comunale sull'incremento di valore  degli  &#13;
 immobili),    denunziando    dette    disposizioni,   separatamente   o  &#13;
 congiuntamente,  in  quanto  nella  determinazione  dell'incremento  di  &#13;
 valore imponibile, e delle detrazioni da tale incremento in rapporto al  &#13;
 tempo  intercorso  tra la data di acquisto o di riferimento e quella di  &#13;
 alienazione o trasmissione dell'immobile, non consentirebbero  adeguato  &#13;
 apprezzamento dell'incidenza della svalutazione monetaria, sottoponendo  &#13;
 cosi all'imposizione plusvalenze nominali e non reali, in contrasto con  &#13;
 i principi della capacità contributiva e dell'eguaglianza tributaria;   &#13;
     che  con  l'ordinanza  n.  483/1977  è stata altresì sollevata in  &#13;
 riferimento  al  medesimo   parametro   costituzionale   questione   di  &#13;
 legittimità  dell'art.  15 dello stesso decreto legislativo, in quanto  &#13;
 le  aliquote  progressive  per  scaglioni  di  incremento   imponibile,  &#13;
 prescindendo    da    ogni    riferimento   di   carattere   temporale,  &#13;
 determinerebbero nell'applicazione dell'imposta un onere  fiscale  più  &#13;
 elevato  per  i  trasferimenti  immobiliari che si verificano a maggior  &#13;
 distanza di tempo dall'acquisto, con disparità di  trattamento  fra  i  &#13;
 contribuenti.                                                            &#13;
     Considerato  che le indicate questioni, proposte dalle ordinanze di  &#13;
 rimessione con varia prospettazione e con motivi in parte  diversi,  ma  &#13;
 aventi  contenuto  sostanzialmente  identico, richiedono, ai fini della  &#13;
 decisione di questa Corte, una considerazione unitaria;                  &#13;
     che il 18 dicembre 1977 è entrata in vigore la legge  16  dicembre  &#13;
 1977,  n.  904, con la quale è stato tra l'altro stabilito che ai fini  &#13;
 dell'imposta sull'incremento di valore degli immobili, di cui al d.P.R.  &#13;
 26 ottobre 1972, n. 643  e  successive  modificazioni,  "la  detrazione  &#13;
 prevista dall'art. 14 del decreto stesso è elevata al dieci per cento,  &#13;
 per  ogni  anno o frazione di anno superiore al semestre, successivo al  &#13;
 31 dicembre 1972, fino al 31 dicembre 1979" (art. 8, primo comma);  che  &#13;
 detta  disposizione,  nei  casi  in  cui il presupposto di applicazione  &#13;
 dell'imposta siasi verificato anteriormente alla  data  di  entrata  in  &#13;
 vigore  della legge, "si applica quando il termine per la presentazione  &#13;
 della dichiarazione non sia ancora scaduto alla data  predetta  nonché  &#13;
 agli  effetti  della  definizione  degli accertamenti in rettifica o di  &#13;
 ufficio relativi alle dichiarazioni presentate o che  avrebbero  dovuto  &#13;
 essere presentate prima della data stessa";                              &#13;
     che  di  conseguenza  si  rende  necessario  che i giudici a quibus  &#13;
 riesaminino il loro giudizio sulla rilevanza delle  proposte  questioni  &#13;
 di costituzionalità, tenendo conto della nuova normativa in vigore;     &#13;
     che occorre pertanto disporre la restituzione degli atti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione  degli  atti  alle  Commissioni tributarie  &#13;
 indicate in epigrafe.                                                    &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte Costituzionale, Palazzo della Consulta, il 27 aprile 1978.         &#13;
                                   F.to:  PAOLO  ROSSI - LUIGI OGGIONI -  &#13;
                                   LEONETTO AMADEI - EDOARDO VOLTERRA  -  &#13;
                                   GUIDO  ASTUTI  -  MICHELE  ROSSANO  -  &#13;
                                   ANTONINO DE STEFANO - LEOPOLDO ELIA -  &#13;
                                   GUGLIELMO ROEHRSSEN - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE.                             &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
