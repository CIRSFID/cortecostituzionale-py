<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>314</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:314</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/06/1995</data_decisione>
    <data_deposito>12/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 336 del  codice    &#13;
 penale promosso con ordinanza emessa l'8 febbraio 1995 dal Pretore di    &#13;
 Venezia,  Sezione  distaccata  di Chioggia, nei procedimenti penali a    &#13;
 carico di Fortuna Mario ed altro, iscritta al  n.  201  del  registro    &#13;
 ordinanze 1995 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 16, prima serie speciale, dell'anno 1995;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  14  giugno  1995  il  Giudice    &#13;
 relatore Giuliano Vassalli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Pretore di Venezia - Sezione distaccata di Chioggia, ha    &#13;
 sollevato, in riferimento agli artt. 3, 27, terzo comma, e  97  della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art. 336    &#13;
 del codice penale nella parte in cui prevede come minimo edittale  la    &#13;
 pena di mesi sei di reclusione.                                          &#13;
    Il giudice a quo si limita a richiamare i principi affermati dalla    &#13;
 Corte  nella  sentenza  n.  341 del 1994 in tema di oltraggio e, dopo    &#13;
 aver indicato a raffronto la fattispecie prevista dall'art.  610  del    &#13;
 codice  penale,  priva  di  minimo edittale, osserva che "la violenza    &#13;
 privata sta alla violenza o minaccia  a  pubblico  ufficiale  proprio    &#13;
 come l'ingiuria sta all'oltraggio". In conclusione, rileva il giudice    &#13;
 a quo, la previsione dell'indicato minimo edittale determinerebbe "un    &#13;
 irragionevole  bilanciamento tra la tutela dell'amministrazione e del    &#13;
 pubblico ufficiale e il valore della libertà personale".                &#13;
    2. - Nel giudizio è intervenuto il Presidente del  Consiglio  dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata non fondata. Osserva    &#13;
 l'Avvocatura  che  il  trattamento  sanzionatorio   investe   un'area    &#13;
 riservata   alla  discrezionalità  del  legislatore,  sia  pure  nel    &#13;
 rispetto del limite  della  ragionevolezza.  L'art.  336  del  codice    &#13;
 penale  si  sottrarrebbe,  dunque, alle dedotte censure in quanto "la    &#13;
 tutela differenziata della pubblica  amministrazione  corrisponde  ad    &#13;
 una  necessità  insopprimibile  dello Stato democratico". Neppure è    &#13;
 corretto  evocare  a  raffronto  il  diverso   trattamento   previsto    &#13;
 dall'art.  610  del codice penale, considerata la diversità dei beni    &#13;
 giuridici  protetti,  mentre  per  ciò  che  concerne   la   dedotta    &#13;
 violazione dell'art. 27, terzo comma, della Costituzione, il richiamo    &#13;
 si  rivela  improprio  giacché l'invocato parametro va riferito alla    &#13;
 sola fase di esecuzione della pena.<diritto>Considerato in diritto</diritto>1.  -  Il  Pretore  di  Venezia  - Sezione distaccata di Chioggia,    &#13;
 impugna, per contrasto con gli artt. 3, 27, terzo comma, e  97  della    &#13;
 Costituzione,  l'art.  336  del  codice  penale,  nella  parte in cui    &#13;
 prevede come minimo edittale la pena di mesi sei di reclusione.          &#13;
    A parere del remittente varrebbero infatti integralmente, anche in    &#13;
 relazione alla fattispecie oggetto di impugnativa, le  considerazioni    &#13;
 poste  a  fondamento  della  sentenza  n.  341 del 1994, con la quale    &#13;
 questa  Corte  ebbe  a  dichiarare  l'illegittimità   costituzionale    &#13;
 dell'art.  341,  primo  comma,  del codice penale, nella parte in cui    &#13;
 prevedeva il medesimo minimo edittale. Così come, opina il giudice a    &#13;
 quo, fu in quell'occasione operato un raffronto  tra  il  delitto  di    &#13;
 oltraggio  a  pubblico  ufficiale  e  quello di ingiuria, a identiche    &#13;
 conclusioni dovrebbe pervenirsi comparando fra  loro  la  fattispecie    &#13;
 prevista  dall'art.  336  del  codice penale e il delitto di violenza    &#13;
 privata di cui all'art. 610 dello stesso codice.                         &#13;
    2. - La questione è infondata,  dal  momento  che  nessuna  delle    &#13;
 considerazioni  svolte nella richiamata sentenza n. 341 del 1994 può    &#13;
 ritenersi pertinente alla  fattispecie  ora  sottoposta  a  scrutinio    &#13;
 della  Corte.  Già  l'art.  187 del codice penale del 1889, infatti,    &#13;
 prevedeva, in luogo delle blande sanzioni stabilite per il  reato  di    &#13;
 oltraggio,  la  pena  minima  di  tre mesi di reclusione per chiunque    &#13;
 avesse usato violenza o minaccia verso  un  pubblico  ufficiale  "per    &#13;
 costringerlo a fare o ad omettere un atto del suo ufficio", cosicché    &#13;
 non  potrebbe  in alcun modo sostenersi che la sanzione stabilita nel    &#13;
 minimo dalla norma oggetto di censura abbia  rappresentato,  come  la    &#13;
 pena  minima prevista per l'oltraggio, un unicum, generato dal codice    &#13;
 penale del 1930", quale  "prodotto  della  concezione  autoritaria  e    &#13;
 sacrale  dei  rapporti  tra  pubblici ufficiali e cittadini tipica di    &#13;
 quell'epoca storica e discendente  dalla  matrice  ideologica  allora    &#13;
 dominante".  V'è  anzi da osservare, a tal proposito, che vale nella    &#13;
 specie l'esatto reciproco di  quanto  il  giudice  a  quo  mostra  di    &#13;
 ritenere,  giacché  ove venisse accolto il petitum inteso a caducare    &#13;
 il minimo edittalmente previsto dall'art. 336 del codice  penale,  si    &#13;
 assimilerebbe,   sotto   questo   aspetto,  il  relativo  trattamento    &#13;
 sanzionatorio a quello ora stabilito per il delitto di oltraggio,  in    &#13;
 aperto  contrasto,  come  si  è  visto,  con  la  stessa  tradizione    &#13;
 codicistica, doverosamente attenta a rimarcare la  maggior  lesività    &#13;
 che  presenta una sia pur "minima" violenza o minaccia ad un pubblico    &#13;
 ufficiale rispetto ad una parimenti "minima" offesa al  suo  onore  o    &#13;
 prestigio.                                                               &#13;
    D'altra  parte,  questa Corte, nel porre a raffronto la disciplina    &#13;
 prevista dall'art. 196 del codice penale militare di pace con  quella    &#13;
 stabilita  dall'art.  336 del codice penale ordinario, non ha mancato    &#13;
 di rilevare come in quest'ultima ipotesi fosse "previsto un  elemento    &#13;
 teleologico  di consistente gravità - che qualifica il comportamento    &#13;
 dell'autore - diretto a costringere il soggetto passivo del  reato  a    &#13;
 compiere  un  atto  contrario  ai propri doveri o ad omettere un atto    &#13;
 d'ufficio" (v. sentenza n. 405 del 1994): un  elemento,  questo,  del    &#13;
 tutto  estraneo  alla  fattispecie  prevista dall'art. 610 del codice    &#13;
 penale, evocata dal remittente quale termine di comparazione, e  che,    &#13;
 pertanto,   adeguatamente   giustifica   il   differente  trattamento    &#13;
 sanzionatorio.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 336 del codice penale, sollevata, in riferimento agli artt.    &#13;
 3, 27, terzo comma, e 97 della Costituzione, dal Pretore di Venezia -    &#13;
 Sezione distaccata di Chioggia, con l'ordinanza in epigrafe.             &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 28 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 12 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
