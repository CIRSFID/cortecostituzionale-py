<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2015</anno_pronuncia>
    <numero_pronuncia>62</numero_pronuncia>
    <ecli>ECLI:IT:COST:2015:62</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CRISCUOLO</presidente>
    <relatore_pronuncia>Paolo Grossi</relatore_pronuncia>
    <redattore_pronuncia>Paolo Grossi</redattore_pronuncia>
    <data_decisione>24/03/2015</data_decisione>
    <data_deposito>16/04/2015</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>cessata materia del contendere</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alessandro CRISCUOLO; Giudici : Paolo Maria NAPOLITANO, Giuseppe FRIGO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, comma 299, della legge 24 dicembre 2012, n. 228 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge di stabilità 2013), promosso dalla Regione autonoma Sardegna con ricorso notificato il 26 febbraio 2013, depositato in cancelleria l'8 marzo 2013 ed iscritto al n. 41 del registro ricorsi 2013.&#13;
 Visto l'atto di costituzione del Presidente del Consiglio dei ministri;  &#13;
          udito nell'udienza pubblica del 24 marzo 2015 il Giudice relatore Paolo Grossi;&#13;
          uditi l'avvocato Massimo Luciani per la Regione autonoma Sardegna e l'avvocato dello Stato Stefano Varone per il Presidente del Consiglio dei ministri.&#13;
 Ritenuto che, con ricorso notificato il 26 febbraio 2013, e depositato in cancelleria il successivo 8 marzo (iscritto al n. 41 del registro ricorsi dell'anno 2013), la Regione autonoma Sardegna ha promosso, tra l'altro, questione di legittimità costituzionale dell'art. 1, comma 299, della legge 24 dicembre 2012, n. 228 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2013), in riferimento all'art. 8 della legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna);&#13;
 che, ad avviso della difesa regionale, tale disposizione, prevedendo che le maggiori entrate derivanti dall'attività di contrasto dell'evasione fiscale siano di fatto riservate all'erario, il quale le impiega per finanziare la riduzione della pressione fiscale, acquisisce alla disponibilità dello Stato maggiori entrate che dovrebbero essere di spettanza regionale;&#13;
 che, viceversa, le risorse derivanti dal recupero dell'evasione fiscale, se riferite a presupposti d'imposta maturati nel territorio regionale, sono comuni entrate erariali che non sono state acquisite dalla finanza pubblica solo per una patologia del sistema e che, dopo il loro recupero, non possono essere distratte in favore dello Stato;&#13;
 che, dunque, l'art. 1, comma 299, della legge n. 228 del 2012 contrasta con l'art. 8 dello statuto di autonomia speciale, perché il legislatore statale non può, in mancanza di disposizioni statutarie che consentano l'istituzione di riserve erariali, escludere la Regione dalla compartecipazione prevista dalla citata norma statutaria (come già affermato, con riferimento ad identica previsione, nella sentenza n. 241 del 2012);&#13;
 che, peraltro, la ricorrente rileva la inoperatività della clausola di salvaguardia prevista dall'art. 1, comma 554, della medesima legge n. 228 del 2012 (anch'esso oggetto di autonoma impugnazione da parte della Regione ricorrente), che, così come formulata - nel prevedere che «Le regioni a statuto speciale e le province autonome di Trento e di Bolzano attuano le disposizioni di cui alla presente legge nelle forme stabilite dai rispettivi statuti di autonomia e dalle relative norme di attuazione» -, è insufficiente a far salve le attribuzioni delle Regioni a statuto speciale; &#13;
 che, nel giudizio, si è costituito il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto il rigetto della impugnazione, affermando che una questione di legittimità costituzionale concernente una norma analoga è stata dichiarata non fondata da questa Corte con la sentenza n. 241 del 2012;&#13;
 che la Regione autonoma ricorrente ha depositato memoria in cui ribadisce i propri assunti.&#13;
 Considerato che è riservata a separate pronunce la decisione delle ulteriori questioni, promosse dalla Regione autonoma Sardegna col ricorso in epigrafe nei confronti di altre disposizioni della legge 24 dicembre 2012, n. 228 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2013);&#13;
 che la Regione ricorrente ha notificato in data 23 marzo 2015 atto di rinuncia parziale al ricorso, comprensiva anche della impugnazione dell'art. 1, comma 299, della medesima legge n. 228 del 2012;&#13;
 che, in ordine a tale rinuncia, il Presidente del Consiglio dei ministri non s'è pronunciato;&#13;
 che, in mancanza di formale accettazione, la rinuncia medesima comporta la cessazione della materia del contendere in relazione alla norma oggetto del presente giudizio (sentenza n. 19 del 2015).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 riservata a separate pronunce la decisione delle altre questioni di legittimità costituzionale riguardanti ulteriori disposizioni della legge 24 dicembre 2012, n. 228 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2013), promosse, con il ricorso di cui in epigrafe, dalla Regione autonoma Sardegna;&#13;
 dichiara cessata la materia del contendere in relazione alla questione di legittimità costituzionale dell'art. 1, comma 299, della legge 24 dicembre 2012, n. 228 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2013), promossa dalla Regione autonoma Sardegna con il ricorso indicato in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 marzo 2015.&#13;
 F.to:&#13;
 Alessandro CRISCUOLO, Presidente&#13;
 Paolo GROSSI, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 16 aprile 2015.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
