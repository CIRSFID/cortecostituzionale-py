<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>308</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:308</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1999</data_decisione>
    <data_deposito>14/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 206 del decreto    &#13;
 legislativo  30  aprile  1992,  n.  285  (Nuovo codice della strada),    &#13;
 promosso con ordinanza emessa il 31  dicembre  1997  dal  pretore  di    &#13;
 Lucca nel procedimento civile vertente tra Vaccaro Marzia e il comune    &#13;
 di Lucca, iscritta al n. 570 del registro ordinanze 1998 e pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  36,  prima  serie    &#13;
 speciale, dell'anno 1998.                                                &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  23 giugno 1999 il giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
   Ritenuto che nel corso di un  procedimento  civile  di  opposizione    &#13;
 avverso  la  cartella  esattoriale  emessa  contro soggetto cui erano    &#13;
 state contestate infrazioni a  norme  del  codice  della  strada,  il    &#13;
 pretore  di  Lucca  ha sollevato, con ordinanza del 31 dicembre 1997,    &#13;
 questione di legittimità costituzionale - in riferimento all'art. 3,    &#13;
 "secondo comma" (recte: primo  comma),  della  Costituzione  -  degli    &#13;
 artt.  "210" (recte: 206, come esattamente indicato nella motivazione    &#13;
 del provvedimento) del decreto legislativo 30  aprile  1992,  n.  285    &#13;
 (Nuovo  codice  della  strada)  e  27,  sesto  comma,  della legge 24    &#13;
 novembre 1981, n. 689 (Modifiche al sistema penale), nella  parte  in    &#13;
 cui  prevedono,  in  caso  di  ritardo  nel  pagamento della sanzione    &#13;
 amministrativa pecuniaria conseguente a violazione del  codice  della    &#13;
 strada,  una  maggiorazione della somma dovuta, pari ad un decimo per    &#13;
 ogni semestre a decorrere da quello in cui la  sanzione  è  divenuta    &#13;
 esigibile e fino a quello in cui il ruolo è trasmesso all'esattore;     &#13;
     che,  secondo il rimettente, siffatta previsione di un "interesse    &#13;
 composto"  superiore  al  20%  annuo   in   favore   della   pubblica    &#13;
 amministrazione,  determinerebbe, attesa l'identità dei presupposti,    &#13;
 un'ingiustificata disparità di trattamento rispetto  al  regime  dei    &#13;
 normali  rapporti  di  credito  tra  privati, sia perché questi sono    &#13;
 assoggettati  al  meno  oneroso  tasso   legale,   sia   perché   la    &#13;
 maggiorazione  opererebbe  anche  nel  caso  di inerzia colposa della    &#13;
 pubblica amministrazione, senza che sia applicabile  il  disposto  di    &#13;
 cui all'art. 1227 cod. civ;                                              &#13;
     che,  quanto  alla  rilevanza  della  questione, il giudice a quo    &#13;
 osserva  che  l'accoglimento   della   questione   comporterebbe   il    &#13;
 "ricalcolo degli interessi" dovuti dalla parte ricorrente;               &#13;
     che  è  intervenuto  in giudizio il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura   dello   Stato,    &#13;
 preliminarmente  eccependo  l'inammissibilità  della  questione  per    &#13;
 difetto di motivazione sulle circostanze del fatto  (in  particolare,    &#13;
 per  la mancata specificazione dell'infrazione in relazione a cui era    &#13;
 stata applicata la sanzione amministrativa, per la non pertinenza del    &#13;
 denunciato art. "210" del Nuovo codice della strada e per la  mancata    &#13;
 denuncia  dell'art.  27  della  legge n. 689 del 1981) e, nel merito,    &#13;
 chiedendo la declaratoria di manifesta infondatezza della questione.     &#13;
   Considerato  che  non  ha   consistenza   l'eccezione   preliminare    &#13;
 d'inammissibilità,   costituendo   sufficiente   motivazione   sulla    &#13;
 rilevanza della sollevata questione, l'asserzione del  rimettente  di    &#13;
 dover  applicare  le denunciate norme nel giudizio a quo, concernente    &#13;
 la  riscossione  d'una   sanzione   amministrativa   pecuniaria   per    &#13;
 violazione del codice della strada;                                      &#13;
     che, nel merito, il pretore di Lucca - denunciando, attraverso il    &#13;
 richiamo  effettuato  dall'art.  206  cod.  strada,  l'art. 27, sesto    &#13;
 comma, della legge 24 novembre 1981,  n.  689  -  muove  dall'erronea    &#13;
 premessa  dell'identità  di  natura  e  funzione dell'istituto degli    &#13;
 interessi moratori o di pieno diritto nelle obbligazioni tra  privati    &#13;
 e  dell'istituto  delle  maggiorazioni  delle sanzioni amministrative    &#13;
 pecuniarie in caso di ritardo nel pagamento;                             &#13;
     che, infatti, la maggiorazione  per  ritardo  prevista  dall'art.    &#13;
 27,  sesto  comma,  della  legge n. 689 del 1981 a carico dell'autore    &#13;
 dell'illecito amministrativo, cui sia  stata  inflitta  una  sanzione    &#13;
 pecuniaria,  ha  funzione,  non  già  risarcitoria  o corrispettiva,    &#13;
 bensì di sanzione aggiuntiva, nascente al  momento  in  cui  diviene    &#13;
 esigibile la sanzione principale;                                        &#13;
     che,  stante  la  diversità  di presupposto e di finalità delle    &#13;
 discipline menzionate, manca  dunque  l'omogeneità  dei  termini  di    &#13;
 raffronto,  necessaria  a fondare un eventuale giudizio di disparità    &#13;
 di trattamento rilevante ai sensi dell'art. 3, primo comma, Cost;        &#13;
     che, d'altronde, neppure con riguardo al regime  ordinario  delle    &#13;
 obbligazioni  tra  privati  sarebbe pertinente il richiamo, contenuto    &#13;
 nell'ordinanza di rimessione, al capoverso dell'art. 1227  cod.  civ.    &#13;
 Infatti,  l'onere  di  diligenza  che  questa  norma  fa  gravare sul    &#13;
 creditore non si estende alla sollecitudine nell'agire a  tutela  del    &#13;
 proprio  credito  onde evitare maggiori danni, i quali viceversa sono    &#13;
 da imputare esclusivamente alla  condotta  del  debitore,  tenuto  al    &#13;
 tempestivo adempimento della sua obbligazione;                           &#13;
     che,    pertanto,    la   questione   sollevata   va   dichiarata    &#13;
 manifestamente infondata sotto tutti i profili  prospettati.             &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 206 del decreto legislativo 30 aprile 1992    &#13;
 n. 285 (Nuovo codice della strada) e 27, sesto comma, della legge  24    &#13;
 novembre  1981,  n.  689 (Modifiche al sistema penale), sollevata, in    &#13;
 riferimento all'art. 3, primo comma, della Costituzione, dal  pretore    &#13;
 di Lucca con l'ordinanza indicata in epigrafe.                           &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta il 7 luglio 1999.                                 &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 14 luglio 1999.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
