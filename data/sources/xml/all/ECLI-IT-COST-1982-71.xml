<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>71</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:71</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/04/1982</data_decisione>
    <data_deposito>16/04/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale della legge approvata  &#13;
 il 23 aprile 1980 e riapprovata  il  30  dicembre  1980  dal  Consiglio  &#13;
 regionale   della   Campania,   recante   "Nuova   normativa   per   la  &#13;
 classificazione delle aziende ricettive alberghiere e all'aria aperta",  &#13;
 promosso  con  ricorso  del  Presidente  del  Consiglio  dei   ministri  &#13;
 notificato  il  16  gennaio  1981,  depositato  in  cancelleria  il  26  &#13;
 successivo ed iscritto al n. 3 del registro ricorsi 1981.                &#13;
     Visto l'atto di costituzione della Regione Campania;                 &#13;
     udito  nell'udienza  pubblica  del  24  febbraio  1982  il  Giudice  &#13;
 relatore Livio Paladin;                                                  &#13;
     uditi  l'avvocato dello Stato Giorgio Azzariti per il ricorrente, e  &#13;
 l'avv. Francesco D'Onofrio per la Regione.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ricorso notificato il 16 e depositato il 26 gennaio  1981,  &#13;
 il  Presidente  del  Consiglio  dei  ministri  ha promosso questione di  &#13;
 legittimità  costituzionale  della  legge  approvata   dal   Consiglio  &#13;
 regionale  della  Campania  il  23  aprile e riapprovata il 30 dicembre  &#13;
 1980, recante "Nuova normativa per  la  classificazione  delle  aziende  &#13;
 ricettive  alberghiere  e  all'aria  aperta": con cui tali aziende sono  &#13;
 classificate in categorie contraddistinte dal numero di stelle - da uno  &#13;
 a cinque - sulla base di determinati requisiti,  indicati  in  apposite  &#13;
 tabelle allegate alla legge medesima.                                    &#13;
     Detta  disciplina contrasterebbe con l'art. 117, primo comma, della  &#13;
 Costituzione, ledendo l'esigenza di omogeneità su tutto il  territorio  &#13;
 nazionale  -  dei criteri di classificazione delle aziende alberghiere,  &#13;
 già posti dalle leggi dello Stato (r.d.l. 18 gennaio 1937, n. 975, e 5  &#13;
 settembre 1938, n. 1729), e violando pertanto il  limite  dei  principi  &#13;
 fondamentali stabiliti in materia di turismo ed industria alberghiera.   &#13;
     2.  -  La Regione Campania, costituitasi il 20 febbraio 1981, al di  &#13;
 là del termine  fissato  dall'art.  23,  ultimo  comma,  delle  "Norme  &#13;
 integrative  per  i  giudizi  davanti  alla  Corte"  (ma  invocando  la  &#13;
 sospensione disposta dall'art. 4 del decreto-legge 26 novembre 1980, n.  &#13;
 776, convertito nella legge 22 dicembre 1980, n. 874), contesta in toto  &#13;
 il fondamento del ricorso.                                               &#13;
     In  particolare,  "l'assoluta  indeterminatezza"  dei rilievi mossi  &#13;
 dallo Stato non consentirebbe  di  procedere  a  puntuali  confutazioni  &#13;
 dell'asserita  violazione  dei  principi  della legislazione statale; e  &#13;
 dimostrerebbe invece - al di là della forma "la volontà  dello  Stato  &#13;
 di  impedire l'esercizio delle potestà legislative regionali in attesa  &#13;
 di una propria disciplina in materia di classificazione alberghiera".<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Va  anzitutto  precisato  che  la  Regione  Campania  si  è  &#13;
 costituita  fuori termine, anche a voler tenere conto della sospensione  &#13;
 disposta dall'art. 4 del d.l. n. 776 del 1980, convertito  nella  legge  &#13;
 n. 874 del medesimo anno.                                                &#13;
     Vero  è  che l'art. 4, primo e secondo comma, del predetto decreto  &#13;
 come modificato dalla legge di conversione, ha previsto la  sospensione  &#13;
 -  fino  al 31 gennaio 1981 - di tutti i termini processuali, "a favore  &#13;
 delle persone fisiche o giuridiche residenti, domiciliate o aventi sede  &#13;
 nelle regioni Basilicata e Campania. Ma il quarto  comma  dell'articolo  &#13;
 stesso  aggiunge  che  "la  sospensione  opera  per  i soli termini che  &#13;
 scadono nel periodo compreso fra il 23 novembre 1980 ed il  31  gennaio  &#13;
 1981";  mentre  i  venti  giorni  dal  deposito  del  ricorso,  fissati  &#13;
 nell'art. 23, ultimo comma, delle  "Norme  integrative  per  i  giudizi  &#13;
 davanti  alla Corte", venivano a scadere al di là di tale periodo, dal  &#13;
 momento che il deposito è stato effettuato come già si è chiarito in  &#13;
 narrativa - il 26 gennaio 1981.                                          &#13;
     2. - Nel merito, la questione può dirsi in sostanza  già  risolta  &#13;
 dalla Corte - secondo l'avviso concordemente espresso dalle parti nella  &#13;
 pubblica udienza - mediante la sentenza n. 70 del 1981.                  &#13;
     Quella  decisione  ha infatti dichiarato non fondate le impugnative  &#13;
 della legge della Regione Puglia riapprovata il 26 aprile 1979, in tema  &#13;
 di "disciplina della classificazione alberghiera",  della  legge  della  &#13;
 Regione   Piemonte   riapprovata   il  10  ottobre  1979,  in  tema  di  &#13;
 "classificazione  delle  aziende  alberghiere",  e  della  legge  della  &#13;
 Regione  Umbria  riapprovata il 4 febbraio 1980, in tema di "disciplina  &#13;
 della classificazione delle aziende ricettive, alberghiere  e  all'aria  &#13;
 aperta",   promosse  dal  Presidente  del  Consiglio  dei  ministri  in  &#13;
 riferimento al primo comma dell'art. 117 Cost..  Rispetto  al  caso  in  &#13;
 esame,  identiche  erano  le  censure  allora  formulate dal Governo ed  &#13;
 analoghi i contenuti normativi degli atti  impugnati  (con  particolare  &#13;
 riguardo  alla  predetta  legge  umbra): sicché, per le stesse ragioni  &#13;
 allora addotte, anche il presente ricorso dev'essere respinto.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 della  legge regionale riapprovata dal Consiglio della Regione Campania  &#13;
 il 30 dicembre 1980, recante "Nuova normativa  per  la  classificazione  &#13;
 delle  aziende  ricettive  alberghiere e all'aria aperta", promossa dal  &#13;
 Presidente del Consiglio dei ministri in  riferimento  all'art.    117,  &#13;
 primo comma, della Costituzione, con il ricorso indicato in epigrafe.    &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 2 aprile 1982.                                &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   -  ANTONINO  DE  STEFANO  - GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELI DUCCI - ALBERTO MALAGUGINI  &#13;
                                   -  LIVIO PALADIN - ANTONIO LA PERGOLA  &#13;
                                   -  VIRGILIO   ANDRIOLI   -   GIUSEPPE  &#13;
                                   FERRARI  -  FRANCESCO SAJA - GIOVANNI  &#13;
                                   CONSO.                                 &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
