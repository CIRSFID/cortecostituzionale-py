<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>490</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:490</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1990</data_decisione>
    <data_deposito>22/10/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. &#13;
 Luigi MENGONI, &#13;
 prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  12, ultimo    &#13;
 comma, della legge 30 marzo 1971, n. 118 (Conversione  in  legge  del    &#13;
 decreto-legge  30  gennaio  1971,  n.  5,  e nuove norme in favore di    &#13;
 mutilati  ed  invalidi  civili),  come  autenticamente   interpretato    &#13;
 dall'art.  1,  primo  comma,  della  legge  13  dicembre 1986, n. 912    &#13;
 (Interpretazione autentica dell'art. 12, ultimo comma, della legge 30    &#13;
 marzo  1971,  n.  118,  e  dell'art.  7, ultimo comma, della legge 26    &#13;
 maggio 1970, n. 381, in  materia  di  quote  di  assegni  o  pensioni    &#13;
 spettanti  agli  eredi di mutilati o invalidi civili e di sordomuti),    &#13;
 promossi con le seguenti ordinanze:                                      &#13;
    1)  ordinanza  emessa  il 20 ottobre 1989 dal Pretore di Parma nel    &#13;
 procedimento civile vertente tra  Barbieri  Pomezia  e  il  Ministero    &#13;
 dell'interno,  iscritta  al  n.  344  del  registro  ordinanze 1990 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  24,  prima    &#13;
 serie speciale, dell'anno 1990;                                          &#13;
   2)  ordinanza  emessa  il  26 ottobre 1989 dal Pretore di Parma nel    &#13;
 procedimento civile vertente tra  Bologni  Silvano  ed  il  Ministero    &#13;
 dell'Interno,  iscritta  al  n.  345  del  registro  ordinanze 1990 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  24,  prima    &#13;
 serie speciale, dell'anno 1990;                                          &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 15 ottobre 1990 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che il Pretore di Parma, nei giudizi civili tra Barbieri    &#13;
 Pomezia ed il Ministero dell'interno  e  tra  Bologni  Silvano  e  lo    &#13;
 stesso  Ministero  dell'interno, aventi ad oggetto il pagamento della    &#13;
 indennità di accompagnamento e della pensione di inabilità  civile,    &#13;
 con  ordinanze  del 26 ottobre 1989 (R.O. nn. 344 e 345 del 1990), ha    &#13;
 sollevato questione  di  legittimità  costituzionale  dell'art.  12,    &#13;
 ultimo  comma, della legge 30 marzo 1971, n. 118, come autenticamente    &#13;
 interpretato dall'art. 1, primo comma, della legge 13 dicembre  1986,    &#13;
 n.  912,  nella  parte  in  cui  subordina il diritto degli eredi del    &#13;
 mutilato  e  dell'invalido  civile  di  percepire  la   pensione   di    &#13;
 invalidità  o  inabilità  alla condizione che la morte del de cuius    &#13;
 sia avvenuta in epoca  successiva  all'accertamento  dello  stato  di    &#13;
 invalidità da parte dell'apposita Commissione sanitaria;                &#13;
      che secondo il giudice remittente sarebbero violati:                &#13;
        a)  l'art.  3  della Costituzione in quanto, mentre si ritiene    &#13;
 che  l'accertamento  dell'invalidità  debba  essere  effettuato   in    &#13;
 persona  dell'invalido,  d'altra  parte si afferma che la sussistenza    &#13;
 delle  condizioni  per  il   riconoscimento   della   indennità   di    &#13;
 accompagnamento può accertarsi su base documentale;                     &#13;
        b)  gli artt. 24 e 113 della Costituzione in quanto il decesso    &#13;
 dell'interessato  durante  il  giudizio  di   accertamento   dovrebbe    &#13;
 comportare   l'estinzione  del  processo  e  del  diritto  soggettivo    &#13;
 dell'erede alle  quote  già  maturate  senza  che  si  verifichi  la    &#13;
 successione nel processo;                                                &#13;
      che  le  parti  private  Barbieri  Pomezia  e  Bologni  Silvano,    &#13;
 costituitesi   nel   giudizio,   hanno   richiesto    una    sentenza    &#13;
 interpretativa di rigetto o una declaratoria di illegittimità;          &#13;
      che,  invece, l'Avvocatura Generale dello Stato, intervenuta nei    &#13;
 due giudizi  in  rappresentanza  del  Presidente  del  Consiglio  dei    &#13;
 ministri,  ha concluso per la manifesta infondatezza della questione,    &#13;
 essendo identica ad altre già decise nello stesso senso;                &#13;
    Considerato  che  i  due  giudizi,  siccome  prospettano la stessa    &#13;
 questione, per evidenti  ragioni  di  connessione,  vanno  riuniti  e    &#13;
 decisi con un unico provvedimento;                                       &#13;
      che   la  questione  è  stata  già  dichiarata  manifestamente    &#13;
 infondata (ordd. nn. 61, 264, 475 e 486 del 1989; n. 391 del 1990);      &#13;
      che  non  sono  stati dedotti motivi nuovi e diversi che possano    &#13;
 portare ad un mutamento della decisione;                                 &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riunisce  i  giudizi,  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale dell'art. 12, ultimo  comma,    &#13;
 della  legge  30  marzo  1971,  n.  118  (Conversione  in  legge  del    &#13;
 decreto-legge 30 gennaio 1971, n. 5,  e  nuove  norme  in  favore  di    &#13;
 mutilati   ed  invalidi  civili),  come  autenticamente  interpretato    &#13;
 dall'art. 1, primo comma,  della  legge  13  dicembre  1986,  n.  912    &#13;
 (Interpretazione autentica dell'art. 12, ultimo comma, della legge 30    &#13;
 marzo 1971, n. 118, e dell'art.  7,  ultimo  comma,  della  legge  26    &#13;
 maggio  1970,  n.  381,  in  materia  di  quote di assegni o pensioni    &#13;
 spettanti agli eredi di mutilati o invalidi civili e  di  sordomuti),    &#13;
 in  riferimento  agli  art. 3, 24 e 113 della Costituzione, sollevata    &#13;
 dal Pretore di Parma con le ordinanze indicate in epigrafe.              &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1990.                              &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 22 ottobre 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
