<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>155</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:155</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/06/1971</data_decisione>
    <data_deposito>30/06/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 688,  secondo  &#13;
 comma,  del  codice  penale,  promosso con ordinanza emessa il 27 marzo  &#13;
 1970 dal pretore di Gardone  Val  Trompia  nel  procedimento  penale  a  &#13;
 carico  di Rambaldini Ettore, iscritta al n. 221 del registro ordinanze  &#13;
 1970 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 222  del  &#13;
 2 settembre 1970.                                                        &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di  consiglio  del  4  giugno  1971  il  Giudice  &#13;
 relatore Paolo Rossi.                                                    &#13;
     Ritenuto  che  il  pretore  di  Gardone  Val  Trompia  ha sollevato  &#13;
 questione incidentale di  legittimità  costituzionale  dell'art.  688,  &#13;
 secondo comma, del codice penale, che prevede la pena dell'arresto da 3  &#13;
 a  6 mesi per chiunque venga colto in stato di manifesta ubriachezza in  &#13;
 luogo  pubblico  o  aperto  al  pubblico,  se  l'ubriaco  risulti  già  &#13;
 condannato  per  delitto  non  colposo  contro  la vita o l'incolumità  &#13;
 individuale, mentre il primo comma dello  stesso  articolo  commina  la  &#13;
 pena alternativa dell'ammenda o dell'arresto fino a 6 mesi, per chi non  &#13;
 abbia tali specifici precedenti penali;                                  &#13;
     che,  secondo  l'ordinanza  di remissione, esisterebbe dubbio sulla  &#13;
 legittimità  di  tale  norma  in  quanto  essa,  contro  il  principio  &#13;
 dell'uguaglianza  sancito dall'art. 3 della Costituzione, imporrebbe un  &#13;
 trattamento discriminatorio e più  severo  in  considerazione  di  una  &#13;
 condizione  personale  del  soggetto attivo, quella di pregiudicato per  &#13;
 delitto contro la vita e l'incolumità, che  non  dovrebbe  qualificare  &#13;
 l'individuo, una volta per sempre, come particolarmente pericoloso.      &#13;
     Considerato  che  la Corte costituzionale ha già ritenuto privo di  &#13;
 ragion d'essere (sentenze n. 110 del 1968 e n. 100 del 1971) il  dubbio  &#13;
 relativo  alla  legittimità  di  un  trattamento differenziato per gli  &#13;
 autori di reati, anche all'infuori delle ipotesi regolate nel libro  I,  &#13;
 titolo IV, capo II del codice penale;                                    &#13;
     che   le   "condizioni   personali"  collocate  dall'art.  3  della  &#13;
 Costituzione sullo stesso piano del sesso, della razza,  della  lingua,  &#13;
 della   religione,   delle   opinioni   politiche  per  escludere  ogni  &#13;
 discriminazione fra  i  cittadini  non  sono  quelle  che  derivano  da  &#13;
 un'attività criminosa del soggetto;                                     &#13;
     che  il principio di uguaglianza è invocabile in situazioni uguali  &#13;
 o giuridicamente comparabili: razionalmente, quindi, la legge distingue  &#13;
 fra l'incensurato e il recidivo  nella  commisurazione  delle  pene,  e  &#13;
 altrettanto  giustamente,  nel  caso  dell'art. 688, secondo comma, del  &#13;
 codice penale, ritiene più pericoloso l'ubriaco  già  condannato  per  &#13;
 delitto doloso contro la vita o l'incolumità.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     avvalendosi  dei  poteri di cui agli artt. 26, secondo comma, della  &#13;
 legge  11  marzo  1953,  n.  87,  e  9,  secondo  comma,  delle   Norme  &#13;
 integrative,  dichiara  la  manifesta  infondatezza  della questione di  &#13;
 legittimità costituzionale dell'art. 688, secondo  comma,  del  codice  &#13;
 penale, sollevata, con l'ordinanza 27 marzo 1970 del pretore di Gardone  &#13;
 Val Trompia, in riferimento all'art.  3 della Costituzione.              &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 18 giugno 1971.         &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
