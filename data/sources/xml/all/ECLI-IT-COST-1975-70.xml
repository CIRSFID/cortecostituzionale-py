<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>70</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:70</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/03/1975</data_decisione>
    <data_deposito>25/03/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 512, n. 2,  &#13;
 del codice di procedura penale, promosso con  ordinanza  emessa  il  24  &#13;
 ottobre  1972  dal tribunale di Napoli nel procedimento penale a carico  &#13;
 di Capasso Ernesto ed altra, iscritta al n. 43 del  registro  ordinanze  &#13;
 1973  e  pubblicata nella Gazzetta Ufficiale della Repubblica n. 81 del  &#13;
 28 marzo 1973.                                                           &#13;
     Udito nella camera di consiglio del  23  gennaio  1975  il  Giudice  &#13;
 relatore Paolo Rossi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di  un procedimento d'appello il tribunale di Napoli ha  &#13;
 sollevato  questione   incidentale   di   legittimità   costituzionale  &#13;
 dell'art. 512, n. 2, del codice di procedura penale, nella parte in cui  &#13;
 esclude  l'appello  dell'imputato  avverso  la sentenza del pretore che  &#13;
 l'abbia  prosciolto  per  amnistia  in  conseguenza  del  giudizio   di  &#13;
 equiparazione  tra circostanze aggravanti ed attenuanti (generiche), in  &#13;
 riferimento al principio costituzionale d'eguaglianza ed al diritto  di  &#13;
 difesa.                                                                  &#13;
     osserva  il  giudice  a  quo che la sentenza di proscioglimento per  &#13;
 amnistia pronunciata a seguito del giudizio  di  comparazione  previsto  &#13;
 dall'art.  69  c.p., implicando il riconoscimento della responsabilità  &#13;
 dell'imputato in ordine ai fatti ascrittigli, gli reca pregiudizio  sia  &#13;
 sotto il profilo morale, sia sotto quello giuridico per gli effetti del  &#13;
 giudicato  penale  nel  giudizio  di  danno o in altri giudizi civili o  &#13;
 amministrativi (artt. 27 e 28 c.p.p.).                                   &#13;
     La  violazione  dell'art.  3  Cost.  risulterebbe   dalla   diversa  &#13;
 disciplina  apprestata  nei  confronti  dell'analoga  situazione in cui  &#13;
 versa il minore prosciolto per concessione del perdono giudiziale,  cui  &#13;
 è  riconosciuto  il  diritto  d'appello,  mentre la privazione di tale  &#13;
 impugnativa, e per giunta  nei  confronti  della  sola  parte  privata,  &#13;
 lederebbe l'inviolabilità del diritto alla difesa.                      &#13;
     Nessuna parte si è costituita in questa sede.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte  è  chiamata  a  decidere  se  contrasti  o  meno con il  &#13;
 principio d'eguaglianza e con il diritto  di  difesa  (artt.  3,  primo  &#13;
 comma,  e  24,  secondo  comma,  Cost.) l'art. 512, n. 2, del codice di  &#13;
 procedura penale, nella parte  in  cui  esclude  il  diritto  del  solo  &#13;
 imputato  di  appellare  contro  la  sentenza  del  pretore che l'abbia  &#13;
 prosciolto per amnistia in conseguenza del giudizio di comparazione tra  &#13;
 circostanze aggravanti ed attenuanti, anche in raffronto  alla  diversa  &#13;
 disciplina  vigente  per  colui  cui  sia  stato  concesso  il  perdono  &#13;
 giudiziale, ed ai maggiori poteri attribuiti all'accusa.                 &#13;
     La questione è fondata.                                             &#13;
     Va innanzitutto precisato che oggetto del presente giudizio non  è  &#13;
 la disposizione escludente il diritto di appellare ogni sentenza che in  &#13;
 dibattimento  abbia  dichiarato  l'estinzione del reato per intervenuta  &#13;
 amnistia,  bensì  la  più  limitata  norma  che  impedisce  l'appello  &#13;
 dell'imputato  prosciolto  per  amnistia  a  seguito  del  giudizio  di  &#13;
 comparazione tra circostanze aggravanti ed attenuanti.                   &#13;
     Mentre  di  regola,  ove  non  sussistano  le  condizioni  previste  &#13;
 dall'art.   152   c.p.p.,   l'applicazione   dell'amnistia  postula  il  &#13;
 riconoscimento, da parte dell'organo  giudicante,  che  le  ipotesi  di  &#13;
 reato  addebitate  agli  imputati rientrino, astrattamente considerate,  &#13;
 tra quelle per le  quali  è  stata  concessa  amnistia,  sicché  alla  &#13;
 relativa applicazione può procedersi anche in istruttoria, nei casi in  &#13;
 esame, occorre procedere al dibattimento.  Invero non è sufficiente un  &#13;
 giudizio  ipotetico,  formulato  allo  stato  degli  atti,  ma  occorre  &#13;
 valutare in concreto la condotta dell'imputato al fine di accertare  in  &#13;
 quella  sede se il fatto sussista, se l'imputato lo abbia commesso e se  &#13;
 sia previsto dalla legge come reato; è soltanto sul presupposto di  un  &#13;
 giudizio  affermativo  di  colpevolezza  che  potranno  aver  luogo  la  &#13;
 concessione  delle   attenuanti   generiche   ed   il   proscioglimento  &#13;
 dell'imputato   per   estinzione   del   reato.  Da  ciò  discende  la  &#13;
 possibilità che le sentenze di proscioglimento per amnistia arrechino,  &#13;
 a seconda dei casi,  un  diverso  pregiudizio  morale  e  giuridico  al  &#13;
 soggetto  prosciolto.  Può corrispondentemente giustificarsi, a favore  &#13;
 dello stesso, una maggiore o minore tutela, pur  nella  imprescindibile  &#13;
 rinunciabilità all'amnistia (sent. n. 175 del 1971).                    &#13;
     La norma in esame concerne dunque un proscioglimento caratterizzato  &#13;
 da  un  riconoscimento  di  colpevolezza,  idoneo  a  produrre  effetti  &#13;
 negativi in  altri  giudizi  civili  e  amministrativi.    Essa  quindi  &#13;
 sopprime  ingiustificatamente  taluni  modi  generali d'esercizio della  &#13;
 difesa, escludendo  il  solo  imputato  dal  diritto  di  appellare  la  &#13;
 sentenza di primo grado.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale dell'art. 512, n. 2, del  &#13;
 codice di procedura penale, nella  parte  in  cui  esclude  il  diritto  &#13;
 dell'imputato   di  appellare  la  sentenza  del  pretore  che  l'abbia  &#13;
 prosciolto per amnistia a seguito  del  giudizio  di  comparazione  tra  &#13;
 circostanze aggravanti ed attenuanti.                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 20 marzo 1975.          &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIOVANNI  &#13;
                                   BATTISTA  BENEDETTI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE ROCCHETTI  -  &#13;
                                   ENZO  CAPALOZZA  VINCENZO  -  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  - EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO.                               &#13;
                                   ARDUINO SALUSTRI Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
