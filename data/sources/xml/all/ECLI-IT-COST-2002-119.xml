<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>119</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:119</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VARI</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>10/04/2002</data_decisione>
    <data_deposito>12/04/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Massimo VARI; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, &#13;
Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco &#13;
AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 8, commi 7 e 8, &#13;
del  d.l.  19 settembre  1992,  n. 384  (Misure urgenti in materia di &#13;
previdenza,  di  sanità  e di pubblico impiego, nonché disposizioni &#13;
fiscali),  convertito,  con  modificazioni,  dalla  legge 14 novembre &#13;
1992,  n. 438,  promosso  con  ordinanza  emessa il 27 settembre 1999 &#13;
dalla   Commissione  tributaria  regionale  di  Venezia  sul  ricorso &#13;
proposto  da  Agosti  Paolo  contro  Ufficio  del  Registro di Schio, &#13;
iscritta  al  n. 588  del  registro ordinanze 2001 e pubblicata nella &#13;
Gazzetta   Ufficiale   della  Repubblica  n. 33,  1ª serie  speciale, &#13;
dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio  del 13 marzo 2002 il giudice &#13;
relatore Francesco Amirante. &#13;
    Ritenuto  che nel corso di un procedimento tributario in grado di &#13;
appello  proposto  contro  un avviso di accertamento riguardante c.d. &#13;
beni  di  lusso,  la  Commissione  tributaria regionale di Venezia ha &#13;
sollevato, in riferimento agli articoli 2, 3 e 53 della Costituzione, &#13;
questione  di  legittimità  costituzionale dell'art. 8, commi 7 e 8, &#13;
del  d.l.  19 settembre  1992,  n. 384  (Misure urgenti in materia di &#13;
previdenza,  di  sanità  e di pubblico impiego, nonché disposizioni &#13;
fiscali),  convertito con modificazioni dalla legge 14 novembre 1992, &#13;
n. 438; &#13;
        che  la commissione remittente osserva che il contribuente ha &#13;
proposto  ricorso, già respinto in primo grado, avverso un avviso di &#13;
tassazione  di  un  motociclo  di  cilindrata 650 con potenza di nove &#13;
cavalli,   sostenendo  l'illegittimità  costituzionale  del  tributo &#13;
fondato   sulla  norma  impugnata,  in  quanto  ritenuto  non  idoneo &#13;
rivelatore di ricchezza; &#13;
        che  in  sede  di  appello  il  ricorrente  ha  insistito nel &#13;
prospettare l'illegittimità costituzionale della norma in questione, &#13;
ritenendo  che  la  tassazione  dei  motocicli  con  potenza  fiscale &#13;
superiore  a  sei  cavalli,  posta  a  carico di coloro i quali erano &#13;
intestatari  del  bene  presso il pubblico registro in un determinato &#13;
periodo  di  tempo,  sia  in  contrasto con gli artt. 2, 3 e 53 della &#13;
Costituzione; &#13;
        che  il  giudice a quo ha sollevato questione di legittimità &#13;
costituzionale  della  norma  impugnata nella parte in cui impone, in &#13;
caso di mancato pagamento del tributo, una sovrattassa pari al doppio &#13;
del tributo oltre a lire seicentomila; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo  che  la questione sia dichiarata inammissibile per &#13;
omessa  indicazione  delle  ragioni  che dovrebbero supportarla e, in &#13;
subordine,  infondata,  trattandosi  di questione già dichiarata non &#13;
fondata  da questa Corte con le ordinanze n. 475 del 1994, n. 355 del &#13;
1995 e n. 471 del 1997. &#13;
    Considerato  che  la  Commissione  tributaria  rimettente  si  è &#13;
limitata   a  fare  proprie  le  argomentazioni  svolte  dalla  parte &#13;
ricorrente  circa  la  presunta  illegittimità  costituzionale della &#13;
norma, senza compiere alcuna diretta osservazione sull'argomento, con &#13;
ciò  venendo  meno al principio di autosufficienza dell'ordinanza di &#13;
remissione   che   anima  il  giudizio  incidentale  di  legittimità &#13;
costituzionale; &#13;
        che  sussiste,  inoltre,  un'evidente  contraddizione  tra la &#13;
motivazione dell'ordinanza - la quale sembra contestare l'istituzione &#13;
stessa   di  un  tributo  straordinario  sui  motocicli  con  potenza &#13;
superiore  a  sei  cavalli  -  e  la  prospettazione  di un dubbio di &#13;
legittimità  costituzionale  dei  commi  7  e 8 dell'art. 8 del d.l. &#13;
n. 384  del  1992,  nei  quali  sono  previste  le  sanzioni relative &#13;
all'omessa   presentazione   della   dichiarazione  ed  all'omesso  o &#13;
insufficiente pagamento del tributo medesimo; &#13;
        che  il  giudice  a quo, inoltre, non specifica per quale dei &#13;
due  comportamenti  previsti dalla norma impugnata sia stato promosso &#13;
il  procedimento  sottoposto  al  suo  giudizio, il che è tanto più &#13;
importante  in  quanto la sovrattassa di lire seicentomila, stabilita &#13;
per  l'omessa  presentazione  della dichiarazione, è stata soppressa &#13;
dall'art. 2, comma 163, della legge 23 dicembre 1996, n. 662; &#13;
        che  la presente questione, quindi, essendo stata prospettata &#13;
con   motivazione   carente   e   contraddittoria,   deve   ritenersi &#13;
manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità   costituzionale   dell'art. 8,   commi   7   e  8,  del &#13;
decreto-legge 19 settembre 1992, n. 384 (Misure urgenti in materia di &#13;
previdenza,  di  sanità  e di pubblico impiego, nonché disposizioni &#13;
fiscali),  convertito con modificazioni dalla legge 14 novembre 1992, &#13;
n. 438,  sollevata,  in  riferimento  agli  articoli  2, 3 e 53 della &#13;
Costituzione,  dalla  Commissione tributaria regionale di Venezia con &#13;
l'ordinanza di cui in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 aprile 2002. &#13;
                         Il Presidente: Vari &#13;
                       Il redattore: Amirante &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 12 aprile 2002. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
