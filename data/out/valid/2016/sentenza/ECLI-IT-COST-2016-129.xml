<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-06/129/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-06/129"/>
          <FRBRalias value="ECLI:IT:COST:2016:129" name="ECLI"/>
          <FRBRdate date="2016-06-06" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="129"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-06/129/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-06/129/ita@"/>
          <FRBRdate date="2016-06-06" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-06/129/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-06/129/ita@.xml"/>
          <FRBRdate date="2016-06-06" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2016-04-06" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2016-06-06" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale parziale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="GROSSI" href="/akn/ontology/person/it/GROSSI" showAs="GROSSI"/>
        <TLCPerson eId="giuseppeFrigo" href="/akn/ontology/person/it/giuseppeFrigo" showAs="Giuseppe FRIGO"/>
        <TLCPerson eId="alessandroCriscuolo" href="/akn/ontology/person/it/alessandroCriscuolo" showAs="Alessandro CRISCUOLO"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="paoloGrossi" href="/akn/ontology/person/it/paoloGrossi" showAs="Paolo GROSSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2016</akn4cc:anno>
        <akn4cc:numero>129</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2016:129</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>GROSSI</akn4cc:presidente>
        <akn4cc:relatore>Marta Cartabia</akn4cc:relatore>
        <akn4cc:data_decisione>2016-04-06</akn4cc:data_decisione>
        <akn4cc:data_deposito>2016-06-06</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale parziale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#paoloGrossi">Paolo GROSSI</judge>; Giudici : <judge refersTo="#giuseppeFrigo">Giuseppe FRIGO</judge>, <judge refersTo="#alessandroCriscuolo">Alessandro CRISCUOLO</judge>, <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>, <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2012-07-06/95/!main#art_16__para_6">art. 16, comma 6, del decreto-legge 6 luglio 2012, n. 95</ref> (Disposizioni urgenti per la revisione della spesa pubblica con invarianza dei servizi ai cittadini nonché misure di rafforzamento patrimoniale delle imprese del settore bancario), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2012-08-07/135/!main#art_1__para_1">art. 1, comma 1, della legge 7 agosto 2012, n. 135</ref>, promosso dal Tribunale amministrativo regionale del Lazio nel procedimento vertente tra il Comune di Lecce e il Ministero dell'interno ed altri, con ordinanza del 2 dicembre 2014, iscritta al n. 195 del registro ordinanze 2015, pubblicata nella Gazzetta Ufficiale della Repubblica n. 40, prima serie speciale, dell'anno 2015.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> del Comune di Andria, fuori termine, nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 6 aprile 2016 il Giudice relatore Marta Cartabia.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza depositata il 2 dicembre 2014 e iscritta al n. 195 del registro ordinanze del 2015, il Tribunale amministrativo regionale del Lazio dubita della legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2012-07-06/95/!main#art_16__para_6">art. 16, comma 6, del decreto-legge 6 luglio 2012, n. 95</ref> (Disposizioni urgenti per la revisione della spesa pubblica con invarianza dei servizi ai cittadini nonché misure di rafforzamento patrimoniale delle imprese del settore bancario), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2012-08-07/135/!main#art_1__para_1">art. 1, comma 1, della legge 7 agosto 2012, n. 135</ref>, nella parte in cui, nel prevedere per l'anno 2013 la riduzione del fondo sperimentale di riequilibrio e del fondo perequativo per un ammontare complessivo di 2.250 milioni di euro, dispone che la riduzione per ciascun Comune è «determinat[a], con decreto di natura non regolamentare del Ministro dell'interno, in proporzione alla media delle spese sostenute per consumi intermedi nel triennio 2010-2012, desunte dal SIOPE».
 Il giudice rimettente ritiene la questione di legittimità costituzionale rilevante e non manifestamente infondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>1.</num>
            <content>
              <p>1.- La rilevanza della questione deriverebbe dal fatto che il Tribunale amministrativo regionale del Lazio è chiamato a decidere in merito al ricorso con cui il Comune di Lecce contesta la riduzione dei trasferimenti erariali determinata dal decreto del Ministero dell'interno 24 settembre 2013, chiedendone l'annullamento: trattandosi di fonte secondaria di mera applicazione della fonte primaria, la determinazione circa la legittimità costituzionale della disposizione censurata si porrebbe in termini di pregiudizialità rispetto alla decisione nel merito.
 1.2.- La questione sarebbe non manifestamente infondata in riferimento agli artt. 3, 97 e 119, primo e terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 In particolare, il giudice rimettente afferma che la censurata disposizione comporterebbe la lesione dell'autonomia finanziaria riconosciuta all'ente locale dall'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_119">art. 119 Cost.</ref> Ciò per due ordini di ragioni. In primo luogo, perché la disposizione censurata, a differenza di quella previgente per i Comuni e quella vigente per le Province, non stabilisce un termine entro il quale il decreto ministeriale che determina la riduzione di entrate erariali per ciascun Comune deve essere emanato. Un intervento di riduzione dei trasferimenti, afferma il rimettente, che avvenisse a esercizio finanziario quasi concluso inciderebbe sull'elaborazione e sull'approvazione del bilancio di previsione, strumento attraverso cui si esplica l'autonomia finanziaria di ciascun ente locale, necessariamente condizionato alla previa conoscenza delle entrate su cui è possibile contare. In secondo luogo, perché la disposizione censurata individua il parametro per la determinazione della riduzione dei trasferimenti statali nelle spese sostenute, da parte di ciascun ente locale, per i «consumi intermedi» del triennio in esame, categoria nella quale rientrano, però, secondo il rimettente, sia le spese stanziate nell'interesse di ogni singola amministrazione, sia quelle destinate ad assicurare servizi ai cittadini (quali, come nel caso del Comune ricorrente, i costi del servizio di raccolta dei rifiuti). A sostegno della incostituzionalità della disposizione censurata, il TAR Lazio richiama l'<ref href="/akn/it/act/decretoLegge/stato/2014-04-08/35/!main#art_10__para_1">art. 10, comma 1, del decreto-legge 8 aprile 2014, n. 35</ref> (Disposizioni urgenti per il pagamento dei debiti scaduti della pubblica amministrazione, per il riequilibrio finanziario degli enti territoriali, nonché in materia di versamento di tributi degli enti locali), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2013-06-06/64/!main#art_1__para_1">art. 1, comma 1, della legge 6 giugno 2013, n. 64</ref>, il quale, intervenendo sul comma 7 del qui censurato art. 16, esclude, per le sole Province, dal computo dei consumi intermedi le spese per la formazione professionale, per il trasporto pubblico locale, per la raccolta di rifiuti solidi urbani e per i servizi socialmente utili finanziati dallo Stato.
 In riferimento all'art. 119, terzo comma, Cost., il TAR Lazio osserva che la riduzione delle erogazioni statali in base alle spese sostenute dai singoli Comuni per i consumi intermedi sia ispirata a una ratio diversa da quella che connota la previsione costituzionale del fondo perequativo, che si basa sul criterio della capacità fiscale per abitante. La riduzione del fondo perequativo non potrebbe avvenire, secondo il rimettente, sulla base di un parametro (le spese per i consumi intermedi) diverso da quello che ispira l'istituzione del medesimo (la capacità contributiva per abitante).
 La disposizione censurata violerebbe altresì gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, nella parte in cui, a differenza di quanto previsto per le riduzioni dei trasferimenti ai Comuni nell'anno 2012 e per le riduzioni dei trasferimenti alle Province per l'anno 2013, non subordina la determinazione unilateralmente assunta dallo Stato con decreto ministeriale all'ipotesi di inerzia della Conferenza Stato-Città e autonomie locali.
 2.- Nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha chiesto, nell'atto depositato in data 27 ottobre 2015, che la questione di legittimità costituzionale sia dichiarata non fondata.
 2.1.- Ad avviso della difesa statale, la scelta legislativa sarebbe del tutto legittima, posto che la disposizione censurata «rappresenta una tappa [del] percorso avviato dal legislatore statale per realizzare il contenimento della spesa pubblica», incidendo su una voce di spesa complessiva, quella relativa ai consumi intermedi, senza peraltro determinare gli strumenti e le modalità per l'attuazione dello stesso, nel rispetto degli artt. 117, terzo comma, e 119, primo comma, Cost. Inoltre, la difesa statale precisa che la mancata indicazione del termine per l'adozione del decreto ministeriale non regolamentare relativo all'anno 2013 «non ha avuto alcuna incidenza sull'autonomia finanziaria e di spesa dell'ente locale», in quanto l'atto normativo è stato emanato il 24 settembre 2013, un mese in anticipo rispetto all'anno precedente, in cui il termine per l'adozione del decreto ministeriale era previsto per legge.
 2.2.- Sarebbe, altresì, infondata la questione sollevata in riferimento all'art. 119, terzo comma, Cost., per avere la legge statale individuato nella media delle spese sostenute per i consumi intermedi nel triennio precedente il criterio per la determinazione della riduzione dei trasferimenti statali a ciascun Comune. Il criterio individuato per la riduzione, secondo il Presidente del Consiglio dei ministri, «ha una valenza tecnica e neutra, disancorata dall'eventuale valore politico del dato ed orientata al solo fine di ripartire il sacrificio tra tutti gli enti coinvolti». Inoltre, la difesa statale osserva che le riduzioni in base alle spese sui consumi intermedi per l'anno 2013 riguarderebbero non più il fondo sperimentale di riequilibrio, abrogato dall'<ref href="/akn/it/act/legge/stato/2012-12-24/228/!main#art_1__para_380__point_e">art. 1, comma 380, lettera e), della legge 24 dicembre 2012, n. 228</ref> (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - Legge di stabilità 2013), bensì il fondo di solidarietà comunale, istituito dall'art. 1, del medesimo comma 380, lettera d), il quale, avendo una ratio autonoma, può dotarsi di un criterio diverso da quello previsto dall'art. 119, terzo comma, Cost.
 2.3.- Priva di fondamento sarebbe, poi, la censura che lamenta, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, l'unilateralità della suddivisione delle riduzioni disposte tramite decreto ministeriale, senza il preliminare passaggio in Conferenza Stato-Città e autonomie locali. La difesa statale ritiene, infatti, che, al fine di provvedere alla suddivisione delle riduzioni del sopprimendo fondo sperimentale di riequilibrio per il 2013, la norma ha adottato i criteri già determinati, per l'anno 2012, dalla Conferenza Stato-città e autonomie locali, criteri poi recepiti nel decreto ministeriale 25 ottobre 2012.
 3.- Con memoria depositata in data 27 novembre 2015 (fuori termine), si è costituito il Comune di Andria, chiedendo l'accoglimento della questione sollevata dal giudice rimettente.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Tribunale amministrativo regionale del Lazio, con ordinanza iscritta al n. 195 del registro ordinanze del 2015, dubita della legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2012-07-06/95/!main#art_16__para_6">art. 16, comma 6, del decreto-legge 6 luglio 2012, n. 95</ref> (Disposizioni urgenti per la revisione della spesa pubblica con invarianza dei servizi ai cittadini nonché misure di rafforzamento patrimoniale delle imprese del settore bancario), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2012-08-07/135/!main#art_1__para_1">art. 1, comma 1, della legge 7 agosto 2012, n. 135</ref>. La norma censurata, nel disporre, per l'anno 2013, la riduzione del fondo sperimentale di riequilibrio, del fondo perequativo e dei trasferimenti erariali dovuti ai Comuni per un ammontare complessivo di 2.250 milioni di euro, prevede che le quote da imputare a ciascun Comune sono «determinate, con decreto di natura non regolamentare del Ministro dell'interno, in proporzione alla media delle spese sostenute per consumi intermedi nel triennio 2010-2012, desunte dal SIOPE».
 1.1.- Il TAR Lazio lamenta la violazione degli artt. 3, 97 e 119, primo e terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 Secondo il giudice rimettente, la mancata previsione di un termine per l'adozione del decreto ministeriale volto a determinare la quota di riduzione spettante a ciascun Comune lederebbe l'autonomia finanziaria e il buon andamento dell'amministrazione dell'ente medesimo, incidendo l'eventuale tardività nell'adozione del decreto ministeriale sulla redazione del bilancio finanziario del Comune. Inoltre, la disposizione impugnata comporterebbe una lesione del principio di leale collaborazione, in quanto non subordina la determinazione unilaterale delle quote, da parte dello Stato, all'inerzia della Conferenza Stato-Città e autonomie locali - come, al contrario, era previsto per le riduzioni dei trasferimenti ai Comuni e alle Province per l'anno 2012 e per le riduzioni alle sole Province per l'anno 2013. La disposizione censurata violerebbe, altresì, il primo comma dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_119">art. 119 Cost.</ref>, dato che individua nei «consumi intermedi» il criterio per la determinazione della quota di riduzione delle risorse da trasferire, senza decurtare da detti consumi le spese sostenute per i servizi ai cittadini. Infine, la scelta del legislatore violerebbe il terzo comma dello stesso <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_119">art. 119 Cost.</ref>, ricorrendo a un criterio (i consumi intermedi) diverso da quello previsto dalla disposizione costituzionale per il fondo perequativo (minore capacità contributiva per abitante).
 2.- Le questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2012/95/!main#art_16__para_6">art. 16, comma 6, del d.l. n. 95 del 2012</ref> sono fondate.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>1.- Occorre preliminarmente ricostruire la più recente evoluzione della disciplina dei trasferimenti erariali agli enti locali, nel cui ambito si inserisce la disposizione impugnata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il <ref href="/akn/it/act/decretoLegge/stato/2012/95/!main">d.l. n. 95 del 2012</ref> e, per quel che rileva in questo giudizio, il suo art. 16, comma 6, hanno subito, in un breve lasso di tempo, diversi interventi legislativi, che hanno inciso sia sulla entità dei trasferimenti erariali ai Comuni, sia sulle modalità di determinazione delle quote di riduzione spettanti a ciascun Comune.
 L'articolo 16, comma 6, nella formulazione modificata dall'<ref href="/akn/it/act/decretoLegge/stato/2012-10-10/174/!main#art_8__para_2">art. 8, comma 2, del decreto-legge 10 ottobre 2012, n. 174</ref> (Disposizioni urgenti in materia di finanza e funzionamento degli enti territoriali, nonché ulteriori disposizioni in favore delle zone terremotate nel maggio 2012), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2012-12-07/213/!main#art_1__para_1">art. 1, comma 1, della legge 7 dicembre 2012, n. 213</ref>, prevedeva che le riduzioni del fondo sperimentale di riequilibrio, del fondo perequativo e dei trasferimenti erariali ai Comuni fossero determinate «dalla Conferenza Stato-Città e autonomie locali [...], e recepite con decreto del Ministero dell'interno entro il 15 ottobre, relativamente alle riduzioni da operare nell'anno 2012, ed entro il 31 gennaio 2013 relativamente alle riduzioni da operare per gli anni 2013 e successivi». In caso di mancata deliberazione della Conferenza Stato-Città ed autonomie locali, il decreto ministeriale avrebbe dovuto essere «comunque emanato entro i 15 giorni successivi, ripartendo la riduzione in proporzione alle spese sostenute per consumi intermedi desunte, per l'anno 2011, dal SIOPE».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La disposizione riproponeva un meccanismo già sperimentato in questo ambito, in base al quale lo Stato determina l'ammontare complessivo della riduzione per anno di riferimento e per tipologia di ente, mentre la quota assegnata a ciascun ente è stabilita dalla Conferenza Stato-Città e autonomie locali (nel caso dei Comuni e delle Province) e recepita con decreto del Ministero dell'interno entro una data certa. Solo in assenza di un accordo, il Ministero può procedere unilateralmente alla quantificazione delle riduzioni, adottando un decreto ministeriale, sempre entro una data certa, «in proporzione alle spese sostenute per consumi intermedi desunte [...] dal SIOPE».
 La legge di stabilità 2013 - <ref href="/akn/it/act/legge/stato/2012-12-24/228/!main">legge 24 dicembre 2012, n. 228</ref> (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato) - ridisegna il sistema di finanziamento degli enti locali.
 Infatti, nell'attribuire ai Comuni l'intero gettito IMU (a esclusione di quello derivante dagli immobili produttivi, che rimane destinato allo Stato), alla lettera e) del suo art. 1, comma 380, sopprime il fondo sperimentale di riequilibrio istituito dall'<ref href="/akn/it/act/decretoLegislativo/stato/2011-03-14/23/!main#art_2">art. 2 del decreto legislativo 14 marzo 2011, n. 23</ref> (Disposizioni in materia di federalismo fiscale municipale). Tale fondo sperimentale - alimentato dal gettito derivante dai tributi sugli immobili e poi ridistribuito tra i Comuni - avrebbe dovuto avere una durata di tre anni ed essere sostituito, una volta determinati i fabbisogni standard, dal Fondo perequativo per il finanziamento delle spese dei Comuni e delle Province (secondo quanto previsto dall'art. 13 dello stesso <ref href="/akn/it/act/decretoLegislativo/stato/2011/23/!main">d.lgs. n. 23 del 2011</ref>).
 Le difficoltà e i ritardi nell'attuazione del federalismo fiscale hanno invece indotto - come detto - il legislatore a sopprimere il Fondo sperimentale di riequilibrio (art. 1, comma 380, lettera e, della <ref href="/akn/it/act/legge/stato/2012/228/!main">legge n. 228 del 2012</ref>), sostituendolo con il Fondo di solidarietà comunale, alimentato con una quota dell'IMU di spettanza dei Comuni (art. 1, comma 380, lettera b, della <ref href="/akn/it/act/legge/stato/2012/228/!main">legge n. 228 del 2012</ref>), i cui criteri di formazione e di riparto devono essere determinati, previo accordo da sancire presso la Conferenza Stato-Città e autonomie locali, con decreto del Presidente del Consiglio dei ministri da emanare entro il 30 aprile 2013 per l'anno 2013 ed entro il 31 dicembre 2013 per l'anno 2014, e comunque entro i successivi 15 giorni in caso di mancato accordo. Il suddetto Fondo di solidarietà è stato poi messo a regime dalla legge di stabilità del 2014 (<ref href="/akn/it/act/legge/stato/2013-12-27/147/!main">legge 27 dicembre 2013, n. 147</ref> - Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato).
 Con <ref href="/akn/it/act/decretoLegge/stato/2013-04-08/35/!main">decreto-legge 8 aprile 2013, n. 35</ref> (Disposizioni urgenti per il pagamento dei debiti scaduti della pubblica amministrazione, per il riequilibrio finanziario degli enti territoriali, nonché in materia di versamento di tributi degli enti locali), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2013-06-06/64/!main#art_1__para_1">art. 1, comma 1, della legge 6 giugno 2013, n. 64</ref>, l'<ref href="/akn/it/act/decretoLegge/stato/2012/95/!main#art_16__para_6">art. 16, comma 6, del d.l. n. 95 del 2012</ref> è stato ulteriormente modificato: è stato abrogato il termine del 31 gennaio 2013 (nel frattempo inutilmente decorso), introdotto dal <ref href="/akn/it/act/decretoLegge/stato/2012/174/!main">d.l. n. 174 del 2012</ref> per la determinazione, in sede di Conferenza Stato-Città e autonomie locali, delle riduzioni del soppresso Fondo sperimentale di riequilibrio da imputare a ciascun Comune; ed è stato previsto che tali riduzioni «a decorrere dall'anno 2013 sono determinate, con decreto di natura non regolamentare del Ministro dell'interno, in proporzione alla media delle spese sostenute per i costi intermedi nel triennio 2010-2012, desunte dal SIOPE».
 Per il solo anno 2013, dunque, convivono due fondi con due diversi meccanismi di imputazione delle riduzioni: il primo riguarda il Fondo sperimentale di riequilibrio, in via di estinzione, e prevede che lo Stato, con una decisione unilaterale, distribuisca le risorse, ridotte in proporzione alle spese sostenute per i consumi intermedi; il secondo riguarda il nuovo Fondo di solidarietà comunale, il cui riparto è affidato primariamente alla Conferenza Stato-Città e autonomie locali e, nel solo caso di mancato accordo, all'intervento unilaterale dello Stato con decreto del Presidente del Consiglio dei ministri.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>2.- La dinamica del contesto legislativo rivela, dunque, come la disposizione censurata operi quale deroga all'ordinario procedimento di riparto dei fondi erariali: una deroga circoscritta al solo anno 2013 per il sopprimendo Fondo sperimentale di riequilibrio e funzionale all'avvio del nuovo regime basato sul Fondo di solidarietà comunale, già contestualmente istituito con la medesima legge di stabilità per il 2013 abrogativa del primo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Tuttavia, neppure tali caratteristiche - che inducono a qualificare la deroga disposta dalla norma impugnata come transitoria ed eccezionale - consentono di superare le censure di illegittimità costituzionale sollevate dal giudice rimettente. Il mancato coinvolgimento della Conferenza Stato-Città e autonomie locali nella fase di determinazione delle riduzioni addossate a ciascun Comune, seppur limitatamente all'anno 2013, unitamente alla mancanza di un termine per l'adozione del decreto ministeriale e alla individuazione dei costi intermedi come criterio base per la quantificazione dei tagli finanziari, comporta, infatti, la violazione degli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_119">119 Cost.</ref></mref></p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>3.- L'impugnato <ref href="/akn/it/act/decretoLegge/stato/2012/95/!main#art_16__para_6">art. 16, comma 6, del d.l. n. 95 del 2012</ref>, indicando gli obiettivi di contenimento delle spese degli enti locali, si pone come principio di coordinamento della finanza pubblica, che vincola senz'altro anche i Comuni. Nessun dubbio che, come già ripetutamente affermato da questa Corte (<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/65/!main">sentenze n. 65</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/1/!main">1 del 2016</ref></mref>, n. 88 e n. 36 del 2014, n. 376 del 2003), le politiche statali di riduzione delle spese pubbliche possano incidere anche sull'autonomia finanziaria degli enti territoriali; tuttavia, tale incidenza deve, in linea di massima, essere mitigata attraverso la garanzia del loro coinvolgimento nella fase di distribuzione del sacrificio e nella decisione sulle relative dimensioni quantitative, e non può essere tale da rendere impossibile lo svolgimento delle funzioni degli enti in questione (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/10/!main">sentenze n. 10 del 2016</ref>, n. 188 del 2015 e n. 241 del 2012).
 Vero è che i procedimenti di collaborazione tra enti debbono sempre essere corredati da strumenti di chiusura che consentano allo Stato di addivenire alla determinazione delle riduzioni dei trasferimenti, anche eventualmente sulla base di una sua decisione unilaterale, al fine di assicurare che l'obiettivo del contenimento della spesa pubblica sia raggiunto pur nella inerzia degli enti territoriali (ex mult<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/82/!main">is, sentenze n. 82</ref> e <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/19/!main">19 del 2015</ref></mref>). Ma tale condizione non può giustificare l'esclusione sin dall'inizio di ogni forma di coinvolgimento degli enti interessati, tanto più se il criterio posto alla base del riparto dei sacrifici non è esente da elementi di dubbia razionalità, come è quello delle spese sostenute per i consumi intermedi.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In effetti, non appare destituita di fondamento la considerazione, sviluppata dal giudice rimettente, che nella nozione di «consumi intermedi» possono rientrare non solo le spese di funzionamento dell'apparato amministrativo - ciò che permetterebbe al criterio utilizzato di colpire le inefficienze dell'amministrazione e di innescare virtuosi comportamenti di risparmio -, ma, altresì, le spese sostenute per l'erogazione di servizi ai cittadini. Si tratta, dunque, di un criterio che si presta a far gravare i sacrifici economici in misura maggiore sulle amministrazioni che erogano più servizi, a prescindere dalla loro virtuosità nell'impiego delle risorse finanziarie.
 Dati questi elementi di ambiguità, si deve ritenere che il ricorso al criterio delle spese sostenute per i consumi intermedi come parametro per la quantificazione delle riduzioni delle risorse da imputare a ciascun Comune possa trovare giustificazione solo se affiancato a procedure idonee a favorire la collaborazione con gli enti coinvolti e a correggerne eventuali effetti irragionevoli. Il criterio delle spese sostenute per i consumi intermedi non è dunque illegittimo in sé e per sé; la sua illegittimità deriva dall'essere parametro utilizzato in via principale anziché in via sussidiaria, vale a dire solo dopo infruttuosi tentativi di coinvolgimento degli enti interessati attraverso procedure concertate o in ambiti che consentano la realizzazione di altre forme di cooperazione.
 Né si deve sottovalutare il fatto che la disposizione impugnata non stabilisce alcun termine per l'adozione del decreto ministeriale che determina il riparto delle risorse e le relative decurtazioni. Un intervento di riduzione dei trasferimenti che avvenisse a uno stadio avanzato dell'esercizio finanziario comprometterebbe un aspetto essenziale dell'autonomia finanziaria degli enti locali, vale a dire la possibilità di elaborare correttamente il bilancio di previsione, attività che richiede la previa e tempestiva conoscenza delle entrate effettivamente a disposizione.
 Per tutte queste ragioni, complessivamente considerate, deve essere dichiarata l'illegittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2012/95/!main#art_16__para_6">art. 16, comma 6, del d.l. n. 95 del 2012</ref>, convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2012/135/!main#art_1__para_1">art. 1, comma 1, della l. n. 135 del 2012</ref>.
 2.4.- Restano assorbiti gli altri motivi di censura.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2012-07-06/95/!main#art_16__para_6">art. 16, comma 6, del decreto-legge 6 luglio 2012, n. 95</ref> (Disposizioni urgenti per la revisione della spesa pubblica con invarianza dei servizi ai cittadini nonché misure di rafforzamento patrimoniale delle imprese del settore bancario), convertito, con modificazioni, dall'<ref href="/akn/it/act/legge/stato/2012-08-07/135/!main#art_1__para_1">art. 1, comma 1, della legge 7 agosto 2012, n. 135</ref>, nella parte in cui non prevede, nel procedimento di determinazione delle riduzioni del Fondo sperimentale di riequilibrio da applicare a ciascun Comune nell'anno 2013, alcuna forma di coinvolgimento degli enti interessati, né l'indicazione di un termine per l'adozione del decreto di natura non regolamentare del Ministero dell'interno.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2016-04-06">6 aprile 2016</date>.
 F.to:
 <signature><judge refersTo="#paoloGrossi" as="#presidente">Paolo GROSSI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#martaCartabia" as="#relatore">Marta CARTABIA</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2016-06-06">6 giugno 2016</date>.
 Il Cancelliere
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
