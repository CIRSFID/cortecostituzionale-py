<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>56</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:56</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>27/02/2002</data_decisione>
    <data_deposito>15/03/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Valerio ONIDA, Carlo &#13;
MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 11, commi 16 e &#13;
18,  della  legge  24 dicembre 1993, n. 537 (Interventi correttivi di &#13;
finanza  pubblica), promosso con ordinanza emessa il 16 febbraio 2000 &#13;
dalla  Corte  dei  conti,  sezione  giurisdizionale  per  la  Regione &#13;
Trentino-Alto  Adige, sede di Trento, sul ricorso proposto da Scatena &#13;
Paolo  contro Azienda provinciale per i Servizi Sanitari di Trento ed &#13;
altro,  iscritta  al  n. 745 del registro ordinanze 2000 e pubblicata &#13;
nella  Gazzetta  Ufficiale della Repubblica n. 49, 1ª serie speciale, &#13;
dell'anno 2000. &#13;
    Visti  l'atto  di costituzione di Scatena Paolo nonché l'atto di &#13;
intervento del Presidente del Consiglio dei ministri; &#13;
    Udito  nell'udienza  pubblica  del  29 gennaio  2002  il  giudice &#13;
relatore Francesco Amirante; &#13;
    Uditi  l'avvocato  Mario  Fedrizzi per Scatena Paolo e l'Avvocato &#13;
dello  Stato  Giorgio  D'Amato  per  il  Presidente del Consiglio dei &#13;
ministri. &#13;
    Ritenuto  che nel corso di un giudizio promosso da un ex primario &#13;
ospedaliero  avverso il provvedimento di liquidazione provvisoria del &#13;
trattamento  di  pensione la Corte dei conti, sezione giurisdizionale &#13;
per  il  Trentino-Alto  Adige,  sede  di  Trento,  ha  sollevato,  in &#13;
riferimento  agli  artt.  3  e  38  della  Costituzione, questione di &#13;
legittimità  costituzionale dell'art. 11, commi 16 e 18, della legge &#13;
24 dicembre 1993, n. 537 (Interventi correttivi di finanza pubblica); &#13;
        che  il  ricorrente  nel  giudizio  principale,  collocato in &#13;
pensione  (di  anzianita)  a  seguito dell'annullamento, disposto con &#13;
sentenza  definitiva  del  Consiglio  di  Stato,  della  delibera  di &#13;
assunzione  in  qualità  di  primario,  si  è  visto  liquidare  il &#13;
trattamento  provvisorio di pensione con la riduzione dell'undici per &#13;
cento  prevista  -  per  il  caso  di conseguimento della pensione di &#13;
anzianità  prima  del  raggiungimento  della soglia dei trentacinque &#13;
anni di cui alle leggi vigenti - dalla norma impugnata e dall'art. 1, &#13;
commi 26 e 27, della legge 8 agosto 1995, n. 335; &#13;
        che   la   norma   impugnata,   infatti,   nell'ottica  della &#13;
progressiva  eliminazione dell'istituto della pensione di anzianità, &#13;
stabilisce,  con  effetto  dal  1  gennaio  1994,  che coloro i quali &#13;
conseguono detta pensione con un'anzianità contributiva inferiore ai &#13;
trentacinque  anni,  a  meno  che  non  si  tratti  di cessazione dal &#13;
servizio  per  invalidità,  subiscano  una riduzione del trattamento &#13;
economico in proporzione al numero di anni mancanti al raggiungimento &#13;
del  predetto  requisito,  secondo  quanto  indicato  dalla tabella A &#13;
allegata alla legge n. 537 del 1993; &#13;
        che  l'impugnato art. 11 va collegato con le successive norme &#13;
intervenute  sul punto, ossia i commi 27 e 32 dell'art. 1 della legge &#13;
n. 335  del  1995, i quali dispongono che, nella fase transitoria, la &#13;
pensione  di  anzianità  può  ancora  essere conseguita, purché in &#13;
presenza  di  determinati  requisiti  contributivi,  con applicazione &#13;
delle  riduzioni  percentuali  di  cui  alla  tabella  D  allegata  a &#13;
quest'ultima legge; &#13;
        che il menzionato art. 1, comma 32, dispone che le precedenti &#13;
disposizioni  in  materia  di  pensione  di anzianità (scilicet più &#13;
favorevoli)  continuano  a  trovare  applicazione  in  una  serie ben &#13;
identificata di casi; &#13;
        che  il  complesso normativo richiamato non risolve - a detta &#13;
della  Corte  dei  conti  -  il  problema  dell'applicabilità  delle &#13;
riduzioni  in  questione  nei  casi  in  cui  non  ne venga stabilita &#13;
espressamente   l'esclusione;  nel  giudizio  pendente,  infatti,  il &#13;
ricorrente  si  è  collocato  in  pensione  non di propria spontanea &#13;
volontà,  bensì  a  seguito  dell'annullamento  del proprio atto di &#13;
assunzione; &#13;
        che,  ad  avviso  del  giudice  a quo, stante la formulazione &#13;
tassativa  contenuta  nell'art. 1,  comma  32, della legge n. 335 del &#13;
1995,  la  riduzione di cui alla norma impugnata dev'essere applicata &#13;
anche  nel caso del ricorrente, benché il pensionamento sia avvenuto &#13;
per  fatti indipendenti dalla volontà del medesimo; e ciò in quanto &#13;
non   possono   essere  aggiunte  in  via  di  interpretazione  altre &#13;
fattispecie di esclusione non previste dalla norma; &#13;
        che   tale   lettura   del   sistema,   pur  essendo  l'unica &#13;
tecnicamente  possibile,  appare  al  rimettente in contrasto con gli &#13;
invocati parametri costituzionali; &#13;
        che da un lato, infatti, ricondurre al medesimo trattamento i &#13;
lavoratori che optano per il pensionamento anticipato di anzianità e &#13;
quelli  che,  viceversa,  si  trovano  in  pensione contro la propria &#13;
volontà,  appare lesivo del principio di eguaglianza, trattandosi di &#13;
situazioni  difformi; dall'altro, la penalizzazione rappresentata dal &#13;
decurtamento  della  pensione  può determinare la compressione delle &#13;
esigenze  vitali  dell'interessato, con violazione anche dell'art. 38 &#13;
della Carta fondamentale; &#13;
        che  la Corte dei conti, quindi, sollecita l'emissione di una &#13;
pronuncia  che  dichiari  l'illegittimità costituzionale della norma &#13;
impugnata  nella  parte  in  cui  dispone la riduzione, "anche per il &#13;
personale involontariamente collocato a riposo ante tempus per motivi &#13;
diversi  da  quelli  indicati  nel  comma  32 dell'art. 1 della legge &#13;
n. 335/1995,  del  trattamento pensionistico in proporzione agli anni &#13;
mancanti    al   raggiungimento   dell'anzianità   contributiva   di &#13;
trentacinque anni"; &#13;
        che  si  è  costituito  in  giudizio  il  medico ricorrente, &#13;
sollecitando l'accoglimento della prospettata questione; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo  che  la  prospettata  questione  venga  dichiarata &#13;
inammissibile o infondata. &#13;
    Considerato  che  la  questione  di  legittimità  costituzionale &#13;
proposta  dal  giudice  remittente  si  centra  essenzialmente  sulla &#13;
mancata   estensione   delle  norme  che  consentono  di  fruire  del &#13;
pensionamento   anticipato   di   anzianità,   senza   incorrere  in &#13;
decurtazioni  del trattamento pensionistico, a tutti i casi nei quali &#13;
il  collocamento  a  riposo non è riconducibile ad una libera scelta &#13;
dell'interessato; &#13;
        che  alla  predetta  ipotesi  sarebbe  da  ricondurre,  nella &#13;
ricostruzione operata dal giudice a quo, anche la fattispecie oggetto &#13;
del giudizio principale, nella quale il medico ricorrente si è visto &#13;
annullare  il  proprio  atto  di  nomina  con sentenza definitiva del &#13;
giudice  amministrativo,  optando  per  il  pensionamento soltanto in &#13;
virtù di detta situazione sopravvenuta; &#13;
        che  la  Corte  remittente, peraltro, omette completamente di &#13;
considerare che, proprio in conseguenza dell'intervenuto annullamento &#13;
dell'atto   di   nomina   a  primario  ospedaliero,  la  controversia &#13;
attualmente  pendente  non  ha  ad oggetto un pensionamento derivante &#13;
dalla  cessazione  di  un  rapporto di lavoro validamente instaurato, &#13;
bensì  il  diverso caso di un pensionamento derivante dal venir meno &#13;
di  un  rapporto  contrattuale  di  fatto  ricadente  nell'ambito  di &#13;
applicazione  dell'art. 2126  cod. civ., norma pacificamente valevole &#13;
anche per il pubblico impiego (ordinanza n. 12 del 1994); &#13;
        che  il  giudice  remittente,  perciò,  non ha in alcun modo &#13;
ottemperato  all'obbligo  di motivazione circa il profilo preliminare &#13;
della  riconducibilità della cessazione di un rapporto di fatto alla &#13;
diversa fattispecie del pensionamento susseguente alla conclusione di &#13;
un   rapporto   di   lavoro   validamente   instaurato,   presupposto &#13;
indispensabile  per  l'eventuale  pronuncia  additiva sollecitata nei &#13;
confronti di questa Corte; &#13;
        che  tale  carenza  di  motivazione sul requisito preliminare &#13;
della  rilevanza  si  traduce  in  manifesta  inammissibilità  della &#13;
sollevata questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale dell'art. 11, commi 16 e 18, della legge &#13;
24 dicembre 1993, n. 537 (Interventi correttivi di finanza pubblica), &#13;
sollevata, in riferimento agli artt. 3 e 38 della Costituzione, dalla &#13;
Corte  dei conti, sezione giurisdizionale per il Trentino-Alto Adige, &#13;
sede di Trento, con l'ordinanza di cui in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 27 febbraio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                       Il redattore: Amirante &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 15 marzo 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
