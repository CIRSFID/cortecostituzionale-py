<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>6</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:6</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/1992</data_decisione>
    <data_deposito>22/01/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, &#13;
 prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, &#13;
 prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  del  combinato  disposto    &#13;
 degli  artt.  13 e 20 della legge 26 aprile 1990, n. 86 (Modifiche in    &#13;
 tema  di  delitti  dei  pubblici   ufficiali   contro   la   pubblica    &#13;
 amministrazione),  in  relazione  agli  artt.  323  e  324 del codice    &#13;
 penale, promosso con ordinanza emessa il 9 aprile  1991  dal  Giudice    &#13;
 per  le  indagini  preliminari  presso  il Tribunale di Catanzaro nel    &#13;
 procedimento penale a carico di Costa Giulio Vito ed altri,  iscritta    &#13;
 al  n.  449  del  registro ordinanze 1991 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 27,  prima  serie  speciale,  dell'anno    &#13;
 1991;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 4  dicembre  1991  il  Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto  che, nel corso di un processo penale a carico di persone    &#13;
 imputate di interesse privato in atti di ufficio, il Giudice  per  le    &#13;
 indagini  preliminari  presso  il  Tribunale  di  Catanzaro  ha,  con    &#13;
 ordinanza del 9 aprile 1991, sollevato,  in  riferimento  all'art.  3    &#13;
 della Costituzione, questione di legittimità "del combinato disposto    &#13;
 degli  artt.  13 e 20 della legge 26 aprile 1990, n. 86, in relazione    &#13;
 agli artt. 323 e 324 del codice penale", nella parte in  cui  "introduce,  nel  passato",  una  chiara  disparità di trattamento fra gli    &#13;
 autori dell'abuso già punibile a  norma  dell'art.  323  del  codice    &#13;
 penale  e  l'autore del più grave reato di interesse privato in atti    &#13;
 di ufficio previsto dall'art. 324 dello stesso codice,  espressamente    &#13;
 abrogato dall'art. 20 della legge n. 86 del 1990;                        &#13;
      che  il giudice a quo denuncia, quindi, la mancata previsione di    &#13;
 una disciplina transitoria che renda punibili i  fatti  di  interesse    &#13;
 privato,  non  potendo  ravvisarsi  "omogeneità"  oggettiva  fra  la    &#13;
 condotta punita dall'art. 324 del codice penale e la condotta  punita    &#13;
 dall'art.  323  dello  stesso  codice,  quale sostituito dall'art. 13    &#13;
 della legge n. 86 del  1990,  una  disomogeneità  "accentuata  dalla    &#13;
 proclamata abrogazione della vecchia norma";                             &#13;
      e  che,  quindi,  stando  al  giudice  a  quo,  l'assetto  così    &#13;
 delineato  rivelerebbe  una  vera   e   propria   "iniquità"   nella    &#13;
 disciplina,  risultando essa incentrata nella persistente punibilità    &#13;
 della fattispecie di abuso innominato, "ad offensività  meno  grave"    &#13;
 rispetto  alla  presa  di  interesse,  per  giunta, patrimoniale, una    &#13;
 fattispecie  con  un  tasso  di  antigiuridicità  più  elevato   e,    &#13;
 nonostante ciò, espressamente abrogata dalla legge n. 86 del 1990;      &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  Generale  dello    &#13;
 Stato,  chiedendo  che  la questione sia dichiarata inammissibile per    &#13;
 difetto  di  rilevanza  e,  in  subordine,  non  fondata,  avendo  il    &#13;
 legislatore,  nell'esercizio  del  suo  potere discrezionale, operato    &#13;
 scelte non sindacabili in sede di legittimità costituzionale;           &#13;
    Considerato   che    l'eccezione    d'inammissibilità    avanzata    &#13;
 dall'Avvocatura   Generale   dello   Stato   deve  essere  disattesa,    &#13;
 desumendosi dall'ordinanza di rimessione l'addebito contestato  anche    &#13;
 con  riferimento  alla ritenuta ipotizzabilità di una fattispecie di    &#13;
 presa  d'interesse  di  contenuto   patrimoniale   e   tanto   appare    &#13;
 sufficiente perché venga ritenuto assolto l'onere di cui all'art. 23    &#13;
 della legge 11 marzo 1953, n. 87;                                        &#13;
      che,   peraltro,   in   punto  di  rilevanza,  la  questione  va    &#13;
 rigorosamente circoscritta alla disciplina di diritto intertemporale,    &#13;
 del resto espressamente richiamata dal giudice a  quo  sia  allorché    &#13;
 contesta la disparità di trattamento relativamente al "passato", sia    &#13;
 quando  imputa l'"iniquità" della norma censurata con riferimento al    &#13;
 regime transitorio;                                                      &#13;
      che, così delimitato l'ambito della denuncia di illegittimità,    &#13;
 il petitum avuto di mira dall'ordinanza di  rimessione  si  sostanzia    &#13;
 nella  richiesta  di  introdurre  una  norma  transitoria  che  renda    &#13;
 punibili i fatti di presa d'interesse di contenuto patrimoniale,  non    &#13;
 perseguibili  alla stregua della disciplina risultante dalla legge n.    &#13;
 86 del 1990, in base ai princìpi che governano la successione  della    &#13;
 legge penale nel tempo;                                                  &#13;
      che,  a  parte la contraddizione insita nel richiamo al "divieto    &#13;
 di irretroattività" coinvolgente una disciplina sopravvenuta che  il    &#13;
 giudice  a  quo  afferma "disomogenea" rispetto a quella abrogata, la    &#13;
 questione così come proposta, si sostanzia in una censura diretta  a    &#13;
 sindacare  scelte discrezionali del legislatore nella valutazione dei    &#13;
 beni tutelati dalla norma penale, scelte non  censurabili  in  questa    &#13;
 sede  -  soprattutto  allorché venga dedotta la non conformità alla    &#13;
 Costituzione di una disciplina destinata ad esaurirsi con il  decorso    &#13;
 del  tempo  -  ove  tali  scelte  non sconfinino nella arbitrarietà,    &#13;
 certamente non invocabile  nella  specie,  perché  alla  abrogazione    &#13;
 dell'art.  324,  conseguente  ad  una  più  complessa  ed articolata    &#13;
 repressione dell'abuso di ufficio, assurto  da  ipotesi  residuale  a    &#13;
 fattispecie   di   reato  caratterizzata  da  peculiari  connotazioni    &#13;
 soggettive ed oggettive, non era comunque necessario porre riparo con    &#13;
 la previsione di un'espressa disciplina transitoria;                     &#13;
      che,  peraltro,  il  giudice  a  quo  ha  omesso  del  tutto  di    &#13;
 considerare   come   la   predetta   disciplina  risulta  individuata    &#13;
 dall'ormai consolidata giurisprudenza della Corte di  cassazione,  la    &#13;
 quale,  anche  a  seguito di una decisione delle Sezioni Unite è nel    &#13;
 senso di ritenere operanti i princìpi disciplinanti  la  successione    &#13;
 della  legge  penale  nel  tempo,  con  la conseguente applicabilità    &#13;
 dell'abrogato art. 324 quando questo si riveli, quoad  poenam,  norma    &#13;
 più favorevole;                                                         &#13;
      che,  quindi,  sotto  entrambi  i  profili  sopra  enunciati, la    &#13;
 questione proposta deve dirsi manifestamente infondata;                  &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale del combinato disposto degli artt. 13 e 20 della legge    &#13;
 26 aprile 1990, n. 86 (Modifiche in  tema  di  delitti  dei  pubblici    &#13;
 ufficiali  contro  la  pubblica  amministrazione),  in relazione agli    &#13;
 artt.  323  e  324  del  codice  penale,  questione   sollevata,   in    &#13;
 riferimento  all'art.  3  della  Costituzione,  dal  Giudice  per  le    &#13;
 indagini preliminari presso il Tribunale di Catanzaro  con  ordinanza    &#13;
 del 9 aprile 1991.                                                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 gennaio 1992.                              &#13;
                       Il presidente: CORASANITI                          &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 22 gennaio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
