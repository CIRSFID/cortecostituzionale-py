<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>83</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:83</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/02/1994</data_decisione>
    <data_deposito>10/03/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  28,  secondo    &#13;
 comma,  seconda  parte,  del codice di procedura penale, promosso con    &#13;
 ordinanza emessa il 23 novembre 1992  dal  giudice  per  le  indagini    &#13;
 preliminari  presso  la  Pretura  di Torino nel procedimento penale a    &#13;
 carico di Sicurella Pietro, iscritta al n. 39 del registro  ordinanze    &#13;
 1993  e  pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 7,    &#13;
 prima serie speciale, dell'anno 1993;                                    &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 26 gennaio 1994 il Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto che il giudice per  le  indagini  preliminari  presso  la    &#13;
 Pretura  di  Torino  ha  sollevato,  in  riferimento all'art. 3 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art.  28,    &#13;
 secondo comma, del codice di procedura penale, nella parte in cui non    &#13;
 consente   al  giudice  per  le  indagini  preliminari  di  sollevare    &#13;
 conflitto di competenza dinanzi alla Corte di cassazione in  caso  di    &#13;
 contrasto con il giudice del dibattimento;                               &#13;
      che   ad  avviso  del  giudice  remittente  la  norma  impugnata    &#13;
 determinerebbe  una  irragionevole  disparità  di   trattamento   in    &#13;
 situazioni  sostanzialmente analoghe, e, in particolare, risulterebbe    &#13;
 inadeguata rispetto al  fine  di  pervenire  ad  una  più  sollecita    &#13;
 definizione dei processi, nonché irragionevole perché gli eventuali    &#13;
 contrasti  tra  giudici dello stesso ufficio non possono ritenersi di    &#13;
 così secondaria importanza da giustificare una disciplina diversa da    &#13;
 quella prevista nel caso di giudici appartenenti ad uffici giudiziari    &#13;
 diversi;                                                                 &#13;
      che un ulteriore profilo di  illogicità  viene  indicato  nelle    &#13;
 difficoltà interpretative cui la norma darebbe luogo nel caso in cui    &#13;
 il  giudice  delle  indagini  preliminari,  tenuto a rinnovare l'atto    &#13;
 dichiarato nullo dal  giudice  del  dibattimento,  non  sappia  quali    &#13;
 determinazioni  assumere,  in  mancanza  di specifiche indicazioni da    &#13;
 parte di quest'ultimo;                                                   &#13;
      che nel giudizio è intervenuto il Presidente del Consiglio  dei    &#13;
 ministri,   rappresentato   dall'Avvocatura   generale  dello  Stato,    &#13;
 concludendo per la manifesta infondatezza della questione sulla  base    &#13;
 delle ordinanze n. 13 del 1992 e n. 241 del 1991 di questa Corte;        &#13;
    Considerato che questa Corte ha già avuto più volte occasione di    &#13;
 esaminare,  non solo sotto il profilo dell'art. 3 della Costituzione,    &#13;
 ma anche in riferimento al principio  espresso  dall'art.  101  della    &#13;
 Costituzione,  la  norma  impugnata,  e  di rilevare che il principio    &#13;
 dell'indipendenza  dei  giudici comporta, nel sistema processuale, la    &#13;
 previsione   di    disposizioni    preordinate    al    coordinamento    &#13;
 dell'esercizio     delle     funzioni    giurisdizionali,    mediante    &#13;
 l'individuazione della competenza e la determinazione  degli  effetti    &#13;
 degli  atti  processuali,  anche  in relazione all'attività di altra    &#13;
 autorità  giudiziaria,  allo  scopo  di  perseguire   finalità   di    &#13;
 giustizia  e,  come  nel caso della norma in esame, di pervenire alla    &#13;
 sollecita definizione del processo (cfr. ord. n. 241 del 1991);          &#13;
      che detto rilievo non solo vale ad escludere che la norma limiti    &#13;
 l'esercizio della funzione giurisdizionale  oltre  il  termine  della    &#13;
 stretta soggezione del giudice alla legge, ma a riconoscerne anche la    &#13;
 ragionevolezza  in  quanto, in uno sviluppo logico delle diverse fasi    &#13;
 processuali,   non   pone   alcuna   discriminazione   tra   funzioni    &#13;
 giurisdizionali  distinte  (cfr.  ord. n. 13 del 1992), ma si limita,    &#13;
 coerentemente, a stabilire, in caso di contrasto, la prevalenza della    &#13;
 decisione  del  giudice  della   fase   dibattimentale   su   quella,    &#13;
 antecedente, del giudice dell'udienza preliminare;                       &#13;
      che,  inoltre,  la  disciplina  processuale sui conflitti mira -    &#13;
 come reso esplicito dalla stessa Relazione ministeriale - a  regolare    &#13;
 la  sfera  della  giurisdizione  e  della  competenza  e  non anche i    &#13;
 dissensi tra gli uffici in ordine a situazioni diverse; casi  in  cui    &#13;
 l'interesse  ad  una  sollecita  definizione  del  processo  è stato    &#13;
 ritenuto preminente sull'interesse del giudice a non essere vincolato    &#13;
 dalla statuizione di  un  altro  giudice,  almeno  nel  caso  in  cui    &#13;
 quest'ultimo sia quello dibattimentale;                                  &#13;
      che,  alla  luce  di  dette  considerazioni,  nell'ordinanza  di    &#13;
 rimessione non si rinvengono rilievi idonei a pervenire a conclusioni    &#13;
 diverse da quelle già espresse nelle ordinanze nn. 13, 69 e  71  del    &#13;
 1992  e  254  del  1991,  anche  sotto  il  profilo dell'art. 3 della    &#13;
 Costituzione,   sicché   la   questione   deve   essere   dichiarata    &#13;
 manifestamente infondata.                                                &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  28, secondo comma, del codice di procedura    &#13;
 penale, sollevata, in riferimento all'art. 3 della Costituzione,  dal    &#13;
 giudice  per le indagini preliminari presso la Pretura di Torino, con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 23 febbraio 1994.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 marzo 1994.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
