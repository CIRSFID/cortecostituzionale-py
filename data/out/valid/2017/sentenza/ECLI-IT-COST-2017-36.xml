<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2017-02-15/36/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2017-02-15/36"/>
          <FRBRalias value="ECLI:IT:COST:2017:36" name="ECLI"/>
          <FRBRdate date="2017-02-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="36"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2017-02-15/36/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2017-02-15/36/ita@"/>
          <FRBRdate date="2017-02-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2017-02-15/36/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2017-02-15/36/ita@.xml"/>
          <FRBRdate date="2017-02-15" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2017-01-10" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2017-02-15" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="GROSSI" href="/akn/ontology/person/it/GROSSI" showAs="GROSSI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="paoloGrossi" href="/akn/ontology/person/it/paoloGrossi" showAs="Paolo GROSSI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2017</akn4cc:anno>
        <akn4cc:numero>36</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2017:36</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>GROSSI</akn4cc:presidente>
        <akn4cc:relatore>Aldo Carosi</akn4cc:relatore>
        <akn4cc:data_decisione>2017-01-10</akn4cc:data_decisione>
        <akn4cc:data_deposito>2017-02-15</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#paoloGrossi">Paolo GROSSI</judge>; Giudici : <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>, <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale degli artt. 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 e 12 della legge della Regione Abruzzo 6 novembre 2015, n. 38, recante «Istituzione del Parco Naturale Regionale Costa dei Trabocchi e modifiche alla legge regionale 21 giugno 1996, n. 38 (Legge-quadro sulle aree protette della Regione Abruzzo per l'Appennino Parco d'Europa)», promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 4-5 gennaio 2016, depositato in cancelleria il 12 gennaio 2016 ed iscritto al n. 1 del registro ricorsi 2016.
 Udito nell'udienza pubblica del 10 gennaio 2017 il Giudice relatore Aldo Carosi;
 udito l'avvocato dello Stato Gianna Galluzzo per il Presidente del Consiglio dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Presidente del Consiglio dei ministri, con ricorso notificato il 4-5 gennaio 2016 e depositato il successivo 12 gennaio (reg. ric. n. 1 del 2016), ha promosso questioni di legittimità costituzionale degli artt. 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 e 12 della legge della Regione Abruzzo 6 novembre 2015, n. 38, recante «Istituzione del Parco Naturale Regionale Costa dei Trabocchi e modifiche alla legge regionale 21 giugno 1996, n. 38 (Legge-quadro sulle aree protette della Regione Abruzzo per l'Appennino Parco d'Europa)», in riferimento agli artt. 117, secondo comma, lettera s), e 118, secondo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> ed in relazione agli <mref><ref href="/akn/it/act/legge/stato/1991-12-06/394/!main#art_19">artt. 19</ref> e <ref href="/akn/it/act/legge/stato/1991-12-06/394/!main#art_20">20 della legge 6 dicembre 1991, n. 394</ref></mref> (Legge quadro sulle aree protette) ed agli <mref><ref href="/akn/it/act/legge/stato/1982-12-31/979/!main#art_25">artt. 25</ref> e <ref href="/akn/it/act/legge/stato/1982-12-31/979/!main#art_26">26 della legge 31 dicembre 1982, n. 979</ref></mref> (Disposizioni per la difesa del mare).
 Il ricorrente rileva che l'art. 1, comma 1, della legge reg. Abruzzo n. 38 del 2015 istituisce il Parco naturale regionale "Costa dei Trabocchi", classificandolo, nel comma seguente, come tale ai sensi dell'<ref href="/akn/it/act/legge/stato/1991-12-06/394/!main#art_2__para_2">art. 2, comma 2, della legge 6 dicembre 1991, n. 394</ref> (Legge quadro sulle aree protette), oltre che dell'art. 9, comma 1, della legge regionale n. 38 del 1996.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il successivo art. 2, comma 1, della legge reg. Abruzzo n. 38 del 2015 individua l'area interessata disponendo che «Il Parco è composto dal tratto di mare prospiciente la costa dei Comuni di San Vito Chietino e di Rocca San Giovanni a partire dalla linea di costa fino a sei miglia marine a partire dai rispettivi limiti nord e sud lungo la costa secondo le coordinate dei vertici stabilite dal comma 2».
 Dunque, a giudizio del ricorrente, l'area protetta di nuova istituzione interessa, secondo le coordinate riportate all'art. 2, comma, 2 della legge reg. Abruzzo n. 38 del 2015, unicamente il territorio marino, vale a dire quello entro le sei miglia dalla costa. Si configurerebbe, allora, a tutti gli effetti come un'area marina protetta, nonostante la denominazione di «Parco naturale regionale».
 Il Presidente del Consiglio dei ministri rammenta che ai sensi dell'<ref href="/akn/it/act/legge/stato/1991/394/!main#art_2__para_2">art. 2, comma 2, della legge n. 394 del 1991</ref>, i parchi naturali regionali sono costituiti da aree terrestri fluviali, lacuali e, solo eventualmente, da tratti di mare prospicienti la costa, da intendersi peraltro come possibili limitate estensioni della parte terrestre, fluviale o lacuale, di valore naturalistico e ambientale, che costituirebbero, nell'ambito di una o più regioni limitrofe, un sistema omogeneo individuato dagli assetti naturali dei luoghi, dai valori paesaggistici ed artistici e dalle tradizioni culturali delle popolazioni locali.
 Secondo il ricorrente, sarebbe evidente che la norma in esame eccederebbe dalla competenza regionale, considerato che l'istituzione di aree protette interessanti gli ambienti marini rientrerebbe nell'ambito delle competenze riservate allo Stato. A tale proposito occorrerebbe richiamare la normativa di cui al Titolo V (Riserve marine) della <ref href="/akn/it/act/legge/stato/1982/979/!main">legge n. 979 del 1982</ref>, come successivamente modificata, in base alla quale le riserve marine sono attualmente istituite, «previa istruttoria tecnica di valutazione concernente gli appositi studi conoscitivi di carattere ambientale e socio-economico, con decreto del Ministro dell'ambiente e della tutela del territorio e del mare, d'intesa con la regione interessata e previo parere della Conferenza unificata» (art. 26).
 Il Presidente del Consiglio richiama l'<ref href="/akn/it/act/legge/stato/1991/394/!main#art_19">art. 19 della legge n. 394 del 1991</ref>, il quale definisce la gestione delle aree protette marine, disponendo che il raggiungimento delle finalità istitutive di ciascuna area marina è assicurato attraverso l'Ispettorato centrale per la difesa del mare. D'altra parte il Titolo III (Aree naturali protette regionali) della <ref href="/akn/it/act/legge/stato/1991/394/!main">legge n. 394 del 1991</ref> delinea e definisce, agli articoli da 22 a 28, le norme istitutive delle stesse. Tali disposizioni non contemplerebbero alcun riferimento al territorio marino, riferendosi anzi più propriamente all'ambito terrestre.
 In sintesi, secondo il ricorrente, la norma regionale istituirebbe un'area protetta con una denominazione, una strumentazione gestionale e di salvaguardia relative ad un parco regionale terrestre, perimetrando invece come zona da proteggere un'area marina ed esercitando competenze non spettanti alla regione.
 In ciò consisterebbe il contrasto con le vigenti norme costituzionali, secondo quanto emerge chiaramente dal tenore dell'art. 117, secondo comma, lettera s), Cost., che affida allo Stato la competenza legislativa esclusiva nella materia «tutela dell'ambiente».
 Inoltre, la richiamata disciplina regionale confliggerebbe con la disposizione del successivo art. 118, secondo comma, Cost., in base al quale «I Comuni, le Province e le Città metropolitane sono titolari di funzioni amministrative proprie e di quelle conferite con legge statale o regionale secondo le rispettive competenze». Tale norma, infatti, anche se esplicitamente riferita alle sole competenze degli enti territoriali minori, porrebbe chiaramente il principio secondo il quale la competenza legislativa circa l'allocazione delle funzioni amministrative dipenderebbe dalla competenza legislativa nel settore di volta in volta considerato.
 Le menzionate disposizioni dell'impugnata legge regionale violerebbero i parametri costituzionali evocati nella parte in cui assegnano direttamente, mediante la normazione primaria, una funzione amministrativa ricadente in una materia, la «tutela dell'ambiente», che non spetta alla competenza legislativa regionale e che è assegnata in via esplicita dalla legge statale alla competenza dell'amministrazione centrale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Conseguenza della illegittimità costituzionale degli artt. 1 e 2 della legge reg. Abruzzo n. 38 del 2015 sarebbe l'illegittimità dei successivi articoli della medesima legge regionale, con esclusione dell'art. 11, che sono volti a dettare la disciplina di dettaglio dell'istituito Parco regionale. Risulterebbero, in particolare, contraddittori rispetto alla perimetrazione esclusivamente marina individuata i riferimenti alla tutela dell'ambiente terrestre (art. 2, comma 4, e art. 3, comma 1), così come la previsione del Piano e del Regolamento del parco e del Programma pluriennale economico e sociale (artt. 6 e 7), nonché il rimando alle norme di salvaguardia previste dall'art. 9 della legge <ref href="/akn/it/act/regolamentoUe/eu/1996/38/!main">reg. n. 38 del 1996</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Si tratterebbe infatti di contenuti e strumenti di gestione inerenti ai parchi terrestri e che dunque non potrebbero trovare applicazione per un'area protetta marina.
 2.- La Regione Abruzzo non si è costituita.
 3.- All'udienza pubblica, l'Avvocatura generale dello Stato ha delimitato l'oggetto del giudizio rinviando alle disposizioni individuate nella delibera ad impugnare - che, a sua volta, richiama la relazione del Dipartimento degli affari regionali - e, dunque, agli artt. 1, comma 1; 2, commi 1, 2 e 4; 3, comma 1; 6, 7 e 9 della legge regionale in esame.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p> - Con il ricorso indicato in epigrafe il Presidente del Consiglio dei ministri ha impugnato gli artt. 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 e 12 della legge della Regione Abruzzo 6 novembre 2015, n. 38, recante «Istituzione del Parco Naturale Regionale Costa dei Trabocchi e modifiche alla legge regionale 21 giugno 1996, n. 38 (Legge-quadro sulle aree protette della Regione Abruzzo per l'Appennino Parco d'Europa)», deducendo la violazione degli artt. 117, secondo comma, lettera s), e 118, secondo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione agli <mref><ref href="/akn/it/act/legge/stato/1991-12-06/394/!main#art_19">artt. 19</ref> e <ref href="/akn/it/act/legge/stato/1991-12-06/394/!main#art_20">20 della legge 6 dicembre 1991, n. 394</ref></mref> (Legge quadro sulle aree protette) e agli <mref><ref href="/akn/it/act/legge/stato/1982-12-31/979/!main#art_25">artt. 25</ref> e <ref href="/akn/it/act/legge/stato/1982-12-31/979/!main#art_26">26 della legge 31 dicembre 1982, n. 979</ref></mref> (Disposizioni per la difesa del mare).
 Il ricorrente rileva che l'art. 1, comma 1, della legge reg. Abruzzo n. 38 del 2015 istituisce il Parco naturale regionale "Costa dei Trabocchi", classificandolo, nel comma seguente, come tale ai sensi dell'<ref href="/akn/it/act/legge/stato/1991-12-06/394/!main#art_2__para_2">art. 2, comma 2, della legge 6 dicembre 1991, n. 394</ref> (Legge quadro sulle aree protette), oltre che dell'art. 9, comma 1, della legge regionale n. 38 del 1996.
 Il successivo art. 2, comma 1, della legge reg. Abruzzo n. 38 del 2015 individua l'area interessata disponendo che «Il Parco è composto dal tratto di mare prospiciente la costa dei Comuni di San Vito Chietino e di Rocca San Giovanni a partire dalla linea di costa fino a sei miglia marine a partire dai rispettivi limiti nord e sud lungo la costa secondo le coordinate dei vertici stabilite dal comma 2».
 Dunque, a giudizio del ricorrente, l'area protetta di nuova istituzione interessa, secondo le coordinate riportate all'art. 2, comma, 2 della legge reg. Abruzzo n. 38 del 2015, unicamente il territorio marino, vale a dire quello entro le sei miglia dalla costa.
 Secondo il Presidente del Consiglio dei ministri, le menzionate disposizioni, che istituiscono un'area protetta denominandola Parco naturale regionale "Costa dei Trabocchi" e ne regolamentano la gestione e la salvaguardia, violerebbero, innanzitutto, l'art. 117, secondo comma, lettera s), Cost., dal momento che l'area di nuova istituzione interessa unicamente il territorio marino e si configura a tutti gli effetti come un'area marina protetta, la cui disciplina - contenuta negli <mref><ref href="/akn/it/act/legge/stato/1991/394/!main#art_19">artt. 19</ref> e <ref href="/akn/it/act/legge/stato/1991/394/!main#art_20">20 della legge n. 394 del 1991</ref></mref> e negli <mref><ref href="/akn/it/act/legge/stato/1982/979/!main#art_25">artt. 25</ref> e <ref href="/akn/it/act/legge/stato/1982/979/!main#art_26">26 della legge n. 979 del 1982</ref></mref> - afferisce alla materia di competenza esclusiva statale «tutela dell'ambiente».
 Le norme censurate sarebbero altresì lesive dell'art. 118, secondo comma, Cost., in quanto, sebbene la disposizione costituzionale si riferisca esplicitamente solo alle competenze degli enti territoriali minori, porrebbe chiaramente il principio secondo il quale la competenza legislativa circa l'allocazione delle funzioni amministrative dipende dalla competenza legislativa nel settore di volta in volta considerato.
 2.- All'udienza pubblica, l'Avvocatura generale dello Stato ha precisato che l'oggetto del giudizio deve essere circoscritto alle disposizioni individuate nella delibera ad impugnare - che, a sua volta, richiama la relazione del Dipartimento affari regionali - e, dunque, agli artt. 1, comma 1; 2, commi 1, 2 e 4; 3, comma 1; 6, 7 e 9 della legge regionale in esame.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In questi termini risulta quindi individuato l'oggetto del giudizio.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>- Le questioni sollevate in riferimento all'art. 117, secondo comma, lettera s), Cost. sono fondate.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>1.- Con la legge regionale in esame, la Regione Abruzzo ha istituito il Parco naturale regionale "Costa dei Trabocchi".
 Il Parco viene classificato con riferimento agli <ref href="/akn/it/act/legge/stato/1991/394/!main#art_2__para_2">artt. 2, comma 2, della legge n. 394 del 1991</ref> e 9, comma 1, della legge Reg. Abruzzo n. 38 del 1996. Tali disposizioni prevedono che i parchi naturali regionali sono costituiti da aree terrestri, fluviali, lacuali ed eventualmente da tratti di mare prospicienti la costa, in cui siano inclusi uno o più ecosistemi intatti o poco alterati da interventi antropici, che costituiscono, nell'ambito di una o più regioni limitrofe, un sistema omogeneo caratterizzato dalla presenza di specie animali, vegetali o siti geomorfologici di rilevante interesse naturalistico, scientifico, culturale, educativo e ricreativo, nonché da valori paesaggistici, artistici e dalle tradizioni delle popolazioni locali.
 Nel caso in esame viene enunciata la sottoposizione a tutela dell'area interessata da elementi floro-faunistici di particolare valore naturalistico.
 3.2.- Tuttavia, l'individuazione dell'area tutelata riguarda il «tratto di mare prospiciente la costa dei Comuni di San Vito Chietino e di Rocca San Giovanni a partire dalla linea di costa fino a sei miglia marine a partire dai rispettivi limiti nord e sud lungo la costa secondo le coordinate dei vertici stabilite dal comma 2». In tal modo la perimetrazione del parco naturale "Costa dei Trabocchi" comprende esclusivamente un tratto di mare prospiciente la costa compreso tra le coordinate indicate nel secondo comma dell'art. 2.
 Nella relazione di accompagnamento alla legge regionale n. 38 del 2015, impugnata con il presente ricorso, si legge come in passato la legislazione regionale abbia privilegiato «la dimensione interna dell'Abruzzo, affidando un ruolo quanto meno limitato alla parte costiera».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In tale contesto si riconosce espressamente che la parte terrestre prospiciente all'area del Parco naturale regionale "Trabocchi del Chietino" coincide con l'area del sito di interesse comunitario (SIC) IT714106 e che con la legge della Regione Abruzzo 30 marzo 2007, n. 5 (Disposizioni urgenti per la tutela e la valorizzazione della Costa Teatina) sono state istituite sei riserve costiere, tra le quali la riserva naturale "Grotta delle Farfalle" (art. 3, comma 1), in un'area corrispondente a quella marina che la legge impugnata intende istituire.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Inoltre, sia la relazione, sia l'impugnato art. 2, comma 3, riconoscono che il valore della presenza dei "trabocchi", quali beni storico-artistici, è stato già in passato tutelato con la legge reg. Abruzzo 14 dicembre 1994, n. 93 (Disposizioni per il recupero e la valorizzazione dei trabucchi della costa abruzzese), poi integrata da successivi interventi legislativi, quali la legge della Regione Abruzzo 11 agosto 2009, n. 13, recante: «Modifiche ed integrazioni alla L.R. n. 71/2001 (Rifinanziamento della L.R. n. 93/1994 concernente: Disposizioni per il recupero e la valorizzazione dei trabucchi della costa abruzzese) e norme relative al recupero, alla salvaguardia e alla valorizzazione dei trabocchi da molo, anche detti «caliscendi» o «bilancini», della costa abruzzese».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>I "trabocchi" assurgerebbero, di fatto, ad icòna di una relazione tra terra e mare che «se pur ovvia, [...] non è, sin ora, assurta ad oggetto di tutela e valorizzazione integrata».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Conseguentemente, la legge regionale in esame, al menzionato art. 2, comma 3, incoerentemente afferma che la presenza dei "trabocchi", antichi strumenti di pesca, caratterizza l'ambiente marino.
 La contraddizione tra le norme di principio istitutive del Parco naturale regionale (art. 1) - che, come già detto, solo marginalmente può comprendere anche tratti di mare prospicienti la costa - e la concreta perimetrazione dello stesso (art. 2) contenente solo un ampio tratto di mare prospiciente la costa, palesa la reale portata della legge in esame, tesa, in realtà, alla creazione di un'area marina protetta in violazione del riparto di competenze legislative tra Stato e Regioni.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La disciplina delle aree protette rientra, invero, nella competenza esclusiva dello Stato in materia di «tutela dell'ambiente» ex art. 117, secondo comma, lettera s), Cost. (ex mult<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/212/!main">is, sentenze n. 212 del 2014</ref> e n. 14 del 2012), ed è contenuta nella <ref href="/akn/it/act/legge/stato/1991/394/!main">legge n. 394 del 1991</ref> che detta i principi fondamentali della materia, ai quali la legislazione regionale è chiamata ad adeguarsi, assumendo anche i connotati di normativa interposta (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/212/!main">sentenze n. 212 del 2014</ref>, n. 325 del 2011, n. 315 e n. 193 del 2010). L'<ref href="/akn/it/act/legge/stato/1991/394/!main#art_2">art. 2 della legge n. 394 del 1991</ref> classifica, difatti, le aree naturali protette in parchi nazionali e regionali (art. 2, commi 1 e 2), a seconda del loro rilievo nazionale o locale, e in riserve naturali, statali o regionali in base alla rilevanza degli interessi in esse rappresentati (art. 2, comma 3). Mentre i parchi hanno finalità generali di protezione e valorizzazione della natura, le riserve (oltre ad avere di regola dimensioni molto più ridotte) hanno principalmente una finalità di natura conservativa connessa alla presenza di specifici valori floro-faunistici o di diversità biologica.
 La legge quadro (art. 2, comma 2) non prevede, dunque, la figura del parco regionale marino, ma solamente la possibilità che tratti di mare prospicienti la costa vengano a far parte di parchi regionali costituiti da aree terrestri, fluviali e lacuali.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Con specifico riferimento all'ambiente marino, poi, l'<ref href="/akn/it/act/legge/stato/1991/394/!main#art_2__para_4">art. 2, comma 4, della legge n. 394 del 1991</ref> distingue le aree particolarmente protette e quelle definite dall'<ref href="/akn/it/act/legge/stato/1982/979/!main#art_25">art. 25 della legge n. 979 del 1982</ref>, in base alla quale le riserve naturali marine sono costituite da ambienti marini, dati dalle acque, dai fondali e dai tratti di costa prospicienti che presentano un rilevante interesse per le caratteristiche naturali, geomorfologiche, fisiche, biochimiche con particolare riguardo alla flora e alla fauna marine e costiere e per l'importanza scientifica, ecologica, culturale, educativa ed economica che rivestono.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ai sensi dell'art. 18 della medesima <ref href="/akn/it/act/legge/stato/1991/394/!main">legge n. 394 del 1991</ref>, il Ministro dell'ambiente istituisce le aree protette marine: l'istituto è imperniato sulla centralità del ruolo dello Stato anche nella fase di gestione (art. 19) e, in generale, nella disciplina richiamata (art. 20). L'ambito marino, diversamente da quello terrestre, risulta, dunque, caratterizzato da un prevalente interesse statale.
 In definitiva, dal momento che il Parco naturale regionale, istituito con la legge reg. Abruzzo n. 38 del 2015, per le ragioni sopra esposte non comprende in via prevalente un'area di terra emersa, ma esclusivamente un'area marina, esso è in realtà da ascrivere alla aree marine protette, e pertanto la suddetta legge si pone in contrasto con la classificazione e l'istituzione delle aree naturali di cui agli <mref><ref href="/akn/it/act/legge/stato/1991/394/!main#art_2">artt. 2</ref>, <ref href="/akn/it/act/legge/stato/1991/394/!main#art_18">18</ref>, <ref href="/akn/it/act/legge/stato/1991/394/!main#art_19">19</ref> e <ref href="/akn/it/act/legge/stato/1991/394/!main#art_20">20 della legge n. 394 del 1991</ref></mref> e, di conseguenza, con l'art. 117, secondo comma, lettera s), Cost., che attribuisce allo Stato la competenza legislativa esclusiva in materia di tutela dell'ambiente e dell'ecosistema: materia nella quale rientrano le aree marine protette.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>3.- Se le norme impugnate istituiscono la "Costa dei Trabocchi" come «Parco naturale regionale» (art. 1, comma 1); delimitano i confini del Parco (art. 2, commi 1 e 2); individuano i principi della tutela dell'ambiente terrestre e le finalità del Parco (art. 2, comma 4 e art. 3), disciplinano il Piano del Parco (art. 6), il Programma pluriennale economico e sociale e il Regolamento del Parco (art. 7) e le Norme di salvaguardia (art. 9), ulteriori disposizioni della legge regionale impugnata (art. 1, commi 2 e 3, che classificano il parco e dispongono la tutela, nell'area marina interessata, di elementi floro-faunistici di particolare valore naturalistico; art. 2, comma 3, che descrive gli ulteriori elementi caratterizzanti l'area; art. 4, che individua i confini; artt. 5 e 8, che regolamentano l'ente e il piano di gestione; ed infine artt. 11 e 12, che prevedono la copertura finanziaria e l'entrata in vigore) risultano in rapporto di stretta ed esclusiva dipendenza funzionale con le disposizioni censurate.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ad esse deve dunque estendersi, in via consequenziale, ai sensi dell'<ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_27">art. 27 della legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), la declaratoria di illegittimità costituzionale.
 4.- Restano assorbite le ulteriori questioni sollevate dal ricorrente.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 1) dichiara l'illegittimità costituzionale degli artt. 1, comma 1; 2, commi 1, 2 e 4; 3, comma 1; 6, 7 e 9 della legge della Regione Abruzzo 6 novembre 2015, n. 38 recante «Istituzione del Parco Naturale Regionale Costa dei Trabocchi e modifiche alla legge regionale 21 giugno 1996, n. 38 (Legge-quadro sulle aree protette della Regione Abruzzo per l'Appennino Parco d'Europa)»;
 2) dichiara, in via consequenziale, ai sensi dell'<ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_27">art. 27 della legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), l'illegittimità costituzionale degli artt. 1, commi 2 e 3; 2, comma 3; 4, 5, 8, 11 e 12.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2017-01-10">10 gennaio 2017</date>.
 F.to:
 <signature><judge refersTo="#paoloGrossi" as="#presidente">Paolo GROSSI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#aldoCarosi" as="#relatore">Aldo CAROSI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2017-02-15">15 febbraio 2017</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
