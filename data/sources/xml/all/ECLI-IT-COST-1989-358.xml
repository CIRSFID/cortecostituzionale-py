<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>358</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:358</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/06/1989</data_decisione>
    <data_deposito>27/06/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale degli artt. 2 e 4 della    &#13;
 legge 26  settembre  1985,  n.  482  (Modificazioni  del  trattamento    &#13;
 tributario   delle   indennità  di  fine  rapporto  e  dei  capitali    &#13;
 corrisposti in dipendenza di contratti di assicurazione sulla  vita),    &#13;
 e  17, primo comma, del d.P.R. 22 dicembre 1986, n. 917 (Approvazione    &#13;
 del testo unico delle imposte sui redditi) promossi con  le  seguenti    &#13;
 ordinanze:                                                               &#13;
      1)  ordinanza  emessa  il  25 marzo 1988 dal Pretore di Roma nel    &#13;
 procedimento  civile  vertente  tra  Contardi  Ottavio  ed  altri   e    &#13;
 l'I.N.A.D.E.L.,  iscritta  al  n.  328  del registro ordinanze 1988 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  30,  prima    &#13;
 serie speciale, dell'anno 1988;                                          &#13;
      2)   ordinanza  emessa  il  26  aprile  1988  dalla  Commissione    &#13;
 Tributaria  di  primo  grado  di  Bergamo  sul  ricorso  proposto  da    &#13;
 Pellegrinelli   Irma  contro  l'Intendenza  di  Finanza  di  Bergamo,    &#13;
 iscritta al n. 639 del registro ordinanze  1988  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  47, prima serie speciale,    &#13;
 dell'anno 1988;                                                          &#13;
      3) ordinanza emessa il 16 giugno 1988 dal Pretore di Firenze nel    &#13;
 procedimento civile  vertente  tra  Renard  Carlo  e  l'I.N.A.D.E.L.,    &#13;
 iscritta al n. 644 del registro ordinanze del 1988 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  47,  prima  serie  speciale,    &#13;
 dell'anno 1988;                                                          &#13;
    Visto l'atto di costituzione di Contardi Ottavio ed altri;            &#13;
    Udito  nell'udienza  pubblica  dell'8  febbraio  1989  il  Giudice    &#13;
 relatore Francesco Greco.                                                &#13;
    Ritenuto  che  il Pretore di Roma, con ordinanza del 25 marzo 1988    &#13;
 (R.O. n. 328 del 1988), nel procedimento civile promosso da  Contardi    &#13;
 Ottavio  ed  altri  contro l'I.N.A.D.E.L., per ottenere la indennità    &#13;
 premio di servizio comprensiva anche dei ratei dei contributi versati    &#13;
 dagli  stessi  impiegati,  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale, in riferimento all'art. 53 della Costituzione,  degli    &#13;
 artt. 2 e 4 della legge 26 settembre 1985, n. 482, nella parte in cui    &#13;
 non prevedono che dall'imponibile I.R.P.E.F. sia detratta  una  somma    &#13;
 pari   alla   percentuale   della   indennità   premio  di  servizio    &#13;
 corrispondente al rapporto esistente, alla data  del  collocamento  a    &#13;
 riposo,  tra  il  contributo posto a carico del pubblico dipendente e    &#13;
 l'aliquota   complessiva   del   contributo   previdenziale   versato    &#13;
 all'I.N.A.D.E.L.;                                                        &#13;
      che,  a  parere  del giudice remittente, sarebbero applicabili i    &#13;
 principi affermati dalla sentenza n. 178 del 1986 di questa Corte per    &#13;
 l'indennità di buonuscita degli statali;                                &#13;
      che  nel  giudizio  le  parti  private, regolarmente costituite,    &#13;
 hanno concluso per la fondatezza della questione;                        &#13;
      che   l'Avvocatura   Generale   dello   Stato,   intervenuta  in    &#13;
 rappresentanza del Presidente del Consiglio dei ministri, ha concluso    &#13;
 per   la  inammissibilità  della  questione,  non  essendo  essa  di    &#13;
 competenza del giudice ordinario ma delle Commissioni Tributarie;        &#13;
      che  identiche  questioni sono state sollevate con ordinanza del    &#13;
 16 giugno 1988 (R.O. n. 639 del 1988) dalla Commissione Tributaria di    &#13;
 primo  grado  di Bergamo, nel giudizio proposto da Pellegrinelli Irma    &#13;
 contro l'Intendenza di Finanza di Bergamo, e dal Pretore di  Firenze,    &#13;
 nel  procedimento  civile  tra  Renard  Carlo  e  l'I.N.A.D.E.L., con    &#13;
 ordinanza del 16 giugno 1988  (R.O.  n.  644  del  1988),  estensiva,    &#13;
 peraltro, della censura anche all'art. 17, primo comma, del d.P.R. 22    &#13;
 dicembre 1986, n. 917;                                                   &#13;
    Considerato  che  i  giudizi  vanno  riuniti  per  la  sussistente    &#13;
 connessione e decisi con un unico provvedimento;                         &#13;
      che  la  legge 13 maggio 1988, n. 154 (Conversione in legge, con    &#13;
 modificazioni, del decreto-legge 14 marzo 1988, n. 70, recante  norme    &#13;
 in   materia   tributaria,  nonché  per  la  esemplificazione  delle    &#13;
 procedure di accatastamento degli immobili urbani), ai commi 3- ter e    &#13;
 3-quater,   ha  regolato  la  materia  di  cui  trattasi  (detrazione    &#13;
 dall'imponibile per la indennità di  fine  rapporto  del  contributo    &#13;
 versato dall'impiegato);                                                 &#13;
      che,  pertanto,  la rilevanza della questione va riesaminata dai    &#13;
 giudici remittenti alla stregua delle nuove norme;                       &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Riunisce  i  giudizi (R.O. nn. 328, 639 e 644 del 1988), ordina la    &#13;
 restituzione degli atti rispettivamente al Pretore di Roma  (R.O.  n.    &#13;
 328  del 1988); alla Commissione Tributaria di primo grado di Bergamo    &#13;
 (R.O. n. 639 del 1988) e al Pretore  di  Firenze  (R.O.  n.  644  del    &#13;
 1988).                                                                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 maggio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 giugno 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
