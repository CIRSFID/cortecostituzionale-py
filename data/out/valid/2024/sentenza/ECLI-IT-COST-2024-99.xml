<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2024-06-04/99/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2024-06-04/99"/>
          <FRBRalias value="ECLI:IT:COST:2024:99" name="ECLI"/>
          <FRBRdate date="2024-06-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="99"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2024-06-04/99/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2024-06-04/99/ita@"/>
          <FRBRdate date="2024-06-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2024-06-04/99/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2024-06-04/99/ita@.xml"/>
          <FRBRdate date="2024-06-04" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2024-04-16" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2024-06-04" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale parziale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="BARBERA" href="/akn/ontology/person/it/BARBERA" showAs="BARBERA"/>
        <TLCPerson eId="marcoDAlberti" href="/akn/ontology/person/it/marcoDAlberti" showAs="Marco D'Alberti"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2024</akn4cc:anno>
        <akn4cc:numero>99</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2024:99</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>BARBERA</akn4cc:presidente>
        <akn4cc:relatore>Marco D'Alberti</akn4cc:relatore>
        <akn4cc:data_decisione>2024-04-16</akn4cc:data_decisione>
        <akn4cc:data_deposito>2024-06-04</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale parziale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta da:
 Presidente: <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>; Giudici : <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI, Giovanni PITRUZZELLA, Antonella SCIARRONE ALIBRANDI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/2001-03-26/151/!main#art_42bis__para_1">art. 42-bis, comma 1, del decreto legislativo 26 marzo 2001, n. 151</ref> (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'<ref href="/akn/it/act/legge/stato/2000-03-08/53/!main#art_15">articolo 15 della legge 8 marzo 2000, n. 53</ref>), promosso dal Consiglio di Stato, terza sezione, nel procedimento vertente tra il Ministero dell'interno, Dipartimento Vigili del fuoco, del soccorso pubblico e della difesa civile e E. B., con sentenza non definitiva del 15 novembre 2023, iscritta al n. 158 del registro ordinanze 2023 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 51, prima serie speciale, dell'anno 2023.
 Udito nella camera di consiglio del 16 aprile 2024 il Giudice relatore Marco D'Alberti;
 deliberato nella camera di consiglio del 16 aprile 2024.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con sentenza non definitiva del 15 novembre 2023, iscritta al n. 158 del registro ordinanze 2023, il Consiglio di Stato, sezione terza, ha sollevato, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_29">29</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30">30</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_31">31 della Costituzione</ref></mref>, questioni di legittimità costituzionale dell'art. 42&amp;#727;bis, comma 1, <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-26/151/!main">del decreto legislativo 26 marzo 2001, n.</ref> 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma d<ref href="/akn/it/act/legge/stato/2000-03-08/53/!main#art_15">ell'articolo 15 della legge 8 marzo 2000, n</ref>. 53), inserito d<ref href="/akn/it/act/legge/stato/2003-12-24/350/!main#art_3__para_105">all'art. 3, comma 105, della legge 24 dicembre 2003, n.</ref> 350, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2004)».
 La disposizione censurata prevede che «[i]l genitore con figli minori fino a tre anni di età dipendente di amministrazioni pubbliche di cui <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_1__para_2">all'articolo 1, comma 2, del decreto legislativo 30 marzo 2001, n.</ref> 165, e successive modificazioni, può essere assegnato, a richiesta, anche in modo frazionato e per un periodo complessivamente non superiore a tre anni, ad una sede di servizio ubicata nella stessa provincia o regione nella quale l'altro genitore esercita la propria attività lavorativa, subordinatamente alla sussistenza di un posto vacante e disponibile di corrispondente posizione retributiva e previo assenso delle amministrazioni di provenienza e destinazione».
 2.- Il rimettente descrive la fattispecie oggetto del giudizio a quo nei seguenti termini.
 2.1.- E. B. presta servizio presso il Comando dei Vigili del fuoco di Firenze ed è residente, insieme al proprio nucleo familiare composto dal coniuge e da due figli (di cui uno minore di tre anni), in un comune della Città metropolitana di Napoli.
 Ai sensi d<ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main#art_42bis">ell'art. 42-bis del d.lgs. n. 151 del </ref>2001, la dipendente presentava un'istanza di trasferimento temporaneo al Comando dei Vigili del fuoco di Napoli, la quale veniva tuttavia rigettata dall'amministrazione di appartenenza in ragione del fatto che - oltre a non esservi disponibilità di posti vacanti nella sede richiesta - il coniuge dell'interessata prestava servizio in Molise e, quindi, in una regione diversa da quella della sede in cui era stato richiesto il trasferimento.
 2.2- Il Tribunale amministrativo regionale per la Toscana, sezione prima, con sentenza del 28 luglio 2022, n. 964, ha accolto il ricorso promosso da E. B. avverso il provvedimento di diniego, rilevando vizi nell'istruttoria compiuta dall'amministrazione con riguardo alla carenza di posti disponibili nella sede di Napoli e, comunque, ritenendo non ostativo all'accoglimento dell'istanza il fatto che il coniuge della ricorrente prestasse servizio in una regione diversa da quella della sede presso cui era stato richiesto il trasferimento, posto che, nella medesima provincia di tale sede, era stata fissata la residenza del nucleo familiare.
 Ad avviso del giudice di primo grado, infatti, l'art. 42&amp;#727;bis, comma<ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main"> 1, del d.lgs. n. 151 </ref>del 2001, non andrebbe interpretato «in senso strettamente letterale a pena di avallare situazioni palesemente irragionevoli come quella in cui il coniuge lavori a pochi chilometri dalla sede in cui viene richiesto il trasferimento ma questa si trovi oltre il confine di una diversa regione». Pertanto, sebbene il legislatore abbia valorizzato, ai fini del trasferimento temporaneo, non già la residenza del nucleo familiare, ma il luogo di lavoro dell'altro genitore, ove quest'ultimo «si trovi in una posizione che consenta (per distanza, rete viaria, rete di trasporto, etc.) di raggiungere quotidianamente il luogo di ricongiungimento, la ratio legis (che è quella di agevolare la riunione della famiglia nei primi anni di vita della prole) deve ritenersi realizzata al pari di quanto accadrebbe se la sede di servizio del primo si trovasse nella medesima regione, non potendo ragionevolmente costituire le linee di confine fra i diversi ambiti amministrativi in cui è suddiviso il Paese elemento discriminante nella materia di cui ci si occupa».
 2.3.- Avverso la sentenza del giudice di primo grado ha proposto appello il Ministero dell'interno, il quale - oltre a denunciare l'incompetenza territoriale del TAR Toscana rispetto all'impugnazione di un atto amministrativo generale presupposto e a contestare il rilevato vizio istruttorio in merito alla carenza di posti vacanti nella sede di Napoli - ha dedotto anche la violazio<ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main#art_42bis__para_1">ne dell'art. 42-bis, comma 1, del d.lgs. n. 151 </ref>del 2001: secondo l'appellante, infatti, il coniuge dell'interessata presterebbe la propria attività lavorativa al di fuori della Regione Campania e ciò non consentirebbe di ritenere perfezionato il presupposto richiesto dal legislatore per ottenere il trasferimento temporaneo.
 3.- Il giudice rimettente, dopo aver rigettato i primi due motivi di appello, ha ritenuto - con riferimento al terzo e ultimo motivo - di dover sollevare questione di legittimità costituzionale della censurata disposizione, nella parte in cui subordina la possibilità di ottenere il trasferimento temporaneo al fatto che «il coniuge del richiedente abbia la propria attività lavorativa (e non l'attività lavorativa o la residenza del nucleo familiare, ove le nozioni non coincidano) nella stessa Provincia o Regione ove è ubicata la sede di servizio presso la quale si domanda il trasferimento».
 3.1.- Ad avviso del Consiglio di Stato, infatti, in relazione a tale disposizione non sarebbe possibile accogliere l'interpretazione adeguatrice seguita dal giudice di prime cure, posto che la stessa, «pur muovendo da premesse pienamente condivisibili», sarebbe «impedita dal chiaro tenore letterale della disposizione», la quale ha fatto «espresso riferimento, quale elemento che dà titolo al richiesto trasferimento (nella medesima Provincia o Regione), alla sede di servizio del coniuge, e non alla sua (e del nucleo familiare) residenza».
 3.2.- Proprio in ragione dell'impossibilità di praticare un'interpretazione costituzionalmente orientata, secondo il giudice rimettente l'applicazione letterale della disposizione censurata condurrebbe, nel caso in esame, ad un esito irragionevole e, dunque, contra<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">rio all'art.</ref> 3 Cost., nonché contrastante con la tutela costituzionale della famiglia, della genitorialità e dell'infanzia, di c<mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_29">ui agli </ref>ar<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30">tt</ref>. 2<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_31">9, 30 e </ref></mref>31 Cost.
 Infatti, a fronte della chiara finalità dell'istituto del trasferimento temporaneo di assicurare l'unità del nucleo familiare nei primissimi anni di vita del figlio, consentendo ad entrambi i coniugi di prendersene cura, sarebbe del tutto irragionevole limitare il trasferimento del dipendente solo nella provincia o regione in cui si trova la sede di servizio del coniuge.
 Secondo il Consiglio di Stato, un simile requisito, «probabilmente conforme - quale parametro di riferimento per individuare la localizzazione territoriale del nucleo familiare, e le relative esigenze di unità e stabilità - ad un criterio di normalità sociale al momento dell'introduzione della disposizione di cui si discute», circa venti anni or sono, «e dunque tale da costituire in modo non irragionevole il perno della disciplina della tutela del nucleo familiare in relazione agli spostamenti dettati da esigenze lavorative dei suoi componenti adulti, si presta ora - anche a seguito dei mutamenti indotti negli ultimi due decenni (non escluso quello relativo al c.d. lavoro a distanza), e comunque alla maggiore facilità di spostamenti quotidiani fra Regioni limitrofe - ad applicazioni che, come nel caso di specie, possono tradire o frustrare l'intenzione del legislatore, con violazione degli indicati parametri di costituzionalità».
 Alla luce di simili trasformazioni, non risulterebbe più ragionevole, pertanto, riconoscere il trasferimento temporaneo esclusivamente nella provincia o regione in cui si trova la sede di lavoro del coniuge, posto che ciò, tra l'altro, avrebbe l'effetto di imporre lo spostamento della residenza del nucleo familiare «in funzione della sede di servizio di uno dei coniugi», con conseguente «sradicamento del nucleo familiare medesimo, laddove - come nel caso di specie - residenza familiare e sede (o sedi) di servizio, pur se collocati in Regioni diverse (ma limitrofe), sono compatibili con spostamenti quotidiani, che non alterano il radicamento territoriale del nucleo familiare».
 3.3.- Per queste ragioni, ad avviso del Consiglio di S<ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main#art_42bis__para_1">tato, l'art. 42-bis, comma 1, del d.lgs. n. 151 </ref>del 2001, si porrebbe in contrasto <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">con gli</ref> a<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_29">rt</ref>t.<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30"> 3</ref>, 2<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_31">9, 30 e </ref></mref>31 Cost.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Consiglio di Stato, sezione terza, ha sollevato, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_29">29</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30">30</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_31">31 Cost.</ref></mref>, questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main#art_42bis__para_1">art. 42-bis, comma 1, del d.lgs. n. 151 del 2001</ref>.
 2.- La disposizione censurata ha introdotto l'istituto del trasferimento temporaneo in favore dei dipendenti pubblici di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_1__para_2">art. 1, comma 2, del decreto legislativo 30 marzo 2001, n. 165</ref> (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche), che siano genitori di figli minori di tre anni.
 In base a tale previsione, il dipendente «può essere assegnato» a richiesta, per un periodo, anche frazionato, complessivamente non superiore a 3 anni, «ad una sede di servizio ubicata nella stessa provincia o regione nella quale l'altro genitore esercita la propria attività lavorativa». L'assegnazione è subordinata alla sussistenza di un posto «vacante e disponibile di corrispondente posizione retributiva», salvo motivato dissenso delle amministrazioni di provenienza e di destinazione, «limitato a casi o esigenze eccezionali», da comunicare «all'interessato entro trenta giorni dalla domanda» (<ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main#art_42bis__para_1">art. 42-bis, comma 1, del d.lgs. n. 151 del 2001</ref>).
 3.- Il rimettente denuncia l'illegittimità costituzionale dell'art. 42&amp;#727;bis, comma 1, <ref href="/akn/it/act/decretoLegislativo/stato/2001/151/!main">del d.lgs. n. 151 del </ref>2001, là dove consente il trasferimento del dipendente pubblico solo presso «una sede di servizio ubicata nella stessa provincia o regione nella quale l'altro genitore esercita la propria attività lavorativa» e non anche presso una sede ubicata nella stessa provincia o regione ove è fissata la residenza del nucleo familiare.
 Ad avviso del Consiglio di Stato, un simile limite legale alla concreta applicazione del trasferimento temporaneo sarebbe irragionevole rispetto alle finalità costituzionali cui il medesimo istituto è preposto, in violazione de<mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">gli art</ref>t.<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_29"> 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30">29</ref>, 3<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_31">0 e 31 C</ref></mref>ost.
 4.- La questione è fondata in riferimento <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">all'art. 3 C</ref>ost.
 4.1.- Questa Corte ha da tempo chiarito che le scelte del legislatore concernenti i criteri selettivi per il riconoscimento di benefici pubblici devono «essere operate, sempre e comunque, in ossequio al principio di ragionevolezza» (c<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2005/432/!main">osì sentenza n. 432 del </ref>2005; tra le alt<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/166/!main">re, sentenze n.</ref> 166 e<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/107/!main"> n. 107 del </ref></mref>2018, n. 168 del 2014, n. 172 del 2013, n. 2 del 2013, n. 40 del 2011). Ciò è stato affermato anche in relazione a disposizioni che limitavano, in maniera irragionevole, l'ambito soggettivo di applicazione di permessi o congedi straordinari per l'assistenza di familia<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/232/!main">ri (sentenze n. 232 del </ref>2018 e n. 213 del 2016).
 In tali casi, lo scrutinio di costituzionalità «va operato all'interno della specifica disposizione, al fine di verificare se vi sia una ragionevole correlazione tra la condizione prevista per l'ammissibilità al beneficio e gli altri peculiari requisiti che ne condizionano il riconoscimento e ne definiscono la ratio» (c<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2013/133/!main">osì sentenza n. 133 del </ref>2013; da ulti<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2024/42/!main">mo, sentenza n. 42 del </ref>2024): tale scrutinio deve svolgersi «secondo la struttura tipica del sindacato svolto ai sensi dell'art. 3, primo comma, Cost., che muove dall'identificazione della ratio della norma di riferimento e passa poi alla verifica della coerenza con tale ratio del filtro selettivo introdott<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/44/!main">o» (sentenza n. 44 del </ref>2020).
 4.2.- Nel caso in esame, il legislatore statale, nel consentire ai dipendenti pubblici di ottenere il trasferimento temporaneo solo «ad una sede di servizio ubicata nella stessa provincia o regione nella quale l'altro genitore esercita la propria attività lavorativa», ha introdotto un requisito che condiziona il concreto ambito di applicazione dell'istituto, anche sul piano soggettivo. In base a tale previsione, infatti, è stata esclusa in radice la possibilità di accedere al beneficio del trasferimento per quei dipendenti pubblici che hanno deciso di fissare la residenza familiare (ove vive il figlio minore) in una regione o provincia diversa da quelle in cui lavorano entrambi i genitori.
 4.3.- Orbene, proprio alla luce della sopramenzionata giurisprudenza costituzionale, una simile restrizione legale dell'ambito di applicazione dell'istituto non risulta essere ragionevole rispetto alla finalità, anche di rilievo costituzionale, che il trasferimento temporaneo mira ad assolvere.
 4.4.- Il trasferimento temporaneo dei dipendenti pubblici, proponendosi di favorire la ricomposizione dei nuclei familiari nei primissimi anni di vita dei figli, nel caso in cui i genitori si trovino a vivere separati per esigenze lavorative, è chiaramente preordinato alla realizzazione dell'obiettivo costituzionale di sostegno e promozione della famiglia, dell'infanzia e della parità dei genitori nell'accudire i figli.
 Come è stato sottolineato anche dalla giurisprudenza amministrativa, il trasferimento temporaneo ha la «funzione di agevolare la cura dei minori nella primissima infanzia», proteggendo quindi «i valori della famiglia, e più in generale della genitorialità, tutelati d<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30">all'art. 30 della Costituz</ref>ione [...] e dal successivo art. 31 [...]» (Consiglio di S<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021-02-06/1/!main">tato, sezione quarta, sentenza 16 febbraio </ref>2021, n. 1418).
 4.5.- A fronte di una simile ratio dell'istituto, non risulta ragionevole consentire il trasferimento temporaneo del genitore che sia dipendente pubblico solo nella provincia o regione in cui lavora l'altro genitore: tale limitazione, infatti, si fonda sul presupposto per cui il figlio minore da accudire si trovi necessariamente nella medesima provincia o regione in cui è fissata la sede lavorativa dell'altro genitore.
 Tuttavia, una simile presunzione non tiene adeguatamente conto della maggiore complessità ed eterogeneità che viene oggi a caratterizzare l'organizzazione della vita familiare, alla luce delle trasformazioni che hanno investito sia le modalità di svolgimento delle prestazioni lavorative, grazie anche alle nuove tecnologie, sia i sistemi di trasporto (si veda<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/209/!main"> la sentenza n. 209 del </ref>2022).
 4.6.- Proprio in virtù di tali trasformazioni, la disposizione censurata, nel consentire l'assegnazione temporanea del dipendente pubblico solo ad una sede che si trova nella provincia o regione in cui lavora l'altro genitore, non assicura una tutela adeguata in favore di quei nuclei familiari in cui entrambi i genitori lavorano in regioni diverse da quelle in cui è stata fissata la residenza familiare: situazione che, nella realtà, è divenuta sempre meno rara.
 In relazione a tali casi, appare rispondente alla finalità dell'istituto consentire almeno a uno dei genitori di lavorare, sia pur nel primo triennio di vita del minore, in una sede che si trova nella regione o nella provincia in cui è stata fissata la residenza della famiglia e, quindi, in cui è domiciliato il minore (ai sensi dell'art. 45, comma secondo, <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">del codice ci</ref>vile).
 Un simile ampliamento dell'ambito di applicazione dell'istituto, oltre a risultare pienamente coerente con la finalità di protezione della famiglia e di sostegno all'infanzia, risponde anche all'esigenza di preservare la più ampia autonomia dei genitori rispetto alle scelte concernenti la concreta definizione dell'indirizzo familiare. Tale autonomia, infatti, mal si concilia con la fissazione, da parte del legislatore, di rigide e non ragionevoli limitazioni all'ottenimento di benefici che dovrebbero essere diretti a sostenere la genitorialità e, quindi, a promuovere la formazione delle nuove famiglie.
 4.7.- Va pertanto dichiarata l'illegittimità costituzionale della disposizione censurata, per contrasto co<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">n l'art. 3 C</ref>ost., nella parte in cui prevede che il trasferimento temporaneo del dipendente pubblico, con figli minori fino a tre anni di età, possa essere disposto «ad una sede di servizio ubicata nella stessa provincia o regione nella quale l'altro genitore esercita la propria attività lavorativa», anziché «ad una sede di servizio ubicata nella stessa provincia o regione nella quale è fissata la residenza della famiglia o nella quale l'altro genitore eserciti la propria attività lavorativa».
 5.- Sono assorbite le ulteriori questioni sollevate in riferimento a<mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_29">gli artt</ref>. <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_30">29</ref>, 3<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_31">0 e 31 C</ref></mref>ost.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'art. 42&amp;#727;bis, comma 1, <ref href="/akn/it/act/decretoLegislativo/stato/2001-03-26/151/!main">del decreto legislativo 26 marzo 2001, n.</ref> 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma d<ref href="/akn/it/act/legge/stato/2000-03-08/53/!main#art_15">ell'articolo 15 della legge 8 marzo 2000, n</ref>. 53), nella parte in cui prevede che il trasferimento temporaneo del dipendente pubblico, con figli minori fino a tre anni di età, possa essere disposto «ad una sede di servizio ubicata nella stessa provincia o regione nella quale l'altro genitore esercita la propria attività lavorativa», anziché «ad una sede di servizio ubicata nella stessa provincia o regione nella quale è fissata la residenza della famiglia o nella quale l'altro genitore eserciti la propria attività lavorativa».
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2024-04-16">16 aprile 2024</date>.
 F.to:
 <signature><judge refersTo="#augustoAntonioBarbera" as="#presidente">Augusto Antonio BARBERA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 Marco D'ALBERTI, Redattore
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2024-06-04">4 giugno 2024</date>
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
