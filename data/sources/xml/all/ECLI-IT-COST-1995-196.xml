<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>196</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:196</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/05/1995</data_decisione>
    <data_deposito>26/05/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 26 della  legge    &#13;
 30  aprile  1969, n. 153 (Revisione degli ordinamenti pensionistici e    &#13;
 norme in materia di sicurezza sociale), come modificato  dall'art.  3    &#13;
 del  decreto-legge  2  marzo  1974,  n. 30, convertito nella legge 16    &#13;
 aprile 1974, n. 114 (Conversione  in  legge,  con  modificazioni  del    &#13;
 decreto-legge   2  marzo  1974,  n.  30,  concernente  norme  per  il    &#13;
 miglioramento di alcuni trattamenti previdenziali ed assistenziali) e    &#13;
 dell'art. 14-septies, aggiunto al decreto-legge 30 dicembre 1979,  n.    &#13;
 663,  dalla legge di conversione 29 febbraio 1980, n. 33 (Conversione    &#13;
 in legge, con modificazioni, del decreto-legge 30 dicembre  1979,  n.    &#13;
 663,  concernente  provvedimenti  per  il  finanziamento del Servizio    &#13;
 sanitario nazionale nonché proroga  dei  contratti  stipulati  dalle    &#13;
 pubbliche  amministrazioni  in base alla legge 1 giugno 1977, n. 285,    &#13;
 sull'occupazione giovanile), promosso  con  ordinanza  emessa  il  26    &#13;
 ottobre  1994  dal  Pretore di Parma nel procedimento civile vertente    &#13;
 tra Balestrieri Maria e l'I.N.P.S., iscritta al n.  28  del  registro    &#13;
 ordinanze 1995 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 5, prima serie speciale, dell'anno 1995;                              &#13;
    Visti   gli   atti   di   costituzione   di  Balestrieri  Maria  e    &#13;
 dell'I.N.P.S.  nonché  l'atto  di  intervento  del  Presidente   del    &#13;
 Consiglio dei ministri;                                                  &#13;
    Udito  nell'udienza pubblica del 2 maggio 1995 il Giudice relatore    &#13;
 Cesare Ruperto;                                                          &#13;
    Uditi gli avv.ti Franco Agostini per Balestrieri Maria e Carlo  De    &#13;
 Angelis per l'I.N.P.S.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  corso  di  una controversia previdenziale vertente tra    &#13;
 Balestrieri  Maria  e  l'I.N.P.S.  per  l'erogazione  della  pensione    &#13;
 sociale   (negata  in  sede  amministrativa  poiché  la  ricorrente,    &#13;
 ultrasessantacinquenne dal 19 ottobre 1989, godeva di un reddito che,    &#13;
 cumulato con quello del marito, superava  il  limite  di  legge),  il    &#13;
 Pretore  di  Parma,  in funzione di giudice del lavoro, ha sollevato,    &#13;
 con ordinanza emessa il 26 ottobre 1994,  questione  di  legittimità    &#13;
 costituzionale,  in riferimento agli artt. 3 e 38, primo comma, della    &#13;
 Costituzione, dell'art.  26  della  legge  30  aprile  1969,  n.  153    &#13;
 (Revisione  degli  ordinamenti  pensionistici  e  norme in materia di    &#13;
 sicurezza sociale), come modificato dall'art. 3 del  decreto-legge  2    &#13;
 marzo  1974,  n.  30,  convertito  nella legge 16 aprile 1974, n. 114    &#13;
 (Conversione in legge, con modificazioni del  decreto-legge  2  marzo    &#13;
 1974,  n.  30,  concernente  norme  per  il  miglioramento  di alcuni    &#13;
 trattamenti previdenziali ed assistenziali) e  dell'art.  14-septies,    &#13;
 aggiunto  al  decreto-legge 30 dicembre 1979, n.  663, dalla legge di    &#13;
 conversione 29 febbraio  1980,  n.  33  (Conversione  in  legge,  con    &#13;
 modificazioni,   del   decreto-legge  30  dicembre  1979,  n.    663,    &#13;
 concernente provvedimenti per il finanziamento del Servizio sanitario    &#13;
 nazionale nonché proroga dei  contratti  stipulati  dalle  pubbliche    &#13;
 amministrazioni   in   base   alla  legge  1  giugno  1977,  n.  285,    &#13;
 sull'occupazione giovanile), nella parte  in  cui,  nell'indicare  il    &#13;
 limite  di  reddito  cumulato  con  quello  del  coniuge, ostativo al    &#13;
 conseguimento   della   pensione  sociale,  non  prevedono,  per  gli    &#13;
 ultrasessantacinquenni, il medesimo meccanismo di determinazione  del    &#13;
 reddito individuale stabilito per la concessione dell'assegno mensile    &#13;
 agli  invalidi parziali di cui all'art. 13 della legge 30 marzo 1971,    &#13;
 n.   118,   indipendentemente   dall'accertamento   della   effettiva    &#13;
 invalidità e solo in ragione dell'età.                                 &#13;
    Il  Pretore  a  quo  -  operata  una  ricognizione dell'evoluzione    &#13;
 giurisprudenziale della Corte costituzionale contenuta nelle sentenze    &#13;
 n. 769 del 7 luglio 1988 e n. 88 del 9 marzo 1992 - sostiene  che  se    &#13;
 si assume (conformemente a quanto affermato nella sentenza n. 769 del    &#13;
 1988) la sussistenza di una sostanziale equivalenza fra la condizione    &#13;
 di  inabilità  dell'infrasessantacinquenne  e  lo stato di vecchiaia    &#13;
 (pure presumibilmente invalidante) dell'ultrasessantacinquenne, se ne    &#13;
 deve dedurre che non hanno  ragion  d'essere  differenziazioni  nella    &#13;
 individuazione  delle  condizioni  di  bisogno  che  danno  titolo al    &#13;
 sostegno solidaristico della collettività; né  ha  senso  che  dopo    &#13;
 l'età   suddetta  (65  anni)  si  riconoscano  stati  di  inabilità    &#13;
 superiore ai due terzi, che essa  già  di  per  sé  presuntivamente    &#13;
 comporta.                                                                &#13;
    Se,  invece,  si vuol distinguere (come chiarito dalla sentenza n.    &#13;
 88 del 1992), fra gli anziani  ultrasessantacinquenni  e  coloro  che    &#13;
 siano   effettivamente   invalidi,   se  ne  dovrebbe  dedurre  -  in    &#13;
 applicazione  del  principio  di  uguaglianza,  ex   art.   3   della    &#13;
 Costituzione,  ed  al  fine  di superare la incoerenza di tale ultima    &#13;
 pronuncia - che se l'ultrasessantacinquenne diventa anche invalido (e    &#13;
 non solo per  l'età)  ha  diritto  sì  di  richiedere  la  pensione    &#13;
 sociale,  ma  alle  stesse  condizioni  reddituali  previste  per  la    &#13;
 pensione    sociale    sostitutiva,    in    favore     dell'invalido    &#13;
 ultrasessantacinquenne,  già  titolare  di assegno ex art. 13, della    &#13;
 legge n. 118 del 1971. E se così  è,  non  sembrerebbe  ammissibile    &#13;
 demandare al legislatore o addirittura al giudice di operare le nuove    &#13;
 determinazioni del reddito cumulato.                                     &#13;
    2.  -  È  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura dello Stato, che  ha  concluso    &#13;
 per  l'inammissibilità  o  l'infondatezza della questione, assumendo    &#13;
 che la stessa Corte costituzionale,  nel  contesto  della  richiamata    &#13;
 sentenza  n.  769  del 1988, ha espressamente affermato che spetta al    &#13;
 legislatore il compito di "realizzare l'omogeneizzazione,  a  parità    &#13;
 di  condizioni,  tra  i  livelli  reddituali idonei ad individuare lo    &#13;
 stato di bisogno, così come il porre mano all'opportuno  adeguamento    &#13;
 dei  livelli  di  prestazione";  laddove,  inoltre, "al paradigma del    &#13;
 principio  di  uguaglianza  non  si  può  fare  ricorso  quando   le    &#13;
 disposizioni di legge ordinaria, dalle quali si pretende di trarre il    &#13;
 tertium  comparationis,  si rivelino derogatorie rispetto alla regola    &#13;
 desumibile dal  sistema"  come  nel  caso  dell'art.  14-septies  del    &#13;
 decreto-legge  n.  663  del  1979,  secondo quanto riconosciuto dallo    &#13;
 stesso remittente.                                                       &#13;
    Afferma  inoltre  l'Avvocatura  dello  Stato  -  sul  punto  della    &#13;
 fondatezza  - come debba farsi esclusivo riferimento alla sentenza n.    &#13;
 88 del 1992 (peraltro ritenuta non  contrastante  con  la  precedente    &#13;
 sentenza  n.  769  del  1988), nella parte in cui pone in evidenza la    &#13;
 diversità di situazioni che possono rendere addirittura  illegittima    &#13;
 la mancata previsione di meccanismi differenziati di valutazione.        &#13;
    3.  -  Si  sono  costituite entrambe le parti private: l'I.N.P.S.,    &#13;
 concludendo   nel   senso   della   inammissibilità   o,   comunque,    &#13;
 dell'infondatezza  della questione, in base a considerazioni conformi    &#13;
 a  quelle  svolte  dall'Avvocatura  dello  Stato,   con   l'ulteriore    &#13;
 precisazione  che,  nella  fattispecie,  la ricorrente risulta essere    &#13;
 anziana  ma  non  invalida;  la  Balestrieri,  concludendo   per   la    &#13;
 dichiarazione  dell'illegittimità delle norme impugnate alla stregua    &#13;
 della  motivazione  contenuta   nell'ordinanza   di   remissione,   e    &#13;
 sottolineando - in una memoria depositata nell'imminenza dell'udienza    &#13;
 -  come  non  possa più essere ritenuto il carattere "derogatorio (e    &#13;
 temporaneo)" dello scostamento tra il limite di  reddito  individuale    &#13;
 previsto  dall'impugnato art. 14-septies del decreto-legge n. 663 del    &#13;
 1979 ai fini della concessione dell'assegno di invalidità  e  quello    &#13;
 fissato  per  la  pensione  sociale,  con conseguente utilizzabilità    &#13;
 della norma quale tertium comparationis, suscettibile  di  estensione    &#13;
 ai sensi degli artt. 3 e 38, primo comma, della Costituzione.<diritto>Considerato in diritto</diritto>1.  - Il Pretore di Parma ha sollevato questione di illegittimità    &#13;
 costituzionale dell'art. 26 della legge 30 aprile 1969, n. 153  (come    &#13;
 modificato  dall'art.  3  del  decreto-legge  2  marzo  1974,  n. 30,    &#13;
 convertito  nella  legge  16  aprile  1974,  n.  114)   e   dell'art.    &#13;
 14-septies, aggiunto al decreto-legge 30 dicembre 1979, n. 663, dalla    &#13;
 legge  29  febbraio 1980, n. 33, nella parte in cui, nell'indicare il    &#13;
 limite di reddito  cumulato  con  quello  del  coniuge,  ostativo  al    &#13;
 conseguimento   della   pensione  sociale,  non  prevedono,  per  gli    &#13;
 ultrasessantacinquenni, il medesimo meccanismo di determinazione  del    &#13;
 reddito individuale stabilito per la concessione dell'assegno mensile    &#13;
 agli  invalidi parziali ex art. 13 della legge 30 marzo 1971, n. 118,    &#13;
 indipendentemente dall'accertamento dell'effettiva invalidità e solo    &#13;
 in ragione dell'età.                                                    &#13;
   Ad avviso  del  remittente,  le  norme  de  quibus  si  pongono  in    &#13;
 contrasto  con  gli artt. 3 e 38, primo comma, della Costituzione, in    &#13;
 quanto - attesa la sostanziale equivalenza fra lo stato d'invalidità    &#13;
 parziale   dell'infrasessantacinquenne   e   quello   di    vecchiaia    &#13;
 conseguente  al  superamento  del  sessantacinquesimo  anno di età -    &#13;
 appare  irragionevole  la  previsione   di   differenti   regimi   di    &#13;
 individuazione  delle  condizioni  di  bisogno  che  danno  titolo al    &#13;
 sostegno solidaristico della collettività.                              &#13;
     All'evidente scopo di rafforzare tale conclusione, il  giudice  a    &#13;
 quo    prospetta    l'alternativa   conseguenziale   alla   eventuale    &#13;
 riaffermazione della necessità di un differente regime giuridico tra    &#13;
 gli ultrasessantacinquenni solamente anziani e coloro i  quali  siano    &#13;
 effettivamente  invalidi (secondo quanto ritenuto da questa Corte con    &#13;
 la sentenza n. 88 del 1992, che ha  dichiarato  l'incostituzionalità    &#13;
 dell'art.  26 della legge n. 153 del 1969 e successive modificazioni,    &#13;
 nella parte in cui, nell'indicare il limite di reddito  cumulato  con    &#13;
 quello del coniuge, ostativo al conseguimento della pensione sociale,    &#13;
 non  prevede  un  meccanismo  differenziato di determinazione per gli    &#13;
 ultrasessantacinquenni divenuti invalidi). E sostiene come da codesta    &#13;
 distinzione  si  dovrebbe  dedurre,  proprio  in   applicazione   del    &#13;
 principio di uguaglianza, sancito dall'art. 3 della Costituzione, che    &#13;
 se  l'ultrasessantacinquenne  diventa  anche invalido (e non solo per    &#13;
 l'età) ha diritto sì di richiedere la  pensione  sociale,  ma  alle    &#13;
 stesse   condizioni  reddituali  previste  per  la  pensione  sociale    &#13;
 sostitutiva  in  favore  dell'invalido  ultrasessantacinquenne   già    &#13;
 titolare  di  assegno  ex  art.  13  della  legge  n.  118  del 1971:    &#13;
 altrimenti risultando viziato di incoerenza il sistema normativo.        &#13;
    2.  -  Va  anzitutto  rilevato  che  tale  ultima   prospettazione    &#13;
 alternativa  risulta  estranea  rispetto alla fattispecie oggetto del    &#13;
 giudizio a quo e dunque - giusta l'eccezione sollevata dal costituito    &#13;
 I.N.P.S. - essa deve ritenersi priva della necessaria  rilevanza;  in    &#13;
 merito  alla  quale,  d'altronde,  il  remittente  non  offre  alcuna    &#13;
 motivazione.                                                             &#13;
    Pertanto, l'esame della Corte va limitato al quesito se  le  norme    &#13;
 denunziate  siano  da ritenersi costituzionalmente illegittime - come    &#13;
 sospetta il giudice a quo - "nella parte  in  cui,  nell'indicare  il    &#13;
 limite  di  reddito  cumulato  con  quello  del  coniuge, ostativo al    &#13;
 conseguimento  della  pensione  sociale,  non  prevedono,   per   gli    &#13;
 ultrassessantacinquenni, il medesimo meccanismo di determinazione del    &#13;
 reddito individuale stabilito per la concessione dell'assegno mensile    &#13;
 agli  invalidi parziali ex art. 13 della legge 30 marzo 1971, n. 118,    &#13;
 indipendentemente dall'accertamento dell'effettiva invalidità e solo    &#13;
 in ragione dell'età".                                                   &#13;
    3. - La questione non è fondata.                                     &#13;
    Già investita del vaglio  di  costituzionalità  delle  norme  in    &#13;
 esame,  questa  Corte  ha  messo in luce - con la sentenza n. 769 del    &#13;
 1988, richiamata nell'ordinanza di  remissione  -  la  necessità  di    &#13;
 porre  rimedio,  nel  sistema dei requisiti reddituali fissati per il    &#13;
 conseguimento, rispettivamente,  dei  trattamenti  di  invalidità  e    &#13;
 della  pensione  sociale,  al difetto di coerenza provocato dall'art.    &#13;
 14-septies , anche ora in esame, a cagione sia del divario venutosi a    &#13;
 determinare  riguardo  ai  limiti  di  reddito   stabiliti   per   il    &#13;
 conseguimento  del  richiesto  trattamento,  sia  dell'esclusione nel    &#13;
 relativo calcolo, rispetto ai soli invalidi civili, del cumulo con  i    &#13;
 redditi   del   coniuge.   Tuttavia   la   Corte  ha,  nel  contempo,    &#13;
 ripetutamente affermato  come  siffatta  opera  vada  effettuata  dal    &#13;
 legislatore,  poiché  essa  implica  necessariamente  una  complessa    &#13;
 riconsiderazione dell'intero sistema  nei  suoi  molteplici  aspetti,    &#13;
 mediante  l'elaborazione  di  criteri di determinazione dei limiti di    &#13;
 reddito capaci di riportare i vari trattamenti  ad  omogeneità  (che    &#13;
 non  si  riduca  ad  una  mera  parificazione  di  singole previsioni    &#13;
 legislative), nella quale gli apporti di chi abbia  specifici  doveri    &#13;
 solidaristici e quello della collettività (presenti in una relazione    &#13;
 di integrazione reciproca), trovino un idoneo bilanciamento, a fronte    &#13;
 delle  specifiche  esigenze  del  soggetto  bisognoso,  derivanti,  a    &#13;
 seconda dei casi,  unicamente  dall'età  ovvero  da  una  inabilità    &#13;
 totale o parziale, pregressa o sopravvenuta (v. in particolare, oltre    &#13;
 alla sentenza n. 88 del 1992, anche le sentenze nn. 75 del 1991 e 286    &#13;
 del 1990).                                                               &#13;
    Orbene,   va   rilevato   in   questa  sede  che,  alle  reiterate    &#13;
 sollecitazioni  della  Corte,  il   legislatore   ha   risposto   con    &#13;
 l'emanazione  dell'art.  12 della legge 30 dicembre 1991, n. 412, che    &#13;
 ha totalmente assimilato, a decorrere dal 1 gennaio 1992,  il  limite    &#13;
 di  reddito  individuale  valevole  per  la  concessione dell'assegno    &#13;
 mensile di invalidità parziale e quello stabilito  per  la  pensione    &#13;
 sociale.                                                                 &#13;
    Vero  è,  peraltro, che il cumulo viene ancora mantenuto solo per    &#13;
 gli aspiranti alla pensione sociale diretta. Tuttavia, pur esprimendo    &#13;
 l'auspicio che anche con riguardo a tale punto il legislatore proceda    &#13;
 sulla via della sopra indicata omogeneizzazione dei  sistemi,  questa    &#13;
 Corte  non  può  non  escludere  che gli inconvenienti lamentati dal    &#13;
 giudice a quo si  risolvano  nel  denunciato  vizio  di  legittimità    &#13;
 costituzionale.                                                          &#13;
    Infatti,  anche  nella  sostenuta  tendenziale  indistinguibilità    &#13;
 (quale condizione di  fatto  finale)  fra  lo  stato  di  invalidità    &#13;
 parziale  e  quello  di  vecchiaia, non può certo ritenersi priva di    &#13;
 rilievo, onde razionalmente valutare gli elementi  significativi  per    &#13;
 la  determinazione  dello  stato  di  bisogno, l'esistenza o meno nel    &#13;
 soggetto, prima del compimento del sessantacinquesimo anno  di  età,    &#13;
 di  un  impedimento  derivante  da  pregresse patologie alla completa    &#13;
 idoneità a procurarsi, attraverso la propria  attività  lavorativa,    &#13;
 il   reddito   sufficiente  per  vivere  nonché  la  piena  garanzia    &#13;
 previdenziale per la vecchiaia.  Situazione,  codesta,  evidentemente    &#13;
 implicante  maggiori costi umani ed economici a carico del soggetto e    &#13;
 della sua famiglia,  "rispetto  a  quella  di  colui  la  cui  storia    &#13;
 personale  non  ha  risentito  di  un  simile  impedimento"  (come si    &#13;
 osserva, per un'ipotesi diversa ma parallela, nella  sentenza  n.  88    &#13;
 del  1992);  così  da indurre non irragionevolmente il legislatore a    &#13;
 non  trasferire,  in  tali  casi,  dall'àmbito  familiare  a  quello    &#13;
 statuale  i  relativi  doveri  assistenziali ex art. 38, primo comma,    &#13;
 della Costituzione.                                                      &#13;
    Per cui, la  lamentata  disparità  di  trattamento  -  in  quanto    &#13;
 riferita a situazioni fondate su differenti presupposti fattuali, che    &#13;
 per   ciò   stesso  possono  giustificare  la  discrezionale  scelta    &#13;
 (ancorché essa non costituisca  l'unica  opzione  costituzionalmente    &#13;
 legittima) di ricorrere rispettivamente alla solidarietà del coniuge    &#13;
 ovvero  a  quella  della  collettività  - neppure risulta lesiva del    &#13;
 principio di uguaglianza.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art. 26 della legge 30 aprile 1969, n. 153                          &#13;
 (Revisione  degli  ordinamenti  pensionistici  e  norme in materia di    &#13;
 sicurezza sociale), come modificato dall'art. 3  del  d.-l.  2  marzo    &#13;
 1974,  n.  30,  convertito  nella  legge  16  aprile  1974,  n.   114    &#13;
 (Conversione in legge, con modificazioni del d.-l. 2 marzo  1974,  n.    &#13;
 30,  concernente  norme  per  il  miglioramento di alcuni trattamenti    &#13;
 previdenziali ed assistenziali) e dell'art. 14-septies del  d.-l.  30    &#13;
 dicembre  1979,  n. 663, introdotto con la legge 29 febbraio 1980, n.    &#13;
 33 (Conversione in legge, con modificazioni, del  d.-l.  30  dicembre    &#13;
 1979,  n.  663,  concernente  provvedimenti  per il finanziamento del    &#13;
 Servizio sanitario nazionale nonché proroga dei contratti  stipulati    &#13;
 dalle  pubbliche amministrazioni in base alla legge 1 giugno 1977, n.    &#13;
 285,  sull'occupazionegiovanile),  sollevata,  con  riferimento  agli    &#13;
 artt.  3 e 38, primo comma, della Costituzione, dal Pretore di Parma,    &#13;
 con ordinanza emessa il 26 ottobre 1994.                                 &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 maggio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: RUPERTO                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 maggio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
