<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>147</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:147</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/06/1973</data_decisione>
    <data_deposito>18/07/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Dott. &#13;
 LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. &#13;
 ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Prof. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 137  e  138  &#13;
 del  codice  penale,  promosso  con  ordinanza emessa il 21 luglio 1971  &#13;
 dalla  sezione  istruttoria  della  Corte  d'appello  di  Palermo   nel  &#13;
 procedimento  penale  a carico di La Mattina Angelo, iscritta al n. 329  &#13;
 del registro ordinanze 1971 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 290 del 17 novembre 1971.                                  &#13;
     Udito  nella  camera  di  consiglio  del  14 giugno 1973 il Giudice  &#13;
 relatore Ercole Rocchetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     In data 7 settembre 1957, il cittadino italiano Angelo  La  Mattina  &#13;
 veniva  arrestato in Nuova Zelanda per rispondere del reato di omicidio  &#13;
 volontario a scopo di rapina commesso a Wellington nei confronti di  un  &#13;
 connazionale.    Condannato in data 2 dicembre 1957 alla pena capitale,  &#13;
 successivamente commutata in ergastolo, il La Mattina, dopo dieci  anni  &#13;
 di detenzione veniva graziato e ricondotto in Italia, dove su ordine di  &#13;
 cattura  per  il  medesimo reato, il 28 maggio 1968 veniva arrestato ai  &#13;
 sensi dell'art. 11 del codice penale. Con sentenza istruttoria  del  25  &#13;
 marzo  1970 egli veniva rinviato a giudizio per rispondere, in stato di  &#13;
 custodia preventiva, del delitto di omicidio volontario e di rapina.     &#13;
     Su istanza presentata il  12  maggio  1970  dal  difensore  del  La  &#13;
 Mattina,   diretta  ad  ottenere  la  scarcerazione  dell'imputato  per  &#13;
 decorrenza dei termini di custodia preventiva, la  sezione  istruttoria  &#13;
 della  Corte  d'appello  di  Palermo,  con  ordinanza  9  giugno  1970,  &#13;
 disponeva la scarcerazione dell'imputato sulla base del rilievo che  la  &#13;
 sentenza  di  rinvio  a  giudizio  era  stata  depositata  dopo che era  &#13;
 interamente decorso il termine di due anni previsto per la carcerazione  &#13;
 preventiva, in quanto, alla carcerazione preventiva sofferta in  Italia  &#13;
 (dal  28  maggio  1968  al  25  marzo  1970)  doveva aggiungersi quella  &#13;
 scontata in Nuova Zelanda dal 7 settembre al 2 dicembre 1957.            &#13;
     In seguito a ricorso promosso dal pubblico ministero, la  Corte  di  &#13;
 cassazione   con   sentenza  30  marzo  1971,  annullava  senza  rinvio  &#13;
 l'ordinanza della sezione istruttoria, ritenendo che nel termine legale  &#13;
 previsto per la carcerazione preventiva non potesse essere computato il  &#13;
 periodo di custodia preventiva sofferta dall'imputato in Nuova Zelanda.  &#13;
     Con ordinanza emessa il 21  luglio  1971,  la  sezione  istruttoria  &#13;
 della   Corte   d'appello   di   Palermo   riteneva   rilevante  e  non  &#13;
 manifestamente infondata la questione di  legittimità  costituzionale,  &#13;
 proposta  dal difensore dell'imputato, degli artt. 137 e 138 del codice  &#13;
 penale, nella parte in cui escludono la  incidenza  della  carcerazione  &#13;
 preventiva   sofferta   all'estero   dall'imputato  nel  computo  della  &#13;
 carcerazione preventiva sofferta  nello  Stato  per  lo  stesso  reato.  &#13;
 Secondo il giudice a quo, le norme impugnate sarebbero in contrasto con  &#13;
 gli   artt.  2,  3  e  13  della  Costituzione,  "relativi  ai  diritti  &#13;
 inviolabili dell'uomo e di eguaglianza di  tutti  i  cittadini  davanti  &#13;
 alla  legge,  nonché  alle  disposizoni  che  regolano  i limiti della  &#13;
 carcerazione preventiva".                                                &#13;
     Nel giudizio innanzi a questa Corte non vi è stata Costituzione di  &#13;
 parti, né intervento della Presidenza del  Consiglio  dei  ministri  e  &#13;
 pertanto  la  causa  viene  decisa  in  camera  di  consiglio  ai sensi  &#13;
 dell'art. 9 delle Norme integrative.<diritto>Considerato in diritto</diritto>:                          &#13;
     Viene proposta alla  Corte  questione  di  costituzionalità  degli  &#13;
 artt.  137  e 138 del codice penale, dal cui combinato disposto risulta  &#13;
 che, quando nei confronti  di  un  cittadino  o  di  uno  straniero  il  &#13;
 giudizio  penale  seguito all'estero è rinnovato nello Stato, si tiene  &#13;
 conto della carcerazione preventiva subita all'estero, ma solo ai  fini  &#13;
 dello  scomputo  della pena e - come deve intendersi nel silenzio della  &#13;
 legge - non anche a  quelli  della  durata  della  stessa  carcerazione  &#13;
 preventiva   cui  l'imputato  viene  sottoposto  nello  Stato  e  della  &#13;
 decorrenza del connesso termine per la scarcerazione automatica.         &#13;
     Secondo  il  giudice  a quo, la mancata previsione legislativa, che  &#13;
 non consente di considerare e  computare  ai  fini  della  carcerazione  &#13;
 preventiva  da  subire  nello Stato quella cui l'imputato è già stato  &#13;
 sottoposto  all'estero,  violerebbe  gli  artt.  2,  3   e   13   della  &#13;
 Costituzione.                                                            &#13;
     La questione non è fondata.                                         &#13;
     1. - La carcerazione preventiva ha scopi essenzialmente connessi al  &#13;
 processo  e  natura prevalentemente cautelare (sentenze nn. 64 e 96 del  &#13;
 1970). Essa è perciò  limitata,  nei  suoi  effetti,  all'ambito  del  &#13;
 processo  nel  corso  del  quale  è  stata  disposta,  sì  che non è  &#13;
 immaginabile che possa assolvere alla funzione che le  è  propria  una  &#13;
 precedente  carcerazione  preventiva  subita dall'imputato, benché per  &#13;
 gli stessi fatti, ma in altro processo celebrato all'estero  e  secondo  &#13;
 le norme di un altro ordinamento giuridico.                              &#13;
     2.  -  Perciò,  nel giudizio che contro l'imputato viene rinnovato  &#13;
 nello Stato, la carcerazione da lui subita all'estero, benché  produca  &#13;
 effetti  ai  fini  della  durata  della  pena  dalla  quale deve essere  &#13;
 scomputata, non ne produce alcuno rispetto alla carcerazione preventiva  &#13;
 cui egli, nel nuovo giudizio debba essere  sottoposto.  Essa  non  può  &#13;
 pertanto  né  escluderne l'attuazione né influire sul termine massimo  &#13;
 di durata.                                                               &#13;
     La differenza di efficacia nei due casi non viola il  principio  di  &#13;
 eguaglianza, stante la diversità dei loro presupposti.                  &#13;
     Infatti,   la   carcerazione   preventiva,  come  parte,  anche  se  &#13;
 anticipata, della pena, ha lo stesso contenuto afflittivo,  comunque  e  &#13;
 dovunque  sofferta,  mentre  la  cautela  processuale  che  in  essa si  &#13;
 estrinseca ha  scopi  propri  in  ciascuno  dei  due  processi,  quello  &#13;
 svoltosi all'estero e quello che si rinnova nello Stato.                 &#13;
     Sotto  questo  profilo  le due situazioni giuridiche sono diverse e  &#13;
 perciò non si configura, nel caso, la denunziata violazione  dell'art.  &#13;
 3, comma primo, della Costituzione.                                      &#13;
     3.  - Né ha consistenza la pur dedotta violazione dell'articolo 2,  &#13;
 rispetto alla quale ogni considerazione può essere omessa, perché  è  &#13;
 stata   proposta   censura  anche  in  riferimento  all'art.  13  della  &#13;
 Costituzione, più specificatamente pertinente nella materia in  esame,  &#13;
 in  cui  si discute del diritto del cittadino alla libertà personale e  &#13;
 del limite massimo della carcerazione preventiva.                        &#13;
     4. - Ma anche la questione così posta, in riferimento  alla  norma  &#13;
 dell'art. 13, comma quinto, è da ritenersi non fondata.                 &#13;
     Deve  osservarsi  in  proposito che, solo basandosi su un equivoco,  &#13;
 può dirsi che, nel caso, non sarebbe posto dalla legge un limite o che  &#13;
 quello da essa imposto possa in fatto venir superato.                    &#13;
     E l'equivoco deriva dalla convinzione del  giudice  a  quo  che  la  &#13;
 durata  della carcerazione preventiva sofferta dall'imputato all'estero  &#13;
 possa o debba sommarsi con la durata di quella cui  egli  nel  processo  &#13;
 contro di lui rinnovato venga poi sottoposto per soddisfare le esigenze  &#13;
 di cautela processuale che le sono proprie.                              &#13;
     Ora  una  tale  operazione  non  è concepibilie tra due periodi di  &#13;
 carcerazione preventiva che non sono fra  loro  fungibili,  in  quanto,  &#13;
 secondo si è già detto, ognuno di essi è volto a soddisfare esigenze  &#13;
 proprie  in  ciascuno  dei due processi e non idoneo perciò a spiegare  &#13;
 alcun effetto nell'altro.                                                &#13;
     La reiterazione della misura cautelare nel processo che si  rinnova  &#13;
 è  una  conseguenza naturale e ineliminabile della stessa rinnovazione  &#13;
 del  processo,  prevista  in  norme  del  codice   penale   della   cui  &#13;
 legittimità  costituzionale  non  si  dubita  e  rispetto  alle quali,  &#13;
 comunque, nessuna questione è stata posta nell'ordinanza di rinvio.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 137 e 138 del  codice  penale,  proposta,  con  l'ordinanza  &#13;
 indicata  in  epigrafe,  in riferimento agli artt. 2, 3, comma primo, e  &#13;
 13, comma quinto, della Costituzione.                                    &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 28 giugno 1973.         &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
