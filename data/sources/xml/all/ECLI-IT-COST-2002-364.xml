<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>364</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:364</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>10/07/2002</data_decisione>
    <data_deposito>18/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale degli artt. da 74 a 88, &#13;
da  90  a 95, 154, 187, comma 3, 441, commi 2 e 3, 444, comma 2, 451, &#13;
comma  3, 491, 505, da 538 a 541, 543 del codice di procedura penale, &#13;
promosso con ordinanza emessa il 13 luglio 2001 dal Tribunale di Roma &#13;
nel  procedimento  penale  a  carico  di D.M.A. ed altra, iscritta al &#13;
n. 883  del  registro  ordinanze  2001  e  pubblicata  nella Gazzetta &#13;
Ufficiale della Repubblica n. 44, 1ª serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 22 maggio 2002 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto   che,  con  ordinanza  emessa  il  13 luglio  2001,  il &#13;
Tribunale  di  Roma  -  chiamato  a  pronunciarsi  sulla richiesta di &#13;
esclusione  della  parte  civile,  formulata dal pubblico ministero e &#13;
dagli  imputati  nel  corso  di  un  processo penale nei confronti di &#13;
persone  imputate  dei  reati  di lesioni personali e di detenzione e &#13;
porto  illecito  di armi - ha sollevato, in riferimento agli artt. 2, &#13;
3,   13   e   111   della  Costituzione,  questione  di  legittimità &#13;
costituzionale  degli artt. da 74 a 88, da 90 a 95, 154, 187 comma 3, &#13;
441, commi 2 e 3, 444, comma 2, 451, comma 3, 491, 505, da 538 a 541, &#13;
e  543  del  codice di procedura penale, nella parte in cui prevedono &#13;
"la  possibilità  di  azione civile delle parti private nel processo &#13;
penale"; &#13;
        che   ad   avviso   del   rimettente,   le   norme  impugnate &#13;
contrasterebbero  con  i  principi  del  "giusto  processo",  di  cui &#13;
all'art. 111  Cost.,  a fronte dei quali il processo deve tradursi in &#13;
un "duello ad armi e forze pari"; &#13;
        che  l'intervento nel processo penale di una parte ulteriore, &#13;
che   mira   a   realizzare   "interessi   morali   e   civilistici", &#13;
provocherebbe,  infatti,  uno  "sbilanciamento" a favore dell'accusa, &#13;
potendo  generare  nel  giudice una "pressione inconscia" - correlata &#13;
all'aspirazione a rendere comunque giustizia alla vittima del reato - &#13;
tale  da compromettere la serenità e la correttezza della decisione: &#13;
e ciò tanto più in un sistema processuale come quello italiano, che &#13;
ammette decisioni su base indiziaria e nel quale la valutazione della &#13;
convergenza  e  della forza degli indizi non si fonda "su un criterio &#13;
rigorosamente scientifico"; &#13;
        che  l'imparzialità  e  la  terzietà  del giudice sarebbero &#13;
assicurate  solo  da  un sistema rigorosamente "binario" di parti che &#13;
contendano  in  via  esclusiva sul tema della responsabilità penale: &#13;
tema  che,  nella  cornice  della  Costituzione,  assume  un  rilievo &#13;
prioritario  rispetto  alla  salvaguardia  dei  diritti  delle  parti &#13;
private,   in   quanto  incidente  sulla  libertà  fisica  e  morale &#13;
dell'individuo; &#13;
        che,  al  riguardo,  verrebbero  segnatamente  in rilievo gli &#13;
artt. 2  e  13  Cost.,  e  soprattutto  l'art. 3 Cost., che impone di &#13;
rimuovere  gli  ostacoli  che  di  fatto  limitano  la  libertà  dei &#13;
cittadini,  tutti  avendo  poi  l'eguale  diritto,  ove  imputati, di &#13;
accedere ad un processo rapido e ad armi pari con l'accusa; &#13;
        che  l'inserimento  dell'azione civile in un processo di tipo &#13;
accusatorio  -  introducendo  un  nuovo  thema  decidendum, ampliando &#13;
l'ambito  delle  prove  e  rendendo  necessari  ulteriori adempimenti &#13;
processuali  (quali notifiche ed avvisi) - comprometterebbe, inoltre, &#13;
i  principi  di  economia,  concentrazione  e  celerità del processo &#13;
stesso; &#13;
        che  nel  giudizio  di  costituzionalità  è  intervenuto il &#13;
Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso &#13;
dall'Avvocatura  generale  dello  Stato,  il  quale ha chiesto che la &#13;
questione sia dichiarata inammissibile o infondata. &#13;
    Considerato  che il tribunale rimettente sottopone a scrutinio di &#13;
costituzionalità  trentatré articoli del codice di procedura penale &#13;
di contenuto eterogeneo; &#13;
        che,  in  prevalenza,  si  tratta di disposizioni concernenti &#13;
l'esercizio  dell'azione civile nel processo penale: disposizioni che &#13;
peraltro,  da  un  lato,  si  riferiscono  anche  a  profili privi di &#13;
qualsiasi attinenza con la decisione che il rimettente è chiamato ad &#13;
assumere nel giudizio a quo; e, dall'altro lato, non coprono l'intero &#13;
ventaglio  delle  disposizioni  del  codice  di  rito  concernenti la &#13;
predetta azione; &#13;
        che   tra   le  norme  impugnate  figurano,  tuttavia,  anche &#13;
disposizioni  che non riguardano affatto la tematica dianzi indicata, &#13;
ma  ineriscono  in via esclusiva alla persona offesa dal reato e agli &#13;
enti  ed  associazioni  rappresentativi  di  interessi lesi dal reato &#13;
(artt. da  90  a  95,  e  505 cod. proc. pen.): soggetti, questi, ben &#13;
distinti, nella sistematica del codice, dalla parte civile; &#13;
        che, pertanto, non è dato ravvisare, tra le norme impugnate, &#13;
quella  reciproca,  intima  connessione che sola consente, secondo la &#13;
costante   giurisprudenza  di  questa  Corte,  di  coinvolgere  nello &#13;
scrutinio  di  costituzionalità un intero complesso normativo (cfr., &#13;
ex  plurimis,  sentenza  n. 156  del 2001; ordinanze nn. 81 e 286 del &#13;
2001); &#13;
        che    d'altra    parte   -   come   correttamente   rilevato &#13;
dall'Avvocatura  dello  Stato  -  il  quesito di costituzionalità si &#13;
risolve,  nella  sostanza, in una mera critica, a livello di politica &#13;
giudiziaria, di una scelta "di sistema" (quella del possibile cumulo) &#13;
operata  dal  legislatore, nell'esercizio della sua discrezionalità, &#13;
in  tema  di  rapporti  fra azione civile e azione penale relative al &#13;
medesimo fatto: scelta che il rimettente vorrebbe veder sostituita da &#13;
una  soluzione  di tipo diverso, in assunto preferibile (quella della &#13;
separazione assoluta); &#13;
        che,  sotto  entrambi  i  profili,  la  questione deve essere &#13;
dunque dichiarata manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale degli artt. da 74 a 88, da 90 a 95, 154, &#13;
187  comma 3, 441, commi 2 e 3, 444, comma 2, 451, comma 3, 491, 505, &#13;
da  538  a  541,  e 543 del codice di procedura penale, sollevata, in &#13;
riferimento  agli  artt. 2,  3,  13  e  111  della  Costituzione, dal &#13;
Tribunale di Roma con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 18 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
