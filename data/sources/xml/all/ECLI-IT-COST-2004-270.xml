<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>270</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:270</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ZAGREBELSKY</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>08/07/2004</data_decisione>
    <data_deposito>23/07/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della deliberazione della Camera dei deputati del 30 gennaio 2003 relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dall'onorevole Alessio Butti nei confronti di Roberto Zaccaria e Vittorio Emiliani, promosso dal Tribunale di Roma, sezione dei giudici per le indagini preliminari e dell'udienza preliminare, con ricorso depositato il 19 aprile 2003 ed iscritto al n. 242 del registro ammissibilità conflitti. &#13;
    Udito nella camera di consiglio del 9 giugno 2004 il Giudice relatore Francesco Amirante. &#13;
    Ritenuto che, con ordinanza del 10 aprile 2003, il Giudice dell'udienza preliminare del Tribunale di Roma ha promosso conflitto di attribuzione tra poteri dello Stato, nei confronti della Camera dei deputati, in relazione alla delibera adottata il 30 gennaio 2003 con la quale - in conformità alla proposta della Giunta per le autorizzazioni a procedere - è stato dichiarato che i fatti per i quali il deputato Alessio Butti è sottoposto a procedimento penale per il delitto di diffamazione a mezzo stampa riguardano opinioni espresse da quest'ultimo nell'esercizio delle sue funzioni parlamentari e sono, quindi, insindacabili ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    che il procedimento penale ha preso avvio da una querela sporta nei confronti del suddetto deputato, in data 23 gennaio 2002, da Roberto Zaccaria e Vittorio Emiliani, all'epoca rispettivamente presidente e componente del consiglio di amministrazione della RAI radiotelevisione italiana s.p.a., in conseguenza delle seguenti dichiarazioni del deputato Butti, riportate dalle agenzie di stampa ANSA ed AGI il precedente 30 ottobre 2001: «Si dovrà verificare per quale motivo Zaccaria, Emiliani e compagni volevano concludere un accordo penalizzante per gli interessi della RAI. Ma soprattutto perché, all'indomani della presa d'atto negativa del Governo, gli stessi personaggi si stracciano le vesti per la mancata vendita di Raiway: non vorremmo che con la vignetta di sabato sul Foglio (gli americani che riconsegnano le antenne si chiedono se devono rendere anche le mazzette) Vincino abbia, involontariamente e con il sorriso sulle labbra, rappresentato la verità o qualcosa che si avvicina ad essa»; &#13;
    che, instaurato il procedimento per il delitto di diffamazione aggravata, la Camera dei deputati, con la delibera oggetto del conflitto, ha stabilito che le dichiarazioni sopra riportate dovevano ritenersi rientranti nella prerogativa di cui all'art. 68, primo comma, Cost., e ciò perché nei giorni immediatamente successivi al rilascio delle medesime (sedute del 6, 14 e 21 novembre 2001) si era svolto in seno alla Commissione parlamentare di vigilanza sul servizio pubblico radiotelevisivo un dibattito proprio sul “caso Raiway”; la stessa questione, inoltre, era stata oggetto di interrogazione da parte del deputato Rositani, appartenente al gruppo di AN come il deputato Butti; &#13;
    che il GUP del Tribunale di Roma osserva come la giurisprudenza di questa Corte sia ormai costante nel ritenere che la prerogativa in questione non possa coprire tutte le opinioni comunque espresse dal parlamentare nello svolgimento della sua attività politica, bensì vada ristretta a quelle che sono legate da nesso funzionale con l'attività di componente di una delle Camere del Parlamento (vengono citate le sentenze di questa Corte n. 10, n. 11, n. 56, n. 58 e n. 321 del 2000, nonché la sentenza n. 79 del 2002), senza che si possa attribuire alcun rilievo agli atti parlamentari svolti in un momento successivo ai fatti oggetto dell'imputazione penale (sentenza n. 298 del 1998 di questa Corte); &#13;
    che nel caso specifico, invece, le dichiarazioni dell'onorevole Butti, benché inserite nel contesto di un vivace dibattito politico, sono state rese ad un'agenzia giornalistica, e quindi fuori dall'esercizio delle funzioni parlamentari tipiche; &#13;
    che, infatti, il dibattito avvenuto nella Commissione di vigilanza è successivo a tali dichiarazioni, né risulta che il deputato Butti abbia assunto iniziative parlamentari sull'argomento prima del rilascio delle medesime; &#13;
    che, pertanto, il GUP del Tribunale di Roma conclude nel senso che la delibera di insindacabilità opposta dalla Camera dei deputati si deve ritenere lesiva delle attribuzioni costituzionali dell'autorità giudiziaria e chiede pertanto alla Corte di dichiarare che non spetta alla Camera adottare una simile deliberazione, con conseguente annullamento della stessa. &#13;
    Considerato che in questa fase la Corte è chiamata, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a deliberare esclusivamente se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzione tra poteri dello Stato, impregiudicata rimanendo ogni definitiva decisione anche in ordine all'ammissibilità; &#13;
    che, quanto al requisito soggettivo, il Tribunale di Roma, sezione dei giudici per le indagini preliminari e dell'udienza preliminare, è legittimato a sollevare il conflitto, essendo competente a dichiarare definitivamente, in relazione al procedimento del quale è investito, la volontà del potere cui appartiene, in considerazione della posizione di indipendenza, costituzionalmente garantita, di cui godono i singoli organi giurisdizionali; &#13;
    che analogamente la Camera dei deputati, che ha deliberato l'insindacabilità delle opinioni espresse da un proprio membro, è legittimata ad essere parte del conflitto, in quanto organo competente a dichiarare definitivamente la volontà del potere che rappresenta;  &#13;
    che, per quanto riguarda il profilo oggettivo del conflitto, il ricorrente denuncia la menomazione della propria sfera di attribuzione, garantita da norme costituzionali, in conseguenza dell'adozione, da parte della Camera dei deputati, di una deliberazione ove si afferma, in modo asseritamente illegittimo, che le opinioni espresse da un proprio membro rientrano nell'esercizio delle funzioni parlamentari, in tal modo godendo della garanzia di insindacabilità stabilita dall'art. 68, primo comma, della Costituzione; &#13;
    che, pertanto, esiste la materia di un conflitto la cui risoluzione spetta alla competenza della Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dal Tribunale di Roma, sezione dei giudici per le indagini preliminari e dell'udienza preliminare, nei confronti della Camera dei deputati con l'atto introduttivo indicato in epigrafe; &#13;
    dispone:  &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al ricorrente Tribunale di Roma, sezione dei giudici per le indagini preliminari e dell'udienza preliminare;  &#13;
    b) che l'atto introduttivo e la presente ordinanza siano, a cura del ricorrente, notificati alla Camera dei deputati entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere poi depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di venti giorni previsto dall'art. 26, terzo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8  luglio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Francesco AMIRANTE, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 23 luglio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
