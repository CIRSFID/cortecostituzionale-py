<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>75</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:75</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Guglielmo Roehrssen</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/04/1982</data_decisione>
    <data_deposito>16/04/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi riuniti di legittimità costituzionale degli artt. 6, 14 e  &#13;
 15 del  d.P.R.  26  ottobre  1972,  n.  643  (Istituzione  dell'imposta  &#13;
 comunale  sull'incremento  di  valore degli immobili) e degli artt. 35,  &#13;
 comma secondo, e 39, comma primo, del d.P.R. 26 ottobre  1972,  n.  636  &#13;
 (Revisione della disciplina del contenzioso tributario) promossi con le  &#13;
 seguenti ordinanze:                                                      &#13;
     1)  ordinanza emessa il 16 giugno 1976 dalla Commissione tributaria  &#13;
 di primo grado di Perugia sul  ricorso  proposto  da  Martini  Bernardi  &#13;
 Bufalini  Carlo ed altri, iscritta al n. 61 del registro ordinanze 1977  &#13;
 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  94  del  6  &#13;
 aprile 1977;                                                             &#13;
     2)  ordinanza  emessa il 10 marzo 1977 dalla Commissione tributaria  &#13;
 di secondo grado di Latina sul ricorso  proposto  da  Capponi  Placido,  &#13;
 iscritta  al  n.  375  del  registro  ordinanze 1977 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 279 del 12 ottobre 1977;          &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 12 gennaio 1982 il Giudice relatore  &#13;
 Guglielmo Roehrssen;                                                     &#13;
     udito  l'avvocato  dello Stato Giovanni Albisinni per il Presidente  &#13;
 del Consiglio dei ministri.                                              &#13;
     Ritenuto  che  con  le  ordinanze  in  epigrafe,  aventi  contenuto  &#13;
 sostanzialmente identico, sono state sollevate:                          &#13;
     a)   dalla  Commissione  tributaria  di  primo  grado  di  Perugia,  &#13;
 questione di legittimità costituzionale, in riferimento agli artt.  3,  &#13;
 76  e  77  Cost., degli artt. 6, 14 e 15 del d.P.R. 26 ottobre 1972, n.  &#13;
 643 ("Istituzione dell'imposta comunale sull'incremento di valore degli  &#13;
 immobili"), nella parte in cui  nello  stabilire  che  l'incremento  di  &#13;
 valore  imponibile è costituito dalla differenza tra valore iniziale e  &#13;
 valore finale del bene calcolati  in  termini  monetari  nominali,  non  &#13;
 tiene conto della svalutazione monetaria;                                &#13;
     b)  dalla  stessa Commissione tributaria di primo grado di Perugia,  &#13;
 altra questione di legittimità  costituzionale,  in  riferimento  agli  &#13;
 artt.  3,  24, 76 e 77 Cost., degli artt. 35 e 39 del d.P.R. 26 ottobre  &#13;
 1972, n. 636 ("Revisione della disciplina del contenzioso tributario"),  &#13;
 in quanto attribuiscono alle Commissioni tributarie il potere-dovere di  &#13;
 acquisire elementi conoscitivi tecnici soltanto attraverso relazioni di  &#13;
 organi  tecnici  dell'Amministrazione  dello   Stato,   escludendo   la  &#13;
 consulenza  tecnica  a  mezzo  di  periti nominati ad hoc ed escludendo  &#13;
 altresì comunque la partecipazione dei difensori del contribuente  nel  &#13;
 corso delle indagini svolte dagli organi tecnici dello Stato;            &#13;
     c)  dalla  Commissione  tributaria  di  secondo  grado  di  Latina,  &#13;
 questione di legittimità costituzionale, in  riferimento  all'art.  53  &#13;
 Cost.,  degli artt. 6 e 14 del d.P.R. n. 643/1972 suddetto, laddove per  &#13;
 la determinazione della base imponibile ai fini  dell'INVIM  non  tiene  &#13;
 conto della svalutazione monetaria;                                      &#13;
     d)  dalla  stessa Commissione tributaria di secondo grado di Latina  &#13;
 altra questione di legittimità  costituzionale,  in  riferimento  agli  &#13;
 artt. 3 e 24 Cost., dell'art. 35 del d.P.R. n.  636/1972, citato, nella  &#13;
 parte  in  cui,  sancendo  l'obbligo  per  le Commissioni tributarie di  &#13;
 avvalersi per le indagini tecniche di organi dello Stato, violerebbe il  &#13;
 diritto di difesa ed il principio di eguaglianza.                        &#13;
     Considerato che questioni identiche a quelle sub a) e c) sono state  &#13;
 già  decise  dalla  Corte  con  sentenza  8  novembre  1979,  n.  126,  &#13;
 dichiarando la illegittimità costituzionale dell'art. 14 del d.P.R. 26  &#13;
 ottobre  1972,  n.  643, e dell'art. 8 della legge 16 dicembre 1977, n.  &#13;
 904 ("Modificazioni alla  disciplina  dell'imposta  sul  reddito  delle  &#13;
 persone giuridiche e al regime tributario dei dividendi e degli aumenti  &#13;
 di  capitale,  adeguamento  del capitale minimo delle società ed altre  &#13;
 norme in materia  fiscale  e  societaria"),  "nella  parte  in  cui  le  &#13;
 disposizioni   concernenti   il   calcolo   dell'incremento  di  valore  &#13;
 imponibile netto determinano - in relazione al  periodo  di  formazione  &#13;
 dell'incremento stesso - ingiustificata disparità di trattamento tra i  &#13;
 soggetti  passivi  del tributo", e dichiarando non fondate le questioni  &#13;
 di costituzionalità degli artt. 2, 4, 6, 7, 15  e  16  del  d.P.R.  26  &#13;
 ottobre  1972, n.  643, sollevate in riferimento agli artt. 3, 42, 47 e  &#13;
 53 della Costituzione; e che  nelle  ordinanze  in  epigrafe  non  sono  &#13;
 prospettati  profili  nuovi,  né sono stati addotti motivi che possano  &#13;
 indurre la Corte a modificare la propria giurisprudenza;                 &#13;
     che, peraltro, successivamente alla decisione n. 126 del  1979,  la  &#13;
 disciplina  normativa  dell'INVIM è stata modificata con decreto-legge  &#13;
 12 novembre 1979, n. 571, convertito con modificazioni nella  legge  12  &#13;
 gennaio  1980,  n. 2, la quale ha soppresso l'art. 14 del d.P.R. n. 643  &#13;
 del 1972, sostituito l'art.  15, e regolato le  misure  delle  aliquote  &#13;
 stabilite per gli anni 1979 e 1980 ai sensi dell'art. 16, statuendo che  &#13;
 le  nuove disposizioni si applicano anche ai rapporti sorti prima della  &#13;
 loro entrata in vigore ed a tale data non ancora definiti, "per i quali  &#13;
 tuttavia l'ammontare dell'imposta dovuta non può in ogni caso superare  &#13;
 quello    determinabile   con   i   criteri   contenuti   nelle   norme  &#13;
 precedentemente in vigore" (art. 3);                                     &#13;
     considerato inoltre, in ordine alle questioni  sub  b)  e  d),  che  &#13;
 successivamente  alle  ordinanze  di  rimessione l'art. 23 del d.P.R. 3  &#13;
 novembre 1981, n. 739 ("Norme integrative e correttive del decreto  del  &#13;
 Presidente  della  Repubbilca  26  ottobre 1972, n. 636, concernente la  &#13;
 revisione del  contenzioso  tributario")  ha  integralmente  sostituito  &#13;
 l'art.  35 del d.P.R. n. 636 del 1972 ed ha dato una diversa disciplina  &#13;
 tanto  alla  acquisizione  di  ufficio  da  parte   delle   Commissioni  &#13;
 tributarie dei necessari elementi conoscitivi tecnici (di cui tratta in  &#13;
 particolare  l'art.  35)  quanto  alla  possibilità  di  nomina  di un  &#13;
 consulente tecnico (di cui si occupava l'art. 39 del d.P.R. n. 636  del  &#13;
 1972);                                                                   &#13;
     che,  conseguentemente,  si  ravvisa  la  necessità di disporre la  &#13;
 restituzione degli  atti  alle  Commissioni  tributarie  sopraindicate,  &#13;
 perché  accertino  se, ed in qual misura, le questioni sollevate siano  &#13;
 tuttora rilevanti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina la  restituzione  degli  atti  alle  Commissioni  tributarie  &#13;
 indicate in epigrafe.                                                    &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 2 aprile 1982.                                &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   - GUGLIELMO ROEHRSSEN - ORONZO  REALE  &#13;
                                   -  BRUNETTO BUCCIARELLI DUCCI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO SAJA.     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
