<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2020</anno_pronuncia>
    <numero_pronuncia>242</numero_pronuncia>
    <ecli>ECLI:IT:COST:2020:242</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MORELLI</presidente>
    <relatore_pronuncia>Nicolò Zanon</relatore_pronuncia>
    <redattore_pronuncia>Nicolò Zanon</redattore_pronuncia>
    <data_decisione>22/10/2020</data_decisione>
    <data_deposito>20/11/2020</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>altro</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Mario Rosario MORELLI; Giudici : Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANO', Luca ANTONINI, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 4, commi 1 e 2, e 13 della legge della Regione Siciliana 19 luglio 2019, n. 13 (Collegato al DDL n. 476 'Disposizioni programmatiche e correttive per l'anno 2019. Legge di stabilità regionale'), promosso dal Presidente del Consiglio dei ministri con ricorso notificato a mezzo di posta elettronica certificata il 23 settembre 2019, depositato in cancelleria il 25 settembre 2019, iscritto al n. 99 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 44, prima serie speciale, dell'anno 2019.&#13;
 Visti l'atto di costituzione in giudizio della Regione Siciliana e l'atto di intervento dell'associazione denominata ANCE Sicilia - Collegio Regionale dei Costruttori Edili Siciliani; &#13;
 udito nell'udienza pubblica del 21 ottobre 2020 il Presidente della Corte costituzionale Mario Rosario Morelli, in luogo e con l'assenso del Giudice relatore Nicolò Zanon;&#13;
 uditi l'avvocato dello Stato Ettore Figliolia e l'avvocato Marina Valli per la Regione Siciliana; &#13;
 deliberato nella camera di consiglio del 22 ottobre 2020.&#13;
 Ritenuto che il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, con ricorso notificato a mezzo di posta elettronica certificata in data 23 settembre 2019 e depositato il 25 settembre 2019 (reg. ric. n. 99 del 2019), ha impugnato, tra gli altri, gli artt. 4, commi 1 e 2, e 13 della legge della Regione Siciliana 19 luglio 2019, n. 13 (Collegato al DDL n. 476 'Disposizioni programmatiche e correttive per l'anno 2019. Legge di stabilità regionale');&#13;
 che nel giudizio così instaurato, con atto depositato in data 28 ottobre 2019, la Regione Siciliana si è costituita al solo scopo di eccepire l'inammissibilità del ricorso, asseritamente determinata dal vizio relativo alla notifica dello stesso, in quanto effettuata esclusivamente a mezzo di posta elettronica certificata (di seguito: PEC);&#13;
 che, secondo la resistente, «in ragione della sua inapplicabilità nei giudizi di legittimità costituzionale in via principale, la notifica telematica effettuata al Presidente della Regione» risulterebbe «tamquam non esset»;&#13;
 che, di conseguenza, essendosi consumato il «termine perentorio di legge», lo Stato sarebbe «irrimediabilmente decadut[o] dal potere di impugnativa delle norme regionali»;&#13;
 che, a sostegno di tale eccezione, la Regione Siciliana richiama la sentenza n. 200 del 2019, nella quale la Corte costituzionale ha affermato che, «[a]ttesa la specialità dei giudizi innanzi a questa Corte, la modalità della notifica mediante PEC non può, allo stato, ritenersi compatibile - né è stata sin qui mai utilizzata - per la notifica dei ricorsi in via principale o per conflitto di attribuzione»;&#13;
 che, con atto depositato il 15 novembre 2019, l'associazione ANCE Sicilia - Collegio regionale dei Costruttori Edili Siciliani (di seguito: ANCE Sicilia) ha spiegato atto di intervento ad opponendum, con specifico riferimento alle censure mosse nei confronti dell'art. 4 della legge reg. Siciliana n. 13 del 2019;&#13;
 che l'Avvocatura generale ha depositato memoria, in cui ha eccepito l'inammissibilità dell'intervento di ANCE Sicilia e ha insistito per l'accoglimento del ricorso, senza nulla replicare sulla eccezione avanzata dalla resistente;&#13;
 che, all'udienza pubblica, il Presidente della Corte costituzionale ha reso noto che l'eccezione di inammissibilità formulata dalla Regione Siciliana sarebbe stata decisa separatamente e preliminarmente, in ragione della natura pregiudiziale della stessa; &#13;
 che, conseguentemente, ha invitato le parti a discutere esclusivamente l'eccezione relativa alla ritualità della notifica; &#13;
 che, secondo l'Avvocatura generale, la notifica a mezzo PEC sarebbe consentita nel giudizio costituzionale alla luce del «rinvio dinamico» contenuto nel codice del processo amministrativo, approvato dall'art. 1 del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo);&#13;
 che la Regione Siciliana, richiamando quanto affermato da questa Corte nella sentenza n. 200 del 2019, ha insistito per l'accoglimento dell'eccezione, chiedendo in subordine di poter comunque discutere tutte le censure sollevate. &#13;
 Considerato che il giudizio indicato in epigrafe è stato introdotto con ricorso del Presidente del Consiglio dei ministri sottoscritto digitalmente e notificato esclusivamente a mezzo di posta elettronica certificata (di seguito: PEC) il 23 settembre 2019;&#13;
 che il ricorso è stato depositato in cancelleria il 25 settembre 2019 con allegata attestazione di conformità;&#13;
 che la Regione Siciliana si è costituita con atto depositato il 28 ottobre 2019, esclusivamente per eccepire l'inammissibilità del ricorso sul rilievo dell'asserita irritualità della sua notifica a mezzo PEC;&#13;
 che la decisione su tale eccezione di inammissibilità è pregiudiziale rispetto all'esame di tutti gli altri profili, sia di inammissibilità che di merito, relativi alle questioni promosse, nonché alla stessa decisione sull'ammissibilità dell'atto d'intervento depositato dall'associazione ANCE Sicilia - Collegio regionale dei Costruttori Edili Siciliani;&#13;
 che deve essere oggetto di precisazioni quanto statuito da questa Corte nella sentenza n. 200 del 2019;&#13;
 che, a ben vedere, infatti, nella sentenza appena citata, l'affermazione secondo cui la notifica del ricorso via PEC non risultava «compatibile» con la specificità del processo costituzionale era inserita in una vicenda particolare, incentrata sulla tempestività della costituzione della parte resistente effettuata a seguito della reiterata notifica affidata ad ufficiale giudiziario; &#13;
 che, in ogni caso, la disciplina delle notificazioni dei ricorsi in via principale non è espressamente contenuta nelle fonti che regolano i giudizi davanti a questa Corte;&#13;
 che, in mancanza di disposizioni dettate appositamente per il giudizio costituzionale, soccorre il rinvio contenuto nell'art. 22, primo comma, della legge 11 marzo 1953, n. 87, recante «Norme sulla costituzione e sul funzionamento della Corte costituzionale» (ex plurimis, sentenza n. 144 del 2015 e ordinanza n. 101 del 2017);&#13;
 che, secondo tale previsione, nei procedimenti davanti a questa Corte si osservano, in quanto applicabili, le norme del regolamento per la procedura innanzi al Consiglio di Stato in sede giurisdizionale, oggi disciplinata dal codice del processo amministrativo, approvato dall'art. 1 del decreto legislativo 2 luglio 2010, n. 104 (Attuazione dell'articolo 44 della legge 18 giugno 2009, n. 69, recante delega al governo per il riordino del processo amministrativo);&#13;
 che l'art. 39, comma 2, del citato codice dispone che «[l]e notificazioni degli atti del processo amministrativo sono comunque disciplinate dal codice di procedura civile e dalle leggi speciali concernenti la notificazione degli atti giudiziari in materia civile»;&#13;
 che, in particolare, l'art. 55, comma 1, della legge 18 giugno 2009, n. 69 (Disposizioni per lo sviluppo economico, la semplificazione, la competitività nonché in materia di processo civile) attribuisce all'Avvocatura generale dello Stato la possibilità di eseguire le notificazioni secondo quanto prevede la legge 21 gennaio 1994, n. 53 (Facoltà di notificazioni di atti civili, amministrativi e stragiudiziali per gli avvocati e procuratori legali);&#13;
 che questa Corte ha già affermato che il citato art. 55 deve considerarsi «pacificamente applicabile anche ai giudizi di legittimità costituzionale» (sentenza n. 245 del 2017; in termini simili sentenza n. 310 del 2011);&#13;
 che l'art. 1, comma 1, della legge n. 53 del 1994 - a seguito delle modifiche apportate dall'art. 25, comma 3, lettera a), della legge 12 novembre 2011, n. 183, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (Legge di stabilità 2012)» - prevede, al secondo periodo, la possibilità di eseguire le notificazioni degli atti a mezzo PEC;&#13;
 che il successivo art. 3-bis della stessa legge n. 53 del 1994, introdotto dall'art. 16-quater, comma 1, lettera d), del decreto-legge 18 ottobre 2012, n. 179, recante «Ulteriori misure urgenti per la crescita del Paese», convertito, con modificazioni, nella legge 17 dicembre 2012, n. 221, come inserito dall'art. 1, comma 19, numero 2), della legge 24 dicembre 2012, n. 228, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (Legge di stabilità 2013)», disciplina dettagliatamente le modalità con cui la notificazione a mezzo PEC può essere eseguita;&#13;
 che questa Corte ha riconosciuto l'applicabilità di altre previsioni della legge n. 53 del 1994 ai giudizi costituzionali (sentenze n. 245 del 2017 e n. 310 del 2011);&#13;
 che, alla luce di tale quadro normativo, deve riconoscersi la possibilità che la notifica dei ricorsi introduttivi di giudizi di legittimità costituzionale in via principale sia validamente effettuata mediante PEC;&#13;
 che deve essere pertanto respinta l'eccezione di inammissibilità formulata dalla Regione Siciliana; &#13;
 che, tuttavia, in ragione della novità del caso, nonché dell'affidamento riposto dalla resistente Regione Siciliana su quanto affermato nella citata sentenza n. 200 del 2019, va disposto il rinvio della causa a nuovo ruolo, così da consentire alle parti, ai sensi dell'art. 10 delle Norme integrative per i giudizi davanti alla Corte costituzionale, di depositare eventuali memorie illustrative e di discutere il merito del ricorso in una nuova udienza pubblica;&#13;
 che resta, comunque, impregiudicata ogni altra valutazione, sia sull'ammissibilità del ricordato atto d'intervento, sia su ogni eventuale, ulteriore profilo di ammissibilità delle censure sollevate.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 1) respinge l'eccezione di inammissibilità del ricorso sollevata dalla Regione Siciliana;&#13;
 2) rinvia la causa a nuovo ruolo. &#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 ottobre 2020.&#13;
 F.to:&#13;
 Mario Rosario MORELLI, Presidente&#13;
 Nicolò ZANON, Redattore&#13;
 Filomena PERRONE, Cancelliere&#13;
 Depositata in Cancelleria il 20 novembre 2020.&#13;
 Il Cancelliere&#13;
 F.to: Filomena PERRONE</dispositivo>
  </pronuncia_testo>
</pronuncia>
