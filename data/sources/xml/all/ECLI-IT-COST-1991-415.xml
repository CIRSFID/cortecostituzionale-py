<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>415</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:415</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/11/1991</data_decisione>
    <data_deposito>19/11/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente                                               &#13;
                               ORDINANZA                                  &#13;
 nel giudizio di legittimità costituzionale dell'art. 8, primo comma,    &#13;
 lett. b) del d.P.R. 25 ottobre 1981, n.  737  (Sanzioni  disciplinari    &#13;
 per   il  personale  dell'Amministrazione  di  pubblica  sicurezza  e    &#13;
 regolamentazione dei relativi procedimenti)  promosso  con  ordinanza    &#13;
 emessa  il 20 dicembre 1990 dal Consiglio di giustizia amministrativa    &#13;
 per la Regione Sicilia sul ricorso proposto dal  Questore  di  Ragusa    &#13;
 p.t.  ed  altro  contro  Giacomo  Pampallona,  iscritta al n. 324 del    &#13;
 registro ordinanze 1991 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 21, prima serie speciale dell'anno 1991;                   &#13;
    Udito  nella  camera  di  consiglio  del 9 ottobre 1991 il Giudice    &#13;
 relatore Gabriele Pescatore;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel prendere cognizione - in sede di appello - di un provvedimento    &#13;
 di destituzione di diritto di  un  agente  della  polizia  di  Stato,    &#13;
 emanato  prima dell'entrata in vigore della legge 10 ottobre 1986, n.    &#13;
 668, il Consiglio di giustizia amministrativa per la Regione Sicilia,    &#13;
 con ordinanza 29 dicembre 1989, sollevava questione  di  legittimità    &#13;
 costituzionale  dell'art.  8,  primo  comma,  lett. b), del d.P.R. 25    &#13;
 ottobre 1981, n. 737, che prevedeva la destituzione  di  diritto  del    &#13;
 dipendente  che, a seguito di condanna penale, fosse stato interdetto    &#13;
 anche temporaneamente dai pubblici uffici.                               &#13;
    Con  ordinanza  n.  403 del 1990, questa Corte restituiva gli atti    &#13;
 all'anzidetto  Consiglio  per  il  riesame  della   rilevanza   della    &#13;
 questione  alla  luce  della legge 7 febbraio 1990, n. 19, contenente    &#13;
 una nuova disciplina della riammissione in  servizio  del  dipendente    &#13;
 destituito.                                                              &#13;
    Il   Consiglio  di  giustizia  amministrativa,  con  ordinanza  20    &#13;
 dicembre  1990,  ha  nuovamente  sollevato   la   stessa   questione,    &#13;
 osservando  che  l'eventuale  declaratoria di illegittimità del già    &#13;
 richiamato  art.   8,   nel   suo   testo   originario,   condurrebbe    &#13;
 all'annullamento,  da parte dello stesso Consiglio, del provvedimento    &#13;
 di destituzione emanato il 29 gennaio 1985, col  conseguente  diritto    &#13;
 dell'interessato alla integrale ricostruzione ex tunc della carriera.    &#13;
 La  riammissione in servizio, ai sensi dell'art. 10 della legge n. 19    &#13;
 del 1990,  comporta  invece  la  reintegrazione  nel  ruolo  "con  la    &#13;
 qualifica,   il   livello  e  l'anzianità  posseduti  alla  data  di    &#13;
 cessazione dal  servizio",  operando  quindi  con  effetto  ex  nunc.    &#13;
 Permarrebbe  dunque,  ad  avviso del giudice remittente, la rilevanza    &#13;
 della questione, già sollevata  in  data  29  dicembre  1989  e  ora    &#13;
 riproposta.<diritto>Considerato in diritto</diritto>1.  -  Il  Consiglio  di  giustizia  amministrativa per la Regione    &#13;
 Sicilia ha sollevato, in riferimento all'art. 3  della  Costituzione,    &#13;
 questione  di  legittimità  costituzionale dell'art. 8, primo comma,    &#13;
 lett. b), del d.P.R. 25 ottobre 1981, n. 737, nel testo vigente prima    &#13;
 dell'entrata in vigore delle leggi 10  ottobre  1986,  n.  668,  e  7    &#13;
 febbraio 1990, n. 19.                                                    &#13;
    La   norma   impugnata   prevedeva   la  destituzione  di  diritto    &#13;
 dell'appartenente  ai  ruoli  dall'Amministrazione   della   pubblica    &#13;
 sicurezza    a   seguito   di   condanna   penale   che   comportasse    &#13;
 l'interdizione, anche temporanea, dai pubblici uffici.                   &#13;
    La questione, pur dopo le menzionate leggi n. 668 del 1986 e n. 19    &#13;
 del 1990, conserverebbe  la  propria  rilevanza  perché  l'eventuale    &#13;
 accoglimento    condurrebbe    all'annullamento   del   provvedimento    &#13;
 destitutivo, con  il  conseguente  diritto  dell'interessato  ad  una    &#13;
 integrale  ricostruzione  ex tunc della carriera sotto il profilo sia    &#13;
 giuridico che economico. L'applicazione dell'art. 10 della  legge  n.    &#13;
 19 del 1990 porterebbe invece alla meno favorevole reintegrazione nel    &#13;
 ruolo con la qualifica, il livello e l'anzianità posseduti alla data    &#13;
 di cessazione del servizio. Donde l'interesse del pubblico dipendente    &#13;
 "a   vedere   decisa  la  questione  di  legittimità  costituzionale    &#13;
 sollevata con l'ordinanza n. 504 del 1989".                              &#13;
    2. - La questione è da dichiararsi irrilevante.                      &#13;
    Con la normativa posta dalla legge n. 19 del 1990  il  legislatore    &#13;
 ha inteso dare una generale ed uniforme disciplina alla materia della    &#13;
 destituzione  del pubblico dipendente e risolvere altresì i problemi    &#13;
 insorti a seguito della sentenza di questa  Corte  n.  971  del  1988    &#13;
 (preceduta da altre decisioni), che aveva dichiarato l'illegittimità    &#13;
 di  numerose norme che comminavano la destituzione di diritto, invece    &#13;
 che l'apertura  del  procedimento  disciplinare,  con  le  possibili,    &#13;
 diversificate sanzioni, in relazione alle specifiche situazioni.         &#13;
    A  tal  fine, la legge n. 19 provvede con gli articoli 9 e 10. Con    &#13;
 il primo di essi definisce la nuova normativa, in base alla quale  il    &#13;
 pubblico  dipendente  può  essere  destituito  a seguito di condanna    &#13;
 penale soltanto all'esito del procedimento  disciplinare,  che  viene    &#13;
 assoggettato   a   termini   rigorosi   (centottanta  giorni  per  il    &#13;
 promovimento del giudizio disciplinare, novanta  giorni  per  la  sua    &#13;
 definizione),   diretti   a  limitare  nel  tempo  la  situazione  di    &#13;
 incertezza che tocca sia la p.a. che il soggetto implicato.              &#13;
    L'art.  10,  a  sua   volta,   detta   una   coerente   disciplina    &#13;
 intertemporale,  che  predispone  un'adeguata  tutela per il soggetto    &#13;
 già  incorso  nella  destituzione  di   diritto,   prevedendone   la    &#13;
 riammissione   in   servizio,   a   domanda.  Il  legislatore  si  è    &#13;
 preoccupato, contestualmente, di realizzare  il  pubblico  interesse,    &#13;
 inteso  a  conservare  alla  p.a.  il  personale, la cui condotta sia    &#13;
 riconosciuta non incompatibile  con  le  esigenze  del  servizio.  Si    &#13;
 tratta, in sostanza, di un complesso normativo, diretto ad attuare un    &#13;
 assetto  definitivo  di situazioni, già definite con l'inflizione di    &#13;
 un provvedimento destitutivo, contrastato dalle ricordate pronunce di    &#13;
 incostituzionalità e dalla nuova legge, attraverso  misure  sorrette    &#13;
 da una più equa considerazione delle ragioni del pubblico dipendente    &#13;
 destituito  e di quelle della p.a., vo'lte a condizionare il recupero    &#13;
 dell'impiegato alle necessarie garanzie amministrative,  da  definire    &#13;
 con particolare rapidità.                                               &#13;
    Da tale normativa si desume innanzitutto che con la presentazione,    &#13;
 da  parte  del  pubblico  dipendente  destituito,  della  domanda  di    &#13;
 riammissione in servizio  (art.  10,  comma  2),  cessa  la  sanzione    &#13;
 inflitta  e  sorge  la  potestà  della  p.a.  di promuovere un nuovo    &#13;
 procedimento disciplinare entro i rapidi termini innanzi indicati.       &#13;
    La nuova disciplina sostituisce integralmente  quelle  precedenti,    &#13;
 le  quali  erano  sorrette  dalla ratio dell'automatismo destitutivo,    &#13;
 abrogato espressamente dalla legge n. 19 e  integralmente  sostituito    &#13;
 dal nuovo sistema.                                                       &#13;
    Si regola così in modo organico, generale ed uniforme la delicata    &#13;
 materia  della destituzione ex lege con riguardo ad ogni categoria di    &#13;
 pubblici dipendenti.                                                     &#13;
    3. - Sulla scorta della nuova normativa, posta dalla legge n.  19,    &#13;
 appare  opportuno  fissare  il  quadro delle posizioni dei dipendenti    &#13;
 destituiti.                                                              &#13;
    Per le posizioni, già definite con il provvedimento  destitutivo,    &#13;
 l'amministrazione  provvede  all'apertura  di  un  nuovo procedimento    &#13;
 disciplinare, sul  presupposto  della  estinzione  della  sanzione  a    &#13;
 seguito   della   presentazione  della  domanda  di  riammissione  in    &#13;
 servizio.                                                                &#13;
    Il nuovo procedimento rientra  nel  dominio  della  legge  n.  19,    &#13;
 vigente  fin  dal  momento  della  sua instaurazione. Tale legge, per    &#13;
 effetto dell'espressa abrogazione, dalla sua entrata  in  vigore,  di    &#13;
 "ogni contraria disposizione di legge" (art. 9, primo comma, l. cit.)    &#13;
 è  la  sola  competente  a regolare la materia oggetto del rinnovato    &#13;
 giudizio.                                                                &#13;
    Per le posizioni, rispetto alle quali il procedimento disciplinare    &#13;
 è in corso (ad esse si riferiscono il secondo comma dell'art. 9 e il    &#13;
 terzo comma dell'art. 10 della legge n. 19 quando  fanno  riferimento    &#13;
 ai  procedimenti  che  debbono  essere  "proseguiti"),  è  del  pari    &#13;
 applicabile alla legge n. 19, come è  espressamente  previsto  dalle    &#13;
 norme  ora  dette  che  stabiliscono  -  facendo riferimento al nuovo    &#13;
 procedimento - che esso deve essere proseguito e definito nei termini    &#13;
 ristretti e perentori dalle norme stesse sanciti.                        &#13;
    Le  situazioni  in  via  di  svolgimento vanno regolate secondo un    &#13;
 disegno unitario e uniforme per tutte le categorie degli interessati,    &#13;
 per   pervenire   a   soluzioni   eque   e   rapide,   cessata   ogni    &#13;
 diversificazione  di  trattamento  sostanziale e formale, consentita,    &#13;
 invece, dalle precedenti normative.                                      &#13;
    Per le posizioni,  infine,  caratterizzate  dall'inflizione  della    &#13;
 destituzione,  contro  la  quale sia stata esperita impugnativa (e il    &#13;
 relativo  giudizio  sia  ancora  pendente:  situazione,  questa,  che    &#13;
 caratterizza la fattispecie), è del pari applicabile la nuova legge,    &#13;
 che  -  come  si  è  posto in luce - è espressione di una rilevante    &#13;
 esigenza di pubblico interesse alla  concreta  e  rapida  definizione    &#13;
 delle  situazioni pendenti e, come tale, regola la materia prevalendo    &#13;
 su ogni altra precedente, fissando  limiti  e  termini  specifici  al    &#13;
 giudizio.                                                                &#13;
    Si   realizza   così   un   sistema   normativo,   sostanziale  e    &#13;
 procedimentale, che ha come momento iniziale la cessazione  dell'atto    &#13;
 destitutivo  e  come  momento finale la rideterminazione dello status    &#13;
 del dipendente, a  seguito  della  rinnovata  valutazione  della  sua    &#13;
 condotta.  La nuova disciplina agevola il dipendente, facendo cessare    &#13;
 la destituzione, e lo recupera all'amministrazione tutte le volte che    &#13;
 il procedimento disciplinare lo  consente.  Si  realizza,  così,  un    &#13;
 trattamento unitario di tutte le pregresse posizioni di destituzione,    &#13;
 con   l'impossibilità  di  perseguire  soluzioni  differenziate  dal    &#13;
 modello   descritto.   Si   impedisce   inoltre    la    prosecuzione    &#13;
 dell'impugnativa   in   corso   con   le   (eventuali)  pronuncie  di    &#13;
 annullamento. Anche le posizioni ad esse relative  vengono  assorbite    &#13;
 nel  disegno  unificatore, ispirato al perseguimento delle già dette    &#13;
 esigenze pubbliche e si consente al  dipendente,  già  destituito  e    &#13;
 riammesso  ai sensi dell'art. 10, secondo e terzo comma, legge n. 19,    &#13;
 di essere "reintegrato nel ruolo, con  la  qualifica,  il  livello  e    &#13;
 l'anzianità posseduti alla data di cessazione del servizio" (art. 10    &#13;
 cit., quarto comma).                                                     &#13;
    Il contenuto e gli effetti della normativa posta dalla nuova legge    &#13;
 costituiscono l'elemento fondamentale del giudizio di rilevanza della    &#13;
 questione  di costituzionalità proposta alla Corte. Il giudice a quo    &#13;
 si è limitato a considerare l'interesse del dipendente ad ottenere i    &#13;
 benefici della eventuale pronuncia di  annullamento.  Vale  in  segno    &#13;
 contrario,   oltre  a  quanto  si  è  già  osservato,  il  richiamo    &#13;
 all'insegnamento di questa Corte, la quale ha affermato, (cfr.  sent.    &#13;
 20  luglio 1990, n. 344) che "secondo una giurisprudenza consolidata,    &#13;
 la rilevanza di una determinata  questione  di  costituzionalità  va    &#13;
 valutata,  non  già  in  relazione  agli  ipotetici  vantaggi di cui    &#13;
 potrebbero beneficiare le parti in causa, ma, piuttosto, in relazione    &#13;
 alla semplice applicabilità nel giudizio a quo della legge di cui si    &#13;
 contesta la legittimità costituzionale e, quindi, alla influenza che    &#13;
 sotto tale profilo il giudizio di costituzionalità  può  esercitare    &#13;
 su quello dal quale proviene la questione".                              &#13;
    Non  appare, quindi, pertinente l'argomentazione del giudice a quo    &#13;
 circa la rilevanza della  questione  di  legittimità  costituzionale    &#13;
 dell'abrogato  art.  8,  primo  comma, lett. b) del d.P.R. 25 ottobre    &#13;
 1981, n. 737.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 8, primo comma, lett. b) del d.P.R. 25 ottobre 1981, n. 737    &#13;
 (Sanzioni  disciplinari  per  il  personale  dell'Amministrazione  di    &#13;
 pubblica  sicurezza  e  regolamentazione  dei relativi procedimenti),    &#13;
 sollevata in riferimento all'art. 3 della Costituzione, dal Consiglio    &#13;
 di giustizia amministrativa per la Regione Sicilia,  con  l'ordinanza    &#13;
 in epigrafe.                                                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 6 novembre 1991.                                 &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 novembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
