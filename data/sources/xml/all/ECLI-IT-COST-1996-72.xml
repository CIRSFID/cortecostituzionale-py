<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>72</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:72</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1996</data_decisione>
    <data_deposito>15/03/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Guidici: pof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 369, primo    &#13;
 comma, del codice della navigazione,  approvato  con  r.d.  30  marzo    &#13;
 1942,  n.  327,  promosso  con  ordinanza emessa il 30 marzo 1995 dal    &#13;
 Pretore di Siracusa - sezione distaccata di Augusta, nel procedimento    &#13;
 civile vertente tra Morello Domenico e  Banca  di  Credito  Popolare,    &#13;
 iscritta  al  n.  323  del registro ordinanze 1995 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  24,  prima  serie  speciale,    &#13;
 dell'anno 1995;                                                          &#13;
   Udito  nella  camera  di  consiglio del 21 febbraio 1996 il Giudice    &#13;
 relatore Luigi Mengoni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. -  Nel  corso  di  un  giudizio  di  opposizione  all'esecuzione    &#13;
 promossa  dalla Banca di Credito Popolare di Siracusa contro Domenico    &#13;
 Morello mediante pignoramento dei crediti da  lui  vantati  verso  la    &#13;
 S.p.a.    Augustea Imprese Marittime in dipendenza di un contratto di    &#13;
 arruolamento, il Pretore di Siracusa - sezione distaccata di Augusta,    &#13;
 con ordinanza  del  30  marzo  1995,  ha  sollevato,  in  riferimento    &#13;
 all'art.    3   della   Costituzione,   questione   di   legittimità    &#13;
 costituzionale dell'art.  369 del r.d. 30 marzo 1942, n.  327  (testo    &#13;
 definitivo  del  codice  della  navigazione)  nella  parte in cui non    &#13;
 prevede, analogamente a quanto dispone l'art. 545  cod.proc.civ.  per    &#13;
 gli altri dipendenti privati, la pignorabilità e la sequestrabilità    &#13;
 delle  retribuzioni  corrisposte all'arruolato, fino alla concorrenza    &#13;
 di un quinto, per ogni credito vantato nei suoi confronti.               &#13;
   La  questione  viene  riproposta  nonostante  il  precedente  della    &#13;
 sentenza  di  infondatezza  n. 101 del 1974, non sembrando al giudice    &#13;
 rimettente che il mero fatto  della  navigazione  sia  per  sé  solo    &#13;
 sufficiente a giustificare il trattamento privilegiato, rispetto agli    &#13;
 altri lavoratori, accordato dalla norma denunciata ai marittimi.         &#13;
   La  disparità  di trattamento è prospettata anche in relazione ai    &#13;
 crediti,   considerato   che   l'art.   369   cod.nav.   ammette   la    &#13;
 pignorabilità  di  un  quinto  della  retribuzione  limitatamente ai    &#13;
 debiti per alimenti e ai debiti certi,  liquidi  ed  esigibili  verso    &#13;
 l'armatore dipendenti dal servizio della nave.                           &#13;
   Si osserva infine che la disparità di trattamento è ulteriormente    &#13;
 sottolineata  dalle  successive  pronunce  di questa Corte, che hanno    &#13;
 progressivamente eliminato analoghe disposizioni in favore  di  altre    &#13;
 categorie di lavoratori, in particolare dei dipendenti pubblici.<diritto>Considerato in diritto</diritto>1.  -  Con  ordinanza  del  30  marzo 1995 il Pretore di Siracusa -    &#13;
 sezione distaccata di Augusta ha sollevato questione di  legittimità    &#13;
 costituzionale  dell'art. 369 del codice della navigazione, approvato    &#13;
 con r.d. 30 marzo 1942,  n.  327,  per  contrasto  col  principio  di    &#13;
 eguaglianza  di cui all'art. 3 della Costituzione, nella parte (primo    &#13;
 comma) in cui, in  deroga  all'art.  545  cod.proc.civ.,  ammette  il    &#13;
 sequestro  o il pignoramento delle retribuzioni degli arruolati, fino    &#13;
 a un quinto del loro ammontare, esclusivamente a  causa  di  alimenti    &#13;
 dovuti  per  legge  o  per  debiti  certi, liquidi ed esigibili verso    &#13;
 l'armatore, dipendenti dal servizio della nave.                          &#13;
   2.1. - La questione è fondata.                                        &#13;
   La norma impugnata, risalente all'Ordinanza della marina 1 novembre    &#13;
 1745 di Luigi XV, fu giustificata all'origine in  ragione  del  fatto    &#13;
 della  navigazione,  allora  caratterizzato  dalla  lunga  durata dei    &#13;
 viaggi e dalla difficoltà di rimesse di denaro in patria durante  il    &#13;
 viaggio:   circostanze che potevano occasionare "il libertinaggio dei    &#13;
 marinai"  distraendo  i  salari  dalla   naturale   destinazione   al    &#13;
 sostentamento delle loro famiglie.                                       &#13;
   La  norma, tenuta ferma dalla giurisprudenza francese nonostante il    &#13;
 silenzio della codificazione napoleonica, fu accolta  nel  codice  di    &#13;
 commercio  italiano  del 1882: l'art. 545 ammetteva il sequestro o il    &#13;
 pignoramento delle paghe dei  marinai  solo  per  causa  di  alimenti    &#13;
 legali,  nei  limiti di un terzo, e, senza limite, per i debiti verso    &#13;
 la nave dipendenti dal servizio della nave stessa.  Venuta  meno  col    &#13;
 progresso  tecnologico  la ratio originaria, il trattamento di favore    &#13;
 fu collegato agli aspetti pubblicistici dei rapporti  di  lavoro  del    &#13;
 personale  navigante,  legati  all'interesse  della  sicurezza  della    &#13;
 navigazione e della regolarità dei servizi di trasporto marittimo, e    &#13;
 quindi fu giustificato  per  analogia  con  i  rapporti  di  pubblico    &#13;
 impiego,  per  i  quali  una  norma simile era stata introdotta dalle    &#13;
 leggi 14 aprile e 17 giugno 1864, nn. 1731 e 1807.                       &#13;
   2.2. - Ammesso -  come  pure  aveva  ritenuto  questa  Corte  nella    &#13;
 sentenza  n.  101  del  1974  -  che una simile giustificazione fosse    &#13;
 ripetibile nel 1942 per l'art. 369 cod.nav. (che ha riprodotto l'art.    &#13;
 545 cod.comm.  con riduzione del limite da un terzo a  un  quinto  ed    &#13;
 estensione  di  esso  anche  al secondo caso), essa è venuta meno in    &#13;
 conseguenza di successive sentenze che hanno eliminato il trattamento    &#13;
 privilegiato dei pubblici dipendenti  e,  con  esso,  il  modello  di    &#13;
 riferimento giustificativo della norma.                                  &#13;
   Si  tratta  delle  sentenze nn. 89 del 1987, 878 del 1988 e 115 del    &#13;
 1990, che hanno dichiarato l'illegittimità costituzionale, le  prime    &#13;
 due,  dell'art.  2,  primo  comma, n. 3 del d.P.R. 5 gennaio 1950, n.    &#13;
 180  (testo  unico  delle  leggi   concernenti   il   sequestro,   il    &#13;
 pignoramento  e  la  cessione  degli  stipendi, salari e pensioni dei    &#13;
 dipendenti delle pubbliche amministrazioni), la  terza  dell'art.  1,    &#13;
 terzo  comma,  lett.    b)  della legge 27 maggio 1959, n. 324, nella    &#13;
 parte in cui non prevedevano la sequestrabilità e la  pignorabilità    &#13;
 delle  retribuzioni,  nonché  dell'indennità  integrativa speciale,    &#13;
 corrisposte dallo Stato e dagli enti pubblici, fino a concorrenza  di    &#13;
 un  quinto,  per  ogni  credito  vantato nei confronti del personale.    &#13;
 L'art. 2, primo comma, n. 3, del d.P.R. n. 180 del 1950 è stato  poi    &#13;
 nuovamente censurato dalla sentenza n. 99 del 1993 nella parte in cui    &#13;
 non prevedeva la sequestrabilità e la pignorabilità, entro i limiti    &#13;
 stabiliti  dall'art.  545  cod.proc.civ.,  delle  indennità  di fine    &#13;
 rapporto  spettanti  ai  dipendenti  degli  enti  pubblici   indicati    &#13;
 nell'art. 1 del decreto, per ogni credito vantato nei loro confronti.    &#13;
   Le  decisioni sono motivate in relazione, per un verso, al processo    &#13;
 in atto di osmosi tra i settori dell'impiego pubblico e  dell'impiego    &#13;
 privato,  comportante  una  progressiva attenuazione delle differenze    &#13;
 tra i due  tipi  di  rapporto;  per  l'altro  verso,  alla  crescente    &#13;
 dilatazione  del  settore  pubblico  "fino a comprendere una serie di    &#13;
 fattispecie e di soggetti nettamente diversi tra  loro  in  raffronto    &#13;
 alle  caratteristiche  della  prestazione  del  dipendente e dei fini    &#13;
 istituzionali dell'ente pubblico", di  guisa  che  "non  appare  più    &#13;
 ricostruibile   la   ratio  unitaria  della  norma  nell'esigenza  di    &#13;
 garantire il buon andamento degli uffici e la continuità dei servizi    &#13;
 della pubblica amministrazione".                                         &#13;
   Siffatta esigenza non era più  sufficiente  a  fornire  una  ratio    &#13;
 coerente  della norma in esame già sul finire degli anni '50, quando    &#13;
 divenne prevalente, trovando poi una prima conferma nella sentenza n.    &#13;
 123  del  1962  di  questa  Corte,  la  giurisprudenza  favorevole  a    &#13;
 riconoscere  il  diritto  di  sciopero  anche ai pubblici dipendenti.    &#13;
 Analogamente, non è  più  adducibile  a  fondamento  dell'art.  369    &#13;
 cod.nav. l'interesse pubblico alla regolarità dei servizi marittimi,    &#13;
 una  volta  riconosciuto  ai componenti dell'equipaggio il diritto di    &#13;
 sciopero quando la nave non  si  trovi  in  navigazione,  in  via  di    &#13;
 interpretazione   restrittiva   della   fattispecie   del   reato  di    &#13;
 ammutinamento prevista dall'art. 1105 cod.nav. (sentenza n.  124  del    &#13;
 1962).                                                                   &#13;
   La   rilevanza   dell'aspetto   pubblicistico   del   contratto  di    &#13;
 arruolamento è stata ulteriormente attenuata dalla legge 19 dicembre    &#13;
 1979, n.  649, aggiuntiva di un nuovo comma  all'art.  325  cod.nav.,    &#13;
 che ha rimesso alla contrattazione collettiva la determinazione della    &#13;
 misura  e  delle  componenti  della  retribuzione,  così confermando    &#13;
 implicitamente la spettanza del diritto di sciopero  anche  a  questa    &#13;
 categoria  di  lavoratori, nel limite indicato dalla sentenza citata.    &#13;
 Altri elementi di specialità, essi pure  correlati  al  fatto  della    &#13;
 navigazione,  sono stati rimossi dalle sentenze nn. 96 del 1987 e 364    &#13;
 del 1991, che hanno dichiarato l'illegittimità dell'art.  35,  terzo    &#13;
 comma,  della  legge 20 maggio 1970, n. 300 (statuto dei lavoratori),    &#13;
 nelle parti in cui escludeva l'applicabilità al personale  navigante    &#13;
 delle   imprese  di  navigazione  delle  norme  di  tutela  contro  i    &#13;
 provvedimenti disciplinari e i licenziamenti di  cui  agli  artt.  7,    &#13;
 primi tre commi, e 18 della legge medesima.                              &#13;
   Per  tutte queste considerazioni si può concludere che, allo stato    &#13;
 attuale dell'ordinamento,  è  cessata  ogni  ragione  giustificativa    &#13;
 della norma impugnata, la quale pertanto risulta lesiva del principio    &#13;
 di eguaglianza.                                                          &#13;
   3.   -   In   conseguenza  della  dichiarazione  di  illegittimità    &#13;
 costituzionale dell'art.  369,  primo  comma,  cod.  nav.,  la  Corte    &#13;
 ritiene  di  dichiarare,  in applicazione dell'art. 27 della legge 11    &#13;
 marzo   1953,   n.   87,   l'illegittimità   costituzionale    anche    &#13;
 dell'identica  norma dettata nell'art. 930, primo comma, dello stesso    &#13;
 codice per il personale di volo delle imprese di navigazione aerea.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  dell'art.  369,   primo    &#13;
 comma,  del  codice  della  navigazione  (approvato con r.d. 30 marzo    &#13;
 1942, n.  327);                                                          &#13;
   Dichiara, in applicazione dell'art. 27 della legge 11  marzo  1953,    &#13;
 n.  87,  l'illegittimità  costituzionale dell'art. 930, primo comma,    &#13;
 dello stesso codice.                                                     &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 7 marzo 1996.                                 &#13;
                         Il Presidente:  Ferri                            &#13;
                        Il redattore:  Mengoni                            &#13;
                       Il cancelliere:  Di Paola                          &#13;
   Depositata in cancelleria il 15 marzo 1996.                            &#13;
               Il direttore della cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
