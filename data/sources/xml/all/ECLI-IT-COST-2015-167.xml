<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2015</anno_pronuncia>
    <numero_pronuncia>167</numero_pronuncia>
    <ecli>ECLI:IT:COST:2015:167</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CRISCUOLO</presidente>
    <relatore_pronuncia>Daria de Pretis</relatore_pronuncia>
    <redattore_pronuncia>Daria de Pretis</redattore_pronuncia>
    <data_decisione>08/07/2015</data_decisione>
    <data_deposito>15/07/2015</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>improcedibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alessandro CRISCUOLO; Giudici : Giuseppe FRIGO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 7, commi 2, 3 e 4, e 13, commi 2 e 3, della delibera legislativa della Regione siciliana relativa al disegno di legge n. 381-3-306-346 (Norme per la tutela della salute e del territorio dai rischi derivanti dall'amianto), approvata dall'Assemblea regionale siciliana nella seduta del 26 marzo 2014, promosso dal Commissario dello Stato per la Regione siciliana con ricorso notificato il 3 aprile 2014, depositato in cancelleria l'8 aprile 2014 ed iscritto al n. 30 del registro ricorsi 2014.&#13;
 Udito nella camera di consiglio dell'8 luglio 2015 il Giudice relatore Daria de Pretis.&#13;
 Ritenuto che, con il ricorso in epigrafe, il Commissario dello Stato per la Regione siciliana ha promosso questioni di legittimità costituzionale degli artt. 7, commi 2, 3 e 4, e 13, commi 2 e 3, della delibera legislativa relativa al disegno di legge n. 381-3-306-346 (Norme per la tutela della salute e del territorio dai rischi derivanti dall'amianto), approvato dalla Assemblea regionale siciliana nella seduta del 26 marzo 2014;&#13;
 che, ad avviso del ricorrente, l'art. 7, commi 2, 3 e 4, nella parte in cui dispone l'assunzione a carico della Regione siciliana - già sottoposta al piano di rientro dal disavanzo sanitario - di oneri aggiuntivi per garantire un livello di assistenza supplementare in favore dei pazienti affetti da patologie causate dall'amianto, violerebbe l'art. 117, terzo comma, della Costituzione, in riferimento alla norma di coordinamento della finanza pubblica dettata dall'art. 2, comma 95, della legge 23 dicembre 2009, n. 191 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato - legge finanziaria 2010), secondo cui «[g]li interventi individuati dal piano di rientro sono vincolanti per la regione, che è obbligata a rimuovere i provvedimenti, anche legislativi, e a non adottarne di nuovi che siano di ostacolo alla piena attuazione del piano di rientro»;&#13;
 che, inoltre, il successivo art. 13, commi 2 e 3, nella parte in cui introduce nuove sanzioni amministrative, contrasterebbe con l'art. 23 Cost., in quanto, in violazione del principio di legalità sostanziale, non definirebbe compiutamente né l'autorità titolare del potere, né i soggetti tenuti all'adempimento dell'obbligo sanzionato, né, tantomeno, l'obbligo stesso; &#13;
 che la medesima disposizione contrasterebbe, altresì, con il principio di imparzialità dell'azione amministrativa di cui all'art. 97 Cost., poiché, in ragione del suo contenuto generico, consentirebbe all'autorità preposta al potere di vigilanza di «ritenere variamente leciti o illeciti gli stessi comportamenti e di sanzionare o meno i singoli soggetti ritenuti rientranti o meno nella generica categoria individuata dalla norma»;&#13;
 che la Regione siciliana non si è costituita in giudizio;&#13;
 che, in sede di promulgazione del suddetto disegno di legge, con legge regionale 29 aprile 2014, n. 10 (Norme per la tutela della salute e del territorio dai rischi derivanti dall'amianto), pubblicata nella Gazzetta Ufficiale della Regione siciliana del 9 maggio 2014, n. 19, sono state omesse le disposizioni oggetto della presente impugnazione;&#13;
 che, successivamente, questa Corte, con la sentenza n. 255 del 2014, pronunciata a seguito di autorimessione, ha dichiarato costituzionalmente illegittimo l'art. 31, comma 2, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), come sostituito dall'art. 9, comma l, della legge 5 giugno 2003, n. 131 (Disposizioni per l'adeguamento dell'ordinamento della Repubblica alla legge costituzionale 18 ottobre 2001, n. 3), limitatamente alle parole «Ferma restando la particolare forma di controllo delle leggi prevista dallo statuto speciale della Regione siciliana», per contrasto con l'art. 10 della legge cost. 18 ottobre 200l, n. 3 (Modifiche al titolo V della parte seconda della Costituzione). &#13;
 Considerato che, con la citata sentenza n. 255 del 2014, sopravvenuta alla proposizione del ricorso, questa Corte, riconoscendo che «il peculiare controllo di costituzionalità delle leggi [...] della Regione siciliana - strutturalmente preventivo - è caratterizzato da un minor grado di garanzia dell'autonomia rispetto a quello previsto dall'art. 127 Cost.», ha dichiarato la illegittimità costituzionale dell'art. 31, comma 2, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), come sostituito dall'art. 9, comma l, della legge 5 giugno 2003, n. 131 (Disposizioni per l'adeguamento dell'ordinamento della Repubblica alla legge costituzionale 18 ottobre 2001, n. 3), nella parte in cui, mantenendo in vigore la predetta peculiare forma di controllo delle leggi prevista dallo statuto speciale della Regione siciliana, si poneva in contrasto con la «clausola di maggior favore» prevista dall'art. 10 della legge costituzionale 18 ottobre 200l, n. 3 (Modifiche al titolo V della parte seconda della Costituzione), a garanzia delle autonomie speciali;&#13;
 che, in conseguenza di tale pronuncia, «deve pertanto estendersi anche alla Regione siciliana il sistema di impugnativa [successiva] delle leggi regionali, previsto dal riformato art. 127 Cost.» e devono ritenersi «non più operanti le norme statutarie relative alle competenze del Commissario dello Stato nel controllo delle leggi siciliane, alla stessa stregua di quanto affermato da questa Corte con riguardo a quelle dell'Alta Corte per la Regione siciliana (sentenza n. 38 del 1957), nonché con riferimento al potere del Commissario dello Stato circa l'impugnazione delle leggi e dei regolamenti statali (sentenza n. 545 del 1989)» (sentenza n. 255 del 2014);&#13;
 che, pertanto, gli artt. 27 (sulla competenza del Commissario dello Stato ad impugnare le delibere legislative dell'Assemblea regionale siciliana), 28, 29 e 30 del regio decreto legislativo 15 maggio 1946, n. 455 (Approvazione dello Statuto della Regione siciliana), convertito in legge costituzionale 26 febbraio 1948, n. 2, non trovano più applicazione, per effetto dell'estensione alla Regione siciliana del controllo successivo previsto dagli artt. 127 Cost. e 31 della legge n. 87 del 1953 per le Regioni a statuto ordinario, secondo quanto già affermato dalla richiamata giurisprudenza di questa Corte per le altre Regioni ad autonomia differenziata e per le Province autonome;&#13;
 che la predetta estensione alla Regione siciliana del controllo successivo di legittimità costituzionale impedisce che il presente giudizio possa avere seguito (anche agli effetti, quindi, di una pronuncia di cessazione della materia del contendere per mancata promulgazione delle disposizioni impugnate, circostanza quest'ultima che preclude anche la concessione di una eventuale rimessione in termini in favore della Presidenza del Consiglio dei ministri), non essendo più previsto che questa Corte eserciti il suo sindacato sulla delibera legislativa regionale prima che quest'ultima sia stata promulgata e pubblicata e, quindi, sia divenuta legge in senso proprio;&#13;
 che, pertanto, deve dichiararsi in limine l'improcedibilità del ricorso (sentenza n. 17 del 2002 e ordinanze n. 123, n. 111 e n. 105 del 2015 e n. 228, n. 182 e n. 65 del 2002).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara improcedibile il ricorso indicato in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 luglio 2015.&#13;
 F.to:&#13;
 Alessandro CRISCUOLO, Presidente&#13;
 Daria de PRETIS, Redattore&#13;
 Gabriella Paola MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 15 luglio 2015.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella Paola MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
