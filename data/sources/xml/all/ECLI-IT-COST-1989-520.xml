<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>520</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:520</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/11/1989</data_decisione>
    <data_deposito>30/11/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  1, secondo    &#13;
 comma, della legge 6 agosto 1984, n. 425  (Disposizioni  relative  al    &#13;
 trattamento  economico  dei magistrati) e dell'art. 9, secondo comma,    &#13;
 della legge 2 aprile 1979, n. 97 (Norme  sullo  stato  giuridico  dei    &#13;
 magistrati  e  sul  trattamento  economico  dei magistrati ordinari e    &#13;
 amministrativi, dei  magistrati  della  giustizia  militare  e  degli    &#13;
 avvocati  dello  Stato),  in  relazione all'art. 5, ultimo comma, del    &#13;
 d.P.R. 28 dicembre 1970, n. 1080, all'art. 2, lett. d),  della  legge    &#13;
 16  dicembre 1961, n. 1308, ed all'art. 10, ultimo comma, della legge    &#13;
 20 dicembre 1961, n.  1345,  così  come  interpretati  dall'art.  1,    &#13;
 secondo  comma,  della  legge 6 agosto 1984, n. 425, promossi con tre    &#13;
 ordinanze emesse il 14  dicembre  1988  dal  Consiglio  di  giustizia    &#13;
 amministrativa  per la Regione siciliana, iscritte rispettivamente ai    &#13;
 nn. 300, 301 e 302 del registro ordinanze  1989  e  pubblicate  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  25, prima serie speciale,    &#13;
 dell'anno 1989;                                                          &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 25 ottobre 1989 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto  che  nel  corso di alcuni giudizi in cui gli appellanti,    &#13;
 già  magistrati  ordinari,  avevano  ricorso  avverso   il   mancato    &#13;
 riconoscimento,  da  parte  delle  sentenze  di primo grado, del loro    &#13;
 diritto  a  percepire  una  serie  di  emolumenti,  il  Consiglio  di    &#13;
 giustizia  amministrativa per la Regione siciliana, con tre ordinanze    &#13;
 d'identico contenuto emesse in data 14 dicembre  1988,  ha  sollevato    &#13;
 questioni  di  legittimità  costituzionale:  a) dell'art. 1, secondo    &#13;
 comma, della legge 6 agosto 1984, n. 425, in riferimento  agli  artt.    &#13;
 24,  102  e  103  della  Costituzione; b) dell'art. 9, secondo comma,    &#13;
 della legge 2 aprile 1979, n. 97, in  relazione  all'art.  5,  ultimo    &#13;
 comma,  del  d.P.R.  28 dicembre 1970, n. 1080, all'art. 2, lett. d),    &#13;
 della legge 16 dicembre 1961, n. 1308 ed all'art. 10,  ultimo  comma,    &#13;
 della  legge  20  dicembre  1961,  n.  1345  (così come interpretati    &#13;
 dall'art. 1, secondo comma, della  legge  6  agosto  1984,  n.  425),    &#13;
 nonché,  per  quanto  di ragione del medesimo art. 1, secondo comma,    &#13;
 della legge 6 agosto 1984, n. 425, in riferimento agli artt. 3  e  36    &#13;
 della Costituzione;                                                      &#13;
      che  il  giudice  a quo rileva come il legislatore abbia imposto    &#13;
 una soluzione contraria alle pronunce  giurisdizionali  sino  a  quel    &#13;
 momento  intervenute,  rilevando  inoltre che i ricorrenti, in quanto    &#13;
 cessati dal servizio anteriormente al 1° gennaio 1983,  hanno  subito    &#13;
 soltanto  gli  effetti  sfavorevoli e non anche quelli positivi della    &#13;
 legge                                                                    &#13;
 6 agosto 1984, n. 425;                                                   &#13;
      che il giudice rimettente, nel segnalare l'irrazionalità insita    &#13;
 nella differenziazione di  trattamento  tra  le  varie  categorie  di    &#13;
 magistrati  e,  nel  richiamare  analoghe ordinanze di rimessione del    &#13;
 Consiglio di Stato sollecita il riesame, da parte  di  questa  Corte,    &#13;
 delle  questioni già dichiarate infondate con la sentenza n. 413 del    &#13;
 1988;                                                                    &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato e difeso  dall'Avvocatura  dello  Stato,  il  quale  ha    &#13;
 chiesto la declaratoria d'infondatezza;                                  &#13;
    Considerato  che  i  giudizi  possono  essere riuniti e decisi con    &#13;
 un'unica ordinanza;                                                      &#13;
      che  questa  Corte,  con  la  sentenza  n. 413 del 1988, ha già    &#13;
 dichiarato   l'infondatezza   della   questione    di    legittimità    &#13;
 costituzionale dell'art. 1, secondo comma, della legge 6 agosto 1984,    &#13;
 n. 425, escludendo, in particolare, la lesione degli artt. 24, 102  e    &#13;
 103  della Costituzione sulla base della ratio della norma impugnata,    &#13;
 la quale, oltre ad eliminare incertezze interpretative,  è  volta  a    &#13;
 costituire  "l'indispensabile  presupposto  logico  e  organizzatorio    &#13;
 della  ristrutturazione  del  trattamento  economico  per  tutte   le    &#13;
 categorie dei magistrati";                                               &#13;
      che tale principio è stato altresì ribadito nelle ordinanze n.    &#13;
 1047 del 1988, n. 48 del 1989 e, soprattutto, n. 1083 del 1988;          &#13;
      che   in   quest'ultima   decisione   la   Corte  ha  dichiarato    &#13;
 manifestamente  infondata  anche   la   questione   di   legittimità    &#13;
 costituzionale concernente l'art. 9, secondo comma, della legge n. 97    &#13;
 del  1979,  sollevata  dal  Consiglio  di  Stato  con  le   ordinanze    &#13;
 richiamate dal giudice a quo;                                            &#13;
      che  tale conclusione è stata raggiunta in quanto nel complesso    &#13;
 della normativa  si  è  ravvisato  l'esercizio  di  discrezionalità    &#13;
 legislativa   finalizzata   alla   realizzazione   del  principio  di    &#13;
 eguaglianza o di ragionevolezza;                                         &#13;
      che  il rimettente Consiglio non prospetta argomenti ulteriori o    &#13;
 diversi rispetto a quelli  a  suo  tempo  esaminati,  limitandosi  in    &#13;
 sostanza a richiedere un riesame delle suddette affermazioni;            &#13;
      che le questioni sono pertanto manifestamente infondate;            &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi,                                                     &#13;
      1)   dichiara  la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 1, secondo comma, della legge 6    &#13;
 agosto  1984,  n. 425 (Disposizioni relative al trattamento economico    &#13;
 dei magistrati), sollevata, in riferimento agli artt. 24, 102  e  103    &#13;
 della  Costituzione, dal Consiglio di giustizia amministrativa per la    &#13;
 Regione siciliana con le ordinanze di cui in epigrafe;                   &#13;
      2)   dichiara  la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 9, secondo comma, della legge 2    &#13;
 aprile  1979, n. 97 (Norme sullo stato giuridico dei magistrati e sul    &#13;
 trattamento economico dei magistrati ordinari e  amministrativi,  dei    &#13;
 magistrati della giustizia militare e degli avvocati dello Stato), in    &#13;
 relazione all'art. 5, ultimo comma, del d.P.R. 28 dicembre  1970,  n.    &#13;
 1080,  all'art. 2, lett. d), della legge 16 dicembre 1961, n. 1308 ed    &#13;
 all'art. 10, ultimo comma, della legge  20  dicembre  1961,  n.  1345    &#13;
 (così  come  interpretati  dall'art. 1, secondo comma, della legge 6    &#13;
 agosto 1984, n. 425), nonché, "per quanto di ragione", del  medesimo    &#13;
 art.  1, secondo comma, della legge 6 agosto 1984, n. 425, sollevata,    &#13;
 in riferimento agli artt. 3 e 36 della Costituzione, dal Consiglio di    &#13;
 giustizia amministrativa per la Regione siciliana con le ordinanze di    &#13;
 cui in epigrafe.                                                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 novembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 30 novembre 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
