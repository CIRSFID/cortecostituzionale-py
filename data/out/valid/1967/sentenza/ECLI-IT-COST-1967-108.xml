<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1967-07-12/108/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1967-07-12/108"/>
          <FRBRalias value="ECLI:IT:COST:1967:108" name="ECLI"/>
          <FRBRdate date="1967-07-12" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="108"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1967-07-12/108/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1967-07-12/108/ita@"/>
          <FRBRdate date="1967-07-12" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1967-07-12/108/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1967-07-12/108/ita@.xml"/>
          <FRBRdate date="1967-07-12" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1967-06-26" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1967-07-12" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="AMBROSINI" href="/akn/ontology/person/it/AMBROSINI" showAs="AMBROSINI"/>
        <TLCPerson eId="gaspareAmbrosini" href="/akn/ontology/person/it/gaspareAmbrosini" showAs="GASPARE AMBROSINI"/>
        <TLCPerson eId="nicolaJaeger" href="/akn/ontology/person/it/nicolaJaeger" showAs="NICOLA JAEGER"/>
        <TLCPerson eId="giovanniCassandro" href="/akn/ontology/person/it/giovanniCassandro" showAs="GIOVANNI CASSANDRO"/>
        <TLCPerson eId="antonioManca" href="/akn/ontology/person/it/antonioManca" showAs="ANTONIO MANCA"/>
        <TLCPerson eId="giuseppeBranca" href="/akn/ontology/person/it/giuseppeBranca" showAs="GIUSEPPE BRANCA"/>
        <TLCPerson eId="micheleFragali" href="/akn/ontology/person/it/micheleFragali" showAs="MICHELE FRAGALI"/>
        <TLCPerson eId="costantinoMortati" href="/akn/ontology/person/it/costantinoMortati" showAs="COSTANTINO MORTATI"/>
        <TLCPerson eId="giuseppeChiarelli" href="/akn/ontology/person/it/giuseppeChiarelli" showAs="GIUSEPPE CHIARELLI"/>
        <TLCPerson eId="giuseppeVerzì" href="/akn/ontology/person/it/giuseppeVerzì" showAs="GIUSEPPE VERZÌ"/>
        <TLCPerson eId="giovanniBattistaBenedetti" href="/akn/ontology/person/it/giovanniBattistaBenedetti" showAs="GIOVANNI BATTISTA BENEDETTI"/>
        <TLCPerson eId="francescoPaoloBonifacio" href="/akn/ontology/person/it/francescoPaoloBonifacio" showAs="FRANCESCO PAOLO BONIFACIO"/>
        <TLCPerson eId="luigiOggioni" href="/akn/ontology/person/it/luigiOggioni" showAs="LUIGI OGGIONI"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1967</akn4cc:anno>
        <akn4cc:numero>108</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1967:108</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>AMBROSINI</akn4cc:presidente>
        <akn4cc:relatore>Giovanni Battista Benedetti</akn4cc:relatore>
        <akn4cc:data_decisione>1967-06-26</akn4cc:data_decisione>
        <akn4cc:data_deposito>1967-07-12</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#gaspareAmbrosini">GASPARE AMBROSINI</judge>, Presidente - Prof. <judge refersTo="#nicolaJaeger">NICOLA JAEGER</judge> - Prof. <judge refersTo="#giovanniCassandro">GIOVANNI CASSANDRO</judge> - Dott. <judge refersTo="#antonioManca">ANTONIO MANCA</judge> - Prof. <judge refersTo="#giuseppeBranca">GIUSEPPE BRANCA</judge> - Prof. <judge refersTo="#micheleFragali">MICHELE FRAGALI</judge> - Prof. <judge refersTo="#costantinoMortati">COSTANTINO MORTATI</judge> - 
 Prof. <judge refersTo="#giuseppeChiarelli">GIUSEPPE CHIARELLI</judge> - Dott. <judge refersTo="#giuseppeVerzì">GIUSEPPE VERZÌ</judge>- Dott. <judge refersTo="#giovanniBattistaBenedetti">GIOVANNI 
 BATTISTA BENEDETTI</judge> - Prof. <judge refersTo="#francescoPaoloBonifacio">FRANCESCO PAOLO BONIFACIO</judge> - Dott. <judge refersTo="#luigiOggioni">LUIGI 
 OGGIONI</judge>, Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale  dell'<ref href="/akn/it/act/legge/stato/1965-03-30/340/!main#art_11">art.    11  della  
 legge  30  marzo  1965,  n.    340</ref>, recante "Norme concernenenti taluni  
 servizi di competenza dell'Amministrazione statale delle  antichità  e  
 delle  arti",  promosso  con  ordinanza emessa il 18 gennaio 1966 dalla  
 Corte dei conti  -  Sezione  seconda  giurisdizionale  -  nel  giudizio  
 promosso   dal   Procuratore   Generale   contro  Bartoccini  Fiorella,  
 Bartoccini Franco e Bartoccini Maria, nella loro qualità di  eredi  di  
 Bartoccini  Renato,  e  contro  Giusto  Giuseppe, iscritta al n. 54 del  
 Registro ordinanze  1966  pubblicata  nella  Gazzetta  Ufficiale  della  
 Repubblica n. 105 del 30 aprile 1966.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  
 Ministri e di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> dei Bartoccini e di Giusto Giuseppe;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udita nell'udienza pubblica del 18 maggio  1967  la  relazione  del  
 Giudice Giovanni Battista Benedetti;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>uditi  gli avvocati Stefano Giagheddu e Mario Barra Caracciolo, per  
 i Bartoccini e per il Giusto, e il sostituto  avvocato  generale  dello  
 Stato Luciano Tracanna, per il Presidente del Consiglio dei Ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto:</heading>
          <paragraph>
            <content>
              <p>Il  Procuratore Generale della Corte dei conti, con atto in data 21  
 luglio 1961, conveniva in giudizio il  Prof.  Renato  Bartoccini  e  il  
 dott.  Giuseppe  Giusto,  rispettivamente  titolare  ed  economo  della  
 Sopraintendenza alle antichità dell'Etruria meridionale,  per  sentire  
 dichiarare  il  loro  obbligo  alla resa del conto giudiziale in ordine  
 alla gestione extra- bilancio della somma di lire  4.164.000,  avvenuta  
 negli  anni  1955-56, e, in caso di mancata dichiarazione, per sentirli  
 condannare al pagamento di detta somma in favore  del  Ministero  della  
 pubblica istruzione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Deceduto il Prof. Bartoccini nelle more del giudizio, questo veniva  
 riassunto, con atto 26 aprile 1965, nei confronti degli eredi.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Sostenevano  i  convenuti  in  punto  di  diritto che la pretesa di  
 credito  dell'erario  dovesse  ritenersi  cessata  per  effetto   della  
 sopravvenuta  <ref href="/akn/it/act/legge/stato/1965-03-30/340/!main">legge  30 marzo 1965, n. 340</ref>, la quale stabilisce che non  
 vi è obbligo di dare giustificazione  mediante  la  presentazione  dei  
 conti  giudiziali  (art.  11)  di  cespiti affluiti alla gestione delle  
 Sovraintendenze in data anteriore di più di un quinquennio all'entrata  
 in vigore (11 maggio 1965) della suddetta legge. Precisavano,  inoltre,  
 che  da  un  conto  a suo tempo reso, e dal quale comunque non potevano  
 trarsi elementi a loro  carico,  la  spesa  della  somma  in  questione  
 risultava  documentata  (lire  1.500.000  erogate  in conformità della  
 volontà del donante e le  restanti  lire  2.664.000  per  fronteggiare  
 inderogabili  necessità  di  servizio),  e  soggiungevano che, in ogni  
 caso, data la insussistenza di dolo, essi convenuti non potevano essere  
 chiamati a rispondere patrimonialmente ai  sensi  del  citato  art.  11  
 della <ref href="/akn/it/act/legge/stato/1965/340/!main">legge n. 340 del 1965</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Con  ordinanza  emessa  il  18 gennaio 1966 la Corte  dei conti, in  
 accoglimento della eccezione del Pubblico  Ministero,  il  quale  aveva  
 però  prospettato la questione solo in riferimento agli artt. 3 e 103,  
 secondo  comma,  della  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>,  ha  sollevato  la  questione  di  
 illegittimità  costituzionale  della  citata  norma anche in relazione  
 agli artt. 81, primo comma, 97, primo e secondo comma, e  100,  secondo  
 comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per  quanto riguarda l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della costituzione</ref> la Corte dei conti  
 osserva che il  legislatore  del  1965,  sia  nel  limitare  all'ultimo  
 quinquennio    l'obbligo   di   dare   giustificazione,   mediante   la  
 presentazione dei conti giudiziali, delle somme gestite fuori bilancio,  
 sia nel circoscrivere la responsabilità patrimoniale alla sola ipotesi  
 di dolo, avrebbe creato, per gli  agenti  contabili  ed  ordinatori  di  
 spese  delle  gestioni  in parola, una particolare situazione di favore  
 non solo nei confronti dei contabili di fatto  e  di  diritto  e  degli  
 ordinatori  di  spese delle altre amministrazioni statali, ma anche nei  
 riguardi dei funzionari della stessa Amministrazione delle antichità e  
 belle arti che ebbero ad operare - nel medesimo periodo di tempo  -  in  
 normali  gestioni  di bilancio. Tale disparità di trattamento, secondo  
 l'ordinanza, non sarebbe sorretta da una diversità di situazioni posto  
 che la <ref href="/akn/it/act/legge/stato/1965/340/!main">legge n. 340 del  1965</ref>,  intesa  a  riportare  nell'alveo  della  
 legalità  le  gestioni  fuori  bilancio,  non solo non ha riconosciuto  
 l'esistenza di ragioni tali da giustificarle, ma, al contrario,  ne  ha  
 disposto la soppressione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Circa   la   violazione   dell'art.  103,  secondo  comma,    della  
 <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> che riserva alla Corte dei conti  la  giurisdizione  nella  
 materia  di contabilità pubblica, nonché degli artt. 81, primo comma,  
 97, primo e secondo comma, e 100, secondo comma, della  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>  i  
 cui precetti assicurano il controllo sulla gestione del pubblico denaro  
 da   parte   del  Parlamento  e  della  Corte  dei  conti  e  la  buona  
 organizzazione   degli   uffici   pubblici  e  la  responsabilità  dei  
 funzionari, l'ordinanza afferma che l'avere esonerato taluni funzionari  
 delle antichità e belle  arti  dall'obbligo  della  presentazione  dei  
 conti  e  l'avere  per  di più limitato il giudizio di responsabilità  
 alle sole ipotesi di dolo,  importa  come  conseguenza  la  sottrazione  
 delle  relative  gestioni finanziarie al controllo e alla giurisdizione  
 della Corte e quindi anche al controllo successivo del Parlamento.  Né  
 varrebbe,  per  contro,  il rilievo che il controllo previsto dall'art.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>100</num>
            <content>
              <p>, comma secondo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> si riferisce alle  gestioni  del  
 bilancio  dello  Stato  e non anche a quelle fuori bilancio perché - a  
 parte ogni  considerazione  circa  la  legittimità  costituzionale  di  
 siffatte  gestioni  (questione questa che non ha rilevanza nel presente  
 giudizio in cui si discute su gestioni non previste da leggi o da norme  
 aventi valore di  legge)  -  è  fuori  di  dubbio  che  il  rendiconto  
 consuntivo  di  cui  all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_81">art.  81  della costituzione</ref> debba comprendere  
 tutte le somme comunque riscosse ed erogate dalle amministrazioni dello  
 Stato e che, conseguentemente, il controllo e  la  giurisdizione  della  
 Corte   dei  conti  concerne  ogni  riscossione  ed  ogni  spesa  delle  
 amministrazioni stesse.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'ordinanza  è  stata   ritualmente   comunicata,   notificata   e  
 pubblicata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nel  giudizio  dinanzi  a questa Corte si sono costituite  le parti  
 private, rappresentate e difese  dagli  avvocati  Stefano  Giagheddu  e  
 Mario  Barra  Caracciolo, mediante deposito di deduzioni in cancelleria  
 in data 18 maggio 1966. È pure intervenuto il Presidente del Consiglio  
 dei Ministri col patrocinio dell'Avvocatura generale dello Stato che ha  
 depositato atto di intervento e deduzioni in data 29 aprile 1966.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La difesa delle parti  private  contesta  anzitutto  che  la  norma  
 impugnata  violi l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della costituzione</ref> e, dopo aver richiamato la  
 costante giurisprudenza della Corte in ordine  all'interpretazione  del  
 principio   di  eguaglianza,  afferma  che  nella  legge  in  esame  il  
 legislatore ha,  con  chiara  evidenza,  voluto  disciplinare  in  modo  
 particolare    un    complesso   di   situazioni   soggettivamente   ed  
 oggettivamente diverse.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Si sostiene al riguardo che le attività dell'Amministrazione delle  
 antichità e belle arti hanno un carattere del tutto particolare per le  
 esigenze alle quali hanno fatto fronte, per il processo storico che  le  
 hanno  determinate,  per  le  fonti dei cespiti gestiti, nella maggiore  
 parte non di pertinenza statale, ed infine per le ragioni  psicologiche  
 e  soggettive che hanno indotto i funzionari in parola a soddisfare con  
 questi mezzi esigenze imprescindibili, cui non sarebbe stato  possibile  
 far   fronte  con  i  fondi  di  bilancio,  cronicamente  e  gravemente  
 inadeguati.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Infondate - secondo la difesa - sarebbero anche le   altre  censure  
 di  incostituzionalità.  Insussistente  quella  dell'art.  103,  comma  
 secondo, perché la Corte costituzionale ha  già  disatteso  l'assunto  
 (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1965/17/!main">sent.  n.  17 del 1965</ref>) che la giurisdizione della Corte dei conti sia  
 assoluta ed esclusiva; in ogni caso, a parte  questo  insegnamento  pur  
 decisivo, non si vede quale incidenza possa avere nella "giurisdizione"  
 un precetto legislativo che, come quello in esame, riduca il termine di  
 prescrizione  e  limiti  le  ipotesi  di  responsabilità patrimoniale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ininfluenti sarebbero altresì  i  riferimenti  agli  artt.  81,  comma  
 primo,   97,  comma  primo  e  secondo,  e  100,  comma  secondo  della  
 <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Rileva  la  difesa  che  non  può  condividersi  la tesi sostenuta  
 nell'ordinanza che "il rendiconto consuntivo debba comprendere tutte le  
 somme comunque riscosse ed erogate dalle Amministrazioni  dello  Stato"  
 osservando in contrario che le gestioni fuori bilancio, proprio perché  
 tali,  sono estranee al bilancio e non vengono comprese né nello stato  
 di previsione, né nel rendiconto consuntivo;</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>che le fonti di entrare dello Stato sono tassativamente determinate  
 con legge mentre praeter legem sono, invece, le  somme  affluenti  alle  
 gestioni fuori bilancio.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Conclude,  pertanto,  chiedendo  che  la  Corte  voglia  dichiarare  
 l'infondatezza della denunciata questione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Anche per l'Avvocatura dello Stato la questione di illegittitimità  
 costituzionale deve ritenersi infondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Sul contrasto con il principio di eguaglianza l'Avvocatura  osserva  
 che  dal  testo  della norma impugnata e dalla relazione sul disegno di  
 legge si desumono con sufficiente chiarezza le peculiarità e i  motivi  
 speciali  della  situazione  alla  quale la norma ha inteso provvedere.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Diversità che si sostanziano: a) nella  particolare  condizione  nella  
 quale  sono  venuti  a  trovarsi gli agenti contabili, interessati alle  
 gestioni fuori bilancio, in relazione all'obbligo di presentazione  dei  
 conti  relativi  alle gestioni passate, per le quali i conti stessi non  
 si presentavano, e  quindi  non  sarebbe  stato  possibile  reperire  i  
 documenti   giustificativi;   b)   nella  necessità  di  attenuare  la  
 responsabilità di funzionari che hanno agito  quasi  sempre  in  buona  
 fede  per  salvare  tesori  dello  Stato di immenso valore culturale ed  
 economico. Orbene, ad avviso dell'Avvocatura, si tratta nella specie di  
 valutazione di situazioni speciali con conseguente speciale normazione,  
 ispirata all'intento di accordare una sanatoria a pregresse  obbiettive  
 irregolarità   formali   verificatesi  per  riconosciute  esigenze  di  
 servizio.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per quanto riguarda la pretesa  violazione  dell'art.    81,  primo  
 comma,  della  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>,  l'Avvocatura  sostiene  che per il nostro  
 ordinamento il bilancio di previsione dello Stato non contiene  "poste"  
 di entrata e "stanziamenti di spesa" relativi a gestioni fuori bilancio  
 ed il rendiconto consuntivo non contiene parimenti le somme riscosse ed  
 erogate  dalle  Amministrazioni  dello  Stato  al di fuori del bilancio  
 dello Stato. Se questa è la situazione amministrativa-contabile  delle  
 gestioni  fuori  bilancio,  ne  deriva  altresì  che nella espressione  
 "gestione del bilancio dello Stato" contenuta  nell'art.  100,  secondo  
 comma,  della  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>, con riferimento alla funzione di controllo  
 successivo della Corte dei conti, non possono comprendersi le  gestioni  
 fuori  bilancio  che,  in  effetti,  non  sono  state  soggette  a quel  
 controllo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Sotto il profilo dell'asserita violazione dell'art.   103,  secondo  
 comma,   della   <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>,  l'Avvocatura  rileva  che  la  concreta  
 disciplina del controllo giurisdizionale da parte della Corte dei conti  
 nelle materie  di  contabilità  pubblica  ed  eventualmente  in  altre  
 materie è demandata al legislatore ordinario.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Non  essendo  le gestioni fuori bilancio in questione - prima della  
 <ref href="/akn/it/act/legge/stato/1965/340/!main">legge n. 340 del 1965</ref> - previste e disciplinate da  norme  legislative,  
 appare  chiaro  che, per il passato, erano sottratte al controllo della  
 Corte dei conti.   Sotto  questo  aspetto,  l'art.  11  non  appare  in  
 contrasto  con  i  precetti  costituzionali  richiamati nell'ordinanza,  
 giacché esso estende al passato il controllo della  Corte,  altrimenti  
 non  esercitabile,  in base alla legge stessa, se non per il futuro, in  
 conseguenza della attrazione di tali gestioni nell'orbita del  bilancio  
 dello  Stato,  secondo  il principio stabilito nell'art. 1 della legge,  
 mentre  il  controllo,  per  il  passato,  non  era  stato,   comunque,  
 esercitato dalla Corte stessa.  Può quindi affermarsi che l'art. 11 ha  
 operato  una  concreta  determinazione  dei  limiti  dell'esercizio del  
 controllo e della giurisdizione della Corte dei conti  nella  sfera  di  
 discrezionalità spettante al legislatore ordinario, né può imputarsi  
 alla  <ref href="/akn/it/act/legge/stato/1965/340/!main">legge  n.    340  del  1965</ref> la precedente assenza di disposizione  
 legislative circa la disciplina delle gestioni fuori bilancio.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'Avvocatura pone inoltre in rilievo il carattere transitorio della  
 norma impugnata osservando che essa  non  vale  per  il  futuro  mentre  
 numerose sono le norme limitatrici delle responsabilità dei dipendenti  
 pubblici  che  valgono, nel nostro ordinamento, in via normale, e cioè  
 anche per l'avvenire.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Del pari inesistente sarebbe il preteso contrasto con   l'art.  97,  
 primo   e   secondo   comma,   della  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>  perché  l'art.  11  
 costituisce, se mai, un valido mezzo per la normalizzazione, sul  piano  
 della  legalità,  di  una  situazione  anomala,  assicurando  il  buon  
 andamento e l'imparzialità dell'amministrazione. La limitazione  della  
 responsabilità  contabile al quinquennio, osserva infine l'Avvocatura,  
 è conforme anche alle norme ed ai principi di diritto comune circa  la  
 normale   conservazione   dei  documenti  giustificativi  delle  spese:</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>conclude pertanto per la non fondatezza della questione di legittimità  
 costituzionale.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto:</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p> - La prima censura di incostituzionalità  mossa  dall'ordinanza  
 di  rinvio  all'<ref href="/akn/it/act/legge/stato/1965-03-30/340/!main#art_11">art.  11 della legge 30 marzo 1965, n. 340</ref>, riguarda la  
 violazione del principio  di  eguaglianza  sancito  dall'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.  3  della  
 Costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La censura non è fondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Dalla  relazione  al  disegno  di legge, presentato al Senato nella  
 seduta del 16 giugno 1964 e dalle successive  discussioni  parlamentari  
 (sedute  del  15  e  22 ottobre 1964 della VI Commissione del Senato in  
 sede deliberante), è dato dedurre quale  sia  lo  spirito  informatore  
 della   legge  in  esame  e  quali  siano  in  particolare  le  ragioni  
 giustificatrici delle limitazioni disposte dalla norma  denunciata.  Si  
 desume  anzitutto  da  tali  atti  che  i  motivi  del provvedimento si  
 riassumono  nella  urgente  necessità  di  risolvere  una   situazione  
 irregolare  in  cui  era  venuta  a  trovarsi  l'Amministrazione  delle  
 antichità e belle arti, costretta, nella mancanza di norme e strutture  
 adeguate  e  nella  persistente  scarsezza  di  fondi  di  bilancio,  a  
 ricorrere  a  forme  anomale  di  gestione  fuori  bilancio  per  poter  
 adempiere i suoi compiti istituzionali  e  soddisfare  le  complesse  e  
 crescenti  esigenze  della  propria  azione  in  difesa  del patrimonio  
 artistico  e  paesistico.  Il  legislatore,  peraltro,   legittimamente  
 preoccupato del pregiudizio e dei gravi danni che sul piano scientifico  
 e culturale sarebbero derivati dalla soppressione pura e semplice delle  
 predette  gestioni,  ritenne  necessario  dettare una disciplina che ne  
 assicurasse  la  continuità  e   pertanto   stabilì   di   ricondurle  
 nell'ambito  del  bilancio  dello  Stato,  con  la conseguente concreta  
 applicabilità, per il futuro, degli ordinari controlli previsti  dalle  
 norme  sulla contabilità generale dello Stato. L'art. 1 della legge ha  
 perciò disposto la soppressione di tutte le gestioni non  previste  da  
 norme  legislative  e  regolamentari esistenti presso l'Amministrazione  
 delle antichità e belle arti nonché il versamento  in  Tesoreria  sia  
 delle somme pertinenti alle predette gestioni, non erogate alla data di  
 pubblicazione della legge, sia di quelle conseguite dopo tale data.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per regolare poi tali gestioni per il periodo anteriore all'entrata  
 in  vigore  della  legge  è  stato  dettato l'art. 11 che limita ad un  
 quinquennio l'obbligo degli agenti contabili  di  dare  giustificazione  
 delle  loro  gestioni  mediante la presentazione dei conti giudiziali e  
 limita inoltre la loro responsabilità e  quella  degli  ordinatori  di  
 spese ai danni arrecati all'Erario imputabili a dolo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>I  motivi  di  questo  differenziato trattamento emergono con tutta  
 evidenza dai citati lavori preparatori nei quali può leggersi  che  la  
 norma  risponde  ad evidenti ragioni di equità vuoi per circostanze di  
 carattere obbiettivo, quali le particolari  ed  effettive  esigenze  di  
 servizio  che  dettero  vita alle gestioni fuori bilancio e gli indubbi  
 notevoli  vantaggi  che  esse  hanno  procurato  allo  Stato,  vuoi  di  
 carattere soggettivo, perché - si afferma - "sarebbe non solo disumano  
 ma   controproducente   nell'interesse   della   collettività   se  si  
 continuasse  a  mantenere  nello  stato  di   disagio   e   apprensione  
 moltissimi,  ottimi  funzionari  che  hanno  solo  la  colpa  di  avere  
 anteposto al regolamento di  contabilità  generale  la  necessità  di  
 salvare tesori di immenso valore culturale ed economico".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il  legislatore  ha  altresì  chiarito lo scopo della disposizione  
 precisando che esso consiste nella sanatoria di  irregolarità  formali  
 la   quale   -  lungi  dal  voler  tradurre  in  norma  una  situazione  
 antigiuridica - mira solo a riconoscere  le  esigenze  particolari  che  
 l'hanno  provocata  e  che  ne  costituiscono la base. Contrariamente a  
 quanto  sostenuto  nell'ordinanza    può  quindi  affermarsi  che   il  
 legislatore, nella specie, ha voluto dettare, dopo meditate e ponderate  
 discussioni,   una  disciplina  diversa,  implicante  un  differenziato  
 trattamento,  per  regolare  situazioni  particolari  di  una  speciale  
 categoria  di  ordinatori  di  spese e di contabili.  La valutazione di  
 tali situazioni,  per  come  risulta  dall'indagine  compiuta,  non  è  
 arbitraria ma appare per contro sorretta da criteri logici e razionali,  
 e  ciò  è  sufficiente  per escludere che la norma impugnata urti col  
 principio di eguaglianza enunciato nell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p> - Del pari infondate  sono  le  censure  di  incostituzionalità  
 sollevate  in  riferimento  agli  artt.  81,  comma  primo,  100, comma  
 secondo, e 103, comma secondo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per quanto riguarda la pretesa violazione dei  primi  due  precetti  
 costituzionali la Corte osserva che essi sono indubbiamente ispirati al  
 principio del controllo del Parlamento e della Corte dei conti su tutta  
 la  gestione finanziaria dello Stato e che per conseguenza nel bilancio  
 di previsione e nel rendiconto consuntivo dovrebbe essere compresa ogni  
 entrata ed ogni spesa a qualsiasi titolo introitata ed erogata.    Ora,  
 la  legge  nella quale è contenuta la norma impugnata, proprio perché  
 ha ricondotto nell'ambito del bilancio dello Stato  le  gestioni  fuori  
 bilancio   dell'Amministrazione  delle  antichità  e  belle  arti  non  
 autorizzate da leggi e regolamenti,  è  da  considerarsi  conforme  al  
 principio sopra ricordato.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Va   peraltro   rilevato   che   la   questione   di   legittimità  
 costituzionale  proposta  investe  le  disposizioni  dell'art.  11  che  
 riguardano  i  giudizi  di  conto  e  di responsabilità amministrativa  
 relativi alle gestioni fuori bilancio onde più pertinente va  ritenuto  
 il  riferimento  all'art.  103,  comma  secondo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> che  
 tratta  appunto  della  giurisdizione  della  Corte  nelle  materie  di  
 contabilità pubblica e nelle altre specificate dalla legge.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La  norma  denunciata ha per di più carattere transitorio giacché  
 la limitazione al  quinquennio  dell'obbligo  della  presentazione  dei  
 conti  giudiziali  e  la  limitazione  della  responsabilità  ai danni  
 imputabili a dolo è stata disposta  solo  per  il  periodo  precedente  
 all'entrata in vigore della legge.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Naturalmente  la  responsabilità  per dolo sia degli ordinatori di  
 spese che degli agenti contabili può essere  accertata  anche  per  il  
 periodo anteriore al quinquennio dato che il diritto al risarcimento si  
 estingue  solo  con  il  decorso  del termine ordinario di prescrizione  
 previsto dal <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">codice civile</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Non  è  quindi  esatta   l'affermazione   che   la   norma   abbia  
 completamente   sottratto  le  gestioni  fuori  bilancio  al  sindacato  
 giurisdizionale, ma è vero, per contro, che essa - in via  transitoria  
 e  per  situazioni  meritevoli  di  un  differenziato  trattamento - ha  
 soltanto limitato l'estensione e le modalità di esercizio  del  potere  
 giurisdizionale  che  la  Corte dei conti ha nella materia in base alle  
 comuni norme sulla contabilità generale dello Stato e sull'ordinamento  
 della Corte.  Ed è fuor di dubbio  che  ciò  il  legislatore  potesse  
 fare,  posto  che la disciplina concreta della funzione giurisdizionale  
 contabile è demandata proprio al legislatore ordinario.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p> - Ha infine rilevato l'ordinanza che la  disposizione    di  cui  
 trattasi, dispensando i funzionari dello Stato dal dare giustificazione  
 di  somme  gestite  ed esonerandoli da qualsiasi responsabilità, anche  
 nella ipotesi di colpa grave, è in contrasto col primo e secondo comma  
 dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 della Costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Questa Corte ritiene che nessun pregiudizio  derivi    dalla  norma  
 impugnata    ai   principi   del   buon   andamento   e   imparzialità  
 dell'Amministrazione e della responsabilità dei funzionari.  La  norma  
 è,  se  mai,  in  armonia  con tali principi perché con essa è stata  
 determinata la responsabilità patrimoniale di una  data  categoria  di  
 funzionari  sia pure nei limiti in cui la responsabilità stessa poteva  
 essere  ammessa  in  relazione  alle  obbiettive  particolarità  della  
 situazione che il legislatore ha inteso normalizzare.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara  non  fondata  la questione di legittimità costituzionale  
 dell'<ref href="/akn/it/act/legge/stato/1965-03-30/340/!main#art_11">art. 11  della  legge  30  marzo  1965,  n.  340</ref>,  recante  "Norme  
 concernenti  taluni  servizi di competenza dell'Amministrazione statale  
 delle antichità e delle arti", sollevata con <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1966-01-08/1/!main">ordinanza 18 gennaio 1966</ref>  
 della Corte dei conti, in riferimento agli artt. 3,  comma  primo,  81,  
 comma  primo,  97,  comma  primo  e secondo, 100, comma secondo, e 103,  
 comma secondo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1967-06-26">26 giugno 1967</date>.</block>
      <p><signature><judge refersTo="#gaspareAmbrosini">GASPARE  AMBROSINI</judge></signature>  - <signature><judge refersTo="#nicolaJaeger">NICOLA JAEGER</judge></signature> -  
                                   <signature><judge refersTo="#giovanniCassandro">GIOVANNI CASSANDRO</judge></signature> - <signature><judge refersTo="#antonioManca">ANTONIO MANCA</judge></signature>  -  
                                   <signature><judge refersTo="#giuseppeBranca">GIUSEPPE  BRANCA</judge></signature>  - <signature><judge refersTo="#micheleFragali">MICHELE FRAGALI</judge></signature> -  
                                   <signature><judge refersTo="#costantinoMortati">COSTANTINO   MORTATI</judge></signature>    -    <signature><judge refersTo="#giuseppeChiarelli">GIUSEPPE  
                                   CHIARELLI</judge></signature>   -   <signature><judge refersTo="#giuseppeVerzì">GIUSEPPE    VERZÌ</judge></signature>   -  
                                   <signature><judge refersTo="#giovanniBattistaBenedetti">GIOVANNI    BATTISTA    BENEDETTI</judge></signature>   -  
                                   <signature><judge refersTo="#francescoPaoloBonifacio">FRANCESCO  PAOLO  BONIFACIO</judge></signature>  -  <signature><judge refersTo="#luigiOggioni">LUIGI  
                                   OGGIONI</judge></signature>.</p>
    </conclusions>
  </judgment>
</akomaNtoso>
