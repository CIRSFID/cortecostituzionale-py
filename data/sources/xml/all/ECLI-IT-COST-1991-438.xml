<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>438</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:438</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/12/1991</data_decisione>
    <data_deposito>09/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, prof. &#13;
 Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio promosso con ricorso della Regione Friuli-Venezia Giulia    &#13;
 notificato  il 19 aprile 1991, depositato in Cancelleria il 23 aprile    &#13;
 successivo, per conflitto  di  attribuzione  sorto  a  seguito  della    &#13;
 lettera  circolare  della  Soprintendenza  archeologica  e per i beni    &#13;
 ambientali, architettonici, artistici e storici del  Friuli-  Venezia    &#13;
 Giulia  -  Ufficio  staccato  di Udine - n. 781/66/30 del 15 febbraio    &#13;
 1991 con  cui  è  stata  disposta  la  disapplicazione  della  legge    &#13;
 regionale  13  dicembre 1989, n. 36 ed iscritto al n. 24 del registro    &#13;
 conflitti 1991;                                                          &#13;
    Udito nell'udienza pubblica del 9 luglio 1991 il Giudice  relatore    &#13;
 Antonio Baldassarre;                                                     &#13;
    Udito  l'Avvocato  Gaspare  Pacia  per  la  Regione Friuli-Venezia    &#13;
 Giulia;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - La Regione Friuli-Venezia Giulia ha  sollevato  conflitto  di    &#13;
 attribuzione  nei  confronti  dello  Stato in relazione alla lettera-circolare  15  febbraio  1991,  n.  781/66/30,  della  Soprintendenza    &#13;
 archeologica  e  per  i  beni ambientali, architettonici, artistici e    &#13;
 storici del Friuli- Venezia Giulia - Ufficio staccato di  Udine,  con    &#13;
 la  quale è stata adottata una direttiva interpretativa del seguente    &#13;
 tenore: nonostante l'emanazione della  legge  della  Regione  Friuli-Venezia Giulia 13 dicembre 1989, n. 36, "rimane l'obbligo di ottenere    &#13;
 il  necessario nulla osta ai sensi dell'art. 7 della legge n. 1497/39    &#13;
 e non viene meno la necessità di  dare  immediata  comunicazione  al    &#13;
 Ministero   beni   culturali   e   ambientali   delle  autorizzazioni    &#13;
 rilasciate".                                                             &#13;
    Secondo la ricorrente -  al  di  là  di  qualsiasi  questione  di    &#13;
 legittimità  costituzionale  relativa alla legge regionale n. 36 del    &#13;
 1989, che ha sostituito le autorizzazioni ministeriali  previste  dal    &#13;
 decreto-legge n. 312 del 1985, come convertito nella legge n. 431 del    &#13;
 1985,   con   analoghi   provvedimenti   regionali   -  la  direttiva    &#13;
 interpretativa contenuta nella circolare impugnata avrebbe  l'effetto    &#13;
 di   produrre   una   inammissibile   disapplicazione,  con  un  atto    &#13;
 amministrativo statale, di una legge  regionale.  Come  tale,  l'atto    &#13;
 impugnato,   di   cui   la   ricorrente   chiede   consequenzialmente    &#13;
 l'annullamento, lederebbe i seguenti parametri costituzionali: a) gli    &#13;
 artt. 4, 5 e 6 dello Statuto speciale per il  Friuli-Venezia  Giulia,    &#13;
 dal momento che la suddetta circolare ostacolerebbe l'applicazione di    &#13;
 una legge adottata sulla base degli articoli statutari appena citati;    &#13;
 b)  l'art.  8  dello  stesso  Statuto,  in  quanto analogo effetto si    &#13;
 produrrebbe nei confronti delle  funzioni  amministrative  attribuite    &#13;
 alla  ricorrente dalla suddetta disposizione statutaria; c) l'art. 31    &#13;
 dello Statuto, il quale prevede che ogni legge regionale  sia  munita    &#13;
 della  formula  promulgativa  e  di  quella esecutiva, poiché l'atto    &#13;
 impugnato contrasterebbe con l'obbligo ivi contenuto di osservare  la    &#13;
 legge regionale e di farla osservare.                                    &#13;
    2.  - Non si è costituito in giudizio il Presidente del Consiglio    &#13;
 dei Ministri.<diritto>Considerato in diritto</diritto>1. - La Regione Friuli-Venezia Giulia con il ricorso  indicato  in    &#13;
 epigrafe  ha  sollevato  conflitto  di attribuzione nei confronti del    &#13;
 Ministro per i beni culturali e ambientali in relazione alla lettera-circolare 15 febbraio 1991 (pervenuta alla  Presidenza  della  Giunta    &#13;
 regionale  il  26  febbraio  1991),  con  la  quale la Soprintendenza    &#13;
 archeologica e per i beni  ambientali,  architettonici,  artistici  e    &#13;
 storici  del  Friuli-Venezia  Giulia,  Ufficio  staccato di Udine, ha    &#13;
 affermato  che  "anche  con  l'adozione  di   particolari   strumenti    &#13;
 urbanistici di valenza ambientale denominati piani di conservazione e    &#13;
 di  sviluppo'  ( ..) rimane l'obbligo di ottenere il necessario nulla    &#13;
 osta ai sensi dell'art. 7 della legge 1497/39 e  non  viene  meno  la    &#13;
 necessità   di   dare  immediata  comunicazione  al  Ministero  beni    &#13;
 culturali e ambientali delle autorizzazioni rilasciate".  Poiché,  a    &#13;
 suo  giudizio,  tale  circolare  mira  sostanzialmente a disporre una    &#13;
 disapplicazione della legge regionale n. 36 del 1989 e  a  ostacolare    &#13;
 le  relative  funzioni  amministrative,  la ricorrente chiede che sia    &#13;
 ripristinata l'integrità delle attribuzioni ad essa riservate  dagli    &#13;
 artt.  4, 5, 6, 8 e 31 del proprio Statuto e sia, pertanto, annullato    &#13;
 l'atto impugnato.                                                        &#13;
    2. - Il ricorso è inammissibile.                                     &#13;
    Per effetto della sentenza di questa Corte n. 437 del 1991, che ha    &#13;
 pronunziato l'illegittimità  costituzionale  in  via  consequenziale    &#13;
 dell'art. 1 della legge della Regione Friuli-Venezia Giulia n. 36 del    &#13;
 1989,  viene  meno l'interesse a ricorrere di quest'ultima Regione in    &#13;
 relazione al conflitto di  attribuzione  sollevato,  con  il  ricorso    &#13;
 indicato in epigrafe, nei confronti del Ministro per i beni culturali    &#13;
 e ambientali.                                                            &#13;
    La   ricorrente,  infatti,  lamentava  la  lesione  delle  proprie    &#13;
 competenze in conseguenza del fatto che la circolare  impugnata,  nel    &#13;
 mantenere  ferme  l'autorizzazione prevista all'art. 7 della legge n.    &#13;
 1497 del 1939 e la necessità di dare comunicazione al Ministro per i    &#13;
 beni  culturali  e   ambientali   delle   autorizzazioni   rilasciate    &#13;
 "nonostante   l'emanazione  della  Legge  regionale  36/89",  avrebbe    &#13;
 prodotto, in sostanza, una disapplicazione di quest'ultima legge  con    &#13;
 conseguente   intralcio   delle  funzioni  amministrative  da  quella    &#13;
 dipendenti.                                                              &#13;
    La dichiarazione d'illegittimità costituzionale della legge n. 36    &#13;
 del  1989  proprio  in  relazione  ai   poteri   ministeriali   sopra    &#13;
 menzionati,  operata  con  la  citata  sentenza  n. 437 del 1991, fa,    &#13;
 dunque, venir meno la  possibilità  della  lamentata  lesione  delle    &#13;
 competenze  regionali.  Il  conflitto  di  attribuzione  in esame va,    &#13;
 pertanto,  dichiarato  inammissibile  per  sopravvenuta  mancanza  di    &#13;
 interesse della Regione ricorrente.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile,  per mancanza di interesse a ricorrere, il    &#13;
 ricorso per conflitto di attribuzione proposto dalla Regione  Friuli-Venezia  Giulia  nei  confronti  del  Ministro per i beni culturali e    &#13;
 ambientali, in relazione alla lettera-  circolare  15  febbraio  1991    &#13;
 della   Soprintendenza   archeologica   e   per  i  beni  ambientali,    &#13;
 architettonici, artistici  e  storici  del  Friuli-  Venezia  Giulia,    &#13;
 Ufficio staccato di Udine.                                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 2 dicembre 1991.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: BALDASSARRE                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 9 dicembre 1991.                         &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
