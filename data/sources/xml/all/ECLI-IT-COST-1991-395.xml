<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>395</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:395</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1991</data_decisione>
    <data_deposito>31/10/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 1, secondo    &#13;
 comma, del regio decreto  16  marzo  1942,  n.  267  (Disciplina  del    &#13;
 fallimento,    del    concordato   preventivo,   dell'amministrazione    &#13;
 controllata e della liquidazione coatta amministrativa), promosso con    &#13;
 ordinanza emessa l'8  gennaio  1991  dal  Tribunale  di  Bologna  sul    &#13;
 ricorso  proposto  da  s.n.c.  Autotre  Ricambi  ed  altra  c/ s.d.f.    &#13;
 Carrozzeria Caselle  ed  altri,  iscritta  al  n.  310  del  registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 18, prima serie speciale, dell'anno 1991;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  10  luglio  1991  il  Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  il  Tribunale  di Bologna, nel giudizio promosso da    &#13;
 s.c.n. Autotre Ricambi contro la s.d.f. Carrozzeria Caselle ed altri,    &#13;
 costituita tra artigiani,  per  la  dichiarazione  di  fallimento  di    &#13;
 questa  ultima,  con  ordinanza  dell'8 gennaio 1991 (R.O. n. 310 del    &#13;
 1991),  ha  sollevato  questione  di   legittimità   costituzionale,    &#13;
 dell'art.  1, secondo comma, del regio decreto 16 marzo 1942, n. 267,    &#13;
 nella parte in cui esclude che siano considerate piccoli imprenditori    &#13;
 le società artigiane  non  costituite  nella  forma  di  società  a    &#13;
 responsabilità limitata;                                                &#13;
      che, a parere del remittente, sarebbero violati:                    &#13;
        a)   l'art.   3  della  Costituzione,  per  la  disparità  di    &#13;
 trattamento che si verifica tra i piccoli imprenditori artigiani e le    &#13;
 società artigiane di modeste dimensioni come quella in controversia;    &#13;
        b) l'art. 24, secondo comma, della Costituzione, in quanto non    &#13;
 sarebbe possibile  alla  società  artigiana  di  modeste  dimensioni    &#13;
 provare  la  possidenza  dei  requisiti richiesti per la qualifica di    &#13;
 piccolo imprenditore;                                                    &#13;
        c) l'art. 45, secondo comma, della Costituzione, in  combinato    &#13;
 disposto  con  gli  artt.  3  e  35 della Costituzione, in quanto non    &#13;
 sarebbe consentito alla società artigiana di modeste  dimensioni  di    &#13;
 provare  la  prevalenza  del lavoro dei soci sul capitale, nonostante    &#13;
 l'avvenuta iscrizione nell'albo delle imprese artigiane;                 &#13;
      che l'Avvocatura Generale dello Stato, intervenuta nel  giudizio    &#13;
 in  rappresentanza  del  Presidente  del  Consiglio  dei ministri, ha    &#13;
 concluso per la inammissibilità della questione;                        &#13;
    Considerato che questa Corte, con sentenza n. 54 del 1991, ha già    &#13;
 dichiarato la illegittimità costituzionale della norma ora di  nuovo    &#13;
 denunciata;                                                              &#13;
      che, pertanto, essa è ormai espunta dall'ordinamento giuridico;    &#13;
      che,   quindi,   la   questione   sollevata   deve   dichiararsi    &#13;
 manifestamente infondata;                                                &#13;
    Visti gli art. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 1,  secondo  comma,  del  regio    &#13;
 decreto  16  marzo  1942,  n.  267  (Disciplina  del  fallimento, del    &#13;
 concordato  preventivo,  dell'amministrazione  controllata  e   della    &#13;
 liquidazione coatta amministrativa), in riferimento agli artt. 3, 24,    &#13;
 secondo   comma,   35,  primo  comma,  e  45,  secondo  comma,  della    &#13;
 Costituzione, sollevata dal Tribunale di Bologna con  l'ordinanza  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1991.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 31 ottobre 1991.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
