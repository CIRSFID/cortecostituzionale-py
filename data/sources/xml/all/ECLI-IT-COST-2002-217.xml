<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>217</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:217</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia>Annibale Marini</redattore_pronuncia>
    <data_decisione>20/05/2002</data_decisione>
    <data_deposito>23/05/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 79 del decreto del Presidente della Repubblica 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), come sostituito dall'art. 16 del decreto legislativo 26 febbraio 1999, n. 46 (Riordino della disciplina della riscossione mediante ruolo, a norma dell'articolo 1 della legge 28 settembre 1998, n. 337), in relazione all'art. 52, comma 4, del decreto del Presidente della Repubblica 26 aprile 1986, n. 131 (Approvazione del testo unico delle disposizioni concernenti l'imposta di registro), promosso con ordinanza emessa il 14 maggio 2001 dal Tribunale di Forlì nel procedimento civile Grandini Ilario contro il Concessionario per la riscossione dei tributi (CO.RI.T.) per la Provincia di Forlì - Cesena e Rimini s.p.a., iscritta al n. 583 del registro ordinanze 2001 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 33, prima serie speciale, dell'anno 2001. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 10 aprile 2002 il Giudice relatore Annibale Marini. &#13;
    Ritenuto che, con ordinanza del 14 maggio 2001, depositata il 17 maggio 2001, il giudice dell'esecuzione del Tribunale di Forlì ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 79 del decreto del Presidente della Repubblica 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), come sostituito dall'art. 16 del decreto legislativo 26 febbraio 1999, n. 46 (Riordino della disciplina della riscossione mediante ruolo, a norma dell'articolo 1 della legge 28 settembre 1998, n. 337), «nella parte in cui non prevede la possibilità di accertamento del valore commerciale dei beni immobili sottoposti ad esecuzione esattoriale anche quando il calcolo aritmetico o riferito alla rendita catastale determini un valore notevolmente inferiore a quello reale»; &#13;
    che, ad avviso del rimettente, la norma denunciata -secondo la quale il prezzo base dell'incanto è pari all'importo stabilito a norma dell'art. 52, comma 4, del testo unico delle disposizioni concernenti l'imposta di registro, approvato con decreto del Presidente della Repubblica 26 aprile 1986, n. 131 -  renderebbe possibile la vendita all'incanto dei beni pignorati ad un prezzo base di gran lunga inferiore al loro valore di commercio; &#13;
    che sotto tale profilo la norma si porrebbe in contrasto con l'art. 3 della Costituzione per la palese disparità di trattamento che introdurrebbe tra il cittadino sottoposto a procedura esecutiva ordinaria e quello sottoposto a procedura esecutiva per debiti tributari; &#13;
    che la possibilità di vendita del bene ad un prezzo inferiore al valore di mercato sarebbe, d'altro canto, suscettibile di danneggiare non soltanto il debitore ma anche lo stesso ente impositore, in tal modo maggiormente esposto al rischio di incapienza; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la declaratoria di non fondatezza della questione; &#13;
    che, ad avviso della parte pubblica, la norma impugnata troverebbe una ragionevole giustificazione nelle esigenze di celerità del soddisfacimento del credito d'imposta, derivanti dalla necessità di assicurare la regolarità del gettito delle entrate tributarie; &#13;
    che, sotto altro aspetto, la novella del 1999, richiamando per la valutazione automatica del bene il medesimo criterio utilizzato ai fini impositivi, risponderebbe anche ad evidenti ragioni sistematiche. &#13;
    Considerato che il rimettente dubita, in riferimento all'art. 3 della Costituzione, della legittimità costituzionale dell'art. 79 del decreto del Presidente della Repubblica 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), come sostituito dall'art. 16 del decreto legislativo 26 febbraio 1999, n. 46 (Riordino della disciplina della riscossione mediante ruolo, a norma dell'articolo 1 della legge 28 settembre 1998, n. 337), in quanto non prevede - diversamente dalle norme che regolano l'esecuzione ordinaria - alcuna possibilità di accertamento del valore venale dei beni pignorati ai fini della fissazione del prezzo base dell'incanto; &#13;
    che il procedimento amministrativo di riscossione coattiva delle imposte non pagate, improntato a criteri di semplicità e di speditezza, risponde all'esigenza di pronta realizzazione del credito fiscale a garanzia del regolare svolgimento della vita finanziaria dello Stato (sentenze n. 351 del 1998, n. 415 del 1996, n. 444 del 1995 e n. 358 del 1994; ordinanza n. 455 del 2000); &#13;
    che non può, quindi, ritenersi ingiustificata la diversità di disciplina, rispetto all'esecuzione ordinaria, riguardante le modalità di determinazione del prezzo base dell'incanto, in quanto il riferimento, in ogni caso, al valore catastale dell'immobile, con esclusione di qualsiasi indagine - e possibili contestazioni - riguardo all'effettivo valore commerciale del bene, è pienamente coerente con l'indicata finalità di tempestiva riscossione dei crediti tributari e d'altro canto si fonda su una ragionevole presunzione di congruità del suddetto valore catastale; &#13;
    che questa Corte ha dichiarato non fondate questioni sostanzialmente analoghe a quella in esame, riferite alla previgente disciplina, di non difforme contenuto precettivo (sentenze n. 66 del 1986 e n. 83 del 1966); &#13;
    che la questione va, pertanto, dichiarata manifestamente infondata. &#13;
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 79 del decreto del Presidente della Repubblica 29 settembre 1973, n. 602 (Disposizioni sulla riscossione delle imposte sul reddito), come sostituito dall'art. 16 del decreto legislativo 26 febbraio 1999, n. 46 (Riordino della disciplina della riscossione mediante ruolo, a norma dell'articolo 1 della legge 28 settembre 1998, n. 337), sollevata, in riferimento all'art. 3 della Costituzione, dal giudice dell'esecuzione del Tribunale di Forlì. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 maggio 2002. &#13;
F.to: &#13;
Cesare RUPERTO, Presidente &#13;
Annibale MARINI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 23 maggio 2002. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
