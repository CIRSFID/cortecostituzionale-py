<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>182</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:182</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/05/1998</data_decisione>
    <data_deposito>20/05/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 5-bis del d.-l.    &#13;
 21 settembre 1987, n. 387  (Copertura  finanziaria  del  decreto  del    &#13;
 Presidente  della  Repubblica  10  aprile 1987, n. 150, di attuazione    &#13;
 dell'accordo  contrattuale  triennale  relativo  al  personale  della    &#13;
 Polizia  di  Stato  ed  estensione  agli  altri  Corpi  di  polizia),    &#13;
 convertito in legge 20 novembre 1987, n. 472, promosso con  ordinanza    &#13;
 emessa il 12 dicembre 1996 dal tribunale amministrativo regionale del    &#13;
 Lazio  sul ricorso proposto da Colomela Mario Carmelo contro la Cassa    &#13;
 Depositi e Prestiti  ed  altro,  iscritta  al  n.  179  del  registro    &#13;
 ordinanze 1997 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 16, prima serie speciale, dell'anno 1997.                             &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di  consiglio  del  7  aprile  1998  il  giudice    &#13;
 relatore Massimo Vari.                                                   &#13;
   Ritenuto  che,  con  ordinanza  emessa il 12 dicembre 1996 (r.o. n.    &#13;
 179 del 1997), il tribunale amministrativo regionale del Lazio -  nel    &#13;
 corso   del   giudizio   proposto  da  Colomela  Mario  Carmelo,  per    &#13;
 l'annullamento del   provvedimento con il quale,  in  conformità  al    &#13;
 parere  negativo  espresso  dal Comitato per le pensioni privilegiate    &#13;
 ordinarie, ed in difformità dal parere positivo del Collegio  medico    &#13;
 dell'Ospedale militare di Roma, era stata rigettata la sua domanda di    &#13;
 concessione  dell'equo  indennizzo per infermità dipendente da causa    &#13;
 di servizio - ha sollevato, in riferimento agli artt. 3  e  97  della    &#13;
 Costituzione,  questione  di  legittimità  costituzionale  dell'art.    &#13;
 5-bis del d.-l.  21 settembre 1987, n. 387 (Copertura finanziaria del    &#13;
 decreto del Presidente della Repubblica 10 aprile 1987,  n.  150,  di    &#13;
 attuazione  dell'accordo contrattuale triennale relativo al personale    &#13;
 della Polizia di Stato ed estensione agli altri  Corpi  di  polizia),    &#13;
 convertito  nella legge 20 novembre 1987, n. 472, il quale prevede la    &#13;
 definitività  del   parere   espresso   dalla   Commissione   medica    &#13;
 ospedaliera,  ai  fini  del  riconoscimento  delle  infermità per la    &#13;
 dipendenza da causa di servizio, salvo il parere del Comitato per  le    &#13;
 pensioni  privilegiate  ordinarie solo in sede di  liquidazione della    &#13;
 pensione privilegiata e dell'equo indennizzo;                            &#13;
     che il giudice rimettente -  nel  rilevare  che  la  disposizione    &#13;
 impugnata ha "introdotto una disciplina  giuridica del riconoscimento    &#13;
 della  dipendenza  da  causa  di  servizio  di una infermità e delle    &#13;
 menomazioni                                                              &#13;
  d'integrità fisica connesse, comportante la  possibile  coesistenza    &#13;
 dell'affermazione  e  della negazione di tale dipendenza in relazione    &#13;
 all'una   o   all'altra    delle    misure    riparatorie    previste    &#13;
 dall'ordinamento" - considera evidente la "disparità di trattamento"    &#13;
 insita  nel  fatto che a certi fini l'infermità del dipendente sarà    &#13;
 dovuta a causa di servizio, mentre non lo sarà per altri;               &#13;
     che altrettanto evidente sarebbe - ad avviso dell'ordinanza -  la    &#13;
 violazione  del  principio  di buon andamento "il quale indubbiamente    &#13;
 richiede l'emissione di provvedimenti che qualifichino una situazione    &#13;
 una volta per tutte".                                                    &#13;
   Considerato  che  la questione, nei termini in cui viene sollevata,    &#13;
 ha già formato oggetto di esame da parte  della  Corte,  che  si  è    &#13;
 pronunziata  per  la sua non fondatezza (sentenza n. 209 del 1996) e,    &#13;
 successivamente, per la sua manifesta infondatezza (ordinanze n.  323    &#13;
 del 1996 e n. 150 del 1997);                                             &#13;
     che  l'ordinanza  in  epigrafe  non introduce profili o argomenti    &#13;
 nuovi rispetto a quelli  già  esaminati  dalla  Corte  o,  comunque,    &#13;
 suscettibili di indurre a diverso avviso, sicché la questione stessa    &#13;
 va dichiarata manifestamente infondata.                                  &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 5-bis del d.-l. 21 settembre  1987,  n.  387    &#13;
 (Copertura finanziaria del decreto del Presidente della Repubblica 10    &#13;
 aprile   1987,   n.  150,  di  attuazione  dell'accordo  contrattuale    &#13;
 triennale relativo al personale della Polizia di Stato ed  estensione    &#13;
 agli  altri  Corpi  di  polizia),  convertito nella legge 20 novembre    &#13;
 1987, n. 472, sollevata, in riferimento  agli  artt.  3  e  97  della    &#13;
 Costituzione,  dal  tribunale  amministrativo regionale del Lazio con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 maggio 1998.                                 &#13;
                         Il Presidente: Granata                           &#13;
                           Il redattore: Vari                             &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 20 maggio 1998.                           &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
