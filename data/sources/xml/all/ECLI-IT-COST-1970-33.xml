<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>33</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:33</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Enzo Capolozza</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/02/1970</data_decisione>
    <data_deposito>04/03/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale  dell'art.    92  &#13;
 del codice penale, promossi con le seguenti ordinanze:                   &#13;
     1)  ordinanza  emessa  il  1  marzo  1969  dal  pretore di Roma nel  &#13;
 procedimento penale a carico di Baldini Pio, iscritta  al  n.  188  del  &#13;
 registro  ordinanze  1969  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 145 dell' 11 giugno 1969;                                  &#13;
     2) ordinanza emessa il 3 marzo 1969 dalla Corte d'assise di  Padova  &#13;
 nel  procedimento penale a carico di Piovan Giorgio, iscritta al n. 313  &#13;
 del registro ordinanze 1969 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 207 del 13 agosto 1969;                                    &#13;
     3) ordinanza emessa il 10 settembre 1969 dal tribunale  di  Livorno  &#13;
 nel  procedimento  penale a carico di Ventura Francesco, iscritta al n.  &#13;
 397 del registro ordinanze 1969 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n.  280 del 5 novembre 1969.                            &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udito nell'udienza pubblica del 14 gennaio 1970 il Giudice relatore  &#13;
 Enzo Capalozza;                                                          &#13;
     udito  il  sostituto  avvocato  generale   dello   Stato   Giovanni  &#13;
 Albisinni, per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Nel corso di un procedimento penale promosso con il rito del  &#13;
 giudizio direttissimo a carico di Pio  Baldini,  per  oltraggio  ad  un  &#13;
 agente di pubblica sicurezza, il pretore di Roma, essendo risultato che  &#13;
 l'imputato aveva commesso il fatto in istato di ebbrezza alcoolica, con  &#13;
 ordinanza  del  1  marzo  1969,  sollevava  questione  di  legittimità  &#13;
 costituzionale dell'art. 92 del codice penale, in riferimento  all'art.  &#13;
 27 della Costituzione.                                                   &#13;
     Osserva  il  pretore  che la norma denunziata, nel tener ferma - in  &#13;
 deroga  al  principio  fissato  nell'art.  85  del  codice   penale   -  &#13;
 l'imputabilità   per   i  reati  commessi  in  istato  di  ubriachezza  &#13;
 volontaria o colposa, escluderebbe ogni  indagine  sulla  capacità  di  &#13;
 intendere  e  di  volere  dell'imputato,  e  violerebbe il canone della  &#13;
 responsabilità personale in materia penale, espresso nel  primo  comma  &#13;
 del ridetto art. 27 e risultante anche dal coordinamento di tale canone  &#13;
 con altri accolti nella Costituzione.                                    &#13;
     Deduce,  al  riguardo, che nella nozione di responsabilità penale,  &#13;
 si è sempre presupposta e ricompresa la capacità di  intendere  e  di  &#13;
 volere  e  che  è  stata, come tale, assunta ed intesa dal legislatore  &#13;
 costituente,   il   quale    avrebbe    ulteriormente    limitato    la  &#13;
 discrezionalità    del   legislatore   ordinario   quanto   alla   sua  &#13;
 configurazione, sia con il riconoscimento e  la  garanzia  dei  diritti  &#13;
 inviolabili dell'uomo - fra i quali sarebbe da annoverare quello di non  &#13;
 essere  punito  quando, al momento della condotta addebitata, manchi la  &#13;
 capacità di autodeterminazione - sia con l'affermazione del  principio  &#13;
 di  eguaglianza, che non consentirebbe, in tema di ubriachezza - che è  &#13;
 tale da annullare o quanto meno da scemare la capacità di intendere  e  &#13;
 di  volere  - di discriminare tra l'ubriachezza fortuita, che, ai sensi  &#13;
 dell'art. 91 del codice penale, consente l'indagine su tale  capacità,  &#13;
 e l'ubriachezza volontaria o colposa che, invece, non la consente e che  &#13;
 conduce automaticamente a porre a carico dell'ebbro il reato commesso.   &#13;
     Ricorda,  inoltre,  il  pretore  il recente progetto di riforma del  &#13;
 codice penale che configura l'ubriachezza come attenuante; fa  presente  &#13;
 che,   secondo   un   affermato  orientamento  giurisprudenziale  della  &#13;
 cassazione, la norma denunziata,  in  deroga  all'art.  42  del  codice  &#13;
 penale,  escluderebbe  pure  l'indagine  sulla  coscienza e volontà; e  &#13;
 ribadisce, infine, la  violazione  dell'art.  27,  primo  comma,  della  &#13;
 Costituzione,  secondo  l'interpretazione  datane  da  questa  Corte, e  &#13;
 risultante anche dagli altri due principi espressi nel secondo e  terzo  &#13;
 comma  dello  stesso  art.  27, i quali presuppongono la partecipazione  &#13;
 psichica dell'autore al fatto delittuoso.                                &#13;
     L'ordinanza,  ritualmente  notificata  e   comunicata,   è   stata  &#13;
 pubblicata nella Gazzetta Ufficiate n. 145 dell' 11 giugno 1969.         &#13;
     Nel  giudizio  innanzi  a questa Corte è intervenuto il Presidente  &#13;
 del Consiglio dei  Ministri,  rappresentato  e  difeso  dall'Avvocatura  &#13;
 generale  dello  Stato, con atto depositato il 1 luglio 1969, nel quale  &#13;
 si chiede che la questione sia dichiarata non fondata.                   &#13;
     Dopo aver accennato alle ragioni di politica criminale della  norma  &#13;
 denunziata  ed  ai diversi orientamenti dottrinali intesi a precisare a  &#13;
 quale  titolo  sia  da  ascrivere  il  reato  commesso  in  istato   di  &#13;
 ubriachezza  volontaria  o colposa, anche l'Avvocatura si richiama alle  &#13;
 sentenze di questa Corte sull'art. 27  della  Costituzione  e  sostiene  &#13;
 che, secondo la dominante giurisprudenza della cassazione e la dottrina  &#13;
 prevalente,  devesi sempre aver riguardo all'atteggiamento psicologico,  &#13;
 quantunque  abnorme,  riferito  al  momento  in   cui   fu   realizzata  &#13;
 l'attività penalmente illecita, cioè al momento in cui fu commesso il  &#13;
 reato  (e  non  a quello della fase precedente, nella quale l'agente si  &#13;
 sia  posto  nello  stato  di  ubriachezza),  di  guisa   che,   neppure  &#13;
 nell'ipotesi  di  ubriachezza  volontaria  o  colposa,  sarebbe esclusa  &#13;
 l'indagine   sulla   coscienza   e   volontà   dell'azione;   e   ciò  &#13;
 contrariamente  a quanto si assume nell'ordinanza, che avrebbe, invece,  &#13;
 confuso    tra   il   concetto   di   imputabilità,   da   considerare  &#13;
 indipendentemente  dalla   commissione   del   reato,   e   quello   di  &#13;
 responsabilità,  comprensivo  anche  della  colpevolezza,  senza della  &#13;
 quale non v'è rapporto  di  causalità  psichica  tra  la  condotta  e  &#13;
 l'evento.                                                                &#13;
     2. - Altra questione di legittimità costituzionale del primo comma  &#13;
 del citato art. 92 del codice penale, è stata sollevata in riferimento  &#13;
 agli  artt.  3  e  27,  primo  e  terzo  comma, della Costituzione, con  &#13;
 ordinanza del 3 marzo 1969, dalla Corte d'assise di Padova,  nel  corso  &#13;
 di  un  procedimento  penale  a  carico  di Giorgio Piovan, imputato di  &#13;
 omicidio aggravato, ai sensi  degli  artt.  575  e  577,  in  relazione  &#13;
 all'art.  61,  n.  4,  del  codice penale, commesso in istato di totale  &#13;
 ubriachezza.                                                             &#13;
     Anche  in  tale  ordinanza  si  assume  che  la  norma   denunziata  &#13;
 profilerebbe   un'ipotesi  di  presunzione  legale  di  responsabilità  &#13;
 penale; si fa  richiamo  alla  giurisprudenza  di  questa  Corte  sulla  &#13;
 nozione  di  responsabilità  personale in materia penale, nonché alla  &#13;
 funzione rieducativa della pena, che sarebbe da  applicare  soltanto  a  &#13;
 chi  ha  commesso  il  reato  in istato di effettiva imputabilità. Per  &#13;
 quanto concerne l'interpretazione della norma denunziata, si  sostiene,  &#13;
 poi,  che  secondo  la  costante  giurisprudenza, chi commette un reato  &#13;
 doloso in istato di ubriachezza volontaria  ne  risponde  a  titolo  di  &#13;
 dolo,  pur  avendo voluto soltanto la ubriachezza e non - stante il suo  &#13;
 stato di incapacità naturale - anche il reato commesso.                 &#13;
     La  violazione  del  principio  di   eguaglianza   viene,   infine,  &#13;
 prospettata  con  riferimento  all'ipotesi  di  chi,  pur trovandosi in  &#13;
 condizioni personali di  eguale  incapacità,  non  incorrerebbe  nella  &#13;
 presunzione di imputabilità.                                            &#13;
     L'ordinanza,   ritualmente   notificata   e  comunicata,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale n. 207 del 13 agosto 1969.           &#13;
     Nel giudizio innanzi a questa Corte non vi è stata costituzione di  &#13;
 parte.                                                                   &#13;
     3. - Un'ulteriore questione di  legittimità  costituzionale  dello  &#13;
 stesso  art.  92  del  codice  penale, in riferimento all'art. 27 della  &#13;
 Costituzione, è stata, da ultimo, sollevata dal tribunale di  Livorno,  &#13;
 con   ordinanza   del  10  settembre  1969,  emessa  nel  corso  di  un  &#13;
 procedimento penale a carico di Francesco Ventura, imputato  del  reato  &#13;
 di  oltraggio  a  pubblico ufficiale, commesso in istato di ubriachezza  &#13;
 non accidentale.                                                         &#13;
     A sostegno della non manifesta  infondatezza  della  questione,  si  &#13;
 deduce  che  l'azione compiuta dall'ubriaco, in molti casi - al pari di  &#13;
 quelle dell'infermo totale di mente - è  a  lui  riferibile  solo  sul  &#13;
 piano  della  causalità  materiale  e  non  su quello della causalità  &#13;
 psichica. Sarebbe, pertanto, assurdo e illogico il suo  assoggettamento  &#13;
 a  sanzione  penale,  che  non potrebbe trovare spiegazione neppure con  &#13;
 riferimento  all'atteggiamento  psichico   che   ebbe   a   determinare  &#13;
 l'ubriachezza, dato che, secondo la giurisprudenza, per stabilire se il  &#13;
 fatto  sia da imputare a titolo di dolo o di colpa, sarebbe da valutare  &#13;
 l'atteggiamento della volontà al momento del fatto di reato,  commesso  &#13;
 in istato di ubriachezza.                                                &#13;
     Si  osserva, ancora, che la circostanza che l'ubriachezza sia stata  &#13;
 determinata  dall'agente  volontariamente   o   colposamente   potrebbe  &#13;
 giustificare  un  più  rigoroso trattamento dell'ubriachezza in quanto  &#13;
 tale, ma non legittima  l'attribuzione  della  paternità  psichica  di  &#13;
 un'azione  od  omissione  che  può  da lui non essere voluta e di cui,  &#13;
 comunque, l'ebbro non era in grado di valutare, in tutto o in parte, le  &#13;
 conseguenze.                                                             &#13;
     Si deduce, da ultimo, che la  norma  denunziata,  a  differenza  di  &#13;
 quanto  l'ordinamento  dispone per l'ubriachezza fortuita, statuirebbe,  &#13;
 per quella volontaria e colposa - allo scopo  di  combattere  la  piaga  &#13;
 dell'alcoolismo - una presunzione di imputabilità e porrebbe in essere  &#13;
 una  vera  e propria finzione giuridica, posto che è priva di senso la  &#13;
 valutazione dell'atteggiamento psichico di una mente  sconvolta.  Tutto  &#13;
 ciò  in  violazione  dell'art.  27  della Costituzione, che sancirebbe  &#13;
 l'esclusione di una responsabilità penale in assenza di quel minimo di  &#13;
 capacità di intendere e di volere che consente l'autodeterminazione.    &#13;
     L'ordinanza,  ritualmente  notificata  e   comunicata,   è   stata  &#13;
 pubblicata nella Gazzetta Ufficiale n. 280 del 5 novembre 1969.          &#13;
     Nel giudizio innanzi a questa Corte non vi è stata costituzione di  &#13;
 parte.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Data l'identità della materia, le questioni sollevate dalle  &#13;
 tre ordinanze del pretore di Roma, della Corte d'assise di Padova e del  &#13;
 tribunale di Livorno, possono essere decise con unica sentenza.          &#13;
     2. - L'ordinanza del pretore di Roma  e  quella  del  tribunale  di  &#13;
 Livorno  hanno  sollevato  la  questione di legittimità costituzionale  &#13;
 dell'art. 92 del codice penale, in riferimento, genericamente, all'art.  &#13;
 27 della Costituzione,  mentre  l'ordinanza  della  Corte  d'assise  di  &#13;
 Padova,  oltre  a richiamarsi anche all'art. 3, specifica che l'art. 27  &#13;
 sarebbe violato sia nel primo che nel terzo comma.                       &#13;
     3. - La prima questione da esaminare,  in  ordine  logico,  attiene  &#13;
 all'art.  3, il quale importa che situazioni diverse siano disciplinate  &#13;
 in modo diverso: in tale senso è la giurisprudenza costante di  questa  &#13;
 Corte.    Si  assume  che  l'art. 92 del codice penale (del quale è in  &#13;
 discussione  il  primo  comma)  violerebbe   esso   art.   3,   perché  &#13;
 l'ubriachezza  volontaria o colposa, pur producendo incapacità (totale  &#13;
 o parziale) di intendere e  di  volere,  non  esclude  l'imputabilità,  &#13;
 contrariamente  a quanto è statuito per l'infermo di mente (artt. 88 e  &#13;
 89 cod. pen.) e per l'ubriaco accidentale (art. 91 cod. pen.).           &#13;
     Gli è  che  non  può  negarsi,  da  un  lato,  che  l'incapacità  &#13;
 naturale,  totale  o  parziale (per infermità di mente), configuri una  &#13;
 situazione fenomenicamente ed etiologicamente diversa dall'ubriachezza;  &#13;
 dall'altro, che i fatti di reato commessi in stato di  ubriachezza  non  &#13;
 possano  essere  sottoposti  ad  una  disciplina  unitaria,  stante  la  &#13;
 varietà degli atteggiamenti che assumono i soggetti allorché  cadono,  &#13;
 o si pongono, in tale stato.                                             &#13;
     Insomma,  la  ragione  della  differente  normativa tra ubriachezza  &#13;
 derivata e ubriachezza  non  derivata  da  caso  fortuito  o  da  forza  &#13;
 maggiore  sta  nell'intento  del  legislatore  di prevenire e reprimere  &#13;
 l'ubriachezza come male sociale e, soprattutto, come situazione che, in  &#13;
 certi  soggetti,  può  spingere  al  delitto.   Il   che   basta   per  &#13;
 giustificare,  sotto  il  profilo  costituzionale,  la norma impugnata:  &#13;
 l'ubriaco, che abbia commesso  un  reato,  risponde  per  una  condotta  &#13;
 antidoverosa, cioè per essersi posto volontariamente o colposamente in  &#13;
 condizione di commetterlo.                                               &#13;
     Quali  che  siano le opinioni dottrinarie in materia (ed è noto che  &#13;
 il progetto di riforma  del  1949-1950  intendeva  perseguire  i  reati  &#13;
 commessi  in stato di ubriachezza volontaria o colposa esclusivamente a  &#13;
 titolo di colpa, mentre il disegno di legge del 1968, al pari di quelli  &#13;
 del 1956 e del 1960, considera quello stato come causa  di  diminuzione  &#13;
 facoltativa della pena, avvicinandosi al sistema del codice Zanardelli,  &#13;
 ove,  però,  la  diminuzione  era  obbligatoriamente  prescritta),  in  &#13;
 realtà, l'ebbro è qui imputabile per la  volontarietà  o  colposità  &#13;
 dell'ubriachezza:   ciò   che   spiega,  ripetesi,  la  differenza  di  &#13;
 normazione rispetto al vizio di mente e all'ubriachezza accidentale.     &#13;
     La norma ha, bensì, dato e ancora dà luogo a critiche severe  sul  &#13;
 piano della logica e della psicopatologia, ma, considerata in relazione  &#13;
 al  fine,  non  può  dirsi viziata di irragionevolezza.   Pertanto, la  &#13;
 questione, alla stregua dell'art. 3 della Costituzione, è infondata.    &#13;
     4. - Neppure l'art. 27, primo comma,  della  Costituzione,  risulta  &#13;
 violato.                                                                 &#13;
     Chi  si  ubriaca  (per  sua volontà o per sua colpa) e commette un  &#13;
 reato risponde, in verità, di un  proprio  comportamento  (arg.  dalla  &#13;
 sentenza n. 42 del 1965).                                                &#13;
     Se,  poi,  si riguardasse lo stato di incapacità di intendere e di  &#13;
 volere dell'ubriaco, per dedurne che, ex art. 27, primo comma, verrebbe  &#13;
 meno l'imputabilità, sarebbe facile replicare ancora una volta, da  un  &#13;
 lato,  che il genus colpevolezza (distinto nelle due species del dolo e  &#13;
 della colpa in senso stretto) sussiste nel comportamento iniziale  (che  &#13;
 ha provocato l'ubriachezza); dall'altro, che il precetto costituzionale  &#13;
 non  esclude che sia responsabilità personale per fatto proprio quella  &#13;
 di chi, incapace nel momento in cui commette il reato, non lo sia stato  &#13;
 quando si è posto in condizione di commetterlo.                         &#13;
     5. - La norma impugnata non contrasta  neppure  col  secondo  comma  &#13;
 dell'art. 27 della Costituzione, dato che essa non pone una presunzione  &#13;
 di  colpevolezza  da  valere  in  giudizio.  Né  viola il terzo comma:  &#13;
 infatti, la pena irrogata per il reato commesso da chi versi  in  stato  &#13;
 di  ubriachezza  volontaria  o  colposa  non differisce da quella a cui  &#13;
 soggiace ogni altro autore di reato; né può ritenersi non emendativa,  &#13;
 cioè non può contestarsi  che  essa  sia  diretta  ad  attivare,  nel  &#13;
 condannato,   una   controspinta   all'abuso  dell'alcool  (ubriachezza  &#13;
 volontaria) o a provocare un energico richiamo alla temperanza  e  alla  &#13;
 prudenza (ubriachezza colposa).                                          &#13;
     Spetterà  al  giudice di merito sia valutare, caso per caso, se si  &#13;
 tratti di ubriachezza colposa o di ubriachezza  accidentale;  sia,  del  &#13;
 pari,  accertare di volta in volta, secondo la giurisprudenza corrente,  &#13;
 il titolo di colpevolezza (dolo o colpa), sulla base dell'atteggiamento  &#13;
 psicologico in concreto  assunto  dall'ubriaco  al  momento  nel  quale  &#13;
 commise il fatto.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 92, primo comma, del codice penale, proposta dalle  ordinanze  &#13;
 citate   in   epigrafe,   in  riferimento  agli  artt.  3  e  27  della  &#13;
 Costituzione.                                                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 26 febbraio 1970.                             &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
