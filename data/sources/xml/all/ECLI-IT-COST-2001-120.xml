<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>120</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:120</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/05/2001</data_decisione>
    <data_deposito>11/05/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionalità dell'art. 299, primo e &#13;
secondo  comma,  del  codice civile, promosso con ordinanza emessa il &#13;
16 maggio 2000 dalla Corte di appello di Palermo sul ricorso proposto &#13;
da S. L., iscritta al n. 472 del registro ordinanze 2000 e pubblicata &#13;
nella  Gazzetta  Ufficiale della Repubblica n. 38, 1ª serie speciale, &#13;
dell'anno 2000. &#13;
    Udito  nella  camera  di  consiglio  del  7 marzo 2001 il giudice &#13;
relatore Fernando Santosuosso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  La  Corte  di  appello  di  Palermo,  adi'ta  in  sede  di &#13;
impugnazione  del  decreto  col  quale  il Tribunale di quella città &#13;
aveva  respinto  la richiesta di L. S. di poter aggiungere al cognome &#13;
acquisito  con l'adozione il proprio cognome originario, ha sollevato &#13;
questione  di  legittimità  costituzionale  del  primo e del secondo &#13;
comma  dell'art. 299  codice civile, in riferimento agli artt. 2, 3 e &#13;
30 della Costituzione. &#13;
    Premette  in  punto  di fatto il giudice a quo che il ricorrente, &#13;
nato nel 1945 e coniugato, è stato adottato da A. S. con decreto del &#13;
4 giugno  1999;  a  seguito  di ciò, egli ha chiesto al Tribunale di &#13;
poter  conservare il proprio cognome anteponendolo a quello adottivo, &#13;
domanda   respinta   perché,  trattandosi  di  figlio  naturale  non &#13;
riconosciuto  dai  propri  genitori,  l'art. 299  cod.  civ.  prevede &#13;
espressamente che, in tale caso, l'adottato assuma come unico cognome &#13;
quello dell'adottante. &#13;
    Ciò posto la Corte rimettente osserva che la norma impugnata non &#13;
è  suscettibile  di ricevere un'interpretazione adeguatrice, perché &#13;
il  testo  della  stessa  esprime,  oggettivamente, un dato lessicale &#13;
indiscutibile.  Ne  consegue,  perciò, la necessità di sollevare la &#13;
questione   di   legittimità  costituzionale  dei  primi  due  commi &#13;
dell'art. 299  cod.  civ; questione che è rilevante, perché dal suo &#13;
accoglimento   deriverebbe   il   buon   esito  del  gravame,  e  non &#13;
manifestamente  infondata  in  riferimento  agli  invocati parametri. &#13;
Rileva  in  proposito  il  giudice a quo che il nome ha, nell'attuale &#13;
ordinamento,  un valore fondamentale di identificazione della persona &#13;
umana,  al  punto da qualificarsi come un diritto della personalità. &#13;
Tale   diritto   si  collega  a  quello,  più  ampio,  all'identità &#13;
personale,  quale  si  è  andato  progressivamente  maturando  nella &#13;
giurisprudenza   e   nella   coscienza   sociale.   Anche   la  Corte &#13;
costituzionale,  con  la sentenza n. 13 del 1994, ha riconosciuto che &#13;
il  diritto  all'identità  personale  rientra  nella tutela prevista &#13;
dall'art. 2  della Costituzione, contribuendo a formare il patrimonio &#13;
inviolabile della persona umana. &#13;
    La norma impugnata, introdotta nel suo testo attuale dall'art. 61 &#13;
della   legge  4 maggio  1983,  n. 184  (Disciplina  dell'adozione  e &#13;
dell'affidamento  dei  minori), ha ribaltato il principio previgente, &#13;
stabilendo  che  nell'adozione di maggiorenni l'adottato anteponga il &#13;
cognome  adottivo  a  quello  originario. Ove, però, egli sia figlio &#13;
naturale  non  riconosciuto,  l'acquisto  del  nuovo  cognome implica &#13;
automaticamente  la  perdita di quello originario. Tale principio, se &#13;
trova   in   linea   di   massima   una  giustificazione  nell'antico &#13;
convincimento  di  tutelare  il figlio nascondendo un cognome imposto &#13;
dall'ufficiale  di  stato civile (e, perciò, rivelatore dell'origine &#13;
illegittima),  appare,  invece,  palesemente irrazionale nell'ipotesi &#13;
dell'adozione di un maggiorenne. Quest'ultimo, infatti, essendo ormai &#13;
una persona adulta, ha una posizione familiare e sociale da tutelare, &#13;
rispetto   alla   quale   il   cognome  originario,  benché  imposto &#13;
dall'ufficiale  di  stato  civile,  è  ormai un segno distintivo che &#13;
costituisce  parte integrante dell'identità personale, avendolo egli &#13;
anche  trasmesso  ai  propri  figli;  la sua eliminazione, quindi, si &#13;
risolve   in  un'oggettiva  lesione  della  predetta  identità,  con &#13;
conseguente violazione dell'art. 2 della Carta fondamentale. &#13;
    Al giudice a quo, inoltre, la norma appare anche in conflitto con &#13;
gli  artt. 3  e 30 Cost., perché il figlio naturale non riconosciuto &#13;
ha  lo  stesso  diritto  del  figlio legittimo di tutelare il proprio &#13;
cognome,  mentre  la  norma  impugnata  determina  "un'ingiustificata &#13;
disparità   di   trattamento  tra  figli  non  riconosciuti  (e  non &#13;
riconoscibili) e figli legittimi".<diritto>Considerato in diritto</diritto>1.  -  La  Corte  di appello di Palermo dubita della legittimità &#13;
costituzionale  del  primo  e  del secondo comma dell'art. 299 codice &#13;
civile,  nella  parte  in  cui  prevedono che l'adottato anteponga il &#13;
cognome  adottivo  a quello originario e che, qualora il medesimo sia &#13;
figlio  naturale  non  riconosciuto  dai propri genitori, egli assuma &#13;
solo   il   cognome   dell'adottante,   perdendo,   perciò,   quello &#13;
originariamente imposto dall'ufficiale di stato civile. &#13;
    Il  giudice  a  quo  ritiene che tale previsione sia lesiva degli &#13;
artt. 2, 3 e 30 della Costituzione, innanzitutto perché, trattandosi &#13;
di adottato maggiorenne, il cognome originario ha ormai acquisito per &#13;
lui  il  carattere  di  segno  distintivo  dell'identità  personale, &#13;
avendolo  l'interessato  trasmesso  ai  propri figli ed essendo egli, &#13;
comunque,  identificato  in tal modo nel contesto familiare e sociale &#13;
di  appartenenza;  ed in secondo luogo perché il figlio naturale non &#13;
riconosciuto ha lo stesso diritto del figlio legittimo di tutelare il &#13;
proprio    cognome,    mentre    la    norma    impugnata   determina &#13;
"un'ingiustificata   disparità   di   trattamento   tra   figli  non &#13;
riconosciuti (e non riconoscibili) e figli legittimi". &#13;
    La   prospettazione  dell'ordinanza  investe,  seguendo  l'ordine &#13;
logico,  preliminarmente  il secondo comma della norma impugnata, che &#13;
non   permette  all'adottato  di  mantenere  il  proprio  cognome  e, &#13;
subordinatamente  all'accoglimento,  anche il primo comma, poiché il &#13;
giudice   rimettente   pare   richiedere  alla  Corte  una  pronuncia &#13;
manipolativa che, invertendo la regola vigente, consenta all'adottato &#13;
di anteporre il cognome originario rispetto a quello adottivo. &#13;
    2. - La questione principale è fondata. &#13;
    Il   secondo   comma   dell'art. 299   cod.  civ.,  nel  regolare &#13;
l'assunzione del cognome in caso di adozione di maggiorenne che abbia &#13;
la  qualità di figlio naturale, prevede, nel suo primo periodo, che, &#13;
qualora  si  tratti  di  figlio naturale non riconosciuto, l'adottato &#13;
assuma  solo il cognome dell'adottante. La ratio di tale norma, sulla &#13;
quale  non ha inciso la sostituzione operata dall'art. 61 della legge &#13;
4 maggio   1983,   n. 184,   risiede   -  come  rileva  la  relazione &#13;
ministeriale  -  nella  ritenuta  opportunità  di  far scomparire il &#13;
cognome imposto dall'ufficiale di stato civile ai sensi dell'art. 71, &#13;
ultimo comma, del regio decreto 9 luglio 1939, n. 1238. &#13;
    Tale scelta, peraltro, risulta in contrasto con l'invocato art. 2 &#13;
della  Costituzione,  dovendosi  ormai ritenere principio consolidato &#13;
nella  giurisprudenza  di  questa  Corte quello per cui il diritto al &#13;
nome  -  inteso  come  primo  e  più  immediato segno distintivo che &#13;
caratterizza  l'identità  personale  -  costituisce  uno dei diritti &#13;
inviolabili  protetti dalla menzionata norma costituzionale (sentenze &#13;
n. 297 del 1996 e n. 13 del 1994). &#13;
    Nel caso in esame, non solo l'interessato ha utilizzato da sempre &#13;
quel  cognome,  trasmettendolo  anche  ai propri figli, ma tale segno &#13;
distintivo si è radicato nel contesto sociale in cui egli si trova a &#13;
vivere, sicché precludere all'adottato la possibilità di mantenerlo &#13;
si  risolve  in  un'ingiusta  privazione  di  un  elemento  della sua &#13;
personalità, tradizionalmente definito come il diritto "ad essere se &#13;
stessi".  Ed è innegabile, d'altra parte, che l'antico sfavore verso &#13;
i   figli   nati  fuori  del  matrimonio  è  superato  dalla  nostra &#13;
Costituzione oltre che dalla coscienza sociale. Per queste ragioni il &#13;
fatto che l'adottato acquisisca uno status del quale era privo non è &#13;
motivo   sufficiente  per  negare  la  violazione  dell'art. 2  della &#13;
Costituzione. &#13;
    Non può essere dimenticato, d'altronde, che la norma in esame è &#13;
anche  del tutto irrazionale alla luce della riforma dell'adozione di &#13;
cui alla menzionata legge n. 184 del 1983. Con questa legge, infatti, &#13;
si  è  compiuta  una netta distinzione fra l'adozione di minori, sia &#13;
essa  legittimante  o  meno,  e  quella  di maggiorenni, regolata dal &#13;
codice  civile.  Se  la  ratio  della  prima  è,  almeno in linea di &#13;
massima,  quella  di  fornire al minore una famiglia che sia idonea a &#13;
consentire  nel  modo  migliore  il  suo  sviluppo  -  il  che spiega &#13;
l'assunzione, da parte dell'adottato, del solo cognome dell'adottante &#13;
e  la  cessazione di ogni rapporto con la famiglia d'origine (art. 27 &#13;
della  legge  n. 184  del  1983),  salvo  la  c.d.  adozione  in casi &#13;
particolari  -  l'obiettivo  della  seconda  evidentemente  non è il &#13;
medesimo,  poiché  tale adozione (art. 300 cod. civ.) non crea alcun &#13;
vincolo  di  parentela  tra  l'adottato e la famiglia dell'adottante, &#13;
tanto  che  il  primo  conserva  tutti  i propri precedenti rapporti, &#13;
specie quelli con la famiglia di origine (v. sentenze n. 500 del 2000 &#13;
e n. 240 del 1998 ed ordinanza n. 82 del 2001). &#13;
    La   scomparsa   del   cognome   originario,   dunque,  nel  caso &#13;
del maggiorenne  appare  anche  priva  di  razionale giustificazione, &#13;
sicché risulta violato l'art. 3 della Costituzione. &#13;
    3.  -  L'ordinanza di rimessione, come si è accennato, prospetta &#13;
un'ulteriore   contrarietà  agli  invocati  parametri  della  regola &#13;
prevista  dal primo comma dell'art. 299 cod. civ., in base alla quale &#13;
il cognome dell'adottante deve essere anteposto al proprio. &#13;
    Alla  luce delle considerazioni svolte, la precedenza del cognome &#13;
dell'adottante non appare irrazionale, così come non può costituire &#13;
violazione  del  diritto  all'identità  personale  il  fatto  che il &#13;
cognome  adottivo  preceda  o  segua quello originario. La lesione di &#13;
tale   identità   è   ravvisabile   nella  soppressione  del  segno &#13;
distintivo,   non  certo  nella  sua  collocazione  dopo  il  cognome &#13;
dell'adottante. &#13;
    Ne consegue l'infondatezza di questa seconda questione.</testo>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    1)   dichiara   l'illegittimità   costituzionale  dell'art. 299, &#13;
secondo  comma, del codice civile, nella parte in cui nonprevede che, &#13;
qualora  sia  figlio  naturale  non riconosciuto dai propri genitori, &#13;
l'adottato  possa  aggiungere  al cognome dell'adottante anche quello &#13;
originariamente attribuitogli; &#13;
    2)   dichiara   non   fondata   la   questione   di  legittimità &#13;
costituzionale     dell'art. 299,    primo    comma,    del    codice &#13;
civilesollevata,   in   riferimento   agli  artt. 2,  3  e  30  della &#13;
Costituzione,  dalla  Corte  di appello di Palermo con l'ordinanza di &#13;
cui in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 7 maggio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                      Il redattore: Santosuosso &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in Cancelleria l'11 maggio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
