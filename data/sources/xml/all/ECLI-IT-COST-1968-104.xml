<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1968</anno_pronuncia>
    <numero_pronuncia>104</numero_pronuncia>
    <ecli>ECLI:IT:COST:1968:104</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/07/1968</data_decisione>
    <data_deposito>16/07/1968</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Dott. &#13;
 ANTONIO MANCA - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 26, secondo  &#13;
 comma, del Codice penale, promosso con ordinanza  emessa  l'8  novembre  &#13;
 1966  dal pretore di Iseo nel procedimento penale a carico di Bresciani  &#13;
 Teresa, iscritta al n. 14 del  Registro  ordinanze  1967  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 51 del 25 febbraio 1967.    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udita nell'udienza pubblica dell'11 giugno 1968  la  relazione  del  &#13;
 Giudice Ercole Rocchetti;                                                &#13;
     udito  il sostituto avvocato generale dello Stato Giorgio Azzariti,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il pretore del mandamento di Iseo,  con  decreto  penale  9  luglio  &#13;
 1966,   riteneva   la   signora  Teresa  Bresciani  responsabile  della  &#13;
 contravvenzione prevista negli artt. 9 e 36 del T.U. delle leggi  sulla  &#13;
 pesca  (R.D.  8 ottobre 1931, n. 1604) "per avere la stessa versato nel  &#13;
 lago di Iseo i rifiuti del proprio stabilimento  industriale  senza  la  &#13;
 prescritta   autorizzazione"'  e  la  condannava  all'ammenda  di  lire  &#13;
 120.000, pari al triplo del massimo edittale, perché riteneva  che,  a  &#13;
 causa  delle condizioni economiche di lei, dovesse farsi luogo nel caso  &#13;
 all'applicazione dell'aumento di pena previsto dall'art. 26 del  Codice  &#13;
 penale.                                                                  &#13;
     La  Bresciani  si opponeva al decreto e, al dibattimento, dopo aver  &#13;
 dedotto che la disposizione del citato art. 26, in base  al  quale  era  &#13;
 stata   nei  suoi  confronti  maggiorata  la  pena,  dovesse  ritenersi  &#13;
 incostituzionale, perché contraria al principio di eguaglianza di  cui  &#13;
 all'art. 3 della Costituzione, chiedeva che, sospendendosi il giudizio,  &#13;
 gli  atti  venissero  rimessi  a  questa Corte, competente in materia a  &#13;
 decidere sull'eccezione.                                                 &#13;
     Il pretore, con ordinanza 8 novembre 1966, disponeva in conformità  &#13;
 della richiesta, dopo avere, sul punto della rilevanza, osservato  che,  &#13;
 in  caso di affermazione della responsabilità dell'imputata, stante le  &#13;
 sue condizioni economiche, l'art. 26 del Codice penale  doveva  trovare  &#13;
 applicazione,  e  sul  punto della non manifesta infondatezza, ritenuto  &#13;
 che il  detto  articolo  appariva  in  contrasto  con  l'art.  3  della  &#13;
 Costituzione,  in  quanto  legittima  un aggravamento della sanzione in  &#13;
 ragione delle sole condizioni economiche del reo,  mentre,  nel  quadro  &#13;
 del  vigente  ordinamento penale, i reati sono puniti proporzionalmente  &#13;
 alla gravità del fatto e alla capacità a delinquere del  colpevole  e  &#13;
 solo  nell'ordinamento  tributario  trova un giustificato e ragionevole  &#13;
 ingresso un principio di progressività.                                 &#13;
     L'ordinanza del pretore, ritualmente notificata  e  comunicata,  è  &#13;
 stata pubblicata nella Gazzetta Ufficiale n. 51 del 25 febbraio 1967.    &#13;
     Nel  presente  giudizio  si  è  costituita,  in rappresentanza del  &#13;
 Presidente del Consiglio  dei  Ministri,  l'Avvocatura  generale  dello  &#13;
 Stato depositando il 17 marzo 1967 l'atto di intervento.                 &#13;
     L'Avvocatura  ha  dedotto  che  la  questione  di costituzionalità  &#13;
 sollevata dal pretore di Iseo non sembra aver fondamento e ciò perché  &#13;
 la ratio del sistema, che prevede la  facoltà  di  aumentare  fino  al  &#13;
 triplo la pena pecuniaria quando, per le condizioni economiche del reo,  &#13;
 la  pena  stessa  può  presumersi  inefficace,  anche  se applicata al  &#13;
 massimo, non sarebbe  da  rinvenire  nell'intento  del  legislatore  di  &#13;
 determinare una disuguaglianza dei cittadini in base ad una distinzione  &#13;
 per  diverse  condizioni  sociali (alle quali sono da rapportare quelle  &#13;
 economiche), ma nell'intento dichiarato di garantire il  carattere  che  &#13;
 si attenua nei confronti dei più economicamente dotati.                 &#13;
     E,  dopo  aver  richiamato  principi espressi da questa Corte nella  &#13;
 sentenza del 22 marzo 1962, n. 29 sulle linee fondamentali  del  nostro  &#13;
 ordinamento  penale  e  sul  punto  della  legittimità delle norme che  &#13;
 dispongono la commutazione della pena pecuniaria in quella detentiva, e  &#13;
 dopo aver altresì  fatto  appello  all'opinione  più  volte  da  essa  &#13;
 espressa,  e  secondo  la  quale  proprio  il  principio di eguaglianza  &#13;
 richiede che a situazioni giuridiche  differenziate  sia  applicato  un  &#13;
 parimenti  differenziato  trattamento,  l'Avvocatura ha concluso che la  &#13;
 disposizione denunciata dell'art. 26 del Codice penale, al contrario di  &#13;
 quanto sembra al pretore di Iseo, deve  ritenersi  affermazione  e  non  &#13;
 negazione   di  quel  principio,  come  che  intesa  a  correggere  una  &#13;
 diversità  di  incidenza  della  sanzione  sul  soggetto  colpito,  in  &#13;
 relazione  alle condizioni economiche di lui, diversità che è propria  &#13;
 della pena pecuniaria.                                                   &#13;
     Alla  pubblica  udienza  dell'11  giugno  1968  il   rappresentante  &#13;
 dell'Avvocatura si riportava ai propri scritti difensivi.<diritto>Considerato in diritto</diritto>:                          &#13;
     Come  risulta  dalla  giurisprudenza  costante  di questa Corte, il  &#13;
 principio di eguaglianza sancito dall'art. 3 della Costituzione postula  &#13;
 non solo che a situazioni oggettivamente uguali debba corrispondere  un  &#13;
 eguale  trattamento,  ma  anche che a situazioni oggettivamente diverse  &#13;
 debba corrispondere un trattamento differenziato.                        &#13;
     Accertare l'eguaglianza o la diversità delle situazioni,  ai  fini  &#13;
 del  trattamento  da applicare, è compito del legislatore, il quale vi  &#13;
 provvede  nell'esercizio  di  una  discrezionalità  che  trova  limite  &#13;
 soltanto nella ragionevolezza delle statuizioni.                         &#13;
     Frequenti occasioni all'esercizio di tale discrezionalità offre al  &#13;
 legislatore  la  disciplina dei reati e delle pene, ove, di fronte alla  &#13;
 variabile  complessità  della  condotta  antigiuridica  dei   singoli,  &#13;
 l'attuazione   di  una  riparatrice  giustizia  distributiva  esige  la  &#13;
 differenziazione più che l'uniformità.                                 &#13;
     Risponde  anzi  alle  esigenze  del  sistema penale che allo stesso  &#13;
 giudice sia conferita una certa discrezionalità fra  il  minimo  e  il  &#13;
 massimo  previsto  dalla  legge  nell'attribuzione  della pena, al fine  &#13;
 della sua determinazione in concreto (art. 133 del Codice penale).       &#13;
     Nell'esercizio di tale potere discrezionale il giudice  deve  tener  &#13;
 conto,  al  fine  di  valutare  la  gravità del reato e la capacità a  &#13;
 delinquere del colpevole, di elementi attinenti alla  personalità  del  &#13;
 reo  desunti  dal  suo  carattere, dalla sua vita e dalla sua condotta,  &#13;
 anche anteriore al commesso reato, e persino dalle condizioni  di  vita  &#13;
 individuale, familiare e sociale di lui (art. 133 del Codice penale).    &#13;
     Non può dubitarsi che la ratio di tale sistema coincida con quella  &#13;
 dell'art.  26,  secondo comma, del Codice penale, ora portato all'esame  &#13;
 di questa Corte, onde il problema della  conformità  al  principio  di  &#13;
 eguaglianza  sancito  dall'art. 3 della Costituzione va posto, rispetto  &#13;
 all'art. 133 e rispetto alla norma di che trattasi, in termini  affatto  &#13;
 paralleli.                                                               &#13;
     Ciò   premesso,   è   da   ricordare   che   sulla   legittimità  &#13;
 costituzionale  dell'art.  133,  questa  Corte  ha  avuto  più   volte  &#13;
 occasione  (v. sentenze n. 29 del 1962, 67 del 1963 e 111 del 1964), di  &#13;
 pronunziarsi, in  modo  esplicito  od  implicito,  in  senso  positivo,  &#13;
 ravvisando  nella  norma  la funzione di garantire, ai fini di una più  &#13;
 efficiente ed equilibrata giustizia, il processo di individualizzazione  &#13;
 della pena.                                                              &#13;
     Alla  stregua  di  analoghe   considerazioni,   la   questione   di  &#13;
 costituzionalità  dell'art.  26,  comma  secondo,  del  Codice  penale  &#13;
 prospettata dal pretore di Iseo, va pure risolta nel  senso  della  sua  &#13;
 infondatezza.                                                            &#13;
     Il criterio ispiratore di questa norma, che autorizza il giudice ad  &#13;
 aumentare sino al triplo l'ammenda quando, per le condizioni economiche  &#13;
 del  reo,  può presumersi che quella stabilita dalla legge risulti per  &#13;
 lui inefficace, è certo un criterio di discriminazione  fondato  sulle  &#13;
 condizioni  personali,  ma  non  è  affatto un criterio contrario alla  &#13;
 logica e alla ragionevolezza. Infatti soddisfa anch'esso  il  principio  &#13;
 della individualizzazione della pena.                                    &#13;
     Il  legislatore  ha inteso in tal modo, per le pene pecuniarie, sia  &#13;
 della multa (art. 24) che dell'ammenda (art. 26), accogliere e superare  &#13;
 - così come si legge nei lavori preparatori del Codice -  proprio  "le  &#13;
 critiche che in genere si sogliono fare contro l'uso delle pene dirette  &#13;
 a  colpire il patrimonio del reo, siccome quelle che riescono diseguali  &#13;
 e non personali".                                                        &#13;
     Si è  voluto  cioè,  con  la  facoltà  concessa  al  giudice  di  &#13;
 aumentarle  fino  al triplo nei confronti dei più abbienti, conseguire  &#13;
 l'effetto di adeguare quelle pene alla condizione del reo mettendole in  &#13;
 grado di possedere,  anche  nei  confronti  degli  anzidetti  soggetti,  &#13;
 quella necessaria efficacia afflittiva e intimidatrice, che rappresenta  &#13;
 l'obbiettivo,  se non unico, almeno precipuo, di ogni genere di pena, e  &#13;
 che altrimenti le pene in questione rischierebbero -  a  cagione  della  &#13;
 maggiore capacità economica dei soggetti stessi - di non possedere.     &#13;
     Il che è proposito che non può qualificarsi contrario alla logica  &#13;
 né  alla  ragionevolezza  e  che  quindi  non  offende,  ma  tutela il  &#13;
 principio di eguaglianza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 26, comma secondo, del Codice penale in relazione all'art.  3  &#13;
 della Costituzione.                                                      &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 2 luglio 1968.                                &#13;
                                   ALDO  SANDULLI  -  ANTONIO  MANCA   -  &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO CRISAFULLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
