<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>59</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:59</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SPAGNOLI</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/02/1995</data_decisione>
    <data_deposito>24/02/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Ugo SPAGNOLI; &#13;
 Giudici: prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  114,  terzo    &#13;
 comma,  del codice di procedura penale, promosso con ordinanza emessa    &#13;
 il 28 giugno 1993 dal giudice per le indagini preliminari  presso  il    &#13;
 Tribunale  di  Siracusa  nel  procedimento penale a carico di Maiorca    &#13;
 Carmelo ed altri, iscritta al n. 575 del registro  ordinanze  1993  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica, prima serie    &#13;
 speciale, dell'anno 1993;                                                &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio dell'8 febbraio  1995  il  Giudice    &#13;
 relatore Mauro Ferri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Il giudice per le indagini preliminari presso il Tribunale di    &#13;
 Siracusa   ha  sollevato  questione  di  legittimità  costituzionale    &#13;
 dell'art. 114, terzo  comma,  del  codice  di  procedura  penale,  in    &#13;
 riferimento agli artt. 3, 21 e 76 della Costituzione.                    &#13;
    2.  -  Il giudice remittente riferisce che, nel caso sottoposto al    &#13;
 suo  esame,  il  pubblico  ministero,  a  chiusura   delle   indagini    &#13;
 preliminari,  ha  chiesto  l'archiviazione  del  procedimento  penale    &#13;
 instaurato nei confronti  di  alcuni  indiziati  del  reato  previsto    &#13;
 dall'art.  684 del codice penale (pubblicazione arbitraria di atti di    &#13;
 un procedimento penale) in quanto  ritiene  che  gli  stessi  abbiano    &#13;
 legittimamente esercitato il diritto-dovere di cronaca.                  &#13;
    Ma, ad avviso del G.I.P., poiché l'avvenuta pubblicazione a mezzo    &#13;
 stampa   di   alcuni   passi  di  registrazioni  telefoniche  integra    &#13;
 un'ipotesi di "pubblicazione parziale" (vietata dall'art. 114,  terzo    &#13;
 comma,  del codice di procedura penale, quando avvenga, come nel caso    &#13;
 di specie, prima della sentenza di primo grado),  è  preliminare  ad    &#13;
 ogni statuizione di merito - ed assume per ciò stesso rilevanza - la    &#13;
 verifica  della  legittimità  costituzionale  della  norma,  essendo    &#13;
 evidente l'inconfigurabilità a  carico  degli  indiziati  del  reato    &#13;
 previsto  dall'art.  684  del  codice  penale  qualora  il fatto loro    &#13;
 ascritto non possa essere vietato dalla legge ordinaria.                 &#13;
    3. -  Ora,  prosegue  il  remittente,  a  fronte  del  divieto  di    &#13;
 pubblicazione,  anche  parziale,  degli  atti  del  fascicolo  per il    &#13;
 dibattimento (anteriormente alla sentenza di primo  grado),  l'ultimo    &#13;
 comma  dello  stesso  art.  114  dispone che "è sempre consentita la    &#13;
 pubblicazione del contenuto di atti non coperti da segreto".             &#13;
    Se quindi, espone il giudice a quo, la pubblicazione del contenuto    &#13;
 degli atti del processo penale costituisce (con  il  solo  limite  di    &#13;
 quelli   coperti   da   segreto)  un  fatto  non  soltanto  privo  di    &#13;
 offensività ma, anzi, espressione di una funzione costituzionalmente    &#13;
 garantita  dall'art.  21  della  Costituzione,  allora,   sussistendo    &#13;
 fondamentali   connotati   di  parità,  eguale  dovrebbe  essere  il    &#13;
 trattamento da riservare a quella pubblicazione  "parziale"  di  atti    &#13;
 del  fascicolo per il dibattimento, la cui divulgazione a mezzo della    &#13;
 stampa null'altro può aggiungere alla conoscenza derivabile  da  una    &#13;
 esauriente notizia del loro contenuto.                                   &#13;
    4. - La norma in esame, inoltre, risulterebbe anche non rispettosa    &#13;
 della  legge  di  delega,  la  cui  direttiva n. 71, mentre impone il    &#13;
 divieto di pubblicazione per gli atti coperti dal segreto e per altri    &#13;
 atti  specificamente  indicati  (diversi  da  quelli   destinati   al    &#13;
 fascicolo  per  il  dibattimento),  non  lo  prevede per gli atti del    &#13;
 fascicolo per il dibattimentale.                                         &#13;
    5. - Lo stesso criterio discretivo, infine, tra  l'illecito  e  il    &#13;
 consentito,  in  quanto fondato sulla enunciazione di una distinzione    &#13;
 concettuale ontologicamente incerta tra  "pubblicazione  parziale"  e    &#13;
 "pubblicazione  del  contenuto"  di atti, non sembra al remittente un    &#13;
 parametro ragionevole di distinzione; gli effetti della pubblicazione    &#13;
 di una notizia, più che dal dato formale della divulgazione  di  una    &#13;
 parte  dell'atto del processo che la documenta, possono scaturire - a    &#13;
 suo avviso - dal suo intrinseco valore informativo e  da  circostanze    &#13;
 specifiche.                                                              &#13;
    6.  -  È intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 ministri rappresentato dall'Avvocatura generale dello Stato.             &#13;
    La difesa del Governo osserva che la direttiva n. 71  della  legge    &#13;
 di  delega  non  esclude specificamente gli atti del fascicolo per il    &#13;
 dibattimento ma semplicemente non li menziona; per cui, in base  alla    &#13;
 costante  giurisprudenza della Corte, non può che concludersi che il    &#13;
 legislatore delegato, pur tenuto conto del  necessario  rispetto  dei    &#13;
 criteri   e   dei   principi   della   delega,  non  è  sfornito  di    &#13;
 discrezionalità nel modo  di  esercizio  della  delegazione  e  che,    &#13;
 quindi,   costituisce   legittimo  esercizio  di  tale  potere  anche    &#13;
 l'estensione ad altri casi  della  disciplina  prevista  nella  legge    &#13;
 delega  quando  sussista  l'eadem  ratio. Non vi sarebbe, poi, dubbio    &#13;
 sulla esistenza della stessa ragione di legge che assiste  i  divieti    &#13;
 temporanei  previsti  dalla  direttiva  n. 71, anche per gli atti del    &#13;
 dibattimento prima della pronuncia del giudice  di  primo  grado:  le    &#13;
 ragioni,     infatti,     non     sarebbero    solo    quelle    che,    &#13;
 esemplificativamente, enuncia nella sua ordinanza il  remittente,  ma    &#13;
 anche  altre  rivolte  ad  evitare  turbative alla fase decisoria del    &#13;
 processo di primo grado.                                                 &#13;
    Ancor meno convincenti, ad avviso  dell'Avvocatura,  sarebbero  le    &#13;
 ulteriori  censure  avanzate,  in riferimento agli artt. 3 e 21 della    &#13;
 Costituzione, sotto il profilo  della  disparità  di  trattamento  e    &#13;
 della lesione della libertà di stampa.                                  &#13;
    Ben  nota  essendo  la  scelta legislativa del codice di procedura    &#13;
 penale del 1988 in ordine alla distinzione tra contenuto dell'atto  e    &#13;
 atto stesso, e la sensibile novità apportata rispetto alla soluzione    &#13;
 che  era  alla  base dell'art. 164 del codice di procedura penale del    &#13;
 1930, ad avviso dell'Avvocatura il G.I.P.  di  Siracusa  censura  una    &#13;
 soluzione  legislativa  in  termini che non appaiono ammissibili alla    &#13;
 luce  degli  insegnamenti  della  Corte.  Si   dedurrebbe,   infatti,    &#13;
 l'esistenza di una situazione di illegittimità non dal confronto tra    &#13;
 due  realtà  normativamente delineate, ma dalla comparazione tra una    &#13;
 fattispecie che il legislatore ha considerato  (pubblicazione  di  un    &#13;
 atto, riprodotto in tutto o in parte o richiamato testualmente) e una    &#13;
 condotta (pubblicazione del contenuto dell'atto, lecita se effettuata    &#13;
 nei termini evidenziati nell'ordinanza di rimessione) facendo leva su    &#13;
 distorsioni  che derivano da aspetti patologici, che scaturiscono dal    &#13;
 talento professionale di  certo  giornalismo  giudiziario  il  quale,    &#13;
 attraverso   l'uso   sapiente  di  tecniche  narrative  e  di  idonei    &#13;
 espedienti espositivi perviene talora a  risultati  al  limite  della    &#13;
 liceità, per quanto attiene il divieto in esame.                        &#13;
    Il  constatato  rischio  di  una  diffusa  elusione del divieto di    &#13;
 pubblicazione dell'atto attraverso una riproduzione attenta  del  suo    &#13;
 contenuto  non sarebbe, in conclusione, ragione tale da rovesciare la    &#13;
 validità delle considerazioni espresse nella relazione  illustrativa    &#13;
 al  progetto  preliminare del codice di procedura penale, quanto alla    &#13;
 formazione del convincimento del giudice.                                &#13;
    Infine, l'Avvocatura sottolinea che la norma impugnata non  tutela    &#13;
 solo  la genuinità dell'opinione del giudicante ma, andando oltre le    &#13;
 stesse iniziali intenzioni del legislatore, consente di prevenire che    &#13;
 nel corso del giudizio la notizia processuale pubblicata acquisti  un    &#13;
 anticipato ed inopportuno crisma di ufficialità.<diritto>Considerato in diritto</diritto>1. - Il giudice per le indagini preliminari presso il Tribunale di    &#13;
 Siracusa  solleva,  in  riferimento  agli  artt.  3,  21  e  76 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art. 114,    &#13;
 terzo comma, del codice di procedura penale, nella parte in cui vieta    &#13;
 la pubblicazione - anche parziale - degli atti del fascicolo  per  il    &#13;
 dibattimento fino alla pronuncia della sentenza di primo grado.          &#13;
    2.  -  Il  remittente,  dopo  aver premesso che a fronte del detto    &#13;
 divieto l'ultimo comma del medesimo art. 114 dispone che  "è  sempre    &#13;
 consentita  la  pubblicazione  del  contenuto  di atti non coperti da    &#13;
 segreto", ritiene che il  citato  terzo  comma,  oltre  a  porre  una    &#13;
 irragionevole    ed    ontologicamente    incerta   distinzione   tra    &#13;
 "pubblicazione di atti" (vietata) e "pubblicazione del  contenuto  di    &#13;
 atti" (lecita), realizzi una ingiustificata disparità di trattamento    &#13;
 tra  due  situazioni sostanzialmente assimilabili, violi il principio    &#13;
 della libertà di stampa sancito dall'art. 21 della Costituzione,  e,    &#13;
 infine,  si  ponga  in  contrasto  con la direttiva n. 71 dell'art. 2    &#13;
 della legge di delega 16 febbraio 1987 n. 81, la  quale  non  prevede    &#13;
 alcun  divieto  di  pubblicazione  degli  atti  del  fascicolo per il    &#13;
 dibattimento.                                                            &#13;
    3. - Sotto quest'ultimo ed  assorbente  profilo  la  questione  è    &#13;
 fondata.                                                                 &#13;
    Nel   dare  riconoscimento  alle  esigenze  di  trasparenza  e  di    &#13;
 controllo sociale sullo  svolgimento  della  vicenda  processuale,  e    &#13;
 quindi   nel   contemperare   interessi  di  giustizia  ed  interessi    &#13;
 dell'informazione  -  entrambi  costituzionalmente  rilevanti  -   il    &#13;
 legislatore delegante ha operato una scelta ben precisa.                 &#13;
    I  primi due periodi della direttiva n. 71 delineano un sistema in    &#13;
 cui "su tutti gli atti  compiuti  dalla  polizia  giudiziaria  e  dal    &#13;
 pubblico ministero" è posto sia l'obbligo del segreto che il divieto    &#13;
 di  pubblicazione  fino  a  quando  i  medesimi  "non  possono essere    &#13;
 conosciuti dall'imputato" (recte: indagato).                             &#13;
    Da questa chiara enunciazione può evincersi che, nell'intento del    &#13;
 legislatore delegante, i limiti alla  divulgabilità  degli  atti  di    &#13;
 indagine  preliminare  si  collegano inequivocabilmente alle esigenze    &#13;
 investigative,  operando  al  fine  di  scongiurare  ogni   possibile    &#13;
 pregiudizio  alle indagini a causa di una anticipata conoscenza delle    &#13;
 stesse da parte della persona indagata.                                  &#13;
    Dal terzo periodo della citata direttiva può trarsi  la  conferma    &#13;
 di  tale obiettivo nelle intenzioni del legislatore delegante; viene,    &#13;
 infatti, riconosciuto al pubblico  ministero  l'ulteriore  potere  di    &#13;
 vietare  "la  pubblicazione di atti non più coperti dal segreto ..",    &#13;
 ma detto potere è vincolato al  "tempo  strettamente  necessario  ad    &#13;
 evitare pregiudizio per lo svolgimento delle stesse (indagini)").        &#13;
    4.  - Ciò posto, è evidente che tali divieti di divulgazione, in    &#13;
 quanto funzionalmente riferiti alle indagini preliminari, non possono    &#13;
 che essere  rivolti  agli  atti  nella  disponibilità  del  pubblico    &#13;
 ministero  per  l'ovvio  motivo  che  non  esiste un fascicolo per il    &#13;
 dibattimento fintantoché non si  sarà  deciso  se  il  dibattimento    &#13;
 dovrà  o  meno  essere  celebrato.  Non  solo: in nessun punto della    &#13;
 direttiva n. 71 è contemplato un divieto di pubblicazione di  quanto    &#13;
 contenuto  nel  fascicolo  per  il dibattimento; anzi, proprio ove la    &#13;
 direttiva considera esplicitamente il meccanismo del doppio fascicolo    &#13;
 (parte quarta), è previsto un divieto di pubblicazione  per  i  soli    &#13;
 "atti  depositati  a norma del numero 58", cioè per quelli contenuti    &#13;
 nel fascicolo del pubblico ministero.                                    &#13;
    5.  - Gli stessi compilatori del codice riconoscono il riferimento    &#13;
 esclusivo della  delega  al  fascicolo  del  pubblico  ministero  (v.    &#13;
 Relazione  al  progetto  preliminare), ma osservano che soltanto alla    &#13;
 fine delle indagini preliminari si ha la formazione del fascicolo del    &#13;
 pubblico ministero, e che pertanto  "non  è  facile,  né  opportuno    &#13;
 operare  distinzioni rispetto al divieto di pubblicazione nell'ambito    &#13;
 degli atti delle indagini preliminari".                                  &#13;
    L'argomento ha scarso rilievo non solo perché - come si è  visto    &#13;
 - la delega distingue, imponendo il divieto di pubblicazione soltanto    &#13;
 sugli atti del fascicolo del pubblico ministero, ma, perché, in ogni    &#13;
 caso,  può  valere  solamente  riguardo  alla  fase  delle  indagini    &#13;
 preliminari, non certo per il dibattimento,  fase  durante  la  quale    &#13;
 sono  ormai  esaurite  quelle  esigenze  di tutela delle indagini che    &#13;
 giustificavano il divieto stesso.                                        &#13;
    E infatti il protrarre il divieto di pubblicazione  del  fascicolo    &#13;
 del pubblico ministero anche oltre il termine delle indagini, durante    &#13;
 il dibattimento, ha, nei principi fondamentali dettati dalla legge di    &#13;
 delega,  ben altro fondamento, in quanto è funzionale ad evitare una    &#13;
 distorsione delle regole dibattimentali, ove il giudice  formasse  il    &#13;
 suo  convincimento sulla base di atti che dovrebbero essergli ignoti,    &#13;
 ma  che,  in  mancanza  del  suddetto  divieto,  potrebbe   conoscere    &#13;
 completamente   per   via   extraprocessuale   attraverso   i   mezzi    &#13;
 d'informazione.                                                          &#13;
    6.  -  Ma  se  questa  è  la  ratio  del  divieto  relativo  alla    &#13;
 divulgabilità  degli  atti  contenuti  nel  fascicolo  del  pubblico    &#13;
 ministero, pur dopo che ne  è  cessato  l'obbligo  del  segreto,  ne    &#13;
 consegue  la  sua  totale  inapplicabilità  a  quanto  contenuto nel    &#13;
 fascicolo per il dibattimento, per definizione concernente  gli  atti    &#13;
 che il giudice deve - invece - conoscere.                                &#13;
    Non si può, evidentemente, sostenere che la pubblicabilità di un    &#13;
 atto viene esclusa per evitare che, attraverso mezzi di informazione,    &#13;
 giunga  a conoscenza del giudice nel cui fascicolo processuale l'atto    &#13;
 è inserito.                                                             &#13;
    Come in dottrina è stato  osservato,  se  si  considera  che  nel    &#13;
 fascicolo  per  il dibattimento sono inseriti anche gli atti di prova    &#13;
 non rinviabili, ed assunti nella fase predibattimentale ex  art.  467    &#13;
 del  codice  di procedura penale, si arriva all'assurdo di un divieto    &#13;
 di pubblicazione diretto ad evitare che il giudice  conosca  atti  da    &#13;
 lui stesso compiuti.                                                     &#13;
    7.  -  In  conclusione:  in  raffronto  a quanto contemplato nella    &#13;
 direttiva n. 71 della legge di delega,  il  legislatore  delegato  ha    &#13;
 certamente  introdotto  al  terzo  comma  dell'art.  114 un ulteriore    &#13;
 divieto (riferito al  fascicolo  per  il  dibattimento),  rispetto  a    &#13;
 quello  relativo  al fascicolo del pubblico ministero. L'analiticità    &#13;
 con cui il delegante  ha  inteso  precisare  i  casi  di  divieto  di    &#13;
 pubblicazione  degli  atti  - evidentemente indicativa del rifiuto di    &#13;
 introdurne ulteriori, in rispetto del principio sancito dall'art.  21    &#13;
 della   Costituzione  -  impedisce  che  in  sede  di  attuazione  il    &#13;
 legislatore delegato possa pervenire a tale risultato, tanto più ove    &#13;
 si consideri che le motivazioni addotte per  giustificarlo  (corretta    &#13;
 formazione del convincimento del giudice) non possono ragionevolmente    &#13;
 riferirsi alla pubblicazione di quanto contenuto nel fascicolo per il    &#13;
 dibattimento,  concernente,  per definizione, gli atti che il giudice    &#13;
 deve conoscere.                                                          &#13;
    Va  pertanto  dichiarata l'illegittimità costituzionale dell'art.    &#13;
 114, terzo comma, del codice di procedura penale nella parte  in  cui    &#13;
 non  consente  la  pubblicazione  degli  atti  del  fascicolo  per il    &#13;
 dibattimento anteriormente alla pronuncia  della  sentenza  di  primo    &#13;
 grado.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   l'illegittimità  costituzionale  dell'art.  114,  terzo    &#13;
 comma, del codice di procedura  penale,  limitatamente  alle  parole:    &#13;
 "del  fascicolo  per  il dibattimento, se non dopo la pronuncia della    &#13;
 sentenza di primo grado, e di quelli".                                   &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 febbraio 1995.                             &#13;
                        Il Presidente: SPAGNOLI                           &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 24 febbraio 1995.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
