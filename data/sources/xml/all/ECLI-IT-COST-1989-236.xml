<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>236</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:236</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/04/1989</data_decisione>
    <data_deposito>21/04/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, prof. Luigi MENGONI, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 13 e 22 della    &#13;
 legge 28  febbraio  1985,  n.  47  (Norme  in  materia  di  controllo    &#13;
 dell'attività  urbanistico-edilizia,  sanzioni, recupero e sanatoria    &#13;
 delle opere edilizie) e dell'art.  8-quater  della  legge  21  giugno    &#13;
 1985,  n.  298  (Conversione in legge, con modificazioni, del decreto    &#13;
 legge 28 aprile 1985, n. 146, recante proroga di  taluni  termini  di    &#13;
 cui  alla  legge 28 febbraio 1985, n. 47 concernente norme in materia    &#13;
 di controllo dell'attività urbanistico-edilizia, sanzioni,  recupero    &#13;
 e sanatoria delle opere edilizie) promosso con ordinanza emessa il 18    &#13;
 luglio 1988 dal Pretore di Sorrento nel procedimento penale a  carico    &#13;
 di  D'Esposito  Raffaele,  iscritta  al n. 732 del registro ordinanze    &#13;
 1988 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  50,    &#13;
 prima serie speciale, dell'anno 1988;                                    &#13;
    Visto   l'atto  d'intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 22 febbraio 1989 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che  il  Pretore di Sorrento, con ordinanza emessa il 18    &#13;
 luglio 1988, ha sollevato, in riferimento all'art. 3 Cost., questione    &#13;
 di  legittimità  costituzionale  degli  artt. 13 e 22 della legge 28    &#13;
 febbraio 1985, n. 47 (Norme in materia  di  controllo  dell'attività    &#13;
 urbanistico-edilizia,  sanzioni,  recupero  e  sanatoria  delle opere    &#13;
 edilizie)  nella  parte   in   cui   prevedono   che   il   beneficio    &#13;
 dell'estinzione del reato consegua al solo caso in cui la conformità    &#13;
 dell'opera  abusiva  sia  consacrata  nel  formale  provvedimento  di    &#13;
 concessione   in  sanatoria,  e  non  anche  nel  caso  in  cui  tale    &#13;
 conformità sia realizzata in  virtù  della  demolizione  dell'opera    &#13;
 abusiva, nonché dell'art. 8-quater del decreto legge 28 aprile 1985,    &#13;
 n. 146, introdotto dalla legge di conversione 21 giugno 1985, n.  298    &#13;
 nella  parte  in  cui dichiara non perseguibili penalmente coloro che    &#13;
 abbiano  demolito  od  eliminato  le  opere  abusive  entro  la  data    &#13;
 d'entrata  in  vigore della legge di conversione del decreto legge, e    &#13;
 quindi entro il 22 giugno 1985, senza nulla prevedere  per  le  opere    &#13;
 demolite successivamente a tale data;                                    &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri, chiedendo che la questione sia dichiarata infondata;           &#13;
    Considerato  che  analoga  questione  è stata decisa con sentenza    &#13;
 interpretativa di rigetto n. 369 del 1988 - la quale ha ritenuto  che    &#13;
 il capo primo della legge n. 47 del 1985 (che prevede la possibilità    &#13;
 d'ottenere la licenza in sanatoria  con  conseguente  estinzione  del    &#13;
 reato)  è  applicabile anche per le opere abusive realizzate dopo il    &#13;
 1°  ottobre  1983  e  demolite  dopo  il  6  luglio  1985  -  e  che,    &#13;
 successivamente,  altre  analoghe  questioni  sono  state  dichiarate    &#13;
 manifestamente infondate con le ordinanze n. 704 del 1988, n. 912 del    &#13;
 1988 e n. 1098 del 1988;                                                 &#13;
      che  dall'ordinanza  di rinvio risulta che nella specie il reato    &#13;
 di costruzione abusiva è stato appunto commesso dopo il  1°  ottobre    &#13;
 1983  e  che  la demolizione delle opere è avvenuta dopo il 6 luglio    &#13;
 1985;                                                                    &#13;
      che  l'attuale  rimettente  non  prospetta profili né argomenti    &#13;
 nuovi o diversi rispetto a quelli già esaminati dalla Corte  con  le    &#13;
 ricordate  decisioni e che, di conseguenza, la sollevata questione va    &#13;
 dichiarata manifestamente infondata;                                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 13 e 22 della legge 28 febbraio  1985,  n.    &#13;
 47     (Norme     in     materia    di    controllo    dell'attività    &#13;
 urbanistico-edilizia, sanzioni,  recupero  e  sanatoria  delle  opere    &#13;
 edilizie)  e  dell'art.  8-quater  del  decreto legge 28 aprile 1985,    &#13;
 n.146, introdotto dalla legge di conversione 21 giugno 1985,  n.  298    &#13;
 (Conversione in legge, con modificazioni, del decreto legge 28 aprile    &#13;
 1985, n. 146, recante proroga di taluni termini di cui alla legge  28    &#13;
 febbraio  1985,  n.  47  concernente  norme  in  materia di controllo    &#13;
 dell'attività urbanistico-edilizia, sanzioni, recupero  e  sanatoria    &#13;
 delle opere edilizie) sollevata, in riferimento all'art. 3 Cost., dal    &#13;
 Pretore di Sorrento con l'ordinanza indicata in epigrafe.                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 13 aprile 1989.         &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 aprile 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
