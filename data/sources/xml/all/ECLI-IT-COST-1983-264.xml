<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>264</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:264</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Brunetto Bucciarelli Ducci</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/09/1983</data_decisione>
    <data_deposito>26/09/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof . GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. &#13;
 ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt.  228, comma  &#13;
 terzo,  r.d.  3  marzo 1934, n. 383 (Testo unico della legge comunale e  &#13;
 provinciale) e 26 della legge 28 ottobre 1970,  n.  775  (Modifiche  ed  &#13;
 integrazioni  alla legge 18 marzo 1968, n. 249), promosso con ordinanza  &#13;
 emessa il 23 novembre 1977 dal Tribunale Amministrativo  Regionale  per  &#13;
 l'Emilia-Romagna,  sul ricorso proposto da Mariotti Francesco contro il  &#13;
 Comune di Rimini, iscritta al n.   561 del registro  ordinanze  1979  e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 265 del 1979;    &#13;
     visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito, nella  pubblica  udienza  del  12  aprile  1983  il  Giudice  &#13;
 relatore Brunetto Bucciarelli Ducci;                                     &#13;
     udito   l'avvocato  dello  Stato  Giuseppe  Angelini  Rota  per  il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ricorso 1  ottobre 1971 al Tribunale  Amministrativo  Regionale  &#13;
 per  l'Emilia-Romagna  Mariotti  Francesco,  impiegato  del  Comune  di  &#13;
 Rimini, impugnò le deliberazioni 16 e 26 aprile 1971, 19 luglio  1971,  &#13;
 con  le  quali  il  Consiglio  Comunale  di  Rimini  - nel procedere al  &#13;
 riassetto dei ruoli, delle carriere e delle retribuzioni del  personale  &#13;
 -  aveva  considerato per intero il precedente servizio da lui prestato  &#13;
 nella carriera direttiva quale funzionario di  ruolo  dal  26  febbraio  &#13;
 1956  e  per  metà  il  servizio prestato quale funzionario avventizio  &#13;
 nella medesima carriera dal 1  luglio 1940.                              &#13;
     Il TAR dell'Emilia-Romagna, con ordinanza 23 novembre 1977, ritenne  &#13;
 rilevante ai fini della decisione e non manifestamente infondata  -  in  &#13;
 riferimento   all'art.   36   della  Costituzione  -  la  questione  di  &#13;
 legittimità costituzionale degli artt.  228, comma terzo, r.d. 3 marzo  &#13;
 1934, n. 383 (Testo unico della legge  comunale  e  provinciale)  e  26  &#13;
 legge  28 ottobre 1970, n. 775 (modifiche ed integrazioni alla legge 18  &#13;
 marzo 1968, n. 249).                                                     &#13;
     L'ordinanza fu pubblicata nella Gazzetta Ufficiale n.  265  del  26  &#13;
 settembre 1979.                                                          &#13;
     Nel  giudizio  davanti a questa Corte non si è costituita la parte  &#13;
 privata.                                                                 &#13;
     È  intervenuto  il  Presidente   del   Consiglio   dei   Ministri,  &#13;
 rappresentato  e  difeso  dall'Avvocato  Generale dello Stato, con atto  &#13;
 depositato  il  16  ottobre  1979,  chiedendo  che  la   questione   di  &#13;
 legittimità costituzionale sia dichiarata non fondata.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'art.  228,  comma  terzo,  r.d. 3 marzo 1934, n.  383, prescrive,  &#13;
 nella parte  seconda,  che  il  servizio  prestato  dagli  impiegati  e  &#13;
 salariati dei Comuni e delle Province presso la stessa amministrazione,  &#13;
 precedentemente alla nomina a posti di ruolo, in qualità di provvisori  &#13;
 o  avventizi,  può  essere  riconosciuto  in loro favore, agli effetti  &#13;
 degli aumenti periodici di stipendio, nella stessa misura stabilita per  &#13;
 gli impiegati dello Stato.                                               &#13;
     L'art. 26 legge 28 ottobre  1970,  n.  775,  recante  modifiche  ed  &#13;
 integrazioni alla legge 18 marzo 1968, n. 249 (Delega al Governo per il  &#13;
 riordinamento  delle  Amministrazioni dello Stato, per il decentramento  &#13;
 delle funzioni e per il riassetto delle carriere e  delle  retribuzioni  &#13;
 dei  dipendenti),  prevede  la  valutazione  per  metà  del  servizio,  &#13;
 comunque prestato, anteriormente alla nomina nella stessa carriera,  ai  &#13;
 fini  delle  attribuzioni  delle  classi  di  stipendio  o  paghe nelle  &#13;
 qualifiche o categorie di appartenenza alla data di entrata  in  vigore  &#13;
 dei  relativi  decreti  delegati.  Tali norme, ad avviso del giudice "a  &#13;
 quo", sarebbero in contrasto con l'art.  36  della  Costituzione  -  il  &#13;
 quale  prescrive  che  il  lavoratore  ha  diritto  ad una retribuzione  &#13;
 proporzionata alla qualità  e  quantità  del  suo  lavoro  -  perché  &#13;
 prevedono  una  valutazione  del  servizio  prestato dall'impiegato del  &#13;
 Comune quale avventizio diversa da quella del servizio di ruolo, mentre  &#13;
 non  vi  sarebbe  differenza  sostanziale  tra  le  prestazioni  di  un  &#13;
 dipendente  di ruolo e quelle del dipendente fuori ruolo, che ricoprono  &#13;
 lo  stesso  posto,  essendo  eguali  sia  il  titolo  di  studio  e  la  &#13;
 preparazione  professionale  richiesti  per  la nomina, sia le mansioni  &#13;
 svolte  sia  i  rischi  ed  i  disagi  imposti,   sia   il   grado   di  &#13;
 responsabilità.                                                         &#13;
     Questa  diversa  valutazione  non  potrebbe  ritenersi giustificata  &#13;
 dalla  mancata  sottoposizione  del  dipendente  avventizio  a   quella  &#13;
 verifica della preparazione e capacità attraverso le prove di concorso  &#13;
 di  ammissione,  alle  quali  è  stato invece soggetto il personale di  &#13;
 ruolo, sia perché la nomina degli impiegati  di  ruolo  a  seguito  di  &#13;
 concorso non costituisce un principio inderogabile e subisce eccezioni,  &#13;
 sia perché il permanere dell'impiegato nella stessa attività comporta  &#13;
 notoriamente  un  affinamento  delle  sue  capacità  lavorative  ed un  &#13;
 miglioramento  del  suo  rendimento  e,  proprio  per   tale   maggiore  &#13;
 produttività  individuale in connessione con la maggiore anzianità di  &#13;
 qualifica, sono previsti gli aumenti periodici di stipendio nelle  loro  &#13;
 diverse denominazioni.                                                   &#13;
     La questione non è fondata.                                         &#13;
     Il  principio  del  diritto  del  lavoratore  ad  una  retribuzione  &#13;
 proporzionata alla quantità e qualità del  suo  lavoro  -  proclamato  &#13;
 dall'art.  36,  comma primo, della Costituzione - impone al legislatore  &#13;
 di attribuire lo stesso trattamento economico a coloro che esplicano le  &#13;
 medesime  mansioni,  e,  quindi,  per quanto concerne la fattispecie in  &#13;
 esame,  di  corrispondere  all'impiegato   non   di   ruolo,   all'atto  &#13;
 dell'immissione in ruolo, lo stipendio con le relative indennità nella  &#13;
 stessa  misura  spettante  all'impiegato  di  ruolo,  con  la  medesima  &#13;
 qualifica, all'inizio del suo rapporto di pubblico impiego.              &#13;
     L'invocato principio tuttavia non opera retroattivamente  né  può  &#13;
 dunque  essere  applicato  nel  senso  di  ritenere  costituzionalmente  &#13;
 garantita  la  completa  equiparazione,  sia  pure  ai   soli   effetti  &#13;
 economici, del pregresso servizio non di ruolo a quello di ruolo.        &#13;
     Invero  la proporzione della retribuzione alla quantità e qualità  &#13;
 del lavoro prestato - prescritta dal citato art. 36 della  Costituzione  &#13;
 - va accertata con riferimento al momento in cui l'attività lavorativa  &#13;
 è   svolta  nell'ambito  di  rapporti  di  impiego  aventi  le  stesse  &#13;
 caratteristiche, mentre l'equiparazione del precedente servizio non  di  &#13;
 ruolo  a  quello  successivo  di ruolo, effettuata dopo l'immissione in  &#13;
 ruolo, comporterebbe  il  disconoscimento  dei  ben  precisi  caratteri  &#13;
 distintivi  delle  due forme di rapporto, quello di ruolo e l'altro non  &#13;
 di ruolo, che sono stati posti in  evidenza  da  questa  Corte  con  la  &#13;
 sentenza n. 52 del 1981.                                                 &#13;
     Al  riguardo  è  sufficiente  rilevare che l'impiegato di ruolo è  &#13;
 assunto  a  seguito  del  superamento  delle  prove  del  concorso   di  &#13;
 ammissione, previsto e disciplinato da particolari norme in adempimento  &#13;
 del  precetto  generale dell'art. 97, comma ultimo, della Costituzione,  &#13;
 il quale prescrive che agli impieghi nelle pubbliche amministrazioni si  &#13;
 accede mediante concorso, salvo i casi stabiliti dalla legge.            &#13;
     Oltre a questa differenza del modo di costituzione, il rapporto non  &#13;
 di ruolo si distingue da quello di ruolo  perché  ha  la  funzione  di  &#13;
 soddisfare  esigenze  eccezionali  ed  indilazionabili,  ma transitorie  &#13;
 della Pubblica Amministrazione; quini, carattere fondamentale  di  esso  &#13;
 è  la  sua  precarietà,  e la relativa disciplina giuridica, in linea  &#13;
 generale, è ben diversa da  quella  dell'impiego  di  ruolo  (sentenza  &#13;
 citata n. 52 del 1981).                                                  &#13;
     Il  permanere  dell'impiegato,  prima  non  di ruolo, poi di ruolo,  &#13;
 nella medesima  attività  ed  il  conseguente  affinamento  delle  sue  &#13;
 capacità  lavorative  e  miglioramento del suo rendimento giustificano  &#13;
 l'immissione dell'avventizio in ruolo, quale giusto riconoscimento  del  &#13;
 modo   con   cui   ha   svolto  le  sue  mansioni,  ma  non  comportano  &#13;
 necessariamente,  alla  stregua   dei   principi   costituzionali,   la  &#13;
 equiparazione  del  servizio non di ruolo a quello di ruolo, neppure ai  &#13;
 soli effetti economici, per la netta distinzione tra queste  due  forme  &#13;
 di rapporto di pubblico impiego, già posta in evidenza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 228, comma terzo, parte seconda, r.d. 3 marzo 1934, n.  383  &#13;
 (Testo  unico della legge comunale e provinciale) e 26 legge 28 ottobre  &#13;
 1970, n. 775 (modifiche ed integrazioni alla legge 18  marzo  1968,  n.  &#13;
 249),    proposta    dal   Tribunale   Amministrativo   Regionale   per  &#13;
 l'Emilia-Romagna, con l'ordinanza in epigrafe, in riferimento  all'art.  &#13;
 36, comma primo, della Costituzione.                                     &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte Costituzionale,  &#13;
 Palazzo della Consulta, il 20 settembre 1983.                            &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   - ANTONINO  DE  STEFANO  -  GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - GIUSEPPE FERRARI  &#13;
                                   - FRANCESCO SAJA - GIOVANNI  CONSO  -  &#13;
                                   ETTORE GALLO.                          &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
