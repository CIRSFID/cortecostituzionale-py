<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>297</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:297</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/07/1996</data_decisione>
    <data_deposito>23/07/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, &#13;
 prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, &#13;
 prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art. 262 del  codice    &#13;
 civile promosso con ordinanza emessa il 15 gennaio 1996 dal tribunale    &#13;
 di Salerno sul ricorso proposto da Libero Luciana iscritta al n.  287    &#13;
 del  registro  ordinanze  1996  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 14, prima serie speciale, dell'anno 1996;            &#13;
   Udito nella camera di consiglio  del  10  luglio  1996  il  giudice    &#13;
 relatore Renato Granata.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  ordinanza  del 15 gennaio 1996 il tribunale di Salerno - adito    &#13;
 con ricorso diretto ad ottenere  l'accertamento  del  diritto  di  un    &#13;
 figlio  naturale  di  anteporre  al cognome, derivatogli   dall'unico    &#13;
 riconoscimento della madre naturale intervenuto oltre  quaranta  anni    &#13;
 dopo  il  parto,  il  precedente cognome attribuito dall'ufficiale di    &#13;
 stato civile - ha sollevato  questione  incidentale  di  legittimità    &#13;
 costituzionale   dell'art.  262  del  codice  civile  in  riferimento    &#13;
 all'art.  2 della Costituzione, nella parte in cui non prevede che il    &#13;
 figlio naturale, assumendo il cognome del genitore che per  primo  lo    &#13;
 abbia    riconosciuto,   ha   diritto   di   mantenere   il   cognome    &#13;
 originariamente attribuitogli ove questo sia ormai da ritenersi segno    &#13;
 distintivo della sua identità personale.                                &#13;
   In particolare il tribunale rimettente osserva che, tra  i  diritti    &#13;
 che  formano il patrimonio irretrattabile della persona umana, l'art.    &#13;
 2  della  Costituzione  riconosce  e  garantisce  anche  il   diritto    &#13;
 all'identità  personale, primo e più immediato elemento della quale    &#13;
 è  proprio  il  nome,  sicché  sussiste  un'autonoma  esigenza   di    &#13;
 protezione  dell'interesse alla conservazione del cognome, attribuito    &#13;
 con atto formalmente legittimo, in presenza di una  situazione  nella    &#13;
 quale  con quel cognome la persona sia ormai individuata e conosciuta    &#13;
 nell'ambiente ove vive.                                                  &#13;
   Il tribunale rimettente invoca poi la sentenza n. 13  del  1994  di    &#13;
 questa  Corte,  che ha dichiarato costituzionalmente illegittimo, per    &#13;
 violazione dell'art. 2 della Costituzione,  l'art.  165  del  r.d.  9    &#13;
 luglio 1939, n. 1238 (Ordinamento dello stato civile), nella parte in    &#13;
 cui  non  prevede  che,  quando  la  rettifica degli atti dello stato    &#13;
 civile, intervenuta per ragioni  indipendenti  dal  soggetto  cui  si    &#13;
 riferisce,  comporti  il  cambiamento  del cognome il soggetto stesso    &#13;
 possa ottenere dal giudice il riconoscimento del diritto a  mantenere    &#13;
 il  cognome  originariamente  attribuitogli  ove  questo sia ormai da    &#13;
 ritenersi segno distintivo della sua identità personale.<diritto>Considerato in diritto</diritto>1.  -  È  stata  sollevata  questione  incidentale di legittimità    &#13;
 costituzionale - in  riferimento  all'art.  2  della  Costituzione  -    &#13;
 dell'art.  262  del codice civile, nella parte in cui non prevede che    &#13;
 il figlio naturale, assumendo il cognome del genitore che  per  primo    &#13;
 lo   abbia   riconosciuto,   ha   diritto  di  mantenere  il  cognome    &#13;
 originariamente attribuitogli ove questo sia ormai da ritenersi segno    &#13;
 distintivo della sua identità personale, per sospetta violazione del    &#13;
 diritto fondamentale all'identità personale.                            &#13;
   2. - La questione è fondata.                                          &#13;
   2.1. - La Corte, nella sentenza n. 13 del 1994 ha già riconosciuto    &#13;
 che il cognome "gode di una distinta tutela anche nella sua  funzione    &#13;
 di  strumento  identificativo  della  persona, e che, in quanto tale,    &#13;
 costituisce parte essenziale ed irrinunciabile  della  personalità";    &#13;
 tutela  che  è  di  rilievo  costituzionale  perché  il  nome,  che    &#13;
 costituisce "il primo e  più  immediato  elemento  che  caratterizza    &#13;
 l'identità   personale",  è  riconosciuto  "come  bene  oggetto  di    &#13;
 autonomo diritto" dall'art. 2 della Costituzione.  D'altra  parte  il    &#13;
 diritto    all'identità   personale   costituisce   tipico   diritto    &#13;
 fondamentale,  rientrando  esso  tra  "i  diritti  che   formano   il    &#13;
 patrimonio irretrattabile della persona umana" sicché la sua lesione    &#13;
 integra la violazione dell'art. 2 citato.                                &#13;
   2.2.  -  Orbene, la disposizione censurata - dopo aver previsto (al    &#13;
 primo comma) che il figlio naturale assume il  cognome  del  genitore    &#13;
 che  per  primo  lo  ha  riconosciuto (con prevalenza del cognome del    &#13;
 padre in caso di riconoscimento contemporaneo di entrambi i genitori)    &#13;
 - prescrive (al secondo comma) che, se la  filiazione  nei  confronti    &#13;
 del   padre   è   accertata   o   riconosciuta   successivamente  al    &#13;
 riconoscimento  da  parte  della  madre,  il  figlio  naturale  possa    &#13;
 assumere  il cognome del padre aggiungendolo o sostituendolo a quello    &#13;
 della madre. In tal modo la norma appronta una specifica e  peculiare    &#13;
 tutela del diritto all'identità personale, che comprende - come già    &#13;
 sottolineato   -   il   diritto   al   nome   come  principale  segno    &#13;
 identificativo   della   persona.      Infatti   è   possibile   che    &#13;
 nell'intervallo  di  tempo tra il riconoscimento della madre e quello    &#13;
 successivo del padre il figlio naturale abbia  maturato  una  precisa    &#13;
 identità  personale  per  il  fatto  di  essere  riconosciuto, nella    &#13;
 comunità dove è vissuto, con il cognome  derivatogli  dalla  madre.    &#13;
 Essendosi  così  radicata  una  corrispondenza  tra soggetto e nome,    &#13;
 riferibile al contenuto tipico  deldiritto  all'identità  personale,    &#13;
 l'ordinamento   appronta   un'idonea  garanzia  contemplando  -  come    &#13;
 rilevato - la facoltà del figlio naturale di aggiungere (invece  che    &#13;
 sostituire)  il  cognome del padre a quello della madre. Una medesima    &#13;
 ratio è sottesa all'art. 5, terzo  comma,  della  legge  1  dicembre    &#13;
 1970,  n.  898 che, in caso di scioglimento del matrimonio, riconosce    &#13;
 la facoltà della donna di conservare il cognome  del  marito  quando    &#13;
 sussiste un interesse suo o dei figli meritevole di tutela.              &#13;
   Per contro, analoga tutela la norma censurata non prevede nel caso,    &#13;
 sostanzialmente  similare,  in cui il primo riconoscimento di uno dei    &#13;
 due genitori avvenga (come nel caso di specie)  in  epoca  ampiamente    &#13;
 successiva  alla  attribuzione  del  nome  e  del  cognome  da  parte    &#13;
 dell'ufficiale di stato  civile.  Anche  in  questo  caso  il  figlio    &#13;
 naturale ha visto intanto radicarsi la sua identità in tale nome, la    &#13;
 cui  conservazione  però  non  viene  salvaguardata  -  come  invece    &#13;
 nell'ipotesi precedente - con il  riconoscimento  della  facoltà  di    &#13;
 aggiungere  il  cognome del genitore che ha operato il riconoscimento    &#13;
 al cognome originariamente attribuitogli. Né rileva  il  fatto  che,    &#13;
 ove  il  figlio  - come nella specie - abbia compiuto sedici anni, il    &#13;
 riconoscimento non possa avvenire senza il suo consenso,  negando  il    &#13;
 quale  l'interessato  eviterebbe la conseguenza di vedere sostituire,    &#13;
 allo stato attuale della normativa, il nuovo  cognome  a  quello  che    &#13;
 ormai  lo individua nella comunità in cui vive, così preservando il    &#13;
 diritto alla sua identità personale:  ciò invero potrebbe  avvenire    &#13;
 soltanto con la rinuncia al riconoscimento stesso, e quindi il figlio    &#13;
 ultrasedicenne si troverebbe costretto a scegliere se privilegiare la    &#13;
 sua  identità  personale  o  il suo stato di filiazione. Alternativa    &#13;
 questa del tutto incongrua ed irragionevole,  ove  si  consideri  che    &#13;
 nessuna situazione di conflitto insorge tra tali diritti, entrambi di    &#13;
 rilievo  costituzionale,  ben  potendo la tutela dell'uno conciliarsi    &#13;
 con la tutela dell'altro, senza necessità di sacrificare alcuno  dei    &#13;
 due, con l'attribuzione al figlio naturale, che assume il cognome del    &#13;
 genitore  che  lo  ha  riconosciuto,  del  diritto  di  conservare  -    &#13;
 aggiungendolo o anteponendolo, a sua scelta, a questo  -  il  cognome    &#13;
 precedentemente  conferitogli  con atto formalmente legittimo, quando    &#13;
 tale cognome - secondo il prudente apprezzamento del giudice - sia da    &#13;
 ritenersi divenuto autonomo  segno  distintivo  della  sua  identità    &#13;
 personale.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale dell'art. 262 del codice    &#13;
 civile, nella parte in  cui  non  prevede  che  il  figlio  naturale,    &#13;
 nell'assumere  il  cognome del genitore che lo ha riconosciuto, possa    &#13;
 ottenere dal giudice  il  riconoscimento  del  diritto  a  mantenere,    &#13;
 anteponendolo  o,  a  sua  scelta, aggiungendolo a questo, il cognome    &#13;
 precedentemente attribuitogli con  atto  formalmente  legittimo,  ove    &#13;
 tale  cognome  sia  divenuto  autonomo  segno  distintivo  della  sua    &#13;
 identità personale.                                                     &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1996.                               &#13;
                         Il Presidente: Ferri                             &#13;
                         Il redattore: Granata                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 luglio 1996.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
