<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>352</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:352</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1985</data_decisione>
    <data_deposito>17/12/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - &#13;
 Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE &#13;
 FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE &#13;
 GALLO - Dott. ALDO CORASANITI - Dott. GIUSEPPE BORZELLINO - Dott. &#13;
 FRANCESCO GRECO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 47 legge 26  &#13;
 luglio 1975, n. 354 (ordinamento penitenziario) in relazione agli artt.  &#13;
 582 cod. proc. pen. e 76 cod. pen.  promosso con ordinanza emessa il 14  &#13;
 febbraio 1985 dalla Sezione di Sorveglianza presso la  Corte  d'appello  &#13;
 delle  Marche  nel  procedimento  di  sorveglianza  relativo a Serafini  &#13;
 Piero, iscritta al n. 195 del  registro  ordinanze  1985  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 101 bis dell'anno 1985.     &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del 20  novembre  1985  il  Giudice  &#13;
 relatore Alberto Malagugini.                                             &#13;
     Ritenuto  che  con  l'ordinanza  indicata in epigrafe la Sezione di  &#13;
 sorveglianza presso  la  Corte  d'appello  delle  Marche  dubita  della  &#13;
 legittimità  costituzionale:  a)  dell'art.    47, ultimo comma, della  &#13;
 legge 26 luglio 1975, n. 354 (ordinamento penitenziario), assumendo che  &#13;
 detta norma, nella parte in cui esclude che valga  come  espiazione  di  &#13;
 pena  il periodo di affidamento in prova al servizio sociale in caso di  &#13;
 revoca  per  inammissibilità   sopravvenuta   del   provvedimento   di  &#13;
 ammissione,  contrasti  con gli artt. 3, 13, secondo comma, e 27, terzo  &#13;
 comma, Cost.; b) del medesimo art. 47 l. n. 354/1975  "in  correlazione  &#13;
 con  gli  artt.  582  c.p.p.    e  76  c.p., per la parte in cui non è  &#13;
 disciplinato,  durante  il periodo di affidamento, il concorso di altre  &#13;
 pene, ed è precluso alla Sezione di sorveglianza (in caso di riarresto  &#13;
 dell'affidato per l'espiazione di diversa condanna) di provvedere  alla  &#13;
 dichiarazione  di estinzione della pena a termine dell'ultimo comma del  &#13;
 medesimo art. 47":  sostenendo al riguardo che la disciplina risultante  &#13;
 dalle predette norme - in forza della quale, in caso di  sopravvenienza  &#13;
 di  altra  condanna,  spetta  al  P.  M.  di  provvedere al cumulo e di  &#13;
 disporre il riarresto dell'affidato ove sia superato il  limite  di  30  &#13;
 mesi  fissato  per  l'ammissibilità  della  misura  dal citato art. 47  &#13;
 darebbe luogo a violazione:                                              &#13;
     - dell'art. 3 Cost., in quanto  il  completamento  del  periodo  di  &#13;
 prova  (con  conseguente  estinzione  della pena) o la sua interruzione  &#13;
 dipenderebbero  dalla  maggiore  o  minore  solerzia  del  P.  M.   nel  &#13;
 provvedere all'unificazione delle pene concorrenti;                      &#13;
     -  con l'art. 25 Cost., in quanto l'attribuzione al P. M., anziché  &#13;
 alla Sezione di sorveglianza, del potere di provvedere al cumulo (e  di  &#13;
 determinare,  con  ciò,  la cessazione dell'affidamento), sottrarrebbe  &#13;
 l'affidato al suo giudice naturale;                                      &#13;
     - con l'art. 13, secondo comma, Cost., in quanto la pena modificata  &#13;
 in affidamento in prova  non  potrebbe  considerarsi  come  pena  della  &#13;
 stessa  specie,  sicché  sarebbe  insuscettibile  di cumulo e potrebbe  &#13;
 ritradursi  in  detenzione  in  carcere  non  per  mero   provvedimento  &#13;
 dell'organo  di  esecuzione  ma  solo  con motivato provvedimento della  &#13;
 Sezione di sorveglianza;                                                 &#13;
     - con  l'art.  27  Cost.,  in  quanto  il  riarresto  dell'affidato  &#13;
 frustrerebbe la sua legittima aspettativa ad offrire una buona prova ed  &#13;
 a conseguire così l'estinzione della pena.                              &#13;
     Considerato  che  la  questione  sub a) è stata già decisa con la  &#13;
 sentenza n. 312/1985 con la quale è stata dichiarata  l'illegittimità  &#13;
 costituzionale  dell'art. 47 della legge 26 luglio 1975, n. 354, "nella  &#13;
 parte in cui non prevede che valga come espiazione di pena  il  periodo  &#13;
 di  affidamento  in  prova  al servizio sociale, nel caso di revoca del  &#13;
 provvedimento  di  ammissione  per  motivi  non  dipendenti  dall'esito  &#13;
 negativo  della  prova";  che  pertanto  tale  questione  va dichiarata  &#13;
 manifestamente infondata;                                                &#13;
     che con la questione sub b) il giudice a quo sostanzialmente assume  &#13;
 che, ove con la  pena  in  relazione  alla  quale  sia  stato  disposto  &#13;
 l'affidamento  in  prova  al  servizio sociale concorra altra pena che,  &#13;
 cumulata  con  la  prima,  comporti  il  superamento  dei   limiti   di  &#13;
 ammissibilità della misura fissata dal primo comma del citato art. 47,  &#13;
 non dovrebbe procedersi all'unificazione delle pene concorrenti secondo  &#13;
 le  regole  ordinarie, ma dovrebbe dettarsi una speciale disciplina che  &#13;
 consenta il completamento del periodo  di  affidamento  (e,  con  esso,  &#13;
 l'estinzione  della  pena) ed attribuirsi alla sezione di sorveglianza,  &#13;
 anziché al P. M., la competenza a provvedere in merito;                 &#13;
     che è peraltro  evidente  che,  con  siffatta  prospettazione,  si  &#13;
 chiede  alla Corte di introdurre, per l'ipotesi in questione, una nuova  &#13;
 e complessa regolamentazione normativa, derogatoria rispetto  a  quella  &#13;
 ordinaria  sia  in  tema  di  disciplina  del  concorso di pene, che di  &#13;
 attribuzione delle competenze nella fase esecutiva;                      &#13;
     che pertanto, poiché  -  secondo  la  costante  giurisprudenza  di  &#13;
 questa   Corte   -   l'innovazione   al   sistema   normativo,  nonché  &#13;
 l'individuazione, tra i vari possibili, del tipo di rimedio che valga a  &#13;
 realizzarla  esulano  dai  poteri  della  Corte  e  rientrano,  invece,  &#13;
 nell'esclusiva  competenza del legislatore, la questione sollevata deve  &#13;
 essere dichiarata manifestamente inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la manifesta infondatezza della questione di  legittimità  &#13;
 costituzionale  dell'art. 47, ultimo comma, della legge 26 luglio 1975,  &#13;
 n. 354 - già dichiarato illegittimo  nella  parte  impugnata,  con  la  &#13;
 sentenza  n. 312 del 1985 - sollevata in riferimento agli artt. 3, 13 e  &#13;
 27 Cost. dalla Sezione di sorveglianza presso la Corte d'appello  delle  &#13;
 Marche con l'ordinanza indicata in epigrafe (r.o. 195/85);               &#13;
     dichiara   la   manifesta   inammissibilità   della  questione  di  &#13;
 legittimità costituzionale del predetto art.  47,  in  relazione  agli  &#13;
 artt.  582 c.p.p. e 76 c.p., sollevata in riferimento agli artt. 3, 13,  &#13;
 25 e 27 Cost. con la medesima ordinanza.                                 &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 dicembre 1985.       &#13;
                                   F.to:  LIVIO PALADIN - ORONZO REALE -  &#13;
                                   BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  &#13;
                                   MALAGUGINI  -  ANTONIO  LA  PERGOLA -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   - RENATO DELL'ANDRO.                   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
