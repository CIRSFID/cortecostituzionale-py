<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>11</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:11</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/1992</data_decisione>
    <data_deposito>22/01/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 56, del r.d.l.    &#13;
 4 ottobre 1935, n. 1827 (Perfezionamento e coordinamento  legislativo    &#13;
 della  previdenza  sociale),  convertito  con modifiche nella legge 6    &#13;
 aprile 1936, n. 1155, come modificato  dall'art.  36  della  legge  3    &#13;
 giugno  1975,  n.  160  (Norme  per  il miglioramento dei trattamenti    &#13;
 pensionistici  e  per  il  collegamento  alla  dinamica   salariale),    &#13;
 promosso  con  ordinanza  emessa  il  29  maggio  1991 dal Pretore di    &#13;
 Firenze nel procedimento civile vertente tra Monti Luigi  e  I.N.P.S.    &#13;
 iscritta  al  n.  523  del registro ordinanze 1991 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  33,  prima  serie  speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Visti  gli atti di costituzione di Allegranti Maria, Monti Daniele    &#13;
 e Monti Sandro aventi causa di Monti Luigi  e  dell'I.N.P.S.  nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
    Udito  nella  camera  di consiglio del 18 dicembre 1991 il Giudire    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto che, nel corso di un giudizio civile  promosso  da  Luigi    &#13;
 Monti   contro   l'INPS  per  il  riconoscimento  della  pensione  di    &#13;
 invalidità, il Pretore di Firenze, con ordinanza del 29 maggio 1991,    &#13;
 ha sollevato questione di legittimità  costituzionale  dell'art.  56    &#13;
 del  r.d.l.  4 ottobre 1935, n. 1827, convertito nella legge 6 aprile    &#13;
 1936, n. 1155, come modificato dall'art.  36  della  legge  3  giugno    &#13;
 1975,  n.  160, "nella parte in cui limita a dodici mesi i periodi di    &#13;
 malattia utili agli effetti del diritto a pensione";                     &#13;
      che, ad avviso del giudice remittente, la norma impugnata  viola    &#13;
 l'art.   38,   secondo   comma   Cost.,   perché  riduce  la  tutela    &#13;
 dell'invalidità e della vecchiaia per quei  lavoratori  che,  avendo    &#13;
 sofferto  nel  corso del rapporto malattie di lunga durata, sono più    &#13;
 bisognevoli di protezione, e l'art.  3  Cost.  perché  riserva  alle    &#13;
 malattie  diverse dalla tubercolosi un trattamento meno favorevole di    &#13;
 quello previsto dall'art. 4, quarto comma, della legge 4 aprile 1952,    &#13;
 n. 218, nel testo sostituito dall'art. 3 della legge 4 marzo 1987, n.    &#13;
 88,  tenuto  conto  che,  "col  progresso  della  medicina  e   della    &#13;
 prevenzione,  la  tubercolosi  ha  perso  il  carattere  di  malattia    &#13;
 sociale", con conseguente idoneità della disposizione testé  citata    &#13;
 a fungere da tertium comparationis;                                      &#13;
      che  nel  giudizio  davanti  alla  Corte si è costituito l'INPS    &#13;
 chiedendo che la questione sia dichiarata  inammissibile  e  comunque    &#13;
 infondata;                                                               &#13;
      che  si  sono  costituiti pure gli eredi del ricorrente con atto    &#13;
 depositato il 25 novembre 1991, e quindi fuori termine;                  &#13;
      che è intervenuto il Presidente  del  Consiglio  dei  ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, chiedendo che la questione    &#13;
 sia dichiarata non fondata;                                              &#13;
    Considerato  che,  essendo  assicurata  in ogni caso al lavoratore    &#13;
 l'integrazione  al  trattamento  minimo   di   pensione,   non   può    &#13;
 prospettarsi una violazione dell'art. 38, secondo comma, Cost.;          &#13;
      che il citato art. 4, quarto comma, della legge n. 218 del 1952,    &#13;
 secondo  cui  i  periodi di assenza dal lavoro per cure sanatoriali e    &#13;
 postsanatoriali della tubercolosi sono utili  senza  limiti  ai  fini    &#13;
 della  pensione,  non  può costituire un termine di paragone per una    &#13;
 valutazione della norma  impugnata  alla  stregua  del  principio  di    &#13;
 eguaglianza  sancito  dall'art.  3  Cost., si ritenga o no cessato il    &#13;
 carattere di malattia sociale della tubercolosi: nel  primo  caso  la    &#13;
 norma   indicata  come  tertium  comparationis  sarebbe  divenuta  un    &#13;
 privilegio ingiustificato, mentre nel caso contrario resterebbe ferma    &#13;
 la  valutazione  di  essa  come  ius  singulare  giustificato   dalla    &#13;
 specialità della situazione regolata;                                   &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  56, primo comma, lettera a), punto 1°, del    &#13;
 r.d.l. 4 ottobre  1935,  n.  1827  (Perfezionamento  e  coordinamento    &#13;
 legislativo  della  previdenza  sociale),  convertito  nella  legge 6    &#13;
 aprile 1936, n. 1155, modificato dall'art. 36 della  legge  4  giugno    &#13;
 1975,   n.   160   (Norme   per   il  miglioramento  dei  trattamenti    &#13;
 pensionistici  e  per  il  collegamento  alla  dinamica   salariale),    &#13;
 sollevata,  in  riferimento agli artt. 3 e 38 della Costituzione, dal    &#13;
 Pretore di Firenze con l'ordinanza indicata in epigrafe.                 &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 gennaio 1992.                              &#13;
                       Il presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 22 gennaio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
