<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>315</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:315</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/06/1995</data_decisione>
    <data_deposito>12/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale dell'art. 141 d.P.R. 15    &#13;
 giugno 1959, n. 393  (Testo  unico  delle  norme  sulla  circolazione    &#13;
 stradale),  come novellato dall'art. 22 della legge 24 marzo 1989, n.    &#13;
 122, del combinato disposto degli artt. 142 e  142-bis  dello  stesso    &#13;
 d.P.R.  15  giugno  1959,  n. 393, cone novellati dagli artt. 23 e 24    &#13;
 della legge 24 marzo 1989, n. 122, degli artt. 142, ultimo  comma,  e    &#13;
 142-bis  del menzionato d.P.R. 15 giugno 1959, n. 393, come novellati    &#13;
 dagli artt. 23 e 24 della legge n. 122 del 1989 e dell'art.  203  del    &#13;
 decreto  legislativo  30  aprile  1992,  n.  285  (Nuovo codice della    &#13;
 strada), promossi con le ordinanze  emesse  il  24  maggio  1994  dal    &#13;
 pretore  di  Torino,  il  17  giugno 1994 dal pretore di Rimini (n. 5    &#13;
 ordinanze),  il  3  maggio  1994  dal  pretore  di  Pordenone  (n.  4    &#13;
 ordinanze),  iscritte  ai nn. 436, 557, 608, 609, 610, 611, 723, 724,    &#13;
 725 e 726 del registro ordinanze 1994  e  pubblicate  nella  Gazzetta    &#13;
 Ufficiale della Repubblica nn. 30, 40, 42 e 50, prima serie speciale,    &#13;
 dell'anno 1994;                                                          &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella  camera  di  consiglio  del  22  febbraio  il  Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
   Ritenuto che, nel corso di un giudizio instaurato per l'opposizione    &#13;
 a due cartelle esattoriali emesse per il pagamento di pene pecuniarie    &#13;
 irrogate  a  seguito di violazioni del codice della strada, accertate    &#13;
 dal giugno del 1989 al settembre del 1992, il pretore di Torino,  con    &#13;
 ordinanza  emessa  il  24  maggio 1994 (reg. ord. n. 436 del 1994) ha    &#13;
 sollevato questioni di  legittimità  costituzionale:  (questione  a)    &#13;
 dell'art.  141  del  d.P.R. 15 giugno 1959, n. 393 (Testo unico delle    &#13;
 norme sulla circolazione stradale), come novellato dall'art. 22 della    &#13;
 legge 24 marzo 1989 n. 122, in riferimento agli artt. 3  e  97  della    &#13;
 Costituzione,  "nella  parte  in cui prevede il termine di giorni 150    &#13;
 per la notificazione delle  contestazioni  di  violazioni  al  codice    &#13;
 della  strada,  anziché  di  90  giorni,  come  avviene per le altre    &#13;
 sanzioni   amministrative",   così   determinando   un'irragionevole    &#13;
 disparità  di trattamento dei trasgressori, senza nemmeno assicurare    &#13;
 il buon andamento e l'imparzialità dell'amministrazione;  (questione    &#13;
 b)  del  combinato  disposto  degli  artt. 142 e 142-bis dello stesso    &#13;
 codice della strada del 1959, come novellati  dagli  artt.  23  e  24    &#13;
 della legge n. 122 del 1989 cit., in riferimento agli artt. 3, 97, 24    &#13;
 e  113 della Costituzione, perché solo coloro che abbiano presentato    &#13;
 ricorso al prefetto contro il verbale di accertamento dell'infrazione    &#13;
 potrebbero poi proporre opposizione all'ordinanza-ingiunzione in sede    &#13;
 giurisdizionale, mentre coloro che  abbiano  omesso  di  proporre  il    &#13;
 ricorso   amministrativo   si  vedrebbero  sottoposti  all'esecuzione    &#13;
 esattoriale a causa della qualità di titolo  esecutivo  che  in  tal    &#13;
 caso  assume il sommario processo verbale; ciò non senza considerare    &#13;
 che il sistema  sanzionatorio  per  le  infrazioni  al  codice  della    &#13;
 strada,  diversamente  da  quanto  previsto  per  le  altre  sanzioni    &#13;
 amministrative,  non  consentirebbe  al  prefetto,  in  mancanza   di    &#13;
 apposito  ricorso,  di controllare la regolarità delle contestazioni    &#13;
 degli organi accertatori e delle relative  notificazioni;  (questione    &#13;
 c)  sempre  del  combinato  disposto  di cui agli artt. 142 e 142-bis    &#13;
 citati, in riferimento agli artt. 24 e 113 della Costituzione, "nella    &#13;
 parte in cui non prevede, quando all'interessato sia stata notificata    &#13;
 la cartella esattoriale non preceduta da ordinanza-ingiunzione per la    &#13;
 mancata  presentazione   del   ricorso   al   prefetto   avverso   la    &#13;
 contestazione,  che  l'interessato  possa  proporre  il  giudizio  di    &#13;
 opposizione ex art. 22 della legge n. 689 del 1981 .. e  nella  parte    &#13;
 in  cui  non prevede il potere del pretore di sospendere l'esecuzione    &#13;
 ex art. 22, ultimo comma, della legge n. 689 del 1981";                  &#13;
      che  è  intervenuto  in  questo  giudizio  il  Presidente   del    &#13;
 Consiglio dei ministri, per il tramite dell'Avvocatura generale dello    &#13;
 Stato, concludendo per l'infondatezza delle prime due questioni e per    &#13;
 l'inammissibilità della terza;                                          &#13;
      che,  con  cinque  ordinanze  di  identico  tenore, emesse il 17    &#13;
 giugno  1994  nel  corso  di  altrettanti  giudizi  promossi  avverso    &#13;
 cartelle  esattoriali  concernenti  il  pagamento  di  somme dovute a    &#13;
 titolo di sanzioni amministrative  per  violazioni  al  codice  della    &#13;
 strada  del  1959, il pretore di Rimini (reg. ord. nn. 557, 608, 609,    &#13;
 610 e 611 del 1994) ha sollevato, in riferimento agli artt.  24  e  3    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 (questione  d) degli artt. 142, ultimo comma, e 142-bis del d.P.R. 15    &#13;
 giugno 1959, n. 392 (recte: 393), nel testo novellato dagli artt.  23    &#13;
 e  24  della  legge  24  marzo  1989, n. 122, vigente all'epoca della    &#13;
 commissione dell'illecito, sul  cui  presupposto  risulta  emessa  la    &#13;
 cartella esattoriale opposta;                                            &#13;
      che  nelle  ordinanze  si  sostiene  che  il  sistema  di cui al    &#13;
 (precedente) codice della strada, come integrato dalla legge  n.  122    &#13;
 del  1989  -  prevedendo  che, in caso di mancato pagamento in misura    &#13;
 ridotta o di omesso ricorso al  prefetto,  acquisti  la  qualità  di    &#13;
 titolo  esecutivo  il  verbale  di  accertamento e non più invece il    &#13;
 provvedimento del prefetto secondo la previsione dell'art.  18  della    &#13;
 legge n. 689 del 1981 - priverebbe "il trasgressore della facoltà di    &#13;
 proporre  opposizione  al  pretore  ex art. 22 della legge n. 689 del    &#13;
 1981"   e   non   contemplerebbe   il   rimedio   della   opposizione    &#13;
 all'esecuzione  (delle  cartelle  esattoriali  in  cui si concreta il    &#13;
 debito delle sanzioni) secondo l'art. 615  c.p.c.,  che  consente  al    &#13;
 debitore di contestare in via generale l' an e il quantum del debito;    &#13;
 ciò  in  quanto  l'art.  54 del d.P.R. n. 602 del 1973 espressamente    &#13;
 esclude quel rimedio nella fase della riscossione né le controversie    &#13;
 avverso il ruolo esattoriale possono proporsi, in  casi  del  genere,    &#13;
 dinanzi  al  giudice  tributario  a  causa  della  tassatività delle    &#13;
 materie indicate nell'art. 1 del d.P.R.  n.  636  del  1972,  tuttora    &#13;
 vigente;                                                                 &#13;
    che  pertanto  il sistema, in palese contrasto con l'art. 24 della    &#13;
 Costituzione,   non   prevederebbe   nessuna    forma    di    tutela    &#13;
 giurisdizionale  avverso  la  pretesa  di  pagamento  della  sanzione    &#13;
 amministrativa conseguente a violazione del codice della strada,  una    &#13;
 volta  divenuto  titolo  esecutivo  il  sommario  processo  verbale e    &#13;
 notificata la cartella di pagamento sul presupposto della  formazione    &#13;
 del ruolo dato in carico all'esattore;                                   &#13;
      che,   inoltre,   si  avrebbe  un'ingiustificata  disparità  di    &#13;
 trattamento dei trasgressori che non si siano avvalsi del ricorso  al    &#13;
 prefetto sia nei confronti di coloro che invece l'abbiano presentato,    &#13;
 sia  nei  confronti  dei  responsabili  di qualsiasi altra violazione    &#13;
 amministrativa;                                                          &#13;
      che in due di questi giudizi (reg. ord. nn. 597 e 608 del  1994)    &#13;
 vi  è  stato l'intervento del Presidente del Consiglio dei ministri,    &#13;
 che ha concluso per l'infondatezza di tutte le questioni,  in  quanto    &#13;
 analoghe  a  quella decisa con la sentenza di questa Corte n. 311 del    &#13;
 1994;                                                                    &#13;
     che, con quattro ordinanze identiche, emesse il 3 maggio 1994 nel    &#13;
 corso di altrettanti giudizi di opposizione  a  cartelle  esattoriali    &#13;
 per   la   riscossione   di   somme   dovute  a  titolo  di  sanzioni    &#13;
 amministrative per violazioni del codice della strada  del  1959,  il    &#13;
 pretore  di Pordenone (reg. ord. nn. 723, 724, 725 e 726 del 1994) ha    &#13;
 sollevato, in riferimento agli artt. 3, 24 e 111 (recte:  113)  della    &#13;
 Costituzione,  questione di legittimità costituzionale (questione e)    &#13;
 degli artt. 142-bis del d.P.R. 15 giugno 1959, n. 393 (come novellato    &#13;
 dall'art. 24  della  legge  n.  122  del  1989)  e  203  del  decreto    &#13;
 legislativo  30  aprile 1992, n. 285 (nuovo codice della strada) " ..    &#13;
 nella parte in cui ciascuna  di  tali  disposizioni  non  prevede  la    &#13;
 possibilità  di  proporre  avanti  al pretore (giudice deputato alla    &#13;
 valutazione della fondatezza dell'illecito amministrativo in caso  di    &#13;
 emissione  di  ordinanza-ingiunzione)  opposizione  avverso  il ruolo    &#13;
 esattoriale,  emesso  ai  sensi  dei predetti articoli sulla base del    &#13;
 sommario  verbale  di  accertamento  divenuto  titolo  esecutivo  per    &#13;
 mancata proposizione di ricorso al prefetto";                            &#13;
      che  nelle  ordinanze si osserva che la legge n. 122 del 1989 ha    &#13;
 modificato il procedimento sanzionatorio per le violazioni del codice    &#13;
 della strada, in precedenza disciplinato dalla legge n. 689 del 1981,    &#13;
 comune a tutte le sanzioni  amministrative,  e  che  la  modifica  ha    &#13;
 comportato  che  il ricorso al prefetto, avverso il sommario processo    &#13;
 verbale  di  contestazione  dell'infrazione,   costituisca   ora   la    &#13;
 necessaria     "condizione    di    procedibilità    della    tutela    &#13;
 giurisdizionale", che resta pertanto assicurata  solo  nei  confronti    &#13;
 dell'ordinanza-ingiunzione   che  il  prefetto  emana  all'esito  del    &#13;
 ricorso amministrativo, non essendo invece previsto  dal  sistema  di    &#13;
 poter  fare  opposizione  né  al  verbale  di accertamento una volta    &#13;
 divenuto titolo esecutivo, né al  ruolo  predisposto  dall'autorità    &#13;
 accertatrice né alla cartella esattoriale;                              &#13;
      che,   potendo  il  trasgressore  soltanto  "esperire  i  rimedi    &#13;
 previsti per la fase esecutiva  (ricorso  all'intendente  di  finanza    &#13;
 contro   gli  atti  esecutivi  dell'esattore,  anche  ai  fini  della    &#13;
 sospensione, con la conseguente possibilità di ricorrere al  giudice    &#13;
 amministrativo,  nonché azione giudiziaria di responsabilità contro    &#13;
 l'esattore)", in tal modo  si  sarebbe  in  presenza  di  una  tutela    &#13;
 giurisdizionale "resa eccessivamente difficoltosa", con disparità di    &#13;
 trattamento riguardo agli altri illeciti amministrativi non attinenti    &#13;
 a violazioni del codice della strada.                                    &#13;
    Considerato  che  le  ordinanze di rimessione sollevano, pur sotto    &#13;
 profili in parte diversi, questioni simili, tutte attinenti ad alcune    &#13;
 norme del codice della strada anteriore rispetto a quello attualmente    &#13;
 vigente, tranne una sola che riguarda  una  corrispondente  norma  di    &#13;
 quest'ultimo;  onde  i  relativi  giudizi  possono essere riuniti per    &#13;
 essere decisi con un'unica pronuncia;                                    &#13;
      che in particolare la questione sub a), concernente  l'art.  141    &#13;
 del  codice  della strada del 1959, come novellato dall'art. 22 della    &#13;
 legge n. 122 del 1989, e in modo specifico  la  denunciata  eccessiva    &#13;
 lunghezza  del  termine  di 150 giorni per la notifica del verbale di    &#13;
 accertamento delle infrazioni in tema di  circolazione  stradale,  è    &#13;
 manifestamente infondata, perché, essendo già stata esaminata negli    &#13;
 stessi  termini  ora prospettati, è stata dichiarata non fondata con    &#13;
 la sentenza n. 255 del 1994 di questa Corte;                             &#13;
      che, quanto alla questione sub  b),  che  censura  il  combinato    &#13;
 disposto  degli artt. 142 e 142-bis del codice della strada del 1959,    &#13;
 come novellati dagli artt. 23 e 24 della legge n. 122  del  1989,  in    &#13;
 riferimento  agli  artt. 3, 24, 97 e 113 della Costituzione, il primo    &#13;
 dei  profili  con  essa  prospettati  muove  dall'errato  presupposto    &#13;
 secondo cui il mancato preventivo esperimento del ricorso al prefetto    &#13;
 precluderebbe  l'azione  giudiziaria, il che è stato già escluso da    &#13;
 questa  Corte  (sent.  n.  255  del  1994),  onde  la  sua  manifesta    &#13;
 infondatezza;                                                            &#13;
      che  la  medesima  questione  sub b) è manifestamente infondata    &#13;
 anche sotto il profilo secondo cui soltanto il  ricorso  al  prefetto    &#13;
 provocherebbe "il dovere in capo a quell'Autorità .. di esaminare ..    &#13;
 l'iter  sanzionatorio  e  .. la regolarità delle contestazioni degli    &#13;
 organi  accertatori   ..,   laddove   tutte   le   altre   violazioni    &#13;
 amministrative   ..   provocano   sempre  il  potere-dovere  in  capo    &#13;
 all'Autorità  competente  (nel  caso,  il  prefetto) di esaminare la    &#13;
 regolarità  della  sanzione  e  procedere  in  sede  di   autotutela    &#13;
 dell'annullamento o archiviazione del verbale .."; difatti, come già    &#13;
 affermato  da questa Corte (sent. n. 311 del 1994), per le violazioni    &#13;
 del codice della strada "il  riesame  della  contestazione  da  parte    &#13;
 dell'autorità  prefettizia  non  è  stato  eliminato,  ma, anziché    &#13;
 avvenire d'ufficio, come era in precedenza in base alla legge n.  689    &#13;
 del  1981, consegue al ricorso dell'interessato avverso il verbale di    &#13;
 accertamento", consentendogli in questa sede di far  valere  comunque    &#13;
 le  sue  ragioni  e  di  ottenere  quindi  l'invocato riesame in sede    &#13;
 amministrativa,  per  cui  è  privo  di  riflessi  sul  piano  della    &#13;
 costituzionalità  che  in  alcuni  casi  la  legge  subordini questo    &#13;
 riesame  all'istanza  dell'interessato,  dato  che  la   Costituzione    &#13;
 assicura  soltanto  la  tutela  giurisdizionale,  peraltro  anch'essa    &#13;
 esperibile in via d'azione;                                              &#13;
      che  quanto  alle  questioni  sub  c,   d,   e,   (quest'ultima,    &#13;
 relativamente  al solo art. 142-bis del codice della strada del 1959,    &#13;
 come novellato dall'art. 24 della legge n. 122 del 1989), prospettate    &#13;
 sostanzialmente  sotto  il  profilo  della  carenza  di  un   rimedio    &#13;
 giurisdizionale avverso le cartelle esattoriali, va considerato che -    &#13;
 mancando  una  specifica disciplina circa i termini e le modalità da    &#13;
 osservarsi per l'esperimento dell'azione giudiziaria, nel caso in cui    &#13;
 dopo la notifica del verbale di contestazione  l'interessato  non  si    &#13;
 sia  avvalso  del  rimedio  del  ricorso  amministrativo  - una volta    &#13;
 escluso dalla già menzionata giurisprudenza di questa Corte  (sentt.    &#13;
 nn.  311  e  255  del 1994 cit.)   che in questo caso sia impedita la    &#13;
 tutela giurisdizionale, spetta al giudice dinanzi al  quale  l'azione    &#13;
 è  proposta  di  verificare,  alla  stregua  del diritto vigente, il    &#13;
 quomodo ed il quando della  sua  esperibilità  affinché  la  tutela    &#13;
 risulti  assicurata  nella  sua  pienezza,  con  la  conseguenza che,    &#13;
 formulando le ordinanze di rinvio quesiti d'ordine interpretativo, le    &#13;
 relative questioni sono manifestamente inammissibili;                    &#13;
      che quanto alla medesima questione sub e), con riguardo all'art.    &#13;
 203 del nuovo codice della strada (decreto  legislativo  n.  285  del    &#13;
 1992)   -   impugnato   insieme   alla   norma,   solo   parzialmente    &#13;
 corrispondente, dell'art. 142-bis del precedente codice della  strada    &#13;
 per  ragioni  meramente  cautelative,  nella incertezza, sottesa alla    &#13;
 prospettazione del giudice a quo, circa la disposizione  in  concreto    &#13;
 applicabile  nella  successione  tra il regime normativo precedente e    &#13;
 quello del nuovo codice  -  essa  è,  nella  specie,  manifestamente    &#13;
 inammissibile  perché  della norma, ratione temporis, il giudice non    &#13;
 deve fare applicazione nei giudizi a quibus (V. artt. 237, comma 2, e    &#13;
 238, comma 1, del decreto legislativo n. 285 del 1992).                  &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi, dichiara la manifesta inammissibilità:             &#13;
       a) delle questioni di legittimità costituzionale del combinato    &#13;
 disposto degli artt. 142 e 142-bis del d.P.R. 15 giugno 1959, n.  393    &#13;
 (Testo unico delle norme sulla circolazione stradale), come novellati    &#13;
 dagli  artt. 23 e 24 della legge 24 marzo 1989, n. 122, sollevate, in    &#13;
 riferimento agli artt. 24 e 113 della Costituzione,  dal  pretore  di    &#13;
 Torino con l'ordinanza indicata in epigrafe (reg. ord. 436 del 1994);    &#13;
       b)  delle  questioni di legittimità costituzionale degli artt.    &#13;
 142, ultimo comma, e 142-bis del d.P.R. 15 giugno 1959, n. 393,  come    &#13;
 novellati  dagli  artt.  23  e  24 della legge 24 marzo 1989, n. 122,    &#13;
 sollevate, in riferimento agli artt. 3 e 24 della  Costituzione,  dal    &#13;
 pretore  di  Rimini  con le ordinanze indicate in epigrafe (reg. ord.    &#13;
 nn. 557, 608, 609, 610 e 611 del 1994);                                  &#13;
       c) delle questioni  di  legittimità  costituzionale  dell'art.    &#13;
 142-bis  del  d.P.R. 15 giugno 1959, n. 393, come novellato dall'art.    &#13;
 24 della legge 24 marzo 1989, n. 122, sollevate, in riferimento  agli    &#13;
 artt. 3, 24 e 113 della Costituzione, dal pretore di Pordenone con le    &#13;
 ordinanze indicate in epigrafe (reg. ord. nn. 723, 724, 725 e 726 del    &#13;
 1994);                                                                   &#13;
       d) delle questioni di legittimità costituzionale dell'art. 203    &#13;
 del  decreto  legislativo  30 aprile 1992, n. 285 (Nuovo codice della    &#13;
 strada), sollevate, in riferimento agli  artt.  3,  24  e  113  della    &#13;
 Costituzione,  dal  pretore di Pordenone con le ordinanze indicate in    &#13;
 epigrafe (reg. ord. nn. 723, 724, 725 e 726 del 1994);                   &#13;
    Dichiara la manifesta infondatezza:                                   &#13;
       e) delle questioni di legittimità costituzionale dell'art. 141    &#13;
 del d.P.R. 15 giugno 1959, n. 393, come novellato dell'art. 22  della    &#13;
 legge 24 marzo 1989, n. 122, sollevate, in riferimento agli artt. 3 e    &#13;
 97 della Costituzione, dal pretore di Torino con l'ordinanza indicata    &#13;
 in epigrafe (reg. ord. n. 436 del 1994);                                 &#13;
       f) delle questioni di legittimità costituzionale del combinato    &#13;
 disposto  degli  artt.  142  e  142-bis del d.P.R. 15 giugno 1959, n.    &#13;
 393, come novellati dagli artt. 23 e 24 della legge 24 marzo 1989, n.    &#13;
 122, sollevate, in riferimento agli artt.  3,  24,  97  e  113  della    &#13;
 Costituzione,  dal  pretore  di  Torino  con  l'ordinanza indicata in    &#13;
 epigrafe (reg. ord. n. 436 del 1994).                                    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 28 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 12 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
