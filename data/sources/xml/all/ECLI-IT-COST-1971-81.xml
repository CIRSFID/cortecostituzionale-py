<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>81</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:81</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Michele Fragali</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/04/1971</data_decisione>
    <data_deposito>26/04/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO &#13;
 DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA &#13;
 REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio promosso con ricorso del Presidente del Consiglio  dei  &#13;
 ministri,  notificato il 28 dicembre 1970, depositato in cancelleria il  &#13;
 7 gennaio 1971 ed iscritto al n.  1  del  registro  ricorsi  1971,  per  &#13;
 conflitto  di attribuzione sorto per effetto del decreto 19 giugno 1970  &#13;
 del Presidente della Giunta regionale  del  Friuli-Venezia  Giulia  che  &#13;
 autorizzava  il  Comune  di  Udine all'occupazione d'urgenza di un'area  &#13;
 intestata al Demanio dello Stato - ANAS.                                 &#13;
     Visto l'atto di costituzione della Regione Friuli-Venezia Giulia;    &#13;
     udito  nell'udienza  pubblica  del  24  febbraio  1971  il  Giudice  &#13;
 relatore Michele Fragali;                                                &#13;
     uditi  il sostituto avvocato generale dello Stato Michele Savarese,  &#13;
 per il ricorrente, e l'avv. Gaspare Pacia, per la Regione.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Il 28 dicembre 1970 il Presidente del Consiglio  dei  ministri  &#13;
 ha proposto ricorso per conflitto di attribuzione avverso il decreto 19  &#13;
 giugno  1970  del  Presidente della Giunta regionale del Friuli-Venezia  &#13;
 Giulia, che autorizzava il Comune di Udine all'occupazione  di  urgenza  &#13;
 dell'area  sita  in  Udine  accatastata  al  foglio'  40  mappale  256,  &#13;
 intestata al Demanio dello  Stato,  Azienda  nazionale  autonoma  della  &#13;
 strada,  per  la  costruzione  da  parte  del  Comune  e con contributo  &#13;
 regionale, di un centro ambulatoriale ed igienico sanitario.             &#13;
     Il ricorso rileva che l'area predetta, con decreto  prefettizio  15  &#13;
 ottobre  1953,  n.  55840,  era  stata espropriata a favore del Demanio  &#13;
 dello Stato per  sede  di  strada  e  per  la  costruzione  dei  locali  &#13;
 dell'ANAS,  sezione  staccata  di Udine. Le opere non sono state ancora  &#13;
 eseguite  per  temporanea  indisponibilità  di  fondi,  e  il  decreto  &#13;
 regionale  si  presenta  come  ablatorio  della  destinazione  statuale  &#13;
 dell'area,  che  la  Regione  non  è  competente  a   modificare.   La  &#13;
 declassificazione   dei  beni  demaniali  deve  essere  dichiarata  dal  &#13;
 Ministero delle finanze, al quale spetta altresì  la  vigilanza  della  &#13;
 destinazione  dei  beni patrimoniali statali; e l'area di cui si tratta  &#13;
 non è  fra i beni patrimoniali attribuiti o trasferiti alla Regione in  &#13;
 base agli artt. 55 e 56 dello Statuto.                                   &#13;
     Si rileva inoltre che, pur essendo vero che la Regione ha  potestà  &#13;
 legislativa  in  materia  di  espropriazione  per pubblica utilità non  &#13;
 riguardante opere a carico dello  Stato,  questo  potere  non  potrebbe  &#13;
 essere esercitato riguardo ad un bene espropriato per la costruzione di  &#13;
 un'opera  pubblica  da parte dello Stato; il decreto regionale viene ad  &#13;
 arrestare l'iter procedimentale e  sostanziale  di  una  espropriazione  &#13;
 dello  Stato,  che  è  competente  in via esclusiva per le opere a suo  &#13;
 carico, da eseguire nella Regione. Questa  competenza  sarebbe  cessata  &#13;
 solo  se  si  fosse disposta la retrocessione del bene a colui al quale  &#13;
 era stato espropriato; il decreto regionale, fondato sul diritto a tale  &#13;
 retrocessione il  cui  accertamento  è  di  competenza  dell'autorità  &#13;
 giudiziaria, viola anche la riserva della competenza statale in materia  &#13;
 di giurisdizione.                                                        &#13;
     2.  -  Il Presidente della Regione eccepisce l'inammissibilità del  &#13;
 ricorso perché ritiene che le censure mosse al provvedimento regionale  &#13;
 riguardano la legittimità dello stesso, e non la spettanza del  potere  &#13;
 esercitato  dalla  Regione, alla quale competono, ai sensi dell'art. 30  &#13;
 d.P.R. 26 agosto 1965, n. 1116,  tutte  le  attribuzioni  degli  organi  &#13;
 statali  in  materia  di  espropriazione  per  pubblico  interesse e di  &#13;
 occupazione temporanea e d'urgenza riguardo alle  opere  non  a  carico  &#13;
 dello  Stato:  nella  specie,  il decreto impugnato riguardava un'opera  &#13;
 sanitaria deliberata dal Comune di Udine e a carico dello  stesso.  Non  &#13;
 è esatto, secondo il Presidente della Regione, che il decreto in esame  &#13;
 ha  dismesso  l'area  dal  demanio o dal patrimonio indisponibile dello  &#13;
 Stato: la tipicità dell'atto autorizzativo dell'occupazione di urgenza  &#13;
 esclude che questo possa confondersi con un atto di  dismissione  della  &#13;
 demanialità. La Regione può espropriare beni dello Stato senza che ne  &#13;
 derivi usurpazione dei poteri statali.                                   &#13;
     Nel  merito  il Presidente della Regione esclude che il bene di cui  &#13;
 ha ordinato l'occupazione faccia parte del patrimonio' indisponibile  o  &#13;
 del demanio statale.  Era destinato all'esecuzione di un'opera pubblica  &#13;
 di  interesse  statale; ma è giurisprudenza che, fino a quando l'opera  &#13;
 non sia ultimata e adibita all'uso, il bene espropriato  appartiene  al  &#13;
 patrimonio  disponibile.  Non  conta  che  l'area  non sia stata ancora  &#13;
 trasferita alla Regione: da una omissione non può derivare al bene una  &#13;
 qualificazione di contrasto con la realtà, e, del resto, il d.P.R.  31  &#13;
 ottobre 1967, n.  1401, che contiene norme di attuazione statutaria per  &#13;
 il   trasferimento   alla   Regione   di   beni  immobili  patrimoniali  &#13;
 disponibili, comprende, anche  per  sua  espressa  disposizione,  tutti  &#13;
 quegli   altri   beni   immobili   situati  nel  territorio  regionale,  &#13;
 l'appartenenza dei quali al patrimonio  disponibile  dello  Stato,  con  &#13;
 riferimento  alla  data  del  16  febbraio  1963,  venga  in  prosieguo  &#13;
 accertata  con  provvedimento  giurisdizionale  o   con   provvedimento  &#13;
 dell'autorità  amministrativa a norma dell'art. 829 del codice civile.  &#13;
 Non si  vede  poi  come  il  provvedimento  impugnato  possa  incidere,  &#13;
 arrestandone  l'iter,  su  una  espropriazione statale: il procedimento  &#13;
 iniziato dallo Stato  si  concluse  con  l'emanazione  del  decreto  di  &#13;
 espropriazione  e  solo è vero, per un verso, che l'opera non è stata  &#13;
 eseguita entro il termine indicato  nel  decreto  di  approvazione  del  &#13;
 prefetto  e,  per  altro  verso, che il proprietario espropriato non ha  &#13;
 esercitato entro  il  termine  di  prescrizione  il  suo  diritto  alla  &#13;
 retrocessione.                                                           &#13;
     3.  -  Il  28 gennaio 1971 il Presidente del Consiglio dei ministri  &#13;
 presento istanza di sospensione del provvedimento  regionale;  ma  tale  &#13;
 sospensione  fu  disposta  dalla  Regione  di  propria  autorità, e il  &#13;
 procedimento incidentale non ebbe più corso.                            &#13;
     Nel giudizio principale entrambe le parti hanno presentato memorie,  &#13;
 nelle quali ciascuna ha ribadito ed illustrato i propri punti di  vista  &#13;
 ed ha ritenuto l'infondatezza delle prospettazioni della controparte.    &#13;
     4.  -  All'udienza  pubblica del 24 febbraio 1971 i difensori hanno  &#13;
 confermato le rispettive tesi e conclusioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Si è fatta questione fra le  parti  circa  il  contenuto  del  &#13;
 ricorso  proposto  dalla Presidenza del Consiglio: se cioè esso deduca  &#13;
 invasione di una sfera di competenza statale  o  vizi  di  legittimità  &#13;
 dell'atto amministrativo impugnato.                                      &#13;
     La Corte ritiene che, a prescindere da tale indagine, è  la stessa  &#13;
 natura  del  bene oggetto dell'atto predetto che impedisce di scorgere,  &#13;
 nella specie, gli estremi di un conflitto di attribuzione.               &#13;
     2. - La Regione  aveva  autorizzato  l'occupazione  di  urgenza  di  &#13;
 un'area  che,  nel  1953, il prefetto di Udine aveva espropriato per la  &#13;
 costruzione di un edificio da  destinare  ad  uffici  dell'ANAS  e  per  &#13;
 l'apprestamento   di   una   strada   di   accesso   a  tale  edificio.  &#13;
 L'espropriazione  era  rimasta  senza  seguito;  e   pertanto,   quando  &#13;
 intervenne  il provvedimento regionale che autorizza il Comune di Udine  &#13;
 ad occupare l'area predetta,  la  destinazione  demaniale  non  si  era  &#13;
 realizzata.  L'area  doveva  dunque  ritenersi  di  patrimonio  statale  &#13;
 disponibile,  perché  com'è   noto,   i   beni   immobili   destinati  &#13;
 dall'amministrazione  all'esecuzione  di  un'opera  pubblica, solo dopo  &#13;
 l'ultimazione dell'opera acquistano un carattere, a seconda  dei  casi,  &#13;
 demaniale o indisponibile. Né toglie la qualifica di disponibilità il  &#13;
 fatto che il d.P.R. 31 ottobre 1967, n. 1401, non ha compreso l'area di  &#13;
 cui  si tratta fra i beni immobili che, avendo quella qualità, in base  &#13;
 allo Statuto regionale, dovevano trasferirsi alla Regione; tale decreto  &#13;
 contiene una riserva per quegli  altri  beni  la  cui  appartenenza  al  &#13;
 patrimonio  disponibile  dello Stato si fosse accertata successivamente  &#13;
 con riferimento al 16 febbraio 1963.                                     &#13;
     L'amministrazione statale non poteva dunque esplicare, sull'area di  &#13;
 cui si tratta,  altro  che  poteri  iure  privatorum,  perché  i  beni  &#13;
 disponibili  che  le appartengono, pur essendo soggetti ad un regime di  &#13;
 gestione  particolare  ai  fini  della  loro  utilizzazione  e  ad   un  &#13;
 particolare   regime   formale   quanto   alla   loro  destinazione  ed  &#13;
 alienazione, per ogni altro aspetto non sfuggono all'imperio del codice  &#13;
 civile, soprattutto circa la natura del rapporto fra  l'amministrazione  &#13;
 e  i beni, che è rapporto di proprietà. Non v'è perciò, riguardo ad  &#13;
 essi, esercizio di  potestà  pubbliche,  le  sole  implicabili  in  un  &#13;
 conflitto  di  attribuzione; che coinvolge infatti poteri dello Stato o  &#13;
 poteri delle regioni inerenti a sfere  di  competenza  assegnate  dalla  &#13;
 Costituzione (sentenze 19 gennaio 1957 n. 17 e 17 giugno 1970, n. 110).  &#13;
 Non rientra nella competenza costituzionale dello Stato o delle regioni  &#13;
 né  l'esercizio dei diritti dominicali su un bene appartenente al loro  &#13;
 patrimonio disponibile né la vigilanza che gli organi  dello  Stato  o  &#13;
 delle  regioni  debbono esercitare per evitare che siano adibiti ad uso  &#13;
 pubblico beni eccedenti al bisogno, come è prescritto nell'art. 18 del  &#13;
 r.d. 23 maggio 1924, n. 827.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile  il  ricorso per conflitto di attribuzione,  &#13;
 proposto dal Presidente del Consiglio dei ministri il 28 dicembre 1970,  &#13;
 avverso il decreto 19 giugno 1970 del Presidente della Giunta regionale  &#13;
 del  Friuli-Venezia  Giulia,  che  autorizzava  il  Comune   di   Udine  &#13;
 all'occupazione d'urgenza dell'area in detto decreto descritta.          &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 21 aprile 1971.                               &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI - FRANCESCO PAOLO BONIFACIO  &#13;
                                   - LUIGI OGGIONI - ANGELO DE  MARCO  -  &#13;
                                   ERCOLE  ROCCHETTI  - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
