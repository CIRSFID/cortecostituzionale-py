<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>321</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:321</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/06/1993</data_decisione>
    <data_deposito>21/07/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Giuseppe BORZELLINO, prof. Gabriele PESCATORE, avv. &#13;
 Ugo SPAGNOLI, prof. Antonio BALDASSARRE, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi di legittimità costituzionale degli artt. 12 della legge    &#13;
 8  marzo  1968,  n.  152 (Nuove norme in materia previdenziale per il    &#13;
 personale degli enti locali) e 24 della legge 22  novembre  1962,  n.    &#13;
 1646  (Modifiche agli ordinamenti degli Istituti di previdenza presso    &#13;
 il Ministero  del  tesoro),  promossi  con  ordinanze  emesse  il  12    &#13;
 novembre  1992 dal Pretore di Genova, il 16 marzo 1992 dal Pretore di    &#13;
 Vallo della Lucania ed il 3 novembre 1992  dal  Tribunale  di  Reggio    &#13;
 Emilia,  rispettivamente  iscritte  ai  nn.  76, 79 e 99 del registro    &#13;
 ordinanze  1993  e  pubblicate   nelle   Gazzette   Ufficiali   della    &#13;
 Repubblica, nn. 9, 10 e 11, prima serie speciale, dell'anno 1993;        &#13;
    Visto l'atto di costituzione di Gambino Laura ed altre;               &#13;
    Udito nell'udienza pubblica del 25 maggio 1993 il Giudice relatore    &#13;
 Fernando Santosuosso;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Nel corso del procedimento civile vertente tra Gambino Laura    &#13;
 ed  altre  e  INADEL  in  cui   le   ricorrenti,   tutte   dipendenti    &#13;
 dall'Istituto  Gianna  Gaslini  in  Genova in qualità di vigilatrici    &#13;
 d'infanzia, hanno chiesto che venisse loro riconosciuto il diritto al    &#13;
 riscatto ai fini dell'indennità premio di servizio  del  periodo  di    &#13;
 studi  seguito  presso  la  scuola  "Lorenza Gaslini" per vigilatrici    &#13;
 d'infanzia, il Pretore di Genova, con ordinanza del 12 novembre  1992    &#13;
 ha  sollevato,  in  riferimento agli artt. 3 e 97 della Costituzione,    &#13;
 questione di legittimità costituzionale dell'art. 12 della  legge  8    &#13;
 marzo  1968,  n.  152,  nella parte in cui non prevede la facoltà di    &#13;
 riscattare i periodi corrispondenti alla durata legale dei  corsi  di    &#13;
 studio  per  vigilatrici  d'infanzia,  nel caso in cui il diploma sia    &#13;
 richiesto dalla legge come condizione necessaria per l'ammissione  in    &#13;
 carriera.                                                                &#13;
    Rileva  il  giudice  a  quo che la disposizione appare logicamente    &#13;
 incoerente ed irrazionale in quanto, essendo già stato  riconosciuto    &#13;
 il  diritto al riscatto dei suddetti corsi ai fini pensionistici, non    &#13;
 vi è ragione perché non lo sia anche ai fini dell'indennità premio    &#13;
 di servizio stante l'unitarietà del trattamento pensionistico.          &#13;
    Per quanto riguarda la posizione di  due  tra  le  ricorrenti,  il    &#13;
 giudice  a quo solleva questione di legittimità costituzionale anche    &#13;
 dell'art. 24 della legge 22 novembre 1962, n. 1646,  nella  parte  in    &#13;
 cui  consente  la riscattabilità del biennio corrispondente al corso    &#13;
 di studio presso la  scuola  convitto  anziché  dell'intero  periodo    &#13;
 corrispondente alla durata legale del corso, portato a tre anni dalla    &#13;
 legge 30 aprile 1976, n. 338.                                            &#13;
    Con  atto  depositato  il 5 marzo 1993 sono intervenute le signore    &#13;
 Gambino, Zonino,  Perazzo  e  Urbano  rappresentate  e  difese  dagli    &#13;
 avvocati  Carlo  Raggio  e Ciro Intino chiedendo l'accoglimento delle    &#13;
 questioni sollevate dal giudice a quo. Al riguardo le parti  rilevano    &#13;
 che l'illogicità del sistema di cui alla normativa impugnata sarebbe    &#13;
 già  stata acclarata dalle sentenze n. 765 del 1988 e n. 26 del 1992    &#13;
 di questa Corte.                                                         &#13;
    Invero, con la sentenza  n.  765  del  1988  è  stato  esteso  il    &#13;
 riscatto  del  corso di studi ai fini della pensione, originariamente    &#13;
 previsto per i soli infermieri professionali, anche alle  vigilatrici    &#13;
 d'infanzia  stante  la sostanziale identità ed equivalenza delle due    &#13;
 professioni.                                                             &#13;
    Con sentenza n. 26 del 1992, è stata dichiarata  l'illegittimità    &#13;
 costituzionale  dell'art.  12  della  legge  8 marzo 1968, n. 152, in    &#13;
 riferimento alla medesima questione ora  sollevata  ma  relativamente    &#13;
 alla categoria professionale degli infermieri.                           &#13;
    La  mancata  concessione  del  richiesto riconoscimento anche alla    &#13;
 categoria  delle  vigilatrici  d'infanzia   secondo   le   ricorrenti    &#13;
 contrasterebbe  quindi  sia  con  il  principio di eguaglianza di cui    &#13;
 all'art.  3  della  Costituzione,  sia  con  il  principio  del  buon    &#13;
 andamento e della imparzialità della Pubblica Amministrazione di cui    &#13;
 all'art. 97 della Costituzione.                                          &#13;
    2.  -  Con  ordinanza  emessa  in data 16 marzo 1992 il Pretore di    &#13;
 Vallo della Lucania, nel corso del procedimento civile  vertente  tra    &#13;
 Carbone  Ermelinda ed INADEL, ha sollevato, in riferimento all'art. 3    &#13;
 della  Costituzione,   questione   di   legittimità   costituzionale    &#13;
 dell'art. 12 della legge 8 marzo 1968, n. 152, nella parte in cui non    &#13;
 consente  il  riscatto ai fini dell'indennità premio di servizio del    &#13;
 periodo corrispondente al corso legale di studio per il conseguimento    &#13;
 del diploma di vigilatrice d'infanzia, richiesto  dalla  legge  quale    &#13;
 condizione necessaria per l'ammissione in carriera.                      &#13;
    Rileva  il  giudice  a  quo  che  la questione appare rilevante in    &#13;
 quanto la norma denunciata risulta  applicabile  al  caso  di  specie    &#13;
 anche successivamente all'entrata in vigore dell'art. 8 della legge 8    &#13;
 agosto 1991, n. 274.                                                     &#13;
    Sul  punto  della non manifesta infondatezza rileva il Pretore che    &#13;
 l'esclusione ai fini del riscatto per l'indennità premio di servizio    &#13;
 di periodi di studi necessari per ricoprire un posto di lavoro,  già    &#13;
 riconosciuti come validi ai fini della quiescenza, contrasterebbe con    &#13;
 l'art.  3  della  Costituzione  in  quanto  crea una ingiustificabile    &#13;
 disparità tra dipendenti in relazione al  tipo  di  corsi  di  studi    &#13;
 espletato, non sorretta da alcuna logica motivazione.                    &#13;
    3. - Con ordinanza emessa in data 3 novembre 1992 nel procedimento    &#13;
 civile vertente tra Ossnoser Sandra ed INADEL, il Tribunale di Reggio    &#13;
 Emilia  ha  sollevato,  in  riferimento  agli  artt.  3  e  97  della    &#13;
 Costituzione, questione di legittimità costituzionale  dell'art.  12    &#13;
 della  legge  8  marzo  1968,  n. 152, in relazione all'art. 24 della    &#13;
 legge 22 novembre 1962, n. 1646, nella parte in cui non  consente  il    &#13;
 riscatto,  ai  fini  dell'indennità  premio di servizio, del periodo    &#13;
 corrispondente al corso legale di studi per il conseguimento del  diploma di vigilatrice d'infanzia richiesto dalla legge come condizione    &#13;
 necessaria per la relativa ammissione in carriera.                       &#13;
    Nel  motivare  sul  punto  della  non  manifesta  infondatezza  il    &#13;
 tribunale adduce considerazioni analoghe a quelle sopra  riferite  in    &#13;
 relazione  alle  ordinanze  dei  Pretori  di  Genova e di Vallo della    &#13;
 Lucania.                                                                 &#13;
    In prossimità dell'udienza  hanno  presentato  memoria  le  parti    &#13;
 costituite ribadendo le argomentazioni già esposte in precedenza.<diritto>Considerato in diritto</diritto>1.  -  Dai  Pretori  di  Genova  e  di  Vallo  della Lucania e dal    &#13;
 Tribunale  di  Reggio  Emilia  è  stata   sollevata   questione   di    &#13;
 legittimità costituzionale dell'art. 12 della legge 8 marzo 1968, n.    &#13;
 152 (Nuove norme in materia previdenziale per il personale degli enti    &#13;
 locali)  nella  parte  in  cui  esclude  la  riscattabilità  ai fini    &#13;
 dell'indennità premio di  servizio  dei  periodi  corrispondenti  al    &#13;
 corso   legale   di  studio  per  il  conseguimento  del  diploma  di    &#13;
 vigilatrice  d'infanzia  richiesto  dalla   legge   come   condizione    &#13;
 necessaria  per l'ammissione in carriera, in riferimento agli artt. 3    &#13;
 e 97 della Costituzione (l'art. 97 è  stato  invocato  soltanto  dal    &#13;
 Pretore di Vallo della Lucania).                                         &#13;
    Dal  Pretore  di  Genova  è stata sollevata altresì questione di    &#13;
 legittimità  costituzionale  anche  dell'art.  24  della  legge   22    &#13;
 novembre   1962,   n.   1646,   nella   parte   in  cui  consente  la    &#13;
 riscattabilità del biennio corrispondente al corso di studio  presso    &#13;
 la  scuola  convitto anziché dell'intero periodo corrispondente alla    &#13;
 durata legale del corso, portato a tre anni  dalla  legge  30  aprile    &#13;
 1976, n. 338.                                                            &#13;
    Considerata  l'analogia delle questioni esse possono essere decise    &#13;
 con unica sentenza, previa unificazione dei giudizi.                     &#13;
    2. - La prima questione è fondata.                                   &#13;
    Come questa Corte ha già rilevato (sentenza n. 765 del  1988)  la    &#13;
 disciplina dettata dal legislatore per conseguire il titolo di studio    &#13;
 necessario   per   l'esercizio   della   professione  di  vigilatrice    &#13;
 d'infanzia risulta identica a quella prevista per  l'esercizio  della    &#13;
 professione  sanitaria  ausiliaria di infermiere professionale. Se ne    &#13;
 deduce l'appartenenza delle due professioni ad un medesimo, specifico    &#13;
 campo dell'attività sanitaria.                                          &#13;
    Rilevanti sono infatti le affinità delle materie, dei programmi e    &#13;
 dei requisiti necessari per accedere ai rispettivi corsi, dal momento    &#13;
 che viene richiesta una  preparazione  sanitaria  teorico-pratica  di    &#13;
 uguale livello.                                                          &#13;
    Premesse   tali   considerazioni,  questa  Corte  ha  riconosciuto    &#13;
 l'illegittimità costituzionale della disciplina che escludeva per le    &#13;
 vigilatrici  di  infanzia  la  possibilità  di  riscattare  ai  fini    &#13;
 pensionistici  il  periodo  corrispondente  al corso di studio per il    &#13;
 conseguimento del relativo diploma,  possibilità  già  riconosciuta    &#13;
 agli infermieri professionali.                                           &#13;
    Viene ora sollevata questione di legittimità costituzionale della    &#13;
 disposizione  (art.  12  della  legge  8  marzo 1968, n. 152) che non    &#13;
 consente il riscatto del suddetto periodo ai fini della  liquidazione    &#13;
 dell'indennità premio di servizio.                                      &#13;
    In  altra  decisione  (sentenza  n.  26  del 1992) questa Corte ha    &#13;
 affermato che l'indennità premio di servizio "è  da  porre  accanto    &#13;
 alla  pensione  nell'ambito  dell'intero trattamento di quiescenza, e    &#13;
 pertanto unitaria va riconosciuta la tendenzialità a concedere  alla    &#13;
 preparazione  professionale  acquisita ogni migliore considerazione".    &#13;
 In  quell'occasione   è   stata   di   conseguenza   dichiarata   la    &#13;
 illegittimità   costituzionale  della  disposizione  ora  nuovamente    &#13;
 impugnata, nella parte in cui escludeva (relativamente alla categoria    &#13;
 degli infermieri professionali)  la  riscattabilità  ai  fini  della    &#13;
 liquidazione   della   indennità  premio  di  servizio  del  periodo    &#13;
 corrispondente  al  corso  legale  di   studi   necessario   per   il    &#13;
 conseguimento   del  diploma,  allorché  detta  riscattabilità  era    &#13;
 viceversa riconosciuta ai fini del trattamento di quiescenza.            &#13;
    Coerentemente con tali presupposti, risulta irrazionale, ex art. 3    &#13;
 della Costituzione, la disposizione impugnata in  quanto  esclude  la    &#13;
 riscattabilità  ai  fini  dell'indennità  premio  di  servizio  del    &#13;
 periodo corrispondente al corso legale di  studi  necessario  per  il    &#13;
 conseguimento  del  diploma  di  vigilatrice  di infanzia, sempre che    &#13;
 ovviamente tale diploma risulti indispensabile  per  l'ammissione  in    &#13;
 carriera.                                                                &#13;
    3.   -   Quanto  alla  questione  di  legittimità  costituzionale    &#13;
 dell'art. 24 della legge 22 novembre 1962, n. 1646,  deve  precisarsi    &#13;
 che  la  prospettazione  corretta  della  questione  va riferita, per    &#13;
 essere rilevante nel giudizio a quo, al  combinato  disposto  di  cui    &#13;
 agli  artt.  12  della legge 8 marzo 1968, n. 152 e 24 della legge 22    &#13;
 novembre 1962, n. 1646, nella parte in cui prevedono, ai  fini  della    &#13;
 liquidazione  dell'indennità  premio di servizio, la riscattabilità    &#13;
 del biennio  corrispondente  al  corso  di  studi  presso  la  scuola    &#13;
 convitto  anziché dell'intero periodo corrispondente al corso legale    &#13;
 di studi necessario per il conseguimento del relativo diploma.           &#13;
    La disposizione contenuta nell'art. 12  della  legge  n.  152  del    &#13;
 1968,  infatti,  opera un rinvio - relativamente alla riscattabilità    &#13;
 ai fini dell'indennità premio di servizio dei periodi  di  studio  -    &#13;
 alla  disciplina riguardante i trattamenti di quiescenza: è pertanto    &#13;
 il combinato disposto delle due norme che deve  trovare  applicazione    &#13;
 da  parte  del  giudice  a  quo  e  ad  esso deve intendersi riferita    &#13;
 pertanto la questione di costituzionalità sottoposta a questa Corte.    &#13;
    Riguardo a tale combinato  disposto,  uno  dei  giudici  a  quibus    &#13;
 rileva  che,  essendo in materia intervenuta la legge 30 aprile 1976,    &#13;
 n. 338, che ha elevato da due a tre anni la durata  obbligatoria  del    &#13;
 corso  di  studi  necessario  per  il  conseguimento  del  diploma di    &#13;
 vigilatrice d'infanzia, esso risulterebbe in contrasto con gli  artt.    &#13;
 3 e 97 della Costituzione.                                               &#13;
    La questione deve essere accolta.                                     &#13;
    In  effetti  risulta  irrazionale, anche perché conseguenza di un    &#13;
 mancato coordinamento legislativo,  oltre  che  contrastante  con  il    &#13;
 principio  della  piena riscattabilità ai fini previdenziali di ogni    &#13;
 tipo di studio  e  di  preparazione  professionale  già  più  volte    &#13;
 affermato  da questa Corte (vedi sentenze n. 275, 209 e 178 del 1993,    &#13;
 27 e 26 del 1992, 280 e 133 del 1991, 535 e 426  del  1990,  163  del    &#13;
 1989  ecc.),  la  previsione  in  oggetto,  che  limita a due anni il    &#13;
 periodo riscattabile  ai  fini  dell'indennità  premio  di  servizio    &#13;
 allorché la durata del corso legale di studi sia diversa per legge.     &#13;
    Va   pertanto   dichiarata   l'illegittimità  costituzionale  del    &#13;
 combinato disposto di cui agli artt. 12 della legge 8 marzo 1968,  n.    &#13;
 152  e  24  della legge 22 novembre 1962, n. 1646, nella parte in cui    &#13;
 prevedono  la  riscattabilità  ai  fini  dell'indennità  premio  di    &#13;
 servizio  del  biennio  corrispondente  al  corso  di studi presso la    &#13;
 scuola convitto anziché dell'intero periodo corrispondente al  corso    &#13;
 legale  di  studi  necessario  per  il  conseguimento  del diploma di    &#13;
 vigilatrice d'infanzia.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 Riuniti i giudizi,                                                       &#13;
    Dichiara l'illegittimità costituzionale dell'art. 12 della  legge    &#13;
 8  marzo  1968,  n.  152 (Nuove norme in materia previdenziale per il    &#13;
 personale degli enti locali) nella  parte  in  cui  non  consente  la    &#13;
 facoltà  di  riscattare,  ai fini della liquidazione dell'indennità    &#13;
 premio di servizio, i periodi corrispondenti alla durata  legale  del    &#13;
 corso  di  studi  per  il conseguimento del diploma di vigilatrice di    &#13;
 infanzia;                                                                &#13;
    Dichiara l'illegittimità costituzionale del combinato disposto di    &#13;
 cui agli artt. 12 della legge 8 marzo 1968, n. 152 e 24  della  legge    &#13;
 22   novembre  1962,  n.  1646,  nella  parte  in  cui  prevedono  la    &#13;
 riscattabilità  ai  fini  dell'indennità  premio  di  servizio  del    &#13;
 biennio  corrispondente  al  corso di studi presso la scuola convitto    &#13;
 anziché dell'intero periodo corrispondente al corso legale di  studi    &#13;
 necessario   per   il   conseguimento   del  diploma  di  vigilatrice    &#13;
 d'infanzia.                                                              &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'11 giugno 1993.                                &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: SANTOSUOSSO                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 21 luglio 1993.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
