<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>279</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:279</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/05/1993</data_decisione>
    <data_deposito>10/06/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art. 4, comma 4,    &#13;
 della legge 30 dicembre 1991, n.  412  (Disposizioni  in  materia  di    &#13;
 finanza pubblica), ordinanze emesse il 13 ottobre 1992 dal Pretore di    &#13;
 Vicenza  -  Sezione  distaccata  di  Thiene  (n.  3  ordinanze), il 9    &#13;
 novembre 1992 dal Pretore di Verona ed il 23 ottobre 1992 dal Pretore    &#13;
 di Treviso, rispettivamente iscritte ai nn. 780, 781, 782 e  807  del    &#13;
 registro  ordinanze  1992  e  al  n.  7 del registro ordinanze 1993 e    &#13;
 pubblicate sulla Gazzetta Ufficiale della Repubblica n. 53  dell'anno    &#13;
 1992 e n. 3 dell'anno 1993;                                              &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  del  24  marzo  1993  il  Giudice    &#13;
 relatore Renato Granata;                                                 &#13;
    Ritenuto  che  nel  corso  tre distinti giudizi civili promossi da    &#13;
 alcuni titolari di farmacie convenzionate contro la U.S.L.  n.  6  di    &#13;
 Thiene  il  pretore  di  Vicenza  ha  sollevato, con ordinanze del 13    &#13;
 ottobre 1992, questione incidentale di legittimità costituzionale  -    &#13;
 in  riferimento agli artt. 3 e 53 Cost. - dell'art. 4, comma 4, della    &#13;
 legge 30 dicembre 1991 n. 412 nella  parte  in  cui  dispone  che  il    &#13;
 Servizio sanitario nazionale, nel provvedere alla corresponsione alle    &#13;
 farmacie di quanto dovuto per la vendita delle specialità medicinali    &#13;
 (in  regime  di assistenza diretta), trattiene una quota pari al 2,5%    &#13;
 dell'importo al lordo dei tickets;                                       &#13;
      che  secondo  il  giudice  rimettente  la  norma  si porrebbe in    &#13;
 contrasto sia con il principio di eguaglianza (perché introduce  una    &#13;
 prestazione  patrimoniale  obbligatoria  di  carattere  tributario  a    &#13;
 carico di una limitata categoria di cittadini), sia con il  principio    &#13;
 secondo  cui  ogni  cittadino  è  tenuto  a  concorrere  alle  spese    &#13;
 pubbliche in ragione della propria  capacità  contributiva;  inoltre    &#13;
 non  sarebbe  rispettosa del criterio di progressività al quale deve    &#13;
 essere uniformato il sistema tributario (art. 53 Cost.);                 &#13;
      che con ordinanze del 9 novembre 1992 e del 23 ottobre  1992  il    &#13;
 Pretore  di  Verona  ed  il  Pretore  di  Treviso  hanno sollevato la    &#13;
 medesima  questione  incidentale   di   costituzionalità   svolgendo    &#13;
 analoghe argomentazioni;                                                 &#13;
      che  in  tutti  i  giudizi  è  intervenuto  il  Presidente  del    &#13;
 Consiglio  dei  Ministri  rappresentato  e   difeso   dall'Avvocatura    &#13;
 Generale  dello  Stato  chiedendo  che  le  questioni sollevate siano    &#13;
 dichiarate inammissibili o comunque non fondate;                         &#13;
    Considerato che i giudizi possono essere riuniti per identità  di    &#13;
 contenuto;                                                               &#13;
      che   analoga  questione  di  costituzionalità  è  già  stata    &#13;
 ritenuta non fondata da questa Corte con sentenza n. 102 del 1993;       &#13;
      che  nessuna  nuova  argomentazione  viene  addotta,  né  alcun    &#13;
 diverso profilo prospettato;                                             &#13;
      che pertanto le questioni sono manifestamente infondate;            &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953 n. 87    &#13;
 e  9,  secondo  comma,  delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la  manifesta  infondatezza   delle    &#13;
 questioni  di legittimità costituzionale dell'art. 4, comma 4, della    &#13;
 legge 30 dicembre 1991 n. 412 (Disposizioni  in  materia  di  finanza    &#13;
 pubblica)   sollevate,  in  riferimento  agli  artt.  3  e  53  della    &#13;
 Costituzione, dal Pretore di Vicenza, sez. distaccata di Thiene,  dal    &#13;
 Pretore  di Verona e dal Pretore di Treviso con le ordinanze indicate    &#13;
 in epigrafe.                                                             &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 28 maggio 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: GRANATA                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 giugno 1993.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
