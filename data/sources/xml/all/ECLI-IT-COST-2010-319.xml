<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2010</anno_pronuncia>
    <numero_pronuncia>319</numero_pronuncia>
    <ecli>ECLI:IT:COST:2010:319</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>DE SIERVO</presidente>
    <relatore_pronuncia>Giuseppe Frigo</relatore_pronuncia>
    <redattore_pronuncia>Giuseppe Frigo</redattore_pronuncia>
    <data_decisione>03/11/2010</data_decisione>
    <data_deposito>11/11/2010</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 10-ter del decreto legislativo 10 marzo 2000 n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205) aggiunto dall'art. 35, comma 7 del decreto-legge 4 luglio 2006, n. 223 (Disposizioni urgenti per il rilancio economico e sociale, per il contenimento e la razionalizzazione della spesa pubblica, nonché interventi in materia di entrate e di contrasto all'evasione fiscale), convertito, con modificazioni, dalla legge 4 agosto 2006, n. 248, promosso dal Tribunale di Orvieto nel procedimento penale a carico di L. N. con ordinanza del 16 maggio 2008, iscritta al n. 81 del registro ordinanze 2010 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 12, prima serie speciale, dell'anno 2010.&#13;
 	Visto l'atto di intervento del Presidente del Consiglio dei ministri;&#13;
 	udito nella camera di consiglio del 20 ottobre 2010 il Giudice relatore Giuseppe Frigo.</epigrafe>
    <testo>Ritenuto che, con ordinanza del 16 maggio 2008, trasmessa alla Corte il 25 gennaio 2010, il Tribunale di Orvieto, in composizione monocratica, ha sollevato, in riferimento agli artt. 3 e 25 della Costituzione, questione di legittimità costituzionale dell'art. 10-ter del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), aggiunto dall'art. 35, comma 7, del decreto-legge 4 luglio 2006, n. 223 (Disposizioni urgenti per il rilancio economico e sociale, per il contenimento e la razionalizzazione della spesa pubblica, nonché interventi in materia di entrate e di contrasto all'evasione fiscale), convertito, con modificazioni, dalla legge 4 agosto 2006, n. 248, «in relazione all'art. 5» del medesimo d.lgs. n. 74 del 2000, «nella parte in cui prevede l'applicazione della normativa ai fatti commessi prima della sua entrata in vigore»;&#13;
 	che il giudice a quo - rilevato che il reato di cui all'art. 10-ter del d.lgs. n. 274 del 2000 «è stato introdotto in epoca successiva alla scadenza dei pagamenti periodici relativi ai redditi del 2005» - reputa «doversi accertare la legittimità costituzionale della suddetta normativa relativamente ai fatti contestati»;&#13;
 	che il rimettente «sospetta», in specie, la violazione dell'art. 3 Cost. in rapporto al principio di eguaglianza, «perché il termine di pagamento dell'imposta dovuta in base alla dichiarazione annuale dei redditi coincideva con la data di presentazione della dichiarazione anteriore all'entrata in vigore della normativa di cui se ne pretende l'applicazione»;&#13;
 	che sarebbe, altresì, violato l'art. 25 Cost., «perché nessuno può essere punito se non in forza di una legge che sia entrata in vigore prima dei fatti contestati»;&#13;
 	che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata inammissibile per difetto di motivazione sulla rilevanza e sui parametri costituzionali evocati o, comunque, nel merito, manifestamente infondata per erroneità del presupposto interpretativo.&#13;
 	Considerato che, nell'ordinanza di rimessione, il giudice a quo omette totalmente di descrivere la fattispecie concreta oggetto del giudizio principale e di motivare sulla rilevanza della questione;&#13;
 	che anche la motivazione sulla non manifesta infondatezza presenta evidenti carenze, risultando insufficiente e oscura;&#13;
 	che, quanto alla ipotizzata violazione del principio di irretroattività della norma incriminatrice (art. 25, secondo comma, della Costituzione), il giudice a quo non spiega, infatti, in modo adeguato perché - a suo avviso - il censurato art. 10-bis del d.lgs. 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), che contempla il delitto di omesso versamento dell'imposta sul valore aggiunto, dovrebbe trovare applicazione anche in rapporto a fatti commessi prima della sua entrata in vigore, nonostante il disposto dell'art. 2, primo comma, del codice penale;&#13;
 	che con riguardo, poi, al dedotto contrasto con il principio di eguaglianza (art. 3 Cost.), il giudice a quo prospetta un argomento - oltre che inesatto - di per sé inconferente ai fini della dimostrazione del vulnus denunciato (quale, in specie, l'asserita coincidenza del termine per il versamento dell'imposta sul valore aggiunto dovuta in base alla dichiarazione annuale con quello di presentazione della dichiarazione stessa);&#13;
 	che dall'ordinanza di rimessione non è dato neppure comprendere, infine, per quale ragione venga coinvolto nello scrutinio di costituzionalità («in relazione») anche l'art. 5 del d.lgs. n. 74 del 2000, che prevede il delitto di omessa presentazione della dichiarazione annuale ai fini delle imposte sui redditi o sul valore aggiunto (norma menzionata unicamente nel dispositivo);&#13;
 	che le manchevolezze evidenziate rendono la questione manifestamente inammissibile (ex plurimis, quanto all'omessa descrizione della fattispecie concreta e all'omessa motivazione sulla rilevanza, ordinanze n. 85 del 2010, n. 201 e n. 181 del 2009; quanto al difetto di motivazione sulla non manifesta infondatezza, ordinanze n. 202, n. 191 e n. 122 del 2009).&#13;
 	Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 	dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 10-ter del decreto legislativo 10 marzo 2000, n. 74 (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'articolo 9 della legge 25 giugno 1999, n. 205), aggiunto dall'art. 35, comma 7, del decreto-legge 4 luglio 2006, n. 223 (Disposizioni urgenti per il rilancio economico e sociale, per il contenimento e la razionalizzazione della spesa pubblica, nonché interventi in materia di entrate e di contrasto all'evasione fiscale), convertito, con modificazioni, dalla legge 4 agosto 2006, n. 248, in relazione all'art. 5 del medesimo d.lgs. n. 74 del 2000, sollevata, in riferimento agli artt. 3 e 25 della Costituzione, dal Tribunale di Orvieto con l'ordinanza indicata in epigrafe.&#13;
 	Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 novembre 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Giuseppe FRIGO, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria l'11 novembre 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
