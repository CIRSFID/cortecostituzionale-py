<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>91</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:91</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/01/1991</data_decisione>
    <data_deposito>16/02/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, dott. Francesco &#13;
 GRECO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco &#13;
 Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 458, primo e    &#13;
 secondo comma, del codice di procedura penale in  relazione  all'art.    &#13;
 442, comma secondo, stesso codice, promosso con ordinanza emessa il 6    &#13;
 giugno 1990 dal Tribunale di Milano nel procedimento penale a  carico    &#13;
 di  Kevi  Athanas,  iscritta  al n. 539 del registro ordinanze 1990 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  36,  prima    &#13;
 serie speciale, dell'anno 1990;                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 12 dicembre 1990 il Giudice    &#13;
 relatore Giovanni Conso;                                                 &#13;
    Ritenuto  che  il  Tribunale  di  Milano, nel corso di un giudizio    &#13;
 immediato a carico di  Kevi  Athanas  -  premesso  che  questi  aveva    &#13;
 tempestivamente  proposto  richiesta  di  giudizio abbreviato, cui il    &#13;
 pubblico ministero aveva negato il  proprio  consenso  senza  fornire    &#13;
 alcuna motivazione, e che gli atti erano stati trasmessi al Tribunale    &#13;
 dal Giudice per le indagini preliminari  ai  sensi  dell'art.457  del    &#13;
 codice  di  procedura  penale  - ha, con ordinanza del 6 giugno 1990,    &#13;
 sollevato, su eccezione della difesa, in riferimento  agli  artt.  3,    &#13;
 primo  comma,  e  24, secondo comma, della Costituzione, questione di    &#13;
 legittimità dell'art. 458, primo e  secondo  comma,  del  codice  di    &#13;
 procedura  penale,  "nella  parte  in  cui  non  prevede l'obbligo di    &#13;
 motivazione  del  dissenso  espresso  dal  p.m.  sulla  richiesta  di    &#13;
 giudizio  abbreviato  con  conseguente  esclusione per il giudice del    &#13;
 dibattimento della possibilità di pronunciare sentenza di  condanna,    &#13;
 all'esito  di  giudizio  immediato, con la diminuzione di pena di cui    &#13;
 all'art. 442, secondo comma c.p.p.";                                     &#13;
      e  che  nel  giudizio è intervenuto il Presidente del Consiglio    &#13;
 dei ministri, rappresentato e difeso dall'Avvocatura  Generale  dello    &#13;
 Stato,  chiedendo  che  la  questione venga dichiarata manifestamente    &#13;
 inammissibile;                                                           &#13;
    Considerato  che  questa  Corte,  con  sentenza  n. 81 del 1991 ha    &#13;
 dichiarato, in applicazione dell'art.27 della legge  11  marzo  1953,    &#13;
 n.87,  l'illegittimità  costituzionale dell'art.458, primo e secondo    &#13;
 comma, del codice di procedura penale, tanto nella parte in  cui  non    &#13;
 prevede che il pubblico ministero, in caso di dissenso, sia tenuto ad    &#13;
 enunciarne le ragioni, quanto nella parte in cui non prevede  che  il    &#13;
 giudice,  allorché,  a dibattimento concluso, ritiene ingiustificato    &#13;
 il dissenso del pubblico ministero, possa applicare  all'imputato  la    &#13;
 riduzione  di  pena  contemplata  dall'art.442,  secondo comma, dello    &#13;
 stesso codice;                                                           &#13;
      che,  di  conseguenza,  la  questione  qui  proposta deve essere    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 458, primo e secondo comma, del    &#13;
 codice   di  procedura  penale,  già  dichiarato  costituzionalmente    &#13;
 illegittimo con sentenza n. 81 del  1991,  nella  parte  in  cui  non    &#13;
 prevede che il pubblico ministero, in caso di dissenso, sia tenuto ad    &#13;
 enunciarne le ragioni e  nella  parte  in  cui  non  prevede  che  il    &#13;
 giudice,  quando,  a dibattimento concluso, ritiene ingiustificato il    &#13;
 dissenso del pubblico  ministero,  possa  applicare  all'imputato  la    &#13;
 riduzione  di  pena  contemplata  dall'art. 442, secondo comma, dello    &#13;
 stesso codice,  questione  sollevata  dal  Tribunale  di  Milano  con    &#13;
 ordinanza 6 giugno 1990.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 28 gennaio 1991.                              &#13;
                    Il Presidente e redattore: CONSO                      &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 16 febbraio 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
