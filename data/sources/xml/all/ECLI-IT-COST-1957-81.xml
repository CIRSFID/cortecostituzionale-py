<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1957</anno_pronuncia>
    <numero_pronuncia>81</numero_pronuncia>
    <ecli>ECLI:IT:COST:1957:81</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Antonio Manca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/05/1957</data_decisione>
    <data_deposito>25/05/1957</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Prof. &#13;
 TOMASO PERASSI - Prof. GASPARE AMBROSINI - Prof. ERNESTO BATTAGLINI - &#13;
 Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. MARIO BRACCI &#13;
 - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO &#13;
 PETROCELLI - Dott. ANTONIO MANCA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  del  decreto   del  &#13;
 Presidente  della  Repubblica 3 ottobre 1952, n. 1755, pubblicato nella  &#13;
 Gazzetta Ufficiale del 3 dicembre 1952, supplemento ordinario  n.  280,  &#13;
 promosso  con  ordinanza  del  Tribunale  di Montepulciano emessa il 19  &#13;
 dicembre 1956, nella causa civile tra  Borghi  Aldo  e  l'Ente  per  la  &#13;
 colonizzazione  della  Maremma tosco-laziale, pubblicata nella Gazzetta  &#13;
 Ufficiale n. 27 del 30 gennaio 1957 ed iscritta al n.  9  del  Registro  &#13;
 ordinanze del 1957.                                                      &#13;
     Udita,  nell'udienza  pubblica  del 27 marzo 1957, la relazione del  &#13;
 giudice Antonio Manca;                                                   &#13;
     uditi gli avvocati Guido Astuti,  Arturo  Carlo  Jemolo,  Francesco  &#13;
 Santoro Passarelli e Massimo Severo Giannini per l'Ente Maremma.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza  19  dicembre  1956 il Tribunale di Montepulciano ha  &#13;
 disposto la  trasmissione  a  questa  Corte  degli  atti  del  giudizio  &#13;
 promosso  da  Borghi  Aldo  contro  l'Ente  per la colonizzazione della  &#13;
 Maremma tosco-laziale, per ottenere la restituzione dei terreni,  posti  &#13;
 nel   Comune   di  Castiglion  d'Orcia,  espropriati  con  decreto  del  &#13;
 Presidente della Repubblica 3 ottobre 1952, pubblicato  nella  Gazzetta  &#13;
 Ufficiale  del 3 dicembre 1952, supplemento ordinario n. 280, previa la  &#13;
 dichiarazione di  illegittimità  costituzionale  del  decreto  stesso,  &#13;
 oltre al risarcimento dei danni.                                         &#13;
     Il  Tribunale,  respinta  l'eccezione preliminare dedotta dall'Ente  &#13;
 Maremma, nel senso che il Borghi avrebbe impugnato in via principale  e  &#13;
 diretta,  anziché  in  via  incidentale,  il decreto di espropriazione  &#13;
 avente  valore  di  legge,  per  quanto  attiene  alla   questione   di  &#13;
 legittimità  costituzionale, ha osservato che i decreti presidenziali,  &#13;
 emanati in base all'art. 5 della legge 12 maggio 1950, n.   230 per  le  &#13;
 espropriazioni  relative  alla riforma agraria, hanno valore di legge e  &#13;
 che perciò il sindacato sulla legittimità dei decreti  stessi  spetta  &#13;
 esclusivamente  alla Corte costituzionale. Ha ritenuto d'altra parte la  &#13;
 questione non manifestamente infondata, in quanto, tenendo presente  il  &#13;
 testo  dell'art.  4  della  legge  21  ottobre 1950, n. 841, il decreto  &#13;
 presidenziale sopra ricordato sarebbe viziato da eccesso di delega.  Il  &#13;
 quale, nell'ordinanza, è profilato nel senso che il reddito dominicale  &#13;
 della proprietà del Borghi, ai fini dell'espropriazione, sarebbe stato  &#13;
 valutato  erroneamente in base alla consistenza catastale alla data del  &#13;
 15 novembre 1949, non già tenendosi conto esclusivamente  del  reddito  &#13;
 al  1  gennaio  1943.  Ha  ritenuto altresì che, nella specie, sarebbe  &#13;
 irrilevante, ai fini della legittimità del decreto  di  esproprio,  il  &#13;
 fatto  che  il reddito medio della proprietà del Borghi, accertato nel  &#13;
 1943, sarebbe inferiore a lire 100 per ettaro.                           &#13;
     Detta ordinanza, ritualmente notificata alle parti e al  Presidente  &#13;
 del  Consiglio  dei Ministri, comunicata ai Presidenti della Camera dei  &#13;
 deputati e del Senato, è stata pubblicata nella Gazzetta Ufficiale del  &#13;
 30 gennaio 1957.                                                         &#13;
     Nella cancelleria della Corte si è costituito in  termine,  il  15  &#13;
 febbraio  1957,  l'Ente  Maremma,  che  ha depositato le deduzioni, con  &#13;
 procura in calce da parte del Presidente dell'Ente stesso. Il 14  marzo  &#13;
 successivo   sono  stati  depositati:  la  deliberazione  del  predetto  &#13;
 Presidente di resistere nell'attuale giudizio, il parere del  Consiglio  &#13;
 consultivo  e l'approvazione della deliberazione da parte del Ministero  &#13;
 competente.                                                              &#13;
     La difesa dell'Ente Maremma ripropone preliminarmente la  eccezione  &#13;
 di inammissibilità della questione di legittimità costituzionale già  &#13;
 prospettata nel senso che, nel caso, si avrebbe un'impugnazione diretta  &#13;
 del decreto presidenziale, non già incidentale al fine della decisione  &#13;
 della causa.                                                             &#13;
     Nel  merito  contesta  l'eccesso  di  delega,  osservando  che, nel  &#13;
 decreto del 1952, col quale è stata  disposta  l'espropriazione  della  &#13;
 quota   di  proprietà  del  Borghi  non  si  può  riscontrare  alcuna  &#13;
 violazione dell'art. 4 della legge 21 ottobre 1950, n. 841. Assume  che  &#13;
 la  data  del  1  gennaio  1943,  indicata  in  detto  articolo  per la  &#13;
 valutazione del reddito dominicale, è quella in cui andarono in vigore  &#13;
 in tutto il territorio nazionale, per i terreni a nuovo, come a vecchio  &#13;
 catasto, le tariffe di estimo, la cui revisione era stata disposta  dal  &#13;
 R.  D.L.  4 aprile 1939, n. 589, convertito nella legge 29 giugno 1939,  &#13;
 n. 976. La  data  del  1  gennaio  1943  pertanto,  secondo  la  difesa  &#13;
 dell'Ente,  si  riferisce, nell'art. 4 della legge stralcio, non già a  &#13;
 quella a cui si doveva aver riguardo nella valutazione delle risultanze  &#13;
 catastali (qualità e classe dei terreni) per  le  singole  particelle,  &#13;
 bensì alla tariffa di estimo da applicarsi alle risultanze stesse, per  &#13;
 determinare  il  reddito  dominicale  particellare  e  totale  ai  fini  &#13;
 dell'espropriazione.                                                     &#13;
     Aggiunge l'Ente Maremma che erroneamente il Tribunale  non  avrebbe  &#13;
 ritenuto  rilevante  il fatto che il reddito medio della proprietà del  &#13;
 Borghi era inferiore a lire 100 per ettaro; circostanza  per  la  quale  &#13;
 detta  proprietà  sarebbe  sempre  passibile  di scorporo in base alla  &#13;
 tabella annessa alla legge stralcio, secondo cui, nel caso  di  reddito  &#13;
 medio  inferiore  a  lire  100, la espropriazione è disposta quando il  &#13;
 reddito totale supera, come nella specie, le lire 20.000.<diritto>Considerato in diritto</diritto>:                          &#13;
      Deve ritenersi infondata l'eccezione preliminare dedotta dall'Ente  &#13;
 Maremma  nel  senso che l'impugnazione del decreto di esproprio sarebbe  &#13;
 stata proposta come oggetto principale del giudizio  di  merito  e  non  &#13;
 già   in   via   incidentale,  quale  presupposto  necessario  per  la  &#13;
 definizione del giudizio medesimo.   Tale eccezione è  stata  respinta  &#13;
 con  la  sentenza  n.  59 del 13 maggio 1957, alla quale pertanto basta  &#13;
 fare riferimento per la decisione e per la motivazione.                  &#13;
     Nel merito, secondo quanto si desume dall'ordinanza  del  Tribunale  &#13;
 di  Montepulciano,  il  Borghi  dedusse  che,  per calcolare il reddito  &#13;
 dominicale dell'intera proprietà terriera,  si  sarebbe  dovuto  tener  &#13;
 conto esclusivamente del reddito al 1 gennaio 1943, il quale per la sua  &#13;
 proprietà  non  superava  le  lire 30.000, e perciò la sua proprietà  &#13;
 sarebbe stata immune da  esproprio,  anche  se  il  reddito  dominicale  &#13;
 complessivo   in  epoca  successiva  fosse  stato  maggiore.  Donde  la  &#13;
 illegittimità del decreto, emesso nei suoi confronti, per  eccesso  di  &#13;
 delega,  in  relazione  all'art. 4 della legge 21 ottobre 1950, n. 841,  &#13;
 perché l'Ente espropriante accertò invece il reddito stesso in misura  &#13;
 superiore a quella indicata, tenendo conto della consistenza  catastale  &#13;
 della proprietà terriera del Borghi alla data del 15 novembre 1949.     &#13;
     La  tesi  sostenuta dal Borghi peraltro, ad avviso della Corte, non  &#13;
 risponde  alla  corretta  interpretazione  del  citato  art.  4.  Detto  &#13;
 articolo  dispone che, nei territori considerati dalla legge n. 841 del  &#13;
 21 ottobre 1950  (suscettibili  cioè  di  trasformazione  fondiaria  o  &#13;
 agraria),  la  proprietà terriera privata, nella sua consistenza al 15  &#13;
 novembre 1949, è soggetta ad espropriazione di una  quota  determinata  &#13;
 in  base al reddito dominicale dell'intera proprietà al 1 gennaio 1943  &#13;
 e al reddito dominicale per ettaro, inteso quest'ultimo come  quoziente  &#13;
 della  divisione  del complessivo reddito dominicale per la superficie.  &#13;
 Si desume quindi dal testo legislativo che, secondo  il  sistema  della  &#13;
 legge  n.  841,  per  stabilire  se,  e  in  quale quota, la proprietà  &#13;
 terriera privata è soggetta  a  scorporo,  non  basta  riferirsi  alla  &#13;
 superficie  (come dispone l'art. 2 della legge 12 maggio 1950, n. 230),  &#13;
 ma occorre aver riguardo alla combinazione  di  tale  elemento  con  la  &#13;
 produttività  dei terreni. Della quale è indice il reddito dominicale  &#13;
 secondo le tariffe di estimo catastale in vigore il 1 gennaio 1943,  in  &#13;
 seguito  alla  revisione  generale  disposta dal decreto-legge 4 aprile  &#13;
 1939, n. 589 (convertito, con  modificazioni,  nella  legge  29  giugno  &#13;
 1939,  n. 976). Il che chiarisce che la legge n. 841, come del resto si  &#13;
 rileva anche dalla relazione alla Camera del  Ministro  proponente,  al  &#13;
 fine  della  valutazione  del  reddito richiama le disposizioni di tale  &#13;
 decreto, il quale, a sua volta, si  ricollega  espressamente  al  testo  &#13;
 unico  delle  leggi  sul  nuovo  catasto  (approvato con decreto dell'8  &#13;
 ottobre 1931, n. 1572). Ond'è che, per intendere le  disposizioni  del  &#13;
 citato  art.  4,  non  si  può  prescindere  da  quelle  contenute nei  &#13;
 provvedimenti legislativi ora ricordati.                                 &#13;
     Dispone infatti al riguardo l'art. 13 del citato testo unico  delle  &#13;
 leggi  sul  catasto che "la tariffa esprime in moneta legale la rendita  &#13;
 imponibile di un ettaro per  ciascuna  qualità  e  classe".  E  devesi  &#13;
 intendere  per  qualità,  ai  sensi  dell'art.  58 del regolamento per  &#13;
 l'esecuzione delle leggi sull'imposta fondiaria (approvato con  decreto  &#13;
 del  12  ottobre  1932,  n.  1539),  la qualificazione che consiste nel  &#13;
 distinguere  i  terreni   di   ciascun   comune   secondo   le   specie  &#13;
 essenzialmente  differenti,  tanto  per  la  diversa coltivazione a cui  &#13;
 vengono di solito destinati i terreni stessi,  quanto  per  il  diverso  &#13;
 loro  prodotto  spontaneo,  od anche per altre condizioni o circostanze  &#13;
 notevoli e permanenti. Per classe, d'altra  parte,  si  deve  intendere  &#13;
 (art.  60 del detto regolamento) la classificazione dei terreni, tenuto  &#13;
 conto dei gradi notevolmente diversi della rispettiva produttività. Se  &#13;
 ne  deduce  perciò  che,  nel  sistema  delle  leggi   catastali,   la  &#13;
 determinazione  del  reddito  dominicale, mediante l'applicazione delle  &#13;
 tariffe  di  estimo  per  ettaro,  è  strettamente  collegata  ai  due  &#13;
 coefficienti  generali  della  qualità  e  della  classe,  riferiti in  &#13;
 concreto,  in  seguito  alle  operazioni  di   classamento   (prevedute  &#13;
 dall'art.  75  e  seguenti  del citato regolamento), ad una determinata  &#13;
 zona di terreno costituente la particella catastale. Ma appunto perché  &#13;
 il classamento deve essere aderente  alla  situazione  effettiva  della  &#13;
 proprietà  terriera,  esso  può  subire  variazioni  in  seguito alle  &#13;
 verifiche periodiche e a quelle straordinarie, da effettuarsi  in  base  &#13;
 alle  disposizioni contenute negli artt. 118 e seguenti del regolamento  &#13;
 per la conservazione del  nuovo  catasto  dei  terreni,  approvato  con  &#13;
 decreto dell'8 dicembre 1938, n.  2153.                                  &#13;
     Poiché  quindi, come si è già accennato, l'art. 4 della legge n.  &#13;
 841, per il calcolo  del  reddito  dominicale  si  riporta  al  sistema  &#13;
 catastale  vigente,  è  logico  dedurre, che (contrariamente alla tesi  &#13;
 sostenuta, a quanto risulta  dall'ordinanza,  dal  Borghi  in  sede  di  &#13;
 merito),  ai  fini dello scorporo, il reddito dell'intera proprietà è  &#13;
 determinato dall'applicazione delle tariffe di estimo in  vigore  al  1  &#13;
 gennaio  1943,  con  riferimento  però,  in applicazione appunto delle  &#13;
 norme delle leggi sul catasto, alla consistenza, cioè  al  classamento  &#13;
 dei  terreni,  nella  situazione  che il predetto art. 4, ai fini della  &#13;
 riforma fondiaria o agraria,  considera  stabilizzata  al  15  novembre  &#13;
 1949.                                                                    &#13;
     D'altronde  che i dati catastali debbano corrispondere alla realtà  &#13;
 di fatto, risulta anche dall'art. 6 della legge n.    841;  poiché  ad  &#13;
 evitare  sperequazioni, nelle zone ove sono in vigore i vecchi catasti,  &#13;
 anche  il  proprietario  espropriato  ha  facoltà  di   ricorso   alla  &#13;
 commissione  competente,  ai  fini  della determinazione definitiva del  &#13;
 reddito dominicale imponibile, per ogni questione  riflettente  la  non  &#13;
 corrispondenza  dell'estensione,  della classe di produttività e della  &#13;
 qualità di coltura del fondo rispetto ai dati risultanti dal catasto.   &#13;
     Poiché  pertanto  l'Ente  Maremma  ha  ritenuto  suscettibile   di  &#13;
 scorporo la proprietà terriera del Borghi, applicando i principi sopra  &#13;
 esposti  per  valutare  il reddito dominicale complessivo, accertato in  &#13;
 misura superiore alle lire 30.000,  il  decreto  del  Presidente  della  &#13;
 Repubblica  3  ottobre  1952, n. 1755, che ha disposto l'espropriazione  &#13;
 nei   confronti   dello   stesso    Borghi,    non    può    ritenersi  &#13;
 costituzionalmente  illegittimo  per  eccesso  di  delega, in relazione  &#13;
 all'art. 4 della legge n. 841 del 1950.                                  &#13;
     Dato ciò, resta assorbita la questione, dedotta in via subordinata  &#13;
 dall'Ente Maremma, nel senso che se anche il reddito complessivo  della  &#13;
 proprietà  terriera  del Borghi fosse inferiore, come egli assumeva, a  &#13;
 quello accertato dall'Ente espropriante, essendo tuttavia  superiore  a  &#13;
 lire  20.000,  con una media per ettaro inferiore a lire 100 secondo le  &#13;
 ammissioni del Borghi, la proprietà  stessa  sarebbe  stata  parimenti  &#13;
 soggetta ad esproprio.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     respinta   l'eccezione   pregiudiziale  dedotta  dall'Ente  per  la  &#13;
 colonizzazione della Maremma tosco-laziale;                              &#13;
     dichiara non fondata la  questione,  sollevata  con  ordinanza  del  &#13;
 Tribunale  di  Montepulciano  del  19 dicembre 1956, sulla legittimità  &#13;
 costituzionale del  decreto  del  Presidente  della  Repubblica  del  3  &#13;
 ottobre  1952,  n.  1755,  pubblicato  nella  Gazzetta  Ufficiale del 3  &#13;
 dicembre 1952, in relazione all'art. 4 della legge 21 ottobre 1950,  n.  &#13;
 841,   e  in  riferimento  agli  art.  76  e  77,  primo  comma,  della  &#13;
 Costituzione.                                                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 16 maggio 1957.                               &#13;
                                   GAETANO  AZZARITI  - TOMASO PERASSI -  &#13;
                                   GASPARE    AMBROSINI    -     ERNESTO  &#13;
                                   BATTAGLINI    -   MARIO   COSATTI   -  &#13;
                                   FRANCESCO   PANTALEO    GABRIELI    -  &#13;
                                   GIUSEPPE  CASTELLI  AVOLIO - ANTONINO  &#13;
                                   PAPALDO  -  MARIO  BRACCI  -   NICOLA  &#13;
                                   JAEGER  - GIOVANNI CASSANDRO - BIAGIO  &#13;
                                   PETROCELLI - ANTONIO MANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
