<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>16</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:16</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/01/1993</data_decisione>
    <data_deposito>29/01/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 16 della legge    &#13;
 3 maggio 1982, n. 203 (Norme  sui  contratti  agrari),  promosso  con    &#13;
 ordinanza  emessa  il  18  marzo  1992  dal  Tribunale di Foggia, nel    &#13;
 procedimento  civile  vertente  tra  Centonza  Antonio  ed  altro,  e    &#13;
 Fondazione  Domenico  ed  Antonia Siniscalco-Ceci, iscritta al n. 258    &#13;
 del registro ordinanze 1992 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 20, prima serie speciale, dell'anno 1992;            &#13;
    Visto  l'atto di costituzione di Centonza Antonio e Matteo nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei Ministri;          &#13;
    Udito nell'udienza  pubblica  del  18  novembre  1992  il  Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Uditi  gli  avvocati  Giovanni  Di  Mattia ed Emilio Romagnoli per    &#13;
 Centonza Antonio e Matteo e l'Avvocato dello Stato Gaetano Zotta  per    &#13;
 il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un procedimento in cui gli attori, affittuari di    &#13;
 un   fondo   rustico,   avevano   richiesto  una  sentenza  attuativa    &#13;
 dell'obbligo del concedente  di  asservire  il  fondo  ad  una  nuova    &#13;
 costruzione, il Tribunale di Foggia, con ordinanza emessa il 18 marzo    &#13;
 1992,  ha  sollevato,  in riferimento all'art. 44 della Costituzione,    &#13;
 questione di legittimità costituzionale dell'art. 16 della  legge  3    &#13;
 maggio   1982,  n.  203,  nella  parte  in  cui,  pur  prevedendo  la    &#13;
 possibilità  dell'affittuario  di  eseguire  sul  fondo   opere   di    &#13;
 miglioramento,  ovvero  addizioni  e  trasformazioni con l'osservanza    &#13;
 della procedura ivi stabilita, non esclude l'obbligo del proprietario    &#13;
 di sottoporre il fondo a vincoli di natura reale, ove ciò  si  renda    &#13;
 necessario per l'esecuzione di dette opere.                              &#13;
    Espone  il  giudice  a  quo che gli attori erano stati autorizzati    &#13;
 dall'Ispettorato provinciale dell'agricoltura ad eseguire un progetto    &#13;
 di  ristrutturazione  di  locali  per  il   ricovero   del   bestiame    &#13;
 nell'azienda  cerealicola-zootecnica  da essi condotta ed avevano poi    &#13;
 ottenuto la concessione  edilizia  subordinata  all'asservimento  del    &#13;
 fondo   alla  nuova  costruzione,  ma,  a  seguito  del  rifiuto  del    &#13;
 proprietario  di  sottoscrivere  il   relativo   atto   notarile   di    &#13;
 asservimento,  avevano  adito  il  tribunale  ex art. 2932 del codice    &#13;
 civile.                                                                  &#13;
    Secondo il giudice rimettente, la norma  non  porrebbe  limiti  al    &#13;
 diritto  dell'affittuario  di  eseguire  migliorie  e trasformazioni,    &#13;
 anche in presenza del rifiuto del proprietario, così risolvendosi in    &#13;
 una limitazione della proprietà terriera,  potenzialmente  riduttiva    &#13;
 del  valore  di  mercato  dei  fondi  e  lesiva  dell'art.  44  della    &#13;
 Costituzione.                                                            &#13;
    2. - È intervenuto il  Presidente  del  Consiglio  dei  Ministri,    &#13;
 rappresentato  e  difeso dall'Avvocatura dello Stato, che ha concluso    &#13;
 per l'inammissibilità, ovvero per l'infondatezza, in quanto la norma    &#13;
 risulterebbe mal  richiamata  e  perché  non  sarebbe  nella  specie    &#13;
 esperibile l'azione ex art. 2932 del codice civile.                      &#13;
    3.  -  Nel  giudizio dinanzi a questa Corte si sono costituiti gli    &#13;
 attori  nel  giudizio  a  quo  che  hanno  anche  depositato  memoria    &#13;
 ulteriore nell'imminenza dell'udienza.                                   &#13;
    La   difesa   delle  parti  private  ha  preliminarmente  eccepito    &#13;
 l'irrilevanza della  questione  per  l'asserita  inefficacia  di  una    &#13;
 pronuncia  d'illegittimità  costituzionale nei confronti dei vincoli    &#13;
 contenuti nei piani regolatori  e  l'inammissibilità  in  quanto  la    &#13;
 richiesta sentenza verrebbe a configurarsi come mera esecuzione di un    &#13;
 giudicato  già  formatosi  sulla decisione dell'Ispettorato (onde il    &#13;
 problema riguarderebbe l'incremento - o meno - di valore  del  fondo,    &#13;
 da  esaminarsi soltanto in sede d'indennizzo). Nel merito, si insiste    &#13;
 sull'infondatezza,  sottolineandosi  come  la  ristrutturazione   del    &#13;
 manufatto  fosse  finalizzata  a quel più razionale sfruttamento del    &#13;
 suolo che la legge favorisce e la stessa Corte  costituzionale  nella    &#13;
 sentenza  n.  53  del 1974 ha legittimato, sia pure con riguardo alla    &#13;
 previgente normativa.<diritto>Considerato in diritto</diritto>1.  -   Il   Tribunale   di   Foggia   denuncia   l'illegittimità    &#13;
 costituzionale  dell'art. 16 della legge 3 maggio 1982, n. 203 (Norme    &#13;
 sui  contratti  agrari),  nella  parte  in  cui   la   detta   norma,    &#13;
 autorizzando  l'affittuario  ad  eseguire,  secondo  una prestabilita    &#13;
 procedura,   opere   di   miglioramento   del   fondo,   non  esclude    &#13;
 espressamente l'obbligo, per il proprietario, di sottoporre a vincoli    &#13;
 di natura reale il fondo stesso (nella specie: asservimento  a  nuova    &#13;
 costruzione).                                                            &#13;
    L'omessa  previsione  si porrebbe in contrasto con l'art. 44 della    &#13;
 Costituzione, che riserva al legislatore  l'imposizione  di  obblighi    &#13;
 alla   proprietà  terriera  privata  onde  conseguire  il  razionale    &#13;
 sfruttamento del suolo e stabilire equi rapporti sociali.                &#13;
    2. - La questione è inammissibile.                                   &#13;
    La norma impugnata sancisce al  primo  comma  il  diritto  per  il    &#13;
 locatore   e   l'affittuario   di  eseguire  opere  di  miglioramento    &#13;
 fondiario, addizioni e trasformazioni  di  ordinamenti  produttivi  e    &#13;
 fabbricati rurali nel rispetto della destinazione agricola del fondo,    &#13;
 dei  programmi regionali di sviluppo o, in mancanza di questi ultimi,    &#13;
 delle vocazioni colturali delle relative zone.                           &#13;
    All'esercizio  di  tale  diritto  è   ordinato   un   dettagliato    &#13;
 procedimento,  descritto  nei commi successivi, attivato dall'impulso    &#13;
 propositivo della parte che intende eseguire le opere, la  quale,  in    &#13;
 difetto  di  accordo,  ne comunica la natura, le caratteristiche e le    &#13;
 finalità   all'altra   parte    ed    all'Ispettorato    provinciale    &#13;
 dell'agricoltura.                                                        &#13;
    L'Ispettorato  promuove  quindi,  attraverso  la  convocazione del    &#13;
 locatore  e  dell'affittuario,  con  l'eventuale   assistenza   delle    &#13;
 rispettive  organizzazioni  professionali,  un  tentativo  di accordo    &#13;
 sulla descritta proposta. In  mancanza,  viene  emessa  una  motivata    &#13;
 pronuncia,  sostanzialmente  conformativa  del contenuto del diritto,    &#13;
 sia sul piano delle modalità di esecuzione che dei tempi  di  inizio    &#13;
 ed ultimazione delle opere.                                              &#13;
    A  questo  punto  compete  al proprietario-locatore la facoltà di    &#13;
 eseguire le opere stesse. Peraltro, nel caso di inerzia di questi, è    &#13;
 l'affittuario a surrogarsi in tale posizione  soggettiva,  procedendo    &#13;
 all'esecuzione,  e  perfino  richiedendo,  ove  occorrano,  permessi,    &#13;
 concessioni, autorizzazioni ed eventuali finanziamenti (cfr. art. 17,    &#13;
 quinto comma).                                                           &#13;
    3. - L'intero meccanismo  è  volto  a  superare  le  contrapposte    &#13;
 situazioni  nel  pubblico  e prevalente interesse allo sviluppo della    &#13;
 produzione agraria: in tale quadro,  la  declaratoria  richiesta  dal    &#13;
 giudice  a  quo a questa Corte risulta incompatibile con la struttura    &#13;
 della norma impugnata ed in contraddizione con la  ratio  dell'intera    &#13;
 legge  sui contratti agrari, in particolare con la preminenza da essa    &#13;
 accordata all'iniziativa imprenditoriale dell'affittuario.               &#13;
    La sentenza additiva auspicata dal Tribunale di  Foggia,  ove  mai    &#13;
 fosse  ipotizzabile,  verrebbe  infatti  a  vanificare  la  logica di    &#13;
 mediazione sottesa alle  descritte  previsioni,  paralizzando  quelle    &#13;
 istanze   di   miglioria   che  non  possono  invece  trovare  limite    &#13;
 nell'inerzia di una delle parti, a fortiori di  quella  proprietaria,    &#13;
 che non partecipa all'attività d'impresa.                               &#13;
   In  realtà,  il  senso della censura non risiede tanto nell'omessa    &#13;
 previsione dell'esclusione di un obbligo di  sottoporre  il  fondo  a    &#13;
 vincoli, quanto si traduce piuttosto in una critica al modo in cui la    &#13;
 legge   ha  preventivamente  risolto  il  conflitto  d'interessi  tra    &#13;
 proprietario ed affittuario. Ma l'ordinanza di rimessione  interviene    &#13;
 in   un   momento  in  cui  tali  interessi  ormai  sono  stati  già    &#13;
 contemperati nel procedimento dinanzi all'Ispettorato, conclusosi con    &#13;
 la  decisione  -  non  impugnata  -  da questo assunta ed in una fase    &#13;
 sostanzialmente  attuativa  della  stessa.  Superato   tale   momento    &#13;
 procedimentale, del quale questa Corte ha già valutato la congruità    &#13;
 nella logica degli "equi rapporti sociali" (cfr. ordinanza n. 601 del    &#13;
 1988),  è chiaro come la dichiarazione di asservimento - presupposto    &#13;
 di efficacia della concessione edilizia - altro non sia che una delle    &#13;
 conseguenze della pronuncia dell'Ispettorato, e pertanto un  giudizio    &#13;
 di  costituzionalità finalizzato a ridiscutere la sequenza attuativa    &#13;
 della iniziativa di miglioria, per la sua intrinseca illogicità, non    &#13;
 può essere ammesso;</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara inammissibile la questione di legittimità  costituzionale    &#13;
 dell'art.  16  della legge 3 maggio 1982, n. 203 (Norme sui contratti    &#13;
 agrari), sollevata, in riferimento all'art.  44  della  Costituzione,    &#13;
 dal Tribunale di Foggia, con l'ordinanza di cui in epigrafe.             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 gennaio 1993.                              &#13;
                  Il Presidente e redattore: CASAVOLA                     &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 29 gennaio 1993.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
