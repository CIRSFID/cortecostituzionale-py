<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>219</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:219</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Gaetano Silvestri</relatore_pronuncia>
    <redattore_pronuncia>Gaetano Silvestri</redattore_pronuncia>
    <data_decisione>06/06/2007</data_decisione>
    <data_deposito>18/06/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 58-quater, comma 7-bis, della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà), aggiunto dall'art. 7, comma 7, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione), promosso con ordinanza del 31 dicembre 2005 dal Tribunale di sorveglianza di Firenze, iscritta al n. 126 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 18, prima serie speciale, dell'anno 2006. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 4 giugno 2007 il Giudice relatore Gaetano Silvestri. &#13;
    Ritenuto che, con ordinanza del 31 dicembre 2005, il Tribunale di sorveglianza di Firenze ha sollevato, in riferimento all'art. 25, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 58-quater, comma 7-bis, della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà), aggiunto dall'art. 7, comma 7, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione), nella parte in cui, «letto congiuntamente con il disposto dell'art. 10, commi 1 e 2 di quest'ultima legge», prevede che il limite di una sola concessione, stabilito per l'accesso alla misura dell'affidamento in prova al servizio sociale, alla detenzione domiciliare e alla semilibertà, si applichi anche ai soggetti condannati, dichiarati recidivi reiterati, per fatti commessi prima dell'entrata in vigore della stessa legge n. 251 del 2005; &#13;
    che il Tribunale rimettente è chiamato a provvedere sull'istanza, presentata in data 7 luglio 2005, di applicazione della misura dell'affidamento in prova al servizio sociale in relazione all'esecuzione della pena complessiva di anni uno, mesi due e giorni ventotto di reclusione, risultante dal provvedimento di cumulo emesso in data 9 giugno 2005, e già sospesa ai sensi dell'art. 656, comma 5, del codice di procedura penale; &#13;
    che, secondo quanto riferisce il rimettente, con una delle sentenze di condanna cui si riferisce il provvedimento di cumulo è stata applicata, nei confronti del soggetto istante, l'aggravante della recidiva reiterata infraquinquennale;  &#13;
    che l'interessato, il quale risulta - secondo il Tribunale - ben integrato nel contesto socio-familiare di riferimento, ha già beneficiato della misura dell'affidamento in prova al servizio sociale, disposta in data 3 ottobre 2000 e conclusasi positivamente, con dichiarazione di estinzione della pena, in data 19 febbraio 2003;  &#13;
    che, in punto di rilevanza, il giudice a quo osserva come, per effetto dell'introduzione del comma 7-bis dell'art. 58-quater della legge n. 354 del 1975, operata dall'art. 7, comma 7, della legge n. 251 del 2005, legge entrata in vigore mentre l'istruttoria era in corso, l'istanza di affidamento in prova al servizio sociale «per quanto ammissibile nel momento della sua proposizione», non lo è più al momento della decisione, risultando l'aggravante della recidiva ostativa alla concessione per la seconda volta del beneficio richiesto; &#13;
    che, a parere del rimettente, le disposizioni che regolano la fase di esecuzione della pena, per effetto del disposto dell'art. 10 della legge n. 251 del 2005, rimangono soggette al principio tempus regit actum, così risultando applicabili anche ai soggetti condannati per fatti commessi anteriormente all'entrata in vigore della stessa legge; &#13;
    che il giudice a quo sollecita una revisione dell'orientamento, prevalente nella giurisprudenza di legittimità, secondo il quale l'ambito di applicazione del principio di irretroattività sarebbe circoscritto alle sole norme penali sostanziali, sul presupposto che il principio sancito nell'art. 25, secondo comma, Cost. si riferisca unicamente alle norme incriminatici in senso stretto, e non anche alle norme che incidono sulle modalità di esecuzione e sulla quantità e qualità della pena da espiare in concreto; &#13;
    che, infatti, le misure alternative alla detenzione, per quanto regolate da una legge ad hoc e sottratte al governo del giudice della cognizione, andrebbero considerate «intrinseche» al sistema delle norme penali in senso lato;  &#13;
    che, infine, il rimettente rammenta come la tematica in oggetto sia rimasta res integra nella giurisprudenza costituzionale e richiama l'immediato precedente costituito dalla sentenza della Corte n. 273 del 2001, la quale risolse la questione allora sollevata senza investire il quesito di fondo, oggi riproposto. &#13;
    Considerato che il Tribunale di sorveglianza di Firenze dubita della legittimità costituzionale, in riferimento all'art. 25, secondo comma, della Costituzione, dell'art. 58-quater, comma 7-bis, della legge 26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle misure privative e limitative della libertà), aggiunto dall'art. 7, comma 7, della legge 5 dicembre 2005, n. 251 (Modifiche al codice penale e alla legge 26 luglio 1975, n. 354, in materia di attenuanti generiche, di recidiva, di giudizio di comparazione delle circostanze di reato per i recidivi, di usura e di prescrizione), nella parte in cui, «letto congiuntamente con il disposto dell'art. 10, commi 1 e 2 di quest'ultima legge», prevede che il limite di una sola concessione, stabilito per l'accesso alla misura dell'affidamento in prova al servizio sociale, alla detenzione domiciliare e alla semilibertà, si applichi anche ai soggetti condannati, dichiarati recidivi reiterati, per fatti commessi prima dell'entrata in vigore della stessa legge n. 251 del 2005; &#13;
    che, successivamente alla proposizione della questione, questa Corte, con la sentenza n. 79 del 2007, pubblicata nella Gazzetta Ufficiale n. 12 del 21 marzo 2007, ha dichiarato l'illegittimità costituzionale, per violazione dell'art. 27, terzo comma, Cost., dei commi 1 e 7-bis dell'art. 58-quater della legge n. 354 del 1975, introdotti dall'art. 7, commi 6 e 7, della legge n. 251 del 2005, nella parte in cui non prevedono che i benefici in essi indicati possano essere concessi, sulla base della normativa previgente, nei confronti dei condannati che, prima dell'entrata in vigore della citata legge n. 251 del 2005, abbiano raggiunto un grado di rieducazione adeguato ai benefici richiesti; &#13;
    che, pertanto, va ordinata la restituzione degli atti al giudice rimettente, al fine di una nuova valutazione della rilevanza della questione sollevata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    ordina la restituzione degli atti al Tribunale di sorveglianza di Firenze. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 giugno 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Gaetano SILVESTRI, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 giugno 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
