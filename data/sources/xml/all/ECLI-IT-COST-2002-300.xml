<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>300</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:300</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>19/06/2002</data_decisione>
    <data_deposito>28/06/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Giovanni Maria FLICK, Francesco AMIRANTE, &#13;
Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 54,  terzo &#13;
comma,  della  legge  26 luglio  1975, n. 354 (Norme sull'ordinamento &#13;
penitenziario  e sulla esecuzione delle misure privative e limitative &#13;
della  liberta),  promosso  con  ordinanza emessa l'8 maggio 2001 dal &#13;
Tribunale  di sorveglianza di Torino nel procedimento di sorveglianza &#13;
relativo a G.F., iscritta al numero 876 del registro ordinanze 2001 e &#13;
pubblicata  nella Gazzetta Ufficiale della Repubblica n. 43, 1ª serie &#13;
speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio dell'8 maggio 2002 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto  che  il  Tribunale di sorveglianza di Torino, dopo aver &#13;
puntualizzato  di  aver  promosso  d'ufficio un procedimento in vista &#13;
della  eventuale  revoca  della liberazione anticipata compresa in un &#13;
provvedimento di cumulo, essendosi il condannato reso responsabile di &#13;
una  grave  infrazione  disciplinare  per  la  quale  era  verosimile &#13;
l'inizio  di  un  procedimento penale, poi, peraltro, non avviato per &#13;
difetto  di querela della persona offesa, ha sollevato in riferimento &#13;
agli artt. 3, 27, secondo comma, e 112 della Costituzione - questione &#13;
di legittimità costituzionale dell'art. 54, terzo comma, della legge &#13;
26 luglio  1975, n. 354 (Norme sull'ordinamento penitenziario e sulla &#13;
esecuzione  delle misure privative e limitative della liberta), nella &#13;
parte  in  cui  non  prevede  che  causa  di revoca della liberazione &#13;
anticipata  possa  essere,  oltre  alla  condanna  per delitto, anche &#13;
l'accertamento,  da  parte del tribunale di sorveglianza, di un fatto &#13;
costituente  delitto  perseguibile  a querela per il quale la querela &#13;
non è stata presentata; &#13;
        che  a  parere del giudice rimettente la disciplina impugnata &#13;
si  porrebbe in contrasto con gli artt. 27, secondo comma, e 3 Cost., &#13;
in  quanto  il  livello di casualità della revoca, determinato dalla &#13;
libertà   di  scelta  nella  presentazione  o  meno  della  querela, &#13;
renderebbe  eventuale  la correlazione tra la fruizione del beneficio &#13;
ed  il merito penitenziario, incrinando la funzione rieducativa della &#13;
pena  e  ponendo  al  tempo stesso le basi per "abnormi disparità di &#13;
trattamento" fondate su una decisione privata; &#13;
        che  del  pari  vulnerati  risulterebbero  gli  artt. 112 e 3 &#13;
Cost.,  in  quanto  la  revoca  di  benefici  immeritati  assumerebbe &#13;
connotazioni    non    dissimili   da   quelle   che   caratterizzano &#13;
l'obbligatorietà  della  azione  penale,  sia perché essa partecipa &#13;
"dell'originaria  obbligatorietà  del  perseguimento  giudiziale del &#13;
fatto  (che, sia pure espresso con riferimento all'agire del pubblico &#13;
ministero,  pare  esprimere  un  principio generale), sia perché una &#13;
revocabilità  discrezionale  sarebbe inconciliabile con il principio &#13;
di uguaglianza"; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri, rappresentato e difeso dalla Avvocatura generale dello &#13;
Stato, chiedendo dichiararsi non fondata la proposta questione. &#13;
    Considerato  che  il  giudice  a  quo  ha  iniziato  d'ufficio il &#13;
procedimento   di   sorveglianza,  nell'ambito  del  quale  è  stato &#13;
sollevato  l'incidente  di costituzionalità, al limitato fine - come &#13;
sottolinea  l'ordinanza  di  rimessione  - di "... valutare l'ipotesi &#13;
della revoca della liberazione anticipata ..." concessa nei confronti &#13;
di  un  condannato,  ponendo  a  base  di  tale  "ipotesi" una "grave &#13;
infrazione  disciplinare"  commessa  dal condannato medesimo e per la &#13;
quale  poteva reputarsi "probabile" la relativa persecuzione anche in &#13;
sede  penale:  così  da  consigliare  l'apertura del procedimento di &#13;
revoca  del  beneficio, "...da istruirsi parallelamente all'eventuale &#13;
processo penale, all'esito del quale, sarebbe stato decidibile"; &#13;
        che  alla  stregua  di tali premesse in fatto emerge, dunque, &#13;
con chiarezza che il Tribunale rimettente ha iniziato il procedimento &#13;
di  sorveglianza  -  nel  quale  è  stato poi iscritto il quesito di &#13;
costituzionalità  -  al  di fuori di qualsiasi base normativa, posto &#13;
che  è  lo  stesso  rimettente  a  riconoscere che tale procedimento &#13;
sarebbe   stato   "decidibile"  soltanto  all'esito  "dell'eventuale" &#13;
processo  penale: una decisione "eventuale", quindi, non soltanto nel &#13;
merito  ma  - anche e soprattutto - in rito, in quanto subordinata al &#13;
realizzarsi  di un presupposto non soltanto anch'esso "eventuale" (la &#13;
condanna), ma addirittura a sua volta dipendente da altra condizione, &#13;
pure essa "eventuale" (l'inizio del processo penale); &#13;
        che,  pertanto,  il  procedimento  di sorveglianza in oggetto &#13;
finisce  per risultare del tutto eccentrico rispetto al sistema così &#13;
come  positivamente  delineato:  infatti l'intera sequenza degli atti &#13;
cui  il  Tribunale  ha  nella  specie dato vita si configura come una &#13;
atipica   "inchiesta   preliminare",   priva  di  una  norma  che  ne &#13;
legittimasse (e ne imponesse) l'insorgenza; e destinata a chiudersi o &#13;
con  un  "non  provvedimento" ("non doversi provvedere in ordine alla &#13;
revoca",  come riconosciuto dal rimettente), o con l'effettivo inizio &#13;
di  un  legittimo  procedimento  di  sorveglianza per la revoca della &#13;
liberazione   anticipata,  ove  fosse  intervenuta  una  sentenza  di &#13;
condanna.  D'altra  parte, non è senza significato, agli effetti che &#13;
qui   rilevano,   la   circostanza  che  l'art. 103  del  regolamento &#13;
penitenziario  stabilisca, proprio in tema di liberazione anticipata, &#13;
che  l'organo  del pubblico ministero competente per l'esecuzione sia &#13;
tenuto  a  comunicare  al  tribunale  di sorveglianza "la sentenza di &#13;
condanna  inflitta  al  soggetto  per  delitto  non  colposo commesso &#13;
durante   l'esecuzione   della   pena":   così   testimoniando  come &#13;
l'ordinamento  non  tolleri,  in  materia,  inchieste  o accertamenti &#13;
"preliminari"  e configuri - a seguito della sentenza n. 186 del 1995 &#13;
-  la condanna, non più come condizione necessaria e sufficiente per &#13;
la  revoca  della  liberazione  anticipata, ma come "presupposto" che &#13;
legittima l'inizio del relativo procedimento; &#13;
        che,  di  conseguenza,  la  questione  proposta  deve  essere &#13;
dichiarata manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  dell'art. 54,  terzo comma, della legge &#13;
26 luglio  1975, n. 354 (Norme sull'ordinamento penitenziario e sulla &#13;
esecuzione  delle  misure  privative  e  limitative  della  liberta), &#13;
sollevata,  in  riferimento  agli  artt. 3,  27, secondo comma, e 112 &#13;
della  Costituzione,  dal  Tribunale  di  sorveglianza  di Torino con &#13;
l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 19 giugno 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 28 giugno 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
