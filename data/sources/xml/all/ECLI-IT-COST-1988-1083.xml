<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>1083</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:1083</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/11/1988</data_decisione>
    <data_deposito>06/12/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 1, commi primo    &#13;
 e secondo, della legge 6 agosto 1984, n. 425 ("Disposizioni  relative    &#13;
 al  trattamento economico dei magistrati"); 3 della legge 19 febbraio    &#13;
 1981, n. 27 ("Provvidenze per  il  personale  di  magistratura");  9,    &#13;
 secondo  comma,  della legge 2 aprile 1979, n. 97 ("Norme sullo stato    &#13;
 giuridico dei magistrati e sul trattamento economico  dei  magistrati    &#13;
 ordinari  e amministrativi, dei magistrati della giustizia militare e    &#13;
 degli avvocati dello Stato"),  in  relazione  agli  artt.  5,  ultimo    &#13;
 comma,  del  d.P.R.  28  dicembre  1970,  n. 1080 ("Norme sulla nuova    &#13;
 disciplina del trattamento economico del personale di cui alla  legge    &#13;
 24  maggio 1951, n. 392"), 2, lett. d), della legge 16 dicembre 1961,    &#13;
 n. 1308 ("Modifiche alla legge 29 dicembre 1956, n. 1433, concernente    &#13;
 il  trattamento  economico  della  magistratura,  dei  magistrati del    &#13;
 Consiglio di Stato, della Corte dei conti, della Giustizia militare e    &#13;
 degli avvocati e procuratori dello Stato"), e 10, ultimo comma, della    &#13;
 legge 20 dicembre 1961, n. 1345 ("Istituzione di  una  quarta  e  una    &#13;
 quinta  Sezione  speciale  per  i  giudizi  su  ricorsi in materia di    &#13;
 pensioni di guerra ed altre  disposizioni  relative  alla  Corte  dei    &#13;
 conti"),  promossi con undici ordinanze emesse il 16 ottobre 1987 dal    &#13;
 Consiglio di Stato, iscritte rispettivamente ai nn.  256,  257,  258,    &#13;
 259, 260, 261, 262, 263, 264, 265 e 266 del registro ordinanze 1988 e    &#13;
 pubblicate nella Gazzetta Ufficiale della  Repubblica  n.  24,  prima    &#13;
 serie speciale, dell'anno 1988.                                          &#13;
    Visti  gli  atti  di  costituzione  di  Messina  Salvatore,  di De    &#13;
 Francisco Ruggero ed altri e di  Della  Valle  Pauciullo  Giuseppina,    &#13;
 nonché  gli  atti  di  intervento  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 9 novembre 1988 il Giudice    &#13;
 relatore Francesco Paolo Casavola.                                       &#13;
    Ritenuto  che  con  undici identiche ordinanze, tutte emesse il 16    &#13;
 ottobre 1987,  il  Consiglio  di  Stato,  prendendo  le  mosse  dalla    &#13;
 sentenza  di  questa  Corte n. 123 del 7 aprile 1987, ha sollevato le    &#13;
 seguenti questioni di legittimità costituzionale:  A)  dell'art.  1,    &#13;
 commi primo e secondo, della legge 6 agosto 1984, n. 425 in relazione    &#13;
 agli artt. 24, 102 e 103 della Costituzione;  B)  dell'art.  3  della    &#13;
 legge 19 febbraio 1981, n. 27, come interpretato dalla legge 6 agosto    &#13;
 1984, n. 425, nonché dell'art. 1, primo comma, della legge citata da    &#13;
 ultimo, in relazione agli artt. 3 e 36 della Costituzione;               &#13;
  C) dell'art. 9, secondo comma, della legge 2 aprile 1979, n. 97 (con    &#13;
 riferimento all'art. 5, ultimo comma, d.P.R.  28  dicembre  1970,  n.    &#13;
 1080; all'art. 2, lett. d), legge 16 dicembre 1961, n. 1308; all'art.    &#13;
 10, ultimo comma, legge 20 dicembre 1961, n. 1345) in relazione  agli    &#13;
 artt. 3 e 36 della Costituzione;                                         &#13;
      che,  premessa  un'accurata disamina delle vicende legislative e    &#13;
 giurisprudenziali concernenti l'estensione degli aumenti periodici di    &#13;
 stipendio  già attribuiti ai soli magistrati della Corte dei conti a    &#13;
 tutte le categorie equiparate, nonché  il  riconoscimento  a  queste    &#13;
 ultime  della  speciale  indennità  prevista  per  i soli magistrati    &#13;
 ordinari dall'art. 3 della legge 19 febbraio 1981, n. 27, il  giudice    &#13;
 a  quo  prende  atto  della  soluzione data per il futuro al problema    &#13;
 attraverso la legge n. 425 del 1984 e del conseguente  ridursi  della    &#13;
 materia del contendere;                                                  &#13;
      che,  tuttavia,  il Consiglio di Stato rileva come contrasti con    &#13;
 gli  artt.  24,   102   e   103   della   Costituzione   l'intervento    &#13;
 interpretativo  di  cui  alla  norma  impugnata  sub  a),  in  quanto    &#13;
 espressione  di  una  volontà   tesa   a   svalutare   la   funzione    &#13;
 giurisdizionale  e  produttiva,  in  un  ben  circoscritto ambito, di    &#13;
 effetti retroattivi (contrastanti con la ratio della legge citata  da    &#13;
 ultimo, ispirata al principio dell'identità di trattamento tra tutti    &#13;
 i magistrati);                                                           &#13;
      che,  infine,  quanto  all'aspetto  sostanziale della disciplina    &#13;
 risultante dalla censurata normativa il  giudice  rimettente  osserva    &#13;
 che  essa  appare  finalizzata  a  smentire  l'orientamento  espresso    &#13;
 dall'Adunanza plenaria del Consiglio di Stato medesimo e si  sofferma    &#13;
 quindi:  1)  sull'illogicità  della  limitazione  ai soli magistrati    &#13;
 ordinari della  speciale  indennità  sopra  citata  (successivamente    &#13;
 estesa    alle   altre   categorie   dal   1°   gennaio   1983);   2)    &#13;
 sull'irrazionalità di un più favorevole meccanismo di calcolo degli    &#13;
 "scatti"  per  i  magistrati  della Corte dei conti. In ordine a tale    &#13;
 ultimo punto si chiarisce da parte del giudice a quo  che  l'art.  9,    &#13;
 secondo   comma,  della  legge  2  aprile  1979,  n.  97,  era  stato    &#13;
 interpretato dalla disposizione denunziata sub  a),  in  un'accezione    &#13;
 antitetica  a  quella  voluta  dalla  citata  decisione dell'Adunanza    &#13;
 plenaria (e cioè nel senso  che  essa  conserva,  per  i  magistrati    &#13;
 diversi  da  quelli  della Corte dei conti, un deteriore regime degli    &#13;
 aumenti periodici). In tal senso va intesa la censura a  detta  norma    &#13;
 rivolta sub c);                                                          &#13;
      che le ordinanze concludono prospettando, oltre alla lesione del    &#13;
 principio d'eguaglianza,  anche  la  violazione  dell'art.  36  della    &#13;
 Costituzione;                                                            &#13;
      che  nel giudizio dinanzi a questa Corte di cui all'ordinanza di    &#13;
 rimessione n. 263 del 1988 si è costituita la parte privata  che  ha    &#13;
 insistito      per     l'accoglimento     dei     diversi     profili    &#13;
 d'incostituzionalità;                                                   &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, la quale ha  concluso  per    &#13;
 la    declaratoria    d'inammissibilità    ovvero    d'infondatezza,    &#13;
 richiamandosi alla sentenza di questa Corte n. 413 del 24 marzo 1988.    &#13;
    Considerato che i relativi giudizi possono essere riuniti e decisi    &#13;
 con un'unica ordinanza;                                                  &#13;
      che  questa  Corte,  con  la  sentenza  n. 413 del 1988, ha già    &#13;
 dichiarato   l'infondatezza   della   questione    di    legittimità    &#13;
 costituzionale  dell'art.  1,  secondo  comma, della legge n. 425 del    &#13;
 1984  che  limita  ai  soli  magistrati  della  Corte  dei  conti  il    &#13;
 particolare   meccanismo   di   calcolo   degli   aumenti   periodici    &#13;
 d'anzianità, rilevando come la norma, oltre  all'eliminazione  delle    &#13;
 incertezze  interpretative,  sia volta a costituire "l'indispensabile    &#13;
 presupposto  logico  e  organizzatorio  della  ristrutturazione   del    &#13;
 trattamento economico per tutte le categorie dei magistrati";            &#13;
     che,  pertanto,  è  manifestamente infondata l'analoga questione    &#13;
 sollevata dal giudice a quo il quale, con il riferimento all'art.  9,    &#13;
 secondo  comma,  della  legge n. 97 del 1979, ed agli artt. 24, 102 e    &#13;
 103 della Costituzione  intende  motivare  l'asserita  lesione  delle    &#13;
 norme  sulla  giurisdizione già a suo tempo esclusa, avendo la Corte    &#13;
 ravvisato   nel   complesso   della    normativa    l'esercizio    di    &#13;
 discrezionalità   legislativa  finalizzata  alla  realizzazione  del    &#13;
 principio di eguaglianza e di ragionevolezza";                           &#13;
      che  considerazioni  d'identico  contenuto  possono svolgersi in    &#13;
 riferimento al  primo  comma  della  citata  disposizione  il  quale,    &#13;
 correlativamente,  interpreta  l'art.  3  della legge n. 27 del 1981,    &#13;
 istitutivo di  una  speciale  indennità  in  favore  dei  magistrati    &#13;
 dell'ordine  giudiziario,  nel  senso di attribuire soltanto a questi    &#13;
 ultimi tale emolumento;                                                  &#13;
      che  anche  con  riguardo  a  tale  ipotesi  deve richiamarsi la    &#13;
 generale finalità perequativa perseguita dalla legge n. 425 del 1984    &#13;
 intesa non già a vanificare il giudicato ovvero ad invadere l'ambito    &#13;
 proprio  dell'attività  giudiziaria,  bensì  ad   eliminare   esiti    &#13;
 privilegiari  di  trattamento  economico evitando che essi si sommino    &#13;
 con quanto dalla legge stessa previsto,  in  un  quadro  di  generale    &#13;
 equilibrio    delle    retribuzioni    dei    magistrati    ordinari,    &#13;
 amministrativi, contabili e militari, nonché  degli  avvocati  dello    &#13;
 Stato.                                                                   &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi,                                                     &#13;
     a)   dichiara   la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 1, commi primo e secondo, della    &#13;
 legge  6  agosto  1984, n. 425 ("Disposizioni relative al trattamento    &#13;
 economico dei magistrati"), sollevata, in relazione  agli  artt.  24,    &#13;
 102 e 103 della Costituzione, dal Consiglio di Stato con le ordinanze    &#13;
 di cui in epigrafe;                                                      &#13;
     b)   dichiara   la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 3 della legge 19 febbraio 1981,    &#13;
 n.   27  ("Provvidenze  per  il  personale  di  magistratura"),  come    &#13;
 interpretato dalla legge 6 agosto 1984, n. 425, nonché dell'art.  1,    &#13;
 primo  comma,  della  legge  6  agosto 1984, n. 425, sollevata con le    &#13;
 medesime ordinanze in relazione agli artt. 3 e 36 della Costituzione;    &#13;
     c)   dichiara   la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 9, secondo comma, della legge 2    &#13;
 aprile 1979, n. 97 ("Norme sullo stato giuridico dei magistrati e sul    &#13;
 trattamento economico dei magistrati ordinari e  amministrativi,  dei    &#13;
 magistrati  della  giustizia militare e degli avvocati dello Stato"),    &#13;
 con riferimento agli artt. 5, ultimo comma, del  d.P.R.  28  dicembre    &#13;
 1970,   n.  1080  ("Norme  sulla  nuova  disciplina  del  trattamento    &#13;
 economico del personale di cui alla legge 24 maggio 1951,  n.  392"),    &#13;
 2,  lett.  d), della legge 16 dicembre 1961, n. 1308 ("Modifiche alla    &#13;
 legge 29 dicembre 1956, n. 1433, concernente il trattamento economico    &#13;
 della  magistratura,  dei  magistrati  del  Consiglio di Stato, della    &#13;
 Corte  dei  conti,  della  Giustizia  militare  e  degli  avvocati  e    &#13;
 procuratori  dello  Stato"),  e  10,  ultimo  comma,  della  legge 20    &#13;
 dicembre 1961, n. 1345 ("Istituzione  di  una  quarta  e  una  quinta    &#13;
 Sezione  speciale  per i giudizi su ricorsi in materia di pensioni di    &#13;
 guerra  ed  altre  disposizioni  relative  alla  Corte  dei  conti"),    &#13;
 sollevata  con  le  medesime ordinanze in relazione agli artt. 3 e 36    &#13;
 della Costituzione.                                                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 novembre 1988.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 6 dicembre 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
