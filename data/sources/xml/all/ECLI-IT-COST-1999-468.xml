<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>468</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:468</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/12/1999</data_decisione>
    <data_deposito>30/12/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, avv. Fernanda CONTRI, prof. &#13;
 Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale &#13;
 MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale della legge della Regione    &#13;
 Veneto, riapprovata il  12  giugno  1998,  recante  "Addestramento  e    &#13;
 allenamento  dei  falchi  per  l'esercizio  venatorio"  promosso  con    &#13;
 ricorso del Presidente del Consiglio dei Ministri,  notificato  il  1    &#13;
 luglio 1998, depositato in cancelleria l'11 successivo ed iscritto al    &#13;
 n. 31 del registro ricorsi 1998.                                         &#13;
   Visto l'atto di costituzione della Regione Veneto;                     &#13;
   Udito nell'udienza pubblica del 12 ottobre 1999 il giudice relatore    &#13;
 Fernanda Contri;                                                         &#13;
   Uditi l'avvocato dello Stato Ignazio F. Caramazza per il Presidente    &#13;
 del  Consiglio  dei  Ministri e l'avvocato Mario Loria per la Regione    &#13;
 Veneto.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ricorso  regolarmente  notificato  e   depositato   del    &#13;
 Presidente  del Consiglio dei Ministri, il Governo ha promosso in via    &#13;
 principale, in riferimento all'art. 117 della Costituzione e all'art.    &#13;
 18 della legge 11 febbraio 1992, n.  157  (Norme  per  la  protezione    &#13;
 della  fauna  selvatica  omeoterma  e  per  il  prelievo  venatorio),    &#13;
 questione di legittimità costituzionale della  legge  della  Regione    &#13;
 Veneto  concernente  "Addestramento  e  allenamento  dei  falchi  per    &#13;
 l'esercizio  venatorio",  riapprovata  a  maggioranza  assoluta   dal    &#13;
 Consiglio regionale nella seduta del 12 giugno 1998, con modifiche al    &#13;
 testo rinviato dal Governo con atto del 3 aprile 1998.                   &#13;
   Nella  sua  versione  originaria,  l'impugnata delibera legislativa    &#13;
 veniva  rinviata  al  Consiglio   regionale   giacché,   consentendo    &#13;
 l'addestramento   dei   falchi  per  l'esercizio  venatorio  "durante    &#13;
 l'intero periodo  dell'anno"  -  attività,  si  legge  nel  ricorso,    &#13;
 qualificabile come venatoria alla luce della sentenza di questa Corte    &#13;
 n.  578  del  1990  -,  appariva  in  contrasto con la disciplina dei    &#13;
 periodi venatori di cui all'art.  18 della menzionata  legge  n.  157    &#13;
 del 1992.                                                                &#13;
   In   accoglimento   dei   rilievi   formulati  in  sede  di  rinvio    &#13;
 governativo, la Regione Veneto modificava la  legge  introducendo  il    &#13;
 "divieto di predazione di fauna selvatica limitatamente ai periodi di    &#13;
 caccia chiusa" (art. 3,  comma 3).                                       &#13;
   Ad  avviso del ricorrente, tale modifica non consente di superare i    &#13;
 rilievi governativi, in considerazione dell'inidoneità  del  divieto    &#13;
 legislativo "a produrre qualunque modificazione del comportamento del    &#13;
 falco in volo" (così nel ricorso).                                      &#13;
   2.  -  Nel  giudizio  davanti  a  questa  Corte si è costituita la    &#13;
 Regione Veneto per chiedere una declaratoria  di  infondatezza  della    &#13;
 questione di legittimità costituzionale promossa dal Governo.           &#13;
   Premesso  che  già  in altre Regioni sono in vigore discipline del    &#13;
 tutto   analoghe   a   quella   censurata,   e   tuttavia    sfuggite    &#13;
 all'impugnativa  statale,  la  Regione resistente deduce, nel merito,    &#13;
 che il ricorso governativo si baserebbe su  un  presupposto  erroneo:    &#13;
 "che il semplice addestramento del falco da caccia costituisca in sé    &#13;
 esercizio di attività venatoria, pure se esso non abbia come scopo e    &#13;
 come   effetto   l'abbattimento   di  selvaggina".  Con  la  modifica    &#13;
 introdotta in accoglimento del rinvio, il  legislatore  regionale  ha    &#13;
 inteso  escludere  espressamente  che l'attività di addestramento di    &#13;
 cui si tratta  possa  "assumere  i  contenuti  e  le  caratteristiche    &#13;
 dell'intervento  venatorio",  come  individuati anche alla luce della    &#13;
 pronuncia di questa Corte richiamata dal ricorrente.                     &#13;
   La normativa censurata, obietta la difesa della  Regione,  preclude    &#13;
 al falconiere di utilizzare forme di addestramento che contemplino la    &#13;
 cattura  o  l'abbattimento di esemplari di selvaggina: "sarà compito    &#13;
 dell'istruttore assumere le precauzioni ed adottare gli  accorgimenti    &#13;
 necessari  ad evitare che il rapace, nel corso dell'allenamento, posa    &#13;
 attaccare  animali  appartenenti  alla  fauna  selvatica;   parimenti    &#13;
 graverà  sull'istruttore la responsabilità per eventuali violazioni    &#13;
 della legge, qualora il falco ponga in essere azioni predatorie".        &#13;
   3. - In prossimità della data fissata per  l'udienza,  la  Regione    &#13;
 Veneto  ha  depositato  una  memoria  illustrativa ad integrazione di    &#13;
 quanto già dedotto con l'atto di costituzione.                          &#13;
   La difesa della Regione si diffonde sulle caratteristiche  e  sulle    &#13;
 modalità  di  esercizio della falconeria per smentire l'assunto, dal    &#13;
 quale  muove  la  censura  statale,  della   natura   necessariamente    &#13;
 venatoria dell'attività di addestramento del falco.                     &#13;
   Si  legge  nella memoria della Regione che l'addestramento tende in    &#13;
 una prima fase a sviluppare nel rapace una capacità  di  adattamento    &#13;
 alla  presenza  dell'uomo,  inducendolo  a riconoscere e a seguire il    &#13;
 proprio falconiere. In una seconda fase, il falco viene fatto volare,    &#13;
 legato al pugno del falconiere, a distanze sempre maggiori. La  terza    &#13;
 fase  dell'addestramento, che come le precedenti deve necessariamente    &#13;
 precedere l'apertura della caccia, è quella del  volo  libero  senza    &#13;
 l'impiego di prede vive.                                                 &#13;
   In  ordine  al  rilievo  governativo della inidoneità del precetto    &#13;
 legislativo "a produrre qualunque modificazione del comportamento del    &#13;
 falco in volo", la Regione resistente replica che "seguendo l'assunto    &#13;
 dello Stato, si dovrebbe concludere che il tipo di  caccia  in  esame    &#13;
 non  offre  mai  sufficienti  garanzie di rispetto della normativa di    &#13;
 settore, in quanto il rapace potrebbe pur sempre  essere  indotto  ad    &#13;
 abbattere  esemplari sottratti al prelievo, o fare incursione in aree    &#13;
 protette".                                                               &#13;
   La Regione Veneto richiama  infine  l'attenzione  sull'insieme  dei    &#13;
 vincoli  e  dei  controlli cui la legge regionale impugnata subordina    &#13;
 l'attività di addestramento in  questione:  iscrizione  in  apposito    &#13;
 registro   provinciale;   previa   presentazione  di  un  dettagliato    &#13;
 programma di addestramento; limitazione dell'àmbito territoriale per    &#13;
 l'esercizio dell'attività di cui si tratta (comune  di  residenza  o    &#13;
 comune  confinante con il primo); consenso del proprietario dell'area    &#13;
 destinata  allo  scopo;  obbligo  di  preventiva  comunicazione  alla    &#13;
 Provincia del periodo di utilizzazione del falco.                        &#13;
   I   menzionati   vincoli,   conclude   la   difesa  della  Regione,    &#13;
 "restringono fortemente i margini di libertà del falconiere e  danno    &#13;
 completa garanzia in ordine al corretto svolgimento dell'attività".<diritto>Considerato in diritto</diritto>1.  -  Con  ricorso  del  Presidente  del  Consiglio  dei  Ministri    &#13;
 notificato il 1 e depositato l'11 luglio 1998, il Governo ha promosso    &#13;
 in via principale - in riferimento all'art. 117  della  Costituzione,    &#13;
 in  relazione all'art. 18 della legge 11 febbraio 1992, n. 157 (Norme    &#13;
 per la protezione della fauna selvatica omeoterma e per  il  prelievo    &#13;
 venatorio)  - questione di legittimità costituzionale della delibera    &#13;
 legislativa concernente "Addestramento e allenamento dei  falchi  per    &#13;
 l'esercizio   venatorio",  riapprovata  a  maggioranza  assoluta  dal    &#13;
 Consiglio regionale del Veneto nella seduta del 12 giugno  1998,  con    &#13;
 modifiche al testo rinviato dal Governo con atto del 3 aprile 1998.      &#13;
   Il  ricorrente impugna l'art. 3, comma 3, della menzionata delibera    &#13;
 legislativa, a norma del quale "con l'iscrizione al registro  di  cui    &#13;
 al  comma  2  dell'art.  2,  il  falconiere  viene  autorizzato dalla    &#13;
 Provincia ad addestrare ed allenare i falchi durante l'intero periodo    &#13;
 dell'anno, con divieto di predazione di fauna selvatica limitatamente    &#13;
 ai periodi di caccia chiusa", in  quanto,  apparendo  non  idoneo  "a    &#13;
 produrre  qualunque  modificazione  del  comportamento  del  falco in    &#13;
 volo", si  porrebbe  in  contrasto  con  la  disciplina  dei  periodi    &#13;
 venatori di cui all'art.  18 della legge n. 157 del 1992.                &#13;
   2. - La questione non è fondata.                                      &#13;
   3.  -  Alla disciplina dell'attività venatoria di cui all'invocata    &#13;
 legge n. 157 del 1992 si può estendere quanto questa Corte ebbe modo    &#13;
 di affermare, nella decisione richiamata dal ricorrente (sentenza  n.    &#13;
 578  del  1990),  con  riguardo  all'abrogata  legge  n. 968 del 1977    &#13;
 (Principi generali e disposizioni per la protezione  e  tutela  della    &#13;
 fauna  e  disciplina  della caccia). Anche alla stregua della vigente    &#13;
 legge-quadro,  può infatti ribadirsi che l'attività venatoria viene    &#13;
 a caratterizzarsi per  il  tipo  di  azioni  svolte  (abbattimento  o    &#13;
 cattura  di  animali  e  attività  preparatorie),  per l'oggetto cui    &#13;
 l'attività in questione risulta  diretta  (animali  da  abbattere  o    &#13;
 catturare  appartenenti  alla  fauna  selvatica), nonché per i mezzi    &#13;
 destinati allo svolgimento della stessa  attività  (armi  o  animali    &#13;
 consentiti dalla legge come strumenti di caccia).                        &#13;
   Tale  premessa  non  rende  tuttavia  la denunciata legge regionale    &#13;
 lesiva dei principi  stabiliti  dalla  legge  n.  157  del  1992.  Al    &#13;
 contrario, nei limiti in cui, attraverso l'attività di addestramento    &#13;
 del  falco,  o  in  conseguenza  di  essa, si realizzino i menzionati    &#13;
 presupposti, deve trovare piena  applicazione  la  disciplina,  anche    &#13;
 sanzionatoria, in materia di periodi venatori, che, come questa Corte    &#13;
 ha  avuto  occasione  di chiarire in consonanza con la normativa e la    &#13;
 giurisprudenza comunitarie, contribuisce alla delineazione del nucleo    &#13;
 minimo di salvaguardia della fauna selvatica  (sentenze  n.  168  del    &#13;
 1999; n. 323 del 1998).                                                  &#13;
   Ai  fini  del  presente  giudizio costituzionale, non è necessario    &#13;
 entrare nel merito della questione  dell'oggettiva  impossibilità  -    &#13;
 asserita  dal ricorrente e diffusamente contestata nelle difese della    &#13;
 Regione - di addestrare il falco da caccia impedendo al  rapace  ogni    &#13;
 azione di predazione di esemplari di fauna selvatica.                    &#13;
   Anche  se  si dovesse aderire all'assunto - nel ricorso, per altro,    &#13;
 indimostrato - che esclude a  priori  la  possibilità  di  porre  in    &#13;
 essere   pratiche   o   modalità  di  addestramento  non  implicanti    &#13;
 predazione, la delibera legislativa impugnata  non  si  appaleserebbe    &#13;
 idonea a concretare un vulnus agli invocati princìpi fondamentali in    &#13;
 materia  di  periodi venatori. La disciplina censurata, tutt'al più,    &#13;
 verrebbe a configurarsi, per  un  verso,  come  norma  preordinata  a    &#13;
 facoltizzare  un  comportamento  (l'addestramento  senza  predazione)    &#13;
 ritenuto impossibile; per  un  altro  verso,  come  reiterazione  del    &#13;
 divieto di predazione durante la chiusura della caccia. In ogni caso,    &#13;
 la  disposizione  impugnata  non sarebbe di ostacolo - in presenza di    &#13;
 episodi di predazione di esemplari  di  fauna  selvatica  durante  la    &#13;
 chiusura  della  stagione venatoria - alla doverosa irrogazione delle    &#13;
 previste misure sanzionatorie; trattandosi di una norma, assistita da    &#13;
 sanzione, destinata a trovare  applicazione  incondizionata,  poiché    &#13;
 vieta  in  termini  assoluti  ogni  attività  di  addestramento o di    &#13;
 allenamento implicante predazione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 della   legge  della  Regione  Veneto  concernente  "Addestramento  e    &#13;
 allenamento  dei  falchi  per  l'esercizio  venatorio",  approvata  a    &#13;
 maggioranza    assoluta   dal   Consiglio   regionale,   in   seconda    &#13;
 deliberazione,  nella  seduta  del  12  giugno  1998,  sollevata  dal    &#13;
 Presidente del Consiglio dei Ministri con il ricorso in epigrafe.        &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 dicembre 1999.                             &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 30 dicembre 1999.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
