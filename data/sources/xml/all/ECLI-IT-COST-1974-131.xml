<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>131</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:131</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Vezio Crisafulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/05/1974</data_decisione>
    <data_deposito>15/05/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO - Presidente &#13;
 - Dott. GIUSEPPE VERZÌ - Avv. GIOVANNI BATTISTA BENEDETTI - Avv. &#13;
 ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA &#13;
 REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale del combinato  &#13;
 disposto degli artt. 104 e 128 del d.P.R. 27 marzo 1969, n. 130  (Stato  &#13;
 giuridico  dei  dipendenti  degli  enti  ospedalieri), promossi con due  &#13;
 ordinanze emesse il 17 novembre 1972 dal Consiglio di Stato - sezione V  &#13;
 - sui ricorsi di Sedda Maria contro gli  Spedali  riuniti  di  Livorno,  &#13;
 iscritte  ai  nn.  204  e  205 del registro ordinanze 1973 e pubblicate  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 191 del 25 luglio  1973  e  &#13;
 n. 198 del 14 agosto 1973.                                               &#13;
     Visto l'atto di costituzione di Sedda Maria;                         &#13;
     udito  nell'udienza  pubblica  del 6 marzo 1974 il Giudice relatore  &#13;
 Vezio Crisafulli;                                                        &#13;
     udito l'avv. Roberto Lucifredi, per Sedda Maria.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con due ordinanze di contenuto pressoché identico  emesse  il  &#13;
 17  novembre  1972,  nel  corso di altrettanti procedimenti promossi su  &#13;
 ricorso della sig.na Sedda Maria contro gli Spedali riuniti di Livorno,  &#13;
 il Consiglio di  Stato  in  sede  giurisdizionale  -  sezione  V  -  ha  &#13;
 sollevato questione di legittimità, in riferimento agli artt. 76, 3, 4  &#13;
 e  97  della  Costituzione,  degli artt. 104 e 128 della legge delegata  &#13;
 emanata, in materia  di  stato  giuridico  dei  dipendenti  degli  enti  &#13;
 ospedalieri, con d.P.R. 27 marzo 1969, n. 130, "nella parte in cui tali  &#13;
 norme escludono che in via transitoria i posti della carriera direttiva  &#13;
 nel  personale  amministrativo  degli ospedali possano essere assegnati  &#13;
 mediante concorsi (e  in  particolare  mediante  concorsi  interni)  al  &#13;
 personale  di  detta  carriera  fornito  dei  requisiti  previsti dalla  &#13;
 normativa vigente al momento dell'entrata in vigore del suddetto d.P.R.  &#13;
 e ancorché sprovvisto del requisito del diploma di  laurea  prescritto  &#13;
 dal medesimo decreto presidenziale.                                      &#13;
     Vi sarebbe, infatti, secondo l'ordinanza, violazione della legge di  &#13;
 delegazione  12  febbraio  1968, n. 132, e specificamente del principio  &#13;
 direttivo ivi fissato all'art.   42, cpv., secondo il  quale  "in  ogni  &#13;
 caso dovranno essere riconosciute le posizioni giuridiche ed economiche  &#13;
 acquisite  dal  personale  già  in servizio", posto che tale principio  &#13;
 sarebbe stato inteso dallo stesso legislatore delegato nel  senso  più  &#13;
 ampio,  come comprensivo cioè anche delle possibilità di progressione  &#13;
 in  carriera  già  maturate,  per  quanto   riguarda   la   disciplina  &#13;
 transitoria  relativa  alle varie carriere diverse da quella direttiva.  &#13;
 Per analoghe considerazioni l'omesso riconoscimento a  questi  fini  di  &#13;
 posizioni  acquisite nel corso di precedenti anni di servizio dovrebbe,  &#13;
 inoltre, ritenersi lesivo del diritto costituzionale al lavoro  secondo  &#13;
 le    proprie   possibilità   e   la   propria   scelta,   nell'ambito  &#13;
 dell'organizzazione prevista dalla legge. Ma soprattutto il trattamento  &#13;
 sfavorevole che ne risulta per il personale  della  carriera  direttiva  &#13;
 rispetto  a  quello  delle  carriere inferiori ed allo stesso personale  &#13;
 sanitario degli ospedali sarebbe  in  contrasto  con  il  principio  di  &#13;
 eguaglianza,  in  quanto  non  sorretto  da  idonea giustificazione. Un  &#13;
 ultimo  profilo  concerne  poi  la   inosservanza   dei   principi   di  &#13;
 imparzialità e di buon andamento della attività amministrativa, nella  &#13;
 misura  in  cui  la  deviazione  da  tali  principi  possa direttamente  &#13;
 imputarsi alle norme censurate anziché a  comportamenti  concretamente  &#13;
 posti  in  essere  dall'Amministrazione  nell'esercizio dei suoi usuali  &#13;
 poteri di auto-organizzazione.                                           &#13;
     2. - La ricorrente, costituitasi  con  deduzioni  depositate  il  4  &#13;
 giugno, svolge con ulteriori argomenti i profili di illegittimità già  &#13;
 prospettati nell'ordinanza di rinvio e conclude chiedendo una pronuncia  &#13;
 di incostituzionalità delle norme denunciate.                           &#13;
     Alla  pubblica  udienza, la difesa della parte privata ha insistito  &#13;
 nelle già prese conclusioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - I giudizi promossi con le due ordinanze del Consiglio di Stato  &#13;
 hanno  ad  oggetto  un'unica   questione   e   vanno   perciò   decisi  &#13;
 congiuntamente.                                                          &#13;
     2.  - Le ordinanze denunciano il combinato disposto degli artt. 104  &#13;
 e 128 del d.P.R. 27 marzo 1969,  n.  130,  emanato  in  base  a  delega  &#13;
 contenuta  nella legge di riforma ospedaliera 12 febbraio 1968, n. 132,  &#13;
 nella parte in cui escludono che, in linea transitoria, i  posti  della  &#13;
 carriera  direttiva del personale amministrativo degli ospedali possano  &#13;
 venire assegnati mediante concorsi (in particolare,  concorsi  interni)  &#13;
 agli  impiegati  in  possesso  dei  requisiti  previsti dalla normativa  &#13;
 anteriore, ancorché sfornito del titolo di studio (diploma di  laurea)  &#13;
 ora  prescritto  dal detto decreto presidenziale. Poiché dal titolo di  &#13;
 studio si prescinde per il personale della carriera  esecutiva,  mentre  &#13;
 per  quello  amministrativo  di  tutte  le  carriere,  salvo  che della  &#13;
 carriera direttiva, sono istituiti appositi concorsi interni riservati,  &#13;
 risulterebbero violati l'art. 76,  in  relazione  all'art.  3,  nonché  &#13;
 (marginalmente e nello sfondo) gli artt. 4 e 97 della Costituzione.      &#13;
     3.  -  La  questione,  nei  termini, testé riassunti, in cui viene  &#13;
 proposta, non è fondata.                                                &#13;
     È da rilevare preliminarmente che la materia dei titoli di  studio  &#13;
 necessari   per   l'accesso   alle   varie   carriere   del   personale  &#13;
 amministrativo dei nuovi enti ospedalieri risulta regolata per  intero,  &#13;
 anche  quanto al loro assetto definitivo, dal decreto presidenziale del  &#13;
 1969, che, con riferimento al solo personale della carriera  esecutiva,  &#13;
 si  limita  peraltro  a  rinviare  ai  singoli  regolamenti  degli enti  &#13;
 medesimi (artt. 109 e 124). E dunque, poiché la legge di delega non si  &#13;
 occupa  espressamente  dell'argomento   né   contiene   in   proposito  &#13;
 disposizioni  aventi  più  limitato  riguardo  a  speciali  regimi  da  &#13;
 adottare  in  sede  di  prima  attuazione  del  nuovo  ordinamento,  la  &#13;
 particolare  disciplina  dettata,  in  linea  transitoria,  dal decreto  &#13;
 presidenziale, derogando al requisito  del  titolo  di  studio  per  il  &#13;
 personale  della  carriera esecutiva, non può dirsi contrastare con la  &#13;
 legge medesima e quindi con l'art. 76 della Costituzione.                &#13;
     Né l'obbligo di estendere quella deroga alle altre  categorie  del  &#13;
 personale amministrativo potrebbe trarsi dall'ultimo comma dell'art. 42  &#13;
 della  legge,  prescrivente  il  riconoscimento  "in  ogni  caso" delle  &#13;
 "posizioni giuridiche ed economiche" del personale  già  in  servizio,  &#13;
 non  rientrando  tra  queste,  secondo costante giurisprudenza, le mere  &#13;
 aspettative dei pubblici dipendenti, quali  possono  essere,  come  nel  &#13;
 caso,  le  più  o  meno  fondate  speranze  che gli stessi, in base ai  &#13;
 precedenti ordinamenti, fossero autorizzati a nutrire quanto ai  futuri  &#13;
 sviluppi delle rispettive carriere. Che tale sia il proprio significato  &#13;
 tecnico  della  formula  usata  nell'art.  42 e di quella, strettamente  &#13;
 analoga, dell'art.  59  (con  riguardo  alla  particolare  ipotesi  del  &#13;
 trasferimento  agli  enti  ospedalieri  del  personale  precedentemente  &#13;
 addetto ad  ospedali  di  enti  pubblici  perseguenti  anche  finalità  &#13;
 diverse  dall'assistenza ospedaliera), è confermato, d'altronde, dalla  &#13;
 circostanza che, quando, nella più recente legislazione, si è  voluto  &#13;
 assicurare   agli   impiegati  anche  il  mantenimento  delle  semplici  &#13;
 possibilità di carriera ad essi derivanti dalle norme anteriori, lo si  &#13;
 è detto espressamente, adoperando locuzioni dalle quali risulta  senza  &#13;
 possibilità  di dubbio trattarsi di un di più rispetto alle posizioni  &#13;
 qualificanti ormai il loro stato  giuridico  ed  economico.  Così,  ad  &#13;
 esempio,  l'art.  16  della  legge 18 marzo 1968, n. 249, contenente la  &#13;
 delega al  Governo  per  il  riordinamento  dell'amministrazione  dello  &#13;
 Stato,  nel  testo modificato dall'art. 12 della legge 28 ottobre 1970,  &#13;
 n. 775, nella lett. i, stabilisce che le norme transitorie garantiscano  &#13;
 ai  funzionari  già  in  servizio,  oltre  alla   "conservazione   dei  &#13;
 trattamenti  economici  e delle posizioni giuridiche conseguite", anche  &#13;
 "le attuali possibilità di carriera previste dalle norme in  vigore  e  &#13;
 dalle attuali dotazioni organiche".                                      &#13;
     La  deroga  al titolo di studio, in favore del solo personale della  &#13;
 carriera esecutiva, non contrasta, d'altro lato, con  il  principio  di  &#13;
 eguaglianza,  stante  la  radicale  diversità  di  mansioni  tra detto  &#13;
 personale  (costituito,  a  norma  dell'art.  39  della   legge,   "dai  &#13;
 portantini,  dal  personale  di cucina, pulizia, custodia e degli altri  &#13;
 servizi similari" e collocato al livello  iniziale  dell'organizzazione  &#13;
 amministrativa degli enti ospedalieri) e quello delle altre carriere, e  &#13;
 segnatamente,  per  quanto  ora  interessa,  di quella direttiva; ed in  &#13;
 considerazione altresì del fatto che, nei riguardi del  primo,  l'art.  &#13;
 128, primo comma, del decreto presidenziale non ha di mira promozioni o  &#13;
 progressioni di carriera, ma l'immissione nei ruoli degli enti medesimi  &#13;
 che  è  cosa diversa. Né argomenti in contrario senso sono desumibili  &#13;
 dalla disposizione dell'art. 126 del decreto in oggetto, che ammette  i  &#13;
 sanitari  di  ruolo  o  dichiarati idonei a partecipare direttamente ai  &#13;
 concorsi di assunzione presso ospedali di categoria pari  od  inferiore  &#13;
 prescindendo   dal  requisito  della  speciale  idoneità  (secondo  le  &#13;
 ipotesi: nazionale o regionale), prescritto,  di  norma,  dall'art.  43  &#13;
 della  legge per l'accesso a detti concorsi: sia perché l'analogia tra  &#13;
 titolo di studio e conseguita idoneità (che è titolo, per dir  così,  &#13;
 ulteriore   ed   aggiuntivo)   è   alquanto  remota,  sia  perché  la  &#13;
 disposizione  ora  menzionata  è  rivolta  ad  attuare  un   esplicito  &#13;
 principio  direttivo,  contenuto  nel  medesimo art. 43 e che non trova  &#13;
 riscontro, come si è  osservato,  in  alcun'altra  disposizione  della  &#13;
 legge n. 132.                                                            &#13;
     4.   -   Anche   quanto   al   secondo  profilo  di  illegittimità  &#13;
 costituzionale, per il diverso trattamento fatto per l'accesso ai posti  &#13;
 della carriera  direttiva  rispetto  a  quelli  delle  altre  carriere,  &#13;
 profilo  che  le  ordinanze  prospettano  per  inciso  e quasi in linea  &#13;
 subordinata (probabilmente perché,  di  per  sé  sola,  la  questione  &#13;
 sarebbe  stata,  nella  specie,  irrilevante),  valgono  considerazioni  &#13;
 analoghe a quelle sopra esposte, sia in ordine alla assenza nella legge  &#13;
 di delega di specifici principi e criteri direttivi sull'argomento  dei  &#13;
 concorsi  (aperti  ovvero  interni  o riservati); sia quanto al proprio  &#13;
 significato  tecnico  dell'ultimo  comma  dell'art.  42   (così   come  &#13;
 dell'art. 59, a sua volta anch'esso rammentato).                         &#13;
     In altri termini, stabilendo, in sede di prima attuazione del nuovo  &#13;
 ordinamento,  che  i  posti  di  ruolo disponibili nelle varie carriere  &#13;
 amministrative (ad  eccezione  di  quella  direttiva)  siano  assegnati  &#13;
 attraverso  concorsi  interni,  il  legislatore  delegato  non tanto ha  &#13;
 inteso dare attuazione a principi della  legge  di  delega,  cui  fosse  &#13;
 vincolato  a  conformarsi,  quanto  semplicemente  si è avvalso di una  &#13;
 facoltà discrezionale, nel quadro dei principi generali  sul  pubblico  &#13;
 impiego richiamati dall'art. 42, n. 2, della legge stessa ed in armonia  &#13;
 con   criteri   abitualmente   seguiti   nella  prassi  legislativa  ed  &#13;
 amministrativa nei casi di trasferimento di personale ad enti di  nuova  &#13;
 creazione  o  di  riordinamento dell'assetto organizzativo di enti già  &#13;
 operanti. Ciò che risulta confermato ulteriormente dallo  stesso  art.  &#13;
 128,  secondo comma, del decreto presidenziale, che, per le "modalità"  &#13;
 di espletamento dei concorsi interni, richiama, oltre alle disposizioni  &#13;
 in esso contenute, quelle stabilite "nei regolamenti dei singoli enti";  &#13;
 e non - come avrebbe  fatto,  ove  si  fosse  trattato  di  riconoscere  &#13;
 aspettative  di  carriera  - le modalità già previste dai regolamenti  &#13;
 degli ospedali, in vigore anteriormente  alla  istituzione  degli  enti  &#13;
 ospedalieri.                                                             &#13;
     Così  stando  le cose, è chiaro che, a maggior ragione ancora, il  &#13;
 legislatore  delegato  non  aveva  alcun  obbligo   di   estendere   la  &#13;
 facilitazione della riserva di concorsi interni, disposta per l'accesso  &#13;
 ai  posti delle carriere inferiori, alla diversa ipotesi dell'accesso a  &#13;
 posti della carriera direttiva.                                          &#13;
     Nemmeno è configurabile violazione dell'art. 3 Cost., sia  perché  &#13;
 la  qualità delle attribuzioni esplicate dai funzionari della carriera  &#13;
 direttiva impone che la selezione ne avvenga nei  modi  più  rigorosi,  &#13;
 nel  preminente interesse dell'amministrazione; sia perché, allorché,  &#13;
 come  nel  caso  degli  enti  ospedalieri, i posti direttivi da coprire  &#13;
 sono, per la  struttura  stessa  degli  enti,  in  numero  estremamente  &#13;
 limitato  (quando  pure  non  si riducano ad uno solo) ed altrettanto e  &#13;
 corrispondentemente limitato è il numero dei possibili aspiranti (fino  &#13;
 a ridursi a quel solo funzionario che già ne esercitasse, a titolo  di  &#13;
 incarico, le funzioni), un concorso interno finirebbe per non assolvere  &#13;
 più  alle  ben note esigenze di scelta e valutazione comparativa entro  &#13;
 un'ampia cerchia di candidati, che stanno alla base  dell'istituto  del  &#13;
 pubblico concorso.                                                       &#13;
     5.  -  Quanto si è venuto dicendo fin qui persuade al tempo stesso  &#13;
 che il combinato disposto degli artt. 104 e 128 del d.P.R. n.  130  del  &#13;
 1969,  laddove, in sede di prima attuazione dell'ordinamento degli enti  &#13;
 ospedalieri, tiene fermi,  per  la  carriera  direttiva  del  personale  &#13;
 amministrativo, il requisito del titolo di studio e la prescrizione del  &#13;
 pubblico concorso, non contrasta nemmeno con l'art. 97 Cost. (tendendo,  &#13;
 anzi,  nella  formazione dei ruoli organici secondo le varie necessità  &#13;
 del servizio, a dare  soddisfazione  all'esigenza  del  buon  andamento  &#13;
 della  amministrazione), né con il principio, risultante per implicito  &#13;
 dal secondo comma dell'art. 4 Cost., della libera  scelta  del  lavoro,  &#13;
 che  deve  ritenersi  inconferente  nella  specie  e che, del resto, le  &#13;
 ordinanze richiamano solo marginalmente e dubitativamente.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 del  combinato disposto degli artt. 104 e 128 del d.P.R. 27 marzo 1969,  &#13;
 n. 130 (sullo stato giuridico dei dipendenti ospedalieri), sollevata in  &#13;
 riferimento agli artt. 76,  3,  97  e  4  della  Costituzione,  con  le  &#13;
 ordinanze indicate in epigrafe.                                          &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 3 maggio 1974.                                &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - ANGELO DE MARCO - ERCOLE  ROCCHETTI  &#13;
                                   -  ENZO  CAPALOZZA - VINCENZO MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI.                        &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
