<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>83</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:83</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>01/03/2002</data_decisione>
    <data_deposito>21/03/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Valerio ONIDA, Carlo &#13;
MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 4-ter commi 2 e &#13;
3,  lettera  b) del decreto-legge 7 aprile 2000, n. 82 (Modificazioni &#13;
alla  disciplina  dei  termini  di  custodia cautelare nella fase del &#13;
giudizio  abbreviato),  convertito,  con  modificazioni,  dalla legge &#13;
5 giugno  2000,  n. 144, promosso con ordinanza emessa il 27 febbraio &#13;
2001  dalla  Corte  di  assise  di  appello di Lecce nel procedimento &#13;
penale  a  carico di G. D., iscritta al n. 491 del registro ordinanze &#13;
2001 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 26, 1a &#13;
serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 30 gennaio 2002 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto  che  la Corte di assise di appello di Lecce solleva, in &#13;
riferimento   all'art. 111,   secondo   comma,   della  Costituzione, &#13;
questione di legittimità costituzionale dell'art. 4-ter commi 2 e 3, &#13;
lettera b) del decreto-legge 7 aprile 2000, n. 82 (Modificazioni alla &#13;
disciplina  dei termini di custodia cautelare nella fase del giudizio &#13;
abbreviato),  convertito,  con  modificazioni,  dalla  legge 5 giugno &#13;
2000,  n. 144,  nella  parte  in  cui,  verificandosi  le  condizioni &#13;
previste  dalla  stessa  norma, consente all'imputato di chiedere, ai &#13;
fini previsti dall'art. 442, comma 2, del codice di procedura penale, &#13;
che   il  giudizio  sia  immediatamente  definito,  anche  quando  la &#13;
rinnovazione  del  dibattimento  in  appello  sia  stata  disposta su &#13;
richiesta del pubblico ministero; &#13;
        che  a  tal riguardo la Corte rimettente rileva come, essendo &#13;
stata  nella  specie  disposta  la  rinnovazione  del dibattimento su &#13;
richiesta  del  procuratore generale, l'accoglimento della domanda di &#13;
rito  abbreviato  formulata  dall'imputato  impedirebbe  l'assunzione &#13;
della  prova richiesta dalla controparte: sicché - deduce il giudice &#13;
a  quo  -  un  istituto  premiale,  che però implica la rinuncia per &#13;
l'imputato  ad  avvalersi delle facoltà processuali che gli spettano &#13;
in  dibattimento, finisce per risolversi in un "vantaggio", in quanto &#13;
da  esso  deriva per l'altra parte la preclusione all'esercizio di un &#13;
proprio  potere.  Conseguenza, questa, che la Corte rimettente reputa &#13;
"aberrante",   e   tale   da   alterare  "profondamente  l'equilibrio &#13;
processuale   fra  le  parti  e  la  posizione  di  parità  prevista &#13;
dall'art. 111, comma 2, della Costituzione"; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri, rappresentato e difeso dalla Avvocatura generale dello &#13;
Stato, chiedendo che la questione sia dichiarata non fondata. &#13;
    Considerato  che  questa  Corte,  nella sentenza n. 115 del 2001, &#13;
successiva  alla pronuncia della ordinanza di rimessione, ha reputato &#13;
non  irragionevole  ed in linea con il principio della parità tra le &#13;
parti   sancito   dall'art. 111,   secondo   comma,  Cost.,  l'omessa &#13;
previsione  -  quanto  al  giudizio  abbreviato  -  di  un  potere di &#13;
iniziativa  probatoria  del  pubblico  ministero,  analogo  a  quello &#13;
attribuito  all'imputato  che  abbia  presentato  richiesta  di  rito &#13;
abbreviato; &#13;
        che  tali  conclusioni  valgono evidentemente ad escludere, a &#13;
fortiori,  qualsiasi  dubbio  di  costituzionalità di una previsione &#13;
che,  come  quella  ora  censurata,  si  limita  a  consentire in via &#13;
transitoria  la  celebrazione  del  giudizio  alternativo in grado di &#13;
appello  -  ove  sia  stata disposta la rinnovazione della istruzione &#13;
dibattimentale   ai   sensi   dell'art. 603  del  codice  di  rito  - &#13;
precludendo, in capo ad entrambe le parti processuali, esclusivamente &#13;
la  nuova  attività probatoria, per di più non come dato normativo, &#13;
ma  come  conseguenza  "pratica"  che  deriva dalla natura stessa del &#13;
rito; &#13;
        che,   d'altra   parte,  sempre  sul  versante  del  giudizio &#13;
abbreviato,  questa  Corte  ha  anche  avuto  modo di ribadire che il &#13;
principio di parità tra accusa e difesa non comporta necessariamente &#13;
l'identità  tra i poteri processuali del pubblico ministero e quelli &#13;
dell'imputato,  giacché una diversità di trattamento può risultare &#13;
giustificata,  nei  limiti  della  ragionevolezza  -  senz'altro  non &#13;
superati  nella  ipotesi  di  specie  - sia dalla peculiare posizione &#13;
istituzionale  del pubblico ministero, sia dalla funzione allo stesso &#13;
affidata,  sia  da  esigenze  connesse  alla corretta amministrazione &#13;
della  giustizia.  Esigenze,  dunque,  fra  le  quali ben può essere &#13;
annoverata  anche  quella  di  pervenire  ad  "una  rapida e completa &#13;
definizione dei processi" (v. ordinanza n. 421 del 2001); &#13;
        che,  a  proposito  della  specifica  disposizione oggetto di &#13;
impugnativa, questa Corte ha già avuto modo di sottolineare la ratio &#13;
che  sta  al  fondo  della particolare ampiezza con la quale è stata &#13;
prevista  una sorta di "restituzione nel termine" per la proposizione &#13;
della  richiesta  di  giudizio abbreviato, nel caso di reati punibili &#13;
con  la  pena  dell'ergastolo (v. ordinanza n. 99 del 2001): sicché, &#13;
risultando   conforme   a  Costituzione  la  iscrivibilità,  in  via &#13;
transitoria,  del  rito  alternativo  anche  nel giudizio di appello, &#13;
restano  per ciò solo dissolti i dubbi di legittimità relativi agli &#13;
"effetti"  (mancata rinnovazione della istruzione dibattimentale) che &#13;
dalla celebrazione di quel rito conseguono; &#13;
        che,  pertanto,  la questione proposta deve essere dichiarata &#13;
manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale dell'art. 4-ter commi 2 e 3, lettera b), &#13;
del decreto-legge 7 aprile 2000, n. 82 (Modificazioni alla disciplina &#13;
dei   termini   di   custodia   cautelare  nella  fase  del  giudizio &#13;
abbreviato),  convertito,  con  modificazioni,  dalla  legge 5 giugno &#13;
2000,  n. 144, sollevata, in riferimento all'art. 111, secondo comma, &#13;
della  Costituzione,  dalla  Corte  di assise di appello di Lecce con &#13;
l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 1 marzo 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 21 marzo 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
