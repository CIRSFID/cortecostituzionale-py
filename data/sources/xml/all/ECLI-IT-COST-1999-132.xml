<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>132</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:132</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/04/1999</data_decisione>
    <data_deposito>16/04/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 142, terzo    &#13;
 comma, e 669-sexies secondo e terzo comma, del  codice  di  procedura    &#13;
 civile,  promosso  con ordinanza emessa il 29 aprile 1997 dal giudice    &#13;
 designato del tribunale di Nola nel procedimento civile vertente  tra    &#13;
 Cas.  di.  T.  S.r.l.  e  Midas  Corporation  Manifactures  ed altro,    &#13;
 iscritta al n. 728 del registro ordinanze  1998  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  41, prima serie speciale,    &#13;
 dell'anno 1998;                                                          &#13;
   Udito nella camera di  consiglio  del  24  marzo  1999  il  giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
   Ritenuto che il giudice designato del Tribunale di Nola - nel corso    &#13;
 dell'udienza  di  comparizione  delle  parti, fissata a seguito della    &#13;
 pronuncia ante  causam  con  decreto  inaudita  altera  parte  di  un    &#13;
 provvedimento  cautelare atipico nei confronti di una società avente    &#13;
 sede nella Corea del Sud - con ordinanza emessa il 29 aprile 1997  ha    &#13;
 sollevato,  in  riferimento  agli  artt.  3  e 24 della Costituzione,    &#13;
 questione di  legittimità  costituzionale  degli  artt.  142,  terzo    &#13;
 comma,  e  669-sexies  secondo e terzo comma, cod. proc. civ., "nella    &#13;
 parte in cui  non  prevedono  che  la  notificazione  all'estero  del    &#13;
 provvedimento  cautelare  concesso con decreto si perfezioni, ai fini    &#13;
 dell'osservanza del prescritto termine, con il tempestivo  compimento    &#13;
 delle   formalità   imposte   al   notificante   dalle   convenzioni    &#13;
 internazionali  e  dagli  artt. 30 e 75 del d.P.R. 5 gennaio 1967, n.    &#13;
 200":                                                                    &#13;
     che, secondo il rimettente, il  combinato  disposto  delle  norme    &#13;
 impugnate  impone  che la notificazione del decreto venga effettuata,    &#13;
 ove il destinatario sia residente all'estero, nel termine  perentorio    &#13;
 massimo  di  ventiquattro  giorni, entro il quale il notificante deve    &#13;
 inviare a mezzo Ufficiale giudiziario l'atto al Console italiano  del    &#13;
 paese  di  destinazione  e  questi deve provvedere alla notificazione    &#13;
 direttamente, ovvero mediante la collaborazione dello Stato ad quem;     &#13;
     che ciò comporterebbe un sacrificio del diritto di difesa ed una    &#13;
 disparità  di  trattamento  nei  beneficiari  di  un   provvedimento    &#13;
 cautelare concesso con decreto, stante la rilevante probabilità che,    &#13;
 nel  breve  termine sancito dall'art. 669-sexies non si perfezioni la    &#13;
 notificazione nello Stato estero (come in concreto verificatosi);        &#13;
     che, sempre  secondo  il  rimettente,  la  specifica  e  limitata    &#13;
 portata  della  sentenza n. 69 del 1994 - con cui è stata dichiarata    &#13;
 l'illegittimità costituzionale degli artt. 142,  terzo  comma,  143,    &#13;
 terzo  comma, e 680, primo comma, cod. proc. civ. "nella parte in cui    &#13;
 non prevedono  che  la  notificazione  all'estero  del  sequestro  si    &#13;
 perfezioni,  ai  fini  dell'osservanza del prescritto termine, con il    &#13;
 tempestivo compimento delle formalità imposte al  notificante  dalle    &#13;
 convenzioni internazionali e dagli artt. 30 e 75 del d.P.R. 5 gennaio    &#13;
 1967,  n.  200"  - non consente di risolvere la presente questione in    &#13;
 via interpretativa.                                                      &#13;
   Considerato che questa Corte, con  la  sentenza  n.  358  del  1996    &#13;
 (ignorata  dal  rimettente)  -  muovendo dalla premessa dell'avvenuto    &#13;
 ripristino,  a  seguito  della  citata  sentenza  n.  69  del   1994,    &#13;
 dell'operatività  del  principio  della  sempre possibile "scissione    &#13;
 soggettiva" fra il momento perfezionativo  per  la  parte  istante  e    &#13;
 quello  di  efficacia per il destinatario della notificazione di atti    &#13;
 al di fuori dal territorio della Repubblica -, ha già affermato  che    &#13;
 il  meccanismo  della  notifica  all'estero,  sotto  il  suo  aspetto    &#13;
 funzionale, è  stato  definitivamente  modificato  appunto  da  tale    &#13;
 declaratoria  di  incostituzionalità,  la  quale, per la sua valenza    &#13;
 generale,  "trascende  la  specifica  fattispecie  oggetto  di   quel    &#13;
 giudizio  e  coinvolge il complessivo sistema notificativo degli atti    &#13;
 processuali risultante dagli artt.  142  e  143  cod.  proc.    civ.,    &#13;
 delimitandone l'a'mbito di operatività, le modalità ed i momenti di    &#13;
 perfezionamento  a  seconda  dei soggetti coinvolti e, soprattutto, a    &#13;
 prescindere dal contenuto degli atti stessi";                            &#13;
     che alla stregua di tale affermazione,  da  ribadire  ancora  una    &#13;
 volta, la sollevata questione risulta manifestamente infondata.          &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  degli  artt. 142, terzo comma, e 669-sexies secondo e    &#13;
 terzo  comma,  del  codice  di  procedura  civile,  sollevata  -   in    &#13;
 riferimento  agli  artt.  3  e  24  della  Costituzione - dal giudice    &#13;
 designato  del  tribunale  di  Nola,  con  l'ordinanza  indicata   in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 aprile 1999.                               &#13;
                         Il Presidente: Granata                           &#13;
                         Il redattore: Ruperto                            &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 16 aprile 1999.                           &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
