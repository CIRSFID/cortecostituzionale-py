<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>402</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:402</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Giuseppe Tesauro</relatore_pronuncia>
    <redattore_pronuncia>Giuseppe Tesauro</redattore_pronuncia>
    <data_decisione>20/11/2006</data_decisione>
    <data_deposito>01/12/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>inammissibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 25 maggio 2005 relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Antonio Domenico Pasinato nei confronti di Stefano De Cecchi, promosso con ricorso del Giudice di pace di Bassano del Grappa, depositato in cancelleria il 25 maggio 2006 ed iscritto al n. 14 del registro conflitti tra poteri dello Stato 2006, fase di ammissibilità. &#13;
    Udito nella camera di consiglio del 25 ottobre 2006 il Giudice relatore Giuseppe Tesauro. &#13;
    Ritenuto che il Giudice di pace di Bassano del Grappa, con ordinanza pronunciata in udienza il 28 marzo 2006, ha promosso conflitto di attribuzione tra poteri dello Stato in relazione alla deliberazione adottata dal Senato della Repubblica nella seduta del 25 maggio 2005, secondo la quale i fatti per i quali è in corso un procedimento penale nei confronti del senatore Antonio Domenico Pasinato concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, ai sensi dell'articolo 68, primo comma, della Costituzione; &#13;
    che il Giudice di pace, «preso atto delle eccezioni svolte dalle parti, nonché dell'istanza di archiviazione proposta dalla difesa dell'imputato», riferisce di aver trasmesso, con ordinanza del 25 gennaio 2005, copia degli atti del giudizio innanzi ad esso pendente al Senato della Repubblica, a norma dell'art. 3, comma 4, della legge 20 giugno 2003, n. 140 (Disposizioni per l'attuazione dell'articolo 68 della Costituzione nonché in materia di processi penali nei confronti delle alte cariche dello Stato) e che, in data 30 maggio 2005, il Senato della Repubblica comunicava di aver approvato «la proposta della Giunta delle elezioni e delle immunità parlamentari di dichiarare il fatto, oggetto del procedimento de quo, concernente opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e ricadente pertanto nell'ipotesi di cui all'art. 68, I° co., della Costituzione»; &#13;
    che, ciò premesso, «solleva conflitto di attribuzione innanzi alla Corte costituzionale», ritenendo che non sussista alcun nesso tra le dichiarazioni oggetto di giudizio e l'attività funzionale del parlamentare, in quanto, come già indicato nell'ordinanza del 25 gennaio 2005, «l'opinione espressa dall'imputato il 30.12.2002 “extra moenia” è stata pronunciata durante una riunione del Consiglio comunale di Cassola, nell'esercizio delle sue funzioni di Sindaco-Presidente del Consiglio comunale». &#13;
    Considerato che, in questa fase del giudizio, la Corte è chiamata a delibare, ai sensi dell'art. 37, commi terzo e quarto, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), l'ammissibilità del ricorso, valutando, senza contraddittorio, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzione tra poteri dello Stato; &#13;
    che a tale fine non rileva la forma dell'ordinanza rivestita dall'atto introduttivo, bensì la sua rispondenza ai contenuti richiesti dall'art. 37 della legge n. 87 del 1953 e dall'art. 26 delle norme integrative per i giudizi davanti alla Corte costituzionale (sentenza n. 315 del 2006 e ordinanza n. 129 del 2005); &#13;
    che l'atto introduttivo, in quanto privo di ogni riferimento agli specifici fatti per cui si procede e alla loro qualificazione giuridica, non definisce la materia del conflitto (ordinanze n. 129 del 2005, n. 264 del 2000, n. 318 del 1999); &#13;
    che, invero, non sono neppure riportate le dichiarazioni del parlamentare interessato dalla deliberazione d'insindacabilità, il cui nominativo è peraltro desumibile solo dall'atto impugnato; &#13;
    che, a colmare la lacuna della mancata descrizione della fattispecie oggetto del giudizio penale, non possono soccorrere gli atti del procedimento irritualmente trasmessi, in quanto è nell'atto introduttivo e negli eventuali documenti ad esso allegati che devono essere rinvenuti gli elementi identificativi della causa petendi e del petitum (ordinanze n. 129 del 2005 e n. 140 del 2000); &#13;
    che le carenze formali e sostanziali sopra evidenziate impediscono di considerare l'ordinanza del Giudice di pace come un valido atto di promovimento di un conflitto di attribuzione tra poteri dello Stato; &#13;
    che, pertanto, il ricorso va dichiarato inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara inammissibile il ricorso per conflitto di attribuzione tra poteri dello Stato indicato in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 novembre 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giuseppe TESAURO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria l'1 dicembre 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
