<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>402</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:402</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/10/1999</data_decisione>
    <data_deposito>29/10/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art. 8, commi 5, 6 e    &#13;
 9, della legge 31 ottobre 1988, n. 480 (Modificazioni della normativa    &#13;
 relativa  al  fondo di previdenza per il personale di volo dipendente    &#13;
 da aziende di navigazione aerea) (recte: dell'art. 24, commi 5,  6  e    &#13;
 9,  della  legge  13  luglio 1965, n. 859 (Norme di previdenza per il    &#13;
 personale di volo dipendente da aziende di navigazione  aerea),  come    &#13;
 sostituito  dall'art.  8,  comma  1,  della legge 31 ottobre 1988, n.    &#13;
 480), promosso con ordinanza emessa il 14 gennaio 1998 dal pretore di    &#13;
 Busto Arsizio nel procedimento civile vertente tra  Oldra'  Marta  ed    &#13;
 altro  e  l'INPS,  iscritta  al  n. 317 del registro ordinanze 1998 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  19,  prima    &#13;
 serie speciale, dell'anno 1998.                                          &#13;
   Visti  gli  atti  di  costituzione  di  Oldra'  Marta  ed  altro  e    &#13;
 dell'INPS, nonché l'atto di intervento del Presidente del  Consiglio    &#13;
 dei Ministri;                                                            &#13;
   Udito  nell'udienza  pubblica del 6 luglio 1999 il giudice relatore    &#13;
 Fernando Santosuosso;                                                    &#13;
   Uditi gli avvocati Massimo  Luciani  per  Oldra'  Marta  ed  altro,    &#13;
 Vincenzo  Morielli per l'INPS e l'Avvocato dello Stato Giuseppe Stipo    &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ricorso  al  pretore  di Busto Arsizio, la vedova ed il    &#13;
 figlio superstiti di un pilota collaudatore, deceduto  per  causa  di    &#13;
 servizio,  convenivano  in  giudizio  l'INPS,  titolare  del Fondo di    &#13;
 previdenza per il personale di volo, chiedendo  che  la  pensione  di    &#13;
 reversibilità  loro  spettante fosse calcolata, ai sensi della legge    &#13;
 13 luglio 1965, n. 859 (Norme di previdenza per il personale di  volo    &#13;
 dipendente da aziende di navigazione aerea), sulla retribuzione media    &#13;
 effettivamente percepita dal loro dante causa negli anni 1988-1993, e    &#13;
 non su quella percepita nello stesso periodo dal personale dipendente    &#13;
 dall'azienda  di navigazione aerea maggiormente rappresentativa, come    &#13;
 previsto dalla legge 31 ottobre 1988,  n.  480  (Modificazioni  della    &#13;
 normativa  relativa  al  fondo di previdenza per il personale di volo    &#13;
 dipendente da aziende  di  navigazione  aerea)  che  ha  emendato  la    &#13;
 precedente legge n. 859.                                                 &#13;
   Il  pretore  ha  sollevato questione di legittimità costituzionale    &#13;
 dell'art. 8, commi 5, 6 e 9, della suddetta legge  n.  480  del  1988    &#13;
 (recte:  dell'art.  24, commi 5, 6 e 9, della citata legge n. 859 del    &#13;
 1965, come sostituiti dall'art. 8, comma 1, della legge  n.  480  del    &#13;
 1988), per contrasto con l'art. 3 della Costituzione.                    &#13;
   Il  giudice  a  quo innanzitutto, ritiene che la predetta questione    &#13;
 sia rilevante nel giudizio pretorile, in quanto ai ricorrenti  spetta    &#13;
 la  pensione di reversibilità, non più nella misura stabilita dalla    &#13;
 legge n. 859 del 1965 (nel testo previgente al 1988), ma nella misura    &#13;
 assai inferiore fissata dalle norme impugnate.                           &#13;
   La questione si presenterebbe poi non manifestamente infondata,  in    &#13;
 quanto  le  norme  impugnate,  in  presenza di situazioni differenti,    &#13;
 avrebbero introdotto un trattamento indifferenziato.                     &#13;
   Infatti, i piloti collaudatori, in considerazione dei gravi  rischi    &#13;
 che  corrono,  percepiscono una retribuzione decisamente più elevata    &#13;
 rispetto a quella dei piloti delle compagnie di navigazione aerea  e,    &#13;
 conseguentemente, sono assoggettati ad una contribuzione maggiore.       &#13;
   Eppure  la  legge, mentre pone quale base di calcolo per il computo    &#13;
 dei  contributi  la   retribuzione   effettivamente   percepita   dai    &#13;
 collaudatori,  stabilisce poi per la corresponsione della pensione un    &#13;
 limite, calcolato facendo riferimento alla media  delle  retribuzioni    &#13;
 percepite dalla diversa categoria dei piloti di linea.                   &#13;
   2.  -  Si  sono  costituiti  in  giudizio i ricorrenti nel giudizio    &#13;
 pretorile.                                                               &#13;
   Essi  sottolineano  che,  per  quanto  non  in  modo  espresso  nel    &#13;
 dispositivo   dell'ordinanza   di   rimessione,   risulterebbe  dalla    &#13;
 motivazione dell'ordinanza stessa che il giudice a  quo  chiede  alla    &#13;
 Corte  costituzionale  di eliminare la parificazione tra la categoria    &#13;
 dei piloti collaudatori e  quella  dei  piloti  di  voli  commerciali    &#13;
 dipendenti  dalla azienda nazionale di navigazione aerea maggiormente    &#13;
 rappresentativa, ripristinando  così  per  i  primi  il  trattamento    &#13;
 previdenziale  stabilito  dalla  legge  n.  859  del  1965  nel testo    &#13;
 previgente al 1988.                                                      &#13;
   Infatti, né le ragioni del contenimento della spesa pensionistica,    &#13;
 né  quelle  della  solidarietà  categoriale  potrebbero  sorreggere    &#13;
 "l'assurda  scelta  normativa"  di equiparare categorie assolutamente    &#13;
 non paragonabili, come i piloti dei  voli  commerciali  ed  i  piloti    &#13;
 collaudatori,  i  quali  ultimi  svolgono  un'attività  ad altissimo    &#13;
 rischio, che per questo  motivo  è  maggiormente  retribuita  ed  è    &#13;
 assoggettata a maggiori contribuzioni.                                   &#13;
   In  questo  modo  il  legislatore  avrebbe  imposto  "una  sorta di    &#13;
 solidarietà unidirezionale,  tutta  a  carico  della  categoria  dei    &#13;
 piloti  collaudatori,  senza  una ragione giustificativa". Infatti la    &#13;
 normativa  impugnata  non   fisserebbe   semplicemente   dei   limiti    &#13;
 pensionistici,  ma  imporrebbe  "l'astratta, artificiosa e arbitraria    &#13;
 equiparazione  fra  categorie  diverse  di  lavoratori,  con  diverse    &#13;
 attitudini,   diverse   abilità   e   soprattutto   diversi   rischi    &#13;
 professionali".                                                          &#13;
   3. - Si è costituito in giudizio l'INPS, titolare del Fondo  volo,    &#13;
 chiedendo che la questione sia dichiarata manifestamente infondata.      &#13;
   Secondo l'Istituto, il riferimento delle norme impugnate alla media    &#13;
 delle  retribuzioni  soggette  a  contributo implica che le eventuali    &#13;
 variazioni  di  retribuzioni  e  contribuzioni   concorrano   ad   un    &#13;
 trattamento  omologo:  si  tratterebbe, quindi, dell'applicazione del    &#13;
 principio solidaristico, che non vulnera il principio di uguaglianza.    &#13;
   4. - È intervenuto in giudizio il  Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, concludendo per l'inammissibilità o la manifesta infondatezza    &#13;
 della questione in oggetto.                                              &#13;
   Secondo la difesa erariale, la questione sollevata è  innanzitutto    &#13;
 inammissibile,  in  quanto  non  risulterebbe illustrata la posizione    &#13;
 giuridica ed economica del dante causa  dei  ricorrenti  in  rapporto    &#13;
 alla  normativa  precedente a quella impugnata, per cui vi sarebbe un    &#13;
 difetto di motivazione sulla rilevanza della questione.                  &#13;
   Nel merito, l'Avvocatura  dello  Stato  esclude  che  sussista  una    &#13;
 violazione   dell'art.  3  della  Costituzione,  poiché  nel  nostro    &#13;
 ordinamento   non   sarebbe   vietato   al   legislatore   modificare    &#13;
 sfavorevolmente i rapporti giuridici di durata, anche se l'oggetto di    &#13;
 questi  sia  costituito da diritti soggettivi perfetti.  Rientrerebbe    &#13;
 perciò nella discrezionalità legislativa  anche  la  determinazione    &#13;
 dell'ammontare  delle  prestazioni  sociali  e delle variazioni delle    &#13;
 stesse, al fine di contemperare le esigenze di  vita  dei  lavoratori    &#13;
 con le disponibilità finanziarie del bilancio pubblico.                 &#13;
   5.  -  Nell'imminenza dell'udienza di discussione, i ricorrenti nel    &#13;
 giudizio principale hanno depositato una memoria  illustrativa.    In    &#13;
 essa  contestano, innanzitutto, l'eccezione di inammissibilità della    &#13;
 questione di legittimità costituzionale,  sollevata  dall'Avvocatura    &#13;
 dello  Stato  e  relativa alla mancata esposizione, nell'ordinanza di    &#13;
 rimessione, della posizione del loro dante  causa  in  rapporto  alla    &#13;
 precedente normativa previdenziale di cui alla legge n. 859 del 1965.    &#13;
 Secondo  le  parti private, il giudice a quo dà atto che la pensione    &#13;
 di reversibilità per  cui  è  causa  andava  calcolata  sulla  base    &#13;
 dell'ultimo   quinquennio   di  attività  del  defunto  (1988-1993):    &#13;
 pertanto il trattamento pensionistico va determinato sulla base della    &#13;
 legge n. 480 del 1988, modificativa della precedente legge n. 859 del    &#13;
 1965, e non più del testo originario di quest'ultima legge,  la  cui    &#13;
 inapplicabilità  alla  causa  non  andava motivata, ma semplicemente    &#13;
 constatata.  Nel merito, le parti  private  contestano  le  deduzioni    &#13;
 della  difesa erariale, secondo cui non vi sarebbe alcuna disparità,    &#13;
 ma soltanto una diversità di trattamento  dei  collaudatori,  dovuta    &#13;
 all'incidenza  del  decorso  temporale.  Al contrario, il legislatore    &#13;
 avrebbe isolato la categoria dei piloti collaudatori, pregiudicandola    &#13;
 attraverso un trattamento omogeneo rispetto  a  quello  previsto  per    &#13;
 un'altra categoria del tutto differente.  La loro condizione sarebbe,    &#13;
 infatti, del tutto peculiare: dipendono non da imprese di navigazione    &#13;
 aerea,  ma  da imprese di costruzione aeronautica e vedono il proprio    &#13;
 rapporto di lavoro regolato da  uno  specifico  contratto  collettivo    &#13;
 (diverso  da  quello di tutti gli altri piloti), che ne identifica le    &#13;
 mansioni, radicalmente diverse da quelle dei comuni piloti.  La  più    &#13;
 alta   retribuzione   percepita   dai  collaudatori  non  sarebbe  un    &#13;
 privilegio, ma il giusto corrispettivo di una  attività  usurante  e    &#13;
 notevolmente   rischiosa:   tale  retribuzione,  secondo  i  principi    &#13;
 generali, deve avere un corrispondente trattamento pensionistico, che    &#13;
 può anche non coincidere perfettamente con la retribuzione,  ma  che    &#13;
 non  può essere limitato assumendo come parametro il limite previsto    &#13;
 per un'altra categoria.                                                  &#13;
   6. - Anche l'INPS  ha  depositato  una  memoria  a  sostegno  della    &#13;
 infondatezza   della   questione.     Secondo  l'Istituto,  la  Corte    &#13;
 costituzionale  avrebbe  già  chiarito  che   sono   affidati   alla    &#13;
 discrezionalità  del  legislatore  la  determinazione dell'ammontare    &#13;
 delle prestazioni previdenziali e le variazioni dei  trattamenti.  Il    &#13;
 raggiungimento  di  un  trattamento pensionistico massimo non sarebbe    &#13;
 invece un obiettivo costituzionalmente protetto, ma potrebbe soltanto    &#13;
 rientrare in linee di tendenza giustificate da situazioni peculiari e    &#13;
 particolari  (come  precisato  nella  sentenza  n.  459  del   1993).    &#13;
 Pertanto,  in  presenza  di  esigenze  di  contenimento  della  spesa    &#13;
 pubblica   e   nell'ottica   del    principio    solidaristico    che    &#13;
 contraddistingue  il  nostro  sistema  previdenziale,  il legislatore    &#13;
 potrebbe disciplinare diversamente le prestazioni  pensionistiche,  a    &#13;
 condizione  di  assicurare  un  trattamento  più  che decoroso: come    &#13;
 avverrebbe nel caso di  specie,  considerata  l'entità  media  delle    &#13;
 retribuzioni  prese  in  considerazione  ai fini della determinazione    &#13;
 della pensione dei collaudatori.                                         &#13;
   7. -  Nell'udienza  di  discussione  le  parti  hanno  ribadito  le    &#13;
 rispettive deduzioni.<diritto>Considerato in diritto</diritto>1.  -  Il  pretore  di  Busto  Arsizio  ha  sollevato  questione di    &#13;
 legittimità costituzionale dell'art. 24, commi 5, 6 e 9, della legge    &#13;
 13 luglio 1965, n. 859 (Norme di previdenza per il personale di  volo    &#13;
 dipendente   da   aziende  di  navigazione  aerea),  come  sostituito    &#13;
 dall'art.  8,  comma  1,  della  legge  31  ottobre  1988,   n.   480    &#13;
 (Modificazioni della normativa relativa al fondo di previdenza per il    &#13;
 personale  di  volo  dipendente da aziende di navigazione aerea), per    &#13;
 contrasto con l'art. 3 della Costituzione, avendo dette norme ridotto    &#13;
 il trattamento previdenziale spettante  ai  piloti  collaudatori  per    &#13;
 equipararlo a quello della differente categoria dei piloti di linea.     &#13;
   2.    -    Occorre   preliminarmente   esaminare   l'eccezione   di    &#13;
 inammissibilità formulata dall'Avvocatura dello  Stato,  secondo  la    &#13;
 quale  vi  sarebbe  un  difetto  di motivazione sulla rilevanza della    &#13;
 questione,  in  quanto  non  risulterebbe  illustrata  la   posizione    &#13;
 giuridica  ed  economica  del  dante  causa  dei  ricorrenti anche in    &#13;
 rapporto alla normativa precedente a quella impugnata.                   &#13;
   In realtà la rilevanza della  questione  risulta  sufficientemente    &#13;
 motivata.   Il  giudice  a  quo  ha  precisato  che  la  pensione  di    &#13;
 reversibilità per cui è causa va calcolata sulla  base  dell'ultimo    &#13;
 quinquennio  di  attività  del  defunto,  corrispondente  al periodo    &#13;
 1988-1993, e  che  pertanto  il  trattamento  pensionistico  dovrebbe    &#13;
 determinarsi ai sensi della legge n. 480 del 1988, modificativa della    &#13;
 precedente  legge n. 859 del 1965. E proprio le innovazioni apportate    &#13;
 dalla legge n. 480 del 1988 sono oggetto delle sollevate  censure  di    &#13;
 costituzionalità.                                                       &#13;
   3. - Nel merito, la questione è infondata.                            &#13;
   La  normativa  impugnata  stabilisce  dei  limiti  alle prestazioni    &#13;
 pensionistiche per tutti i piloti, siano essi dipendenti  da  aziende    &#13;
 di  navigazione  aerea  (i  piloti  di  linea)  ovvero  da aziende di    &#13;
 costruzione aeronautiche (i collaudatori): ai  fini  del  trattamento    &#13;
 previdenziale  risultano  perciò  equiparate  le  due  categorie  di    &#13;
 lavoratori, considerate unitariamente.                                   &#13;
   Come risulta dai lavori preparatori, il disegno di legge  n.  2573,    &#13;
 presentato dal Governo e poi tradotto, con modificazioni, nella legge    &#13;
 n.  480  del  1988, è stato originato dalla necessità di ridurre il    &#13;
 crescente deficit del  Fondo  di  previdenza  per  il  volo,  gestito    &#13;
 dall'INPS:      l'imposizione   di   un   limite   alle   prestazioni    &#13;
 pensionistiche per gli iscritti al Fondo  ne  rappresentava,  quindi,    &#13;
 l'elemento  principale,  chiaramente  perseguito.  L'equiparazione ai    &#13;
 fini  previdenziali  dei  collaudatori  con   i   piloti   di   linea    &#13;
 costituisce,   invece,   una   conseguenza  dell'approvazione  di  un    &#13;
 emendamento di iniziativa parlamentare, inteso soprattutto  a  fugare    &#13;
 ogni  remora all'accoglimento della richiesta dei primi di iscriversi    &#13;
 al Fondo  volo  (possibilità  a  lungo  negata  dall'INPS  e  infine    &#13;
 riconosciuta  dalla Corte di cassazione, anche in base alla normativa    &#13;
 previgente).                                                             &#13;
   Proprio  l'anzidetta  equiparazione,  con  i  conseguenti   effetti    &#13;
 pensionistici, è oggetto delle censure di costituzionalità, perché    &#13;
 ritenuta contrastante con l'art. 3 della Costituzione.                   &#13;
   4.  -  Va in generale ricordato che "il parametro della eguaglianza    &#13;
 non  esprime  la  concettualizzazione  di  una  categoria   astratta,    &#13;
 staticamente  elaborata  in funzione di un valore immanente dal quale    &#13;
 l'ordinamento non può prescindere,  ma  definisce  l'essenza  di  un    &#13;
 giudizio   di   relazione   che,   come   tale,   assume  un  risalto    &#13;
 necessariamente dinamico" (sentenza n. 89 del 1996). Invero qualunque    &#13;
 disciplina è destinata per sua stessa natura a stabilire  regole  e,    &#13;
 dunque,   ad  operare  assimilazioni  e  distinzioni;  per  cui  ogni    &#13;
 normativa positiva finisce per introdurre nell'ordinamento fattori di    &#13;
 omologazione  e  di  differenziazione  rispetto  alla   gamma   delle    &#13;
 possibili situazioni concrete.                                           &#13;
   In  particolare,  non  tutte  le  singole situazioni diverse devono    &#13;
 ricevere proprie differenti discipline, purché   l'unificazione  del    &#13;
 trattamento abbia una sua causa razionale. Né è consentito a questa    &#13;
 Corte   sostituirsi   alle     scelte  legislative  con  un  giudizio    &#13;
 sull'opportunità dell'omologazione o  della  differenziazione  delle    &#13;
 discipline.                                                              &#13;
   Se,  quindi,  non è censurabile, per contrasto con il principio di    &#13;
 uguaglianza,  qualsiasi  omologazione  di  disciplina  di  situazioni    &#13;
 diverse,  e  nemmeno  qualsiasi eventuale disarmonia o incoerenza che    &#13;
 una normativa possa, sotto alcuni profili o per  talune  conseguenze,    &#13;
 lasciar  trasparire, l'illegittimità è ravvisabile solo nel caso di    &#13;
 sicura carenza  di  una  adeguata  giustificazione  della  disciplina    &#13;
 introdotta.                                                              &#13;
   5.   -   Tale   carenza   di  causa  unificatrice  del  trattamento    &#13;
 previdenziale non sussiste nella specie.  Senza  entrare  nel  merito    &#13;
 delle  differenze di attività, di rischio e di trattamento economico    &#13;
 esistenti tra i collaudatori ed i piloti di linea,  va  rilevato  che    &#13;
 non  è  precluso  al  legislatore,  nell'ambito  di un esercizio non    &#13;
 irragionevole della sua discrezionalità, parificare  ai  fini  delle    &#13;
 prestazioni  pensionistiche i lavoratori iscritti ad uno stesso fondo    &#13;
 previdenziale che rivestano qualifiche assimilabili.                     &#13;
   Le peculiarità  delle  varie  attività  svolte  da  alcuni  degli    &#13;
 appartenenti  al  fondo  possono  riverberarsi  sulla  disciplina del    &#13;
 rapporto di lavoro  e  tradursi  eventualmente  nella  previsione  di    &#13;
 specifiche   coperture   assicurative,  secondo  quanto  risulta  dai    &#13;
 contratti collettivi di lavoro. Ma queste peculiarità non sono  tali    &#13;
 da rendere irragionevole                                                 &#13;
  l'omologazione del trattamento pensionistico.                           &#13;
   Non è dunque arbitraria l'equiparazione degli iscritti al medesimo    &#13;
 fondo di previdenza che siano conduttori di aeromobili, anche perché    &#13;
 -  dato  l'esiguo numero dei collaudatori - non era congruo creare un    &#13;
 apposito  fondo  che  risultasse  finanziariamente   autosufficiente.    &#13;
 Altrettanto  ragionevole  è  che  il  limite,  che il legislatore ha    &#13;
 voluto  imporre  alle  prestazioni  pensionistiche   ricevute   dagli    &#13;
 iscritti,  sia  calcolato sulla base della retribuzione percepita dai    &#13;
 lavoratori appartenenti a quella tra le due categorie con il  maggior    &#13;
 numero  di  addetti  (i  piloti  di  linea) e, all'interno di questa,    &#13;
 facendo riferimento ai dipendenti dell'azienda di  navigazione  aerea    &#13;
 maggiormente  rappresentativa,  secondo le loro diverse qualifiche, a    &#13;
 cui sono state equiparate,  con  apposite  tabelle  di  equipollenza,    &#13;
 quelle corrispondenti dei collaudatori.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 24, commi 5, 6 e 9, della legge  13  luglio  1965,  n.  859    &#13;
 (Norme  di  previdenza per il personale di volo dipendente da aziende    &#13;
 di navigazione aerea), come sostituito dall'art. 8,  comma  1,  della    &#13;
 legge 31 ottobre 1988, n. 480 (Modificazioni della normativa relativa    &#13;
 al fondo di previdenza per il personale di volo dipendente da aziende    &#13;
 di  navigazione  aerea),  sollevata,  in riferimento all'art. 3 della    &#13;
 Costituzione, dal pretore di Busto Arsizio con  l'ordinanza  indicata    &#13;
 in epigrafe.                                                             &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 25 ottobre 1999.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 29 ottobre 1999.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
