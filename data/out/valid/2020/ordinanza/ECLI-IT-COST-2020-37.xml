<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-27/37/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-27/37"/>
          <FRBRalias value="ECLI:IT:COST:2020:37" name="ECLI"/>
          <FRBRdate date="2020-02-27" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="37"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-27/37/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-27/37/ita@"/>
          <FRBRdate date="2020-02-27" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-27/37/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-02-27/37/ita@.xml"/>
          <FRBRdate date="2020-02-27" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-02-10" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-02-27" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>ammissibilità intervento</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>37</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:37</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Francesco Viganò</akn4cc:relatore>
        <akn4cc:data_decisione>2020-02-10</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-02-27</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>ammissibilità intervento</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/1948-02-08/47/!main#art_13">art. 13 della legge 8 febbraio 1948, n. 47</ref> (Disposizioni sulla stampa) e dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_595__para_3">art. 595, comma 3, del codice penale</ref>, promosso dal Tribunale ordinario di Salerno, sezione seconda penale, nel procedimento penale a carico di P. N. e A. S., con ordinanza del 9 aprile 2019, iscritta al n. 140 del registro ordinanze 2019 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2019.
 Visti l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> di P. N., nonché gli atti di intervento del Consiglio nazionale dell'ordine dei giornalisti (CNOG) e del Presidente del Consiglio dei ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>vista l'istanza di fissazione della camera di consiglio per la decisione sull'ammissibilità dell'intervento depositata dal CNOG;
 udito nella camera di consiglio del 29 gennaio 2020 il Giudice relatore Francesco Viganò;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>deliberato nella camera di consiglio del 10 febbraio 2020.
 Ritenuto che il Tribunale ordinario di Salerno, sezione seconda penale, con ordinanza del 9 aprile 2019 ha sollevato, in riferimento all'art. 117, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione all'art. 10 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), nonché agli artt. 3, 21, 25 e 27, terzo comma, Cost., questioni di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/1948-02-08/47/!main#art_13">art. 13 della legge 8 febbraio 1948, n. 47</ref> (Disposizioni sulla stampa) e dell'art. 595, terzo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che il rimettente espone di essere chiamato a giudicare della responsabilità penale di P. N., giornalista, e di A. S., direttore responsabile del giornale, imputati il primo di diffamazione a mezzo stampa compiuta mediante l'attribuzione alla persona offesa di un fatto determinato (<ref href="/akn/it/act/legge/stato/1948/47/!main#art_13">artt. 13 della legge n. 47 del 1948</ref> e 595, terzo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>.), e il secondo di omesso controllo sul contenuto del periodico (<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_57">art. 57 cod. pen.</ref>, in relazione ai medesimi delitti attribuiti al giornalista);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, ad avviso del rimettente, la previsione di una pena detentiva (obbligatoria nell'ipotesi di cui all'<ref href="/akn/it/act/legge/stato/1948/47/!main#art_13">art. 13 della legge n. 47 del 1948</ref>, e alternativa nell'ipotesi di cui all'art. 595, terzo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>.) si porrebbe in contrasto con i parametri costituzionali e convenzionali sopra indicati;
 che con atto depositato l'8 ottobre 2019 il Consiglio nazionale dell'ordine dei giornalisti (CNOG) è intervenuto in giudizio ad adiuvandum, ai sensi dell'art. 4 delle Norme integrative per i giudizi davanti alla Corte costituzionale, chiedendo alla Corte di dichiarare ammissibile l'intervento e di accogliere le questioni di legittimità costituzionale sollevate dal rimettente;
 che, quanto alla propria legittimazione a intervenire, il CNOG richiama la giurisprudenza di questa Corte sull'ammissibilità dell'intervento di ordini professionali nei giudizi di legittimità costituzionale concernenti norme relative ai diritti e doveri del professionista (sono citate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/180/!main">sentenze n. 180 del 2018</ref>, n. 171 del 1996 e n. 456 del 1993, nonché le <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2007/250/!main">ordinanze n. 250 del 2007</ref>, n. 389 e n. 50 del 2004);
 che l'interveniente sottolinea altresì che il CNOG, oltre a esercitare poteri di autogoverno dei giornalisti e di coordinamento delle attività promozionali per il miglioramento, aggiornamento e perfezionamento professionale, esprimerebbe pareri su progetti di legge e di regolamento concernenti la professione giornalistica e da tempo avrebbe orientato la propria azione politica e culturale alla denuncia delle criticità della disciplina sanzionatoria della diffamazione a mezzo stampa, risultante dagli artt. 595, terzo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>. e 13 della <ref href="/akn/it/act/legge/stato/1948/47/!main">legge n. 47 del 1948</ref>, attraverso diverse audizioni parlamentari e l'organizzazione e partecipazione a convegni dedicati a detta tematica;
 che il CNOG costituirebbe dunque organismo rappresentativo del relativo ordine, preposto alla tutela di «tutti gli interessi pubblici, oggettivamente immanenti, della categoria professionale», risultando così portatore di un interesse qualificato, immediatamente inerente al rapporto sostanziale dedotto in giudizio;
 che l'interesse qualificato dell'interveniente - strettamente connesso a quello della parte privata, ma riverberantesi su tutte le posizioni giuridiche individuali che, tramite le diverse attività dell'ente, trovano garanzia e tutela - consisterebbe in «un interesse, pur collettivo e superindividuale, diretto, attuale e concreto al rispetto della finalità della libertà di stampa ex <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_21">art. 21 della Costituzione</ref>», che comporterebbe la «legittimazione a veder eliminata la menomazione di un diritto fondamentale derivante da una norma incostituzionale che si riferisce alla sua "sfera di competenza"», atteso che «un'eventuale pronuncia di accoglimento delle questioni di legittimità costituzionale [...] produrrebbe necessariamente un'immediata incidenza sulla posizione soggettiva del Consiglio Nazionale dell'Ordine dei Giornalisti, ente rappresentativo degli interessi della categoria dei giornalisti italiani»;
 che con istanza depositata il 17 dicembre 2019 il CNOG ha chiesto a questa Corte di essere autorizzato alla consultazione integrale del fascicolo di causa.
 Considerato che l'intervento nel giudizio costituzionale di soggetti diversi dalle parti del giudizio a quo è regolato dagli artt. 4 e 4-bis delle Norme integrative per i giudizi davanti alla Corte costituzionale, come modificati dall'art. 1 della delibera della stessa Corte in sede non giurisdizionale dell'8 gennaio 2020, pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, serie generale, del 22 gennaio 2020, entrata in vigore il giorno successivo alla suddetta pubblicazione ai sensi dell'art. 8 della medesima delibera, con effetto immediato anche nei giudizi in corso;
 che, ai sensi dell'art. 4-bis delle Norme integrative, l'interveniente può chiedere di prendere visione e trarre copia degli atti processuali, dopo che la Corte - con deliberazione da assumere in camera di consiglio prima dell'udienza pubblica - abbia dichiarato ammissibile il suo intervento;
 che il Consiglio nazionale dell'ordine dei giornalisti (CNOG) ha chiesto a questa Corte di dichiarare ammissibile il proprio intervento e, conseguentemente, di essere autorizzato a prendere visione degli atti processuali e a trarne copia;
 che l'art. 4, comma 7, delle Norme integrative stabilisce che «[n]ei giudizi in via incidentale possono intervenire i titolari di un interesse qualificato, inerente in modo diretto e immediato al rapporto dedotto in giudizio»;
 che tale disposizione recepisce la costante giurisprudenza di questa Corte in merito all'ammissibilità dell'intervento nei giudizi in via incidentale di soggetti diversi dalle parti del giudizio a quo, dal Presidente del Consiglio dei ministri e dal Presidente della Giunta regionale (ex plurimis, ordinanza letta all'udienza del 4 giugno 2019, allegata alla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/206/!main">sentenza n. 206 del 2019</ref>, e ordinanza letta all'udienza del 18 giugno 2019, allegata alla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/173/!main">sentenza n. 173 del 2019</ref>; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2019/204/!main">ordinanza n. 204 del 2019</ref>);
 che, in base a tale giurisprudenza, l'incidenza sulla posizione soggettiva dell'interveniente deve derivare dall'immediato effetto che tale pronuncia produce sul rapporto sostanziale oggetto del giudizio a quo (ex plurimis, ordinanza letta all'udienza del 22 ottobre 2019, allegata alla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/253/!main">sentenza n. 253 del 2019</ref>);
 che, rispetto all'istanza ora all'esame, non è pertanto sufficiente a legittimare l'intervento la posizione di rappresentanza istituzionale degli interessi della professione giornalistica rivestita dal CNOG;
 che tale posizione era stata valorizzata, invero, da alcune ormai risalenti decisioni di questa Corte a fondamento dell'ammissibilità dell'intervento in giudizio, rispettivamente, della Federazione nazionale degli ordini dei medici chirurghi e degli odontoiatri (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1993/456/!main">sentenza n. 456 del 1993</ref>) e del Consiglio nazionale forense (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1996/171/!main">sentenza n. 171 del 1996</ref>), ma è stata in seguito sempre giudicata di per sé inidonea a legittimare l'intervento di ordini professionali (ordinanza letta all'udienza del 18 giugno 2019, allegata alla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/173/!main">sentenza n. 173 del 2019</ref>; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2013/156/!main">ordinanza n. 156 del 2013</ref>; ordinanza letta all'udienza del 23 ottobre 2012, allegata alla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2012/272/!main">sentenza n. 272 del 2012</ref>, tutte riferite allo stesso Consiglio nazionale forense);
 che tale soluzione deve essere oggi ribadita, tanto più a fronte della recente introduzione dell'art. 4-ter delle Norme integrative, che consente alle formazioni sociali senza scopo di lucro e ai soggetti istituzionali «portatori di interessi collettivi o diffusi attinenti alla questione di costituzionalità» di presentare alla Corte un'opinione scritta in qualità di amici curiae;
 che neppure valgono ex se a legittimare l'intervento le funzioni di autogoverno e promozione del miglioramento, aggiornamento e perfezionamento della professione giornalistica svolte dal CNOG, né - ancora - il suo generico interesse a «veder eliminata la menomazione di un diritto fondamentale derivante da una norma incostituzionale che si riferisce alla sua "sfera di competenza"», in difetto di un nesso qualificato con lo specifico rapporto sostanziale dedotto nel giudizio a quo, che solo può legittimare l'intervento del terzo (si vedano, a contrariis, la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/180/!main">sentenza n. 180 del 2018</ref>, che ammette l'intervento dell'Unione camere penali italiane in quanto autrice del codice di autoregolamentazione che veniva in rilievo nel giudizio a quo; l'ordinanza letta all'udienza del 22 settembre 2015, allegata all'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2015/200/!main">ordinanza n. 200 del 2015</ref>, che ammette l'intervento dell'Ordine degli avvocati di Pinerolo perché dall'accoglimento della questione dipendeva la stessa sopravvivenza di quell'ordine provinciale, destinato ad essere soppresso unitamente al Tribunale ordinario di Pinerolo; nonché l'ordinanza letta all'udienza del 25 novembre 2003, allegata all'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2004/50/!main">ordinanza n. 50 del 2004</ref>, che ammette l'intervento del Comitato olimpico nazionale italiano, dal momento che la norma oggetto del giudizio di costituzionalità disponeva la destinazione allo stesso CONI di aliquote dell'imposta unica sulle scommesse);
 che, tuttavia, un nesso con lo specifico rapporto giuridico dedotto in giudizio sussiste, nella specie, in relazione alla competenza disciplinare attribuita al CNOG dalla <ref href="/akn/it/act/legge/stato/1963-02-03/69/!main">legge 3 febbraio 1963, n. 69</ref> (Ordinamento della professione di giornalista);
 che, infatti, l'art. 20, primo comma, lettera d), di tale legge attribuisce al CNOG la competenza a decidere sui ricorsi in materia disciplinare;
 che, d'altra parte, l'<ref href="/akn/it/act/legge/stato/1963/69/!main#art_39">art. 39 della legge n. 69 del 1963</ref> dispone che le condanne penali comportanti l'interdizione dai pubblici uffici determinano ipso iure la cancellazione o la sospensione del giornalista dall'albo, mentre in ogni altro caso di condanna penale è previsto che il CNOG inizi l'azione disciplinare, ove sussistano le condizioni di cui al successivo art. 48, primo comma (e cioè ove il fatto offenda il decoro e la dignità professionali, ovvero comprometta la reputazione del giornalista o la dignità dell'ordine);
 che, pertanto, dall'eventuale condanna penale e dalla sua gravità, a carico del giornalista e del direttore responsabile imputati nel procedimento a quo, deriverebbero specifiche conseguenze in ordine all'avvio dell'azione disciplinare nei confronti degli imputati P. N. e A. S.;
 che per tale ragione l'intervento in giudizio del CNOG deve ritenersi ammissibile;
 che, conseguentemente, il CNOG deve essere autorizzato a prendere visione e trarre copia degli atti processuali del presente giudizio.
 Visto l'art. 4-bis delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara ammissibile l'intervento in giudizio del Consiglio nazionale dell'ordine dei giornalisti;
 autorizza il Consiglio nazionale dell'ordine dei giornalisti a prendere visione e trarre copia degli atti processuali del presente giudizio.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-02-10">10 febbraio 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#francescoViganò" as="#relatore">Francesco  VIGANÒ</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-02-27">27 febbraio 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
