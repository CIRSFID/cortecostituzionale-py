<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>83</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:83</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/04/1982</data_decisione>
    <data_deposito>29/04/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, lett.  a  &#13;
 e  b,  2,  3  e  8 della legge della Regione Lazio 2 luglio 1974, n. 30  &#13;
 (vincoli di inedificabilità)  promosso  con  ordinanza  emessa  il  14  &#13;
 gennaio  1976 dal Pretore di Minturno, nel procedimento penale a carico  &#13;
 di De Luca Vincenzo, iscritta al n. 157 del registro ordinanze  1976  e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 85 del 31 marzo  &#13;
 1976.                                                                    &#13;
     Visto l'atto di intervento della Regione Lazio;                      &#13;
     udito  nell'udienza  pubblica  del  24  febbraio  1982  il  Giudice  &#13;
 relatore Francesco Saja;                                                 &#13;
     udito l'avv. Guido Cervati, per la Regione Lazio.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso di un procedimento penale a carico di De  Luca  Vincenzo,  &#13;
 imputato  di  contravvenzione  di cui all'art. 41 l.  17 agosto 1942 n.  &#13;
 1150, per avere iniziato la costruzione di un  edificio  dopo  che,  ai  &#13;
 sensi  della  l.  della  Regione  Lazio  2  luglio  1974  n. 30, doveva  &#13;
 ritenersi   decaduta  la  relativa  licenza  edilizia,  il  Pretore  di  &#13;
 Minturno,  con  ordinanza  14  gennaio  1976,  sollevava  questione  di  &#13;
 legittimità  costituzionale del combinato disposto degli artt. 1 lett.  &#13;
 a) e b), 2, 3 e 8 della citata legge, poi  prorogata  con  altra  legge  &#13;
 regionale  31  dicembre  1974  n.  73, in relazione agli artt. 3 e 117,  &#13;
 primo comma, Cost.                                                       &#13;
     La legge regionale n. 30 del 1974 disponeva, tra l'altro, che nelle  &#13;
 zone costiere, marine e lacustri, di determinata ampiezza, e in  alcuni  &#13;
 comuni   con  un  determinato  incremento  demografico  (art.  1),  non  &#13;
 potessero eseguirsi nuove costruzioni "fino all'entrata in vigore della  &#13;
 legge regionale di approvazione del piano territoriale di coordinamento  &#13;
 regionale o di stralci del medesimo e comunque non oltre il termine del  &#13;
 31 dicembre 1974" (artt. 2 e 3; il termine venne poi prorogato dalla l.  &#13;
 31 dicembre 1974 n. 73 fino al 31 dicembre 1975). L'art. 8 prevedeva la  &#13;
 decadenza delle autorizzazioni a costruire, già rilasciate nelle dette  &#13;
 zone.                                                                    &#13;
     Il Pretore prospetta il contrasto  tra  le  norme  impugnate  ed  i  &#13;
 principi fondamentali espressi nella legislazione statale.               &#13;
     Uno  dei  principi  fondamentali  è, ad avviso del Pretore, quello  &#13;
 secondo cui le misure  previste  dalla  cit.  legge  regionale  possono  &#13;
 essere disposte solo da strumenti urbanistici.                           &#13;
     Altro  principio  fondamentale  è,  ad  avviso del Pretore, quello  &#13;
 secondo cui le misure urbanistiche di salvaguardia previste dalla legge  &#13;
 statale 3 novembre 1952 n. 1902, poi modificata dalle leggi 21 dicembre  &#13;
 1955 n. 1357, 30 luglio 1959 n. 615, 5 luglio 1966  n.  517,  6  agosto  &#13;
 1967 n. 765 (art.  3) e 19 novembre 1968 n. 1187 - comportano bensì il  &#13;
 dovere  del Sindaco di non rilasciare licenze edilizie in contrasto col  &#13;
 piano adottato, ma non  comportano  la  decadenza  delle  licenze  già  &#13;
 rilasciate.  Né potrebbero darsi misure di salvaguardia senza un piano  &#13;
 urbanistico approvato (rectius: adottato). Con questo principio  sembra  &#13;
 al  Pretore  contrastare  la  normativa  impugnata,  che  stabilisce la  &#13;
 decadenza delle licenze edilizie fin dal momento della sua  entrata  in  &#13;
 vigore,  senza  alcun  riguardo  al  momento  di approvazione (rectius:  &#13;
 adozione)  del  piano  territoriale  di  coordinamento   regionale   da  &#13;
 salvaguardare.                                                           &#13;
     Ancora,  secondo  il  Pretore,  la decadenza delle licenze edilizie  &#13;
 disposta nella normativa impugnata  sembra  contrastare  col  principio  &#13;
 (espresso  dall'art.  10  l.  6  agosto  1967  n.  765)  secondo cui la  &#13;
 decadenza della licenza edilizia può conseguire  soltanto  al  mancato  &#13;
 tempestivo   inizio  dei  lavori  ovvero  a  sopravvenute  contrastanti  &#13;
 previsioni del piano regolatore.                                         &#13;
     L'ordinanza,  regolarmente  notificata  e  comunicata,   è   stata  &#13;
 pubblicata nella G.U. n. 85 del 31 marzo 1976.                           &#13;
     È intervenuta la Regione Lazio per sostenere la infondatezza delle  &#13;
 questioni poste nell'ordinanza di rimessione.                            &#13;
     L'interveniente  osserva  non  essere vero che, per principio della  &#13;
 legislazione statale, le misure di salvaguardia possono consistere solo  &#13;
 nel divieto di rilasciare nuove licenze edilizie,  e  non  anche  nello  &#13;
 stabilire  la  decadenza di licenze già rilasciate. E invero, la legge  &#13;
 n. 1902 del 1952 attribuisce al prefetto il  potere  di  sospendere  in  &#13;
 salvaguardia  i lavori edilizi che compromettano o rendano più onerosa  &#13;
 l'attuazione del piano non ancora approvato (oggi,  poi,  detto  potere  &#13;
 prefettizio   compete  alla  Regione),  ciò  che  corrisponde  ad  una  &#13;
 sostanziale   previsione   di  decadenza  delle  licenze.  Nota  ancora  &#13;
 l'interveniente essere bensì vero che, a norma dell'art. 10 l. n.  765  &#13;
 del  1967,  le licenze edilizie possono decadere per il sopravvenire di  &#13;
 contrastanti  "previsioni  urbanistiche";  ma  queste  possono   essere  &#13;
 costituite da leggi regionali di salvaguardia.                           &#13;
     Nega,  comunque,  l'interveniente  che  le norme della legislazione  &#13;
 statale richiamate dal Pretore formulino principi fondamentali,  capaci  &#13;
 di limitare l'autonomia legislativa regionale.                           &#13;
     Questi  argomenti  vengono  sostanzialmente  ripetuti dalla Regione  &#13;
 nella memoria depositata in data 11 febbraio 1982.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. -  Con  l'ordinanza  in  epigrafe  il  Pretore  di  Minturno  ha  &#13;
 impugnato gli artt. 1 lett. a) e b), 2, 3 e 8 della legge della Regione  &#13;
 Lazio  2  luglio 1974 n. 30 (l'efficacia di essa è stata prorogata con  &#13;
 la  legge  reg.  31  dicembre  1974  n.  73),  la  quale,  al  fine  di  &#13;
 salvaguardare  l'attuazione  di  un piano territoriale di coordinamento  &#13;
 regionale non ancora approvato, vieta sino alla  data  di  approvazione  &#13;
 del   piano   stesso  l'edificazione  nei  territori  costieri  di  una  &#13;
 determinata profondità (m. 300 per il mare e m. 150  per  i  laghi)  e  &#13;
 stabilisce  la  decadenza  delle  licenze  edilizie rilasciate per quei  &#13;
 territori, salvo che i lavori siano stati iniziati e vengano completati  &#13;
 entro  due  anni.  Il  giudice  a   quo   dubita   della   legittimità  &#13;
 costituzionale di detta legge regionale, perché essa, non attenendosi,  &#13;
 secondo  il  suo  giudizio, ai principi fondamentali della legislazione  &#13;
 statale in subiecta materia, sarebbe in contrasto con l'art. 117, primo  &#13;
 comma, della Costituzione. Nella ordinanza di  rimessione  è  indicata  &#13;
 quale norma di riferimento anche l'art. 3 Cost.; si tratta però di una  &#13;
 generica  indicazione  non  sorretta da alcun rilievo specifico e fatta  &#13;
 evidentemente in via conseguenziale alla ritenuta violazione  dell'art.  &#13;
 117,  primo comma, della Costituzione; l'esame va pertanto circoscritto  &#13;
 a tale ultima norma.                                                     &#13;
     2. - Il primo principio fondamentale violato  sarebbe,  secondo  il  &#13;
 giudice  a  quo,  quello  per  cui le misure di salvaguardia potrebbero  &#13;
 essere previste soltanto  da  strumenti  urbanistici,  sicché  sarebbe  &#13;
 illegittima la previsione effettuata direttamente dalla legge.           &#13;
     L'argomentazione non può essere condivisa.                          &#13;
     Le  misure  di  salvaguardia  sono  dirette  all'attuazione  di uno  &#13;
 strumento urbanistico ancor prima dell'approvazione di esso  e  servono  &#13;
 ad evitare che lo stato dei luoghi venga medio tempore pregiudicato con  &#13;
 costruzioni   incompatibili   con  quelle  che  saranno  le  previsioni  &#13;
 urbanistiche e risulti così successivamcnte impossibile  ovvero  anche  &#13;
 difficile   la  attuazione  medesima;  si  tratta,  cioè,  di  effetti  &#13;
 prodromici  di  uno  strumento  che  sarà  successivamente  approvato,  &#13;
 effetti  che  la  legge  consente  che  si  producano,  stabilendone  i  &#13;
 presupposti e le modalità.                                              &#13;
     Risulta pertanto  evidente  come  le  misure  di  salvaguardia  non  &#13;
 possano   essere   contenute  negli  strumenti  urbanistici  a  cui  si  &#13;
 riferiscono, ma debbano precedere  detti  strumenti,  alla  cui  futura  &#13;
 attuazione  sono  preordinate. Ed appunto l'ordinamento statale conosce  &#13;
 alternativamente  due  modalità,  a  seconda  che  la  legge  provveda  &#13;
 direttamente  ovvero  autorizzi i pubblici poteri a provvedere con atti  &#13;
 amministrativi: la prima soluzione è accolta ad  esempio  dalla  legge  &#13;
 statale   4  novembre  1963  n.  1460  per  l'incremento  dell'edilizia  &#13;
 economica e popolare nonché dall'altra legge statale 20 marzo 1965  n.  &#13;
 217  sui  programmi  edilizi  della  GESCAL  e degli altri enti, mentre  &#13;
 l'altra soluzione è accolta dalla fondamentale legge 3  novembre  1952  &#13;
 n.  1902,  modificata dall'art. 3, ultimo comma, della legge n. 765 del  &#13;
 1967. Risulta evidente quindi come non  esista  né  possa  logicamente  &#13;
 esistere    nella    legislazione   statale   il   principio   supposto  &#13;
 nell'ordinanza di rimessione.                                            &#13;
     3. - Per quanto concerne gli altri  principi  fondamentali  che  il  &#13;
 giudice  a  quo  ha  ritenuto  di  potere  individuare nell'ordinamento  &#13;
 statale, va rilevato che il limite dei principi consiste non  già  nel  &#13;
 dovere  di osservare le singole leggi statali (perché, se così fosse,  &#13;
 il potere normativo regionale si ridurrebbe ad  una  semplice  potestà  &#13;
 regolamentare),  bensì nell'obbligo di conformarsi ai criteri generali  &#13;
 ai quali si ispira la disciplina statale in una determinata  materia  e  &#13;
 che  di  questa e dei relativi istituti sono espressione caratteristica  &#13;
 (cfr. sent. 14 luglio 1958 n. 49). I principi fondamentali delle  leggi  &#13;
 dello  Stato  non  possono  essere  confusi  dunque  con  gli specifici  &#13;
 disposti legislativi nella materia di cui  trattasi  (cfr.    sent.  18  &#13;
 gennaio  1977,  n. 36), i quali, se non sono espressione di un criterio  &#13;
 generale, non rilevano sulla potestà normativa regionale.               &#13;
     Per contro il giudice a quo, in tutto il suo ragionamento,  non  ha  &#13;
 preso  in considerazione i criteri generali della legislazione statale,  &#13;
 ma si è riferito a singole disposizioni di dettaglio  senza  avvertire  &#13;
 che   nella  stessa  legislazione  statale  vi  sono,  come  subito  si  &#13;
 accennerà, altre norme che contengono disposizioni analoghe  a  quella  &#13;
 regionale  impugnata,  con l'intuitiva conseguenza che esse non possono  &#13;
 servire a integrare il limite dei principi.                              &#13;
     Precisamente,  secondo  il  giudice  a  quo   sussisterebbe   nella  &#13;
 legislazione  statale  un  principio  fondamentale  per cui, in tema di  &#13;
 misure di salvaguardia, sarebbe consentito il divieto  di  rilascio  di  &#13;
 nuove licenze, ma non già la decadenza di quelle già rilasciate.       &#13;
     In  contrario è però da rilevare che questa materia è rimessa al  &#13;
 potere discrezionale del legislatore, che può  provvedere  secondo  le  &#13;
 circostanze   e   le   particolarità   della   materia  normativamente  &#13;
 disciplinata senza che possa ritenersi sussistente una  norma-principio  &#13;
 del  tipo ipotizzato.   Invero la stessa legge 3 novembre 1952 n. 1902,  &#13;
 indicata dal giudice a quo come unica legge statale, prevede anche essa  &#13;
 la decadenza (rectius: l'inefficacia) delle  licenze  già  rilasciate,  &#13;
 sia  pure a specifiche condizioni e su determinati presupposti; inoltre  &#13;
 la l. 6 agosto 1967 n.  765  stabilisce,  all'art.  10,  11  comma,  la  &#13;
 decadenza   delle   licenze   in   contrasto   con  nuove  "previsioni"  &#13;
 urbanistiche, se i lavori non siano stati già iniziati e  non  vengano  &#13;
 completati  nel  triennio dall'inizio. Ancora, la l. 28 gennaio 1977 n.  &#13;
 10, all'art. 18, prevede la decadenza delle  licenze  già  rilasciate,  &#13;
 salvo   che   i   lavori   siano   stati   già   iniziati  (s'intende,  &#13;
 tempestivamente, ossia entro l'anno dal rilascio, di cui  all'art.  10,  &#13;
 10  comma, l.  n. 765 del 1967) alla data della sua entrata in vigore e  &#13;
 siano completati entro quattro anni dalla stessa data.                   &#13;
     L'istituto  dell'inefficacia  sopravvenuta   delle   licenze   già  &#13;
 rilasciate  ha  quindi cittadinanza nella legislazione statale, sebbene  &#13;
 sia disciplinato in  maniera  diversa  dalla  normativa  della  Regione  &#13;
 Lazio. Pertanto, non ha superato i limiti del suo potere il legislatore  &#13;
 regionale  che  ha utilizzato un istituto conosciuto dalla legislazione  &#13;
 statale, sia pure con una disciplina diversa nei dettagli da quella  da  &#13;
 essa fornita.                                                            &#13;
     4.   -   Neanche   può  condividersi  l'affermazione  secondo  cui  &#13;
 sussisterebbe l'illegittimità della normativa  che  fa  decorrere  gli  &#13;
 effetti  delle  misure  di salvaguardia dalla data di entrata in vigore  &#13;
 della legge regionale, perché essa risulterebbe in  contrasto  con  la  &#13;
 citata  legge  n.  1902  del  1952, la quale, invece, prevede che dette  &#13;
 misure decorrano dal momento  dell'adozione  del  piano  sottoposto  ad  &#13;
 approvazione.                                                            &#13;
     Anche   qui,   va   osservato  che  non  si  può  parlare  di  una  &#13;
 norma-principio  esistente  nella  legislazione  statale,  che  avrebbe  &#13;
 impedito  al  legislatore  regionale,  per  il  limite  suindicato,  di  &#13;
 predisporre  una  disciplina  come  quella  adottata.  Il  che  risulta  &#13;
 evidente ove si rilevi che non manca nella legislazione statale qualche  &#13;
 esempio  in  cui  le  misure  di salvaguardia non sono collegate ad uno  &#13;
 strumento urbanistico adottato: precisamente,  l'art.  1  del  d.l.  17  &#13;
 aprile  1948  n.  740,  riprodotto  nell'art.  13 l. 27 ottobre 1951 n.  &#13;
 1402, stabilisce che dopo la pubblicazione  degli  elenchi  dei  comuni  &#13;
 obbligati    all'adozione   del   piano   di   ricostruzione   e   fino  &#13;
 all'approvazione del piano stesso, il prefetto può sospendere i lavori  &#13;
 di costruzione, ricostruzione e  grande  trasformazione  degli  edifici  &#13;
 privati,   suscettibili  di  rendere  più  difficile  o  più  onerosa  &#13;
 l'attuazione del piano. In base alla predetta normativa è  sufficiente  &#13;
 dunque   l'inclusione  nei  predetti  elenchi  perché  possano  essere  &#13;
 disposte le misure  di  salvaguardia,  sicché  non  può  all'evidenza  &#13;
 ritenersi  sussistente  un  principio fondamentale nel senso voluto dal  &#13;
 giudice a quo.                                                           &#13;
     5. - Palesemente infondato è anche l'assunto  della  ordinanza  di  &#13;
 rimessione,  secondo  cui  sussisterebbe  nella legislazione statale il  &#13;
 principio che le licenze  già  rilasciate  possano  perdere  efficacia  &#13;
 soltanto  per  il sopravvenire di contrastanti previsioni urbanistiche.  &#13;
 Dato che, come si è detto, nella legislazione statale e  regionale  è  &#13;
 consentito  prescindere  da  un  piano  già adottato, l'argomentazione  &#13;
 viene a mancare del  suo  indispensabile  presupposto  logico,  perché  &#13;
 manca  un  termine  di  comparazione,  e  quindi  si  rivela  del tutto  &#13;
 inconsistente.                                                           &#13;
     Il  legislatore  regionale  per  vero  ben   poteva   prendere   in  &#13;
 considerazione  l'esigenza  di immediata tutela della collettività per  &#13;
 il godimento e l'utilizzazione  dei  terreni  costieri  compresi  entro  &#13;
 determinate  fasce,  e,  così facendo, non è andato sicuramente al di  &#13;
 là  dei  limiti  consentiti  alla   potestà   legislativa   regionale  &#13;
 ordinaria.                                                               &#13;
     Sotto  ogni  profilo  la  proposta  questione  di costituzionalità  &#13;
 risulta dunque non fondata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 degli artt. 1 lett. a) e b), 2, 3 e 8 della legge della Regione Lazio 2  &#13;
 luglio  1974,  n. 30, sollevata dal Pretore di Minturno con l'ordinanza  &#13;
 in epigrafe, in  riferimento  agli  artt.  117,  1  comma,  e  3  della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 16 aprile 1982.                               &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   -  ANTONINO  DE  STEFANO  - GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI CONSO.                        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
