<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>264</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:264</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/07/1987</data_decisione>
    <data_deposito>13/07/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giovanni CONSO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, prof. Renato DELL'ANDRO, &#13;
 prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  14, quinto    &#13;
 comma, lett. b, d.l. 23 gennaio 1982 n. 9 (Norme per l'edilizia          &#13;
 residenziale  e  provvidenze in materia di sfratti), convertito in l.    &#13;
 25 marzo 1982 n. 94; promossi con ordinanze emesse dal                   &#13;
 Pretore di Milano il 13 ottobre 1986 nel procedimento civile vertente    &#13;
 tra Nardi Lino e Nava Carla Luisa (reg. ord. n. 24                       &#13;
 del  1987)  e  il  16  dicembre  1986  nei  procedimenti vertenti tra    &#13;
 Balducci Luigi e Cioni Emanuela (reg. ord. n. 64 del 1987) e tra Boni    &#13;
 Giovanni  e  C.A.F.  (reg.  ord.  n.  65  del 1987), pubblicate nelle    &#13;
 Gazzette Ufficiali n.12, prima serie speciale, del 18 marzo 1987 e n.    &#13;
 13, prima serie speciale, del 25 marzo 1987;                             &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 17 giugno 1987 il giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che nel corso di un procedimento vertente tra Nardi Lino    &#13;
 e Nava Carla Luisa ed avente per oggetto il rilascio di  un  immobile    &#13;
 abitativo  locato,  il Pretore di Milano con ordinanza del 13 ottobre    &#13;
 1986 (reg. ord. n. 24 del 1987) sollevava questione  di  legittimità    &#13;
 costituzionale  dell'art.14,  quinto  comma, lett. b, d.l. 23 gennaio    &#13;
 1982 n. 9, convertito con modificazioni nella l. 25 marzo 1982  n.94,    &#13;
 il  quale  dispone che le norme relative alla fissazione di una nuova    &#13;
 data per i provvedimenti di rilascio di  immobili  abitativi  non  si    &#13;
 applicano  qualora  il  reddito  complessivo dei componenti il nucleo    &#13;
 familiare  del  conduttore,  in  base  all'ultima  dichiarazione  dei    &#13;
 redditi, superi i diciotto milioni di lire;                              &#13;
      che il Pretore faceva riferimento:                                  &#13;
       a)  agli  artt.  3  e  24  Cost.,  in quanto la norma impugnata    &#13;
 escludeva dalla tutela giurisdizionale alcuni cittadini a causa della    &#13;
 loro condizione economica;                                               &#13;
       b)  agli  artt.  3  e  36  Cost.,  in relazione alla diversità    &#13;
 riscontrabile  nel  sistema  tributario  a  seconda  che  i   redditi    &#13;
 derivassero da lavoro dipendente o da lavoro autonomo;                   &#13;
       c)  agli artt. 3 e 31 Cost., in ragione del fatto che lo stesso    &#13;
 limite  di  reddito  operava  senza  differenziare  il   numero   dei    &#13;
 componenti  la  famiglia,  onde  appariva  lesa  anche  l'esigenza di    &#13;
 un'esistenza libera e dignitosa della famiglia stessa;                   &#13;
       d)  ancora gli artt. 3 e 24 Cost., in quanto la norma escludeva    &#13;
 il limite di reddito suindicato qualora il conduttore dimostrasse  di    &#13;
 non poter ottenere la disponibilità di un alloggio di sua proprietà    &#13;
 per effetto di un provvedimento di graduazione dello  sfratto  emesso    &#13;
 nei  suoi  confronti,  così  riscontrandosi  una  discriminazione in    &#13;
 favore del conduttore più abbiente;                                     &#13;
      che  le stesse questioni venivano sollevate dal medesimo Pretore    &#13;
 con ordinanze del 16 dicembre 1986, emesse nei procedimenti  vertenti    &#13;
 tra  Balducci Luigi e Cioni Emanuela (reg. ord. n. 64 del 1987) e tra    &#13;
 Boni Giovanni e C.A.F. (reg. ord. n. 65 del 1987);                       &#13;
      che  in queste due ordinanze il giudice rimettente prendeva atto    &#13;
 della sopravvenienza del  d.l.  29  ottobre  1986  n.708,  contenente    &#13;
 misure  urgenti per fronteggiare l'eccezionale carenza di abitazioni,    &#13;
 ma riteneva la persistente rilevanza delle questioni sollevate, così    &#13;
 motivando:   "quali   che   possano  essere  i  criteri  della  nuova    &#13;
 pianificazione dei provvedimenti di rilascio,  sembra  da  escludere,    &#13;
 per  il grande numero dei provvedimenti con identica motivazione, che    &#13;
 si possa mai  prescindere  dall'indicatore  di  priorità  costituito    &#13;
 dalla data definitiva di esecutorietà";                                 &#13;
      che   la  Presidenza  del  Consiglio  dei  ministri  interveniva    &#13;
 chiedendo che le questioni fossero dichiarate non fondate;               &#13;
    Considerato  che i giudizi, per l'identità della norma impugnata,    &#13;
 debbono essere riuniti;                                                  &#13;
      che  con  l'art.1  d.l. 7 febbraio 1985 n.12, convertito in l. 5    &#13;
 aprile 1985 n.118, è stata disposta la sospensione e graduazione dei    &#13;
 provvedimenti   di  rilascio  degli  immobili  abitativi  non  ancora    &#13;
 eseguiti (commi 1 e 2), con l'eccezione  dei  provvedimenti  "fondati    &#13;
 sulla  morosità del conduttore o del subconduttore nonché di quelli    &#13;
 emessi in una delle  ipotesi  previste  dall'art.  59,  primo  comma,    &#13;
 nn.1),  limitatamente  all'uso  abitativo,  2), 3), 6), 7) e 8) della    &#13;
 legge 27 luglio 1978 n. 392, e dell'art. 3, primo comma, n.  2),  3),    &#13;
 4) e 5) del d.l. 15 dicembre 1979 n. 629 convertito con modificazioni    &#13;
 nella legge 15 febbraio 1980 n. 25" (comma 3);                           &#13;
      che  nelle  ordinanze  di rimessione i provvedimenti di rilascio    &#13;
 risultano essere stati emessi il 12 maggio 1985, il 18 settembre 1985    &#13;
 ed   il  14  febbraio  1986,  ossia  dopo  l'entrata  in  vigore  dei    &#13;
 provvedimenti legislativi ora detti senza che il giudice a quo  abbia    &#13;
 motivato  sull'applicabilità  della  norma  impugnata,  ossia  sulla    &#13;
 rilevanza delle questioni da lui sollevate;                              &#13;
      che  ultimamente  la  materia  è stata disciplinata con d.l. 29    &#13;
 ottobre 1986 n. 708 conv. in l. 23 dicembre 1986 n. 899;                 &#13;
      che  nelle  ordinanze  di  rimessione  nn.  64  e 65 del 1987 il    &#13;
 Pretore fa bensì riferimento al sopravvenuto d.l. n.  708  del  1986    &#13;
 cit.,  ma  non  verifica  la  sua  incidenza sulle singole e concrete    &#13;
 fattispecie a lui sottoposte.                                            &#13;
      che    pertanto   le   questioni   debbono   essere   dichiarate    &#13;
 manifestamente  inammissibili  per  difetto  di   motivazione   sulla    &#13;
 rilevanza;                                                               &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953  n.  87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 riuniti i giudizi,                                                       &#13;
    dichiara manifestamente inammissibili le questioni di legittimità    &#13;
 costituzionale dell'art. 14, quinto comma, lett. b, d.l.  23  gennaio    &#13;
 1982  n. 9, conv. in l. 25 marzo 1982 n. 94, sollevate in riferimento    &#13;
 agli artt. 3, 24, 31  e  36  Cost.  dal  Pretore  di  Milano  con  le    &#13;
 ordinanze indicate in epigrafe.                                          &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 2 luglio 1987.          &#13;
                      Il Presidente: SAJA                                 &#13;
                       Il Redattore: SAJA                                 &#13;
    Depositata in cancelleria il 13 luglio 1987.                          &#13;
                        Il cancelliere: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
