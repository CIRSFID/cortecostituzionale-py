<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>89</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:89</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/03/1997</data_decisione>
    <data_deposito>08/04/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, &#13;
 avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero &#13;
 Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di legittimità costituzionale dell'art. 176, comma 22,    &#13;
 del codice della strada (approvato con  d.lgs.  30  aprile  1992,  n.    &#13;
 285), promossi con due ordinanze emesse il 27 aprile 1996 dal pretore    &#13;
 di  Cremona  nei  procedimenti penali a carico di Lara Balzarini e di    &#13;
 Giordano Denti, iscritte ai nn. 773 e 774 del registro ordinanze 1996    &#13;
 e pubblicate nella Gazzetta Ufficiale della Repubblica n.  36,  prima    &#13;
 serie speciale, dell'anno 1996;                                          &#13;
   Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio del 26 febbraio 1997 il giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
   Ritenuto che, con due ordinanze di identico contenuto emesse il  27    &#13;
 aprile  1996  (reg.  ord.  n.  773  e  n.  774 del 1996) nel corso di    &#13;
 altrettanti  procedimenti  penali  promossi  con  le  imputazioni  di    &#13;
 contravvenzione  al  divieto  di inversione del senso di marcia sulle    &#13;
 carreggiate, sulle  rampe  o  sugli  svincoli  delle  autostrade,  il    &#13;
 pretore  di  Cremona  ha  sollevato,  in riferimento all'art. 3 della    &#13;
 Costituzione, questioni di legittimità costituzionale dell'art. 176,    &#13;
 comma 22, del codice della strada (approvato  con  d.lgs.  30  aprile    &#13;
 1992,  n.  285),  nella parte in cui prevede che all'accertamento del    &#13;
 reato segua la sanzione amministrativa accessoria  della  sospensione    &#13;
 della  patente  di  guida  per un periodo da sei a ventiquattro mesi,    &#13;
 anche quando il fatto sia commesso  senza  che  si  determini  alcuna    &#13;
 situazione  di  pericolo,  mentre  la  sospensione  della  patente è    &#13;
 prevista per un periodo inferiore per  la  violazione,  nelle  stesse    &#13;
 aree,  di altre norme che disciplinano la circolazione stradale (art.    &#13;
 222 del codice della strada),  quando  derivino  danni  alle  persone    &#13;
 (lesioni personali colpose e omicidio colposo);                          &#13;
      che  il  giudice  rimettente  ritiene che la durata minima della    &#13;
 sospensione della patente (sei  mesi)  sia  eccessiva  e  palesemente    &#13;
 arbitraria,   giacché   non   sarebbe   giustificato  il  differente    &#13;
 trattamento sanzionatorio rispetto a  quello  previsto  per  condotte    &#13;
 che,  violando  altre  norme  della  circolazione,  pur cagionando la    &#13;
 lesione di beni costituzionalmente protetti  (la  salute  o  la  vita    &#13;
 della  persona)  sono sanzionate con la sospensione della patente per    &#13;
 una durata più breve (minimo quindici giorni, nel  caso  di  lesione    &#13;
 personale colposa lieve, due mesi per omicidio colposo);                 &#13;
      che  in  entrambi  i  giudizi  è  intervenuto il Presidente del    &#13;
 Consiglio  dei  Ministri,  rappresentato  e  difeso   dall'Avvocatura    &#13;
 generale dello Stato, chiedendo che le questioni siano dichiarate non    &#13;
 fondate.                                                                 &#13;
   Considerato che i dubbi di legittimità costituzionale investono la    &#13;
 stessa  disposizione  e  sono  prospettati in modo analogo, sicché i    &#13;
 relativi  giudizi  possono   essere   riuniti   per   essere   decisi    &#13;
 congiuntamente;                                                          &#13;
     che,  successivamente  alla  emanazione  delle  due  ordinanze di    &#13;
 rimessione, una identica questione è stata  dichiarata  non  fondata    &#13;
 con sentenza n. 373 del 1996;                                            &#13;
     che, difatti, non spetta alla Corte rimodulare le scelte punitive    &#13;
 del  legislatore  né  stabilire  la  quantificazione  delle sanzioni    &#13;
 (sentenze n. 217 del 1996 e n. 313 del 1995);                            &#13;
     che, in ogni caso,  il  raffronto  tra  trattamenti  sanzionatori    &#13;
 diversi,   per   verificare   se   sia  rispettato  il  limite  della    &#13;
 ragionevolezza,  non  può  essere   compiuto   considerando,   nelle    &#13;
 fattispecie   indicate   dal  giudice  rimettente  quale  termine  di    &#13;
 comparazione, esclusivamente  uno  degli  elementi  sanzionatori  (la    &#13;
 sanzione  amministrativa  della  sospensione  della patente) separato    &#13;
 dalla pena principale cui accede;                                        &#13;
     che,  pertanto,  i  dubbi  di  legittimità  costituzionale  sono    &#13;
 manifestamente infondati.                                                &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi, dichiara manifestamente non fondate le questioni    &#13;
 di  legittimità  costituzionale  dell'art. 176, comma 22, del codice    &#13;
 della  strada  (approvato  con  d.lgs.  30  aprile  1992,  n.   285),    &#13;
 sollevate,  in riferimento all'art. 3 della Costituzione, dal pretore    &#13;
 di Cremona con le ordinanze indicate in epigrafe.                        &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 25 marzo 1997.                                &#13;
                         Il Presidente: Granata                           &#13;
                        Il redattore: Mirabelli                           &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria l'8 aprile 1997.                             &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
