<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>138</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:138</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Guglielmo Roehrssen</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/11/1979</data_decisione>
    <data_deposito>06/12/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  17  della  &#13;
 legge  8  marzo  1968, n. 152; articolo unico legge 15 ottobre 1969, n.  &#13;
 746 (Nuove norme in materia previdenziale per il personale  degli  Enti  &#13;
 locali)  promosso con ordinanza emessa il 23 gennaio 1976 dal tribunale  &#13;
 amministrativo regionale dell'Umbria, sui ricorsi di  Mazzoni  Umberto,  &#13;
 Brunori  Bruno e Pinca Bruno, iscritta al n. 315 del registro ordinanze  &#13;
 1976 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 151  del  &#13;
 9 giugno 1976.                                                           &#13;
     Udito  nella  camera  di  consiglio  del  4  maggio 1979 il Giudice  &#13;
 relatore Guglielmo Roehrssen.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso di un giudizio promosso da taluni dipendenti  del  Comune  &#13;
 di  Foligno,  collocati  a  riposo  fra  il 1962 ed il 1965, diretto ad  &#13;
 ottenere il trattamento supplementare di fine  servizio  deliberato  da  &#13;
 detto  Comune  con  delibere  del 1948 e 1949, annullate dal Governo ex  &#13;
 art. 6 t.u. n. 383 del 1934, il tribunale amministrativo dell'Umbria ha  &#13;
 sollevato  questione  di  legittimità  costituzionale  del   combinato  &#13;
 disposto degli articoli 17 della legge 8 marzo 1968, n. 152 e dell'art.  &#13;
 unico  della  legge  15 ottobre 1969, n. 746, in riferimento all'art. 3  &#13;
 della Costituzione.                                                      &#13;
     Nell'ordinanza  di  rimessione emessa il 23 gennaio 1976, si rileva  &#13;
 che l'art. 17 della legge n. 152 del 1968 ha, da un  lato,  sancito  il  &#13;
 divieto, a decorrere dal 1 marzo 1966, della istituzione di trattamenti  &#13;
 supplementari  di  fine  servizio  e  ha,  dall'altro  riconosciuto  la  &#13;
 legittimità dei trattamenti supplementari, istituiti dagli Enti, nella  &#13;
 loro autonomia, prima della data predetta, stabilendo  però  che  tali  &#13;
 trattamenti  "sono  mantenuti  limitatamente  al personale in servizio"  &#13;
 alla data del 1 marzo 1966. D'altro canto l'articolo unico della  legge  &#13;
 n.  746  del  1969,  nel  dare  interpretazione  autentica dell'art. 17  &#13;
 anzidetto, ha  precisato  che  "i  trattamenti  supplementari  di  fine  &#13;
 servizio  e  pensionistici  deliberati dagli organi competenti a favore  &#13;
 del personale degli Enti locali entro il 1  marzo  1966  e  debitamente  &#13;
 approvati  dagli  organi  di  tutela,  sono  mantenuti limitatamente al  &#13;
 personale  in  servizio  a  tale  data,  anche  nei  casi  ove  per   i  &#13;
 provvedimenti  concessivi  di detti Enti sia intervenuto l'annullamento  &#13;
 ex art. 6 t.u. 3 marzo 1934, n. 383".                                    &#13;
     Si  osserva  nell'ordinanza  di  rimessione  che  in  tal  modo  la  &#13;
 reviviscenza   delle  delibere  annullate  è  stata  disposta  solo  a  &#13;
 vantaggio del personale che risultasse in  servizio  alla  data  del  1  &#13;
 marzo  1966, con esclusione quindi dal beneficio predetto del personale  &#13;
 che, a quella data risultasse già collocato  a  riposo,  cosicché  ne  &#13;
 deriverebbe   una  disciplina  ingiustificatarmente  differenziata  nei  &#13;
 riguardi di quest'ultima categoria di personale - nella quale rientrano  &#13;
 i ricorrenti che promossero il giudizio a quo - rispetto:                &#13;
     a) al personale, cessato dal servizio, in  data  antecedente  al  1  &#13;
 marzo  1966,  da  Enti  presso  i  quali risultavano operanti delibere,  &#13;
 istitutive dei trattamenti supplementari, non annullate ex  art.  6  in  &#13;
 quanto  tali  dipendenti  hanno  percepito, in applicazione delle dette  &#13;
 delibere, il trattamento supplementare e sono, ormai, al riparo da ogni  &#13;
 futuro intervento repressivo;                                            &#13;
     b) al personale, in servizio al 1 marzo 1966, presso Enti  nel  cui  &#13;
 ordinamento  è  stato  disposto  l'annullamento di ufficio di delibere  &#13;
 istitutive di trattamenti supplementari.                                 &#13;
     Si osserva nell'ordinanza  di  rimessione  che  non  sarebbe  dato,  &#13;
 infatti,  scorgere  per quale ragione - nel quadro di un regime volto a  &#13;
 riconoscere la retroattiva legittimità  ai  trattamenti  supplementari  &#13;
 istituiti   prima  del  1  marzo  1966  -  sia  stata  riconosciuta  la  &#13;
 inoperatività degli annullamenti di ufficio nei riguardi del personale  &#13;
 in servizio al 1 marzo 1966 e non anche di quello cessato prima di tale  &#13;
 data.                                                                    &#13;
     Nel giudizio non vi è stata costituzione di parti.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Il tribunale amministrativo regionale dell'Umbria ha sollevato  &#13;
 questione di legittimità  costituzionale  della  normativa  risultante  &#13;
 dall'art.  17  della  legge  8 marzo 1968, n. 152 e dall'articolo unico  &#13;
 della legge 15 ottobre 1969, n. 746, in quanto con essa  sarebbe  stato  &#13;
 sancito  il  divieto, a decorrere dal 1 marzo 1966, di istituire per il  &#13;
 personale dipendente da enti locali trattamenti supplementari  di  fine  &#13;
 servizio  e  sarebbe stata riconosciuta la legittimità dei trattamenti  &#13;
 supplementari istituiti prima della data predetta (ancorché  annullati  &#13;
 dal  Governo ex art. 6 t.u., n. 383 del 1934), per il solo personale in  &#13;
 servizio alla data del 1  marzo  1966  e  non  anche  per  quello  già  &#13;
 collocato  a  riposo  a  tale  data,  in  contrasto  con l'art. 3 della  &#13;
 Costituzione,  data  la  disparità  di  trattamento  che ne deriva nei  &#13;
 riguardi del personale collocato a riposo anteriormente al 1 marzo 1966  &#13;
 da enti le cui delibere in materia  di  trattamenti  supplementari  non  &#13;
 siano  state  annullate dal Governo, nonché nei riguardi del personale  &#13;
 in servizio al 1 marzo 1966.                                             &#13;
     La questione non è fondata.                                         &#13;
     2. - La legge n.  152  del  1968  ha  riordinato  profondamente  la  &#13;
 materia previdenziale per i dipendenti degli enti locali, adottando una  &#13;
 disciplina    molto    più    favorevole    per    il   personale   e,  &#13;
 contemporaneamente, ha  risolto  definitivamente,  con  l'art.  17,  il  &#13;
 problema  dei  trattamenti  supplementari che in precedenza erano stati  &#13;
 deliberati da numerosi enti.                                             &#13;
     Il  citato  art.  17  ha  posto  in  essere  il  seguente  sistema:  &#13;
 soppressione di ogni trattamento supplementare per il personale entrato  &#13;
 in  servizio  dopo  il  1  marzo  1966 (che è la data dalla quale, per  &#13;
 l'art. 4, decorre il nuovo trattamento previdenziale); mantenimento  ad  &#13;
 esaurimento  di  detti  trattamenti  supplementari per coloro che erano  &#13;
 rimasti in servizio alla cen nata data  1  marzo  1966,  ma  in  misura  &#13;
 ridotta,  e  cioè  con  decurtazione  di  somme  pari  all'aumento del  &#13;
 trattamento previdenziale apportato con la stessa legge (commi  secondo  &#13;
 e terzo).                                                                &#13;
     In  tal  modo  il legislatore ha fissato al 1 marzo 1966 la data di  &#13;
 applicazione del nuovo ordinamento, dal quale non vengono in alcun modo  &#13;
 toccati i dipendenti già cessati  dal  servizio  alla  medesima  data:  &#13;
 nello  stabilire questa data il legislatore, come è evidente, ha fatto  &#13;
 uso del potere discrezionale che gli è  proprio  quando  determina  il  &#13;
 momento dal quale una legge deve produrre i suoi effetti.                &#13;
     E  questa  Corte  ha  già affermato (sentenza n. 138 del 1977) che  &#13;
 "non può contrastare con il principio di uguaglianza un  differenziato  &#13;
 trattamento  applicato alla stessa categoria di soggetti, ma in momenti  &#13;
 diversi nel tempo, perché lo stesso fluire di  questo  costituisce  di  &#13;
 per   sé   un   elemento  diversificatore  in  rapporto  a  situazioni  &#13;
 concernenti  sia  gli  stessi  soggetti  come  gli   altri   componenti  &#13;
 dell'aggregato  sociale":  il  che  è stato affermato anche in tema di  &#13;
 trattamento  di  quiescenza,  essendosi  ritenute  le  situazioni   dei  &#13;
 collocati  a riposo legittimamente differenziate in relazione alla data  &#13;
 di cessazione dal servizio (sentenze n. 57/1973 e n.  92/1975).          &#13;
     Non è, pertanto, censurabile sotto il profilo  dell'art.  3  della  &#13;
 Costituzione  il disposto della legge n. 152 del 1968 per avere posto a  &#13;
 base delle sue norme la differenza fra personale a riposo o in servizio  &#13;
 alla data 1 marzo 1966 e per avere applicato  soltanto  al  secondo  la  &#13;
 nuova normativa.                                                         &#13;
     3.  - Tutto ciò premesso, va poi osservato che la legge n. 746 del  &#13;
 1969 ha avuto il solo scopo di eliminare  una  situazione  che  si  era  &#13;
 venuta a creare in sede di interpretazione dell'art. 17 della legge del  &#13;
 1968  e  nei  riguardi del solo personale ancora in servizio al 1 marzo  &#13;
 1966, in quanto in tale sede era sorto il dubbio se il mantenimento dei  &#13;
 trattamenti supplementari a favore dell'or cennato personale si dovesse  &#13;
 arrestare dinanzi all'intervento dei provvedimenti di  annullamento  ex  &#13;
 officio  degli  atti  che  li  avevano deliberati e cioè se si dovesse  &#13;
 mantenere il trattamento  in  questione  sol  quando  la  deliberazione  &#13;
 istitutiva  non  fosse  stata invalidata. Il legislatore, conformemente  &#13;
 all'orientamento che già era stato manifestato in sede di  discussione  &#13;
 della legge del 1968 (vedi Atti Senato V legislatura, documento n. 468,  &#13;
 relazione  al  disegno  di  legge  d'iniziativa dei Senatori Cengarle e  &#13;
 altri, comunicata alla Presidenza del Senato il  6  febbraio  1969)  ha  &#13;
 accolto la tesi più larga.                                              &#13;
     Così  facendo,  la  legge  del  1969  si  è  riferita alla stesso  &#13;
 personale del quale si era occupata la legge del 1968, cioè, ripetesi,  &#13;
 al personale ancora in servizio alla data del 1 marzo  1966,  chiarendo  &#13;
 la  interpretazione  esatta della norma ed eliminando la diversità che  &#13;
 si era venuta a creare, nell'ambito di questo solo  personale,  per  il  &#13;
 fatto  che  talune  delle  cennate deliberazioni istitutive erano state  &#13;
 annullate ed altre no.                                                   &#13;
     Se così stanno le cose, appare chiaro che la legge del 1969 si  è  &#13;
 mantenuta  nel medesimo ambito della precedente legge del 1968 e che il  &#13;
 riferimento alla data del 1 marzo 1966 altro  non  costituisce  che  la  &#13;
 ripetizione  di  un  dato  fondamentale  di  quella  legge, necessaria,  &#13;
 peraltro, per identificare l'ambito anche della nuova norma.             &#13;
     Valgono, di conseguenza, anche per la  legge  del  1969  le  stesse  &#13;
 considerazioni svolte a proposito di quella del 1968.                    &#13;
     4.  -  Quanto  all'altro  profilo  della  questione - disparità di  &#13;
 trattamento fra personale cessato dal servizio prima del 1  marzo  1966  &#13;
 da   Enti  presso  i  quali  erano  operanti  delibere,  istitutive  di  &#13;
 trattamenti supplementari, non annullate ex art. 6 t.u. n. 383 del 1934  &#13;
 e personale cessato  dal  servizio  da  enti  le  cui  delibere  furono  &#13;
 annullate - non si vede come esso possa risolversi in una censura della  &#13;
 normativa  impugnata,  sostanziandosi nella deduzione di una disparità  &#13;
 sorta in sede applicativa e come tale non idonea a dar luogo a  fondate  &#13;
 censure  d'incostituzionalità  e  che,  oltre  tutto, non deriva dalle  &#13;
 norme impugnate, bensì dalla normativa in precedenza  esistente  nella  &#13;
 materia.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 del combinato disposto dell'art. 17 della legge 8 marzo  1968,  n.  152  &#13;
 ("Nuove  norme  in  materia  previdenziale  per il personale degli enti  &#13;
 locali") e dell'articolo unico della legge  15  ottobre  1969,  n.  746  &#13;
 ("Interpretazione  autentica  dell'articolo  17,  secondo  comma, della  &#13;
 legge  8  marzo  1968,  n.  152,  recante  nuove   norme   in   materia  &#13;
 previdenziale  per  il personale degli enti locali")' sollevata dal TAR  &#13;
 dell'Umbria con l'ordinanza in  epigrafe,  in  riferimento  all'art.  3  &#13;
 della Costituzione.                                                      &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 30 novembre 1979.       &#13;
                                   F.to:  LEONETTO  AMADEI   -   EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO  -  ANTONINO  DE  STEFANO   -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
