<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>836</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:836</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/07/1988</data_decisione>
    <data_deposito>21/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale della legge 27 ottobre    &#13;
 1973, n.  628  (Concessione  dell'assegno  perequativo  al  personale    &#13;
 militare  e  adeguamento  della  indennità  per servizio di Istituto    &#13;
 spettante agli appartenenti ai Corpi di polizia e  ai  funzionari  di    &#13;
 pubblica  sicurezza),  promossi  con  ordinanze emesse il 29 novembre    &#13;
 1979  e  l'8  gennaio  1980  dalla  Corte  dei  Conti   -   Sez.   IV    &#13;
 giurisdizionale - sui ricorsi proposti da Vitucci Rocco e Paolantonio    &#13;
 Giacinto, iscritte ai nn. 442 e 817 del  registro  ordinanze  1980  e    &#13;
 pubblicate nella Gazzetta Ufficiale della Repubblica n. 194 dell'anno    &#13;
 1980 e n. 34 dell'anno 1981;                                             &#13;
    Visti  l'atto di costituzione di Vitucci Rocco nonché gli atti di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che,  con  ordinanza emessa il 29 novembre 1979 (R.O. n.    &#13;
 442/80), nel giudizio promosso da Vitucci Rocco,  capo  musicante  di    &#13;
 III  classe  della Marina Militare, collocato a riposo dal 31 gennaio    &#13;
 1948, diretto ad ottenere la riliquidazione  della  pensione  con  il    &#13;
 computo   dell'assegno  perequativo  pensionabile,  concesso  dal  1°    &#13;
 gennaio 1973 al personale in servizio dalla legge 27 ottobre 1973, n.    &#13;
 628,  la  Corte  dei  Conti  ha  sollevato  questione di legittimità    &#13;
 costituzionale  della  legge  27  ottobre  1973,  n.  628,  nel   suo    &#13;
 complesso,  in  quanto  non  prevede la riliquidazione con il computo    &#13;
 dell'assegno  perequativo  teorico,  delle  pensioni  percepite   dal    &#13;
 personale  militare  cessato dal servizio anteriormente al 1° gennaio    &#13;
 1973,  in  riferimento  all'art.  3,  primo  comma,  Cost.,  per   la    &#13;
 disparità  di  trattamento  fatta  alla  categoria dei sottufficiali    &#13;
 rispetto ai pensionati militari che abbiano lasciato il servizio  con    &#13;
 il  grado  di  colonnello e di generale, per i quali ha riconosciuto,    &#13;
 con efficacia retroattiva, la perequazione delle pensioni sulla  base    &#13;
 della   retribuzione   comprensiva   della   indennità  di  funzione    &#13;
 pensionabile;                                                            &#13;
      che,  con ordinanza emessa l'8 gennaio 1980 (R.O. n. 817/80) nel    &#13;
 giudizio promosso da Giacinto  Paolantonio,  tenente  colonnello  dei    &#13;
 Carabinieri  cessato  dal  servizio  il  19  gennaio 1951, diretto ad    &#13;
 ottenere  la   riliquidazione   della   pensione   con   l'inclusione    &#13;
 dell'indennità   di   servizio,   analoga,  secondo  il  ricorrente,    &#13;
 all'indennità di funzione, la stessa Corte dei  Conti  ha  sollevato    &#13;
 questione  di legittimità costituzionale degli artt. 8, 9 e 10 della    &#13;
 legge 27 ottobre 1973, n. 628, nella parte in cui non ha previsto  la    &#13;
 computabilità  nella  pensione anche della indennità di servizio in    &#13;
 riferimento all'art. 3 Cost. per la disparità di trattamento che  si    &#13;
 verifica  rispetto  alle  altre  categorie di ufficiali (colonnelli e    &#13;
 generali) per i quali è stata prevista la  computabilità,  ai  fini    &#13;
 della  pensione, della indennità di funzione con effetto retroattivo    &#13;
 anche a favore dei già pensionati;                                      &#13;
      che  nel  giudizio si è costituito Vitucci Rocco, il quale, con    &#13;
 l'atto di costituzione e una memoria, ha  sostenuto  l'illegittimità    &#13;
 della  legge  impugnata in quanto l'indennità di servizio è analoga    &#13;
 alla indennità di funzione  e  all'assegno  perequativo,  stante  la    &#13;
 parità di carriera;                                                     &#13;
      che l'Avvocatura Generale dello Stato, intervenuta in entrambi i    &#13;
 giudizi in rappresentanza della Presidenza del Consiglio, ha concluso    &#13;
 per  l'infondatezza  delle  questioni,  essendo le situazioni poste a    &#13;
 confronto disomogenee, essendo i trattamenti migliorativi, sui quali,    &#13;
 peraltro,  gravano  i  contributi  ai fini pensionistici, intervenuti    &#13;
 dopo che il rapporto di impiego dei ricorrenti era cessato  e  stante    &#13;
 la  valenza  del  principio di gradualità nella materia, imposto dal    &#13;
 fatto che i miglioramenti comportano aggravi finanziari di bilancio;     &#13;
    Considerato  che,  sebbene  l'impugnazione sia riferita alla legge    &#13;
 nel suo complesso, è possibile identificare e individuare  le  norme    &#13;
 applicabili alla fattispecie;                                            &#13;
      che  i  due  giudizi,  siccome  prospettano  identica questione,    &#13;
 possono essere riuniti e decisi con un unico provvedimento;              &#13;
      che  sussiste  la discrezionalità del legislatore nel prevedere    &#13;
 trattamenti  retributivi  diversi  e  nello   stabilirne   anche   la    &#13;
 decorrenza;                                                              &#13;
      che  la  situazione  di  coloro che godevano della indennità di    &#13;
 servizio non è identica a quella di coloro che godevano l'indennità    &#13;
 di funzione;                                                             &#13;
      che la valutazione del tempo e dei modi della perequazione delle    &#13;
 pensioni è rimessa alla discrezionalità del legislatore che l'attua    &#13;
 con   gradualità,   in   ordine   alle  disponibilità  finanziarie,    &#13;
 risultando un aggravio del bilancio;                                     &#13;
      che, pertanto, le questioni sono manifestamente infondate;          &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale degli artt. 8, 9 e 10  della    &#13;
 legge  27  ottobre 1973, n. 628 (Concessione dell'assegno perequativo    &#13;
 al personale militare e adeguamento della indennità per servizio  di    &#13;
 Istituto  spettante  agli  appartenenti  ai  Corpi  di  polizia  e ai    &#13;
 funzionari di pubblica sicurezza), sollevata, in riferimento all'art.    &#13;
 3 Cost., dalla Corte dei Conti con le ordinanze in epigrafe.             &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 luglio 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
