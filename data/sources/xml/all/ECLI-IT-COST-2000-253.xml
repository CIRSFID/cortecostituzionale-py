<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>253</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:253</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/06/2000</data_decisione>
    <data_deposito>03/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 21 del d.P.R.    &#13;
 26  ottobre  1972,  n. 634  (Disciplina  dell'imposta  di  registro),    &#13;
 riprodotto   nell'art. 22   del   d.P.R.   26   aprile  1986,  n. 131    &#13;
 (Approvazione   del   testo   unico  delle  disposizioni  concernenti    &#13;
 l'imposta  di  registro),  promosso con ordinanza emessa il 28 giugno    &#13;
 1999  dalla  Commissione  tributaria  regionale di Milano sul ricorso    &#13;
 proposto  dall'Ufficio  del  Registro  di Milano contro Fadini Mario,    &#13;
 iscritta  al  n. 743  del  registro ordinanze 1999 e pubblicata nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n. 4,  prima  serie speciale,    &#13;
 dell'anno 2000;                                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio dell'8 giugno 2000 il giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
     Ritenuto  che,  nell'ambito  di  un  giudizio pendente davanti ad    &#13;
 essa,  la  Commissione  tributaria regionale di Milano, con ordinanza    &#13;
 del  6  marzo  1997,  sollevava  d'ufficio  questione di legittimità    &#13;
 costituzionale  dell'art. 21  del  d.P.R.  26  ottobre  1972,  n. 634    &#13;
 (Disciplina  dell'imposta  di  registro), riprodotto nell'art. 22 del    &#13;
 d.P.R.  26  aprile  1986,  n. 131 (Approvazione del testo unico delle    &#13;
 disposizioni  concernenti  l'imposta di registro), nella parte in cui    &#13;
 sottopone ad imposta di registro le disposizioni enunciate negli atti    &#13;
 dell'autorità giudiziaria, in riferimento agli artt. 24, 53, 76 e 77    &#13;
 della Costituzione;                                                      &#13;
         che  la  Corte  costituzionale,  con  sentenza n. 7 del 1999,    &#13;
 rigettava  la  questione, ritenendola infondata sotto tutti i profili    &#13;
 denunciati,  rilevando,  in particolare, che sono soggetti ad imposta    &#13;
 non  gli  atti  genericamente  enunciati  dalle parti, ma solo quelli    &#13;
 posti  dal  giudice  a  base della propria decisione e che "se l'atto    &#13;
 enunciato  (e  per  questo motivo tassato) era soggetto ad imposta in    &#13;
 termine  fisso,  le  parti  risultano inadempienti ad un loro preciso    &#13;
 dovere  fiscale",  né  la  successiva tassazione viola il diritto di    &#13;
 difesa;  se,  invece,  "il  provvedimento  enunciato  è  soggetto  a    &#13;
 tassazione  in  caso d'uso, è proprio la sua allegazione in giudizio    &#13;
 che, rappresentandone una forma d'uso, ne legittima la sottoposizione    &#13;
 all'imposta di registro";                                                &#13;
         che,  con  ordinanza  del  28  giugno  1999,  la  Commissione    &#13;
 tributaria  regionale  di Milano, nell'ambito dello stesso giudizio a    &#13;
 quo   ha   nuovamente   sollevato   una   questione  di  legittimità    &#13;
 costituzionale,  del tutto identica alla precedente, ritenendo che le    &#13;
 ragioni  poste  dalla  Corte costituzionale a fondamento della citata    &#13;
 sentenza  n. 7  del  1999 non siano convincenti, in quanto il giudice    &#13;
 delle  leggi  non  avrebbe  considerato  che  "l'alternativa assoluta    &#13;
 nell'ambito  della  quale ... ha risolto la questione di legittimità    &#13;
 costituzionale ...  non  esaurisce  in  realtà le fattispecie legali    &#13;
 della   sottoposizione  ad  imposta  delle  enunciazioni,  e  non  le    &#13;
 esaurisce  proprio con riferimento al caso concreto in cui le ipotesi    &#13;
 indicate  dal  legislatore  sono  rilevanti  per  il giudizio" a quo.    &#13;
 Risulterebbero,  secondo  il  rimettente, sottoposti a tassazione non    &#13;
 solo  le  enunciazioni degli atti soggetti a registrazione in termine    &#13;
 fisso  e  quelle degli atti sottoposti a registrazione in caso d'uso,    &#13;
 ma  anche  le  disposizioni, i contratti e gli altri atti per i quali    &#13;
 non  sarebbe mai sorta l'obbligazione tributaria; che verrebbe invece    &#13;
 ad   esistenza   solo  in  ragione  dell'enunciazione  contenuta  nel    &#13;
 provvedimento dell'autorità giudiziaria;                                &#13;
         che,  pertanto,  il giudice a quo ripropone tutte le censure,    &#13;
 avanzate  contro  la  norma  impugnata  nella precedente ordinanza di    &#13;
 rimessione,  alla  quale  esplicitamente  si richiama, chiedendo alla    &#13;
 Corte costituzionale di pronunciarsi su di esse;                         &#13;
         che  nel  giudizio  avanti  la Corte costituzionale non si è    &#13;
 costituita  la parte privata, mentre è intervenuto il Presidente del    &#13;
 Consiglio   dei  Ministri,  rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale  dello  Stato, il quale ha concluso per l'inammissibilità o    &#13;
 comunque la manifesta infondatezza della questione, rilevando che "il    &#13;
 giudice  a quo replica inammissibilmente, nella sostanza, nell'ambito    &#13;
 dello  stesso  giudizio  a  quo  alla sentenza n. 7/1999 della Corte,    &#13;
 dichiarativa  della non fondatezza della questione già sollevata con    &#13;
 riferimento    alla   stessa   norma   ed   agli   stessi   parametri    &#13;
 costituzionali";                                                         &#13;
         che   inoltre,   secondo   la  difesa  erariale,  il  giudice    &#13;
 rimettente,  muovendo da un inesatto apprezzamento della legge delega    &#13;
 e  considerando  ostacolo all'agire in giudizio ciò che ostacolo non    &#13;
 è,   introdurrebbe   una   distinzione  consistente  in  un'astratta    &#13;
 classificazione  delle tecniche impositive nel sistema della legge di    &#13;
 registro,   del   tutto   sterile   ai   fini   dello   scrutinio  di    &#13;
 costituzionalità.                                                       &#13;
     Considerato  che,  nel  caso in cui la Corte costituzionale abbia    &#13;
 emesso una pronuncia di carattere decisorio, è precluso al giudice a    &#13;
 quo  rimetterle  una  seconda  volta la medesima questione, nel corso    &#13;
 dello  stesso  grado  di giudizio pendente tra le stesse parti, ferma    &#13;
 restando  la  proponibilità  della  questione riformulata in termini    &#13;
 sostanzialmente  diversi,  cioè con riferimento a norme, parametri o    &#13;
 profili  del  tutto  nuovi  (cfr. sentenze n. 12 del 1998, n. 257 del    &#13;
 1991,  n. 350  del  1987);  e  ciò per evitare un bis in idem che si    &#13;
 risolverebbe  nella  impugnazione  della  precedente  decisione della    &#13;
 Corte, in violazione dell'art. 137, comma terzo, della Costituzione;     &#13;
         che,  nella  specie,  la  Commissione tributaria regionale di    &#13;
 Milano  ripropone,  nell'ambito  dello  stesso  grado di giudizio, la    &#13;
 medesima  questione già sollevata nel 1997 ed oggetto della sentenza    &#13;
 di rigetto n. 7 del 1999, senza far riferimento ad un quadro diverso,    &#13;
 ma  anzi  chiedendo  a  questa  Corte un riesame nel merito della sua    &#13;
 pronuncia, sia pure sottolineandone particolari aspetti;                 &#13;
         che, pertanto, la questione è manifestamente inammissibile.     &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale dell'art. 21 del d.P.R. 26 ottobre 1972,    &#13;
 n. 634 (Disciplina dell'imposta di registro), riprodotto nell'art. 22    &#13;
 del d.P.R. 26 aprile 1986, n. 131 (Approvazione del testo unico delle    &#13;
 disposizioni  concernenti  l'imposta  di  registro),  sollevata dalla    &#13;
 Commissione  tributaria regionale di Milano, con l'ordinanza indicata    &#13;
 in epigrafe.                                                             &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 giugno 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 3 luglio 2000.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
