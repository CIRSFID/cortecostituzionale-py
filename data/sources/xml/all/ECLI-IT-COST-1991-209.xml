<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>209</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:209</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/04/1991</data_decisione>
    <data_deposito>13/05/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 1, comma quarto-quinquies  del  decreto-legge  27 dicembre 1989, n. 413 (Disposizioni    &#13;
 urgenti in materia di trattamento economico dei dirigenti dello Stato    &#13;
 e delle categorie ad essi equiparate, nonché in materia di  pubblico    &#13;
 impiego),  convertito  in  legge 28 febbraio 1990, n. 37, e dell'art.    &#13;
 10, comma sesto, del decreto-legge 6 novembre 1989, n. 357 (Norme  in    &#13;
 materia  di  reclutamento  del personale della scuola), convertito in    &#13;
 legge 27 dicembre 1989, n. 417, promossi con le seguenti ordinanze:      &#13;
      1) ordinanza emessa il 17 novembre 1990 dal T.A.R. del Lazio nel    &#13;
 ricorso proposto da Lopes Felice contro A.N.A.S. ed altra iscritta al    &#13;
 n.  87  del  registro  ordinanze  1991  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale della Repubblica n.  9,  prima  serie  speciale,  dell'anno    &#13;
 1991;                                                                    &#13;
      2) ordinanza emessa il 17 novembre 1990 dal T.A.R. del Lazio nel    &#13;
 ricorso  proposto  da  Pastore  Giuliano  contro  A.N.A.S.  ed  altra    &#13;
 iscritta al n. 88 del registro  ordinanze  1991  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  9,  prima serie speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Udito nella camera di consiglio del  22  aprile  1991  il  Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Ritenuto  che  con  due  ordinanze  emesse  il  17 ottobre 1990 di    &#13;
 identico contenuto il Tribunale amministrativo regionale del Lazio ha    &#13;
 sollevato  questione  incidentale  di   legittimità   costituzionale    &#13;
 "dell'art.  1,  comma  quarto-quinquies del decreto-legge 27 dicembre    &#13;
 1989,  n.  413  (Disposizioni  urgenti  in  materia  di   trattamento    &#13;
 economico  dei  dirigenti  dello  Stato  e  delle  categorie  ad essi    &#13;
 equiparate, nonché in materia di pubblico  impiego),  convertito  in    &#13;
 legge 28 febbraio 1990, n. 37, nonché dell'art. 10, comma sesto, del    &#13;
 decreto-legge   6   novembre  1989,  n.  357  (Norme  in  materia  di    &#13;
 reclutamento del personale della  scuola),  convertito  in  legge  27    &#13;
 dicembre 1989, n. 417, nella parte in cui, non ammettendo, neppure in    &#13;
 via  transitoria,  la possibilità di revocare il riscatto di periodi    &#13;
 di servizio o di  studio  o  di  rinunciare,  comunque,  ai  benefici    &#13;
 derivanti  dall'esercizio  del  relativo  diritto,  non  prevedono il    &#13;
 diritto al mantenimento in servizio, sino al limite dei  70  anni  di    &#13;
 età,  in  favore  di  coloro  che  abbiano  esercitato il diritto al    &#13;
 riscatto anteriormente all'entrata in vigore  dello  stesso  decreto-legge  n.  413  del  1989,  per  contrasto con gli artt. 3 e 97 della    &#13;
 Costituzione";                                                           &#13;
      che nei giudizi  a  quibus  è  intervenuto  il  Presidente  del    &#13;
 Consiglio  dei  ministri  che  ha  concluso  per l'infondatezza della    &#13;
 questione.                                                               &#13;
    Considerato che le  ordinanze  di  rimessione  sollevano  identica    &#13;
 questione,  sì che i relativi giudizi possono essere riuniti ai fini    &#13;
 di un'unica pronuncia;                                                   &#13;
      che la norma di cui all'art. 15  l.  30  luglio  1973,  n.  477,    &#13;
 oggetto  di  interpretazione  autentica  da parte dell'art. 10, sesto    &#13;
 comma, decreto-legge 6 novembre 1989, n. 357, convertito in legge  27    &#13;
 dicembre  1989  n.  417,  e di applicazione anche ai dirigenti civili    &#13;
 dello Stato in base all'art. 1, comma quarto-quinquies, decreto-legge    &#13;
 27 dicembre 1989, n. 413, convertito in legge 28  febbraio  1990,  n.    &#13;
 37, prevede la possibilità per le categorie ivi previste di rimanere    &#13;
 in  servizio  su  richiesta fino al raggiungimento del limite massimo    &#13;
 della pensione, anche oltre il 65° anno di età e comunque non  oltre    &#13;
 il 70°;                                                                  &#13;
      che  tale norma è stata introdotta dal legislatore, nel ridurre    &#13;
 il limite d'età pensionabile al compimento del 65°  anno  (art.  15,    &#13;
 primo  comma,  legge  30  luglio  1973,  n.  477),  allo scopo di far    &#13;
 conseguire il previsto beneficio a coloro che non avessero  raggiunto    &#13;
 il limite massimo della pensione;                                        &#13;
      che,  pertanto,  non  appare irrazionale né discriminatoria, in    &#13;
 relazione alla descritta ratio, la circostanza di mero fatto  in  cui    &#13;
 vengono  a  trovarsi coloro che, mediante domanda di riscatto accolta    &#13;
 dall'Amministrazione  con   provvedimento   formale,   abbiano   già    &#13;
 conseguito  il  raggiungimento del massimo della pensione, rispetto a    &#13;
 coloro che ciò non abbiano  conseguito  per  non  aver  ottenuto  (o    &#13;
 potuto   ottenere  per  qualsivoglia  ragione)  detto  beneficio  del    &#13;
 riscatto;                                                                &#13;
      che neppure si ravvisa alcuna  violazione  dell'art.  97  Cost.,    &#13;
 semmai  vulnerato  da  una  pronuncia che consentisse, trascendendo i    &#13;
 limiti della circoscritta situazione  occupazionale  tenuta  presente    &#13;
 dal  legislatore, la facoltà, da parte del dipendente, di revoca del    &#13;
 beneficio del riscatto.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la  manifesta  infondatezza   della    &#13;
 questione  di  legittimità costituzionale dell'art. 1, comma quarto-quinquies del decreto-legge 27 dicembre 1989,  n.  413  (Disposizioni    &#13;
 urgenti in materia di trattamento economico dei dirigenti dello Stato    &#13;
 e  delle categorie ad essi equiparate, nonché in materia di pubblico    &#13;
 impiego), convertito in  legge  28  febbraio  1990,  n.  37,  nonché    &#13;
 dell'art.  10, comma sesto, del decreto-legge 6 novembre 1989, n. 357    &#13;
 (Norme in  materia  di  reclutamento  del  personale  della  scuola),    &#13;
 convertito  in  legge  27  dicembre 1989, n. 417, in riferimento agli    &#13;
 artt. 3 e 97  della  Costituzione,  sollevata  con  le  ordinanze  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 aprile 1991.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: BORZELLINO                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 13 maggio 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
