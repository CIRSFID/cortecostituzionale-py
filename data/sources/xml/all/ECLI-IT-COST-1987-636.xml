<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>636</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:636</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/12/1987</data_decisione>
    <data_deposito>30/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, prof. Luigi MENGONI, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi di legittimità costituzionale dell'art. 217 del r.d. 16    &#13;
 marzo  1942,  n.  267  (Disciplina  del  fallimento,  del  concordato    &#13;
 preventivo,  dell'amministrazione  controllata  e  della liquidazione    &#13;
 coatta amministrativa), in relazione agli artt.1, 42 e 43 del  codice    &#13;
 penale  e  14  delle  preleggi e dell'art. 21 del codice di procedura    &#13;
 penale, in relazione all'art. 2909 del codice  civile,  promossi  con    &#13;
 ordinanze  emesse  il  2  giugno,  il 23 giugno (n. 2 ordinanze) il 9    &#13;
 giugno  e  il  16  giugno  1982  dal  Pretore  di   Fermo,   iscritte    &#13;
 rispettivamente  ai  nn.  932,  933,  934,  935  e  936  del registro    &#13;
 ordinanze 1982 e pubblicate nella Gazzetta Ufficiale della Repubblica    &#13;
 nn. 156 e 149 dell'anno 1983;                                            &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto che il Pretore di Fermo, con ordinanza emessa il 2 giugno    &#13;
 1982 (Reg. Ord. 932/1982)  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale:                                                          &#13;
      a)  dell'art.  217  r.d.  16  marzo 1942, n. 267 (Disciplina del    &#13;
 fallimento,   del   concordato    preventivo,    dell'amministrazione    &#13;
 controllata  e  della liquidazione coatta amministrativa) e dell'art.    &#13;
 21 c.p.p. (in relazione  all'art.  2909  c.c.)  nella  parte  in  cui    &#13;
 considerano  che  la  sentenza dichiarativa di fallimento, passata in    &#13;
 giudicato, fa stato circa la qualità di imprenditore commerciale del    &#13;
 fallito cui consegue l'obbligo della tenuta delle scritture contabili    &#13;
 di legge, con riferimento agli artt. 2, 24, commi  primo  e  secondo,    &#13;
 25,  comma primo, 27, comma secondo, 101, comma secondo, e 102, comma    &#13;
 primo, Cost.;                                                            &#13;
      b)  dell'art.  217 r.d. 16 marzo 1942, n. 267 cit., in relazione    &#13;
 agli artt. 1, 42 e 43 c.p. e 14 preleggi, nella parte in cui, secondo    &#13;
 un  orientamento consolidato della Cassazione, il reato di bancarotta    &#13;
 semplice,  per  omessa  tenuta  delle  scritture   contabili,   viene    &#13;
 soggettivamente punito non a titolo di dolo ma a titolo o di semplice    &#13;
 colpa o indifferentemente sia a titolo di  dolo  che  di  colpa,  con    &#13;
 riferimento  agli  artt.  3, 25, comma secondo, 27, comma primo, 101,    &#13;
 comma secondo, e 111, comma secondo, Cost.;                              &#13;
      c) dell'art. 217 r.d. 16 marzo 1942, n. 267 cit., nella parte in    &#13;
 cui, secondo un orientamento consolidato della Cassazione,  considera    &#13;
 la  sentenza dichiarativa di fallimento come elemento costitutivo del    &#13;
 reato di bancarotta e non come condizione obiettiva  di  punibilità,    &#13;
 con riferimento agli artt. 27, comma primo, e 3 Cost.;                   &#13;
      che  il  medesimo  Pretore di Fermo, con altre quattro ordinanze    &#13;
 emesse il 9 giugno 1982  (R.O.  935/82),  il  16  giugno  1982  (R.O.    &#13;
 936/82) e il 23 giugno 1982 (Reg. Ord. 933/82 e 934/82) ha nuovamente    &#13;
 sollevato, in riferimento agli stessi artt.  27,  comma  primo,  e  3    &#13;
 Cost.,   e  sotto  gli  stessi  profili,  questione  di  legittimità    &#13;
 costituzionale dell'art. 217 r.d. 16 marzo 1942, n. 267  cit.,  nella    &#13;
 parte  in  cui, secondo un orientamento consolidato della Cassazione,    &#13;
 considera  la  sentenza  dichiarativa  di  fallimento  come  elemento    &#13;
 costitutivo  del  reato di bancarotta e non come condizione obiettiva    &#13;
 di punibilità;                                                          &#13;
    Considerato  che, per l'identità o connessione delle questioni, i    &#13;
 giudizi possono essere riuniti;                                          &#13;
      che  la prima delle questioni sollevate - relativa all'efficacia    &#13;
 vincolante  della  sentenza  dichiarativa  di  fallimento  circa   la    &#13;
 qualità   di   imprenditore  commerciale  del  fallito  -  è  stata    &#13;
 dichiarata manifestamente infondata, in riferimento agli artt.  24  e    &#13;
 25 Cost., con ordinanza n. 59 del 1971 (in relazione alla sentenza n.    &#13;
 141 del 1970) e con sentenza n. 275 del 1974; che  queste  decisioni,    &#13;
 fondate    sostanzialmente    sul    principio    dell'unità   della    &#13;
 giurisdizione, esprimono motivazioni valide anche per  gli  ulteriori    &#13;
 profili e parametri cui fa cenno il giudice a quo;                       &#13;
      che   la   terza  delle  questioni  sollevate  -  relativa  alla    &#13;
 qualificazione  della  sentenza  dichiarativa  di   fallimento   come    &#13;
 elemento costitutivo del reato di bancarotta anziché come condizione    &#13;
 obiettiva di punibilità - è stata dichiarata infondata con sentenze    &#13;
 nn.110  e  190  del 1972 e, da ultimo, con sentenza n. 146 del 1982 e    &#13;
 che non sono stati addotti profili o motivi nuovi rispetto  a  quelli    &#13;
 già esaminati dalla Corte con le decisioni in parola;                   &#13;
      che,   pertanto,   le   due  suddette  questioni  devono  essere    &#13;
 dichiarate manifestamente infondate;                                     &#13;
      che  la  seconda  delle  questioni  sollevate  -  relativa  alla    &#13;
 punibilità del reato di bancarotta semplice, per omessa tenuta delle    &#13;
 scritture  contabili,  a  titolo  sia  di  dolo  sia  di  colpa  - è    &#13;
 manifestamente inammissibile in quanto è dato al giudice a quo e non    &#13;
 a questa Corte interpretare la disposizione impugnata nel modo che lo    &#13;
 stesso giudice ritiene corretto;                                         &#13;
    Visti  gli  artt. 26, comma secondo, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 Costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi, dichiara manifestamente infondata la questione    &#13;
 di legittimità costituzionale dell'art. 217 r.d. 16 marzo  1942,  n.    &#13;
 267  e  dell'art.  21  c.p.p.  (in  relazione  all'art.  2909  c.c.),    &#13;
 sollevata, in riferimento agli artt. 2, 24, commi  primo  e  secondo,    &#13;
 25,  comma  primo,  27, comma secondo, 101, comma secondo, 102, comma    &#13;
 primo, Cost., dal Pretore di Fermo con ordinanza del 2 giugno 1982;      &#13;
    Dichiara  manifestamente  infondata  la  questione di legittimità    &#13;
 costituzionale dell'art. 217 r.d. 16 marzo 1942, n.  267,  sollevata,    &#13;
 in  riferimento agli artt. 3 e 27, comma primo, Cost., dal Pretore di    &#13;
 Fermo con ordinanze del 2 giugno, 9 giugno, 16  giugno  e  23  giugno    &#13;
 1982;                                                                    &#13;
    Dichiara manifestamente inammissibile la questione di legittimità    &#13;
 costituzionale dell'art. 217 r.d. 16 marzo 1942, n. 267, in relazione    &#13;
 agli  artt.  1, 42 e 43 c.p. e 14 preleggi, sollevata, in riferimento    &#13;
 agli artt. 3, 25, comma secondo, 27, comma primo, 101, comma secondo,    &#13;
 e  111,  comma  secondo,  Cost., dal Pretore di Fermo con ordinanza 2    &#13;
 giugno 1982.                                                             &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 17 dicembre 1987.       &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: DELL'ANDRO                          &#13;
    Depositata in cancelleria il 30 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
