<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>130</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:130</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/01/1988</data_decisione>
    <data_deposito>02/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco P. CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 2, secondo    &#13;
 comma, della legge 16 dicembre  1977,  n.  904  (Modificazione  della    &#13;
 disciplina  dell'imposta  sul  reddito  delle persone giuridiche e al    &#13;
 regime tributario dei dividendi e degli aumenti di capitale, ecc.)  e    &#13;
 dell'art.  54,  ultimo  comma,  del  d.P.R. 29 settembre 1973, n. 600    &#13;
 (Disposizioni comuni in materia di  accertamento  delle  imposte  sui    &#13;
 redditi),  promosso  con  ordinanza  emessa il 23 febbraio 1987 dalla    &#13;
 Commissione Tributaria di I grado di Verbania, iscritta al n. 222 del    &#13;
 registro  ordinanze  1987 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 25, prima serie speciale dell'anno 1987;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 dicembre 1987 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  nel corso di un procedimento iniziato da Marcolungo    &#13;
 Egilio ed avente per oggetto un accertamento d'ufficio di redditi  di    &#13;
 capitale  non  dichiarati a fini irpef e conseguente pena pecuniaria,    &#13;
 la Commissione tributaria di primo grado di  Verbania  con  ordinanza    &#13;
 del  23  febbraio  1987  (reg.  ord.  n.  222 del 1987) sollevava, in    &#13;
 riferimento agli artt. 2, 3 e 53  Cost.,  questioni  di  legittimità    &#13;
 costituzionale  dell'art.  2,  secondo  comma, l. 16 dicembre 1977 n.    &#13;
 904, nonché, in riferimento agli artt. 2, 3 e  24  Cost.,  dell'art.    &#13;
 54, ultimo comma, d.P.R. 29 settembre 1973 n. 600;                       &#13;
      che  la  Commissione rilevava come ai soci delle società di cui    &#13;
 all'art. 2 lett. a d.P.R. n. 598 del  1973  (ossia  le  società  per    &#13;
 azioni, in accomandita per azioni, a responsabilità limitata, ecc.),    &#13;
 che percepissero utili in qualsiasi forma, l'art. 1 cit.  l.  n.  904    &#13;
 del   1977   attribuisse  "un  credito  d'imposta  pari  a  un  terzo    &#13;
 (successivamente aumentato con disposizioni che qui non  interessano)    &#13;
 dell'ammontare degli utili" rientranti nel loro reddito imponibile ai    &#13;
 fini irpef o irpeg; il successivo art. 2,  secondo  comma,  stabiliva    &#13;
 che  le  relative  detrazioni  non  spettassero  in  caso  di  omessa    &#13;
 indicazione degli utili nella dichiarazione dei redditi;                 &#13;
      che,  ciò  premesso,  la  Commissione  riteneva  che  la  detta    &#13;
 omissione di indicazione, seppure poteva  giustificare  l'irrogazione    &#13;
 di  una  sanzione,  non poteva tuttavia influire sull'ammontare delle    &#13;
 detrazioni, e quindi sulla determinazione dell'imposta, senza violare    &#13;
 il  principio  della capacità contributiva (art. 53 Cost.), ossia di    &#13;
 proporzione fra reddito e tributo;                                       &#13;
      che  il collegio rimettente considerava altresì come l'art. 54,    &#13;
 ultimo comma, d.P.R. n. 600 del 1973 riducesse  alla  metà  la  pena    &#13;
 pecuniaria  prevista  per  le  omesse  o  infedeli  dichiarazioni dei    &#13;
 redditi  ove  il  contribuente   avesse   rinunciato   ad   impugnare    &#13;
 l'accertamento  d'ufficio  prima  della  decisione  della Commissione    &#13;
 tributaria di primo grado;                                               &#13;
      che  in  tale  disposizione  il  collegio  ravvisava un'indebita    &#13;
 pressione del legislatore, diretta ad indurre il contribuente  a  non    &#13;
 esercitare  il  proprio  diritto, e quindi una violazione dell'art. 2    &#13;
 Cost.,  ossia  del  diritto  inviolabile  del  cittadino  "ad  essere    &#13;
 governato  da un corpus di norme fiscali equo e democratico", nonché    &#13;
 degli artt. 3 e 24 Cost.;                                                &#13;
      che  la  Presidenza  del  Consiglio  dei ministri, costituitasi,    &#13;
 chiedeva  dichiararsi  l'inammissibilità  o   l'infondatezza   delle    &#13;
 questioni;                                                               &#13;
    Considerato che, quanto alla censura avente ad oggetto l'art. 2 l.    &#13;
 n. 904 del 1977, questa Corte ha già osservato  (sent.  n.  186  del    &#13;
 1982) come la determinazione del quantum del tributo ben possa essere    &#13;
 connessa   con   l'osservanza   di   alcuni   oneri,   purché    non    &#13;
 irragionevolmente  gravosi,  da  parte  del contribuente, quale, come    &#13;
 nella specie, la veridica indicazione di utili percepiti,  rendendosi    &#13;
 così  manifestamente  infondato il riferimento all'art. 53 Cost. (il    &#13;
 riferimento  agli  artt.  2  e  3  Cost.   non  è  neppure  motivato    &#13;
 nell'ordinanza di rimessione);                                           &#13;
      che,  quanto  alla censura avente ad oggetto l'art. 54 d.P.R. n.    &#13;
 600 del 1973, va osservato che l'interesse dell'Erario alla sollecita    &#13;
 riscossione  dei  tributi  ben  può essere perseguito facilitando il    &#13;
 contribuente che rinunci al contenzioso relativo,  palesandosi  così    &#13;
 inconsistente  il riferimento della Commissione rimettente all'art. 3    &#13;
 Cost.;                                                                   &#13;
      che  l'art.  2  Cost.  non tutela affatto un preteso diritto del    &#13;
 cittadino all'equità fiscale (v. sent. n. 283 del 1987) così  come,    &#13;
 non  trattandosi  di  diritto  processuale,  è errato il riferimento    &#13;
 all'art. 24 Cost. (v. ancora sent. n. 186 del 1982);                     &#13;
      che  in  conclusione  le  questioni  debbono  essere  dichiarate    &#13;
 manifestamente infondate;                                                &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953  n.  87 e 9 delle Norme    &#13;
 integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 2, secondo comma, l.  16  dicembre  1977  n.    &#13;
 904,  sollevata  in  riferimento  agli  artt.  2,  3 e 53 Cost. dalla    &#13;
 Commissione tributaria di primo grado  di  Verbania  con  l'ordinanza    &#13;
 indicata in epigrafe;                                                    &#13;
    Dichiara  manifestamente  infondata  la  questione di legittimità    &#13;
 costituzionale dell'art. 54, ultimo comma, d.P.R. 29 settembre  1973,    &#13;
 n.  600,  sollevata  in  riferimento agli artt. 2, 3 e 24 Cost. dalla    &#13;
 medesima Commissione tributaria con la stessa ordinanza.                 &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 febbraio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
