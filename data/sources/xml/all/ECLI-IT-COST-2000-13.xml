<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>13</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:13</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/01/2000</data_decisione>
    <data_deposito>17/01/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 408, comma 3,    &#13;
 del codice di procedura penale, promosso con ordinanza emessa  il  30    &#13;
 novembre  1998  dal giudice per le indagini preliminari del tribunale    &#13;
 di Torino nel procedimento penale a carico di C. L., iscritta  al  n.    &#13;
 62  del registro ordinanze 1999 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 7, prima serie speciale, dell'anno 1999.             &#13;
   Udito nella camera di consiglio del  27  ottobre  1999  il  giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto  che  il giudice per le indagini preliminari del tribunale    &#13;
 di Torino ha sollevato, in riferimento all'art.  24,  secondo  comma,    &#13;
 della      Costituzione,  questione  di  legittimità  costituzionale    &#13;
 dell'art.  408, comma 3, del codice di procedura penale, nella  parte    &#13;
 in  cui  non  prevede  che  la  persona  offesa  abbia la facoltà di    &#13;
 estrarre copia degli atti di cui può prendere visione;                  &#13;
     che il rimettente  premette:  che  il  pubblico  ministero  aveva    &#13;
 formulato  richiesta  di  archiviazione,  avverso la quale la persona    &#13;
 offesa  aveva  presentato  opposizione,  indicando  l'oggetto   della    &#13;
 investigazione  suppletiva  e  i  relativi  elementi di prova; che il    &#13;
 pubblico ministero, espletate le indagini disposte ai sensi dell'art.    &#13;
 409, comma 4, cod.   proc. pen.,  aveva  reiterato  la  richiesta  di    &#13;
 archiviazione  e  che,  a  seguito  della  notificazione del relativo    &#13;
 avviso, il difensore della persona offesa aveva chiesto  copia  degli    &#13;
 atti  dell'indagine  suppletiva, rilevando - ad avviso del rimettente    &#13;
 "non senza ragione" - essere "del tutto illogico ed  assurdo  che  un    &#13;
 difensore,  al  fine  di  fare fino in fondo il proprio dovere, debba    &#13;
 essere costretto ad opera di amanuense ...", ma il pubblico ministero    &#13;
 aveva respinto l'istanza, in base al rilievo che  a  norma  dell'art.    &#13;
 408  cod. proc. pen. al difensore spetta solo il diritto di "prendere    &#13;
 visione del fascicolo";                                                  &#13;
     che la persona offesa  aveva  nuovamente  presentato  opposizione    &#13;
 alla  richiesta  di  archiviazione,  eccependo, in via principale, la    &#13;
 nullità  del  procedimento  "per  inosservanza   delle   norme   che    &#13;
 consentono al difensore di estrarre copia degli atti del procedimento    &#13;
 una volta che allo stesso sia consentito prenderne visione";             &#13;
     che  il  rimettente  osserva  che  nel caso in esame "non sarebbe    &#13;
 sorto alcun problema se il p.m. si fosse adeguato alla sentenza delle    &#13;
 S.U.  della  S.C.  3/2-14/4/1995,  ric.  Sciancalepore,   ed   avesse    &#13;
 acconsentito  all'estrazione di copia informale" degli atti quando il    &#13;
 difensore ne aveva fatto richiesta;                                      &#13;
     che, invece, il diritto di difesa era stato  "ingiustificatamente    &#13;
 ostacolato  e  compromesso  dal mancato riconoscimento della facoltà    &#13;
 del difensore di estrarre copia degli atti dell'indagine  suppletiva,    &#13;
 tenuto  conto  del  breve margine di tempo per proporre opposizione e    &#13;
 della intuibile difficoltà di stabilire un  tempestivo  contatto  da    &#13;
 parte  del  difensore  (presso  il  cui studio sono, ex art. 33 disp.    &#13;
 att. cod. proc. pen., depositati gli avvisi) con il suo cliente";        &#13;
     che, a sostegno di tali argomentazioni, il  rimettente  rinvia  a    &#13;
 quanto  affermato  in  relazione  ad  un caso analogo da questa Corte    &#13;
 nella sentenza n. 192 del 1997,  menzionando,  in  particolare,  quel    &#13;
 passaggio   della   decisione   ove,   con   riferimento  alla  ratio    &#13;
 dell'istituto, la Corte aveva rilevato che "il deposito degli atti in    &#13;
 cancelleria a disposizione delle parti deve,  di  regola,  comportare    &#13;
 necessariamente, insieme al diritto di prenderne visione, la facoltà    &#13;
 di estrarne copia".                                                      &#13;
   Considerato  che  il giudice rimettente, da un lato rileva che "non    &#13;
 sarebbe sorto alcun problema" se il  pubblico  ministero,  a  seguito    &#13;
 della richiesta del difensore della persona offesa, si fosse adeguato    &#13;
 ad   uno   specifico   precedente   delle  Sezioni  Unite  ed  avesse    &#13;
 acconsentito al rilascio di copia informale degli atti, dall'altro, e    &#13;
 soprattutto, coglie la forza espansiva delle argomentazioni  poste  a    &#13;
 fondamento  della sentenza di questa Corte n. 192 del 1997, mostrando    &#13;
 di non condividere l'interpretazione della norma denunciata  che  sta    &#13;
 alla base del provvedimento reiettivo del pubblico ministero;            &#13;
     che  nel  caso  di specie il rimettente, avendo la disponibilità    &#13;
 degli atti trasmessi dal pubblico ministero unitamente alla richiesta    &#13;
 di archiviazione a norma dell'art. 408, comma  1,  cod.  proc.  pen.,    &#13;
 avrebbe  quindi potuto rilasciare egli stesso le copie richieste alla    &#13;
 luce della sentenza n. 192 del 1997;                                     &#13;
     che,  ove  la  norma  consenta  una  interpretazione  conforme  a    &#13;
 Costituzione, il giudice è tenuto a farla propria, dovendo sollevare    &#13;
 questione  di legittimità costituzionale solo se risulta impossibile    &#13;
 darne  una  interpretazione  costituzionalmente  corretta  (cfr.   in    &#13;
 particolare la sentenza n. 356 del 1996);                                &#13;
     che  l'ordinanza  di  rimessione  prospetta, invece, il dubbio di    &#13;
 costituzionalità  riferendosi  alla  interpretazione  del   pubblico    &#13;
 ministero  senza  condividerla ed è del tutto carente di motivazione    &#13;
 in ordine alle  ragioni  per  cui  il  giudice  non  ha  ritenuto  di    &#13;
 applicare  la  norma denunciata alla stregua dell'interpretazione, da    &#13;
 lui stesso propugnata, conforme a Costituzione;                          &#13;
     che, pertanto, la questione di legittimità  costituzionale  deve    &#13;
 essere dichiarata manifestamente inammissibile.                          &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  dell'art.  408,  comma 3, del codice di    &#13;
 procedura penale, sollevata,  in  riferimento  all'art.  24,  secondo    &#13;
 comma,  della  Costituzione,  dal giudice per le indagini preliminari    &#13;
 del tribunale di Torino, con l'ordinanza in epigrafe.                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'11 gennaio 2000.                               &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Neppi Modona                       &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 17 gennaio 2000.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
