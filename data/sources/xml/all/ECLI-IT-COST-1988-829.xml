<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>829</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:829</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/07/1988</data_decisione>
    <data_deposito>21/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della legge della Regione    &#13;
 Toscana n. 86/80 approvata il 30 giugno  1981,  avente  per  oggetto:    &#13;
 "Contributi  della Regione Toscana al Fondo di solidarietà nazionale    &#13;
 istituito  dalla  Regione  Piemonte",  promosso   con   ricorso   del    &#13;
 Presidente  del Consiglio dei Ministri, notificato il 21 luglio 1981,    &#13;
 depositato in cancelleria il 27 successivo ed iscritto al n.  48  del    &#13;
 registro ricorsi 1981;                                                   &#13;
    Visto l'atto di costituzione della Regione Toscana;                   &#13;
    Udito  nell'udienza  pubblica  del  23  febbraio  1988  il Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Uditi l'Avvocato dello Stato Mario Imponente, per il ricorrente, e    &#13;
 l'Avvocato Fabio Lorenzoni per la Regione;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con ricorso notificato il 21 luglio 1981, il Presidente del    &#13;
 Consiglio  dei  Ministri,  rappresentato  e  difeso   dall'Avvocatura    &#13;
 Generale    dello    Stato,   ha   chiesto   che   venga   dichiarata    &#13;
 l'illegittimità costituzionale della legge  della  Regione  Toscana,    &#13;
 riapprovata, a seguito del rinvio governativo, il 30 giugno 1981, con    &#13;
 la quale è stato disposto  un  contributo  dell'ammontare  di  venti    &#13;
 milioni  di  lire  a  favore del "Comitato regionale di solidarietà"    &#13;
 istituito dalla Regione Piemonte per coordinare attività di soccorso    &#13;
 "in  occasione di avvenimenti, anche di carattere internazionale, che    &#13;
 sollecitino l'intervento concreto della comunità regionale".            &#13;
    Richiamando quanto già rilevato nell'atto di rinvio, l'Avvocatura    &#13;
 dello Stato prospetta  dubbi  di  legittimità  costituzionale  sulla    &#13;
 legge  impugnata  sotto  un  duplice  profilo.  Innanzitutto, perché    &#13;
 violerebbe l'art. 117 Cost., come attuato dall'art. 25 del d.P.R.  24    &#13;
 luglio  1977,  n.  616,  per  il quale, in materia di assistenza e di    &#13;
 beneficenza  pubblica,  le  regioni  hanno   soltanto   funzioni   di    &#13;
 indirizzo,  essendo  affidato  ai  Comuni  l'esercizio delle concrete    &#13;
 funzioni  amministrative.  Con  l'erogazione  operata  con  la  legge    &#13;
 impugnata,   la  Toscana  contravverrebbe  a  tale  norma,  prestando    &#13;
 direttamente un  servizio  assistenziale  nel  quadro  di  iniziative    &#13;
 assunte  anche  da  altre  regioni  per  sostenere  economicamente  i    &#13;
 lavoratori impegnati nella vertenza  FIAT  e  peraltro  già  fruenti    &#13;
 della Cassa integrazione guadagni. In secondo luogo, sempre ad avviso    &#13;
 dell'Avvocatura dello Stato, la stessa legge violerebbe il  principio    &#13;
 di territorialità, in quanto diretta a produrre i suoi effetti al di    &#13;
 là dei confini regionali e a  tutela  di  interessi  localizzati  in    &#13;
 altra  regione,  e  precisamente  a favore di persone che risiedono e    &#13;
 lavorano in Piemonte.                                                    &#13;
    2.  -  Si  è costituita nei termini la Regione Toscana, chiedendo    &#13;
 che le questioni sollevate siano dichiarate non fondate.                 &#13;
    Relativamente  alla  prima censura, la Regione osserva che è vano    &#13;
 richiamarsi  alla  ripartizione  delle  funzioni  amministrative  tra    &#13;
 comuni  e regioni in materia di assistenza e di beneficenza, peraltro    &#13;
 erroneamente   interpretata,    quando    all'esame    della    Corte    &#13;
 costituzionale è sottoposta una legge regionale di spesa. Questa, al    &#13;
 contrario, dovrebbe essere inquadrata nell'ambito  dell'autonomia  di    &#13;
 spesa  posseduta dalle regioni nell'ambito delle competenze garantite    &#13;
 loro  dalla  Costituzione  e  dei  principi  generali  che   regolano    &#13;
 l'esercizio di quell'autonomia, fra i quali rientrerebbe senza dubbio    &#13;
 l'adempimento dei doveri inderogabili di solidarietà.                   &#13;
    Quanto alla pretesa lesione del limite territoriale, la resistente    &#13;
 osserva che se quest'ultimo, impedisce  di  disciplinare  rapporti  o    &#13;
 attività   svolgentisi  fuori  del  territorio  della  regione,  non    &#13;
 escluderebbe, tuttavia, l'erogazione di un beneficio a favore  di  un    &#13;
 soggetto avente sede fuori del medesimo territorio. A seguire la tesi    &#13;
 del ricorrente,  sostiene  la  Regione,  si  arriverebbe  all'assurda    &#13;
 conclusione   che  dovrebbe  essere  vietato  di  disporre  qualsiasi    &#13;
 pagamento a favore  di  qualsiasi  soggetto  avente  sede  fuori  del    &#13;
 territorio regionale.                                                    &#13;
    3.  -  In  prossimità dell'udienza la Regione Toscana ha prodotto    &#13;
 una memoria, con la  quale,  oltre  a  ribadire  gli  argomenti  già    &#13;
 formulati  all'atto della costituzione, precisa che, a suo avviso, la    &#13;
 legge impugnata rispetta il limite territoriale, in quanto, lungi dal    &#13;
 disporre  nei  confronti  di  persone non collegate con il territorio    &#13;
 regionale, contiene in effetti un precetto diretto all'ufficio  della    &#13;
 Regione che deve provvedere all'erogazione.                              &#13;
    In  secondo  luogo,  sempre  a  giudizio  della Regione, il limite    &#13;
 territoriale è ordinariamente inteso  come  riferito  all'ambito  di    &#13;
 validità   degli  atti  legislativi  o  amministrativi  di  ciascuna    &#13;
 regione, non già all'ambito di efficacia, poiché numerose  sono  le    &#13;
 fattispecie,  comunemente  ammesse,  che  producono effetti diretti o    &#13;
 indiretti in ordine a situazioni extra regionali, come nel caso delle    &#13;
 attività   regionali   di  rilevanza  internazionale  (promozionali,    &#13;
 culturali, etc.).                                                        &#13;
    Infine,  va  tenuto presente, secondo la Regione, che nel definire    &#13;
 il  principio  territoriale  la   Corte   costituzionale   ha   fatto    &#13;
 riferimento a parametri diversi dai confini geografici, che investono    &#13;
 la  definizione  delle  materie  di  competenza  regionale  o   degli    &#13;
 interessi nazionali.<diritto>Considerato in diritto</diritto>1.   -   Sottoposta   al   presente   giudizio   di   legittimità    &#13;
 costituzionale è la legge della Regione Toscana, riapprovata  il  30    &#13;
 giugno  1981,  con  la quale si attribuisce al "Comitato regionale di    &#13;
 solidarietà" ivi indicato un contributo di venti  milioni  di  lire.    &#13;
 Tale  legge è impugnata dal Presidente del Consiglio dei Ministri in    &#13;
 quanto ritenuta lesiva:                                                  &#13;
       a) dell'art. 117 Cost., come attuato dall'art. 25 del d.P.R. 24    &#13;
 luglio 1977, n. 616, il  quale,  riservando  ai  comuni  le  funzioni    &#13;
 amministrative   relative  all'organizzazione  e  all'erogazione  dei    &#13;
 servizi di assistenza e di beneficenza pubblica, ne precluderebbe  il    &#13;
 diretto esercizio da parte delle regioni;                                &#13;
       b)   del  principio  di  territorialità,  che,  vincolando  la    &#13;
 potestà legislativa di ciascuna regione ad operare  all'interno  del    &#13;
 territorio  di  propria  spettanza,  ne  vieterebbe  lo svolgimento a    &#13;
 tutela di interessi localizzabili in altra regione.                      &#13;
    2.  - Le anzidette censure non possono essere accolte, poiché né    &#13;
 l'uno né l'altro  dei  limiti  invocati  trovano  applicazione,  nei    &#13;
 termini  prospettati  dal  ricorrente,  nella fattispecie dedotta nel    &#13;
 presente giudizio.                                                       &#13;
    2.1.  -  L'ipotesi di erogazione finanziaria contenuta nella legge    &#13;
 impugnata non  può  essere  ricondotta  al  pur  ampio  concetto  di    &#13;
 assistenza  e  di  beneficenza  pubblica  previsto come oggetto della    &#13;
 potestà legislativa regionale dall'art. 117 Cost.,  nel  significato    &#13;
 precisato  dall'art.  22  del d.P.R. n. 616 del 1977. Anche se appare    &#13;
 giustificato dai motivi di solidarietà sociale,  il  contributo  ivi    &#13;
 previsto  manca dei caratteri strutturali propri delle prestazioni di    &#13;
 assistenza e di beneficenza pubblica, in quanto, anziché  consistere    &#13;
 in  un'erogazione  di  servizi  o  di denaro a favore di singoli o di    &#13;
 gruppi sociali, come richiesto dal predetto art. 22 del d.P.R. n. 616    &#13;
 del  1977,  è dato da una prestazione pecuniaria compiuta una tantum    &#13;
 dalla Regione  Toscana  a  favore  della  Regione  Piemonte  o,  più    &#13;
 precisamente, a favore del Comitato regionale di solidarietà, vale a    &#13;
 dire di un organo della Regione Piemonte che è  stato  istituito  al    &#13;
 fine di promuovere, coordinare e organizzare iniziative di soccorso e    &#13;
 di  aiuto  a  favore  di   popolazioni   o   di   categorie   sociali    &#13;
 particolarmente   colpite   da   avvenimenti,   anche   di  carattere    &#13;
 internazionale, che sollecitino l'intervento concreto della comunità    &#13;
 regionale (cfr. legge Reg. Piemonte 28 gennaio 1982, n. 4, nonché la    &#13;
 deliberazione del Consiglio Regionale del  Piemonte  del  10  gennaio    &#13;
 1980, n. 545-263).                                                       &#13;
    In altre parole, la legge impugnata non è, certo, esercizio della    &#13;
 specifica competenza posseduta dalla Regione Toscana  in  materia  di    &#13;
 assistenza  e  di  beneficenza  pubblica, ma costituisce una forma di    &#13;
 collaborazione e di cooperazione solidaristica della  stessa  Regione    &#13;
 nei   confronti  della  Regione  Piemonte  in  quanto  ente  politico    &#13;
 impegnato,  attraverso  il  proprio  Comitato  di  solidarietà,   in    &#13;
 iniziative  di soccorso e di aiuto a favore di persone danneggiate da    &#13;
 licenziamenti  o  sospensioni  dal  lavoro  occorsi   nel   capoluogo    &#13;
 piemontese. Per tali ragioni, al fine di giudicare della legittimità    &#13;
 costituzionale  della  legge  impugnata,  non  ha  alcuna   rilevanza    &#13;
 domandarsi se, a norma dell'art. 117 Cost., come attuato dall'art. 25    &#13;
 del d.P.R. n. 616 del 1977, la regione  possa  concretamente  erogare    &#13;
 servizi  di  assistenza  o  di  beneficenza pubblica, per il semplice    &#13;
 fatto che il  problema  sottoposto  al  presente  giudizio  consiste,    &#13;
 piuttosto, nel decidere se una regione possa disporre di contributi a    &#13;
 favore di altre regioni per motivi di solidarietà sociale.              &#13;
    2.2. - In relazione a un diverso caso di versamento di fondi della    &#13;
 Regione Trentino Alto-Adige a favore di un istituto di cultura  della    &#13;
 Provincia  di  Trento,  questa  Corte  ha  già  affermato  che  "gli    &#13;
 interessi regionali non sono soltanto quelli puntualmente  rilevabili    &#13;
 dalle  competenze che (la Costituzione o) lo Statuto attribuisce alla    &#13;
 Regione" e che, anzi, "può esser configurato un  interesse  generale    &#13;
 proprio  della  Regione che questa può e deve tutelare" (sent. n. 56    &#13;
 del 1964). Ciò significa, in altre parole,  che,  al  di  là  delle    &#13;
 finalità  in  relazione  alle  quali  le regioni possono svolgere le    &#13;
 proprie competenze legislative e amministrative  nelle  materie  loro    &#13;
 attribuite,  sussistono interessi e fini rispetto ai quali le regioni    &#13;
 stesse possono provvedere nell'esercizio dell'autonomia politica  che    &#13;
 ad  esse  spetta  in  quanto  enti  esponenziali  delle collettività    &#13;
 sociali rappresentate.                                                   &#13;
    Questo  ruolo  di  rappresentanza  generale  degli interessi della    &#13;
 collettività  regionale  e  di  prospettazione  istituzionale  delle    &#13;
 esigenze  e,  persino,  delle aspettative che promanano da tale sfera    &#13;
 comunitaria deriva  alle  singole  regioni  dal  complessivo  disegno    &#13;
 costituzionale   sulle  autonomie  territoriali  (come  ulteriormente    &#13;
 precisato dal d.P.R. n. 616 del 1977) e, in primo luogo, dall'art.  5    &#13;
 Cost.  e  dai  principi  fondamentali  contenuti  nelle  disposizioni    &#13;
 iniziali  della  Costituzione.  Grazie  a  tali  norme,  infatti,  si    &#13;
 afferma,  per un verso, il principio generale che le autonomie locali    &#13;
 costituiscono una  parte  essenziale  dell'articolazione  democratica    &#13;
 dell'ordinamento   unitario  repubblicano  e,  per  altro  verso,  si    &#13;
 attribuisce  a   siffatto   articolato   complesso   di   istituzioni    &#13;
 democratiche  - ora sotto la denominazione di "Repubblica", ora sotto    &#13;
 quella  di  "Italia"  -  l'adempimento  di  una  serie   di   compiti    &#13;
 fondamentali:  compiti  che  vanno  svolti,  oltreché  attraverso le    &#13;
 proprie   competenze,   nella   pienezza   delle   potenzialità   di    &#13;
 partecipazione  comunitaria  di  cui ciascuna istituzione è capace e    &#13;
 che sono diretti a favorire il più elevato  sviluppo  della  persona    &#13;
 umana,  della  solidarietà  sociale  ed  economica, della democrazia    &#13;
 politica, della cultura e del  progresso  tecnico-scientifico,  della    &#13;
 convivenza pacifica tra i popoli (artt. 2, 3, 9 e 11 Cost.).             &#13;
    In  base  a  questi  principi,  riprodotti  dalla  totalità degli    &#13;
 statuti regionali e, per quel che qui concerne, dagli artt. 1-5 dello    &#13;
 Statuto  toscano,  si  legittima, dunque, una presenza politica della    &#13;
 regione, in rapporto allo Stato o anche ad altre regioni, riguardo  a    &#13;
 tutte  le  questioni di interesse della comunità regionale, anche se    &#13;
 queste sorgono in settori  estranei  alle  singole  materie  indicate    &#13;
 nell'art.   117   Cost.  e  si  proiettano  al  di  là  dei  confini    &#13;
 territoriali della regione medesima. Si tratta, più precisamente, di    &#13;
 una  presenza  che,  quando  si  manifesta al di fuori dell'ambito di    &#13;
 validità delle potestà  che  la  regione  vanta  nelle  materie  di    &#13;
 propria  competenza,  si  realizza  attraverso  atti  di proposta, di    &#13;
 stimolo, di iniziative o, anche, attraverso intese, accordi  o  altre    &#13;
 forme  di cooperazione, che possono comunque comportare, sia sotto il    &#13;
 profilo organizzativo  sia  sotto  quello  dell'esecuzione,  spese  a    &#13;
 carico  del  bilancio  regionale,  le quali esigono l'adozione di una    &#13;
 legge ad hoc da parte della regione interessata.                         &#13;
    2.3.  -  Del  resto,  questo  particolare  aspetto  dell'autonomia    &#13;
 regionale non è soltanto affermato nelle disposizioni  di  principio    &#13;
 della  Costituzione  e  degli  Statuti,  ma è anche sviluppato nella    &#13;
 legislazione statale  o  in  atti  equiparati,  nelle  cui  norme  si    &#13;
 rinvengono  numerosissime  fattispecie  che  coinvolgono  le regioni,    &#13;
 nella  loro  qualità  di  enti  rappresentativi   delle   rispettive    &#13;
 comunità  e degli interessi pubblici che vi si agitano, in attività    &#13;
 di rilievo nazionale ed anche internazionale, le quali  si  svolgono,    &#13;
 ovviamente,  in  una  dimensione  che oltrepassa i limiti materiali e    &#13;
 territoriali delle competenze puntualmente  attribuite  alle  singole    &#13;
 regioni.                                                                 &#13;
    Basta   pensare,  a  titolo  esemplificativo,  da  un  lato,  alla    &#13;
 previsione di interventi regionali di promozione  anche  fuori  delle    &#13;
 materie  indicate  dall'art.  117  della Costituzione (artt. 49 e 52,    &#13;
 ultimo comma, del  d.P.R.  n.  616  del  1977)  e,  dall'altro,  alle    &#13;
 molteplici  ipotesi  di  programmazione settoriale coinvolgenti nello    &#13;
 stesso tempo organi statali e organi regionali o, ancora, ai numerosi    &#13;
 momenti  di  coinvolgimento  regionale in iniziative e funzioni dello    &#13;
 Stato, oppure alle  svariate  forme  di  cooperazione  paritaria  tra    &#13;
 diverse  regioni  o  tra  le  regioni  medesime  e  gli enti omologhi    &#13;
 operanti in ordinamenti stranieri.                                       &#13;
    Si  tratta,  come  è evidente, di svariate ipotesi normative che,    &#13;
 pur avendo in comune il fine di permettere alle Regioni  di  operare,    &#13;
 anche   legislativamente,   al   di  fuori  dei  limiti  materiali  e    &#13;
 territoriali fissati dall'art. 117  Cost.,  nell'attuazione  ad  essi    &#13;
 data  dal d.P.R. n. 616 del 1977, sono già state giudicate da questa    &#13;
 Corte come non contrarie a Costituzione (v., soprattutto, le  recenti    &#13;
 sentt. nn. 179 del 1987 e 562 del 1988, nonché già sent. n. 359 del    &#13;
 1985).                                                                   &#13;
    2.4.  -  A  questo  stesso  filone  si  collega la legge regionale    &#13;
 oggetto del presente giudizio di costituzionalità. Nel  disporre  un    &#13;
 contributo  di  solidarietà  a  favore  di  altra regione al fine di    &#13;
 cooperare in un'attività di sostegno a favore di persone sospese dal    &#13;
 lavoro  nel  corso  di  una  vertenza  economica  di rilievo politico    &#13;
 nazionale, il legislatore toscano  ha  ritenuto  di  interpretare  il    &#13;
 sentimento   della   propria   popolazione   attraverso  un  atto  di    &#13;
 liberalità solidaristica fatto a nome dell'intera comunità da  esso    &#13;
 rappresentata.  Emanata  nell'esercizio di tale potere discrezionale,    &#13;
 la legge impugnata non contravviene ai limiti che le sono propri.        &#13;
    Innanzitutto,  non  si  può  dire che essa non sia sorretta da un    &#13;
 interesse  della  propria  comunità   regionale   costituzionalmente    &#13;
 qualificato.  Se, in altra circostanza (sent. n. 56 del 1964), questa    &#13;
 Corte ha rilevato tale mancanza, al contrario, nel caso in questione,    &#13;
 non  si  può  negare l'interesse della collettività regionale della    &#13;
 Toscana  a  manifestare  la  propria  solidarietà  a   una   diversa    &#13;
 collettività  regionale,  già  impegnata  a  sostenere con i propri    &#13;
 mezzi i  suoi  concittadini  in  lotta  contro  la  disoccupazione  e    &#13;
 l'emarginazione dal mondo produttivo, vale a dire contro un male e un    &#13;
 pericolo che possono colpire la cittadinanza in qualsiasi  parte  del    &#13;
 territorio   nazionale.   Non   è,   dunque,  irragionevole  che  il    &#13;
 legislatore toscano abbia voluto partecipare,  con  l'erogazione  del    &#13;
 contributo qui in contestazione, al perseguimento di un fine ritenuto    &#13;
 comune e che, pertanto, giustifica l'atto di solidarietà compiuto in    &#13;
 nome di un dovere che l'art. 2 Cost. definisce come inderogabile.        &#13;
    In  secondo  luogo,  non  si  può  certo  dire  che  l'intervento    &#13;
 finanziario previsto,  in  considerazione  della  quantità  e  della    &#13;
 qualità  della  concreta erogazione, possa esser ritenuto un fattore    &#13;
 di alterazione o di turbativa delle competenze proprie della  regione    &#13;
 destinataria  (v.  sent.  n.  562  del  1988)  o di quelle statali o,    &#13;
 addirittura,  possa  esser  considerato  un   modo   di   svolgimento    &#13;
 surrettizio  di  una  competenza  non  assegnata  ovvero  un  modo di    &#13;
 esercizio  improprio  o  scorretto  di  altre  specifiche  competenze    &#13;
 attribuite  alla  regione  stessa (come è, invece, accaduto nei casi    &#13;
 giudicati con le sentt. nn. 66 del 1961, 56 del 1964, 27 del 1965, 29    &#13;
 del 1968).                                                               &#13;
    Infine,  si  deve  recisamente  negare  che il contributo previsto    &#13;
 dalla legge regionale impugnata  possa  essere  causa  o  fattore  di    &#13;
 disparità  o  di  irragionevoli  interferenze  nell'esercizio  e nel    &#13;
 godimento dei diritti dei cittadini (come, invece, si è  riscontrato    &#13;
 nelle ipotesi giudicate con le sentt. nn. 39 del 1973 e 79 del 1988).    &#13;
    2.5. - Dalle considerazioni già svolte appare chiaro come non sia    &#13;
 appropriato  invocare  il  limite   territoriale   in   relazione   a    &#13;
 fattispecie  normative  come quella regolata dalla legge impugnata e,    &#13;
 più in generale,  con  riferimento  alla  rilevata  posizione  delle    &#13;
 regioni  come  rappresentanti  degli interessi generali della propria    &#13;
 collettività. Le  disposizioni  costituzionali  e  le  pronunzie  di    &#13;
 questa  Corte  precedentemente  ricordate mostrano con tutta evidenza    &#13;
 che, nei limiti appena detti, l'autonomia regionale può  esercitarsi    &#13;
 anche  in forme che si proiettano al di là del territorio proprio di    &#13;
 ciascun ente. La regione, per la Costituzione, non è  una  monade  e    &#13;
 l'adempimento dei doveri inderogabili di solidarietà non può essere    &#13;
 confinato nel ristretto ambito  regionale.  Sicché,  soprattutto  in    &#13;
 relazione  alle  espressioni  dell'autonomia regionale collegate alla    &#13;
 posizione della regione  come  ente  esponenziale  e  rappresentativo    &#13;
 degli  interessi  generali della propria comunità, si deve ammettere    &#13;
 che il principio di territorialità, come non ha escluso anche questa    &#13;
 stessa  Corte  in  una  lontana  sentenza  (n.  58  del  1958) e come    &#13;
 riconosce parte della  dottrina,  possa  subìre  relativizzazioni  o    &#13;
 anche  deroghe,  purché  giustificate, ovviamente, nei termini sopra    &#13;
 detti.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 della legge della Regione Toscana  riapprovata  il  30  giugno  1981,    &#13;
 avente  ad  oggetto:  "Contributo  della  Regione  Toscana  al  fondo    &#13;
 regionale  di  solidarietà  istituito   dalla   Regione   Piemonte",    &#13;
 sollevata  dal  Presidente del Consiglio dei Ministri, con il ricorso    &#13;
 indicato in epigrafe, in riferimento all'art. 117 Cost., come attuato    &#13;
 dall'art.  25 del d.P.R. 24 luglio 1977, n. 616, e per violazione del    &#13;
 principio costituzionale di territorialità.                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 5 luglio 1988.                                &#13;
                          Il Presidente: SAJA                             &#13;
                       Il redattore: BALDASSARRE                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
