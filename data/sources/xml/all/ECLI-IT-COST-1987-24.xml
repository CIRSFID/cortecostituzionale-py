<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>24</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:24</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/01/1987</data_decisione>
    <data_deposito>22/01/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, prof. Ettore GALLO, prof. &#13;
 Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, &#13;
 prof. Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco P. CASAVOLA, prof. Antonio BALDASSARRE, &#13;
 prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 276 (così    &#13;
 come modificato dalla legge 22 dicembre 1960  n.  1579),  277,  primo    &#13;
 comma (così come modificato dall'art. 46 legge 2 luglio 1952 n. 703)    &#13;
 e terzo comma, del r.d. 14 settembre 1931                                &#13;
 n.  1175  (Testo  Unico per la finanza locale) promosso con ordinanza    &#13;
 emessa il 19 gennaio 1979 dalla  Corte  di  Appello  di  Bologna  nel    &#13;
 procedimento  civile  vertente  tra Montalti Carlo e Comune di Cesena    &#13;
 iscritta al n. 632 del registro ordinanze  1979  e  pubblicata  nella    &#13;
 Gazzetta Ufficiale della Repubblica n. 304 dell'anno 1979;               &#13;
    Visto  l'atto  di costituzione di Montalti Carlo nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di consiglio del 12 dicembre 1986 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto   che  la  Corte  di  Appello  di  Bologna  dubita  della    &#13;
 legittimità costituzionale degli artt. 276  (come  modificato  dalla    &#13;
 legge  22  dicembre  1960 n. 1579), 277, primo comma (come modificato    &#13;
 dall'art. 46 della legge 2 luglio 1952 n. 703) e terzo comma del r.d.    &#13;
 14  settembre  1931  n. 1175, nella parte in cui, prevedendo che, per    &#13;
 determinare  l'imponibile  ai  fini  dell'imposta  di   famiglia   in    &#13;
 relazione  ad  un  dato  anno,  il  periodo  di riferimento cui vanno    &#13;
 riconnessi i presupposti dell'imposta stessa  è  quello  dei  dodici    &#13;
 mesi   anteriori   al   termine   di   denuncia,  precluderebbero  la    &#13;
 possibilità di tenere                                                   &#13;
 conto   di   eventuali   diminuzioni   dell'imponibile  nell'anno  di    &#13;
 riferimento  del  tributo  ed  impedirebbero  così,  in   violazione    &#13;
 dell'art.  53  Cost., la correlazione di esso all'effettiva capacità    &#13;
 contributiva del soggetto dell'imposta;                                  &#13;
      che,  ai  fini  dell'applicazione dell'imposta de qua, ormai non    &#13;
 più esistente, entro il 20 settembre di ogni anno il  capo  famiglia    &#13;
 doveva  denunciare i cespiti familiari, in relazione ai quali sarebbe    &#13;
 stato  determinato  l'imponibile  da  tassarsi  secondo  le   tariffe    &#13;
 comunali;                                                                &#13;
      che  erano  esonerati dall'obbligo della denuncia i contribuenti    &#13;
 già  iscritti  a  ruolo,  rispetto  ai  quali   le   condizioni   di    &#13;
 tassabilità  fossero  rimaste invariate (art. 274, comma terzo, r.d.    &#13;
 cit.), potendo essi, altrimenti, denunciare al Comune  le  variazioni    &#13;
 intervenute;                                                             &#13;
      che,  dovendo  la  denuncia  essere  effettuata  entro  la  data    &#13;
 suddetta, in coincidenza della quale il contribuente non era in grado    &#13;
 di conoscere con esattezza la situazione economica familiare relativa    &#13;
 all'intero anno, si rendeva necessario il riferimento all'anno solare    &#13;
 precedente a quello in cui la denuncia stessa era stata presentata;      &#13;
      che,   sulla   base   di   questa   o,   in  mancanza  di  essa,    &#13;
 dell'accertamento dell'ufficio  e,  per  quanto  riferito,  tenendosi    &#13;
 conto  delle  variazioni indicate dallo stesso contribuente, venivano    &#13;
 compilati i ruoli e gli avvisi di accertamento  entro  il  30  giugno    &#13;
 dell'anno  successivo  a  quello in cui era stata o si sarebbe dovuta    &#13;
 presentare la denuncia:                                                  &#13;
      che  tale  essendo  la  disciplina dell'imposta in questione, è    &#13;
 evidente che non sussiste la lamentata violazione dell'art. 53 Cost.,    &#13;
 in  quanto  la  determinazione  dell'imposta  risultava adeguata alla    &#13;
 capacità contributiva del soggetto passivo della stessa;                &#13;
      che,   invero,   non   sussistevano   presunzioni   assolute  di    &#13;
 titolarità  di  cespiti  in  base  ai  quali   era   effettuata   la    &#13;
 determinazione   dell'imponibile,   essendo   sempre   consentito  al    &#13;
 contribuente denunciare le variazioni della consistenza  dei  cespiti    &#13;
 stessi;                                                                  &#13;
      che,  pertanto,  non  è  pertinente  il  richiamo al precedente    &#13;
 giurisprudenziale di questa Corte (sent. n. 200/76, che ha dichiarato    &#13;
 l'illegittimità  costituzionale  di  una  norma  fiscale  che poneva    &#13;
 presunzioni assolute di redditività a danno del contribuente,  senza    &#13;
 che allo stesso fosse possibile fornire alcuna prova contraria);         &#13;
      che,  invece, vanno, in materia, ricordate altre decisioni della    &#13;
 Corte (n. 77/67; n. 129/69) che hanno ritenuto la legittimità  delle    &#13;
 presunzioni  fiscali  sempre  che  esse, come nella specie, non siano    &#13;
 assolute e conservino al  contribuente  la  facoltà  di  provare  le    &#13;
 avvenute diminuzioni o, perfino, la cessazione del reddito;              &#13;
      che,  infine, non rileva, per la stessa, la sola circostanza del    &#13;
 riferimento alla possidenza di cespiti nell'anno solare precedente la    &#13;
 denuncia,  in  quanto  detto  sistema  non è, in astratto, contrario    &#13;
 all'invocato precetto costituzionale ed è in vigore anche per  altri    &#13;
 tipi di imposte (es. I.R.P.E.F.);</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 dichiara   manifestamente  infondata  la  questione  di  legittimità    &#13;
 costituzionale degli artt. 276, modificato dalla  legge  22  dicembre    &#13;
 1960 n. 1579, 277, comma primo modificato dall'art. 46 legge 2 luglio    &#13;
 1952, n. 703, e terzo, r.d. 14  settembre  1931  n.  1175,  sollevata    &#13;
 dalla  Corte  di Appello di Bologna in riferimento all'art. 53 Cost.,    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte Costituzionale, Palazzo della Consulta, il 16 gennaio 1987.        &#13;
                    Il Presidente: LA PERGOLA                             &#13;
                     Il Redattore: GRECO                                  &#13;
    Depositata in cancelleria il 22 gennaio 1987.                         &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
