<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>72</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:72</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1974</data_decisione>
    <data_deposito>20/03/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - &#13;
 Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 2, secondo  &#13;
 comma, della legge 17 ottobre 1967, n. 974  (Trattamento  pensionistico  &#13;
 dei  congiunti dei militari e dei dipendenti civili dello Stato vittime  &#13;
 di azioni terroristiche o criminose e  dei  congiunti  dei  caduti  per  &#13;
 cause  di  servizio),  promosso  con ordinanza emessa il 17 maggio 1971  &#13;
 dalla Corte dei conti - sezione IV pensioni militari - sul  ricorso  di  &#13;
 Cimino  Rosa  contro  il Ministero della difesa, iscritta al n. 438 del  &#13;
 registro ordinanze 1971 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n.  16 del 19 gennaio 1972.                                   &#13;
     Udito  nella  camera  di  consiglio  del 10 gennaio 1974 il Giudice  &#13;
 relatore Giuseppe Verzì.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Rosa Cimino ha impugnato avanti la Corte dei  conti  -  sezione  IV  &#13;
 giurisdizionale  (pensioni  militari) - la nota 19 novembre 1960 con la  &#13;
 quale il Ministero della difesa ha respinto l'istanza prodotta da  essa  &#13;
 Cimino  intesa  ad  ottenere  - in applicazione della legge 15 febbraio  &#13;
 1958, n.   46 (art. 12)  -  la  pensione  privilegiata  indiretta  già  &#13;
 attribuita  alla  di  lei defunta madre, Rosa Marante vedova Cimino, in  &#13;
 dipendenza della morte per infermità dipendente da causa  di  servizio  &#13;
 del  figlio  Nunzio, sergente in rafferma dell'esercito, avvenuta il 25  &#13;
 novembre 1930.                                                           &#13;
     Con ordinanza 17 maggio 1971,  la  Corte  dei  conti  -  dopo  aver  &#13;
 premesso  che  non  poteva riconoscersi alla ricorrente il diritto alla  &#13;
 pensione privilegiata indiretta, già attribuita alla madre, in  quanto  &#13;
 la  invocata  legge  n. 46 del 1958 attua il principio della esclusione  &#13;
 tra soggetti appartenenti a categorie  diverse  (madre  e  sorella  del  &#13;
 militare  deceduto),  ha osservato che la legge 17 ottobre 1967, n. 974  &#13;
 ha esteso le disposizioni in materia di pensioni di guerra ai congiunti  &#13;
 dei militari caduti per causa di servizio  o  deceduti  per  infermità  &#13;
 contratta od aggravata per causa di servizio, ma la ricorrente non può  &#13;
 godere  neppure  di  questo beneficio in quanto, secondo l'art. 2 della  &#13;
 legge, le  nuove  disposizioni  si  applicano  per  "tutti  gli  eventi  &#13;
 verificatisi dopo la cessazione della guerra 1940 - 1945" e, per quelli  &#13;
 anteriori,  limitatamente  ai  congiunti  dei  militari  di  leva,  con  &#13;
 esclusione  cioè,  come  nella  specie,  dei congiunti dei militari di  &#13;
 carriera.                                                                &#13;
     Del secondo comma di detto  art.  2,  la  difesa  della  Cimino  ha  &#13;
 eccepito però l'illegittimità costituzionale per contrasto con l'art.  &#13;
 3  della  Costituzione.  E  la  Corte,  ritenuta  la rilevanza e la non  &#13;
 manifesta infondatezza della questione stessa, ha accolto l'istanza.     &#13;
     Nel giudizio avanti questa Corte non vi è  stata  costituzione  di  &#13;
 parti né intervento del Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte  dei  conti  denunzia,  per  violazione  del principio di  &#13;
 uguaglianza, l'art. 2, comma secondo, della legge 17 ottobre  1967,  n.  &#13;
 974,  in quanto limita ai congiunti dei militari di leva la concessione  &#13;
 della pensione privilegiata ordinaria secondo le norme  sulle  pensioni  &#13;
 di  guerra  "per  tutti  gli  eventi verificatisi antecedentemente alla  &#13;
 cessazione della guerra 1940 - 1945",  determinando  un  ingiustificato  &#13;
 trattamento   differenziato  rispetto  ai  congiunti  dei  militari  di  &#13;
 carriera, ugualmente deceduti per causa di servizio.                     &#13;
     La questione è fondata                                              &#13;
     Dopo aver attribuito ai congiunti dei militari caduti per causa  di  &#13;
 servizio  o  deceduti per infermità contratta o aggravata per causa di  &#13;
 servizio, la pensione privilegiata  ordinaria  "nella  misura  ed  alle  &#13;
 condizioni previste dalle disposizioni in materia di pensioni di guerra  &#13;
 (art.  1)",  la  legge  n.  974  del  1967  ha  stabilito  che le nuove  &#13;
 disposizioni hanno effetto dalla data di entrata in vigore della  legge  &#13;
 e  che  esse  si  applicano  per  tutti gli eventi verificatisi dopo la  &#13;
 cessazione della guerra 1940 - 1945 (art. 2, comma primo).  Il  secondo  &#13;
 comma  dello  stesso  articolo,  accogliendo un emendamento proposto in  &#13;
 sede di discussione parlamentare, estende soltanto per i congiunti  dei  &#13;
 militari di leva l'applicabilità della nuova legge a "tutti gli eventi  &#13;
 già  verificatisi" cioè anche agli eventi antecedenti alla cessazione  &#13;
 della guerra 1940 - 1945. Dall'esame dei due commi deriva  quindi  che,  &#13;
 per gli eventi verificatisi dopo la guerra 1940 - 1945, i congiunti dei  &#13;
 militari,  sia  di  carriera  che  di  leva,  sono soggetti alla stessa  &#13;
 disciplina, mentre, per gli eventi verificatisi  in  epoca  precedente,  &#13;
 essi  hanno  un  trattamento  differenziato,  rispetto  al quale non si  &#13;
 rinviene alcuna  giustificazione.  Come  bene  osserva  l'ordinanza  di  &#13;
 rimessione,  trattasi  in  entrambi  i  casi  di  congiunti di militari  &#13;
 deceduti nell'adempimento del proprio dovere, e quindi meritevoli della  &#13;
 medesima considerazione. E la disparità di trattamento non fondata  su  &#13;
 presupposti  logici  ed  obbiettivi, che ne giustifichino razionalmente  &#13;
 l'adozione, importa l'illegittimità della norma.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale   dell'art.   2,   comma  &#13;
 secondo, della legge 17 ottobre 1967, n. 974 (Trattamento pensionistico  &#13;
 dei  congiunti dei militari e dei dipendenti civili dello Stato vittime  &#13;
 di azioni terroristiche o criminose e  dei  congiunti  dei  caduti  per  &#13;
 cause  di  servizio),  in  quanto  esclude  i congiunti dei militari di  &#13;
 carriera dal beneficio concesso per eventi  verificatisi  anteriormente  &#13;
 alla cessazione della guerra 1940 - 1945.                                &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 7 marzo 1974.           &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
