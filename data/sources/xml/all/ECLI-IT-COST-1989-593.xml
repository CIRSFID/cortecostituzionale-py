<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>593</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:593</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/12/1989</data_decisione>
    <data_deposito>29/12/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 41- bis del    &#13;
 codice di procedura penale, approvato con regio  decreto  19  ottobre    &#13;
 1930,  n.  1399, introdotto con legge 22 dicembre 1980, n. 879 (Norme    &#13;
 sulla connessione e sulla  competenza  nei  procedimenti  relativi  a    &#13;
 magistrati  e  nei  casi di rimessione), e 11, primo e secondo comma,    &#13;
 del nuovo  codice  di  procedura  penale,  approvato  con  d.P.R.  24    &#13;
 settembre  1988,  n.  447,  promosso con ordinanza emessa il 3 aprile    &#13;
 1989 dal Giudice  Istruttore  presso  il  Tribunale  di  Bologna  nel    &#13;
 procedimento  penale  a  carico di Fanton Martino, iscritta al n. 366    &#13;
 del registro ordinanze 1989 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 35, prima serie speciale, dell'anno 1989;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 16 novembre 1989 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che, nel corso di un procedimento penale per calunnia in    &#13;
 danno  di  un  sostituto  procuratore  della  Repubblica  presso   il    &#13;
 Tribunale di Firenze, il Giudice Istruttore del Tribunale di Bologna,    &#13;
 con ordinanza in data  3  aprile  1989,  ha  sollevato  questione  di    &#13;
 legittimità  costituzionale  degli  artt.  41-  bis  del  codice  di    &#13;
 procedura penale, approvato con regio decreto  19  ottobre  1930,  n.    &#13;
 1399,  e  11,  primo  e  secondo comma, del nuovo codice di procedura    &#13;
 penale,  approvato  con  d.P.R.  24  settembre  1988,  n.   447,   in    &#13;
 riferimento agli artt. 3, 97 e 101 della Costituzione, nella parte in    &#13;
 cui  dette  norme  assegnano  ad  uffici  giudiziari  (nella  specie,    &#13;
 Tribunale  e  Corte  d'Appello  di  Bologna  e Firenze) la competenza    &#13;
 territoriale  reciproca  per  i   procedimenti   penali   concernenti    &#13;
 magistrati ad essi addetti;                                              &#13;
      che,  in  particolare,  il  giudice  a quo ha rilevato che, tale    &#13;
 essendo il regime della  competenza  territoriale  in  materia,  può    &#13;
 verificarsi  che un medesimo magistrato, imputato o persona offesa in    &#13;
 un  procedimento  pendente  presso  l'altra  sede,  esplichi,   nella    &#13;
 propria, funzioni di giudice in un procedimento in cui sia imputato o    &#13;
 persona offesa quello stesso  collega  che  esercita  o  concorre  ad    &#13;
 esercitare funzioni giudicanti nella sede ove pende il primo di detti    &#13;
 procedimenti, con conseguente pericolo di condizionamento psicologico    &#13;
 e    concreto    pregiudizio    dell'indipendenza   del   giudice   e    &#13;
 dell'imparzialità delle sue decisioni, in  violazione  dei  principi    &#13;
 costituzionali  di  uguaglianza,  di  soggezione  del  giudice stesso    &#13;
 soltanto alla legge e di imparzialità nell'esercizio delle pubbliche    &#13;
 funzioni;                                                                &#13;
      che   nel   giudizio  davanti  a  questa  Corte  è  intervenuta    &#13;
 l'Avvocatura Generale dello Stato, in rappresentanza  del  Presidente    &#13;
 del Consiglio dei ministri, che ha concluso per l'inammissibilità e,    &#13;
 comunque, per l'infondatezza della questione;                            &#13;
    Considerato   che   questa  Corte  ha  già  affermato  che  nelle    &#13;
 situazioni in cui possa sorgere il  dubbio  del  verificarsi,  per  i    &#13;
 rapporti interpersonali tra giudici, di una turbativa della serenità    &#13;
 e imparzialità degli stessi, rientra nella esclusiva competenza  del    &#13;
 legislatore  statuire se ed in quale misura i rapporti che si creano,    &#13;
 nell'ambito della organizzazione giudiziaria, tra  organo  e  singoli    &#13;
 influiscano sulla determinazione della competenza;                       &#13;
      che  allo  stesso  legislatore  spetta  la  determinazione delle    &#13;
 soluzioni più idonee a garantire l'indipendenza  dei  giudici  e  il    &#13;
 prestigio della magistratura;                                            &#13;
      che le dette scelte, essendo riservate alla discrezionalità del    &#13;
 legislatore, sono insindacabili nel giudizio di costituzionalità  se    &#13;
 non  concretano dei meri arbitri (sentenza n. 232 del 1984; ordinanze    &#13;
 nn. 285 del 1985, 164 e 165 del 1987, 261 del 1989);                     &#13;
      che gli stessi principi trovano applicazione nella questione ora    &#13;
 sollevata per la cui soluzione si invoca dal remittente  l'intervento    &#13;
 additivo  della  Corte,  diretto  ad  apprestare,  in sostituzione di    &#13;
 quello vigente, un diverso sistema tra i vari ipotizzabili di  scelta    &#13;
 del giudice competente;                                                  &#13;
      che, pertanto, la questione è manifestamente inammissibile;        &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità  costituzionale  degli  artt.  41-  bis  del  codice  di    &#13;
 procedura  penale  approvato  con  regio  decreto 19 ottobre 1930, n.    &#13;
 1399, introdotto con legge 22 dicembre  1980,  n.  879  (Norme  sulla    &#13;
 connessione e sulla competenza nei procedimenti relativi a magistrati    &#13;
 e nei casi di rimessione), e 11, primo e  secondo  comma,  del  nuovo    &#13;
 codice  di  procedura penale, approvato con d.P.R. 24 settembre 1988,    &#13;
 n. 447, in riferimento agli artt. 3, 97  e  101  della  Costituzione,    &#13;
 sollevata  dal  Giudice Istruttore presso il Tribunale di Bologna con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 dicembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 29 dicembre 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
