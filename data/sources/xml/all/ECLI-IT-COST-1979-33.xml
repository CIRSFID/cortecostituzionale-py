<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>33</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:33</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Arnaldo Maccarone</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/05/1979</data_decisione>
    <data_deposito>25/05/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art.  131  (recte:  &#13;
 631),  ultimo  cpv.,  del  codice  di    procedura penale, promosso con  &#13;
 ordinanza emessa il 24 settembre 1976 dalla Corte  d'appello di Napoli,  &#13;
 nel procedimento penale a carico di Della Medaglia  Pasquale,  iscritta  &#13;
 al    n.  239  del  registro ordinanze 1977 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 176 del 29 giugno 1977.                    &#13;
     Udito nella camera di  consiglio  del  22  marzo  1979  il  Giudice  &#13;
 relatore Arnaldo Maccarone.                                              &#13;
     Ritenuto che con ordinanza emessa il 24 settembre 1976 (pervenuta a  &#13;
 questa  Corte  il  10    maggio  1977)  la Corte d'appello di Napoli ha  &#13;
 sollevato, in riferimento agli artt. 3, 24 e 27,  secondo comma,  della  &#13;
 Costituzione,  questione  di legittimità costituzionale dell'art. 631,  &#13;
 ultimo  comma,  c.p.p.,  il  quale  dispone  che  il  ricorso   avverso  &#13;
 l'ordinanza con cui il giudice  decide l'incidente di esecuzione non ha  &#13;
 effetto  sospensivo, aggiungendo peraltro che  l'esecuzione può essere  &#13;
 sospesa dal giudice che ha emesso l'ordinanza;                           &#13;
     che,  a  sostegno  della   proposta   questione   di   legittimità  &#13;
 costituzionale si assume, da parte del  giudice a quo, che le decisioni  &#13;
 relative  agli incidenti di esecuzione, cui la disposizione  denunziata  &#13;
 si riferisce, spesso involgono, come le sentenze, situazioni  attinenti  &#13;
 alla  libertà  personale,  sicché non sarebbe giustificata, per esse,  &#13;
 una disciplina diversa da quella stabilita  in via  generale  dall'art.  &#13;
 205  c.p.p.,  il  quale  appunto stabilisce che "durante il termine per  &#13;
 impugnare un provvedimento e durante  il  giudizio  sulla  impugnazione  &#13;
 l'esecuzione è sospesa  salvo che la legge disponga altrimenti".        &#13;
     Considerato  che  questa  Corte con la sentenza n. 112 del 5 aprile  &#13;
 1974 (di cui non si tiene  alcun conto nell'ordinanza di rimessione) ha  &#13;
 già dichiarato non fondata la questione con  riferimento agli artt. 3,  &#13;
 primo comma, 13, primo e secondo comma, e 24,  secondo  comma,    della  &#13;
 Costituzione osservando, tra l'altro, che nella disposizione denunziata  &#13;
 l'esclusione      dell'effetto   sospensivo  dell'impugnazione  "appare  &#13;
 pienamente giustificata, trattandosi del  procedimento per la decisione  &#13;
 sugli incidenti di  esecuzione,  da  parte  del  giudice  competente  a  &#13;
 provvedere  circa  l'esecuzione penale", tanto più che "l'esecutività  &#13;
 dell'ordinanza può operare non solo a danno ma anche a  vantaggio  del  &#13;
 soggetto interessato";                                                   &#13;
     che l'art. 27, secondo comma, della Costituzione, come questa Corte  &#13;
 ha  avuto occasione di affermare (sia pure a fini diversi da quello che  &#13;
 oggi viene in considerazione), ha esclusivo riferimento alla  posizione  &#13;
 dell'imputato,  (al  quale  ha  inteso  garantire la presunzione di non  &#13;
 colpevolezza per tutto lo svolgimento del rapporto processuale)  e  non  &#13;
 è  applicabile  al  condannato,  la  cui  condizione  giuridica non si  &#13;
 ricollega al processo ma segue la sua definizione  (sent.  n.  124  del  &#13;
 1972);                                                                   &#13;
     che,  pertanto, la violazione di detta disposizione non può venire  &#13;
 in considerazione nel caso  di  specie,  posto  che  gli  incidenti  di  &#13;
 esecuzione presuppongono il passaggio in giudicato della sentenza;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale dell'art. 631 c.p.p. sollevata  -  in  riferimento  agli  &#13;
 artt.  3,  24  e  27,  secondo  comma, della Costituzione - dalla Corte  &#13;
 d'appello di Napoli con l'ordinanza in epigrafe.                         &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 maggio 1979.          &#13;
                                   F.to:   LEONETTO   AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA -  GUIDO  ASTUTI  -  MICHELE  &#13;
                                   ROSSANO   -  ANTONINO  DE  STEFANO  -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO REALE -  BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO MA LAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
