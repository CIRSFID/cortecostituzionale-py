<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>137</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:137</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/03/1994</data_decisione>
    <data_deposito>13/04/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 avv. Massimo VARI, dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 30 e 31 del    &#13;
 codice penale militare di pace promosso con  ordinanza  emessa  il  6    &#13;
 luglio  1993 dal Tribunale militare di Padova nel procedimento penale    &#13;
 a carico di Piccolomini Giovanni iscritta  al  n.  604  del  registro    &#13;
 ordinanze 1993 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 42, prima serie speciale, dell'anno 1993;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella Camera di  consiglio  del  9  marzo  1994  il  Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto  che  il  Tribunale  militare  di  Padova,  all'esito del    &#13;
 dibattimento svoltosi nei confronti di un sottufficiale  imputato  di    &#13;
 due  distinti episodi di diserzione, dovendo pervenire a una sentenza    &#13;
 di condanna poiché a carico dell'imputato risultavano  sicure  prove    &#13;
 di  responsabilità  e  non  potendo  disporre in favore dello stesso    &#13;
 imputato,  in  considerazione  dei   suoi   precedenti   penali,   la    &#13;
 sospensione  condizionale  della  pena,  ha sollevato, in riferimento    &#13;
 agli artt. 3 e 52, ultimo comma,  della  Costituzione,  questione  di    &#13;
 legittimità costituzionale degli artt. 30 e 31 c.p.m.p., nella parte    &#13;
 in  cui prevedono per i sottufficiali e i graduati di truppa una pena    &#13;
 accessoria   diversa   da   quella   prevista   per   gli   ufficiali    &#13;
 (rispettivamente, sospensione dal grado e sospensione dall'impiego);     &#13;
      che,  secondo  il giudice a quo, nei confronti dell'imputato non    &#13;
 sarebbe possibile applicare la pena accessoria  della  rimozione  dal    &#13;
 grado,  prevista  dall'art.  156  c.p.m.p.  per  il caso in cui venga    &#13;
 accertata la responsabilità penale per il reato  di  diserzione,  in    &#13;
 quanto  l'art.  29  c.p.m.p., il quale include la rimozione dal grado    &#13;
 tra le pene accessorie militari,  deve  ritenersi  ormai  abrogato  a    &#13;
 seguito della entrata in vigore della legge 7 febbraio 1990, n. 19, e    &#13;
 in particolare dell'art. 9 della stessa;                                 &#13;
      che,   conseguentemente,   dovendosi   applicare  nei  confronti    &#13;
 dell'imputato  la  pena  accessoria  della  sospensione  dal   grado,    &#13;
 risulterebbe  evidente  la  diversità di trattamento riservata dalle    &#13;
 disposizioni impugnate ai sottufficiali e agli ufficiali, dal momento    &#13;
 che per questi ultimi la pena accessoria è quella della  sospensione    &#13;
 dall'impiego  e  non  dal  grado,  mentre per i sottufficiali la pena    &#13;
 accessoria, ingiustificatamente  più  afflittiva,  è  quella  della    &#13;
 sospensione dal grado;                                                   &#13;
      che,  è  intervenuto  nel  presente  giudizio il Presidente del    &#13;
 Consiglio dei ministri, chiedendo che  la  questione  sia  dichiarata    &#13;
 inammissibile ovvero non fondata;                                        &#13;
      che,  in particolare, quanto alla inammissibilità, l'Avvocatura    &#13;
 generale dello Stato rileva  che  il  giudice  a  quo  muove  da  una    &#13;
 premessa  errata,  dovendosi  escludere  che  l'art.  9 della legge 7    &#13;
 febbraio 1990, n. 19,  abbia  abrogato  le  disposizioni  del  codice    &#13;
 penale militare di pace concernenti la individuazione e la disciplina    &#13;
 delle   pene   accessorie  militari,  dal  momento  che  le  sanzioni    &#13;
 disciplinari, alle quali soltanto si riferisce l'art. 9 citato,  sono    &#13;
 sanzioni  amministrative,  mentre  le  pene  accessorie  sono  vere e    &#13;
 proprie pene criminali, subordinate, quanto alla loro  efficacia,  al    &#13;
 passaggio  in giudicato della sentenza recante la condanna alla quale    &#13;
 accedono;                                                                &#13;
      che, comunque,  la  diversità  di  trattamento  riservata  agli    &#13;
 ufficiali,  da  un  lato, e ai sottufficiali e ai graduati di truppa,    &#13;
 dall'altro, discende dalla diversità  dello  stato  giuridico  delle    &#13;
 categorie  considerate, dal momento che solo per gli ufficiali, e non    &#13;
 anche per i sottufficiali, è stabilito che il grado sia indipendente    &#13;
 dall'impiego (art. 4 della legge  10  aprile  1954,  n.  113)  e  dal    &#13;
 momento  che,  conseguentemente,  solo per gli ufficiali, e non anche    &#13;
 per  i  sottufficiali,   è   possibile   disporre   la   sospensione    &#13;
 dall'impiego e non dal grado;                                            &#13;
    Considerato  che  il  Tribunale  militare  di  Padova ha sollevato    &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3    &#13;
 e 52, ultimo comma, della Costituzione, degli artt. 30 e 31 c.p.m.p.,    &#13;
 nella parte in cui prevedono per i  sottufficiali  e  i  graduati  di    &#13;
 truppa  una  pena  accessoria  diversa  da  quella  prevista  per gli    &#13;
 ufficiali;                                                               &#13;
      che la questione si fonda sul presupposto  che  l'art.  9  della    &#13;
 legge 7 febbraio 1990, n. 19, avrebbe abrogato l'art. 29 c.p.m.p., il    &#13;
 quale  disciplina  la  pena  accessoria  militare della rimozione dal    &#13;
 grado, pena accessoria che  sarebbe  applicabile  immediatamente  nei    &#13;
 confronti  dell'imputato  nel giudizio a quo, quale conseguenza della    &#13;
 condanna  per  il  reato  di  diserzione  (art.  156  c.p.m.p.),  non    &#13;
 ricorrendo  le  condizioni  per  la  concessione  del beneficio della    &#13;
 sospensione condizionale della pena;                                     &#13;
      che, in altri  termini,  le  disposizioni  impugnate,  le  quali    &#13;
 disciplinano,  rispettivamente,  le  pene accessorie temporanee della    &#13;
 sospensione dall'impiego e della  sospensione  dal  grado,  sarebbero    &#13;
 applicabili  nel  giudizio  a quo, in quanto non potrebbe più essere    &#13;
 disposta la pena accessoria perpetua della rimozione dal grado;          &#13;
      che, peraltro, a parte ogni rilievo in ordine alla  formulazione    &#13;
 della  questione  e,  in  particolare,  in ordine alla individuazione    &#13;
 delle disposizioni impugnate, l'assunto dal quale  muove  l'ordinanza    &#13;
 di rimessione non può essere condiviso, dal momento che, come questa    &#13;
 Corte  ha già avuto modo di affermare, sia pure con riferimento alla    &#13;
 pena accessoria della  interdizione  perpetua  dai  pubblici  uffici,    &#13;
 l'art.  9  della  legge  7  febbraio 1990, n. 19 non ha in alcun modo    &#13;
 inciso sulla disciplina delle pene accessorie  e,  per  quel  che  in    &#13;
 questo   giudizio   rileva,  della  pena  accessoria  militare  della    &#13;
 rimozione dal grado (sent. n. 197 del 1993);                             &#13;
      che, conseguentemente,  poiché  nel  giudizio  a  quo,  ove  il    &#13;
 Tribunale  militare  di  Padova  dovesse pervenire ad una sentenza di    &#13;
 condanna  del  sottufficiale  imputato  del  reato   di   diserzione,    &#13;
 dovrebbe,  secondo  quanto  previsto  dall'art.  156  c.p.m.p., farsi    &#13;
 applicazione dell'art. 29 dello stesso codice (il  quale,  a  seguito    &#13;
 della  decisione  di  questa  Corte n. 258 del 1993, non prevede più    &#13;
 alcuna distinzione, quanto alla entità della pena  principale  della    &#13;
 reclusione militare cui la pena della rimozione dal grado accede, tra    &#13;
 la  posizione degli ufficiali e quella dei sottufficiali) e non anche    &#13;
 delle   disposizioni   impugnate   (o,   più   precisamente,   della    &#13;
 disposizione di cui all'art. 31 c.p.m.p.);                               &#13;
      che,  pertanto,  la  questione  di  legittimità  costituzionale    &#13;
 sollevata dal Tribunale militare di  Padova  deve  essere  dichiarata    &#13;
 manifestamente  inammissibile,  in  quanto ha ad oggetto disposizioni    &#13;
 che non sono applicabili nel giudizio a quo;                             &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e  9  delle  Norme  integrative  per i giudizi davanti alla Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara manifestamente inammissibile la questione di  legittimità    &#13;
 costituzionale   degli   artt.   30  e  31  c.p.m.p.,  sollevata,  in    &#13;
 riferimento agli artt. 3 e 52, ultimo comma, della Costituzione,  dal    &#13;
 Tribunale militare di Padova con l'ordinanza indicata in epigrafe.       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 25 marzo 1994.                                &#13;
                        Il presidente: CASAVOLA                           &#13;
                       Il redattore: BALDASSARRE                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 13 aprile 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
