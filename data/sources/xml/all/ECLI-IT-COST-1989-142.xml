<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>142</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:142</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1989</data_decisione>
    <data_deposito>21/03/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 1, secondo    &#13;
 comma, della legge 9 gennaio 1963, n. 9 (Elevazione  dei  trattamenti    &#13;
 minimi  di  pensione  e  riordinamento  delle  norme  in  materia  di    &#13;
 previdenza dei coltivatori diretti e dei coloni e mezzadri), promosso    &#13;
 con  ordinanza  emessa  il 19 ottobre 1988 dal Tribunale di Pordenone    &#13;
 nel  procedimento  civile  vertente  tra   l'I.N.P.S.   e   Cesaratto    &#13;
 Margherita,  iscritta  al  n.  773  del  registro  ordinanze  1988  e    &#13;
 pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  1,  prima    &#13;
 serie speciale, dell'anno 1989.                                          &#13;
    Udito  nella  camera  di consiglio del 22 febbraio 1989 il Giudice    &#13;
 relatore Francesco Paolo Casavola.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel  corso  di  un  giudizio  in cui l'I.N.P.S. aveva appellato la    &#13;
 sentenza di primo grado affermativa del diritto  della  ricorrente  -    &#13;
 titolare  di  pensione  diretta  a  carico  della  Gestione  speciale    &#13;
 coltivatori diretti - ad  ottenere  l'integrazione  al  minimo  della    &#13;
 pensione  di  riversibilità  erogata  dalla  medesima  Gestione,  il    &#13;
 Tribunale di Pordenone, con ordinanza emessa il 19 ottobre  1988,  ha    &#13;
 sollevato,  in  relazione all'art. 3 della Costituzione, questione di    &#13;
 legittimità costituzionale dell'art. 1, secondo comma, della legge 9    &#13;
 gennaio 1963, n. 9.                                                      &#13;
    Il  giudice a quo denuncia la disposizione nella parte in cui essa    &#13;
 preclude l'integrazione al minimo della pensione  di  riversibilità,    &#13;
 erogata  dal  Fondo  speciale  per  i coltivatori diretti, mezzadri e    &#13;
 coloni, per i titolari di pensione  diretta  di  vecchiaia  a  carico    &#13;
 dello  stesso Fondo allorché, per effetto del cumulo, venga superato    &#13;
 il minimo garantito dalla legge.<diritto>Considerato in diritto</diritto>Il Tribunale di Pordenone sospetta d'illegittimità costituzionale    &#13;
 l'art. 1, secondo comma, della legge 9 gennaio 1963, n. 9 (Elevazione    &#13;
 dei  trattamenti  minimi  di  pensione e riordinamento delle norme in    &#13;
 materia  di  previdenza  dei  coltivatori  diretti  e  dei  coloni  e    &#13;
 mezzadri),  in  quanto  non  consente  l'integrazione al minimo della    &#13;
 pensione  di  riversibilità  a  carico  del   Fondo   speciale   per    &#13;
 coltivatori  diretti,  mezzadri  e  coloni,  in  caso  di  cumulo con    &#13;
 pensione di vecchiaia a carico della stessa Gestione.                    &#13;
    La questione è fondata.                                              &#13;
    La norma impugnata è stata dichiarata illegittima in relazione al    &#13;
 cumulo tra pensione di  riversibilità  e  pensione  d'invalidità  a    &#13;
 carico del Fondo in argomento con sentenza n. 1144 del 1988.             &#13;
    Nella  decisione  citata,  nonché  in numerose altre pronunce, la    &#13;
 Corte  ha  perseguito  l'intento  di   eliminare   ogni   preclusione    &#13;
 all'integrazione al minimo per i titolari di più pensioni (allorché    &#13;
 per  effetto  del  cumulo  venisse  superato  il  trattamento  minimo    &#13;
 garantito)   "così   rendendo   possibile  la  titolarità  di  più    &#13;
 integrazioni al minimo sino all'entrata in vigore  del  decreto-legge    &#13;
 12 settembre 1983, n. 463, convertito, con modificazioni, nella legge    &#13;
 11 novembre 1983, n. 638, che ha disciplinato  ex  novo  la  materia"    &#13;
 (sentenza n. 1086 del 1988).                                             &#13;
    Anche nel caso in esame va seguita la medesima ratio, in quanto le    &#13;
 residue applicazioni della  norma  denunziata  risultano  chiaramente    &#13;
 incompatibili con il principio d'eguaglianza.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   l'illegittimità  costituzionale  dell'art.  1,  secondo    &#13;
 comma, della legge 9 gennaio 1963, n. 9 (Elevazione  dei  trattamenti    &#13;
 minimi  di  pensione  e  riordinamento  delle  norme  in  materia  di    &#13;
 previdenza dei coltivatori diretti e dei coloni  e  mezzadri),  nella    &#13;
 parte  in cui non consente l'integrazione al minimo della pensione di    &#13;
 riversibilità a carico del Fondo speciale per i coltivatori diretti,    &#13;
 mezzadri  e coloni nell'ipotesi di cumulo con pensione di vecchiaia a    &#13;
 carico del Fondo medesimo.                                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1989.                                  &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 marzo 1989.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
