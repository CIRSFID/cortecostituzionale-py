<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>360</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:360</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/11/2001</data_decisione>
    <data_deposito>07/11/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 1, comma 58, &#13;
della  legge  23 dicembre  1996,  n. 662 (Misure di razionalizzazione &#13;
della  finanza pubblica), promosso con ordinanza emessa il 4 dicembre &#13;
2000  dal Tribunale di La Spezia nel procedimento civile vertente tra &#13;
G.  B.  e  il  comune  di  Vernazza,  iscritta  al n. 97 del registro &#13;
ordinanze 2001 e pubblicata nella Gazzetta Ufficiale della Repubblica &#13;
n. 7, 1ª serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella camera di consiglio del 26 settembre 2001 il giudice &#13;
relatore Piero Alberto Capotosti. &#13;
    Ritenuto  che  il  Tribunale  di  La  Spezia  ha  sollevato,  con &#13;
ordinanza    del   4 dicembre   2000,   questione   di   legittimità &#13;
costituzionale  dell'art. 1,  comma 58, della legge 23 dicembre 1996, &#13;
n. 662  (Misure  di  razionalizzazione  della  finanza  pubblica), in &#13;
riferimento  agli  artt. 3, primo comma, 97, primo comma, e 98, primo &#13;
comma, della Costituzione; &#13;
        che  il  Tribunale  di  La  Spezia  è stato adito in sede di &#13;
reclamo  avverso  il provvedimento di rigetto della domanda cautelare &#13;
proposta da un dipendente del comune di Vernazza, diretta ad ottenere &#13;
la  trasformazione  del  rapporto  di  lavoro  da tempo pieno a tempo &#13;
parziale; &#13;
        che,    secondo    il    rimettente,   i   dipendenti   delle &#13;
amministrazioni   pubbliche  sarebbero  titolari  di  un  diritto  ad &#13;
ottenere  la  trasformazione  del rapporto di lavoro da tempo pieno a &#13;
tempo  parziale  e  l'amministrazione  avrebbe  la  mera  facoltà di &#13;
differirla  per  un  termine  massimo  di  sei mesi, in quanto questa &#13;
fattispecie sarebbe disciplinata esclusivamente dall'art. 1, commi 57 &#13;
e  58,  della  legge  n. 662  del 1996, poiché, in parte qua nessuna &#13;
modificazione  sarebbe  stata  introdotta  dagli  artt. 39, comma 27, &#13;
della  legge  27 dicembre  1997,  n. 449, e 31, comma 41, della legge &#13;
23 dicembre 1998, n. 448; &#13;
        che,   ad  avviso  del  giudice  a  quo  la  norma  impugnata &#13;
violerebbe  l'art. 97,  primo  comma,  della  Costituzione,  poiché, &#13;
soprattutto  nel caso di comuni di piccole dimensioni, impedirebbe la &#13;
razionale ed efficiente organizzazione del servizio pubblico, anche a &#13;
causa  della  complessità  delle procedure per l'assunzione di nuovo &#13;
personale  e del diritto del dipendente di ottenere il ripristino del &#13;
rapporto di lavoro a tempo pieno; &#13;
        che la norma censurata, secondo il Tribunale di La Spezia, si &#13;
porrebbe  in  contrasto  anche  con  l'art. 98,  primo  comma,  della &#13;
Costituzione,   in   quanto   subordinerebbe   l'interesse   pubblico &#13;
all'interesse  privato  del  dipendente  di  espletare  una ulteriore &#13;
attività   lavorativa,   realizzando   altresì  una  ingiustificata &#13;
disparità  di  trattamento  in danno della pubblica amministrazione, &#13;
poiché  quest'ultima,  nonostante la privatizzazione del rapporto di &#13;
lavoro  pubblico,  non  potrebbe impedirne la trasformazione da tempo &#13;
pieno  a  tempo  parziale, diversamente da quanto è stabilito per il &#13;
datore di lavoro privato; &#13;
        che,  nel  giudizio  innanzi  alla  Corte,  è intervenuto il &#13;
Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso &#13;
dall'Avvocatura  generale dello Stato, chiedendo che la questione sia &#13;
dichiarata infondata; &#13;
        che,  ad avviso della difesa erariale, la norma impugnata non &#13;
violerebbe  i  parametri  costituzionali  indicati  nell'ordinanza di &#13;
rimessione,  in  quanto  il divieto della trasformazione del rapporto &#13;
per  i  dipendenti che esercitano determinate mansioni, la previsione &#13;
che  il  contingente  del  personale  ammesso  al part-time non possa &#13;
superare  una  determinata  percentuale  dell'organico  complessivo e &#13;
l'introduzione di procedure dirette a sopperire all'eventuale carenza &#13;
di  personale, costituirebbero efficaci misure in grado di assicurare &#13;
la  continuità  e  l'efficienza dell'attività delle amministrazioni &#13;
pubbliche. &#13;
    Considerato  che, successivamente all'ordinanza di rimessione, è &#13;
stato  emanato  il  decreto  legislativo 30 marzo 2001, n. 165 (Norme &#13;
generali   sull'ordinamento   del   lavoro   alle   dipendenze  delle &#13;
amministrazioni   pubbliche),   il   quale,   all'art. 70,  comma  3, &#13;
stabilisce  che  "il  rapporto  di  lavoro  dei dipendenti degli enti &#13;
locali è disciplinato dai contratti collettivi previsti dal presente &#13;
decreto nonché dal decreto legislativo 18 agosto 2000, n. 267"; &#13;
        che  il  decreto legislativo da ultimo richiamato dispone che &#13;
la  potestà  regolamentare  degli  enti  locali  ha  ad oggetto, tra &#13;
l'altro,  la  disciplina  "delle  incompatibilità  tra impiego nelle &#13;
pubbliche  amministrazioni  ed  altre  attività e casi di divieto di &#13;
cumulo  di impieghi ed incarichi pubblici" (art. 89, comma 2, lettera &#13;
g) e reca altresì una norma che facoltizza specificamente gli stessi &#13;
enti  a definire rapporti di lavoro a tempo parziale, nonché a tempo &#13;
determinato   dei   propri  dipendenti,  anche  in  riferimento  alla &#13;
necessità  di  assunzioni  eventualmente  occorrenti  allo  scopo di &#13;
sopperire ad esigenze temporanee di detti enti (art. 92); &#13;
        che,  indipendentemente  da  ogni  valutazione in ordine alla &#13;
mancata  considerazione  da parte del rimettente di ulteriori profili &#13;
normativi  della  complessiva disciplina applicabile alla fattispecie &#13;
sottoposta  al  suo  esame,  la sopravvenuta modificazione del quadro &#13;
legislativo  di  riferimento  rende  necessaria la restituzione degli &#13;
atti  al  giudice a quo affinché egli valuti la perdurante rilevanza &#13;
della questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Ordina la restituzione degli atti al Tribunale di La Spezia. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 6 novembre 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                       Il redattore: Capotosti &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 7 novembre 2001. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
