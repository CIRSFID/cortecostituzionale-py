<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>353</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:353</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/11/1997</data_decisione>
    <data_deposito>21/11/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art. 7, comma 2, del    &#13;
 d.-l. 30 dicembre 1989, n. 416 (Norme urgenti  in  materia  di  asilo    &#13;
 politico,  di  ingresso  e soggiorno dei cittadini extracomunitari ed    &#13;
 apolidi già presenti nel territorio dello  Stato),  convertito,  con    &#13;
 modificazioni,  nella  legge  28  febbraio  1990, n. 39, promosso con    &#13;
 ordinanza emessa il 1 dicembre 1995 dal  T.A.R.  del  Lazio,  sezione    &#13;
 distaccata  di  Latina,  sul  ricorso  proposto da Abbioui Abderrahim    &#13;
 contro la prefettura di Frosinone, iscritta al n. 1349  del  registro    &#13;
 ordinanze 1996 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 3, prima serie speciale, dell'anno 1997;                              &#13;
   Udito  nella  camera  di  consiglio  del  2  luglio 1997 il giudice    &#13;
 relatore Francesco Guizzi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel  corso  del  giudizio  amministrativo  per  l'annullamento  del    &#13;
 decreto  di espulsione dall'Italia di un cittadino marocchino, munito    &#13;
 di   passaporto,   mai   però   "regolarizzatosi",   il    tribunale    &#13;
 amministrativo  regionale  per  il Lazio ha sollevato, in riferimento    &#13;
 all'art.   3   della   Costituzione,   questione   di    legittimità    &#13;
 costituzionale  dell'art.  7, comma 2, del d.-l. 30 dicembre 1989, n.    &#13;
 416 (Norme urgenti in  materia  di  asilo  politico,  di  ingresso  e    &#13;
 soggiorno  dei  cittadini  extracomunitari  e di regolarizzazione dei    &#13;
 cittadini extracomunitari ed apolidi  già  presenti  nel  territorio    &#13;
 dello  Stato), convertito, con modificazioni, nella legge 28 febbraio    &#13;
 1990, n. 39.                                                             &#13;
   Tale  disposizione,  ad   avviso   del   Collegio,   configurerebbe    &#13;
 l'espulsione come un atto dovuto senza discriminare quelle situazioni    &#13;
 che  riflettono  casi umani disperati, negando loro qualsiasi tutela.    &#13;
 La  questione,  soggiunge  il  rimettente,  non  perderebbe  la   sua    &#13;
 rilevanza, anche dopo l'emanazione del d.-l. 18 novembre 1995, n. 489    &#13;
 (Disposizioni  urgenti in materia di politica dell'immigrazione e per    &#13;
 la  regolamentazione  dell'ingresso  e   soggiorno   nel   territorio    &#13;
 nazionale   dei  cittadini  dei  Paesi  non  appartenenti  all'Unione    &#13;
 europea), e delle successive reiterazioni  di esso.<diritto>Considerato in diritto</diritto>1. - Viene all'esame della Corte, con riferimento all'art. 3  della    &#13;
 Costituzione,  la  questione di legittimità costituzionale dell'art.    &#13;
 7, comma 2, del d.-l. 30 dicembre 1989,  n.  416  (Norme  urgenti  in    &#13;
 materia  di  asilo  politico,  di  ingresso e soggiorno dei cittadini    &#13;
 extracomunitari e di regolarizzazione dei  cittadini  extracomunitari    &#13;
 ed apolidi già presenti nel territorio dello Stato), convertito, con    &#13;
 modificazioni,  nella  legge  28  febbraio 1990, n. 39, perché - nel    &#13;
 prevedere l'espulsione dal territorio nazionale degli  stranieri  che    &#13;
 violino  le  disposizioni  in  materia  di ingresso e soggiorno - non    &#13;
 discriminerebbe i  casi  umani  più  dolorosi,  così  negando  loro    &#13;
 tutela.                                                                  &#13;
   2. - La questione non è fondata.                                      &#13;
   Già con la sentenza n. 129 del 1995 questa Corte, sottolineando la    &#13;
 distinzione  fra  le  due  figure  di  espulsione  previste nel testo    &#13;
 originario del d.-l. n. 416 del 1989, convertito nella  legge  n.  39    &#13;
 del   1990,   ritenne   necessario   garantire   la   valutazione  di    &#13;
 pericolosità sociale soltanto per la misura di  sicurezza.  Ora,  il    &#13;
 giudice  a  quo  mira,  attraverso  la declaratoria di illegittimità    &#13;
 costituzionale dell'art. 7, comma 2, del  citato  d.-l.  n.  416  del    &#13;
 1989,  a  cancellare  l'automatismo  espulsivo  per  tutti coloro che    &#13;
 entrino clandestinamente nel territorio dello Stato. E tanto perché,    &#13;
 in violazione  dell'art.    3  della  Costituzione,  la  disposizione    &#13;
 censurata  non  prenderebbe  in  considerazione  i casi meritevoli di    &#13;
 maggiore attenzione.                                                     &#13;
   Le ragioni della solidarietà umana non possono essere affermate al    &#13;
 di  fuori di un corretto bilanciamento dei valori in gioco, di cui si    &#13;
 è fatto carico il legislatore. Lo Stato non può infatti abdicare al    &#13;
 compito, ineludibile, di presidiare le proprie frontiere:  le  regole    &#13;
 stabilite   in   funzione   d'un  ordinato  flusso  migratorio  e  di    &#13;
 un'adeguata accoglienza vanno dunque rispettate, e non eluse, o anche    &#13;
 soltanto derogate di volta in  volta  con  valutazioni  di  carattere    &#13;
 sostanzialmente   discrezionale,   essendo   poste   a  difesa  della    &#13;
 collettività nazionale e, insieme, a tutela di coloro che  le  hanno    &#13;
 osservate  e  che  potrebbero  ricevere  danno  dalla  tolleranza  di    &#13;
 situazioni illegali.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  7,  comma  2,  del  d.-l.  30 dicembre 1989, n. 416 (Norme    &#13;
 urgenti in materia di asilo politico, di  ingresso  e  soggiorno  dei    &#13;
 cittadini   extracomunitari   e  di  regolarizzazione  dei  cittadini    &#13;
 extracomunitari ed apolidi già presenti nel territorio dello Stato),    &#13;
 convertito, con modificazioni, nella legge 28 febbraio 1990,  n.  39,    &#13;
 sollevata,   in   riferimento  all'art.  3  della  Costituzione,  dal    &#13;
 tribunale amministrativo regionale per il Lazio, con  l'ordinanza  in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 novembre 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Guizzi                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 21 novembre 1997.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
