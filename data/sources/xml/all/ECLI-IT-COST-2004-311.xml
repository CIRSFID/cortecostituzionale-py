<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>311</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:311</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MEZZANOTTE</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia>Annibale Marini</redattore_pronuncia>
    <data_decisione>13/10/2004</data_decisione>
    <data_deposito>21/10/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Carlo MEZZANOTTE; Giudici: Fernanda CONTRI, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 28 maggio 2003 relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della Costituzione, delle opinioni espresse dal senatore Loreto Rocco nei confronti del dott. Matteo Di Giorgio, promosso dal Tribunale di Potenza, sezione GIP/GUP, con ricorso depositato l'11 agosto 2003 ed iscritto al n. 252 del registro di ammissibilità conflitti. &#13;
    Udito nella camera di consiglio del 29 settembre 2004 il Giudice relatore Annibale Marini.  &#13;
    Ritenuto che, con ricorso dell'8 luglio 2003, il Giudice per l'udienza preliminare del Tribunale di Potenza – nel corso di un procedimento penale instaurato nei confronti dell'ex senatore Rocco Loreto per i reati di calunnia continuata in danno di Matteo Di Giorgio e di violenza privata aggravata in danno di Francesco Maiorino – ha sollevato conflitto di attribuzione tra poteri dello Stato nei confronti del Senato della Repubblica in relazione alla deliberazione adottata dall'Assemblea il 28 maggio 2003 con la quale è stato dichiarato che i fatti per i quali è in corso il processo a carico dell'ex senatore Rocco Loreto, concernendo opinioni espresse nell'esercizio delle funzioni parlamentari, rientrano nell'ambito di immunità ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    che il giudice ricorrente, esposti i fatti alla base della vicenda processuale, ritiene che, alla luce della giurisprudenza di questa Corte, la delibera di insindacabilità abbia illegittimamente menomato la propria sfera di attribuzioni costituzionalmente garantita dall'art. 101 della Costituzione; &#13;
    che, ad avviso dello stesso giudice, nel caso in esame difetterebbero, infatti, tutti i requisiti richiesti dall'art. 68 della Costituzione, non avendo il Loreto espresso opinioni né dato voti, ma posto in essere atti materiali che, come affermato dalla sentenza di questa Corte n. 137 del 2001, non possono, in quanto tali, rientrare nell'ambito applicativo dell'art. 68, primo comma, della Costituzione; &#13;
    che priva di qualsiasi rilevanza, ai fini di cui si discute, risulterebbe, infine, l'entrata in vigore dell'art. 3, comma 1, della legge 20 giugno 2003, n. 140. E ciò in quanto «la presentazione di esposti aventi contenuto calunnioso e la condotta costrittiva e minatoria che avrebbe posto in essere il Loreto non possono costituire “attività di ispezione, di divulgazione, di critica e di denuncia politica, connessa alla funzione di parlamentare, espletata anche fuori dal Parlamento”». &#13;
    Considerato che in questa fase occorre delibare esclusivamente se il conflitto sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo ed oggettivo di un conflitto di attribuzione tra poteri dello Stato, impregiudicata ogni definitiva decisione anche in ordine all'ammissibilità (art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87); &#13;
    che, quanto al requisito soggettivo, il Giudice per l'udienza preliminare del Tribunale di Potenza è legittimato a sollevare conflitto, essendo competente a dichiarare definitivamente, in relazione al procedimento del quale è investito, la volontà del potere cui appartiene, in considerazione della posizione di indipendenza, costituzionalmente garantita, di cui godono i singoli organi giurisdizionali; &#13;
    che, parimenti, il Senato della Repubblica che ha deliberato l'insindacabilità delle opinioni espresse da un proprio membro è legittimato ad essere parte del conflitto essendo competente a dichiarare definitivamente la volontà del potere che rappresenta; &#13;
    che, per quanto riguarda il profilo oggettivo del conflitto, il ricorrente denuncia la menomazione della propria sfera di attribuzioni, garantita da norme costituzionali, in conseguenza dell'adozione, da parte del Senato, di una deliberazione ove si afferma, in modo asseritamente illegittimo, che le opinioni espresse da un proprio membro rientrano nell'esercizio delle funzioni parlamentari, e sono quindi coperte dalla garanzia di insindacabilità stabilita dall'art. 68, primo comma, della Costituzione; &#13;
      che, pertanto, esiste la materia di un conflitto la cui risoluzione spetta alla competenza della Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dal Giudice per l'udienza preliminare del Tribunale di Potenza nei confronti del Senato della Repubblica con l'atto introduttivo indicato in epigrafe; &#13;
    dispone:  &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al ricorrente Giudice per l'udienza preliminare del Tribunale di Potenza;  &#13;
    b) che l'atto introduttivo e la presente ordinanza siano, a cura del ricorrente, notificati al Senato della Repubblica entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere poi depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di venti giorni previsto dall'art. 26, terzo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 ottobre 2004. &#13;
F.to: &#13;
Carlo MEZZANOTTE, Presidente &#13;
Annibale MARINI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 21 ottobre 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
