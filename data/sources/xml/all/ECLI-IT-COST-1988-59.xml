<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>59</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:59</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1988</data_decisione>
    <data_deposito>21/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2601 del codice    &#13;
 civile,  promosso  con  ordinanza  emessa  il  7  febbraio  1980  dal    &#13;
 Tribunale di Milano, iscritta al n. 477 del registro ordinanze 1980 e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 256 dell'anno    &#13;
 1980;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 dicembre 1987 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto  che la società distributrice di un prodotto che avrebbe    &#13;
 dovuto favorire le diete dimagranti, ha convenuto in giudizio dinanzi    &#13;
 al  Tribunale  di  Milano gli autori di alcuni articoli giornalistici    &#13;
 ritenuti diffamatori, nonché i responsabili dei relativi  organi  di    &#13;
 stampa, richiedendo l'inibitoria ed il risarcimento dei danni;           &#13;
      che nel corso del procedimento ha spiegato intervento volontario    &#13;
 la libera associazione denominata "Comitato Difesa  Consumatori",  la    &#13;
 quale  ha richiesto nei confronti della società attrice l'inibitoria    &#13;
 della vendita del prodotto e la pubblicazione della sentenza;            &#13;
      che  il  Tribunale adito ha sollevato, con ordinanza emessa il 7    &#13;
 febbraio 1980, questione  di  legittimità  costituzionale  dell'art.    &#13;
 2601  del  codice civile in riferimento all'art. 3 della Costituzione    &#13;
 nella  parte  in   cui,   consentendo   la   tutela   giurisdizionale    &#13;
 esclusivamente   avverso   gli   atti   di   concorrenza  sleale  che    &#13;
 pregiudicano gli interessi di una categoria professionale, esclude la    &#13;
 legittimazione  ad  agire  di  altre  categorie organizzate, quali ad    &#13;
 esempio quelle di tutela del consumatore;                                &#13;
      che  è  intervenuta l'Avvocatura dello Stato, in rappresentanza    &#13;
 del Presidente del Consiglio dei ministri, chiedendo che la questione    &#13;
 venga dichiarata non fondata;                                            &#13;
    Considerato  che  la norma denunziata si colloca nell'ambito della    &#13;
 disciplina della concorrenza sleale e pertanto la ratio della  stessa    &#13;
 va  individuata  nell'esigenza  di  apprestare  una  specifica tutela    &#13;
 rispetto al compimento degli atti di cui  all'art.  2598  del  codice    &#13;
 civile;                                                                  &#13;
      che,   in   particolare,   la   giurisprudenza  della  Corte  di    &#13;
 cassazione, ha affermato il persistente vigore della norma  anche  in    &#13;
 seguito   al   venir  meno  dell'ordinamento  corporativo,  essendosi    &#13;
 trasferito dal piano pubblicistico, caratteristico di quel sistema, a    &#13;
 quello   privatistico   l'interesse   protetto   -  quello  cioè  di    &#13;
 associazioni imprenditoriali - all'eliminazione di fattispecie lesive    &#13;
 della categoria rappresentata;                                           &#13;
      che,   conseguentemente,   non  appare  neppur  ipotizzabile  il    &#13;
 confronto  con   enti   od   associazioni   che   abbiano   finalità    &#13;
 istituzionali   diverse   dal   potenziamento  del  commercio  di  un    &#13;
 determinato prodotto e che fanno quindi valere  interessi  del  tutto    &#13;
 estranei alla correttezza dei rapporti economici di mercato;             &#13;
      che  pertanto compete al legislatore e non già al giudice delle    &#13;
 leggi di  apprestare,  per  il  consumatore,  adeguati  strumenti  di    &#13;
 salvaguardia,  attualmente limitati alla tutela penale (cfr. art. 444    &#13;
 c.p.), prevedendo e le forme e l'ambito  di  azioni  specifiche,  sul    &#13;
 modello  di  quelle contemplate dalle legislazioni tedesca e svizzera    &#13;
 in favore delle associazioni dei consumatori;                            &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente inammissibile la questione di legittimità    &#13;
 costituzionale  dell'art.  2601  del  codice  civile,  sollevata,  in    &#13;
 riferimento  all'art.  3  della Costituzione, dal Tribunale di Milano    &#13;
 con l'ordinanza di cui in epigrafe.                                      &#13;
    Così  deciso  in  Roma,  in Camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
