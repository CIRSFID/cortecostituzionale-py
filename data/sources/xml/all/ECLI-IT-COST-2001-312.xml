<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>312</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:312</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2001</data_decisione>
    <data_deposito>25/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, CarloMEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel   giudizio   di  ammissibilità  del  ricorso  per  conflitto  di &#13;
attribuzione  tra  poteri  dello Stato sorto a seguito della delibera &#13;
della   Camera   dei   deputati   del  25 marzo  1999  relativa  alla &#13;
insindacabilità  delle  opinioni espresse dall'on. Domenico Gramazio &#13;
nei  confronti  del  dott.  Stefano  Balassone ed altra, promosso dal &#13;
tribunale  di  Roma,  prima  sezione  civile;  ricorso  depositato il &#13;
12 febbraio  2001  ed  iscritto al n. 182 del registro ammissibilità &#13;
conflitti. &#13;
    Udito  nella  camera  di  consiglio  del 4 luglio 2001 il giudice &#13;
relatore Valerio Onida. &#13;
    Ritenuto  che  il tribunale di Roma, in composizione monocratica, &#13;
con  ricorso  in  data  30 gennaio 2001 (depositato nella cancelleria &#13;
della   Corte   il  12 febbraio  2001),  ha  sollevato  conflitto  di &#13;
attribuzione  tra  poteri  dello Stato nei confronti della Camera dei &#13;
deputati  in  relazione  alla delibera adottata dalla assemblea nella &#13;
seduta del 25 marzo 1999 (doc. IV-quater, n. 67), secondo la quale le &#13;
dichiarazioni   rese   dal   deputato   Domenico  Gramazio,  in  data &#13;
10 novembre 1998, attraverso la diffusione di un comunicato stampa in &#13;
cui   egli   dava   notizia   di   una   interrogazione   presentata, &#13;
accompagnandola  con  ulteriori  informazioni  - dichiarazioni per le &#13;
quali  è  in corso davanti allo stesso tribunale procedimento civile &#13;
per  risarcimento  del  danno da diffamazione, proposto nei confronti &#13;
dell'on. Gramazio   dal  dott.  Stefano  Balassone  e  dalla  signora &#13;
Annamaria  Grignola  -  concernono opinioni espresse da un membro del &#13;
Parlamento  nell'esercizio delle sue funzioni, ai sensi dell'art. 68, &#13;
primo comma, della Costituzione; &#13;
        che,  secondo  il  ricorrente, la Camera dei deputati avrebbe &#13;
illegittimamente   valutato   come   insindacabili  le  dichiarazioni &#13;
dell'on. Gramazio: posto, infatti, che l'interrogazione parlamentare, &#13;
presentata  dall'on. Gramazio  in  ordine  agli eventuali rapporti di &#13;
collaborazione  e  di  consulenza  tra la società Extra (avente alle &#13;
proprie   dipendenze   la  signora  Annamaria  Grignola,  moglie  del &#13;
consigliere  di amministrazione della RAI-TV dott. Stefano Balassone) &#13;
e la RAI-TV, era stata dichiarata non ammissibile ex art. 139-bis del &#13;
regolamento  della Camera, il tribunale ritiene che la non pertinenza &#13;
della  domanda di interrogazione alla funzione ispettiva parlamentare &#13;
e   l'indebita  diffusione  del  testo  collocherebbero  l'iniziativa &#13;
dell'on. Gramazio   "in  un  ambito  improprio",  diverso  da  quello &#13;
funzionale,  con la conseguenza che le opinioni espresse dal deputato &#13;
sarebbero manifestazione di pensiero riconducibile solo all'esercizio &#13;
di attività politica in genere, come tale non protetta dall'art. 68, &#13;
primo comma, della Costituzione; &#13;
        che,  pertanto, il tribunale ricorrente, nel sollecitare, con &#13;
il   ricorso  per  conflitto  di  attribuzioni,  un  controllo  della &#13;
correttezza    sul    piano    costituzionale   della   delibera   di &#13;
insindacabilità   adottata   dalla   Camera  e  una  verifica  della &#13;
sussistenza  in  concreto  della  prerogativa,  chiede l'annullamento &#13;
della predetta delibera parlamentare. &#13;
    Considerato  che  in  questa  fase  la Corte è chiamata, a norma &#13;
dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, &#13;
a  delibare,  senza contraddittorio, se il ricorso sia ammissibile in &#13;
quanto  esista  "la materia di un conflitto la cui risoluzione spetti &#13;
alla  sua competenza", impregiudicata ogni decisione definitiva anche &#13;
sull'ammissibilità; &#13;
        che,  secondo  la costante giurisprudenza di questa Corte, il &#13;
conflitto  che  l'autorità  giudiziaria,  chiamata a giudicare della &#13;
eventuale  responsabilità  di  un parlamentare in un giudizio civile &#13;
per  risarcimento  del danno, promuova nei confronti della Camera che &#13;
ha   valutato   tali   dichiarazioni   come   opinioni  espresse  dal &#13;
parlamentare  nell'esercizio  delle  sue  funzioni,  contestandone la &#13;
riconducibilità  all'art. 68, primo comma, della Costituzione, verte &#13;
su   attribuzioni  costituzionalmente  garantite  agli  organi  della &#13;
giurisdizione,    che    si   assumono   lese   dalla   deliberazione &#13;
dell'assemblea  parlamentare,  ed  insorge  tra  organi  competenti a &#13;
dichiarare  in via definitiva la volontà del potere cui appartengono &#13;
(cfr., da ultimo, ordinanze n. 10, n. 196, n.197 e n. 198 del 2001); &#13;
        che  dal  ricorso è dato ricavare le ragioni del conflitto e &#13;
le norme costituzionali che regolano la materia.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  ammissibile, ai sensi dell'art. 37 della legge 11 marzo &#13;
1953,  n. 87,  il  ricorso  per  conflitto di attribuzione, di cui in &#13;
epigrafe,  proposto  dal tribunale di Roma nei confronti della Camera &#13;
dei deputati; &#13;
    Dispone: &#13;
        a) che la cancelleria della Corte dia immediata comunicazione &#13;
della presente ordinanza al tribunale di Roma ricorrente; &#13;
        b)  che  il ricorso e la presente ordinanza siano, a cura del &#13;
ricorrente,  notificati  alla Camera dei deputati entro il termine di &#13;
sessanta  giorni  dalla  comunicazione di cui al punto a), per essere &#13;
successivamente depositati nella cancelleria di questa Corte entro il &#13;
termine  di  venti  giorni dalla notificazione, a norma dell'art. 26, &#13;
terzo comma, delle norme integrative per i giudizi davanti alla Corte &#13;
costituzionale. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 12 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
Il redattore: Onida &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 25 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
