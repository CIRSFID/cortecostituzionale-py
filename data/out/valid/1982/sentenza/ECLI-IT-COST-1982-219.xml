<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-12-16/219/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-12-16/219"/>
          <FRBRalias value="ECLI:IT:COST:1982:219" name="ECLI"/>
          <FRBRdate date="1982-12-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="219"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-12-16/219/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-12-16/219/ita@"/>
          <FRBRdate date="1982-12-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-12-16/219/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-12-16/219/ita@.xml"/>
          <FRBRdate date="1982-12-16" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1982-12-02" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1982-12-16" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="ELIA" href="/akn/ontology/person/it/ELIA" showAs="ELIA"/>
        <TLCPerson eId="leopoldoElia" href="/akn/ontology/person/it/leopoldoElia" showAs="LEOPOLDO ELIA"/>
        <TLCPerson eId="guglielmoRoehrssen" href="/akn/ontology/person/it/guglielmoRoehrssen" showAs="GUGLIELMO ROEHRSSEN"/>
        <TLCPerson eId="oronzoReale" href="/akn/ontology/person/it/oronzoReale" showAs="ORONZO REALE"/>
        <TLCPerson eId="albertoMalagugini" href="/akn/ontology/person/it/albertoMalagugini" showAs="ALBERTO MALAGUGINI"/>
        <TLCPerson eId="livioPaladin" href="/akn/ontology/person/it/livioPaladin" showAs="LIVIO PALADIN"/>
        <TLCPerson eId="arnaldoMaccarone" href="/akn/ontology/person/it/arnaldoMaccarone" showAs="ARNALDO MACCARONE"/>
        <TLCPerson eId="antonioLaPergola" href="/akn/ontology/person/it/antonioLaPergola" showAs="ANTONIO LA PERGOLA"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="VIRGILIO ANDRIOLI"/>
        <TLCPerson eId="giuseppeFerrari" href="/akn/ontology/person/it/giuseppeFerrari" showAs="GIUSEPPE FERRARI"/>
        <TLCPerson eId="francescoSaja" href="/akn/ontology/person/it/francescoSaja" showAs="FRANCESCO SAJA"/>
        <TLCPerson eId="giovanniConso" href="/akn/ontology/person/it/giovanniConso" showAs="GIOVANNI CONSO"/>
        <TLCPerson eId="giovanniVitale" href="/akn/ontology/person/it/giovanniVitale" showAs="GIOVANNI VITALE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1982</akn4cc:anno>
        <akn4cc:numero>219</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1982:219</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>ELIA</akn4cc:presidente>
        <akn4cc:relatore>Guglielmo Roehrssen</akn4cc:relatore>
        <akn4cc:data_decisione>1982-12-02</akn4cc:data_decisione>
        <akn4cc:data_deposito>1982-12-16</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge>, Presidente - Prof. <judge refersTo="#guglielmoRoehrssen">GUGLIELMO ROEHRSSEN</judge> - Avv. <judge refersTo="#oronzoReale">ORONZO REALE</judge> - Dott. BRUNETTO BUCCIARELLI 
 DUCCI - Avv. <judge refersTo="#albertoMalagugini">ALBERTO MALAGUGINI</judge> - Prof. <judge refersTo="#livioPaladin">LIVIO PALADIN</judge> - Dott. <judge refersTo="#arnaldoMaccarone">ARNALDO 
 MACCARONE</judge> - Prof. <judge refersTo="#antonioLaPergola">ANTONIO LA PERGOLA</judge> - Prof. <judge refersTo="#virgilioAndrioli">VIRGILIO ANDRIOLI</judge> - Prof. <judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge> - Dott. <judge refersTo="#francescoSaja">FRANCESCO SAJA</judge> - Prof. <judge refersTo="#giovanniConso">GIOVANNI CONSO</judge>, 
 Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei  giudizi riuniti di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/1974-07-08/261/!main#art_6">art. 6 del  
 d.l. 8 luglio 1974, n. 261</ref> (Modificazioni alla <ref href="/akn/it/act/legge/stato/1970-05-04/2/!main">legge 24 maggio 1970</ref>, n.</p>
          </content>
        </paragraph>
        <paragraph>
          <num>336</num>
          <list>
            <intro>
              <p>, concernente norme a favore dei  dipendenti  dello  Stato  ed  enti  
 pubblici  ex  combattenti  ed  assimilati), come modificato dall'art. 1  
 della <ref href="/akn/it/act/legge/stato/1974-08-14/355/!main">legge 14 agosto 1974, n. 355</ref>, in riferimento all'art. 67,  ultimo  
 comma,  del  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-06-30/748/!main">d.P.R.  30  giugno  1972, n. 748</ref>, promossi con le seguenti  
 ordinanze:</p>
            </intro>
            <point>
              <num>1)</num>
              <content>
                <p> ordinanza emessa il 13 ottobre 1978  dal  Pretore  di  Pisa  nel  
 procedimento  civile vertente tra Tosi Ettore e l'ENPAS, iscritta al n.</p>
              </content>
            </point>
          </list>
        </paragraph>
        <paragraph>
          <num>659</num>
          <list>
            <alinea>
              <content>
                <p> del registro ordinanze 1978 e pubblicata nella  Gazzetta  Ufficiale  
 della Repubblica n. 59 del 28 febbraio 1979;</p>
              </content>
            </alinea>
            <point>
              <num>2)</num>
              <content>
                <p>  ordinanza  emessa  il  13  ottobre 1978 dal Pretore di Pisa nel  
 procedimento civile vertente tra Scarselli Sergio e l'INAM, iscritta al  
 n.  660  del  registro  ordinanze  1978  e  pubblicata  nella  Gazzetta  
 Ufficiale della Repubblica n. 59 del 28 febbraio 1979;</p>
              </content>
            </point>
            <point>
              <num>3)</num>
              <content>
                <p>   ordinanza   emessa   il   18  aprile  1980  dal  Tribunale  di  
 Caltanissetta nel procedimento civile vertente  tra  l'INAIL  e  Romano  
 Luigi,  iscritta  al  n.  517  del registro ordinanze 1980 e pubblicata  
 nella Gazzetta Ufficiale della Repubblica n. 249 del 10 settembre 1980.</p>
              </content>
            </point>
          </list>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> di  Scarselli  Sergio,  dell'INAM  e  
 dell'INAIL  e  gli  atti di intervento del Presidente del Consiglio dei  
 ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica del 19 maggio 1982 il Giudice  relatore  
 Guglielmo Roehrssen;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>uditi  l'avvocato  Antonio Funari, per Scarselli Sergio, l'avvocato  
 Lucio Mancini, per l'INAIL e l'avvocato dello Stato Renato Carafa,  per  
 il Presidente del Consiglio dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto:</heading>
          <paragraph>
            <content>
              <p>Con <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1978-10-03/1/!main">ordinanze 13 ottobre 1978</ref> (nn. 659-660/1978) il Pretore di Pisa  
 nel  corso  di giudizi promossi a seguito di risoluzione di rapporti di  
 lavoro autonomo da parte dell'ENPAS e dell'INAM ai  sensi  dell'art.  6  
 del  <ref href="/akn/it/act/decretoLegge/stato/1974-07-08/261/!main">d.l.  8  luglio  1974, n. 261</ref> ("Modificazioni alla <ref href="/akn/it/act/legge/stato/1970-05-24/336/!main">legge 24 maggio  
 1970, n. 336</ref>, concernente norme a favore dei dipendenti dello Stato  ed  
 enti pubblici ex combattenti ed assimilati"), ha sollevato questione di  
 legittimità   costituzionale  -  per  contrasto  con  l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.  3  Cost.</ref></p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>-dell'art.   6 del detto <ref href="/akn/it/act/decretoLegge/stato/1974/261/!main">d.l.  n.  261/1974</ref>,  nella  parte  in  cui  (a  
 differenza  di  quanto  disposto  dall'art. 67, ultimo comma, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-06-30/748/!main">d.P.R. 30  
 giugno 1972, n. 748</ref>  ("Disciplina  delle  funzioni  dirigenziali  nelle  
 amministrazioni  dello  Stato  anche  ad  ordinamento  autonomo") per i  
 dirigenti), prevede la possibilità per gli ex combattenti, collocati a  
 riposo ai sensi dell'<ref href="/akn/it/act/legge/stato/1970-05-24/336/!main#art_3">art. 3 della L.24 maggio 1970, n.  336</ref>  ("Norme  a  
 favore  dei  dipendenti  dello Stato ed enti pubblici ex combattenti ed  
 assimilati"), di assumere e mantenere, non  solo  rapporti  di  impiego  
 pubblico,  ma anche incarichi di lavoro autonomo con lo Stato e con gli  
 altri enti pubblici.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Con ciò si violerebbe il principio di uguaglianza, per non  essere  
 stata emanata analoga normativa limitatrice di assunzioni anche per gli  
 altri  soggetti  che  abbiano  usufruito di disposizioni analoghe nelle  
 finalità  e  nell'attribuzione  di  un  trattamento  preferenziale  di  
 quiescenza  a  quelle  della L. n. 336/ 1970: infatti con l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/748/!main#art_67">art. 67 del  
 d.P.R. n. 748 del  1972</ref>,  è  stato  favorito  l'esodo  volontario  dei  
 dirigenti  e del personale direttivo delle Amministrazioni dello Stato,  
 attribuendo a costoro, ove  richiedessero  il  "collocamento  a  riposo  
 anticipato"  aumenti di servizio identici a quelli riconosciuti agli ex  
 combattenti dall'<ref href="/akn/it/act/legge/stato/1970/336/!main#art_3">art. 3 della L. n. 336/1970</ref>, ma la  limitazione  della  
 libertà  di  lavoro,  ai  sensi  del citato art. 67, è stata prevista  
 unicamente con riguardo  "all'assunzione  in  impiego  alle  dipendenze  
 dello  Stato  o  di  enti pubblici" (art. 67, cit., ultimo comma) e non  
 anche con riguardo al conferimento di incarichi  libero-  professionali  
 da parte delle stesse amministrazioni.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Non  vi  sarebbe  alcuna  giustificazione all'adozione da parte del  
 legislatore del diverso trattamento normativo sopra evidenziato.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il Presidente del Consiglio dei ministri, chiede che gli atti siano  
 rimessi al giudice a quo per  il  riesame  della  rilevanza  o  che  la  
 questione sia dichiarata non fondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Si  osserva  infatti  preliminarmente che il Pretore di Pisa non ha  
 tenuto conto dell'ultimo comma dell'art. 6 della L.20  marzo  1975,  n.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>70</num>
            <content>
              <p>,  per  il  quale  "non  possono comunque essere attribuiti incarichi  
 professionali ai dipendenti dell'Amministrazione dello Stato, anche  ad  
 ordinamento  autonomo,  che  si  siano  avvalsi  delle norme sull'esodo  
 volontario, di cui al <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-06-30/748/!main">d.P.R. 30 giugno 1972, n. 748</ref>, ed ai dirigenti di  
 enti pubblici collocati a riposo".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ciò,  secondo  l'Avvocatura  dello  Stato,   giustificherebbe   la  
 remissione  degli atti al giudice a quo per un riesame della rilevanza.</p>
            </content>
          </paragraph>
          <paragraph>
            <list>
              <alinea>
                <content>
                  <p>Comunque, essa afferma, la differenza di trattamento fatta dalla  norma  
 impugnata  rispetto  a  quella  dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/748/!main#art_67">art.  67 del d.P.R. n.   748/1972</ref>,  
 sarebbe giustificata in base alle seguenti considerazioni.</p>
                </content>
              </alinea>
              <point>
                <num>A)</num>
                <content>
                  <p>  I  "benefici  combattentistici"  si  riferiscono  a  tutti  gli  
 impiegati  dello  Stato  e degli enti pubblici, mentre quelli derivanti  
 dall'"esodo  volontario"   riguardano   esclusivamente   i   funzionari  
 direttivi  e  dirigenti  dello Stato, cioè una categoria non solo più  
 qualificata e con specifiche competenze,  ma  soprattutto  notevolmente  
 più ristretta.</p>
                </content>
              </point>
              <point>
                <num>B)</num>
                <content>
                  <p>  I  benefici derivanti ai funzionari direttivi e dirigenti dello  
 Stato  dall'"esodo  volontario"  furono  accordati  esclusivamente  per  
 soddisfare  la  esigenza  di ridurre il loro numero esuberante rispetto  
 alle necessità del nuovo ordinamento delle funzioni  dirigenziali,  di  
 cui  al  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/748/!main">d.P.R.  n.  748/1972</ref>,  mentre  i  "benefici  combattentistici"  
 rispondevano  principalmente  alla  finalità  di  riparare  ritardi  e  
 menomazioni,  che  i  destinatari  avevano subito nella loro carriera a  
 causa della guerra.</p>
                </content>
              </point>
              <point>
                <num>C)</num>
                <content>
                  <p> I benefici accordati alle due categorie sono pure obiettivamente  
 differenti,  tanto  che  ne  è  prevista  in   particolari   modi   la  
 cumulabilità  (art.  67  cit.,  quarto  comma),  mentre  la tempestiva  
 rinuncia a quelli previsti nell'art. 3 della L. n. 336/ 1970 consentiva  
 agli ex  combattenti,  che  ne  avevano  usufruito,  di  mantenere  gli  
 incarichi  in  discussione,  nonostante  la  conservazione dei restanti  
 benefici (art. 1 cit., penultimo comma).</p>
                </content>
              </point>
            </list>
          </paragraph>
          <paragraph>
            <content>
              <p>Ad  ogni  modo  rientra  nella  discrezionalità  del   legislatore  
 ordinario  per  la  concessione  di  determinati  benefici  a  distinte  
 categorie  di  soggetti  valutare  tutte  le  diverse  circostanze   in  
 relazione  ai vari fini da perseguire ponendo in essere una conseguente  
 peculiare regolamentazione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Davanti a  questa  Corte  si  sono  costituite  pure  alcune  parti  
 private;  il dott. Sergio Scarselli e l'INAM, parti del giudizio a quo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il primo ha chiesto che la questione sia dichiarata fondata. Il secondo  
 che la si dichiari inammissibile per difetto di  rilevanza  o  comunque  
 infondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Identica   questione   è   stata   sollevata   dal   Tribunale  di  
 Caltanissetta con ordinanza del 18 aprile 1980 (n. 517 r.o.).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il Presidente del Consiglio dei ministri si è costituito anche  in  
 tale  giudizio  chiedendo  che la questione sia dichiarata non fondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Uguali  conclusioni  ha  formulato  la  difesa  dell'INAIL,   anch'esso  
 costituitosi.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto:</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>  -  Le ordinanze di cui in epigrafe sollevano tutte una identica  
 questione di  legittimità  costituzionale,  onde  i  relativi  giudizi  
 possono essere riuniti ai fini di un, unica pronuncia.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>  -  La  Corte  è  chiamata  ad esaminare se l'<ref href="/akn/it/act/decretoLegge/stato/1974-07-08/261/!main#art_6">art. 6 del D.L. 8  
 luglio 1974, n. 261</ref> ("Modificazioni alla <ref href="/akn/it/act/legge/stato/1970-05-24/336/!main">legge 24 maggio 1970, n.  336</ref>,  
 concernente  norme a favore dei dipendenti dello Stato ed enti pubblici  
 ex combattenti ed assimilati"), così come modificato  dalla  legge  di  
 conversione  14  agosto 1974, n. 355, violi il principio di uguaglianza  
 posto  dall'art.  3,  primo  comma,  Cost.,  avendo  vietato  agli   ex  
 combattenti  collocati  a  riposo  ai  sensi dell'<ref href="/akn/it/act/legge/stato/1970-05-24/336/!main#art_3">art. 3 della legge 24  
 maggio 1970, n. 336</ref> ("Norme a favore dei dipendenti dello Stato ed enti  
 pubblici ex combattenti  ed  assimilati"),  di  assumere  incarichi  di  
 lavoro  autonomo  con  lo Stato e gli altri enti pubblici oltre che con  
 società a partecipazione statale e ciò in difformità da  quanto  era  
 stato  disposto con l'art. 67, ultimo comma, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-06-30/748/!main">d.P.R. 30 giugno 1972,  
 n. 748</ref> ("Disciplina delle funzioni dirigenziali  nelle  Amministrazioni  
 dello  Stato  anche  ad  ordinamento  autonomo"),  il  quale vieta agli  
 "esodati", ai sensi dello stesso  decreto  presidenziale,  soltanto  le  
 assunzioni di nuovi rapporti di impiego.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p> - La questione non è fondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>E  noto  che  questa  Corte  ha  costantemente  ritenuto  che  può  
 ipotizzarsi violazione del principio di uguaglianza quando si pongano a  
 raffronto situazioni identiche o, almeno, omogenee (cfr. fra  le  altre  
 <mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/1981/1/!main">sentenze nn. 1</ref> e <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1981/52/!main">52 del 1981</ref></mref>).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nella  specie,  peraltro,  non  sussiste omogeneità di situazioni,  
 poiché diversi sono sia i destinatari sia la ratio delle  disposizioni  
 che vengono messe a raffronto nelle ordinanze di rimessione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Infatti  la  <ref href="/akn/it/act/legge/stato/1970-05-04/2/!main">legge 24 maggio 1970</ref>, come la Corte ha già avuto modo  
 di chiarire (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1976/194/!main">sentenza n. 194/1976</ref>) e  come  risulta  anche  dai  lavori  
 preparatori,  ha  per  destinatari  soltanto  i  dipendenti  statali in  
 possesso della qualifica di ex combattenti  od  appartenenti  ad  altre  
 determinate categorie che a causa della guerra avevano subito ritardi o  
 menomazioni  nella  loro carriera, allo scopo di accordare a costoro un  
 particolare  beneficio,  che  negli  stessi  lavori  preparatori  viene  
 definito atto di "giustizia riparatrice".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'art.  67, ultimo comma, del successivo d.P.R. n.  748, invece, è  
 rivolto  esclusivamente  ai  dipendenti   statali   appartenenti   alla  
 dirigenza  ed alla carriera direttiva ai quali è stato consentito, con  
 la stessa norma, l'esodo anticipato dall'Amministrazione dello Stato al  
 fine di sfoltirne i ruoli.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Siffatta diversità è sufficiente a  rendere  non  irrazionale  la  
 differenza di trattamento esistente fra le due norme poste a raffronto.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Va  peraltro  osservato che in prosieguo di tempo il legislatore è  
 ritornato sull'argomento e con l'art. 6, ultimo comma, della  <ref href="/akn/it/act/legge/stato/1975-03-20/70/!main">legge  20  
 marzo  1975, n. 70</ref> ("Disposizioni sul riordinamento degli enti pubblici  
 e del rapporto di lavoro del personale dipendente") ha  disposto  anche  
 per  i  dipendenti statali, che si erano avvalsi delle norme sull'esodo  
 volontario previsto dal ripetuto <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/748/!main#art_67">art. 67 del  d.P.R.  n.  748/1972</ref>,  un  
 divieto  sostanzialmente  simile a quello già compreso nell'<ref href="/akn/it/act/decretoLegge/stato/1974/261/!main#art_6">art. 6 del  
 D.L. n. 261 del 1974</ref>.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara non fondata la questione  di  legittimità  costituzionale  
 dell'<ref href="/akn/it/act/decretoLegge/stato/1974-07-08/261/!main#art_6">art.  6  del  decreto-legge  8 luglio 1974, n. 261</ref> ("Modificazioni  
 alla <ref href="/akn/it/act/legge/stato/1970-05-24/336/!main">legge 24 maggio 1970, n.  336</ref>,  concernente  norme  a  favore  dei  
 dipendenti dello Stato ed enti pubblici ex combattenti ed assimilati"),  
 convertito  con  modificazioni  in <ref href="/akn/it/act/legge/stato/1974-08-14/355/!main">legge 14 agosto 1974, n. 355</ref> - nella  
 parte in cui prevede per gli ex combattenti collocati a riposo ai sensi  
 dell'<ref href="/akn/it/act/legge/stato/1970/336/!main#art_3">art. 3 della legge n. 336 del  1970</ref>,  il  divieto  di  assumere  e  
 mantenere  non  solo rapporti d'impiego pubblico, ma anche incarichi di  
 lavoro autonomo con lo Stato e gli altri enti pubblici -  sollevata  in  
 riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref> con le ordinanze in epigrafe.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1982-12-02">2 dicembre 1982</date>.</block>
      <p>F.to:  <signature><judge refersTo="#leopoldoElia">LEOPOLDO  ELIA</judge></signature>   -   <signature><judge refersTo="#guglielmoRoehrssen">GUGLIELMO  
                                   ROEHRSSEN</judge></signature>  -  <signature><judge refersTo="#oronzoReale">ORONZO REALE</judge></signature> - BRUNETTO  
                                   BUCCIARELLI    DUCCI    -     <signature><judge refersTo="#albertoMalagugini">ALBERTO  
                                   MALAGUGINI</judge></signature>  - <signature><judge refersTo="#livioPaladin">LIVIO PALADIN</judge></signature> - <signature><judge refersTo="#arnaldoMaccarone">ARNALDO  
                                   MACCARONE</judge></signature>  -  <signature><judge refersTo="#antonioLaPergola">ANTONIO  LA  PERGOLA</judge></signature>  -  
                                   <signature><judge refersTo="#virgilioAndrioli">VIRGILIO  ANDRIOLI</judge></signature> - <signature><judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge></signature>  
                                   - <signature><judge refersTo="#francescoSaja">FRANCESCO SAJA</judge></signature> - <signature><judge refersTo="#giovanniConso">GIOVANNI CONSO</judge></signature>.</p>
      <p>
        <signature><person refersTo="#giovanniVitale" as="#cancelliere">GIOVANNI VITALE</person> - <role refersTo="#cancelliere">Cancelliere</role></signature>
      </p>
    </conclusions>
  </judgment>
</akomaNtoso>
