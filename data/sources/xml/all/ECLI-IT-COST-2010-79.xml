<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2010</anno_pronuncia>
    <numero_pronuncia>79</numero_pronuncia>
    <ecli>ECLI:IT:COST:2010:79</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>DE SIERVO</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>22/02/2010</data_decisione>
    <data_deposito>26/02/2010</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 5, comma 7, della legge della Regione Abruzzo 24 marzo 2009, n. 4 (Principi generali in materia di riordino degli Enti locali),  promosso dal Presidente del Consiglio dei ministri con ricorso spedito per la notifica il 26 maggio 2009, depositato in cancelleria il 3 giugno 2009 ed iscritto al n. 34 del registro ricorsi 2009.&#13;
 Udito nella camera di consiglio del 10 febbraio 2010 il Giudice relatore Ugo De Siervo.</epigrafe>
    <testo>Ritenuto che con ricorso notificato il 26 maggio 2009 e depositato il successivo 3 giugno (iscritto nel reg. ric. n. 34 del 2009), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha sollevato, in riferimento agli artt. 3, 97 e 117, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 5, comma 7, ultimo periodo, della legge della Regione Abruzzo 24 marzo 2009, n. 4 (Principi generali in materia di riordino degli Enti locali), pubblicata nel Bollettino ufficiale regionale n. 20 del 27 marzo 2009;&#13;
 che l'impugnato art. 5, dopo aver disciplinato le nomine degli organi di vertice degli enti regionali, al comma 7 stabilisce che le indennità di carica degli amministratori, oltre a non poter essere cumulate con le indennità spettanti ai componenti delle Camere e del Parlamento europeo, non sono cumulabili con nessun altro emolumento fisso o variabile derivante da nomina politica di competenza regionale anche presso enti pubblici economici;&#13;
 che, in particolare, questo divieto di cumulo, come testualmente dispone l'impugnato ultimo periodo dell'art. 5, «non vale per gli amministratori dei comuni al di sotto dei 5000 abitanti»; &#13;
 che, secondo il ricorrente, la denunciata previsione violerebbe i princìpi fondamentali di coordinamento della finanza pubblica dettati dall'art. 2, commi 25 e 26, della legge 24 dicembre 2007, n. 244 (Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato. Legge finanziaria 2008);&#13;
 che, più precisamente, il comma 25, dell'art. 2, nel sostituire l'art. 82, comma 2, del decreto legislativo 18 agosto 2000, n. 267 (Testo unico delle leggi sull'ordinamento degli enti locali), prevede: che i consiglieri comunali, provinciali, circoscrizionali, limitatamente ai comuni capoluogo di provincia, e delle comunità montane hanno diritto a percepire un gettone di presenza per la partecipazione a consigli e commissioni; che in nessun caso l'ammontare mensile può superare l'importo pari ad un quarto dell'indennità massima prevista per il rispettivo sindaco o presidente; che nessuna indennità è dovuta ai consiglieri circoscrizionali;&#13;
 che lo stesso art. 25 ha abrogato il comma 6 dell'art. 82, del decreto legislativo n. 267 del 2000, il quale consentiva il cumulo tra indennità di funzione e gettoni di presenza, ove dovuti per mandati elettivi presso enti diversi ricoperti dalla stessa persona;&#13;
 che, dal canto suo, il comma 26 dell'art. 2 ha integralmente sostituito l'art. 83 del decreto legislativo n. 267 del 2000, fissando il divieto per i parlamentari nazionali ed europei, nonché per i consiglieri regionali, di percepire i gettoni di presenza previsti, per gli amministratori locali, dal decreto legislativo n. 267 del 2000;&#13;
 che, dunque, la disposizione impugnata sarebbe in contrasto con l'art. 117, terzo comma, della Costituzione, «oltre che con l'art. 97 Cost.», per violazione dell'art. 82 del decreto legislativo n. 267 del 2000, come modificato, in quanto, nel fare eccezione al divieto con riferimento a qualsivoglia emolumento fisso o variabile, consentirebbe il venir meno del divieto generale di cumulo tra indennità di funzione e gettoni di presenza anche al di là dei limiti mensili ivi espressamente stabiliti, nonché per violazione dell'art. 83 dello stesso decreto legislativo n. 267 del 2000, come modificato, in quanto consentirebbe agli amministratori in parola di cumulare all'indennità, ad essi spettante, i compensi relativi alla partecipazione ad organi o commissioni comunque denominate anche se tale partecipazione è connessa all'esercizio di funzioni pubbliche;&#13;
 che la disposizione impugnata sarebbe, altresì, in contrasto con gli artt. 3 e 97 della Costituzione, giacché la specifica eccezione così stabilita sarebbe irragionevole ed ingiustificata, oltre a porsi in evidente contrasto con il principio di eguaglianza, avendo introdotto un regime di favore esclusivamente nei confronti degli amministratori di piccoli comuni;&#13;
 che la Regione Abruzzo, pur non costituitasi nel presente giudizio, ha presentato istanza di estinzione del giudizio per sopravvenuta cessazione della materia del contendere, giacché l'art. 6 della legge della Regione Abruzzo 4 agosto 2009, n. 12 (Disposizioni di carattere urgente ed indifferibile), ha abrogato l'impugnata disposizione e la stessa non ha avuto medio tempore applicazione alcuna;&#13;
 che con atto notificato il 20 novembre 2009 e depositato il successivo 27 novembre, il Presidente del Consiglio dei ministri ha rinunciato al presente ricorso.&#13;
 Considerato che, in mancanza di costituzione in giudizio della parte convenuta, la rinuncia al ricorso comporta, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (cfr., tra le più recenti, la sentenza n. 247 del 2009 e le ordinanze n. 14 e n. 8 del 2010; n. 292, n. 136 e n. 48 del 2009).</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara estinto il processo.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 febbraio 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente e Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 26 febbraio 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
