<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>315</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:315</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1986</data_decisione>
    <data_deposito>31/12/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, prof. Ettore GALLO, prof. &#13;
 Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, &#13;
 prof. Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco P. CASAVOLA, prof. Antonio BALDASSARRE, &#13;
 prof. Vincenzo CAIANIELLO.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 55 della legge    &#13;
 27 luglio 1978 n. 392 (Disciplina delle locazioni d'immobili urbani),    &#13;
 promosso  con  ordinanza emessa il 10 aprile 1979 dal Pretore di Lodi    &#13;
 nel procedimento civile vertente tra Giani Mario  e  Bianchi  Sergio,    &#13;
 iscritta  al  n.  907  del reg. ord. 1979 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 43 dell'anno 1980;                         &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 12 dicembre 1986 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che  nel  corso  di un giudizio di sfratto per morosità    &#13;
 promosso da Giani Mario contro Bianchi Sergio, il  Pretore  di  Lodi,    &#13;
 con  ordinanza  del  10  aprile  1979  (n.  907  del 1979), sollevava    &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3    &#13;
 e  24  Cost., dell'art. 55 l. 27 luglio 1978 n. 392 (Disciplina delle    &#13;
 locazioni d'immobili urbani), laddove stabilisce che la morosità del    &#13;
 conduttore  può  essere  sanata in sede giudiziale "se il conduttore    &#13;
 alla prima udienza versa l'importo dovuto" e che  "ove  il  pagamento    &#13;
 non avvenga in udienza il giudice, dinanzi a comprovate condizioni di    &#13;
 difficoltà del conduttore, può assegnare un termine non superiore a    &#13;
 giorni 90";                                                              &#13;
      che,  a  parere  del Pretore, la norma citata, escludendo che il    &#13;
 termine  per  sanare  la  morosità  possa  essere  richiesto   anche    &#13;
 nell'ulteriore   corso  del  procedimento  e  possa  comunque  essere    &#13;
 concesso con la pronuncia che condanni  il  conduttore  al  rilascio,    &#13;
 contrasterebbe:  a)  con  l'art.  24,  primo  comma, Cost., in quanto    &#13;
 creerebbe un ostacolo di fatto ai poteri processuali  del  conduttore    &#13;
 convenuto  in  giudizio per morosità; b) con gli artt. 3 e 24 Cost.,    &#13;
 in quanto determinerebbe una ingiustificata disparità di trattamento    &#13;
 tra conduttori realmente morosi e chi ritenga in buona                   &#13;
 fede di non esserlo e di dovere quindi resistere in giudizio, nonché    &#13;
 tra conduttori che traggano  i  mezzi  di  sostentamento  solo  dalla    &#13;
 propria attività di lavoratori subordinati e conduttori abbienti;       &#13;
      che  interveniva  la  Presidenza  del  Consiglio  dei  ministri,    &#13;
 chiedendo che la questione fosse dichiarata non fondata.                 &#13;
    Considerato che la norma denunciata, non limita affatto la normale    &#13;
 tutela giurisdizionale prevista dall'art. 24, primo comma, Cost.,  in    &#13;
 quanto,  lungi dal rappresentare un ostacolo alla possibilità per il    &#13;
 conduttore di far valere le proprie ragioni, prevede al contrario  un    &#13;
 ulteriore specifica agevolazione a suo favore, che si aggiunge, senza    &#13;
 comprimerle  o  menormarle,  alle  facoltà  che  ordinariamente  gli    &#13;
 spettano, in quanto convenuto in giudizio;                               &#13;
      che  del  pari  risulta manifestamente infondata la questione di    &#13;
 incostituzionalità relativa agli artt. 3 e 24 Cost.,  posto  che  la    &#13;
 norma   impugnata   attribuisce   obiettivo  rilievo  alla  mora  del    &#13;
 conduttore, senza limitare l'esercizio del diritto di difesa e  senza    &#13;
 creare ingiustificate disparità di trattamento;                         &#13;
      che  anzi,  contrariamente  a quanto si assume nell'ordinanza di    &#13;
 rimessione, la disposizione dell'art. 55 l. cit. accorda una speciale    &#13;
 protezione proprio ai soggetti meno abbienti, laddove stabilisce che,    &#13;
 ove il pagamento non avvenga in udienza, il giudice può assegnare un    &#13;
 termine  per la sanatoria della mora "dinanzi a comprovate condizioni    &#13;
 di difficoltà del conduttore".                                          &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953  n.  87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 dichiara  manifestamente  non  fondata  la  questione di legittimità    &#13;
 costituzionale dell'art. 55 l. 27  luglio  1978  n.  392  (Disciplina    &#13;
 delle locazioni degli immobili urbani), sollevata in riferimento agli    &#13;
 artt. 3 e 24 Cost. dal Pretore di Lodi con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma  in  camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 19 dicembre 1986.       &#13;
                     Il Presidente: LA PERGOLA                            &#13;
                     Il redattore: SAJA                                   &#13;
    Depositata in cancelleria il 31 dicembre 1986.                        &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
