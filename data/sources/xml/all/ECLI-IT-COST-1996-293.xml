<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>293</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:293</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/1996</data_decisione>
    <data_deposito>22/07/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 2059 del codice    &#13;
 civile, promosso  con  ordinanza  emessa  il    13  giugno  1995  dal    &#13;
 Tribunale  di  Bologna  nel  procedimento  civile  vertente tra Luzzi    &#13;
 Flavio ed altra e Piredda Bruna ed altre,  iscritta  al  n.  473  del    &#13;
 registro  ordinanze  1996 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 20, prima serie speciale, dell'anno 1996;                  &#13;
   Udito nella camera di consiglio  del  10  luglio  1996  il  giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
   Ritenuto  che,  nel  corso di un giudizio civile promosso da Flavio    &#13;
 Luzzi ed altra contro Bruna Piredda ed altre per il risarcimento  dei    &#13;
 danni  a  cose  e alle persone cagionati da un incidente stradale, il    &#13;
 Tribunale di Bologna, con ordinanza del 13 giugno 1995,  pervenuta  a    &#13;
 questa  Corte  il  29  aprile 1996, ha sollevato, in riferimento agli    &#13;
 artt.  24  e  32  Cost.,  questione  di  legittimità  costituzionale    &#13;
 dell'art.      2059   cod.   civ.  nella  parte  in  cui  esclude  la    &#13;
 risarcibilità del danno morale al di fuori di accertate  ipotesi  di    &#13;
 reato;                                                                   &#13;
     che  nel  caso  di specie, dovendosi applicare la presunzione del    &#13;
 concorso di colpa prevista dall'art. 2054, secondo comma, cod.  civ.,    &#13;
 non   sussistono  le  condizioni  richieste  dall'art.  2059  per  la    &#13;
 risarcibilità del danno morale soggettivo;                              &#13;
     che, ad avviso del giudice rimettente, "il danno morale (definito    &#13;
 come  "acuta  sensazione  di  sofferenza    più psichica che fisica,    &#13;
 destinata comunque ad essere riassorbita in un periodo  più  o  meno    &#13;
 lungo,  senza  lasciare strascichi") altro non è che una sottospecie    &#13;
 del danno biologico", inteso in senso ampio come alterazione    dello    &#13;
 standard  psico-fisico  del  soggetto  (con  la sola esclusione delle    &#13;
 attività  finalizzate  a  produrre  un  reddito)    comportante  "un    &#13;
 radicale peggioramento delle abitudini e delle condizioni di vita";      &#13;
     che  pertanto, poiché le due figure di danno - entrambe comprese    &#13;
 dalla sentenza n. 88 del 1979 di   questa Corte nella  categoria  del    &#13;
 danno non patrimoniale - non sono, nella loro essenza, diverse, anche    &#13;
 il  danno morale rientra nell'ambito di tutela dell'art. 32 Cost., il    &#13;
 quale non consente "limiti e compressioni  in  base  a    una  scelta    &#13;
 discrezionale  del  legislatore ordinario, quale dal medesimo operata    &#13;
 con  l'art.  2059  cod.  civ.  nella  parte   in   cui   esclude   la    &#13;
 risarcibilità  del  danno morale al di fuori di accertate ipotesi di    &#13;
 reato",  sicché  "l'applicazione     dell'art.  2059   deve   essere    &#13;
 coordinata con l'esigenza di tutela dei diritti fondamentali, e così    &#13;
 ampliata  ricorrendo,  per  analogia iuris, alla ratio dell'art. 2043    &#13;
 cod. civ.";                                                              &#13;
   Considerato che l'inclusione del danno alla salute nella  categoria    &#13;
 considerata   dall'art.  2059  cod.  civ.  -  in  ragione  della  non    &#13;
 valutabilità diretta in denaro, riconosciuta dalla  sentenza  n.  88    &#13;
 del  1979 di questa Corte e ribadita dalla sentenza n. 372 del 1994 -    &#13;
 non  significa  identificazione  col  danno  morale  soggettivo,   ma    &#13;
 soltanto  riconducibilità delle due figure, quali specie diverse, al    &#13;
 genere del danno non patrimoniale, come intende chiaramente la  prima    &#13;
 sentenza  là  dove afferma che "l'ambito di applicazione degli artt.    &#13;
 2059 cod. civ. e 185 cod. pen.  si  estende  (scil.  oltre  il  danno    &#13;
 morale) fino a ricomprendere ogni danno non suscettibile direttamente    &#13;
 di valutazione economica, compreso quello alla salute";                  &#13;
     che  la  premessa  del  giudice a quo secondo cui il danno morale    &#13;
 costituisce pur sempre una  lesione  della  salute  psico-fisica,  è    &#13;
 contraddetta  da  una  indicazione  costante  della giurisprudenza di    &#13;
 questa Corte nel senso della differenza di natura delle due specie di    &#13;
 danno (cfr. sentenze n. 356 del 1991 e 37 del 1994): pure la sentenza    &#13;
 più recente, n. 372 del 1994, non si è allontanata su questo  punto    &#13;
 dalla  sentenza  n.  184  del  1986,  ma  piuttosto  vi  ha apportato    &#13;
 un'aggiunta con riguardo all'ipotesi particolare -  già  prefigurata    &#13;
 nella  precedente  sentenza n. 37 del 1994 come problema a sé stante    &#13;
 di ordine interpretativo - in cui il danno alla salute,  sofferto  da    &#13;
 una  persona in conseguenza della morte di un familiare cagionata dal    &#13;
 fatto illecito penalmente rilevante  di  un  terzo,  è  "il  momento    &#13;
 terminale  di  un processo patogeno originato dal medesimo turbamento    &#13;
 dell'equilibrio psichico che sostanzia il danno morale soggettivo,  e    &#13;
 che  in  persone  predisposte  da  particolari  condizioni (debolezza    &#13;
 cardiaca, fragilità  nervosa, ecc.), anziché esaurirsi in un patema    &#13;
 d'animo o in uno stato di angoscia transeunte, può degenerare in  un    &#13;
 trauma  fisico  o psichico permanente". In relazione a questa ipotesi    &#13;
 di somatizzazione del  danno  morale,  che  delimitava  la  rilevanza    &#13;
 dell'incidente  di  costituzionalità,  la  sentenza  n.   372 non ha    &#13;
 confuso il danno biologico col pretium doloris ma ne ha parificato il    &#13;
 trattamento giuridico sul presupposto  della  loro  distinzione,  nel    &#13;
 senso  (conforme  alla  sentenza  n.  88 del 1979) che le conseguenze    &#13;
 risarcibili  in  base  agli  elementi  costitutivi  del   fatto-reato    &#13;
 sanzionato  dall'art.  185 cod. pen. si estendono a tutti i danni non    &#13;
 patrimoniali da  esso  cagionati,  compreso  il  danno  alla  salute,    &#13;
 indipendentemente  dal problema generale del coordinamento, in via di    &#13;
 interpretazione, dell'art. 2059 cod. civ. con la diversa  fattispecie    &#13;
 dell'art.  2043  in  ordine  al  danno  biologico causato da un fatto    &#13;
 illecito non qualificato come reato;                                     &#13;
     che pertanto  il  risarcimento  del  danno  morale,  non  essendo    &#13;
 assistito dalla garanzia dell'art. 32 della Costituzione (sentenza n.    &#13;
 37  del  1994  cit.),  può  essere  discrezionalmente  limitato  dal    &#13;
 legislatore a determinate ipotesi, come quelle  sanzionate  dall'art.    &#13;
 185 cod.  pen., cui rinvia sotto questo aspetto l'art. 2059 cod. civ;    &#13;
     che, conseguentemente, non è violato nemmeno l'art. 24 Cost., la    &#13;
 tutela  giurisdizionale  dei diritti essendo garantita nei limiti del    &#13;
 diritto sostanziale;                                                     &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  2059  del  codice  civile,  sollevata,  in    &#13;
 riferimento  agli  artt. 24 e 32 della Costituzione, dal Tribunale di    &#13;
 Bologna con l'ordinanza in epigrafe.                                     &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'11 luglio 1996.                                &#13;
                         Il Presidente: Ferri                             &#13;
                         Il redattore: Mengoni                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 luglio 1996.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
