<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>225</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:225</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/06/1997</data_decisione>
    <data_deposito>04/07/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, prof. Guido NEPPI MODONA, prof. &#13;
 Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art. 4 della legge 8    &#13;
 giugno  1966,  n. 424 (Abrogazione di norme che prevedono la perdita,    &#13;
 la riduzione o la sospensione delle pensioni a carico dello  Stato  o    &#13;
 di  altro  ente pubblico) e dell'art. 21 del d.P.R. 29 dicembre 1973,    &#13;
 n. 1032 (Approvazione del testo unico delle norme  sulle  prestazioni    &#13;
 previdenziali a favore dei dipendenti civili e militari dello Stato),    &#13;
 promosso  con ordinanza emessa il 4 marzo 1996 dalla Corte dei conti,    &#13;
 sezione giurisdizionale per la Regione Lazio,  sul  reclamo  proposto    &#13;
 dal  Procuratore  regionale  della  Corte  dei  conti  in  ordine  al    &#13;
 sequestro conservativo nei confronti di Solfaroli Luigi, iscritta  al    &#13;
 n.  577  del  registro  ordinanze  1996  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 26,  prima  serie  speciale,  dell'anno    &#13;
 1996.                                                                    &#13;
   Udito  nella  camera  di  consiglio  del  7  maggio 1997 il giudice    &#13;
 relatore Francesco Guizzi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un procedimento in materia di danno  erariale  il    &#13;
 Procuratore  regionale  della  Corte dei conti impugnava davanti alla    &#13;
 sezione giurisdizionale per la  Regione  Lazio,  ai  sensi  dell'art.    &#13;
 669-terdecies   del   codice   di   procedura  civile,  il  sequestro    &#13;
 conservativo dell'indennità di fine servizio, confermato dal giudice    &#13;
 designato solo sino al limite del quinto dell'emolumento.                &#13;
   In  sede  di  reclamo,  la  sezione  ha  sollevato  d'ufficio,   in    &#13;
 riferimento  all'art. 3 della Costituzione, questione di legittimità    &#13;
 costituzionale degli artt. 4  della  legge  8  giugno  1966,  n.  424    &#13;
 (Abrogazione  di  norme  che  prevedono la perdita, la riduzione o la    &#13;
 sospensione delle pensioni a carico  dello  Stato  o  di  altro  ente    &#13;
 pubblico),  e  21  del d.P.R. 29 dicembre 1973, n. 1032 (Approvazione    &#13;
 del testo unico delle norme sulle prestazioni previdenziali a  favore    &#13;
 dei dipendenti civili e militari dello Stato).                           &#13;
    2.  -  Osserva  il  giudice  rimettente  che  nulla  sarebbe stato    &#13;
 innovato dopo la recente sentenza della Corte  costituzionale  n.  99    &#13;
 del 1993, che ha escluso la sequestrabilità o pignorabilità - entro    &#13;
 i  limiti  stabiliti  dall'art.  545 del codice di procedura civile -    &#13;
 dell'indennità di fine rapporto spettante  ai  dipendenti  pubblici,    &#13;
 avendo  circoscritto  la  propria  efficacia al rapporto ordinario di    &#13;
 credito-debito fra il "privato creditore" e il "pubblico dipendente".    &#13;
 Non  si  estenderebbe,  pertanto,  al  rapporto  generato  dal  danno    &#13;
 erariale  - derivante alla pubblica amministrazione dal comportamento    &#13;
 dei  propri  dipendenti - come disciplinato dagli artt. 4 della legge    &#13;
 n. 424 del 1966 e 21 del d.P.R. n. 1032 del 1973, che  non  prevedono    &#13;
 limiti alla sequestrabilità o pignorabilità del trattamento di fine    &#13;
 rapporto per la riscossione dei crediti di detta natura.                 &#13;
   La   giurisprudenza   costituzionale  -  ricorda  a  tal  proposito    &#13;
 l'ordinanza - ha sostanzialmente parificato, di fronte  al  creditore    &#13;
 privato,  le  posizioni dei dipendenti privati e pubblici, eliminando    &#13;
 il privilegio in favore di questi ultimi per  avere  esteso  anche  a    &#13;
 costoro  la  disciplina  (più  sfavorevole)  prevista dall'art. 545,    &#13;
 quarto comma, del codice di procedura civile.  Tuttavia  permane  una    &#13;
 diversità fra le due categorie, con riferimento ai crediti derivanti    &#13;
 da  danno  erariale, in relazione ai quali gli artt. 4 della legge n.    &#13;
 424 del 1966 e  21  del  d.P.R.    n.  1032  del  1973  ammettono  la    &#13;
 possibilità  di  sequestrare o pignorare l'intera indennità di fine    &#13;
 rapporto. Si  tratterebbe,  ad  avviso  della  Corte  dei  conti,  di    &#13;
 disposizioni  volte  alla  tutela  delle  ragioni erariali, in quanto    &#13;
 finalizzate al recupero di somme dovute per obbligazioni risarcitorie    &#13;
 conseguenti a  violazioni  di  obblighi  di  servizio  da  parte  dei    &#13;
 pubblici  dipendenti;  disposizioni,  queste,  che  attribuiscono  un    &#13;
 potere autoritativo all'amministrazione e si giustificano in  base  a    &#13;
 interessi  superiori,  quali  la  salvaguardia  dei crediti erariali,    &#13;
 l'ordinato andamento della pubblica  amministrazione,  l'esigenza  di    &#13;
 legalità.   Ma   si   configurerebbe   pur   sempre   un  privilegio    &#13;
 dell'amministrazione creditrice rispetto alla diversa disciplina  che    &#13;
 l'ordinamento  stabilisce  per i dipendenti privati nei confronti dei    &#13;
 propri datori di lavoro.                                                 &#13;
   Richiamata  da  ultimo  nella  sentenza  n.   99   del   1993,   la    &#13;
 giurisprudenza  costituzionale  mirerebbe  a omologare i due settori.    &#13;
 Senonché  persisterebbe  una  disparità  in  danno   del   pubblico    &#13;
 dipendente rispetto a quelli privati, che sono soggetti alle norme di    &#13;
 diritto  comune:  con  ciò  palesandosi un contrasto con la tendenza    &#13;
 equiparativa, peraltro confermata  di  recente  dalla  normativa  sul    &#13;
 pubblico  impiego e ribadita da una serie di pronunce di questa Corte    &#13;
 in base alle quali il trattamento di fine  rapporto  si  caratterizza    &#13;
 come retribuzione differita, anche in ragione della esigua componente    &#13;
 previdenziale. Di qui, la richiesta di uniformare i due comparti.<diritto>Considerato in diritto</diritto>1.  -  Vengono all'esame della Corte gli artt. 4 della legge n. 424    &#13;
 del 1966 e 21 del d.P.R. n. 1032 del 1973, nella  parte  in  cui  non    &#13;
 prevedono  che,  nelle ipotesi di danno erariale, sia sequestrabile o    &#13;
 pignorabile - nei limiti di cui all'art. 545 del codice di  procedura    &#13;
 civile  -  l'indennità  di  fine  rapporto  dei  dipendenti civili e    &#13;
 militari dello Stato. Di tali norme dubita la sezione giurisdizionale    &#13;
 per la Regione Lazio della Corte dei conti, in relazione  all'art.  3    &#13;
 della    Costituzione,    poiché    manterrebbero   un   trattamento    &#13;
 ingiustificatamente   sfavorevole   nei   confronti   dei    pubblici    &#13;
 dipendenti,  circa  la  ipotesi  devoluta  all'esame  del rimettente:    &#13;
 d'onde, il denunciato contrasto con la tendenza alla omologazione fra    &#13;
 i due settori quale risulta dalla giurisprudenza costituzionale (cfr.    &#13;
 da ultimo la sentenza n.    99  del  1993),  ribadita  dalla  recente    &#13;
 disciplina legislativa del pubblico impiego.                             &#13;
    2. - La questione è fondata.                                         &#13;
    Occupandosi  del regime giuridico dell'indennità di fine rapporto    &#13;
 erogata ai dipendenti delle pubbliche amministrazioni (d.P.R. n.  180    &#13;
 del 1950), questa Corte è intervenuta, con la  sentenza  n.  99  del    &#13;
 1993, sul trattamento loro riservato, e ha esteso la sequestrabilità    &#13;
 o  pignorabilità  per  ogni  credito,  negli stessi limiti stabiliti    &#13;
 dall'art. 545, quarto comma, del codice di procedura civile. Ciò per    &#13;
 l'ingiustificata disparità fra i dipendenti pubblici, fino ad allora    &#13;
 privilegiati, e quelli del comparto privato che erano sottoposti alla    &#13;
 soggezione, sebbene limitata, del potere  legalmente  esercitato  dai    &#13;
 creditori  ordinari.  Disparità  non  più tollerabile, secondo tale    &#13;
 pronuncia,  per  la  progressiva  eliminazione  delle  differenze  in    &#13;
 materia, quale sviluppo della tendenza a omogeneizzare i due settori.    &#13;
   L'istituto  in  esame  è  stato  oggetto  di  una  valutazione del    &#13;
 legislatore, volta a una graduale equiparazione, che pur nel rispetto    &#13;
 delle peculiarità dei due tipi di lavoro, si palesa nel nuovo  ruolo    &#13;
 affidato  alla  contrattazione  collettiva  nell'ambito  del  settore    &#13;
 pubblico, come si riscontra per le prestazioni di cui al  giudizio  a    &#13;
 quo.                                                                     &#13;
    3.  -  Le  argomentazioni  svolte  conducono  a  una  decisione di    &#13;
 fondatezza.  Non si può infatti asserire, di contro,  che  l'assenza    &#13;
 di  limiti  al  pignoramento  o  al  sequestro conosca le sue ragioni    &#13;
 giustificatrici nella tutela rafforzata, prevista per l'erario, se (e    &#13;
 quando) esso debba realizzare il ristoro per il  danno  cagionato  da    &#13;
 dipendenti  incapaci e infedeli, in quanto è proprio tale privilegio    &#13;
 che, nel bilanciamento dei valori, non può prevalere sul diritto  al    &#13;
 trattamento  di  fine rapporto del lavoratore, pubblico o privato che    &#13;
 sia.                                                                     &#13;
   Va  pertanto  dichiarata  l'illegittimità   costituzionale   delle    &#13;
 disposizioni  censurate,  nella  parte  in  cui  non prevedono che la    &#13;
 sequestrabilità o la pignorabilità del trattamento di fine rapporto    &#13;
 dei dipendenti civili e militari dello Stato, per i crediti derivanti    &#13;
 da danno erariale, sia contenuta nei limiti  previsti  dall'art.  545    &#13;
 del codice di procedura civile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità costituzionale degli artt. 4 della legge    &#13;
 8 giugno 1966, n. 424 (Abrogazione di norme che prevedono la perdita,    &#13;
 la riduzione o la sospensione delle pensioni a carico dello  Stato  o    &#13;
 di  altro  ente  pubblico),  e 21 del d.P.R 29 dicembre 1973, n. 1032    &#13;
 (Approvazione  del  testo  unico  delle   norme   sulle   prestazioni    &#13;
 previdenziali a favore dei dipendenti civili e militari dello Stato),    &#13;
 nella  parte  in  cui  prevedono,  per i dipendenti civili e militari    &#13;
 dello Stato, la sequestrabilità o la pignorabilità delle indennità    &#13;
 di fine rapporto di lavoro, anche per i crediti  da  danno  erariale,    &#13;
 senza  osservare  i limiti stabiliti dall'art. 545, quarto comma, del    &#13;
 codice di procedura civile.                                              &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 19 giugno 1997.                               &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Guizzi                             &#13;
                        Il cancelliere: Malvica                           &#13;
   Depositata in cancelleria il 4 luglio 1997.                            &#13;
                        Il cancelliere: Malvica</dispositivo>
  </pronuncia_testo>
</pronuncia>
