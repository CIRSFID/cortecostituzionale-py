<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>85</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:85</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/05/1963</data_decisione>
    <data_deposito>08/06/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 251 del Codice di  &#13;
 procedura  civile, promosso con ordinanza emessa il 26 ottobre 1962 dal  &#13;
 Pretore di Trivento  nel  procedimento  penale  a  carico  di  Donatone  &#13;
 Nazario  e  Civico  Agostino, iscritta al n. 188 del Registro ordinanze  &#13;
 1962 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 313  del  &#13;
 7 dicembre 1962.                                                         &#13;
     Udita  nella camera di consiglio del 7 maggio 1963 la relazione del  &#13;
 Giudice Giovanni Cassandro;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - In una causa civile davanti al Pretore di Trivento  i  signori  &#13;
 Nazario   Donatone   e  Agostino  Civico  rifiutarono  di  prestare  il  &#13;
 giuramento prescritto dall'art.  251 del Cod. proc.  civile,  assumendo  &#13;
 che  la  religione  "protestante  evangelica",  che  essi professavano,  &#13;
 vietava loro di prestarlo.                                               &#13;
     Nel procedimento penale promosso in conseguenza a loro carico,  per  &#13;
 rispondere  del  reato  previsto  dall'art.  366 del Cod. penale, i due  &#13;
 imputati  affermarono  che  la  religione  "pentecostale",   che   essi  &#13;
 professavano,   vietava  loro,  conformemente  all'insegnamento  di  S.  &#13;
 Matteo, capitolo V, 34-37, di prestare giuramento sotto qualsiasi forma  &#13;
 e in qualsiasi modo. La difesa dei due imputati sollevò  la  questione  &#13;
 di  costituzionalità  dell'art.  366, secondo comma, del Cod.  penale,  &#13;
 per  contrasto  con  l'art.  8  in  relazione  all'articolo  21   della  &#13;
 Costituzione,  sostenendo che il rifiuto dei due imputati sarebbe stato  &#13;
 motivato  dall'osservanza,  alla  quale  essi  erano  tenuti,  per  non  &#13;
 incorrere  in  peccato,  dei precetti della loro religione. Il Pubblico  &#13;
 Ministero si associò alla richiesta della difesa.                       &#13;
     Il Vicepretore di Trivento, viceversa, ha sottoposto alla Corte  la  &#13;
 questione  di legittimità costituzionale dell'art.  251 del Cod. proc.  &#13;
 civile, non soltanto perché in contrasto con gli artt. 8  e  21  della  &#13;
 Costituzione,  ma  anche  perché  in contrasto con l'art. 19, il quale  &#13;
 sancisce il diritto di tutti di professare liberamente la propria  fede  &#13;
 religiosa  in  qualsiasi  forma, di farne propaganda, di esercitarne in  &#13;
 privato o in pubblico il culto, purché non si tratti di riti  contrari  &#13;
 al buon costume. In conseguenza, con ordinanza 26 ottobre 1962, egli ha  &#13;
 sospeso il giudizio e rimesso gli atti a questa Corte.                   &#13;
     L'ordinanza,  notificata al Presidente del Consiglio dei Ministri e  &#13;
 comunicata  ai  Presidenti  dei  due  rami  del  Parlamento,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale n.  313 del 7 dicembre 1962.         &#13;
     Gli  imputati  non si sono costituiti e il Presidente del Consiglio  &#13;
 non è intervenuto.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. -  La  questione  di  costituzionalità,  che  viene  sottoposta  &#13;
 all'esame  della  Corte,  si  risolve  nello stabilire se il rifiuto di  &#13;
 prestare giuramento con qualsiasi  formula  e  in  qualsiasi  modo,  in  &#13;
 obbedienza  a un precetto religioso, trovi giustificazione in una norma  &#13;
 della Costituzione.                                                      &#13;
     La questione non è fondata.                                         &#13;
     In primo luogo, la  Corte  non  ritiene  che  possa  profilarsi  un  &#13;
 contrasto  dell'art.  251  del  Cod.  proc.  civile,  che  è  la norma  &#13;
 impugnata, con gli artt. 8 e 21 della Costituzione, essendo evidente  e  &#13;
 non  bisognevole  di  dimostrazione  il  fatto che l'obbligo imposto ai  &#13;
 testimoni  di giurare secondo una certa formula, previsto dal ricordato  &#13;
 articolo e sanzionato penalmente dall'art. 366, secondo comma, del Cod.  &#13;
 penale, non  viola  la  eguale  libertà  delle  confessioni  religiose  &#13;
 davanti  alla  legge,  dato  che  esso  ha  come  destinatari  tutti  i  &#13;
 cittadini,  quale  che  sia  la  religione  che  essi  professino,  né  &#13;
 interferisce   negli   ordinamenti   statutari  delle  confessioni  non  &#13;
 cattoliche o nel procedimento previsto per la disciplina  dei  rapporti  &#13;
 tra  queste  confessioni  e  lo  Stato  (art.  8);  ed essendo non meno  &#13;
 evidente l'altro fatto che quell'obbligo dei testimoni non  ferisce  il  &#13;
 diritto  che tutti hanno di manifestare liberamente il proprio pensiero  &#13;
 con la parola, lo scritto e ogni altro mezzo di  diffusione  (art.  21,  &#13;
 primo  comma),  diritto che resta in tutta la sua ampiezza garantito ai  &#13;
 cittadini ai quali l'ordinamento imponga un comportamento della  natura  &#13;
 di quello previsto dalla norma impugnata.                                &#13;
     2.  -  La  questione,  pertanto,  si limita, come del resto si può  &#13;
 dedurre dall'ordinanza pretorile, che ha aggiunto agli artt.  8  e  21,  &#13;
 che  la  difesa delle parti e il Pubblico Ministero ritenevano violati,  &#13;
 l'art.  19  della  Costituzione,  soltanto  al  contrasto  della  norma  &#13;
 impugnata con quest'ultimo articolo, il quale riconosce ai cittadini il  &#13;
 diritto   di  professare  liberamente  la  propria  fede  religiosa  in  &#13;
 qualsiasi forma, individuale o associata,  di  farne  propaganda  e  di  &#13;
 esercitarne  in  privato o in pubblico il culto, col limite segnato dal  &#13;
 carattere eventualmente contrario al buon costume dei riti professati e  &#13;
 propagandati.                                                            &#13;
     Tuttavia, nemmeno questo contrasto sussiste.                         &#13;
     In  primo  luogo,  potrebbe  addirittura  sembrare  che  anche   la  &#13;
 violazione dell'ora ricordato art. 19 sia manifestamente insussistente,  &#13;
 se  la  libertà  religiosa,  garantita  dal  precetto  costituzionale,  &#13;
 dovesse essere interpretata come libertà di professione religiosa e di  &#13;
 culto in ogni sua forma e senza altro limite che  non  sia  quello  del  &#13;
 buon costume, giacché in questa ipotesi sarebbe evidente che l'obbligo  &#13;
 imposto  a  tutti  i  cittadini  di  osservare  il comportamento di cui  &#13;
 all'articolo 251 del Cod. proc. civile non  contrasterebbe  con  quella  &#13;
 libertà.    Ma  la  libertà  religiosa  deve essere intesa anche come  &#13;
 libertà da ogni coercizione che imponga il compimento di atti di culto  &#13;
 propri di questa o quella confessione da persone che  non  siano  della  &#13;
 confessione  alla  quale l'atto di culto, per così dire, appartiene. E  &#13;
 lo stesso è da dire perfino quando l'atto  di  culto  appartenga  alla  &#13;
 confessione professata da colui al quale esso sia imposto:  perché non  &#13;
 è dato allo Stato di interferire, come che sia, in un "ordine" che non  &#13;
 è  il  suo,  se  non  ai  fini e nei casi espressamente previsti dalla  &#13;
 Costituzione.                                                            &#13;
     Ma, pur accogliendo della libertà religiosa questa definizione, la  &#13;
 questione di costituzionalità, sollevata dal Pretore di Trivento, deve  &#13;
 essere dichiarata non fondata.                                           &#13;
     La norma dell'art. 251  del  Cod.  proc.  civile  (come  del  resto  &#13;
 l'altra  dell'art.  449  del  Cod.  proc. penale) non impone un atto di  &#13;
 culto, perché la formula che vi è contenuta, come già  la  Corte  ha  &#13;
 affermato  (sentenza n.   58 del 6 luglio 1960 e ordinanza n. 15 del 17  &#13;
 marzo 1961), ha il carattere di richiamo a  generali  valori  religiosi  &#13;
 che  non  possono essere ascritti a questa o a quella "denominazione" o  &#13;
 "confessione", sicché essa non interviene  nell'ordine  proprio  delle  &#13;
 religioni  professate,  ma  sta  e  resta nell'"ordine" statale, che è  &#13;
 indipendente  e  sovrano  e  non può essere condizionato o menomato da  &#13;
 precetti a sé estranei.                                                 &#13;
     La libertà religiosa, quale è stata definita dalla  Costituzione,  &#13;
 non   può   essere  intesa  in  guisa  da  contrastare  e  soverchiare  &#13;
 l'ordinamento giuridico dello Stato,  tutte  le  volte  in  cui  questo  &#13;
 imponga ai cittadini obblighi che, senza violare la libertà religiosa,  &#13;
 nel  senso  che è stato sopra definito, si assumano vietati dalla fede  &#13;
 religiosa dei destinatari della norma: tanto più,  poi,  quando,  come  &#13;
 nel  caso  in  esame,  l'obbligo  ha la sua ultima fonte in un precetto  &#13;
 costituzionale, quale quello contenuto nel secondo comma  dell'art.  54  &#13;
 della  Costituzione,  il  quale  stabilisce  che  "i cittadini cui sono  &#13;
 affidate  funzioni  pubbliche  hanno  il  dovere  di   adempierle   con  &#13;
 disciplina  ed  onore,  prestando  giuramento  nei  casi previsti dalla  &#13;
 legge".</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  251 del Cod. proc. civile, in riferimento agli artt. 8, 21 e  &#13;
 19 della Costituzione.                                                   &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 25 maggio 1963.         &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE  VERZÌ.</dispositivo>
  </pronuncia_testo>
</pronuncia>
