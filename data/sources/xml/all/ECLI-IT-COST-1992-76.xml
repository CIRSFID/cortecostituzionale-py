<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>76</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:76</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/02/1992</data_decisione>
    <data_deposito>28/02/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Cesare MIRABELLI, prof. Francesco GUIZZI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 438,439,440 e    &#13;
 442  del  codice di procedura penale promosso con ordinanza emessa il    &#13;
 14 febbraio 1991 dal Giudice per le indagini  preliminari  presso  il    &#13;
 Tribunale  di Rieti nel procedimento penale a carico di Conti Galafro    &#13;
 ed altro iscritta al n. 644 del registro ordinanze 1991 e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  41,  prima  serie    &#13;
 speciale, dell'anno 1991;                                                &#13;
    Udito  nella  camera  di  consiglio del 5 febbraio 1992 il Giudice    &#13;
 relatore prof. Vincenzo Caianiello;                                      &#13;
    Ritenuto che con ordinanza in data  14  febbraio  1991  (pervenuta    &#13;
 alla  Corte  costituzionale  il 3 ottobre 1991 - reg. ord. n. 644 del    &#13;
 1991) il Giudice per le Indagini preliminari presso il  Tribunale  di    &#13;
 Rieti   ha  sollevato,  in  riferimento  agli  artt.  3  e  25  della    &#13;
 Costituzione, questione di legittimità  costituzionale  degli  artt.    &#13;
 438,  439,  440  e 442 del codice di procedura penale, nelle parti in    &#13;
 cui  subordinano  al  consenso  non  motivato  ed  insindacabile  del    &#13;
 pubblico  ministero  l'adozione  del  giudizio  abbreviato  richiesto    &#13;
 dall'imputato, non consentendo al  giudice  di  valutare  le  ragioni    &#13;
 addotte  dal  pubblico  ministero a giustificazione del "dissenso", e    &#13;
 non attribuendogli, una volta  ritenuto  ingiustificato  il  dissenso    &#13;
 medesimo,  il  potere  di  applicare  la  riduzione  di pena prevista    &#13;
 dall'art. 442, secondo comma, dello stesso codice;                       &#13;
      che,  ad  avviso  del  giudice  a  quo,   le   norme   impugnate    &#13;
 violerebbero  gli  artt.  3  e  25  della  Costituzione, determinando    &#13;
 un'irragionevole disparità di trattamento, in primo luogo fra accusa    &#13;
 e difesa, in secondo luogo fra imputati  nell'ambito  di  uno  stesso    &#13;
 procedimento  o  per  gli  stessi  reati,  e,  infine,  rispetto alla    &#13;
 disciplina dettata per il patteggiamento,  nell'ambito  della  quale,    &#13;
 l'esercizio della funzione giurisdizionale non risulta menomato dalla    &#13;
 scelta insindacabile del pubblico ministero;                             &#13;
      che  non  si sono costituite le parti né ha spiegato intervento    &#13;
 l'Avvocatura generale dello Stato.                                       &#13;
    Considerato che, con sentenza n. 81 del 1991, questa Corte ha già    &#13;
 dichiarato, l'illegittimità costituzionale  del  combinato  disposto    &#13;
 degli artt. 438, 439, 440 e 442 del codice di procedura penale, nella    &#13;
 parte  in  cui  non  prevede  che  il  pubblico ministero, in caso di    &#13;
 dissenso, sia tenuto ad enunciarne le ragioni e nella  parte  in  cui    &#13;
 non  prevede  che  il giudice, quando a dibattimento concluso ritiene    &#13;
 ingiustificato il dissenso del pubblico  ministero,  possa  applicare    &#13;
 all'imputato la riduzione di pena contemplata dall'art. 442, comma 2,    &#13;
 dello stesso codice;                                                     &#13;
      che la questione sollevata va pertanto dichiarata manifestamente    &#13;
 inammissibile.                                                           &#13;
    Visti,  gli artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt.  438,  439,  440  e  442  del    &#13;
 codice  di procedura penale, sollevata, in riferimento agli artt. 3 e    &#13;
 25 della Costituzione, dal Giudice per le Indagini preliminari presso    &#13;
 il Tribunale di Rieti con l'ordinanza indicata in epigrafe.              &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 17 febbraio 1992.                             &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 28 febbraio 1992.                        &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
