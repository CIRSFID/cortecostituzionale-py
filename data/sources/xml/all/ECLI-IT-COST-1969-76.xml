<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>76</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:76</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Giuseppe Branca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/03/1969</data_decisione>
    <data_deposito>11/04/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI &#13;
 BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  6,  comma  &#13;
 secondo,  del  contratto collettivo nazionale 23 dicembre 1939, tuttora  &#13;
 in vigore  ex  art.  43  del  decreto  legislativo  luogotenenziale  23  &#13;
 novembre  1944, n. 369, promosso con ordinanza emessa il 15 giugno 1967  &#13;
 dal tribunale di Belluno nel procedimento  civile  vertente  tra  Tisot  &#13;
 Antonio  e l'I.N.A.M., iscritta al n. 161 del Registro ordinanze 1967 e  &#13;
 pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  221  del  2  &#13;
 settembre 1967.                                                          &#13;
     Visti  gli  atti di costituzione di Tisot Antonio e dell'I.N.A.M. e  &#13;
 d'intervento del Presidente del Consiglio dei Ministri;                  &#13;
     udita nell'udienza pubblica del 12  marzo  1969  la  relazione  del  &#13;
 Giudice Giuseppe Branca;                                                 &#13;
     uditi  gli  avvocati  Ugo  Novelli e Agostino Perale, per il Tisot,  &#13;
 l'avv. Arturo Carlo Jemolo, per l'I.N.A.M., ed  il  sostituto  Avvocato  &#13;
 generale  dello Stato Vito Cavalli, per il Presidente del Consiglio dei  &#13;
 Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Nel corso d'un giudizio civile, proposto  dal  signor  Antonio  &#13;
 Tisot  nei  confronti  dell'I.N.A.M.,  il  tribunale  di  Belluno,  con  &#13;
 ordinanza  15  giugno  1967,  sollevava   questione   di   legittimità  &#13;
 costituzionale  dell'art.  6,  comma  secondo, del contratto collettivo  &#13;
 nazionale 23 dicembre 1939, tuttora in vigore ex art.  43  del  decreto  &#13;
 legislativo  luogotenenziale  23  novembre  1944,  n. 369. La norma non  &#13;
 consente ai familiari degli  impiegati  dell'industria  il  ricovero  a  &#13;
 spese  dell'I.N.A.M.,  per  malattie  nervose  o mentali e ad andamento  &#13;
 cronico. Perciò contrasterebbe con l'art. 38 della  Costituzione,  che  &#13;
 vuole  l'assicurazione,  da  parte  di  organi e istituti predisposti o  &#13;
 integrati dallo Stato, di mezzi adeguati alle esigenze di vita in  caso  &#13;
 di  malattia  e  non  può  non  riferirsi  ai familiari del prestatore  &#13;
 d'opera: se restassero a carico del  lavoratore  proprio  le  spese  di  &#13;
 spedalità per le malattie più costose che colpiscano la sua famiglia,  &#13;
 verrebbe  meno  quella garanzia d'un'esistenza "libera e dignitosa" che  &#13;
 riposa nell'art. 36 della Costituzione.                                  &#13;
     La difesa del Tisot, nelle  deduzioni  depositate  il  9  settembre  &#13;
 1967,  sostiene  innanzi  tutto  la  proponibilità della questione (il  &#13;
 contratto collettivo 23 dicembre 1939  ha  forza  di  legge  in  virtù  &#13;
 dell'art.  7 legge Il gennaio 1943, n. 138, istitutiva dell'I.N.A.M., o  &#13;
 del  citato  art.  43  del decreto legislativo luogotenenziale 1944, n.  &#13;
 369, che mantiene in vita i contratti collettivi resi obbligatori  erga  &#13;
 omnes  dalla  legge  3  aprile  1926,  n. 563); afferma tuttavia che il  &#13;
 contratto impugnato (art. 6, comma secondo) non sarebbe applicabile  al  &#13;
 caso  di  specie  (e che perciò la Corte potrebbe emanare una sentenza  &#13;
 interpretativa di rigetto) poiché i limiti  e  le  modalità  relative  &#13;
 alla  assistenza  dei  familiari  dovevano  ricavarsi (secondo la legge  &#13;
 istitutiva  dell'I.N.A.M.  del  1943)  non  dai  contratti   collettivi  &#13;
 preesistenti,  come  quello  denunciato,  ma  dai contratti futuri (che  &#13;
 mancano); conclude infine chiedendo  che,  se  non  si  accetta  questa  &#13;
 interpretazione,  l'art.  6  sia  dichiarato  illegittimo  per i motivi  &#13;
 espressi nell'ordinanza di rinvio:   che l'art. 38  della  Costituzione  &#13;
 garantisca  anche  l'assistenza  ai  familiari  del lavoratore, sarebbe  &#13;
 provato   perfino   dalla   precedente,   citata    legge    istitutiva  &#13;
 dell'I.N.A.M., che soccorre anche costoro.                               &#13;
     2.  -  L'I.N.A.M.,  nelle  deduzioni  presentate  il 1 agosto 1967,  &#13;
 premette che un antico contratto collettivo, qualora gli  si  riconosca  &#13;
 forza  di  legge,  resta sempre un contratto:   impegni o rinuncie, che  &#13;
 contenga, potranno rivelarsi contrastanti con la Costituzione,  emanata  &#13;
 successivamente,  e  perciò  inefficaci;  ma  non si potrà dichiarare  &#13;
 l'incostituzionalità dell'accordo solo perché una parte  avrebbe  ora  &#13;
 maggiori  diritti  di  quanto  esso  le  attribuiva: se questi maggiori  &#13;
 diritti derivassero  da  una  legge  ordinaria  o  dalla  Costituzione,  &#13;
 avrebbero  efficacia  automatica  senza  bisogno d'una dichiarazione di  &#13;
 incostituzionalità del contratto.                                       &#13;
     Comunque, secondo la  difesa  dell'I.N.A.M.,  né  la  legislazione  &#13;
 ordinaria,  né  l'art.  38  della  Costituzione  estendono  le  misure  &#13;
 assistenziali alle malattie mentali o croniche. La norma costituzionale  &#13;
 enuncia solo un principio che dovrà realizzarsi con leggi  future,  un  &#13;
 ideale  che  non  ha  trovato  finora  piena  attuazione.  L'assistenza  &#13;
 malattie ha incontrato e incontra ancora limiti che  sono  direttamente  &#13;
 connessi con la misura dei contributi corrisposti dai datori di lavoro:  &#13;
 limiti  che  inoltre  hanno una propria giustificazione per le malattie  &#13;
 mentali, dato che queste, a differenza delle altre,  non  costituiscono  &#13;
 un  semplice  episodio  della  vita  del lavoratore e non consentono le  &#13;
 normali forme d'assistenza, per così dire, "di massa";  tanto  che  in  &#13;
 tutta  la  nostra  tradizione  legislativa  esse  hanno sempre avuto un  &#13;
 capitolo a parte. La norma, che s'è adeguata  a  questa  realtà,  non  &#13;
 violerebbe perciò la Costituzione.                                      &#13;
     3.   -   Nell'atto   d'intervento  del  Presidente  del  Consiglio,  &#13;
 depositato il 21 settembre 1967, l'Avvocatura dello Stato argomenta  in  &#13;
 modo analogo alla difesa dell'I.N.A.M.                                   &#13;
     4.  -  Le  parti  hanno presentato memorie: quella della Avvocatura  &#13;
 dello Stato richiama la sentenza 1963 n. 1 della Corte  costituzionale,  &#13;
 che ha negato forza di legge alle norme corporative.                     &#13;
     5. - Nella discussione orale si sono riassunte e chiarite le difese  &#13;
 scritte.<diritto>Considerato in diritto</diritto>:                          &#13;
     È   stato   denunciato,   per   contrasto   con  l'art.  38  della  &#13;
 Costituzione,  l'art.  6,  comma  secondo,  del  contratto   collettivo  &#13;
 nazionale  23  dicembre  1939,  tuttora  vigente ex art. 43 del decreto  &#13;
 legislativo luogotenenziale 23 novembre 1944, n. 369.                    &#13;
     La  questione è inammissibile, come risulta anche dalla precedente  &#13;
 sentenza 1963 n. 1 della Corte costituzionale.                           &#13;
     Notoriamente i contratti collettivi, come gli altri atti  normativi  &#13;
 previsti  nell'art.  5  delle disposizioni sulla legge in generale, non  &#13;
 ebbero forza di legge  nel  sistema  in  cui  sorsero,  tanto  che  non  &#13;
 potevano  derogare neanche alle disposizioni imperative dei regolamenti  &#13;
 (art. 7 delle disposizioni sulla  legge  in  generale).  Caduto  questo  &#13;
 sistema,  il  decreto legislativo luogotenenziale 1944 n. 369 (art. 43)  &#13;
 non dette forza di legge  alle  norme  corporative,  ma  si  limitò  a  &#13;
 mantenere inalterata la loro originaria efficacia: non "legificò" tali  &#13;
 norme,  ma  riconobbe  agli  atti  che  le  avevano poste la permanenza  &#13;
 dell'antico vigore.   Non a caso  nuovi  contratti  collettivi  possono  &#13;
 "modificarle"  con  effetto  per  gli iscritti alle associazioni che li  &#13;
 stipulino:  ciò proprio in virtù  dello  stesso  decreto  legislativo  &#13;
 luogotenenziale  1944,  n.  369,  che  verosimilmente  non  lo  avrebbe  &#13;
 consentito se avesse inteso attribuire forza di legge all'insieme delle  &#13;
 norme corporative mantenute in efficienza; rispetto alle quali pertanto  &#13;
 non si possono sollevare questioni di legittimità costituzionale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione, sollevata,  con  ordinanza  15  &#13;
 giugno  1967  del tribunale di Belluno in riferimento all'art. 38 della  &#13;
 Costituzione, sulla  legittimità  costituzionale  dell'art.  6,  comma  &#13;
 secondo,  del contratto collettivo nazionale 23 dicembre 1939 in vigore  &#13;
 ex art. 43 del decreto legislativo luogotenenziale 23 novembre 1944, n.  &#13;
 369.                                                                     &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 27 marzo 1969.                                &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI -  ERCOLE  ROCCHETTI  -  ENZO  &#13;
                                   CAPALOZZA    -    VINCENZO    MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
