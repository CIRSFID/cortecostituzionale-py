<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1980</anno_pronuncia>
    <numero_pronuncia>59</numero_pronuncia>
    <ecli>ECLI:IT:COST:1980:59</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Brunetto Bucciarelli Ducci</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/04/1980</data_decisione>
    <data_deposito>22/04/1980</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - &#13;
 Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA &#13;
 - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di  legittimità  costituzionale  dell'art.  2,  &#13;
 lett.  a),  del d.P.R. 4 agosto 1978, n. 413 (concessione di amnistia e  &#13;
 indulto) promossi con le seguenti ordinanze:                             &#13;
     1) n.3 ordinanze emesse il 17, il 10  e  il  24  gennaio  1979  dal  &#13;
 Pretore  di  Reggio  Emilia  rispettivamente  nei procedimenti penali a  &#13;
 carico di Menozzi Giovanni ed altro,  Repetti  Sebastiano  ed  altri  e  &#13;
 Oleari  Benito, iscritte ai numeri 202,203 e 263 del registro ordinanze  &#13;
 1979 e pubblicate nella Gazzetta  Ufficiale  della  Repubblica  n.  102  &#13;
 dell'11 aprile 1979 e n. 154 del 6 giugno 1979;                          &#13;
     2)  ordinanza  emessa  il 20 febbraio 1979 dal Pretore di Correggio  &#13;
 nel procedimento penale  a  carico  di  Branchetti  William  ed  altri,  &#13;
 iscritta  al  n.  291  del  registro  ordinanze 1979 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 168 del 20 giugno 1979;           &#13;
     3) ordinanza emessa il 23 settembre 1978 dal Pretore di Chieri  nel  &#13;
 procedimento penale a carico di Vasino Giuseppe, iscritta al n. 406 del  &#13;
 registro  ordinanze  1979  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 196 del 18 luglio 1979.                                    &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 5 dicembre 1979 il Giudice relatore  &#13;
 Brunetto Bucciarelli Ducci;                                              &#13;
     udito  il  vice avvocato generale dello Stato Franco Chiarotti, per  &#13;
 il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con cinque ordinanze emesse dai Pretori di Reggio Emilia, di  &#13;
 Correggio e di Chieri, iscritte rispettivamente ai nn. 202,  203,  263,  &#13;
 291  e  406  del  registro  ordinanze  del 1979, è stata sollevata, in  &#13;
 riferimento all'art. 3, primo comma, Cost.,  questione  incidentale  di  &#13;
 legittimità  costituzionale  dell'art. 2, lett. a) del d.P.R. 4 agosto  &#13;
 1978, n. 413, nella parte in cui esclude  l'applicazione  dell'amnistia  &#13;
 ai reati di lesioni colpose gravi e gravissime, commessi con violazione  &#13;
 delle  norme per la prevenzione degli infortuni sul lavoro, che abbiano  &#13;
 determinato le conseguenze previste dal primo comma n. 2 (indebolimento  &#13;
 permanente di un senso o di un organo) o dal  secondo  comma  dell'art.  &#13;
 583 cod. pen. (lesioni personali gravissime).                            &#13;
     Ad   avviso  dei  giudici  a  quibus  non  si  giustificherebbe  il  &#13;
 trattamento differenziato disposto dalla norma impugnata  in  raffronto  &#13;
 alle  ipotesi  perfettamente corrispondenti di reato di lesioni colpose  &#13;
 commesse con violazione delle norme sulla disciplina stradale,  per  le  &#13;
 quali è invece concessa amnistia.                                       &#13;
     Dall'identità del bene giuridico protetto (incolumità personale),  &#13;
 dalla  pena  edittale,  e  dalla  omogeneità  di disciplina giuridica,  &#13;
 avrebbe dovuto conseguire, secondo  le  ordinanze  di  rimessione,  una  &#13;
 parità  di trattamento, onde la denunciata violazione del principio di  &#13;
 eguaglianza.                                                             &#13;
     Dalla censurata esclusione del beneficio, il solo Pretore di Reggio  &#13;
 Emilia  deduce  altresì   una   corrispondente   carenza   di   tutela  &#13;
 processuale,  con  violazione,  altresì,  dell'art. 24, secondo comma,  &#13;
 della Costituzione.                                                      &#13;
     Nelle ordinanze di rimessione si riconosce che la scelta dei  reati  &#13;
 da ricomprendere tra quelli amnistiabili rientra nella discrezionalità  &#13;
 legislativa,  fatto  salvo però il limite della ragionevolezza, il cui  &#13;
 controllo è rimesso alla Corte costituzionale.                          &#13;
     2. - È intervenuto in tutti i giudizi il Presidente del  Consiglio  &#13;
 dei  ministri,  rappresentato  e  difeso dall'Avvocatura generale dello  &#13;
 Stato, con unico atto di  deduzioni,  depositato  il  30  aprile  1979,  &#13;
 chiedendo dichiararsi l'infondatezza della questione sollevata.          &#13;
     Osserva   la   difesa  dello  Stato  che  i  criteri  della  eguale  &#13;
 obiettività giuridica del reato e della identità della pena  edittale  &#13;
 non sarebbero decisivi per dimostrare l'irrazionalità della diversità  &#13;
 di disciplina denunziata. Invero nei delitti colposi le modalità della  &#13;
 condotta  possono  esser  diverse  quanto  a  disvalore giuridico ed il  &#13;
 legislatore può aver ragionevolmente ritenuto che la violazione  delle  &#13;
 norme  sulla  prevenzione  degli  infortuni  sul  lavoro  meritasse  un  &#13;
 trattamento più severo di quello riservato all'infrazione delle regole  &#13;
 di circolazione stradale,  anche  se  produttiva  della  lesione  degli  &#13;
 stessi beni tutelati.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Le ordinanze di rimessione descritte in narrativa propongono  &#13;
 sostanzialmente la medesima questione, sicché i relativi giudizi vanno  &#13;
 definiti con unica sentenza.                                             &#13;
     2. - La Corte costituzionale è chiamata a decidere se contrasti, o  &#13;
 meno, con l'art. 3 della Costituzione, l'art. 2 lett. a) del  d.P.R.  4  &#13;
 agosto  1978,  n.  413,  nella  parte  in  cui  esclude  l'applicazione  &#13;
 dell'amnistia ai reati di lesioni colpose gravi e  gravissime  commessi  &#13;
 con  violazione  delle  norme  per  la  prevenzione degli infortuni sul  &#13;
 lavoro (che abbiano  determinato  le  conseguenze  previste  dal  primo  &#13;
 comma,  n.  2,  o dal secondo comma, dell'art. 583 c.p.), per il dubbio  &#13;
 che  ciò realizzi un'ingiustificata disparità di trattamento rispetto  &#13;
 allo stesso reato di lesioni  colpose  compiuto  con  violazione  delle  &#13;
 norme  sulla  circolazione  stradale,  rientrante,  invece, nell'ambito  &#13;
 dell'amnistia.                                                           &#13;
     La norma impugnata è denunciata altresì -  dal  solo  pretore  di  &#13;
 Reggio  Emilia  - per l'ipotesi che contrasti anche con la garanzia del  &#13;
 diritto di difesa (art. 24, secondo comma, Cost.), senza una  specifica  &#13;
 formulazione dei profili di illegittimità da cui sarebbe affetta.       &#13;
     3. - La questione non è fondata.                                    &#13;
     Va  ricordato  che  secondo  la giurisprudenza della Corte "compete  &#13;
 esclusivamente al legislatore la scelta del criterio di discriminazione  &#13;
 tra reati amnistiabili e non, e che le relative valutazioni non possono  &#13;
 essere sindacate, salvo che ricorrano  casi  in  cui  la  sperequazione  &#13;
 normativa  tra  figure  omogenee di reati assuma dimensioni tali da non  &#13;
 potersi considerare sorretta da alcuna ragionevole giustificazione" (da  &#13;
 ultimo sentenza 214/1975).                                               &#13;
     Nella  specie  risulta  dai  lavori  preparatori  della  legge   di  &#13;
 delegazione  che  il  legislatore  ha  voluto consapevolmente escludere  &#13;
 dall'ambito  dell'amnistia  i   reati   sopra   descritti.   Ciò   non  &#13;
 rappresenta,  ad avviso della Corte, una scelta irrazionale, sol che si  &#13;
 consideri che la rilevante diffusione di certi reati in un  determinato  &#13;
 momento  ed  il  conseguente allarme sociale causato dai medesimi, può  &#13;
 costituire ragionevole motivo di discriminazione ai fini  dell'amnistia  &#13;
 (cfr.  anche sentenza n. 175 del 1971). Né va ignorato che la condotta  &#13;
 del datore di lavoro, il quale non abbia  osservato  le  norme  per  la  &#13;
 prevenzione  degli  infortuni  sul  lavoro,  può  essere  determinata,  &#13;
 secondo una ragionevole presunzione, da motivi di lucro,  a  differenza  &#13;
 di  quella  del  soggetto  che  abbia  contravvenuto  alle  norme sulla  &#13;
 circolazione stradale, onde la possibilità che il legislatore disponga  &#13;
 con maggior rigore nel primo dei casi considerati.                       &#13;
     Potrebbe infine  osservarsi  che  dalla  affermazione  della  Corte  &#13;
 secondo  cui "la diversità del bene giuridico tutelato consente sempre  &#13;
 una diversa valutazione politico - sociale ed un diverso trattamento ai  &#13;
 fini  della  amnistia"  non  può  dedursi  automaticamente,  dato   il  &#13;
 carattere  non  esaustivo  del principio invocato dai giudici a quibus,  &#13;
 che  a  parità  dei  beni  giuridici  protetti,  dovrebbe   conseguire  &#13;
 necessariamente, in ogni caso, una pari disciplina.                      &#13;
     4.  - Del tutto priva di motivazione è la censura mossa alla norma  &#13;
 impugnata per asserita violazione dell'art. 24, secondo  comma,  Cost.,  &#13;
 attesa   l'assenza   di   una  qualsiasi  formulazione  di  profili  di  &#13;
 illegittimità prospettati.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1)   dichiara   inammissibile   la   questione   di    legittimità  &#13;
 costituzionale  dell'art.  2,  lett.  a)  d.P.R. 4 agosto 1978, n. 413,  &#13;
 sollevata, in  riferimento  all'art.  24  della  Costituzione,  con  le  &#13;
 ordinanze del pretore di Reggio Emilia;                                  &#13;
     2) dichiara non fondata la questione di legittimità costituzionale  &#13;
 della stessa norma, indicata sub 1), sollevata, in riferimento all'art.  &#13;
 3 della Costituzione, con le ordinanze in epigrafe descritte.            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 16 aprile 1980.                               &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE  STEFANO   -   LEOPOLDO   ELIA   -  &#13;
                                   GUGLIELMO  ROEHRSSEN - ORONZO REALE -  &#13;
                                   BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
