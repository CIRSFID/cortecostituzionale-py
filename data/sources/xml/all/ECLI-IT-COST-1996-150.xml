<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>150</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:150</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/05/1996</data_decisione>
    <data_deposito>08/05/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI: &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, &#13;
 prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, &#13;
 prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 75  del  d.P.R.    &#13;
 9  ottobre  1990,  n.  309  (Testo  unico  delle  leggi in materia di    &#13;
 disciplina degli stupefacenti  e  sostanze  psicotrope,  prevenzione,    &#13;
 cura  e riabilitazione dei relativi stati di tossicodipendenza), come    &#13;
 modificato a seguito del d.P.R. 5 giugno 1993, n. 171,  promosso  con    &#13;
 ordinanza  emessa  l'11  ottobre  1995  dal  tribunale di Bologna nel    &#13;
 procedimento penale a carico di Manicone Maria Rosaria,  iscritta  al    &#13;
 n.  870  del  registro  ordinanze  1995  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n.   52, prima serie  speciale,  dell'anno    &#13;
 1995;                                                                    &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di  consiglio  del  26  marzo  1996  il  giudice    &#13;
 relatore Renato Granata;                                                 &#13;
   Ritenuto  che,  nel  corso del procedimento penale nei confronti di    &#13;
 Manicone Maria Rosaria per il delitto  di  cui  all'art.  73,  quinto    &#13;
 comma, del d.P.R. 9 ottobre 1990, n. 309, perché senza la prescritta    &#13;
 autorizzazione  aveva  coltivato  alcune piante di canapa indiana, il    &#13;
 tribunale di Bologna ha sollevato d'ufficio  (con  ordinanza  dell'11    &#13;
 ottobre  1995)  questione  incidentale di legittimità costituzionale    &#13;
 dell'art. 75 del cit. d.P.R. n.309 del 1990 (Testo unico delle  leggi    &#13;
 in  materia  di  disciplina degli stupefacenti e sostanze psicotrope,    &#13;
 prevenzione,  cura   e   riabilitazione   dei   relativi   stati   di    &#13;
 tossicodipendenza),  come  modificato a seguito del d.P.R. n. 171 del    &#13;
 1993, nella parte in cui non prevede che  anche  la  coltivazione  di    &#13;
 (piante  da  cui  si  estraggono)  sostanze  stupefacenti - oltre che    &#13;
 l'importazione, l'acquisto o la detenzione -  venga  punita  soltanto    &#13;
 con  sanzioni  amministrative, se finalizzata all'uso personale della    &#13;
 sostanza;                                                                &#13;
     che secondo il tribunale rimettente vi sarebbe la violazione  del    &#13;
 principio di eguaglianza sotto il profilo che per condotte ugualmente    &#13;
 caratterizzate  dalla  destinazione  della sostanza all'uso personale    &#13;
 (coltivazione da  un  lato  e  acquisto,  importazione  e  detenzione    &#13;
 dall'altro)    sarebbe    previsto   un   trattamento   sanzionatorio    &#13;
 diversificato;                                                           &#13;
     che la finalità dell'uso personale costituisce unico  discrimine    &#13;
 tra  l'illecito penale e quello amministrativo, indipendente dal tipo    &#13;
 di condotta e dalla natura e quantità della sostanza stupefacente;      &#13;
     che, quindi, il rilievo depenalizzante assunto dall'uso personale    &#13;
 della droga nella nuova disciplina,  indipendentemente  da  parametri    &#13;
 quantitativi  non  più  esistenti,  comporta  -  secondo  il giudice    &#13;
 rimettente  -  che  debba  equipararsi  la  coltivazione  alle  altre    &#13;
 condotte  previste  dall'art.  75  ai fini degli effetti sanzionatori    &#13;
 indicati nella medesima norma, sicché la  perdurante  incriminazione    &#13;
 della   coltivazione  costituisce  oggi  una  non  più  giustificata    &#13;
 diversità di trattamento sanzionatorio;                                 &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo  che  la  questione sia dichiarata manifestamente infondata    &#13;
 perché già decisa da questa Corte con sentenza n. 360 del 1995;        &#13;
   Considerato che questa Corte, con sentenza n. 360 del 1995, ha già    &#13;
 ritenuto non fondata tale questione ritenendo in particolare  la  non    &#13;
 comparabilità della condotta delittuosa, prevista dalla disposizione    &#13;
 censurata, con alcuna di quelle allegate come tertium comparationis e    &#13;
 quindi  escludendo  che  possa ravvisarsi la denunciata disparità di    &#13;
 trattamento;                                                             &#13;
     che il giudice rimettente  non  introduce  argomenti  nuovi,  né    &#13;
 prospetta profili diversi di censura;                                    &#13;
     che quindi la questione è manifestamente infondata;                 &#13;
   Visti  gli artt. 26, secondo comma, della legge 11 marzo 1953, n.87    &#13;
 e 9, secondo comma, delle norme integrative  per  i  giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 75 del d.P.R. 9 ottobre 1990, n. 309  (Testo    &#13;
 unico  delle  leggi  in  materia  di  disciplina degli stupefacenti e    &#13;
 sostanze psicotrope, prevenzione, cura e riabilitazione dei  relativi    &#13;
 stati  di  tossicodipendenza), come modificato a seguito del d.P.R. 5    &#13;
 giugno  1993  n.171,  sollevata,  in  riferimento  all'art.  3  della    &#13;
 Costituzione,  dal  tribunale  di Bologna con l'ordinanza indicata in    &#13;
 epigrafe.                                                                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 2 maggio 1996.                                &#13;
                         Il Presidente: Ferri                             &#13;
                         Il redattore: Granata                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria l'8 maggio 1996.                             &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
