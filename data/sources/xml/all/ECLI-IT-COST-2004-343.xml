<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>343</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:343</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ONIDA</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia>Annibale Marini</redattore_pronuncia>
    <data_decisione>28/10/2004</data_decisione>
    <data_deposito>12/11/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 6 della legge 23 dicembre 1994, n. 724 (Misure di razionalizzazione della finanza pubblica), promosso con ordinanza del 14 ottobre 2002 dal Tribunale di Bologna nel procedimento civile vertente tra la Regione Emilia-Romagna e Lazzarini Giuliano ed altra, iscritta al n. 565 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 2, prima serie speciale, dell'anno 2003. &#13;
    Visti l'atto di costituzione della Regione Emilia-Romagna nonché l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nell'udienza pubblica del 12 ottobre 2004 il Giudice relatore Annibale Marini; &#13;
    uditi l'avvocato Domenico Fazio per la Regione Emilia-Romagna e l'avvocato dello Stato Franco Favara per il Presidente del Consiglio dei ministri. &#13;
    Ritenuto che il Tribunale di Bologna, con ordinanza del 14 ottobre 2002, nel corso di un giudizio ex art. 615, primo comma, del codice di procedura civile, ha sollevato, in riferimento agli artt. 81 e 119 della Costituzione, questione di legittimità costituzionale dell'art. 6 della legge 23 dicembre 1994, n. 724 (Misure di razionalizzazione della finanza pubblica), «nella parte in cui attribuisce alle Regioni la responsabilità patrimoniale di tutti i debiti delle unità sanitarie locali e non delle sole obbligazioni nascenti da contratti di fornitura di beni e di servizi e, quindi, connesse con il normale svolgimento dell'attività istituzionale»; &#13;
    che nel giudizio a quo l'opponente Regione Emilia-Romagna - alla quale  è stato notificato atto di precetto in virtù di ordinanza di condanna ai sensi dell'art. 186-quater cod. proc. civ., emanata dal Tribunale di Ravenna, nei confronti della unità sanitaria locale n. 36 di Lugo, in un giudizio per il risarcimento di danni causati da emotrasfusione effettuata nell'anno 1985 - eccepisce la propria estraneità al rapporto dedotto in giudizio, sostenendo spettare alle gestioni liquidatorie delle unità sanitarie locali l'esclusiva legittimazione sostanziale e processuale rispetto all'esecuzione dei provvedimenti resi nei confronti delle soppresse unità sanitarie locali; &#13;
    che la medesima opponente assume dunque - per quanto si legge nell'ordinanza di rimessione - che l'art. 6 della legge n. 724 del 1994 debba essere interpretato «nel senso di ritenere le Regioni esclusivamente tenute ad istituire le gestioni a stralcio (ora gestioni liquidatorie) ed a mettere a disposizione i fondi necessari ad estinguere i debiti gravanti sulle USL limitatamente alle obbligazioni nascenti da contratti di fornitura di beni e servizi e non a quelle estranee alla categoria delle spese di gestione in relazione alle quali non è stata prevista dalla successiva normativa statale e regionale (…) alcuna copertura finanziaria», essendo tale interpretazione l'unica a suo avviso compatibile con l'obbligo costituzionale di finanziamento delle leggi che comportano nuove spese e con il principio di autonomia finanziaria delle Regioni; &#13;
    che, in evidente subordine, l'opponente «ha, altresì, richiesto in via pregiudiziale la rimessione degli atti alla Corte costituzionale affinché venga accertata e dichiarata l'illegittimità, per contrasto con gli artt. 81 e 119 Cost., dell'art. 6 della legge 23 dicembre 1994, n. 724, nella parte in cui attribuisce alle Regioni la responsabilità patrimoniale di tutti i debiti delle unità sanitarie locali, ove si acceda alla tesi fatta propria dalla sentenza della Corte di cassazione a sezioni unite 6 marzo 1997, n. 1989, secondo la quale l'art. 6 della legge 23 dicembre 1994, n. 724, sarebbe applicabile a tutte le obbligazioni delle unità sanitarie locali»; &#13;
    che, premessa dunque l'esposizione, nei termini che precedono, delle difese di parte opponente, il giudice a quo solleva la enunciata questione di legittimità costituzionale, limitandosi ad affermarne l'evidente rilevanza e non manifesta infondatezza; &#13;
    che si è costituita in giudizio la Regione Emilia-Romagna, concludendo per la declaratoria di illegittimità costituzionale della norma impugnata, nella parte in cui prevede che «in nessun caso è consentito alle Regioni di far gravare sulle aziende di cui al decreto legislativo 30 dicembre 1992, n. 502, e successive modificazioni ed integrazioni, né direttamente né indirettamente, i debiti e i crediti facenti capo alle gestioni pregresse delle unità sanitarie locali»; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'inammissibilità o l'infondatezza della questione. &#13;
    Considerato che l'ordinanza di rimessione è del tutto priva di motivazione in ordine alla rilevanza della questione avendo il rimettente omesso qualsiasi valutazione della tesi interpretativa prospettata dalla Regione in via principale sul suo difetto di legittimazione passiva nel giudizio a quo; &#13;
    che manca, altresì, nell'ordinanza di rimessione una sia pur sintetica motivazione sulla non manifesta infondatezza della questione; &#13;
            che la questione va, pertanto, dichiarata, in relazione ad entrambi tali profili, manifestamente inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 6 della legge 23 dicembre 1994, n. 724 (Misure di razionalizzazione della finanza pubblica), sollevata, in riferimento agli artt. 81 e 119 della Costituzione, dal Tribunale di Bologna con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 28 ottobre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Annibale MARINI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 12 novembre 2004. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
