<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>184</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:184</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/04/1992</data_decisione>
    <data_deposito>16/04/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: dott. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  sull'ammissibilità  del conflitto di attribuzione fra    &#13;
 poteri  dello  Stato  sollevato   dal   Consiglio   superiore   della    &#13;
 magistratura nei confronti del Ministro di grazia e giustizia nonché    &#13;
 nei  confronti del Presidente del Consiglio dei ministri, con ricorso    &#13;
 depositato in Cancelleria il 20 marzo 1992, ed iscritto al n. 41  del    &#13;
 registro ammissibilità conflitti;                                       &#13;
    Udito  nella  camera  di  consiglio  del 15 aprile 1992 il Giudice    &#13;
 relatore Aldo Corasaniti;                                                &#13;
    Ritenuto  che,  con  ricorso  depositato  il  20  marzo  1992,  il    &#13;
 Consiglio   superiore   della   magistratura,  in  persona  del  Vice    &#13;
 Presidente, a ciò delegato dal Presidente, ha sollevato conflitto di    &#13;
 attribuzione nei confronti del Ministro di grazia e giustizia nonché    &#13;
 del Presidente del Consiglio dei ministri, in  relazione  al  rifiuto    &#13;
 opposto dal Ministro di dare corso, mediante la proposta del relativo    &#13;
 decreto del Presidente della Repubblica, alla deliberazione di nomina    &#13;
 del  Presidente  della  Corte d'appello di Palermo, nella persona del    &#13;
 dott. Pasquale Giardina, deliberazione  adottata  da  esso  Consiglio    &#13;
 nella seduta dell'11 dicembre 1991;                                      &#13;
      che il Consiglio ricorrente ha lamentato la lesione, per effetto    &#13;
 del  rifiuto  opposto  dal  Ministro, delle attribuzioni garantite ad    &#13;
 esso Consiglio da norme costituzionali in tema di provvedimenti sullo    &#13;
 stato dei magistrati e  in  particolare  di  conferimento  di  uffici    &#13;
 direttivi, e ha chiesto che sia dichiarato che non spetta al Ministro    &#13;
 il  potere di non dare corso alla suindicata deliberazione; o, in via    &#13;
 subordinata, che non  spetta  al  Ministro  il  potere  di  impedire,    &#13;
 "negando  il  proprio  positivo concerto alla proposta di nomina", la    &#13;
 deliberazione stessa;                                                    &#13;
    Considerato che ricorrono i requisiti di  cui  all'art.  37  della    &#13;
 legge  11  marzo  1953,  n.  87, ai fini della configurabilità di un    &#13;
 conflitto di attribuzioni fra poteri dello Stato la  cui  risoluzione    &#13;
 spetti a questa Corte;                                                   &#13;
      che,  infatti, per un verso ciascuno degli organi fra i quali si    &#13;
 assume essere insorto il conflitto è abilitato ad esercitare,  nella    &#13;
 materia,  attribuzioni  proprie  ad esso conferite dalla Costituzione    &#13;
 (artt. 105, 110, 95 Cost.);                                              &#13;
      che, per altro verso, è lamentata in concreto la lesione di una    &#13;
 data  attribuzione  costituzionalmente  garantita,  qual  è   quella    &#13;
 conferita  al  Consiglio  superiore della magistratura in ordine allo    &#13;
 status dei magistrati (art. 105 Cost.);                                  &#13;
      che, pertanto, va dichiarato  ammissibile  il  ricorso,  mentre,    &#13;
 atteso il carattere di mera delibazione, senza contraddittorio, della    &#13;
 presente   pronuncia,   resta  impregiudicata,  secondo  la  costante    &#13;
 giurisprudenza di questa Corte, ogni  decisione  anche  in  punto  di    &#13;
 ammissibilità.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  ammissibile,  ai  sensi dell'art. 37 della legge 11 marzo    &#13;
 1953, n. 87, il ricorso per conflitto di  attribuzione  proposto  dal    &#13;
 Consiglio  superiore della magistratura nei confronti del Ministro di    &#13;
 grazia e giustizia nonché del Presidente del Consiglio dei ministri;    &#13;
    Dispone:  a)  che  la  cancelleria  della  Corte   dia   immediata    &#13;
 comunicazione  al ricorrente della presente ordinanza; b) che, a cura    &#13;
 del ricorrente, il ricorso e la presente ordinanza  siano  notificati    &#13;
 agli  organi  indicati  nel ricorso stesso entro il termine di giorni    &#13;
 trenta dalla comunicazione.                                              &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 aprile 1992.                               &#13;
                 Il Presidente: CORASANITI                                &#13;
                 Il redattore: CORASANITI                                 &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 16 aprile 1992.                          &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
