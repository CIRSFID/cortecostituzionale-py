<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>298</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:298</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/02/1988</data_decisione>
    <data_deposito>10/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 76 del d.P.R.    &#13;
 29 settembre 1973, n. 597 (Istituzione e disciplina dell'imposta  del    &#13;
 reddito  delle  persone fisiche), promosso con ordinanza emessa il 25    &#13;
 marzo 1986 dalla Commissione tributaria di primo grado di Bassano del    &#13;
 Grappa  iscritta  al  n. 349 del registro ordinanze 1987 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 34, prima serie speciale    &#13;
 dell'anno 1987;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio del    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 febbraio 1988 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che  nel  corso  di un procedimento iniziato dalla ditta    &#13;
 Poli Giovanni Battista avverso l'accertamento effettuato dall'Ufficio    &#13;
 imposte  dirette  di  Bassano del Grappa la Commissione tributaria di    &#13;
 primo grado della stessa città, sollevava, in riferimento agli artt.    &#13;
 76,  77,  3  e  53  Cost.,  questione  di legittimità costituzionale    &#13;
 dell'art. 76 d.P.R. 29 settembre 1973 n. 597:  precisamente  di  tale    &#13;
 disposizione   veniva  impugnato  il  primo  comma,  secondo  cui  le    &#13;
 plusvalenze  conseguite  mediante  operazioni  speculative   (e   non    &#13;
 rientranti  nei  redditi  di impresa) concorrono a formare il reddito    &#13;
 complessivo tassabile; e veniva altresì impugnato  il  terzo  comma,    &#13;
 secondo cui la lottizzazione o l'esecuzione di opere intese a rendere    &#13;
 edificabili terreni e la successiva vendita si  considerano  in  ogni    &#13;
 caso aventi fini speculativi;                                            &#13;
      che  secondo  il  giudice  a quo tali disposizioni violavano gli    &#13;
 artt. 76 e 77  nonché  3  e  53  Cost.,  in  quanto  il  legislatore    &#13;
 ordinario,  incorrendo  in  eccesso  di  delega,  aveva  posto  delle    &#13;
 presunzioni assolute, che invero violavano  altresì  i  principi  di    &#13;
 eguaglianza e di capacità contributiva;                                 &#13;
      che  si  costituiva la Presidenza del Consiglio dei ministri, la    &#13;
 quale eccepiva  l'inammissibilità  ed  in  subordine  l'infondatezza    &#13;
 della questione;                                                         &#13;
    Considerato  che  la  questione  relativa  all'art.  76  Cost.  è    &#13;
 palesemente infondata, poiché il  potere  conferito  al  legislatore    &#13;
 delegato dalla l. 9 ottobre 1971, n. 825 comprende la possibilità di    &#13;
 regolare l'intera materia delle imposte  sul  reddito  secondo  ampia    &#13;
 discrezionalità  (  v. ordd. n. 321 e 334 del 1987) (mentre fuori di    &#13;
 proposito è stato invocato nell'ordinanza di rimessione anche l'art.    &#13;
 77 Cost.);                                                               &#13;
      che  la  qualificazione  del  fine  speculativo delle operazioni    &#13;
 anzidette  trova  la  sua  non  irragionevole  giustificazione  nella    &#13;
 realtà  socio-economica  secondo  la  finalità ordinaria degli atti    &#13;
 (acquisto   per   rivendere,   lottizzazione,    ecc.)    presi    in    &#13;
 considerazione, sicché la qualificazione stessa non merita censura;     &#13;
      che  anche  la  determinazione  normativa,  che fissa il sorgere    &#13;
 dell'obbligazione tributaria alla data in cui gli atti suddetti  sono    &#13;
 posti  in  essere,  non  è  affatto irrazionale, in quanto spetta al    &#13;
 legislatore determinare il  momento  in  cui  si  realizza  il  fatto    &#13;
 genetico, indipendentemente dal concreto prodursi della ricchezza;       &#13;
      che   in   conclusione   la  questione  deve  essere  dichiarata    &#13;
 manifestamente  infondata  in  relazione  a  tutti   i   profili   di    &#13;
 incostituzionalità denunciati;                                          &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953  n.  87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  76  d.P.R.  29  settembre  1973  n.   597,    &#13;
 sollevata,  in  riferimento  agli  artt.  76, 77, 3 e 53 Cost., dalla    &#13;
 Commissione tributaria di primo  grado  di  Bassano  del  Grappa  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 25 febbraio 1988.                             &#13;
                    Il Presidente e redattore: SAJA                       &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 10 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
