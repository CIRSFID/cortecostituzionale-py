<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-03-30/56/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-03-30/56"/>
          <FRBRalias value="ECLI:IT:COST:2023:56" name="ECLI"/>
          <FRBRdate date="2023-03-30" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="56"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-03-30/56/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-03-30/56/ita@"/>
          <FRBRdate date="2023-03-30" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-03-30/56/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-03-30/56/ita@.xml"/>
          <FRBRdate date="2023-03-30" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2023-03-09" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2023-03-30" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="SCIARRA" href="/akn/ontology/person/it/SCIARRA" showAs="SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="BERNARDINI" href="/akn/ontology/person/it/BERNARDINI" showAs="BERNARDINI"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2023</akn4cc:anno>
        <akn4cc:numero>56</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2023:56</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>SCIARRA</akn4cc:presidente>
        <akn4cc:relatore>Francesco Viganò</akn4cc:relatore>
        <akn4cc:data_decisione>2023-03-09</akn4cc:data_decisione>
        <akn4cc:data_deposito>2023-03-30</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>; Giudici : <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 414, primo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, promosso dal Tribunale ordinario di Udine nel procedimento penale a carico di T. D. e C. C., con ordinanza del 3 febbraio 2022, iscritta al n. 82 del registro ordinanze 2022 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 33, prima serie speciale, dell'anno 2022.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio dell'8 marzo 2023 il Giudice relatore Francesco Viganò;
 deliberato nella camera di consiglio del 9 marzo 2023.
 Ritenuto che, con ordinanza del 3 febbraio 2022, il Tribunale ordinario di Udine ha sollevato, in riferimento agli artt. 3, 21, primo comma, 27, terzo comma, e 117, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, quest'ultimo in relazione agli artt. 10 e 17 della Convenzione europea dei diritti dell'uomo, questioni di legittimità costituzionale dell'art. 414, primo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, nella parte in cui prevede, per il delitto di istigazione a delinquere, la pena minima di un anno di reclusione;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che il rimettente è chiamato, nell'ambito di un processo penale celebrato con rito ordinario, a giudicare della responsabilità di T. D. e C. C., entrambi imputati del delitto di istigazione a delinquere di cui all'art. 414, primo comma, numero 1), <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>.;
 che, espone il giudice a quo, al primo imputato è contestato di aver fatto l'apologia, in un discorso pronunciato durante una manifestazione pubblica, di «azioni di sabotaggio» commesse in danno di alcune sedi di una banca e di un partito politico, nonché di avere «istigato alla commissione di ulteriori atti di violenza del genere, quantomeno sulle cose», mentre alla seconda imputata è contestato di avere, nel corso di un'intervista radiofonica, «istigato alla commissione di reati contro l'onore o contro la persona» ai danni di personale medico in servizio presso una casa circondariale;
 che il rimettente ritiene di sollevare le predette questioni di legittimità costituzionale «[a]ll'esito dell'istruttoria dibattimentale» e «prima di affrontare, in sede di deliberazione, la valutazione di merito sulla sussistenza dei singoli fatti, sulla effettiva pericolosità delle condotte, sull'applicabilità dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_131bis">art. 131-bis cod. pen.</ref> e sulla stessa adeguatezza dell'attuale cornice edittale rispetto ai fatti concreti in esame», dovendo egli comunque fare applicazione della disposizione censurata nel giudizio, e «anche perché talune delle valutazioni suddette possono essere influenzate dalla indicazione normativa di un certo minimo di pena non derogabile»;
 che la pena minima edittale di un anno prevista dalla disposizione censurata non sarebbe conforme, ad avviso del giudice a quo, ai principi di ragionevolezza e finalità rieducativa della pena;
 che infatti, sotto il profilo della ragionevolezza, tale pena minima sarebbe prevista «indipendentemente dall'entità del pericolo concreto di realizzazione del delitto a cui l'apologia o l'istigazione si riferisce e in relazione alla apologia o istigazione riferite a qualsiasi tipo di delitto», compresi quelli puniti soltanto con la pena pecuniaria o con una pena detentiva nei limiti minimi di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_23">art. 23 cod. pen.</ref>, ovvero con una delle sanzioni previste per i reati perseguibili davanti al giudice di pace;
 che ciò lederebbe, altresì, il principio di proporzione tra gravità del reato e severità della pena, che sarebbe a sua volta presupposto indispensabile per garantire la finalità rieducativa della pena medesima;
 che l'evidenziato difetto di ragionevolezza e proporzionalità risulterebbe acuito dall'astratta applicabilità al delitto in esame dell'istituto della non punibilità per particolare tenuità del fatto di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_131bis">art. 131-bis cod. pen.</ref>, dal momento che, allorché non vi fossero le condizioni per accedere a tale istituto, il giudice dovrebbe, «senza alcuna gradualità e possibilità di adeguamento», necessariamente infliggere la pena di un anno di reclusione «anche per una situazione appena oltre detta soglia di irrilevanza»;
 che sarebbe ulteriormente ravvisabile una lesione della libertà di manifestazione del pensiero, riconosciuta dall'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_21">art. 21 Cost.</ref> e dalle norme convenzionali menzionate, con conseguente violazione dell'art. 117, primo comma, Cost., dal momento che l'attuale cornice edittale rischia di non permettere un'applicazione proporzionata, e pertanto legittima, della pena, secondo i parametri di cui agli artt. 10 e 17 CEDU, così come interpretati dalla Corte europea dei diritti dell'uomo;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, sostenendo l'inammissibilità e la manifesta infondatezza delle questioni di legittimità costituzionale sollevate;
 che esse sarebbero, anzitutto, inammissibili per difetto di rilevanza, in relazione al loro carattere astratto, eventuale e prematuro, dal momento che il rimettente non avrebbe effettuato alcuna prognosi sulla sussistenza dei fatti contestati, né avrebbe escluso la possibile applicabilità agli imputati della causa di non punibilità di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_131bis">art. 131-bis cod. pen.</ref>, con conseguente difetto di motivazione sulla necessità di applicare effettivamente la pena della cui legittimità costituzionale egli dubita;
 che l'inammissibilità delle questioni discenderebbe, altresì, dal difetto di motivazione circa la loro non manifesta infondatezza in relazione ai singoli parametri evocati;
 che, ancora, il giudice a quo avrebbe omesso di indicare una «grandezza preesistente di pena minima asseritamente corretta che possa essere trasposta "per linee interne" o similari nella disposizione censurata», sicché egli chiederebbe in effetti a questa Corte di sostituirsi al legislatore nel determinare un nuovo minimo edittale, ciò che determinerebbe una ulteriore ragione di inammissibilità delle questioni (è citata la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021/117/!main">sentenza n. 117 del 2021</ref>);
 che, nel merito, le questioni sarebbero comunque manifestamente infondate quanto all'asserita violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_21">art. 21 Cost.</ref>, dal momento che non verrebbe qui in considerazione la conformità a <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> del divieto penalmente sanzionato, bensì unicamente il quantum di pena previsto per la condotta inosservante di tale divieto;
 che manifestamente infondate sarebbero anche le censure di asserita sproporzione della pena formulate in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_27">27 Cost.</ref></mref>, dal momento che la pena minima prevista dall'art. 414, primo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>. non presenterebbe alcun carattere di manifesta arbitrarietà, irragionevolezza o sproporzione, e ricadrebbe pertanto entro gli ampi margini di cui dispone il legislatore nella quantificazione delle pene, tanto più a fronte dell'applicabilità al delitto in questione della causa di non punibilità per particolare tenuità del fatto di cui all'art. 163-bis (recte: 131-bis) <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>.
 Considerato che le questioni di legittimità costituzionale all'esame concernono il trattamento sanzionatorio del delitto di istigazione a delinquere di cui all'art. 414, primo comma, numero 1), <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., e, in particolare, il minimo edittale di un anno di reclusione che, secondo il rimettente, contrasterebbe con gli artt. 3, 21, primo comma, 27, terzo comma, e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 10 e 17 CEDU;
 che, tuttavia il giudice a quo non chiarisce, nell'ordinanza di rimessione, se egli debba fare applicazione nel caso concreto del frammento di disposizione censurato, concernente per l'appunto il trattamento sanzionatorio di chi abbia posto in essere una condotta corrispondente alla figura tipica di reato;
 che, infatti, il rimettente si riserva espressamente di compiere, all'esito dell'incidente di legittimità costituzionale, «la valutazione di merito sulla sussistenza dei singoli fatti, sulla effettiva pericolosità delle condotte», nonché sull'applicabilità della causa di non punibilità di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_131bis">art. 131-bis cod. pen.</ref> in relazione alla ipotetica particolare tenuità del fatto;
 che è pertanto evidente che, qualora le prime due verifiche avessero esito negativo, il rimettente dovrebbe già escludere la responsabilità degli imputati per il reato loro ascritto, ciò che renderebbe superflua nel caso concreto la stessa pronuncia di questa Corte sulla cornice edittale prevista dal legislatore (sulla necessità di una interpretazione costituzionalmente orientata, ispirata alla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1970/65/!main">sentenza n. 65 del 1970</ref> di questa Corte, che circoscriva la figura criminosa in esame alle sole manifestazioni di pensiero che per le loro modalità integrino un «comportamento concretamente idoneo a provocare la commissione di delitti», requisito che spetta al giudice penale accertare nel singolo caso, si veda Corte di cassazione, sezione quinta penale, sentenza 12 settembre-27 novembre 2019, n. 48247; nello stesso senso, sezione prima penale, sentenze 23 aprile-4 luglio 2012, n. 25833 e 5 giugno-3 luglio 2001, n. 26907);
 che ad analogo risultato si perverrebbe ove il giudice riconoscesse bensì la responsabilità degli imputati, ma qualificasse il fatto dagli stessi commesso come di particolare tenuità e meritevole, dunque, dell'applicazione della causa di non punibilità di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_131bis">art. 131-bis cod. pen.</ref>;
 che le questioni risultano pertanto, come eccepito dall'Avvocatura generale dello Stato, astratte e premature (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/141/!main">sentenze n. 141 del 2022</ref> e n. 114 del 2021, <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/210/!main">ordinanza n. 210 del 2020</ref>; da ultimo, sull'inammissibilità di questioni che difettano di «attualità e concretezza», <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/269/!main">sentenza n. 269 del 2022</ref>), ciò che impedisce a questa Corte di apprezzarne la concreta rilevanza nel caso oggetto del giudizio a quo;
 che, per tale ragione, esse debbono essere ritenute manifestamente inammissibili, restando assorbite le ulteriori eccezioni di inammissibilità formulate dall'Avvocatura generale dello Stato.
 Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 11, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara manifestamente inammissibili le questioni di legittimità costituzionale dell'art. 414, primo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, sollevate, in riferimento agli artt. 3, 21, primo comma, 27, terzo comma, e 117, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, quest'ultimo in relazione agli artt. 10 e 17 della Convenzione europea dei diritti dell'uomo, dal Tribunale ordinario di Udine con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2023-03-09">9 marzo 2023</date>.
 F.to:
 <signature><judge refersTo="#silvanaSciarra" as="#presidente">Silvana SCIARRA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#francescoViganò" as="#relatore">Francesco VIGANÒ</judge>, <role refersTo="#relatore">Redattore</role></signature>
 Igor DI <signature><person refersTo="#BERNARDINI" as="#cancelliere">BERNARDINI</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2023-03-30">30 marzo 2023</date>.
 Il Cancelliere
 F.to: Igor DI BERNARDINI</block>
    </conclusions>
  </judgment>
</akomaNtoso>
