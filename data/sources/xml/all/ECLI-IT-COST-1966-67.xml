<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1966</anno_pronuncia>
    <numero_pronuncia>67</numero_pronuncia>
    <ecli>ECLI:IT:COST:1966:67</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Chiarelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/06/1966</data_decisione>
    <data_deposito>10/06/1966</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - &#13;
 Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - &#13;
 Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE &#13;
 VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO &#13;
 BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di  legittimità  costituzionale dei decreti del  &#13;
 Presidente della Repubblica 9 maggio 1961, n. 803, e 2 gennaio 1962, n.  &#13;
 346,  contenenti  norme  sul  trattamento  dei  lavoratori  dipendenti,  &#13;
 rispettivamente,  da  imprese  della  macinazione e pastificazione e da  &#13;
 ristoranti e imprese similari, promossi con due  ordinanze  emesse  dal  &#13;
 Pretore di Campobasso il 6 maggio 1965 nel procedimento penale a carico  &#13;
 di  Martino Nicola e il 26 aprile 1965 nel procedimento penale a carico  &#13;
 di De Franceschi Maria, iscritte ai nn. 92 e 107 del Registro ordinanze  &#13;
 1965 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 151  del  &#13;
 19 giugno 1965 e n. 178 del 17 luglio 1965.                              &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 Ministri;                                                                &#13;
     udita nell'udienza pubblica del 17 maggio  1966  la  relazione  del  &#13;
 Giudice Giuseppe Chiarelli;                                              &#13;
     uditi  i sostituti avvocati generali dello Stato Michele Savarese e  &#13;
 Francesco Agrò, per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel procedimento penale a carico di Nicola  Martino,  imputato,  ai  &#13;
 sensi  dell'art.  8  della  legge 14 luglio 1959, n.   741, di non aver  &#13;
 adempiuto agli obblighi di cui al contratto collettivo 1  ottobre  1959  &#13;
 per  i  dipendenti da imprese della macinazione e della pastificazione,  &#13;
 il Pretore di Campobasso, con ordinanza 6  maggio  1965,  ha  sollevato  &#13;
 d'ufficio  questione di legittimità costituzionale dell'articolo unico  &#13;
 del D.P.R. 9 maggio 1961, n. 803, che ha recepito in legge le  clausole  &#13;
 del detto contratto collettivo.                                          &#13;
     Rileva  il  Pretore  che  tale  decreto  fu  emanato  in dipendenza  &#13;
 dell'art. 2 della legge 1 ottobre 1960, n. 1027, la quale  prorogò  di  &#13;
 quindici  mesi  il  termine  per  l'esercizio della delega conferita al  &#13;
 Governo con  la  ricordata  legge  n.    741.    Se  non  che,  secondo  &#13;
 l'ordinanza, il detto decreto presidenziale sarebbe stato emanato oltre  &#13;
 i  limiti  temporali  di  esercizio legittimo della delega, in quanto i  &#13;
 quindici mesi di proroga avrebbero avuto  come  termine  di  decorrenza  &#13;
 l'entrata  in vigore della legge n. 741, e sarebbero perciò scaduti il  &#13;
 3 gennaio 1961.  Il decreto stesso avrebbe perciò violato la legge  di  &#13;
 delega.                                                                  &#13;
     L'ordinanza è stata regolarmente notificata e pubblicata.           &#13;
     Si  è  costituito  in  giudizio  il  Presidente  del Consiglio dei  &#13;
 Ministri, rappresentato dall'Avvocatura generale dello Stato, con  atto  &#13;
 di intervento e deduzioni, depositato il 24 giugno 1965.                 &#13;
     Con  tale  atto  si sostiene che l'assunto del Pretore è resistito  &#13;
 sia dalla lettera che dalla ratio della  legge  di  proroga  1  ottobre  &#13;
 1960, e si chiede pertanto che la questione sia dichiarata infondata.    &#13;
     Analoga questione di legittimità costituzionale è stata sollevata  &#13;
 dallo  stesso  Pretore di Campobasso, con ordinanza del 26 aprile 1965,  &#13;
 nel procedimento penale a carico di Maria De  Franceschi,  imputata  di  &#13;
 violazione  delle clausole del contratto collettivo 15 maggio 1959, per  &#13;
 i dipendenti da ristoranti e imprese similari, recepite  in  legge  con  &#13;
 D.P.R. 2 gennaio 1962, n. 346.                                           &#13;
     L'ordinanza è stata regolarmente notificata e pubblicata.           &#13;
     Anche  in  questa  causa si è costituito in giudizio il Presidente  &#13;
 del Consiglio  dei  Ministri,  rappresentato  dall'Avvocatura  generale  &#13;
 dello Stato, con atto di intervento e deduzioni depositato il 12 luglio  &#13;
 1965.  Con esso si chiede che la questione sia dichiarata infondata, in  &#13;
 base  alla  interpretazione  dell'art. 2 della legge di proroga, che si  &#13;
 presenta come la sola consentita dalla lettera della legge stessa.<diritto>Considerato in diritto</diritto>:                          &#13;
     Le cause,  unitamente  trattate,  hanno  per  oggetto  una  analoga  &#13;
 questione  di  legittimità  costituzionale,  e  possono perciò essere  &#13;
 decise con unica sentenza.                                               &#13;
     La legge 14 luglio 1959, n. 741, contenente norme  transitorie  per  &#13;
 garantire  minimi  di  trattamento economico e normativo ai lavoratori,  &#13;
 stabiliva all'art. 6 che  i  decreti  legislativi  previsti  dal  primo  &#13;
 articolo dovevano essere emanati entro un anno dalla data di entrata in  &#13;
 vigore  della  legge  stessa.    La successiva legge 1 ottobre 1960, n.  &#13;
 1027, disponeva all'art. 2 che il predetto termine  era  prorogato  "di  &#13;
 quindici mesi".                                                          &#13;
     Secondo  l'ordinanza,  tale  disposizione  sarebbe da intendere nel  &#13;
 senso che i quindici mesi si dovrebbero computare a partire dalla  data  &#13;
 di decorrenza del termine di un anno, già previsto dalla legge n. 741.  &#13;
     Ma la tesi è infondata.                                             &#13;
     Le  parole adoperate dalla legge di proroga non danno luogo a dubbi  &#13;
 sulla intenzione  del  legislatore,  che  era  di  prorogare  di  altri  &#13;
 quindici  mesi  il predetto termine di un anno, e non già di portare a  &#13;
 quindici mesi il periodo di tempo inizialmente previsto come utile  per  &#13;
 l'esercizio, da parte del Governo, del potere ad esso delegato.          &#13;
     Tale  sicura  interpretazione letterale è confermata dalla ragione  &#13;
 della legge, considerata  in  relazione  alle  circostanze  in  cui  fu  &#13;
 emanata,  risultanti  anche  dai  lavori  preparatori,  e  in relazione  &#13;
 all'esigenza a cui  essa  corrispondeva  di  dar  modo  al  Governo  di  &#13;
 completare,   mediante  l'esercizio  della  delega,  la  sua  opera  di  &#13;
 emanazione di norme conformi ai contratti collettivi,  ai  sensi  della  &#13;
 legge n. 741.                                                            &#13;
     La  questione  è  pertanto  infondata,  e si deve conseguentemente  &#13;
 riconoscere  che  nell'emanazione  dei  decreti  in  esame   è   stato  &#13;
 rispettato il termine previsto dalla legge di proroga.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riunite le cause,                                                    &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 proposta  con  le  ordinanze  in  epigrafe   indicate,   in   relazione  &#13;
 all'articolo  unico  del  D.P.R.  9 maggio 1961, n. 803, e all'articolo  &#13;
 unico  del  D.P.R.  2  gennaio  1962,  n.  346,  contenenti  norme  sul  &#13;
 trattamento  dei  lavoratori  dipendenti,  rispettivamente,  da imprese  &#13;
 della macinazione e pastificazione e da ristoranti e imprese similari.   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 1 giugno 1966.                                &#13;
                                   GASPARE    AMBROSINI    -    GIOVANNI  &#13;
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  &#13;
                                   ANTONIO   MANCA  -  ALDO  SANDULLI  -  &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
