<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>178</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:178</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/06/1975</data_decisione>
    <data_deposito>03/07/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti di legittimità costituzionale dell'art.  18,  &#13;
 secondo comma,  della  legge  20  maggio  1970,  n.  300  (Statuto  dei  &#13;
 lavoratori), promossi con le seguenti ordinanze:                         &#13;
     1)  ordinanza emessa il 9 aprile 1973 dal pretore di Senigallia nel  &#13;
 procedimento civile vertente tra Brocchi Vettese  Antonio  e  la  ditta  &#13;
 Nestplast,  iscritta al n. 260 del registro ordinanze 1973 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 205 dell'8 agosto 1973;     &#13;
     2) ordinanza emessa il 12 gennaio 1974 dal  pretore  di  Desio  nel  &#13;
 procedimento  civile  vertente  tra  Patricelli  Antonietta e l'impresa  &#13;
 fratelli Dubini, iscritta al  n.  90  del  registro  ordinanze  1974  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 107 del 24  &#13;
 aprile 1974;                                                             &#13;
     3) ordinanza emessa il 25  marzo  1974  dal  pretore  di  Roma  nel  &#13;
 procedimento  civile  vertente  tra  Scalcione Alessandro e la società  &#13;
 Alitalia, iscritta al n. 301 del registro ordinanze 1974  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 263 del 9 ottobre 1974.     &#13;
     Visti  gli  atti  di  costituzione  di  Patricelli  Antonietta,  di  &#13;
 Scalcione Alessandro  e  della  società  Alitalia,  nonché  gli  atti  &#13;
 d'intervento del Presidente del Consiglio dei ministri;                  &#13;
     udito  nell'udienza pubblica del 21 maggio 1975 il Giudice relatore  &#13;
 Paolo Rossi;                                                             &#13;
     uditi l'avv. Luciano Ventura, per Patricelli, gli avvocati Spartaco  &#13;
 Spano e Luciano  Ventura,  per  Scalcione,  ed  il  sostituto  avvocato  &#13;
 generale   dello  Stato  Giovanni  Albisinni,  per  il  Presidente  del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Nel corso di un procedimento civile promosso da tale Brocchi  &#13;
 contro la ditta Nestplast, il pretore di Senigallia  ha  sollevato,  in  &#13;
 riferimento  al  principio  costituzionale di eguaglianza, questione di  &#13;
 legittimità costituzionale dell'art. 18, secondo comma, della legge 20  &#13;
 maggio 1970, n. 300 (c.d. statuto dei lavoratori), nella parte  in  cui  &#13;
 fissa  in  cinque  mensilità  di  retribuzione  la  misura  minima del  &#13;
 risarcimento  del  danno  subito  dal  lavoratore   per   effetto   del  &#13;
 licenziamento dichiarato inefficace o invalido.                          &#13;
     Osserva il giudice a quo che mentre l'art. 8 della legge n. 604 del  &#13;
 1966, avente una volta ampia portata, prevedeva, quale alternativa alla  &#13;
 riassunzione  del prestatore di lavoro, "una indennità da un minimo di  &#13;
 cinque ad un massimo di dodici  mensilità  dell'ultima  retribuzione",  &#13;
 configurando   in   sostanza   tale  indennità  come  una  penale  per  &#13;
 l'inadempimento, lo statuto dei lavoratori assicura oggi,  in  aggiunta  &#13;
 alla  reintegrazione  nel posto di lavoro, un'ulteriore risarcimento la  &#13;
 cui misura minima è ingiustificatamente fissata per legge, consentendo  &#13;
 in tal modo l'eventualità di un indebito arricchimento in  favore  del  &#13;
 prestatore  di  lavoro,  con  violazione  del  principio costituzionale  &#13;
 d'eguaglianza tra le varie categorie di cittadini.                       &#13;
     È  intervenuto  in  giudizio  il  Presidente  del  Consiglio   dei  &#13;
 ministri,  rappresentato e difeso dall'Avvocatura generale dello Stato,  &#13;
 con  atto  depositato  il  27  agosto   1973,   chiedendo   dichiararsi  &#13;
 l'infondatezza della questione sollevata.                                &#13;
     Osserva  la  difesa  dello  Stato che il giudice a quo erroneamente  &#13;
 presuppone che il legislatore, nel comminare una sanzione nei confronti  &#13;
 di chi abbia commesso un illecito civile,  debba  necessariamente  fare  &#13;
 ricorso  alla  sanzione  tipica  del risarcimento danni, quale istituto  &#13;
 generale disciplinato dal codice civile, senza potervi apportare  delle  &#13;
 deroghe,  qualora ne ravvisi l'opportunità secondo un apprezzamento di  &#13;
 politica legislativa. Pertanto non è irrazionale che,  innovandosi  al  &#13;
 sistema  previsto dalla legge 15 luglio 1966, n. 604, ed introducendosi  &#13;
 il principio della continuità giuridica del rapporto, limitatamente al  &#13;
 periodo che decorre dalla  data  della  sentenza  di  primo  grado,  lo  &#13;
 statuto  dei  lavoratori  preveda,  per  il  periodo  antecedente  alla  &#13;
 sentenza, il diritto del lavoratore al risarcimento del  danno  in  una  &#13;
 misura minima predeterminata.                                            &#13;
     Tanto  sia  per  sollevare  il  lavoratore da una prova che avrebbe  &#13;
 potuto rendere eccessivamente gravoso il processo, sia specialmente  al  &#13;
 fine  di  indurre il datore di lavoro ad astenersi da atti rivolti alla  &#13;
 risoluzione illegittima del rapporto, in pregiudizio della  parte  più  &#13;
 debole,  il  cui  interesse è stato ritenuto meritevole di particolare  &#13;
 tutela per ragioni di natura sociale.                                    &#13;
     2. - Nel corso di un procedimento promosso da Patricelli Antonietta  &#13;
 contro l'impresa fratelli Dubini, il  pretore  di  Desio  ha  sollevato  &#13;
 questione  incidentale  di  legittimità  costituzionale  dell'art. 18,  &#13;
 secondo comma, del c.d. statuto dei  lavoratori,  nella  parte  in  cui  &#13;
 dispone  che  in  ogni  caso la misura del risarcimento non può essere  &#13;
 inferiore a cinque mensilità  di  retribuzione,  in  riferimento  agli  &#13;
 artt. 3, primo comma, e 24, secondo comma, della Costituzione.           &#13;
     Premette  in  fatto  il  giudice  a  quo  che  l'attrice, assumendo  &#13;
 d'essere  stata  licenziata  in  tronco  il  2  febbraio   1972   senza  &#13;
 provvedimento  scritto,  chiedeva,  in un primo tempo, la dichiarazione  &#13;
 d'inefficacia del licenziamento e la reintegra nel lavoro con  condanna  &#13;
 al  risarcimento  nella  misura  minima  di  legge,  ed eccependosi dai  &#13;
 convenuti  che  con  raccomandata dell'8 aprile 1972, previa revoca del  &#13;
 licenziamento, la lavoratrice era stata invitata a riassumere servizio,  &#13;
 ma non vi aveva provveduto, in un secondo tempo concludeva soltanto per  &#13;
 il risarcimento dei danni  nella  misura  minima,  mentre  i  convenuti  &#13;
 chiedevano d'essere ammessi a provare che l'attrice aveva trovato altro  &#13;
 lavoro già a partire dal febbraio del 1972.                             &#13;
     Pertanto l'ordinanza di rimessione prosegue osservando che la norma  &#13;
 impugnata  altera  in  maniera  rilevante i principi vigenti in tema di  &#13;
 risarcimento danni (artt. 1223, 1226 e 1227  c.c.),  in  considerazione  &#13;
 della condizione sociale del lavoratore che non consentirebbe, ai sensi  &#13;
 dell'art.  3  Cost., alcuna discriminazione. Inoltre contrasterebbe con  &#13;
 l'art. 24, secondo comma, Cost., perché vieta al datore di  lavoro  di  &#13;
 provare  che  il  danno sofferto dal lavoratore sia inferiore al minimo  &#13;
 previsto per legge.                                                      &#13;
     È  intervenuto  in  giudizio  il  Presidente  del  Consiglio   dei  &#13;
 ministri,  rappresentato e difeso dall'Avvocatura generale dello Stato,  &#13;
 con atto del 13 maggio 1974, chiedendo dichiararsi l'infondatezza della  &#13;
 questione sollevata.                                                     &#13;
     La difesa dello Stato si riporta, per quanto attiene  al  principio  &#13;
 d'eguaglianza,   alle   argomentazioni   già   svolte  in  riferimento  &#13;
 all'ordinanza del pretore di Senigallia, ed osserva, relativamente alla  &#13;
 pretesa violazione dell'art. 24, secondo comma, Cost., che, secondo  la  &#13;
 costante  giurisprudenza della Corte costituzionale (da ultimo sentenza  &#13;
 n. 136 del 1972), la garanzia del diritto di difesa  opera  all'interno  &#13;
 del rapporto processuale, sicché è del tutto inconferente il richiamo  &#13;
 a  tale  principio  costituzionale  a proposito di una norma di diritto  &#13;
 sostanziale che determina in misura  fissa  e  forfettaria  l'ammontare  &#13;
 minimo del danno.                                                        &#13;
     Si   è   costituita   in   giudizio   la   Patricelli  Antonietta,  &#13;
 rappresentata e difesa dall'avv. Luciano Ventura, con  atto  depositato  &#13;
 il 9 maggio 1974, chiedendo dichiararsi l'infondatezza della questione.  &#13;
     La parte privata rileva che rientra nella discrezionalità politica  &#13;
 del  legislatore  di  fissare  in  una  misura minima predeterminata il  &#13;
 risarcimento  del   danno   spettante   al   lavoratore   illecitamente  &#13;
 licenziato, ove si osservi che l'intimazione di un licenziamento contra  &#13;
 legem  costituisce  un  gravissimo inadempimento da parte del datore di  &#13;
 lavoro, perché colpisce alla  radice  il  fondamentale  interesse  del  &#13;
 lavoratore  al  mantenimento  di  uno status da cui derivano stabilità  &#13;
 economica e sicurezza sociale.  Al  suddetto  risarcimento  deve  esser  &#13;
 riconosciuta anche una funzione di penale, in relazione alla pluralità  &#13;
 di  interessi  connessi  all'osservanza  delle norme vigenti in tema di  &#13;
 stabilità d'impiego dei lavoratori.                                     &#13;
     3. - Nel corso di altro procedimento civile vertente tra  Scalcione  &#13;
 Alessandro  e la società Alitalia, il pretore di Roma ha sollevato, in  &#13;
 riferimento  agli  artt.  3  e  24  della  Costituzione,  questione  di  &#13;
 legittimità costituzionale dell'art. 18, secondo comma, della legge 20  &#13;
 maggio  1970,  n.  300,  nella  parte in cui riconosce al lavoratore il  &#13;
 diritto al  risarcimento  danni  "in  misura  non  inferiore  a  cinque  &#13;
 mensilità  di  retribuzione,  determinata  secondo  i  criteri  di cui  &#13;
 all'art.  2121  c.c.",  ove  sia  stata   accertata   l'inefficacia   o  &#13;
 l'invalidità  del licenziamento. Osserva il giudice a quo che la norma  &#13;
 impugnata introduce un'indubbia deroga ai principi generali vigenti  in  &#13;
 tema   di   obbligazione   risarcitoria   nascente   da   inadempimento  &#13;
 contrattuale,  sia  sul  piano  sostanziale,  per  ciò   che   attiene  &#13;
 all'esistenza  e  quantificazione del danno, sia sul piano processuale,  &#13;
 per  quanto  concerne  l'onere  della  prova  altrimenti  gravante  sul  &#13;
 creditore.  Invero  l'art.  18 dello statuto dei lavoratori sancisce il  &#13;
 principio  dell'automatismo  del  danno  risarcibile  in   misura   non  &#13;
 inferiore a cinque mensilità di retribuzione.                           &#13;
     Il suddetto trattamento di favore, operante sul piano sostanziale e  &#13;
 probatorio,  può  determinare  un  indebito arricchimento a favore del  &#13;
 lavoratore, sicché deve ritenersi violare il principio  costituzionale  &#13;
 d'eguaglianza  ed  il diritto di difesa, garantiti dagli artt. 3, primo  &#13;
 comma, e 24, secondo comma, della Costituzione.                          &#13;
     La norma impugnata sembra altresì ingiustificata ove si  consideri  &#13;
 che,  ai  sensi  dell'art.  8  della  legge  15 luglio 1966, n. 604, il  &#13;
 risarcimento del danno in misura minima forfettizzata  è  previsto  in  &#13;
 alternativa alla riassunzione del lavoratore, mentre secondo lo statuto  &#13;
 dei lavoratori esso spetta in ogni caso.                                 &#13;
     È   intervenuto  in  giudizio  il  Presidente  del  Consiglio  dei  &#13;
 ministri, rappresentato e difeso dall'Avvocatura generale dello  Stato,  &#13;
 con   atto   depositato  il  28  ottobre  1974,  chiedendo  dichiararsi  &#13;
 l'infondatezza della questione sollevata.                                &#13;
     L'Avvocatura generale si riporta - a sostegno della sua richiesta -  &#13;
 alle argomentazioni sopra riassunte.                                     &#13;
     Si è costituito in giudizio Alessandro Scalcione, rappresentato  e  &#13;
 difeso  dall'avv.  Spartaco  Spano,  con  atto depositato il 26 ottobre  &#13;
 1974, chiedendo dichiararsi non fondata la questione prospettata.        &#13;
     Osserva la difesa della parte privata che la norma impugnata non è  &#13;
 affetta da vizi di illegittimità  costituzionale,  perché  il  regime  &#13;
 speciale  che  introduce  è  frutto di scelte politiche discrezionali,  &#13;
 ponderate  e  non  irrazionali.  Il  meccanismo  introdotto,   infatti,  &#13;
 risponde all'esigenza di contemperare l'interesse collettivo con quello  &#13;
 dell'autonomia privata, prendendo atto che il licenziamento illegittimo  &#13;
 produce  "in  ogni  caso"  un  danno per il lavoratore licenziato, come  &#13;
 dimostra l'esperienza; danno da liquidarsi in misura  non  inferiore  a  &#13;
 cinque  mesi  di  retribuzione.  D'altronde  non  vi sarebbe ragione di  &#13;
 vietare al legislatore  quanto  è  consentito  all'autonomia  privata,  &#13;
 ossia  l'apposizione  di clausole penali indipendenti dalla esistenza e  &#13;
 dalla prova del danno.                                                   &#13;
     Si  è  altresì  costituita  in  giudizio  la  società  Alitalia,  &#13;
 rappresentata  e difesa dall'avv. Maurizio Marazza, con atto depositato  &#13;
 il 24 ottobre 1974, chiedendo dichiararsi la fondatezza della questione  &#13;
 prospettata.                                                             &#13;
     La difesa dell'Alitalia svolge a  sostegno  delle  sue  istanze  le  &#13;
 seguenti  argomentazioni:  1)  la  norma  impugnata, mentre consente al  &#13;
 lavoratore la prova d'aver subito un danno maggiore  di  quello  minimo  &#13;
 predeterminato, pone il datore di lavoro in condizione di inferiorità,  &#13;
 precludendogli  di  dimostrare la sussistenza di un danno minore; 2) in  &#13;
 materia  di  responsabilità  da  inadempimento  contrattuale,  non  si  &#13;
 giustifica  la  disparità  di  trattamento  tra  le  due  parti; 3) la  &#13;
 previsione di una misura minima risarcitoria, se aveva ragion  d'essere  &#13;
 secondo  la  legge  n.  604 del 1966, avendo carattere alternativo alla  &#13;
 riassunzione, non trova  giustificazione  nel  sistema  previsto  dallo  &#13;
 statuto   dei   lavoratori,   ove   il  danno  subito  dovrebbe  essere  &#13;
 sostanzialmente commisurato alla mancata retribuzione per il  lasso  di  &#13;
 tempo  intercorrente  tra il licenziamento illegittimo e la sentenza di  &#13;
 primo grado, atteso l'effetto reintegrativo di quest'ultima.             &#13;
     Con  successive  memorie  le  parti  hanno sviluppato le rispettive  &#13;
 argomentazioni insistendo, alla  pubblica  udienza,  nelle  conclusioni  &#13;
 prese.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Attesa  l'identità  delle  questioni  prospettate i giudizi  &#13;
 possono essere riuniti e definiti con unica sentenza.                    &#13;
     2. - La Corte costituzionale è chiamata a decidere se contrasti  o  &#13;
 meno  con  il principio d'eguaglianza e con il diritto di difesa (artt.  &#13;
 3, primo comma, e 24, secondo comma, Cost.) l'art. 18,  secondo  comma,  &#13;
 della  legge  20 maggio 1970, n. 300, nella parte in cui attribuisce in  &#13;
 ogni caso al  lavoratore  illegittimamente  licenziato  il  diritto  al  &#13;
 risarcimento  danni  "in  misura  non  inferiore a cinque mensilità di  &#13;
 retribuzione, determinata secondo i criteri di cui all'art. 2121 c.c.".  &#13;
     Le ordinanze di remissione censurano la norma impugnata  in  quanto  &#13;
 la  deroga  apportata  ai  principi  vigenti  in  tema  di risarcimento  &#13;
 impedisce  al  datore  di  lavoro  di  provare  la  minore  entità,  o  &#13;
 addirittura l'eventuale mancanza del danno.                              &#13;
     La questione è infondata.                                           &#13;
     Il  richiamo  all'art. 24, secondo comma, della Costituzione appare  &#13;
 inconferente.  Secondo  la  costante  giurisprudenza  della  Corte   la  &#13;
 garanzia  costituzionale  della difesa opera entro i limiti del diritto  &#13;
 sostanziale.  Sicché in presenza di una  disposizione  che  ricolleghi  &#13;
 automaticamente alla condotta illecita il risarcimento dei danni in una  &#13;
 misura minima predeterminata, non può aversi violazione del diritto di  &#13;
 difesa,  ma,  in  ipotesi,  eventuale  illegittimità per contrasto con  &#13;
 l'art.  3  della  Costituzione,  ove  essa  attribuisca   ingiusto   ed  &#13;
 irrazionale privilegio ad una parte.                                     &#13;
     L'art.  18,  secondo  comma,  dello  statuto  dei  lavoratori,  non  &#13;
 contrasta  con  l'art.  3  della  Costituzione.  La  norma   impugnata,  &#13;
 innovando  al  regime stabilito dall'art. 8 della legge 15 luglio 1966,  &#13;
 n. 604, prevede che dalla sentenza di reintegra  nel  posto  di  lavoro  &#13;
 consegua,  in  caso  di  mancata  ottemperanza  del  datore  di lavoro,  &#13;
 l'obbligo  di  corrispondere  al  lavoratore  anche   le   retribuzioni  &#13;
 dovutegli  in  virtù del rapporto di lavoro, dalla data della sentenza  &#13;
 fino a quella dell'effettiva reintegrazione. Per il periodo antecedente  &#13;
 alla sentenza, la riconosciuta invalidità del  licenziamento  comporta  &#13;
 invece  una  disciplina  particolare:  da  un lato il rapporto non può  &#13;
 ritenersi estinto, sicché ai fini dell'anzianità va configurato  come  &#13;
 mai  interrotto;  dall'altro il legislatore ha previsto la sanzione del  &#13;
 risarcimento danni, con la predeterminazione  di  un  minimo.  Da  ciò  &#13;
 deriva  che  sulla  misura  del risarcimento potrà incidere - oltre il  &#13;
 limite di legge - quanto il  lavoratore  abbia  in  ipotesi  guadagnato  &#13;
 impiegando  altrimenti  le  proprie energie. La predeterminazione di un  &#13;
 risarcimento minimo, spettante in ogni caso di  licenziamento  invalido  &#13;
 od  inefficace,  costituisce  una  presunzione  legale  che, per essere  &#13;
 configurata in una misura realistica, in  rapporto  alle  ipotesi  più  &#13;
 frequenti e alla durata media dei procedimenti pretorili, non contrasta  &#13;
 con  l'art. 3 della Costituzione, ma costituisce legittimo esercizio di  &#13;
 discrezionalità politica da parte del legislatore.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 18, secondo comma, della legge 20 maggio 1970, n.  300  (c.d.  &#13;
 statuto dei lavoratori), nella parte in cui attribuisce in ogni caso al  &#13;
 lavoratore,  il  cui  licenziamento  sia  stato  dichiarato invalido od  &#13;
 inefficace, il diritto al risarcimento danni in misura non inferiore  a  &#13;
 cinque mensilità di retribuzione, sollevata, in riferimento agli artt.  &#13;
 3,  primo  comma,  e  24,  secondo  comma,  della  Costituzione, con le  &#13;
 ordinanze in epigrafe indicate.                                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  Costituzionale,  &#13;
 Palazzo della Consulta, il 18 giugno 1975.                               &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGT  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO  AMADEI - GIULIO GIONFRIDA -  &#13;
                                   EDOARDO VOLTERRA  -  GUIDO  ASTUTI  -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
