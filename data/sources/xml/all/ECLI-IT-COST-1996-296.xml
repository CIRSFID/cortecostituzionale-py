<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>296</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:296</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/07/1996</data_decisione>
    <data_deposito>23/07/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nei giudizi di legittimità costituzionale degli artt. 73, commi 4  e    &#13;
 5, ultima parte, e 14, lettera b), numeri 1 e 2, del d.P.R. 9 ottobre    &#13;
 1990,  n. 309 (Testo unico delle leggi in materia di disciplina degli    &#13;
 stupefacenti   e   sostanze   psicotrope,   prevenzione,    cura    e    &#13;
 riabilitazione  dei relativi stati di tossicodipendenza) promossi con    &#13;
 n. 2 ordinanze emesse il 18 ottobre 1995 e il  10  gennaio  1996  dal    &#13;
 giudice  per  le indagini preliminari presso il Tribunale di Roma nei    &#13;
 procedimenti  penali  a  carico  di  Pannella  Giacinto  ed  altri  e    &#13;
 Buccolini Cesare ed altra, iscritte ai nn. 878 del registro ordinanze    &#13;
 1995  e  168  del registro ordinanze 1996 e pubblicate nella Gazzetta    &#13;
 Ufficiale della Repubblica nn. 52, prima  serie  speciale,  dell'anno    &#13;
 1995 e 10, prima serie speciale, dell'anno 1996;                         &#13;
   Visti  gli  atti di costituzione di Pannella Giacinto ed altri e di    &#13;
 Pezzuto Vittorio nonché gli atti di intervento  del  Presidente  del    &#13;
 Consiglio dei Ministri;                                                  &#13;
   Udito  nell'udienza  pubblica del 9 luglio 1996 il giudice relatore    &#13;
 Renato Granata;                                                          &#13;
   Uditi gli avvocati Franco Coppi, Adelmo Manna e Beniamino  Caravita    &#13;
 di  Toritto  per  Pannella  Giacinto  ed altri, Beniamino Caravita di    &#13;
 Toritto per Pezzuto Vittorio e l'avvocato dello Stato Paolo di Tarzia    &#13;
 di Belmonte per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso del procedimento penale di convalida dell'arresto di    &#13;
 Pannella Giacinto, detto Marco, ed altri per il reato di  cessione  a    &#13;
 terzi di sostanze stupefacenti di cui alle tab. II e IV dell'art.  14    &#13;
 del  d.P.R.  9  ottobre  1990,  n.  309,  il  giudice per le indagini    &#13;
 preliminari presso il Tribunale di Roma ha sollevato  (con  ordinanza    &#13;
 del      18  ottobre  1995)  questione  incidentale  di  legittimità    &#13;
 costituzionale degli artt. 73, commi 4  e  5,  ultima  parte,  e  14,    &#13;
 lettera  b),  numeri  1 e 2, del d.P.R. 9 ottobre 1990, n. 309 (Testo    &#13;
 unico delle leggi in materia di  disciplina  degli    stupefacenti  e    &#13;
 sostanze  psicotrope, prevenzione, cura e riabilitazione dei relativi    &#13;
 stati di tossicodipendenza) con riferimento agli artt.  3,  13  e  25    &#13;
 della Costituzione. In particolare il giudice rimettente si duole del    &#13;
 fatto  che  dette  norme  includano,  ai fini della prevista sanzione    &#13;
 penale, i derivati della canapa indiana tra le sostanze  stupefacenti    &#13;
 e   psicotrope   con  riferimento  alle  condotte  di  lieve  entità    &#13;
 riconducibili al quinto comma dell'art. 73 cit.                          &#13;
   Premette il giudice a quo che a seguito del referendum  abrogativo,    &#13;
 il cui esito è stato sancito dal d.P.R. 5 giu-gno 1993, n. 171, sono    &#13;
 state depenalizzate le condotte di detenzione, acquisto, importazione    &#13;
 di  qualsiasi  sostanza stupefacente per uso personale, mentre rimane    &#13;
 sanzionata penalmente, tra l'altro, la condotta di cessione  gratuita    &#13;
 di  cosiddette  droghe  leggere  in  modica quantità che costituisce    &#13;
 un'attività  "logicamente  e   necessariamente   propedeutica   alla    &#13;
 detenzione per uso personale".                                           &#13;
   È  però  contraddittorio  -  ritiene il remittente - continuare a    &#13;
 sanzionare tale attività, mentre la detenzione per uso personale  è    &#13;
 stata  depenalizzata.  Ed in particolare le disposizioni censurate si    &#13;
 rivelano arbitrarie ed irragionevoli perché - violando il  principio    &#13;
 di  offensività - sanzionano penalmente comportamenti, diversi dalla    &#13;
 detenzione, relativi a stupefacenti la cui nocività è  paragonabile    &#13;
 (o  addirittura  inferiore) a quella di sostanze non vietate (ed anzi    &#13;
 talora  commercializzate  dallo  Stato),  ancorché   dichiaratamente    &#13;
 nocive,  quali  il  tabacco,  l'alcool  e gli psicofarmaci, dai quali    &#13;
 derivano dipendenza ed assuefazione (come riconosciuto  espressamente    &#13;
 dal  legislatore  nell'art. 2, lettere a), c) e g), del d.P.R. n. 309    &#13;
 del  1990).  Si  determina  in  tal  modo  anche  una  disparità  di    &#13;
 trattamento  che  non trova giustificazione nella tutela della salute    &#13;
 (e dunque nella nocività delle sostanze), né in ragioni  di  ordine    &#13;
 pubblico.                                                                &#13;
   2.  -  È  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo che la questione sia dichiarata infondata.                     &#13;
   L'Avvocatura  richiama  in  particolare  i principi affermati nella    &#13;
 sentenza n. 360 del 1995, in cui questa Corte ha puntualizzato che la    &#13;
 detenzione, l'acquisto e l'importazione di sostanze stupefacenti  per    &#13;
 uso  personale  si  presentano come condotte in stretto collegamento,    &#13;
 immediato  e   diretto,   con   l'uso   stesso;   collegamento   che,    &#13;
 naturalmente,  si  riferisce  all'uso personale del medesimo soggetto    &#13;
 che detiene.  Il che rende non irragionevole  un  atteggiamento  meno    &#13;
 rigoroso  del legislatore nei confronti di chi, ponendo in essere una    &#13;
 condotta direttamente antecedente al consumo,  ha  già  operato  una    &#13;
 scelta  che  l'ordinamento  non intende più contrastare nella rigida    &#13;
 forma della sanzione penale. Invece -  ritiene  l'Avvocatura  -  può    &#13;
 dirsi che nella condotta di chi cede droghe leggere manca quel "nesso    &#13;
 di immediatezza" con l'uso personale richiesto nella citata pronuncia    &#13;
 di  questa  Corte,  e ciò è sufficiente a giustificare un possibile    &#13;
 atteggiamento di maggior rigore nella previsione della norma  penale,    &#13;
 rientrando  nella discrezionalità del legislatore anche la scelta di    &#13;
 non agevolare comportamenti  propedeutici  all'approvvigionamento  di    &#13;
 sostanze stupefacenti per uso personale.                                 &#13;
   Comunque   la  cessione  di  droghe  leggere  può  ragionevolmente    &#13;
 ritenersi "pericolosa" in quanto, agevolando l'offerta delle sostanze    &#13;
 stupefacenti, aumenta di converso la domanda e quindi il  consumo  di    &#13;
 droga,  con gravissimo danno della salute pubblica, della sicurezza e    &#13;
 dell'ordine  pubblico;  sicché  è  logico  e  razionale  che a tali    &#13;
 condotte sia riservato un trattamento normativo più severo  rispetto    &#13;
 alle condotte finalizzate all'uso personale.                             &#13;
   L'Avvocatura  ricorda  infine  che  il disposto dell'art. 3, quarto    &#13;
 comma, lettera c) della convenzione di Vienna del  1988  preclude  la    &#13;
 liberalizzazione del commercio delle droghe, anche leggere, in quanto    &#13;
 consente  l'irrogazione di sanzioni di natura amministrativa soltanto    &#13;
 per le fattispecie di minore entità.                                    &#13;
   3. - Si sono costituite le parti private concludendo, anche con due    &#13;
 successive   memorie,   per   la   fondatezza   delle   censure    di    &#13;
 incostituzionalità formulate dal giudice remittente.                    &#13;
   Dopo la vicenda referendaria, che ha condotto alla depenalizzazione    &#13;
 della  detenzione  di sostanze stupefacenti per uso personale, l'art.    &#13;
 73  cit.  -  sostiene  la  difesa   -   ormai   ingloba   fattispecie    &#13;
 incriminatrici  di contenuto non più compatibile con quelle relative    &#13;
 alla detenzione per uso personale. Infatti,  oltre  a  prevedere  una    &#13;
 serie  di  condotte  latamente  riconducibili  all'acquisizione delle    &#13;
 sostanze per scopi di traffico commerciale, individua anche  condotte    &#13;
 fra  le  quali è da ricomprendere la cessione a titolo gratuito (per    &#13;
 evidente  scopo  dimostrativo)  di  piccoli  quantitativi  di  droghe    &#13;
 leggere,  condotta  questa  del  tutto  estranea  alla  finalità  di    &#13;
 spaccio. La scelta punitiva del legislatore  conserva  la  necessaria    &#13;
 coerenza  e  razionalità  soltanto  fino  a che ricevono il medesimo    &#13;
 trattamento ipotesi (descritte dall'art.  73) affini allo spaccio  al    &#13;
 fine  di  lucro;  invece  tale ragionevolezza viene meno quando nella    &#13;
 stessa disciplina si ricomprendono condotte le quali, pur comportando    &#13;
 una  diffusione  dell'uso  degli  stupefacenti,  sono  funzionalmente    &#13;
 collegate a quell'uso personale reso penalmente lecito dall'art. 75.     &#13;
   La  difesa poi, con riferimento alla disciplina degli stupefacenti,    &#13;
 ritiene identificabili due tipi normativi: il consumatore (da curare)    &#13;
 e lo spacciatore (da punire). Però questa  distinzione  tra  le  due    &#13;
 tipologie  di autore tenute presenti dal legislatore nel disegnare il    &#13;
 complessivo quadro normativo non è coerente  con  la  inclusione  di    &#13;
 taluni comportamenti nella fattispecie incriminatrice di cui all'art.    &#13;
 73.  Infatti  il  comma  primo  di  tale  disposizione, nel reprimere    &#13;
 svariati comportamenti con esclusione delle ipotesi di  cui  all'art.    &#13;
 75,  rende  manifesto l'intento del legislatore di sanzionare solo il    &#13;
 traffico  illecito  delle  sostanze  stupefacenti,   eliminando   nel    &#13;
 contempo   la   punibilità  del  consumo  e  delle  azioni  ad  esso    &#13;
 finalizzate. E invece  la  norma,  nell'includere  genericamente  una    &#13;
 moltitudine  di comportamenti, ha esteso eccessivamente il suo raggio    &#13;
 di applicazione, sconfinando in zone disciplinate dall'art. 75.          &#13;
   La difesa argomenta ancora che la normativa in tema di cessione  di    &#13;
 sostanze  stupefacenti  del  tipo  cannabis  e  suoi derivati rivela,    &#13;
 d'altro canto, i caratteri della irrazionalità e della arbitrarietà    &#13;
 anche alla luce del  principio  di  "offensività  necessaria"  della    &#13;
 norma  penale.  Infatti  i  derivati della cannabis indica posseggono    &#13;
 un'azione psicotropa pari a quella di altre  sostanze,  lecite  e  di    &#13;
 larghissimo uso quali psicofarmaci, caffeina, alcool, nicotina; ma, a    &#13;
 differenza di tali sostanze, non producono i danni alla salute che in    &#13;
 maniera  inconfutabile  sono  determinati  dall'uso di queste ultime.    &#13;
 Pertanto, la inclusione dell'hashish  e  marijuana  tra  le  sostanze    &#13;
 illecite,  avuto  riguardo  ai  parametri  di offensività desumibili    &#13;
 dalla  affermata  liceità delle altre sostanze psicotrope, determina    &#13;
 la  punibilità  di  condotte  prive  di  offensività   e   comporta    &#13;
 un'ingiustificata disparità di trattamento.                             &#13;
   4.  -  Nel corso del procedimento penale contro Buccolini Cesare ed    &#13;
 altri per il reato di cessione a terzi  di sostanze  stupefacenti  di    &#13;
 cui  alle tab. II e IV dell'art. 14 del d.P.R. 9 ottobre 1990, n.309,    &#13;
 il giudice per le  indagini preliminari presso il Tribunale  di  Roma    &#13;
 ha sollevato (con ordinanza del 10 gennaio 1996) questione               &#13;
  incidentale di legittimità costituzionale degli artt. 73, commi 4 e    &#13;
 5,  ultima  parte,  e  14,  lettera b), numeri 1 e 2,   del d.P.R.  9    &#13;
 ottobre 1990, n.309 in termini identici  a  quelli  della  precedente    &#13;
 ordinanza sopra indicata.                                                &#13;
   5.  -  È  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 riportandosi  integralmente  all'atto  di  intervento  depositato nel    &#13;
 giudizio  incidentale  promosso  con  la  precedente   ordinanza   di    &#13;
 rimessione del medesimo giudice.<diritto>Considerato in diritto</diritto>1.   -    In  entrambi  i  giudizi  è  stata  sollevata  questione    &#13;
 incidentale di legittimità  costituzionale  -  in  riferimento  agli    &#13;
 artt.  3,  13  e  25  della  Costituzione - degli artt. 14, lett. b),    &#13;
 numeri 1 e 2, e 73, commi 4 e 5, del d.P.R.  9  ottobre  1990,  n.309    &#13;
 (Testo  unico delle leggi in materia di disciplina degli stupefacenti    &#13;
 e sostanze                                                               &#13;
  psicotrope, prevenzione, cura e riabilitazione dei relativi stati di    &#13;
 tossicodipendenza),  nella  parte  in  cui  prevedono  come  condotta    &#13;
 sanzionata   penalmente   anche   la  cessione  gratuita  di  modesti    &#13;
 quantitativi di "droghe  leggere"  (quali  i  derivati  della  canapa    &#13;
 indiana)  per uso personale del cessionario. Si sospetta da parte del    &#13;
 giudice remittente la violazione del principio di ragionevolezza e di    &#13;
 eguaglianza in quanto  -  una  volta  depenalizzate  le  condotte  di    &#13;
 detenzione, acquisto, importazione di qualsiasi sostanza stupefacente    &#13;
 per  uso personale - sarebbe illogico mantenere penalmente sanzionata    &#13;
 la suddetta condotta di  cessione  gratuita  di  droghe  leggere  che    &#13;
 costituisce  un'attività "logicamente e necessariamente propedeutica    &#13;
 alla  detenzione  per  uso  personale".  Parimenti  il  principio  di    &#13;
 ragionevolezza  e quello di eguaglianza sarebbero violati perché non    &#13;
 è  prevista  come  reato  la  cessione  di  sostanze  egualmente  (o    &#13;
 addirittura   più)   nocive,   quali  il  tabacco,  l'alcool  e  gli    &#13;
 psicofarmaci. Infine sarebbe violato  il  principio  di  offensività    &#13;
 perché  non  è  posta in pericolo la salute degli assuntori di tali    &#13;
 sostanze stupefacenti (droghe  leggere),  né  è  leso  o  posto  in    &#13;
 pericolo l'ordine pubblico.                                              &#13;
   2. - I due giudizi vanno riuniti per identità dell'oggetto.           &#13;
   3. - Le questioni non sono fondate.                                    &#13;
   Il  citato  art.  73 del d.P.R. 9 ottobre 1990, n.309 (disposizione    &#13;
 censurata unitamente al precedente art. 14)  contemplava,  nella  sua    &#13;
 formulazione    originaria    prima    della   parziale   abrogazione    &#13;
 referendaria, un tipico reato a condotta  plurima  prevedendosi  come    &#13;
 delitto  il  fatto  di  chi  senza l'autorizzazione coltiva, produce,    &#13;
 fabbrica, estrae, raffina, vende, offre o mette in  vendita,  cede  o    &#13;
 riceve,   a  qualsiasi  titolo,  distribuisce,  commercia,  acquista,    &#13;
 trasporta,  esporta,  importa,  procura  ad  altri,  invia,  passa  o    &#13;
 spedisce  in  transito,  consegna  per  qualunque  scopo  o  comunque    &#13;
 illecitamente detiene, fuori dalle ipotesi previste dagli articoli 75    &#13;
 e 76, sostanze stupefacenti o psicotrope.  Al successivo quinto comma    &#13;
 era (ed è) poi delineata un'ipotesi attenuata, prevedendosi una pena    &#13;
 ridotta  quando,  per  il  mezzo,  per  le modalità o le circostanze    &#13;
 dell'azione ovvero per la qualità  e  quantità  delle  sostanze,  i    &#13;
 fatti sono di lieve entità.                                             &#13;
   Dal  novero  delle condotte contemplate dall'art. 73, il successivo    &#13;
 art. 75 ne estrapola tre - l'importazione, l'acquisto e la detenzione    &#13;
 - per riferirle ad una finalità specifica dell'agente, che è quella    &#13;
 di farne uso personale; per queste condotte la disciplina  precedente    &#13;
 alla  abrogazione  referendaria,  ispirata  ad  un maggior rigore nel    &#13;
 contrastare la diffusione della tossicodipendenza  e  dell'assunzione    &#13;
 di  sostanze  stupefacenti,  operava  una distinzione, utilizzando un    &#13;
 criterio quantitativo  (quello  della  dose  media  giornaliera)  per    &#13;
 tracciare  un  confine  tra l'illecito perseguito penalmente e quello    &#13;
 sanzionato solo in via amministrativa.                                   &#13;
   Per effetto dell'esito  referendario  è  caduta  tale  limitazione    &#13;
 quantitativa (il criterio della dose media giornaliera) e con essa la    &#13;
 distinzione  suddetta,  sicché le tre condotte contemplate dall'art.    &#13;
 75,  ove  finalizzate  all'uso  personale,  sono  state   interamente    &#13;
 attratte  nell'area  dell'illecito  amministrativo divenendo estranee    &#13;
 all'area del penalmente rilevante; in tal modo è risultata anche  in    &#13;
 parte  modificata la stessa strategia di (confermato) contrasto della    &#13;
 diffusione della droga nel senso che è stata  isolata  la  posizione    &#13;
 del   tossicodipendente  (e  anche  del  tossicofilo)  rendendo  tale    &#13;
 soggetto  destinatario  soltanto   di   sanzioni   amministrative   -    &#13;
 significative  peraltro  del  perdurante  disvalore  attribuito  alla    &#13;
 attività di assunzione di sostanze stupefacenti - ma  non  anche  di    &#13;
 sanzione  penale.  Ciò  però  non sulla base soggettiva dell'autore    &#13;
 della condotta, quasi si trattasse di una immunità personale, bensì    &#13;
 sulla  base  oggettiva  della  condotta  stessa  (quale   specificata    &#13;
 nell'art.  75 nelle tre ipotesi suddette) e dell'elemento teleologico    &#13;
 (della destinazione della droga ad uso personale). In tal modo - come    &#13;
 questa Corte ha già puntualizzato (sentenza n.360  del  1995)  -  ne    &#13;
 risulta  tracciata  una  "cintura  protettiva"  del consumo, volta ad    &#13;
 evitare il rischio che l'assunzione di  sostanze  stupefacenti  possa    &#13;
 indirettamente risultare di fatto assoggettata a sanzione penale.  In    &#13;
 quest'area   di   rispetto   ricadono  "comportamenti  immediatamente    &#13;
 precedenti  essendo  di  norma  la  detenzione  (spesso   l'acquisto,    &#13;
 talvolta l'importazione) l'antecedente ultimo dell'assunzione"; ed è    &#13;
 l'elemento   teleologico   della  destinazione  della  droga  all'uso    &#13;
 personale ad assicurare (secondo l'id quod  plerumque  accidit)  tale    &#13;
 nesso  di immediatezza.   Ove invece non ricorra l'elemento oggettivo    &#13;
 (di una delle tre condotte tipizzate  nell'art.  75  cit.)  o  quello    &#13;
 teleologico  (appena  ricordato)  si  ricade  nell'area dell'illecito    &#13;
 penale. Ciò anche nell'ipotesi di una condotta, quale  quella  della    &#13;
 coltivazione  di  piante da cui si possono estrarre i principi attivi    &#13;
 di sostanze stupefacenti al fine di fare uso personale delle  stesse,    &#13;
 che  si  approssima  notevolmente  a tale "cintura protettiva", ma ne    &#13;
 rimane  pur  sempre  all'esterno  mancando  la  puntuale  e  rigorosa    &#13;
 identificazione  di uno dei due requisiti prescritti: condotta questa    &#13;
 la cui perdurante rilevanza penale è stata ritenuta proprio per tale    &#13;
 ragione non illegittima da questa Corte nella citata  sentenza  n.360    &#13;
 del 1995.                                                                &#13;
   4.  -  Della incriminazione penale di un'altra condotta - anch'essa    &#13;
 esterna all'area di quelle depenalizzate - le ordinanze di rimessione    &#13;
 domandano ora  il  controllo  di  costituzionalità:  esattamente  la    &#13;
 condotta  che  i  giudici  remittenti  individuano  in  quella  della    &#13;
 cessione gratuita di modesti quantitativi di droghe leggere  per  uso    &#13;
 personale  del  cessionario.    Precisazione  questa,  circa l'esatto    &#13;
 oggetto della questione proposta, che appare  indispensabile  perché    &#13;
 la  condotta  di  cui  il  giudice  remittente  invoca in sostanza la    &#13;
 depenalizzazione a mezzo di pronuncia  di  questa  Corte  è  affatto    &#13;
 distinta  e  diversa  da altre fattispecie evocate dalla difesa, che,    &#13;
 proprio perché del tutto peculiari, sono estranee a quella  devoluta    &#13;
 allo  scrutinio  della  Corte,  nella  quale  non  rilevano  elementi    &#13;
 tipizzanti, oggettivi o soggettivi, ulteriori rispetto a quelli sopra    &#13;
 precisati.                                                               &#13;
   5. - Così definito il thema decidendum va innanzi  tutto  rilevata    &#13;
 la  non  fondatezza  del  profilo di censura, secondo cui non sarebbe    &#13;
 più giustificata la perdurante rilevanza penale della condotta  come    &#13;
 sopra  specificata  (cessione  gratuita  di  modesti  quantitativi di    &#13;
 droghe leggere per uso personale del cessionario) una  volta  che  è    &#13;
 stata  depenalizzata, a seguito del referendum, la detenzione per uso    &#13;
 personale, alla quale la cessione  è  necessariamente  propedeutica.    &#13;
 Infatti  il  trattamento  differenziato  tra cessione e detenzione (o    &#13;
 acquisto  o  importazione),  anche  quando  qualificate  l'una  dalla    &#13;
 destinazione finale della sostanza stupefacente all'uso personale del    &#13;
 cessionario  e  l'altra  a quello del detentore, non è irragionevole    &#13;
 perché - come già osservato evidenziandosi  le  ragioni  che  hanno    &#13;
 indotto   a  ritenere  parimenti  non  irragionevole  il  trattamento    &#13;
 differenziato  tra  coltivazione   e   detenzione   (o   acquisto   o    &#13;
 importazione)  pur  quando  qualificate  entrambe  dalla destinazione    &#13;
 all'uso personale dell'agente (sentenza n. 360 del 1995) -  non  c'è    &#13;
 immediatezza  tra  la  condotta  del  cedente e la destinazione della    &#13;
 sostanza all'uso  personale  del  cessionario,  immediatezza  che  è    &#13;
 invece  sottesa  alle  ragioni  della  depenalizzazione,  giacché il    &#13;
 rapporto fra cessione ed uso personale è mediato  dalla  condotta  -    &#13;
 anzi,   di   più:  dalla  mera  intenzione  -  di  un  soggetto  (il    &#13;
 cessionario)  diverso  dall'autore  (il   cedente)   della   condotta    &#13;
 penalmente sanzionata. Le ragioni della depenalizzazione referendaria    &#13;
 attengono  integralmente  alla  persona dell'assuntore, e le condotte    &#13;
 prossime, con  nesso  di  immediatezza,  al  consumo  in  tanto  sono    &#13;
 attratte  nell'area  della  depenalizzazione  in  quanto si è voluto    &#13;
 evitare ogni rischio  di  indiretta  criminalizzazione  del  consumo.    &#13;
 Queste  ragioni  vengono,  invece,  radicalmente  meno  quando  dalla    &#13;
 persona dell'assuntore si passa a considerare altri soggetti diversi,    &#13;
 onde si giustifica l'atteggiamento di rigore, serbato dal legislatore    &#13;
 nell'esercizio  della  sua  discrezionalità,  diretto  a   reprimere    &#13;
 qualsiasi  ipotesi  di  cessione,  anche  se  gratuita  e  di  modica    &#13;
 quantità (v. altresì infra), qualunque sia il fine  perseguito  dal    &#13;
 cedente. E come non è dubbio che il contrasto della diffusione della    &#13;
 droga costituisca un legittimo obiettivo di politica criminale, così    &#13;
 anche  non  è  incoerente,  ed è quindi parimenti legittimo, che il    &#13;
 legislatore  intenda  ostacolare  in  ogni  caso  la  fase  terminale    &#13;
 costituita  dalla  distribuzione  al  minuto, che è quella connotata    &#13;
 appunto dalla finalizzazione della  cessione  all'uso  personale  del    &#13;
 cessionario.    Anche e soprattutto perché, se non ci fosse cessione    &#13;
 al  consumatore,  il  mercato  della  droga  non avrebbe ragione, né    &#13;
 possibilità di esistere.                                                &#13;
   Nessun elemento di irragionevolezza contiene quindi  il  perdurante    &#13;
 mantenimento  della  cessione  in  questione nell'area del penalmente    &#13;
 illecito.                                                                &#13;
   6.  -  Anche,  poi,  quanto   all'elemento   della   modestia   dei    &#13;
 quantitativi  ceduti  ed  alla  gratuità della cessione non emergono    &#13;
 elementi di per sé idonei a rendere irragionevole  l'incriminazione.    &#13;
 Come  già rilevato, è proprio e soltanto la cessione al consumatore    &#13;
 finale, qualunque sia l'entità del quantitativo di  volta  in  volta    &#13;
 ceduto, che alimenta e realizza la circolazione della droga ed il suo    &#13;
 mercato;  quel mercato che il legislatore vuole combattere nel quadro    &#13;
 di una "efficace strategia di contrasto del narcotraffico", incidendo    &#13;
 proprio sull'"ultima fase di spaccio" (sentenza n. 333 del 1991).        &#13;
   Ciò che si vuole impedire è che la droga giunga  al  consumatore,    &#13;
 sia  perché  -  si ripete - senza quest'ultima fase il narcotraffico    &#13;
 non avrebbe ragione  di  esistere,  sia  perché  comunque  si  vuole    &#13;
 contrastare  ogni  ulteriore incremento del consumo di tali sostanze.    &#13;
 Non senza considerare che la cessione gratuita della droga  può  non    &#13;
 implausibilmente essere valutata come particolarmente pericolosa (per    &#13;
 il più elevato rischio di iniziazione di soggetti che altrimenti non    &#13;
 verrebbero  in  contatto  con  il mercato clandestino della droga) ed    &#13;
 insidiosa  (per  il  carattere  più  invitante  ed  accattivante  di    &#13;
 un'offerta  senza  richiesta  di  controprestazione),  sicché non è    &#13;
 irragionevole temere  che  dalla  (invocata)  depenalizzazione  della    &#13;
 cessione  gratuita in questione conseguirebbe in realtà una maggiore    &#13;
 diffusione della tossicodipendenza e della tossicofilia con indiretto    &#13;
 incremento ed incentivo del mercato clandestino,  il  quale  di  tale    &#13;
 depenalizzazione mirata finirebbe per giovarsi.                          &#13;
   Quindi  la  modica  quantità ed il carattere oneroso o gratuito di    &#13;
 tale ultima  cessione  incidono  soltanto  sulla  maggiore  o  minore    &#13;
 gravità del reato.                                                      &#13;
   D'altra   parte,   anche   l'asserito   fallimento  della  politica    &#13;
 proibizionistica, sul quale particolarmente insiste la  difesa  delle    &#13;
 parti   private  costituite,  non  può  che  formare  oggetto  della    &#13;
 valutazione  discrezionale  del  legislatore,  cui  appartiene   (nel    &#13;
 rispetto  dei  vincoli derivanti da accordi internazionali) la scelta    &#13;
 della più o meno rigida  strategia  di  controllo  della  diffusione    &#13;
 della droga e del contenimento del fenomeno delle tossicodipendenze.     &#13;
   7.  -  Quanto infine alla assunta non offensività (della cessione)    &#13;
 delle droghe leggere, perché  non  nocive  o  comunque  meno  nocive    &#13;
 dell'alcool, valgono le considerazioni già svolte da questa Corte da    &#13;
 ultimo  nella  sentenza  n.  333  del  1991 per escludere che sia non    &#13;
 giustificata la repressione penale dello spaccio di entrambi  i  tipi    &#13;
 di   sostanze   psicotrope.     Del  resto  è  risalente  nel  tempo    &#13;
 l'affermazione  della   Corte   circa   la   discrezionalità   della    &#13;
 valutazione del legislatore in ordine alla nocività dei vari tipi di    &#13;
 droga (sentenza n.170 del 1982), come pure circa la corrispondenza ad    &#13;
 un preciso obbligo internazionale dell'inibizione e repressione della    &#13;
 diffusione  anche  delle  droghe  leggere  (sentenza  n. 170 del 1982    &#13;
 citata; ordinanza n. 386 del 1987).   Obbligo  che  non  può  essere    &#13;
 ritenuto  inoperante  -  come  sembra sostenere la difesa delle parti    &#13;
 private costituite - rispetto alla ipotesi  della  cessione  per  uso    &#13;
 personale  del  cessionario sulla base dell'asserita equiparazione di    &#13;
 tale ipotesi con quella della detenzione per uso personale, dovendosi    &#13;
 una tale equiparazione escludere per le ragioni già indicate.           &#13;
   Come  anche è stato dalla Corte già ritenuto inconferente al tema    &#13;
 il richiamo alla nocività delle bevande alcooliche (sentenza n.  170    &#13;
 del 1982 citata), rispetto  alle  quali  la  Corte  stessa  ha  avuto    &#13;
 occasione  di  sottolineare  la  risalente  differenza di disciplina,    &#13;
 ispirata "ad una larga tolleranza" (sentenza n.104 del 1982).            &#13;
   Sotto questo profilo, dunque,  non  è  carente  del  connotato  in    &#13;
 astratto dell'offensività - salvo apprezzamento da parte del giudice    &#13;
 ordinario  dell'offensività  in concreto (sentenze nn. 133 e 308 del    &#13;
 1992; n.  333 del 1991) - la condotta relativa  alla  fase  terminale    &#13;
 del   circuito  del  narcotraffico,  consistente  nella  cessione  al    &#13;
 consumatore; fase questa che normalmente si realizza proprio mediante    &#13;
 la  cessione  capillare   di   modesti   quantitativi   di   sostanza    &#13;
 stupefacente.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i   giudizi,   dichiara   non  fondate  le  questioni  di    &#13;
 legittimità costituzionale degli artt. 14, lettera b), numeri 1 e 2,    &#13;
 e 73, commi 4 e 5, del d.P.R. 9  ottobre  1990,  n.309  (Testo  unico    &#13;
 delle  leggi  in  materia di disciplina degli stupefacenti e sostanze    &#13;
 psicotrope, prevenzione, cura e riabilitazione dei relativi stati  di    &#13;
 tossicodipendenza),  sollevate - in riferimento agli artt. 3, 13 e 25    &#13;
 della Costituzione - dal giudice per le indagini  preliminari  presso    &#13;
 il Tribunale di Roma con le ordinanze indicate in epigrafe.              &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1996.                               &#13;
                         Il Presidente: Ferri                             &#13;
                         Il redattore: Granata                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 luglio 1996.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
