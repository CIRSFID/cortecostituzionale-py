<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>132</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:132</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Vezio Crisafulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1970</data_decisione>
    <data_deposito>13/07/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale degli artt.  54 e 531  &#13;
 del codice di procedura penale, promosso con   ordinanza emessa  il  29  &#13;
 novembre 1968 dalla Corte d'appello di  Bologna nel procedimento penale  &#13;
 a  carico di Nanni Maria Luisa  Ernesta, iscritta al n. 31 del registro  &#13;
 ordinanze 1969 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 78 del 26  marzo 1969.                                                &#13;
     Visto  l'atto  d'intervento  del    Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 17  giugno 1970 il Giudice relatore  &#13;
 Vezio Crisafulli;                                                        &#13;
     udito il sostituto avvocato generale  dello Stato Franco Chiarotti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. -  Con ordinanza emessa il 29   novembre 1968 nel  corso  di  un  &#13;
 procedimento  penale  a  carico di Nanni   Maria Luisa Ernesta la Corte  &#13;
 d'appello  di  Bologna  ha     sollevato  questione   di   legittimità  &#13;
 costituzionale  degli artt. 54 e  531 del codice di procedura penale in  &#13;
 riferimento all'art. 24  della Costituzione, ritenendo che la normativa  &#13;
 denunciata che prevede  la  risoluzione  dei  conflitti  di  competenza  &#13;
 attraverso  un  procedimento  in  camera di consiglio presso la   Corte  &#13;
 suprema di cassazione senza intervento dei difensori sia  in  contrasto  &#13;
 con  il  principio costituzionale sulla inviolabilità  della difesa in  &#13;
 ogni stato e grado del procedimento. Nella  specie la questione sarebbe  &#13;
 rilevante, in quanto alla difesa   dell'imputata non fu  consentito  di  &#13;
 intervenire  nel procedimento  con il quale la Cassazione ha risolto un  &#13;
 conflitto fra il tribunale  ed il pubblico ministero che concerneva  il  &#13;
 giudizio in  corso.                                                      &#13;
     2.  -  È  intervenuto in giudizio il  Presidente del Consiglio dei  &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura    di  Stato,  con  &#13;
 deduzioni  depositate  il  13 febbraio 1969,   contestando anzitutto la  &#13;
 rilevanza della questione proposta, sia  perché il procedimento per la  &#13;
 risoluzione del conflitto di competenza si sarebbe ormai  concluso  con  &#13;
 una  pronuncia  avente efficacia di giudicato nei confronti del giudice  &#13;
 a quo, sia  perché detto procedimento avrebbe natura incidentale e del  &#13;
 tutto autonoma rispetto a quello ordinario in corso con la  conseguenza  &#13;
 che  il  giudice  di  quest'ultimo non sarebbe comunque   legittimato a  &#13;
 sollevare questioni ad esso attinenti.                                   &#13;
     Nel merito poi l'Avvocatura di Stato   osserva che  ai  fini  della  &#13;
 risoluzione  di  un  conflitto  di  competenza,   elevato dal giudice o  &#13;
 denunziato dalle parti, gli atti e i   documenti del  processo  vengono  &#13;
 trasmessi  alla  Corte  di    cassazione;  che le stesse parti ricevono  &#13;
 comunicazione dell'avvenuta fissazione dell'udienza di deliberazione in  &#13;
 camera di consiglio  e possono pertanto presentare documenti, istanze e  &#13;
 memorie  scritte.   Mancherebbe, in definitiva, soltanto  la  fase  del  &#13;
 dibattimento,  vale  a  dire  la  partecipazione  dei  difensori ad una  &#13;
 discussione  orale,  ma  ciò  costituirebbe  soltanto   una   speciale  &#13;
 caratteristica   del   procedimento   che   non   impedisce  né  rende  &#13;
 estremamente  difficile  l'esercizio  del   diritto   di   difesa.   Le  &#13;
 conclusioni    della  Avvocatura di Stato sono quindi volte ad ottenere  &#13;
 una  pronuncia  di  inammissibilità  ovvero  di  infondatezza    della  &#13;
 questione sollevata.                                                     &#13;
     3.    -  All'udienza  del 17 giugno 1970  l'avvocato dello Stato ha  &#13;
 confermato le proprie tesi e conclusioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     Deve accogliersi l'eccezione di   inammissibilità della  questione  &#13;
 per irrilevanza, sollevata in linea pregiudiziale dall'Avvocatura dello  &#13;
 Stato.                                                                   &#13;
     Le  disposizioni  denunciate (artt. 54 e  531 cod.  proc. pen.) non  &#13;
 concernono, infatti, il giudizio in corso presso  la Corte d'appello di  &#13;
 Bologna né  quello  di  primo  grado    oggetto  del  gravame,  avendo  &#13;
 esclusivo  riferimento ai   procedimenti in camera di consiglio dinanzi  &#13;
 alla Corte di cassazione  in  sede  di  risoluzione  dei  conflitti  di  &#13;
 competenza.  E, nella   specie, di dette norme ebbe a fare applicazione  &#13;
 la Corte di  cassazione decidendo, con sentenza  23  giugno  1966,  sul  &#13;
 conflitto   denunciato  dal  Procuratore  della  Repubblica  presso  il  &#13;
 tribunale di Bologna avverso  un'ordinanza  di  quel  tribunale    che,  &#13;
 dichiarata  la  nullità  dell'istruttoria sommaria per violazione  del  &#13;
 diritto di difesa, aveva rimesso gli atti allo stesso Procuratore della  &#13;
 Repubblica affinché procedesse a rinnovare  l'istruttoria.              &#13;
     La questione di legittimità  costituzionale degli artt. 54  e  531  &#13;
 del  codice  di  procedura  penale  viene  ora    proposta  dalla Corte  &#13;
 d'appello,  dopo  che  il  procedimento   in      cassazione   si   era  &#13;
 definitivamente  concluso  con  la menzionata   sentenza, che annullava  &#13;
 l'ordinanza  del  tribunale,  restituendo  a  questo   gli   atti   per  &#13;
 l'ulteriore corso del giudizio di merito.                                &#13;
     Nessuna   influenza,  pertanto,     potrebbe  avere  una  qualsiasi  &#13;
 pronuncia di questa Corte sul giudizio  a  quo,    ostandovi  l'effetto  &#13;
 preclusivo  della  sentenza  emessa dalla Corte di   cassazione, cui il  &#13;
 codice di rito, nell'art. 54, quarto comma,   attribuisce  testualmente  &#13;
 "autorità  di  cosa  giudicata" con   la sola eccezione prevista nello  &#13;
 stesso comma per l'ipotesi,  che qui non interessa, di  nuovi  fatti  o  &#13;
 circostanze che vengano  a modificare la competenza per materia.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione  di legittimità costituzionale  &#13;
 degli  artt.  54  e  531 del codice di procedura   penale, sollevata in  &#13;
 riferimento all'art.   24, comma secondo,    della  Costituzione  dalla  &#13;
 Corte d'appello di Bologna con  ordinanza 29 novembre 1968.              &#13;
     Così  deciso  in  Roma,  nella  sede   della Corte costituzionale,  &#13;
 Palazzo della Consulta, il 24 giugno 1970.                               &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE   VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI   -   ENZO   CAPALOZZA    -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
