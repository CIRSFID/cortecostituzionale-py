<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-05-04/87/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-05-04/87"/>
          <FRBRalias value="ECLI:IT:COST:2023:87" name="ECLI"/>
          <FRBRdate date="2023-05-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="87"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-05-04/87/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-05-04/87/ita@"/>
          <FRBRdate date="2023-05-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-05-04/87/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2023-05-04/87/ita@.xml"/>
          <FRBRdate date="2023-05-04" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2023-04-06" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2023-05-04" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="SCIARRA" href="/akn/ontology/person/it/SCIARRA" showAs="SCIARRA"/>
        <TLCPerson eId="stefanoPetitti" href="/akn/ontology/person/it/stefanoPetitti" showAs="Stefano Petitti"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2023</akn4cc:anno>
        <akn4cc:numero>87</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2023:87</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>SCIARRA</akn4cc:presidente>
        <akn4cc:relatore>Stefano Petitti</akn4cc:relatore>
        <akn4cc:data_decisione>2023-04-06</akn4cc:data_decisione>
        <akn4cc:data_deposito>2023-05-04</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>; Giudici : <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 1, commi da 261 a 266, della <ref href="/akn/it/act/legge/stato/2018-12-30/145/!main">legge 30 dicembre 2018, n. 145</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021), promosso dalla Corte dei conti, sezione giurisdizionale per la Regione Liguria, nel procedimento vertente tra L. C. e l'Istituto nazionale della previdenza sociale (INPS), con ordinanza del 4 marzo 2020 iscritta al n. 88 del registro ordinanze 2022 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 35, prima serie speciale, dell'anno 2022, la cui trattazione è stata fissata per l'adunanza in camera di consiglio del 5 aprile 2023.
 Visti l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> dell'INPS, nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 6 aprile 2023 il Giudice relatore Stefano Petitti;
 deliberato nella camera di consiglio del 6 aprile 2023.
 Ritenuto che, con ordinanza del 4 marzo 2020, iscritta al n. 88 del registro ordinanze 2022, la Corte dei conti, sezione giurisdizionale per la Regione Liguria, ha sollevato questioni di legittimità costituzionale dell'art. 1, commi da 261 a 266, della <ref href="/akn/it/act/legge/stato/2018-12-30/145/!main">legge 30 dicembre 2018, n. 145</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021), per contrasto con gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_2">artt. 2</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 della Costituzione</ref></mref>;
 che, per quanto riferisce l'ordinanza, nel giudizio principale il titolare di una pensione di elevato importo rivendica l'integralità del trattamento di quiescenza, al netto della riduzione quinquennale stabilita dalle disposizioni censurate per gli assegni superiori a 100.000 euro lordi su base annua;
 che, ad avviso del rimettente, tale decurtazione si risolverebbe in un prelievo tributario ingiustificatamente selettivo, abnorme nella durata e nella progressività; lederebbe inoltre i principi di adeguatezza e proporzionalità in materia previdenziale, deludendo l'affidamento di chi ha versato ingenti contributi durante la propria vita lavorativa; violerebbe infine il canone della buona amministrazione, considerata la genericità della destinazione dei risparmi di spesa;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo dichiararsi le questioni inammissibili o, in subordine, non fondate;
 che infatti, a parere dell'interveniente, quelli presentati dal rimettente sarebbero «argomenti generalissimi», comunque superati dalla sopravvenuta <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/234/!main">sentenza n. 234 del 2020</ref>, con la quale questa Corte ha dichiarato l'illegittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_261">art. 1, comma 261, della legge n. 145 del 2018</ref> limitatamente alla parte in cui ha stabilito la riduzione dei trattamenti pensionistici «per la durata di cinque anni», anziché «per la durata di tre anni»;
 che si è costituito in giudizio l'Istituto nazionale della previdenza sociale (INPS), parte nel giudizio a quo, e ha formulato conclusioni analoghe a quelle della difesa statale, in ragione della medesima constatata sopravvenienza, questa avendo determinato la cessazione del prelievo fin dal mese di gennaio 2022.
 Considerato che la Corte dei conti, sezione giurisdizionale per la Regione Liguria (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2022/88/!main">ord. n. 88 del 2022</ref>), ha sollevato questioni di legittimità costituzionale dell'art. 1, commi da 261 a 266, della <ref href="/akn/it/act/legge/stato/2018/145/!main">legge n. 145 del 2018</ref>, per contrasto con gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_2">artt. 2</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>;
 che le disposizioni censurate, stabilendo la riduzione quinquennale dei trattamenti pensionistici di importo annuo superiore a 100.000 euro lordi, avrebbero introdotto un prelievo tributario ingiustificatamente selettivo, abnorme per durata e progressività; avrebbero inoltre violato i principi di adeguatezza e proporzionalità in materia previdenziale, anche sotto il profilo dell'affidamento generato dal versamento dei contributi; avrebbero leso infine il canone della buona amministrazione, per la genericità nella destinazione dei risparmi di spesa;
 che questa Corte ha già deciso questioni analoghe a quelle ora in esame, con la sentenza n. 234, depositata il 9 novembre 2020, quindi sopravvenuta all'ordinanza di rimessione in scrutinio;
 che tale sentenza ha dichiarato l'illegittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_261">art. 1, comma 261, della legge n. 145 del 2018</ref>, per violazione degli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_23">23</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38 Cost.</ref></mref>, nella parte in cui stabilisce la riduzione dei trattamenti di quiescenza «per la durata di cinque anni», anziché «per la durata di tre anni»;
 che le questioni di legittimità costituzionale allora sollevate in riferimento alla natura tributaria del prelievo sono state dichiarate non fondate, avendo la menzionata sentenza riconosciuto nel contributo una misura di solidarietà endoprevidenziale, in quanto, a norma dell'art. 1, comma 265, della medesima legge, i conseguenti risparmi di spesa non sono acquisiti al bilancio statale, ma accantonati in fondi previdenziali;
 che la citata sentenza ha osservato che l'incidenza della riduzione è temperata non soltanto dalla progressività delle aliquote, ma anche dalla clausola di salvaguardia posta dall'art. 1, comma 267, della stessa legge, per effetto della quale l'applicazione del contributo di solidarietà non può ridurre l'assegno al di sotto di 100.000 euro annui;
 che questa Corte, sempre nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/234/!main">sentenza n. 234 del 2020</ref>, ha evidenziato la funzione di riequilibrio intergenerazionale del contestato prelievo, atteso che il comma 263 dell'<ref href="/akn/it/act/legge/stato/2018/145/!main#art_1">art. 1 della legge n. 145 del 2018</ref> ne esclude l'applicazione alle pensioni interamente liquidate con il sistema contributivo, trattamenti normalmente riservati ai lavoratori più giovani, il cui importo è di regola inferiore a quello calcolato col metodo retributivo o misto;
 che si è altresì rilevata la connessione tra la misura in questione e gli obiettivi di ricambio generazionale nel mercato del lavoro, perseguiti dal legislatore tramite l'istituto del pensionamento anticipato "quota 100", introdotto in via sperimentale per il triennio 2019-2021;
 che la violazione degli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_23">23</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38 Cost.</ref></mref>, esclusa dalla menzionata sentenza quanto al prelievo in sé, è stata invece dalla stessa sentenza riconosciuta in relazione alla durata del contributo superiore al triennio, la quale eccede la proiezione temporale della sperimentazione di "quota 100" e lo stesso orizzonte triennale del bilancio di previsione, oltre che il lasso ordinario delle valutazioni diacroniche in materia previdenziale;
 che questa Corte, con l'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2022/172/!main">ordinanza n. 172 del 2022</ref>, ha definito analoghe questioni di legittimità costituzionale aventi ad oggetto il medesimo contributo di solidarietà, sollevate prima della pubblicazione della <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/234/!main">sentenza n. 234 del 2020</ref>, ma giunte a decisione dopo di essa;
 che tali questioni sono state dichiarate manifestamente inammissibili per quanto concerne la durata quinquennale della decurtazione, «poiché essa è già stata ricondotta a legittimità costituzionale, con limitazione al triennio, sicché è sopravvenuta la carenza dell'oggetto della censura»;
 che, viceversa, le medesime questioni sono state dichiarate manifestamente infondate per quanto concerne la riduzione degli assegni nei limiti della durata triennale, «atteso che il rimettente non porta argomenti nuovi rispetto a quelli giudicati non fondati dalla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/234/!main">sentenza n. 234 del 2020</ref>»;
 che la stessa soluzione, e per gli stessi motivi, si impone in riferimento alle questioni ora in esame;
 che infatti, laddove denunciano l'abnormità della durata quinquennale del prelievo, tali questioni non hanno più oggetto, venuto meno per effetto della limitazione al triennio, mentre, quando denunciano il contributo in sé, entro il triennio, esse non sono sostenute da argomenti ulteriori rispetto a quelli già disattesi dalla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/234/!main">sentenza n. 234 del 2020</ref>;
 che anche l'odierna evocazione dei parametri di cui agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_2">artt. 2</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, non specificamente vagliati da tale sentenza, in realtà non introduce alcuna novità argomentativa, poiché quella decisione ha fatto leva proprio sul canone di solidarietà ex <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_2">art. 2 Cost.</ref> per giustificare il prelievo, nei limiti del triennio, quale prestazione patrimoniale imposta, e, circa la destinazione dei risparmi, qui denunciata per genericità al metro dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref>, ha rimarcato la sufficienza della devoluzione endoprevidenziale.
 Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 2, delle Norme integrative per i giudizi davanti alla Corte costituzionale, quest'ultimo vigente ratione temporis.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 1) dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 1, commi da 261 a 266, della <ref href="/akn/it/act/legge/stato/2018-12-30/145/!main">legge 30 dicembre 2018, n. 145</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021), nella parte in cui stabilisce la riduzione dei trattamenti pensionistici ivi indicati «per la durata di cinque anni», anziché «per la durata di tre anni», sollevate, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_2">artt. 2</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 della Costituzione</ref></mref>, dalla Corte dei conti, sezione giurisdizionale per la Regione Liguria, con l'ordinanza indicata in epigrafe;
 2) dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'art. 1, commi da 261 a 266, della <ref href="/akn/it/act/legge/stato/2018/145/!main">legge n. 145 del 2018</ref>, nella parte in cui stabilisce la riduzione dei trattamenti pensionistici ivi indicati «per la durata di tre anni», sollevate, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_2">artt. 2</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, dalla Corte dei conti, sezione giurisdizionale per la Regione Liguria, con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2023-04-06">6 aprile 2023</date>.
 F.to:
 <signature><judge refersTo="#silvanaSciarra" as="#presidente">Silvana SCIARRA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 Stefano PETITTI, Redattore
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2023-05-04">4 maggio 2023</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
