<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>82</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:82</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>OGGIONI</presidente>
    <relatore_pronuncia>Vezio Crisafulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/04/1976</data_decisione>
    <data_deposito>14/04/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. LUIGI OGGIONI, Presidente - Avv. &#13;
 ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA &#13;
 REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE &#13;
 ROSSANO - Prof. ANTONINO DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt.  342  &#13;
 e  352 del codice di procedura penale, promossi con ordinanze emesse il  &#13;
 5 ottobre 1973 dal pretore di Verona e l'11  giugno  1974  dal  giudice  &#13;
 istruttore   del   tribunale   di   Ravenna   nei  procedimenti  penali  &#13;
 rispettivamente a carico di ignoti e di Giulio  Maurizio,  iscritte  ai  &#13;
 nn.    17 e 410 del registro ordinanze 1974 e pubblicate nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 69 del 13 marzo 1974  e  n.  309  del  27  &#13;
 novembre 1974.                                                           &#13;
     Udito  nella  camera  di consiglio dell'11 dicembre 1975 il Giudice  &#13;
 relatore Vezio Crisafulli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza emessa il  5  ottobre  1973,  nel  corso  di  un  &#13;
 procedimento   penale   contro   ignoti,   rivolto   ad   accertare  le  &#13;
 responsabilità ai sensi  dell'art.  659  c.p.  per  il  disturbo  alle  &#13;
 occupazioni  ed  al  riposo  delle  persone  causato  dal noto fenomeno  &#13;
 acustico del "bang sonico" prodotto da aerei  militari  in  volo  sulla  &#13;
 città,  il  pretore  di Verona ha sollevato, in riferimento agli artt.  &#13;
 101 e 104 della Costituzione, questione di legittimità  costituzionale  &#13;
 degli  artt.  342  e 352 del codice di procedura penale, disciplinanti,  &#13;
 rispettivamente,  il  dovere  di  esibizione  da  parte  dei   pubblici  &#13;
 ufficiali  e  il  diritto di astenersi dal testimoniare e il divieto di  &#13;
 esame determinati dal segreto di ufficio.                                &#13;
     Secondo il giudice a quo, le norme impugnate  contrasterebbero  con  &#13;
 gli artt. 101 e 104 della Costituzione che rispettivamente sottopongono  &#13;
 il  giudice  soltanto  alla  legge  e  sanciscono  l'indipendenza della  &#13;
 magistratura da ogni altro potere, perché, consentendo  alla  P.A.  di  &#13;
 valutare  discrezionalmente la segretezza o meno di notizie o documenti  &#13;
 con  efficacia  preclusiva  nei  confronti  dell'autorità  giudiziaria  &#13;
 porrebbero il giudice in una posizione di soggezione rispetto al potere  &#13;
 esecutivo fino a paralizzare l'esercizio dell'azione penale.             &#13;
     2.  -  Con  altra  ordinanza,  emessa  l'11  giugno 1974 il giudice  &#13;
 istruttore del tribunale di Ravenna, ha sollevato analoga questione  di  &#13;
 legittimità  costituzionale  degli  artt.  342 e 352, terzo comma, del  &#13;
 codice di procedura penale per contrasto con gli artt. 3, 24,  28,  52,  &#13;
 terzo  comma,  101,  102,  103, 111, 112, e 113 della Costituzione, nel  &#13;
 corso di un procedimento penale a carico del colonnello Giulio Maurizio  &#13;
 che, in qualità di Presidente della Commissione di inchiesta  nominata  &#13;
 dall'autorità  militare  sulla  caduta  di un aereo G 91, rifiutava di  &#13;
 consegnare il relitto del velivolo attestando con dichiarazione scritta  &#13;
 che tale materiale costituiva segreto militare.                          &#13;
     In particolare, l'art. 342, primo comma, c.p.p., nella parte in cui  &#13;
 non  distingue,  ai  fini  della  opponibilità  al  giudice penale del  &#13;
 segreto militare, atti e documenti  che  debbono  rimanere  segreti  da  &#13;
 quelli  che hanno carattere semplicemente riservato, contrasterebbe con  &#13;
 gli artt.  24,  101,  112,  113,  102  e  103  della  Costituzione  che  &#13;
 sanciscono  l'inviolabilità del diritto di difesa in giudizio, nonché  &#13;
 la realizzazione della giustizia attraverso l'esercizio della  funzione  &#13;
 giurisdizionale.  La  stessa norma, inoltre, nella parte in cui dispone  &#13;
 che la dichiarazione di segreto  militare  possa  essere  fatta  "anche  &#13;
 senza  motivazione"  impedirebbe  qualsiasi sindacato del giudice sulla  &#13;
 sua fondatezza e comporterebbe un  difetto  di  motivazione  anche  nei  &#13;
 conseguenti  provvedimenti  giurisdizionali,  in violazione degli artt.  &#13;
 113, 103, terzo comma, e 111 nonché dell'art. 52, ultimo comma,  della  &#13;
 Costituzione,  dal  quale  si argomenterebbe la soggezione al controllo  &#13;
 giurisdizionale dell'ordinamento delle forze armate.  L'esenzione dalla  &#13;
 motivazione sarebbe  altresì  in  contrasto  con  l'art.  3  Cost.  in  &#13;
 considerazione  della  diversa disciplina dettata in tema di segreto di  &#13;
 ufficio e professionale, essendo ammesso, in tali  casi,  il  sindacato  &#13;
 del  giudice sulla fondatezza della dichiarazione di segreto, sindacato  &#13;
 ammesso anche, secondo la giurisprudenza, quando si procede per i reati  &#13;
 di spionaggio e simili.                                                  &#13;
     Sotto il profilo della  disparità  di  trattamento  di  situazioni  &#13;
 ritenute analoghe viene anche impugnato autonomamente l'art. 342, u.c.,  &#13;
 c.p.p. che conferisce al giudice il potere di indagare sulla fondatezza  &#13;
 della dichiarazione nei casi di segreto professionale o d'ufficio.       &#13;
     Un'ultima  censura  investe  l'art.  342, in relazione all'articolo  &#13;
 352,   terzo   comma,   c.p.p.,   nella   parte   in   cui    subordina  &#13;
 all'autorizzazione  del  Ministro  di  grazia  e  giustizia l'esercizio  &#13;
 dell'azione penale per il caso che  la  dichiarazione  di  segreto  sia  &#13;
 ritenuta  non  fondata,  autorizzazione non richiesta per il segreto di  &#13;
 ufficio e professionale, in quanto darebbe luogo ad una  disparità  di  &#13;
 trattamento  di  fattispecie  analoghe  in violazione dell'art. 3 della  &#13;
 Costituzione,  tenuto  anche  conto  che  non  è   prescritta   alcuna  &#13;
 autorizzazione  per  il  promuovimento  dell'azione penale in ordine ai  &#13;
 reati di violazione di segreti politici o militari.                      &#13;
     La norma sarebbe anche in contrasto con gli artt. 24  e  28  Cost.,  &#13;
 per  l'ostacolo  che ne deriverebbe all'esercizio del diritto di difesa  &#13;
 ed  all'attuazione  del  principio  della  responsabilità  penale  dei  &#13;
 funzionari dello Stato.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Le  ordinanze del pretore di Verona e del giudice istruttore  &#13;
 del tribunale di Ravenna  sollevano  questioni  tra  loro  strettamente  &#13;
 connesse,  aventi  ad  oggetto  le  medesime disposizioni di legge (gli  &#13;
 artt. 342 e 352 cod. proc. pen., la prima;  l'art.  342,  in  relazione  &#13;
 all'art.  352,  ultimo  comma,  la seconda). I relativi giudizi possono  &#13;
 perciò riunirsi per essere decisi congiuntamente.                       &#13;
     2. - L'ordinanza del pretore di Verona è stata emessa nel corso di  &#13;
 un procedimento penale contro ignoti, diretto  ad  accertare  eventuali  &#13;
 responsabilità ai sensi dell'art. 659 cod.  pen. per i disagi arrecati  &#13;
 alla  popolazione  della  città  dal  fenomeno del "bang" provocato da  &#13;
 aerei supersonici nel corso  di  esercitazioni  interessanti  la  zona.  &#13;
 Poiché   lo  stato  maggiore  aeronautico,  richiesto  di  notizie  in  &#13;
 proposito, aveva rifiutato di indicare le unità impiegate nelle  dette  &#13;
 esercitazioni,  in quanto coperte da "segreto militare", rendendo così  &#13;
 impossibile   identificare   i   piloti   ai   quali   attribuire    la  &#13;
 responsabilità  per i fatti testé accennati, il pretore sospendeva il  &#13;
 giudizio, denunciando a questa Corte gli artt. 342 e 352  cod.    proc.  &#13;
 pen.,  ritenuti  applicabili alla specie "per analogia", in riferimento  &#13;
 agli artt. 102 e 104 Cost., che garantiscono l'indipendenza dei giudici  &#13;
 e l'autonomia dell'ordine giudiziario.                                   &#13;
     Ma  la  questione  è  manifestamente  irrilevante,   perché   una  &#13;
 "richiesta  di  informazioni"  (come  lo  stesso  pretore la definisce)  &#13;
 rivolta dall'autorità giudiziaria all'autorità militare  non  rientra  &#13;
 né  nell'una  né  nell'altra  delle  disposizioni  del codice di rito  &#13;
 censurate nell'ordinanza.                                                &#13;
     Nel caso in oggetto, infatti, il segreto non fu opposto al  pretore  &#13;
 procedente  da un teste sottoposto ad interrogatorio (come esige l'art.  &#13;
 352) né dallo stato maggiore aeronautico, cui fosse stato  formalmente  &#13;
 ordinato  di  consegnare  cose  od  atti in suo possesso (come previsto  &#13;
 dall'art. 342),  ma  semplicemente  nel  corso  di  una  corrispondenza  &#13;
 ufficiosa  con  il  pretore.  Né  in siffatte condizioni sarebbe stato  &#13;
 possibile procedere a norma dell'ultimo comma dell'art. 352, richiamato  &#13;
 dal secondo   comma dell'art. 342, ovvero,  in  ipotesi,  a  norma  del  &#13;
 successivo  comma finale di quest'ultimo, in relazione all'articolo 372  &#13;
 del codice penale.                                                       &#13;
     3. - Fa altresì difetto la rilevanza di alcune  tra  le  questioni  &#13;
 sollevate   dal   giudice  istruttore  del  tribunale  di  Ravenna  con  &#13;
 l'ordinanza in oggetto, nel  testo  della  quale,  d'altronde,  non  si  &#13;
 rinviene in proposito neppure un cenno di motivazione.                   &#13;
     L'ordinanza,  infatti,  è  stata emessa nel corso del procedimento  &#13;
 penale promosso (per falso ideologico) contro  l'ufficiale  che,  quale  &#13;
 presidente   della   commissione  d'inchiesta  nominata  dall'autorità  &#13;
 militare a seguito di un incidente aviatorio, aveva opposto il "segreto  &#13;
 militare" all'ordine di sequestro del relitto disposto dal giudice, nel  &#13;
 corso  ed  ai  fini  di  altro  procedimento   penale   contro   ignoti  &#13;
 (cronologicamente  e  logicamente  antecedente)  per  il  reato  di cui  &#13;
 all'art.  428  cod.   pen.   (disastro   aviatorio).   Sono   in   essa  &#13;
 contestualmente  accolti  e  prospettati  due  ordini  di  censure, cui  &#13;
 avevano distinto riferimento due diverse richieste del P.M.:  avanzata,  &#13;
 l'una  nel  procedimento  contro  ignoti (per l'asserita illegittimità  &#13;
 costituzionale del secondo comma dell'art. 342 cod. proc.  pen.,  nella  &#13;
 parte  in  cui  sottrarrebbe  all'obbligo  di  esibizione  anche  atti,  &#13;
 documenti e cose concernenti notizie militari diverse  da  quelle  che,  &#13;
 nell'interesse  della  sicurezza  dello Stato, devono rimanere segrete,  &#13;
 consentendo altresì che la dichiarazione di segreto possa farsi "anche  &#13;
 senza  motivazione";  nonché  del  successivo  ultimo  comma,  che,  a  &#13;
 differenza  da  quanto  disposto  per altre specie di segreti, preclude  &#13;
 all'autorità  giudiziaria  qualsiasi  sindacato  sulla   dichiarazione  &#13;
 medesima);   avanzata,   l'altra,  nel  procedimento  aperto  a  carico  &#13;
 dell'ufficiale   autore    della    dichiarazione    (per    l'asserita  &#13;
 illegittimità costituzionale dello stesso art. 342, terzo comma, nella  &#13;
 parte  in  cui,  attraverso  il  rinvio all'ultimo comma dell'art. 352,  &#13;
 subordina il proseguimento dell'azione  penale  alla  autorizzazione  a  &#13;
 procedere del Ministro di grazia e giustizia).                           &#13;
     Dal  che  risulta  evidente  che  quest'ultima è l'unica questione  &#13;
 direttamente rilevante  nel  giudizio  a  quo  (che  è  quello  contro  &#13;
 l'autore  della dichiarazione), poiché la sola norma incidente su tale  &#13;
 giudizio, e  di  cui  devesi  in  esso  fare  applicazione,  è  quella  &#13;
 prescrivente  l'autorizzazione  a procedere, rimanendo, invece, a monte  &#13;
 le altre questioni, concernenti norme delle quali già è  stata  fatta  &#13;
 applicazione  nel  giudizio diretto ad accertare le responsabilità del  &#13;
 disastro aviatorio, che di quello a quo costituisce il presupposto.      &#13;
     4.  -  Secondo  l'assunto  dell'ordinanza,   la   norma   anzidetta  &#13;
 contrasterebbe con l'art. 3 Cost., per il diverso trattamento riservato  &#13;
 all'ipotesi del segreto militare rispetto a quella di segreto d'ufficio  &#13;
 e professionale, come pure ai casi di procedimento penale per i delitti  &#13;
 di  rivelazione  di  segreti militari, previsti dagli artt. 256 e segg.  &#13;
 cod. pen., poiché nella prima  soltanto,  a  differenza  dalle  altre,  &#13;
 l'azione  penale  nei  confronti  del  soggetto che, in altro processo,  &#13;
 aveva  opposto  il  segreto  viene  subordinata   alla   autorizzazione  &#13;
 ministeriale.                                                            &#13;
     Si  deduce  altresì  violazione  degli  artt. 24 e 28 Cost., ma il  &#13;
 richiamo a queste due disposizioni, non sorretto né comunque  chiarito  &#13;
 nell'ordinanza   da   argomentazione   alcuna,  si  rivela  agevolmente  &#13;
 inconferente. Non si vede, infatti,  quale  menomazione,  e  per  quale  &#13;
 soggetto,  possa  derivare  al diritto di difesa "in ogni stato e grado  &#13;
 del  giudizio"  dall'istituto  dell'autorizzazione  a  procedere   che,  &#13;
 impedendo  il  proseguimento  dell'azione  penale, condiziona lo stesso  &#13;
 ulteriore svolgimento di un  processo;  né  sarebbe  configurabile  un  &#13;
 interesse  giuridicamente rilevante di chi sia indiziato di un reato ad  &#13;
 essere sottoposto a giudizio (sent. nn. 17 e 142 del 1973).  Dal  canto  &#13;
 suo,  l'art. 28, affermando che i pubblici dipendenti sono responsabili  &#13;
 "degli atti compiuti in violazione dei diritti secondo le leggi penali,  &#13;
 civili ed amministrative", si riferisce a tutt'altre ipotesi e comunque  &#13;
 non vieta alle leggi, cui rinvia,  di  disciplinare  variamente  questa  &#13;
 triplice   responsabilità,   in   funzione  delle  diverse  situazioni  &#13;
 oggettive e degli interessi che vi si riconnettono.  E  per  l'appunto,  &#13;
 l'esigenza  che  contro colui che abbia opposto il segreto militare non  &#13;
 si proceda senza autorizzazione non attiene alla qualità dei  soggetti  &#13;
 indicati  negli  artt. 342 e 352, ma alla materia alla quale il segreto  &#13;
 si riferisce.                                                            &#13;
     5. - La questione torna  così  ad  accentrarsi  sul  punto  se  la  &#13;
 normativa  in  oggetto  contrasti  con  l'art.  3  Cost., sotto l'uno o  &#13;
 l'altro degli specifici profili prospettati nell'ordinanza  e  poc'anzi  &#13;
 riassunti.                                                               &#13;
     La  questione  non  è  fondata.  Per  quanto riguarda anzitutto il  &#13;
 diverso trattamento del segreto militare rispetto al segreto  d'ufficio  &#13;
 e professionale, è preliminarmente da rilevare che la necessità della  &#13;
 autorizzazione  del Ministro di grazia e giustizia per procedere contro  &#13;
 il soggetto dal quale proviene la dichiarazione  di  segreto  militare,  &#13;
 ove  l'autorità  giudiziaria  non  la  ritenga  fondata, risponde alla  &#13;
 medesima  ratio  di  tutela  dello  stesso  segreto,   che   giustifica  &#13;
 l'esclusione  assoluta  delle  prove,  reali  e testimoniali, stabilita  &#13;
 nelle restanti parti degli artt. 342 e 352  in  ragione  del  carattere  &#13;
 proprio  del  thema probandi.  Giacché, contrariamente a quanto mostra  &#13;
 di ritenere il giudice a  quo,  anche  il  procedimento  penale  contro  &#13;
 l'autore  della  dichiarazione, come già il rapporto al Ministro della  &#13;
 giustizia che lo precede, sono preordinati a consentire  l'acquisizione  &#13;
 delle  prove,  se  ed  in  quanto le circostanze cui si riferiscono non  &#13;
 siano   legittimamente   coperte   dal   segreto.   Ed   infatti,    se  &#13;
 l'autorizzazione  è  data, vuoi dire che il segreto non sussiste ed in  &#13;
 tal  caso  il  giudice non incontra più limiti nell'esercizio dei suoi  &#13;
 poteri  di  accertamento  della  verità;  ma  la  situazione   sarebbe  &#13;
 identica,  ove il filtro rappresentato dalla autorizzazione a procedere  &#13;
 non  ci  fosse,  e  perciò  la  tutela  del  segreto  ne  risulterebbe  &#13;
 compromessa.                                                             &#13;
     Ciò  precisato,  non  può  considerarsi irrazionale che il modo e  &#13;
 l'intensità della protezione - penale  e  processuale  -  delle  varie  &#13;
 specie   di  segreti  riconosciuti  nella  vigente  legislazione  siano  &#13;
 diversificati,  in  funzione  della  rilevanza  degli   interessi   cui  &#13;
 ineriscono, toccando il grado più alto quando sia in giuoco il segreto  &#13;
 militare vero e proprio, che, come si legge nell'art. 86 cod. pen. mil.  &#13;
 di pace, assiste le notizie concernenti "la forza, la preparazione o la  &#13;
 difesa  militare dello Stato", involgendo pertanto il supremo interesse  &#13;
 della sicurezza dello Stato nella sua  personalità  internazionale,  e  &#13;
 cioè   l'interesse   dello  Stato-comunità  alla  propria  integrità  &#13;
 territoriale,  indipendenza  e  -  al  limite   -   alla   stessa   sua  &#13;
 sopravvivenza.  Interesse  presente e preminente su ogni altro in tutti  &#13;
 gli ordinamenti statali, quale ne sia il  regime  politico,  che  trova  &#13;
 espressione,  nel  nostro  testo  costituzionale, nella formula solenne  &#13;
 dell'art. 52, che proclama la difesa della  Patria  "sacro  dovere  del  &#13;
 cittadino".                                                              &#13;
     6.  -  Quanto  poi  all'altro  profilo  di illegittimità sempre in  &#13;
 relazione all'art. 3 Cost.,  dedotto  nell'ordinanza,  per  non  essere  &#13;
 richiesta  l'autorizzazione  "nei procedimenti relativi alla violazione  &#13;
 dei segreti politici e militari",  a  differenza  che  nell'ipotesi  in  &#13;
 oggetto, è agevole rilevare che le situazioni che, così argomentando,  &#13;
 si  vorrebbe mettere a raffronto non sono, come si assume, analoghe, ma  &#13;
 per contro qualitativamente diverse.                                     &#13;
     Giacché, quando si procede per uno dei delitti di cui  agli  artt.  &#13;
 256,  257, 259 e 261 cod. pen., il segreto è già stato violato, ed il  &#13;
 giudizio è rivolto alla punizione del colpevole; ed anche nel caso  di  &#13;
 tentativo, il segreto non è più tale, perché la stessa contestazione  &#13;
 dell'accusa  implica  che  i  fatti,  cui il segreto si riferiva, siano  &#13;
 noti. Laddove, quando si procede a norma del combinato  disposto  degli  &#13;
 artt.  342,  secondo  comma,  e  352,  terzo comma, cod. proc. pen., il  &#13;
 presupposto è che la dichiarazione o l'esibizione  della  cosa  o  del  &#13;
 documento  siano  state  rifiutate,  adducendo  il  segreto:  il quale,  &#13;
 perciò, è ancora intatto. Di qui, secondo il già  detto,  l'esigenza  &#13;
 che  non  si  proceda senza l'autorizzazione del Ministro, dalla quale,  &#13;
 invece, è logico si prescinda nei casi sopra menzionati di rivelazione  &#13;
 e spionaggio.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibili:                                              &#13;
     a) la questione di legittimità costituzionale degli  artt.  342  e  &#13;
 352  del  codice  di  procedura  penale, sollevata, in riferimento agli  &#13;
 artt. 102  e  104  della  Costituzione,  dal  pretore  di  Verona,  con  &#13;
 l'ordinanza in epigrafe;                                                 &#13;
     b)  la  questione  di legittimità costituzionale del medesimo art.  &#13;
 342, primo e secondo comma, del codice di procedura penale,  sollevata,  &#13;
 in   riferimento  agli  artt.  24,  101,  102,  103  112  e  113  della  &#13;
 Costituzione, dal giudice istruttore presso il tribunale di Ravenna;     &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dello stesso art. 342, secondo comma, in relazione all'art. 352,  terzo  &#13;
 comma,  del  codice di procedura penale, sollevata, in riferimento agli  &#13;
 artt. 3, 24 e 28 della Costituzione, dal  giudice  istruttore  predetto  &#13;
 con l'ordinanza in epigrafe.                                             &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 6 aprile 1976.          &#13;
                                   F.to: LUIGI OGGIONI - ANGELO DE MARCO  &#13;
                                   - ERCOLE ROCCHETTI - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE STEFANO.                            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
