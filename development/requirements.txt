-e git+git@gitlab.com:CIRSFID/pykomantoso.git@e7fed1ef4cdbd7dfef5242868f6a8cf669f433cf#egg=akomantoso
beautifulsoup4==4.8.0
bs4==0.0.1
certifi==2019.6.16
chardet==3.0.4
-e git+git@gitlab.com:CIRSFID/cirsfid-parsers.git@fe938e2a7724888b6bf2bb7a9ad8c244357026e1#egg=cirsfidparsers
-e git+git@gitlab.com:CIRSFID/cortecostituzionale-py.git@5fcc9bb72de1128064dd9f71015270843bee70ee#egg=cortecostituzionale&subdirectory=../../CIRSFID/cortecostituzionale-py/development
cycler==0.10.0
idna==2.8
isodate==0.6.0
Jinja2==2.10
kiwisolver==1.1.0
lxml==4.3.4
MarkupSafe==1.1.1
matplotlib==3.1.3
multiprocessing-logging==0.3.0
numpy==1.18.1
pandas==1.0.1
pep8==1.7.1
pyflakes==2.1.1
pyparsing==2.4.6
python-dateutil==2.8.1
python-dotenv==0.10.3
pytz==2019.3
rdflib==4.2.2
regex==2019.8.19
replus==0.1.0
requests==2.22.0
scipy==1.4.1
seaborn==0.10.0
six==1.14.0
soupsieve==1.9.3
SPARQLWrapper==1.8.5
tqdm==4.32.2
urllib3==1.25.3
virtualenv==16.7.0
