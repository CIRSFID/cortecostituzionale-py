<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-03-26/58/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-03-26/58"/>
          <FRBRalias value="ECLI:IT:COST:2020:58" name="ECLI"/>
          <FRBRdate date="2020-03-26" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="58"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-03-26/58/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-03-26/58/ita@"/>
          <FRBRdate date="2020-03-26" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-03-26/58/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-03-26/58/ita@.xml"/>
          <FRBRdate date="2020-03-26" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-02-12" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-03-26" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>non fondatezza</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="stefanoPetitti" href="/akn/ontology/person/it/stefanoPetitti" showAs="Stefano Petitti"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>58</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:58</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Stefano Petitti</akn4cc:relatore>
        <akn4cc:data_decisione>2020-02-12</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-03-26</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>non fondatezza</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 del codice di procedura civile</ref>, promosso dalla Corte d'appello di Milano nel procedimento vertente tra C.  B. e la Italo Sicav plc, con ordinanza del 30 gennaio 2019, iscritta al n. 82 del registro ordinanze 2019 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 23, prima serie speciale, dell'anno 2019.
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nella camera di consiglio del 12 febbraio 2020 il Giudice relatore Stefano Petitti;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>deliberato nella camera di consiglio del 12 febbraio 2020.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 30 gennaio 2019, la Corte d'appello di Milano ha sollevato questione di legittimità costituzionale dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 del codice di procedura civile</ref>, in riferimento agli artt. 3, 24, 111 e 117, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, quest'ultimo in relazione all'art. 6 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con <ref href="/akn/it/act/legge/stato/1955-08-04/848/!main">legge 4 agosto 1955, n. 848</ref>.
 La disposizione censurata violerebbe gli evocati parametri costituzionali «nella parte in cui non prevede che il giudice d'appello debba rimettere la causa al giudice di primo grado, se è mancato il contraddittorio, non essendo stata da questo neppure valutata, in conseguenza di un'erronea dichiarazione di improcedibilità dell'opposizione, la richiesta di chiamata in causa del terzo, proposta dall'opponente in primo grado, con conseguente lesione del diritto di difesa di una delle parti».
 1.1.- L'art. 354, primo comma, <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main">cod. proc. civ</ref>. stabilisce che «[f]uori dei casi previsti nell'articolo precedente, il giudice d'appello non può rimettere la causa al primo giudice, tranne che dichiari nulla la notificazione della citazione introduttiva, oppure riconosca che nel giudizio di primo grado doveva essere integrato il contraddittorio o non doveva essere estromessa una parte, ovvero dichiari la nullità della sentenza di primo grado a norma dell'articolo 161, secondo comma».
 Il secondo comma, prevede che «[i]l giudice d'appello rimette la causa al primo giudice anche nel caso di riforma della sentenza che ha pronunciato sulla estinzione del processo a norma e nelle forme dell'articolo 308».
 L'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 cod. proc. civ.</ref> richiama ed integra il precedente art. 353, il quale, a sua volta, stabilisce che «[i]l giudice d'appello, se riforma la sentenza di primo grado dichiarando che il giudice ordinario ha sulla causa la giurisdizione negata dal primo giudice, pronuncia sentenza con la quale rimanda le parti davanti al primo giudice».
 2.- La Corte d'appello di Milano riferisce che il giudice di primo grado, quale giudice dell'opposizione a decreto ingiuntivo, aveva dichiarato l'opposizione stessa improcedibile per mancato esperimento del tentativo obbligatorio di mediazione.
 Il primo giudice non aveva, quindi, neppure valutato l'istanza dell'opponente, il quale, con l'atto stesso di opposizione, aveva chiesto di chiamare in causa la compagnia assicuratrice garante della restituzione del finanziamento oggetto dell'iniziativa monitoria.
 2.1.- Il giudice a quo precisa che, investito dell'appello dell'opponente, aveva assegnato il termine di legge per l'esperimento della mediazione obbligatoria, come avrebbe dovuto fare il primo giudice, e che, in tal modo soddisfatta la condizione di procedibilità dell'opposizione, era finalmente da valutarsi l'istanza di chiamata in garanzia, come reiterata dall'appellante, previa rimessione della causa al giudice di primo grado.
 La Corte rimettente osserva che l'intervento del garante non avrebbe potuto essere provocato in grado d'appello, nel quale l'intervento del terzo è consentito soltanto ai soggetti legittimati all'opposizione di terzo, per effetto del combinato disposto degli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_344">artt. 344</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_404">404 cod. proc. civ.</ref></mref>
 Posto che l'appellante ha chiesto la rimessione della causa al giudice di primo grado, in applicazione analogica degli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref>, per potervi svolgere la chiamata del terzo in garanzia, la Corte d'appello di Milano rileva come a tale istanza osti il consolidato orientamento della giurisprudenza di legittimità, che qualifica come tassative, eccezionali e insuscettibili di applicazione analogica le ipotesi di rimessione in primo grado contemplate dagli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref>
 3.- Ad avviso del giudice a quo, l'impossibilità della rimessione della causa al giudice di primo grado pregiudica il diritto di difesa dell'opponente, che si vede costretto ad agire in via autonoma contro il garante, senza potersi avvalere, nei suoi confronti, del giudicato formatosi sull'azione principale.
 Il principio di tassatività ed eccezionalità delle cause di rimessione in primo grado dovrebbe essere ripensato, quindi, anche alla luce della metamorfosi del giudizio d'appello, che il legislatore avrebbe progressivamente trasformato da novum iudicium, di carattere sostitutivo, in revisio prioris instantiae, di stampo cassatorio.
 3.1.- Il giudice a quo fa riferimento alla disposizione dell'art. 105 dell'Allegato 1 (Codice del processo amministrativo) al <ref href="/akn/it/act/decretoLegislativo/stato/2010-07-02/104/!main">decreto legislativo 2 luglio 2010, n. 104</ref> (Attuazione dell'<ref href="/akn/it/act/legge/stato/2009-06-18/69/!main#art_44">articolo 44 della legge 18 giugno 2009, n. 69</ref>, recante delega al governo per il riordino del processo amministrativo), che prevede la rimessione in primo grado con una formula "aperta", «se è mancato il contraddittorio, oppure è stato leso il diritto di difesa di una delle parti», sicché l'omessa previsione della rimessione in ogni ipotesi di difetto del contraddittorio ed ogni lesione del diritto di difesa anche nel processo civile paleserebbe un'irragionevole disparità tra modelli processuali.
 L'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 cod. proc. civ.</ref> violerebbe, pertanto, gli artt. 3, 24, 111 e 117, primo comma, Cost., quest'ultimo in relazione all'art. 6 CEDU, «nella parte in cui non prevede che il giudice d'appello debba rimettere la causa al primo giudice, se è mancato il contraddittorio, oppure è stato leso il diritto di difesa di una delle parti».
 4.- Nel giudizio innanzi a questa Corte è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha eccepito l'inammissibilità della questione, sotto due distinti profili.
 4.1.- In primo luogo, l'Avvocatura ha eccepito l'inammissibilità della questione per il carattere meramente ipotetico del denunciato pregiudizio difensivo, atteso che la chiamata in causa del garante sarebbe stata comunque soggetta al potere autorizzativo del giudice di primo grado, che avrebbe potuto discrezionalmente rifiutarne lo svolgimento.
 4.2.- L'interveniente ha eccepito l'inammissibilità della questione anche per l'incertezza oggettiva dell'ordinanza di rimessione, che, in parte motiva, si riferisce genericamente ad ogni difetto del contraddittorio e ad ogni lesione della difesa, mentre, in dispositivo, si riferisce unicamente alla fattispecie dell'omessa valutazione dell'istanza di chiamata.
 4.3.- L'Avvocatura aggiunge che la pretermissione in primo grado dell'istanza di chiamata in garanzia non ha una soluzione costituzionalmente obbligata, rientrando nella discrezionalità del legislatore optare per l'una o l'altra delle soluzioni astrattamente praticabili, onde consentire alla parte l'esercizio dell'azione di garanzia, comunque sempre possibile tramite l'instaurazione di un autonomo giudizio nei confronti del garante.
 4.4.- Negata la trasformazione dell'appello da mezzo devolutivo a mezzo cassatorio, l'Avvocatura osserva che non la disciplina della rimessione nel processo civile debba adeguarsi a quella del processo amministrativo, semmai l'inverso.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- La Corte d'appello di Milano ha sollevato questione di legittimità costituzionale dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 del codice di procedura civile</ref>, in riferimento agli artt. 3, 24, 111 e 117, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, quest'ultimo in relazione all'art. 6 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con <ref href="/akn/it/act/legge/stato/1955-08-04/848/!main">legge 4 agosto 1955, n. 848</ref>, «nella parte in cui non prevede che il giudice d'appello debba rimettere la causa al giudice di primo grado, se è mancato il contraddittorio, non essendo stata da questo neppure valutata, in conseguenza di un'erronea dichiarazione di improcedibilità dell'opposizione, la richiesta di chiamata in causa del terzo, proposta dall'opponente in primo grado, con conseguente lesione del diritto di difesa di una delle parti».
 1.1.- Ad avviso del giudice a quo, l'impossibilità della rimessione della causa al giudice di primo grado, derivante dal consolidato orientamento della giurisprudenza di legittimità che qualifica come tassative, eccezionali e insuscettibili di applicazione analogica le ipotesi di rimessione contemplate dagli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref>, pregiudicherebbe il diritto di difesa dell'opponente, che sarebbe costretto ad agire in via autonoma contro il garante, senza potersi avvalere, nei suoi confronti, del giudicato formatosi sull'azione principale.
 1.2.- Secondo il giudice a quo, il principio di tassatività ed eccezionalità delle ipotesi di rimessione dovrebbe essere rivisto alla luce della progressiva trasformazione dell'appello civile, che avrebbe ormai assunto, per effetto delle riforme succedutesi nel tempo, i caratteri di una revisio prioris instantiae di natura cassatoria.
 1.3.- Il principio di tassatività ed eccezionalità delle ipotesi di rimessione dovrebbe essere riconsiderato anche alla luce della disposizione dell'art. 105 dell'Allegato 1 (Codice del processo amministrativo) al <ref href="/akn/it/act/decretoLegislativo/stato/2010-07-02/104/!main">decreto legislativo 2 luglio 2010, n. 104</ref> (Attuazione dell'<ref href="/akn/it/act/legge/stato/2009-06-18/69/!main#art_44">articolo 44 della legge 18 giugno 2009, n. 69</ref>, recante delega al governo per il riordino del processo amministrativo), che prevede la rimessione in primo grado con una locuzione generale, riferita ad ogni difetto del contraddittorio e ad ogni lesione del diritto di difesa.
 2.- In via preliminare, devono essere esaminate le eccezioni di inammissibilità della questione, sollevate dall'Avvocatura generale dello Stato.
 2.1.- Secondo l'Avvocatura, il petitum additivo dell'ordinanza di rimessione è ancipite, poiché questa, in motivazione, si riferisce a qualunque mancanza del contraddittorio o lesione della difesa, mentre, in dispositivo, ad una specifica mancanza e lesione, cioè all'omessa valutazione dell'istanza di chiamata del terzo.
 2.1.1.- L'eccezione non è fondata.
 L'alternatività del petitum che rende ancipite, e pertanto inammissibile, la questione di legittimità costituzionale è quella che non può essere sciolta per via interpretativa, e che si configura, quindi, come un'alternatività irrisolta (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/175/!main">is, sentenze n. 175 del 2018</ref>, n. 22 del 2016, n. 247 del 2015 e n. 248 del 2014; <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2017/221/!main">ordinanze n. 221</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2017/130/!main">130 del 2017</ref></mref>).
 Nel caso in esame, la discrepanza tra motivazione e dispositivo dell'ordinanza di rimessione può agevolmente risolversi tramite gli ordinari criteri ermeneutici, che consentono di intendere l'esposizione più ampia del petitum, di cui alla motivazione, come una semplice premessa generale, introduttiva dell'indicazione specifica del petitum effettivo, contenuta in dispositivo.
 2.2.- L'Avvocatura ha eccepito l'inammissibilità della questione anche per il carattere meramente ipotetico del pregiudizio difensivo dell'opponente a decreto ingiuntivo, convenuto sostanziale, la cui istanza di chiamata del terzo in garanzia, non sussistendo un'ipotesi di litisconsorzio necessario, avrebbe potuto essere discrezionalmente respinta dal giudice di primo grado.
 2.2.1.- L'eccezione non è fondata.
 Nella prospettiva del giudice a quo, la lesione del diritto di difesa del convenuto-opponente non è rappresentata dall'omessa autorizzazione della chiamata di terzo, bensì, in radice, dall'omessa valutazione della relativa istanza, del tutto pretermessa per effetto dell'errata declaratoria di improcedibilità dell'opposizione.
 Non si configura, pertanto, quel carattere solo teorico, astratto o prematuro, che rende la questione incidentale priva di effettiva rilevanza nel giudizio a quo, e come tale inammissibile per ipoteticità (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/217/!main">is, sentenze n. 217 del 2019</ref> e n. 60 del 2014; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2009/77/!main">ordinanze n. 77 del 2009</ref> e n. 109 del 2006).
 3.- Nel merito, la questione non è fondata.
 3.1.- Secondo la costante giurisprudenza di questa Corte, il legislatore dispone di un'ampia discrezionalità nella conformazione degli istituti processuali, incontrando il solo limite della manifesta irragionevolezza o arbitrarietà delle scelte compiute, limite che, con riferimento specifico all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">art. 24 Cost.</ref>, viene superato solo qualora emerga un'ingiustificabile compressione del diritto di agire, mentre il parametro evocato non esige che il cittadino sia messo in grado di conseguire la tutela giurisdizionale sempre nello stesso modo e con i medesimi effetti, purché non vengano imposti oneri o prescritte modalità tali da rendere impossibile o estremamente difficile l'esercizio del diritto di difesa o lo svolgimento dell'attività processuale (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/271/!main">is, sentenze n. 271 del 2019</ref>, n. 199 del 2017, n. 121 e n. 44 del 2016).
 In particolare, nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2002/1/!main">sentenza n. 1 del 2002</ref>, si è evidenziato che «nell'ordinamento processuale civile, la rimessione al primo giudice è fenomeno limitato ai casi previsti dagli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref>», sicché corrisponde ai principi che il secondo giudice decida nel merito, senza dare luogo a rimessione, qualora abbia «constatata una violazione in prima istanza delle regole del contraddittorio o del diritto di difesa non riconducibile ai casi di rimessione espressamente previsti».
 3.1.1.- La tassatività ed eccezionalità delle ipotesi normative di rimessione in primo grado, del resto, è affermata dall'univoca e risalente giurisprudenza delle Sezioni unite della Corte di cassazione, come un riflesso della natura prevalentemente rescissoria del giudizio d'appello, coerente con la regola di assorbimento dei vizi di nullità in motivi di gravame, potendo il giudice d'appello limitarsi ad emettere una pronuncia rescindente, cioè di mero annullamento con rinvio, nei soli casi espressamente indicati dal legislatore (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1972-11-04/1/!main">sentenze 14 novembre 1972</ref>, n. 3368, 12 gennaio 1963, n. 34 e 28 luglio 1962, n. 2208; conformi, tra molte, <ref href="/akn/it/judgment/sentenza/corteDiCassazione/2006-03-07/1CIV/!main">Corte di cassazione, sezione terza civile, sentenza 17 marzo 2006</ref>, n. 5907, e<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2002-05-05/1CIV/!main"> sezione seconda civile, sentenza 15 maggio 2002</ref>, n. 7057).
 3.2.- Né meritevole di accoglimento appare la deduzione del giudice a quo, secondo cui la necessità di agire in via autonoma contro il terzo garante, derivante dall'impossibilità del regresso in primo grado, lederebbe il diritto di difesa del convenuto-opponente poiché questo verrebbe privato del vantaggio del processo simultaneo, segnatamente del beneficio della formazione contestuale del giudicato sul rapporto principale e sul rapporto di garanzia.
 3.2.1.- Questa Corte ha, infatti, più volte evidenziato che, nel quadro della discrezionalità conformativa del legislatore processuale, il simultaneus processus non gode di garanzia costituzionale, trattandosi di un mero espediente tecnico finalizzato, laddove possibile, a realizzare un'economia dei giudizi e a prevenire il conflitto tra giudicati, sicché la sua inattuabilità non lede il diritto di azione, né quello di difesa, se la pretesa sostanziale dell'interessato può essere fatta valere nella competente, pur se distinta, sede giudiziaria con pienezza di contraddittorio e difesa (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1997/451/!main">is, sentenze n. 451 del 1997</ref> e n. 295 del 1995; <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2005/215/!main">ordinanze n. 215</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2005/124/!main">124 del 2005</ref></mref>, n. 18 del 1999 e n. 308 del 1991).
 La scelta del legislatore di non includere tra le ipotesi di rimessione in primo grado quella della pretermissione dell'istanza del convenuto-opponente di chiamata di un terzo in garanzia impedisce di recuperare il processo simultaneo tra la domanda principale e la domanda di garanzia, ma non impedisce che quest'ultima sia fatta valere nella competente, pur se distinta, sede giudiziaria con pienezza di contraddittorio e difesa.
 3.3.- Né è ravvisabile, per effetto delle modifiche introdotte dal legislatore nella disciplina del giudizio civile d'appello, una sua piena trasformazione dal modello sostitutivo del novum iudicium a quello di una revisio prioris instantiae di tipo cassatorio, sicché la configurazione in termini di tassatività e di eccezionalità delle ipotesi di rimessione in primo grado non sarebbe più coerente con l'attuale struttura dell'appello civile.
 3.3.1.- In realtà, come evidenziato dalle Sezioni unite della Corte di cassazione, l'accentuazione dei tratti di revisione determinata dalle riforme del codice di rito, in particolare da quella di cui al <ref href="/akn/it/act/decretoLegge/stato/2012-06-22/83/!main">decreto-legge 22 giugno 2012, n. 83</ref> (Misure urgenti per la crescita del Paese), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2012-08-07/134/!main">legge 7 agosto 2012, n. 134</ref>, non ha certamente trasformato l'appello civile in un mezzo di impugnazione a critica vincolata, «una sorta di anticipato ricorso per cassazione» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017-11-06/1/!main">sentenza 16 novembre 2017</ref>, n. 27199).
 Alla struttura del giudizio d'appello resta connaturato il profilo rescissorio, mantenendosi tassative ed eccezionali le ipotesi normative nelle quali il gravame può arrestarsi al solo profilo rescindente, in funzione della rimessione della causa al primo giudice (<ref href="/akn/it/judgment/ordinanza/corteDiCassazione/2019-04-07/1CIV/!main">Corte di cassazione, sezione seconda civile, ordinanza 17 aprile 2019</ref>, n. 10744).
 3.4.- Neanche può essere accolta la deduzione del giudice a quo, secondo cui la formula ampia dell'art. 105 cod. proc. amm., laddove prevede la rimessione in primo grado «se è mancato il contraddittorio, oppure è stato leso il diritto di difesa di una delle parti», raffrontata alla più restrittiva disposizione del codice di rito civile, metterebbe in luce un'irragionevole disparità tra modelli processuali, che andrebbe ricomposta attraverso il superamento del principio di tassatività di cui agli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref>
 3.4.1.- Così argomentando, infatti, il rimettente inverte il rapporto sistematico tra la disciplina del processo civile e quella del processo amministrativo, nel momento in cui eleva quest'ultima a paradigma dell'altra, in aperta contraddizione con quanto indicato dalla <ref href="/akn/it/act/legge/stato/2009-06-18/69/!main">legge 18 giugno 2009, n. 69</ref> (Disposizioni per lo sviluppo economico, la semplificazione, la competitività nonché in materia di processo civile), che, all'art. 44, reca la «delega al Governo per il riassetto della disciplina del processo amministrativo».
 L'<ref href="/akn/it/act/legge/stato/2009/69/!main#art_44__para_1">art. 44, comma 1, della legge n. 69 del 2009</ref> individua tra le finalità della riforma delle norme sul processo amministrativo quella «di coordinarle con le norme del <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main">codice di procedura civile</ref> in quanto espressione di principi generali», sicché è la disciplina del processo amministrativo a sperimentare un percorso di assimilazione alla disciplina di principio del processo civile, e non viceversa, come testimonia, altresì, il rinvio esterno disposto dall'art. 39, comma 1, cod. proc. amm.
 In tal senso, del resto, l'Adunanza plenaria del Consiglio di Stato ha affermato che, nonostante la formulazione apparentemente indeterminata, il disposto dell'art. 105 cod. proc. amm. è in realtà allineato a quello degli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref>, da intendersi, quest'ultimo, espressivo dei principi generali di sostitutività dell'appello e conversione dei vizi di nullità in motivi di gravame, sicché, anche nel processo amministrativo, le ipotesi normative di rimessione al primo giudice sono da qualificarsi come tassative ed eccezionali, insuscettibili di interpretazione analogica o estensiva (sentenze 30 luglio 2018, n. 10 e n. 11).
 Non si registra, quindi, la disparità tra modelli processuali che il giudice a quo censura sotto il profilo dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, bensì, al contrario, una convergenza di modelli processuali, orientata alla tassatività ed eccezionalità delle ipotesi normative di regressione in primo grado. Tale convergenza di principio non impedisce, peraltro, che le esigenze specifiche di ciascun modello possano comportare una diversa articolazione delle relative discipline.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>5.- Quanto detto finora esclude la prospettata violazione dei principi del giusto processo di cui agli <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_111">artt. 111 Cost.</ref> e 6 CEDU, quest'ultimo parametro interposto dall'art. 117, primo comma, Cost.
 Invero, acquisito che il doppio grado di giurisdizione di merito non è, di per sé, assistito da copertura costituzionale (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/199/!main">is, sentenze n. 199 del 2017</ref> e n. 243 del 2014; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2014/42/!main">ordinanze n. 42 del 2014</ref>, n. 226 e n. 190 del 2013), né convenzionale (Corte EDU, sentenza 20 ottobre 2015, Costantino Di Silvio contro Italia, paragrafo 50), questa Corte ha escluso che la mancata previsione della rimessione al giudice di primo grado in un caso di definizione in rito non contemplato dagli <mref><ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">artt. 353</ref> e <ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">354 cod. proc. civ.</ref></mref> (declinatoria di competenza del giudice di pace) integri un pregiudizio del diritto di difesa, atteso che «il diritto di difesa deve ritenersi rispettato quando la causa venga effettivamente sottoposta alla cognizione dei giudici di primo e di secondo grado, restando irrilevante che l'esame del fondamento della domanda non sia compiuto dall'uno, alla stregua di situazioni processuali preclusive, ma soltanto dall'altro» (<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2000/585/!main">ordinanza n. 585 del 2000</ref>).
 3.5.1.- È proprio la finalità di assicurare la ragionevole durata del processo, garantita dagli <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_111">artt. 111 Cost.</ref> e 6 CEDU, ad opporsi, in linea di principio, alla rimessione del giudizio in primo grado, quando questa non sia imposta da esigenze indefettibili, come quella di integrare il contraddittorio rispetto ad una parte necessaria.
 La regressione processuale diretta a consentire l'ingresso in giudizio del garante del convenuto, parte non necessaria, determinerebbe un ritardo, esso pure non necessario, nella definizione della controversia sul rapporto principale.
 4.- La scelta del legislatore di non includere tra le ipotesi di rimessione in primo grado quella della pretermissione dell'istanza del convenuto-opponente di chiamata di un terzo in garanzia è, dunque, un'opzione discrezionale, legittima perché non manifestamente irragionevole, attesa la sua funzionalità al valore costituzionale della ragionevole durata del processo sul rapporto principale, e non ingiustificatamente compressiva del diritto di azione, potendo il convenuto-opponente esercitare la domanda di garanzia tramite l'instaurazione di un autonomo giudizio contro il terzo.
 Deve essere, pertanto, dichiarata non fondata la questione di legittimità costituzionale dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 cod. proc. civ.</ref> in riferimento a tutti i parametri evocati.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara non fondata la questione di legittimità costituzionale dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_354">art. 354 del codice di procedura civile</ref>, sollevata dalla Corte d'appello di Milano, in riferimento agli artt. 3, 24, 111 e 117, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, quest'ultimo in relazione all'art. 6 della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali (CEDU), firmata a Roma il 4 novembre 1950, ratificata e resa esecutiva con <ref href="/akn/it/act/legge/stato/1955-08-04/848/!main">legge 4 agosto 1955, n. 848</ref>, con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-02-12">12 febbraio 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 Stefano PETITTI, Redattore
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-03-26">26 marzo 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
