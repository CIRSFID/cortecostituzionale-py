<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>272</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:272</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/06/1992</data_decisione>
    <data_deposito>12/06/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo CAIANIELLO, prof. Luigi MENGONI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 4  del  regio    &#13;
 decreto-legge  26  settembre  1935,  n. 1952 (Disciplina del servizio    &#13;
 delle guardie particolari giurate), conv. in legge 19 marzo 1936,  n.    &#13;
 508,  4 del regio decreto-legge 12 novembre 1936, n. 2144 (Disciplina    &#13;
 degli istituti di vigilanza privata), conv. in legge 3  aprile  1937,    &#13;
 n.  526, 11, ultimo comma, e 138 del regio decreto 18 giugno 1931, n.    &#13;
 773 (Approvazione del testo unico delle leggi di pubblica sicurezza),    &#13;
 promosso con ordinanza emessa il 4  settembre  1991  dal  Pretore  di    &#13;
 Napoli  -  Sezione  distaccata  di  Pozzuoli, nel procedimento civile    &#13;
 vertente  tra  Luigi  Carpetta  e  Istituto  di   vigilanza   privata    &#13;
 "L'Aquila",   iscritta  al  n.  78  del  registro  ordinanze  1992  e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  10,  prima    &#13;
 serie speciale, dell'anno 1992;                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  20  maggio  1992  il  Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
   Ritenuto  che  con  ordinanza emessa il 4 settembre 1991 (pervenuta    &#13;
 alla Corte costituzionale il 12 febbraio 1992) dal Pretore di  Napoli    &#13;
 -  Sez.  distaccata  di Pozzuoli nel procedimento civile vertente tra    &#13;
 Carpetta Luigi ed Istituto di Vigilanza privata "L'Aquila" (Reg. ord.    &#13;
 n. 78 del 1992), è  stata  sollevata  la  questione  incidentale  di    &#13;
 legittimità  costituzionale  degli  artt.  4  del  regio  decreto 26    &#13;
 settembre 1935,  n.  1952  (Disciplina  del  servizio  delle  guardie    &#13;
 particolari  giurate),  recte  4 del regio decreto-legge 26 settembre    &#13;
 1935, n. 1952, conv. in legge 19 marzo 1936, n. 508; e  4  del  regio    &#13;
 decreto-legge  12  novembre 1936, n. 2144, recte 4 del regio decreto-legge 12 novembre 1936, n. 2144, conv. in legge  3  aprile  1937,  n.    &#13;
 526,  nella  parte in cui non distinguono tra violazioni disciplinari    &#13;
 attinenti alla funzionalità del servizio, e violazioni  disciplinari    &#13;
 relative  agli  aspetti  privatistici  del  rapporto di lavoro, e non    &#13;
 limitano solo alle prime la vigilanza ed i poteri disciplinari  degli    &#13;
 organi  amministrativi (questore e prefetto), in riferimento all'art.    &#13;
 3  della  Costituzione;  nonché   la   questione   di   legittimità    &#13;
 costituzionale  degli  artt. 11, ultimo comma, e 138, t.u.l.p.s., nel    &#13;
 loro combinato disposto, per la parte in cui, non prevedendo  che  il    &#13;
 provvedimento  di  revoca della nomina a guardia giurata debba essere    &#13;
 motivato  in  ordine  agli  elementi  che  hanno  dato   luogo   alla    &#13;
 valutazione  del  venir  meno  del  requisito  della  buona condotta,    &#13;
 impediscono il controllo sulla imparzialità e legalità  dell'azione    &#13;
 amministrativa, in riferimento agli artt. 3 e 97 della Costituzione;     &#13;
      che  in particolare il contrasto con l'art. 3 della Costituzione    &#13;
 viene evidenziato "rispetto  agli  altri  lavoratori  subordinati,  i    &#13;
 quali  possono  giovarsi dell'ordinario regime garantistico stabilito    &#13;
 per gli illeciti disciplinari";                                          &#13;
      che è intervenuto in giudizio per il Presidente  del  Consiglio    &#13;
 dei  ministri,  l'Avvocatura generale dello Stato che ha concluso per    &#13;
 l'inammissibilità e l'infondatezza della questione.                     &#13;
    Considerato che, con riguardo alla seconda questione sollevata, è    &#13;
 privo di  fondamento  il  presupposto  tenuto  presente  dal  giudice    &#13;
 rimettente  circa  il non obbligo di motivazione del provvedimento di    &#13;
 revoca, motivazione  necessaria  invece  per  consentire  al  giudice    &#13;
 amministrativo  la  verifica  della  legittimità  del  provvedimento    &#13;
 stesso (cfr. ad es. Cons. St. 30 marzo 1987 n. 186) -  principio  del    &#13;
 resto  riaffermato in generale dall'art. 3 della legge 7 agosto 1990,    &#13;
 n. 241;                                                                  &#13;
      che, con riferimento alla prima questione, la  Corte  rileva  la    &#13;
 palese disomogeneità delle situazioni poste a confronto, giacché la    &#13;
 categoria delle guardie giurate, diversamente da quella relativa agli    &#13;
 altri  lavoratori,  è  caratterizzata  da  indubbie  connotazioni di    &#13;
 carattere pubblicistico che impongono  una  peculiare  disciplina  in    &#13;
 ordine  alla  verifica, da parte dell'autorità amministrativa, della    &#13;
 sussistenza  dei  presupposti  che  consentono  l'esercizio  di  tale    &#13;
 attività lavorativa;                                                    &#13;
      che va aggiunto altresì che il corretto uso del potere concesso    &#13;
 all'autorità  amministrativa è, comunque, soggetto al sindacato del    &#13;
 giudice amministrativo avanti al quale il singolo può ricorrere  per    &#13;
 la tutela delle proprie ragioni;                                         &#13;
      che,    pertanto,   le   suesposte   questioni   si   appalesano    &#13;
 manifestamente infondate;                                                &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza delle questioni di legittimità    &#13;
 costituzionale degli artt. 4 del  regio  decreto-legge  26  settembre    &#13;
 1935,  n.  1952  (Disciplina  del  servizio delle guardie particolari    &#13;
 giurate), conv. in legge 19 marzo 1936, n. 508, 4 del regio  decreto-legge  12  novembre  1936,  n.  2144  (Disciplina  degli  istituti di    &#13;
 vigilanza privata), conv. in legge 3 aprile 1937, n. 526, 11,  ultimo    &#13;
 comma,  e  138 del regio decreto 18 giugno 1931, n. 773 (Approvazione    &#13;
 del testo unico delle leggi di pubblica  sicurezza),  in  riferimento    &#13;
 agli artt. 3 e 97 della Costituzione, sollevate dal Pretore di Napoli    &#13;
 con l'ordinanza in epigrafe.                                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte Costituzionale,    &#13;
 Palazzo della Consulta, il 3 giugno 1992.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: BORZELLINO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 12 giugno 1992.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
