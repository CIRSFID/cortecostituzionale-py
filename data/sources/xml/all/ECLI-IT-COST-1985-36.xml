<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>36</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:36</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Giuseppe Ferrari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/02/1985</data_decisione>
    <data_deposito>13/02/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MAEAGUGINI - &#13;
 Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. &#13;
 GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. &#13;
 GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale della legge 8 gennaio  &#13;
 1979, n. 2 (Interpretazione autentica dell'art. 8 della legge 26 maggio  &#13;
 1965, n.   590, con le modificazioni ed  integrazioni  della  legge  14  &#13;
 agosto  1971,  n.  817) promosso con ordinanza emessa il 20 giugno 1980  &#13;
 dal Pretore di Montagnana nel procedimento civile vertente tra Eredi di  &#13;
 Sofonisbi Ovidio e Ferrari Maria.  iscritta  al  n.  650  del  registro  &#13;
 ordinanze  1980  e pubblicata nella Gazzetta Ufficiale della Repubblica  &#13;
 n. 311 del 1980.                                                         &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  27  novembre  1984  il  Giudice  &#13;
 relatore Giuseppe Ferrari;                                               &#13;
     udito l'Avvocato dello Stato Giuseppe Del Greco per  il  Presidente  &#13;
 del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con  ordinanza  emessa  il  20  giugno  1980  il  Pretore di  &#13;
 Montagnana  ha  sollevato  questione  di  legittimità   costituzionale  &#13;
 dell'articolo  unico  della  legge 8 gennaio 1979, n. 2, in riferimento  &#13;
 agli artt.  2, 4, 25, 41 e 42 Cost..                                     &#13;
     Adito in una controversia civile promossa dal titolare del  diritto  &#13;
 di  prelazione  nell'acquisto di un fondo agricolo, il quale intendeva,  &#13;
 ai sensi dell'art.  8, quinto comma, della legge  26  maggio  1965,  n.  &#13;
 590,   riscattare   dal  terzo  acquirente  il  fondo  a  sua  insaputa  &#13;
 vendutogli, il giudice a quo osserva che, in  difetto  di  un'esplicita  &#13;
 previsione  normativa circa il termine entro il quale il prezzo avrebbe  &#13;
 dovuto, in tale ipotesi, rimborsarsi al terzo, la giurisprudenza aveva,  &#13;
 dopo talune iniziali incertezze, definitivamente chiarito (Cass.,  sez.  &#13;
 un. civ., 16 ottobre 1976, n. 3498) che il pagamento doveva effettuarsi  &#13;
 entro  tre  mesi  dalla  data  della  manifestazione  di  volontà  del  &#13;
 riscattante  di voler acquisire il fondo; e ciò in analogia con quanto  &#13;
 stabilito dal sesto comma dello stesso art. 8 che, per il caso  in  cui  &#13;
 sia stato esercitato il diritto di prelazione, impone che il versamento  &#13;
 del  prezzo  avvenga  appunto  entro  quel  termine.  La  ratio di tale  &#13;
 interpretazione  -  continua  l'ordinanza  -  risiedeva   evidentemente  &#13;
 nell'esigenza  di  tutelare  l'interesse del terzo acquirente a vedersi  &#13;
 rimborsare  in  tempi  brevi  il  prezzo  erogato  per  il  fondo   poi  &#13;
 retrattato,  ed in quella correlativa di evitare possibili speculazioni  &#13;
 da parte del riscattante.                                                &#13;
     In tale contesto ermeneutico è sopraggiunta la legge n. 2 del 1979  &#13;
 la quale, sotto il titolo "interpretazione autentica dell'art. 8  della  &#13;
 legge 26 maggio 1965, n. 590, con le modificazioni e integrazioni della  &#13;
 legge  14  agosto 1971, n. 817", nel suo articolo unico da un canto, al  &#13;
 primo comma, stabilisce che "la disciplina relativa al  versamento  del  &#13;
 prezzo  di acquisto, prevista dal sesto e dal settimo comma dell'art. 8  &#13;
 della legge 26 maggio 1965, n. 590, modificato dalla  legge  14  agosto  &#13;
 1971,  n. 817, si intende riferita anche ai casi di cui al quinto comma  &#13;
 dello stesso  articolo";  dall'altro,  al  secondo  comma,  recita:  "i  &#13;
 termini  decorrono  dalla comunicazione scritta dell'adesione del terzo  &#13;
 acquirente,  o  di  successivo  avente  causa,  alla  dichiarazione  di  &#13;
 riscatto,  oppure,  ove sorga contestazione, dal passaggio in giudicato  &#13;
 della sentenza che riconosce  il  diritto";  al  terzo  comma,  infine,  &#13;
 statuisce  che "la presente legge costituisce interpretazione autentica  &#13;
 della legge 26 maggio 1965, n. 590". Orbene, continua il giudice a quo,  &#13;
 dal  valore  retroattivo  connesso   alla   qualificazione   di   legge  &#13;
 interpretativa consegue che la disciplina sui termini si applichi anche  &#13;
 ai giudizi in corso, e quindi anche a quello de quo, nel quale l'attore  &#13;
 aveva fatto offerta reale del prezzo solo il 6 maggio 1975, in corso di  &#13;
 causa,  dopo  oltre  due  anni dalla notifica dell'atto di citazione (6  &#13;
 marzo 1973) con il quale si chiedeva l'attuazione dell'addotto  diritto  &#13;
 di riscatto.                                                             &#13;
     2. - Gli addotti profili di incostituzionalità concernono:          &#13;
     a)  il  valore  retroattivo  surrettiziamente  conferito alla legge  &#13;
 attraverso lo strumento della qualificazione della stessa come legge di  &#13;
 interpretazione autentica, mentre la relativa disciplina  si  presenta,  &#13;
 invece,  come  assolutamente  innovativa  rispetto  alla precedente per  &#13;
 quanto concerne il decorso del termine dal passaggio in giudicato della  &#13;
 sentenza che riconosce il diritto, in caso di controversia  giudiziale.  &#13;
 Ne risulterebbero violati, ad avviso del giudice a quo, "gli artt. 11 e  &#13;
 12 delle, 'disposizioni sulla legge in generale', preliminari al codice  &#13;
 civile" e "lo stesso art. 25, secondo comma, della Costituzione" il cui  &#13;
 ambito  applicativo  non  potrebbe ritenersi limitato alla sola materia  &#13;
 penale;                                                                  &#13;
     b) la disparità di trattamento fra riscattante e terzo  acquirente  &#13;
 nel  senso  che  la  tutela  del primo (pur, in sé, costituzionalmente  &#13;
 corretta) comporterebbe un sacrificio  eccessivo  della  posizione  del  &#13;
 secondo,  al  quale  nessun  onere  di comunicazione è preventivamente  &#13;
 imposto e che, tuttavia, può vedersi rimborsare il prezzo a suo  tempo  &#13;
 corrisposto con moneta svalutata, senza interessi, magari procurata dal  &#13;
 riscattante  negli  anni necessari alla definitiva soluzione giudiziale  &#13;
 della controversia, in ipotesi instaurata sulla  base  di  una  pretesa  &#13;
 assolutamente   infondata.  La  norma  avrebbe,  quindi,  ignorato  "il  &#13;
 principio della parità dei cittadini davanti alla legge e il principio  &#13;
 della libertà della proprietà e del libero  commercio  dei  beni"  in  &#13;
 violazione degli artt. 2 (rectius 3), 4, 41 e 42 Cost..                  &#13;
     3.  -  È  intervenuto nel giudizio il Presidente del Consiglio dei  &#13;
 ministri, rappresentato dall'Avvocatura generale dello Stato,  instando  &#13;
 per  la  declaratoria  di  infondatezza  della  sollevata  questione di  &#13;
 legittimità costituzionale.                                             &#13;
     In atto  d'intervento  si  rileva  anzitutto  che  l'ordinanza  non  &#13;
 chiarisce  quale  sia  l'addotta violazione dell'art. 4 Cost., comunque  &#13;
 affermandosi  che  il  diritto  di  riscatto   accordato   al   diretto  &#13;
 coltivatore  del  fondo  certamente non lede quello al lavoro del terzo  &#13;
 acquirente.                                                              &#13;
     Del pari - si afferma - non è spiegata e non s'intende la  pretesa  &#13;
 violazione  degli  artt.  41  e 42 Cost., essendo la proprietà privata  &#13;
 costituzionalmente tutelata con l'ovvio limite della  funzione  sociale  &#13;
 alla quale deve in primo luogo rispondere; finalità assicurata proprio  &#13;
 dalle leggi del 1965 e del 1979 sulla proprietà coltivatrice.           &#13;
     Quanto alla prospettata violazione dell'art. 25 Cost., premesso che  &#13;
 l'irretroattività  è  sancita  solo per le leggi penali, l'Avvocatura  &#13;
 nega  che  la   legge   impugnata   non   abbia   carattere   meramente  &#13;
 interpretativo della legge n. 590 del 1965. Essa, infatti, appare priva  &#13;
 di  autonomia  ed  ha  senso  solo  se  coordinata  con le preesistenti  &#13;
 disposizioni dell'art. 8 della legge del 1965, delle quali chiarisce il  &#13;
 significato e  l'ambito  di  efficacia  in  ordine  alle  modalità  di  &#13;
 esercizio  del riscatto agrario, eliminando dubbi che avevano provocato  &#13;
 contrasti giurisprudenziali riconosciuti dallo stesso giudice a quo.     &#13;
     In ordine alla prospettata violazione dell'art. 3 Cost. si osserva,  &#13;
 infine, che il bilanciamento degli interessi in conflitto è  riservato  &#13;
 alla  discrezionalità del legislatore. E che tale discrezionalità sia  &#13;
 stata  nella   specie   ragionevolmente   esercitata   discende   dalla  &#13;
 considerazione che la necessità di attendere il passaggio in giudicato  &#13;
 della  sentenza  che  riconosce  il  diritto  di  riscatto, ai fini del  &#13;
 decorso  del  termine  per  il  versamento  del  prezzo  da  parte  del  &#13;
 riscattante,  non  è che una conseguenza connessa al comportamento del  &#13;
 riscattato,  che  ingiustamente  si  sia  opposto  alla   pretesa   del  &#13;
 riscattante,  secondo  quanto  reiteratamente  ritenuto  dalla Corte di  &#13;
 Cassazione, che ha sempre escluso qualsiasi sospetto di  illegittimità  &#13;
 costituzionale della norma impugnata (Cass. civ., 18 settembre 1979, n.  &#13;
 4801;  20 settembre 1979, n. 4833; 13 novembre 1979, n.  5900; 3 aprile  &#13;
 1980, n. 2207).<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  L'articolo  unico  della  legge  8  gennaio   1979,   n.   2  &#13;
 ("interpretazione  autentica dell'art. 8 della legge 26 maggio 1965, n.  &#13;
 590, con le modificazioni ed integrazioni della legge 14  agosto  1971,  &#13;
 n.  817")  si  distribuisce  in  tre  commi: i primi due attengono alla  &#13;
 disciplina dell'esercizio del diritto di riscatto dei fondi  rustici  -  &#13;
 più  propriamente,  alla  decorrenza del termine per il versamento del  &#13;
 prezzo  -,  il  terzo   dichiara   esplicitamente   il   carattere   di  &#13;
 interpretazione  autentica  della  legge.  Ora,  secondo  il Pretore di  &#13;
 Montagnana, questa sarebbe costituzionalmente illegittima, non tanto  e  &#13;
 solo  perché,  pur  proclamandosi meramente interpretativa, in realtà  &#13;
 sarebbe innovativa, stante la introduzione di un termine  che  non  era  &#13;
 previsto  dalla  legge asseritamente interpretata, quanto e soprattutto  &#13;
 perché comporterebbe l'applicabilità di tale termine anche ai giudizi  &#13;
 in corso ed un trattamento di sfavore per il terzo acquirente in  buona  &#13;
 fede,   che  per  di  più  è  del  tutto  estraneo  al  rapporto  fra  &#13;
 proprietario  e  titolare del diritto di riscatto. Da ciò, la denuncia  &#13;
 della legge in riferimento agli artt. 2, 4, 25, 41 e 42 Cost..           &#13;
     2. - La questione, come prospettata nell'ordinanza in  esame,  deve  &#13;
 dichiararsi non fondata.                                                 &#13;
     Ed invero, a parte il rilievo che i vizi denunciati, eccezion fatta  &#13;
 per   la  censura  formulata  in  riferimento  all'art.  25  Cost.,  si  &#13;
 contengono,  prima  ancora  che  nella  legge   de   qua,   in   quella  &#13;
 interpretata,  non  è  agevole  comprendere  -  la  doglianza risulta,  &#13;
 infatti, sul punto del tutto immotivata - come possa ritenersi  violato  &#13;
 l'art.  4  Cost., cioè il principio del diritto al lavoro, ad opera di  &#13;
 una  norma  la  quale  si  propone  di  assicurare  al  coltivatore  la  &#13;
 possibilità  di  lavorare  in  proprio  il fondo che già lavorava per  &#13;
 conto del proprietario.   Né può  dirsi  che  attingano  dignità  di  &#13;
 motivazione   i   richiami,  generici  e  fuggevoli,  che,  a  sostegno  &#13;
 dell'asserito  contrasto  con  gli  artt.  41  e  42  Cost.,  risultano  &#13;
 nell'ordinanza  fatti  ai  principi "della libertà della proprietà" e  &#13;
 "del libero commercio dei beni".                                         &#13;
     In quanto, poi, alla  sollecitazione  a  non  "dimenticare  che  il  &#13;
 danneggiato  non  è  il  venditore,  cui  solo  incombe  l'onere della  &#13;
 comunicazione della vendita, ma l'acquirente, che  nessun  rapporto  ha  &#13;
 mai avuto col fittavolo" e che, in conseguenza del rinvio del pagamento  &#13;
 del  prezzo del riscatto, potrebbe subire anche l'eventuale danno della  &#13;
 svalutazione  monetaria  -  per  cui  si  creerebbe  "una   illegittima  &#13;
 distinzione tra due categorie di cittadini, che hanno invece il diritto  &#13;
 di essere ugualmente tutelati" - è di piena evidenza, in rapporto alla  &#13;
 motivazione  offerta, che solo per disattenzione il riferimento risulta  &#13;
 fatto all'art. 2, anziché all'art. 3 Cost..  Ma,  chiariti  i  termini  &#13;
 dell'impugnativa  nel  senso  di  cui sopra, va subito detto che non si  &#13;
 configura nella specie la lamentata  violazione  del  "principio  della  &#13;
 parità   dei   cittadini  avanti  alla  legge":    in  una  situazione  &#13;
 dilemmatica,  quale  quella  prospettata  dal  giudice  a   quo,   sono  &#13;
 tutt'altro  che  irrazionali  l'opinione  che sia il fittavolo la parte  &#13;
 più debole e la conseguente opzione per esso, anziché  per  il  terzo  &#13;
 acquirente;  non  costituisce  anomalia nel nostro sistema il fatto che  &#13;
 anche a riguardo del caso di specie "il giudice di merito  (non)  possa  &#13;
 modificare  la situazione economica nel frattempo creatasi" per effetto  &#13;
 della svalutazione monetaria; la decorrenza del termine  dal  passaggio  &#13;
 in  giudicato  della  sentenza  che  definisce  il giudizio - del resto  &#13;
 disposta anche per il riscatto degli immobili urbani dall'art.  39,  u.  &#13;
 comma,  legge  27  luglio  1978, n. 392 ("disciplina delle locazioni di  &#13;
 immobili urbani")  -  è  corretta  applicazione  del  principio  della  &#13;
 certezza del diritto e dei rapporti giuridici.                           &#13;
     3.  -  Residua  così    solo  la  doglianza,  secondo  cui "con la  &#13;
 definizione di interpretazione autentica dell'art. 8, legge  26  maggio  &#13;
 1965,  n. 590..., la legge n. 279 viene ad avere necessariamente valore  &#13;
 retroattivo, applicabile pertanto anche ai giudizi tuttora pendenti,  e  &#13;
 quindi  anche  al  presente".  Come  appare  con  tutta chiarezza dalla  &#13;
 trascritta prospettazione, in sostanza il giudice a quo lamenta che  la  &#13;
 legge    impugnata   venga   fatta   retroagire   contravvenendo   alle  &#13;
 "disposizioni sulla legge in generale" premesse al codice civile (artt.  &#13;
 11 e 12) a sensi delle quali "la legge non dispone che per l'avvenire".  &#13;
 Essendo, quindi, la retroattività il preciso oggetto  della  questione  &#13;
 sollevata,  è  indifferente che il legislatore disponga l'operatività  &#13;
 di una legge anche per il passato, anziché mediante un'apposita norma,  &#13;
 mediante   un   diverso   strumento,   qual   è,  come  nella  specie,  &#13;
 l'autodefinizione di interpretazione autentica. Se  così    è,  perde  &#13;
 ogni   rilievo,   ai  fini  del  decidere  la  presente  questione,  la  &#13;
 problematica relativa alla categoria  delle  leggi  di  interpretazione  &#13;
 autentica  e,  conseguentemente,  il  solo nodo che in proposito questa  &#13;
 Corte deve sciogliere è se la retroattività di che trattasi, comunque  &#13;
 disposta, confligga con l'art.   25 Cost.. Ma a  torto  il  Pretore  di  &#13;
 Montagnana  fa richiamo al suddetto articolo. Questo - per l'esattezza,  &#13;
 il  secondo  comma  -  vieta   espressamente   ed   esclusivamente   la  &#13;
 retroattività   della  legge  penale.  Inoltre,  a  prescindere  dalla  &#13;
 questione, già esaminata con la sentenza n. 187 del 1981, ma  che  qui  &#13;
 non  interessa,  se  tale  divieto  debba ritenersi limitato al diritto  &#13;
 penale sostantivo o esteso anche a quello  processuale,  questa  Corte,  &#13;
 già  con  sentenza  n.  118  del  1957,  ha  insegnato che "nel nostro  &#13;
 ordinamento  il  principio  della  irretroattività  della  legge   non  &#13;
 assurge,  nella sua assolutezza, a precetto costituzionale", precisando  &#13;
 tuttavia (anche con la sentenza n. 81 del 1958) non doversi  "escludere  &#13;
 che  in  singole materie, anche fuori di quella penale, l'emanazione di  &#13;
 una  legge  retroattiva  possa  rivelarsi  in  contrasto  con   qualche  &#13;
 specifico  precetto  costituzionale".  E  poiché  nel  caso  di specie  &#13;
 l'impugnata legge non viola, come più sopra si è  visto,  alcuno  dei  &#13;
 principi  costituzionali  invocati  nell'ordinanza de qua, la questione  &#13;
 deve dirsi infondata anche in riferimento all'art. 25 Cost..</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'articolo  unico  della legge 8 gennaio 1979, n. 2 (interpretazione  &#13;
 autentica dell'art. 8 della legge  26  maggio  1965,  n.  590,  con  le  &#13;
 modificazioni  ed  integrazioni  della  legge  14  agosto 1971, n. 817)  &#13;
 sollevata,  in  riferimento  agli  artt.  3,  4,  25,  41  e  42  della  &#13;
 Costituzione  dal Pretore di Montagnana con ordinanza in data 20 giugno  &#13;
 1980 (reg. ord. n. 650 del 1980).                                        &#13;
     Così   deciso in Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 7 febbraio 1985.                              &#13;
                                   F.to:  LEOPOLDO ELIA - ORONZO REALE -  &#13;
                                   BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
