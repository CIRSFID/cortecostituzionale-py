<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>496</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:496</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/12/1991</data_decisione>
    <data_deposito>27/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  20, comma    &#13;
 quarto, del regio decreto 16 luglio 1905  n.  646  (Approvazione  del    &#13;
 testo unico delle leggi sul credito fondiario) promosso con ordinanza    &#13;
 emessa  il  9 marzo 1991 dal Giudice dell'esecuzione del Tribunale di    &#13;
 Vigevano nel ricorso proposto  da  Garelli  Francesco  ed  altri  nei    &#13;
 confronti dell'Istituto Bancario S. Paolo di Torino ed altra iscritta    &#13;
 al  n.  318  del  registro ordinanze 1991 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 21,  prima  serie  speciale,  dell'anno    &#13;
 1991;                                                                    &#13;
    Visti  gli  atti  di  costituzione di Garelli Francesco ed altri e    &#13;
 dell'Istituto  Bancario  S.  Paolo  di  Torino  nonché   l'atto   di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio del 6 novembre 1991 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto che il giudice dell'esecuzione del Tribunale di Vigevano,    &#13;
 con ordinanza in  data  9  marzo  1991,  ha  sollevato  questione  di    &#13;
 legittimità  costituzionale  dell'art.  20,  quarto comma, del regio    &#13;
 decreto 16 luglio 1905, n. 646 (Approvazione del  testo  unico  delle    &#13;
 leggi sul credito fondiario);                                            &#13;
      che  la  norma  impugnata  viene  censurata  nella parte in cui,    &#13;
 consentendo all'istituto di credito mutuante di  promuovere  l'azione    &#13;
 esecutiva nei confronti dell'originario debitore, ancorché deceduto,    &#13;
 qualora  gli  eredi non abbiano provveduto alla notifica (allo stesso    &#13;
 istituto fondiario) del loro subentro nel possesso  o  godimento  del    &#13;
 fondo,  si  porrebbe in contrasto con l'art. 6.3.a) della Convenzione    &#13;
 europea dei diritti dell'uomo (resa esecutiva con legge 4 agosto 1955    &#13;
 n. 848),  secondo  cui,  "ciascun  convenuto  ha  diritto  ad  essere    &#13;
 informato  in maniera dettagliata, in una lingua a lui comprensibile,    &#13;
 della natura e dei motivi dell'azione portata a  suo  carico",  così    &#13;
 violando  l'art.  10,  primo  comma,  della  Costituzione, che impone    &#13;
 all'ordinamento giuridico italiano  di  conformarsi  alle  norme  del    &#13;
 diritto internazionale generalmente riconosciute;                        &#13;
      che  gli eredi del debitore originario, opponenti nel giudizio a    &#13;
 quo, si sono costituiti osservando che le ragioni per le quali questa    &#13;
 Corte ha già  escluso  che  la  norma  impugnata  possa  determinare    &#13;
 un'ingiustificata  disparità  di  trattamento con le altre procedure    &#13;
 esecutive (art. 3 della Costituzione) o possa ledere  il  diritto  di    &#13;
 difesa  dei  successori  (art.  24  della Costituzione) non sarebbero    &#13;
 invocabili allorquando, come nella  specie,  risulti  documentalmente    &#13;
 provato  che  l'istituto  di credito era sicuramente a conoscenza del    &#13;
 decesso del debitore originario e dell'identità dei suoi eredi;         &#13;
      che in questo caso, infatti, imporre l'onere della  notifica  in    &#13;
 mancanza  della  quale  il  processo  esecutivo può essere intentato    &#13;
 contro  l'originario  debitore  defunto,   risulterebbe   del   tutto    &#13;
 irragionevole ed irrazionale;                                            &#13;
      che,  la  questione  andrebbe pertanto esaminata da questa Corte    &#13;
 anche sotto tale profilo, poiché l'eccezione di illegittimità della    &#13;
 norma - da loro sollevata  con  esclusivo  riferimento  al  parametro    &#13;
 della  ragionevolezza  intrinseca  (art.  3  della Costituzione) - è    &#13;
 stata ritenuta, nella fattispecie,  irrilevante  dal  giudice  a  quo    &#13;
 nell'inesatto  presupposto  che  l'istituto  procedente  non fosse al    &#13;
 corrente della situazione successoria;                                   &#13;
      che si è costituito l'istituto Bancario  San  Paolo  di  Torino    &#13;
 deducendo  che  le  ragioni  per  le quali si è già ritenuto che la    &#13;
 disposizione denunciata non violi l'art. 24  della  Costituzione  non    &#13;
 potrebbero  non  valere anche in riferimento al prospettato contrasto    &#13;
 con l'art. 6.3.a) della Convenzione dei diritti dell'uomo, attesa  la    &#13;
 sostanziale  analogia  tra  la norma costituzionale e la disposizione    &#13;
 della convenzione;                                                       &#13;
      che il predetto istituto  ha  poi  osservato  che  la  questione    &#13;
 risulterebbe  comunque  infondata,  in  quanto,  secondo  la costante    &#13;
 giurisprudenza di questa Corte, il rinvio di cui  all'art.  10  della    &#13;
 Costituzione  non  riguarda  le  norme  pattizie  ma  solo il diritto    &#13;
 consuetudinario;                                                         &#13;
      che è intervenuta l'Avvocatura generale dello Stato  la  quale,    &#13;
 osservando   che  l'adeguamento  automatico  alle  norme  di  diritto    &#13;
 internazionale generalmente riconosciute si riferisce  soltanto  alle    &#13;
 norme  a  carattere  consuetudinario  e  non  anche a quelle di fonte    &#13;
 pattizia, ha chiesto che la questione venga dichiarata manifestamente    &#13;
 infondata.                                                               &#13;
    Considerato che, secondo la consolidata giurisprudenza  di  questa    &#13;
 Corte,  i  termini  della  questione  di  legittimità costituzionale    &#13;
 sollevata in via incidentale sono quelli fissati dal  giudice  a  quo    &#13;
 nell'ordinanza  di  rimessione  e deve quindi escludersi che le parti    &#13;
 possano estendere o modificarne il contenuto o i profili;                &#13;
      che,  pertanto,  la  questione  non  può  essere  esaminata  in    &#13;
 relazione   all'ipotesi   che   l'istituto  procedente  risulti  -  a    &#13;
 prescindere dalla notifica dovuta dagli eredi - comunque a conoscenza    &#13;
 del subentro di quest'ultimi nel  possesso  del  fondo,  e  ciò  sia    &#13;
 perché  la  violazione  del parametro costituzionale invocato non è    &#13;
 stata dedotta in questi termini, sia perché  la  relativa  questione    &#13;
 sollevata  sotto  tale profilo dalle parti, in riferimento all'art. 3    &#13;
 della Costituzione, è stata motivatamente ritenuta dal giudice a quo    &#13;
 priva di rilevanza;                                                      &#13;
      che, peraltro, rispetto alla questione, così  come  prospettata    &#13;
 nell'ordinanza   di  rimessione,  appare  assorbente  su  ogni  altra    &#13;
 considerazione   (attinente   alla   riferibilità   o   meno   della    &#13;
 disposizione  di  raffronto  ai  procedimenti  di  carattere  civile,    &#13;
 ovvero,  alla  sua  sostanziale  analogia   con   l'art.   24   della    &#13;
 Costituzione,  in  relazione  al quale qualsiasi violazione, da parte    &#13;
 della norma impugnata, è stata già esclusa) il rilievo che, in base    &#13;
 alla costante giurisprudenza di questa Corte, le norme internazionali    &#13;
 pattizie, quale l'invocato art. 6,  paragrafo  3,  lettera  a,  della    &#13;
 Convenzione europea dei diritti dell'uomo, fuoriescono dall'ambito di    &#13;
 operatività  dell'art.  10  della  Costituzione  che  può  avere ad    &#13;
 oggetto soltanto norme di carattere consuetudinario  (sentt.  nn.  32    &#13;
 del  1960, 69 del 1976, 48 del 1979, 96 del 1982, 323 e 364 del 1989,    &#13;
 e in particolare, 104 del 1969, 188 del 1980, 91 del  1986,  153  del    &#13;
 1987, 315 del 1990);                                                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 20, quarto comma, regio  decreto  16  luglio    &#13;
 1905,  n.  646  (Approvazione del testo unico delle leggi sul credito    &#13;
 fondiario), sollevata in riferimento all'art. 10, primo comma,  della    &#13;
 Costituzione,  dal  giudice  dell'esecuzione  presso  il Tribunale di    &#13;
 Vigevano, con l'ordinanza indicata in epigrafe.                          &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 dicembre 1991.                             &#13;
                       Il presidente: CORASANITI                          &#13;
                       Il redattore: CAIANIELLO                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 dicembre 1991.                        &#13;
                 Il direttore di cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
