<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>291</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:291</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Oronzo Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1984</data_decisione>
    <data_deposito>19/12/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI &#13;
 - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 8, lett. b,  &#13;
 del d.P.R. 30 maggio 1955, n. 797 (t.u.  delle  norme  concernenti  gli  &#13;
 assegni  familiari), promosso con ordinanza emessa il 17 marzo 1977 dal  &#13;
 Pretore di Ancona sul ricorso  proposto  da  Antonini  Patrizio  contro  &#13;
 l'INPS,  iscritta  al  n.  473 del registro ordinanze 1977 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 334 dell'anno 1977.         &#13;
     Visto l'atto di costituzione dell'INPS;                              &#13;
     udito nell'udienza pubblica del 29 maggio 1984 il Giudice  relatore  &#13;
 Oronzo Reale;                                                            &#13;
     udito l'avv.  Giacomo Giordano per l'INPS.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di  una causa di lavoro avente ad oggetto la richiesta,  &#13;
 avanzata da Patrizio Antonini, nei confronti dell'INPS, di ottenere  la  &#13;
 corresponsione  degli  assegni  familiari  per  il  proprio nonno a suo  &#13;
 carico, vivente peraltro il genitore dell'istante che aveva abbandonato  &#13;
 la famiglia e senza che per  esso  l'Antonini  percepisse  gli  assegni  &#13;
 familiari, il pretore di Ancona, con ordinanza datata 17 marzo 1977 (n.  &#13;
 473 del reg. ord.  1977), sollevava - con riferimento agli artt. 3 e 38  &#13;
 della    Costituzione   -   questione   incidentale   di   legittimità  &#13;
 costituzionale dell'art. 8, lett. b, del d.P.R. 30 maggio 1955, n.  797  &#13;
 "Testo  unico  delle  norme  concernenti  gli assegni familiari", nella  &#13;
 parte in cui detta norma  subordina  la  corresponsione  degli  assegni  &#13;
 familiari a favore degli ascendenti in linea diretta al godimento degli  &#13;
 assegni  a  favore  del genitore da essi  discendente ovvero alla morte  &#13;
 dello stesso, escludendo l'ipotesi dell'abbandono.                       &#13;
     A sostegno del dedotto dubbio di illegittimità costituzionale,  il  &#13;
 giudice a quo pone in risalto che:                                       &#13;
     -  gli  assegni familiari, ex art. 10 del citato d.P.R. n.  797 del  &#13;
 1955, spettano al lavoratore per l'ascendente quando  il  genitore  del  &#13;
 lavoratore  è  ricoverato  in casa di cura o di  assistenza mentre non  &#13;
 spetta ove detto genitore abbia abbandonato la famiglia;                 &#13;
     -  gli  assegni  familiari,  ex art. 8, lett. b, del citato d.P.R.,  &#13;
 spettano al lavoratore per l'ascendente ove i  discendenti  di  questi,  &#13;
 genitori  del  lavoratore  stesso, siano deceduti e non, invece, quando  &#13;
 abbiano abbandonato la famiglia;                                         &#13;
     - gli assegni familiari, ex art. 3 del d.P.R.  più  volte  citato,  &#13;
 spettano per i nipoti ex-filio senza particolari condizioni concernenti  &#13;
 la  posizione  dei  genitori dei nipoti stessi, essendo sufficiente che  &#13;
 questi siano abbandonati dal padre.                                      &#13;
     Ad avviso del  pretore  di  Ancona,  le  surricordate  disposizioni  &#13;
 regolerebbero  in maniera diversa situazioni che presenterebbero invece  &#13;
 connotati analoghi e sarebbero perciò da trattare in maniera conforme.  &#13;
 Donde  la  violazione  dell'art.   3   nonché   dell'art.   38   della  &#13;
 Costituzione,  in  quanto  verrebbe  leso  il  "diritto alla assistenza  &#13;
 sociale per chi sia sprovvisto dei mezzi necessari per vivere".          &#13;
     Si è costituito l'INPS, parte nel giudizio a quo, chiedendo che le  &#13;
 proposte questioni vengano dichiarate non fondate.                       &#13;
     Negando la violazione dell'art. 38 della  Costituzione,  la  difesa  &#13;
 INPS  ricorda  la  giurisprudenza della Corte, secondo cui non tutte le  &#13;
 situazioni  di  bisogno,  pur  meritevoli  di  tutela,  devono  trovare  &#13;
 soddisfazione  nell'attuale  sistema  legislativo,  che ben può subire  &#13;
 modifiche,  nel  senso   dell'ampliamento,   attraverso   l'opera   del  &#13;
 legislatore,  cui  spetta  il  compito  di  graduare  nel  tempo i suoi  &#13;
 interventi, in considerazione dei bisogni emergenti,  della  situazione  &#13;
 dei   soggetti,   ma  anche  in  relazione  al  reperimento  dei  mezzi  &#13;
 finanziari.                                                              &#13;
     Ad avviso dell'INPS non  sussisterebbe  neppure  la  disparità  di  &#13;
 trattamento  lamentata  dal  giudice  a  quo  tra le situazioni poste a  &#13;
 raffronto.  Il riferimento all'art. 10 del d.P.R. n. 797 del  1955  non  &#13;
 appare  pertinente,  poiché nel caso di ricovero in istituto di cura o  &#13;
 di assistenza, l'assegno spetta solo per le persone  per  le  quali  è  &#13;
 prevista   la   corresponsione   dell'assegno  e  quando  esse  debbano  &#13;
 corrispondere per il ricovero una retta non  inferiore  all'assegno  di  &#13;
 cui godono.                                                              &#13;
     L'INPS   nega   poi  che  il  decesso  sia  concetto  o  situazione  &#13;
 equiparabile  all'abbandono,  donde  la  legittimità  del   differente  &#13;
 trattamento normativo delle due ipotesi.                                 &#13;
     La  difesa  dell'INPS  infine  contesta  la  equiparabilità, quoad  &#13;
 requisiti  per  la  corresponsione  degli  assegni  familiari,  tra  la  &#13;
 posizione  dei  nipoti  ex-filio  e quella del nonno del lavoratore, in  &#13;
 quanto  sarebbe  palese  la  ragione  del   trattamento   differenziato  &#13;
 stabilito  dal  legislatore, dato che la sostanziale difformità tra le  &#13;
 due situazioni emergerebbe sotto molteplici aspetti (familiare, sociale  &#13;
 ed economico).<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La situazione di fatto in ordine  alla  quale  il  pretore  di  &#13;
 Ancona solleva questione di legittimità costituzionale è la seguente:  &#13;
 il Patrizio Antonini, abbandonato fin  dall'infanzia dal padre emigrato  &#13;
 in  Francia  tanto  che il nonno, del quale era vissuto a carico, aveva  &#13;
 percepito gli assegni per esso Antonini, a sua volta,  vivendo  ora  il  &#13;
 nonno  a  carico  di  lui,  chiedeva  che l'INPS gli corrispondesse gli  &#13;
 assegni per il nonno medesimo a norma dell'art. 8 del d.P.R. 30  maggio  &#13;
 1955,  n.  797. L'INPS negava di dovere gli assegni perché il genitore  &#13;
 dell'istante non era morto (né si verificava l'altra ipotesi  che  per  &#13;
 lui il figlio percepisse gli assegni).                                   &#13;
     Il  pretore  dubita  della  conformità  agli  artt.  3  e 38 della  &#13;
 Costituzione di questa limitazione del  diritto  dell'istante  al  solo  &#13;
 caso di morte del genitore, con l'esclusione del caso di abbandono, che  &#13;
 il  pretore  considera equivalente, e che tale è considerato dall'art.  &#13;
 3 dello stesso d.P.R. n. 797 il quale accorda gli assegni al prestatore  &#13;
 di lavoro che abbia a carico nipoti per la morte o  l'abbandono...  del  &#13;
 loro padre.                                                              &#13;
     2. - La questione è fondata.                                        &#13;
     La evidente restio della disposizione dell'art. 8 del d.P.R. n. 797  &#13;
 sta  in ciò che allorquando viva il padre del lavoratore tocchi a lui,  &#13;
 discendente immediato, e non al nipote, di provvedere agli alimenti per  &#13;
 suo padre (cioè per il nonno del lavoratore), a meno che  non  sia  il  &#13;
 lavoratore  tenuto  all'obbligo  alimentare verso il padre e che quindi  &#13;
 abbia diritto a percepire gli assegni per il padre, stesso, Insomma, il  &#13;
 diritto agli assegni per l'ascendente spetta al figlio quando è questi  &#13;
 tenuto all'obbligo alimentare; spetta al nipote quando  -  non  dovendo  &#13;
 provvedere  il  figlio o perché è morto, o perché non è in grado di  &#13;
 assolvere tale obbligo tanto che il  di  lui  figlio  ha  diritto  agli  &#13;
 assegni familiari per lui - l'obbligo alimentare cade sul nipote.        &#13;
     Pertanto   appare   senza   giustificazione   che,  ai  fini  della  &#13;
 corresponsione dell'assegno familiare, al caso di  morte  del  genitore  &#13;
 non  sia equiparato quello di abbandono da parte di lui, essendo eguale  &#13;
 in entrambi i casi l'effetto di porre a   carico del  nipote  l'obbligo  &#13;
 alimentare.                                                              &#13;
     La   difesa   dell'INPS  afferma  che  la  morte  "non  (è)  certo  &#13;
 assimilabile all'abbandono, anche se ugualmente  esonerativo  del  più  &#13;
 volte  ricordato  onere  del  mantenimento".  Senonché non soltanto il  &#13;
 riconoscimento di questo eguale effetto "esonerativo" è  in  contrasto  &#13;
 con  la  negazione  dell'assimilabilità  dei  due  eventi  al fini del  &#13;
 diritto  a  percepire  gli  assegni  del  lavoratore  sul   quale,   in  &#13;
 conseguenza  del detto effetto "esonerativo" viene a cadere l'onere del  &#13;
 mantenimento; ma, ciò che è più importante e certamente decisivo, è  &#13;
 lo stesso legislatore,  nella  stessa  legge,  che  agli  stessi  figli  &#13;
 assimila i due eventi quando, nell'art. 3 del d.P.R. n. 797, stabilisce  &#13;
 che  spettano gli assegni ai "prestatori di lavoro che abbiano a carico  &#13;
 fratelli o sorelle o nipoti per la morte o l'abbandono o  l'invalidità  &#13;
 permanente al lavoro del loro padre, sempreché la madre non usufruisca  &#13;
 di assegni familiari".                                                   &#13;
     Il  che  significa che anche per il legislatore l'elemento di fatto  &#13;
 che rileva, ai fini del diritto a percepire gli assegni  è  costituito  &#13;
 dall'obbligo   per  il  lavoratore  di  mantenere  il  nipote,  obbligo  &#13;
 conseguente alla circostanza che, per morte o abbandono  o  invalidità  &#13;
 permanente,  quell'obbligo  non  sia  assolto  dal padre del nipote del  &#13;
 prestatore d'opera, cioè dal figlio di questi.                          &#13;
     Pertanto appare del tutto irrazionale che nell'art. 8 del d.P.R. n.  &#13;
 797 ai fini dell'attribuzione degli assegni  per  il  mantenimento  del  &#13;
 nonno,  il caso di abbandono del genitore non sia assimilato al caso di  &#13;
 morte, mentre questa assimilazione è operata  nell'art.  3  quando  il  &#13;
 lavoratore chiede gli assegni per il mantenimento del nipote e il padre  &#13;
 di questi sia morto o lo abbia abbandonato.                              &#13;
     La   violazione   del   principio  di  eguaglianza  (art.  3  della  &#13;
 Costituzione) è evidente e ciò dispensa la Corte dall'esaminare se la  &#13;
 norma denunciata violi,  come  ritiene  il  pretore  rimettente,  anche  &#13;
 l'art. 38 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 8, lett.  b, del  &#13;
 d.P.R.   30   maggio   1955,  n.  797,  nella  parte  in  cui  ai  fini  &#13;
 dell'attribuzione degli assegni familiari non assimila  all'ipotesi  di  &#13;
 morte del genitore l'abbandono da parte di questi.                       &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 14 dicembre 1984.                             &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE  FERRARI  - GIOVANNI CONSO -  &#13;
                                   ETTORE GALLO - ALDO CORASANITI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
