<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>145</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:145</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/06/1971</data_decisione>
    <data_deposito>30/06/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale del d.P.R. 30 giugno  &#13;
 1965, n. 1124  (testo  unico  delle  disposizioni  per  l'assicurazione  &#13;
 obbligatoria   contro   gli   infortuni   sul   lavoro  e  le  malattie  &#13;
 professionali), promosso con ordinanza emessa il 3 settembre  1969  dal  &#13;
 giudice  istruttore  del tribunale di Trapani nel procedimento penale a  &#13;
 carico di Ardito Antonina ed altri, iscritta al  n.  378  del  registro  &#13;
 ordinanze  1969  e pubblicata nella Gazzetta Ufficiale della Repubblica  &#13;
 n. 280 del 5 novembre 1969.                                              &#13;
     Visto  l'atto  d'intervento  del  Presidente  del   Consiglio   dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza pubblica del 18 maggio 1971 il Giudice relatore  &#13;
 Giuseppe Verzì;                                                         &#13;
     udito il sostituto avvocato generale dello Stato Franco  Chiarotti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso del procedimento penale contro Ardito Antonina, Mistretta  &#13;
 Girolamo  e  La  Rocca Giacomo, imputati di concorso nel delitto di cui  &#13;
 agli artt. 56, 110 e 640 del codice penale per avere tentato di indurre  &#13;
 in errore  l'INAIL  per  procurare  un  ingiusto  profitto  all'Ardito,  &#13;
 simulando  le  circostanze  di  un  infortunio  agricolo,  al  fine  di  &#13;
 conseguire l'indennizzo dal predetto Istituto,  il  giudice  istruttore  &#13;
 presso  il  tribunale di Trapani con ordinanza del 3 settembre 1969, ha  &#13;
 sollevato la questione di legittimità  costituzionale  del  d.P.R.  30  &#13;
 giugno  1965,  n. 1124 (legge delegata) contenente il testo unico delle  &#13;
 disposizioni per l'assicurazione obbligatoria contro gli infortuni  sul  &#13;
 lavoro  e  le  malattie  professionali,  in relazione all'art. 30 della  &#13;
 legge delegante 19 gennaio 1963, n. 15, ed in riferimento  all'art.  76  &#13;
 della Costituzione.                                                      &#13;
     Secondo  l'ordinanza,  nel  fatto  attribuito agli imputati sarebbe  &#13;
 ravvisabile, non già il reato di tentata truffa,  ma  quello  previsto  &#13;
 dall'art. 18 del d.l.l. 23 agosto 1917, n. 1450, convertito nella legge  &#13;
 17  aprile  1925,  n. 473, che puniva con le penalità comminate per il  &#13;
 reato  di truffa il lavoratore che simulava l'infortunio o ne aggravava  &#13;
 dolosamente le conseguenze, e che deve intendersi abrogato, non essendo  &#13;
 stato riprodotto nel  suindicato  d.P.R.  n.  1124  del  1965,  che  ha  &#13;
 raccolto   in   testo   unico  tutte  le  disposizioni  in  materia  di  &#13;
 assicurazione  obbligatoria  contro  gli  infortuni  sul  lavoro  e  le  &#13;
 malattie professionali. Siffatta abrogazione supererebbe i limiti della  &#13;
 delega contenuta nell'art. 30 della legge n. 15 del 1963, atteso che da  &#13;
 questa  non  si  desumerebbe  alcun criterio che possa interpretarsi in  &#13;
 maniera tanto lata da far ritenere che il legislatore  delegante  abbia  &#13;
 autorizzato un trattamento più benevolo all'assicurato fraudolento.     &#13;
     Nel  presente  giudizio,  vi  è  stato  soltanto  l'intervento del  &#13;
 Presidente del Consiglio dei ministri.                                   &#13;
     L'Avvocatura generale dello Stato riconosce che il  t.u.  del  1965  &#13;
 non  ha  riprodotto la norma di cui all'art. 18 del d.l.l.  n. 1450 del  &#13;
 1917, né ha comunque previsto autonomamente il reato di simulazione di  &#13;
 infortunio agricolo, ed ammette quindi la abrogazione  tacita  rilevata  &#13;
 dall'ordinanza  di  rimessione; ma ritiene che non vi sia stato eccesso  &#13;
 di delega attesa la formula dell'art. 30 della legge  delegante  n.  15  &#13;
 del 1963.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Secondo  l'ordinanza  di  rimessione,  il Governo - delegato  &#13;
 dall'art. 30 della legge 19 gennaio 1963, n. 15, a riunire in un  testo  &#13;
 unico  le  norme  relative  all'assicurazione  obbligatoria  contro gli  &#13;
 infortuni sul lavoro e le malattie professionali -  avrebbe  violato  i  &#13;
 principi  informatori  della delega, omettendo di riportare, nel d.P.R.  &#13;
 30 giugno 1965, n. 1124, l'art. 18 del d.l.l. 23 agosto 1917, n.  1450,  &#13;
 convertito  nella  legge  17  aprile  1925,  n. 473, che dispone che il  &#13;
 lavoratore agricolo, il quale abbia simulato l'infortunio  o  ne  abbia  &#13;
 dolosamente   aggravato  le  conseguenze,  perde  il  diritto  ad  ogni  &#13;
 indennizzo ed è sottoposto alle penalità comminate dagli artt. 413  e  &#13;
 414 del codice penale allora vigente.                                    &#13;
     Siffatta  omissione,  che  determina  sostanzialmente l'abrogazione  &#13;
 della  norma,  appare  vantaggiosa  per  l'assicurato  simulatore,  dal  &#13;
 momento  che  sopprime  una autonoma figura di reato, assimilabile alla  &#13;
 truffa soltanto quoad poenam e non abbisognevole quindi  dei  requisiti  &#13;
 propri  di  tale  delitto.  Il  legislatore  delegato  avrebbe pertanto  &#13;
 violato l'art. 76 della Costituzione per inosservanza  dei  principi  e  &#13;
 criteri  direttivi  della  delega contenuta nell'art. 30, atteso che da  &#13;
 questa non si desumerebbe alcun criterio  che  possa  interpretarsi  in  &#13;
 maniera  tanto  lata da far ritenere che il legislatore delegante abbia  &#13;
 autorizzato un trattamento più benevolo all'assicurato fraudolento.     &#13;
     2. - La questione è infondata.                                      &#13;
     Ragioni di coordinamento fra  le  varie  norme  di  legge  relative  &#13;
 all'assicurazione  obbligatoria  contro  gli  infortuni sul lavoro e le  &#13;
 malattie professionali, sia dei lavoratori in agricoltura sia di quelli  &#13;
 dell'industria,  e  necessità  di  semplicità  e   speditezza   nelle  &#13;
 procedure  giustificano  pienamente  la soppressione dell'art.  18; per  &#13;
 altro autorizzata dalla delega contenuta nell'art. 30 della legge n. 15  &#13;
 del 1963, la quale prevede che il Governo possa  "stabilire  modifiche,  &#13;
 correzioni,  ampliamenti,  ed,  ove  occorra,  soppressioni delle norme  &#13;
 vigenti".  Ed invero, siffatta soppressione è valsa ad eliminare molte  &#13;
 questioni,  che  erano  sorte  in  merito  alla  autonomia  del   reato  &#13;
 ipotizzato  in  detto  articolo  ed  in merito alla integrazione con le  &#13;
 norme  del  codice  penale,  specialmente  dopo  l'entrata in vigore di  &#13;
 quello del 1930, che  aveva  ulteriormente  disciplinato  il  reato  di  &#13;
 simulazione  di  infortunio  rispetto  al  codice  precedente;  ed  era  &#13;
 soprattutto richiesta dalla necessità  di  coordinamento  delle  norme  &#13;
 vigenti  per  i  lavoratori  in  agricoltura  con quelle dei lavoratori  &#13;
 dell'industria.                                                          &#13;
     Infatti, l'art. 46  della  legge  n.  1765  del  1935  dispone  che  &#13;
 l'assicurato  simulatore  di  infortunio  "perde  il  diritto  ad  ogni  &#13;
 prestazione, ferme rimanendo le pene stabilite  dalla  legge",  afferma  &#13;
 cioè  il  principio  del  rinvio  al  codice  penale  qualora il fatto  &#13;
 costituisca  reato.  Ed   a   tale   principio,   per   gli   infortuni  &#13;
 nell'industria, si è attenuto il legislatore delegato, riproducendo la  &#13;
 norma contenuta in detto articolo, nell'art. 65 del d.P.R. n.  1124 del  &#13;
 1965.                                                                    &#13;
     Non  sussistendo  alcun  plausibile  motivo  di  mantenere  per gli  &#13;
 infortuni in agricoltura una  autonoma  figura  di  reato,  per  punire  &#13;
 violazioni  aventi le medesime caratteristiche, non è stato riprodotto  &#13;
 nello stesso  decreto  presidenziale  l'art.  18  di  cui  si  discute,  &#13;
 raggiungendosi così l'intento di realizzare una unica disciplina.       &#13;
     Né  varrebbe il rilievo che l'art. 46 della legge n. 1765 del 1935  &#13;
 non è stato riprodotto anche per  gli  infortuni  in  agricoltura,  in  &#13;
 quanto per il rinvio alle norme del codice penale era, sostanzialmente,  &#13;
 superflua   la   specificazione   fatta   dall'art.   65   del  decreto  &#13;
 presidenziale sopraindicato.                                             &#13;
     Non sussiste, pertanto, la denunziata violazione  dell'articolo  76  &#13;
 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 del d.P.R. 30 giugno 1965, n. 1124 (testo unico delle disposizioni  per  &#13;
 l'assicurazione  ohbligatoria  contro  gli  infortuni  sul  lavoro e le  &#13;
 malattie professionali), sollevata dall'ordinanza 3 settembre 1969  del  &#13;
 giudice  istruttore del tribunale di Trapani in riferimento all'art. 76  &#13;
 della Costituzione ed in relazione all'art. 30 della legge delegante 19  &#13;
 gennaio 1963, n. 15.                                                     &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 18 giugno 1971.                               &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
