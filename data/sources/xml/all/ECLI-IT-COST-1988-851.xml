<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>851</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:851</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/07/1988</data_decisione>
    <data_deposito>21/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 20 e tabella D    &#13;
 allegata della legge 27 dicembre 1983, n. 730  (Disposizioni  per  la    &#13;
 formazione  del  bilancio  annuale  e pluriennale dello Stato - legge    &#13;
 finanziaria 1984) e dell'art. 23 della legge 28 febbraio 1986, n.  41    &#13;
 (Disposizioni  per  la  formazione del bilancio annuale e pluriennale    &#13;
 dello Stato - legge  finanziaria  1986),  promossi  con  le  seguenti    &#13;
 ordinanze:                                                               &#13;
    1)  ordinanza  emessa  il  5  novembre  1984  dal  T.A.R.  per  il    &#13;
 Friuli-Venezia Giulia sul ricorso  proposto  da  Carchio  Gian  Paolo    &#13;
 contro  il  Ministero  di Grazia e Giustizia ed altro, iscritta al n.    &#13;
 547 del registro ordinanze 1985 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 297- bis dell'anno 1985;                             &#13;
    2)  ordinanza  emessa  il  4 agosto 1986 dal Pretore di Modena nel    &#13;
 procedimento civile vertente tra Fornaciari Carlo e l'INPS,  iscritta    &#13;
 al  n.  81  del  registro  ordinanze 1987 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 14,  prima  serie  speciale,  dell'anno    &#13;
 1987;                                                                    &#13;
    Visti  l'atto  di  costituzione  dell'INPS  nonché  gli  atti  di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto   che   il   Tribunale   Amministrativo   Regionale   del    &#13;
 Friuli-Venezia Giulia, con ordinanza in data 5 dicembre 1984 (R.O. n.    &#13;
 547/85),  ha  sollevato,  in riferimento agli artt. 3, 29 e 31 Cost.,    &#13;
 questione di legittimità costituzionale dell'art. 20 della legge  27    &#13;
 dicembre  1983,  n.  730,  nella parte in cui dispone che le quote di    &#13;
 aggiunta di famiglia, nonché ogni  altro  trattamento  di  famiglia,    &#13;
 comunque  denominato,  cessano  di essere corrisposti, ad iniziare da    &#13;
 quelli di importo più elevato, in relazione al reddito familiare  ed    &#13;
 al  numero  delle  persone  a  carico  dei soggetti percettori (nella    &#13;
 specie, dipendenti dello Stato), secondo la tabella D  allegata  alla    &#13;
 predetta legge;                                                          &#13;
      che  il Pretore di Modena, con ordinanza emessa il 4 agosto 1986    &#13;
 (R.O. n. 81/87), ha sollevato: a) in  via  principale,  questione  di    &#13;
 legittimità  costituzionale  dello stesso art. 20 della legge n. 730    &#13;
 del 1983 e della tabella D ad  essa  annessa,  nonché  dell'art.  23    &#13;
 della  legge  28 febbraio 1986, n. 41, in quanto, in violazione degli    &#13;
 artt. 3, primo comma, 29, primo comma, 31, primo comma, e  36,  primo    &#13;
 comma,  Cost.,  condizionano  a  determinati  redditi  familiari,  in    &#13;
 relazione al numero delle persone a carico dei  soggetti  percettori,    &#13;
 ed anche indipendentemente da tale numero (art. 23, quarto comma), la    &#13;
 corresponsione degli assegni familiari di cui  al  d.P.R.  30  maggio    &#13;
 1955, n. 797, come di ogni altro trattamento per carichi di famiglia;    &#13;
 b) in subordine, questione di legittimità costituzionale di entrambe    &#13;
 le norme testé citate, nella parte in cui, in violazione degli artt.    &#13;
 3, primo comma, 29, primo comma, 36,  primo  comma,  e  53,  primo  e    &#13;
 secondo   comma,   Cost.,  nel  condizionare  a  determinati  redditi    &#13;
 familiari, in relazione al numero delle persone a carico dei soggetti    &#13;
 percettori   ed   anche   indipendentemente   da   tale   numero,  la    &#13;
 corresponsione degli assegni suddetti, determinano i redditi  stessi,    &#13;
 con  riguardo all'importo assoggettabile ad imposta sul reddito delle    &#13;
 persone fisiche e non,  invece,  a  quello  minore  risultante  dalla    &#13;
 detrazione  dal  primo  dell'importo pagato (o comunque dovuto a tale    &#13;
 titolo da colui o da coloro i cui redditi concorrono alla  formazione    &#13;
 di quello familiare complessivo);                                        &#13;
      che il T.A.R. del Friuli-Venezia Giulia ha rilevato che, essendo    &#13;
 l'aggiunta di famiglia di cui al D.C.P.S. 27 novembre 1947, n.  1331,    &#13;
 e successive modifiche, collegata al carico familiare del lavoratore,    &#13;
 la sua soppressione, anche se solo in alcuni casi, implica violazione    &#13;
 dell'art.   3   Cost.:   infatti,  la  stessa  retribuzione  verrebbe    &#13;
 attribuita ai lavoratori sia con carichi di famiglia che senza, così    &#13;
 prevedendosi   eguale  trattamento  pur  in  presenza  di  situazioni    &#13;
 diverse; resterebbero, inoltre, violati  gli  artt.  29  e  31  Cost.    &#13;
 poiché  le  norme censurate limiterebbero i diritti della famiglia e    &#13;
 non ne agevolerebbero la formazione;                                     &#13;
      che  il  Pretore  di  Modena  ha  rilevato che il dipendente con    &#13;
 carichi di famiglia, ma con reddito (proprio o  familiare)  superiore    &#13;
 ai  limiti  fissati dalle leggi in questione, non ha diritto ad alcun    &#13;
 emolumento che lo sovvenga in ordine alle maggiori  spese  incontrate    &#13;
 per  il  mantenimento  della  famiglia: ciò che violerebbe l'art. 31    &#13;
 Cost., rendendo irrilevante, ai fini del trattamento economico, avere    &#13;
 o non carichi di famiglia; lo stesso art. 31 nonché l'art. 29 Cost.,    &#13;
 disconoscendosi  i  diritti  della  famiglia   e   non   agevolandosi    &#13;
 l'adempimento  dei  doveri  familiari; l'art. 36 Cost., risultando la    &#13;
 retribuzione inadeguata alle esigenze familiari; ed,  infine,  l'art.    &#13;
 53  Cost.  perché  la  mancata  considerazione  dell'imposta  dovuta    &#13;
 implica un peggior trattamento per  le  famiglie  monoreddito,  nelle    &#13;
 quali  l'imposta pagata risulta maggiore che non nelle famiglie nelle    &#13;
 quali il reddito sia diviso tra più titolari;                           &#13;
    Considerato  che  gli  assegni  familiari o l'aggiunta di famiglia    &#13;
 rappresentano non tanto una parte dello stipendio o  salario,  quanto    &#13;
 un'ulteriore erogazione a carattere assistenziale e sociale destinata    &#13;
 alle necessità familiari;                                               &#13;
      che,  nel  caso  di  redditi  superiori  ad  un certo limite, il    &#13;
 legislatore ha discrezionalmente valutato già sufficienti i  redditi    &#13;
 medesimi  al soddisfacimento delle esigenze personali e familiari dei    &#13;
 lavoratori ed ha conseguentemente  ravvisato  la  non  necessità  di    &#13;
 un'ulteriore prestazione a carattere assistenziale;                      &#13;
      che  sembra  impropriamente  richiamato  l'art.  29  Cost.  che,    &#13;
 riguardando  i  diritti  della  famiglia  quale  società   naturale,    &#13;
 concerne l'ambito delle norme che favoriscono il ricongiungimento dei    &#13;
 lavoratori, la successione dei familiari nei rapporti di locazione  e    &#13;
 simili;                                                                  &#13;
      che  non  sembra  ipotizzabile  un contrasto con l'art. 31 Cost.    &#13;
 poiché l'assegno familiare (nei casi in cui  i  livelli  di  reddito    &#13;
 siano  discrezionalmente  valutati  dal  legislatore come inidonei al    &#13;
 soddisfacimento delle esigenze familiari)  è  destinato  appunto  ad    &#13;
 agevolare l'adempimento dei doveri familiari;                            &#13;
      che   il   riferimento   all'art.  36  Cost.  appare  improprio,    &#13;
 considerata la natura  non  propriamente  retributiva  degli  assegni    &#13;
 familiari;                                                               &#13;
      che,  quanto  al  richiamo  all'art. 53 Cost., va rilevato che i    &#13;
 limiti di reddito,  presi  in  considerazione  per  stabilire  se  le    &#13;
 condizioni  economiche  familiari siano tali o non da giustificare il    &#13;
 suddetto intervento integrativo di natura sociale, non possono essere    &#13;
 stabiliti  se  non  con  riferimento  ai  redditi  stessi e non anche    &#13;
 all'importo che  di  essi  residua  dopo  l'adempimento  di  obblighi    &#13;
 imposti  da  norme  aventi  finalità  e  natura  del  tutto estranee    &#13;
 all'ambito della normativa censurata;                                    &#13;
      che,  pertanto,  le  esaminate questioni appaiono manifestamente    &#13;
 infondate;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza delle questioni di legittimità    &#13;
 costituzionale dell'art. 20 della legge  27  dicembre  1983,  n.  730    &#13;
 (Disposizioni  per  la  formazione del bilancio annuale e pluriennale    &#13;
 dello Stato - Legge finanziaria 1984) e dell'art. 23 della  legge  28    &#13;
 febbraio  1986,  n.  41  (Disposizioni per la formazione del bilancio    &#13;
 annuale  e  pluriennale  dello  Stato  -  Legge  finanziaria   1986),    &#13;
 sollevate,  in  riferimento  agli artt. 3, 29, 31, 36, primo comma, e    &#13;
 53, primo  e  secondo  comma,  Cost.,  dal  Tribunale  Amministrativo    &#13;
 Regionale  del  Friuli-Venezia  Giulia e dal Pretore di Modena con le    &#13;
 ordinanze in epigrafe.                                                   &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 luglio 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
