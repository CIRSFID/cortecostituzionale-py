<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>258</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:258</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Leopoldo Elia</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/12/1982</data_decisione>
    <data_deposito>31/12/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - &#13;
 Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  10,  comma  &#13;
 secondo,  della  legge  13  giugno  1912,  n.  555  (Acquisizione della  &#13;
 cittadinanza italiana) e dell'art. 143  ter.  cod.  civ.  (cittadinanza  &#13;
 della  moglie)  promosso  con  ordinanza emessa il 17 febbraio 1977 dal  &#13;
 Tribunale di Grosseto sul ricorso proposto da Rosasco  Elisa,  iscritta  &#13;
 al  n.  167  del  registro  ordinanze  1977 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 141 del 25 maggio 1977.                    &#13;
     Udito nell'udienza pubblica del 5 maggio 1982 il  Giudice  relatore  &#13;
 Leopoldo Elia.                                                           &#13;
     Ritenuto  che  il Tribunale di Grosseto, con ordinanza emessa il 17  &#13;
 febbraio 1977, ha sollevato questione  di  legittimità  costituzionale  &#13;
 degli  artt.  10,  secondo comma, della legge 13 giugno 1912, n. 555, e  &#13;
 143 ter del codice civile nella parte in cui prevedono che la cittadina  &#13;
 straniera che sposi un cittadino italiano  non  possa  rinunziare  alla  &#13;
 cittadinanza italiana acquisita per matrimonio, in relazione all'art. 3  &#13;
 della  Costituzione,  sia  perché  pone  la  moglie  in  condizione di  &#13;
 inferiorità rispetto al marito, obbligandola  ad  assumere  la  stessa  &#13;
 cittadinanza  senza  o  anche  contro  la  sua  volontà, non essendole  &#13;
 concesso di rinunciarvi, sia perché crea una disparità di trattamento  &#13;
 con  la  cittadina  italiana,  sposa  di  straniero,  che  invece  può  &#13;
 rinunziare alla cittadinanza italiana.                                   &#13;
     Considerato  che  pregiudiziale all'esame della questione sollevata  &#13;
 dal Tribunale è la  verifica  della  legittimità  costituzionale  del  &#13;
 principio della juris communicatio per matrimonio;                       &#13;
     che  non  è  manifestamente  infondato  il  dubbio  che l'art. 10,  &#13;
 secondo comma, della legge n. 555 del 1912 contrasti con gli artt. 3  e  &#13;
 29  della  Costituzione,  sia  per la condizione di inferiorità in cui  &#13;
 pone la moglie rispetto al marito, sia per la disparità  che  ingenera  &#13;
 tra  cittadino  e cittadina stranieri, acquisendo solo quest'ultima per  &#13;
 matrimonio la cittadinanza italiana (cfr.  sent. n. 87 del 1975);        &#13;
     che tale condizione di inferiorità  nell'ambito  familiare  sembra  &#13;
 ledere  i  diritti  inviolabili  della  persona di cui all'art. 2 della  &#13;
 Costituzione (cfr. sent. n.  181/1976);                                  &#13;
     che  la  norma  in esame pare anche contrastare con l'art. 22 della  &#13;
 Costituzione, laddove quest'ultimo esclude automatismi nell'acquisto  a  &#13;
 titolo non originario della cittadinanza italiana;                       &#13;
     che,  ove  fosse  accolta la questione così sollevata, rilevante e  &#13;
 non   manifestamente   infondata   sarebbe   pure   la   questione   di  &#13;
 costituzionalità dell'art. 4, n. 3 della legge 13 giugno 1912, n. 555,  &#13;
 nella  parte  in  cui  non  prevede  che  la  cittadinanza possa essere  &#13;
 concessa anche  alla  straniera  che  abbia  contratto  matrimonio  con  &#13;
 cittadino   italiano,   in   riferimento   agli  artt.  3  e  29  della  &#13;
 Costituzione,  per  la  disparità  di  trattamento  tra  straniero   e  &#13;
 straniera e tra marito e moglie.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     Visto l'art. 23 della legge 11 marzo 1953, n. 87;                    &#13;
     1)  dichiara  rilevante e non manifestamente infondata la questione  &#13;
 di legittimità costituzionale dell'art. 10, secondo comma, della legge  &#13;
 13 giugno 1912, n. 555,  nella  parte  in  cui  prevede  che  la  donna  &#13;
 straniera   che  si  marita  con  il  cittadino  italiano  acquista  la  &#13;
 cittadinanza italiana, in riferimento agli artt. 2, 3, 22  e  29  della  &#13;
 Costituzione;                                                            &#13;
     2)  dichiara,  nell'ipotesi  di  accoglimento della prima questione  &#13;
 sollevata, rilevante e non manifestamente  infondata  la  questione  di  &#13;
 legittimità  costituzionale  dell'art.  4,  n.  3, della stessa legge,  &#13;
 nella parte in  cui  non  prevede  che  la  cittadinanza  possa  essere  &#13;
 concessa,  alle  stesse condizioni stabilite nei riguardi del cittadino  &#13;
 straniero, anche alla straniera  che  abbia  contratto  matrimonio  con  &#13;
 cittadino   italiano,   in   riferimento   agli  artt.  3  e  29  della  &#13;
 Costituzione;                                                            &#13;
     ordina la  sospensione  del  giudizio  introdotto  con  l'ordinanza  &#13;
 iscritta al n. 167 del registro ordinanze 1977;                          &#13;
     manda  alla Cancelleria per gli adempimenti di legge.  Così deciso  &#13;
 in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  &#13;
 il 22 dicembre 1982.                                                     &#13;
                                   F.to: LEOPOLDO  ELIA  -  ANTONINO  DE  &#13;
                                   STEFANO   -   GUGLIELMO  ROEHRSSEN  -  &#13;
                                   ORONZO REALE -  BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN  -  ANTONIO  LA   PERGOLA   -  &#13;
                                   VIRGILIO  ANDRIOLI - GIUSEPPE FERRARI  &#13;
                                   - FRANCESCO SAJA - GIOVANNI CONSO.     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
