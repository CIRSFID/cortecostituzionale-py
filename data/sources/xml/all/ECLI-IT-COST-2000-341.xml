<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>341</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:341</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2000</data_decisione>
    <data_deposito>24/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO; Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 3, commi da 193    &#13;
 a  203  della  legge  13 (recte: 23) dicembre 1996, n. 662 (Misure di    &#13;
 razionalizzazione  della  finanza  pubblica),  promosso con ordinanza    &#13;
 emessa  il  25 febbraio 1998 dalla Commissione tributaria provinciale    &#13;
 di  Roma  sul  ricorso  proposto  da  Paci Eliseo contro la Direzione    &#13;
 regionale  delle  entrate  del  Lazio, iscritta al n. 66 del registro    &#13;
 ordinanze 1999 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 8, prima serie speciale, dell'anno 1999.                              &#13;
     Visti  l'atto  di  costituzione di Paci Eliseo, nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
     Udito  nell'udienza  pubblica  del  20  giugno  2000  il  giudice    &#13;
 relatore Riccardo Chieppa;                                               &#13;
     Uditi l'avvocato Mario Tonucci per Paci Eliseo e l'avvocato dello    &#13;
 Stato  Maria  Giancarlo  Mandò  per  il Presidente del Consiglio dei    &#13;
 Ministri.                                                                &#13;
     Ritenuto  che, nel corso di un giudizio attinente al rimborso del    &#13;
 contributo  straordinario  per  l'Europa,  la  Commissione tributaria    &#13;
 provinciale  di Roma, condividendo i rilievi mossi dal ricorrente, ha    &#13;
 sollevato,  in  riferimento agli artt. 3, 23 e 53 della Costituzione,    &#13;
 questione  di legittimità costituzionale dell'art. 3, commi da 193 a    &#13;
 203,  della  legge  13  (recte:  23) dicembre 1996, n. 662 (Misure di    &#13;
 razionalizzazione  della  finanza pubblica), prevedenti l'istituzione    &#13;
 per il 1996 dell'anzidetto contributo straordinario;                     &#13;
         che,  secondo  il  giudice  a quo il nuovo tributo colpirebbe    &#13;
 principalmente  determinate fasce di reddito, esonerando quelle al di    &#13;
 sotto  di  una  certa soglia, con la conseguenza che il criterio alla    &#13;
 base del contributo straordinario risulterebbe, in tal modo, difforme    &#13;
 da  quello  concretizzato  con  la  imposizione  generale sui redditi    &#13;
 (IRPEF, IRPEG, ecc.), pur incidendo sullo stesso reddito imponibile;     &#13;
         che verrebbe, così, violato il criterio della progressività    &#13;
 della  imposizione  fiscale,  della uguaglianza e della universalità    &#13;
 della  imposizione,  nonché della necessaria aderenza alla capacità    &#13;
 contributiva;                                                            &#13;
         che,  ad  avviso  del  giudice  rimettente,  la  normativa in    &#13;
 questione   interferirebbe   con   il   principio   della   capacità    &#13;
 contributiva  anche  sotto  il  profilo  della  retroattività  della    &#13;
 imposizione, senza, peraltro, alcuna razionale giustificazione;          &#13;
         che,  infatti,  nel caso di specie, un reddito di un soggetto    &#13;
 contribuente,  che  è  già  assoggettato  alla imposta generale sul    &#13;
 reddito,   verrebbe   ad   essere   sottoposto   ad   una   ulteriore    &#13;
 contribuzione;                                                           &#13;
         che nel giudizio innanzi alla Corte si è costituita la parte    &#13;
 privata,  ricorrente  nel giudizio a quo la quale ha insistito per la    &#13;
 declaratoria  di  incostituzionalità  della  norma  impugnata  ed ha    &#13;
 sottolineato  che il meccanismo di incidenza del tributo in questione    &#13;
 colpirebbe,  in  particolare  modo,  una ristretta fascia reddituale,    &#13;
 mentre  la  maggior  parte  delle  fasce  di  reddito ne rimarrebbero    &#13;
 esenti;  queste  ultime,  peraltro, non necessariamente avrebbero una    &#13;
 capacità  contributiva  minima,  giacché  l'imposta,  così come è    &#13;
 strutturata, finirebbe per esentare livelli di capacità contributiva    &#13;
 media,  con una concentrazione del carico tributario specifico su una    &#13;
 minoranza  della  platea dei contribuenti, con conseguente violazione    &#13;
 del  principio  di capacità contributiva soggettiva sotto il profilo    &#13;
 sia  della  universalità e della generalità del concorso, sia della    &#13;
 uguaglianza e della razionalità della norma;                            &#13;
         che,   in   relazione  alla  lamentata  retroattività  della    &#13;
 normativa  in  questione,  la parte privata, dopo aver richiamato gli    &#13;
 orientamenti  della  Corte  sull'argomento,  sottolinea  come,  nella    &#13;
 specie,  non  ricorrerebbero  le  ragioni  di giustificazione di tale    &#13;
 efficacia   retroattiva,   rinvenute  dalla  giurisprudenza  in  casi    &#13;
 precedenti,  in quanto il contributo straordinario, andando a colpire    &#13;
 un reddito già tassato, non potrebbe connotarsi per prevedibilità e    &#13;
 logicità;                                                               &#13;
         che, nel giudizio introdotto con l'ordinanza di cui sopra, è    &#13;
 intervenuto   il  Presidente  del  Consiglio  dei  Ministri,  con  il    &#13;
 patrocinio dell'Avvocatura generale dello Stato, il quale ha concluso    &#13;
 per  la infondatezza della questione, osservando, in particolare, che    &#13;
 il confronto è fatto tra imposte di diversa natura, una delle quali,    &#13;
 peraltro,  con carattere di straordinarietà, per cui il principio di    &#13;
 uguaglianza non risulterebbe violato;                                    &#13;
         che,  nella  memoria dell'Avvocatura generale dello Stato, si    &#13;
 aggiunge che non sussisterebbe la dedotta retroattività della norma,    &#13;
 in  quanto  il  contributo  è  riferito ad un reddito che ancora non    &#13;
 sarebbe  maturato;  peraltro, la retroattività sarebbe incompatibile    &#13;
 con  l'art. 53  della  Costituzione  solo quando la norma sopravviene    &#13;
 dopo  molti  anni dall'avveramento del presupposto, quando, cioè, si    &#13;
 siano disperse le risorse della capacità contributiva;                  &#13;
         che,   nell'imminenza   della   data  fissata  per  l'udienza    &#13;
 pubblica,  la  parte  privata ha presentato una memoria, con la quale    &#13;
 insiste   nelle  proprie  conclusioni,  ponendo  l'accento  sul  modo    &#13;
 discriminatorio con cui le norme denunciate, attraverso il meccanismo    &#13;
 delle  esenzioni  e delle detrazioni, inciderebbero sui contribuenti,    &#13;
 recando  violazione  al principio di universalità della obbligazione    &#13;
 tributaria;                                                              &#13;
         che  nella  anzidetta memoria si sottolinea come la lamentata    &#13;
 retroattività  della  norma  non  trovi giustificazione né sotto il    &#13;
 profilo  della  prevedibilità,  né,  tanto meno, sotto quello della    &#13;
 ragionevolezza,  in quanto sarebbe una norma "impositiva, innovativa,    &#13;
 costitutiva  di  un  prelievo aggiuntivo sul reddito soggettivo, già    &#13;
 assoggettato all'imposta generale";                                      &#13;
         che  anche  l'Avvocatura generale dello Stato, in prossimità    &#13;
 della  pubblica  udienza,  ha  prodotto  una memoria, con la quale ha    &#13;
 ribadito le conclusioni già rassegnate, sottolineando che i principi    &#13;
 di   progressività   ed  universalità  dell'imposizione  tributaria    &#13;
 sarebbero  del  tutto  inconferenti;  il  primo,  perché va riferito    &#13;
 all'intero  sistema  tributario e non al singolo tributo; il secondo,    &#13;
 strettamente  correlato  alla  capacità contributiva, perché sta ad    &#13;
 indicare  il  diverso  concetto, secondo cui tutti coloro che sono in    &#13;
 possesso  dell'attitudine  economica  a sottostare al tributo, devono    &#13;
 concorrere alla copertura della spesa della collettività;               &#13;
         che,  in ordine all'asserita retroattività della imposizione    &#13;
 in  esame,  premesso  il  suo  carattere  straordinario,  la  memoria    &#13;
 dell'Avvocatura  generale  dello  Stato  osserva che la dissociazione    &#13;
 temporale tra la norma istitutiva ed il verificarsi del fatto assunto    &#13;
 quale  presupposto  impositivo  presenterebbe un intervallo temporale    &#13;
 estremamente  breve, tale da fondare la ragionevole presunzione della    &#13;
 permanenza delle risorse prodotte, nel pieno rispetto della effettiva    &#13;
 capacità contributiva del contribuente.                                 &#13;
     Considerato che l'ordinanza si basa su di un presupposto inesatto    &#13;
 cioè  che  l'imposizione  tributaria  debba  essere  necessariamente    &#13;
 uniforme  tra  imposte  (diverse)  che  colpiscono lo stesso reddito,    &#13;
 indipendentemente  dal  carattere  ordinario  o  straordinario  della    &#13;
 singola imposta e della sua ricorrenza e finalità;                      &#13;
         che  l'art. 53 della Costituzione deve essere interpretato in    &#13;
 modo  unitario  e  coordinato,  e  non  per  preposizioni staccate ed    &#13;
 autonome le une dalle altre;                                             &#13;
         che,  infatti, la universalità della imposizione, desumibile    &#13;
 dalla  espressione  testuale  "tutti"  (cittadini o non cittadini, in    &#13;
 qualche   modo   con  rapporti  di  collegamento  con  la  Repubblica    &#13;
 italiana),   deve  essere  intesa  nel  senso  di  obbligo  generale,    &#13;
 improntato   al   principio   di   eguaglianza  (senza  alcuna  delle    &#13;
 discriminazioni vietate: art. 3, primo comma, della Costituzione), di    &#13;
 concorrere  alle  "spese  pubbliche  in  ragione della loro capacità    &#13;
 contributiva"  (con  riferimento  al  singolo tributo ed al complesso    &#13;
 della   imposizione  fiscale),  come  dovere  inserito  nei  rapporti    &#13;
 politici    in   relazione   all'appartenenza   del   soggetto   alla    &#13;
 collettività organizzata;                                               &#13;
         che,  nello  stesso tempo, la Costituzione non impone affatto    &#13;
 una tassazione fiscale uniforme, con criteri assolutamente identici e    &#13;
 proporzionali  per  tutte  le tipologie di imposizione tributaria; ma    &#13;
 esige invece un indefettibile raccordo con la capacità contributiva,    &#13;
 in  un  quadro di sistema informato a criteri di progressività, come    &#13;
 svolgimento   ulteriore,   nello   specifico  campo  tributario,  del    &#13;
 principio  di  eguaglianza,  collegato  al compito di rimozione degli    &#13;
 ostacoli  economico-sociali  esistenti  di  fatto  alla  libertà  ed    &#13;
 eguaglianza  dei  cittadini-persone umane, in spirito di solidarietà    &#13;
 politica, economica e sociale (artt. 2 e 3 della Costituzione);          &#13;
         che    si    ravvisano   manifesti   elementi   di   completa    &#13;
 differenziazione  di  tipologia - rispetto all'IRPEF - del contributo    &#13;
 per  l'Europa:  imposizione  straordinaria  ed  una tantum seguita da    &#13;
 impegno  politico di un parziale futuro rimborso - poi realizzato per    &#13;
 il 60%, a due anni esatti di distanza, con la legge 23 dicembre 1998,    &#13;
 n. 448  -, in modo da costituire, in parte, un mero anticipo forzoso;    &#13;
 imposizione  esclusivamente  "finalizzata  all'adeguamento  dei conti    &#13;
 pubblici   ai   parametri   previsti  dal  Trattato  di  Maastricht",    &#13;
 obiettivo,   peraltro,   facilmente  prevedibile  in  relazione  alla    &#13;
 deficitaria  situazione finanziaria in Italia; imposizione con indice    &#13;
 di  riferimento  ai  redditi  del  1996,  già assoggettati ad IRPEF,    &#13;
 comunque  non  compensabile né detraibile rispetto ad altre imposte,    &#13;
 tasse  e  contributi;  imposizione  con  criteri  che dovevano tenere    &#13;
 presenti  le capacità contributive residue rispetto all'intero onere    &#13;
 tributario, con particolare considerazione delle situazioni familiari    &#13;
 e dei redditi di lavoro ed assimilati, con un meccanismo consono alla    &#13;
 qualità di contribuente (persona fisica) e delle sue imprescindibili    &#13;
 esigenze economiche e sociali;                                           &#13;
         che  di conseguenza appaiono non manifestamente irragionevoli    &#13;
 né  arbitrarie  le  scelte  legislative di introdurre, rispetto alle    &#13;
 altre  imposte  sul  reddito, differenziate disposizioni in ordine al    &#13;
 minimo imponibile, in modo da escludere qualsiasi aggravio impositivo    &#13;
 sulla   fascia   bassa   fino   a   L. 7.200.000  nonché  su  quella    &#13;
 immediatamente  contigua  attraverso  il  meccanismo delle detrazioni    &#13;
 operanti  in  modo  indifferenziato,  in  relazione  a  scaglioni  di    &#13;
 reddito, con aliquote progressive, in ordine a detrazioni legate alla    &#13;
 situazione personale familiare e al rapporto di lavoro o generali per    &#13;
 ogni  contribuente tenuto all'imposta, in modo da ridurre o escludere    &#13;
 il  carico  per  le  fasce  con capacità contributiva tutt'altro che    &#13;
 elevata;                                                                 &#13;
         che egualmente manifestamente infondate sono le altre censure    &#13;
 relative  alla  retroattività  della  norma,  correlate  sempre alla    &#13;
 capacità  contributiva,  in  quanto,  nella  specie,  si  tratta  di    &#13;
 imposizione,  che  assume  come  riferimento il reddito relativo a un    &#13;
 anno  (1996),  ancora in corso al momento della imposizione, ed ha le    &#13;
 speciali finalità e caratteristiche sopra enunciate;                    &#13;
         che  sia  per il suo immediato contesto temporale, sia per le    &#13;
 modalità   di   pagamento,   non  era  irragionevole  la  previsione    &#13;
 legislativa  di persistenza della capacità contributiva in relazione    &#13;
 alla particolare conformazione delle fasce di contribuenti colpite;      &#13;
         che,   su   un   piano  più  generale,  non  è  inibito  al    &#13;
 legislatore,  al  di  fuori  della norma penale, di emanare norme con    &#13;
 efficacia  retroattiva,  a  condizione  che  la  retroattività trovi    &#13;
 adeguata  giustificazione  sul  piano  della  ragionevolezza e non si    &#13;
 ponga  in  contrasto  con altri valori o interessi costituzionalmente    &#13;
 protetti (sentenze nn. 416 e 229 del 1999);                              &#13;
         che  è  evidente  che,  nel  caso in esame, non si tratta di    &#13;
 tassazione di specifici atti od operazioni o transazioni economiche e    &#13;
 commerciali,  per le quali, invece, si potrebbero delineare, sotto il    &#13;
 profilo  della  non  ragionevolezza,  problemi  di  affidamento  e di    &#13;
 retroattività, quando la tassazione incida sul passato, con aggravio    &#13;
 impositivo  rispetto a specifici rapporti ormai definiti ed esauriti,    &#13;
 tra le parti, anche dal punto di vista economico;                        &#13;
         che  per  quanto  riguarda  il  parametro dell'art. 23 Cost.,    &#13;
 semplicemente   richiamato   nel   dispositivo   della  ordinanza  di    &#13;
 rimessione,  è  sufficiente  ai  fini  della  manifesta infondatezza    &#13;
 sottolineare  che  le  norme denunciate, senza alcuna possibilità di    &#13;
 incertezza,  contengono  tutti  gli  elementi perché una prestazione    &#13;
 patrimoniale  possa  ritenersi  imposta  in  base alla legge, cioè i    &#13;
 soggetti  tenuti  alla prestazione, l'oggetto della stessa, i criteri    &#13;
 per  la  concreta  individuazione  dell'onere  ed  infine  il  modulo    &#13;
 procedimentale   che   concorra   ad   escludere   arbitri  da  parte    &#13;
 dell'amministrazione (sentenza n. 507 del 1988);                         &#13;
         che  di  conseguenza  deve  essere  dichiarata  la  manifesta    &#13;
 infondatezza della questione per tutti i profili sollevati.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 3,  commi da 193 a 203, della    &#13;
 legge  23  dicembre  1996,  n. 662 (Misure di razionalizzazione della    &#13;
 finanza  pubblica),  sollevata,  in riferimento agli artt. 3, 23 e 53    &#13;
 della Costituzione, dalla Commissione tributaria provinciale di Roma,    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 luglio 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Chieppa                            &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 24 luglio 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
