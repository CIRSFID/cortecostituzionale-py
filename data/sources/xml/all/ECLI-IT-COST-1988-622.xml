<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>622</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:622</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/1988</data_decisione>
    <data_deposito>10/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 11 della legge    &#13;
 della Regione Toscana 20 marzo 1975, n.  22  (Norme  sulle  procedure    &#13;
 contrattuali  degli  enti ospedalieri), promosso con ordinanza emessa    &#13;
 il 30 luglio  1981  dal  pretore  di  Firenze  sul  ricorso  proposto    &#13;
 dall'Ospedale   Generale  Olga  Basilewsky  contro  Salvadori  Bruno,    &#13;
 iscritta al n. 740 del registro ordinanze  1981  e  pubblicata  nella    &#13;
 Gazzetta Ufficiale della Repubblica n. 47 del 1982;                      &#13;
    Visto l'atto di costituzione della Regione Toscana;                   &#13;
    Udito  nell'udienza  pubblica del 9 marzo 1988 il Giudice relatore    &#13;
 Francesco Greco;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  atto  notificato  il  10  marzo 1978, Salvadori Bruno,    &#13;
 creditore dell'Ospedale generale di zona  "Olga  Basilewski"  per  la    &#13;
 somma  di  lire  3.206.400,  pignorava,  fino  a  concorrenza di tale    &#13;
 importo, il deposito pecuniario di cui l'ente aveva  la  titolarità,    &#13;
 presso  la  propria  tesoreria,  gestita  dalla Cassa di Risparmio di    &#13;
 Firenze.                                                                 &#13;
    All'esecuzione  così intrapresa proponeva opposizione l'Ospedale,    &#13;
 eccependo l'impignorabilità,  ai  sensi  dell'art.  11  della  legge    &#13;
 regionale   della   Toscana   20   marzo  1975,  del  denaro  di  sua    &#13;
 appartenenza.                                                            &#13;
    Il  creditore  procedente  si  costituiva  in  giudizio  eccependo    &#13;
 l'illegittimità della citata norma, in relazione agli artt. 3  e  24    &#13;
 Cost., e l'adito Pretore di Firenze, ritenendo la questione rilevante    &#13;
 e non manifestamente infondata, ne rimetteva l'esame a questa  Corte,    &#13;
 con ordinanza in data 30 luglio 1981.                                    &#13;
    All'uopo,  premetteva  che l'ente pubblico condannato al pagamento    &#13;
 di  una  somma  di  denaro  versa  in   una   situazione   pienamente    &#13;
 assimilabile   a   quella  di  qualsiasi  privato  debitore,  dovendo    &#13;
 anch'esso rispondere dell'adempimento con  tutti  i  propri  beni  ed    &#13;
 eventualmente  soggiacere all'espropriazione degli stessi, sempre che    &#13;
 non facciano parte del  demanio  o  del  patrimonio  indisponibile  e    &#13;
 siano, in quanto tali, sottratti alla pignorabilità.                    &#13;
    Fra   i   beni  pignorabili  dell'ente  pubblico  deve  certamente    &#13;
 annoverarsi  il  denaro,  sia  che  giaccia  nella  cassa   dell'ente    &#13;
 medesimo, sia che costituisca oggetto dei suoi crediti: esso, invero,    &#13;
 per il carattere "neutro", non può, di per sé, ritenersi gravato da    &#13;
 vincoli  di  destinazione;  e  non valgono a conferirgli carattere di    &#13;
 indisponibilità  né  la  natura  pubblicistica   della   fonte   di    &#13;
 provenienza,  né  l'iscrizione  in  bilancio  di  uscite  idonee  ad    &#13;
 assorbire i mezzi pecuniari dell'ente.                                   &#13;
    Tuttavia, anche per il denaro, un vincolo di indisponibilità e di    &#13;
 conseguente pignorabilità può essere stabilito da una legge che  lo    &#13;
 destini   a   soddisfazione   di  una  specifica  esigenza  correlata    &#13;
 all'espletamento di un pubblico servizio. Nel caso in  questione,  un    &#13;
 vincolo  siffatto  risulta  stabilito dalla suddetta norma, la quale,    &#13;
 sebbene menzioni soltanto genericamente i "beni"  dell'ente  pubblico    &#13;
 ospedaliero,  non  può  non  riferirsi anche al denaro, tecnicamente    &#13;
 riconducibile  nell'ambito  di  tale  generale  categoria,  come   si    &#13;
 argomenta dagli artt. 810 e 820 cod. civ.                                &#13;
    Osserva,  pertanto,  il  giudice  a  quo  che  la menzionata norma    &#13;
 preclude lo svolgimento dell'esecuzione forzata  di  cui  trattasi  e    &#13;
 che,    conseguentemente,    assume    rilevanza    il    dubbio   di    &#13;
 costituzionalità sollevato in ordine ad essa.                           &#13;
    Nel  merito osserva, poi, che tale dubbio si pone, in primo luogo,    &#13;
 in  riferimento  all'art.  24,  primo  comma,   Cost.   La   garanzia    &#13;
 costituzionale  del diritto di difesa, anche nei confronti degli enti    &#13;
 pubblici, non può ritenersi circoscritta alla sola  cognizione,  ma,    &#13;
 per  essere compiutamente assicurata, deve estendersi al procedimento    &#13;
 esecutivo   diretto   all'effettiva   realizzazione    del    credito    &#13;
 insoddisfatto.                                                           &#13;
    Per  contro,  la  norma censurata, sancendo il suddetto vincolo in    &#13;
 modo  indiscriminato,  senza,  cioè,  che  rilevi  in  modo   alcuno    &#13;
 l'accertamento  di  una concreta sussistenza della strumentalità del    &#13;
 bene allo svolgimento del pubblico servizio,  preclude  ai  creditori    &#13;
 dell'ente    ogni    possibilità    di   azione   esecutiva,   causa    &#13;
 l'insussistenza di beni patrimoniali disponibili da aggredire: e ciò    &#13;
 senza  che  la  soddisfazione  del  diritto vantato possa conseguirsi    &#13;
 attraverso la diversa via del giudizio di  ottemperanza,  praticabile    &#13;
 nei  casi  in  cui  la sentenza di condanna del giudice ordinario non    &#13;
 escluda un residuo potere della P.A. di scegliere il comportamento da    &#13;
 osservare,  alla  luce  dello  specifico  interesse affidato alle sue    &#13;
 cure; potere, invece, insussistente in caso di condanna al  pagamento    &#13;
 di  una  somma di denaro, che esige soltanto il compimento di un atto    &#13;
 dovuto, versando, al riguardo, la P.A. nella identica  situazione  di    &#13;
 un privato debitore.                                                     &#13;
    La  norma  viene,  poi,  ritenuta in contrasto con l'art. 3 Cost.,    &#13;
 poiché, attese le suddette conseguenze da essa prodotte,  ne  deriva    &#13;
 un'irrazionale  discriminazione,  nel  lato passivo del rapporto, tra    &#13;
 gli enti ospedalieri toscani e tutti gli altri debitori,  pubblici  o    &#13;
 privati,  affermandosi  solo  relativamente  ai  primi l'eccezione ai    &#13;
 principi della responsabilità patrimoniale (art. 2740 cod.  civ.)  e    &#13;
 dell'assoggettabilità  dei  beni  ad  esecuzione  forzata in caso di    &#13;
 inadempimento (art. 2910 cod. civ.).  Discriminazione  ingiustificata    &#13;
 si  produce  anche  nel  lato  attivo del rapporto, tra creditori dei    &#13;
 detti enti ospedalieri  e  creditori  di  altri  soggetti  privati  o    &#13;
 pubblici,  trovando  l'esecuzione  intrapresa  dai  primi un ostacolo    &#13;
 insormontabile nell'assenza dal patrimonio dell'ente debitore di beni    &#13;
 disponibili da aggredire.                                                &#13;
    Sottolinea,  infine,  il giudice remittente che tale situazione di    &#13;
 illegittimità non è venuta  meno  a  seguito  dell'istituzione  del    &#13;
 servizio   sanitario  nazionale,  la  quale  ha  solo  comportato  il    &#13;
 subingresso  delle  UU.SS.LL.  nella  posizione  dei   cessati   enti    &#13;
 ospedalieri,  il  cui  patrimonio  è  stato  trasferito ai Comuni di    &#13;
 appartenenza dei nuovi organismi, senza  cessazione  del  vincolo  di    &#13;
 destinazione  dal  quale  si  trovava  ad  essere indiscriminatamente    &#13;
 gravato, per effetto della norma denunciata.                             &#13;
    2.  -  L'ordinanza,  ritualmente comunicata e notificata, è stata    &#13;
 altresì pubblicata nella Gazzetta Ufficiale.                            &#13;
    Si è costituita la Regione Toscana, sollecitando una declaratoria    &#13;
 di infondatezza  della  questione  e  riservandosi  di  formulare  in    &#13;
 prosieguo ulteriori deduzioni.                                           &#13;
    Nell'imminenza dell'udienza ha depositato una memoria difensiva la    &#13;
 stessa Regione sostenendo che la questione potrebbe  essere  ritenuta    &#13;
 infondata  alla  stregua di un'interpretazione della norma censurata,    &#13;
 diversa da quella fatta propria dal giudice a quo.                       &#13;
    Osserva,  invero, la difesa della Regione che detta norma sancisce    &#13;
 l'indisponibilità dei  beni  degli  enti  ospedalieri  con  espresso    &#13;
 riferimento all'art. 828, secondo comma, cod. civ.                       &#13;
    Quest'ultima disposizione, poi, menziona soltanto genericamente "i    &#13;
 beni  che  fanno  parte  del  patrimonio  indisponibile"  degli  enti    &#13;
 pubblici  e, pertanto, ai fini della concreta individuazione dei beni    &#13;
 stessi, non può ritenersi implicitamente richiamato l'art. 826  cod.    &#13;
 civ., che appunto contiene elementi idonei a siffatta individuazione,    &#13;
 attribuendo al suddetto patrimonio "gli edifici destinati a  sede  di    &#13;
 uffici  pubblici,  con i loro arredi e gli altri beni destinati ad un    &#13;
 pubblico servizio", senza far menzione del denaro.                       &#13;
    Questo,  pertanto,  in  difetto di una espressa norma di legge che    &#13;
 come tale lo ascriva al patrimonio indisponibile (ciò che non fa  la    &#13;
 disposizione  impugnata),  essendo un bene tipicamente fungibile, non    &#13;
 può risultare sottratto alla garanzia  dei  creditori  dell'ente  ed    &#13;
 essere, a tal fine, liberamente pignorabile.<diritto>Considerato in diritto</diritto>1.   -   Il   Pretore   di   Firenze   dubita  della  legittimità    &#13;
 costituzionale dell'art. 11 della legge regionale  della  Toscana  20    &#13;
 marzo    1975,   n.   22,   perché,   sancendo   indiscriminatamente    &#13;
 l'indisponibilità di tutti  i  beni  degli  enti  ospedalieri  senza    &#13;
 escludere  il  denaro e, comunque, senza consentire l'accertamento di    &#13;
 una  sua  effettiva  destinazione  al  soddisfacimento  di   esigenze    &#13;
 connesse all'espletamento del pubblico servizio, violerebbe:             &#13;
       a)  l'art. 24 Cost., in quanto limiterebbe il diritto di difesa    &#13;
 del  creditore  che  abbia  ottenuto  una  sentenza  di  condanna  al    &#13;
 pagamento  di  una  determinata somma, non consentendogli, in caso di    &#13;
 inadempimento dell'ente, di eseguirla forzatamente;                      &#13;
       b) l'art. 3 Cost., per la disparità di trattamento che produce    &#13;
 fra enti ospedalieri toscani ed ogni altro  debitore  nonché  fra  i    &#13;
 creditori di tali enti e quelli di altro soggetto pubblico o privato.    &#13;
    1.1. - La questione non è fondata.                                   &#13;
    La   norma   impugnata   sancisce  la  indisponibilità  dei  beni    &#13;
 appartenenti  agli  enti  ospedalieri  ai  sensi  del  secondo  comma    &#13;
 dell'art.  828  cod.  civ., siccome si intendono destinati a pubblico    &#13;
 servizio ospedaliero.                                                    &#13;
    La  norma  del  codice  civile,  richiamata  ai fini della sancita    &#13;
 indisponibilità  dei  detti   beni,   riguarda   i   beni   immobili    &#13;
 appartenenti al patrimonio indisponibile dello Stato, i quali, per la    &#13;
 loro destinazione, possono essere sottratti alla  loro  destinazione,    &#13;
 in  via  generale,  solo  nei  modi  stabiliti  dalle  leggi  che  li    &#13;
 riguardano.                                                              &#13;
    Tra  i detti beni non possono essere ritenute comprese le somme di    &#13;
 denaro e i crediti pecuniari esistenti  nel  patrimonio  di  un  ente    &#13;
 pubblico. Secondo il costante indirizzo giurisprudenziale anche della    &#13;
 Corte di cassazione,  essi  rientrano  nel  patrimonio  indisponibile    &#13;
 dell'ente a norma dell'art. 828, secondo comma, cod. civ. solo quando    &#13;
 una espressa disposizione di legge o un provvedimento  amministrativo    &#13;
 dia  loro una univoca, precisa e concreta destinazione ad un servizio    &#13;
 pubblico,  cioè  all'esercizio  di  una  determinata  attività  per    &#13;
 l'attuazione   di   una   funzione  dell'ente  sia  direttamente  sia    &#13;
 strumentalmente  con  l'erogazione  della  spesa  per  le   strutture    &#13;
 necessarie  al fine dell'esercizio di quella attività. Essi vengono,    &#13;
 così, sottratti alla garanzia generica ex art. 2740  cod.  civ.  dei    &#13;
 creditori dell'ente.                                                     &#13;
    Ora,  la  norma  censurata,  siccome  generica  e  non riferentesi    &#13;
 specificamente ed univocamente alle somme di denaro,  non  è  idonea    &#13;
 per se stessa a sancirne l'indisponibilità.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 11 della legge della Regione Toscana 20 marzo 1975, n.  22,    &#13;
 sollevata,  in  riferimento  agli  artt. 3 e 24 Cost., dal Pretore di    &#13;
 Firenze con l'ordinanza in epigrafe.                                     &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, l'8 giugno 1988.           &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 10 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
