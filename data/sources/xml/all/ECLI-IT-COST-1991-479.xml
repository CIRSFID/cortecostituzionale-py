<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>479</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:479</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/12/1991</data_decisione>
    <data_deposito>19/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, &#13;
 prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, &#13;
 prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  304,  quarto    &#13;
 comma,  del  codice di procedura penale promosso con ordinanza emessa    &#13;
 il 18 marzo 1991 dal Tribunale di Bologna nel procedimento  penale  a    &#13;
 carico  di  Amato  Antonino ed altri, iscritta al n. 402 del registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 23, prima serie speciale, dell'anno 1991;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 20 novembre  1991  il  Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto che il Tribunale di Bologna, adito - ai sensi degli artt.    &#13;
 304,  terzo  comma, e 310 del codice di procedura penale - in sede di    &#13;
 appello avverso l'ordinanza con cui la locale  Corte  d'assise  aveva    &#13;
 disposto  la sospensione dei termini di durata massima della custodia    &#13;
 cautelare ex art. 304, secondo comma, del codice di procedura penale,    &#13;
 ha sollevato questione di legittimità costituzionale dell'art.  304,    &#13;
 quarto  comma,  del  medesimo codice, il quale fissa in due terzi del    &#13;
 massimo della pena prevista per il reato  contestato  o  ritenuto  in    &#13;
 sentenza  la  durata  insuperabile  della  custodia cautelare, tenuto    &#13;
 conto anche delle sospensioni dei termini eventualmente  disposte  ai    &#13;
 sensi dei commi precedenti dello stesso articolo;                        &#13;
      che,  ad  avviso  del  remittente,  la norma impugnata viola gli    &#13;
 artt. 76, 77 e 13, quinto comma, della Costituzione, "in entrambe  le    &#13;
 interpretazioni  possibili  dell'art.  2,  punto  61,  della legge n.    &#13;
 81/8/7";                                                                 &#13;
      che, in particolare, il giudice a quo osserva che, se  la  legge    &#13;
 di  delega, al punto 61, con la frase "previsione che in ogni caso la    &#13;
 durata massima della custodia in carcere, tenuto conto anche di tutte    &#13;
 le proroghe, non possa superare i quattro anni,  sino  alla  sentenza    &#13;
 definitiva",  intendesse  riferirsi  anche alla durata della custodia    &#13;
 tenuto conto della sospensione dei termini di  durata  massima  delle    &#13;
 misure  cautelari,  l'art. 304, quarto comma, del codice di procedura    &#13;
 penale sarebbe incostituzionale per violazione della norma delegante;    &#13;
      che se invece, prosegue il remittente, la legge di delega avesse    &#13;
 fissato soltanto il termine ultimo di custodia  tenendo  conto  delle    &#13;
 proroghe  senza nulla prevedere in riferimento alle sospensioni dello    &#13;
 stesso termine, la norma delegante (art. 2, punto 61, legge n. 81/87)    &#13;
 "potrebbe ritenersi viziata per  violazione  del  combinato  disposto    &#13;
 degli                                                                    &#13;
 artt.  13,  quinto  comma,  e  76,  della Costituzione, non avendo il    &#13;
 legislatore delegante fornito principi e criteri direttivi  idonei  a    &#13;
 stabilire  per legge i limiti massimi della carcerazione preventiva";    &#13;
 e pur avendo il  legislatore  delegato  introdotto  tale  limite  con    &#13;
 l'art.  304,  quarto  comma, del codice di procedura penale, "permane    &#13;
 dubbia  la   legittimità   costituzionale   di   questa   norma   in    &#13;
 considerazione  del  termine massimo, determinato in misura (anni 20)    &#13;
 tale da rischiare di vanificare di fatto la previsione costituzionale    &#13;
 dell'art. 13, quinto comma, ed in  considerazione  della  carenza  di    &#13;
 criteri direttivi in proposito nella legge delega";                      &#13;
      che  la  questione  è, infine, ritenuta rilevante in quanto "il    &#13;
 presente  procedimento   verte   proprio   sulla   legittimità   del    &#13;
 provvedimento   che   ha  dato  applicazione  alla  norma  della  cui    &#13;
 legittimità si dubita";                                                 &#13;
      che è intervenuto in giudizio il Presidente del  Consiglio  dei    &#13;
 ministri,  chiedendo  che  la  questione sia dichiarata inammissibile    &#13;
 (per contraddittorietà e perplessità  di  motivazione)  o  comunque    &#13;
 infondata.                                                               &#13;
    Considerato che il giudice a quo è stato adito dalla difesa degli    &#13;
 imputati,  ai sensi degli artt. 304, terzo comma, e 310 del codice di    &#13;
 procedura penale, quale giudice di appello avverso l'ordinanza  della    &#13;
 locale  corte  d'assise,  con  cui  è  stata  disposta, ex art. 304,    &#13;
 secondo  e  terzo comma, la sospensione dei termini di durata massima    &#13;
 della custodia cautelare durante il  tempo  in  cui  si  terranno  le    &#13;
 udienze e si delibererà la sentenza;                                    &#13;
      che  il  giudizio  a  quo  verte, pertanto, esclusivamente sulla    &#13;
 legittimità di tale provvedimento in relazione ai requisiti indicati    &#13;
 nel citato art. 304, secondo comma, del codice di procedura penale;      &#13;
      che, ciò posto, appare evidente la irrilevanza  della  proposta    &#13;
 questione di legittimità costituzionale dell'art. 304, quarto comma,    &#13;
 del  codice  di  procedura  penale  (che  fissa  il  termine  massimo    &#13;
 insuperabile  di  durata  della  custodia  cautelare,   comprese   le    &#13;
 eventuali sospensioni del termine stesso), in quanto non si comprende    &#13;
 quale nesso di pregiudizialità necessaria tale questione presenti ai    &#13;
 fini  della  decisione  demandata  al  remittente; né, del resto, la    &#13;
 motivazione dell'ordinanza di rimessione fornisce  alcun  chiarimento    &#13;
 al  riguardo,  ed  anzi  conferma indirettamente quanto ora detto là    &#13;
 dove, in punto di rilevanza,  il  giudice  a  quo,  con  affermazione    &#13;
 chiaramente  erronea,  dichiara  che  "il presente procedimento verte    &#13;
 proprio sulla legittimità del provvedimento che ha dato applicazione    &#13;
 alla norma della cui legittimità si dubita";                            &#13;
      che, in conclusione, la questione va  dichiarata  manifestamente    &#13;
 inammissibile.                                                           &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara  la  manifesta  inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 304, quarto comma, del codice    &#13;
 di procedura penale, sollevata, in riferimento agli artt. 13,  quinto    &#13;
 comma,  76  e  77  della  Costituzione,  dal Tribunale di Bologna con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 16 dicembre 1991.                             &#13;
                       Il presidente: CORASANITI                          &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 dicembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
