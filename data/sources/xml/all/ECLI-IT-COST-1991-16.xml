<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>16</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:16</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/01/1991</data_decisione>
    <data_deposito>18/01/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, dott. Francesco &#13;
 GRECO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco &#13;
 Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  26, primo    &#13;
 comma, lett. a), della legge  regionale  della  Lombardia  25  maggio    &#13;
 1983,  n.  44  (Destituzione  di  diritto  di  dipendente regionale a    &#13;
 seguito di condanna penale), promosso  con  ordinanza  emessa  il  29    &#13;
 marzo  1990 dal T.A.R. per la Lombardia sul ricorso proposto da Marro    &#13;
 Dante contro la Regione Lombardia, iscritta al n.  537  del  registro    &#13;
 ordinanze 1990 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 36, prima serie speciale dell'anno 1990;                              &#13;
     Udito  nella  camera di consiglio del 12 dicembre 1990 il Giudice    &#13;
 relatore Ettore Gallo;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ordinanza  29  marzo  1990 il Tribunale Amministrativo    &#13;
 Regionale per la Lombardia - Sezione terza - sollevava  questione  di    &#13;
 legittimità  costituzionale  dell'art.  26,  primo  comma, lett. a),    &#13;
 della legge regionale della Lombardia  25  maggio  1983,  n.  44,  in    &#13;
 riferimento agli artt. 3 e 97 della Costituzione.                        &#13;
     Riferiva  il  Tribunale  nell'ordinanza  che  il Presidente della    &#13;
 Regione aveva destituito, con decreto 18  marzo  1987,  n.  3304,  un    &#13;
 funzionario   della   Regione   perché   condannato,   con  sentenza    &#13;
 definitiva, per il delitto di truffa aggravata ai danni della Regione    &#13;
 stessa,  nonché  di  falso  ideologico  continuato in atto pubblico:    &#13;
 reati comportanti la destituzione di diritto, ai sensi  dell'art.  26    &#13;
 sopra citato.                                                            &#13;
     Ricorreva,  però,  il  funzionario  deducendo  come unico motivo    &#13;
 l'illegittimità  costituzionale  della  norma  regionale   posta   a    &#13;
 fondamento  della  sanzione  espulsiva, perché non prevede un previo    &#13;
 procedimento disciplinare. Il che rappresentava offesa  al  principio    &#13;
 di  cui  all'art.  3  della  Costituzione  per  carenza  di qualunque    &#13;
 criterio di adeguatezza di una sanzione unica e automatica alla varia    &#13;
 gravità e natura degl'illeciti elencati dalla norma impugnata.          &#13;
     D'altra  parte,  l'allontanamento  automatico  di  un dipendente,    &#13;
 senza alcuna  valutazione  della  rilevanza  disciplinare  del  fatto    &#13;
 commesso,  non  era  nemmeno  compatibile  con  il principio di buona    &#13;
 amministrazione di cui all'art. 97 della Costituzione.                   &#13;
     1.2.  -  Nelle  more  del  giudizio,  interveniva  la sentenza 14    &#13;
 ottobre 1988, n. 971, di questa Corte che dichiarava l'illegittimità    &#13;
 costituzionale  di  numerose  norme  statali  di  contenuto analogo a    &#13;
 quello di cui all'art. 26 impugnato, e proprio nella parte in cui non    &#13;
 prevedono l'apertura e lo svolgimento di un procedimento disciplinare    &#13;
 in luogo della destituzione di diritto.                                  &#13;
     A seguito di ciò, i legislatori statale e regionale adeguavano i    &#13;
 rispettivi ordinamenti alla detta pronuncia,  con  legge  7  febbraio    &#13;
 1990,  n.  19,  e  con  l.  reg.  13  febbraio  1990, n. 10, abolendo    &#13;
 l'istituto  della  destituzione  di  diritto  e  disponendo  che   il    &#13;
 provvedimento  di destituzione possa essere assunto solo a seguito di    &#13;
 procedimento disciplinare.                                               &#13;
     Ambo le leggi, poi, prevedevano che i dipendenti, precedentemente    &#13;
 destituiti di diritto,  potevano  essere  riammessi  in  servizio,  a    &#13;
 domanda, previo procedimento disciplinare.                               &#13;
    2.  -  Secondo  l'ordinanza,  tuttavia,  tali eventi non avrebbero    &#13;
 capacità  d'influenzare  il  presente  giudizio,   né   sul   piano    &#13;
 sostanziale né su quello processuale.                                   &#13;
     Non  sul  piano sostanziale, perché la citata sentenza di questa    &#13;
 Corte, pur riguardando norme di analogo tenore, come l'art. 85, lett.    &#13;
 a), del T.U. 10 gennaio 1957, n. 3, non ha travolto, però, l'art. 26    &#13;
 impugnato. Ben è vero che successivamente si è avuta  l'abrogazione    &#13;
 della  norma, ad opera della citata legge regionale n. 10 del 1990, e    &#13;
 prima ancora in virtù della richiamata legge statale n. 19 del 1990,    &#13;
 ma  queste  leggi  non  producono effetti se non "ex nunc". Di qui la    &#13;
 validità dei provvedimenti assunti sulla base della legge impugnata,    &#13;
 e quindi anche della destituzione de qua.                                &#13;
     Non   sul   piano   processuale   perché,  nonostante  la  norma    &#13;
 transitoria, la riammissione in servizio non consegue alla domanda ma    &#13;
 soltanto  all'esito  favorevole  del  giudizio  disciplinare:  mentre    &#13;
 l'eventuale  declaratoria  di  illegittimità  della  norma,  facendo    &#13;
 decadere  "ex  tunc"  il provvedimento destitutorio, consentirebbe al    &#13;
 ricorrente di riassumere servizio,  salvo  l'esito  del  procedimento    &#13;
 disciplinare.                                                            &#13;
     Si tratterebbe - conclude l'ordinanza - di un'utilità autonoma e    &#13;
 più ampia rispetto a quella assicurata dalla norma transitoria.         &#13;
     Nessuno  è  intervenuto  o si è costituito nel giudizio innanzi    &#13;
 alla Corte.<diritto>Considerato in diritto</diritto>1.  -  La  questione  sollevata è incentrata essenzialmente sulla    &#13;
 rilevanza della richiesta declaratoria d'illegittimità  della  norma    &#13;
 impugnata  a  fronte  di  una  legge  sopravvenuta, che non solo l'ha    &#13;
 abrogata, ma ha anche predisposto un complesso di  norme  transitorie    &#13;
 che consente a chi fosse stato destituito di diritto in precedenza di    &#13;
 poter  essere  riassunto,  a  domanda,  all'esito  favorevole  di  un    &#13;
 procedimento disciplinare.                                               &#13;
     2.   -   Come  osserva  esattamente  l'ordinanza  di  rimessione,    &#13;
 nonostante l'abrogazione della norma impugnata, persiste  l'interesse    &#13;
 del  ricorrente  ad  ottenere la dichiarazione d'illegittimità della    &#13;
 norma, che non è stata contemplata dalla sentenza 14  ottobre  1988,    &#13;
 n.   971,   di   questa   Corte.  Proprio  per  questo,  infatti,  il    &#13;
 provvedimento destitutorio assunto  dalla  Pubblica  amministrazione,    &#13;
 sulla  base  di  una norma che all'epoca era ancora vigente, conserva    &#13;
 tuttora  la  sua  validità  perché   l'abrogazione   della   norma,    &#13;
 sopravvenuta  in  epoca  successiva,  fa  cessare  la vigenza solo da    &#13;
 quest'ultimo momento, e  non  elimina,  perciò,  il  fondamento  del    &#13;
 provvedimento assunto in allora.                                         &#13;
     Ne consegue che, nonostante la citata sentenza di questa Corte, e    &#13;
 la successiva abrogazione della norma impugnata da parte della  legge    &#13;
 regionale,   il  ricorrente  conserva  la  situazione  soggettiva  di    &#13;
 "destituito di diritto", e la norma transitoria gli consente soltanto    &#13;
 di  "chiedere"  la  riammissione in servizio, ma non di ottenerla, se    &#13;
 non dopo avere favorevolmente superato il procedimento  disciplinare.    &#13;
 Procedimento  che, fra termine per iniziarlo e termine per definirlo,    &#13;
 può giungere a conclusione anche dopo 180 giorni, durante i quali il    &#13;
 ricorrente  resterebbe  pur sempre nella condizione di "destituito di    &#13;
 diritto".                                                                &#13;
     Al  contrario,  ove  la  norma  fosse  dalla Corte delegittimata,    &#13;
 cadrebbe al contempo "ex tunc" il provvedimento  assunto  sulla  base    &#13;
 della  norma dichiarata illegittima, e il ricorrente si troverebbe di    &#13;
 nuovo automaticamente in servizio, salva l'iniziativa della  pubblica    &#13;
 amministrazione in ordine al procedimento disciplinare.                  &#13;
     Quand'anche  fosse  applicabile nei suoi confronti il terzo comma    &#13;
 dell'art. 2 della legge reg.  Lombardia  13  febbraio  1990,  n.  10,    &#13;
 secondo cui, quando vi sia stata sospensione cautelare dal servizio a    &#13;
 causa del procedimento penale  (e,  nella  specie,  c'è  stata),  la    &#13;
 stessa  conserva  efficacia,  se  non  revocata, ciò non influirebbe    &#13;
 sulla posizione giuridica di impiegato in  costanza  di  rapporto  di    &#13;
 servizio,  sia  pure  sospeso, sicuramente più favorevole rispetto a    &#13;
 quella di impiegato destituito.  La  rilevanza,  pertanto,  non  può    &#13;
 essere negata.                                                           &#13;
     3.  - Nel merito, non può esservi dubbio che la norma denunciata    &#13;
 presenti gli stessi aspetti di incompatibilità, rispetto all'art.  3    &#13;
 della  Costituzione,  che  questa  Corte  ha  rilevato  nei  riguardi    &#13;
 dell'art. 85, lett. a), d.P.R. 10 gennaio 1957, n. 3  (Statuto  degli    &#13;
 impiegati  civili  dello  Stato), e delle altre norme di cui è stata    &#13;
 dichiarata l'illegittimità costituzionale con la citata sentenza  14    &#13;
 ottobre  1988,  n.  971, peraltro ribadita con la sentenza 31 gennaio    &#13;
 1990   n.   40.   In   ambo   le   decisioni,   la   nota   dominante    &#13;
 dell'incompatibilità     costituzionale     è    stata    ravvisata    &#13;
 nell'automatismo della sanzione della  "destituzione"  che  colpisce,    &#13;
 senza   alcuna   distinzione,   la  molteplicità  dei  comportamenti    &#13;
 possibili nell'area dello stesso illecito penale. Il che offende quel    &#13;
 "principio  di  proporzione"  che è alla base della razionalità che    &#13;
 domina "il principio di eguaglianza",  e  che  postula  l'adeguatezza    &#13;
 della  sanzione  al  caso  concreto.  Adeguatezza che non può essere    &#13;
 raggiunta  se  non  attraverso   la   valutazione   degli   specifici    &#13;
 comportamenti   messi   in   atto   nella  commissione  dell'illecito    &#13;
 amministrativo, che soltanto il procedimento disciplinare consente.      &#13;
    L'automatismo  della  massima sanzione è, perciò, la causa prima    &#13;
 dell'illegittimità della  norma  in  riferimento  all'art.  3  della    &#13;
 Costituzione.  La  violazione  di questo parametro, d'altra parte, è    &#13;
 assorbente della incompatibilità che l'ordinanza denuncia  anche  in    &#13;
 riferimento all'art. 97 della Costituzione, giacché, in questi casi,    &#13;
 alla base dell'offesa ai valori di  imparzialità  e  buon  andamento    &#13;
 della pubblica amministrazione c'è pur sempre quell'inadeguatezza al    &#13;
 caso  concreto  che  sostanzia  la  violazione   del   principio   di    &#13;
 eguaglianza.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara l'illegittimità costituzionale dell'art. 26, primo comma,    &#13;
 lett. a), della legge regionale della Lombardia 25  maggio  1983,  n.    &#13;
 44.                                                                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 gennaio 1991.                               &#13;
                          Il Presidente: CONSO                            &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 gennaio 1991.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
