<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>218</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:218</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>06/06/2007</data_decisione>
    <data_deposito>18/06/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 33, comma 7, lettera a), della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), promosso dal Tribunale amministrativo regionale per la Campania, sede di Napoli, sul ricorso proposto da A. R. contro l'Ufficio Territoriale del Governo di Napoli, con ordinanza del 7 novembre 2005, iscritta al n. 598 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 1, prima serie speciale, dell'anno 2007. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 4 giugno 2007 il Giudice relatore Francesco Amirante. &#13;
    Ritenuto che, nel corso di un giudizio relativo all'impugnativa di un provvedimento prefettizio di rigetto della domanda presentata da una datrice di lavoro al fine di legalizzare un rapporto di lavoro irregolare con un dipendente di nazionalità pachistana, il Tribunale amministrativo regionale per la Campania, sede di Napoli, con ordinanza del 7 novembre 2005 (pervenuta a questa Corte il 20 novembre 2006), ha sollevato, in riferimento all'art. 3, primo comma, della Costituzione, questione di legittimità costituzionale dell'art. 33, comma 7, lettera a), della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), nella parte in cui esclude automaticamente dalla legalizzazione del lavoro irregolare i cittadini extracomunitari che siano stati destinatari di un provvedimento di espulsione da eseguire con accompagnamento alla frontiera a mezzo della forza pubblica; &#13;
    che, nella specie, il ricorrente è stato in precedenza espulso con accompagnamento alla frontiera (con provvedimento del 22 novembre 2001) per essere entrato nel territorio nazionale sottraendosi ai controlli di frontiera e sul presupposto della sussistenza del concreto pericolo di sottrazione all'esecuzione del provvedimento espulsivo, non risultando «inserito in un contesto familiare, sociale e lavorativo»; &#13;
    che non avendo la questura specificato se il provvedimento espulsivo, dopo la notificazione, sia stato effettivamente eseguito, non sussiste l'ulteriore causa ostativa alla relativa revoca costituita dal rientro non autorizzato nel territorio nazionale da parte dei soggetti precedentemente espulsi (che configura anche il reato di cui all'art. 13, comma 13, del decreto legislativo 25 luglio 1998, n. 286); &#13;
    che il giudice remittente, dopo aver richiamato alcune precedenti decisioni di questa Corte in materia, sostiene, in particolare, come valutazioni analoghe a quelle poste a fondamento della sentenza n. 78 del 2005 potrebbero indurre questa Corte ad accogliere l'attuale questione, in quanto anche in questo caso la pericolosità sociale del soggetto viene automaticamente desunta da un elemento inidoneo allo scopo e, cioè, dalla circostanza che lo stesso sia stato destinatario di un provvedimento di espulsione adottato con la modalità dell'accompagnamento coattivo alla frontiera; &#13;
    che, ad avviso del giudice a quo, la disposizione di cui si tratta sarebbe inoltre in contrasto con il principio di uguaglianza in quanto, del tutto irragionevolmente, riserva il medesimo trattamento a soggetti che si trovano in situazioni diverse, accomunando lavoratori colpiti da decreti di espulsione con accompagnamento alla frontiera per motivi di ordine pubblico o sicurezza dello Stato o perché ritenuti socialmente pericolosi a coloro che siano destinatari di analoghi provvedimenti esclusivamente per essersi trattenuti nel territorio nazionale oltre il prescritto termine dall'intimazione dell'espulsione ovvero – come il ricorrente – per essere entrati clandestinamente in Italia privi di un valido documento di identità, ma senza essere concretamente pericolosi per la sicurezza pubblica e senza aver riportato condanne penali; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per l'inammissibilità o, comunque, l'infondatezza della questione, in quanto essa si risolverebbe nel chiedere una valutazione circa il merito di scelte discrezionali del legislatore che appaiono del tutto ragionevoli.  &#13;
    Considerato che il Tribunale amministrativo regionale per la Campania, sede di Napoli, dubita, in riferimento all'art. 3, primo comma, Cost., della legittimità costituzionale dell'art. 33, comma 7, lettera a), della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), nella parte in cui esclude automaticamente dalla legalizzazione del lavoro irregolare i cittadini extracomunitari che siano stati destinatari di un provvedimento di espulsione da eseguire con accompagnamento alla frontiera a mezzo della forza pubblica; &#13;
    che, ad avviso del giudice a quo, la disposizione censurata violerebbe l'invocato parametro costituzionale sia perché, del tutto irragionevolmente, stabilisce lo stesso trattamento per situazioni sostanzialmente diverse, sia per intrinseca irragionevolezza; &#13;
    che, come già precisato da questa Corte (si veda, per tutte, l'ordinanza n. 126 del 2005), la disposizione stessa contiene, con riferimento ai cosiddetti badanti e lavoratori domestici, una norma uguale a quella dettata, per i dipendenti di imprese, dall'art. 1, comma 8, lettera a), del decreto-legge 9 settembre 2002, n. 195 (Disposizioni urgenti in materia di legalizzazione del lavoro irregolare di extracomunitari), convertito, con modificazioni, dalla legge 9 ottobre 2002, n. 222; &#13;
    che, con riguardo a tale ultima disposizione, con la sentenza n. 206 del 2006 e con l'ordinanza n. 44 del 2007 sono state, rispettivamente, dichiarate non fondata e manifestamente infondata questioni di costituzionalità analoghe a quella attualmente proposta; &#13;
    che nelle suddette pronunce – dopo aver evidenziato che la disposizione censurata si riferisce alla legalizzazione dei rapporti di lavoro intrattenuti da cittadini extracomunitari in epoca antecedente l'entrata in vigore della legge 30 luglio 2002, n. 189, la quale ha sensibilmente modificato la disciplina dell'espulsione amministrativa – si è sottolineato come, in riferimento a tale pregresso quadro normativo, l'espulsione amministrativa venisse di regola eseguita con intimazione all'interessato ad abbandonare il territorio dello Stato e non tramite accompagnamento coattivo alla frontiera, sicché questa seconda modalità di esecuzione, correlata non «a lievi irregolarità amministrative ma alla situazione di coloro che avessero già dimostrato la pervicace volontà di rimanere in Italia in una posizione di irregolarità», non irragionevolmente implica il divieto di sanatoria della relativa posizione di lavoro; &#13;
    che il TAR per la Campania non sottopone alla Corte alcuna argomentazione diversa ed ulteriore rispetto a quelle già scrutinate nelle menzionate decisioni; &#13;
    che la presente questione, pertanto, deve essere dichiarata manifestamente infondata. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 33, comma 7, lettera a), della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), sollevata, in riferimento all'art. 3, primo comma, della Costituzione, dal Tribunale amministrativo regionale per la Campania, sede di Napoli, con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 giugno 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Francesco AMIRANTE, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 giugno 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
