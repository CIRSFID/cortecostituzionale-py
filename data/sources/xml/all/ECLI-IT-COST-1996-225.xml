<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>225</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:225</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/06/1996</data_decisione>
    <data_deposito>03/07/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  1,  comma    &#13;
 quarto,  lettera  a),  della  legge  7  agosto  1982,  n.  516,  come    &#13;
 modificato  dalla legge 15 maggio 1991, n. 151 promosso con ordinanza    &#13;
 emessa il 27 settembre 1995 dal Tribunale di Verona nel  procedimento    &#13;
 penale  a  carico  di  Marangoni  Fabrizio,  iscritta  al  n. 853 del    &#13;
 registro ordinanze 1995 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 5, prima serie speciale, dell'anno 1995.                   &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  29 maggio 1996 il giudice    &#13;
 relatore Renato Granata.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - In un  giudizio  penale  a  carico  di  un  amministratore  di    &#13;
 società  chiamato a rispondere della contravvenzione di cui all'art.    &#13;
 1, secondo comma, lettere a) e b) della legge 7 agosto 1982, n.  516,    &#13;
 per   omessa   annotazione   di   ricavi  nelle  scritture  contabili    &#13;
 obbligatorie ai fini delle imposte dirette e dell'IVA,  il  Tribunale    &#13;
 di  Verona procedente - rilevato che dalla ispezione della Guardia di    &#13;
 finanza era risultato che il contribuente "aveva comunque  conservato    &#13;
 tutta la documentazione relativa alle operazioni commerciali attive e    &#13;
 passive"  per  cui  ricorreva  nei  suoi confronti la prima delle tre    &#13;
 condizioni   (possesso    di    documentazione    alternativa    alle    &#13;
 scritturazioni  omesse)  per  l'applicazione  dell'esimente di cui al    &#13;
 comma quarto, lettera a), del medesimo art. 1 della legge n. 516  del    &#13;
 1982,  mentre le altre due condizioni, ivi a tal fine pure prescritte    &#13;
 (inclusione dei corrispettivi non annotati  nella  dichiarazione  dei    &#13;
 redditi  e  versamento  della  correlativa imposta), "non potevano de    &#13;
 iure  sussistere  per   la   casuale   circostanza   dell'intervenuto    &#13;
 accertamento fiscale in epoca anteriore a quella prevista per il loro    &#13;
 assolvimento";  e  considerato, altresì, che  la norma di favore non    &#13;
 poteva essere, a suo avviso, correttivamente interpretata  nel  senso    &#13;
 della  sufficienza  del primo requisito nell'ipotesi considerata - ha    &#13;
 ritenuto di conseguenza rilevante, e non manifestamente infondata  in    &#13;
 riferimento all'art.  3 della Costituzione, per cui ha sollevato, con    &#13;
 ordinanza   del   27   settembre   1995,   questione  incidentale  di    &#13;
 legittimità  costituzionale  del  predetto  art.  1,  comma  quarto,    &#13;
 lettera  a)  della  legge n. 516 del 1982 (nel testo modificato dalla    &#13;
 legge  n.  151  del  1991),  nella  parte  in  cui  non  consente  al    &#13;
 contribuente  di  fruire  della riferita esimente, pur in presenza di    &#13;
 annotazioni alternative,  in  caso  di  accertamento  dell'infrazione    &#13;
 prima  del termine di presentazione della dichiarazione dei redditi e    &#13;
 del versamento d'imposta.                                                &#13;
   Deriverebbe infatti - ad avviso del Tribunale - dalla  norma  così    &#13;
 censurata   una   "macroscopica   ed   irragionevole   disparità  di    &#13;
 trattamento in danno del contribuente nei cui confronti venga operato    &#13;
 un accertamento della Guardia di finanza  prima  della  scadenza  del    &#13;
 suddetto  termine  annuale rispetto al contribuente nei cui confronti    &#13;
 la verifica intervenga invece successivamente: il primo, non  potendo    &#13;
 fruire  dell'esimente,  si  vedrebbe colpito da sanzione penale, alla    &#13;
 quale sfuggirebbe il secondo, essendo questi in grado di  esibire  la    &#13;
 dichiarazione  e la prova dell'avvenuto versamento, e ciò in base ad    &#13;
 una circostanza di fatto del tutto casuale (epoca  della  verifica)".    &#13;
 Per  cui  appunto  "identiche  situazioni,  conseguenti  ad identiche    &#13;
 condotte,  subirebbero  trattamenti  processuali   differenziati   in    &#13;
 dipendenza  dell'essere  un  accertamento casualmente avvenuto in una    &#13;
 data piuttosto che in un'altra".                                         &#13;
   2. - Nel giudizio innanzi alla Corte, è intervenuto il  Presidente    &#13;
 del  Consiglio  dei  ministri  per  il  tramite dell'Avvocatura dello    &#13;
 Stato, che ha contestato la fondatezza  della  questione,  in  quanto    &#13;
 basata su una non corretta interpretazione della norma denunciata.<diritto>Considerato in diritto</diritto>1.  -  Con riguardo alle fattispecie di reato omissivo previste dal    &#13;
 comma secondo lettere a) e b), dell'art. 1 della legge 7 agosto 1982,    &#13;
 n. 516 (omessa annotazione e/o fatturazione di  cessione  di  beni  o    &#13;
 prestazione  di  servizi  nelle  scritture  contabili obbligatorie ai    &#13;
 fini,  rispettivamente,  delle  imposte  sui  redditi e dell'IVA), il    &#13;
 successivo comma quarto della stessa norma (come modificato dall'art.    &#13;
 1 del decreto-legge 16 marzo 1991,  n.  83  convertito  in  legge  15    &#13;
 maggio  1991,  n. 151) testualmente dispone, sub lettera a), che "...    &#13;
 non si  considerano  omesse  le  annotazioni  e  le  fatturazioni  di    &#13;
 corrispettivi  purché  ...  le annotazioni siano state effettuate in    &#13;
 talune  delle  scritture  contabili  indicate  nel  comma  6   (libro    &#13;
 giornale; libro degli inventari; registro delle fatture ...) o i dati    &#13;
 delle   operazioni   risultino   da  documenti  la  cui  emissione  e    &#13;
 conservazione è obbligatoria a norma di legge e i corrispettivi  non    &#13;
 annotati  o  non  fatturati  (nelle  scritture  di  cui  al  comma 2)    &#13;
 risultino altresì compresi nella relativa  dichiarazione  annuale  e    &#13;
 sia   versata  l'imposta  globalmente  dovuta".    Ed  aggiunge:  "le    &#13;
 annotazioni devono essere effettuate  o  i  documenti  devono  essere    &#13;
 emessi  prima  che  la  violazione  sia  stata  constata  e che siano    &#13;
 iniziate ispezioni o verifiche".                                         &#13;
   2. - Il Tribunale di Verona dubita -  come  in  narrativa  detto  -    &#13;
 della  legittimità  di  tale  norma, e ne prospetta il contrasto con    &#13;
 l'art.  3  della  Costituzione,  per  l'irragionevole  disparità  di    &#13;
 trattamento   che,  a  suo  avviso,  ne  deriverebbe  "in  danno  dei    &#13;
 contribuenti nei cui  confronti  sia  stato  effettuato  accertamento    &#13;
 fiscale,  ispezione o verifica prima del termine per la dichiarazione    &#13;
 dei redditi ed il versamento  d'imposta",  i  quali  solo  "per  tale    &#13;
 casuale   circostanza",   ove   pur  in  possesso  di  documentazione    &#13;
 alternativa  alle  annotazioni  fiscali   omesse,   si   troverebbero    &#13;
 nell'impossibilità,  comunque,  di  avvalersi  dell'esimente  di cui    &#13;
 sopra.                                                                   &#13;
   3. - La questione è infondata per la erroneità della premessa  da    &#13;
 cui muove.                                                               &#13;
   3.1. - Nella presupposta lettura della norma denunciata, il giudice    &#13;
 a  quo  dà,  infatti,  per  scontato  che  l'intervento degli organi    &#13;
 accertatori determini la paralisi della causa di non punibilità, non    &#13;
 consentendo al contribuente di poter più  utilmente,  a  quei  fini,    &#13;
 invocare  la  successiva  fedele dichiarazione dei redditi e l'esatto    &#13;
 pagamento del tributo.                                                   &#13;
   E da ciò inferisce la violazione  del  precetto  dell'eguaglianza,    &#13;
 per  la  "casualità", appunto, della circostanza suscettibile in tal    &#13;
 guisa di precludere al contribuente l'integrale  realizzazione  della    &#13;
 fattispecie giustificativa.                                              &#13;
   3.2. - Una tale esegesi è però arbitraria, non trovando riscontro    &#13;
 né nella lettera né nella ratio legis.                                 &#13;
   L'autorità   rimettente   trascura   infatti  in  primo  luogo  di    &#13;
 considerare che la condizione di anteriorità, all'accertamento degli    &#13;
 organi ispettivi, è espressamente ed inequivocabilmente  riferita  -    &#13;
 nel  (sopra trascritto) periodo finale del comma quarto dell'articolo    &#13;
 citato - alla sola predisposizione della  documentazione  sostitutiva    &#13;
 (di  quella  fiscalmente  obbligatoria)  e  non  anche agli altri due    &#13;
 elementi - della  fedele  dichiarazione  dei  cespiti  e  dell'esatto    &#13;
 pagamento  del  tributo  -  che  concorrono  a definire il quadro dei    &#13;
 presupposti dell'esimente in parola.                                     &#13;
   Per cui già in base al solo canone della interpretazione letterale    &#13;
 è dato evincere dalla predetta disposizione come il contribuente, in    &#13;
 possesso  di  quella  alternativa   documentazione   all'atto   della    &#13;
 ispezione   fiscale,  ben  possa  -  contrariamente  all'assunto  del    &#13;
 Tribunale  -  avvalersi  comunque  della  esimente ove ne completi in    &#13;
 prosieguo le condizioni applicative, con  la  puntuale  dichiarazione    &#13;
 dei  cespiti  e  l'integrale  pagamento  del tributo, alle prescritte    &#13;
 scadenze.                                                                &#13;
   3.3.  -  Anche  la  considerazione  della   ratio   legis   conduce    &#13;
 all'identico risultato ermeneutico.                                      &#13;
   Le cause di giustificazione od esimenti (tra cui quella sub lettera    &#13;
 a)  previste nel comma quarto dell'art. 1 della legge n. 516 del 1982    &#13;
 rispondono  infatti  ad  una  logica  premiale  nei   confronti   del    &#13;
 contribuente  che, recedendo dall'iter criminis (avviato con l'omessa    &#13;
 annotazione di corrispettivi nelle scritture  contabili  obbligatorie    &#13;
 ai   fini   fiscali),   faccia   spontaneamente   comunque   emergere    &#13;
 l'irregolarità.                                                         &#13;
   Ed in  questa  prospettiva  resta  evidentemente  fermo  il  valore    &#13;
 esimente  di  condotte che, all'atto dell'accertamento, si presentino    &#13;
 appunto orientate in direzione dell'interruzione  della  progressione    &#13;
 criminosa,  mentre  coerentemente  la  valenza impeditiva dell'azione    &#13;
 accertatrice  si  dispiega  quando,   in   carenza   della   suddetta    &#13;
 documentazione  alternativa,  l'omissione della infrazione tributaria    &#13;
 unicamente dipende dall'attività ispettiva dell'amministrazione.        &#13;
   3.4. - Diversamente - nella successiva ipotesi esimente sub lettera    &#13;
 b) del medesimo comma quarto dell'art. 1 della legge n. 516 del 1982,    &#13;
 invocabile  dal  contribuente,   non   in   possesso   di   documenti    &#13;
 sostitutivi,  che  abbia  comunque  fedelmente  dichiarato  i  propri    &#13;
 cespiti e versato  la  correlativa  imposta  -  è  bensì  richiesta    &#13;
 l'anteriorità,   di   tali   dichiarazione  e  versamento,  rispetto    &#13;
 all'eventuale accertamento fiscale, ma ciò  proprio  in  quanto,  in    &#13;
 mancanza  di  ogni  annotazione  anche  irrituale  dei  corrispettivi    &#13;
 percepiti, non potrebbe, in tal caso,  altrimenti  evidenziarsi  quel    &#13;
 ravvedimento  spontaneo  ed  attivo  che  costituisce il fondamento e    &#13;
 l'obiettivo della disposizione premiale.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  1, comma quarto, lettera a), della legge 7 agosto 1982, n.    &#13;
 516 nel  testo  modificato  dalla  legge  15  maggio  1991,  n.  151,    &#13;
 sollevata in riferimento all'art. 3 della Costituzione, dal tribunale    &#13;
 di Verona, con l'ordinanza in epigrafe.                                  &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 giugno 1996.                               &#13;
                         Il Presidente: Ferri                             &#13;
                        Il redattore:  Granata                            &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 3 luglio 1996.                            &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
