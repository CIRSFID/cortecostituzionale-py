<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>566</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:566</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/05/1988</data_decisione>
    <data_deposito>19/05/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio promosso con ricorso della Regione Sicilia notificato il    &#13;
 4 maggio 1979, depositato in Cancelleria il 10 maggio  successivo  ed    &#13;
 iscritto  al  n.  13  del  registro  ricorsi  1979,  per conflitto di    &#13;
 attribuzione sorto a seguito del decreto del Ministro  per  i  Lavori    &#13;
 Pubblici  16 ottobre 1978, n. 4920, con il quale sono state approvate    &#13;
 le  deliberazioni  in  data  10  settembre  1977  e  30  giugno  1978    &#13;
 dell'Istituto  Autonomo  Case  Popolari  di  Acireale (iscrizione del    &#13;
 personale alla Cassa pensione dei dipendenti degli Enti locali).         &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza pubblica dell'8 marzo 1988 il Giudice relatore    &#13;
 Gabriele Pescatore;                                                      &#13;
    Udito  l'avv. Guido Aula per la Regione Sicilia e l'Avvocato dello    &#13;
 Stato Giorgio Azzariti per il Presidente del Consiglio dei  ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - La Regione siciliana, con ricorso notificato il 4 maggio 1979    &#13;
 al Presidente del Consiglio dei Ministri, ha  proposto  conflitto  di    &#13;
 attribuzione avverso il decreto del Ministro per i lavori pubblici 16    &#13;
 ottobre 1978, n. 4920, emanato di concerto con  il  Ministro  per  il    &#13;
 tesoro  e  con il Ministro del lavoro e della previdenza sociale, con    &#13;
 cui furono approvate le deliberazioni 10 settembre 1977 e  30  giugno    &#13;
 1978,  con  le  quali  l'Istituto  autonomo  per  le case popolari di    &#13;
 Acireale aveva stabilito  l'iscrizione  del  proprio  personale  alla    &#13;
 Cassa pensioni dei dipendenti degli enti locali.                         &#13;
    Nel  ricorso  si  deduce  che  il  decreto impugnato fu emanato in    &#13;
 violazione degli artt. 20 e 43 dello Statuto della Regione siciliana,    &#13;
 nonché  degli  artt. 4 e 5 del d.P.R. 1° luglio 1977, n. 683. Questi    &#13;
 ultimi hanno sostituito gli artt. 5 e 6 del d.P.R. 30 luglio 1950, n.    &#13;
 878  (recante  norme di attuazione dello Statuto siciliano in materia    &#13;
 di  opere  pubbliche)  stabilendo  che  "la   regione   esercita   le    &#13;
 attribuzioni dell'amministrazione dello Stato nelle materie attinenti    &#13;
 all'edilizia economica e popolare" (art. 5, comma primo) e che  "sono    &#13;
 esercitate  dalla  regione  le  funzioni amministrative, ivi comprese    &#13;
 quelle di vigilanza e di  tutela,  svolte  dagli  organi  centrali  e    &#13;
 periferici  dello  Stato,  in ordine agli enti, consorzi, istituti ed    &#13;
 organizzazioni, operanti esclusivamente in Sicilia nelle  materie  di    &#13;
 cui al presente decreto" (art. 6, primo comma).                          &#13;
    Pertanto,  in  base  ad  essi  - secondo la Regione siciliana - il    &#13;
 controllo sugli Istituti autonomi per le case popolari che operano in    &#13;
 Sicilia,   è   di   competenza   dell'Amministrazione  regionale  e,    &#13;
 nell'ambito  di  questa,  dell'Assessore  regionale  per   i   lavori    &#13;
 pubblici.  Conseguentemente  l'approvazione delle deliberazioni degli    &#13;
 I.A.C.P.,  operanti  in   Sicilia,   riguardanti   l'iscrizione   del    &#13;
 personale,  alla  Cassa  per  le  pensioni  ai  dipendenti degli enti    &#13;
 locali, rientrerebbe nella competenza dell'Assessore regionale per  i    &#13;
 lavori pubblici e non in quella del Ministro per i lavori pubblici il    &#13;
 cui decreto sarebbe quindi illegittimo.                                  &#13;
    Nel  ricorso  si deduce anche la violazione dell'art. 17, lett. f)    &#13;
 dello Statuto siciliano e delle norme  di  attuazione  approvate  con    &#13;
 d.P.R.  25  giugno  1952,  n.  1138,  come  modificate  dal d.P.R. 16    &#13;
 febbraio 1979, n. 76.                                                    &#13;
    In  proposito  la  regione espone che l'art. 39 della l. 11 aprile    &#13;
 1955, n. 379 prevedeva che  il  decreto  col  quale  si  esercita  il    &#13;
 controllo  sull'ente  che  adotta  la  deliberazione  di  massima che    &#13;
 stabilisce l'iscrizione ad una delle casse indicate nel  primo  comma    &#13;
 dello  stesso  articolo,  deve  essere  emanato  "di  concerto con il    &#13;
 Ministro per il tesoro ed il Ministro  del  lavoro  e  la  previdenza    &#13;
 sociale".  Per  quanto  concerne il Ministero per il tesoro, la Corte    &#13;
 costituzionale ha affermato che, nelle materie di competenza di  tale    &#13;
 Ministro,  lo  Statuto  siciliano non attribuisce alla regione alcuna    &#13;
 potestà legislativa, neppure concorrente (sentenze 16  luglio  1968,    &#13;
 n. 105 e 15 luglio 1969, n. 127). Pertanto, la regione sarebbe tenuta    &#13;
 ad un'intesa con detto ministero  (Corte  cost.  sentt.  30  dicembre    &#13;
 1958,  n.  82;  19  dicembre  1959,  n.  65;  15 giugno 1972, n. 104)    &#13;
 sull'emanazione del decreto in questione.                                &#13;
    Viceversa, poiché nella materia oggetto di tale decreto, ai sensi    &#13;
 dell'art. 17 lettera f), dello Statuto le attribuzioni  del  ministro    &#13;
 per   il  lavoro  e  la  previdenza  sociale  sono  state  trasferite    &#13;
 all'Amministrazione regionale siciliana (con le norme  di  attuazione    &#13;
 approvate con d.P.R. 25 giugno 1952, n. 1138, modificate ed integrate    &#13;
 con il d.P.R. 16 febbraio 1979, n. 76),  il  concerto,  in  relazione    &#13;
 alla  competenza  di  detto  Ministro,  nell'emanazione  del decreto,    &#13;
 doveva intervenire tra l'Assessore regionale per i lavori pubblici  e    &#13;
 l'Assessore regionale per il lavoro e la previdenza sociale.             &#13;
    La  regione  ha  chiesto,  pertanto,  che  questa Corte annulli il    &#13;
 decreto impugnato dichiarando che: a) esso  è  illegittimo;  b)  che    &#13;
 spetta  alla Regione siciliana e, per essa, all'Assessorato regionale    &#13;
 per i lavori pubblici, il controllo sugli Istituti  autonomi  per  le    &#13;
 case  popolari  che operano in Sicilia e, in conseguenza, di adottare    &#13;
 il provvedimento di approvazione  delle  deliberazioni  dell'Istituto    &#13;
 autonomo  per  la case popolari di Acireale, concernenti l'iscrizione    &#13;
 del personale alla Cassa pensioni per i dipendenti da enti locali; c)    &#13;
 che  il  provvedimento dell'Assessore regionale per i lavori pubblici    &#13;
 deve essere adottato, previa intesa con il Ministro  per  il  tesoro,    &#13;
 con  il  "concerto"  dell'Assessore  regionale  per  il  lavoro  e la    &#13;
 previdenza sociale.                                                      &#13;
    2.  -  Dinanzi  a  questa Corte si è costituito il Presidente del    &#13;
 Consiglio dei Ministri chiedendo che il ricorso sia respinto.            &#13;
    Nell'atto  di  costituzione  l'Avvocatura  generale dello Stato ha    &#13;
 dedotto al riguardo che  esso  è  infondato  poiché  l'approvazione    &#13;
 prevista  dall'art.  39  della legge 11 aprile 1955 sugli ordinamenti    &#13;
 degli Istituti di previdenza  presso  il  Ministero  del  Tesoro  non    &#13;
 costituisce  atto  di  controllo  sull'esercizio,  da parte dell'ente    &#13;
 pubblico,  della  sua  attività  istituzionale.   Il   provvedimento    &#13;
 impugnato,  perciò,  non  sarebbe  legittimo,  non  costituendo "né    &#13;
 esercizio diretto di attribuzioni,  né  esercizio  di  controllo  in    &#13;
 materia di edilizia economica e popolare".<diritto>Considerato in diritto</diritto>3. - Ha dato origine al conflitto promosso dalla Regione siciliana    &#13;
 il decreto n. 4920 del 16 ottobre 1978, emesso  dal  Ministro  per  i    &#13;
 lavori  pubblici,  di concerto con il Ministro per il tesoro e con il    &#13;
 Ministro per il lavoro e la previdenza sociale,  con  il  quale  sono    &#13;
 state  approvate le deliberazioni 10 settembre 1977 e 30 giugno 1978,    &#13;
 adottate dall'Istituto autonomo per le  case  popolari  di  Acireale,    &#13;
 relative all'iscrizione del proprio personale alla Cassa pensioni dei    &#13;
 dipendenti degli enti locali.                                            &#13;
   Non   è   fondata   la   censura  di  invasione  operata  da  tale    &#13;
 provvedimento  nelle  attribuzioni  statutariamente  conferite   alla    &#13;
 regione dagli artt. 43 e 20, in relazione all'art. 14 lett. g), dello    &#13;
 Statuto della Regione siciliana, e dagli artt. 5 e 6 d.P.R. 30 luglio    &#13;
 1950,  n. 878 (Norme di attuazione dello Statuto della regione stessa    &#13;
 in materia di opere pubbliche), come sostituiti dagli  artt.  4  e  5    &#13;
 d.P.R. 1° luglio 1977, n. 683.                                           &#13;
    4.  -  Lo  Statuto per la Regione siciliana configura come materia    &#13;
 oggetto di legislazione esclusiva della regione  quella  relativa  ai    &#13;
 lavori  pubblici  (art.  14,  primo  comma,  lett. f) e ne demanda le    &#13;
 funzioni esecutive ed amministrative al Presidente ed agli  Assessori    &#13;
 regionali (art. 20, primo comma). Gli artt. 5 e 6, comma primo, delle    &#13;
 ricordate norme di attuazione, come sostituite dai citati artt. 4 e 5    &#13;
 del   d.P.R.   n.   683  del  1977,  conferiscono  alla  regione  "le    &#13;
 attribuzioni dell'amministrazione dello Stato nelle materie attinenti    &#13;
 all'edilizia economica e popolare" e "le funzioni amministrative, ivi    &#13;
 comprese quelle di vigilanza e di tutela svolte dagli organi centrali    &#13;
 e  periferici  dello Stato in ordine agli enti, consorzi, istituti ed    &#13;
 organizzazioni, operanti esclusivante in Sicilia nella materia di cui    &#13;
 al presente decreto".                                                    &#13;
    Appare  indubbia,  sulla base di queste norme, la devoluzione alla    &#13;
 Regione siciliana di una competenza, qualificabile per l'oggetto come    &#13;
 specificazione  della  materia  globalmente  designata  dallo Statuto    &#13;
 "lavori pubblici" (art. 14 lett. g),  alla  stregua  delle  norme  di    &#13;
 attuazione  dello  Statuto  stesso  ("materie  attinenti all'edilizia    &#13;
 economica e popolare o comunque sovvenzionata": art. 4, primo  comma,    &#13;
 d.P.R. n. 683 del 1977 cit).                                             &#13;
    Senonché  il  provvedimento  ministeriale,  nel  quale la regione    &#13;
 riscontra la  violazione  delle  sue  competenze,  non  attiene  alla    &#13;
 materia afferente a tale edilizia in senso proprio, anche se tocca un    &#13;
 momento del trattamento del personale dipendente da istituto autonomo    &#13;
 delle  case  popolari.  Si  tratta  invero della iscrizione di questo    &#13;
 personale alla Cassa  per  le  pensioni  dei  dipendenti  degli  enti    &#13;
 locali,   ai   sensi   dell'art.   39  l.  11  aprile  1955,  n.  379    &#13;
 ("Miglioramento  dei  trattamenti  di  quiescenza  e  modifiche  agli    &#13;
 ordinamenti  degli  Istituti  di  previdenza  presso il Ministero del    &#13;
 tesoro"). Il relativo controllo (devoluto al Ministro  per  i  lavori    &#13;
 pubblici, di concerto con il Ministro per il tesoro e il Ministro per    &#13;
 il lavoro e la previdenza sociale) si inserisce in  attribuzioni  che    &#13;
 concernono  il  personale  di  diverse categorie di enti, nazionali e    &#13;
 locali, ai fini  della  iscrizione  ad  un  istituto  di  previdenza,    &#13;
 operante  presso  il  Ministero  del  tesoro  e  da  questo  gestito.    &#13;
 Pertanto,  come  ha  osservato  l'Avvocatura  generale  dello  Stato,    &#13;
 l'approvazione  prevista  dall'art.  39  cit. non costituisce atto di    &#13;
 controllo  sull'esercizio,  da  parte  dell'Istituto  per   le   case    &#13;
 popolari,  della  sua  attività  tipica; il provvedimento impugnato,    &#13;
 perciò, non si configura come esercizio diretto di attribuzioni, né    &#13;
 come  esplicazione  di  controllo  in materia di edilizia economica e    &#13;
 popolare.   Esso   più   propriamente   attiene   alla   valutazione    &#13;
 dell'opportunità  della iscrizione presso un particolare istituto di    &#13;
 previdenza, di pertinenza statale, e, tra l'altro,  dei  riflessi  di    &#13;
 tale iscrizione sulla gestione unitaria del trattamento pensionistico    &#13;
 di un complesso non  omogeneo  di  dipendenti  di  enti  nazionali  e    &#13;
 locali.  Tra  questi  si  colloca,  e  in misura non certo prevalente    &#13;
 rispetto al personale degli altri  enti  indicati  dall'art.  39,  il    &#13;
 personale dell'Istituto delle case popolari di Acireale.</testo>
    <dispositivo>per questi motivi                              &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
   Rigetta  il ricorso indicato in epigrafe e dichiara che spetta allo    &#13;
 Stato l'approvazione delle deliberazioni dell' Istituto autonomo  per    &#13;
 le  case  popolari  di  Acireale  relative all'iscrizione del proprio    &#13;
 personale alla Cassa pensioni dei dipendenti degli enti locali.          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 maggio 1988.                                &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 maggio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
