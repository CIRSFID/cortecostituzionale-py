<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>897</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:897</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1988</data_decisione>
    <data_deposito>26/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 7, nn. 2, 3 e    &#13;
 4, e 40 della legge 17 agosto 1942, n. 1150 (Legge  urbanistica),  in    &#13;
 relazione all'art. 2 della legge 19 novembre 1968, n. 1187 (Modifiche    &#13;
 ed integrazioni alla legge urbanistica  17  agosto  1942,  n.  1150),    &#13;
 promosso  con  ordinanza  emessa  il 22 maggio 1986 dal T.A.R. per la    &#13;
 Lombardia sul ricorso proposto dalla S.p.A. Manifattura  A.  Randi  e    &#13;
 figli  contro  il  Comune  di  Busto  Arsizio, iscritta al n. 680 del    &#13;
 registro ordinanze 1986 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 57, prima serie speciale dell'anno 1986;                   &#13;
    Visti  l'atto  di costituzione del Comune di Busto Arsizio nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
    Udito  nella  camera  di  consiglio  del 20 aprile 1988 il Giudice    &#13;
 relatore Francesco Greco.                                                &#13;
    Ritenuto   che   il  Tribunale  Amministrativo  Regionale  per  la    &#13;
 Lombardia ha sollevato questione di legittimità costituzionale degli    &#13;
 artt. 7 e 40 della legge 17 agosto 1942, n. 1150 (Legge urbanistica),    &#13;
 in relazione all'art. 2, primo comma, della legge 19  novembre  1968,    &#13;
 n.  1187  (Modifiche ed integrazioni alla legge urbanistica 17 agosto    &#13;
 1942, n. 1150), nella parte in cui, limitandosi a stabilire  il  solo    &#13;
 termine  di  efficacia  dei vincoli urbanistici senza dimensionare il    &#13;
 potere pianificatorio della Pubblica Amministrazione,  consentono  ad    &#13;
 essa  di riprodurre indefinitamente, con gli strumenti urbanistici, i    &#13;
 vincoli apposti ad aree di proprietà privata e decaduti per  inutile    &#13;
 decorso  del  quinquennio di cui al citato art. 2, primo comma, della    &#13;
 legge n.  1187 del 1968,  operandosi  in  tal  modo  una  limitazione    &#13;
 sostanzialmente espropriativa senza la corresponsione di indennizzo;     &#13;
      che è stata dedotta la violazione degli artt. 3 e 42 Cost., per    &#13;
 la disparità di trattamento che si verifica per  i  proprietari  dei    &#13;
 terreni, in quanto, alla cessazione del vincolo, il titolare del bene    &#13;
 gravato si troverà  ad  utilizzare  la  capacità  edificatoria  nei    &#13;
 ristretti  limiti  individuati  dalla  giurisprudenza amministrativa,    &#13;
 senza alcun ragionevole motivo, mentre  il  vicino,  non  colpito  da    &#13;
 vincoli,  beneficerà   della maggiore volumetria assentibile, secondo    &#13;
 le previsioni del piano;                                                 &#13;
      che  il  Comune  di  Busto Arsizio e l'Avvocatura Generale dello    &#13;
 Stato, in rappresentanza della Presidenza del Consiglio dei ministri,    &#13;
 intervenuti  nel  giudizio,  hanno  concluso per l'inammissibilità o    &#13;
 l'infondatezza della questione;                                          &#13;
    Considerato  che la questione sollevata è puramente teorica e non    &#13;
 rileva nel giudizio, in quanto, come  lo  stesso  giudice  remittente    &#13;
 afferma,  tutti  i motivi di merito sono stati rigettati, talché non    &#13;
 sussiste    la     necessaria     pregiudizialità     dell'incidente    &#13;
 costituzionale,   la  quale  esige  che  la  relativa  questione  sia    &#13;
 finalizzata alla decisione della controversia instaurata  dinanzi  al    &#13;
 giudice remittente;                                                      &#13;
      che, pertanto, la questione appare manifestamente inammissibile;    &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt. 7 e 40 della legge 17  agosto    &#13;
 1942,  n.  1150  (Legge  urbanistica),  in relazione all'art. 2 della    &#13;
 legge 19 novembre 1968, n. 1187 (Modifiche ed integrazioni alla legge    &#13;
 urbanistica  17 agosto 1942, n. 1150), sollevata, in riferimento agli    &#13;
 artt. 3 e  42,  terzo  comma,  Cost.,  dal  Tribunale  Amministrativo    &#13;
 Regionale per la Lombardia con l'ordinanza in epigrafe.                  &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 7 luglio 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
