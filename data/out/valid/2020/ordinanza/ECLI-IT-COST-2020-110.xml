<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-06-10/110/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-06-10/110"/>
          <FRBRalias value="ECLI:IT:COST:2020:110" name="ECLI"/>
          <FRBRdate date="2020-06-10" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="110"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-06-10/110/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-06-10/110/ita@"/>
          <FRBRdate date="2020-06-10" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-06-10/110/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-06-10/110/ita@.xml"/>
          <FRBRdate date="2020-06-10" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-05-19" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-06-10" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>110</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:110</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Nicolò Zanon</akn4cc:relatore>
        <akn4cc:data_decisione>2020-05-19</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-06-10</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 1 della legge della Regione Emilia-Romagna 3 giugno 2019, n. 5 (Disposizioni urgenti in materia di organizzazione), promosso dal Presidente del Consiglio dei ministri, con ricorso notificato il 31 luglio-5 agosto 2019, depositato in cancelleria il 6 agosto 2019, iscritto al n. 85 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2019.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Emilia-Romagna;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito il Giudice relatore Nicolò Zanon ai sensi del decreto della Presidente della Corte del 20 aprile 2020, punto 1), lettere a) e c), in collegamento da remoto, senza discussione orale, in data 19 maggio 2020;
 deliberato nella camera di consiglio del 19 maggio 2020.
 Ritenuto che, con ricorso notificato il 31 luglio-5 agosto 2019, depositato il 6 agosto 2019 e iscritto al n. 85 del registro ricorsi 2019, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato la legge della Regione Emilia-Romagna 3 giugno 2019, n. 5 (Disposizioni urgenti in materia di organizzazione), per contrasto con gli artt. 3, 51, 97, 117, secondo comma, lettere l) e m), e terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;
 che, in particolare, il ricorso censura l'art. 1 (Utilizzo delle graduatorie della Regione Emilia-Romagna, delle Agenzie ed enti regionali, nonché delle Aziende e degli enti del Servizio sanitario regionale) della citata legge, secondo cui «1. Al fine di assicurare la continuità dei servizi pubblici erogati e l'efficienza ed economicità dell'attività amministrativa, in coerenza con i principi di buon andamento e di coordinamento con i vincoli di finanza pubblica, la Regione, le Agenzie e gli enti regionali, nonché le Aziende e gli enti del Servizio sanitario regionale possono utilizzare le proprie graduatorie di idonei per la copertura di ulteriori posti rispetto a quelli messi a concorso, entro il periodo di vigenza delle medesime, a condizione che le assunzioni siano coerenti con il proprio piano triennale del fabbisogno del personale. 2. Per le medesime finalità i soggetti di cui al comma 1 possono utilizzare le graduatorie di idonei dei pubblici concorsi approvate da altre amministrazioni, previo accordo tra le amministrazioni interessate. Le medesime graduatorie possono altresì essere utilizzate per il reclutamento di personale a tempo determinato nei limiti di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_36__para_2">articolo 36, comma 2, del decreto legislativo 30 marzo 2001, n. 165</ref> (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche)»;
 che, secondo l'Avvocatura generale dello Stato, la normativa regionale sarebbe in contrasto con quanto disposto dall'<mref><ref href="/akn/it/act/legge/stato/2018-12-30/145/!main#art_1__para_361">art. 1, commi 361</ref> e <ref href="/akn/it/act/legge/stato/2018-12-30/145/!main#art_1__para_365">365, della legge 30 dicembre 2018, n. 145</ref></mref> (Bilancio di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021), così come modificato dall'<ref href="/akn/it/act/decretoLegge/stato/2019-01-28/4/!main#art_14ter__para_1">art. 14-ter, comma 1, del decreto-legge 28 gennaio 2019, n. 4</ref> (Disposizioni urgenti in materia di reddito di cittadinanza e di pensioni), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2019-03-28/26/!main">legge 28 marzo 2019, n. 26</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, in particolare, secondo il citato comma 361, «[f]ermo quanto previsto dall'<ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_35__para_5ter">articolo 35, comma 5-ter, del decreto legislativo 30 marzo 2001, n. 165</ref>, le graduatorie dei concorsi per il reclutamento del personale presso le amministrazioni pubbliche di cui all'articolo 1, comma 2, del medesimo decreto legislativo sono utilizzate esclusivamente per la copertura dei posti messi a concorso nonché di quelli che si rendono disponibili, entro i limiti di efficacia temporale delle graduatorie medesime, fermo restando il numero dei posti banditi e nel rispetto dell'ordine di merito, in conseguenza della mancata <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> o dell'avvenuta estinzione del rapporto di lavoro con i candidati dichiarati vincitori. Le graduatorie possono essere utilizzate anche per effettuare, entro i limiti percentuali stabiliti dalle disposizioni vigenti e comunque in via prioritaria rispetto alle convenzioni previste dall'<ref href="/akn/it/act/legge/stato/1999-03-12/68/!main#art_11">articolo 11 della legge 12 marzo 1999, n. 68</ref>, le assunzioni obbligatorie di cui agli articoli 3 e 18 della medesima <ref href="/akn/it/act/legge/stato/1999/68/!main">legge n. 68 del 1999</ref>, nonché quelle dei soggetti titolari del diritto al collocamento obbligatorio di cui all'<ref href="/akn/it/act/legge/stato/1998-11-23/407/!main#art_1__para_2">articolo 1, comma 2, della legge 23 novembre 1998, n. 407</ref>, sebbene collocati oltre il limite dei posti ad essi riservati nel concorso»;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, secondo il successivo comma 365 - così come modificato dall'<ref href="/akn/it/act/decretoLegge/stato/2018-12-14/135/!main#art_9bis__para_1__point_a">art. 9-bis, comma 1, lettera a), del decreto-legge 14 dicembre 2018, n. 135</ref> (Disposizioni urgenti in materia di sostegno e semplificazione per le imprese e per la pubblica amministrazione), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2019-02-11/12/!main">legge 11 febbraio 2019, n. 12</ref> - «[l]a previsione di cui al comma 361 si applica alle graduatorie delle procedure concorsuali bandite successivamente alla data di entrata in vigore della presente legge. Le previsioni di cui ai commi 361, 363 e 364 si applicano alle procedure concorsuali per l'assunzione di personale medico, tecnico-professionale e infermieristico, bandite dalle aziende e dagli enti del Servizio sanitario nazionale a decorrere dal 1° gennaio 2020»;
 che, secondo l'Avvocatura generale dello Stato, divergendo da quelle statali, le previsioni regionali impugnate violerebbero in primo luogo l'art. 117, secondo comma, lettera l), e l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, poiché la disciplina generale degli atti funzionali all'instaurazione dei rapporti di lavoro, come le graduatorie concorsuali, sarebbe riconducibile alla materia «ordinamento civile», per la quale sussisterebbe competenza legislativa esclusiva statale;
 che l'art. 1 della legge reg. Emilia-Romagna n. 5 del 2019 violerebbe inoltre l'art. 117, secondo comma, lettera m), Cost., nonché gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_51">51</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, poiché la disciplina delle graduatorie concorsuali dovrebbe qualificarsi alla stregua di una «"prestazione" in relazione alla quale emerge l'esigenza di fissare un "livello essenziale"» desumibile dal citato <ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_361">art. 1, comma 361, della legge n. 145 del 2018</ref>, e di conseguenza la previsione regionale violerebbe l'ambito di competenza del legislatore statale, nonché i principi di eguaglianza, di parità delle condizioni di accesso ai pubblici impieghi e di buon andamento della pubblica amministrazione;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, infine, la disposizione impugnata, disponendo in senso diverso da quanto previsto dall'<ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_361">art. 1, comma 361, della legge n. 145 del 2018</ref> - che conterrebbe un principio fondamentale in materia di «coordinamento della finanza pubblica» - violerebbe l'art. 117, terzo comma, Cost., nonché gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_51">51</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>;
 che, con atto depositato il 9 settembre 2019, la Regione Emilia-Romagna si è costituita in giudizio, chiedendo alla Corte costituzionale di respingere il ricorso in quanto inammissibile e, comunque, infondato;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, in via preliminare, la Regione Emilia-Romagna deduce come, nonostante il ricorso sia «apparentemente diretto contro l'intera legge regionale n. 5 del 2019», in realtà le censure si appunterebbero soltanto sull'art. 1 della stessa legge e dunque l'oggetto dell'impugnazione dovrebbe essere circoscritto a tale disposizione;
 che, quanto al primo ordine di censure, non sussisterebbe la pretesa violazione dell'art. 117, secondo comma, lettera l), Cost., poiché la disposizione impugnata sarebbe ascrivibile alla competenza legislativa regionale residuale in materia di organizzazione amministrativa della Regione e degli enti pubblici, e non a quella dell'ordinamento civile;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che neppure sarebbe violato l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, posto che la difformità tra norme regionali e statali, negli ambiti di competenza della legge regionale, non sarebbe suscettibile di determinare una violazione del principio di eguaglianza;
 che non rileverebbe in alcun modo la competenza statale in materia di determinazione dei livelli essenziali delle prestazioni di cui all'art. 117, secondo comma, lettera m), Cost., posto che le previsioni censurate non farebbero riferimento ad alcuna prestazione, bensì conterrebbero misure volte a regolare l'attività amministrativa;
 che neppure vi sarebbe violazione degli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_51">51</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, poiché l'utilizzabilità di preesistenti graduatorie concorsuali «non costituisce affatto una deroga alla regola costituzionale del concorso», in quanto il reclutamento presuppone proprio lo svolgimento di una procedura selettiva concorsuale (viene citata la sentenza del Consiglio di Stato, adunanza plenaria, 28 luglio 2011, n. 14);
 che, infine, rispetto all'ultima censura mossa dal ricorrente, la Regione osserva che la previsione di cui all'<ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_361">art. 1, comma 361, della legge n. 145 del 2018</ref> non avrebbe affatto una funzione di coordinamento finanziario, «non determinando alcun risparmio per la finanza pubblica»;
 che, con memoria depositata via PEC il 28 aprile 2020, la Regione Emilia-Romagna ha evidenziato come la sopravvenuta entrata in vigore della <ref href="/akn/it/act/legge/stato/2019-12-27/160/!main">legge 27 dicembre 2019, n. 160</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2020 e bilancio pluriennale per il triennio 2020-2022) - il cui art. 1, comma 148, ha disposto l'abrogazione dei commi 361 e 365 dell'<ref href="/akn/it/act/legge/stato/2018/145/!main#art_1">art. 1 della legge n. 145 del 2018</ref> - testimonierebbe la non fondatezza delle censure contenute nel ricorso;
  che tale non fondatezza si dedurrebbe anche dalla più recente giurisprudenza costituzionale sopravvenuta in materia (sono richiamate le <mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/77/!main">sentenze n. 77</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/5/!main">5 del 2020</ref></mref>);
 che, con atto notificato in data 27 aprile 2020 e depositato il 30 aprile 2020, il Presidente del Consiglio dei ministri ha dichiarato di rinunciare al ricorso, su conforme delibera del Consiglio dei ministri assunta nella seduta del 24 aprile 2020;
 che con atto depositato via PEC il 13 maggio 2020, la Regione Emilia-Romagna ha accettato la rinuncia al ricorso, giusta delibera della Giunta regionale assunta l'11 maggio 2020.
 Considerato che vi è stata rinuncia al ricorso da parte del Presidente del Consiglio dei ministri, accettata dalla Regione Emilia-Romagna;
 che, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, la rinuncia al ricorso, seguita dall'accettazione della controparte costituita, comporta l'estinzione del processo (ex mult<mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/68/!main">is, ordinanze n. 68</ref>, n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/48/!main">48</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/28/!main">28 del 2020</ref></mref>).</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 2, e 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara estinto il processo.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-05-19">19 maggio 2020</date>.</block>
      <p>F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#nicolòZanon" as="#relatore">Nicolò ZANON</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-06-10">10 giugno 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</p>
    </conclusions>
  </judgment>
</akomaNtoso>
