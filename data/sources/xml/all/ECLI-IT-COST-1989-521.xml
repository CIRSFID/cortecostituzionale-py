<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>521</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:521</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/11/1989</data_decisione>
    <data_deposito>30/11/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  1, secondo    &#13;
 comma, della legge 9 gennaio 1963, n. 9 (Elevazione  dei  trattamenti    &#13;
 minimi  di  pensione  e  riordinamento  delle  norme  in  materia  di    &#13;
 previdenza dei  coltivatori  diretti  e  dei  coloni  e  mezzadri)  e    &#13;
 dell'art.  1,  secondo  comma,  della  legge  12 agosto 1962, n. 1339    &#13;
 (Disposizioni  per  il  miglioramento  dei  trattamenti  di  pensione    &#13;
 corrisposti  dalla Gestione speciale per l'assicurazione obbligatoria    &#13;
 invalidità,  vecchiaia  e  superstiti   degli   artigiani   e   loro    &#13;
 familiari), promossi con tre ordinanze emesse il 24 novembre 1988 dal    &#13;
 Pretore di Perugia, iscritte rispettivamente ai nn. 310,  311  e  312    &#13;
 del  registro  ordinanze  1989  e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 25, prima serie speciale, dell'anno 1989;            &#13;
    Udito  nella  camera  di  consiglio del 25 ottobre 1989 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto   che   nel  corso  di  tre  giudizi  aventi  ad  oggetto    &#13;
 l'integrazione al minimo di pensioni erogate,  rispettivamente  dalla    &#13;
 Gestione speciale per i coltivatori diretti mezzadri e coloni e dalla    &#13;
 Gestione speciale per gli artigiani, il Pretore di Perugia,  con  tre    &#13;
 ordinanze  emesse  in data 24 novembre 1988 (pervenute a questa Corte    &#13;
 il  6  giugno  1989)   ha   sollevato   questione   di   legittimità    &#13;
 costituzionale:  a) dell'art. 1, secondo comma, della legge 9 gennaio    &#13;
 1963, n. 9, nella parte in cui non consente l'integrazione al  minimo    &#13;
 della pensione di riversibilità a carico della Gestione speciale per    &#13;
 i coltivatori diretti, mezzadri e coloni per i titolari  di  pensione    &#13;
 diretta a carico della medesima Gestione (R.O. n. 310 e n. 311);         &#13;
  b)  dell'art. 1, secondo comma, della legge 12 agosto 1962, n. 1339,    &#13;
 nella parte in  cui  non  consente  l'integrazione  al  minimo  della    &#13;
 pensione  di  riversibilità  a carico della Gestione artigiani per i    &#13;
 titolari di pensione diretta a carico della medesima  Gestione  (R.O.    &#13;
 n. 312);                                                                 &#13;
      che  nelle ordinanze di rimessione si rileva come le fattispecie    &#13;
 esulino dall'ambito considerato nella sentenza n. 184 del 1988,  alla    &#13;
 cui motivazione viene fatto integrale richiamo;                          &#13;
    Considerato   che   le   questioni,   per  identità  ed  analogia    &#13;
 dell'oggetto, possono essere esaminate congiuntamente;                   &#13;
      che   in   tutte  le  ordinanze  il  giudice  a  quo  ha  omesso    &#13;
 qualsivoglia  indicazione  circa  le  norme  costituzionali  che   si    &#13;
 assumono violate;                                                        &#13;
      che   anche   la   delibazione  in  ordine  alla  non  manifesta    &#13;
 infondatezza appare svolta  per  relationem  attraverso  l'apodittico    &#13;
 richiamo alla sentenza n. 184 del 1988;                                  &#13;
      che quindi le questioni sono manifestamente inammissibili, tanto    &#13;
 più che entrambe sono già state risolte  da  questa  Corte  con  le    &#13;
 declaratorie  d'illegittimità costituzionale in parte qua contenute,    &#13;
 rispettivamente, nelle sentenze n. 1144 del 1988 e n. 81 del 1989;       &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi,                                                     &#13;
      1)  dichiara  la  manifesta  inammissibilità della questione di    &#13;
 legittimità costituzionale dell'art. 1, secondo comma, della legge 9    &#13;
 gennaio  1963,  n. 9 (Elevazione dei trattamenti minimi di pensione e    &#13;
 riordinamento delle norme in materia di  previdenza  dei  coltivatori    &#13;
 diretti  e  dei  coloni e mezzadri), sollevata dal Pretore di Perugia    &#13;
 con l'ordinanza di cui in epigrafe;                                      &#13;
      2)  dichiara  la  manifesta  inammissibilità della questione di    &#13;
 legittimità costituzionale dell'art. 1, secondo comma,  della  legge    &#13;
 12  agosto  1962,  n.  1339  (Disposizioni  per  il miglioramento dei    &#13;
 trattamenti di  pensione  corrisposti  dalla  Gestione  speciale  per    &#13;
 l'assicurazione  obbligatoria  invalidità,  vecchiaia  e  superstiti    &#13;
 degli artigiani e loro familiari), sollevata dal Pretore  di  Perugia    &#13;
 con l'ordinanza di cui in epigrafe.                                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 novembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 30 novembre 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
