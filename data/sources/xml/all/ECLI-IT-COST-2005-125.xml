<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>125</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:125</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>NEPPI  MODONA</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>21/03/2005</data_decisione>
    <data_deposito>25/03/2005</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Guido NEPPI MODONA; Giudici: Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 548, comma 3, del codice di procedura penale, promosso con ordinanza del 4 giugno 2003 dal Tribunale di Fermo sul ricorso proposto da A. C., iscritta al n. 665 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2003. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 9 febbraio 2005 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che con l'ordinanza in epigrafe il Tribunale di Fermo – chiamato a pronunciarsi, quale giudice della esecuzione, in un procedimento promosso da un condannato al fine di far dichiarare la non esecutività di una sentenza di condanna, della quale veniva dedotta la «mancata irrevocabilità per omessa notifica dell'avviso di deposito con l'estratto del provvedimento ad esso imputato, rinunciante a comparire e, quindi, non presente all'udienza conclusiva» – ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 548, comma 3, del codice di procedura penale, nella parte in cui non prevede che l'avviso di deposito con l'estratto della sentenza vengano notificati all'imputato comunque non presente alla lettura del provvedimento conclusivo del giudizio; &#13;
    che a tal proposito il giudice a quo si limita a richiamare integralmente le «argomentazioni svolte … dalla difesa del condannato al punto 2 del ricorso per cassazione», aggiungendo, quale ulteriore rilievo, la circostanza che sussisterebbe «un'evidente discrasia nella disciplina della materia in esame anche rispetto a quanto previsto dall'art. 442, comma 3, cod. proc. pen. in tema di giudizio abbreviato», giacché la sentenza emessa all'esito di tale rito «è notificata all'imputato che non sia comparso»; &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dalla Avvocatura generale dello Stato, la quale ha chiesto dichiararsi infondata la questione proposta, in forza dei principî affermati da questa Corte nella sentenza n. 18 del 1976, con la quale venne dichiarata non fondata una questione, del tutto analoga, sollevata in riferimento all'art. 500 del codice di procedura penale abrogato. &#13;
    Considerato che la questione sollevata in riferimento agli artt. 24 e 111 della Costituzione deve essere dichiarata manifestamente inammissibile, per assoluta carenza di motivazione: contrariamente a quanto mostra di ritenere il giudice  a quo, tale indispensabile requisito della ordinanza di rimessione non può essere sostituito, o integrato, dal mero rinvio per relationem ad altri atti, dal momento che il giudice – con motivazione che deve presentare i caratteri della autosufficienza – ha l'obbligo di rendere esplicite le ragioni in base alle quali dubita della costituzionalità della norma che ritiene di dover applicare (v. ordinanza n. 59 del 2004 ed altre ivi richiamate); &#13;
    che, quanto alla dedotta violazione dell'art. 3 Cost., il richiamo al diverso regime previsto per il giudizio abbreviato si rivela del tutto improprio, al fine di fondare su di esso un pertinente termine di raffronto: giacché, per un verso, la natura di procedimento speciale che caratterizza quel giudizio ne contrassegna i caratteri ampiamente derogatori rispetto al giudizio ordinario; mentre, sotto altro profilo, la stessa distinzione concettuale tra imputato contumace ed imputato assente, già ritenuta esente da rilievi sul piano costituzionale – come ha rammentato l'Avvocatura – in riferimento alla sede dibattimentale (v. le sentenze n. 315 del 1990 e n. 18 del 1976), ben può comportare scelte differenziate, ai fini che qui rilevano, ove correlata ad un modello processuale che, come il giudizio abbreviato, si celebra in camera di consiglio, in una fase processuale che precede il dibattimento e secondo uno schema procedimentale idealmente caratterizzato dalla massima concentrazione; &#13;
    che, pertanto, la questione proposta in riferimento all'art. 3 Cost. deve essere dichiarata manifestamente infondata. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 548, comma 3, del codice di procedura penale, sollevata, in riferimento agli artt. 24 e 111 della Costituzione, dal Tribunale di Fermo con l'ordinanza indicata in epigrafe; &#13;
    dichiara la manifesta infondatezza della medesima questione di legittimità costituzionale, sollevata in riferimento all'art. 3 della Costituzione. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 marzo 2005. &#13;
F.to: &#13;
Guido NEPPI MODONA, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 25 marzo 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
