<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>20</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:20</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Luigi Oggioni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/01/1975</data_decisione>
    <data_deposito>05/02/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. &#13;
 MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1 del d.l.  9  &#13;
 luglio  1926, n. 1331 (Costituzione della Associazione nazionale per il  &#13;
 controllo della combustione), convertito in legge 16  giugno  1927,  n.  &#13;
 1132,  promosso con ordinanza emessa il 10 aprile 1972 dal tribunale di  &#13;
 Ascoli Piceno nel procedimento penale a  carico  di  Ciabattoni  Dante,  &#13;
 iscritta  al  n.  309  del  registro  ordinanze 1972 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 254 del 27 settembre 1972.        &#13;
     Visto  l'atto  d'intervento  del  Presidente  del   Consiglio   dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 4 dicembre 1974 il Giudice relatore  &#13;
 Luigi Oggioni;                                                           &#13;
     udito  il sostituto avvocato generale dello Stato Michele Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza emessa il 10 aprile 1972 nel procedimento  penale  in  &#13;
 grado   di  appello  a  carico  di  Ciabattoni  Dante,  imputato  della  &#13;
 contravvenzione di cui all'art. 112 del r.d. 12 maggio  1927,  n.  824,  &#13;
 per  avere  tenuto  in  esercizio  un  impianto  di  autoclave senza il  &#13;
 preventivo  controllo  da  parte  dell'Associazione  nazionale  per  il  &#13;
 controllo  della combustione, nonché imputato della contravvenzione di  &#13;
 cui all'art. 651 C. P., per essersi rifiutato di declinare  le  proprie  &#13;
 generalità ad un agente tecnico della detta associazione, il tribunale  &#13;
 di  Ascoli Piceno ha sollevato questione di legittimità costituzionale  &#13;
 dell'art. 1 del d.l. 9 luglio 1926, n.   1331 (convertito in  legge  16  &#13;
 giugno  1927,  n.  1132),  istitutivo dell'Ente suddetto, ritenendo che  &#13;
 tale norma,  per  la  parte  che  attiene  alla  obbligatorietà  della  &#13;
 associazione gli esercenti determinati apppressione di vapore o a gas e  &#13;
 di  determinati  apparecchi  ed impianti per la combustione, sia lesiva  &#13;
 dei principi della libertà  e  volontarietà  delle  associazioni  fra  &#13;
 privati  cittadini,  e  pertanto  contrasti con gli artt. 13 e 18 della  &#13;
 Costituzione.                                                            &#13;
     Sul  punto della rilevanza, il tribunale osserva che la risoluzione  &#13;
 della questione è pregiudiziale,  essendo  state  le  contravvenzioni,  &#13;
 ascritte  al  Ciabattoni,  accertate  da  un agente tecnico della detta  &#13;
 associazione.                                                            &#13;
     L'ordinanza,  debitamente  comunicata  e   notificata,   è   stata  &#13;
 pubblicata sulla Gazzetta Ufficiale n. 254 del 27 settembre 1972.        &#13;
     Avanti   a   questa  Corte  si  è  tempestivamente  costituito  il  &#13;
 Presidente  del  Consiglio  dei  ministri,   rappresentato   e   difeso  &#13;
 dall'Avvocatura  generale  dello  Stato,  che  ha depositato le proprie  &#13;
 deduzioni il 29 luglio 1972.                                             &#13;
     L'Avvocatura contesta la fondatezza  delle  censure,  richiamandosi  &#13;
 alla  giurisprudenza  di  questa Corte concernente i limiti della così  &#13;
 detta libertà negativa di associazione, che sarebbe esclusa  nei  casi  &#13;
 in cui l'obbligatorietà dell'associazione tenda all'attuazione di fini  &#13;
 pubblici,   come   sarebbero  appunto  quelli  perseguiti  dalla  norma  &#13;
 impugnata.  Nega  poi  che,  nella  specie,  ricorra   l'applicabilità  &#13;
 dell'art.  13  Cost.,  riguardante  esclusivamente  la  garanzia  della  &#13;
 persona  fisica,  tutelata  attraverso  il  rispetto  della   sicurezza  &#13;
 personale.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  tribunale  di  Ascoli Piceno sottopone a questa Corte la  &#13;
 questione concernente la pretesa illegittimità della norma  istitutiva  &#13;
 del  Consorzio  obbligatorio, denominate "Associazione per il controllo  &#13;
 della combustione" (art. 1 d.l.  9 luglio 1926, n. 1331), rilevando che  &#13;
 l'obbligatorietà sarebbe incompatibile con l'art. 18 Cost.,  in  forza  &#13;
 del quale al cittadino verrebbe garantita, in via assoluta, non solo la  &#13;
 libertà  positiva  di associazione, ma, altresì, quella, negativa, di  &#13;
 non partecipare, se non volontariamente, ad associazioni  di  qualsiasi  &#13;
 natura.                                                                  &#13;
     In  proposito, l'Avvocatura non contesta l'esistenza della invocata  &#13;
 garanzia di libertà, ma  afferma  che  l'attività  della  combustione  &#13;
 esercitata  con  apparecchi a pressione di vapore o a gas è d indubbia  &#13;
 rilevanza  per  l'interesse  generale  ed  è  stata   conseguentemente  &#13;
 regolata  da  provvedimenti normativi tendenti a dettare una disciplina  &#13;
 organica e completa della materia, in rapporto ai due aspetti salienti,  &#13;
 in cui si articola  l'interesse  generale  suddetto,  cioè  la  tutela  &#13;
 dell'economia   pubblica,   attraverso   il   controllo   dell'uso  dei  &#13;
 combustibili, e la garanzia della incolumità personale dei  lavoratori  &#13;
 addetti al settore.                                                      &#13;
     Tali  finalità  renderebbero  operanti nella specie le limitazioni  &#13;
 che la giurisprudenza di questa Corte avrebbe ritenuto insite nella pur  &#13;
 riconosciuta libertà negativa di associazione, e  collegate,  appunto,  &#13;
 all'esistenza  di  determinati  fini  pubblici,  il  cui  perseguimento  &#13;
 autorizzerebbe  l'istituzione   di   enti   a   struttura   associativa  &#13;
 obbligatoria.                                                            &#13;
     2.  -  Deve osservarsi, al riguardo, che questa Corte ha in effetti  &#13;
 già avuto occasione di rilevare che  il  precetto  costituzionale  del  &#13;
 quale  si  discute,  si risolve anche nella libertà del singolo di non  &#13;
 partecipare ad associazioni se non di sua libera e  volontaria  scelta,  &#13;
 precisando,  peraltro, che ciò non esclude per lo Stato la facoltà di  &#13;
 assicurare il raggiungimento di determinati  fini  pubblici  attraverso  &#13;
 l'istituzione  di  enti  del  tipo  suddetto, sempreché l'associazione  &#13;
 coattiva  non  violi  un  diritto,  una   libertà   o   un   principio  &#13;
 costituzionalmente  garantito,  ed  il fine pubblico che si dichiara di  &#13;
 perseguire non sia palesemente arbitrario,  pretestuoso  o  artificioso  &#13;
 (vedi sentenze nn. 69 del 1962 e 120 del 1973).                          &#13;
     Alla  stregua  dei  criteri  sopra  enunciati,  l'istituzione della  &#13;
 Associazione obbligatoria per il controllo  della  combustione  rientra  &#13;
 indubbiamente  tra  le  facoltà del legislatore ordinario, compatibili  &#13;
 con la garanzia della libertà di associazione apprestata dall'art.  18  &#13;
 della  Costituzione.  Al  riguardo, appare, invero, decisivo il rilievo  &#13;
 che, come si desume anche dai  lavori  preparatori,  l'Associazione  in  &#13;
 parola  persegue finalità sulla cui natura non è lecito alcun dubbio.  &#13;
 Ed infatti è evidente,  come  esattamente  sostiene  l'Avvocatura,  la  &#13;
 pubblicità  dell'interesse  all'efficace sorveglianza sugli apparecchi  &#13;
 ed impianti in discorso, sia ai fini della prevenzione degli incidenti,  &#13;
 trattandosi di apparecchi pericolosi per le conseguenze che si  possono  &#13;
 produrre,  sia  ai  fini  del  controllo  sulla loro utilizzazione, per  &#13;
 ottenere la maggiore economia di combustibile ed il massimo  rendimento  &#13;
 possibile  in un Paese come l'Italia, che è tributario dell'estero per  &#13;
 i combustibili di più largo consumo.   Ed è,  altresì,  indubitabile  &#13;
 che  tali  fini,  per la loro generalità ed importanza, possono essere  &#13;
 assunti  come  propri  dallo  Stato.  La  normativa  istitutiva   della  &#13;
 Associazione, d'altra parte, puntualmente persegue detti scopi, come è  &#13;
 reso  evidente  dall'esame  della  stessa,  e segnatamente del r.d.l. 9  &#13;
 luglio 1926, n. 1331, convertito nella legge 16 giugno 1927, n. 1132, e  &#13;
 del regolamento approvato con r.d. 12 maggio 1927, n. 824. Dette  norme  &#13;
 investono   tutte  le  fasi  della  fabbricazione,  dell'impianto,  del  &#13;
 funzionamento e della manutenzione  degli  apparecchi  ed  impianti  in  &#13;
 esame,  prevedendo  una serie di obblighi, accorgimenti ed adempimenti,  &#13;
 tutti  intesi  ai  fini  sia  della  prevenzione  degli  incidenti  (v.  &#13;
 specialmente  art.  1  d.l. n. 1331 del 1926 e tit. del Reg. n. 824 del  &#13;
 1927), sia del miglior rendimento degli apparecchi col minor consumo di  &#13;
 combustibile (art.  1  cit.  d.l.    n.  1331  e  tit.  II  del  citato  &#13;
 regolamento n. 824 del 1927).                                            &#13;
     Deve,  pertanto,  affermarsi  che  l'istituzione della Associazione  &#13;
 obbligatoria per il controllo della combustione disposta con  l'art.  1  &#13;
 citato  d.l.  n.  1331  del  1926  non contrasta con il principio della  &#13;
 libertà di associazione sancito dall'art.  18 della Costituzione.       &#13;
     3. - Egualmente deve escludersi il preteso  contrasto  della  norma  &#13;
 impugnata  con l'art. 13 della Costituzione, questione che il giudice a  &#13;
 quo ha pure ritenuto di sottoporre a questa Corte. Secondo ciò che  è  &#13;
 dato   desumere   dall'ordinanza   di   rinvio,   la   norma  impugnata  &#13;
 contrasterebbe con l'invocato precetto costituzionale  per  gli  stessi  &#13;
 motivi  dedotti  in  relazione  all'art.  18  Cost., e cioè in quanto,  &#13;
 rendendo obbligatoria l'Associazione  in  discorso,  inciderebbe  sulla  &#13;
 libertà  personale  del  cittadino,  che  l'art.  13  Cost.,  appunto,  &#13;
 garantisce. Ma è di tutta evidenza che, nella specie, si è fuori  del  &#13;
 campo  di applicazione della invocata garanzia costituzionale, la quale  &#13;
 riguarda la tutela  della  libertà  personale  contro  ogni  forma  di  &#13;
 costrizione   o   limitazione   fisica   compiuta   senza  l'intervento  &#13;
 dell'autorità giudiziaria, e concerne quindi  le  guarentigie  supreme  &#13;
 dell'habeas  corpus,  mentre  la  norma  impugnata,  imponendo,  per  i  &#13;
 descritti   scopi   di   pubblico    interesse,    la    partecipazione  &#13;
 all'Associazione  in  esame,  non  vincola la liberta della persona né  &#13;
 più né meno di quanto avvenga per effetto di  qualsiasi  altra  norma  &#13;
 precettiva.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art.  1  del  dl.   9   luglio   1926,   n.   1331   (Costituzione  &#13;
 dell'Associazione   nazionale  per  il  controllo  della  combustione),  &#13;
 convertito in legge 16 giugno 1927, n.  1132, sollevata  dal  tribunale  &#13;
 di  Ascoli  Piceno  con  l'ordinanza di cui in epigrafe, in riferimento  &#13;
 agli artt. 13 e 18 della Costituzione.                                   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 23 gennaio 1975.                              &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  PAOLO  ROSSI-  LEONETTO  AMADEI  -  &#13;
                                   GIULIO GIONFRIDA - EDOARDO VOLTERRA -  &#13;
                                   GUIDO ASTUTI - MICHELE ROSSANO.        &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
