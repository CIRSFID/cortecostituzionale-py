<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>6</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:6</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>OGGIONI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1976</data_decisione>
    <data_deposito>15/01/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. LUIGI OGGIONI, Presidente - Avv. ANGELO &#13;
 DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - &#13;
 Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA &#13;
 - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE &#13;
 STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 90 del codice  &#13;
 di procedura penale, promosso con ordinanza emessa il 2 aprile 1974 dal  &#13;
 tribunale di Venezia  nel  procedimento  penale  a  carico  di  Manzato  &#13;
 Silvio,  iscritta  al  n.  219 del registro ordinanze 1974 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 167 del 26 giugno 1974.     &#13;
     Udito nella camera di consiglio del  30  ottobre  1975  il  Giudice  &#13;
 relatore Ercole Rocchetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  del  procedimento  penale  a  carico  di Manzato Silvio  &#13;
 imputato del delitto di ratto, e, per lo stesso fatto, già giudicato e  &#13;
 condannato in primo e secondo grado per i reati  di  atti  di  libidine  &#13;
 violenta  e  di  atti  osceni,  il  tribunale  di  Venezia  ha proposto  &#13;
 questione di legittimità costituzionale dell'art.  90  del  codice  di  &#13;
 procedura  penale  (che  sancisce  il  principio, in ordine al medesimo  &#13;
 fatto, della  inammissibilità  di  un  secondo  giudizio),  sotto  due  &#13;
 distinti profili, deducendo rispettivamente la violazione dell'art. 24,  &#13;
 secondo comma, e 3 della Costituzione.                                   &#13;
     Osserva,  innanzi  tutto,  il  giudice a quo che la interpretazione  &#13;
 della giurisprudenza e della dottrina assolutamente prevalenti, secondo  &#13;
 cui la preclusione dell'art. 90 non opera  nella  ipotesi  di  concorso  &#13;
 formale  di  reati,  viola il diritto di difesa dell'imputato in quanto  &#13;
 incide sul suo interesse a non essere vessato più volte  in  relazione  &#13;
 al  medesimo  fatto  con  distinte  procedure giudiziarie, rimesse alla  &#13;
 libera determinazione del pubblico  ministero,  che  avrebbe  così  il  &#13;
 potere  di  frazionare nel tempo la contestazione dei diversi reati, in  &#13;
 contrasto  con  i  principi  dell'unità  dell'azione  penale  e  della  &#13;
 eccezionalità dell'accusa suppletiva.                                   &#13;
     In secondo luogo, secondo il tribunale  di  Venezia,  il  requisito  &#13;
 della   irrevocabilità   della   sentenza   precedente,  espressamente  &#13;
 richiesto dall'art.  90,  vanificherebbe  l'esigenza  a  cui  tende  il  &#13;
 principio  del  ne  bis in idem ponendosi in contrasto col principio di  &#13;
 eguaglianza, perché non ci sarebbe ragione di regolare diversamente il  &#13;
 caso in cui sia intervenuta sentenza irrevocabile e quello  in  cui  la  &#13;
 sentenza emessa irrevocabile non sia ancora divenuta.                    &#13;
     L'ordinanza   è   stata   ritualmente   notificata   comunicata  e  &#13;
 pubblicata.                                                              &#13;
     Nel  giudizio  dinanzi  alla  Corte  nessuna  delle  parti  si   è  &#13;
 costituita,   né  è  intervenuto  il  Presidente  del  Consiglio  dei  &#13;
 ministri: pertanto la causa  viene  discussa  e  decisa  in  camera  di  &#13;
 consiglio,  ai  sensi dell'art. 26, secondo comma, della legge 11 marzo  &#13;
 1953, n. 87, e 9, primo comma, delle Norme integrative  per  i  giudizi  &#13;
 dinanzi alla Corte costituzionale.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Dispone  l'art.  90  del  codice  di  procedura  penale  che  &#13;
 l'imputato condannato o prosciolto con sentenza  divenuta  irrevocabile  &#13;
 non può essere sottoposto a procedimento penale per il medesimo fatto.  &#13;
     Secondo  la  interpretazione comunemente accolta, la norma in esame  &#13;
 non si applica nel caso di concorso formale di reati, che  si  verifica  &#13;
 quando  (art.  81  c.p.),  con  una  sola  azione od omissione, vengono  &#13;
 violate diverse disposizioni di legge. In tal caso, anche  se  l'azione  &#13;
 è unica, gli eventi, che sono plurimi e diversi, danno ontologicamente  &#13;
 luogo a più fatti, che possono anche essere separatamente perseguiti.   &#13;
     Chiamato  a  decidere  se un imputato, che era stato condannato per  &#13;
 atti  di  libidine  violenti  in  persona  di  una  minore  degli  anni  &#13;
 quattordici,   potesse   in   successivo  giudizio  essere  chiamato  a  &#13;
 rispondere, in base allo stesso episodio, anche  di  ratto  a  fine  di  &#13;
 libidine,  il  tribunale di Venezia ha ritenuto che, nella specie, alla  &#13;
 stregua dell'orientamento interpretativo dominante, dovesse  ammettersi  &#13;
 la  possibilità di un separato e nuovo giudizio "per lo stesso fatto".  &#13;
 Non condividendo, però, tale  soluzione,  il  tribunale  ha  sollevato  &#13;
 questione  di  legittimità  costituzionale  del  citato  art.  90,  in  &#13;
 riferimento agli artt. 24 e 3 della Costituzione.                        &#13;
     Secondo il giudice  a  quo,  la  violazione  dell'art.  24  sarebbe  &#13;
 determinata  dal  disagio  materiale  e  morale che subisce l'imputato,  &#13;
 allorché è costretto a difendersi più volte per  uno  stesso  fatto;  &#13;
 l'art.  3, poi, sarebbe violato perché la norma denunciata, prevedendo  &#13;
 la non reiterabilità del procedimento per uno stesso  fatto  solo  per  &#13;
 l'imputato  condannato o prosciolto con sentenza divenuta irrevocabile,  &#13;
 non la esclude anche quando la  sentenza  non  sia  ancora  passata  in  &#13;
 giudicato.                                                               &#13;
     2. - Entrambe le questioni non sono fondate.                         &#13;
     Benché   sia  certamente  opportuno  che,  per  uno  stesso  fatto  &#13;
 determinante più eventi  criminosi,  come  per  più  fatti  tra  loro  &#13;
 connessi,  si celebri un unico processo - il che, del resto, dispongono  &#13;
 precise norme processuali (artt. 45 e seguenti del c.p.p.)  -  non  per  &#13;
 questo  può  dirsi  che,  ove  ciò  non  si verifichi, possa restarne  &#13;
 violato l'art. 24, secondo  comma,  della  Costituzione.  È  evidente,  &#13;
 infatti,  che se i giudizi, invece di essere riuniti, sono separati, il  &#13;
 diritto di difesa è sempre rispettato, purché in ciascuno di essi  si  &#13;
 faccia  applicazione  di  quelle  norme  che  sono preordinate alla sua  &#13;
 tutela.                                                                  &#13;
     In altri termini, la semplice reiterazione del processo in ordine a  &#13;
 uno stesso episodio, ma con riferimento a più "fatti" in cui  esso  si  &#13;
 scinda, non può influire sul diritto di difesa dell'imputato, perché,  &#13;
 nel caso, la tutela che a quel diritto è riservata, non viene limitata  &#13;
 od esclusa in alcun modo.                                                &#13;
     Né può dirsi che sia violato il principio di eguaglianza perché,  &#13;
 nella  sua  funzione  preclusiva  alla  reiterazione di un procedimento  &#13;
 penale, la norma denunciata non equipara le due situazioni  giuridiche,  &#13;
 della  sentenza divenuta irrevocabile rispetto a quella che tale ancora  &#13;
 non sia. Dette situazioni sono profondamente  differenziate  sul  piano  &#13;
 giuridico e perciò il diverso trattamento è di per sé giustificato.   &#13;
     Tanto  più, deve aggiungersi, che l'ordinamento processuale penale  &#13;
 appresta, a tutela dell'unità del processo, altri istituti, dalla  cui  &#13;
 applicazione   risulta  oltremodo  difficile  che  ad  un  procedimento  &#13;
 terminato con sentenza, benché non ancora irrevocabile, altro ne possa  &#13;
 seguire  per  lo  stesso  fatto,  (artt.  45   c.p.p.   citato,   sulla  &#13;
 connessione;  445  c.p.p. sulla contestazione di reati concorrenti; 447  &#13;
 sulla rimessione al  P.M.  ove  il  fatto  risulti  diverso  da  quello  &#13;
 contestato  ecc.).  Nel  caso  poi  di una difettosa applicazione della  &#13;
 normativa connessa ai richiamati istituti, va  ricordato  che  sussiste  &#13;
 pur  sempre  il  rimedio  fornito dalla norma di chiusura dell'art. 579  &#13;
 c.p.p., il quale dispone che, quando più sentenze su un medesimo fatto  &#13;
 e contro la stessa persona siano divenute  irrevocabili,  la  Corte  di  &#13;
 cassazione  deve  provvedere  a  decidere  quale  tra esse debba essere  &#13;
 eseguita, e ad annullare le altre.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  90 del codice di procedura penale, proposta, con l'ordinanza  &#13;
 indicata in epigrafe, in riferimento agli artt. 24, secondo comma, e  3  &#13;
 della Costituzione.                                                      &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 gennaio 1976.        &#13;
                                   F.to: LUIGI OGGIONI - ANGELO DE MARCO  &#13;
                                   - ERCOLE ROCCHETTI - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE TRIMARCHI  -  NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   -  GUIDO  ASTUTI  - MICHELE ROSSANO -  &#13;
                                   ANTONINO DE STEFANO.                   &#13;
                                    .fo on                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
