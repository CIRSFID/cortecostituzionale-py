<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>36</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:36</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/01/1988</data_decisione>
    <data_deposito>19/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 586 del codice    &#13;
 di procedura civile, promosso con ordinanza emessa l'11  luglio  1983    &#13;
 dal Pretore di Milano, iscritta al n. 987 del registro ordinanze 1983    &#13;
 e  pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n.   95    &#13;
 dell'anno 1984;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto  che  il  Pretore  di  Milano,  con ordinanza emessa l'11    &#13;
 luglio 1983 (R.O. n. 987 del 1983) ha sollevato, in riferimento  agli    &#13;
 artt. 2, 3, 41, 42 e 47 della Costituzione, questione di legittimità    &#13;
 costituzionale dell'art. 586 del codice di  procedura  civile,  nella    &#13;
 parte  in  cui  stabilisce  che,  nel  procedimento di espropriazione    &#13;
 forzata  immobiliare,  il  giudice   dell'esecuzione,   avvenuto   il    &#13;
 versamento  del  prezzo,  pronunci  decreto  con il quale trasferisce    &#13;
 all'aggiudicatario  il   bene   espropriato   ingiungendo   all'   ex    &#13;
 proprietario che abita l'immobile il rilascio immediato dell'immobile    &#13;
 stesso;                                                                  &#13;
      che  ad  avviso  del  giudice  a quo la disposizione censurata -    &#13;
 omettendo di prevedere, per il giudice dell'esecuzione, il potere  di    &#13;
 vagliare    i    contrapposti    interessi    dell'aggiudicatario   e    &#13;
 dell'occupante espropriato e di fissare, sulla base di  tale  vaglio,    &#13;
 un   termine   per   l'inizio   dell'esecuzione   -   creerebbe   una    &#13;
 ingiustificata disparità di trattamento tra l'espropriato che  abita    &#13;
 l'immobile,  il  quale resta sottoposto al potere dell'aggiudicatario    &#13;
 di agire in executivis nel termine di 10  giorni  previsto  dall'art.    &#13;
 480  del codice di procedura civile, ed il conduttore per il quale la    &#13;
 legge 27 luglio  1978,  n.  392,  regola  le  modalità  di  rilascio    &#13;
 dell'immobile alla scadenza della locazione;                             &#13;
      che  sempre  secondo  il  giudice  rimettente la norma impugnata    &#13;
 comprometterebbe  la  funzione  sociale  della   proprietà   sancita    &#13;
 dall'art.   42   della   Costituzione   e   violerebbe   il   diritto    &#13;
 all'abitazione tutelato dagli artt. 2 e 47 della Costituzione;           &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei Ministri,    &#13;
 rappresentanto  dall'Avvocatura  dello  Stato,   chiedendo   che   la    &#13;
 questione sia dichiarata infondata;                                      &#13;
    Considerato  che le diversità riscontrabili tra la disciplina del    &#13;
 rilascio dell'immobile espropriato, dettata dall'art. 586 del  codice    &#13;
 di  procedura  civile,  e la regolamentazione prevista dalla legge n.    &#13;
 392 del  1978  per  il  rilascio  degli  immobili  nelle  ipotesi  di    &#13;
 cessazione  della  locazione  non  contrastano  con  il  principio di    &#13;
 eguaglianza riflettendo le  oggettive  differenze  esistenti  tra  la    &#13;
 situazione  del  debitore  espropriato,  che  detiene l'immobile sine    &#13;
 titulo, e la posizione  del  conduttore  contrassegnata,  anche  alla    &#13;
 scadenza  del  contratto di locazione, dal riferimento all'originario    &#13;
 schema contrattuale;                                                     &#13;
      che  la  tempestiva  acquisizione  del bene espropriato da parte    &#13;
 dell'aggiudicatario è diretta ad  assicurare  il  buon  esito  delle    &#13;
 vendite  effettuate  nell'ambito  delle  procedure espropriative ed a    &#13;
 soddisfare esigenze di regolare e sollecito  trasferimento  dei  beni    &#13;
 venduti,  e,  pertanto, essa non si pone in contrasto con la funzione    &#13;
 sociale della proprietà;                                                &#13;
      che  la  Corte  con  la  sentenza  n.  252 del 15 luglio 1983 ha    &#13;
 escluso che l'abitazione possa essere configurata come indispensabile    &#13;
 presupposto  dei  diritti  inviolabili  menzionati  nell'art. 2 della    &#13;
 Costituzione e,  in  particolare,  ha  negato  che  l'art.  47  della    &#13;
 Costituzione  individui  e  tuteli un diritto all'abitazione distinto    &#13;
 dal diritto di proprietà dell'abitazione medesima;                      &#13;
      che   per  le  ragioni  suesposte  la  questione  va  dichiarata    &#13;
 manifestamente infondata;                                                &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  586  del  codice  di   procedura   civile,    &#13;
 sollevata,  in  riferimento  agli  artt.  2,  3,  41,  42  e 47 della    &#13;
 Costituzione, dal  Pretore  di  Milano  con  l'ordinanza  di  cui  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 13 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
