<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>472</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:472</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/10/2000</data_decisione>
    <data_deposito>06/11/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di ammissibilità del conflitto tra poteri dello Stato    &#13;
 sorto  a seguito della delibera del 23 novembre 1999 della Camera dei    &#13;
 deputati  relativa  alla insindacabilità delle opinioni espresse dal    &#13;
 deputato  Tiziana  Maiolo  nei confronti del dott. Giancarlo Caselli,    &#13;
 promosso  dal  giudice  per  le indagini preliminari del Tribunale di    &#13;
 Roma  con  ricorso  depositato il 4 aprile 2000 ed iscritto al n. 150    &#13;
 del registro ammissibilità conflitti.                                   &#13;
     Udito  nella camera di consiglio del 27 settembre 2000 il giudice    &#13;
 relatore Cesare Mirabelli.                                               &#13;
     Ritenuto  che, con ricorso datato 21 marzo 2000, depositato nella    &#13;
 cancelleria   della   Corte   il   4 aprile   successivo   unitamente    &#13;
 all'ordinanza  del  18 febbraio  2000  con  la  quale si disponeva la    &#13;
 sospensione  della  udienza  preliminare  per  sollevare conflitto di    &#13;
 attribuzione, il giudice per le indagini preliminari del Tribunale di    &#13;
 Roma,  investito  di  un  procedimento  penale  con  l'imputazione di    &#13;
 diffamazione  a  carico  del  deputato  Tiziana  Maiolo, ha sollevato    &#13;
 conflitto  di attribuzione tra poteri dello Stato nei confronti della    &#13;
 Camera  dei  deputati  in  relazione  alla deliberazione con la quale    &#13;
 l'Assemblea,  nella seduta del 23 novembre 1999 (documento IV-quater,    &#13;
 n. 90),  ha  dichiarato  che  i  fatti  per  i  quali era in corso il    &#13;
 procedimento  penale  concernevano opinioni espresse da un membro del    &#13;
 Parlamento   nell'esercizio   delle  sue  funzioni,  in  quanto  tali    &#13;
 insindacabili (art. 68, primo comma, della Costituzione);                &#13;
         che  nel ricorso per conflitto di attribuzione il giudice per    &#13;
 le  indagini  preliminari,  dopo  aver esposto i fatti che hanno dato    &#13;
 origine  alla  richiesta  di rinvio a giudizio formulata dal pubblico    &#13;
 ministero,  ritiene che la deliberazione di insindacabilità riguardi    &#13;
 dichiarazioni  prive di nesso con la funzione parlamentare e, dunque,    &#13;
 menomi  la sfera di attribuzioni dell'autorità giudiziaria investita    &#13;
 del procedimento.                                                        &#13;
     Considerato  che si deve, in questa fase, delibare esclusivamente    &#13;
 se  il  ricorso sia ammissibile, valutando, senza contraddittorio tra    &#13;
 le  parti,  se  sussistono  i requisiti soggettivo ed oggettivo di un    &#13;
 conflitto di attribuzione tra poteri dello Stato, impregiudicata ogni    &#13;
 definitiva  decisione  anche  in  ordine all'ammissibilità (art. 37,    &#13;
 terzo e quarto comma, della legge 11 marzo 1953, n. 87);                 &#13;
         che,  quanto  al  requisito  soggettivo,  il  giudice  per le    &#13;
 indagini preliminari del Tribunale di Roma è legittimato a sollevare    &#13;
 il conflitto, essendo competente a dichiarare definitivamente, per il    &#13;
 procedimento  del  quale  è  investito,  la  volontà del potere cui    &#13;
 appartiene,  in ragione dell'esercizio delle funzioni giurisdizionali    &#13;
 svolte in posizione di indipendenza costituzionalmente garantita;        &#13;
         che,  parimenti, la Camera dei deputati, che ha deliberato la    &#13;
 dichiarazione  di  insindacabilità  delle  opinioni  espresse  da un    &#13;
 proprio membro, è legittimata ad essere parte del conflitto, essendo    &#13;
 competente  a  dichiarare  definitivamente la volontà del potere che    &#13;
 rappresenta;                                                             &#13;
         che,  quanto  al  profilo oggettivo del conflitto, il giudice    &#13;
 ricorrente  denuncia  la lesione della propria sfera di attribuzioni,    &#13;
 garantita    da    norme   costituzionali,   in   conseguenza   della    &#13;
 deliberazione,  che  ritiene  illegittima, con la quale la Camera dei    &#13;
 deputati  qualifica  le  dichiarazioni del parlamentare, per le quali    &#13;
 era  in  corso  il  giudizio,  come  insindacabili in quanto comprese    &#13;
 nell'esercizio  delle  funzioni  parlamentari  (art. 68, primo comma,    &#13;
 della Costituzione);                                                     &#13;
         che,  pertanto,  esiste  la  materia  di  un conflitto la cui    &#13;
 risoluzione spetta alla competenza della Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     dichiara  ammissibile, ai sensi dell'art. 37 della legge 11 marzo    &#13;
 1953, n. 87, il conflitto di attribuzione proposto dal giudice per le    &#13;
 indagini preliminari del Tribunale di Roma nei confronti della Camera    &#13;
 dei deputati con l'atto introduttivo indicato in epigrafe;               &#13;
     dispone:                                                             &#13;
         a)  che  la  cancelleria  della Corte dia comunicazione della    &#13;
 presente  ordinanza  al  giudice  per  le  indagini  preliminari  del    &#13;
 Tribunale di Roma, ricorrente;                                           &#13;
         b)  che  l'atto introduttivo e la presente ordinanza siano, a    &#13;
 cura  del  ricorrente,  notificati  alla Camera dei deputati entro il    &#13;
 termine   di   sessanta   giorni   dalla  comunicazione,  per  essere    &#13;
 successivamente    depositati,    con   la   prova   delle   eseguite    &#13;
 notificazioni,  nella  cancelleria  della  Corte  entro il termine di    &#13;
 venti  giorni dalle notificazioni stesse (art. 26, terzo comma, delle    &#13;
 norme integrative per i giudizi davanti alla Corte costituzionale).      &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 ottobre 2000.                              &#13;
                 Il Presidente e redattore: Mirabelli                     &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 6 novembre 2000.                        &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
