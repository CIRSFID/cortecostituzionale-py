<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>208</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:208</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/04/1993</data_decisione>
    <data_deposito>29/04/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 4 della legge    &#13;
 27 luglio 1962, n. 1115 (Estensione dei benefici previsti dalla legge    &#13;
 12 aprile 1943, n. 455, ai lavoratori colpiti da silicosi associata o    &#13;
 no da altre forme morbose  contratta  nelle  miniere  di  carbone  in    &#13;
 Belgio  e  rimpatriati), promosso con ordinanza emessa il 19 dicembre    &#13;
 1991 dal Tribunale di Brescia nel procedimento  civile  vertente  tra    &#13;
 Salari  Gaudenzio  e  l'I.N.A.I.L.,  iscritta  al n. 668 del registro    &#13;
 ordinanze 1992 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 43, prima serie speciale, dell'anno 1992;                             &#13;
    Visti  gli  atti   di   costituzione   di   Salari   Gaudenzio   e    &#13;
 dell'I.N.A.I.L.;                                                         &#13;
    Udito  nella  camera  di  consiglio  del  24 marzo 1993 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto che, nel corso di un giudizio promosso per  ottenere  una    &#13;
 rendita  maggiore  di  quella  riconosciuta in dipendenza di silicosi    &#13;
 contratta in Belgio, il Tribunale di  Brescia,  sezione  lavoro,  con    &#13;
 ordinanza  del 19 dicembre 1991 (pervenuta alla Corte il 24 settembre    &#13;
 1992), ha sollevato d'ufficio,  in  riferimento  agli  artt.  3,  35,    &#13;
 quarto  comma,  e  38  della  Costituzione, questione di legittimità    &#13;
 costituzionale dell'art. 4  della  legge  27  luglio  1962,  n.  1115    &#13;
 (Estensione dei benefici previsti dalla legge 12 aprile 1943, n. 455,    &#13;
 ai  lavoratori  colpiti  da  silicosi  associata  o no ad altre forme    &#13;
 morbose contratta nelle miniere di carbone in Belgio e  rimpatriati),    &#13;
 che  fissa  in  15  anni  dalla  data  di abbandono della lavorazione    &#13;
 morbigena il "periodo massimo di indennizzabilità";                     &#13;
      che  il  giudice  a  quo  ha  rilevato  che  il  suddetto limite    &#13;
 temporale, previsto per i lavoratori italiani che  abbiano  contratto    &#13;
 la    malattia   in   Belgio,   non   è   invece   applicabile   per    &#13;
 l'indennizzabilità a favore dei lavoratori che tale malattia abbiano    &#13;
 contratto in patria; il che determinerebbe una palese discriminazione    &#13;
 -  anche  in  considerazione  dell'evoluzione  subita   dal   sistema    &#13;
 previdenziale  per effetto della sentenza n. 54/1981 di questa Corte,    &#13;
 che  aveva  dichiarato  l'illegittimità  costituzionale  di  analoga    &#13;
 disposizione  -  oltreché la violazione del diritto di emigrazione e    &#13;
 di quello alla previdenza sociale;                                       &#13;
      che si è costituito in  giudizio  l'I.N.A.I.L.,  sostenendo  la    &#13;
 inammissibilità della questione in quanto irrilevante nel giudizio a    &#13;
 quo, ove la norma impugnata non può trovare applicazione sia perché    &#13;
 essa  concerne  la  costituzione ex novo della rendita per silicosi e    &#13;
 non, come nella specie, la revisione del beneficio già concesso, sia    &#13;
 perché in ogni  caso  dove  ritenersi  implicitamente  abrogata  per    &#13;
 effetto della sentenza di questa Corte n. 54/1981;                       &#13;
      che   si   è   costituita,   altresì,  la  parte  privata  con    &#13;
 argomentazioni a sostegno della fondatezza della questione;              &#13;
    Considerato  che  con  la  sentenza  n.  436  del  1992  è  stata    &#13;
 dichiarata l'illegittimità costituzionale dell'art. 4 della legge 27    &#13;
 luglio  1962,  n.  1115,  che  è  la  norma  ora denunciata; onde la    &#13;
 questione è manifestamente inammissibile;                               &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta inammissibilità dell'art. 4 della  legge  27    &#13;
 luglio 1962, n. 1115 (Estensione dei benefici previsti dalla legge 12    &#13;
 aprile 1943, n. 455, ai lavoratori colpiti da silicosi associata o no    &#13;
 ad altre forme morbose contratta nelle miniere di carbone in Belgio e    &#13;
 rimpatriati)  sollevata,  in  riferimento  agli  artt.  3, 35, quarto    &#13;
 comma, e 38 della Costituzione,  dal  Tribunale  di  Brescia  -  sez.    &#13;
 lavoro, con l'ordinanza indicata in epigrafe.                            &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 21 aprile 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 29 aprile 1993.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
