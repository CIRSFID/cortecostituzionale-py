<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>71</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:71</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Nicola Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/03/1971</data_decisione>
    <data_deposito>05/04/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art.  32, primo  &#13;
 comma, dell'Ordinamento giudiziario approvato con r.d. 30 gennaio 1941,  &#13;
 n. 12, promosso con ordinanza emessa il 13 maggio 1969 dal tribunale di  &#13;
 Trieste nel procedimento penale a carico di Lo Martire Pompeo ed altri,  &#13;
 iscritta al n. 291 del  registro  ordinanze  1969  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 200 del 6 agosto 1969.            &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  10  febbraio  1971  il  Giudice  &#13;
 relatore Nicola Reale;                                                   &#13;
     udito  il sostituto avvocato generale dello Stato Franco Chiarotti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza del 13 maggio 1969, nel corso del giudizio di appello  &#13;
 proposto dall'imputato  Lo  Martire  Pompeo  avverso  la  sentenza  del  &#13;
 pretore  di Trieste, che lo aveva condannato per il reato continuato di  &#13;
 atti osceni, il tribunale  di  detta  città,  accogliendo  l'eccezione  &#13;
 della  difesa,  ha  denunziato  l'art.  32 dell'Ordinamento giudiziario  &#13;
 (approvato  con  r.d.  30  gennaio  1941,  n.  12),  prospettandone  il  &#13;
 contrasto con l'art. 101, secondo comma, della Costituzione.             &#13;
     Premesso  che  nella  specie  la  sentenza di primo grado era stata  &#13;
 pronunziata, a seguito di dibattimento, da un  vice  pretore  onorario,  &#13;
 iscritto  nell'albo  dei procuratori legali ed esercente la professione  &#13;
 forense  nella  stessa  circoscrizione  di  Trieste,  il  tribunale  ha  &#13;
 ritenuto la questione rilevante ai fini del giudizio sull'impugnazione,  &#13;
 a motivo della quale risultava dedotta, fra l'altro, ai sensi dell'art.  &#13;
 185,   n.   1  cod.  proc.  pen.,  la  nullità  del  procedimento  per  &#13;
 l'inosservanza dei requisiti di capacità del giudice.                   &#13;
     Nel merito il tribunale ha osservato che non senza fondamento  può  &#13;
 dubitarsi,   in   riferimento   al   principio   costituzionale   della  &#13;
 indipendenza   del   giudice,   della   legittimità    dell'art.    32  &#13;
 dell'Ordinamento  giudiziario,  nella parte concernente la possibilità  &#13;
 che procuratori esercenti siano nominati vice pretori onorari.           &#13;
     Anche    ai    giudici   onorari,   ancorché   appartengano   solo  &#13;
 temporaneamente all'ordine giudiziario, dovrebbe infatti, essere esteso  &#13;
 il  divieto,  stabilito  per  i  magistrati  ordinari  e  speciali,  di  &#13;
 assumere,  durante  la carica, impieghi ed uffici pubblici e privati e,  &#13;
 in particolare, di esercitare  attività  professionali  che,  per  gli  &#13;
 interessi  ad  esse  connessi,  possano  incidere  sulla obiettività e  &#13;
 indipendenza dei giudizi.                                                &#13;
     Costituitasi  davanti  a  questa  Corte   in   rappresentanza   del  &#13;
 Presidente  del  Consiglio  dei  ministri,  l'Avvocatura generale dello  &#13;
 Stato ha sostenuto essere la questione infondata.                        &#13;
     Il principio della soggezione  del  giudice  soltanto  alla  legge,  &#13;
 espressamente   enunciato   nel   secondo  comma  dell'art.  101  della  &#13;
 Costituzione, sarebbe erroneamente richiamato dal tribunale di Trieste,  &#13;
 che non ne avrebbe colto  l'esatto  significato,  volto  unicamente  ad  &#13;
 escludere  che il giudice possa essere subordinato o vincolato ad altri  &#13;
 soggetti ed in particolare agli altri poteri dello Stato.                &#13;
     Nella specie, si è argomentato, il vice pretore onorario, nominato  &#13;
 in base ai requisiti preveduti dal menzionato art. 32  dell'Ordinamento  &#13;
 giudiziario,  non  potrebbe  dirsi  privo  di  autonomia,  solo perché  &#13;
 esercente la professione di procuratore legale.                          &#13;
     L'esercizio  di  tale  attività  professionale  non   importerebbe  &#13;
 infatti  vincoli,  giuridicamente  rilevanti,  di  dipendenza  da altri  &#13;
 soggetti,  essendone,  anzi,  preveduta   dalle   leggi   professionali  &#13;
 l'incompatibilità   assoluta  con  attività  di  lavoro  subordinato,  &#13;
 particolarmente di tipo impiegatizio.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con l'ordinanza del tribunale di Trieste viene  sollevata,  in  &#13;
 riferimento all'art. 101, secondo comma, della Costituzione, enunciante  &#13;
 il  principio  che  i  giudici  sono  soggetti  soltanto alla legge, la  &#13;
 questione di legittimità costituzionale  dell'art.  32,  primo  comma,  &#13;
 dell'Ordinamento  giudiziario  (approvato  con r.d. 30 gennaio 1941, n.  &#13;
 12), in quanto prevede la nomina a vice pretori onorari di  procuratori  &#13;
 legali  esercenti,  ai  quali  non  è  vietato,  quindi, di esercitare  &#13;
 attività professionali; con la conseguenza che gli interessi a  queste  &#13;
 connessi  possano  incidere  sulla  obiettività  ed  imparzialità dei  &#13;
 giudizi.                                                                 &#13;
     Dall'oggetto della questione esorbita, pertanto, il  secondo  comma  &#13;
 di  detta  norma,  concernente  la destinazione temporanea, in luogo di  &#13;
 uditori giudiziari e nelle preture ove questi manchino, di vice pretori  &#13;
 onorari, ai quali è inibito, per la durata dell'incarico,  l'esercizio  &#13;
 della professione forense.                                               &#13;
     In  correlazione,  poi,  col  giudizio  di rilevanza espresso nella  &#13;
 specie dal tribunale con riguardo esclusivo alla posizione giuridica di  &#13;
 un procuratore legale investito delle  funzioni  di  vice  pretore,  va  &#13;
 anche chiarito, sempre in via preliminare, che l'attuale contestazione,  &#13;
 ancorché genericamente rivolta dall'ordinanza al primo comma dell'art.  &#13;
 32,  è  in  effetti  diretta  alla sola disposizione concernente detta  &#13;
 categoria di soggetti, e non a quella relativa ai notai, il cui assetto  &#13;
 professionale ha caratteri istituzionalmente propri e peculiari.         &#13;
     2. - Nel merito la questione non è fondata.                         &#13;
     Il vigente ordinamento prevede la nomina, da  parte  del  Consiglio  &#13;
 superiore  della  magistratura,  su proposta dei Presidenti delle Corti  &#13;
 d'appello, per la durata di  un  triennio  e  con  la  possibilità  di  &#13;
 conferma  per  ulteriori  periodi, di vice pretori onorari scelti fra i  &#13;
 detti  procuratori  legali,  nonché  fra  laureati in giurisprudenza e  &#13;
 notai,  nel  numero  massimo  di  due  per  ciascuna   pretura,   salvo  &#13;
 particolari esigenze di servizio.                                        &#13;
     Per  l'importanza  e  delicatezza delle funzioni giudiziarie, cui i  &#13;
 vice pretori onorari sono chiamati, è   richiesto  l'accertamento  che  &#13;
 essi,   per  la  loro  condotta,  diano  sicuro  affidamento  di  poter  &#13;
 degnamente  esercitare  le  funzioni  medesime.  In  particolare  e  in  &#13;
 conseguenza  del  carattere  non retribuito dell'attività demandata ai  &#13;
 detti magistrati onorari, che  non  esclude  lo  svolgimento  di  altra  &#13;
 attività  professionale,  ogni cautela è posta nell'accertarsi che la  &#13;
 detta attività non possa  determinare,  tenendosi  conto  anche  delle  &#13;
 caratteristiche  dell'ambiente,  pericoli di parzialità nell'esercizio  &#13;
 delle funzioni giudiziarie. In proposito va menzionata  la  circostanza  &#13;
 che ai requisiti comuni ad ogni altro pubblico funzionario, riguardanti  &#13;
 la  preparazione tecnica e la irreprensibilità della condotta morale e  &#13;
 civile,  nonché  la  inesistenza  delle  cause   di   incompatibilità  &#13;
 derivanti da attività industriali e commerciali, comprovate mediante i  &#13;
 rituali  certificati  e le informazioni delle autorità amministrative,  &#13;
 il Consiglio superiore della magistratura richiede,  nei  confronti  di  &#13;
 coloro  che  aspirano alle funzioni di vice pretore onorario, il parere  &#13;
 del competente Consiglio dell'Ordine degli avvocati e procuratori e  la  &#13;
 dichiarazione  con  la  quale  gli  stessi interessati si impegnano, in  &#13;
 quanto esercenti la professione forense, a non trattare  cause  innanzi  &#13;
 alla pretura presso la quale chiedono di essere nominati (sempre quando  &#13;
 questo  non  sia  l'unico  ufficio  del  luogo), ovvero alla sezione di  &#13;
 pretura cui venissero destinati, ove si tratti di pretura divisa in piu  &#13;
 sezioni.                                                                 &#13;
     In conseguenza e pur escludendosi, secondo le istruzioni  impartite  &#13;
 dal  Consiglio  superiore,  nei  riguardi  dei vice pretori onorari non  &#13;
 reggenti, le disposizioni in materia di  incompatibilità  di  funzioni  &#13;
 riguardanti  i  magistrati  dell'ordine  giudiziario,  ad  eccezione di  &#13;
 quelle sopramenzionate, la nomina dei vice pretori onorari, così  come  &#13;
 la  loro  conferma,  è  subordinata  a  caute  valutazioni  miranti ad  &#13;
 assicurare  che  nell'esercizio  delle  attribuzioni  conferitegli,  il  &#13;
 giudice  rimanga  soggetto  soltanto  alla  legge,  secondo il precetto  &#13;
 dell'art. 101, secondo comma, della Costituzione e, quindi, sottratto a  &#13;
 pressioni  od  ingerenze  che  valgano  a  diminuirne  le  garanzie  di  &#13;
 imparzialità.                                                           &#13;
     A questo stesso fine è preordinata la revoca dell'incarico, quando  &#13;
 vengano   meno   i   requisiti   legittimanti  la  stessa  attribuzione  &#13;
 dell'ufficio o si manifestino  nuove  circostanze  che  ne  sconsiglino  &#13;
 l'ulteriore esplicazione.                                                &#13;
     Con   riferimento   ai  singoli  processi  affidati  al  magistrato  &#13;
 onorario, non vanno trascurati, infine, i rimedi previsti dai codici di  &#13;
 rito penale e civile, concernenti  le  incompatibilità  specifiche,  i  &#13;
 doveri di astensione e le cause di ricusazione.                          &#13;
     Orbene, in base a tale complessa normativa, non può ammettersi che  &#13;
 non  risulti  garantita  in  concreto  la posizione assolutamente super  &#13;
 partes del giudice onorario, con l'esclusione, come esige  il  precetto  &#13;
 della  Costituzione  sopra  ricordato,  di  qualsiasi  anche  indiretto  &#13;
 interesse alla causa da decidere e di qualsiasi  aspettativa  tanto  di  &#13;
 vantaggi quanto di pregiudizi.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 32, primo comma, dell'Ordinamento giudiziario  approvato  con  &#13;
 r.d.  30  gennaio  1941,  n. 12, nella parte in cui prevede la nomina a  &#13;
 vice pretori onorari di procuratori.esercenti; questione sollevata, con  &#13;
 l'ordinanza di cui in epigrafe, in riferimento all'art.   101,  secondo  &#13;
 comma, della Costituzione.                                               &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 25 marzo 1971.                                &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
