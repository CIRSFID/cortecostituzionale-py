<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>90</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:90</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1995</data_decisione>
    <data_deposito>17/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 146, n. 3, del    &#13;
 codice penale, così come  modificato  dall'art.  1  della  legge  14    &#13;
 luglio  1993,  n.  222,  che ha convertito il decreto-legge 14 maggio    &#13;
 1993, n. 139 (Disposizioni urgenti relative al trattamento di persone    &#13;
 detenute affette da HIV e di  tossicodipendenti),  promossi  con  due    &#13;
 ordinanze  emesse  il 12 aprile 1994 dal Tribunale di sorveglianza di    &#13;
 Torino nei procedimenti riuniti  di  sorveglianza  nei  confronti  di    &#13;
 Bergamo Ciro e di Moscaritolo Michelangelo, iscritte ai nn. 526 e 527    &#13;
 del  registro  ordinanze  1994  e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 39, prima serie speciale, dell'anno 1994.            &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 22 febbraio 1995 il Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto che il Tribunale di sorveglianza di Torino ha  sollevato,    &#13;
 con  due  ordinanze  di identico contenuto, questione di legittimità    &#13;
 costituzionale dell'art. 146 n. 3 del codice  penale,  come  inserito    &#13;
 dall'art.  2  del  decreto-legge 14 maggio 1993, n. 139 (Disposizioni    &#13;
 urgenti relative al trattamento di persone detenute affette da HIV  e    &#13;
 di  tossicodipendenti)  convertito, con modificazioni, dalla legge 14    &#13;
 luglio  1993,  n.  222,  nella  parte  in  cui  prevede   il   rinvio    &#13;
 obbligatorio  dell'esecuzione  della  pena  se  deve  avere luogo nei    &#13;
 confronti di persona affetta da HIV nei casi di incompatibilità  con    &#13;
 lo  stato  di  detenzione  ai  sensi dell'art. 286- bis, comma 1, del    &#13;
 codice di procedura penale;                                              &#13;
      che a tal proposito  il  giudice  a  quo  deduce  la  violazione    &#13;
 dell'art.  3,  primo comma, della Costituzione in quanto "la malattia    &#13;
 continuerà a sussistere  e  la  pena  detentiva  di  fatto  resterà    &#13;
 sospesa  sine  die",  con effetto asseritamente "deflagrante rispetto    &#13;
 alla sistematicità dell'ordinamento e alla razionalità  dell'intero    &#13;
 sistema",  giacché  qualunque  reato  commetta  la  persona  che  ha    &#13;
 ottenuto  il  differimento  dell'esecuzione  della  pena  "vi  è  la    &#13;
 certezza  che nessuna sanzione penale potrà essere eseguita nei suoi    &#13;
 confronti";                                                              &#13;
      che  in  uno  dei  giudizi  è  intervenuto  il  Presidente  del    &#13;
 Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura    &#13;
 Generale dello Stato,  chiedendo  che  la  questione  sia  dichiarata    &#13;
 inammissibile;                                                           &#13;
    Considerato che le ordinanze sollevano la medesima questione e che    &#13;
 pertanto i relativi giudizi vanno riuniti per essere decisi con unico    &#13;
 provvedimento;                                                           &#13;
      che questa Corte, chiamata a pronunciarsi su questione del tutto    &#13;
 analoga,  ne  ha  dichiarato la non fondatezza (v. sentenza n. 70 del    &#13;
 1994) osservando, fra l'altro,  che,  dovendosi  porre  a  fondamento    &#13;
 della  nuova  ipotesi  di  differimento  della  esecuzione della pena    &#13;
 "l'esigenza di assicurare il  diritto  alla  salute  nel  particolare    &#13;
 consorzio  carcerario",  ne  deriva che la liberazione del condannato    &#13;
 non può "ritenersi frutto  di  una  scelta  arbitraria"  così  come    &#13;
 neppure  può affermarsi "che la liberazione stessa integri, sempre e    &#13;
 comunque, un fattore di compromissione delle contrapposte esigenze di    &#13;
 tutela collettiva", giacché non è la pena differita in quanto  tale    &#13;
 "a  determinare una situazione di pericolo, ma, semmai, la carenza di    &#13;
 adeguati  strumenti  preventivi  volti ad impedire che il condannato,    &#13;
 posto in libertà, commetta nuovi reati";                                &#13;
      e che,  pertanto,  non  adducendo  le  ordinanze  di  rimessione    &#13;
 argomenti  nuovi  o  diversi da quelli allora esaminati, la questione    &#13;
 deve essere dichiarata manifestamente infondata;                         &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi;  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale dell'art. 146, primo  comma,    &#13;
 n.  3  del codice penale, come inserito dall'art. 2 del decreto-legge    &#13;
 14 maggio 1993, n. 139 (Disposizioni urgenti relative al  trattamento    &#13;
 di   persone   detenute   affette  da  HIV  e  di  tossicodipendenti)    &#13;
 convertito, con modificazioni, dalla legge 14 luglio  1993,  n.  222,    &#13;
 sollevata,   in   riferimento  all'art.  3  della  Costituzione,  dal    &#13;
 Tribunale di sorveglianza di Torino con le ordinanze in epigrafe.        &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1995.                                  &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 17 marzo 1995.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
