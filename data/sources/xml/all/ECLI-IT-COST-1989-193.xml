<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>193</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:193</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/04/1989</data_decisione>
    <data_deposito>12/04/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  92, primo    &#13;
 comma, del d.P.R. 29  settembre  1973,  n.  602  (Disposizioni  sulla    &#13;
 riscossione delle imposte sul reddito), promosso con ordinanza emessa    &#13;
 il 4 marzo 1988 dalla Commissione  Tributaria  di  secondo  grado  di    &#13;
 Pesaro  sul  ricorso  proposto  dalla  U.S.L.  n.  3 di Pesaro contro    &#13;
 l'Ufficio delle Imposte Dirette di Pesaro, iscritta  al  n.  613  del    &#13;
 registro  ordinanze  1988 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 46, prima serie speciale, dell'anno 1988;                  &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 25 gennaio 1989 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto che nel corso di un procedimento iniziato dalla U.S.L. n.    &#13;
 3 di Pesaro contro l'Ufficio Imposte  Dirette  della  stessa  città,    &#13;
 avente  ad  oggetto  la  soprattassa di cui all'art. 92 del d.P.R. 29    &#13;
 settembre 1973, n.  602,  per  ritardato  versamento  delle  ritenute    &#13;
 operate  su redditi di lavoro autonomo e dipendente relativi all'anno    &#13;
 1982, la  Commissione  Tributaria  di  secondo  grado  di  Pesaro  ha    &#13;
 sollevato,  con  ordinanza  del  4 marzo 1988 (R.O. n. 613 del 1988),    &#13;
 questione di legittimità  costituzionale  del  citato  art.  92,  in    &#13;
 riferimento agli artt. 3, 76 e 77 della Costituzione;                    &#13;
      che,  ad  avviso  della Commissione remittente, l'equiparazione,    &#13;
 sotto il profilo sanzionatorio, operata dalla  norma  denunciata  fra    &#13;
 due  situazioni  non  omogenee,  quali  quella di chi spontaneamente,    &#13;
 seppure tardivamente, adempia l'obbligazione tributaria, e quella  di    &#13;
 chi  ometta  il  versamento  fino  a  quando,  rilevata  dall'Ufficio    &#13;
 competente  la  omissione,  sia  costretto   ad   adempiere,   appare    &#13;
 irragionevole   e,   pertanto,  in  contrasto  con  il  principio  di    &#13;
 eguaglianza di cui all'art. 3 della Costituzione;                        &#13;
      che,  inoltre,  la  norma  censurata violerebbe, ad avviso della    &#13;
 stessa Commissione, gli artt. 76 e 77 della Costituzione,  in  quanto    &#13;
 esorbiterebbe  dai  limiti  posti dall'art. 10, n. 11, della legge di    &#13;
 delega 9 ottobre 1971, n.  825,  il  quale  imponeva  al  legislatore    &#13;
 delegato  di  commisurare  le  sanzioni  all'effettiva gravità delle    &#13;
 violazioni;                                                              &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 Ministri per il tramite dell'Avvocatura Generale dello Stato, che  ha    &#13;
 concluso per la manifesta inammissibilità della questione.              &#13;
    Considerato  che  identiche questioni, con riferimento ai medesimi    &#13;
 profili, sono già state dichiarate manifestamente inammissibili  con    &#13;
 ordinanze  n.  132  e  n.  954  del  1988,  in base al rilievo che la    &#13;
 relativa  censura  trova  ostacolo  nell'impossibilità,  per  questa    &#13;
 Corte, di dettare una disciplina positiva della materia sostituendosi    &#13;
 al  legislatore  in  scelte  discrezionali,   essendo   evidentemente    &#13;
 necessario  stabilire  pur sempre un limite di tempo massimo oltre il    &#13;
 quale ritardo ed omissione si equivalgono;                               &#13;
      che,  in  ogni  caso, il maggiore o minore perdurare del ritardo    &#13;
 non rimane privo di giuridico rilievo,  riflettendosi  sull'ammontare    &#13;
 degli  interessi  dovuti  dal contribuente moroso (vedi artt. 9 e 92,    &#13;
 ultimo comma, del d.P.R. n. 602 del 1973);                               &#13;
      che, non essendo stati proposti motivi nuovi o diversi, non v'è    &#13;
 ragione di modificare le precedenti decisioni  e  che,  pertanto,  la    &#13;
 questione appare manifestamente inammissibile.                           &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art.  92  del  d.P.R.  29  settembre    &#13;
 1973,  n.  602  (Disposizioni  sulla  riscossione  delle  imposte sul    &#13;
 reddito), in riferimento agli artt. 3, 76 e  77  della  Costituzione,    &#13;
 sollevata dalla Commissione Tributaria di secondo grado di Pesaro con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 10 aprile 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 12 aprile 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
