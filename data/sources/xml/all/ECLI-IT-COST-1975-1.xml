<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>1</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:1</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Nicola Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/01/1975</data_decisione>
    <data_deposito>16/01/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità  costituzionale  dell'art.  83,  &#13;
 sesto comma, del d.P.R. 15 giugno 1959, n. 393 (Testo unico delle norme  &#13;
 concernenti la disciplina della circolazione stradale), promossi con le  &#13;
 seguenti ordinanze:                                                      &#13;
     1)  ordinanza  emessa  il 14 giugno 1972 dal pretore di Firenze nel  &#13;
 procedimento penale a carico di Camiciotti Sergio, iscritta al  n.  279  &#13;
 del registro ordinanze 1972 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 254 del 27 settembre 1972;                                 &#13;
     2)  ordinanza  emessa  il 14 novembre 1972 dal pretore di Arena nel  &#13;
 procedimento penale a carico di Schinella Antonio ed altro, iscritta al  &#13;
 n. 44 del registro ordinanze 1973 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 81 del 28 marzo 1973.                                &#13;
     Visto  l'atto  d'intervento  del  Presidente  del   Consiglio   dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 23 ottobre 1974 il Giudice relatore  &#13;
 Nicola Reale;                                                            &#13;
     udito  il sostituto avvocato generale dello Stato Giorgio Azzariti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Nel corso del  procedimento  penale  a  carico  di  Camiciotti  &#13;
 Sergio,  imputato  del  reato  di  guida senza patente per essere stato  &#13;
 sorpreso alla guida  di  un  motoveicolo  sprovvisto  della  prescritta  &#13;
 patente  di  guida  mentre  - a suo dire - stava esercitandosi sotto la  &#13;
 sorveglianza di persona che, stando sul sedile posteriore,  fungeva  da  &#13;
 istruttore,  il  pretore di Firenze ha sollevato d'ufficio questione di  &#13;
 legittimità costituzionale dell'art. 83, comma sesto,  del  d.P.R.  15  &#13;
 giugno 1959, n. 393, in riferimento all'art. 3 della Costituzione.       &#13;
     Si  assume  nell'ordinanza  che  la  norma  impugnata violerebbe il  &#13;
 principio di uguaglianza poiché escluderebbe senza  alcuna  plausibile  &#13;
 ragione  dalla  pena  alternativa dell'arresto o dell'ammenda, prevista  &#13;
 per i casi di esercitazione effettuata senza autorizzazione ma sotto la  &#13;
 sorveglianza di un  istruttore,  chi  si  esercita  alla  guida  di  un  &#13;
 motoveicolo  di  cat.  A  ad uso privato (quale nella specie era quello  &#13;
 condotto dal Camiciotti), e che  resterebbe  così  esposto  alla  più  &#13;
 grave  sanzione  (pena congiunta dell'arresto e dell'ammenda) comminata  &#13;
 dall'art. 80 comma decimo (ora tredicesimo, per le modifiche  apportate  &#13;
 dall'art.  2  della  legge 14 febbraio 1974, n. 62) per i casi di guida  &#13;
 senza patente.                                                           &#13;
     Nel giudizio non  si  sono  costituite  parti  private.  È  invece  &#13;
 intervenuto  il  Presidente del Consiglio dei ministri, rappresentato e  &#13;
 difeso dall'Avvocatura dello Stato con deduzioni in data  15  settembre  &#13;
 1972,  in  cui  si  assume  che  la  diversità di disciplina è invece  &#13;
 giustificata dal fatto che per il conseguimento della patente di  guida  &#13;
 di  cat.  A  ad  uso  privato non è previsto, a differenza che per gli  &#13;
 altri tipi, il superamento di alcuna prova  pratica  di  abilità  alla  &#13;
 guida.                                                                   &#13;
     La questione andrebbe pertanto dichiarata non fondata.               &#13;
     2.   -   Analoga   questione   è  stata  sollevata  con  identiche  &#13;
 argomentazioni dal pretore di Arena nel corso del procedimento penale a  &#13;
 carico di Schinella Antonio ed altro.                                    &#13;
     In questo secondo giudizio non si è costituita nessuna delle parti  &#13;
 private né vi è stato intervento del  Presidente  del  Consiglio  dei  &#13;
 ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Le due ordinanze di rimessione sollevano la stessa questione  &#13;
 di legittimità costituzionale e, pertanto, i relativi giudizi  possono  &#13;
 essere riuniti e definiti con unica sentenza.                            &#13;
     2.  - Come si è già accennato in narrativa, il pretore di Firenze  &#13;
 ed il pretore  di  Arena  hanno  sollevato  questione  di  legittimità  &#13;
 costituzionale dell'art. 83, comma sesto, del d.P.R. 15 giugno 1959, n.  &#13;
 393,  approvante il testo unico delle norme sulla circolazione stradale  &#13;
 (c.d.  codice  della  strada),  in   riferimento   all'art.   3   della  &#13;
 Costituzione.                                                            &#13;
     Il  comma  primo  del  suddetto art. 83 prevede l'autorizzazione ad  &#13;
 esercitarsi alla guida, prima  del  conseguimento  della  patente,  per  &#13;
 tutti i veicoli eccetto che per i motoveicoli di cat. A ad uso privato,  &#13;
 per  i  quali  l'esame di idoneità per ottenere la patente è limitato  &#13;
 alla  conoscenza  della  segnaletica  e  delle  norme  di  circolazione  &#13;
 stradale  e  non  comprende  anche  una  prova pratica di abilità alla  &#13;
 guida.                                                                   &#13;
     Nelle ordinanze si rileva che il sesto comma del  citato  articolo,  &#13;
 data  la sua formulazione e per il collegamento con il precedente comma  &#13;
 primo, porta a non applicare la sanzione,  alternativa  dell'arresto  o  &#13;
 dell'ammenda,  comminata  a  colui  che  si  esercita  alla guida senza  &#13;
 autorizzazione, ma sotto la sorveglianza di un istruttore, anche a  chi  &#13;
 nelle stesse condizioni si esercita alla guida di motoveicoli cat. A ad  &#13;
 uso  privato, e che rimane quindi assoggettato alla più grave sanzione  &#13;
 prevista dall'art. 80 per i casi di guida senza patente (pena congiunta  &#13;
 dell'arresto e dell'ammenda).                                            &#13;
     Si tratta di un risultato cui, contro l'avviso di quanti sostengono  &#13;
 la liceità del comportamento in oggetto, sono  pervenute  parte  della  &#13;
 dottrina e l'ormai consolidata giurisprudenza della Corte di cassazione  &#13;
 con  una  interpretazione basata sulla lettera della legge e suffragata  &#13;
 da altri argomenti fra cui di particolare rilievo quello che, a volersi  &#13;
 ritenere  la  liceità   del  fatto, rimarrebbe impunito chi si mettesse  &#13;
 alla guida di motoveicoli  di  cat.  A  senza  nemmeno  possedere  quei  &#13;
 requisiti  fisici  e  psichici  negli altri casi necessari, ex art. 83,  &#13;
 comma primo, per ottenere l'autorizzazione ad esercitarsi.               &#13;
     La normativa così intesa è peraltro ritenuta nelle  ordinanze  in  &#13;
 contrasto  con  il principio di uguaglianza sotto il profilo che non vi  &#13;
 sarebbe giustificazione razionale nel punire  con  la  stessa  sanzione  &#13;
 comminata  per chi circola senza aver conseguito la patente il soggetto  &#13;
 che - pur se sprovvisto di  autorizzazione  -  si  esercita,  sotto  la  &#13;
 sorveglianza  di  un istruttore, alla guida di un motoveicolo di cat. A  &#13;
 ad uso privato quando, per contro, viene punito  meno  gravemente,  chi  &#13;
 nelle medesime condizioni e con le stesse finalità si ponga alla guida  &#13;
 di un veicolo di uguale o maggiore pericolosità.                        &#13;
     3. - La questione non e fondata.                                     &#13;
     Questa  Corte,  con  giurisprudenza costante, ha deciso che rientra  &#13;
 nella discrezionalità del  legislatore  statuire  quali  comportamenti  &#13;
 debbano  essere  puniti  e quali debbano essere la qualità e la misura  &#13;
 della pena e che, finché siffatto  potere  sia  contenuto  nei  limiti  &#13;
 della razionalità, non vi è violazione dell'art. 3 della Costituzione  &#13;
 (confr. per tutte la sentenza n.  1601 del 1973).                        &#13;
     Nel  caso in esame non può dirsi che la diversità di trattamento,  &#13;
 messa in evidenza dal pretore di Firenze e  da  quello  di  Arena,  sia  &#13;
 priva  di giustificazione e che, di conseguenza, illegittima, in quanto  &#13;
 ispirata  da  discriminazioni  non  consentite  dal  l'art.   3   della  &#13;
 Costituzione,  risulti  la  disciplina  dettata dall'articolo 83, comma  &#13;
 sesto, del codice della strada.                                          &#13;
     Proprio in relazione al diverso  contenuto  superiormente  riferito  &#13;
 delle  prove  di  esame, l'art. 83, comma primo, prevede il rilascio di  &#13;
 una autorizzazione per esercitarsi  alla  guida  solo  per  i  tipi  di  &#13;
 veicoli diversi da quelli di cat. A ad uso privato.                      &#13;
     Si  comprende,  in  conseguenza, perché sia prevista una pena più  &#13;
 lieve rispetto a quella irrogata per il reato di guida senza patente  a  &#13;
 chi  -  pur non essendovi autorizzato ma avendo a fianco l'istruttore -  &#13;
 si eserciti alla guida di uno dei veicoli per i quali sono previste  in  &#13;
 sede  di  esame,  oltre conoscenze teoriche, anche una prova pratica di  &#13;
 abilità alla guida: in tal caso, infatti, l'esercitazione  alla  guida  &#13;
 è  valutata  come necessaria per il conseguimento della patente, tanto  &#13;
 che si dispone che l'esame di  idoneità  non  possa  essere  sostenuto  &#13;
 prima  che sia trascorso un mese dal rilascio dell'autorizzazione (art.  &#13;
 85, comma quarto).                                                       &#13;
     Nel caso, invece, di motoveicoli di  cat.  A  l'esercitazione  alla  &#13;
 guida  non  è  -  nel  sistema  della  legge in vigore - in alcun modo  &#13;
 collegata con il superamento di una prova  pratica,  vertendo  l'esame,  &#13;
 come   si   è   già  detto,  esclusivamente  sulla  conoscenza  della  &#13;
 segnaletica e delle norme di circolazione stradale.                      &#13;
     4. - Ciò premesso, e a parte la  considerazione  che  la  suddetta  &#13;
 limitazione  della prova di esame non esonera il titolare della patente  &#13;
 dall'acquisire la necessaria pratica nella guida prima di affrontare le  &#13;
 difficoltà e i pericoli della normale circolazione, è evidente che la  &#13;
 disciplina legislativa avrebbe potuto essere diversa.                    &#13;
     Non  sono  mancate,  per  vero,  critiche  per  essersi  esclusa la  &#13;
 necessità  della  prova  pratica  per  l'abilitazione  alla  guida  di  &#13;
 motoveicoli di cat. A ad uso privato e sono state sollecitate, anche in  &#13;
 sede comunitaria, innovazioni legislative.                               &#13;
     Ma  allo  stato,  e  rimanendo  nell'ambito della normativa oggetto  &#13;
 dell'impugnazione, è indubbio che le ipotesi esaminate nelle ordinanze  &#13;
 sono obbiettivamente diverse e che per esse risulta ammissibile  e  non  &#13;
 irrazionale un trattamento differenziato.</testo>
    <dispositivo>per questi motivi                             &#13;
                        LA CORTE  COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 83, comma sesto, del d.P.R. 15 giugno  1959,  n.  393  (Testo  &#13;
 unico   delle   norme  concernenti  la  disciplina  della  circolazione  &#13;
 stradale), sollevata, in riferimento all'art. 3 della Costituzione, dal  &#13;
 pretore di Firenze e dal pretore di Arena con le ordinanze in epigrafe.  &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 9 gennaio 1975.                               &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI - MICHELE ROSSANO       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
