<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>936</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:936</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1988</data_decisione>
    <data_deposito>28/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 415, terzo,    &#13;
 quarto e quinto comma, 645,  secondo  comma,  e  649  del  codice  di    &#13;
 procedura civile, promosso con ordinanza emessa il 25 luglio 1987 dal    &#13;
 pretore di Gravina in Puglia nel  procedimento  civile  vertente  tra    &#13;
 l'Impresa  Edil  Popolare  Gravinese e Aquila Michele, iscritta al n.    &#13;
 641 del registro ordinanze 1987 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 47, prima serie speciale, dell'anno 1987;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  9 giugno 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che, nel corso del procedimento di opposizione a decreto    &#13;
 ingiuntivo,  promosso  innanzi  al  Pretore  di  Gravina  in   Puglia    &#13;
 dall'Impresa  Edil  Popolare  Gravinese  contro  Aquila  Michele,  il    &#13;
 giudice  adito,  con  l'ordinanza  in  epigrafe,  ha  sollevato,   in    &#13;
 riferimento  agli  artt.  3  e  24  Cost.,  questioni di legittimità    &#13;
 costituzionale:                                                          &#13;
       a)  del  combinato  disposto  degli  artt. 415, terzo, quarto e    &#13;
 quinto comma, 645, secondo comma, e 649 cod. proc. civ., nella  parte    &#13;
 in  cui,  prevedendo  e disponendo tassativamente ed inderogabilmente    &#13;
 che "fra la data di notificazione al convenuto - opposto -  e  quella    &#13;
 dell'udienza  di  discussione deve intercorrere un termine non minore    &#13;
 di trenta giorni", non consente che gli artt. 645, secondo  comma,  e    &#13;
 649  cod.  proc.  civ.  possano  in qualche modo trovare applicazione    &#13;
 antecedentemente all'udienza di prima comparizione, fissata  ex  art.    &#13;
 415,  terzo,  quarto  e  quinto  comma, udienza che si identifica con    &#13;
 quella di discussione della causa, realizzando così, in un ordinario    &#13;
 giudizio  di  opposizione  ad  ingiunzione,  emessa  dal  Pretore  in    &#13;
 funzione di giudice del lavoro, che non si diversifica da ogni  altro    &#13;
 giudizio   ordinario   di  opposizione  ad  ingiunzione,  una  tutela    &#13;
 assolutamente diversa, limitativa per l'opponente rispetto  a  quanto    &#13;
 è  possibile  ottenere  in  procedimenti  di  opposizione  a decreti    &#13;
 ingiuntivi non pronunziati dal giudice del lavoro;                       &#13;
       b)  dello  stesso  art.  649  cod.  proc.  civ.,  autonomamente    &#13;
 considerato, perché, pur essendo norma concernente  un  giudizio  di    &#13;
 impugnazione  (del  decreto  ingiuntivo),  realizza,  per un identico    &#13;
 istituto giuridico, quale è quello della sospensione dell'esecuzione    &#13;
 provvisoria,  una più limitata tutela rispetto a quanto previsto nel    &#13;
 secondo comma dell'art. 351 cod. proc. civ.;                             &#13;
      che  nel  giudizio  ha  spiegato  intervento  il  Presidente del    &#13;
 Consiglio  dei  ministri,  rappresentato  e  difeso   dall'Avvocatura    &#13;
 Generale  dello  Stato, che ha concluso per la inammissibilità della    &#13;
 questione;                                                               &#13;
    Considerato  che  la diversità di trattamento tra l'opposizione a    &#13;
 decreto nelle forme del rito ordinario e quella nelle forme del  rito    &#13;
 del lavoro trova, alla stregua dei principi ripetutamente espressi da    &#13;
 questa  Corte,  ragionevole  giustificazione   e   fondamento   nelle    &#13;
 peculiarità  del  rito  speciale,  finalizzate all'accelerazione del    &#13;
 procedimento (anche e principalmente  attraverso  l'eliminazione  del    &#13;
 sistema  della  citazione a udienza fissa e la contestuale previsione    &#13;
 di una rete di termini, tale da sottrarre alle  parti  il  potere  di    &#13;
 determinazione   dei   tempi  di  introduzione  della  lite)  e  alla    &#13;
 concentrazione della  trattazione  nonché  alla  immediatezza  della    &#13;
 pronunzia;                                                               &#13;
      che,  quanto  al  diverso problema della possibilità o meno, in    &#13;
 caso di opposizione a decreto ingiuntivo - sia  essa  proposta  nelle    &#13;
 forme  del  rito  ordinario  o  in  quelle  del  rito  speciale  - di    &#13;
 applicare, ai fini della pronunzia di  provvedimenti  attinenti  alla    &#13;
 provvisoria  esecuzione  del  decreto  stesso, le disposizioni di cui    &#13;
 all'art. 351, deve anzitutto rilevarsi che tale ultima norma,  mentre    &#13;
 prevede  che  detta  pronunzia  possa essere resa anche anteriormente    &#13;
 alla prima udienza, implicitamente esclude, contrariamente  a  quanto    &#13;
 opina  il  giudice  remittente,  che  possa  costituire oggetto di un    &#13;
 decreto emesso inaudita altera parte, in  quanto  le  attribuisce  la    &#13;
 forma  dell'ordinanza  e  cioè  di  un  provvedimento che, di norma,    &#13;
 presuppone il previo contraddittorio tra le parti;                       &#13;
      che,   pertanto,  nell'ordinanza  di  rimessione,  la  doglianza    &#13;
 relativa all'asserita inutilizzabilità del rimedio costituito da  un    &#13;
 decreto  di  sospensione  o  revoca,  inaudita  altera  parte,  della    &#13;
 provvisoria  esecuzione  dell'ingiunzione  anteriormente  alla  prima    &#13;
 udienza  appare  posta  in  riferimento  ad  un tertium comparationis    &#13;
 inesistente;                                                             &#13;
      che,  inoltre,  non si rinvengono ostacoli di rilievo (specie se    &#13;
 si configura, come sembra adombrare il giudice remittente, l'atto  di    &#13;
 opposizione a decreto ingiuntivo come introduttivo di un procedimento    &#13;
 di impugnazione) all'applicabilità, anche in tale procedimento ed in    &#13;
 via  analogica,  delle  disposizioni  del  citato art. 351 cod. proc.    &#13;
 civ., ai fini di  una  delibazione  anticipata  rispetto  all'udienza    &#13;
 delle  questioni  concernenti  la provvisoria esecuzione del decreto,    &#13;
 come è stato ritenuto anche in giurisprudenza, con  riferimento  sia    &#13;
 al rito ordinario, sia al rito speciale del lavoro;                      &#13;
      che,  conseguentemente,  non  si  rinvengono  la  disparità  di    &#13;
 trattamento e le violazioni del  diritto  di  difesa  ipotizzate  dal    &#13;
 giudice a quo, sicché la questione appare manifestamente infondata;     &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 415, terzo, quarto e  quinto  comma,  645,    &#13;
 secondo  comma,  e 649 cod. proc civ., sollevata, in riferimento agli    &#13;
 artt. 3 e 24 Cost., dal Pretore di Gravina in Puglia con  l'ordinanza    &#13;
 in epigrafe.                                                             &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, l'8 luglio 1988            &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 28 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
