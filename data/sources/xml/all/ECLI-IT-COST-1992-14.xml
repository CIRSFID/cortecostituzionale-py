<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>14</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:14</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/1992</data_decisione>
    <data_deposito>22/01/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale dell'art. 443, secondo    &#13;
 comma, del codice  di  procedura  penale  promossi  con  le  seguenti    &#13;
 ordinanze:                                                               &#13;
      1)  ordinanza  emessa il 19 aprile 1991 dalla Corte d'appello di    &#13;
 Roma nel procedimento penale a carico di Beniamino Narduzzi, iscritta    &#13;
 al n. 525 del registro ordinanze 1991  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  33, prima serie speciale, dell'anno    &#13;
 1991;                                                                    &#13;
      2)  ordinanza  emessa il 26 febbraio 1991 dal Pretore di Catania    &#13;
 nei procedimenti penali riuniti a carico di Antonia Crimaldi ed altri    &#13;
 iscritta al n. 588 del registro ordinanze  1991  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  39, prima serie speciale,    &#13;
 dell'anno 1991;                                                          &#13;
      3) ordinanza emessa il 21 giugno 1991 dalla Corte di  cassazione    &#13;
 sul  ricorso  proposto  da  Leonardo  Sanna  iscritta  al  n. 627 del    &#13;
 registro ordinanze 1991 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 40, prima serie speciale, dell'anno 1991;                  &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 18 dicembre  1991  il  Giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
    Ritenuto  che  nel  processo  di  appello  avverso  la sentenza di    &#13;
 condanna emessa a seguito di giudizio  abbreviato  dal  Tribunale  di    &#13;
 Roma nei confronti di Beniamino Narduzzi, la Corte d'appello di Roma,    &#13;
 con  ordinanza  del  19  aprile  1991  (R.O.  n.  525  del  1991), ha    &#13;
 dichiarato rilevante e non manifestamente infondata - in  riferimento    &#13;
 all'art.   3  della  Costituzione  -  la  questione  di  legittimità    &#13;
 costituzionale dell'art. 443, secondo comma, del codice di  procedura    &#13;
 penale, che detta limiti all'appello avverso le sentenze adottate con    &#13;
 il  rito  abbreviato,  precludendo  all'imputato l'appello "contro le    &#13;
 sentenze di  condanna  a  una  pena  che  comunque  non  deve  essere    &#13;
 eseguita";                                                               &#13;
      che   nell'ordinanza   di  rinvio  si  evidenzia  come  il  rito    &#13;
 abbreviato corrisponda ad una esigenza di sollecitudine  di  giudizio    &#13;
 "comune,  oltre  che  ad  imputati  colpevoli, ad imputati che sono o    &#13;
 vogliono essere riconosciuti innocenti";                                 &#13;
      che pertanto, secondo il giudice remittente, la conclusione  del    &#13;
 giudizio  abbreviato  con  una sentenza di condanna - anche se a pena    &#13;
 soggetta a sospensione condizionale - da un lato delude l'aspettativa    &#13;
 dell'imputato,  il  quale  fonda  la  propria  convinzione   di   non    &#13;
 colpevolezza  su una valutazione degli elementi di giudizio acquisiti    &#13;
 diversa da quella compiuta dalla sentenza  e,  dall'altro,  impedisce    &#13;
 all'imputato   stesso  di  sperimentare  l'appello,  unico  mezzo  di    &#13;
 impugnazione con cui possono essere dedotti motivi di  merito  ed  è    &#13;
 possibile ottenere una nuova valutazione degli elementi di giudizio;     &#13;
      che  da  tale  situazione  deriverebbe  -  sempre  ad avviso del    &#13;
 giudice a quo - una condizione ingiustificatamente deteriore per  gli    &#13;
 imputati  con  pena  sospesa  rispetto  agli  imputati  che non hanno    &#13;
 ricevuto quel beneficio;                                                 &#13;
      che nel giudizio dinanzi alla Corte ha  spiegato  intervento  il    &#13;
 Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura generale dello Stato, chiedendo che la questione  sia    &#13;
 dichiarata infondata;                                                    &#13;
      che  nel  procedimento  a  carico  di  Antonia Crimaldi ed altri    &#13;
 diciassette imputati del reato di cui  all'art.  20  della  legge  28    &#13;
 febbraio  1985,  n.  47,  il Pretore di Catania, con ordinanza del 26    &#13;
 febbraio 1991 (R.O. n. 588  del  1991),  ha  sollevato  questione  di    &#13;
 legittimità   costituzionale   -   in  relazione  all'art.  3  della    &#13;
 Costituzione - dell'art. 443, secondo comma, del codice di  procedura    &#13;
 penale  che  detta i limiti dell'appello avverso le sentenze adottate    &#13;
 con   il   rito   abbreviato   escludendo   "il  diritto  di  appello    &#13;
 dell'imputato avverso  la  sentenza  di  condanna  ad  una  pena  che    &#13;
 comunque non deve essere eseguita";                                      &#13;
      che  il giudice a quo ha riproposto integralmente la motivazione    &#13;
 di una sua precedente ordinanza di  rimessione  del  12  giugno  1990    &#13;
 (emessa  nello  stesso  procedimento  e  dichiarata  dalla Corte, con    &#13;
 l'ordinanza  n.   566   del   28   dicembre   1990,   "manifestamente    &#13;
 inammissibile"  perché  "prematura"), denunciando il contrasto della    &#13;
 norma impugnata con l'art. 3 della Costituzione;                         &#13;
      che nel giudizio dinanzi alla Corte è intervenuto il Presidente    &#13;
 del Consiglio dei ministri, rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale  dello  Stato,  chiedendo  che  la  questione sia dichiarata    &#13;
 inammissibile e comunque infondata;                                      &#13;
      che con ordinanza del 21 giugno 1991 (R.O. n. 627 del 1991),  la    &#13;
 Corte  di  cassazione  ha  dichiarato  rilevante e non manifestamente    &#13;
 infondata - in relazione all'art. 3 della Costituzione - la questione    &#13;
 di legittimità costituzionale  dell'art.  443,  secondo  comma,  del    &#13;
 codice  di procedura penale, denunciando la ingiustificata disparità    &#13;
 di trattamento operata dalla disposizione impugnata  "tra  l'imputato    &#13;
 che,  non  avendo goduto del beneficio della sospensione condizionale    &#13;
 della pena, è posto nella condizione di impugnare la sentenza con un    &#13;
 gravame che gli consente di provocare  una  nuova  valutazione  delle    &#13;
 prove a suo carico o della gravità oggettiva e soggettiva del reato,    &#13;
 in  funzione  della  misura della pena che è stata a lui in concreto    &#13;
 inflitta, e l'imputato che, solo perché ha goduto di quel beneficio,    &#13;
 è, invece, privato della possibilità di quel gravame,  pur  essendo    &#13;
 egualmente interessato alla assoluzione o ad una pena più mite";        &#13;
      che nel giudizio dinanzi alla Corte è intervenuto il Presidente    &#13;
 del  Consiglio  dei  ministri, rappresentato e difeso dall'Avvocatura    &#13;
 generale dello Stato, chiedendo una pronuncia di infondatezza;           &#13;
    Considerato  che  i  giudizi,  in  quanto  relativi  a   questioni    &#13;
 identiche, vanno riuniti e decisi congiuntamente;                        &#13;
      che  questa  Corte,  con  la sentenza 23 luglio 1991, n. 363, ha    &#13;
 già  dichiarato  l'illegittimità  costituzionale   dell'art.   443,    &#13;
 secondo  comma,  del  codice  di  procedura penale nella parte in cui    &#13;
 stabilisce  che  l'imputato  non  può  proporre  appello  contro  le    &#13;
 sentenze  di  condanna  ad  una  pena  che  comunque  non deve essere    &#13;
 eseguita;                                                                &#13;
      che, pertanto, le  questioni  dedotte,  avendo  ad  oggetto  una    &#13;
 disposizione  già  dichiarata costituzionalmente illegittima, devono    &#13;
 essere dichiarate manifestamente inammissibili;                          &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la manifesta inammissibilità delle    &#13;
 questioni  di  legittimità  costituzionale  dell'art.  443,  secondo    &#13;
 comma,  del  codice  di  procedura  penale, sollevate, in riferimento    &#13;
 all'art. 3 della Costituzione, dalla Corte  d'appello  di  Roma,  dal    &#13;
 Pretore  di  Catania  e dalla Corte di cassazione con le ordinanze di    &#13;
 cui in epigrafe.                                                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 gennaio 1992.                              &#13;
                       Il presidente: BORZELLINO                          &#13;
                          Il redattore: CHELI                             &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 22 gennaio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
