<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-06-23/128/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-06-23/128"/>
          <FRBRalias value="ECLI:IT:COST:2023:128" name="ECLI"/>
          <FRBRdate date="2023-06-23" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="128"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-06-23/128/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-06-23/128/ita@"/>
          <FRBRdate date="2023-06-23" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-06-23/128/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-06-23/128/ita@.xml"/>
          <FRBRdate date="2023-06-23" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2023-05-09" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2023-06-23" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="SCIARRA" href="/akn/ontology/person/it/SCIARRA" showAs="SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2023</akn4cc:anno>
        <akn4cc:numero>128</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2023:128</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>SCIARRA</akn4cc:presidente>
        <akn4cc:relatore>Giulio Prosperetti</akn4cc:relatore>
        <akn4cc:data_decisione>2023-05-09</akn4cc:data_decisione>
        <akn4cc:data_deposito>2023-06-23</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>; Giudici : <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Marco D'ALBERTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei giudizi di legittimità costituzionale degli artt. 1 e 2 della legge della Regione Molise 4 agosto 2022, n. 16, recante «Riconoscimento della legittimità dei debiti fuori bilancio ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">articolo 73, comma 1, lettera e), del decreto legislativo 23 giugno 2011, n. 118</ref> e ss.mm.ii., derivante dal servizio pubblico essenziale, quale la fornitura di connettività e sicurezza nell'ambito del sistema pubblico di connettività reso da Olivetti S.p.A. nell'anno 2017 e 2019», degli artt. 1 e 2 della legge della Regione Molise 4 agosto 2022, n. 17, recante «Riconoscimento della legittimità dei debiti fuori bilancio ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">articolo 73, comma 1, lettera e), del decreto legislativo 23 giugno 2011, n. 118</ref> e ss.mm.ii., derivante dal Servizio Fonia Fissa reso da TIM S.p.A. nell'anno 2020», promossi dal Presidente del Consiglio dei ministri con ricorsi notificati il 4 ottobre 2022, depositati in cancelleria il 6 ottobre successivo, iscritti, rispettivamente, ai numeri 68 e 69 del registro ricorsi 2022 e pubblicati nella Gazzetta Ufficiale della Repubblica numeri 44 e 45, prima serie speciale, dell'anno 2022.
 Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Molise;
 udito nell'udienza pubblica del 9 maggio 2023 il Giudice relatore Giulio Prosperetti;
 uditi l'avvocato dello Stato Alfonso Peluso per il Presidente del Consiglio dei ministri e l'avvocato Claudia Angiolini per la Regione Molise;
 deliberato nella camera di consiglio del 9 maggio 2023.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con due ricorsi analoghi, depositati il 6 ottobre 2022 (reg. ric. n. 68 e n. 69 del 2022), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato: con il primo ricorso, gli artt. 1 e 2 della legge della Regione Molise 4 agosto 2022, n. 16, recante «Riconoscimento della legittimità dei debiti fuori bilancio ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">articolo 73, comma 1, lettera e), del decreto legislativo 23 giugno 2011, n. 118</ref> e ss.mm.ii., derivante dal servizio pubblico essenziale, quale la fornitura di connettività e sicurezza nell'ambito del sistema pubblico di connettività reso da Olivetti S.p.A. nell'anno 2017 e 2019»; con il secondo ricorso, gli artt. 1 e 2 della legge della Regione Molise 4 agosto 2022, n. 17, recante «Riconoscimento della legittimità dei debiti fuori bilancio ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">articolo 73, comma 1, lettera e), del decreto legislativo 23 giugno 2011, n. 118</ref> e ss.mm.ii., derivante dal Servizio Fonia Fissa reso da TIM S.p.A. nell'anno 2020».
 In entrambi i ricorsi, di identico contenuto, il ricorrente deduce la violazione da parte delle disposizioni regionali impugnate degli artt. 81, terzo comma, e 117, secondo comma, lettera e), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, in relazione all'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_3">art. 3 del decreto legislativo 23 giugno 2011, n. 118</ref> (Disposizioni in materia di armonizzazione dei sistemi contabili e degli schemi di bilancio delle Regioni, degli enti locali e dei loro organismi, a norma degli <mref><ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_1">articoli 1</ref> e <ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_2">2 della legge 5 maggio 2009, n. 42</ref></mref>), nonché al principio 9.1 dell'Allegato 4/2 al medesimo decreto legislativo.
 1.1.- Le disposizioni impugnate della legge reg. Molise n. 16 del 2022 stabiliscono: l'art. 1 (Riconoscimento della legittimità di debito fuori bilancio in assenza di preventivo impegno di spesa) che: «[a]i sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">art. 73, comma 1, lettera e) del decreto legislativo 23 giugno 2011, n. 118</ref> "Disposizioni in materia di armonizzazione dei sistemi contabili e degli schemi di bilancio delle Regioni, degli enti locali e dei loro organismi, a norma degli <mref><ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_1">articoli 1</ref> e <ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_2">2 della legge 5 maggio 2009, n. 42</ref></mref>" e ss.mm.ii., è riconosciuta la legittimità del debito fuori bilancio della Regione Molise, per il valore complessivo di euro 2.578,96, derivante dal servizio pubblico essenziale, quale la fornitura di connettività e sicurezza nell'ambito del sistema pubblico di connettività reso da Olivetti S.p.A. nell'anno 2017 e 2019»; l'art. 2 (Norma finanziaria) che: «[a]l finanziamento dei debiti di cui all'articolo 1, dell'importo complessivo di euro 2.578,96 si provvede mediante utilizzo del bilancio di previsione 2021-2023, esercizio 2021», e indica i capitoli di bilancio sui quali effettuare il correlato prelievo.
 1.2.- Le disposizioni impugnate della legge reg. Molise n. 17 del 2022 prevedono: l'art. 1 (Riconoscimento della legittimità di debito fuori bilancio in assenza di preventivo impegno di spesa) che: «[a]i sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">art. 73, comma 1, lettera e) del decreto legislativo 23 giugno 2011, n. 118</ref> "Disposizioni in materia di armonizzazione dei sistemi contabili e degli schemi di bilancio delle Regioni, degli enti locali e dei loro organismi, a norma degli <mref><ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_1">articoli 1</ref> e <ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_2">2 della legge 5 maggio 2009, n. 42</ref></mref>" e ss.mm.ii., è riconosciuta la legittimità del debito fuori bilancio della Regione Molise, per il valore complessivo di euro 16.357,73, derivante dal Servizio Fonia Fissa reso da TIM S.p.A. nell'anno 2020»; l'art. 2 (Norma finanziaria) che: «[a]l finanziamento dei debiti di cui all'articolo 1, dell'importo complessivo di euro 16.357,73 si provvede mediante utilizzo del bilancio di previsione 2021-2023, esercizio 2021», con indicazione dei capitoli di bilancio sui quali effettuare il corrispondente prelievo.
 1.3.- Nei rispettivi atti introduttivi dei giudizi di legittimità costituzionale è prospettato un identico ordine di argomentazioni a sostegno dell'impugnativa.
 Innanzitutto, secondo il ricorrente, le disposizioni regionali impugnate ledono la competenza legislativa statale nella materia «armonizzazione dei bilanci pubblici», di cui all'art. 117, secondo comma, lettera e), Cost.
 Ciò in quanto la copertura finanziaria degli oneri recati dal riconoscimento del debito fuori bilancio operato dalle due leggi regionali impugnate è individuata in riferimento all'esercizio 2021, già decorso, in contrasto con l'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_3">art. 3 del d.lgs. n. 118 del 2011</ref>, che impone alle pubbliche amministrazioni di conformarsi al principio contabile dell'annualità del bilancio previsto dall'Allegato 1, paragrafo 1, secondo cui «[i] documenti del sistema di bilancio, sia di previsione sia di rendicontazione, sono predisposti con cadenza annuale e si riferiscono a distinti periodi di gestione coincidenti con l'anno solare».
 Inoltre, il medesimo parametro costituzionale sarebbe violato in relazione al principio contabile applicato di cui al paragrafo 9.1 dell'Allegato 4/2 al medesimo <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main">d.lgs. n. 118 del 2011</ref>, secondo cui, nel caso in cui il riconoscimento del debito fuori bilancio intervenga successivamente alla scadenza dell'obbligazione, la spesa è impegnata nell'esercizio in cui il debito stesso è riconosciuto.
 Per effetto di tali principi, il ricorrente afferma che le coperture indicate dalle disposizioni regionali impugnate avrebbero, pertanto, dovuto fare riferimento all'anno 2022, nel quale i rispettivi debiti fuori bilancio sono stati riconosciuti, anziché all'esercizio 2021 del bilancio di previsione 2021-2023.
 Ad avviso della difesa statale, è altresì leso l'art. 81, terzo comma, Cost., perché la violazione del principio contabile dell'annualità del bilancio determinerebbe nuovi e maggiori oneri a carico del bilancio regionale che rimarrebbero privi della correlata fonte di finanziamento. Viene richiamato l'<ref href="/akn/it/act/legge/stato/2009-12-31/196/!main#art_19__para_1">art. 19, comma 1, della legge 31 dicembre 2009, n. 196</ref> (Legge di contabilità e finanza pubblica), secondo cui le leggi comportanti oneri a carico dei bilanci dell'amministrazione pubblica devono prevedere l'onere stesso e l'indicazione della copertura finanziaria riferita ai relativi bilanci, annuali e pluriennali.
 2.- La Regione Molise si è costituita nei due giudizi con atti depositati rispettivamente l'11 e il 14 novembre 2022.
 In entrambi gli atti, la resistente, sulla scorta di coincidenti argomentazioni, ha eccepito l'inammissibilità della questione di legittimità costituzionale promossa in riferimento all'art. 81, terzo comma, Cost., assumendo la genericità della motivazione; nel merito, confutate le tesi della difesa statale, ha chiesto il rigetto dei ricorsi.
 2.1.- La difesa regionale premette che le leggi regionali n. 16 e n. 17 del 2022 scaturiscono dal procedimento avviato dalla Regione Molise in forza di deliberazione di Giunta regionale 31 dicembre 2021, n. 506, contenente la proposta di legge per il riconoscimento della legittimità di debiti fuori bilancio, ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_73__para_1__point_e">art. 73, comma 1, lettera e), del d.lgs. n. 118 del 2011</ref>, derivanti da fatture emesse per forniture di servizi di fonia e connettività rese da Fastweb spa, Olivetti spa e TIM spa.
 La copertura del relativo onere (complessivamente pari ad euro 323.590,79) è stata individuata nel bilancio di previsione pluriennale 2021-2023 (esercizio 2021), capitolo 61052, mediante utilizzo dello specifico accantonamento denominato «Fondo copertura debiti fuori bilancio», appositamente formato in sede di assestamento del bilancio 2021, di cui alla legge della Regione Molise 29 dicembre 2021, n. 7 (Assestamento del bilancio di previsione 2021-2023 e modifiche di leggi regionali), a sua volta preceduta dalla legge della Regione Molise 29 dicembre 2021, n. 6 (Rendiconto generale della Regione Molise per l'esercizio finanziario 2020), alle quali ha fatto seguito la deliberazione di Giunta regionale 29 dicembre 2021, n. 470 (Variazione del documento tecnico di accompagnamento e del bilancio finanziario gestionale in applicazione dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_51__para_2">art. 51 comma 2 del d. lgs. 118/2011</ref> e ss.mm.i.. in esecuzione dell'assestamento di bilancio 2021/2023).
 Secondo la difesa della resistente, la mancata impugnazione da parte del Presidente del Consiglio dei ministri delle citate leggi reg. Molise n. 6 e n. 7 del 2021 avrebbe determinato «ovvie conseguenze in termini di stabilità, intangibilità delle risorse finanziarie individuate per la copertura delle maggiori spese rilevate nel corso del 2021 e di immodificabilità delle relative finalità e destinazioni», incidendo sulla procedibilità delle impugnazioni proposte.
 Nel merito, la difesa regionale rappresenta che è nell'esercizio finanziario 2021 che «sono emerse le obbligazioni di pagamento ed è stata rinvenuta la provvista per farvi fronte, benché il Consiglio Regionale si sia riunito nell'anno successivo per approvare formalmente la legge di riconoscimento», e che, a tal fine, la Regione avrebbe utilizzato nel medesimo esercizio 2021 lo «spazio finanziario maggiore di quello che ordinariamente le sarebbe stato permesso (a causa, appunto, dei vincoli posti dalla <ref href="/akn/it/act/legge/stato/2018/145/!main">legge n. 145/2018</ref>)», potendosi essa avvalere della facoltà, riconosciuta dall'<ref href="/akn/it/act/decretoLegge/stato/2021-05-25/73/!main#art_56__para_2">art. 56, comma 2, del decreto-legge 25 maggio 2021, n. 73</ref> (Misure urgenti connesse all'emergenza da COVID-19, per le imprese, il lavoro, i giovani, la salute e i servizi territoriali), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2021-07-23/106/!main">legge 23 luglio 2021, n. 106</ref>, di derogare ai limiti posti dai commi 897 e 898 dell'<ref href="/akn/it/act/legge/stato/2018-12-30/145/!main#art_1">art. 1 della legge 30 dicembre 2018, n. 145</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021); tale deroga non è stata, invece, prevista per l'esercizio finanziario 2022.
 In tale contesto, la difesa regionale deduce: che i debiti oggetto delle leggi regionali n. 16 e n. 17 del 2022 sono stati finanziati con risorse che si sono rese disponibili eccezionalmente e, dunque, utilizzabili una tantum, solamente nell'anno 2021; che le apposite disponibilità di bilancio non avrebbero potuto, pertanto, essere individuate diversamente; che, in ogni caso, «la soluzione prescelta all'epoca dell'adozione della deliberazione n. 506/2021 era e resta in linea con i principi contabili previsti dall'impianto normativo statale, con la garanzia della sottoposizione della proposta di legge di riconoscimento della legittimità del debito all'esame dell'organo legislativo».
 La resistente sostiene che, qualora non si ritenesse di condividere la predetta impostazione, sarebbe lacunoso il principio contabile applicato di cui al paragrafo 9.1 dell'Allegato 4/2 al <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main">d.lgs. n. 118 del 2011</ref>, «nel momento in cui esso non prevede e disciplina la situazione di un ente territoriale che abbia la possibilità di adempiere alle proprie obbligazioni di pagamento, costituenti debiti fuori bilancio, con risorse finanziar[i]e eccezionalmente individuate nell'anno di emersione - e non di riconoscimento - del debito stesso».
 Secondo la difesa regionale, l'aver riferito all'esercizio 2021 la copertura finanziaria degli interventi normativi in oggetto di riconoscimento di debiti fuori bilancio, individuata grazie all'utilizzo del risultato di amministrazione al 31 dicembre 2020, non altererebbe i saldi complessivi e le risultanze finali del conto, garantendo l'obbligo costituzionale di copertura in termini quantitativi, qualitativi e temporali e il rispetto dei principi contabili dell'integrità, della veridicità, dell'attendibilità, della correttezza e della coerenza, nel mentre i predetti principi non sarebbero rispettati «ove gli impegni si dovessero assumere in una annualità per la quale non fossero rinvenibili le necessarie coperture finanziarie».
 D'altro canto, la difesa della resistente afferma che, alla fattispecie, dovrebbe allora applicarsi il principio contabile di cui all'art. 18 dell'Allegato 1 al <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main">d.lgs. n. 118 del 2011</ref>, «ossia quello della prevalenza della sostanza sulla forma, in base al quale se l'informazione contabile deve rappresentare fedelmente ed in modo veritiero le operazioni ed i fatti che sono accaduti durante l'esercizio, è necessario che essi siano rilevati contabilmente secondo la loro natura finanziaria, economica e patrimoniale in conformità alla loro sostanza effettiva e quindi alla realtà economica che li ha generati e ai contenuti della stessa, e non solamente secondo le regole e le norme vigenti che ne disciplinano la contabilizzazione formale».
 Inoltre, la difesa regionale afferma che l'emersione dei debiti fuori bilancio e il loro finanziamento rappresentano «gli esiti di un più ampio percorso virtuoso dell'Ente regionale, ai fini della rappresentazione veritiera dei fatti di gestione contabile», e che tale percorso istituzionale, amministrativo e contabile «sta richiedendo un enorme sforzo di razionalizzazione della spesa regionale, ma ha consentito e consente di dare copertura ai debiti e [di] adempiere alle obbligazioni di pagamento con efficienza e tempestività, garantendo la solvibilità dell'Ente».
 Da ultimo, in ordine alla dedotta violazione dell'art. 81, terzo comma, Cost., la difesa della Regione afferma che essa non sarebbe argomentata e che, pertanto, per la sua genericità, sarebbe inammissibile, non essendo indicate le ragioni dell'assenza di copertura finanziaria. Inoltre, afferma che «anche a volere ipotizzare che l'assenza di copertura derivi, a cascata, dalla dichiarazione di incostituzionalità ai sensi del primo motivo, l'argomentata infondatezza di quest'ultimo, risultante da quanto illustrato nei paragrafi precedenti, comporta, ugualmente a cascata, la manifesta infondatezza del secondo motivo di ricorso».</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con i due ricorsi in epigrafe (reg. ric. n. 68 e n. 69 del 2022), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato: con il primo ricorso, gli artt. 1 e 2 della legge reg. Molise n. 16 del 2022; con il secondo ricorso, gli artt. 1 e 2 della legge reg. Molise n. 17 del 2022.
 Gli artt. 1 e 2 delle leggi reg. Molise n. 16 del 2022 dispongono, rispettivamente, il riconoscimento della legittimità del debito fuori bilancio della Regione Molise derivanti da forniture di servizi resi da Olivetti spa negli anni 2017 e 2019, e la relativa copertura finanziaria dei conseguenti oneri.
 A loro volta, gli artt. 1 e 2 della legge reg. Molise n. 17 del 2022, oggetto del ricorso n. 69 del 2022 stabiliscono il riconoscimento della legittimità del debito fuori bilancio della Regione Molise per il servizio di fonia fissa reso da TIM spa nell'anno 2020 e provvedono alla copertura dei correlati oneri finanziari.
 1.1.- A sostegno delle impugnazioni, il ricorrente svolge un identico ordine di argomentazioni.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Le disposizioni regionali impugnate violerebbero innanzitutto l'art. 117, secondo comma, lettera e), Cost., in riferimento alla competenza legislativa statale in materia di armonizzazione dei bilanci pubblici, in quanto pongono la copertura finanziaria degli oneri recati dal riconoscimento del debito fuori bilancio, rispettivamente previsti a carico dell'esercizio 2021, già decorso e definito, in contrasto sia con il principio dell'annualità del bilancio di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_3">art. 3 del d.lgs. n. 118 del 2011</ref>, che richiama il principio contabile applicato di cui al paragrafo 1 dell'Allegato 1, sia con il principio contabile applicato di cui al paragrafo 9.1 dell'Allegato 4/2 al medesimo <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main">d.lgs. n. 118 del 2011</ref>. Ciò in quanto, in base a quest'ultimo principio contabile, ove il riconoscimento del debito fuori bilancio intervenga successivamente alla scadenza dell'obbligazione, la spesa deve essere impegnata nell'esercizio in cui il debito stesso è riconosciuto.
 Pertanto, secondo il ricorrente, le disposizioni regionali impugnate, in quanto approvate nel 2022, avrebbero dovuto porre a carico dell'esercizio finanziario 2022, e non 2021, gli oneri recati dal riconoscimento dei debiti fuori bilancio da esse riconosciuti.
 Sarebbe, inoltre, leso l'art. 81, terzo comma, Cost. perché la violazione del principio contabile dell'annualità del bilancio determinerebbe nuovi e maggiori oneri a carico del bilancio regionale, che rimarrebbero privi della correlata fonte di finanziamento.
 2.- Va disposta la riunione dei due giudizi, stante la identità delle questioni, dei parametri evocati, delle argomentazioni prospettate nei relativi atti introduttivi e in quelli di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Molise.
 3.- In via preliminare, vanno disattese le eccezioni di inammissibilità sollevate dalla difesa regionale.
 3.1.- Innanzitutto, non è fondata l'eccezione di improcedibilità dei ricorsi per la preclusione che si sarebbe determinata per effetto della mancata impugnazione, da parte del Presidente del Consiglio dei ministri, delle precedenti leggi regionali n. 6 del 2021 (relativa al rendiconto generale per l'esercizio finanziario 2020), e n. 7 del 2021 (relativa all'assestamento del bilancio di previsione 2021-2023), che, secondo la difesa regionale, avrebbe "cristallizzato" l'imputazione al bilancio regionale dell'anno 2021 delle risorse necessarie al ripianamento dei debiti fuori bilancio riconosciuti dalle impugnate disposizioni, di cui alle leggi regionali n. 16 e n. 17 del 2022.
 Per costante giurisprudenza di questa Corte, «[l]'ammissibilità dell'impugnazione, in termini di tempestività e di sussistenza dell'interesse a ricorrere, deve essere valutata in relazione alle singole leggi adottate. Come questa Corte ha affermato in altre occasioni, l'acquiescenza rispetto ad altre leggi regionali non milita a favore della legittimità costituzionale delle disposizioni impugnate» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/24/!main">sentenza n. 24 del 2022</ref>, punto 2.2.2. del Considerato in diritto; nello stesso senso, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021/124/!main">sentenze n. 124 del 2021</ref>, punto 3.3.2. del Considerato in diritto, n. 107 del 2021, punto 2.3. del Considerato in diritto, n. 25 del 2021, punto 17 del Considerato in diritto, e n. 117 del 2020).
 3.2.- Nemmeno è fondata l'eccezione di inammissibilità della censura dedotta in riferimento all'art. 81, terzo comma, Cost., per mancanza di indicazione delle ragioni dell'assenza di copertura finanziaria.
 Il ricorrente ha fornito sul punto una pur essenziale ma, comunque, adeguata motivazione, individuata nella circostanza che la illegittimità della denunciata modalità di copertura finanziaria priverebbe la disposizione di riconoscimento del debito della correlata fonte di finanziamento, determinando perciò stesso la lesione del parametro finanziario in oggetto. In tal modo l'argomentazione del ricorrente raggiunge quella soglia minima di chiarezza e completezza che secondo la giurisprudenza di questa Corte consente di esaminare il merito dell'impugnativa proposta (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/123/!main">is, sentenze n. 123 del 2022</ref>, n. 195 e n. 95 del 2021).
 4.- Nel merito, è fondata la questione di legittimità costituzionale delle impugnate disposizioni delle leggi reg. Molise n. 16 e n. 17 del 2022, promossa in riferimento all'art. 117, secondo comma, lettera e), Cost., per lesione della competenza legislativa esclusiva statale nella materia «armonizzazione dei bilanci pubblici», in relazione al principio contabile della annualità del bilancio, posto dal paragrafo 1 dell'Allegato 1, richiamato dall'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_3">art. 3 del d.lgs. n. 118 del 2011</ref>.
 4.1.- Questa Corte ha di recente vagliato la medesima problematica, oggetto del presente giudizio, in riferimento a leggi della stessa Regione Molise di contenuto analogo.
 Nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2023/51/!main">sentenza n. 51 del 2023</ref> è stato difatti affermato: che le risorse occorrenti per effettuare la spesa derivante dal riconoscimento del debito fuori bilancio «non possono che essere rinvenute nel bilancio di previsione che gestisce l'esercizio in cui la spesa è introdotta»; che, pertanto, l'individuazione e la copertura deve essere contestuale alla previsione dell'onere «per cui la legge regionale di riconoscimento di un debito fuori bilancio deve apprestare la relativa copertura facendo riferimento alle risorse finanziarie in quel momento effettivamente disponibili»; che, nella fattispecie, la legge impugnata, approvata nel marzo 2022, ha dunque illegittimamente individuato la correlata copertura finanziaria a valere sull'esercizio 2021 del bilancio di previsione 2021-2023, anziché sul bilancio relativo al 2022.
 Le predette statuizioni trovano applicazione anche nella fattispecie in esame: le impugnate disposizioni, approvate nell'agosto del 2022, hanno parimenti individuato la copertura finanziaria nel bilancio di previsione 2021-2023, esercizio 2021, anziché 2022, anno in cui è intervenuto il riconoscimento da esse previsto dei debiti fuori bilancio in oggetto e, pertanto, violano anch'esse l'art. 117, secondo comma, lettera e), Cost., in relazione al principio di annualità del bilancio contemplato dal paragrafo 1, Allegato 1, richiamato dall'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_3">art. 3 del d.lgs. n. 118 del 2011</ref>.
 4.2.- La difesa della Regione ha, peraltro, sostenuto che dovrebbe trovare applicazione il principio contabile della prevalenza della sostanza sulla forma, enunciato al paragrafo 18 dell'Allegato 1 al <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main">d.lgs. n. 118 del 2011</ref>.
 La tesi non è condivisibile.
 Il procedimento di riconoscimento del debito fuori bilancio, regolato dall'art. 73 del citato <ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main">d.lgs. n. 118 del 2011</ref>, stante il suo carattere eccezionale rispetto alle modalità ordinarie con le quali l'ente regionale deve effettuare la programmazione e gestione finanziarie, presenta una spiccata specificità che esige la rigorosa osservanza del principio contabile richiamato dal ricorrente; principio che resterebbe eluso ove fosse accolto l'assunto della difesa regionale.
 5.- L'accertata lesione dell'art. 117, secondo comma, lettera e), Cost., in relazione al principio contabile dell'annualità del bilancio di cui al paragrafo 1, Allegato 1, richiamato dall'<ref href="/akn/it/act/decretoLegislativo/stato/2011/118/!main#art_3">art. 3 del d.lgs. n. 118 del 2011</ref>, conduce a ritenere assorbite le ulteriori questioni, promosse, rispettivamente, in riferimento al medesimo parametro costituzionale, in relazione al principio contabile applicato di cui al paragrafo 9.1 dell'Allegato 4/2 allo stesso decreto legislativo e in riferimento all'art. 81, terzo comma, Cost.
 6.- Dall'illegittimità costituzionale degli artt. 1 e 2 delle leggi reg. Molise n. 16 e n. 17 del 2022 discende che il successivo art. 3 di entrambe le leggi, limitandosi a disciplinare l'entrata in vigore delle citate leggi regionali, non ha più ragion d'essere (<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/161/!main">sentenze n. 161</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/124/!main">124 del 2022</ref></mref>).</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 riuniti i giudizi,
 1) dichiara l'illegittimità costituzionale degli artt. 1 e 2 della legge della Regione Molise 4 agosto 2022, n. 16, recante «Riconoscimento della legittimità dei debiti fuori bilancio ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">articolo 73, comma 1, lettera e), del decreto legislativo 23 giugno 2011, n. 118</ref> e ss.mm.ii., derivante dal servizio pubblico essenziale, quale la fornitura di connettività e sicurezza nell'ambito del sistema pubblico di connettività reso da Olivetti S.p.A. nell'anno 2017 e 2019)»;
 2) dichiara l'illegittimità costituzionale degli artt. 1 e 2 della legge della Regione Molise 4 agosto 2022, n. 17, recante «Riconoscimento della legittimità dei debiti fuori bilancio ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2011-06-23/118/!main#art_73__para_1__point_e">articolo 73, comma 1, lettera e), del decreto legislativo 23 giugno 2011, n. 118</ref> e ss.mm.ii., derivante dal Servizio Fonia Fissa reso da TIM S.p.A. nell'anno 2020».
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2023-05-09">9 maggio 2023</date>.
 F.to:
 <signature><judge refersTo="#silvanaSciarra" as="#presidente">Silvana SCIARRA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulioProsperetti" as="#relatore">Giulio PROSPERETTI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2023-06-23">23 giugno 2023</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
