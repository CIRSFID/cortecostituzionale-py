<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>176</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:176</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/05/1998</data_decisione>
    <data_deposito>20/05/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli articoli 10,  comma    &#13;
 2  e  11,  comma  3  del decreto legislativo 26 febbraio 1994, n. 143    &#13;
 (Istituzione dell'Ente nazionale per le strade), e  del  decreto  del    &#13;
 Presidente  del Consiglio dei Ministri 26 luglio 1995 (Trasformazione    &#13;
 dell'Azienda nazionale autonoma delle strade in Ente nazionale per le    &#13;
 strade, ente pubblico economico), promosso con ordinanza emessa il  2    &#13;
 aprile  1997  dal  pretore di Bologna sul ricorso proposto da Ermanno    &#13;
 Bernardini contro l'ANAS ed altre, iscritta al n.  319  del  registro    &#13;
 ordinanze 1997 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 24, prima serie speciale, dell'anno 1997.                             &#13;
   Visto  l'atto di costituzione della Bonifica S.p.A., nonché l'atto    &#13;
 di intervento del Presidente del Consiglio dei Ministri;                 &#13;
   Udito nell'udienza pubblica del 10 marzo 1998 il  giudice  relatore    &#13;
 Piero Alberto Capotosti;                                                 &#13;
   Udito  l'Avvocato  dello  Stato Oscar Fiumara per il Presidente del    &#13;
 Consiglio dei Ministri;                                                  &#13;
   Ritenuto che, nel corso di un giudizio promosso nei confronti,  tra    &#13;
 gli   altri,   dell'ANAS   -   Ente  nazionale  per  le  strade,  per    &#13;
 l'accertamento di un rapporto di lavoro subordinato,  il  pretore  di    &#13;
 Bologna,  in  funzione  di  giudice  del  lavoro, con ordinanza del 2    &#13;
 aprile 1997, ha sollevato questione  di  legittimità  costituzionale    &#13;
 degli articoli 10, comma 2, e 11, comma 3, del decreto legislativo 26    &#13;
 febbraio  1994,  n.  143  (Istituzione  dell'Ente  nazionale  per  le    &#13;
 strade),  nonché  del  decreto  del  Presidente  del  Consiglio  dei    &#13;
 Ministri   26  luglio  1995  (Trasformazione  dell'Azienda  nazionale    &#13;
 autonoma delle strade in Ente nazionale per le strade, ente  pubblico    &#13;
 economico), in riferimento agli articoli 25 e 76 della Costituzione;     &#13;
     che  l'ANAS, costituitosi nel processo principale, ha eccepito il    &#13;
 difetto di giurisdizione ex art. 10, comma 2, del d.lgs. n.  143  del    &#13;
 1994, secondo cui "continuano ad essere attribuite alla giurisdizione    &#13;
 esclusiva  del  giudice  amministrativo  le  controversie  relative a    &#13;
 questioni attinenti al periodo di lavoro svoltosi anteriormente  alla    &#13;
 trasformazione dell'azienda";                                            &#13;
     che  il  giudice  a  quo  premette  che  il rapporto di lavoro è    &#13;
 cessato in data anteriore a quella dell'emanazione  del  decreto  del    &#13;
 Presidente  del  Consiglio dei Ministri del 26 luglio 1995, il quale,    &#13;
 ai sensi dell'art. 11, comma 3,  del  d.lgs.  n.  143  del  1994,  ha    &#13;
 disposto  la  trasformazione dell'ANAS in ente pubblico economico, ma    &#13;
 dubita che le due suindicate  disposizioni  del  decreto  legislativo    &#13;
 violino  l'art.    25,  primo  comma,  della  Costituzione, in quanto    &#13;
 riservano  l'individuazione  del  giudice  competente  ad   un   atto    &#13;
 amministrativo,   stabilendo,   la   prima,   che  la  trasformazione    &#13;
 dell'azienda  costituisce  il   criterio   per   il   riparto   della    &#13;
 giurisdizione in ordine alle controversie di lavoro con l'ANAS, e, la    &#13;
 seconda,  che  tale  trasformazione  sia  disposta  con  decreto  del    &#13;
 Presidente del Consiglio dei Ministri;                                   &#13;
     che, ad avviso del pretore di Bologna, l'art. 11,  comma  3,  del    &#13;
 d.lgs.  n. 143 del 1994, nella parte in cui  conferisce al Presidente    &#13;
 del Consiglio  dei  Ministri  il  potere  di  disporre,  con  proprio    &#13;
 decreto, la trasformazione dell'ANAS in ente pubblico economico, reca    &#13;
 vulnus  anche  all'art.  76 della Costituzione, qualora "si configuri    &#13;
 come una ulteriore e  non  consentita  delega"  dell'esercizio  della    &#13;
 funzione legislativa, delegata al Governo  dall'art. 1 della legge 24    &#13;
 dicembre 1993, n. 537;                                                   &#13;
     che,  secondo il pretore rimettente, l'art. 76 della Costituzione    &#13;
 risulta altresì violato perché  il  decreto    del  Presidente  del    &#13;
 Consiglio dei Ministri del 26 luglio 1995, qualificato nell'ordinanza    &#13;
 come  atto di legislazione delegata, risulta emanato oltre il termine    &#13;
 di novanta giorni previsto dall'art. 11, comma 3, del d.lgs.  n.  143    &#13;
 del  1994, nonché oltre quello del 31 dicembre 1994, stabilito nella    &#13;
 legge n. 537 del 1993  per  l'esercizio  della  funzione  legislativa    &#13;
 delegata;                                                                &#13;
     che  nel  giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 Ministri, con il patrocinio dell'Avvocatura generale dello Stato, che    &#13;
 ha concluso per l'infondatezza delle questioni;                          &#13;
     che si è, altresì, costituita la Società  Bonifica,  convenuta    &#13;
 nel   giudizio   principale,   chiedendo   che   le     questioni  di    &#13;
 costituzionalità siano dichiarate infondate;                            &#13;
   Considerato che l'art. 11, comma 3, del  d.lgs.  n.  143  del  1994    &#13;
 dispone   che,   "previa   approvazione   dello  statuto,  l'ANAS  è    &#13;
 trasformata nell'Ente  nazionale  per  le  strade,  con  decreto  del    &#13;
 Presidente  del  Consiglio dei Ministri, su proposta del Ministro dei    &#13;
 lavori pubblici" e fissa a tal fine  un  termine  non  perentorio  di    &#13;
 novanta   giorni,  demandando  ad  un  atto  amministrativo  la  mera    &#13;
 esecuzione  della  trasformazione  all'esito   di   un   procedimento    &#13;
 specificamente disciplinato;                                             &#13;
     che,  pertanto,  la  norma  denunziata  non configura affatto una    &#13;
 fattispecie di sub-delegazione della funzione legislativa, sicché la    &#13;
 censura riferita all'art. 76  della  Costituzione  è  manifestamente    &#13;
 infondata  e  la  questione  è, invece, manifestamente inammissibile    &#13;
 nella parte in cui investe il decreto del  Presidente  del  Consiglio    &#13;
 dei Ministri del 26 luglio 1995, dato che tale atto è privo di forza    &#13;
 di legge;                                                                &#13;
     che,  secondo  la  consolidata giurisprudenza di questa Corte, il    &#13;
 principio della precostituzione per legge  del  giudice  naturale  è    &#13;
 leso  soltanto  quando il giudice è designato in modo arbitrario e a    &#13;
 posteriori, oppure direttamente dal legislatore in via  di  eccezione    &#13;
 singolare alle regole generali, ovvero attraverso atti di soggetti ai    &#13;
 quali  sia  attribuito il relativo potere in violazione della riserva    &#13;
 assoluta  di  legge  stabilita  dall'art.  25,  primo  comma,   della    &#13;
 Costituzione,  ma  non  anche  qualora  l'identificazione del giudice    &#13;
 competente sia operata dalla legge sulla scorta  di  criteri  dettati    &#13;
 preventivamente,  oppure con riferimento ad elementi oggettivi capaci    &#13;
 di costituire un discrimen della competenza o della giurisdizione dei    &#13;
 diversi organi giudicanti (ex plurimis, ordinanza n.  257  del  1995,    &#13;
 sentenza n. 217 del 1993, ordinanza n. 161 del 1992);                    &#13;
     che l'art. 11, comma 3, del d.lgs. n. 143 del 1994 attribuisce al    &#13;
 Presidente  del Consiglio dei Ministri il potere di emanare l'atto di    &#13;
 trasformazione dell'ANAS in ente pubblico economico all'esito  di  un    &#13;
 procedimento specificamente definito dalle norme primarie;               &#13;
     che  il fatto che, fra i molteplici effetti della trasformazione,    &#13;
 vi sia anche quello previsto dall'art. 10, comma  2,  del  d.lgs.  n.    &#13;
 143  del  1994,  secondo  il  quale  resta ferma la giurisdizione del    &#13;
 giudice amministrativo sulle controversie  di  lavoro  relative  alla    &#13;
 fase anteriore alla trasformazione, non viola l'art. 25, primo comma,    &#13;
 della  Costituzione,  in quanto definisce un criterio di collegamento    &#13;
 generale, predeterminato e non arbitrario, fra un  numero  indefinito    &#13;
 di  controversie ed il giudice dotato della giurisdizione su di esse,    &#13;
 sicché la questione va dichiarata manifestamente infondata.             &#13;
   Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme  integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale del decreto del Presidente del Consiglio    &#13;
 dei Ministri 26 luglio 1995  (Trasformazione  dell'Azienda  nazionale    &#13;
 autonoma  delle strade in Ente nazionale per le strade, ente pubblico    &#13;
 economico),   sollevata   dal   pretore  di  Bologna  in  riferimento    &#13;
 all'articolo 76  della  Costituzione,  con  l'ordinanza  indicata  in    &#13;
 epigrafe;                                                                &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli articoli 10, comma 2, e 11, comma 3, del decreto    &#13;
 legislativo 26 febbraio 1994, n. 143 (Istituzione dell'Ente nazionale    &#13;
 per le strade), sollevata, in riferimento agli articoli 25 e 76 della    &#13;
 Costituzione, con la medesima ordinanza.                                 &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 maggio 1998.                                 &#13;
                         Il Presidente: Granata                           &#13;
                        Il redattore: Capotosti                           &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 20 maggio 1998.                           &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
