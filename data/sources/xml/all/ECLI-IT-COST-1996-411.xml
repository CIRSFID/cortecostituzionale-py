<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>411</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:411</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/12/1996</data_decisione>
    <data_deposito>24/12/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, prof. Gustavo ZAGREBELSKY, prof. &#13;
 Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. Fernanda CONTRI, prof. &#13;
 Guido NEPPI MODONA, prof. Pier Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 34  del  codice    &#13;
 di  procedura  penale,  promosso con ordinanza emessa il 6 marzo 1996    &#13;
 dal pretore di Crotone, sezione distaccata di Petilia Policastro, nel    &#13;
 procedimento penale a carico di Manfreda Vincenzo, iscritta al n. 623    &#13;
 del registro ordinanze 1996 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 28, prima serie speciale, dell'anno 1996;            &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 13  novembre  1996  il  giudice    &#13;
 relatore Gustavo Zagrebelsky;                                            &#13;
   Ritenuto  che il pretore di Crotone ha sollevato, con ordinanza del    &#13;
 6 marzo 1996, in riferimento agli artt. 76 e 77 della Costituzione  e    &#13;
 in  relazione all'art. 2, direttiva n. 103, della legge-delega per il    &#13;
 nuovo codice di procedura penale 16 febbraio 1987, n.  81,  questione    &#13;
 di  legittimità  costituzionale dell'art. 34 (comma 2) del codice di    &#13;
 procedura penale, nella parte  in  cui  non  prevede  che  non  possa    &#13;
 partecipare  al  giudizio  dibattimentale  il giudice per le indagini    &#13;
 preliminari presso la pretura che, sulla richiesta  di  archiviazione    &#13;
 formulata  dal  pubblico  ministero, abbia ordinato a quest'ultimo di    &#13;
 eseguire ulteriori indagini, a norma  dell'art.  554,  comma  2,  del    &#13;
 codice di procedura penale (quale risultante a seguito della sentenza    &#13;
 n.  445  del  1990  di questa Corte, dichiarativa dell'illegittimità    &#13;
 costituzionale della disposizione nella parte in cui, diversamente da    &#13;
 quanto stabilito nell'art.  409,  comma  4,  per  i  procedimenti  di    &#13;
 competenza  del  tribunale,  non  prevedeva  la  possibilità  per il    &#13;
 giudice per le indagini preliminari presso la  pretura  circondariale    &#13;
 di  indicare  le ulteriori necessarie indagini al pubblico ministero,    &#13;
 fissando  il  termine  indispensabile  per   il   loro   compimento),    &#13;
 ravvisando,  nell'anzidetta  ipotesi, alla luce anche degli enunciati    &#13;
 della sentenza n. 432 del 1995 di questa Corte, una  menomazione  del    &#13;
 principio  di  terzietà  del  giudice,  in  contrasto  con la citata    &#13;
 direttiva n. 103 della legge-delega;                                     &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, che ha concluso per l'infondatezza della questione;               &#13;
   Considerato che questa Corte ha già affrontato e risolto nel senso    &#13;
 della manifesta infondatezza analoghe questioni;                         &#13;
     che, posta la premessa generale secondo cui può configurarsi una    &#13;
 incompatibilità  del giudice rispetto alla funzione di giudizio solo    &#13;
 quando la valutazione precedentemente effettuata dal medesimo giudice    &#13;
 sia resa nell'ambito e in occasione  dello  svolgimento  di  funzioni    &#13;
 decisorie  e  non  anche puramente processuali (sentenze nn.  131 del    &#13;
 1996; 455 e 453 del 1994), questa  Corte  ha,  più  in  particolare,    &#13;
 osservato  che,  con  il  provvedimento  con  il quale dispone che il    &#13;
 pubblico ministero compia  ulteriori  indagini,  il  giudice  per  le    &#13;
 indagini  preliminari non effettua una valutazione contenutistica del    &#13;
 materiale di indagine, ma adotta una decisione di natura processuale,    &#13;
 meramente  interlocutoria,  che  può   essere   seguita   non   solo    &#13;
 dall'esercizio  dell'azione penale da parte del pubblico ministero ma    &#13;
 anche da un'ulteriore richiesta di archiviazione e quindi dalla gamma    &#13;
 dei provvedimenti che in tale ultimo caso il giudice può adottare, a    &#13;
 norma dell'art. 409 del codice di procedura penale (ordinanze nn. 281    &#13;
 del 1996; 157 del 1993);                                                 &#13;
     che le argomentazioni addotte dal giudice a quo in riferimento al    &#13;
 principio di separazione tra  le  funzioni  di  accusa  e  quelle  di    &#13;
 giudice e sotto il profilo della violazione della legge-delega, al di    &#13;
 là  della piena pertinenza dei parametri invocati, non sono comunque    &#13;
 idonee a condurre a diversa conclusione,  dovendo  restare  fermo  il    &#13;
 necessario presupposto della valutazione decisoria e di contenuto non    &#13;
 puramente   processuale   ai   fini   della  configurabilità  di  un    &#13;
 pregiudizio incidente sull'imparzialità del giudice, presupposto che    &#13;
 nell'ipotesi dedotta non ricorre;                                        &#13;
     che, pertanto, la  questione  sollevata  deve  essere  dichiarata    &#13;
 manifestamente infondata;                                                &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale dell'art. 34, comma 2, del codice di procedura penale,    &#13;
 sollevata,  in riferimento agli artt. 76 e 77 della Costituzione, dal    &#13;
 pretore di Crotone, con l'ordinanza indicata in epigrafe.                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'11 dicembre 1996.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 24 dicembre 1996.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
