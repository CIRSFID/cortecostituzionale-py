<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>149</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:149</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Guido Astuti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/12/1979</data_decisione>
    <data_deposito>14/12/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 9, primo  &#13;
 comma, del decreto luogotenenziale 1 maggio 1916, n. 497  (Liquidazione  &#13;
 delle pensioni privilegiate di guerra); art. 169 del d.P.R. 29 dicembre  &#13;
 1973,  n.  1092  (t.u.  delle  norme  sul trattamento di quiescenza dei  &#13;
 dipendenti civili e militari dello Stato), con riferimento all'art.  89  &#13;
 della  legge  18 marzo 1968, n. 313 promosso con ordinanza emessa il 27  &#13;
 ottobre 1975 dalla Corte dei conti - sezione 4  giurisdizionale  -  sul  &#13;
 ricorso  di  Leva  Giovanni,  iscritta al n. 437 del registro ordinanze  &#13;
 1976 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  184 del  &#13;
 14 luglio 1976.                                                          &#13;
     Udito nella camera di  consiglio  del  4  maggio  1979  il  Giudice  &#13;
 relatore Guido Astuti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  del  giudizio  a  seguito  di  ricorso proposto da Leva  &#13;
 Giovanni,  la  Corte  dei  conti,  sezione   giurisdizionale   pensioni  &#13;
 militari,   ha   sollevato   di   ufficio   questione  di  legittimità  &#13;
 costituzionale degli artt. 9, primo comma del d.lgt. 1 maggio 1916,  n.  &#13;
 497  e 169 del d.P.R. 29 dicembre 1973, n. 1092 in riferimento all'art.  &#13;
 3, primo comma, della Costituzione.                                      &#13;
     Si osserva nell'ordinanza che la disciplina impugnata stabilisce un  &#13;
 termine di decadenza di cinque anni per la proposizione  della  domanda  &#13;
 volta   ad   ottenere  il  trattamento  pensionistico  privilegiato  in  &#13;
 relazione ad una infermità contratta durante il  servizio  militare  e  &#13;
 comportante  la  incapacità  di  intendere  e di volere, mentre nessun  &#13;
 termine di decadenza, in analoga  situazione  soggettiva,  è  previsto  &#13;
 dall'art.  89  della  legge  18  marzo  1968, n. 313 relativamente alle  &#13;
 pensioni privilegiate di guerra. Poiché la ragione giustificatrice  di  &#13;
 quest'ultima  disposizione  è da rinvenire unicamente nella condizione  &#13;
 personale degli interessati i quali non siano in grado, essendo  minori  &#13;
 di  età  ovvero  malati  di  mente, di tutelare i propri interessi, le  &#13;
 norme  impugnate  comporterebbero  una  ingiustificata  e   irrazionale  &#13;
 disparità  di  trattamento di fronte a situazioni soggettive del tutto  &#13;
 identiche.                                                               &#13;
     Non  vi è, nel presente giudizio, costituzione della parte privata  &#13;
 né intervento del Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con l'ordinanza  indicata  in  epigrafe  la  Corte  dei  conti  &#13;
 solleva,  in  riferimento all'art. 3, priino comma, della Costituzione,  &#13;
 la questione di legittimità costituzionale dell'art. 9,  primo  comma,  &#13;
 del d.lgt. 1 maggio 1916, n. 497, sulla procedura di liquidazione delle  &#13;
 pensioni  privilegiate  di guerra, e dell'art. 169 del t.u. delle norme  &#13;
 sul trattamento di quiescenza dei dipendenti civili  e  militari  dello  &#13;
 Stato,  approvato  con  d.P.R.  29 dicembre 1973, n. 1092, in relazione  &#13;
 all'art. 89 della legge 18 marzo 1968, n. 313, sul riordinamento  della  &#13;
 legislazione  pensionistica  di  guerra. Le due disposizioni denunciate  &#13;
 stabiliscono un termine perentorio di cinque anni dalla cessazione  del  &#13;
 servizio  per  la  presentazione  delle  domande  di  constatazione  di  &#13;
 infermità o lesioni dipendenti  da  causa  di   servizio  al  fine  di  &#13;
 ottenere  un  trattamento  pensionistico privilegiato:   mentre a norma  &#13;
 dell'art. 89, primo comma, della legge 18 marzo 1968, n.  313,  per  il  &#13;
 conseguimento delle pensioni (privilegiate) di guerra, per i minori e i  &#13;
 dementi quel termine è sospeso finché duri la incapacità di agire.    &#13;
     La  Corte dei conti, dovendo pronunciarsi sulla eccepita tardività  &#13;
 di una domanda di pensione militare  privilegiata  ordinaria,  prodotta  &#13;
 dal  padre  dell'interessato,  infermo  di  mente, dopo la scadenza del  &#13;
 termine quinquennale di cui all'art. 9 del  d.lgt.  n.  497  del  1916,  &#13;
 sostanzialmente  recepito  dall'art.  169  del  t.u.  n.  1092 del 1973  &#13;
 (applicabile nel caso di specie in base al disposto del successivo art.  &#13;
 256), ha  rilevato  una  ingiustificata  violazione  del  principio  di  &#13;
 eguaglianza,  osservando  che  la diversità di disciplina, di fronte a  &#13;
 situazioni  soggettive  del  tutto  identiche,  non   avrebbe   "alcuna  &#13;
 razionale    giustificazione   o   alcuna   rispondenza   in   esigenze  &#13;
 etico-sociali",  in  quanto  la  sospensione   del   termine   per   le  &#13;
 constatazioni  sanitarie, prevista dal ricordato art. 89 della legge n.  &#13;
 313 del 1968 in  favore  dei  minori  e  dei  dementi,  "trova  il  suo  &#13;
 fondamento  e  la  sua giustificazione non nelle particolari situazioni  &#13;
 oggettive di disagio legate  alle  anormali  condizioni  del  tempo  di  &#13;
 guerra,   ma   nelle   condizioni  soggettive  degli  interessati  che,  &#13;
 unicamente a causa della minore età o delle loro  condizioni  mentali,  &#13;
 non  siano  in  grado  di  far  valere i propri diritti e di tutelare i  &#13;
 propri interessi". Infatti la detta sospensione è  prevista  dall'art.  &#13;
 89 fino al momento in cui i soggetti interessati abbiano reintegrata la  &#13;
 loro capacità di agire (o per la raggiunta maggiore età, nel caso dei  &#13;
 minori,  ovvero  per  la  guarigione  o  la nomina di un rappresentante  &#13;
 legale, nel caso dei dementi), "e non invece fino al  momento  in  cui,  &#13;
 cessato  lo stato di guerra, si siano ripristinate nel Paese obbiettive  &#13;
 condizioni di normalità".                                               &#13;
     2. - La questione è fondata. Questa Corte è già stata chiamata a  &#13;
 giudicare della legittimità dell'art. 9, primo  comma,  del  d.lgt.  1  &#13;
 maggio  1916,  n.  497,  sotto  un  diverso  profilo, e precisamente in  &#13;
 relazione alla  pretesa  disparità  di  trattamento  tra  le  pensioni  &#13;
 militari  di  guerra,  per le quali è richiesta da detta norma la sola  &#13;
 constatazione dell'infermità, e le  pensioni  privilegiate  ordinarie,  &#13;
 per  le  quali  invece occorre altresì l'accertamento della dipendenza  &#13;
 della infermità da causa di servizio. Con sentenza 5 dicembre 1974, n.  &#13;
 277,  tale  questione  di  legittimità  fu  dichiarata  non   fondata,  &#13;
 riconoscendo  la  piena  giustificazione  del trattamento differenziato  &#13;
 delle pensioni privilegiate ordinarie, per cui  tutte  le  leggi  hanno  &#13;
 sempre  richiesto  la prova rigorosa, a carico degli interessati, della  &#13;
 dipendenza da causa di servizio, mentre per le  pensioni  di  guerra  a  &#13;
 favore  dei  soggetti militari o ad essi equiparati, tale dipendenza è  &#13;
 presunta,  salvo  prova  contraria   da   parte   dell'Amministrazione,  &#13;
 ritenendosi    sufficiente    secondo   la   costante   e   consolidata  &#13;
 giurisprudenza della Corte  dei  conti,  la  constatazione  dell'evento  &#13;
 dannoso, ferita, lesione o infermità, che si considera come dipendente  &#13;
 da causa di servizio in correlazione con lo stato di guerra, ovvero con  &#13;
 situazioni  che,  per la peculiarità delle vicende sociali e politiche  &#13;
 nelle quali si sono verificate, la legge abbia assimilato alla guerra.   &#13;
     Diverso oggetto ha la questione oggi sottoposta all'esame di questa  &#13;
 Corte, rispetto alla quale non ha rilievo  il  criterio  discriminatore  &#13;
 della  speciale  disciplina  delle pensioni di guerra, costituito dalla  &#13;
 peculiarità degli eventi bellici e delle situazioni oggettive connesse  &#13;
 alle anormali condizioni del tempo di  guerra,  essendo  denunciata  la  &#13;
 disparità  di trattamento in ordine ad una condizione soggettiva degli  &#13;
 interessati (minore età o demenza), che non ha alcuna correlazione con  &#13;
 lo stato di guerra ovvero di pace.                                       &#13;
     Ora non sembra possibile dubbio sulla ratio  della  già  ricordata  &#13;
 disposizione  dell'art.  89,  primo comma, della legge n. 313 del 1968,  &#13;
 ove, in ordine al  medesimo  termine  perentorio  di  cinque  anni,  è  &#13;
 tuttavia  stabilito  che  "per i minori e i dementi il termine predetto  &#13;
 rimane sospeso finché duri la incapacità di  agire".  La  ragione  di  &#13;
 questa  disposizione  (confermata anche dall'art. 99, ultimo comma, del  &#13;
 nuovo t.u. oggi vigente delle norme in materia di pensioni  di  guerra,  &#13;
 approvato con d.P.R. 23 dicembre 1978, n. 915), consiste nella esigenza  &#13;
 di  assicurare piena possibilità di tutela giuridica a coloro che, per  &#13;
 l'età minore o per le loro condizioni mentali, non siano in  grado  di  &#13;
 far  valere i propri diritti; ed è ragione che si riferisce unicamente  &#13;
 alla particolare condizione  personale  di  questi  soggetti,  come  è  &#13;
 confermato  anche  dal  fatto,  rilevato  dalla Corte dei conti, che la  &#13;
 sospensione  del  termine  per  l'ammissibilità   delle   domande   di  &#13;
 constatazione  è dalla legge disposta finché duri la loro incapacità  &#13;
 di agire, e non già in relazione alla cessazione dello stato di guerra  &#13;
 o dell'anormale situazione ad esso assimilata.                           &#13;
     Nell'ambito dello speciale diritto pensionistico, si deve  pertanto  &#13;
 riconoscere  che  la  denunciata  disparità di trattamento è priva di  &#13;
 razionale fondamento e che non v'è motivo per cui possa  giustificarsi  &#13;
 la  inapplicabilità  di  quella  disposizione,  anche  in  materia  di  &#13;
 pensioni privilegiate  ordinarie.  A1  riguardo,  una  sicura  conferma  &#13;
 dell'esigenza  che  ne  postula  la  estensione è offerta dall'analogo  &#13;
 disposto dell'art. 191, ultimo comma, del vigente  t.u.  del  1973,  n.  &#13;
 1092,  sul  trattamento  di quiescenza dei dipendenti civili e militari  &#13;
 dello Stato, ove, ai fini  della  decorrenza  delle  pensioni  e  degli  &#13;
 assegni  liquidabili a domanda, è stabilito in via generale che "per i  &#13;
 minori non emancipati e gli interdetti, il  termine  di  cui  al  comma  &#13;
 precedente  nonché quelli stabiliti da altre disposizioni del presente  &#13;
 testo unico rimangono sospesi finché duri la incapacità di agire".</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la illegittimità costituzionale dell'art. 9, primo comma,  &#13;
 del d.lgt. 1 maggio 1916, n. 497 e dell'art. 169 del t.u. approvato con  &#13;
 d.P.R.  29  dicembre  1973,  n.  1092, - in relazione al disposto degli  &#13;
 artt. 89 della legge 18 marzo 1968, n. 313, e 99 del d.P.R. 23 dicembre  &#13;
 1978, n.  915 -, in quanto non consentono, nei confronti dei  minori  e  &#13;
 dei  dementi,  la  sospensione  del  termine  per  l'accertamento della  &#13;
 dipendenza delle infermità o lesioni da causa  di  servizio,  "finché  &#13;
 duri la (loro) incapacità di agire".                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 7 dicembre 1979.        &#13;
                                   F.to:  LEONETTO  AMADEI   -   EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO  -  ANTONINO  DE  STEFANO   -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
