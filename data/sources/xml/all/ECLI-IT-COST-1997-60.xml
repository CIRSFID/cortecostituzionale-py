<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>60</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:60</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/02/1997</data_decisione>
    <data_deposito>04/03/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 1  della  legge    &#13;
 approvata  dalla  Assemblea  regionale  siciliana  il  24  marzo 1996    &#13;
 (Provvedimenti  per  il  personale  dell'ITALTER   e   della   SIRAP.    &#13;
 Interventi   per   le  imprese  fornitrici  creditrici  della  SIRAP.    &#13;
 Istituzione di sportelli per l'Unione europea), promosso con  ricorso    &#13;
 del Commissario dello Stato per la Regione Siciliana, notificato il 1    &#13;
 aprile  1996 e depositato in Cancelleria il 10 successivo ed iscritto    &#13;
 al n. 15 del registro ricorsi 1996;                                      &#13;
   Visto l'atto di costituzione della Regione Siciliana;                  &#13;
   Udito  nell'udienza  pubblica  del  12  novembre  1996  il  giudice    &#13;
 relatore Gustavo Zagrebelsky;                                            &#13;
   Uditi  l'avvocato  dello  Stato Luigi Mazzella per il ricorrente, e    &#13;
 gli avvocati Giovanni Lo  Bue  e  Laura  Ingargiola  per  la  Regione    &#13;
 Siciliana.                                                               &#13;
   Ritenuto   che,  con  ricorso  notificato  il  1  aprile  1996,  il    &#13;
 commissario dello Stato per la regione siciliana ha impugnato  l'art.    &#13;
 1  della  legge  approvata  dall'Assemblea  regionale siciliana nella    &#13;
 seduta del 24 marzo 1996, recante  "Provvedimenti  per  il  personale    &#13;
 dell'ITALTER  e  della  SIRAP.  Interventi  per le imprese fornitrici    &#13;
 creditrici  della  SIRAP.  Istituzione  di  sportelli  per   l'Unione    &#13;
 europea"  (disegno di legge n. 1182-1210), per violazione degli artt.    &#13;
 3, 51 e 97 della  Costituzione,  nonché  degli  artt.  2,  comma  1,    &#13;
 lettera  r),  della  legge  23  ottobre  1992, n. 421 in relazione ai    &#13;
 limiti posti dall'art. 14, lettera  q),  dello  statuto  speciale  di    &#13;
 autonomia;                                                               &#13;
     che   la  norma  impugnata  prevede  la  trasformazione  a  tempo    &#13;
 indeterminato dei rapporti di lavoro di durata biennale, istituiti ai    &#13;
 sensi dell'art.   76 della legge regionale n.  25  del  1993  con  il    &#13;
 personale  dell'ITALTER  s.p.a  e  della  SIRAP  s.p.a., società con    &#13;
 capitale a partecipazione pubblica, entrambe poste in liquidazione, e    &#13;
 che l'iniziativa sarebbe asseritamente finalizzata ad una più rapida    &#13;
 esecuzione dei lavori e delle attività di competenza  dell'ANAS  nel    &#13;
 settore della viabilità;                                                &#13;
     che  il  commissario  dello  Stato,  dopo  aver  affermato che la    &#13;
 regione siciliana non ha ancora avviato le procedure di verifica  dei    &#13;
 carichi di lavoro dei propri dipendenti, né rideterminato la propria    &#13;
 pianta organica, come chiesto dalla legislazione statale, osserva che    &#13;
 la   surrettizia   "stabilità  occupazionale"  che  si  intenderebbe    &#13;
 assicurare a determinati soggetti (circa  novanta)  avverrebbe  senza    &#13;
 alcuna  previa  verifica  della  capacità  e idoneità professionale    &#13;
 degli  interessati  e  senza  alcuna  valutazione  delle   specifiche    &#13;
 esigenze della pubblica amministrazione;                                 &#13;
     che,  inoltre,  si  porrebbe  a  carico  della regione l'onere di    &#13;
 personale  messo  a  disposizione  di   un   ente   (ANAS)   estraneo    &#13;
 all'amministrazione regionale stessa, e che, per di più, non avrebbe    &#13;
 avanzato  nessuna  richiesta  in  proposito,  ma che "soltanto dietro    &#13;
 offerta  specifica   dell'amministrazione   regionale   ha   mostrato    &#13;
 disponibilità  ad avvalersi eventualmente di tecnici, purché dotati    &#13;
 di qualificata professionalità";                                        &#13;
     che verrebbe in tal modo disatteso anche il  principio  derivante    &#13;
 dalla  legislazione statale (art. 2, comma 1, lettera r), della legge    &#13;
 n. 421 del 1992) nonché l'interesse nazionale unitario sotteso  alla    &#13;
 riforma del pubblico impiego;                                            &#13;
     che  si  è  costituita  in  giudizio  la  regione  siciliana per    &#13;
 chiedere il rigetto del ricorso, in quanto  le  norme  impugnate  non    &#13;
 sarebbero  né  arbitrarie  né  irragionevoli  anche  alla  luce  di    &#13;
 precedenti, analoghi interventi regionali in specifici settori (legge    &#13;
 regionale 6 luglio 1990, n. 11, modificata dalla legge  regionale  12    &#13;
 gennaio  1993,  n.    9, e legge regionale 18 aprile 1981, n. 66, che    &#13;
 hanno riguardato assunzioni a tempo  indeterminato,  rispettivamente,    &#13;
 di  personale  tecnico del genio civile e di lavoratori forestali), e    &#13;
 non sarebbe vero che  il  legislatore  regionale  avrebbe  assicurato    &#13;
 "stabilità  occupazionale"  ai soggetti in questione, poiché invece    &#13;
 ha inteso dare attuazione alla normativa  statale  (legge  18  aprile    &#13;
 1962,  n.  230), applicabile anche alle pubbliche amministrazioni, la    &#13;
 quale impone per l'assunzione di lavoratori il contratto di lavoro  a    &#13;
 tempo  indeterminato,  salve  talune  eccezioni, ivi indicate, tra le    &#13;
 quali non rientrano le categorie di personale  regionale  interessato    &#13;
 dalle nuove normative;                                                   &#13;
     che,  sempre  ad  avviso  della regione, il rapporto di lavoro "a    &#13;
 contratto", ai  sensi  della  citata  legge  n.  230,  "non  perde  i    &#13;
 connotati  di  precarietà  che lo distinguono, sol perché stipulato    &#13;
 senza la fissazione di un termine e  perché  il  limite  apposto  al    &#13;
 tempo   del   contratto  attiene  alla  durata  (preventivamente  non    &#13;
 determinabile) del rapporto di lavoro e non alla  sua  stabilità"  e    &#13;
 tale  stabilità  sarebbe esclusa "dal fatto che l'assunzione avviene    &#13;
 fuori ruolo (fuori dei posti della pianta  organica),  cioè  in  una    &#13;
 posizione  di  avventiziato che, per sua stessa natura e funzione, è    &#13;
 destinata a cessare con la cessazione del bisogno  di  personale  che    &#13;
 l'ha  determinata"; ne deriverebbe che il rapporto di lavoro degli ex    &#13;
 dipendenti delle due società non avrebbe natura di pubblico impiego,    &#13;
 bensì di  impiego  privato  disciplinato  dal  contratto  collettivo    &#13;
 nazionale dei lavoratori edili.                                          &#13;
   Considerato  che,  in  data  10  agosto 1996, l'Assemblea regionale    &#13;
 siciliana ha approvato una legge (disegno di legge n. 139) che regola    &#13;
 nuovamente i contratti a termine già stipulati tra l'Amministrazione    &#13;
 regionale e il personale di cui all'art. 76 della legge  regionale  1    &#13;
 settembre 1993, n. 25;                                                   &#13;
     che   tale  nuova  normativa,  disponendo  il  rinnovo  di  detti    &#13;
 contratti per una durata non superiore a un triennio, in luogo  della    &#13;
 loro  trasformazione  da  tempo determinato a tempo indeterminato, è    &#13;
 incompatibile con quella ora  impugnata  (contenuta  nel  disegno  di    &#13;
 legge n. 1182-1210 approvato il 24 marzo 1996);                          &#13;
     che  la  legge  regionale approvata più di recente ha sostituito    &#13;
 quella  anteriore,  oggetto  del  presente  ricorso,  non   potendosi    &#13;
 ammettere  la  contemporanea  esistenza  di plurime e contraddittorie    &#13;
 manifestazioni di  volontà  del  legislatore  regionale,  in  attesa    &#13;
 dell'eventuale perfezionamento dell'iter del controllo costituzionale    &#13;
 da  cui eventualmente consegua la possibilità dell'entrata in vigore    &#13;
 di una o di un'altra;                                                    &#13;
     che, pertanto, l'anzidetta sostituzione fa venir meno la  materia    &#13;
 del   giudizio  sulla  legge  regionale  impugnata  e  ne  impedisce,    &#13;
 naturalmente, la promulgazione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara cessata la materia del contendere in ordine alla questione    &#13;
 di legittimità costituzionale  dell'art.  1  della  legge  approvata    &#13;
 dall'Assemblea  regionale  siciliana  nella seduta del 24 marzo 1996,    &#13;
 recante "Provvedimenti per il personale dell'ITALTER e  della  SIRAP.    &#13;
 Interventi   per   le  imprese  fornitrici  creditrici  della  SIRAP.    &#13;
 Istituzione  di  sportelli  per  l'Unione  europea",   proposta   dal    &#13;
 Commissario dello Stato per la Regione Siciliana, in riferimento agli    &#13;
 artt.  3,  51  e 97 della Costituzione nonché agli artt. 2, comma 1,    &#13;
 lettera r), della legge 23 ottobre 1992, n. 421  e  in  relazione  ai    &#13;
 limiti  posti  dall'art.  14,  lettera   q) dello statuto speciale di    &#13;
 autonomia, col ricorso indicato in epigrafe.                             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 26 febbraio 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 4 marzo 1997.                             &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
