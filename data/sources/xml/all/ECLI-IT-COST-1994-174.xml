<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>174</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:174</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/04/1994</data_decisione>
    <data_deposito>05/05/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 102, primo    &#13;
 comma, della legge 24 novembre 1981, n.  689  (Modifiche  al  sistema    &#13;
 penale),  promosso  con  ordinanza  emessa  il  7  settembre 1993 dal    &#13;
 Magistrato  di  sorveglianza  presso  il  Tribunale  di   Nuoro   nel    &#13;
 procedimento  di  sorveglianza  nei  confronti  di  Garzon  Castaneda    &#13;
 Libardo, iscritta al n. 681 del registro ordinanze 1993 e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  47,  prima  serie    &#13;
 speciale, dell'anno 1993;                                                &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1994 il Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto che il Magistrato di sorveglianza presso il Tribunale  di    &#13;
 Nuoro, chiamato a decidere, a norma dell'art. 102, primo comma, della    &#13;
 legge  24 novembre 1981, n. 689, relativamente alla conversione della    &#13;
 pena pecuniaria della multa di lire 24 milioni ad un cittadino  extra    &#13;
 comunitario,  ha  sollevato,  in  riferimento agli artt. 3 e 27 della    &#13;
 Costituzione, questione di legittimità di detta  disposizione  della    &#13;
 legge  n. 689 del 1981 "nella parte in cui prevede un'unica modalità    &#13;
 di conversione delle pene pecuniarie non eseguite per  insolvibilità    &#13;
 del condannato a prescindere dalla sussistenza o meno del presupposto    &#13;
 della residenza in Italia";                                              &#13;
     che,  in  punto  di  rilevanza, il giudice a quo osserva che, nel    &#13;
 caso concreto, l'interessato dovrà espiare la sola pena detentiva  -    &#13;
 per  giunta, probabilmente, non nella sua totalità, risultando dagli    &#13;
 atti la sua volontà di essere espulso dal territorio dello  Stato  a    &#13;
 norma  dell'art.  7  commi 12-bis, ter e quater, quinquies, e sexies,    &#13;
 del decreto-legge 30 dicembre 1989, n. 416, convertito dalla legge 28    &#13;
 febbraio 1990, n. 39, introdotti dal decreto-legge 14 giugno 1993, n.    &#13;
 187, convertito, con modificazioni, dalla legge 12  agosto  1993,  n.    &#13;
 296  -  non  rendendosi  materialmente possibile l'applicazione della    &#13;
 libertà controllata, per l'espulsione all'atto  della  scarcerazione    &#13;
 ovvero  perché,  "trattandosi  di  persona  senza  fissa  dimora nei    &#13;
 confronti  della  quale   non   si   può   individuare   un   legame    &#13;
 territoriale",  non  possono  essere  applicate nei suoi confronti le    &#13;
 prescrizioni inerenti alla libertà controllata;                         &#13;
      che, in punto di non manifesta infondatezza,  il  Magistrato  di    &#13;
 sorveglianza  ravvisa una disparità di trattamento fra il condannato    &#13;
 residente in Italia, nei cui confronti  è  comunque  applicabile  la    &#13;
 sanzione sostitutiva derivante dalla conversione, e il condannato che    &#13;
 non  ha legami territoriali con l'Italia, il quale, in relazione a un    &#13;
 identico reato, non espierà una parte di pena prevista dalla legge;     &#13;
      che,  sempre  quanto  alla  non   manifesta   infondatezza,   il    &#13;
 Magistrato  di  sorveglianza  ravvisa  anche una lesione dell'art. 27    &#13;
 della Costituzione, sotto  il  profilo  della  "fuga  dalla  sanzione    &#13;
 penale",  con  conseguente  impossibilità della pena di adempiere la    &#13;
 sua funzione rieducativa, anche sul piano sostanziale, impedendosi al    &#13;
 legislatore di prevedere già in partenza le tipologie  sanzionatorie    &#13;
 adatte  ad  agevolare  una  esecuzione personalizzata che consenta il    &#13;
 raggiungimento delle finalità di prevenzione generale e speciale";      &#13;
      che nel giudizio è intervenuto il Presidente del Consiglio  dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  Generale  dello    &#13;
 Stato, chiedendo che la questione venga dichiarata  inammissibile  o,    &#13;
 comunque, infondata;                                                     &#13;
    Considerato  che  le  censure sono state proposte in via del tutto    &#13;
 astratta  ed  ipotetica  sul  presupposto  della   espulsione   dello    &#13;
 straniero  dal  territorio  dello  Stato,  provvedimento  non  ancora    &#13;
 adottato dal giudice dell'esecuzione a norma dell'art. 7,  comma  12-ter,  del  decreto-legge  30  dicembre 1989, n. 416, convertito dalla    &#13;
 legge  28  febbraio  1990,  n.  39,  introdotto   dall'art.   8   del    &#13;
 decreto-legge  14  giugno  1993,  n.  187,  convertito dalla legge 12    &#13;
 agosto, 1993, n. 296;                                                    &#13;
      che,  inoltre,  anche  una  volta  che   sia   stata   disposta,    &#13;
 l'espulsione  produce,  per effetto dell'art. 7, comma 12-quater, del    &#13;
 citato decreto-legge, la sospensione della pena, e dunque anche della    &#13;
 pena pecuniaria convertita ai sensi di legge;                            &#13;
      che, dunque, la questione deve essere dichiarata  manifestamente    &#13;
 inammissibile per l'assenza del necessario requisito della rilevanza.    &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 102, primo comma,  della  legge    &#13;
 24  novembre 1981, n. 689, sollevata in riferimento agli artt. 3 e 27    &#13;
 della  Costituzione,  dal  Magistrato  di  sorveglianza   presso   il    &#13;
 Tribunale di Nuoro con ordinanza del 7 settembre 1993.                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 27 aprile 1994.                                  &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 5 maggio 1994.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
