<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>63</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:63</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/03/1975</data_decisione>
    <data_deposito>12/03/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 10,  ultimi  &#13;
 tre commi, e/o 5, 7, 11 e 12 del d.l. 3 febbraio 1970, n. 7, convertito  &#13;
 in  legge  11  marzo  1970,  n.  83 (Norme in materia di collocamento e  &#13;
 accertamento  dei  lavoratori  agricoli),  promosso  con  ricorso   del  &#13;
 Presidente  della  Giunta  provinciale  di  Bolzano,  notificato  il 19  &#13;
 febbraio 1972, depositato in cancelleria il 29 successivo  ed  iscritto  &#13;
 al n. 36 del registro ricorsi 1972.                                      &#13;
     Visto  l'atto  di  costituzione  del  Presidente  del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 22 gennaio 1975 il Giudice relatore  &#13;
 Paolo Rossi;                                                             &#13;
     udito il sostituto avvocato generale dello Stato Michele  Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con atto notificato il 19 febbraio 1972, il Presidente della Giunta  &#13;
 provinciale di Bolzano, rappresentato e difeso dal prof.  avv. Giuseppe  &#13;
 Guarino,    ha    proposto    ricorso    per    la   dichiarazione   di  &#13;
 incostituzionalità dell'art. 10, ultimi tre commi, del d.l. 3 febbraio  &#13;
 1970, n. 7, e/o degli artt. 5,7,11  e  12  dello  stesso  provvedimento  &#13;
 (cvt.  in  l. 11 marzo 1970, n. 83), per contrasto con l'art. 7, ultimo  &#13;
 comma, della legge costituzionale 10 novembre 1971, n. 1,  secondo  cui  &#13;
 "i  cittadini  residenti  nella provincia di Bolzano hanno diritto alla  &#13;
 precedenza nel collocamento al lavoro nel  territorio  della  provincia  &#13;
 stessa,  esclusa ogni distinzione basata sull'appartenenza ad un gruppo  &#13;
 linguistico o sull'anzianità di residenza".                             &#13;
     La difesa della Provincia premette che la decisione della Corte  n.  &#13;
 192  del  1970,  con  la  quale  venne  accolto  un analogo ricorso, ha  &#13;
 prodotto effetti circoscritti alle aziende agricole con non più di sei  &#13;
 dipendenti, mentre  oltre  questo  limite  riprende  vigore  la  regola  &#13;
 generale  dell'osservanza,  nell'avviamento  al  lavoro, dell'ordine di  &#13;
 iscrizione nelle liste di collocamento  obbligatorio,  regola  generale  &#13;
 che  pregiudica il diritto alla precedenza costituzionalmente garantito  &#13;
 ai residenti nella Provincia di Bolzano.  Ad  avviso  della  ricorrente  &#13;
 l'art.  10  del citato d.l.  vanifica, nel settore del lavoro agricolo,  &#13;
 il  suddetto  diritto  alla  precedenza  costituzionalmente  garantito,  &#13;
 consentendo  a qualsiasi cittadino, anche non residente nella Provincia  &#13;
 di Bolzano, di trasferire la propria iscrizione nelle relative liste di  &#13;
 collocamento, senza cambiare  la  propria  residenza,  con  conseguente  &#13;
 necessaria applicazione della regola generale sopra ricordata.           &#13;
     Si  è  costituito  in  giudizio  il  Presidente  del Consiglio dei  &#13;
 ministri, rappresentato e difeso dall'Avvocatura generale dello  Stato,  &#13;
 con   atto   depositato   il   9   marzo  1972,  chiedendo  dichiararsi  &#13;
 inammissibile, o respingersi nel merito, il ricorso della Provincia.     &#13;
     Osserva la difesa dello Stato che la  soluzione  del  problema  dei  &#13;
 rapporti   tra   preesistente  disciplina  legislativa  dello  Stato  e  &#13;
 sopravvenuta modifica dello Statuto speciale deve aver luogo garantendo  &#13;
 la continuità dell'ordinamento giuridico senza produrre vere e proprie  &#13;
 lacune nella  preesistente  disciplina  legislativa.  L'interpretazione  &#13;
 sistematica  e  le norme transitorie della legge costituzionale n.1 del  &#13;
 1971, dimostrerebbero che il  nuovo  statuto  regionale  non  determina  &#13;
 immediatamente l'illegittimità costituzionale delle preesistenti leggi  &#13;
 dello   Stato   con   esso  confliggenti,  dovendosi  invece  attendere  &#13;
 l'emanazione delle necessarie norme di attuazione.                       &#13;
     Alla pubblica udienza l'Avvocatura generale dello Stato ha  chiesto  &#13;
 venisse  dichiarata  la  cessazione  della  materia  del contendere per  &#13;
 effetto dell'emanazione del d.P.R. 22 marzo 1974, n. 280.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte costituzionale dovrebbe decidere  se  violino  o  meno  il  &#13;
 diritto  alla  precedenza  costituzionalmente  garantito  ai lavoratori  &#13;
 residenti nella provincia di  Bolzano  (art.  7,  ultimo  comma,  legge  &#13;
 costituzionale n. 1 del 1971), gli artt. 10, ultimi tre commi, 5, 7, 11  &#13;
 e 12 del d.l. 3 febbraio 1970, n. 7 (convertito in legge 11 marzo 1970,  &#13;
 n.  83),  secondo  cui,  essendo  consentito ai lavoratori agricoli non  &#13;
 residenti nella  provincia  di  trasferire  la  loro  iscrizione  nelle  &#13;
 relative liste di collocamento, deve seguirsi, nel conseguente avvio al  &#13;
 lavoro,   l'ordine   derivante   dall'anzianità   di  iscrizione,  con  &#13;
 equiparazione tra cittadini residenti e non residenti.                   &#13;
     La provincia di Bolzano ha precisato, nel proposto ricorso, di aver  &#13;
 denunciato le norme impugnate  nella  parte  in  cui  avrebbero  potuto  &#13;
 vanificare   il  diritto  alla  precedenza  riconosciuto  ai  cittadini  &#13;
 residenti nella provincia di Bolzano.                                    &#13;
     Occorre ora ricordare che con d.P.R. 22 marzo 1974,  n.  280,  sono  &#13;
 state  emanate norme di attuazione delle nuove disposizioni statutarie,  &#13;
 in materia di collocamento al lavoro, con le quali  si  è  provveduto,  &#13;
 tra   l'altro,  alla  necessaria  armonizzazione  tra  la  preesistente  &#13;
 legislazione   generale   in   argomento    e    i    nuovi    principi  &#13;
 corrispondentemente enunciati dalla legge costituzionale n. 1 del 1971.  &#13;
 In particolare, per effetto degli artt.  5 e 7 del citato d.P.R. n. 280  &#13;
 del  1974,  pubblicato  nelle  more  del  presente  giudizio, risultano  &#13;
 abrogate le norme denunciate dalla ricorrente, essendosi garantito, con  &#13;
 opportune disposizioni, che i lavoratori residenti nella  provincia  di  &#13;
 Bolzano precedano gli altri lavoratori iscritti nelle relative liste di  &#13;
 collocamento,  a  prescindere dall'anzianità di iscrizione maturata in  &#13;
 altre provincie.                                                         &#13;
     Va pertanto dichiarata la cessazione della materia del contendere.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara cessata la materia del contendere.                          &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 5 marzo 1975.                                 &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   -   NICOLA  REALE  -  PAOLO  ROSSI  -  &#13;
                                   LEONETTO AMADEI - GIULIO GIONFRIDA  -  &#13;
                                   EDOARDO  VOLTERRA  -  GUIDO  ASTUTI -  &#13;
                                   MICHELE  ROSSANO.                      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
