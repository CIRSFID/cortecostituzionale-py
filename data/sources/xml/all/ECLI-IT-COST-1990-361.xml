<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>361</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:361</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/1990</data_decisione>
    <data_deposito>20/07/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi di legittimità costituzionale dell'art. 247 delle norme    &#13;
 d'attuazione, di coordinamento e transitorie del codice di  procedura    &#13;
 penale  del  1988  (testo approvato con decreto legislativo 28 luglio    &#13;
 1989, n. 271) promossi con le seguenti ordinanze:                        &#13;
      1)  ordinanza  emessa il 30 ottobre 1989 dal Tribunale di Napoli    &#13;
 nel procedimento penale a carico di Guida Nunzio ed  altri,  iscritta    &#13;
 al  n.  214  del  registro ordinanze 1990 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 19,  prima  serie  speciale,  dell'anno    &#13;
 1990;                                                                    &#13;
      2)  ordinanza emessa il 15 novembre 1989 dal Tribunale di Ancona    &#13;
 nel procedimento penale  a  carico  di  Mancini  Maurizio  ed  altri,    &#13;
 iscritta  al  n.  240  del registro ordinanze 1990 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  20,  prima  serie  speciale,    &#13;
 dell'anno 1990;                                                          &#13;
      3)  ordinanza emessa il 3 novembre 1989 dalla Corte d'appello di    &#13;
 Ancona nel procedimento penale a carico di Notari Cristiano ed altri,    &#13;
 iscritta  al  n.  271  del registro ordinanze 1990 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  21,  prima  serie  speciale,    &#13;
 dell'anno 1990;                                                          &#13;
    Visto   l'atto  d'intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 26 giugno 1990 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che il Tribunale di Napoli, con ordinanza del 30 ottobre    &#13;
 1989 (Reg. ord. n. 214/90) ha sollevato, in riferimento agli artt.  3    &#13;
 e  24  Cost.  questione  di legittimità costituzionale dell'art. 247    &#13;
 delle norme d'attuazione, di coordinamento e transitorie  del  codice    &#13;
 di procedura penale del 1988 (testo approvato con decreto legislativo    &#13;
 28 luglio 1989, n. 271) nella parte in  cui  limita  l'ammissibilità    &#13;
 del  giudizio  abbreviato  ai  soli  procedimenti nei quali non siano    &#13;
 state compiute le formalità d'apertura  del  dibattimento  di  primo    &#13;
 grado;                                                                   &#13;
      che   analoghe  questioni  di  legittimità  costituzionale  del    &#13;
 sopracitato art. 247 nella parte in cui non consente l'applicabilità    &#13;
 del  giudizio  abbreviato  ai  procedimenti  in  corso all'entrata in    &#13;
 vigore del nuovo codice di procedura  penale  nei  quali  siano  già    &#13;
 state  compiute  le  formalità  d'apertura  del  dibattimento  ed ai    &#13;
 procedimenti che alla medesima data si  trovino  in  fase  d'appello,    &#13;
 sono  state  sollevate,  in  riferimento  agli  artt.  3  e 25 Cost.,    &#13;
 rispettivamente dal Tribunale di Ancona con ordinanza del 15 novembre    &#13;
 1989  (Reg.  ord.  n.  240/90)  e dalla Corte d'Appello di Ancona con    &#13;
 ordinanza del 3 novembre 1989 (Reg. ord. n. 271/90);                     &#13;
      che  nei  giudizi è intervenuto il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che le questioni sollevate dal Tribunale di Napoli e    &#13;
 dalla Corte d'Appello di Ancona siano dichiarate infondate e  che  la    &#13;
 questione   sollevata   dal   Tribunale   di  Ancona  sia  dichiarata    &#13;
 inammissibile per mancanza di motivazione in ordine alla  prospettata    &#13;
 violazione dell'art. 25 Cost. e per il resto infondata;                  &#13;
    Considerato  che,  per  l'identità  o  analogia  delle  questioni    &#13;
 sollevate, i giudizi possono essere riuniti;                             &#13;
      che questa Corte, con la sentenza n. 277 del 1990, ha dichiarato    &#13;
 non fondata,  in  riferimento  all'art.  3  Cost.,  la  questione  di    &#13;
 legittimità  costituzionale  dell'art. 247 delle norme d'attuazione,    &#13;
 di coordinamento e transitorie del codice di procedura penale  (testo    &#13;
 approvato con decreto legislativo 28 luglio 1989, n. 271) nella parte    &#13;
 in  cui  limita   l'ammissibilità   del   giudizio   abbreviato   ai    &#13;
 procedimenti  in corso alla data d'entrata in vigore del nuovo codice    &#13;
 di procedura penale nei quali non siano state compiute le  formalità    &#13;
 d'apertura del dibattimento di primo grado;                              &#13;
      che,  in  particolare,  nella  citata  sentenza, la Corte ha fra    &#13;
 l'altro  sottolineato  l'"inscindibile  unità   finalistica"   della    &#13;
 disposizione  impugnata,  osservando  che  la riduzione della pena in    &#13;
 tanto è consentita in quanto è diretta a sollecitare la  richiesta,    &#13;
 da  parte  dell'imputato,  dell'attivazione  d'un  istituto inteso ad    &#13;
 assicurare la rapida definizione  del  maggior  numero  di  processi;    &#13;
 divenuto,  invece,  impossibile,  con  l'apertura  del  dibattimento,    &#13;
 raggiungere le finalità che  il  legislatore  si  prefigge,  diventa    &#13;
 conseguentemente  e razionalmente impossibile all'imputato realizzare    &#13;
 il c.d. "diritto" alla riduzione della pena;                             &#13;
      che,   questo   essendo  lo  scopo  dell'istituto  del  giudizio    &#13;
 abbreviato  (esclusione  della  fase  dibattimentale)  è  del  tutto    &#13;
 razionale  che, per i procedimenti in corso all'entrata in vigore del    &#13;
 nuovo codice di procedura penale, tale istituto sia reso  applicabile    &#13;
 soltanto quando il suo scopo sia interamente perseguibile;               &#13;
      che,  la precitata sentenza ha altresì aggiunto che irrazionale    &#13;
 sarebbe  semmai  l'applicabilità  del   giudizio   abbreviato   dopo    &#13;
 l'apertura del dibattimento; giacché in tal caso i benefici concessi    &#13;
 all'imputato non sarebbero più giustificati né dallo  scopo  (ormai    &#13;
 impossibile)  d'eliminare  la  fase  dibattimentale  né  dal rischio    &#13;
 assunto dall'imputato (il quale si troverebbe, invece,  nella  comoda    &#13;
 situazione di decidere dopo che il pubblico ministero ha già offerto    &#13;
 le sue prove e comunque dopo aver  potuto  valutare  l'andamento  del    &#13;
 dibattimento stesso);                                                    &#13;
      che, pertanto, non è producente il confronto fra imputati per i    &#13;
 quali il dibattimento sia  stato  o  non  sia  stato  ancora  aperto,    &#13;
 proprio perché si tratta di situazioni oggettivamente diverse;          &#13;
      che le ordinanze in esame non adducono profili o argomenti nuovi    &#13;
 o comunque tali da poter indurre la Corte  a  modificare  il  proprio    &#13;
 orientamento;                                                            &#13;
      che,   pertanto,   le   sollevate   questioni   di  legittimità    &#13;
 costituzionale vanno dichiarate manifestamente infondate;                &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  delle    &#13;
 questioni di legittimità costituzionale dell'art.  247  delle  norme    &#13;
 d'attuazione,  di coordinamento e transitorie del codice di procedura    &#13;
 penale del 1988 (testo approvato con decreto  legislativo  28  luglio    &#13;
 1989,  n. 271) sollevate, in riferimento agli artt. 3, 24 e 25 Cost.,    &#13;
 dal Tribunale di Napoli,  dal  Tribunale  di  Ancona  e  dalla  Corte    &#13;
 d'Appello di Ancona con le ordinanze indicate in epigrafe.               &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, l'11 luglio 1990.          &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 20 luglio 1990.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
