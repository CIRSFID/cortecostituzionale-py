<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>538</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:538</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/11/2000</data_decisione>
    <data_deposito>23/11/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 33 del decreto    &#13;
 legislativo  31 marzo  1998,  n. 80 (Nuove disposizioni in materia di    &#13;
 organizzazione   e   di  rapporti  di  lavoro  nelle  amministrazioni    &#13;
 pubbliche,  di  giurisdizione  nelle  controversie  di  lavoro  e  di    &#13;
 giurisdizione  amministrativa,  emanate  in  attuazione dell'art. 11,    &#13;
 comma  4,  della  legge  15 marzo  1997,  n. 59),  promossi  con nove    &#13;
 ordinanze   emesse  il  9 marzo  1999  dal  Tribunale  amministrativo    &#13;
 regionale  della  Sicilia,  rispettivamente iscritte ai nn. 309, 341,    &#13;
 342,  343,  344,  349,  350,  351 e 352 del registro ordinanze 1999 e    &#13;
 pubblicate nella Gazzetta Ufficiale della Repubblica nn. 22, 24 e 25,    &#13;
 1ª serie speciale, dell'anno 1999.                                       &#13;
     Visto l'atto di costituzione di Bruccoleri Raffaele;                 &#13;
     Udito  nella  camera  di consiglio del 12 ottobre 2000 il giudice    &#13;
 relatore Riccardo Chieppa.                                               &#13;
     Ritenuto che il Tribunale amministrativo regionale della Sicilia,    &#13;
 con  nove  ordinanze  di  identico contenuto (r.o. nn. 309, 341, 342,    &#13;
 343,  344,  349,  350, 351, 352 del 1999) emesse il 9 marzo 1999, nel    &#13;
 corso  di  altrettanti  giudizi  aventi  ad  oggetto  la  condanna di    &#13;
 amministrazioni  pubbliche  inadempienti  al  pagamento  di  somme di    &#13;
 denaro  dovute a titolo di forniture di medicinali, ed a fronte delle    &#13;
 domande   dei  ricorrenti  dirette  ad  ottenere,  nelle  more  delle    &#13;
 decisioni  di  merito  ed  in  relazione ad irreparabile pregiudizio,    &#13;
 l'applicazione  degli  artt. 669-sexies e 700 del codice di procedura    &#13;
 civile,  nonché  dell'art. 186-ter  cod.proc.civ.,  ha sollevato, in    &#13;
 riferimento  agli  artt. 3, 97 e 113 della Costituzione, questione di    &#13;
 legittimità  costituzionale  dell'art. 33  del  decreto  legislativo    &#13;
 31 marzo 1998, n. 80 (Nuove disposizioni in materia di organizzazione    &#13;
 e   di   rapporti  di  lavoro  nelle  amministrazioni  pubbliche,  di    &#13;
 giurisdizione   nelle  controversie  di  lavoro  e  di  giurisdizione    &#13;
 amministrativa,  emanate  in  attuazione dell'art. 11, comma 4, della    &#13;
 legge  15 marzo  1997,  n. 59),  nella parte in cui, pur spostando la    &#13;
 giurisdizione  di  talune  materie  dal  giudice ordinario al giudice    &#13;
 amministrativo,  in  via  esclusiva,  non  consente a quest'ultimo di    &#13;
 utilizzare  tutti i mezzi processuali previsti dal codice di rito per    &#13;
 la  tutela  sommaria dei diritti sui quali è legittimato a decidere,    &#13;
 con  particolare  riferimento  agli  istituti  di cui al Titolo I del    &#13;
 Libro IV cod.proc.civ;                                                   &#13;
         che,  secondo  il  giudice  rimettente, le norme del processo    &#13;
 civile,  pur  dotate  di  una  particolare  "vis  espansiva", tale da    &#13;
 renderle applicabili analogicamente in tutti i casi in cui manchi una    &#13;
 regola  processuale  ben  definita,  tuttavia  incontrano  il  limite    &#13;
 costituito dalla struttura propria del processo amministrativo, entro    &#13;
 il   quale   non  possono  essere  trasfuse  per  ragioni  di  ordine    &#13;
 dogmatico-sistematico   e,   soprattutto,  per  ragioni  pratiche  ed    &#13;
 organizzative;                                                           &#13;
         che,   neanche   potrebbe  essere  di  ausilio  il  principio    &#13;
 affermato  dalla  giurisprudenza  costituzionale (sentenza n. 190 del    &#13;
 1995),  secondo  cui  il  giudice  amministrativo, nelle controversie    &#13;
 patrimoniali   sottoposte  alla  sua  giurisdizione  esclusiva,  può    &#13;
 adottare,  in  presenza  di un pregiudizio imminente ed irreparabile,    &#13;
 tutti i provvedimenti urgenti, che appaiano più idonei ad assicurare    &#13;
 provvisoriamente  gli  effetti della decisione di merito, in quanto i    &#13;
 concetti di "tutela cautelare" e di "cognizione sommaria" del diritto    &#13;
 sono  assolutamente  diversi  e,  mentre  il primo non è estraneo al    &#13;
 processo amministrativo, non altrettanto può dirsi del secondo;         &#13;
         che,  ad  avviso  del  giudice  a  quo neanche può giungersi    &#13;
 all'affermazione  di  un parziale difetto di giurisdizione, in quanto    &#13;
 ciò  implicherebbe  una  sostanziale  riduzione  della portata della    &#13;
 legge-delega;                                                            &#13;
         che,  in sostanza, le questioni risolvibili con la cognizione    &#13;
 sommaria sfuggirebbero alla cognizione di qualunque giudice: a quella    &#13;
 del  giudice ordinario in forza delle norme deleganti e delegate ed a    &#13;
 quella   del   giudice   amministrativo  in  difetto  della  puntuale    &#13;
 indicazione normativa degli strumenti processuali all'uopo necessari.    &#13;
     Considerato che, attesa la sostanziale identità delle questioni,    &#13;
 dev'essere disposta la riunione dei relativi giudizi;                    &#13;
         che  la norma censurata è stata modificata dall'art. 8 della    &#13;
 legge  21 luglio  2000,  n. 205,  pubblicata nella Gazzetta Ufficiale    &#13;
 n. 173 del 26 luglio 2000;                                               &#13;
         che  la  nuova formulazione della norma prevede espressamente    &#13;
 che  nelle  controversie  devolute  alla  giurisdizione esclusiva del    &#13;
 giudice  amministrativo,  aventi  ad  oggetto  diritti  soggettivi di    &#13;
 natura  patrimoniale, si applichi il Capo I del Titolo I del Libro IV    &#13;
 del  codice di procedura civile (comma 1), mentre, qualora, in ordine    &#13;
 al   credito   azionato,   ricorrano   i   presupposti  di  cui  agli    &#13;
 artt. 186-bis  e  186-ter del codice di procedura civile, si disponga    &#13;
 in   via   provvisionale,   su   istanza   di  parte,  con  ordinanza    &#13;
 provvisoriamente  esecutiva,  la  condanna  al  pagamento di somme di    &#13;
 denaro;                                                                  &#13;
         che,  pertanto,  va  disposta  la  restituzione degli atti al    &#13;
 giudice  rimettente  perché  valuti  se, a seguito della intervenuta    &#13;
 modifica  legislativa  della disposizione denunciata, la questione di    &#13;
 legittimità  costituzionale  sollevata  sia  tuttora  rilevante  nel    &#13;
 procedimento a quo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti i giudizi,                                                   &#13;
     Ordina  la  restituzione  degli  atti al Tribunale amministrativo    &#13;
 regionale della Sicilia.                                                 &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 novembre 2000.                             &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Chieppa                            &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 23 novembre 2000.                       &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
