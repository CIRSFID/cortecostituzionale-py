<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>45</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:45</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/02/1998</data_decisione>
    <data_deposito>05/03/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'art. 1 del d.-l.   28    &#13;
 marzo  1996,  n. 166 (Norme in materia previdenziale) e dell'art.  22    &#13;
 della legge 21 luglio  1965,  n.  903  (Avviamento  della  riforma  e    &#13;
 miglioramento   dei   trattamenti  di  pensione  e  della  previdenza    &#13;
 sociale),  come modificato dalla sentenza n. 495 del 1993 della Corte    &#13;
 costituzionale, promossi con n. 34 ordinanze emesse il 17 aprile  (n.    &#13;
 33  ordinanze)  ed  il 10 maggio 1996 (n. 1 ordinanza) dal pretore di    &#13;
 Brescia, rispettivamente iscritte al n. 1077, dal n. 1081 al n.  1098    &#13;
 e  dal  n.  1120  al n. 1134 del registro ordinanze 1996 e pubblicate    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  42,  prima   serie    &#13;
 speciale, dell'anno 1996;                                                &#13;
   Udito  nella  camera  di  consiglio  del 28 gennaio 1998 il giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
   Ritenuto che, nel corso di vari giudizi instaurati per ottenere  la    &#13;
 ricostruzione  del trattamento pensionistico in base alla sentenza n.    &#13;
 495 del 1993 di questa Corte, il pretore di Brescia, con 33 ordinanze    &#13;
 di identico contenuto emesse tutte il 17 aprile  1996,  ha  sollevato    &#13;
 questione  di  legittimità  costituzionale  dell'art. 1 del d.-l. 28    &#13;
 marzo 1996, n. 166 (Norme in materia previdenziale);                     &#13;
     che, secondo il rimettente, la  norma  censurata  -  sopravvenuta    &#13;
 nelle  more  dei  giudizi  e  contenente  disposizioni  relative alle    &#13;
 modalità di pagamento delle somme maturate in  favore  degli  aventi    &#13;
 diritto  in  applicazione  della  citata  sentenza  di illegittimità    &#13;
 costituzionale e della sentenza n. 240 del  1994  -  si  porrebbe  in    &#13;
 contrasto  con  l'art.    81,  quarto  comma, della Costituzione, per    &#13;
 violazione dell'obbligo di copertura finanziaria  relativamente  agli    &#13;
 anni  1999, 2000 e 2001, non potendosi ritenere, il denunciato vulnus    &#13;
 eliminato dalla previsione del meccanismo di  estinzione  del  debito    &#13;
 mediante l'assegnazione di titoli di Stato;                              &#13;
     che,  nel  corso  di analoghi giudizi, il pretore di Brescia, con    &#13;
 ordinanza emessa il 10  maggio  1996,  oltre  a  riproporre  identica    &#13;
 questione    di   legittimità   costituzionale   dell'art.   1   del    &#13;
 decreto-legge n. 166 del 1996, ha  sollevato  altresì  questione  di    &#13;
 legittimità  costituzionale dell'art. 22 della legge 21 luglio 1965,    &#13;
 n. 903 (Avviamento della riforma e miglioramento dei  trattamenti  di    &#13;
 pensione  e della previdenza sociale), come modificato dalla sentenza    &#13;
 n. 495 del 1993 di questa Corte (in senso estensivo  del  diritto  di    &#13;
 integrazione   al   minimo)   per   violazione   dell'art.  81  della    &#13;
 Costituzione,   non   sottraendosi   all'obbligo   della    copertura    &#13;
 finanziaria   neppure  la  norma  "virtuale"  creata  dalla  suddetta    &#13;
 pronuncia di incostituzionalità;                                        &#13;
   Considerato che i giudizi possono essere riuniti  e  congiuntamente    &#13;
 decisi, in quanto riguardanti analoghe questioni;                        &#13;
     che  il d.-l. 28 marzo 1996, n. 166 non è stato convertito e che    &#13;
 la censurata normativa è stata reiterata dai dd.-ll. 27 maggio 1996,    &#13;
 n. 295, 26 luglio 1996, n. 396, e 24 settembre 1996,  n.  499,  tutti    &#13;
 decaduti;                                                                &#13;
     che  gli  effetti  della  decretazione d'urgenza sono stati fatti    &#13;
 salvi dall'art. 1, comma 6, della legge 28 novembre 1996, n. 608;        &#13;
     che l'art. 1, commi 181 e 184, della legge 23 dicembre  1996,  n.    &#13;
 662,  ha  introdotto  diversi  criteri di copertura finanziaria della    &#13;
 complessiva  previsione  di  pagamento  delle   somme   dovute   agli    &#13;
 interessati in applicazione delle sentenze della Corte costituzionale    &#13;
 n. 495 del 1993 e n. 240 del 1994;                                       &#13;
     che  peraltro  nelle  fattispecie riveste preliminare rilievo, in    &#13;
 termini  di  sovraordinazione  logico-processuale  rispetto  ad  ogni    &#13;
 possibile  censura  di  incostituzionalità  (v.  sentenza n. 103 del    &#13;
 1995), la considerazione che tanto nella normativa  decretale  quanto    &#13;
 in  quella  di legge (art. 1, comma 183, della legge n. 662 del 1996)    &#13;
 viene  sancito  che  i  giudizi  pendenti  siano  dichiarati  estinti    &#13;
 d'ufficio;                                                               &#13;
     che  la  mancata  censura  di  tale  previsione,  la  quale trova    &#13;
 immediata  applicazione  anche  nei  processi  a  quibus  (come,  tra    &#13;
 l'altro,  avverte  lo  stesso rimettente), rende irrilevanti tutte le    &#13;
 sollevate   questioni,   che   pertanto   risultano    manifestamente    &#13;
 inammissibili (v. ordinanze n. 368 e n. 370 del 1997);                   &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, della norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la manifesta inammissibilità delle    &#13;
 questioni di legittimità costituzionale dell'art.  1  del  d.-l.  28    &#13;
 marzo   1996,  n.  166  (Norme  in  materia  previdenziale),  nonché    &#13;
 dell'art.  22 della legge 21 luglio 1965, n.  903  (Avviamento  della    &#13;
 riforma   e   miglioramento  dei  trattamenti  di  pensione  e  della    &#13;
 previdenza sociale) - come modificato dalla sentenza n. 495 del  1993    &#13;
 della  Corte  costituzionale  - sollevate, in riferimento all'art. 81    &#13;
 della Costituzione, dal pretore di Brescia, con le ordinanze indicate    &#13;
 in epigrafe.                                                             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 25 febbraio 1998.                             &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 5 marzo 1998.                             &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
