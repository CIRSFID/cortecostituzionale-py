<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>381</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:381</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/11/1997</data_decisione>
    <data_deposito>11/12/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio di legittimità costituzionale dell'art. 18 del r.d.-l.    &#13;
 1  luglio  1926,  n.  2290  (Ordinamento  dei  magazzini   generali),    &#13;
 convertito nella legge 9 giugno 1927, n. 1158, promosso con ordinanza    &#13;
 emessa  il  12  giugno  1996  dalla  Corte  d'appello  di  Milano nel    &#13;
 procedimento civile vertente tra il fallimento della  s.r.l.  Michele    &#13;
 Tavella  e  la s.p.a. Magazzini Fiduciari Cariplo iscritta al n. 1183    &#13;
 del registro ordinanze 1996 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 44, prima serie speciale, dell'anno 1996.            &#13;
   Visto  l'atto  di  costituzione del fallimento della s.r.l. Michele    &#13;
 Tavella;                                                                 &#13;
   Udito nella camera di consiglio del  15  ottobre  1997  il  giudice    &#13;
 relatore Fernando Santosuosso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  corso  di  un giudizio civile instaurato dal fallimento    &#13;
 della s.r.l. Michele Tavella nei  confronti  della  s.p.a.  Magazzini    &#13;
 Fiduciari Cariplo la Corte d'appello di Milano ha sollevato questione    &#13;
 di  legittimità  costituzionale,  in riferimento agli artt. 24 e 102    &#13;
 della Costituzione, dell'art. 18 del r.d.-l. 1 luglio 1926,  n.  2290    &#13;
 (Ordinamento dei magazzini generali), convertito nella legge 9 giugno    &#13;
 1927, n. 1158.                                                           &#13;
   Il  giudice  a  quo  ha  premesso  che  il tribunale di Milano, nel    &#13;
 decidere in primo  grado  la  causa  tra  le  medesime  parti,  aveva    &#13;
 dichiarato  l'improponibilità  di  una serie di domande risarcitorie    &#13;
 avanzate dalla società Tavella, successivamente dichiarata  fallita,    &#13;
 trattandosi  di  domande  per  le  quali  doveva  ritenersi per legge    &#13;
 devoluta la decisione ad un collegio arbitrale, ai sensi della  norma    &#13;
 impugnata.                                                               &#13;
   Tanto  premesso  la  Corte  rimettente ha osservato che l'arbitrato    &#13;
 delineato dall'art. 18 in  oggetto  è  effettivamente  un  arbitrato    &#13;
 obbligatorio,   già  dichiarato  costituzionalmente  illegittimo  da    &#13;
 numerose sentenze di questa Corte, fra  le  quali  il  rimettente  ha    &#13;
 richiamato la n. 127 del 1977.                                           &#13;
   2.  -  Nel  giudizio  davanti  a  questa  Corte si è costituito il    &#13;
 fallimento della società Tavella, con  apposita  memoria,  chiedendo    &#13;
 l'accoglimento della prospettata questione.                              &#13;
   La  parte  privata,  dopo  aver  brevemente ripercorso l'iter della    &#13;
 causa di merito, si è  associata  alle  considerazioni  fatte  dalla    &#13;
 Corte d'appello di Milano, richiamando le numerose sentenze di questa    &#13;
 Corte   che  hanno  in  più  occasioni  dichiarato  l'illegittimità    &#13;
 costituzionale dell'istituto dell'arbitrato obbligatorio.<diritto>Considerato in diritto</diritto>1. - La Corte d'appello di Milano dubita che l'art. 18 del  r.d.-l.    &#13;
 1  luglio  1926, n. 2290, convertito in legge 9 giugno 1927, n. 1158,    &#13;
 nella parte in cui stabilisce che le controversie tra gli esercenti i    &#13;
 magazzini generali e i depositanti in ordine  all'applicazione  delle    &#13;
 tariffe   "saranno   risolte  dal  competente  consiglio  provinciale    &#13;
 dell'economia" (ora Camera di commercio), sia in  contrasto  con  gli    &#13;
 artt.  24  e  102  della Costituzione, in quanto prevede una forma di    &#13;
 arbitrato obbligatorio che non consente alle parti di optare  per  la    &#13;
 risoluzione in via giudiziaria delle controversie medesime.              &#13;
   2. - La questione è fondata.                                          &#13;
   La norma impugnata stabilisce che le "controversie" insorte tra gli    &#13;
 esercenti   i   magazzini   generali   ed  i  depositanti  in  ordine    &#13;
 all'applicazione delle  tariffe  "saranno  risolte"  dall'organo  ivi    &#13;
 indicato,  e non prevede alcuna diversa opzione degli interessati. La    &#13;
 Corte remittente (come  già  ritenuto  dal  Tribunale)  ravvisa  nel    &#13;
 tenore di tale norma una forma di arbitrato obbligatorio, dal momento    &#13;
 che  le parti devono devolvere la risoluzione delle controversie alla    &#13;
 Camera di commercio, la decisione sul successivo ricorso al  Ministro    &#13;
 è  "inappellabile"  e  la legge stabilisce a priori anche gli organi    &#13;
 giudicanti in primo  grado  ed  in  sede  di  impugnazione.  Siffatta    &#13;
 disposizione  è  stata  sostanzialmente  recepita  nell'art.  90 del    &#13;
 regolamento dei Magazzini generali di Cremona, approvato con  decreto    &#13;
 ministeriale  del  30 maggio 1959, secondo cui la giunta della Camera    &#13;
 di commercio "decide quale arbitro amichevole compositore".              &#13;
   Tale  arbitrato,  in  quanto  obbligatorio,  è  costituzionalmente    &#13;
 illegittimo  per contrasto con gli artt. 24 e 102 della Costituzione,    &#13;
 secondo il costante insegnamento di questa Corte (v., tra  le  altre,    &#13;
 le  sentenze  nn.  127  del  1977, 488 del 1991, 49 del 1994, 206 del    &#13;
 1994, 232 del 1994, 54 del 1996 e 152 del 1996).                         &#13;
   Ne consegue che nell'odierna sede valgono le  stesse  ragioni  più    &#13;
 volte  indicate  da questa Corte nelle sentenze sopra richiamate; per    &#13;
 cui si impone la declaratoria di illegittimità costituzionale  della    &#13;
 norma impugnata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale dell'art. 18 del r.d.-l.    &#13;
 1  luglio  1926,  n.  2290  (Ordinamento  dei  magazzini   generali),    &#13;
 convertito nella legge 9 giugno 1927, n. 1158.                           &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 27 novembre 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria l'11 dicembre 1997.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
