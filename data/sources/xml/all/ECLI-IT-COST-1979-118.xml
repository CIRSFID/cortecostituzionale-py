<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>118</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:118</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Guglielmo Roehrssen</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/10/1979</data_decisione>
    <data_deposito>10/10/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 512, n. 2,  &#13;
 cod. proc. pen., promosso con ordinanza emessa il 28 ottobre  1976  dal  &#13;
 tribunale  di  Camerino,  nel  procedimento  penale a carico di Massimi  &#13;
 Vittorio ed altro, iscritta al n. 749 del  registro  ordinanze  1976  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n.   31 del 2  &#13;
 febbraio 1977.                                                           &#13;
     Udito nella camera di  consiglio  del  4  maggio  1979  il  Giudice  &#13;
 relatore Guglielmo Roehrssen.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza 28 ottobre 1976 il tribunale di Camerino ha sollevato  &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3 e  &#13;
 24  della Costituzione, dell'art. 512, n. 2, c.p.p., nella parte in cui  &#13;
 tale articolo non prevede  l'appellabilità,  da  parte  dell'imputato,  &#13;
 della  sentenza  di  proscioglimento per mancanza di querela, qualora a  &#13;
 carico dell'imputato  stesso  sia  stata  accertata,  con  la  sentenza  &#13;
 medesima,  la  materiale  sussistenza  del  fatto  costituente  reato e  &#13;
 perseguibile soltanto a querela.                                         &#13;
     Ha esposto nell'ordinanza che, con sentenza del 16 aprile 1976,  il  &#13;
 pretore  di  Camerino  ebbe  a  prosciogliere, per mancanza di querela,  &#13;
 Massimi Vittorio e Giordano Antonio - imputati di  minaccia  grave  nei  &#13;
 confronti   di  un  capitano  dei  carabinieri  -  dopo  avere  esclusa  &#13;
 l'aggravante del capoverso dell'art. 612 C.P. e ritenuto  che  i  fatti  &#13;
 accertati  integravano  gli  estremi  dei  reati di minaccia semplice e  &#13;
 diffamazione, perseguibili a querela di parte e non  d'ufficio.  Contro  &#13;
 tale  sentenza  gli  imputati, che avevano sempre negato ogni addebito,  &#13;
 interposero appello ma - ha osservato il tribunale - tale appello, alla  &#13;
 stregua  del  disposto  dell'art.  512,   C.P.P.,   è   da   ritenersi  &#13;
 inammissibile.                                                           &#13;
     Ciò  premesso,  nell'ordinanza  di  rimessione  si  osserva che il  &#13;
 pretore ebbe a prosciogliere gl'imputati per difetto di  querela,  dopo  &#13;
 avere   accertato,   a   loro  carico,  la  materiale  sussistenza  del  &#13;
 fatto-reato,  accertamento  questo  che,  avendo  ad   oggetto   "fatti  &#13;
 materiali", fa stato erga omnes ed in qualunque sede.                    &#13;
     Ne  consegue,  secondo  il  tribunale  di  Camerino, che la mancata  &#13;
 previsione, da parte dell'art. 512 c.p.p., della  appellabilità  della  &#13;
 sentenza  pretorile  di  proscioglimento  per  difetto  di  querela, si  &#13;
 risolve in una grave menomazione del diritto di  difesa  che,  a  norma  &#13;
 dell'art.  24 della Costituzione, non può subire limitazioni.           &#13;
     Essa, inoltre - si osserva nell'ordinanza - si risolve anche in una  &#13;
 disparità di trattamento in contrasto con l'art. 3 della Costituzione,  &#13;
 giacché  l'art. 512, mentre non consente l'appello avverso la sentenza  &#13;
 di proscioglimento per difetto di  querela,  ammette  invece  l'appello  &#13;
 avverso  la  sentenza di proscioglimento per insufficienza di prove, la  &#13;
 quale è meno gravosa per l'imputato della sentenza di  proscioglimento  &#13;
 per   mancanza   di  querela,  ove  il  giudice  pronunci  sentenza  di  &#13;
 proscioglimento con tale formula  dopo  avere  accertato  la  materiale  &#13;
 sussistenza del fatto-reato a carico del prosciolto.                     &#13;
     Nel giudizio dinanzi a questa Corte nessuna parte si è costituita.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - La Corte è chiamata a decidere se l'art. 512, n. 2, c.p.p. -  &#13;
 nella parte  in  cui  non  consente  all'imputato  di  appellare  dalla  &#13;
 sentenza  di  proscioglimento  per  mancanza  di querela, qualora a suo  &#13;
 carico sia accertata, con la sentenza  stessa,  la  sussistenza  di  un  &#13;
 fatto  materiale  costituente  reato  perseguibile  a  querela - sia in  &#13;
 contrasto con gli artt. 3 e 24 della Costituzione  per  violazione  del  &#13;
 diritto  di  difesa  che ne deriverebbe all'imputato e la disparità di  &#13;
 trattamento rispetto agl'imputati assolti per insufficienza di prove.    &#13;
     La questione non è fondata.                                         &#13;
     2. - In realtà la questione stessa è diversa da quelle  esaminate  &#13;
 da  questa Corte nelle decisioni n. 73 del 1978, n. 70 del 1975 e n. 72  &#13;
 del 1979, aventi per oggetto l'esclusione del diritto dell'imputato  di  &#13;
 appellare   dalle  sentenze  di  proscioglimento  per  amnistia  o  per  &#13;
 prescrizione,  emesse  previo  accertamento   della   sussistenza   del  &#13;
 fatto-reato  e  della  responsabilità  dell'imputato, con pronuncia di  &#13;
 estinzione del reato in conseguenza del riconoscimento  di  circostanze  &#13;
 attenuanti   e,   eventualmente,   di   giudizio  di  prevalenza  sulle  &#13;
 circostanze aggravanti contestategli.                                    &#13;
     Invero, caratteristica delle predette sentenze, come già  rilevato  &#13;
 da  questa Corte nelle decisioni sopra indicate, è quella di contenere  &#13;
 accertamenti a carico dell'imputato, per lui pregiudizievoli, che fanno  &#13;
 stato in altri giudizi o consentono il  prodursi  di  effetti  per  lui  &#13;
 negativi,   dato   il   carattere   di  pronunce  di  merito,  rese  in  &#13;
 dibattimento, proprio delle sentenze in questione.                       &#13;
     Viceversa le sentenze di proscioglimento per mancanza  di  querela,  &#13;
 producono  effetti  meramente processuali, tanto che ai sensi dell'art.  &#13;
 17 c.p.p.,  ancorché  siano  divenute  irrevocabili,  non  impediscono  &#13;
 l'esercizio dell'azione penale contro la stessa persona, ove la querela  &#13;
 sia proposta successivamente ed in termini.                              &#13;
     Inoltre,  gli  eventuali  accertamenti  di  fatti materiali in esse  &#13;
 contenuti hanno carattere incidentale  e  non  costituiscono  l'oggetto  &#13;
 immediato  e  diretto  del  procedimento  penale cosicché - secondo le  &#13;
 affermazioni  della  dottrina  in  materia  -  non  possono  acquistare  &#13;
 efficacia di giudicato in altri giudizi civili o amministrativi.         &#13;
     Ne  deriva  che,  non  potendo  le  sentenze di proscioglimento per  &#13;
 mancanza di querela produrre effetti pregiudizievoli per l'imputato, la  &#13;
 mancata previsione di un giudizio di appello contro di esse (nel quale,  &#13;
 comunque, per la mancanza della querela non potrebbero essere  compiuti  &#13;
 accertamenti  di  merito)  non  può determinare lesione del diritto di  &#13;
 difesa o del principio di uguaglianza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 512, n. 2, codice procedura penale, sollevata con l'ordinanza  &#13;
 di  cui  in  epigrafe,  in  riferimento  agli  artt.  3  e   24   della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 2 ottobre 1 979.        &#13;
                                   F.to:  LEONETTO  AMADEI   -   EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO  -  ANTONINO  DE  STEFANO   -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
