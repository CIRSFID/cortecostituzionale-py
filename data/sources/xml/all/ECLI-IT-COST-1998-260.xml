<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>260</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:260</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/06/1998</data_decisione>
    <data_deposito>09/07/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'art. 33, comma 1, del    &#13;
 decreto legislativo  31  dicembre  1992,  n.  546  (Disposizioni  sul    &#13;
 processo  tributario  in attuazione della delega al Governo contenuta    &#13;
 nell'art. 30 della legge 30 dicembre 1991, n. 413), promossi  con  n.    &#13;
 15  ordinanze  emesse  il 6 febbraio 1997, il 18 dicembre 1996, il 14    &#13;
 gennaio 1997 (n. 2 ordinanze), il 18 dicembre  1996,  il  14  gennaio    &#13;
 1997,  il 16 ottobre 1996 (n. 2 ordinanze), il 20 novembre 1996, il 6    &#13;
 febbraio 1997, il 14 gennaio 1997, il 5 marzo  1997,  il  22  ottobre    &#13;
 1996  ed  il  14  gennaio  1997  (n.  2  ordinanze) dalla commissione    &#13;
 tributaria regionale di Milano, rispettivamente iscritte ai numeri da    &#13;
 701 a 715 del  registro ordinanze 1997 e  pubblicate  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 43, prima serie speciale,  dell'anno    &#13;
 1997.                                                                    &#13;
   Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  17 giugno 1998 il giudice    &#13;
 relatore Annibale Marini.                                                &#13;
   Ritenuto che la commissione tributaria  regionale  di  Milano,  con    &#13;
 quindici  ordinanze  di  identico  contenuto emesse tra il 16 ottobre    &#13;
 1996 e il 5 marzo 1997, ha sollevato, in riferimento agli  artt.  24,    &#13;
 secondo   comma,   53,   primo   comma  e  101,  primo  comma,  della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art.  33,    &#13;
 comma   1,   del   decreto  legislativo  31  dicembre  1992,  n.  546    &#13;
 (Disposizioni sul processo tributario in attuazione della  delega  al    &#13;
 Governo  contenuta  nell'art.    30  della legge 30 dicembre 1991, n.    &#13;
 413), "laddove subordina la pubblicità dell'udienza in cui si svolge    &#13;
 la trattazione della causa alla previa tempestiva istanza  di  almeno    &#13;
 una delle parti";                                                        &#13;
     che,  a  parere  della  commissione  rimettente,  la disposizione    &#13;
 denunciata, condizionando alla valutazione discrezionale delle  parti    &#13;
 costituite   la   pubblicità   dell'udienza   di  trattazione  delle    &#13;
 controversie tributarie, violerebbe: l'art. 101, primo comma,  Cost.,    &#13;
 in  quanto,  trovando  fondamento  l'amministrazione  della giustizia    &#13;
 nella sovranità popolare, dovrebbe ritenersi implicito  in  siffatto    &#13;
 precetto  la  regola  generale  della  pubblicità  dei  dibattimenti    &#13;
 giudiziari; l'art. 53, primo comma, Cost., in quanto il principio  di    &#13;
 trasparenza dell'obbligazione tributaria, enunciato nella sentenza di    &#13;
 questa   Corte   n.  50  del  1989,  risulterebbe  incompatibile  con    &#13;
 l'esclusione della pubblicità  dell'udienza;  l'art.    24,  secondo    &#13;
 comma,  Cost.,  in quanto la norma denunciata impedirebbe alle parti,    &#13;
 sia in proprio che mediante i loro difensori, di essere  presenti  in    &#13;
 camera  di  consiglio  prima  della  decisione  e  subordinerebbe  la    &#13;
 discussione in pubblica udienza ad una apposita istanza da depositare    &#13;
 in segreteria e notificare alle altre parti costituite entro un breve    &#13;
 termine di decadenza;                                                    &#13;
     che  nel  giudizio  dinanzi  a  questa  Corte  è  intervenuto il    &#13;
 Presidente  del  Consiglio  dei  Ministri,  rappresentato  e   difeso    &#13;
 dall'Avvocatura   generale   dello   Stato,   che   ha  concluso  per    &#13;
 l'infondatezza della questione.                                          &#13;
   Considerato che i giudizi riguardano una identica questione e vanno    &#13;
 perciò riuniti per essere decisi con unica pronunzia;                   &#13;
     che la questione, negli stessi termini in cui viene  prospettata,    &#13;
 è  stata già dichiarata non fondata da questa Corte con la sentenza    &#13;
 n. 141 del 1998;                                                         &#13;
     che nelle ordinanze di rimessione non sono dedotti profili  nuovi    &#13;
 o  diversi  che  possano  indurre  questa  Corte  ad un riesame della    &#13;
 questione  che  deve,  pertanto,  essere  dichiarata   manifestamente    &#13;
 infondata.                                                               &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale dell'art. 33, comma  1,  del    &#13;
 decreto  legislativo  31  dicembre  1992,  n.  546  (Disposizioni sul    &#13;
 processo tributario in attuazione della delega al  Governo  contenuta    &#13;
 nell'art.  30  della  legge  30 dicembre 1991, n. 413), sollevata, in    &#13;
 riferimento agli artt.  24, secondo comma, 53,  primo  comma  e  101,    &#13;
 primo   comma,   della  Costituzione,  dalla  commissione  tributaria    &#13;
 regionale di Milano con le ordinanze in epigrafe.                        &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 30 giugno 1998.                               &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Marini                             &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 9 luglio 1998.                            &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
