<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>294</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:294</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ZAGREBELSKY</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>10/07/2003</data_decisione>
    <data_deposito>04/08/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Gustavo ZAGREBELSKY;, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 163 e 151 del decreto legislativo 29 ottobre 1999, n. 490 (Testo unico delle disposizioni legislative in materia di beni culturali e ambientali, a norma dell'articolo 1 della legge 8 ottobre 1997, n. 352), promossi con due ordinanze emesse il 17 settembre 2002 dal Giudice per le indagini preliminari del Tribunale di Grosseto, iscritte ai nn. 8 e 170 del registro ordinanze 2003 e pubblicate nella Gazzetta Ufficiale della Repubblica Italiana nn. 4 e 14, prima serie speciale, dell'anno 2003. &#13;
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 2 luglio 2003 il Giudice relatore Giovanni Maria Flick.  &#13;
    Ritenuto che con due ordinanze di analogo tenore, emesse nell'ambito di distinti procedimenti penali, il Giudice per le indagini preliminari del Tribunale di Grosseto ha sollevato, in riferimento all'art. 76 della Costituzione, questione di legittimità costituzionale degli artt. 163 e 151 del decreto legislativo 29 ottobre 1999, n. 490 (Testo unico delle disposizioni legislative in materia di beni culturali e ambientali, a norma dell'articolo 1 della legge 8 ottobre 1997, n. 352), nella parte in cui non prevedono - per i proprietari, possessori o detentori a qualsiasi titolo di beni ambientali, inclusi negli elenchi approvati con decreti ministeriali adottati ai sensi dell'art. 4 della legge 29 giugno 1939, n. 1497 - l'obbligo di sottoporre alla regione i progetti delle opere di qualunque genere, che intendano eseguire, al fine di ottenere la preventiva autorizzazione; &#13;
    che il giudice a quo premette di doversi pronunciare sulla richiesta di emissione di decreto penale di condanna nei confronti di persone imputate del reato di cui all'art. 163 del d.lgs. n. 490 del 1999, per aver realizzato, in assenza della prescritta autorizzazione, opere in zona sottoposta a vincolo paesaggistico in forza di decreto ministeriale adottato ai sensi dell'art. 4 della legge n. 1497 del 1939; &#13;
    che la richiesta, ad avviso del rimettente, non potrebbe essere accolta allo stato, in quanto il fatto contestato non rientrerebbe tra quelli sanzionati dalla norma incriminatrice; &#13;
    che il d.lgs. n. 490 del 1999 - nel quadro dell'intervento di riunione e coordinamento delle disposizioni legislative vigenti in materia di beni culturali e ambientali - avrebbe infatti sostituito la disposizione incriminatrice di cui all'art. 1-sexies del decreto-legge 27 giugno 1985, n. 312, convertito, con modificazioni, nella legge 8 agosto 1985, n. 431, con altra (quella, appunto, dell'art. 163 del testo unico) che, descrivendo in termini più puntuali la condotta incriminata, lascerebbe tuttavia privi di protezione i vincoli paesaggistici imposti anteriormente con provvedimenti amministrativi ai sensi della legge n. 1497 del 1939; &#13;
    che l'art. 1-sexies del d.l. n. 312 del 1985 era stato costantemente interpretato dalla giurisprudenza di legittimità nel senso della rilevanza di tali vincoli, ai fini della configurabilità del reato: con una interpretazione resa possibile dal fatto che la norma reprimeva genericamente la violazione delle disposizioni del medesimo decreto-legge, le quali a loro volta recepivano, facendolo proprio, il regime del vincolo paesaggistico di cui alla citata legge n. 1497 del 1939; &#13;
    che, per contro, l'art. 163 del d.lgs. n. 490 del 1999 — nel punire con le pene previste dall'art. 20 della legge 28 febbraio 1985, n. 47, «chiunque, senza la prescritta autorizzazione o in difformità di essa, esegue lavori di qualsiasi genere su beni ambientali» — non consentirebbe analoga operazione ermeneutica; &#13;
    che le nozioni di «prescritta autorizzazione» e di «bene ambientale» - su cui si impernia l'odierna descrizione della condotta illecita - verrebbero infatti esplicitate dall'art. 151 del d.lgs. n. 490 del 1999: norma che fa obbligo ai proprietari, possessori o detentori a qualunque titolo di beni ambientali inclusi negli elenchi pubblicati a norma degli artt. 140 e 144, ovvero nelle categorie indicate dall'art. 146 del medesimo decreto legislativo, di sottoporre alla regione i progetti delle opere di qualunque genere che intendano eseguire, al fine di ottenere la preventiva autorizzazione; &#13;
    che nei richiamati artt. 140, 144 e 146 non v'è, peraltro, alcuna menzione dei beni sottoposti a protezione con decreti ministeriali emanati in base alla legge n. 1497 del 1939: l'art. 140 demanda infatti alle regioni la compilazione degli elenchi dei beni e delle località soggetti a tutela; l'art. 144 attribuisce al competente ministro il potere di integrare gli elenchi regionali; mentre l'art. 146 identifica i beni soggetti comunque a tutela per legge; &#13;
    che, di conseguenza, le condotte aggressive dei beni individuati dai decreti ministeriali in questione dovrebbero ritenersi depenalizzate; &#13;
    che sotto tale profilo, tuttavia, le norme impugnate si porrebbero in contrasto con l'art. 1 della legge di delegazione 8 ottobre 1997, n. 352; tale norma, infatti, stabiliva che il Governo, nella predisposizione del testo unico, potesse apportare alle disposizioni legislative vigenti in materia di beni culturali ed ambientali le sole «modificazioni necessarie per il loro coordinamento formale e sostanziale, nonché per assicurare il riordino e la semplificazione dei procedimenti»: prospettive, queste, cui non sarebbe in alcun modo riconducibile la censurata «erosione della tutela penale del paesaggio»; &#13;
    che in entrambi i giudizi di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che la questione sia dichiarata non fondata. &#13;
    Considerato che il Giudice per le indagini preliminari del Tribunale di Grosseto solleva, con due distinte ordinanze, questione di legittimità costituzionale, in riferimento all'art. 76 Cost., degli artt. 163 e 151 del d.lgs. 29 ottobre 1999, n. 490, lamentando che, in contrasto con le previsioni dell'art. 1 della legge di delegazione 8 ottobre 1997, n. 352, le norme impugnate avrebbero lasciato privi di tutela penale i vincoli paesaggistici anteriormente imposti ai sensi della legge n. 1497 del 1939; &#13;
    che le ordinanze di rimessione sono di identico contenuto e che, pertanto, i relativi giudizi vanno riuniti per essere definiti con un'unica decisione; &#13;
    che - a prescindere da ogni considerazione circa l'ammissibilità della questione sul piano dell'intervento, che si richiede a questa Corte, di una pronunzia additiva in malam partem in materia penale — è assorbente il rilievo che, nel ravvisare in via interpretativa l'estraneità dei vincoli paesaggistici dianzi indicati al perimetro applicativo della norma incriminatrice oggetto di censura, il rimettente trascura completamente il disposto dell'art. 160 del d.lgs. n. 490 del 1999; &#13;
    che tale disposizione, proprio al fine di evitare il «vuoto di tutela» denunciato dal giudice a quo, stabilisce espressamente che «gli elenchi compilati a norma della legge 29 giugno 1939, n. 1497» - al pari delle «notifiche di importante interesse pubblico delle bellezze naturali o panoramiche, eseguite in base alla legge 11 giugno 1922, n. 776» - sono «validi a tutti gli effetti» del titolo II del testo unico: titolo nel quale sono comprese entrambe le norme — precettiva (art. 151) e sanzionatoria (art. 163) — sottoposte a scrutinio; &#13;
    che la questione va dichiarata, pertanto, manifestamente infondata, in quanto basata su di un erroneo presupposto interpretativo. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 163 e 151 del decreto legislativo 29 ottobre 1999, n. 490 (Testo unico delle disposizioni legislative in materia di beni culturali e ambientali, a norma dell'articolo 1 della legge 8 ottobre 1997, n. 352), sollevata, in riferimento all'art. 76 della Costituzione, dal Giudice per le indagini preliminari del Tribunale di Grosseto con le ordinanze indicate in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 luglio 2003. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Giovanni Maria FLICK, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 4 agosto 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
