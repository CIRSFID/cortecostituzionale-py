<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>37</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:37</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/01/1986</data_decisione>
    <data_deposito>07/02/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - &#13;
 Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - &#13;
 Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO &#13;
 GRECO Prof. RENATO DELL'ANDRO - Prof. GABRIELE PESCATORE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 51 del codice  &#13;
 di procedura civile promosso con ordinanza emessa il 21  novembre  1979  &#13;
 dal  Pretore  di  Brescia nel procedimento civile vertente tra Affronto  &#13;
 Maria Antonietta  ed  altri  e  la  Cassa  Nazionale  di  previdenza  e  &#13;
 assistenza  geometri  iscritta  al  n.  16  del registro ordinanze 80 e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica  n.  78  dell'anno  &#13;
 1980.                                                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del  22  gennaio  1986  il  Giudice  &#13;
 relatore Virgilio Andrioli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con  ordinanza  resa il 21 novembre 1979 (notificata il 28 e  &#13;
 comunicata il 6 dicembre successivi; pubblicata nella G. U. n.  78  del  &#13;
 19  marzo  1980  e iscritta al n. 16 R.O. 1980) nel procedimento civile  &#13;
 tra Affronto Maria Antonietta e altri e Cassa Nazionale di previdenza e  &#13;
 assistenza geometri, l'adito Pretore del lavoro di Brescia ha sollevato  &#13;
 d'ufficio questione di legittimità costituzionale dell'art. 51  c.p.c.  &#13;
 nella  parte  in  cui da un lato impone al giudice obbligo di astenersi  &#13;
 quando il proprio coniuge sia parente fino al quarto grado di una delle  &#13;
 parti o di alcuno dei difensori, dall'altro non prevede astensione  del  &#13;
 giudice  che  delle parti o di alcuno dei difensori sia affine in grado  &#13;
 superiore al  primo,  quando  la  affinità  sia  acquisita  attraverso  &#13;
 fratelli o sorelle, per contrasto con l'art. 3 comma terzo Cost..        &#13;
     2.1.  -  Avanti la Corte nessuna delle parti del giudizio di merito  &#13;
 si è costituita; ha spiegato intervento il  Presidente  del  Consiglio  &#13;
 dei  ministri  con  atto  depositato  il  4  aprile  1980, con il quale  &#13;
 l'Avvocatura generale dello Stato ha  argomentato  e  concluso  per  la  &#13;
 infondatezza della proposta questione.                                   &#13;
     2.2. - La trattazione dell'incidente è stata assegnata ad adunanza  &#13;
 della  Corte  in  camera  di  consiglio,  dapprima sotto la data del 20  &#13;
 novembre 1985 (rel. Paladin) e poi sotto la data del  22  gennaio  1986  &#13;
 (rel. Andrioli).<diritto>Considerato in diritto</diritto>:                          &#13;
     3.1.  -  Si  apprende dalla ordinanza di rimessione che "il caso di  &#13;
 specie è caratterizzato dalla astensione attuata ai sensi del n. 2 del  &#13;
 primo comma dell'art. 51 c.p.c. da un  magistrato  moglie  di  soggetto  &#13;
 cugino, e quindi parente di quarto grado, del difensore di una parte; e  &#13;
 dalla assegnazione del processo all'altro giudice della sezione lavoro,  &#13;
 sorella della moglie del difensore dell'altra parte, che non ritiene di  &#13;
 ricorrere  al capo dell'ufficio ai sensi del secondo comma dello stesso  &#13;
 art. 51 in quanto, in  altre  occasioni  analoghe  presentatesi  quando  &#13;
 nell'ufficio  vi  era  un  solo  giudice  del  lavoro,  la richiesta di  &#13;
 autorizzazione ad astenersi è stata respinta con  la  motivazione  che  &#13;
 nel  rapporto  di  affinità  non  erano  ravvisabili  i "gravi motivi"  &#13;
 previsti dalla norma citata e che comunque non vi era altro giudice cui  &#13;
 affidare la causa".                                                      &#13;
     Ha proseguito il Pretore di Brescia  con  rilevare  che  a  seguito  &#13;
 della  presenza  di donne magistrato deve nell'art. 51 interpretarsi la  &#13;
 parola "moglie", contenuta nel n. 2 del primo  comma,  come  "coniuge",  &#13;
 che  "La  norma, che considerava esclusivamente i rapporti di affinità  &#13;
 acquisiti al magistrato attraverso la moglie e non attraverso  fratelli  &#13;
 e  sorelle,  corrispondeva ad una concezione della famiglia del giudice  &#13;
 caratterizzata  dalla  presenza  di  consorte   casalinga   dedita   ad  &#13;
 intrallazzi,  opprimente,  portata  ad  imporre  al  marito  la propria  &#13;
 famiglia e addirittura la protezione degli interessi dei propri parenti  &#13;
 in qualche modo coinvolti in processi sui quali  dovesse  giudicare  il  &#13;
 marito suo succube, altrimenti non propenso a subire interferenze nella  &#13;
 propria funzione quando da affini per parte propria provenissero" "che,  &#13;
 in  ogni caso, la presenza di donne magistrato e la conseguente lettura  &#13;
 della  parola  "moglie"  come  "coniuge"  rende  assurdo   il   diverso  &#13;
 trattamento  riservato  agli  affini diversi per acquisizione dall'art.  &#13;
 51, giustificabile solo a costo di estendere analogicamente  al  marito  &#13;
 della  donna  giudice  quella  propensione  alla imposizione dei propri  &#13;
 familiari  e  alla  invadenza  che  il  legislatore  di  un  tempo   ha  &#13;
 considerato caratteristica essenziale della moglie".                     &#13;
     3.2.  -  Conclusione  della  riprodotta  motivazione  e ad un tempo  &#13;
 motivazione della proposta questione di costituzionalità: "Si  ravvisa  &#13;
 in   tale  situazione  ingiustificato  disparità  di  trattamento,  in  &#13;
 contrasto con l'art. 3 della Costituzione,  di  specie  eguali,  eguale  &#13;
 dovendosi  intendere  la  posizione di un magistrato rispetto ai propri  &#13;
 affini in secondo  grado,  comunque  acquisiti,  posizione  considerata  &#13;
 invece  in  modo  diverso  dal  legislatore,  che  impone astensione al  &#13;
 giudice non solo nel caso in cui  parte  o  difensore  sia  fratello  o  &#13;
 sorella  del  coniuge,  ma  addirittura  quando  essi siano del coniuge  &#13;
 parenti fino al quarto grado, e nulla prevede per il caso di  affinità  &#13;
 in secondo grado acquisita attraverso il proprio fratello o sorella".    &#13;
     Tale  infine la valutazione di rilevanza: "La questione proposta è  &#13;
 rilevante ai fini della decisione dipendendo da essa la possibilità di  &#13;
 astensione di questo giudice, nonché la possibilità di  decisione  da  &#13;
 parte  del  primo magistrato cui la causa era stata assegnata una prima  &#13;
 volta,  e  sia  assegnata  una  seconda  in  conseguenza  di  eventuale  &#13;
 pronuncia  che,  al fine di attuare identica disciplina di casi eguali,  &#13;
 limiti l'obbligo di astenersi in cause le cui parti o  difensori  siano  &#13;
 parenti del coniuge".                                                    &#13;
     4.  -  La questione sottoposta all'esame di questa Corte scaturisce  &#13;
 dalla abrogazione dell'art. 8  n.  1  dell'Ordinamento  giudiziario  30  &#13;
 gennaio  1941,  n.  12  -  il  quale prescriveva che per l'ammissione a  &#13;
 funzioni giudiziarie è necessario essere (non solo cittadini  italiani  &#13;
 ma  anche) di sesso maschile - sancita dall'art. 11. 9 febbraio 1963 n.  &#13;
 66, che aprl' alla donna l'accesso a tutte  le  cariche  e  professioni  &#13;
 pubbliche  ivi  compresa  la magistratura. Abrogazione che, se esige di  &#13;
 leggere nell'art. 51 n. 2 "coniuge" in luogo di "moglie", consiglia  di  &#13;
 identificare  le  situazioni  di affinità rispetto alla donna (ed allo  &#13;
 stesso uomo) magistrato che, per rispetto all'art. 3 comma primo Cost.,  &#13;
 giustificano obbligo di astensione a carico del magistrato  che  ne  è  &#13;
 fatto segno.                                                             &#13;
     Senonché il compito non lieve di colmare la lacuna non può essere  &#13;
 espletato   da   questa   Corte   perché   comporta   valutazioni   di  &#13;
 discrezionalità riservate al legislatore.  Intervento del  legislatore  &#13;
 che la Corte, nel dichiarare l'inammissibilità dell'incidente, auspica  &#13;
 perché  l'art.  51 comma secondo c.p.c., che il Pretore di Brescia non  &#13;
 ha  comunque  sospettato  d'incostituzionalità,  non  è  idoneo  alla  &#13;
 bisogna.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 51 comma primo n. 2 c.p.c., sollevata in riferimento all'art.  &#13;
 3 comma primo Cost., con ordinanza 21 novembre  1979  del  Pretore  del  &#13;
 lavoro di Brescia (n. 16 R.O. 1980).                                     &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 23 gennaio 1986.                              &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO -  &#13;
                                   GABRIELE PESCATORE.                    &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
