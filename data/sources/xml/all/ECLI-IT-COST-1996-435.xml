<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>435</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:435</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1996</data_decisione>
    <data_deposito>30/12/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 438 del  codice    &#13;
 di procedura penale, promosso con ordinanza emessa il 6 febbraio 1996    &#13;
 dalla  Corte d'appello di Trieste nel procedimento penale a carico di    &#13;
 Vitiello Raffaele, iscritta al n. 330 del registro ordinanze  1996  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 16, prima    &#13;
 serie speciale, dell'anno 1996;                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di consiglio dell'11 dicembre 1996 il giudice    &#13;
 relatore Gustavo Zagrebelsky;                                            &#13;
   Ritenuto che la Corte d'appello di Trieste,  con  ordinanza  del  6    &#13;
 febbraio  1996, ha sollevato questione di legittimità costituzionale    &#13;
 dell'art. 438 del codice di  procedura  penale  nella  parte  in  cui    &#13;
 subordina al consenso del pubblico ministero l'esperibilità del rito    &#13;
 abbreviato  richiesto dall'imputato, in riferimento agli artt. 3, 24,    &#13;
 25, 27 e 101 della Costituzione;                                         &#13;
     che, recependo un'eccezione formulata dalla difesa dell'imputato,    &#13;
 il giudice a quo deduce che  la  scelta  del  pubblico  ministero  di    &#13;
 indirizzare  le  indagini  in  maniera  più  o  meno approfondita è    &#13;
 suscettibile  di  determinare  l'accoglimento  o  il  rigetto   della    &#13;
 richiesta dell'imputato di essere ammesso al giudizio abbreviato;        &#13;
     che  nel  giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  che, sul rilievo del difetto di motivazione dell'ordinanza di    &#13;
 rimessione, ha richiesto una declaratoria di  inammissibilità  della    &#13;
 questione sollevata;                                                     &#13;
   Considerato  che  nell'ordinanza  di rinvio il giudice a quo non ha    &#13;
 descritto la concreta fattispecie sottoposta al suo giudizio  né  ha    &#13;
 fornito  alcuna  motivazione  in ordine alle ragioni che lo portano a    &#13;
 dubitare della legittimità costituzionale  della  norma  denunciata,    &#13;
 limitandosi   a  enunciare  le  disposizioni  costituzionali  che  si    &#13;
 assumono violate;                                                        &#13;
     che tale lacuna argomentativa impedisce di valutare la  rilevanza    &#13;
 e  i  termini  e  profili  della  questione  sollevata, tanto più in    &#13;
 ragione della incerta individuazione  dell'obiettivo  della  censura,    &#13;
 fra  il  consenso  del  pubblico  ministero allo svolgimento del rito    &#13;
 alternativo - nel  dispositivo  -  e  il  presupposto,  collegato  al    &#13;
 precedente  ma  distinto  da questo, della definibilità del giudizio    &#13;
 allo stato degli  atti  (in  dipendenza  del  materiale  di  indagine    &#13;
 raccolto   dall'accusa)   -   nella  motivazione  ,  ond'è  che,  in    &#13;
 accoglimento del rilievo formulato dall'Avvocatura  dello  Stato,  la    &#13;
 questione    sollevata    deve   essere   dichiarata   manifestamente    &#13;
 inammissibile, per  difetto  di  motivazione  dell'ordinanza  che  la    &#13;
 propone  (tra  molte,  ordinanza n. 229 del 1996; sentenza n.  79 del    &#13;
 1996);                                                                   &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 438  del  codice  di  procedura    &#13;
 penale,  sollevata,  in  riferimento  agli  artt. 3, 24, 25, 27 e 101    &#13;
 della Costituzione, dalla Corte d'appello di Trieste, con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 12 dicembre 1996.                             &#13;
                         Il Presidente: Granata                           &#13;
                       Il redattore: Zagrebelsky                          &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 30 dicembre 1996.                         &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
