<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>9</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:9</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/01/1999</data_decisione>
    <data_deposito>21/01/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  39,  primo    &#13;
 comma,  della  legge 11 febbraio 1971, n. 50 (Norme sulla navigazione    &#13;
 da diporto) - come sostituito dall'art. 22, primo comma, della  legge    &#13;
 26 aprile 1986, n. 193, - promosso con ordinanza emessa il 4 febbraio    &#13;
 1998  dal  giudice  per  le indagini preliminari presso la pretura di    &#13;
 Venezia nel procedimento penale a carico di Scasso  Andrea,  iscritta    &#13;
 al  n.    231 del registro ordinanze 1998 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 15,  prima  serie  speciale,  dell'anno    &#13;
 1998;                                                                    &#13;
   Udito  nella  camera  di  consiglio  del 14 ottobre 1998 il giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
   Ritenuto che, nel corso di un procedimento penale nei confronti  di    &#13;
 Andrea  Scasso,  imputato  del reato di cui all'art. 39, primo comma,    &#13;
 della legge 11 febbraio 1971,  n.  50  (Norme  sulla  navigazione  da    &#13;
 diporto)  - come sostituito dall'art. 22, primo comma, della legge 26    &#13;
 aprile 1986,  n.  193,  -  per  aver  condotto,  il  9  aprile  1997,    &#13;
 un'imbarcazione  con  motore fuoribordo da 78 cavalli senza essere in    &#13;
 possesso  della  prescritta  abilitazione,  il   pubblico   ministero    &#13;
 richiedeva  al  giudice per le indagini preliminari presso la pretura    &#13;
 di  Venezia  l'emissione  di  un decreto penale di condanna alla pena    &#13;
 dell'ammenda di lire 1.000.000;                                          &#13;
     che il giudice per le indagini preliminari  considerava  pacifica    &#13;
 la  materialità  della  condotta  ascritta  all'imputato ed indubbia    &#13;
 l'applicazione  al  caso  di  specie  della  normativa  invocata  dal    &#13;
 pubblico  ministero,  non  risultando  che  la navigazione intrapresa    &#13;
 dallo Scasso avesse scopi lucrativi;                                     &#13;
     che lo stesso giudice per le indagini preliminari, con  ordinanza    &#13;
 emessa  il  4  febbraio  1998,  sollevava  questione  di legittimità    &#13;
 costituzionale dell'art. 22 della legge 26 aprile 1986, n.  193  (che    &#13;
 ha  sostituito  il  predetto art. 39 della legge 11 febbraio 1971, n.    &#13;
 50), in riferimento all'art. 3 della  Costituzione,  in  quanto  esso    &#13;
 stabilisce  un  trattamento sanzionatorio deteriore rispetto a quello    &#13;
 previsto per l'ipotesi di conduzione, per scopi  lucrativi  (al  fine    &#13;
 del trasporto di persone per conto terzi), di un'imbarcazione addetta    &#13;
 alla  navigazione  interna  senza il prescritto titolo professionale,    &#13;
 punita - secondo il prevalente orientamento della Corte di cassazione    &#13;
 - ai sensi dell'art. 1231  del  codice  della  navigazione,  con  una    &#13;
 ingiustificata  disparità  di trattamento fra due situazioni, di cui    &#13;
 in realtà sarebbe eventualmente  la  seconda  a  dover  essere  più    &#13;
 gravemente sanzionata rispetto alla prima, giacché nel primo caso si    &#13;
 tratta di navigazione diportistica, mentre nel secondo di navigazione    &#13;
 professionale;                                                           &#13;
     che  nel  giudizio  avanti  la  Corte  costituzionale  non  si è    &#13;
 costituita la parte privata, né è intervenuto il    Presidente  del    &#13;
 Consiglio dei Ministri;                                                  &#13;
   Considerato  che  identica questione di legittimità costituzionale    &#13;
 è già stata rimessa a  questa  Corte  e  dichiarata  manifestamente    &#13;
 infondata,   con  ordinanza  n.  297  del  1998,  in  quanto  le  due    &#13;
 fattispecie indicate dal giudice a quo  non  sono  omogenee,  essendo    &#13;
 diversi:  a)  lo  scopo  dell'attività,  in  un  caso diportistico e    &#13;
 nell'altro lucrativo; b) la tipologia della navigazione  -  che  può    &#13;
 riflettersi  anche  su  quella  dell'imbarcazione  -  in  un caso non    &#13;
 professionale  e  nell'altro  professionale;  c)  il  titolo  la  cui    &#13;
 mancanza è sanzionata, in un caso consistente nell'abilitazione alla    &#13;
 guida e nell'altro in un titolo professionale;                           &#13;
     che,  pertanto,  non  potendo  il  tertium comparationis indicato    &#13;
 nell'ordinanza di rimessione essere raffrontato  in  modo  pertinente    &#13;
 con  la  norma  impugnata,  la  disciplina sanzionatoria stabilita da    &#13;
 quest'ultima, di per sé, non risulta irragionevole;                     &#13;
     che non sono stati addotti motivi nuovi  e  diversi  che  possano    &#13;
 indurre questa Corte a modificare il  proprio orientamento.              &#13;
   Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme  integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 39, primo comma,  della  legge  11  febbraio    &#13;
 1971,  n.  50  (Norme  sulla navigazione da diporto), come sostituito    &#13;
 dall'art. 22 della legge  26  aprile  1986,  n.  193,  sollevata,  in    &#13;
 riferimento  all'art.  3  della  Costituzione,  dal  giudice  per  le    &#13;
 indagini preliminari presso la  pretura di Venezia,  con  l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 gennaio 1999.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 21 gennaio 1999.                          &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
