<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>452</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:452</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/10/2000</data_decisione>
    <data_deposito>31/10/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI &#13;
 Giudici, Fernando SANTOSUOSSO, Massimo VARI, Cesare RUPERTO, Riccardo &#13;
 CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, &#13;
 Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, &#13;
 Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 4 della legge    &#13;
 della  Regione  Lazio 5 luglio 1994, n. 30 (Disciplina delle sanzioni    &#13;
 amministrative  di  competenza  regionale),  promosso  con  ordinanza    &#13;
 emessa  il  12 ottobre 1998 dal pretore di Latina, sezione distaccata    &#13;
 di  Minturno,  nel  procedimento  vertente  tra Treglia Giovanni e il    &#13;
 comune  di Minturno, iscritta al n. 876 del registro ordinanze 1998 e    &#13;
 pubblicata  nella Gazzetta Ufficiale della Repubblica n. 50, 1ª serie    &#13;
 speciale, dell'anno 1998.                                                &#13;
     Udito  nella camera di consiglio del 27 settembre 2000 il giudice    &#13;
 relatore Massimo Vari.                                                   &#13;
     Ritenuto   che  il  pretore  di  Latina,  sezione  distaccata  di    &#13;
 Minturno,  con  ordinanza del 12 ottobre 1998 (r.o. n. 876 del 1998),    &#13;
 ha  sollevato  questione  di  legittimità costituzionale dell'art. 4    &#13;
 della  legge  della  Regione  Lazio  5 luglio 1994, n. 30 (Disciplina    &#13;
 delle  sanzioni amministrative di competenza regionale), "laddove, in    &#13;
 ipotesi  di  violazione  amministrativa  sanzionata  nel solo massimo    &#13;
 edittale,  non  consente  all'interessato  di  accedere all'oblazione    &#13;
 corrispondendo, secondo la previsione di cui all'art. 16 legge n. 689    &#13;
 del  1981,  anche il doppio del minimo edittale, ricavato, secondo il    &#13;
 diritto  vivente,  alla  stregua del disposto di cui all'art. 26 cod.    &#13;
 pen.";                                                                   &#13;
         che  l'ordinanza  è stata emessa nel corso di un giudizio di    &#13;
 opposizione avverso due ordinanze-ingiunzioni del Sindaco di Minturno    &#13;
 (n. 999   del   1996  e  n. 1000  del  1996),  recanti  "la  sanzione    &#13;
 amministrativa per la violazione, rispettivamente, dell'art. 2, comma    &#13;
 1,  della  legge  29 marzo  1928,  n. 858  (Disposizioni per la lotta    &#13;
 contro  le  mosche),  e  dell'art. 42,  secondo  comma,  della  legge    &#13;
 30 aprile 1962, n. 283" (Modifica degli articoli 242, 243, 247, 250 e    &#13;
 262  del  testo  unico  delle  leggi  sanitarie,  approvato con regio    &#13;
 decreto 27 luglio 1934, n. 1265: Disciplina igienica della produzione    &#13;
 e della vendita delle sostanze alimentari e delle bevande);              &#13;
         che  il rimettente - richiamando l'interpretazione data dalla    &#13;
 Cassazione   all'art. 16   della   legge   24 novembre  1981,  n. 689    &#13;
 (Modifiche  al  sistema  penale),  nel senso che, in caso di sanzioni    &#13;
 amministrative  depenalizzate,  l'importo  minimo per il pagamento in    &#13;
 misura ridotta può essere desunto, in via generale, dall'art. 26 del    &#13;
 codice  penale,  che  indica,  per l'ammenda, la somma minima di lire    &#13;
 quattromila  -  censura  il  mancato rispetto dei "principi generali"    &#13;
 dettati dalla menzionata disposizione della legge n. 689 del 1981, da    &#13;
 ritenere  idonei a vincolare il legislatore regionale, secondo quanto    &#13;
 risulta  anche dalle sentenze con le quali questa Corte ha già avuto    &#13;
 modo  di  dichiarare  l'illegittimità costituzionale di disposizioni    &#13;
 legislative di altre regioni analoghe a quella ora in esame (sentenze    &#13;
 n. 187 del 1996 e n. 152 del 1995).                                      &#13;
     Considerato  che  il  quadro  di  riferimento normativo in cui si    &#13;
 colloca  la  questione  che  l'ordinanza  intende prospettare risulta    &#13;
 tutt'altro  che  chiaro  e  puntuale,  a  causa  di  inesattezze  nei    &#13;
 riferimenti  alle norme la cui violazione avrebbe dato, nella specie,    &#13;
 luogo  all'applicazione,  da parte dell'autorità amministrativa, del    &#13;
 censurato art. 4 della legge regionale n. 30 del 1994;                   &#13;
         che,  in particolare, l'art. 2, comma 1, della legge 29 marzo    &#13;
 1928, n. 858, non ha il contenuto prescrittivo e sanzionatorio che il    &#13;
 rimettente mostra di attribuirgli;                                       &#13;
         che  del  pari  erroneo  si appalesa il richiamo, operato dal    &#13;
 rimettente,  ad  un  inesistente  art. 42 della legge 30 aprile 1962,    &#13;
 n. 283;                                                                  &#13;
         che,  come questa Corte ha già avuto occasione di affermare,    &#13;
 ogni  questione  di  legittimità  costituzionale  deve,  a  pena  di    &#13;
 inammissibilità,  essere  definita nei suoi termini precisi, al fine    &#13;
 di  rendere  possibile  l'inequivoca determinazione dell'oggetto e la    &#13;
 verifica del requisito della rilevanza (sentenza n. 317 del 1992);       &#13;
         che,  a  parte  ciò,  il  rimettente  non ha considerato che    &#13;
 l'art. 16  della  legge  n. 689  del  1981,  è stato successivamente    &#13;
 modificato  dall'art. 52  del  decreto  legislativo  24 giugno  1998,    &#13;
 n. 213  (Disposizioni  per  l'introduzione dell'Euro nell'ordinamento    &#13;
 nazionale,  a  norma  dell'art. 1,  comma  1, della legge 17 dicembre    &#13;
 1997, n. 433);                                                           &#13;
         che,   pertanto,  il  rimettente  stesso  -  tralasciando  di    &#13;
 formulare  qualsiasi valutazione in merito all'influenza che potrebbe    &#13;
 avere   sulla  definizione  del  giudizio  principale  questa  ultima    &#13;
 disposizione   -  non  ha  assolto,  nemmeno  sotto  questo  profilo,    &#13;
 all'obbligo di dare congrua ed esauriente motivazione, sulla base del    &#13;
 complessivo  quadro  normativo  vigente  in  materia,  in ordine alla    &#13;
 rilevanza della prospettata questione;                                   &#13;
         che  tale  carente  ponderazione, non colmabile attraverso un    &#13;
 riscontro  interpretativo  da  parte  di  questa  Corte, costituisce,    &#13;
 dunque,  un  ulteriore  motivo  di  manifesta  inammissibilità della    &#13;
 questione stessa (v. ordinanza n. 86 del 2000).                          &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale  dell'art. 4  della  legge della Regione    &#13;
 Lazio  5 luglio 1994, n. 30 (Disciplina delle sanzioni amministrative    &#13;
 di  competenza  regionale),  sollevata dal pretore di Latina, sezione    &#13;
 distaccata di Minturno, con l'ordinanza indicata in epigrafe.            &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 ottobre 2000.                              &#13;
                       Il Presidente: Mirabelli                           &#13;
                          Il redattore: Vari                              &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 31 ottobre 2000.                        &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
