<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2012</anno_pronuncia>
    <numero_pronuncia>305</numero_pronuncia>
    <ecli>ECLI:IT:COST:2012:305</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>QUARANTA</presidente>
    <relatore_pronuncia>Sabino Cassese</relatore_pronuncia>
    <redattore_pronuncia>Sabino Cassese</redattore_pronuncia>
    <data_decisione>11/12/2012</data_decisione>
    <data_deposito>19/12/2012</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>cessata materia del contendere</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 3, comma 5, della delibera legislativa dell'Assemblea regionale siciliana nella seduta del 30 luglio 2012 (disegno di legge n. 184-354), recante « Istituzione della Commissione regionale per la promozione di condizioni di pari opportunità tra uomo e donna nella Regione», promosso dal Commissario dello Stato per la Regione siciliana con ricorso notificato il 7 agosto 2012, depositato in cancelleria il 14 agosto 2012 ed iscritto al n. 112 del registro ricorsi 2012.&#13;
 Udito nella camera di consiglio del 20 novembre 2012 il Giudice relatore Sabino Cassese.</epigrafe>
    <testo>Ritenuto che il Commissario dello Stato per la Regione siciliana, con ricorso in via principale ritualmente notificato e depositato (reg. ric. n. 112 del 2012), ha proposto questione di legittimità costituzionale dell'articolo 3, comma 5, della delibera legislativa del 30 luglio 2012 dell'Assemblea regionale siciliana, con la quale la stessa ha approvato il disegno di legge n. 184-354 (Istituzione della Commissione regionale per la promozione di condizioni di pari opportunità tra uomo e donna nella Regione), per violazione degli articoli 3 e 97 della Costituzione;&#13;
 che l'art. 3 della delibera legislativa del 30 luglio 2012, disciplinante la composizione e la durata della Commissione regionale per la promozione di condizioni di pari opportunità tra uomo e donna nella Regione, stabilisce, al comma 5, che «Le componenti della Commissione restano in carica fino alla scadenza della legislatura regionale in cui sono state elette; esse continuano, tuttavia, a svolgere le loro funzioni fino al rinnovo della Commissione»;&#13;
 che, ad avviso del Commissario dello Stato, la disposizione impugnata, nel disporre che le suddette componenti «continuano, tuttavia, a svolgere le loro funzioni fino al rinnovo della Commissione», rimette al Presidente della Regione, titolare del potere di nomina, la concreta determinazione della durata in carica dei membri della Commissione, così violando la riserva di legge in materia di organizzazione amministrativa, nonché i principi di imparzialità e buon andamento, stabiliti dall'art. 97 Cost.; &#13;
 che, secondo il ricorrente, la medesima disposizione, non motivando in rapporto a specifiche esigenze o situazioni della Regione l'opportunità della proroga della permanenza in carica oltre la scadenza del termine di durata previsto dalla legge, lederebbe altresì il principio di eguaglianza «rispetto ai membri degli altri comitati ed organi collegiali regionali cui è preclusa la possibilità di prorogatio»;&#13;
 che la Regione siciliana non si è costituita nel giudizio costituzionale;&#13;
 che, come rappresentato anche dal Commissario dello Stato per la Regione siciliana nella memoria depositata il 9 ottobre 2012, successivamente alla proposizione del ricorso, l'impugnata delibera legislativa è stata promulgata e pubblicata (nella Gazzetta Ufficiale della Regione siciliana del 28 settembre 2012, n. 41, supplemento ordinario n. 1 come legge della Regione siciliana 19 settembre 2012, n. 51 (Istituzione della Commissione regionale per la promozione di condizioni di pari opportunità tra uomo e donna nella Regione), con omissione della disposizione oggetto di censura;&#13;
 che, conseguentemente,  con atto  depositato  nella  cancelleria  di  questa  Corte il 29 ottobre 2012, l'Avvocatura generale dello Stato ha proposto istanza affinché sia dichiarata la cessazione della materia del contendere.&#13;
 Considerato che il Commissario dello Stato per la Regione siciliana ha proposto questione di legittimità costituzionale dell'art. 3, comma 5, della delibera legislativa del 30 luglio 2012 dell'Assemblea regionale siciliana, con la quale la stessa ha approvato il disegno di legge n. 184-354 (Istituzione della Commissione regionale per la promozione di condizioni di pari opportunità tra uomo e donna nella Regione), per violazione degli artt. 3 e 97 della Costituzione;&#13;
 che, successivamente alla proposizione del ricorso, la predetta delibera legislativa è stata promulgata e pubblicata come legge della Regione siciliana 19 settembre 2012, n. 51 (Istituzione della Commissione regionale per la promozione di condizioni di pari opportunità tra uomo e donna nella Regione), con omissione della disposizione oggetto di censura;&#13;
 che l'intervenuto esaurimento del potere promulgativo, che si esercita necessariamente in modo unitario e contestuale rispetto al testo deliberato dall'Assemblea regionale siciliana, preclude definitivamente la possibilità che le parti della legge impugnate e omesse in sede di promulgazione acquistino o esplichino una qualche efficacia, privando così di oggetto il giudizio di legittimità costituzionale (ex plurimis, ordinanze nn. 228 e 12 del 2012, nn. 57 e 2 del 2011, nn. 212, 155 e 74 del 2010, n. 186 del 2009, n. 304 del 2008, nn. 358 e 229 del 2007, n. 410 del 2006);&#13;
 che, pertanto, in conformità alla giurisprudenza di questa Corte, deve dichiararsi cessata la materia del contendere.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE &#13;
 dichiara cessata la materia del contendere in ordine al ricorso in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 dicembre 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Sabino CASSESE, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 19 dicembre 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
