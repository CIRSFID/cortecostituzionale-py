<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>461</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:461</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/12/1991</data_decisione>
    <data_deposito>13/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI; prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 1 e 2 della    &#13;
 legge 3 gennaio 1960, n. 5 (Riduzione del limite di età pensionabile    &#13;
 per i lavoratori  delle  miniere,  cave  e  torbiere),  promosso  con    &#13;
 ordinanza  emessa  il  28 marzo 1991 dal Pretore di Ascoli Piceno nel    &#13;
 procedimento civile vertente tra Mendicino  Vittorio  e  I.N.P.S.  ed    &#13;
 altra,  iscritta  al  n. 385 del registro ordinanze 1991 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  23,  prima   serie    &#13;
 speciale, dell'anno 1991;                                                &#13;
    Visti   gli   atti   di   costituzione  di  Mendicino  Vittorio  e    &#13;
 dell'I.N.P.S.  nonché  l'atto  di  intervento  del  Presidente   del    &#13;
 Consiglio dei ministri;                                                  &#13;
    Udito  nella  camera  di consiglio del 20 novembre 1991 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  nel  procedimento  civile  vertente  tra  Mendicino    &#13;
 Vittorio,  l'I.N.P.S.  ed  altro,  il  Pretore  di Ascoli Piceno, con    &#13;
 ordinanza del 28 marzo 1991 (R.O.  n.  385  del  1991)  ha  sollevato    &#13;
 questione  di  legittimità  costituzionale  degli  artt. 1 e 2 della    &#13;
 legge 3  gennaio  1960,  n.  5,  interpretata  nel  senso  della  sua    &#13;
 inapplicabilità  agli operai che lavorano alle dipendenze di imprese    &#13;
 che comunque esercitano lavoro nel sottosuolo;                           &#13;
      che, a parere del remittente, sarebbero violati gli art. 3 e  38    &#13;
 della Costituzione per la disparità di trattamento che si crea tra i    &#13;
 suddetti  e  coloro che lavorano in miniere, in cave e torbiere e per    &#13;
 la privazione di mezzi adeguati alle loro esigenze di  vita  che  gli    &#13;
 stessi subiscono;                                                        &#13;
      che si sono costituiti la parte privata e l'I.N.P.S.;               &#13;
      che  l'una  ha  chiesto una sentenza interpretativa o, comunque,    &#13;
 l'accoglimento della  questione;  e,  nella  memoria  successiva,  la    &#13;
 remissione  della  trattazione della questione alla pubblica udienza;    &#13;
 l'altro ha rilevato che quella impugnata è  una  normativa  speciale    &#13;
 insuscettibile  di  interpretazione  analogica  e  che  con  essa  il    &#13;
 legislatore,  nella  sua  discrezionalità,  ha  inteso  tutelare  un    &#13;
 rischio  particolare;  ed  ha  concluso  per la inammissibilità o la    &#13;
 infondatezza della questione;                                            &#13;
      che  è  intervenuta  l'Avvocatura  Generale  dello  Stato,   in    &#13;
 rappresentanza del Presidente del Consiglio dei ministri, la quale ha    &#13;
 rilevato  che  nella  ordinanza di remissione manca qualsiasi accenno    &#13;
 alla fattispecie e che non è stato effettuato né il giudizio  sulla    &#13;
 rilevanza   né   quello   sulla  non  manifesta  infondatezza  della    &#13;
 questione; ha concluso per la sua inammissibilità;                      &#13;
    Considerato che nella  ordinanza  di  remissione  manca  qualsiasi    &#13;
 elemento  di  fatto  per  cui  non è possibile desumere quale lavoro    &#13;
 abbia svolto  il  ricorrente,  in  particolare  se  esso  sia  stato,    &#13;
 continuamente  o meno, eseguito nel sottosuolo e comunque alla stessa    &#13;
 stregua del lavoro dei minatori o dei cavatori;                          &#13;
      che, quindi, in  tale  situazione  la  questione  va  dichiarata    &#13;
 manifestamente  inammissibile  senza che sia necessario la remissione    &#13;
 della trattazione della questione alla pubblica udienza;                 &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara   la   manifesta   inammissibilità  della  questione  di    &#13;
 legittimità costituzionale degli artt. 1 e 2 della legge  3  gennaio    &#13;
 1960 n. 5 (Riduzione del limite di età pensionabile per i lavoratori    &#13;
 delle  miniere,  cave  e  torbiere), in riferimento agli artt. 3 e 38    &#13;
 della Costituzione,  sollevata  dal  Pretore  di  Ascoli  Piceno  con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 dicembre 1991.                              &#13;
                       Il presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 13 dicembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
