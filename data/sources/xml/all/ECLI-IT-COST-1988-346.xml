<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>346</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:346</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1988</data_decisione>
    <data_deposito>24/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Dott. Francesco SAJA; &#13;
 Giudici: Prof. Giovanni CONSO, Prof. Ettore GALLO, Dott. Aldo &#13;
 CORASANITI, Prof. Giuseppe BORZELLINO, Dott. Francesco GRECO, Prof. &#13;
 Renato DELL'ANDRO, Prof. Gabriele PESCATORE, Avv. Ugo SPAGNOLI, Prof. &#13;
 Francesco Paolo CASAVOLA, Prof. Antonio BALDASSARRE, Prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, Prof. Luigi MENGONI, Prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 444 del codice    &#13;
 di procedura civile, promosso con ordinanza emessa il 3 novembre 1983    &#13;
 dal  Pretore di Modena, iscritta al n. 47 del registro ordinanze 1984    &#13;
 e  pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n.  120    &#13;
 dell'anno 1984;                                                          &#13;
    Visti  l'atto  di  costituzione  dell'I.N.P.S.  nonché  l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  il  Pretore  di  Modena,  con ordinanza emessa il 3    &#13;
 novembre 1983, ha sollevato questione di legittimità costituzionale,    &#13;
 in  riferimento  agli  artt.  25,  primo comma, 24, primo comma, e 3,    &#13;
 primo comma, Cost., dell'art. 444 cod. proc. civ. nella parte in  cui    &#13;
 non  prevede,  nell'ipotesi  in cui non sia applicabile il foro della    &#13;
 residenza dell'assicurato per non averla questi in Italia e la  causa    &#13;
 non  sia  compresa  tra  quelle di cui al secondo e terzo comma dello    &#13;
 stesso articolo (cause di infortunio e malattie  professionali  degli    &#13;
 addetti   alla  navigazione  marittima;  controversie  relative  agli    &#13;
 obblighi dei datori di lavoro e all'applicazione di sanzioni  per  il    &#13;
 relativo  inadempimento),  che sia competente il Pretore, in funzione    &#13;
 di giudice del lavoro, che ha sede nel capoluogo della circoscrizione    &#13;
 del  Tribunale nell'ambito della quale si trova la sede dell'ente cui    &#13;
 l'assicurato abbia  presentato  la  domanda  amministrativa,  ovvero,    &#13;
 nell'ipotesi  di  inapplicabilità  di  detto criterio, il Pretore in    &#13;
 funzione di giudice del  lavoro  che  ha  sede  nel  capoluogo  della    &#13;
 circoscrizione  del Tribunale al quale la domanda giudiziale è stata    &#13;
 proposta dall'assicurato o il Pretore del capoluogo  del  circondario    &#13;
 nel quale ha sede il giudice adito;                                      &#13;
      che  nel presente giudizio si è costituito l'I.N.P.S. rilevando    &#13;
 la infondatezza della questione e chiedendo, comunque, che  la  Corte    &#13;
 decida secondo giustizia;                                                &#13;
      che  ha  spiegato  intervento  il  Presidente  del Consiglio dei    &#13;
 ministri,  rappresentato  dall'Avvocatura   Generale   dello   Stato,    &#13;
 chiedendo  che  la  questione  venga dichiarata non fondata dovendosi    &#13;
 interpretare la  disposizione  impugnata  come  non  esclusiva  della    &#13;
 possibile   applicazione   delle   norme  generali  sulla  competenza    &#13;
 richiamate dall'art. 413 cod. proc. civ.,  a  sua  volta  applicabili    &#13;
 alle  controversie  previdenziali  in  virtù  del  richiamo  operato    &#13;
 dall'art. 442 cod. proc. civ.;                                           &#13;
      considerato  che  il  giudice  a  quo  espressamente  chiede una    &#13;
 sentenza manipolativa di accoglimento che  "introduca  nell'art.  444    &#13;
 cod.  proc.  civ.  una o più disposizioni sussidiarie ora mancanti",    &#13;
 indicando, peraltro, tre distinti criteri per la determinazione della    &#13;
 competenza territoriale nelle controversie previdenziali nell'ipotesi    &#13;
 in cui non sia applicabile quello  del  foro  della  residenza  dello    &#13;
 assicurato;                                                              &#13;
      che,  nei  termini  in cui è stata prospettata, la questione si    &#13;
 appalesa manifestamente  inammissibile  in  quanto  non  rientra  nei    &#13;
 poteri  della  Corte  la  scelta  tra  l'uno  o l'altro dei formulati    &#13;
 criteri di determinazione delle competenze territoriali, non  avendo,    &#13;
 tra  l'altro,  il  giudice  a  quo  indicato  quale dei detti criteri    &#13;
 sussidiari fosse idoneo a realizzare la soluzione  più  adeguata  ai    &#13;
 parametri invocati;                                                      &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 444 cod. proc. civ., sollevata,    &#13;
 in  riferimento  agli  artt.  25,  primo comma, 24, primo comma, e 3,    &#13;
 primo  comma,  Cost.,  dal  Pretore  di  Modena  con  l'ordinanza  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte Costituzionale, Palazzo della Consulta, l'11 marzo 1988.           &#13;
                          Il presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 24 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
