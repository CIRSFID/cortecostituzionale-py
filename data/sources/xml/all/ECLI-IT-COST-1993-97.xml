<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>97</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:97</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1993</data_decisione>
    <data_deposito>15/03/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Antonio BALDASSARRE, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  671,  secondo    &#13;
 comma,  del  d.P.R.  9  aprile  1959,  n. 128 (Norme di polizia delle    &#13;
 miniere e delle cave), promosso con ordinanza emessa il 7 maggio 1992    &#13;
 dal Pretore di Grosseto - Sezione distaccata di Massa  Marittima  nel    &#13;
 procedimento  penale  a  carico  di  Dalle  Cort  Marcello  ed altri,    &#13;
 iscritta al n. 537 del registro ordinanze  1992  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  41, prima serie speciale,    &#13;
 dell'anno 1992;                                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 febbraio 1993 il Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto che, nel corso di un  procedimento  penale  a  carico  di    &#13;
 Marcello Dalle Cort ed altri per violazione degli artt. 158 e 167 del    &#13;
 d.P.R.  9 aprile 1959, n. 128 (Norme di polizia delle miniere e delle    &#13;
 cave),  il  Pretore  di  Grosseto  -  Sezione  distaccata  di   Massa    &#13;
 Marittima,  con  ordinanza  del  7  maggio  1992,  ha  sollevato,  in    &#13;
 riferimento agli artt.  3  e  32  della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale dell'art. 671, secondo comma, del citato    &#13;
 d.P.R. n. 128 del 1959, nella parte in cui non opera una  distinzione    &#13;
 all'interno  degli  "altri  casi"  (diversi  da quelli specificamente    &#13;
 indicati nel primo comma) "tra le ipotesi  nelle  quali  la  condotta    &#13;
 omissiva,  con  riguardo a carenze nella predisposizione di specifici    &#13;
 accorgimenti tecnici, può essere sanata attraverso l'ottemperanza al    &#13;
 contenuto della diffida e quelle nelle  quali  la  condotta  omissiva    &#13;
 abbia oggettivamente integrato, nella sua completezza, una ipotesi di    &#13;
 reato";                                                                  &#13;
     che,  ad  avviso  del giudice remittente, nel caso di infrazioni,    &#13;
 quali quelle contestate nella specie, in  cui  la  condotta  omissiva    &#13;
 abbia  già  prodotto  completamente  tutti  gli  effetti  dannosi  o    &#13;
 pericolosi che giustificano la sanzione  penale,  "la  diffida  e  la    &#13;
 successiva ottemperanza non possono realizzare altro che una forma di    &#13;
 estinzione  del  reato", in contrasto con la ratio legis consistente,    &#13;
 come si desume dagli artt. 672, 673 e 674 del decreto,  nel  fine  di    &#13;
 ottimizzazione  delle condizioni di lavoro in cava e in miniera e non    &#13;
 di  sottrarre  alla  sanzione  penale  fattispecie   criminose   già    &#13;
 completamente attuate;                                                   &#13;
      che  pertanto  la  norma denunciata è ritenuta contraria sia al    &#13;
 principio di eguaglianza, "per l'evidente disparità  di  trattamento    &#13;
 con  identiche  situazioni  che  si  verificano  in settori di lavoro    &#13;
 diversi da quello esaminato", sia al principio di  tutela  della  salute;                                                                    &#13;
      che nel giudizio davanti alla Corte è intervenuto il Presidente    &#13;
 del  Consiglio  dei  ministri,  rappresentato  dall'Avvocatura  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata non fondata;            &#13;
    Considerato che gli illeciti previsti dal secondo comma  dell'art.    &#13;
 671  del  d.P.R.  n.  128  del 1959 sono configurati dalla legge come    &#13;
 reati  di  inosservanza  di  un  ordine  legittimo   della   pubblica    &#13;
 autorità,  di  guisa  che  la  fattispecie  penale  e il conseguente    &#13;
 obbligo   di   rapporto   all'autorità  giudiziaria  (art.  672)  si    &#13;
 concretano  soltanto  con  l'inadempimento,  debitamente  constatato,    &#13;
 della diffida intimata dall'ingegnere capo;                              &#13;
      che  si  tratta  di  una  scelta  discrezionale del legislatore,    &#13;
 fondata  su  una  valutazione  che  ragionevolmente  differenzia   la    &#13;
 disciplina  degli "altri casi" previsti dall'art. 671, secondo comma,    &#13;
 sia rispetto ai casi specificamente indicati  nel  primo  comma,  sia    &#13;
 rispetto  alle  infrazioni  previste  dall'art. 9 del d.P.R. 19 marzo    &#13;
 1955, n. 520, in ordine alle quali la diffida è  una  mera  facoltà    &#13;
 dell'Ispettorato  del lavoro, non, in quanto inadempiuta, un elemento    &#13;
 costitutivo del reato;                                                   &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87,  e  9  delle  Norme  integrative per i giudizi davanti alla Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  671,  secondo  comma,  del d.P.R. 9 aprile    &#13;
 1959,  n.  128  (Norme  di  polizia  delle  miniere  e  delle  cave),    &#13;
 sollevata,  in  riferimento agli artt. 3 e 32 della Costituzione, dal    &#13;
 Pretore di Grosseto -  Sezione  distaccata  di  Massa  Marittima  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1993.                                  &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 15 marzo 1993.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
