<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>293</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:293</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/2000</data_decisione>
    <data_deposito>17/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio di legittimità costituzionale dell'art. 15 della legge    &#13;
 8  febbraio  1948,  n. 47  (Disposizioni  sulla stampa), promosso con    &#13;
 ordinanza  emessa  il  17 febbraio 1999 dalla Corte di cassazione nel    &#13;
 procedimento  penale  a  carico  di  Corvi Luigi e altri, iscritta al    &#13;
 n. 275  del  registro  ordinanze  1999  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 20,  prima serie speciale, dell'anno    &#13;
 1999.                                                                    &#13;
     Visti  gli  atti  di  costituzione  di  Corvi  Luigi e di La Cava    &#13;
 Cristina  nonché  l'atto  di intervento del Presidente del Consiglio    &#13;
 dei Ministri;                                                            &#13;
     Udito nell'udienza pubblica del 4 aprile 2000 il giudice relatore    &#13;
 Francesco Guizzi;                                                        &#13;
     Uditi  gli avvocati Paola Balducci e Caterina Malavenda per Corvi    &#13;
 Luigi,  Franco  Coppi  e  Caterina  Malavenda  per La Cava Cristina e    &#13;
 l'Avvocato  dello Stato Paolo di Tarsia di Belmonte per il Presidente    &#13;
 del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - A  seguito della pubblicazione sul settimanale "Visto" delle    &#13;
 fotografie  scattate  dalla  polizia  giudiziaria  in occasione della    &#13;
 scoperta  del  cadavere  di A.F.dT. venivano incriminati C.V., L.C. e    &#13;
 M.M. per i delitti di ricettazione, pubblicazione di immagini coperte    &#13;
 da  segreto  e di fotografie impressionanti e raccapriccianti, atte a    &#13;
 turbare il comune sentimento della morale.                               &#13;
     Assolti  nei due gradi di merito dalle prime due imputazioni, gli    &#13;
 imputati  proponevano  ricorso  per  cassazione in ordine alla terza,    &#13;
 deducendo diversi motivi, fra i quali l'illegittimità costituzionale    &#13;
 della  norma  incriminatrice,  l'art. 15 della legge 8 febbraio 1948,    &#13;
 n. 47  (Disposizioni  sulla  stampa), sì che il Collegio giudicante,    &#13;
 aderendo   alla   eccezione,   sollevava  questione  di  legittimità    &#13;
 costituzionale   in   riferimento   agli   artt. 21,  25  e  3  della    &#13;
 Costituzione.                                                            &#13;
                                                                          &#13;
     2. - Osserva  il  rimettente  che  l'art. 15  della  legge  sulla    &#13;
 stampa,  richiamando  le  sanzioni stabilite dall'art. 528 del codice    &#13;
 penale,  punisce  come  reato  la fattispecie degli "stampati i quali    &#13;
 descrivano   o   illustrino,   con   particolari   impressionanti   o    &#13;
 raccapriccianti,  avvenimenti realmente verificatisi o anche soltanto    &#13;
 immaginari,  in  modo  da  poter  turbare  il comune sentimento della    &#13;
 morale  o  l'ordine  familiare o da poter provocare il diffondersi di    &#13;
 suicidi o delitti".                                                      &#13;
     Tre  sono  gli elementi della condotta previsti: la descrizione o    &#13;
 l'illustrazione  di avvenimenti, anche immaginari, su stampati; l'uso    &#13;
 di  particolari impressionanti o raccapriccianti; le modalità idonee    &#13;
 a  turbare  la  morale  corrente  o l'ordine delle famiglie, ovvero a    &#13;
 favorire il diffondersi dei suicidi o dei delitti.                       &#13;
     La  questione  sarebbe  rilevante  ai  fini  del  decidere  e non    &#13;
 manifestamente infondata.                                                &#13;
     La  genericità  e l'indeterminatezza della norma incriminatrice,    &#13;
 nella parte in cui utilizza il parametro del "comune sentimento della    &#13;
 morale"   quale  requisito  del  fatto,  violerebbe  l'art. 25  della    &#13;
 Costituzione.  La  condotta punibile - osserva il giudice a quo - non    &#13;
 dovrebbe  essere  rimessa  a  valutazioni soggettive, variabili e non    &#13;
 definibili    a   priori   ma   legata   a   previsioni   legislative    &#13;
 sufficientemente determinate. Significativamente, la Corte di appello    &#13;
 avrebbe convenuto con tale censura, almeno nella parte riguardante il    &#13;
 richiamo  al "comune sentimento della morale"; tuttavia ha creduto di    &#13;
 superare  il  problema,  proponendo  una lettura della incriminazione    &#13;
 tale  da  ovviare  alla  genericità  della previsione: la violazione    &#13;
 della  morale  comune verrebbe in considerazione solo quando essa sia    &#13;
 così marcata da destare la sensazione o il raccapriccio.                &#13;
     Il   Collegio  rimettente  ritiene  però  non  appagante  questa    &#13;
 interpretazione,  atteso  che  la  genericità  del  riferimento alla    &#13;
 morale, priva di oggettività giuridica, sarebbe verificabile proprio    &#13;
 in  base  allo  scarso  numero  di precedenti esistente, di contro al    &#13;
 profluvio  di immagini impressionanti o raccapriccianti che sarebbero    &#13;
 sotto i nostri occhi.                                                    &#13;
     Il  giudice  a quo ipotizza altresì la lesione dell'art. 3 della    &#13;
 Costituzione,  perché  -  rispetto  a  tutti  coloro  che diffondono    &#13;
 immagini  o  notizie  a  mezzo  stampa  -  verrebbero  assoggettati a    &#13;
 sanzione  soltanto  gli autori o i responsabili di immagini o notizie    &#13;
 ritenute impressionanti o raccapriccianti.                               &#13;
     Infine,   l'indebita   estensione   del   divieto  costituzionale    &#13;
 concernente  le  sole pubblicazioni contrarie al buon costume, fino a    &#13;
 ricomprendere  -  con  la  norma  incriminatrice  -  le pubblicazioni    &#13;
 contrarie  alla  morale comune, costituirebbe violazione dell'art 21,    &#13;
 sesto  comma,  della  Costituzione,  dal  momento che si introduce un    &#13;
 concetto   più   ampio   di   quello   vietato   dalla  disposizione    &#13;
 costituzionale,  quindi  restrittivo  della libertà ivi stabilita. E    &#13;
 anche  a  voler  ritenere  come  aventi  un  analogo contenuto le due    &#13;
 espressioni,  la  fattispecie  risulterebbe  comunque  indeterminata,    &#13;
 ricandendo nella prima delle doglianze.                                  &#13;
                                                                          &#13;
     3. - Si sono costituite le parti private chiedendo l'accoglimento    &#13;
 della questione.                                                         &#13;
     La  difesa  concorda con la censura del rimettente e, in ispecie,    &#13;
 con  quella  riguardante la violazione dei principi di tassatività e    &#13;
 determinatezza  della  fattispecie  penale, palesandosi assolutamente    &#13;
 vago  il  turbamento  della  "morale  comune"  quale  requisito della    &#13;
 condotta.  In  particolare  si  osserva  che  la  morale sarebbe cosa    &#13;
 diversa  rispetto  all'impressione  o al raccapriccio suscitati dalle    &#13;
 immagini  censurate. Attraverso la locuzione usata ("in modo da poter    &#13;
 turbare  il comune sentimento della morale o l'ordine familiare"), il    &#13;
 legislatore  avrebbe  inteso  porre  un  limite  alla  tutela penale,    &#13;
 stabilendo  che  non  ogni  immagine impressionante o raccapricciante    &#13;
 verrebbe  ad  assumere,  secondo  la  diversa  opinione  del  giudice    &#13;
 dell'appello,  una  rilevanza penale. Aderendo alle valutazioni della    &#13;
 Corte  di  cassazione,  la  difesa  delle parti private respinge tale    &#13;
 interpretazione  ("segno  di un chiaro disagio ermeneutico") e giunge    &#13;
 alla  conclusione  che  essa  si  risolverebbe  in  una interpretatio    &#13;
 abrogans  poiché  la  morale comune scadrebbe di rilievo e finirebbe    &#13;
 col  coincidere  con  un altro elemento della condotta. Al contrario,    &#13;
 costruita senza evento, essendo soddisfatta dal semplice pericolo, la    &#13;
 fattispecie  determinerebbe  l'impossibilità di restringere il campo    &#13;
 applicativo,   così   determinando   la   violazione   della  regola    &#13;
 costituzionale.                                                          &#13;
     Si  tratterebbe  di una situazione simile a quella già esaminata    &#13;
 da questa Corte nello scrutinio del delitto di plagio, conclusosi con    &#13;
 una  declaratoria di illegittimità della disposizione incriminatrice    &#13;
 per  essere  tale reato accertabile soltanto attraverso " i parametri    &#13;
 culturali  propri del giudicante" (sentenza n. 96 del 1981). È vero,    &#13;
 proseguono  le  difese,  che  per  le  questioni di costituzionalità    &#13;
 sollevate  in  riferimento  all'art. 25  della  Costituzione la Corte    &#13;
 costituzionale  ha dichiarato la non fondatezza di quelle concernenti    &#13;
 le  fattispecie  penali,  all'apparenza indeterminate, che consentono    &#13;
 però  una  interpretazione  univoca a seguito dell'individuazione di    &#13;
 principi  certi  e  determinati  da  parte  della  giurisprudenza  di    &#13;
 legittimità  (sentenze  nn. 31 del 1995 e 122 del 1993), ma nel caso    &#13;
 di  specie  ciò  non  sarebbe  possibile  per  l'esiguo numero delle    &#13;
 pronunce.    Né    una   lettura   della   disposizione   in   senso    &#13;
 costituzionalmente  adeguato  potrebbe dare certezza e definizione al    &#13;
 concetto    di    "morale   comune"   contenuto   nella   fattispecie    &#13;
 incriminatrice.                                                          &#13;
                                                                          &#13;
     4.  -  È  intervenuto  il Presidente del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso dall'Avvocatura dello Stato, che ha concluso    &#13;
 per  l'infondatezza  della  questione,  sostenendo  che  il ricorso a    &#13;
 locuzioni  proprie  del  linguaggio  e  dell'intelligenza  comuni  è    &#13;
 consentito  -  come  si  rileva dalla giurisprudenza costituzionale -    &#13;
 perché  spetterebbe  al  giudice  dare a esse un contenuto concreto.    &#13;
 Tale   compito  sarebbe  dunque  assolto  dalla  giurisprudenza,  che    &#13;
 potrebbe   rinvenire   ragioni  giustificative  dell'elasticità  del    &#13;
 contenuto   normativo  nei  mutamenti  connessi  ai  diversi  momenti    &#13;
 storici. L'art. 15 della legge sulla stampa sarebbe infatti diretto a    &#13;
 tutelare  non  solo  la  comune morale, ma anche l'ordine familiare e    &#13;
 l'ordine pubblico.                                                       &#13;
     Perché   il   fatto   si   configuri   come  reato  occorre  che    &#13;
 l'espressione  narrativa o visiva sia palesemente suggestiva e denoti    &#13;
 "un'insensibilità   morale  dell'autore".  La  norma  incriminatrice    &#13;
 richiederebbe,  cioè,  un  quid  pluris: l'idoneità del documento a    &#13;
 destare sensazione o raccapriccio, il che basterebbe a scongiurare la    &#13;
 pretesa violazione dei parametri costituzionali invocati.                &#13;
                                                                          &#13;
     5. - In una memoria successiva i difensori delle parti costituite    &#13;
 hanno insistito nella richiesta di una declaratoria di illegittimità    &#13;
 costituzionale  della disposizione denunciata, rilevando, da un lato,    &#13;
 la   frequente   circolazione  di  immagini  "forti",  potenzialmente    &#13;
 qualificabili  come  raccapriccianti o impressionanti e, dall' altro,    &#13;
 il profondo mutarsi della sensibilità collettiva.                       &#13;
     La  Corte  dovrebbe  quindi  censurare l'art. 15 in esame, per la    &#13;
 "sopravvenuta  irragionevolezza",  analogamente  a  quanto  affermato    &#13;
 nelle sentenze nn. 370 del 1996 e 519 del 1995.<diritto>Considerato in diritto</diritto>1. - Viene  all'esame della Corte, con riferimento agli artt. 25,    &#13;
 21  e 3 della Costituzione, la questione di legittimità dell'art. 15    &#13;
 della  legge  8 febbraio 1948, n. 47 (Disposizioni sulla stampa), che    &#13;
 sanziona  penalmente,  ai  sensi  dell'art. 528  del  codice  penale,    &#13;
 l'utilizzazione  di  "stampati  i  quali descrivano o illustrino, con    &#13;
 particolari  impressionanti  o raccapriccianti, avvenimenti realmente    &#13;
 verificatisi o anche soltanto immaginari, in modo da poter turbare il    &#13;
 comune  sentimento  della  morale  e  l'ordine  familiare  o da poter    &#13;
 provocare  il  diffondersi  di  suicidi  o  delitti". Esso lederebbe,    &#13;
 infatti,   il   principio  di  tassatività  e  determinatezza  delle    &#13;
 fattispecie  penali,  quello della libertà di stampa e i principi di    &#13;
 ragionevolezza   e   uguaglianza,   perché   non  offrirebbe  idoneo    &#13;
 fondamento  giustificativo  alla  punizione  di coloro che diffondono    &#13;
 siffatte immagini.                                                       &#13;
                                                                          &#13;
     2. - L'art. 15 della legge n. 47 del 1948 dispone che si applichi    &#13;
 l'art. 528  del  codice  penale  ai fatti riguardanti gli "stampati i    &#13;
 quali  descrivano  o  illustrino,  con  particolari  impressionanti o    &#13;
 raccapriccianti,  avvenimenti realmente verificatisi o anche soltanto    &#13;
 immaginari".                                                             &#13;
     La  previsione  penale  esige,  come  elemento  della fattispecie    &#13;
 legale,  che tali stampati siano formati in modo "da poter turbare il    &#13;
 comune  sentimento  della  morale  o  l'ordine  familiare  o da poter    &#13;
 provocare  il diffondersi di suicidi o delitti". Essa è all'esame di    &#13;
 questa  Corte  per  indeterminatezza,  violazione  del  principio  di    &#13;
 uguaglianza  e  indebita  limitazione  della  libertà  di stampa, ma    &#13;
 soltanto  nella parte in cui dispone che questi stampati siano idonei    &#13;
 a "turbare il comune sentimento della morale".                           &#13;
                                                                          &#13;
     3. - La questione non è fondata.                                    &#13;
     Con riguardo all'art. 21, sesto comma, della Costituzione, questa    &#13;
 Corte  non  può  non  ricordare  che  tale articolo - nel vietare le    &#13;
 pubblicazioni  contrarie  al  buon  costume  -  demanda alla legge la    &#13;
 predisposizione  di  meccanismi  e strumenti adeguati a prevenire e a    &#13;
 reprimere le violazioni del precetto costituzionale.                     &#13;
     L'art. 15  della  legge  sulla  stampa  del 1948, esteso anche al    &#13;
 sistema  radiotelevisivo  pubblico  e  privato dall'art. 30, comma 2,    &#13;
 della  legge  6 agosto 1990, n. 223, non intende andare al di là del    &#13;
 tenore  letterale  della  formula  quando vieta gli stampati idonei a    &#13;
 "turbare  il  comune  sentimento  della  morale".  Vale  a  dire, non    &#13;
 soltanto  ciò che è comune alle diverse morali del nostro tempo, ma    &#13;
 anche  alla  pluralità  delle  concezioni etiche che convivono nella    &#13;
 società  contemporanea. Tale contenuto minimo altro non è se non il    &#13;
 rispetto  della  persona  umana,  valore  che  anima  l'art. 2  della    &#13;
 Costituzione,   alla   luce   del   quale   va  letta  la  previsione    &#13;
 incriminatrice denunciata.                                               &#13;
     Solo  quando  la soglia dell'attenzione della comunità civile è    &#13;
 colpita  negativamente,  e  offesa,  dalle pubblicazioni di scritti o    &#13;
 immagini  con  particolari  impressionanti  o raccapriccianti, lesivi    &#13;
 della   dignità   di   ogni  essere  umano,  e  perciò  avvertibili    &#13;
 dall'intera  collettività,  scatta la reazione dell'ordinamento. E a    &#13;
 spiegare  e  a dar ragione dell'uso prudente dello strumento punitivo    &#13;
 è proprio la necessità di un'attenta valutazione dei fatti da parte    &#13;
 dei  differenti organi giudiziari, che non possono ignorare il valore    &#13;
 cardine della libertà di manifestazione del pensiero. Non per questo    &#13;
 la  libertà  di  pensiero  è  tale  da  inficiare la norma sotto il    &#13;
 profilo  della  legittimità  costituzionale,  poiché  essa  è  qui    &#13;
 concepita come presidio del bene fondamentale della dignità umana.      &#13;
                                                                          &#13;
     4. - Così intesa la figura delittuosa, si possono superare anche    &#13;
 le residue censure.                                                      &#13;
     La   descrizione   dell'elemento   materiale   del   fatto-reato,    &#13;
 indubbiamente  caratterizzato  dal  riferimento  a concetti elastici,    &#13;
 trova nella tutela della dignità umana il suo limite, sì che appare    &#13;
 escluso  il  pericolo  di  arbitrarie  dilatazioni della fattispecie,    &#13;
 risultando   quindi   infondate   le   censure   di   genericità   e    &#13;
 indeterminatezza.                                                        &#13;
     Quello  della  dignità  della  persona umana è, infatti, valore    &#13;
 costituzionale  che  permea  di sé il diritto positivo e deve dunque    &#13;
 incidere  sull'interpretazione  di quella parte della disposizione in    &#13;
 esame  che  evoca  il  comune  sentimento  della morale. Nella stessa    &#13;
 chiave  interpretativa  si  dissolvono  i  dubbi sul fondamento della    &#13;
 previsione  incriminatrice. Onde non v'è lesione degli artt. 3, 21 e    &#13;
 25 della Costituzione.</testo>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara  non fondata la questione di legittimità costituzionale    &#13;
 dell'art. 15  della  legge 8 febbraio 1948, n. 47 (Disposizioni sulla    &#13;
 stampa),  sollevata,  in riferimento agli artt. 3, 21, sesto comma, e    &#13;
 25 della Costituzione, con l'ordinanza in epigrafe.                      &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta l'11 luglio 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Guizzi                             &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 17 luglio 2000.                         &#13;
               Il direttore della cancelleria: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
