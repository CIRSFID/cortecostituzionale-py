<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>218</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:218</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1975</data_decisione>
    <data_deposito>17/07/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale dell'art.  1,  &#13;
 terzo comma, della legge 29 ottobre 1949, n. 826,  concernente  aumento  &#13;
 delle  sanzioni pecuniarie relative alle contravvenzioni della legge 28  &#13;
 settembre 1939, n.  1822, sulla disciplina degli autoservizi di  linea,  &#13;
 e  dell'art.    36 della legge 28 settembre 1939, n. 1822, e successive  &#13;
 modifiche, promossi con le seguenti ordinanze:                           &#13;
     1) ordinanza emessa il 25 febbraio 1972 dal pretore  di  Monza  nel  &#13;
 procedimento  penale a carico di Panarotto Faustino, iscritte al n. 179  &#13;
 del registro ordinanze 1972 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n.  158 del 21 giugno 1972;                                   &#13;
     2) ordinanze emesse l'8 gennaio 1973 dal  pretore  di  Valle  della  &#13;
 Lucania  nei  procedimenti  penali  rispettivamente  a carico di Ruocco  &#13;
 Antonio e di Avallone Salvatore, iscritte ai nn. 196 e 197 del registro  &#13;
 ordinanze 1973 e pubblicate nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 176 dell'11 luglio 1973 e n. 191 del 25 luglio 1973.                  &#13;
     Udito  nella  camera  di  consiglio  del  6  marzo  1975 il Giudice  &#13;
 relatore Michele Rossano.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso del procedimento penale a carico di  Panarotto  Faustino,  &#13;
 autista  della  soc.  S.A.S.  -  indiziato,  in  seguito a rapporto dei  &#13;
 carabinieri di Cernusco sul Naviglio,  della  contravvenzione  prevista  &#13;
 dall'art.20  legge  28  settembre  1939,  n.  1822,  e  punita ai sensi  &#13;
 dell'art. 1 legge 29 ottobre 1949, n. 826, per non avere effettuato, il  &#13;
 18 dicembre 1971, il servizio di linea, con  autobus  da  Cernusco  sul  &#13;
 Naviglio  a  Sesto San Giovanni - il pretore di Monza, con ordinanza 25  &#13;
 febbraio 1972, ha sollevato, di ufficio, in  riferimento  all'art.  112  &#13;
 Cost.,  la questione di legittimità costituzionale del terzo comma del  &#13;
 citato art.  1 legge n. 826 del 1949. Ha rilevato che la  norma  citata  &#13;
 è  stata costantemente interpretata dalla giurisprudenza nel senso che  &#13;
 l'accertamento delle contravvenzioni alle disposizioni della menzionata  &#13;
 legge n. 1822 del  1939  è  di  competenza  esclusiva  dei  funzionari  &#13;
 dell'Ispettorato  generale  della  motorizzazione  civile,  e,  quindi,  &#13;
 qualora - il fatto  sia  stato  accertato  e  denunziato  da  autorità  &#13;
 diversa,  deve  essere  necessariamente  dichiarata  l'improcedibilità  &#13;
 dell'azione penale. Ha affermato che sussiste violazione del  principio  &#13;
 dell'art.  112  Cost.,  dato  che  in base a tale principio il pubblico  &#13;
 ministero ha l'obbligo incondizionato di esercitare l'azione penale per  &#13;
 tutti i reati di cui venga a conoscenza, quale che sia la  fonte  della  &#13;
 notitia criminis.                                                        &#13;
     L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n. 158 del  &#13;
 21  giugno  1972.  Nel  giudizio  davanti  a  questa  Corte  non  si è  &#13;
 costituita la parte e non è intervenuto il  Presidente  del  Consiglio  &#13;
 dei ministri.                                                            &#13;
     Nel  corso  dei due procedimenti penali a carico rispettivamente di  &#13;
 Ruocco Antonio, autista della ditta "La Veloce", e Avallone  Salvatore,  &#13;
 autista  della soc. per az. "Sometra" - imputati, in seguito a rapporto  &#13;
 dei carabinieri di Castelnuovo Cilento, della contravvenzione  prevista  &#13;
 dagli  artt.  10  e  36  legge  28 settembre 1939, n.   1822, per avere  &#13;
 trasportato,  su  autocorriera  di  linea,  un  numero  di   passeggeri  &#13;
 superiore  a quello prescritto - il pretore di Vallo della Lucania, con  &#13;
 due ordinanze, dal contenuto identico, emesse l'8  gennaio  1973  -  ha  &#13;
 sollevato  di  ufficio, in riferimento all'art. 112 Cost., la questione  &#13;
 di legittimità costituzionale dell'art. 36 legge 28 settembre 1939, n.  &#13;
 1822, e successive modifiche. Ha affermato che le norme citate sono  in  &#13;
 contrasto   con   il   principio   costituzionale  dell'obbligatorietà  &#13;
 dell'esercizio dell'azione penale da parte del pubblico ministero, dato  &#13;
 che questa azione non può essere promossa qualora le  infrazioni  alle  &#13;
 disposizioni  della  menzionata legge n.  1822 del 1939 non siano state  &#13;
 accertate da funzionari dell'Ispettorato generale della  motorizzazione  &#13;
 civile  e  dei  trasporti  in concessione e la notitia criminis non sia  &#13;
 stata trasmessa dai medesimi funzionari.                                 &#13;
     Le due ordinanze sono state pubblicate nella Gazzetta Ufficiale  n.  &#13;
 176 dell'11 luglio 1973 e n. 191 del 25 luglio 1973.                     &#13;
     Nei  giudizi davanti a questa Corte le parti non si sono costituite  &#13;
 e non è intervenuto il Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Le ordinanze  sollevano  questioni  identiche  che,  pertanto,  &#13;
 possono essere decise con unica sentenza.                                &#13;
     2.  -  Secondo  le  ordinanze  sarebbe  illegittimo, per violazione  &#13;
 dell'art. 112 Cost., l'art. 1, terzo comma, legge 29 ottobre  1949,  n.  &#13;
 826,  che  sostituì  l'art. 36 legge 28 settembre 1939, n. 1822, sulla  &#13;
 disciplina  degli  autoservizi  di  linea,  in   quanto   dispone   che  &#13;
 "l'accertamento   delle   contravvenzioni   spetta   esclusivamente  ai  &#13;
 funzionari dell'Ispettorato generale della motorizzazione civile e  dei  &#13;
 trasporti  in  concessione".  Tale  esclusiva  competenza violerebbe il  &#13;
 principio dell'art. 112 Cost., secondo il quale il  pubblico  ministero  &#13;
 ha  l'obbligo di iniziare l'azione penale qualunque sia la fonte da cui  &#13;
 attinga la notitia criminis, laddove, secondo la  giurisprudenza  della  &#13;
 Corte   di   cassazione,   anche   nel   caso   in  cui  l'accertamento  &#13;
 dell'infrazione contravvenzionale risulti essere stato  effettuato  dai  &#13;
 carabinieri   e   riferito  al  pubblico  ministero,  l'azione  sarebbe  &#13;
 improcedibile. Né, è posto in risalto nell'ordinanza del  pretore  di  &#13;
 Monza,  il  pubblico ministero potrebbe "richiedere l'Ispettorato della  &#13;
 motorizzazione per sanare l'aspetto  formale  della  sua  investitura".  &#13;
 Tale   organo,  "agendo  nell'ambito  dei  suoi  poteri  discrezionali,  &#13;
 potrebbe correttamente ritenere l'insussistenza dell'infrazione. In tal  &#13;
 caso  la  delibazione  sull'eventuale   configurabilità   del   reato,  &#13;
 spetterebbe,   in   definitiva,   non   già  al  pretore,  sibbene  ad  &#13;
 un'autorità   amministrativa   che   illegittimamente   verrebbe    ad  &#13;
 interferire nell'esercizio di una funzione giurisdizionale".             &#13;
     3. - La questione non è fondata.                                    &#13;
     L'art.  1,  terzo  comma,  legge  n. 826 del 1949 cit., che prevede  &#13;
 l'esclusiva competenza dei funzionari dell'Ispettorato  generale  della  &#13;
 motorizzazione civile e dei trasporti in concessione, concerne, come è  &#13;
 precisato  nel  primo  comma  dello  stesso articolo, che stabilisce la  &#13;
 sanzione  dell'ammenda  da  un  minimo  ad  un  massimo,  soltanto   le  &#13;
 contravvenzioni  "alle  disposizioni  della legge 28 settembre 1939, n.  &#13;
 1822, sulla disciplina degli autoservizi di linea". E in ordine ad esse  &#13;
 l'art. 20 del Capo V  intitolato  "Vigilanza  e  facoltà  governative"  &#13;
 dispone: "Spetta al Ministero delle comunicazioni (Ispettorato generale  &#13;
 delle  ferrovie,  tramvie  ed  automobili) di impartire le disposizioni  &#13;
 necessarie per garantire la regolarità e la  sicurezza  dell'esercizio  &#13;
 dei  servizi  pubblici  automobilistici  di cui all'art. 1. Al suddetto  &#13;
 Ispettorato è anche demandata la vigilanza  sui  servizi  stessi.    I  &#13;
 funzionari  dell'Ispettorato hanno la facoltà di chiedere in visione e  &#13;
 di esaminare direttamente  i  libri,  le  contabilità  e  i  documenti  &#13;
 dell'azienda  relativi  alla  gestione  del  servizio  ed hanno inoltre  &#13;
 libero percorso sulle vetture e libero accesso nelle  rimesse  ed  alle  &#13;
 officine,  previa esibizione della tessera di riconoscimento rilasciata  &#13;
 dall'Ispettorato  medesimo.   Il   concessionario   ha   l'obbligo   di  &#13;
 ottemperare alle prescrizioni dell'autorità di vigilanza, di fornire a  &#13;
 questa tutti i dati od elementi statistici concernenti il servizio e di  &#13;
 fare  quant'altro  occorra  per  agevolare  ai  funzionari  predetti il  &#13;
 proprio mandato".                                                        &#13;
     I successivi artt. 21, 22, 23 della stessa  legge  disciplinano  in  &#13;
 particolare  i  provvedimenti  che il Ministero, "ove il concessionario  &#13;
 non  ottemperi  nel  termine  prefisso  alle   disposizioni   impartite  &#13;
 dall'autorità di vigilanza" può adottare, indipendentemente da quelli  &#13;
 stabiliti  dall'art.    19  (sospensione  del pagamento del sussidio) e  &#13;
 dall'art.  36 (accertamento delle contravvenzioni, ora art. 1 legge  n.  &#13;
 826 del 1949 cit.).                                                      &#13;
     4.  -  Tanto  premesso,  può  affermarsi  che  le  contravvenzioni  &#13;
 previste  dalla   norma   impugnata   concernono   la   violazione   di  &#13;
 provvedimenti dell'autorità amministrativa (Ispettorato generale della  &#13;
 motorizzazione  civile  e  dei  trasporti  in concessione) emanati, nei  &#13;
 confronti dei  concessionari,  nell'esercizio  di  un  autonomo  potere  &#13;
 disciplinato  dalla legge per il perseguimento di un interesse pubblico  &#13;
 delimitato e precisato nell'articolo 20 legge n. 1822 del 1939 citata e  &#13;
 successive disposizioni della legge medesima.                            &#13;
     Secondo la disciplina citata (art. 1 legge n. 826 del 1949 e  artt.  &#13;
 20,21,22,23  legge n. 1822 del 1939) l'accertamento del fatto è bensì  &#13;
 di competenza esclusiva dell'Ispettorato della  motorizzazione  civile,  &#13;
 tuttavia   non   è  escluso  che  le  contravvenzioni  possano  essere  &#13;
 contestate  da  qualsiasi  organo  di  polizia  giudiziaria,  spettando  &#13;
 peraltro  all'autorità  giudiziaria  di  richiedere  all'Ispettorato i  &#13;
 necessari e dovuti accertamenti a premessa del promovimento dell'azione  &#13;
 penale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondate:                                                &#13;
     a)  la  questione di legittimità costituzionale dell'art. 1, terzo  &#13;
 comma, della legge 29 ottobre 1949, n. 826, concernente  aumento  delle  &#13;
 sanzioni  pecuniarie  relative  alle  contravvenzioni  della  legge  28  &#13;
 settembre 1939, n. 1822, sulla disciplina degli autoservizi  di  linea,  &#13;
 sollevata  dal  pretore  di  Monza  con  ordinanza 25 febbraio 1972, in  &#13;
 riferimento all'art. 112 della Costituzione;                             &#13;
     b) la questione di legittimità costituzionale dell'art.  36  della  &#13;
 legge 28 settembre 1939, n. 1822, sulla disciplina degli autoservizi di  &#13;
 linea e successive modifiche (legge 29 ottobre 1949, n. 826), sollevata  &#13;
 dal  pretore di Vallo della Lucania con le ordinanze 8 gennaio 1973, in  &#13;
 riferimento all'art. 112 della Costituzione.                             &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'8 luglio 1975.           &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO  AMADEI - GIULIO GIONFRIDA -  &#13;
                                   EDOARDO VOLTERRA  -  GUIDO  ASTUTI  -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
