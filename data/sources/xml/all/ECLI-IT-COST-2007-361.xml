<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>361</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:361</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>24/10/2007</data_decisione>
    <data_deposito>31/10/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Luigi MAZZELLA, Gaetano SILVESTRI, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 19,  comma 2, lettera c) e 29, comma 1, lettera b-bis), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promosso con ordinanza del 30 maggio 2006 dal Giudice di pace di Siracusa sul ricorso proposto da O.K. contro la Prefettura di Siracusa, iscritta al n. 171 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 14, prima serie speciale, dell'anno 2007. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 26 settembre 2007 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che, con ordinanza del 30 maggio 2006, emessa nel corso di un giudizio di opposizione avverso il decreto di espulsione nei confronti di O.K., il Giudice di pace di Siracusa ha sollevato questione di legittimità costituzionale degli artt. 19, comma 2, lettera c), e 29, comma 1, lettera b-bis), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), per violazione degli artt. 2, 3, 10, 29 e 30 della Costituzione; &#13;
      che il rimettente, in punto di fatto, rileva che dalla documentazione allegata al ricorso e da quella acquisita nel corso dell'udienza, risulterebbe che il ricorrente è nato e residente in Italia, nonché inserito in un «regolare nucleo familiare», costituito da padre, madre e tre fratelli; elementi di cui il decreto di espulsione non terrebbe affatto conto. Con la conseguenza che il disposto accompagnamento alla frontiera si porrebbe in contrasto con l'art. 13, comma 15, del d.lgs. n. 286 del 1998, il quale esclude «l'applicazione di detta misura nei confronti dello straniero che dimostri sulla base di elementi obiettivi di essere giunto nel territorio dello Stato prima della data di entrata in vigore della legge 6 marzo 1998, n. 40»; &#13;
      che, a parere del giudice a quo, tra i diritti fondamentali della persona, di cui all'art. 2 della Costituzione, rientrerebbe «a pieno titolo» anche il diritto all'unità familiare, assumendo sul punto rilevanza l'art. 2 del d.lgs. n. 286 del 1998, nonché l'art.8 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali del 1950, ratificata e resa esecutiva con legge 4 agosto 1955, n. 848; &#13;
     che, sempre secondo il giudice a quo, l'art. 19 del d.lgs n. 286 del 1998, per un verso, sarebbe «fortemente ispirato al rispetto dell'unità familiare» poiché «dispone il divieto di espulsione degli “stranieri conviventi con i parenti entro il quarto grado o con il coniuge di nazionalità italiana”»; per altro verso, detta norma avrebbe «esaltato il principio dell'unità familiare in favore del cittadino italiano» negandolo, invece, al cittadino straniero; &#13;
      che il giudice rimettente, pertanto, sollecita una declaratoria di illegittimità costituzionale dell'art. 19 del d.lgs. n. 286 del 1998 «nella parte in cui dispone il divieto di espulsione esclusivamente in favore degli stranieri conviventi con parenti entro il quarto grado o con il coniuge “di nazionalità italiana”, escludendo analogo divieto in favore degli stranieri conviventi con parenti entro il quarto grado o con il coniuge già residenti in Italia e regolarmente muniti di permesso di soggiorno»; &#13;
      che, ad avviso del giudice a quo, in relazione alla fattispecie oggetto del giudizio principale, risulterebbe, altresì, rilevante la disposizione di cui all'art. 29, comma 1, lettera b-bis), del medesimo d.lgs. n. 286 del 1998; &#13;
      che, detta disposizione, «nella parte in cui limita il ricongiungimento familiare in favore di figli maggiorenni a carico totalmente invalidi, senza estendere analogo diritto ai figli maggiorenni a carico per ragioni oggettive», si porrebbe in contrasto con gli artt. 2, 3, 10, 29 e 30 della Costituzione; &#13;
      che, in particolare, l'art. 30 della Costituzione, afferma il «diritto e il dovere dei genitori di “mantenere” i figli», e che, ad avviso del rimettente, tale «formula (…) non può intendersi limitata all'ambito del mero sostentamento materiale, ma piuttosto estesa fino a ricomprendervi ogni forma di assistenza e sostegno dei quali i figli mostrino, stabilmente ovvero occasionalmente, di avere bisogno»; &#13;
      che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate manifestamente inammissibili, o in subordine, manifestamente infondate. &#13;
      Considerato che, il Giudice di pace di Siracusa dubita della legittimità costituzionale dell'art. 19, comma 2, lettera c), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), «nella parte in cui dispone il divieto di espulsione esclusivamente in favore degli stranieri conviventi con parenti entro il quarto grado o con il coniuge “di nazionalità italiana”, escludendo analogo divieto in favore degli stranieri conviventi con parenti entro il quarto grado o con il coniuge già residenti in Italia e regolarmente muniti di permesso di soggiorno» per violazione degli artt. 2, 3, 10, 29 e 30 della Costituzione; &#13;
      che il rimettente dubita, altresì, della legittimità costituzionale dell'art. 29, comma 1, lettera b-bis), del d.lgs. n. 286 del 1998 «nella parte in cui limita il ricongiungimento familiare in favore di figli maggiorenni a carico totalmente invalidi, senza estendere analogo diritto ai figli maggiorenni a carico per ragioni oggettive», per violazione degli artt. 2, 3, 10, 29 e 30 della Costituzione; &#13;
      che, quanto alla censura relativa all'art. 19 del d.lgs. n. 286 del 1998, questa Corte ha già esaminato identica questione di legittimità costituzionale dichiarandola manifestamente infondata con l'ordinanza n. 158 del 2006 e, pertanto, stante l'immutato quadro normativo, le argomentazioni poste a base della indicata pronuncia devono essere confermate; &#13;
      che, in particolare, la Corte ha affermato che, secondo giurisprudenza costante, «il legislatore può legittimamente porre dei limiti all'accesso degli stranieri nel territorio nazionale effettuando “un corretto bilanciamento dei valori in gioco”, esistendo in materia un'ampia discrezionalità legislativa, limitata soltanto dal vincolo che le scelte non risultino manifestamente irragionevoli (sentenza n. 353 del 1997)»; &#13;
      che, quanto alla questione relativa all'art. 29, comma 1, lett. b-bis), del d.lgs. n. 286 del 1998, il rimettente, essendo chiamato a giudicare sulla legittimità del decreto di espulsione impugnato, non deve fare applicazione della norma che disciplina il ricongiungimento familiare: &#13;
      che, pertanto, in difetto del requisito della rilevanza, la stessa va dichiarata manifestamente inammissibile. &#13;
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e, 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
      dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 29, comma 1, lettera b-bis), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero) sollevata, in riferimento agli artt. 2, 3, 10, 29, e 30 della Costituzione, dal Giudice di pace di Siracusa, con l'ordinanza indicata in epigrafe.  &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 19, comma 2, lettera c), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero) sollevata, in riferimento agli artt. 2, 3, 10, 29, e 30 della Costituzione, dal Giudice di pace di Siracusa, con l'ordinanza indicata in epigrafe.  &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 ottobre 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 31 ottobre 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
