from akomantoso.dom.elements import Introduction, P

from cortecostituzionale.parser.paragraphs import parse_paragraphs
from cortecostituzionale.parser.text_cleanup import cleanup


def parse_epigrafe(epigrafe):
    introduction = Introduction()
    if epigrafe.text:
        p = P(text=epigrafe.text)
        introduction.append(p)
    titolo = epigrafe.find("titolo")
    paragraphs = []
    if titolo is not None:
        p = P(text=titolo.text)
        p.add_inline(("docType", (0, len(p.text)), {}))
        introduction.append(p)
        paragraphs = cleanup(titolo.tail)
    parse_paragraphs(paragraphs, introduction)
    return introduction
