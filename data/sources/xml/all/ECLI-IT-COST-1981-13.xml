<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>13</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:13</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/01/1981</data_decisione>
    <data_deposito>10/02/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art.  2948, n.  4,  &#13;
 cod.  civ.  promosso  con  ordinanza,  emessa  il 18 settembre 1979 dal  &#13;
 Pretore di Parma nel procedimento civile vertente tra Calvi  Corrado  e  &#13;
 la  s.p.a.  Salvarani, iscritta al n. 817 del registro ordinanze 1979 e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 15 del 1980.     &#13;
     Udito nella camera di consiglio del 13  novembre  1980  il  Giudice  &#13;
 relatore Virgilio Andrioli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Provvedendo  sulla  domanda, diretta, con ricorso in data 19 luglio  &#13;
 1979, da Corrado Calvi a conseguire, tra  l'altro,  la  condanna  della  &#13;
 Salvarani  s.p.a.,  alle cui dipendenze aveva lavorato, al pagamento, a  &#13;
 suo  favore,  della  complessiva  somma  di  lire   76.548.847   (oltre  &#13;
 rivalutazione monetaria e interessi) per differenze retributive, a vari  &#13;
 titoli maturate, dal 21 maggio 1968 al 16 febbraio 1979, sulla base del  &#13;
 rapporto  subordinato  tra  le  parti  svoltosi,  e  sulla eccezione di  &#13;
 maturatasi prescrizione quinquennale, a sensi  dell'art.  2948,  n.  4,  &#13;
 cod.   civ.,  dei  crediti  fatti  valere  dall'attore  per  differenze  &#13;
 retributive, maturate prima del quinquennio  antecedente  alla  formale  &#13;
 messa  in  mora,  l'adito  Pretore di Parma, in funzione di giudice del  &#13;
 lavoro, con ordinanza 18 settembre 1979, comunicata  e  notificata  nei  &#13;
 modi  di  legge,  pubblicata  nella  Gazzetta  Ufficiale n.   15 del 16  &#13;
 gennaio 1980, e iscritta al n. 817 R.O. 1979, ha giudicato rilevante  e  &#13;
 non  manifestamente  infondata  la  questione di legittimità dell'art.  &#13;
 2948, n. 4, cod.  civ., in riferimento agli artt. 136, comma primo e 36  &#13;
 Cost., perché, successivamente alla pubblicazione  della  sentenza  10  &#13;
 giugno  1966, n. 63, consentirebbe che la prescrizione quinquennale del  &#13;
 diritto alla retribuzione decorra durante lo svolgimento di rapporti di  &#13;
 lavoro  privati  soggetti alla applicazione delle leggi 15 luglio 1966,  &#13;
 n. 604 e 20 maggio  1970,  n.  300,  malgrado  il  generale  "stato  di  &#13;
 soggezione"  del  lavoratore,  consistente  nel  ragionevole  timore di  &#13;
 ritorsione da parte del datore di lavoro, che, pur  senza  giungere  al  &#13;
 licenziamento,  può assumere le forme più disparate (ipotesi di metus  &#13;
 non prese in considerazione nella sentenza n. 63/1966).                  &#13;
     Nessuna  delle  parti  essendosi  costituita  né  avendo  spiegato  &#13;
 intervento  il  Presidente  del  Consiglio  dei  ministri, la decisione  &#13;
 dell'incidente, a sensi dell'art. 26, secondo  comma,  della  legge  11  &#13;
 marzo  1953,  n. 87, è stata fissata per la camera di consiglio del 13  &#13;
 novembre 1980, nel corso della quale il giudice Andrioli ha  svolto  la  &#13;
 relazione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  Pretore  lamenta  che  la Corte, prendendo, con pronunce  &#13;
 successive alla  sent.  n.  63/1966,  in  considerazione  le  leggi  n.  &#13;
 604/1966  e n. 300/1970 al fine di verificare la conformità ai dettami  &#13;
 costituzionali dell'art. 2948, n. 4, cod. civ., non abbia tenuto  nella  &#13;
 debita considerazione il dispositivo della sent. n.  63/1966, nel quale  &#13;
 è  stata  dichiarata  l'illegittimità  dell'art.  2948, n. 4 (e degli  &#13;
 artt. 2955 e 2956, n. 1) cod.  civ., limitatamente alla  parte  in  cui  &#13;
 consentono  che  la  prescrizione del diritto alla retribuzione decorra  &#13;
 durante il rapporto di lavoro, fosse oppur no l'inerzia del  lavoratore  &#13;
 provocata dalla "situazione psicologica del lavoratore, che può essere  &#13;
 indotto  a  non  esercitare il proprio diritto per lo stesso motivo per  &#13;
 cui è portato a rinunciarvi per timore del licenziamento"; la  mancata  &#13;
 inserzione,  nel  dispositivo,  della  frase  della motivazione, or ora  &#13;
 riprodotta in virgolato, impedirebbe, dunque, alla Corte di prendere in  &#13;
 considerazione - si ripete - i due successivi corpi di norme.            &#13;
     Con la sent. n. 40/1979, la Corte ha precisato che il rapporto  tra  &#13;
 la  disciplina  normativa, modificata da sentenza di accoglimento della  &#13;
 Corte medesima, e la disciplina successivamente adottata  con  legge  o  &#13;
 atto   avente   forza   di   legge   (a   loro   volta  non  sospettati  &#13;
 d'incostituzionalità)  dà  vita  a  vicende  di  parziale  o   totale  &#13;
 abrogazione  tacita,  competente  a conoscere delle quali è il giudice  &#13;
 ordinario, non la Corte  costituzionale  per  adire  la  quale  sarebbe  &#13;
 d'uopo  muovere  dalla  premessa,  per la verità inconsistente, che la  &#13;
 sentenza  di  accoglimento  della   Corte   attribuisca   alla   norma,  &#13;
 parzialmente  annullata,  autorità  superiore  al vigore proprio delle  &#13;
 leggi ordinarie e degli atti aventi  forza  di  legge  ordinaria.  Tale  &#13;
 sentenza  è  più che sufficiente a negar fondamento alla denuncia, da  &#13;
 giudicarsi infondata e non manifestamente infondata sol perché si sono  &#13;
 assunti a parametri non gli artt. 3 e 36, ma l'art. 136.                 &#13;
     2. - Inammissibile per irrilevanza è la seconda questione  perché  &#13;
 il  giudice  a  quo  non  ha  speso la benché minima motivazione sulle  &#13;
 misure, diverse dal licenziamento, di cui il lavoratore  sarebbe  stato  &#13;
 vittima  se avesse fatto valere i propri diritti, seppure non è lecito  &#13;
 osservare che soltanto il licenziamento priva il lavoratore del diritto  &#13;
 al salario, garantito dall'art. 36.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1.  -  Dichiara  non fondata la questione di legittimità dell'art.  &#13;
 2948, n. 4, cod. civ. sollevata in riferimento all'art. 136  Cost.  con  &#13;
 l'ordinanza 18 settembre 1979 dal Pretore di Parma;                      &#13;
     2.  -  Dichiara  inammissibile  per  irrilevanza  la  questione  di  &#13;
 legittimità dell'art. 2948, n. 4, cod. civ. sollevata  in  riferimento  &#13;
 all'art. 36 Cost. con la stessa ordinanza.                               &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 29 gennaio 1981.        &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA   -   EDOARDO   VOLTERRA  -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   - LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI  -  LIVIO  PALADIN  -   ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE FERRARI.  &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
