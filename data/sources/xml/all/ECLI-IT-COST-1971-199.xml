<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>199</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:199</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FRAGALI</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/12/1971</data_decisione>
    <data_deposito>16/12/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. MICHELE FRAGALI, Presidente - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  49,  terzo  &#13;
 comma,  e  allegata  tabella B, del r.d.l. 4 ottobre 1935, n. 1827, sul  &#13;
 perfezionamento e coordinamento legislativo della  previdenza  sociale,  &#13;
 convertito  con  modificazioni  nella  legge  6  aprile  1936, n. 1155,  &#13;
 promosso con ordinanza emessa il 9 maggio 1969 dal tribunale  di  Parma  &#13;
 nel  procedimento  civile  vertente tra Nugara Rosalia, Curli Romilda e  &#13;
 l'Istituto nazionale della previdenza sociale, iscritta al n.  363  del  &#13;
 registro  ordinanze  1969  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n.  269 del 22 ottobre 1969.                                  &#13;
     Visto l'atto di costituzione di Nugara Rosalia;                      &#13;
     udito nell'udienza pubblica del 27 ottobre 1971 il Giudice relatore  &#13;
 Giuseppe Verzì;                                                         &#13;
     uditi gli avvocati Paolo Barile e Franco Agostini, per la Nugara.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nugara Rosalia, premesso:                                            &#13;
     a) di aver prestato lavoro quale domestica per non meno di  quattro  &#13;
 ore giornaliere alle dipendenze di Curli Romilda e che questa non aveva  &#13;
 adempiuto  agli  obblighi contributivi omettendo di applicare le marche  &#13;
 previdenziali sulla tessera assicurativa;                                &#13;
     b) che sia l'I.N.P.S., sia, successivamente, il Comitato  esecutivo  &#13;
 del  medesimo,  avevano respinto la denuncia e il ricorso presentato da  &#13;
 essa Nugara contro la datrice di lavoro,  rilevando  la  insussistenza,  &#13;
 nella  specie,  "di un rapporto di lavoro domestico con caratteristiche  &#13;
 di orario  e  di  disciplina  tali  da  renderlo  soggetto  all'obbligo  &#13;
 assicurativo di legge";                                                  &#13;
     citava  in  giudizio avanti il tribunale di Parma sia la Curli, sia  &#13;
 l'I.N.P.S.  chiedendo  che  fosse  dichiarato  intercorso  un  siffatto  &#13;
 rapporto  con  la  conseguente  condanna  della Curli al versamento dei  &#13;
 contributi e dell'I.N.P.S. all'accreditamento dei medesimi.              &#13;
     Subordinatamente,   sollevava   la   questione   di    legittimità  &#13;
 costituzionale dell'art. 37, primo comma, del r.d.l. 4 ottobre 1935, n.  &#13;
 1827,  in  riferimento  agli artt. 3, primo comma, e 38, secondo comma,  &#13;
 della Carta, "per la parte  in  cui,  secondo  l'interpretazione  della  &#13;
 Corte  di  cassazione,  esclude  i domestici ad ore dalla assicurazione  &#13;
 obbligatoria".                                                           &#13;
     Il tribunale, con ordinanza 9 maggio 1969, ritenuta intercorsa  tra  &#13;
 l'attrice  e la Curli una prestazione di lavoro inferiore a quattro ore  &#13;
 giornaliere, condividendo, nella sostanza, l'assunto di  illegittimità  &#13;
 costituzionale  della Nugara, ma individuando non già nell'art. 37, ma  &#13;
 nell'art. 49 dello stesso decreto n. 1827 e nella  tabella  B  ad  esso  &#13;
 allegata  le  disposizioni  che,  consentendo  la  suddetta esclusione,  &#13;
 sarebbero in contrasto con i ripetuti  articoli  della  Carta,  ha  per  &#13;
 queste  sollevato la questione di legittimità costituzionale, sotto il  &#13;
 profilo prospettato dall'attrice.                                        &#13;
     Nel  presente  giudizio  si  è  costituita  la  Nugara.   Non   è  &#13;
 intervenuto il Presidente del Consialio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  La  questione  di  legittimità costituzionale sollevata dal  &#13;
 tribunale di Parma riguarda la tutela previdenziale dei  prestatori  di  &#13;
 lavoro  domestico  continuativo  di  durata  inferiore alle quattro ore  &#13;
 giornaliere.  Costoro  sono  esclusi   dalla   assicurazione   generale  &#13;
 obbligatoria  contro  l'invalidità  e  la vecchiaia perché l'art. 49,  &#13;
 terzo comma, del r.d.l. 4 ottobre 1935, n. 1827, convertito nella legge  &#13;
 6 aprile 1936, n. 1155, determina, nella tabella B ad esso allegata,  i  &#13;
 contributi  per  i  domestici "a servizio intero" e per quelli "a mezzo  &#13;
 servizio", e la  Corte  di  cassazione  ha  interpretato  l'espressione  &#13;
 "servizio  intero"  come  servizio  di  otto  o più ore giornaliere, e  &#13;
 "mezzo servizio" come servizio dalle quattro alle otto ore  al  giorno;  &#13;
 deducendo  altresì,  in  base  ad argomentazioni tratte dalle leggi 18  &#13;
 gennaio 1952, n. 35, e 2 aprile 1958, n. 339, che, per quanto  concerne  &#13;
 l'obbligo  delle  assicurazioni  previdenziali  ed  il  regolamento del  &#13;
 rapporto  stesso,  il  legislatore  ha  voluto  tutelare   soltanto   i  &#13;
 lavoratori  domestici che prestino non meno di quattro ore di lavoro al  &#13;
 giorno.                                                                  &#13;
     Secondo  l'ordinanza  di  rimessione,   la   norma   dell'art.   49  &#13;
 sopraindicata,  così  come è stata interpretata, sarebbe in contrasto  &#13;
 con l'art. 3, primo comma, della Costituzione, in quanto la  esclusione  &#13;
 si  baserebbe  soltanto  su  una  diversità  di condizioni personali e  &#13;
 sociali, e  con  l'art.  38,  secondo  comma,  per  il  quale  tutti  i  &#13;
 lavoratori  hanno  diritto  a  che  siano preveduti ed assicurati mezzi  &#13;
 adeguati alle loro esigenze di vita in caso  di  infortunio,  malattia,  &#13;
 invalidità, vecchiaia e disoccupazione involontaria.                    &#13;
     2. - La questione è inammissibile.                                  &#13;
     L'art.  49, terzo comma, del r.d.l. n. 1827 del 1935, e la relativa  &#13;
 tabella B sono stati abrogati espressamente dall'art. 6 del  r.d.l.  14  &#13;
 aprile  1939,  n.  636.  Pertanto,  non  è  consentito proporre alcuna  &#13;
 questione di legittimità costituzionale per fatti verificatisi -  come  &#13;
 nella  specie  - successivamente alla data in cui tale norma ha cessato  &#13;
 di avere vigore.                                                         &#13;
     Dopo  l'abrogazione  della  norma legislativa è subentrata, per il  &#13;
 personale addetto ai lavori familiari, una nuova disciplina, atteso che  &#13;
 il  Ministro  per  il  lavoro  e  la  previdenza  sociale,  sentite  le  &#13;
 organizzazioni  sindacali dei lavoratori e dei datori di lavoro, può -  &#13;
 con propri decreti - per particolari categorie di lavoratori  ed  anche  &#13;
 per  limitate  zone  del  territorio nazionale, stabilire le tabelle di  &#13;
 retribuzioni medie agli effetti del calcolo del contributo,  e  fissare  &#13;
 altresì  i  periodi medi di attività lavorativa (art. 6 del r.d.l. n.  &#13;
 636 del 1939, modificato dalla legge 4 aprile 1952, n. 218). E dal 1939  &#13;
 ad oggi si sono succeduti vari decreti del genere.                       &#13;
     In considerazione di siffatti poteri del  Ministro,  si  dimostrano  &#13;
 prive  di consistenza le affermazioni della ordinanza di rimessione che  &#13;
 l'opera dello stesso risulti limitata "all'adeguamento dei  criteri  di  &#13;
 determinazione  del  contributo", e che la norma di legge impugnata sia  &#13;
 rimasta in vigore, malgrado l'espressa abrogazione, per quanto riguarda  &#13;
 la distinzione fra domestici a "servizio intero" e "domestici  a  mezzo  &#13;
 servizio". E, per questa ultima parte, è da notare in modo particolare  &#13;
 che,  allorché  si  debbano  stabilire  retribuzioni  medie,  si  deve  &#13;
 necessariamente tenere conto della differenza di salario a seconda  che  &#13;
 si   tratti  di  servizio  intero  o  di  mezzo  servizio,  sicché  la  &#13;
 distinzione in esame deriva da una realtà di fatto e non  dalla  norma  &#13;
 di legge abrogata.                                                       &#13;
     Ciò  posto,  non  può  disconoscersi che l'asserita disparità di  &#13;
 trattamento  e  la  connessa  mancanza  di   assistenza   previdenziale  &#13;
 dipendono  se  mai dalla disciplina disposta con i sopracennati decreti  &#13;
 ministeriali. E poiché questi non sono  atti  aventi  forza  di  legge  &#13;
 sfuggono al sindacato di questa Corte.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 49, terzo comma, (e della allegata tabella B)  del  r.d.l.  4  &#13;
 ottobre  1935, n. 1827 (sul perfezionamento e coordinamento legislativo  &#13;
 della previdenza sociale), convertito con modificazioni nella  legge  6  &#13;
 aprile  1936, n. 1155, questione sollevata in riferimento agli artt. 3,  &#13;
 primo comma, e 38, secondo comma, della Costituzione con ordinanza  del  &#13;
 tribunale di Parma del 9 maggio 1969.                                    &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 10 dicembre 1971.                             &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
