<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2012</anno_pronuncia>
    <numero_pronuncia>233</numero_pronuncia>
    <ecli>ECLI:IT:COST:2012:233</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>QUARANTA</presidente>
    <relatore_pronuncia>Gaetano Silvestri</relatore_pronuncia>
    <redattore_pronuncia>Gaetano Silvestri</redattore_pronuncia>
    <data_decisione>08/10/2012</data_decisione>
    <data_deposito>12/10/2012</data_deposito>
    <tipo_procedimento>GIUDIZIO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>improcedibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato, sorto a seguito della deliberazione della Camera dei deputati del 22 settembre 2010 relativa alla insindacabilità, ai sensi dell'articolo 68, primo comma, della Costituzione, delle opinioni espresse dall'on. Silvio Berlusconi nei confronti dell'on. Antonio Di Pietro, promosso dal Giudice dell'udienza preliminare del Tribunale ordinario di Bergamo con ricorso notificato il 9 maggio 2012, depositato in cancelleria il 7 giugno 2012 ed iscritto al n. 10 del registro conflitti tra poteri dello Stato 2010, fase di merito.&#13;
 Udito nella camera di consiglio del 19 settembre 2012 il Giudice relatore Gaetano Silvestri.</epigrafe>
    <testo>Ritenuto che il Giudice dell'udienza preliminare del Tribunale ordinario di Bergamo, con ricorso del 23 novembre 2010, depositato il 6 dicembre successivo, ha sollevato conflitto di attribuzione tra poteri dello Stato in riferimento alla deliberazione del 22 settembre 2010 della Camera dei deputati, con cui si è affermato che i fatti per i quali è in corso procedimento penale a carico del deputato Silvio Berlusconi, per il reato di diffamazione aggravata, concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, e sono pertanto insindacabili ai sensi dell'art. 68, primo comma, della Costituzione;&#13;
 che il ricorrente procede nei confronti del deputato Silvio Berlusconi, in particolare, per il reato di cui agli articoli 595 del codice penale (Diffamazione), 13 della legge 8 febbraio 1948, n. 47 (Disposizioni sulla stampa) e 30 della legge 6 agosto 1990, n. 223 (Disciplina del sistema radiotelevisivo pubblico e privato), commesso ai danni del deputato Antonio Di Pietro, il quale ha proposto querela ritenendo che la sua reputazione sia stata offesa da alcune dichiarazioni rese dall'imputato nel corso della trasmissione televisiva "Porta a Porta", in data 10 aprile 2008;&#13;
 che il deputato Berlusconi avrebbe tra l'altro affermato: «Di Pietro è un altro emerito bugiardo. Tenga presente che non ha nemmeno una laurea valida. Mi rivolgo qui al Ministro dell'istruzione in carica per vedere se può [...] sottoporre a custodia sicura le documentazioni che esistono presso l'Università circa la laurea del signor Di Pietro. Mi rivolgo al Ministro della giustizia per vedere che possa fare la stessa cosa, per sottoporre a custodia i documenti con cui il signor Di Pietro si è rivolto alla magistratura e ha fatto due o tre concorsi per la magistratura. Non ha mai presentato il diploma originale di laurea. Ha sempre presentato dei certificati, che tra l'altro sono diversi uno dall'altro, sia per il voto di un esame, sia per quanto riguarda la data di un esame. Quindi la sua è una cosiddetta laurea dei servizi, che i servizi hanno chiesto ai professori dell'università di cui nessuno si ricorda di Di Pietro. Quindi il signor Di Pietro non è solo un uomo che mi fa orrore perché non rispetta gli altri e perché ha scaraventato in galera, rovinando le vite degli altri cittadini, è un assoluto bugiardo»;&#13;
 che anzitutto, secondo il Tribunale ricorrente, le espressioni appena trascritte non potrebbero considerarsi «manifestazione di un'opinione, per di più di carattere politico o di rilievo parlamentare, in quanto hanno ad oggetto fatti riguardanti la professione di magistrato svolta da Di Pietro prima di intraprendere la carriera politica, da quest'ultimo ritenuti falsi e quindi lesivi della sua reputazione»;&#13;
 che dunque le dichiarazioni in esame, proprio in quanto riferite a fatti concreti, e non ad opinioni, non potrebbero costituire oggetto della prerogativa di insindacabilità regolata dall'art. 68 Cost.;&#13;
 che d'altronde, sempre a giudizio del ricorrente, non vi sarebbe alcun atto tipico della funzione parlamentare riferibile al deputato Berlusconi e connesso alle affermazioni ritenute diffamatorie, tale da integrare quel «nesso funzionale» che la giurisprudenza costituzionale considera presupposto indefettibile per l'applicabilità dell'art. 68, primo comma, Cost.;&#13;
 che sarebbe inconferente, in particolare, il riferimento della delibera impugnata al prosieguo delle dichiarazioni compiute dal deputato Berlusconi nel corso della trasmissione televisiva, riguardo al tema di interesse politico ed istituzionale della separazione delle carriere dei magistrati, posto che il tema stesso «non risulta correlato ad iniziative parlamentari tipiche recenti, né riproduttivo di opinioni espresse sempre di recente in sede parlamentare, in modo da manifestare una finalità divulgativa delle esternazioni rispetto ad uno specifico intervento parlamentare»;&#13;
 che sarebbero privi di rilievo, per altro verso, i richiami alla qualità altamente conflittuale che segnerebbe da molti anni la relazione tra imputato e querelante, tanto più in epoca prossima ad elezioni politiche come quella di svolgimento dei fatti, non trattandosi di circostanza inerente all'attività parlamentare;&#13;
 che la Camera dei deputati, di conseguenza, avrebbe proceduto al di fuori delle attribuzioni conferite dagli artt. 55 e seguenti Cost., invadendo quelle spettanti alla magistratura secondo il disposto degli artt. 102 e seguenti della stessa Costituzione.&#13;
 Considerato che il Giudice dell'udienza preliminare del Tribunale ordinario di Bergamo ha sollevato conflitto di attribuzione tra poteri dello Stato in riferimento alla deliberazione del 22 settembre 2010 con cui la Camera dei deputati ha affermato che i fatti per i quali è in corso procedimento penale a carico del deputato Silvio Berlusconi, per il reato di diffamazione aggravata in danno del deputato Antonio Di Pietro, concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e sono pertanto insindacabili ai sensi dell'art. 68, primo comma, della Costituzione;&#13;
 che il conflitto è stato dichiarato ammissibile con ordinanza n. 147 del 2011, mediante la quale questa Corte ha assegnato al ricorrente il termine di sessanta giorni per la notifica alla Camera dei deputati dell'atto introduttivo e dello stesso provvedimento di ammissibilità, con l'indicazione dell'ulteriore termine di trenta giorni, a far data dalla notifica, per depositare gli atti presso la cancelleria della Corte costituzionale;&#13;
 che il Giudice dell'udienza preliminare del Tribunale ordinario di Bergamo, secondo quanto risulta dagli atti, ha ricevuto comunicazione dell'ordinanza indicata il giorno 22 aprile 2011, a mezzo del servizio postale;&#13;
 che, sempre dalla documentazione in atti, emerge come l'ufficiale giudiziario di Roma sia stato richiesto di procedere alla notifica del ricorso e del provvedimento dichiarativo dell'ammissibilità solo in data 8 maggio 2012, perfezionando l'adempimento il giorno successivo;&#13;
 che il ricorrente, ricevuti gli atti in restituzione il 23 maggio 2012, ha provveduto al prescritto deposito, presso la cancelleria della Corte costituzionale, il giorno 7 giugno 2012 (data di ricezione del plico spedito mediante il servizio postale);&#13;
 che, alla luce delle circostanze indicate, deve essere dichiarata l'improcedibilità del conflitto;&#13;
 che infatti, secondo la costante giurisprudenza di questa Corte, il termine fissato per la notifica alle parti confliggenti del ricorso che promuove un conflitto tra poteri dello Stato deve essere osservato a pena di decadenza, data l'esigenza costituzionale che il giudizio, una volta instaurato, sia concluso in tempi certi, non rimessi all'iniziativa del ricorrente (ordinanza n. 163 del 2009, sentenza n. 316 del 2006; ordinanze nn. 304 e 295 del 2006, sentenza n. 88 del 2005).</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara improcedibile il giudizio per conflitto di attribuzione tra poteri dello Stato promosso dal Giudice dell'udienza preliminare del Tribunale ordinario di Bergamo, nei confronti della Camera dei deputati, con il ricorso indicato in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 ottobre 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Gaetano SILVESTRI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 12 ottobre 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
