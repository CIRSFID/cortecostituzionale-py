<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>432</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:432</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/12/1993</data_decisione>
    <data_deposito>14/12/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 52 della legge    &#13;
 9 marzo 1989, n. 88 (Ristrutturazione dell'Istituto  nazionale  della    &#13;
 previdenza  sociale  e  dell'Istituto  nazionale  per l'assicurazione    &#13;
 contro gli infortuni sul lavoro), promosso con ordinanza emessa il 19    &#13;
 aprile 1993 dal Pretore di Perugia nel procedimento  civile  vertente    &#13;
 tra Ornella Contini e l'Ente nazionale di assistenza per gli agenti e    &#13;
 rappresentanti  di  commercio  (ENASARCO),  iscritta  al  n.  276 del    &#13;
 registro ordinanze 1993 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 25, prima serie speciale, dell'anno 1993;                  &#13;
    Visti  l'atto  di  costituzione  dell'ENASARCO  nonché  l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito nell'udienza  pubblica  del  16  novembre  1993  il  Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Uditi  l'avvocato  Bartolo  Spallina  per  l'ENASARCO e l'avvocato    &#13;
 dello Stato  Carlo  Carbone  per  il  Presidente  del  Consiglio  dei    &#13;
 ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  corso  di  un procedimento promosso da Ornella Contini    &#13;
 contro l'Ente nazionale di assistenza per gli agenti e rappresentanti    &#13;
 di commercio (ENASARCO) per  l'accertamento  dell'irripetibilità  di    &#13;
 somme   corrisposte   alla   ricorrente   a  titolo  di  pensione  di    &#13;
 riversibilità, somme che l'Ente assumeva indebitamente riscosse,  il    &#13;
 Pretore  di  Perugia,  con  ordinanza  emessa  il  19 aprile 1993, ha    &#13;
 sollevato, in riferimento all'art. 3 della Costituzione, questione di    &#13;
 legittimità costituzionale dell'art. 52 della legge 9 marzo 1989, n.    &#13;
 88 (Ristrutturazione dell'Istituto nazionale della previdenza sociale    &#13;
 e dell'Istituto nazionale per l'assicurazione  contro  gli  infortuni    &#13;
 sul  lavoro), nella parte in cui non prevede anche per le prestazioni    &#13;
 erogate dall'ENASARCO l'irripetibilità  dell'indebito  pensionistico    &#13;
 percepito  in  buona  fede,  così  come  invece  lo  stesso  art. 52    &#13;
 stabilisce per le pensioni corrisposte dall'INPS. Per  queste  ultime    &#13;
 non  si  fa  luogo  a  recupero  delle  somme  corrisposte, salvo che    &#13;
 l'indebita percezione sia dovuta a dolo dell'interessato, nel caso in    &#13;
 cui, in conseguenza di un provvedimento che modifica, per  un  errore    &#13;
 di  qualsiasi  natura,  la  prestazione  pensionistica,  siano  state    &#13;
 riscosse rate risultanti non dovute.                                     &#13;
    Ad  avviso  del  giudice  rimettente  esiste  una  disparità   di    &#13;
 trattamento tra coloro che godono di trattamenti pensionistici, anche    &#13;
 integrativi,  erogati  dall'INPS, ai quali si applica l'art. 52 della    &#13;
 legge n. 88 del 1989, e coloro ai quali le pensioni sono  corrisposte    &#13;
 dall'ENASARCO,   tenuti  invece  a  restituire  quanto  indebitamente    &#13;
 percepito in buona fede. La diversa disciplina sarebbe irrazionale ed    &#13;
 ingiustificata, perché per entrambe  le  categorie  di  soggetti  la    &#13;
 tutela  previdenziale  trova  il  suo  fondamento  nell'art. 38 della    &#13;
 Costituzione.                                                            &#13;
    Ad avviso del Pretore rimettente la questione non è superata  per    &#13;
 effetto  dell'art.  13  della  legge  30  dicembre 1991, n. 412, che,    &#13;
 nell'interpretare autenticamente l'art. 52  della  legge  n.  88  del    &#13;
 1989,  ha  in  realtà  profondamente innovato il sistema. Difatti è    &#13;
 stata dichiarata l'illegittimità costituzionale della  disposizione,    &#13;
 formulata  come  interpretativa,  nella  parte  in cui è applicabile    &#13;
 anche ai rapporti anteriori o pendenti alla data della sua entrata in    &#13;
 vigore (sentenza n. 39 del 1993). Di qui, ad avviso  del  Pretore  di    &#13;
 Perugia,  la  persistente  rilevanza  della  questione,  in quanto le    &#13;
 fattispecie  che  si  sono  esaurite  prima  dell'entrata  in  vigore    &#13;
 dell'art.  13  della  legge  n.  421  del  1991  continuano ad essere    &#13;
 regolate dalla norma denunciata, secondo il suo testo originario.        &#13;
    2. - Si è costituito in giudizio  l'ENASARCO,  chiedendo  che  la    &#13;
 questione  sia  dichiarata  inammissibile  e, comunque, infondata. La    &#13;
 disposizione censurata sarebbe frutto di una scelta  del  legislatore    &#13;
 nell'ambito  di una pluralità di discipline astrattamente possibili;    &#13;
 inoltre si sarebbe in presenza di sistemi  previdenziali  diversi  ed    &#13;
 autonomi,  tra  loro  non  comparabili, tanto più che le prestazioni    &#13;
 erogate dall'ENASARCO  sono  integrative  ed  aggiuntive  rispetto  a    &#13;
 quelle erogate dall'INPS. Nel merito l'Ente afferma che la diversità    &#13;
 di   disciplina,   in   presenza   di  un  trattamento  previdenziale    &#13;
 integrativo  e  soggetto  ad  una  speciale   regolamentazione,   non    &#13;
 violerebbe il principio di eguaglianza.                                  &#13;
    Tali   argomenti   sono   stati   poi   ulteriormente   illustrati    &#13;
 dall'ENASARCO in una memoria depositata in prossimità dell'udienza.     &#13;
    3. - È intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  concludendo  per  l'inammissibilità  e,  nel   merito,   per    &#13;
 l'infondatezza della questione.<diritto>Considerato in diritto</diritto>1.   -   Il   Pretore   di   Perugia   dubita  della  legittimità    &#13;
 costituzionale dell'art. 52 della legge 9  marzo  1989,  n.  88,  che    &#13;
 dispone  non  si  faccia  luogo  a  recupero  delle somme corrisposte    &#13;
 dall'INPS ai propri  assicurati,  se  sono  state  riscosse  rate  di    &#13;
 pensione   risultate   non   dovute  a  seguito  della  modifica  del    &#13;
 provvedimento di  attribuzione,  erogazione  o  riliquidazione  della    &#13;
 prestazione,  salvo  che  l'indebita  percezione  sia  dovuta  a dolo    &#13;
 dell'interessato. Il Pretore ritiene che questa disposizione  sia  in    &#13;
 contrasto  con l'art. 3 della Costituzione, non avendo il legislatore    &#13;
 disposto in modo analogo per le prestazioni erogate dall'ENASARCO.       &#13;
    2. - La disposizione denunciata disciplina, nel  contesto  di  una    &#13;
 legge  concernente  la  ristrutturazione dell'INPS (e dell'INAIL), la    &#13;
 ripetizione dell'indebito relativo a  rate  di  pensioni  corrisposte    &#13;
 dall'INPS,   delineando   un   trattamento  più  favorevole  per  il    &#13;
 percipiente rispetto a quello generale, previsto dall'art.  2033  del    &#13;
 codice  civile;  non  tocca  il  diritto  dell'ENASARCO di trattenere    &#13;
 l'ammontare delle somme ad esso dovute  dagli  iscritti  a  qualsiasi    &#13;
 titolo (art. 28 della legge 2 febbraio 1973, n. 12).                     &#13;
    Il  giudice  rimettente  non individua la norma che è chiamato ad    &#13;
 applicare al caso sottoposto al suo  giudizio  e  che,  imponendo  la    &#13;
 ripetizione dell'indebito per rate di pensione erogate dall'ENASARCO,    &#13;
 attribuirebbe   al   titolare   della   pensione,   per   l'ammontare    &#13;
 erroneamente percepito, una posizione  ingiustificatamente  deteriore    &#13;
 rispetto a quella, ritenuta analoga, dei titolari di pensione erogata    &#13;
 dall'INPS,  ai  quali  invece  si  applica  la più favorevole regola    &#13;
 prevista dall'art. 52 della legge n. 88 del 1989.                        &#13;
    Risulta  chiaramente  come  la  disciplina  che   costituisce   il    &#13;
 parametro  di  comparazione  per  affermare  la lesione del principio    &#13;
 costituzionale di eguaglianza è anche indicata  dal  giudice  a  quo    &#13;
 quale  oggetto  del  giudizio.  Nei  termini  in  cui  è  formulata,    &#13;
 l'ordinanza  di  rimessione  non  individua  quindi  la  norma   che,    &#13;
 consentendo   la   ripetibilità   delle  prestazioni  pensionistiche    &#13;
 indebite  erogate  dall'ENASARCO, lederebbe la parità di trattamento    &#13;
 per gli assicurati da questo Ente. Così  posta,  la  questione  deve    &#13;
 essere dichiarata inammissibile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 52 della  legge  9  marzo  1989,  n.  88  (Ristrutturazione    &#13;
 dell'Istituto  nazionale  della  previdenza  sociale  e dell'Istituto    &#13;
 nazionale per  l'assicurazione  contro  gli  infortuni  sul  lavoro),    &#13;
 sollevata,  in riferimento all'art. 3 della Costituzione, dal Pretore    &#13;
 di Perugia con l'ordinanza indicata in epigrafe.                         &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 1° dicembre 1993.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 14 dicembre 1993.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
