<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1959</anno_pronuncia>
    <numero_pronuncia>16</numero_pronuncia>
    <ecli>ECLI:IT:COST:1959:16</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/02/1959</data_decisione>
    <data_deposito>09/03/1959</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Avv. &#13;
 GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - Dott. &#13;
 MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. GIUSEPPE &#13;
 CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA, Giudici,</collegio>
    <epigrafe>ha deliberato in camera di consiglio la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale del D.P.R. 16 settembre  &#13;
 1951,  n.  1022,  promosso  con  ordinanza  5  gennaio  1958 emessa dal  &#13;
 Tribunale di Cosenza nel procedimento  civile  vertente  tra  Cosentino  &#13;
 Giuseppe  e l'Opera per la valorizzazione della Sila, iscritta al n. 17  &#13;
 del Registro di ordinanze 1958 e pubblicata  nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n. 101 del 26 aprile 1958.                              &#13;
     Ritenuto  che  con  sentenza 15 maggio 1957, n. 72, questa Corte ha  &#13;
 dichiarato "costituzionalmente illegittimo il D.P.R. 16 settembre 1951,  &#13;
 n. 1022, per la parte in cui espropria terreni compresi nei  limiti  di  &#13;
 300  ettari,  in  riferimento alle norme contenute negli artt. 76 e 77,  &#13;
 primo comma, della Costituzione e nell'art. 2  della  legge  12  maggio  &#13;
 1950, n. 230";                                                           &#13;
     che,  riassunta la causa dinanzi al Tribunale di Cosenza, la difesa  &#13;
 dell'Opera per la valorizzazione della Sila ha eccepito che la data del  &#13;
 15 novembre 1949 segnerebbe soltanto il momento al quale fare  risalire  &#13;
 l'inizio  del  vincolo di indisponibilità per i soggetti espropriandi,  &#13;
 ma non escluderebbe dall'esproprio i terreni entrati nel patrimonio del  &#13;
 soggetto colpito, posteriormente a detta data;                           &#13;
     che,  in  conseguenza,  dovrebbe  tenersi  conto  della   divisione  &#13;
 intervenuta  fra  i  germani  Giuseppe,  Rodolfo  e Nicola Cosentino fu  &#13;
 Francesco con l'atto notar Cizza del 25 aprile 1950,  in  virtù  della  &#13;
 quale a Giuseppe Cosentino, in epoca antecedente alla entrata in vigore  &#13;
 della  legge  12  maggio  1950, n. 230, vennero assegnati sulla partita  &#13;
 catastale 166 non già ha 170.13.77,  come  calcolato  dal  consulente,  &#13;
 bensì ha 215.13.00;                                                     &#13;
     che  il  Tribunale  di  Cosenza,  ritenuto  che  codesta  eccezione  &#13;
 proponga una questione di legittimità costituzionale rilevante per  il  &#13;
 giudizio  di  merito  e  non  manifestamente infondata, ha disposto con  &#13;
 ordinanza emessa  il  5  gennaio  1958,  notificata  alle  parti  e  al  &#13;
 Presidente  del Consiglio dei Ministri e comunicata ai Presidenti delle  &#13;
 due Camere, la sospensione del giudizio e la trasmissione degli atti  a  &#13;
 questa Corte;                                                            &#13;
     che  soltanto  il signor Giuseppe Cosentino, rappresentato e difeso  &#13;
 dall'avv. Rodolfo Grimaldi, si è costituito nel presente  giudizio  di  &#13;
 legittimità  costituzionale  con  atto  di  deduzioni depositato nella  &#13;
 cancelleria della Corte il 15  maggio  1958,  chiedendo  che  la  Corte  &#13;
 costituzionale in via pregiudiziale riconosca e dichiari il suo difetto  &#13;
 di   potere   per   l'esame   della  nuova  questione  di  legittimità  &#13;
 costituzionale in quanto  attinente  ad  un  provvedimento  legislativo  &#13;
 delegato   già   dichiarato   con   sua   sentenza  costituzionalmente  &#13;
 illegittimo o, in via  subordinata,  la  manifesta  infondatezza  della  &#13;
 sollevata questione di costituzionalità;                                &#13;
     che  il  signor  Giuseppe  Cosentino  con  memoria depositata nella  &#13;
 cancelleria di questa Corte il 3 febbraio 1959 ha ribadito e illustrato  &#13;
 le sue tesi difensive;                                                   &#13;
     Considerato che la Corte ha  dichiarato  l'illegittimità  soltanto  &#13;
 parziale  del  decreto delegato in questione, e che, pertanto, nulla si  &#13;
 opporrebbe, in linea di massima, all'esame di questioni di legittimità  &#13;
 costituzionale di quelle  parti  del  decreto  che  non  fossero  state  &#13;
 travolte dalla dichiarazione di incostituzionalità;                     &#13;
     che,  peraltro, la questione di legittimità nuovamente proposta è  &#13;
 relativa alla parte del decreto delegato che  la  Corte  ha  dichiarato  &#13;
 costituzionalmente   illegittimo,  sulla  quale  dichiarazione  non  è  &#13;
 consentito ritornare;                                                    &#13;
     che, comunque, la questione medesima è  manifestamente  infondata,  &#13;
 poiché, giusta la costante giurisprudenza di questa Corte, la data del  &#13;
 15  novembre  1949  è  da considerare il termine al quale occorre fare  &#13;
 rigoroso riferimento per l'accertamento della titolarità e consistenza  &#13;
 dei beni soggetti a scorporo, tanto in favore  quanto  in  danno  così  &#13;
 degli enti esproprianti come delle persone soggette a esproprio;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale del D.P.R. 16 settembre 1951,  n.  1022,  sollevata  con  &#13;
 l'ordinanza  sopra  citata  e  ordina  la  restituzione  degli  atti al  &#13;
 Tribunale di Cosenza.                                                    &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 18 febbraio 1959.       &#13;
                                   GAETANO  AZZARITI  - GIUSEPPE CAPPI -  &#13;
                                   TOMASO PERASSI - GASPARE AMBROSINI  -  &#13;
                                   MARIO    COSATTI - FRANCESCO PANTALEO  &#13;
                                   GABRIELI - GIUSEPPE  CASTELLI  AVOLIO  &#13;
                                   ANTONINO PAPALDO - GIOVANNI CASSANDRO  &#13;
                                   BIAGIO PETROCELLI - ANTONIO MANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
