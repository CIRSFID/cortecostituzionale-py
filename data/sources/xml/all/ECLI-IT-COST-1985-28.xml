<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>28</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:28</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/01/1985</data_decisione>
    <data_deposito>30/01/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - AVV. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - AVV. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO &#13;
 LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. &#13;
 ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di  legittimità  costituzionale dell'art. 1  &#13;
 della legge 22 maggio 1975, n. 152 (Disposizioni a  tutela  dell'ordine  &#13;
 pubblico),  e  dell'art.  8  del regio decreto-legge 20 luglio 1934, n.  &#13;
 1404, convertito in  legge  27  maggio  1935,  n.  835  (Istituzione  e  &#13;
 funzionamento  del tribunale per i minorenni), sostituito dall'articolo  &#13;
 unico della legge 25  luglio  1956,  n.  888  (Modificazioni  al  regio  &#13;
 decreto-legge  20  luglio  1934, n. 1404, convertito in legge 27 maggio  &#13;
 1935, n. 835, sull'istituzione e  funzionamento  del  tribunale  per  i  &#13;
 minorenni), promossi con le seguenti ordinanze:                          &#13;
     1)  ordinanza  emessa  il  12  dicembre  1978  dal  Tribunale per i  &#13;
 minorenni di Roma nel procedimento penale a  carico  di  Caruso  Marco,  &#13;
 iscritta  al  n.  74  del  registro  ordinanze  1979 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 87 dell'anno 1979;                &#13;
     2) n. 2 ordinanze emesse il 28 novembre e il 12 dicembre  1978  dal  &#13;
 Tribunale  per  i minorenni di Roma nel procedimento penale a carico di  &#13;
 Leoncini Maurizio, entrambe iscritte al n. 622 del  registro  ordinanze  &#13;
 1979  e  pubblicate  nella  Gazzetta  Ufficiale della Repubblica n. 304  &#13;
 dell'anno 1979;                                                          &#13;
     3) ordinanza emessa il 2 giugno 1980 dal Giudice istruttore  presso  &#13;
 il  Tribunale  di  Pinerolo nel procedimento penale a carico di Rotondo  &#13;
 Francesco, iscritta al n. 700 del registro ordinanze 1980 e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 318 dell'anno 1980.         &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del  4  dicembre  1984  il  Giudice  &#13;
 relatore Giovanni Conso.                                                 &#13;
     Rilevato che il Tribunale per i minorenni di Roma con due ordinanze  &#13;
 del 12 dicembre 1978, emesse nel corso dei procedimenti penali a carico  &#13;
 di  Caruso Marco (R.O. n. 74 del 1979) e Leoncini Maurizio (R.O. n. 622  &#13;
 del 1979), e il Giudice istruttore presso il Tribunale di Pinerolo  con  &#13;
 ordinanza del 2 giugno 1980, emessa nel corso del procedimento penale a  &#13;
 carico di Rotondo Francesco (R.O. n. 700 del 1980), hanno sollevato, in  &#13;
 riferimento  all'art.  3  della Costituzione, questione di legittimità  &#13;
 dell'art. 1 della legge 22 maggio 1975, n. 152, nella parte in cui  non  &#13;
 consente   la  concessione  della  libertà  provvisoria  ai  minorenni  &#13;
 imputati di reati tassativamente indicati in detta norma;                &#13;
     che il Tribunale per i minorenni di Roma, nel  corso  del  medesimo  &#13;
 procedimento penale a carico di Leoncini Maurizio, con ordinanza del 28  &#13;
 novembre  1978,  trasmessa  a  questa  Corte  contestualmente  a quella  &#13;
 pronunciata il 12 dicembre 1978 con lo  stesso  fascicolo  processuale,  &#13;
 tanto  da  venire  iscritta  ad  uno stesso numero di Registro Generale  &#13;
 (R.O. n. 622 del 1979), ha sollevato, in riferimento agli artt. 3 e  24  &#13;
 della  Costituzione,  questione  di  legittimità dell'art. 8 del regio  &#13;
 decreto-legge 20 luglio 1934, n. 1404, convertito in  legge  27  maggio  &#13;
 1935,  n.  835,  sostituito  dall'articolo  unico della legge 25 luglio  &#13;
 1956, n. 888, "nella parte in cui rende  inevitabile  il  trasferimento  &#13;
 dell'imputato  in  carcerazione preventiva - minorenne al momento della  &#13;
 contestazione del reato - dall'istituto  di  osservazione  all'istituto  &#13;
 per adulti, al compimento del diciottesimo anno di età";                &#13;
     e che in tutti i giudizi è intervenuta la Presidenza del Consiglio  &#13;
 dei  ministri,  rappresentata  e  difesa dall'Avvocatura Generale dello  &#13;
 Stato,  chiedendo  che  entrambe  le  questioni  siano  dichiarate  non  &#13;
 fondate;                                                                 &#13;
     considerato, quanto alla prima questione, che, successivamente alla  &#13;
 pronuncia  delle  ordinanze  di  rimessione, è intervenuta la legge 28  &#13;
 luglio 1984, n. 398 (Norme relative alla  diminuzione  dei  termini  di  &#13;
 carcerazione  cautelare e alla concessione della libertà provvisoria),  &#13;
 il cui art. 28 ha espressamente abrogato l'art. 1 della  legge  n.  152  &#13;
 del 1975;                                                                &#13;
     che,  di  conseguenza,  va  disposta  la restituzione degli atti ai  &#13;
 giudici a quibus, affinché  rivalutino  la  rilevanza  della  proposta  &#13;
 questione alla stregua della nuova normativa;                            &#13;
     e  che  la seconda questione richiede un più puntuale, aggiornato,  &#13;
 esame,  oltre  che  della  rilevanza,   anche   della   non   manifesta  &#13;
 infondatezza:    da  un  lato,  perché si valuti l'eventuale incidenza  &#13;
 della nuova disciplina sulla norma impugnata;  dall'altro,  perché  si  &#13;
 riconsideri l'attuale vigenza del regio-decreto 18 giugno 1931, n. 787,  &#13;
 del  quale  peraltro  viene invocato il solo art. 28 e non anche l'art.  &#13;
 29,  relativo  tanto  alla  "esecuzione   della   pena"   quanto   alla  &#13;
 "carcerazione preventiva".</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione degli atti al Tribunale per i minorenni di  &#13;
 Roma e al Giudice istruttore presso il Tribunale di Pinerolo.            &#13;
     Così  deciso in Roma, in camera di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 gennaio 1985.        &#13;
                                   F.to:   LEOPOLDO   ELIA  -  GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
