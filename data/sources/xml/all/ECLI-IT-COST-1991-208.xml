<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>208</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:208</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/04/1991</data_decisione>
    <data_deposito>13/05/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art. 563, quarto    &#13;
 comma, del codice di procedura penale, "e, in ipotesi, del  combinato    &#13;
 disposto  degli  artt.  446, primo comma, 549 e 563, primo comma, del    &#13;
 codice di procedura penale", promossi con  12  ordinanze  emesse  dal    &#13;
 Pretore  di  Perugia, iscritte rispettivamente ai nn. 22, 36, 37, 38,    &#13;
 54, 55, 56, 57,  74,  75,  76,  77  del  registro  ordinanze  1991  e    &#13;
 pubblicate  nelle Gazzette Ufficiali della Repubblica artt. 6, 7 e 8,    &#13;
 prima serie speciale, dell'anno 1991;                                    &#13;
    Visti gli atti di intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 10 aprile 1991 il Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto che il  Pretore  di  Perugia,  con  dodici  ordinanze  di    &#13;
 contenuto identico, ha sollevato - in riferimento agli artt. 76 e 25,    &#13;
 primo   comma,   della   Costituzione  -  questione  di  legittimità    &#13;
 costituzionale dell'art. 563, quarto comma, del codice  di  procedura    &#13;
 penale, "e, in ipotesi, del combinato disposto degli artt. 446, primo    &#13;
 comma, 549 e 563, primo comma, del codice di procedura penale", nella    &#13;
 parte in cui consente all'imputato, anche nel procedimento pretorile,    &#13;
 di presentare la richiesta prevista nell'art. 444, primo comma, dello    &#13;
 stesso  codice  dopo  la  scadenza  del  termine  di  15 giorni dalla    &#13;
 notificazione del decreto di citazione a giudizio  (art.  555,  primo    &#13;
 comma,   lett.   e),  e  fino  alla  dichiarazione  di  apertura  del    &#13;
 dibattimento;                                                            &#13;
      che il giudice remittente osserva,  innanzitutto,  in  punto  di    &#13;
 rilevanza,  che  a suo avviso è unicamente l'art. 563, quarto comma,    &#13;
 del codice di procedura penale  (secondo  cui  "Se  la  richiesta  è    &#13;
 formulata dopo la scadenza del termine previsto nell'art. 555 comma 1    &#13;
 lettera  e), è competente a decidere il pretore del dibattimento") a    &#13;
 consentire all'imputato la anzidetta facoltà, ma che, tuttavia,  ove    &#13;
 la  disposizione  impugnata  dovesse  ritenersi meramente esplicativa    &#13;
 della disciplina comunque applicabile al giudizio pretorile in virtù    &#13;
 del richiamo operato dagli artt. 549 e  563,  primo  comma,  all'art.    &#13;
 446,  primo  comma,  del  codice  di  procedura  penale, la sollevata    &#13;
 questione di costituzionalità  andrebbe,  in  tale  ipotesi,  estesa    &#13;
 anche al combinato disposto di queste ultime norme;                      &#13;
      che,  in  punto  di non manifesta infondatezza, il giudice a quo    &#13;
 osserva che la normativa in esame viola, in primo  luogo,  l'art.  76    &#13;
 della  Costituzione,  per  contrasto  con  la  direttiva n. 103 della    &#13;
 legge-delega 16 febbraio 1987, n. 81, la quale renderebbe necessitata    &#13;
 un'ulteriore semplificazione degli istituti  del  giudizio  pretorile    &#13;
 rispetto  a  quelli previsti per il procedimento dinanzi al tribunale    &#13;
 e,    in    particolare,    richiederebbe,    quanto     all'istituto    &#13;
 dell'applicazione  della  pena  su  richiesta,  l'eliminazione  della    &#13;
 possibilità  per  l'imputato  di  formulare  tale   richiesta   fino    &#13;
 all'apertura  del  dibattimento  -  come  stabilito  nei  giudizi  di    &#13;
 tribunale - anziché entro il più ristretto  termine  di  15  giorni    &#13;
 dalla notificazione del decreto di citazione;                            &#13;
      che  il  prolungamento  del  termine  fino alla dichiarazione di    &#13;
 apertura del dibattimento, prosegue il  giudice  remittente,  sarebbe    &#13;
 del  tutto incongruo nel giudizio pretorile, stante l'assenza in tale    &#13;
 procedimento  dell'udienza   preliminare   e   la   cristallizzazione    &#13;
 dell'accusa  nel decreto di citazione, per cui il detto prolungamento    &#13;
 avrebbe l'unica conseguenza di rendere necessario  il  compimento  di    &#13;
 una   serie   di   incombenti   finalizzati   alla  celebrazione  del    &#13;
 dibattimento e tuttavia suscettibili di essere posti nel nulla da una    &#13;
 successiva richiesta di  patteggiamento  avanzata  dall'imputato  col    &#13;
 consenso del pubblico ministero;                                         &#13;
      che,  infine,  la  norma impugnata violerebbe anche il principio    &#13;
 del giudice naturale precostituito di cui all'art. 25,  primo  comma,    &#13;
 della  Costituzione, consentendo all'imputato di scegliere il giudice    &#13;
 competente a  decidere  sulla  richiesta  (giudice  per  le  indagini    &#13;
 preliminari  o  pretore  del  dibattimento) sulla base della semplice    &#13;
 opzione in  ordine  alla  fase  del  giudizio  in  cui  formulare  la    &#13;
 richiesta stessa;                                                        &#13;
      che è intervenuto nei presenti giudizi (tranne che in quelli di    &#13;
 cui  alle  ordinanze  artt.  37, 38 e 56 reg. 1991) il Presidente del    &#13;
 Consiglio  dei  ministri,  concludendo   per   l'infondatezza   della    &#13;
 questione;                                                               &#13;
    Considerato  che  i giudizi, concernendo identica questione, vanno    &#13;
 riuniti e decisi congiuntamente;                                         &#13;
      che, innanzitutto, va  ritenuta  ammissibile  esclusivamente  la    &#13;
 questione   relativa  all'art.  563,  quarto  comma,  del  codice  di    &#13;
 procedura penale, in quanto è questa la sola  disposizione  che,  ad    &#13;
 avviso  del  giudice  a  quo,  consente  all'imputato  la facoltà di    &#13;
 richiedere l'applicazione  della  pena  fino  alla  dichiarazione  di    &#13;
 apertura del dibattimento, ed è, quindi, l'unica che egli ritiene di    &#13;
 dover applicare nella fattispecie;                                       &#13;
     che,  di  conseguenza,  la  questione  concernente  il  combinato    &#13;
 disposto degli artt. 446, primo comma, 549  e  563,  primo  e  quarto    &#13;
 comma,  del  codice  di  procedura penale, essendo prospettata in via    &#13;
 subordinata ed in forma meramente ipotetica (ipotesi, peraltro, dallo    &#13;
 stesso giudice  a  quo  chiaramente  non  condivisa),  va  dichiarata    &#13;
 manifestamente  inammissibile,  secondo la costante giurisprudenza di    &#13;
 questa Corte (cfr., ad esempio, sentt. nn.  182  del  1984,  146  del    &#13;
 1985, 456 del 1989);                                                     &#13;
      che,  nel  merito,  la  dedotta  violazione  della  legge-delega    &#13;
 evidentemente non sussiste, in quanto la direttiva artt.  103  lascia    &#13;
 al legislatore delegato un ampio spazio di discrezionalità in ordine    &#13;
 alla   disciplina  delle  concrete  modalità  di  funzionamento  del    &#13;
 processo pretorile e, in particolare, in merito  alla  individuazione    &#13;
 dei  casi  e  dei  limiti  nei  quali  attuare  in  tale processo una    &#13;
 semplificazione degli istituti previsti per il  procedimento  davanti    &#13;
 al  tribunale,  onde  adeguarli  alla  naturale  maggior  celerità e    &#13;
 snellezza del giudizio pretorile;                                        &#13;
      che, nel caso di specie,  tale  discrezionalità  non  è  stata    &#13;
 certamente  adoperata  in modo irragionevole, in quanto è assorbente    &#13;
 rilevare che l'aver lasciato inalterata -  rispetto  al  procedimento    &#13;
 dinanzi  al  tribunale  -  la facoltà per l'imputato di formulare la    &#13;
 richiesta di applicazione di una pena fino all'ultimo  momento  utile    &#13;
 (dichiarazione  di  apertura  del  dibattimento) costituisce evidente    &#13;
 espressione del favor per  i  riti  differenziati  -  alternativi  al    &#13;
 dibattimento  -  la  cui  incentivazione,  come  si legge anche nella    &#13;
 relazione al progetto preliminare, mira in  definitiva  a  perseguire    &#13;
 proprio  quella  finalità  di  massima  semplificazione invocata dal    &#13;
 remittente;                                                              &#13;
      che, infine, è  chiaramente  da  escludere  la  violazione  del    &#13;
 principio   del   giudice  naturale  (art.  25,  primo  comma,  della    &#13;
 Costituzione), in quanto, come questa Corte ha già  avuto  occasione    &#13;
 di  osservare  proprio  in relazione all'istituto in esame (cfr. ord.    &#13;
 artt. 353 del  1990),  il  fatto  che  l'imputato  sia  abilitato  ad    &#13;
 avanzare   la   richiesta  di  applicazione  della  pena  nell'una  o    &#13;
 nell'altra  fase  del  giudizio  non  implica  lesione  del  suddetto    &#13;
 principio,  giacché  è  pur  sempre  la legge che precostituisce il    &#13;
 giudice competente ad applicare la pena nelle varie fasi  durante  la    &#13;
 pendenza del termine;                                                    &#13;
      che,  in  conclusione, la questione va dichiarata manifestamente    &#13;
 infondata sotto ogni profilo;                                            &#13;
    Visti gli artt. 26, secondo comma,  della  legge  11  marzo  1953,    &#13;
 artt.  87,  e 9, secondo comma, delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi:                                                     &#13;
       a)  dichiara  la  manifesta inammissibilità della questione di    &#13;
 legittimità costituzionale del combinato disposto degli  artt.  446,    &#13;
 primo comma, 549 e 563, primo e quarto comma, del codice di procedura    &#13;
 penale,  in  riferimento  agli  artt.  76  e  25,  primo comma, della    &#13;
 Costituzione, sollevata dal Pretore di Perugia con  le  ordinanze  in    &#13;
 epigrafe;                                                                &#13;
       b)  dichiara  la  manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 563, quarto comma,  del  codice    &#13;
 di  procedura penale, in riferimento agli artt. 76 e 25, primo comma,    &#13;
 della Costituzione, sollevata dal Pretore di Perugia con le  medesime    &#13;
 ordinanze.                                                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 aprile 1991.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 13 maggio 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
