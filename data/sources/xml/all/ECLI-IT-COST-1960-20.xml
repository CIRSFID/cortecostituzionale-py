<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1960</anno_pronuncia>
    <numero_pronuncia>20</numero_pronuncia>
    <ecli>ECLI:IT:COST:1960:20</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Giuseppe Branca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>31/03/1960</data_decisione>
    <data_deposito>04/04/1960</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>Composta dai signori: Dott. GAETANO AZZARITI, Presidente - Avv. &#13;
 GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - &#13;
 Prof. ERNESTO BATTAGLINI - Dott. MARIO COSATTI - Prof. GIUSEPPE &#13;
 CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. &#13;
 GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - &#13;
 Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della legge 7  novembre  &#13;
 1957,  n.  1051,  promosso  con ordinanza emessa il 3 febbraio 1959 dal  &#13;
 Pretore di Genova nel procedimento civile vertente tra Grandi Astorre e  &#13;
 la Società p. az. "Columbia", iscritta al n. 64 del Registro ordinanze  &#13;
 1959 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  99  del  &#13;
 24 aprile 1959.                                                          &#13;
     Vista  la  dichiarazione di intervento del Presidente del Consiglio  &#13;
 dei Ministri;                                                            &#13;
     udita nell'udienza pubblica del  2  marzo  1960  la  relazione  del  &#13;
 Giudice Giuseppe Branca;                                                 &#13;
     udito  il sostituito avvocato generale dello Stato Luigi Tavassi La  &#13;
 Greca per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Il Pretore di  Genova,  con  sentenza  del  3  febbraio  1959,  &#13;
 condannava  il  sig.  Astorre Grandi a pagare alla Società Columbia la  &#13;
 somma di L. 202.277 quale prezzo di merce  precedentemente  acquistata;  &#13;
 contemporaneamente,  poiché  la  società  attrice  aveva  chiesto  il  &#13;
 rimborso delle spese giudiziali, per  questa  parte  della  domanda  il  &#13;
 Pretore  proponeva questione di legittimità costituzionale della legge  &#13;
 7 novembre 1957, n.  1051, articolo unico, in riferimento agli artt. 70  &#13;
 e 76 della Costituzione: infatti l'attrice pretendeva  la  liquidazione  &#13;
 degli  onorari  d'avvocato  benché  il  suo  difensore  fosse iscritto  &#13;
 soltanto nell'albo dei procuratori:  ma il diritto a questi onorari  (o  &#13;
 meglio,   a   metà   di  questi  onorari)  deriva  dall'art.  8  della  &#13;
 deliberazione  15  febbraio  1958  del  Consiglio  nazionale   forense,  &#13;
 approvato  con  decreto  28  febbraio  1958 dal Ministro Guardasigilli,  &#13;
 cioè da una deliberazione presa in  virtù  dei  poteri  conferiti  al  &#13;
 Consiglio  dalla  legge  predetta; dimodoché, se risultasse provata la  &#13;
 illegittimità costituzionale di quest'ultima, cadrebbe l'art. 8  della  &#13;
 deliberazione  consiliare  e,  con  esso,  il diritto dell'attrice alla  &#13;
 liquidazione degli onorari d'avvocato.                                   &#13;
     Perciò lo stesso giorno, 3 febbraio 1959,  il  Pretore  di  Genova  &#13;
 emetteva  ordinanza  di  rinvio  a  questa  Corte. L'ordinanza è stata  &#13;
 regolarmente notificata il 20 febbraio 1959 e pubblicata nella Gazzetta  &#13;
 Ufficiale il 24 aprile 1959.                                             &#13;
     Il Presidente del Consiglio è intervenuto, a mezzo dell'Avvocatura  &#13;
 generale dello Stato, con deduzioni del 13 marzo 1959.                   &#13;
     2.  - Secondo l'ordinanza di rinvio, la deliberazione del Consiglio  &#13;
 nazionale  forense  è  un  regolamento  delegato;  la  delega  sarebbe  &#13;
 contenuta  nella legge 7 novembre 1957, n.  1051, articolo unico, norma  &#13;
 impugnata, che attribuisce al Consiglio  la  potestà  di  stabilire  i  &#13;
 criteri  per  la determinazione dei compensi spettanti a procuratori ed  &#13;
 avvocati; e questa norma non contiene alcuna determinazione di principi  &#13;
 direttivi e di limiti temporali e spaziali, come invece richiede l'art.  &#13;
 76  della  Costituzione  per  la  delega  dell'esercizio  di   funzioni  &#13;
 legislative;  ma  -  osserva  il  Pretore  di Genova - la materia degli  &#13;
 onorari e dei diritti forensi aveva già la sua disciplina in una legge  &#13;
 precedente (legge 13 giugno 1942, n. 794), che perciò  il  7  novembre  &#13;
 1957   è   stata   implicitamente  abrogata,  tanto  è  vero  che  la  &#13;
 deliberazione consiliare del 15 febbraio 1958 ne riproduceva  le  norme  &#13;
 apportandovi  anche  qualche  modifica:  ne  deriverebbe  che  la legge  &#13;
 impugnata ha  attribuito  al  Consiglio  funzioni  che  sono  di  norma  &#13;
 riservate  al Parlamento (art.  70 della Costituzione); per conseguenza  &#13;
 avrebbe dovuto almeno indicare  criteri  direttivi  e  apporre  limiti,  &#13;
 così  come  stabilisce  l'art.  76  della  Costituzione:  non  sarebbe  &#13;
 ammissibile che, attraverso la delega  di  potestà  regolamentari,  si  &#13;
 diano all'autorità amministrativa poteri così ampi da tradursi in una  &#13;
 rinuncia totale, da parte del Parlamento, della funzione legislativa.    &#13;
     Di qui, secondo il Pretore di Genova, la non manifesta infondatezza  &#13;
 della questione di legittimità costituzionale sollevata d'ufficio.      &#13;
     3.  -  Invece,  secondo l'Avvocatura generale dello Stato, la legge  &#13;
 impugnata  non  contiene  una  delega  di  potestà   legislativa,   ma  &#13;
 attribuisce  al  Consiglio  nazionale  una  potestà  regolamentare  in  &#13;
 materia di tariffe forensi, cioè relativamente a  un  aspetto  che  è  &#13;
 secondario nella complessa disciplina delle professioni d'avvocato e di  &#13;
 procuratore  e  sul  quale  non  c'è  alcuna riserva costituzionale di  &#13;
 legge. Perciò l'art. 76 della  Costituzione,  che  si  riferisce  alla  &#13;
 delega di potestà legislativa, sarebbe stato erroneamente invocato.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  La  tesi  dell'Avvocatura  generale dello Stato, secondo cui  &#13;
 l'articolo unico della legge 7 novembre 1957, n. 1051, non contiene una  &#13;
 delega dell'esercizio di funzioni legislative, è esatta. Lo si  ricava  &#13;
 agevolmente'  sia dal testo della norma, sia dalla natura dell'organo a  &#13;
 cui essa attribuisce certi: poteri, sia dal contenuto di questi ultimi.  &#13;
     Infatti la lettera  della  legge  non  accenna  né  fa  pensare  a  &#13;
 delegazione  dell'esercizio  di potestà che istituzionalmente spettano  &#13;
 solo' alle due Camere: il legislatore vi si limita  a  disporre  che  i  &#13;
 criteri  per  la  determinazione dei compensi ad avvocati e procuratori  &#13;
 "sono stabiliti dal  Consiglio  nazionale  forense",  e  l'attribuzione  &#13;
 d'una   competenza   come   questa,  anche  se  riguarda  materia  già  &#13;
 disciplinata minutamente dalla legge, non è di per se stessa delega di  &#13;
 potestà legislativa. Siccome  la  delega  può  esserci  soltanto  nei  &#13;
 confronti  dei  Governo  non sembra che la norma impugnata abbia inteso  &#13;
 attuarla nei riguardi del Consiglio nazionale forense.                   &#13;
     Inoltre la materia delle prestazioni forensi non è di  quelle  che  &#13;
 debbano  essere  necessariamente  disciplinate  per  legge o per delega  &#13;
 dell'esercizio di funzioni legislative:  anzi, a parte che non vi è su  &#13;
 di essa alcuna riserva  costituzionale  di  legge,  i  criteri  per  la  &#13;
 fissazione  dei compensi e le relative tariffe hanno tale natura che è  &#13;
 opportuno rivederli periodicamente: di qui la  convenienza  d'affidarne  &#13;
 l'aggiornamento  ad  un  organo  tecnico  che  sia in grado di prendere  &#13;
 tempestive' decisioni. Non per niente, mentre  nella  legge  13  giugno  &#13;
 1942,  n.  794, criteri generali e tariffe erano contenuti nello stesso  &#13;
 testo legislativo, precedentemente la loro fissazione spettava,  almeno  &#13;
 in  parte, ai direttori dei sindacati forensi e al Ministro di grazia e  &#13;
 giustizia (artt. 57 e  64  legge  27  novembre  1933,  n.  1578,  sulle  &#13;
 professioni d'avvocato e procuratore).                                   &#13;
     Sotto  questo  aspetto  la norma impugnata non viola né l'art. 76,  &#13;
 né l'art. 70 della Costituzione.                                        &#13;
     2. - Resta da vedere se la legge 7 novembre 1957, n. 1051, come  si  &#13;
 desumerebbe dall'ordinanza di rinvio e in particolare dal suo accenno a  &#13;
 una  delega  di  potestà  regolamentare, abbia attribuito al Consiglio  &#13;
 nazionale forense la potestà di emanare regolamenti in materia coperta  &#13;
 da riserve di legge  o  tali,  comunque,  che  possano  abrogare  leggi  &#13;
 vigenti.  Solo  in questo caso si potrebbe eventualmente dubitare della  &#13;
 legittimità della norma in  riferimento  agli  artt.  70  e  76  della  &#13;
 Costituzione.                                                            &#13;
     Osserva  però  questa Corte che la potestà, conferita dalla norma  &#13;
 impugnata al Consiglio nazionale forense, non invade un campo  soggetto  &#13;
 a  riserva  legislativa,  ma attua un nuovo sistema di elaborazione dei  &#13;
 criteri relativi alla misura dei compensi che è voluto  appunto  dalla  &#13;
 legge: prima del 7 novembre 1957 era il Parlamento l'organo che fissava  &#13;
 direttamente  quei  criteri;  dopo  il  7  novembre  1957  è invece il  &#13;
 Consiglio nazionale forense.                                             &#13;
     La legge 7 novembre 1957, n. 1051,  gli  ha  dato,  modificando  la  &#13;
 legislazione   precedente,   una  potestà  regolamentare  per  il  cui  &#13;
 conferimento non sussistono i  limiti  temporali  e  spaziali  indicati  &#13;
 nell'art.  76  della  Costituzione:  essa ha stabilito che il Consiglio  &#13;
 nazionale forense disciplini ogni biennio, predisponendo i  criteri  di  &#13;
 attuazione   concreta   da  sottoporre  all'approvazione  del  Ministro  &#13;
 Guardasigilli, la materia relativa a diritti a  compenso  derivanti  da  &#13;
 principi   generali   dell'ordinamento   e  da  leggi  speciali:  e  la  &#13;
 determinazione di criteri diretti all'attuazione pratica di principi  e  &#13;
 di  norme  legislative  è compito normalmente attribuito all'autorità  &#13;
 amministrativa.                                                          &#13;
     Che se poi, come afferma il Pretore  di  Genova,  la  deliberazione  &#13;
 consiliare del 15 febbraio 1958 ha toccato materia relativa alla stessa  &#13;
 esistenza  e  inderogabilità  delle  obbligazioni che intercorrono fra  &#13;
 difensori e clienti, ciò non prova che il  Consiglio  abbia  avuto  la  &#13;
 potestà  d'emanare  atti  capaci  di  abrogare  le  leggi;  semmai, si  &#13;
 tratterebbe di problema che attiene all'esercizio,  legittimo  o  meno,  &#13;
 della  potestà regolamentare conferita dalla legge 7 novembre 1957, n.  &#13;
 1051, e non può essere affrontato in questa sede.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione, proposta dal Pretore  di  Genova  &#13;
 con  l'ordinanza  3  febbraio  1959,  sulla legittimità costituzionale  &#13;
 dell'articolo unico legge 7 novembre 1957, n.    1051,  in  riferimento  &#13;
 agli artt. 70 e 76 della Costituzione.                                   &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 31 marzo 1960.                                &#13;
                                   GAETANO  AZZARITI  - GIUSEPPE CAPPI -  &#13;
                                   TOMASO PERASSI - GASPARE AMBROSINI  -  &#13;
                                   ERNESTO  BATTAGLINI - MARIO COSATTI -  &#13;
                                   GIUSEPPE CASTELLI AVOLIO  -  ANTONINO  &#13;
                                   PAPALDO  -  NICOLA  JAEGER - GIOVANNI  &#13;
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  &#13;
                                   ANTONIO   MANCA  -  ALDO  SANDULLI  -  &#13;
                                   GIUSEPPE BRANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
