<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>329</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:329</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/07/1998</data_decisione>
    <data_deposito>24/07/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  sull'ammissibilità  del conflitto di attribuzione tra    &#13;
 poteri dello Stato promosso dal pretore di Brescia nei confronti  del    &#13;
 Presidente  del  Consiglio  dei Ministri, sorto in relazione all'art.    &#13;
 74 del decreto legislativo 3 febbraio 1993, n. 29  (Razionalizzazione    &#13;
 dell'organizzazione delle amministrazioni pubbliche e revisione della    &#13;
 disciplina  in  materia di pubblico impiego a norma dell'art. 2 della    &#13;
 legge 23 ottobre 1992, n. 421),  come  sostituito  dall'art.  38  del    &#13;
 decreto  legislativo 23 dicembre 1993, n. 546 (Ulteriori modifiche al    &#13;
 decreto legislativo 3 febbraio 1993, n. 29,  sul  pubblico  impiego),    &#13;
 con  ricorso  depositato il 2 febbraio 1998, ed iscritto al n. 87 del    &#13;
 registro ammissibilità conflitti.                                       &#13;
   Udito nella camera di consiglio  del  20  maggio  1998  il  giudice    &#13;
 relatore Piero Alberto Capotosti.                                        &#13;
   Ritenuto che, con ricorso depositato il 2 febbraio 1998, il pretore    &#13;
 di  Brescia  ha  sollevato conflitto di attribuzione tra poteri dello    &#13;
 Stato nei confronti del Governo in relazione all'art. 74 del  decreto    &#13;
 legislativo    3    febbraio    1993,    n.   29   (Razionalizzazione    &#13;
 dell'organizzazione delle amministrazioni pubbliche e revisione della    &#13;
 disciplina in materia di pubblico impiego a norma dell'art.  2  della    &#13;
 legge  23  ottobre  1992, n. 421), così come sostituito dall'art. 38    &#13;
 del decreto legislativo 23 dicembre 1993, n. 546 (Ulteriori modifiche    &#13;
 al decreto legislativo 3 febbraio 1993, n. 29, sul pubblico impiego),    &#13;
 nella parte in cui dispone l'abrogazione dell'art. 13 della legge  23    &#13;
 dicembre  1992,  n.  498  (Interventi  urgenti  in materia di finanza    &#13;
 pubblica), come sostituito dall'art. 6-bis del d.-l. 18 gennaio 1993,    &#13;
 n.   9    (Disposizioni    urgenti    in    materia    sanitaria    e    &#13;
 socio-assistenziale),  convertito  con  modificazioni con la legge 18    &#13;
 marzo 1993, n. 67;                                                       &#13;
     che, secondo la ricostruzione del pretore di Brescia, l'art.   13    &#13;
 della  legge  n.  498  del  1992, concernente la disciplina di taluni    &#13;
 adempimenti previdenziali delle province,  delle  comunità  montane,    &#13;
 dei  relativi  consorzi e delle Istituzioni pubbliche di assistenza e    &#13;
 beneficenza (IPAB), è stato dapprima sostituito dall'art. 6-bis  del    &#13;
 d.-l.  n.  9  del  1993,  e,  successivamente, è stato espressamente    &#13;
 abrogato dall'art. 38  del  decreto  legislativo  n.  546  del  1993,    &#13;
 disposizione,  quest'ultima,  che ha sostituito l'art. 74 del decreto    &#13;
 legislativo n. 29 del 1993;                                              &#13;
     che il ricorrente deduce di avere  fatto  applicazione  dell'art.    &#13;
 13  della  legge  n.  498  del 1992 anche dopo che la norma era stata    &#13;
 abrogata, e di "essere venuto  a  conoscenza  dell'abrogazione  (...)    &#13;
 per  pura  casualità,  dopo circa quattro anni", sicché dalla detta    &#13;
 abrogazione è derivato "un  danno  agevolmente  individuabile  nella    &#13;
 perdita   di   credibilità   esterna   ed   interna   dell'autorità    &#13;
 giudiziaria, la quale ha per anni applicato  una  norma  abrogata  e,    &#13;
 così,   violato   il  principio  costituzionale  che  le  impone  di    &#13;
 rispettare la legge";                                                    &#13;
     che,  ad  avviso  del  pretore  di Brescia, l'art. 38 del decreto    &#13;
 legislativo n. 546 del 1993 è inoltre  illegittimo  per  eccesso  di    &#13;
 delega   ed   anche  per  irragionevolezza,  in  quanto  ha  disposto    &#13;
 l'abrogazione di una disciplina legislativa che, al momento in cui la    &#13;
 legge di delegazione è  entrata  in  vigore,  non  risultava  ancora    &#13;
 vigente;                                                                 &#13;
     che,  secondo  il  ricorrente,  la  natura  legislativa dell'atto    &#13;
 impugnato non preclude l'ammissibilità del conflitto, dato  che  gli    &#13;
 istituti del conflitto di attribuzione e del giudizio di legittimità    &#13;
 costituzionale  in  via incidentale "sono coesistenti e concorrenti e    &#13;
 non antagonisti ed incompatibili";                                       &#13;
     che il ricorrente chiede, infine,  che  la  Corte  costituzionale    &#13;
 dichiari  "la  spettanza  in  via esclusiva all'Autorità giudiziaria    &#13;
 della funzione giurisdizionale ed  in  particolare  dell'attribuzione    &#13;
 che  le  impone di conoscere ed applicare la legge, attribuzione lesa    &#13;
 nella sua credibilità esterna ed interna dall'inidoneità  dell'art.    &#13;
 74  del  decreto  legislativo 3 febbraio 1993, n. 29, come sostituito    &#13;
 dall'art. 38 del decreto legislativo 23 dicembre  1993,  n.  546,  ad    &#13;
 essere  conosciuto  ed  applicato"  e,  per  l'effetto,  annulli tale    &#13;
 disposizione   "con    specifico    riferimento    alla    previsione    &#13;
 dell'abrogazione  dell'art.    13  della  legge n. 498 del 1992, come    &#13;
 sostituito dall'art. 6-bis della legge n. 67 del 1993".                  &#13;
   Considerato che, in questa fase del  giudizio,  a  norma  dell'art.    &#13;
 37,  terzo  e  quarto comma, della legge 11 marzo 1953, n. 87, questa    &#13;
 Corte è  chiamata  a  deliberare  senza  contraddittorio  in  ordine    &#13;
 all'ammissibilità  del ricorso sotto il profilo dell'esistenza della    &#13;
 "materia  di  un  conflitto  la  cui  risoluzione  spetti  alla   sua    &#13;
 competenza";                                                             &#13;
     che,  sotto  il  profilo  dei requisiti soggettivi, il pretore di    &#13;
 Brescia deve ritenersi  legittimato  a  sollevare  il  conflitto,  in    &#13;
 ragione  della  posizione  di  piena  indipendenza  attribuita  dalla    &#13;
 Costituzione a ciascun organo  giurisdizionale  nell'esercizio  delle    &#13;
 relative  funzioni (ex plurimis ordinanze nn. 37 del 1998, 469, 442 e    &#13;
 325 del 1997), e che  il  Governo,  in  persona  del  Presidente  del    &#13;
 Consiglio dei Ministri, è legittimato a resistervi;                     &#13;
     che,  in  ordine  alla  sussistenza  dell'oggetto  del conflitto,    &#13;
 occorre valutare se il ricorrente lamenti  la  violazione,  da  parte    &#13;
 dell'atto   impugnato,   di   una   propria   sfera  di  attribuzioni    &#13;
 costituzionalmente garantita (sentenza n. 49 del 1998, ordinanze  nn.    &#13;
 131 e 73 del 1997);                                                      &#13;
     che  il  pretore  di  Brescia  deduce la violazione delle proprie    &#13;
 attribuzioni  costituzionali  da  parte  dell'art.  38  del   decreto    &#13;
 legislativo  n.  546  del  1993,  per il fatto che tale disposizione,    &#13;
 anche in conseguenza dei modi e dei tempi  di  approvazione,  sarebbe    &#13;
 "inidonea"  ad  essere conosciuta ed applicata, e quindi lesiva della    &#13;
 funzione giurisdizionale nella sua "credibilità interna ed esterna";    &#13;
     che i motivi  per  i  quali  la  disposizione  impugnata  sarebbe    &#13;
 "inidonea" ad essere conosciuta ed applicata sin dalla sua entrata in    &#13;
 vigore,    e    la   "perdita   di   credibilità"   della   funzione    &#13;
 giurisdizionale,  dedotta  dal  ricorrente  quale  limitazione  della    &#13;
 propria  sfera  di  competenza,  non concretano però una menomazione    &#13;
 giuridicamente apprezzabile  delle  attribuzioni  costituzionali  del    &#13;
 ricorrente  medesimo,  ma  integrano,  semmai, una situazione di puro    &#13;
 fatto,  insuscettibile  ex  se  di  dar  vita  ad  un  conflitto   di    &#13;
 attribuzione fra poteri dello Stato;                                     &#13;
     che  difetta  conseguentemente,  sotto  il  profilo  oggettivo  e    &#13;
 prescindendo da ulteriori motivi di  inammissibilità  relativi  alla    &#13;
 natura  legislativa dell'atto impugnato, la "materia" di un conflitto    &#13;
 la cui risoluzione spetti alla competenza di questa Corte, in  quanto    &#13;
 il   ricorso   non   prospetta   alcuna  lesione  di  un'attribuzione    &#13;
 costituzionalmente garantita.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara inammissibile, ai sensi dell'art. 37 della legge 11  marzo    &#13;
 1953,  n.  87,  il conflitto di attribuzione sollevato dal pretore di    &#13;
 Brescia  nei  confronti  del  Governo  con  il  ricorso  indicato  in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 luglio 1998.                               &#13;
                        Il Presidente: Vassalli                           &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 24 luglio 1998.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
