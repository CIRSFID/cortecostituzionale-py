<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>704</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:704</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/06/1988</data_decisione>
    <data_deposito>23/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, prof. Luigi MENGONI, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale degli artt. 13, 22, 31,    &#13;
 34, 35, 38 e 44 della legge 28 febbraio 1985, n. 47 (Norme in materia    &#13;
 di  controllo dell'attività urbanistico-edilizia, sanzioni, recupero    &#13;
 e sanatoria delle opere edilizie) e dell'art. 8 quater  del  d.l.  23    &#13;
 aprile  1985,  n.  146,  convertito  in legge 21 giugno 1985, n. 298,    &#13;
 promossi con ordinanze emesse il 17 maggio e il 10  aprile  1985  dal    &#13;
 Pretore  di  Palmi,  il 2 luglio, il 26 novembre 1985 e il 23 gennaio    &#13;
 1986 dal Tribunale di Lucera, il  6  febbraio  1987  dal  Pretore  di    &#13;
 Avola,  il  25 febbraio 1987 dal Pretore di Castelfiorentino, il 13 e    &#13;
 il 12 febbraio 1987 dal Pretore di Avola e  il  27  maggio  1987  dal    &#13;
 Pretore di Castelfiorentino, iscritte rispettivamente ai nn. 566, 568    &#13;
 e 695 del registro ordinanze 1985, ai  nn.  10  e  132  del  registro    &#13;
 ordinanze  1986  e  ai  nn.  198,  251,  285,  286 e 528 del registro    &#13;
 ordinanze 1987 e pubblicate nelle Gazzette Ufficiali della Repubblica    &#13;
 nn.  8,  11,  22 e 30/1ª s.s. dell'anno 1986 e nn. 23, 27, 31 e 43/1ª    &#13;
 s.s. dell'anno 1987;                                                     &#13;
    Visti  gli  atti  d'intervento  del  Presidente  del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 febbraio 1988 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che  il  Pretore di Palmi, con ordinanze 17 maggio (Reg.    &#13;
 Ord. n. 566/85) e 10 aprile 1985 (Reg. Ord. n. 568/85) ha  sollevato,    &#13;
 in   riferimento   all'art.   3   Cost.,  questione  di  legittimità    &#13;
 costituzionale degli artt. 38, primo e terzo comma, e 44 della  legge    &#13;
 28 febbraio 1985, n. 47 (Norme in materia di controllo dell'attività    &#13;
 urbanistico-edilizia, sanzioni,  recupero  e  sanatoria  delle  opere    &#13;
 edilizie)  nella parte in cui (nel caso sia già intervenuta sentenza    &#13;
 definitiva di condanna) non prevedono la sospensione  dell'esecuzione    &#13;
 della  pena  nei  confronti  dei  soggetti  che  possono godere della    &#13;
 sanatoria - il primo - e l'estinzione della pena, ove siano integrati    &#13;
 i  presupposti  della  sanatoria  - il secondo - sotto il profilo che    &#13;
 irragionevolmente  trattano  più   sfavorevolmente,   con   riguardo    &#13;
 all'esecuzione  della  pena  principale,  i  beneficiari  o possibili    &#13;
 beneficiari  dell'oblazione  rispetto  ai  beneficiari  dell'amnistia    &#13;
 impropria;                                                               &#13;
      che  il  Tribunale  di Lucera, con ordinanze 2 luglio 1985 (Reg.    &#13;
 Ord. n. 695/85) 26 novembre 1985 (Reg. Ord.  n. 10/86) e  23  gennaio    &#13;
 1986  (Reg.  Ord.  n. 132/86) ha sollevato, in riferimento all'art. 3    &#13;
 Cost., questione di legittimità costituzionale degli artt.  31,  34,    &#13;
 35, 38 e 44 della medesima legge 28 febbraio 1985, n. 47, nella parte    &#13;
 in cui non prevedono l'applicazione della  disciplina  ivi  contenuta    &#13;
 per  il  reato  di  lottizzazione  abusiva  c.d.  negoziale, sotto il    &#13;
 profilo che la mancata previsione dell'estinzione per  un  reato  che    &#13;
 costituisce  in  ogni  caso  un  quid  minus rispetto alla più grave    &#13;
 ipotesi di lottizzazione abusiva con opere, determina  un'irrazionale    &#13;
 diversità di trattamento per condotte ugualmente lesive dello stesso    &#13;
 bene;                                                                    &#13;
      che  il  Pretore di Avola con ordinanze 6 febbraio (Reg. Ord. n.    &#13;
 198/87) 13 febbraio (Reg. Ord. n. 285/87) e  2  febbraio  1987  (Reg.    &#13;
 Ord.  n.  286/87)  ha  sollevato,  in riferimento all'art. 112 Cost.,    &#13;
 questione di legittimità costituzionale dell'art. 22 della  medesima    &#13;
 legge  28  febbraio  1985,  n.  47,  nella  parte  in  cui prevede la    &#13;
 sospensione dell'azione penale a seguito  della  presentazione  della    &#13;
 domanda  di  sanatoria  ai  sensi  dell'art.  13  della stessa legge,    &#13;
 causando  la  pratica  impossibilità,  o  il  differimento  a  tempo    &#13;
 indeterminato, dell'esercizio dell'azione penale, obbligatorio per il    &#13;
 P.M.;                                                                    &#13;
      che il Pretore di Castelfiorentino, con ordinanza 27 maggio 1987    &#13;
 (Reg. Ord. n. 528/87) ha sollevato, in riferimento agli artt.  101  e    &#13;
 112 Cost., questione di legittimità costituzionale del medesimo art.    &#13;
 22 della legge 28 febbraio 1985, n.  47,  in  quanto,  prevedendo  la    &#13;
 sospensione  dell'azione penale fino all'esito dell'eventuale ricorso    &#13;
 giurisdizionale   amministrativo,   determina   sostanzialmente   una    &#13;
 sospensione  a tempo indeterminato dell'azione penale stessa, nonché    &#13;
 in quanto fa dipendere  l'accertamento  sull'esistenza  di  un  reato    &#13;
 dall'esito  di  un  procedimento  amministrativo, così vincolando il    &#13;
 giudice  non  alla  legge,  ma   alla   decisione   di   un'autorità    &#13;
 amministrativa;                                                          &#13;
      che  il  Pretore  di Castelfiorentino, con ordinanza 25 febbraio    &#13;
 1987 (Reg. Ord. n. 251/87) ha  sollevato  questione  di  legittimità    &#13;
 costituzionale,  in riferimento all'art. 3 Cost., degli artt. 13 e 22    &#13;
 della medesima legge 28 febbraio 1985, n.  47,  nella  parte  in  cui    &#13;
 prevedono che il beneficio dell'estinzione del reato consegua al solo    &#13;
 caso in cui la conformità  dell'opera  abusiva  sia  consacrata  nel    &#13;
 formale  provvedimento  di  concessione  in sanatoria e non anche nel    &#13;
 caso  in  cui  tale  conformità  sia  realizzata  in  virtù   della    &#13;
 demolizione  dell'opera  abusiva,  nonché  questione di legittimità    &#13;
 costituzionale, in riferimento all'art. 3 Cost., dell'art.  8  quater    &#13;
 del  decreto  legge 28 aprile 1985, n. 146, convertito nella legge 21    &#13;
 giugno 1985, n. 298, nella parte in  cui  dichiara  non  perseguibili    &#13;
 penalmente  coloro che abbiano demolito od eliminato le opere abusive    &#13;
 entro la data d'entrata in vigore  della  legge  di  conversione  del    &#13;
 decreto legge e quindi entro il 22 giugno 1985, senza nulla prevedere    &#13;
 per le opere demolite successivamente a tale data;                       &#13;
      che  nei  giudizi (ad eccezione di quello promosso con ordinanza    &#13;
 23 gennaio 1986, Reg. Ord. n. 132/86, del  Tribunale  di  Lucera)  è    &#13;
 intervenuto  il  Presidente del Consiglio dei ministri, chiedendo che    &#13;
 le questioni siano dichiarate inammissibili o, comunque, infondate;      &#13;
    Considerato  che, per l'identità o connessione delle questioni, i    &#13;
 giudizi possono essere riuniti;                                          &#13;
      che  questione  identica  a  quella  sollevata  dal Tribunale di    &#13;
 Lucera con ordinanze 2 luglio 1985 (Reg. Ord. n. 695/85) 26  novembre    &#13;
 1985  (Reg. Ord. n. 10/86) e 23 gennaio 1986 (Reg. Ord. n. 132/86) è    &#13;
 stata da questa Corte dichiarata inammissibile con la  sent.  n.  369    &#13;
 del  1988  per  difetto  di  idonea  motivazione  sulla rilevanza, in    &#13;
 quanto, mancando una sia  pur  sommaria  indicazione  degli  elementi    &#13;
 delle  fattispecie  concrete,  non  era  possibile  stabilire se alle    &#13;
 fattispecie stesse fosse oppur no  applicabile  la  nuova  disciplina    &#13;
 normativa  "più  favorevole al reo" di cui all'art. 18, primo comma,    &#13;
 della legge 28 febbraio 1985, n. 47, la cui  sola  impossibilità  di    &#13;
 applicazione avrebbe invero reso rilevanti le sollevate questioni;       &#13;
      che  anche  le  suddette  ordinanze  di  rinvio del Tribunale di    &#13;
 Lucera  contengono  identico  difetto  di  idonea  motivazione  sulla    &#13;
 rilevanza  e,  pertanto,  le  questioni con le stesse sollevate vanno    &#13;
 dichiarate manifestamente inammissibili;                                 &#13;
      che questioni identiche a quelle sollevate dal Pretore di Palmi,    &#13;
 con ordinanze 17 maggio (Reg. Ord. n. 566/85) e 10 aprile 1985  (Reg.    &#13;
 Ord.  n.  568/85) e dal Pretore di Castelfiorentino, con ordinanza 25    &#13;
 febbraio 1987 (Reg. Ord. n. 251/87) sono state  dichiarate  infondate    &#13;
 con la sentenza n. 369 del 1988;                                         &#13;
      che questioni identiche a quelle sollevate dal Pretore di Avola,    &#13;
 con ordinanze 6 febbraio (Reg. Ord. n. 198/87) 13 febbraio (Reg. Ord.    &#13;
 n.  285/87)  e 2 febbraio 1987 (Reg. Ord. n. 286/87) e dal Pretore di    &#13;
 Castelfiorentino, con ordinanza 27 maggio 1987 (Reg. Ord. n.  528/87)    &#13;
 sono  state  dichiarate  infondate  con  la sentenza n. 369 dell'anno    &#13;
 1988;                                                                    &#13;
      che  gli  attuali rimettenti non prospettano profili o argomenti    &#13;
 diversi rispetto a quelli già esaminati dalla Corte con le decisioni    &#13;
 in  parola  e  che,  di  conseguenza,  le  sollevate  questioni vanno    &#13;
 dichiarate manifestamente infondate;                                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   riuniti i giudizi;                                                     &#13;
    Dichiara   la   manifesta   inammissibilità  delle  questioni  di    &#13;
 legittimità costituzionale degli artt. 31, 34, 35,  38  e  44  della    &#13;
 legge  28  febbraio  1985,  n.  47  (Norme  in  materia  di controllo    &#13;
 dell'attività urbanistico-edilizia, sanzioni, recupero  e  sanatoria    &#13;
 delle opere edilizie) sollevate, in riferimento all'art. 3 Cost., dal    &#13;
 Tribunale di Lucera, con  ordinanze  2  luglio  1985  (Reg.  Ord.  n.    &#13;
 695/85) 26 novembre 1985 (Reg. Ord. n. 10/86) e 23 gennaio 1986 (Reg.    &#13;
 Ord. n. 132/86);                                                         &#13;
    dichiara la manifesta infondatezza delle questioni di legittimità    &#13;
 costituzionale degli artt. 13, 22, 38, primo e terzo comma e 44 della    &#13;
 medesima  legge  28  febbraio 1985, n. 47, nonché dell'art. 8 quater    &#13;
 del decreto legge 28 aprile 1985, n. 146, convertito nella  legge  21    &#13;
 giugno 1985, n. 298 sollevate, in riferimento agli artt. 3, 101 e 112    &#13;
 Cost., dal Pretore di Palmi, con ordinanze 17 maggio  (Reg.  Ord.  n.    &#13;
 566/85) e 10 aprile 1985 (Reg. Ord. n. 568/85); dal Pretore di Avola,    &#13;
 con ordinanze 6 febbraio (Reg. Ord. n. 198/87) 13 febbraio (Reg. Ord.    &#13;
 n.  285/87)  e  2 febbraio 1987 (Reg. Ord. n. 286/87); dal Pretore di    &#13;
 Castelfiorentino, con ordinanze 27 maggio 1987 (Reg. Ord. n.  528/87)    &#13;
 e 25 febbraio 1987 (Reg. Ord. n. 251/87).                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 9 giugno 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
