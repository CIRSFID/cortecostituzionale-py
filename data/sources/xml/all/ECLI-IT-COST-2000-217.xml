<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>217</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:217</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/2000</data_decisione>
    <data_deposito>19/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Annibale MARINI, &#13;
 Franco BILE, Giovanni Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale degli articoli 47 e 49 del    &#13;
 d.lgs. 31 dicembre 1992, n. 546 (Disposizioni sul processo tributario    &#13;
 in  attuazione  della  delega al Governo contenuta nell'art. 30 della    &#13;
 legge  30  dicembre  1991, n. 413), promossi con ordinanze emesse l'8    &#13;
 luglio  1999  (n. 2 ordinanze), il 30 agosto 1999 (n. 2 ordinanze) ed    &#13;
 il 4 novembre 1999 dalla Commissione tributaria regionale di Perugia,    &#13;
 rispettivamente  iscritte  ai numeri 564, 565, 727 e 728 del registro    &#13;
 ordinanze  1999  ed al n. 62 del registro ordinanze 2000 e pubblicate    &#13;
 nella   Gazzetta   Ufficiale  della  Repubblica  n. 42,  prima  serie    &#13;
 speciale,  dell'anno  1999  e  numeri 3  e  9,  prima serie speciale,    &#13;
 dell'anno 2000.                                                          &#13;
     Visti  gli  atti  di  intervento del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 7 giugno 2000 il giudice    &#13;
 relatore Annibale Marini;                                                &#13;
     Ritenuto  che la Commissione tributaria regionale di Perugia, con    &#13;
 cinque  ordinanze di identico contenuto emesse nelle date indicate in    &#13;
 epigrafe,  ha  sollevato,  in  riferimento agli articoli 3 e 24 della    &#13;
 Costituzione,   questione   di   legittimità   costituzionale  degli    &#13;
 articoli 47  e  49  del  decreto legislativo 31 dicembre 1992, n. 546    &#13;
 (Disposizioni  sul  processo tributario in attuazione della delega al    &#13;
 Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413);    &#13;
         che  la  Commissione  rimettente  -  investita  di istanze di    &#13;
 sospensione   della   esecutività   di  sentenze  della  Commissione    &#13;
 tributaria   provinciale  di  Perugia,  ex  art. 283  del  codice  di    &#13;
 procedura  civile,  in pendenza di appelli ritualmente proposti dalle    &#13;
 parti  private  soccombenti - ritiene che la norma del codice di rito    &#13;
 invocata   dalle  parti  istanti  non  sia  applicabile  al  processo    &#13;
 tributario,   sia  perché  l'art. 49  del  d.lgs.  n. 546  del  1992    &#13;
 espressamente  esclude l'applicabilità dell'art. 337 cod. proc. civ.    &#13;
 e  quindi  anche  delle  norme  da  quest'ultimo  richiamate, tra cui    &#13;
 appunto  l'art. 283,  sia  in  quanto  l'art. 47 del medesimo decreto    &#13;
 legislativo   renderebbe   palese  l'intenzione  del  legislatore  di    &#13;
 limitare la tutela cautelare solamente al primo grado di giudizio;       &#13;
         che l'esclusione di ogni possibilità di tutela cautelare nei    &#13;
 confronti  della  efficacia  esecutiva  della sentenza di primo grado    &#13;
 rappresenterebbe  tuttavia  - ad avviso dello stesso rimettente - una    &#13;
 lesione   del   diritto   di  difesa,  garantito  dall'art. 24  della    &#13;
 Costituzione,  del  quale  l'azione  cautelare  costituirebbe  sicura    &#13;
 espressione;                                                             &#13;
         che   le   norme  censurate  sarebbero  altresì  lesive  del    &#13;
 principio  di  eguaglianza, di cui all'art. 3 della Costituzione, per    &#13;
 l'ingiustificata disparità di trattamento che esse determinerebbero,    &#13;
 quanto  alla  tutela  giurisdizionale offerta ai contribuenti, tra le    &#13;
 controversie  in  materia di imposte e tasse devolute alla cognizione    &#13;
 del  giudice  ordinario,  nelle  quali  troverebbero applicazione gli    &#13;
 articoli 283  e  373  cod.  proc.  civ.,  e  quelle  attribuite  alla    &#13;
 giurisdizione delle commissioni tributarie, nelle quali non è invece    &#13;
 prevista  la  possibilità  di  sospensione delle sentenze di primo e    &#13;
 secondo grado;                                                           &#13;
         che  è  intervenuto  in  tutti  i  giudizi il Presidente del    &#13;
 Consiglio  dei  Ministri,  per  mezzo  dell'Avvocatura generale dello    &#13;
 Stato,  concludendo  per  la  declaratoria  di  inammissibilità o di    &#13;
 infondatezza della questione;                                            &#13;
     Considerato che i giudizi, aventi identico oggetto, vanno riuniti    &#13;
 per essere decisi con unica pronunzia;                                   &#13;
         che   questione  sostanzialmente  identica,  sollevata  dallo    &#13;
 stesso  rimettente,  è  stata  già  dichiarata  non  fondata con la    &#13;
 sentenza n. 165 del 2000;                                                &#13;
         che   in   tale  sentenza  si  rileva,  quanto  alla  dedotta    &#13;
 violazione   dell'art. 24   della  Costituzione,  "come  la  garanzia    &#13;
 costituzionale  della  tutela  cautelare debba ritenersi imposta solo    &#13;
 fino al momento in cui non intervenga, nel processo, una pronuncia di    &#13;
 merito  che  accolga - con efficacia esecutiva - la domanda, rendendo    &#13;
 superflua   l'adozione  di  ulteriori  misure  cautelari,  ovvero  la    &#13;
 respinga,  negando  in tal modo, con cognizione piena, la sussistenza    &#13;
 del diritto e dunque il presupposto stesso della invocata tutela. Con    &#13;
 la  conseguenza  che la previsione di mezzi di tutela cautelare nelle    &#13;
 fasi  di  giudizio  successive  a siffatta pronuncia, in favore della    &#13;
 parte   soccombente   nel   merito,   deve   ritenersi  rimessa  alla    &#13;
 discrezionalità del legislatore";                                       &#13;
         che  la  censura  riferita  alla  violazione del principio di    &#13;
 eguaglianza  ed incentrata sulla differente latitudine dei poteri del    &#13;
 giudice  nel  processo  civile e nel processo tributario è stata del    &#13;
 pari   disattesa   in   quanto   in   aperta  contraddizione  con  la    &#13;
 giurisprudenza   di   questa   Corte  che  ha  costantemente  escluso    &#13;
 l'esistenza   di   un  principio  (costituzionalmente  rilevante)  di    &#13;
 necessaria uniformità tra i vari tipi di processo;                      &#13;
         che  non  vengono prospettati profili nuovi o diversi tali da    &#13;
 indurre ad una diversa valutazione della questione;                      &#13;
         che,   pertanto,   la   questione   deve   essere  dichiarata    &#13;
 manifestamente infondata;                                                &#13;
     Visti  gli articoli 26, secondo comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti i giudizi;                                                   &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale  degli  articoli 47  e  49  del  decreto    &#13;
 legislativo  31  dicembre  1992,  n. 546  (Disposizioni  sul processo    &#13;
 tributario   in   attuazione   della   delega  al  Governo  contenuta    &#13;
 nell'art. 30  della  legge  30  dicembre 1991, n. 413), sollevata, in    &#13;
 riferimento   agli   articoli 3   e   24  della  Costituzione,  dalla    &#13;
 Commissione  tributaria  regionale  di  Perugia  con  le ordinanze in    &#13;
 epigrafe.                                                                &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 giugno 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Marini                             &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 19 giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
