<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>53</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:53</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/02/2000</data_decisione>
    <data_deposito>15/02/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, avv. Fernanda CONTRI, prof. &#13;
 Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale &#13;
 MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale della legge della Regione    &#13;
 Umbria,  riapprovata  il 6 luglio 1998, recante "Calendario venatorio    &#13;
 per la stagione 1988-1999", promosso con ricorso del  Presidente  del    &#13;
 Consiglio  dei  Ministri, notificato il 24 luglio 1998, depositato in    &#13;
 Cancelleria il 3 agosto 1998  ed  iscritto  al  n.  33  del  registro    &#13;
 ricorsi 1998.                                                            &#13;
   Visto l'atto di costituzione della Regione Umbria;                     &#13;
   Udito nell'udienza pubblica del 12 ottobre 1999 il giudice relatore    &#13;
 Fernanda Contri;                                                         &#13;
   Uditi  l'avvocato  dello Stato Gabriella Palmieri per il Presidente    &#13;
 del Consiglio dei Ministri  e  l'avvocato  Maurizio  Pedetta  per  la    &#13;
 Regione Umbria.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.   -   Con  ricorso  regolarmente  notificato  e  depositato,  il    &#13;
 Presidente del Consiglio dei Ministri solleva in via principale -  in    &#13;
 riferimento  agli  artt.  117  della Costituzione e 18 della legge 11    &#13;
 febbraio 1992, n. 157 (Norme per la protezione della fauna  selvatica    &#13;
 omeoterma  e  per  il  prelievo venatorio), nonché in relazione alla    &#13;
 direttiva  79/409/CEE  concernente  la  conservazione  degli  uccelli    &#13;
 selvatici  -  questione di legittimità costituzionale della delibera    &#13;
 legislativa recante "Calendario venatorio per la stagione 1998/1999",    &#13;
 riapprovata  a   maggioranza   assoluta   dal   Consiglio   regionale    &#13;
 dell'Umbria  nella  seduta  del  6  luglio  1998, nell'identico testo    &#13;
 rinviato dal Governo con atto del 26 giugno 1998.                        &#13;
   Il ricorrente lamenta la violazione dei citati parametri in  quanto    &#13;
 l'impugnata  delibera  legislativa  -  in deroga ai termini stabiliti    &#13;
 dall'art. 18, comma  1,  della  legge  n.  157  del  1992  -  prevede    &#13;
 l'apertura anticipata della stagione venatoria, consentendo la caccia    &#13;
 ad alcune specie di fauna selvatica (tortora, quaglia, merlo, starna,    &#13;
 pernice  rossa, lepre, alzavola, germano reale, marzaiola e fagiano),    &#13;
 nei giorni 6-12 e  13  settembre,  in  contrasto  "con  il  principio    &#13;
 contenuto  nell'art.  18,  comma  2,  della legge n. 157 del 1992, il    &#13;
 quale  subordina  al  preventivo  parere  tecnico  ...  dell'Istituto    &#13;
 nazionale  della  fauna selvatica l'autorizzazione alle modifiche dei    &#13;
 termini (di apertura e chiusura della caccia) di cui al  primo  comma    &#13;
 del medesimo articolo".                                                  &#13;
   Il ricorrente sottolinea che il menzionato Istituto, con nota del 5    &#13;
 maggio  1998,  aveva  espresso  parere sfavorevole, poiché, si legge    &#13;
 nell'atto di  rinvio,  la  generalizzata  preapertura  della  caccia,    &#13;
 considerate  le caratteristiche biologiche della fauna coinvolta e le    &#13;
 condizioni  ambientali  esistenti  nella   Regione,   "determinerebbe    &#13;
 l'abbattimento di una quota consistente di soggetti immaturi ... e di    &#13;
 soggetti  adulti  ancora  in  fase  riproduttiva,  con la conseguente    &#13;
 perdita di intere nidiate".                                              &#13;
   2.  - Nel presente giudizio si è costituita la Regione Umbria, per    &#13;
 chiedere  a  questa  Corte   di   dichiarare   l'inammissibilità   o    &#13;
 l'infondatezza della questione proposta dal Governo.                     &#13;
   La  difesa della Regione eccepisce innanzi tutto l'inammissibilità    &#13;
 della questione sollevata  in  via  principale  per  genericità  del    &#13;
 ricorso,  limitandosi  il  Governo  a  censurare  il contrasto con il    &#13;
 parere espresso dall'Istituto nazionale della fauna  selvatica  e  ad    &#13;
 affermare  apoditticamente  la  conseguente  violazione  della  legge    &#13;
 quadro e della direttiva 79/409/CEE.                                     &#13;
   Ad avviso della Regione Umbria,  il  ricorso  risulterebbe  inoltre    &#13;
 inammissibilmente  diretto  a  contestare    il  merito  delle scelte    &#13;
 regionali in materia di calendario venatorio "e  opponendo  a  queste    &#13;
 altre  scelte,  sulla  base del parere sfavorevole dell'I.N.F.S.", si    &#13;
 legge nell'atto di costituzione, il Governo "avrebbe dovuto, se  mai,    &#13;
 proporre  ...    la  questione  di  merito per conflitto di interessi    &#13;
 davanti alle Camere ex art. 127, quarto comma, della Costituzione".      &#13;
   Nel  merito,  richiamando  anche  recenti  decisioni   del   T.a.r.    &#13;
 dell'Umbria,  l'ente territoriale resistente deduce che, nel silenzio    &#13;
 della  legge,  "non  appare  seriamente  sostenibile  che  il  parere    &#13;
 espresso  dall'I.N.F.S.   sul calendario venatorio sia vincolante per    &#13;
 la  Regione".  Trattandosi  di  circoscrivere,  con  una   norma   di    &#13;
 principio,  una  competenza  regionale  prevista  dalla  Costituzione    &#13;
 "sarebbe, quantomeno, stata necessaria la previsione  espressa".  Una    &#13;
 diversa  opzione  interpretativa,  aggiunge  la difesa della Regione,    &#13;
 "significa trasferire la potestà di scelta e di decisione in materia    &#13;
 di   calendario   venatorio   -   pur   limitatamente    all'anticipo    &#13;
 dell'apertura  della  caccia a poche specie, e sempre nell'ambito del    &#13;
 periodo previsto dalla legge - dalle  singole  Regioni  a  un  organo    &#13;
 centrale  (di  tipo  tecnico),  stravolgendo  immediatamente la ratio    &#13;
 della legge stessa e negando in radice l'autonomia della Regione".       &#13;
   In merito al  rilievo  formulato  dall'INFS  circa  il  pregiudizio    &#13;
 derivante  alle  specie  selvatiche  da  un'apertura anticipata della    &#13;
 stagione venatoria "in forma pressoché generalizzata", come si legge    &#13;
 nel  medesimo  parere  disatteso  dal   legislatore   regionale,   la    &#13;
 resistente  afferma  trattarsi  di  una censura "del tutto infondata,    &#13;
 riguardando l'anticipo solo dieci specie su quaranta" ed aggiunge che    &#13;
 adeguate misure di salvaguardia e tutela derivano da altri  strumenti    &#13;
 di  cui  la  Regione  si è dotata, quali i piani faunistico-venatori    &#13;
 regionali e provinciali.                                                 &#13;
   3. - In prossimità dell'udienza, la Regione Umbria  ha  depositato    &#13;
 una  memoria  illustrativa ad integrazione di quanto già dedotto con    &#13;
 l'atto di costituzione.                                                  &#13;
   La  difesa  della  Regione  premette  innanzi   tutto   che,   dopo    &#13;
 l'instaurazione del presente giudizio, lo stesso calendario venatorio    &#13;
 approvato  con  la  legge  impugnata  è  stato adottato dalla Giunta    &#13;
 regionale in via amministrativa (con le stesse motivazioni  enunciate    &#13;
 negli  atti  consiliari preparatori della delibera legislativa) e, in    &#13;
 séguito  ai ricorsi presentati da quattro associazioni ambientaliste,    &#13;
 ritenuto legittimo dal T.a.r. dell'Umbria.                               &#13;
   La Regione  ribadisce  che  non  si  tratta,  nel  caso  sottoposto    &#13;
 all'esame  di  questa Corte, di una generalizzata "preapertura" della    &#13;
 caccia (riguardando l'anticipazione solo dieci specie selvatiche), ed    &#13;
 inoltre sottolinea come, in relazione alle specie  interessate  dalla    &#13;
 deroga,  il  periodo  venatorio  complessivo  risulti  inalterato, in    &#13;
 considerazione  della  corrispondente  anticipazione  della  chiusura    &#13;
 della caccia.                                                            &#13;
   Nella  memoria illustrativa, l'ente territoriale resistente insiste    &#13;
 sulla natura obbligatoria ma non vincolante del parere dell'I.N.F.S.,    &#13;
 sia in considerazione  della  formulazione  della  legge-quadro,  che    &#13;
 impone   alla   Regione   di  "sentire"  il  predetto  istituto,  sia    &#13;
 richiamando la giurisprudenza di questa Corte, e  in  particolare  la    &#13;
 sentenza  n.    248  del 1995, che, in un caso ritenuto analogo dalla    &#13;
 Regione, qualifica "onere procedimentale" l'acquisizione  del  parere    &#13;
 dell'I.N.F.S.                                                            &#13;
   4.  -  In  prossimità  della  data fissata per l'udienza, anche il    &#13;
 ricorrente ha depositato una memoria, per confutare le  difese  della    &#13;
 Regione  resistente  ed insistere nella richiesta di accoglimento del    &#13;
 ricorso.                                                                 &#13;
   In  aggiunta  a  quanto  già  esposto  nel  ricorso,  l'Avvocatura    &#13;
 generale  dello Stato richiama la giurisprudenza di questa Corte che,    &#13;
 nella parte in cui "delinea il nucleo minimo  di  salvaguardia  della    &#13;
 fauna  selvatica",  ha  qualificato  norma  fondamentale  di  riforma    &#13;
 economico-sociale la disciplina dei periodi venatori  e  ha  chiarito    &#13;
 come  l'esercizio  del  potere di deroga di cui all'art. 18, comma 2,    &#13;
 della legge n.  157 del 1992 sia subordinato all'adozione - in  alcun    &#13;
 modo  documentata,  ad  avviso  dell'Avvocatura,  in questo caso - di    &#13;
 procedure   e   strumenti   attendibili   dal    punto    di    vista    &#13;
 tecnico-scientifico,   per  l'accertamento  delle  condizioni  e  dei    &#13;
 presupposti di ordine ambientale richiesti dalla disciplina statale e    &#13;
 dalla giurisprudenza comunitaria ai fini  delle  deroghe  ai  periodi    &#13;
 venatori previsti in via generale per l'intero territorio nazionale.<diritto>Considerato in diritto</diritto>1.  -  Il Presidente del Consiglio dei Ministri ha sollevato in via    &#13;
 principale, in riferimento agli artt. 117  della  Costituzione  e  18    &#13;
 della  legge  11  febbraio  1992,  n.  157, nonché in relazione alla    &#13;
 direttiva (non evocata nell'atto di rinvio) 79/409/CEE concernente la    &#13;
 conservazione degli  uccelli  selvatici,  questione  di  legittimità    &#13;
 costituzionale  della delibera legislativa concernente il "Calendario    &#13;
 venatorio per  la  stagione  1998/1999",  riapprovata  a  maggioranza    &#13;
 assoluta  dal  Consiglio  regionale  dell'Umbria  nella  seduta del 6    &#13;
 luglio 1998, nell'identico testo rinviato dal Governo con atto del 26    &#13;
 giugno 1998.                                                             &#13;
   Con tale delibera veniva approvato il Calendario venatorio  per  la    &#13;
 stagione  1998/1999,  nel  testo  allegato  alla legge. Il calendario    &#13;
 prevedeva, per dieci specie di  fauna  selvatica,  l'anticipazione  -    &#13;
 rispetto  ai  termini  iniziali previsti dall'art. 18, comma 1, della    &#13;
 legge n. 157 del 1992 - dell'apertura della caccia,  sulla  base  del    &#13;
 comma  2  del  citato  art.  18,  che,  in  presenza  di  determinati    &#13;
 presupposti e condizioni,  consente  alle  Regioni  di  modificare  i    &#13;
 termini  di  apertura  e  chiusura  della  caccia  previsti  al comma    &#13;
 precedente.                                                              &#13;
   Il Presidente del Consiglio dei  Ministri  sollevava  questione  di    &#13;
 legittimità  costituzionale  della  delibera legislativa, censurando    &#13;
 non già l'omessa acquisizione  del  parere  dell'I.N.F.S.,  prevista    &#13;
 dall'art.  18, comma 2, dell'invocata legge-quadro, bensì l'adozione    &#13;
 delle deroghe ai periodi venatori di cui al comma 1 del  citato  art.    &#13;
 18, in contrasto con il predetto parere.                                 &#13;
   2.  -  L'impugnata  delibera  legislativa  è  un  provvedimento di    &#13;
 approvazione, in deroga all'art. 32 della legge della regione  Umbria    &#13;
 17  maggio 1994, n. 14 (Norme per la protezione della fauna selvatica    &#13;
 omeoterma e per il prelievo venatorio), del calendario venatorio  per    &#13;
 la  stagione  1998/1999,  ad  essa  allegato.  Il  ricorso, pertanto,    &#13;
 investe una disciplina derogatoria destinata a  trovare  applicazione    &#13;
 durante  la  sola  stagione  venatoria  1998/1999,  conclusasi  il 31    &#13;
 gennaio 1999.                                                            &#13;
   Conformemente alla costante giurisprudenza costituzionale (sentenze    &#13;
 nn. 45 e 46 del 1987, n. 37 del  1983),  trovandosi  questa  Corte  a    &#13;
 giudicare dell'impugnazione in via principale di una legge a termine,    &#13;
 che   non  ha  potuto  né  potrebbe,  ormai,  trovare  applicazione,    &#13;
 essendosi chiusa  la  stagione  venatoria  da  essa  considerata,  va    &#13;
 dichiarata la cessazione della materia del contendere.</testo>
    <dispositivo>per questi motivi                             &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  cessata la materia del contendere in ordine al ricorso in    &#13;
 epigrafe.                                                                &#13;
   Così deciso, in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, 9 febbraio 2000.                                 &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 15 febbraio 2000.                         &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
