<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>204</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:204</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/05/1995</data_decisione>
    <data_deposito>30/05/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 8, primo e    &#13;
 sesto comma, della legge regionale del Lazio 3  gennaio  1986,  n.  1    &#13;
 (Regime  urbanistico  dei  terreni  di  uso  civico  e relative norme    &#13;
 transitorie), promosso con ordinanza emessa il 13 febbraio  1992  dal    &#13;
 Tribunale amministrativo regionale del Lazio sul ricorso proposto dal    &#13;
 Comune  di  Frascati  contro la Regione Lazio, iscritta al n. 529 del    &#13;
 registro ordinanze 1994 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 39, prima serie speciale, dell'anno 1994;                  &#13;
    Udito  nella  camera  di  consiglio  del 20 aprile 1995 il Giudice    &#13;
 relatore Cesare Mirabelli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel corso di un giudizio  promosso  dal  Comune  di  Frascati  nei    &#13;
 confronti  della  Regione  Lazio  per  ottenere  l'annullamento della    &#13;
 nomina,  disposta  dal  Comitato  regionale  di  controllo,   di   un    &#13;
 commissario  ad  acta,  in  sostituzione del Comune inadempiente, per    &#13;
 l'esame e la decisione in ordine a domande di alienazione di  terreni    &#13;
 di  proprietà  collettiva, il Tribunale amministrativo regionale del    &#13;
 Lazio ha sollevato d'ufficio questione di legittimità costituzionale    &#13;
 dell'art. 8, primo e sesto comma, della legge regionale del  Lazio  3    &#13;
 gennaio  1986,  n.  1 (Regime urbanistico dei terreni di uso civico e    &#13;
 relative norme transitorie), in riferimento agli artt. 128 e 97 della    &#13;
 Costituzione.                                                            &#13;
    La disposizione denunciata, al primo comma, autorizza ad  alienare    &#13;
 agli  occupatori  le superfici di terreni di proprietà collettiva su    &#13;
 cui siano state  effettuate  costruzioni  non  debitamente  assentite    &#13;
 dall'ente titolare, se le opere siano conformi agli strumenti ed alle    &#13;
 norme  urbanistiche  o suscettibili di sanatoria ai sensi della legge    &#13;
 28 febbraio 1985, n. 47 e della legge regionale 2 maggio 1980, n.  28    &#13;
 e  successive modificazioni. La stessa disposizione prevede, al sesto    &#13;
 comma, che  con  deliberazione  motivata  del  Comune  il  prezzo  di    &#13;
 alienazione  possa  essere  ridotto rispetto al valore determinato da    &#13;
 tecnici designati dalla Giunta regionale, in relazione a  particolari    &#13;
 situazioni  di  esigenze  abitative,  per coloro che hanno eseguito o    &#13;
 acquistato la costruzione per destinarla a prima  abitazione,  se  la    &#13;
 superficie complessiva dell'edificio non supera 400 metri quadrati.      &#13;
    Il Tribunale amministrativo ritiene che questa disposizione sia in    &#13;
 contrasto   con   l'autonomia  degli  enti  locali  (art.  128  della    &#13;
 Costituzione), in quanto  lesiva  del  potere  di  scelta  in  ordine    &#13;
 all'alienazione  ed  alla  determinazione  del prezzo. In presenza di    &#13;
 particolari esigenze abitative la discrezionalità  del  Comune,  che    &#13;
 deve  motivare la propria scelta, sarebbe puramente nominale. Inoltre    &#13;
 sarebbe  leso  il  principio  del  buon  andamento   della   pubblica    &#13;
 amministrazione  (art. 97 della Costituzione), che impone di ricavare    &#13;
 la massima utilità dai beni della comunità locale anche in caso  di    &#13;
 alienazione,   mentre   la  disposizione  denunciata  sostanzialmente    &#13;
 vincolerebbe alla riduzione del prezzo di vendita.                       &#13;
    Il giudice rimettente ritiene che la soluzione della questione  di    &#13;
 legittimità   costituzionale   sia   rilevante,  in  quanto  il  suo    &#13;
 accoglimento farebbe venire meno le ragioni stesse del  provvedimento    &#13;
 oggetto del giudizio principale.<diritto>Considerato in diritto</diritto>1.  -  Il  dubbio di legittimità costituzionale concerne le norme    &#13;
 transitorie per l'alienazione di  terreni  di  proprietà  collettiva    &#13;
 oggetto di costruzioni abusivamente effettuate, dettate dalla Regione    &#13;
 Lazio  nel  contesto  della  legge  3  gennaio  1986,  n.  1  (Regime    &#13;
 urbanistico dei terreni di uso civico e relative norme transitorie).     &#13;
    Il  Tribunale  amministrativo  regionale  del  Lazio  ritiene  che    &#13;
 l'autorizzazione  ad alienare agli occupatori le superfici di terreni    &#13;
 di proprietà collettiva su cui sono state effettuate costruzioni non    &#13;
 debitamente assentite  dall'ente  titolare  (art.  8,  primo  comma),    &#13;
 unitamente  alla  possibilità  di  ridurre  il prezzo di alienazione    &#13;
 quando la costruzione sia destinata a prima abitazione (art. 8, sesto    &#13;
 comma), violi l'autonomia del Comune (art.  128  della  Costituzione)    &#13;
 nello  scegliere  se  procedere  o meno all'alienazione, obbligandolo    &#13;
 sostanzialmente  alla  vendita  ed  alla  riduzione  del  prezzo,  in    &#13;
 contrasto  con  il  principio  di buon andamento dell'amministrazione    &#13;
 (art. 97 della Costituzione).                                            &#13;
    2. - La questione di legittimità costituzionale non è fondata.      &#13;
    La  disposizione  denunciata  si  inserisce  nel  contesto   della    &#13;
 disciplina  che  regolamenta il regime urbanistico dei terreni di uso    &#13;
 civico, prevedendo  che  la  Giunta  regionale,  sentito  l'Assessore    &#13;
 regionale   agli  usi  civici,  possa  autorizzare  l'alienazione  di    &#13;
 proprietà civiche divenute  edificatorie  in  conformità  al  piano    &#13;
 regolatore   generale.  I  proventi  dell'alienazione  devono  essere    &#13;
 destinati all'acquisto  di  terreni  sui  quali  si  trasferiscono  i    &#13;
 vincoli  esistenti  sui terreni alienati o all'esecuzione di opere di    &#13;
 miglioramento fondiario sul demanio collettivo (art.  5  della  legge    &#13;
 regionale n. 1 del 1986).                                                &#13;
    Il    procedimento   previsto   per   l'autorizzazione   regionale    &#13;
 all'alienazione, che nella disciplina a regime è rilasciata in  ogni    &#13;
 singolo  caso  con  uno  specifico provvedimento, è sostituito nella    &#13;
 disciplina transitoria da un'autorizzazione ex  lege  all'alienazione    &#13;
 di  terreni  che  non  rientrino in zone specificamente protette o di    &#13;
 particolare  interesse  pubblico,  edificati  in   conformità   agli    &#13;
 strumenti  urbanistici  o  con  costruzioni  abusive  suscettibili di    &#13;
 sanatoria (art. 8, primo comma, della stessa legge).                     &#13;
    La  legge  non  obbliga  il  Comune  ad  alienare,  ma  disciplina    &#13;
 l'autorizzazione  regionale  necessaria  in  ragione  dei vincoli che    &#13;
 gravano sui beni, restando ogni  altra  valutazione  in  ordine  alla    &#13;
 opportunità della vendita, con gli atti che ne seguono, rimessa alle    &#13;
 discrezionali determinazioni dell'ente locale.                           &#13;
    Anche  la  prevista  possibilità  di  ridurre, rispetto al valore    &#13;
 determinato da tecnici nominati dalla Giunta regionale, il prezzo  di    &#13;
 vendita  del  terreno a chi ha eseguito la costruzione abusiva o l'ha    &#13;
 acquistata al solo scopo di destinarla a prima abitazione non obbliga    &#13;
 il Comune alla riduzione del prezzo né  determina  la  misura  della    &#13;
 riduzione  stessa,  attribuendo  all'ente  locale solo la facoltà di    &#13;
 derogare motivatamente alle valutazioni di stima,  in  ragione  della    &#13;
 situazione  abitativa  esistente. La legge è diretta a consentire in    &#13;
 tal modo una valutazione che tenga conto  dell'interesse  pubblico  a    &#13;
 trarre   la  massima  utilità  dall'alienazione  del  bene,  ma  che    &#13;
 consideri anche la condizione del bene  e  le  esigenze  collegate  a    &#13;
 situazioni  nelle  quali  l'utilizzo  della  costruzione  risponde ad    &#13;
 essenziali necessità abitative (cfr. sentenza n. 169 del 1994).         &#13;
    Si è dunque in presenza di norme  che  non  obbligano  il  Comune    &#13;
 nella   sua  azione  amministrativa,  ma  che  disciplinano  atti  di    &#13;
 competenza regionale,  quali  l'autorizzazione  alla  alienazione,  o    &#13;
 attribuiscono  al  Comune  una  facoltà,  quale  la  possibilità di    &#13;
 ridurre, in presenza e nei limiti di un interesse pubblico, il prezzo    &#13;
 di vendita dei beni considerati.                                         &#13;
    Non è dunque lesa l'autonomia del Comune ed è anche  esclusa  la    &#13;
 violazione del principio di buon andamento dell'amministrazione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 8, primo e sesto comma, della legge regionale del  Lazio  3    &#13;
 gennaio  1986,  n.  1 (Regime urbanistico dei terreni di uso civico e    &#13;
 relative norme transitorie), sollevata, in riferimento agli artt. 128    &#13;
 e  97  della Costituzione, dal Tribunale amministrativo regionale del    &#13;
 Lazio con l'ordinanza indicata in epigrafe.                              &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 maggio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 30 maggio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
