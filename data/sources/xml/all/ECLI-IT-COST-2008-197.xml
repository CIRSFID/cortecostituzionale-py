<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>197</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:197</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>21/05/2008</data_decisione>
    <data_deposito>06/06/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 3, comma 3, ultimo periodo, del decreto del Presidente della Repubblica 31 agosto 1999, n. 394 (Regolamento recante norme di attuazione del testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero, a norma dell'articolo 1, comma 6, del decreto legislativo 25 luglio 1998, n. 286), come sostituito dal decreto del Presidente della Repubblica 18 ottobre 2004, n. 334 (Regolamento recante modifiche ed integrazioni al decreto del Presidente della Repubblica 31 agosto 1999, n. 394, in materia di immigrazione), promossi con due ordinanze del 23 ottobre 2006 dal Giudice di pace di Palermo, sui ricorsi proposti da A. A. e da C. D. J. nei confronti del Prefetto di Palermo, iscritte ai nn. 832 e 833 del registro ordinanze 2007 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 3, prima serie speciale, dell'anno 2008. &#13;
    Visti gli atti di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 7 maggio 2008 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che il Giudice di pace di Palermo, con due ordinanze di identico contenuto depositate il 23 ottobre 2006, nel corso di giudizi di impugnazione di decreti di espulsione emessi, rispettivamente, nei confronti di A. A., cittadino tunisino, e di C. D. J., cittadino rumeno, ha sollevato, in riferimento all'art. 24, secondo e terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 3, comma 3, del d.P.R. 31 agosto 1999, n. 394 (Regolamento recante norme di attuazione del testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero, a norma dell'articolo 1, comma 6, del Decreto legislativo 25 luglio 1998, n. 286), come sostituito dal d.P.R. 18 ottobre 2004, n. 334 (Regolamento recante modifiche ed integrazioni al decreto del Presidente della Repubblica 31 agosto 1999, n. 394, in materia di immigrazione); &#13;
    che, in punto di fatto, il rimettente rileva che i provvedimenti impugnati sono stati redatti senza l'assistenza di un interprete della lingua conosciuta dai predetti cittadini, assistenza prevista dal rinvio contenuto nell'art. 13, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), al comma 8 del medesimo articolo; &#13;
    che, in particolare, la disposizione censurata prevede che, nel caso in cui il destinatario del decreto di espulsione non comprenda la lingua italiana questo sia «accompagnato da una sintesi del suo contenuto, anche mediante appositi formulari sufficientemente dettagliati, nella lingua a lui comprensibile o, se ciò non è possibile per indisponibilità di personale idoneo alla traduzione del provvedimento in tale lingua, in una delle lingue inglese, francese o spagnola, secondo la preferenza indicata dall'interessato»; &#13;
    che nel caso di specie, a parere del rimettente, il Prefetto, nel giustificare la traduzione del proprio provvedimento in lingua francese con il mancato immediato reperimento dell'interprete, avrebbe eluso la ratio delle norme citate, le quali non fanno alcun riferimento alla possibilità o meno di reperire immediatamente personale tecnico idoneo alla traduzione, con la conseguenza che i decreti di espulsione impugnati sarebbero in contrasto con l'art. 24, secondo e terzo comma, della Costituzione; &#13;
    che in entrambi i giudizi è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dell'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o infondata; &#13;
    che, preliminarmente, l'Avvocatura rileva che la norma censurata ha natura regolamentare e, pertanto, non può essere oggetto di giudizio da parte della Corte costituzionale, avendo, comunque, il rimettente omesso qualsiasi motivazione in ordine alla rilevanza e non manifesta infondatezza della questione sollevata la quale, peraltro, si fonda su vizi formali degli atti impugnati nei giudizi principali; &#13;
    che, quanto al merito, l'Avvocatura ritiene che la norma censurata non solo non lede il diritto di difesa dello straniero destinatario del provvedimento di espulsione ma, anzi, si pone a tutela di tale diritto, prevedendo la redazione dell'atto in una lingua comprensibile allo straniero. &#13;
    Considerato che il Giudice di pace di Palermo, con due ordinanze di identico contenuto, dubita, in riferimento all'art. 24, secondo e terzo comma, della Costituzione, della legittimità costituzionale dell'art. 3, comma 3, del d.P.R. 31 agosto 1999, n. 394 (Regolamento recante norme di attuazione del testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero, a norma dell'articolo 1, comma 6, del Decreto legislativo 25 luglio 1998, n. 286), come sostituito dal d.P.R. 18 ottobre 2004, n. 334 (Regolamento recante modifiche ed integrazioni al decreto del Presidente della Repubblica 31 agosto 1999, n. 394, in materia di immigrazione); &#13;
    che le ordinanze di rimessione propongono identica questione, onde i relativi giudizi vanno riuniti per essere definiti con un'unica decisione; &#13;
    che la questione è manifestamente inammissibile, in quanto diretta contro una disposizione regolamentare e, pertanto, sottratta al giudizio di legittimità costituzionale. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi  &#13;
    LA CORTE COSTITUZIONALE  &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 3, comma 3, del decreto del Presidente della Repubblica 31 agosto 1999, n. 394 (Regolamento recante norme di attuazione del testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero, a norma dell'articolo 1, comma 6, del decreto legislativo 25 luglio 1998, n. 286), così come sostituito dall'art. 3, comma 1, lettera a), del d.P.R. 18 ottobre 2004, n. 334 (Regolamento recante modifiche ed integrazioni al decreto del Presidente della Repubblica 31 agosto 1999, n. 394, in materia di immigrazione), sollevata, in riferimento all'art. 24, secondo e terzo comma, della Costituzione, dal Giudice di pace di Palermo con le ordinanze indicate in epigrafe.  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 21 maggio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 6 giugno 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
