<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>687</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:687</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/06/1988</data_decisione>
    <data_deposito>16/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 76 della legge    &#13;
 20 maggio 1982, n. 270 ("Revisione della disciplina del  reclutamento    &#13;
 del personale docente della scuola materna, elementare, secondaria ed    &#13;
 artistica, ristrutturazione degli organici, adozione di misure idonee    &#13;
 ad  evitare  la formazione di precariato e sistemazione del personale    &#13;
 precario esistente"), promosso con ordinanza emessa il 29 maggio 1985    &#13;
 dal  Consiglio  di  giustizia amministrativa per la Regione siciliana    &#13;
 sul ricorso proposto da Palmisano Mario  contro  il  Ministero  della    &#13;
 pubblica  istruzione  ed  altri,  iscritta  al  n.  339  del registro    &#13;
 ordinanze 1986 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 37, prima serie speciale, dell'anno 1986.                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio dell'11 maggio 1988 il Giudice    &#13;
 relatore Francesco Paolo Casavola.                                       &#13;
    Ritenuto  che  il  Consiglio  di  giustizia  amministrativa per la    &#13;
 Regione siciliana,  con  ordinanza  emessa  il  29  maggio  1985,  ha    &#13;
 sollevato,  in  relazione agli artt. 3, 33, comma quinto, e 51, comma    &#13;
 primo, della Costituzione, questione di  legittimità  costituzionale    &#13;
 dell'art.  76  della  legge  20 maggio 1982, n. 270 ("Revisione della    &#13;
 disciplina  del  reclutamento  del  personale  docente  della  scuola    &#13;
 materna,  elementare, secondaria ed artistica, ristrutturazione degli    &#13;
 organici, adozione di misure  idonee  ad  evitare  la  formazione  di    &#13;
 precariato  e  sistemazione del personale precario esistente"), nella    &#13;
 parte in cui non prevede, per i docenti che in uno dei  due  anni  di    &#13;
 insegnamento  richiesti  per l'ammissione alle sessioni riservate per    &#13;
 l'abilitazione  abbiano  prestato  servizio  militare  di  leva,   la    &#13;
 utilizzazione   alternativa   o   surrogatoria   di   altro  anno  di    &#13;
 insegnamento;                                                            &#13;
      che,   a   parere   del   giudice  a  quo,  la  norma  impugnata    &#13;
 comporterebbe  un'irrazionale  discriminazione  degli  insegnanti  di    &#13;
 sesso  maschile rispetto a quelli di sesso femminile che, non essendo    &#13;
 tenuti ad obbligi di leva non verrebbero a perdere il servizio  utile    &#13;
 ai fini della partecipazione agli esami in questione;                    &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri, rappresentato dall'Avvocatura  dello  Stato,  la  quale  ha    &#13;
 concluso per la declaratoria d'inammissibilità ovvero d'infondatezza    &#13;
 della norma.                                                             &#13;
    Considerato  che  l'eventuale  predisposizione di un meccanismo di    &#13;
 valutazione dell'attività  svolta  che  ampliasse  l'arco  temporale    &#13;
 entro  cui  ricomprendere  gli  anni  di  servizio  prestato  - o che    &#13;
 comunque consentisse di apprezzare l'assolvimento degli  obblighi  di    &#13;
 leva - sarebbe di competenza di scelte discrezionali del legislatore,    &#13;
 comportando diverse, possibili soluzioni caratterizzate,  oltretutto,    &#13;
 da notevole specificità tecnica;                                        &#13;
      che,  inoltre, le ragioni di transitorietà e le caratteristiche    &#13;
 di eccezionalità della norma impugnata (già sottolineate da  questa    &#13;
 Corte   con   sentenza   n.   209   del  9  luglio  1986),  imponendo    &#13;
 l'accertamento  dei  requisiti  sulla  base  di  elementi   certi   e    &#13;
 temporalmente  circoscritti, avrebbero reso oltremodo problematica la    &#13;
 previsione di un congegno come quello auspicato dal giudice a quo;       &#13;
      che,  per  tale  assorbente  ragione, non può essere ammesso il    &#13;
 giudizio di costituzionalità  relativo  alla  questione  così  come    &#13;
 prospettata.                                                             &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 76 della legge 20 maggio  1982,    &#13;
 n.  270  ("Revisione  della disciplina del reclutamento del personale    &#13;
 docente della scuola materna, elementare,  secondaria  ed  artistica,    &#13;
 ristrutturazione degli organici, adozione di misure idonee ad evitare    &#13;
 la formazione di precariato e  sistemazione  del  personale  precario    &#13;
 esistente"),  sollevata dal Consiglio di giustizia amministrativa per    &#13;
 la Regione siciliana, in relazione agli artt. 3, 33, comma quinto,  e    &#13;
 51,  comma  primo,  della  Costituzione,  con  l'ordinanza  di cui in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 giugno 1988.                                &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 16 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
