<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>107</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:107</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/03/1998</data_decisione>
    <data_deposito>06/04/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, avv. Fernanda CONTRI, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 445, comma 2,    &#13;
 del codice di procedura penale, promosso con ordinanza emessa  il  27    &#13;
 maggio  1997 dal Tribunale di Montepulciano nel procedimento penale a    &#13;
 carico di L. B., iscritta al n. 501 del  registro  ordinanze  1997  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 35, prima    &#13;
 serie speciale, dell'anno 1997.                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio del 25 febbraio 1998 il giudice    &#13;
 relatore Guido Neppi Modona;                                             &#13;
   Ritenuto che  il  Tribunale  di  Montepulciano,  nel  corso  di  un    &#13;
 procedimento  di  esecuzione  nel  quale  la  persona a cui era stata    &#13;
 applicata  una  pena  patteggiata  aveva  presentato   richiesta   di    &#13;
 estinzione  del reato, ha sollevato, in riferimento all'art. 24 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art. 445,    &#13;
 comma 2, del codice di procedura penale, nella parte in cui impone al    &#13;
 richiedente di provare di non  avere  commesso  alcun  delitto  della    &#13;
 stessa indole nel termine di cinque anni previsto dalla legge;           &#13;
     che  il  giudice  a  quo  premesso  che  all'imputato  era  stata    &#13;
 applicata la pena, condizionalmente sospesa, di  mesi  due  e  giorni    &#13;
 venti  di  reclusione e di lire novecentomila di multa per il delitto    &#13;
 di cui all'art. 73 del d.P.R. 309 del 1990, assume  che  l'art.  445,    &#13;
 comma  2,  cod.  proc.  pen.  pone a carico del condannato l'onere di    &#13;
 provare  di  non  avere commesso alcun delitto nel termine prescritto    &#13;
 dalla legge;                                                             &#13;
     che, ad avviso del rimettente, tale prova non  può  essere  data    &#13;
 né  con  il certificato del casellario giudiziale, ove sono iscritte    &#13;
 solo le  sentenze  di  condanna  divenute  irrevocabili,  né  con  i    &#13;
 certificati   dei   carichi  pendenti:  questi  ultimi,  infatti,  se    &#13;
 rilasciati  dal  pubblico  ministero  del  luogo  di  residenza,  non    &#13;
 contengono   eventuali   pendenze  in  altri  luoghi  del  territorio    &#13;
 nazionale, mentre la produzione dei certificati dei carichi  pendenti    &#13;
 rilasciati  da  tutti gli uffici del pubblico ministero esistenti sul    &#13;
 territorio nazionale, oltre a tradursi in una probatio diabolica, non    &#13;
 potrebbe escludere che il condannato  abbia  commesso  delitti  della    &#13;
 stessa indole, non ancora prescritti e iscritti contro ignoti;           &#13;
     che  ne  conseguirebbe  che  la  dichiarazione  di estinzione del    &#13;
 reato, pur essendo espressamente prevista dalla  legge,  non  sarebbe    &#13;
 concretamente   ottenibile   dal   condannato,   che   si  troverebbe    &#13;
 nell'impossibilità di fornire prova dei fatti che  sono  presupposto    &#13;
 dell'estinzione del reato;                                               &#13;
     che tale disciplina si porrebbe in contrasto con l'art. 24 Cost.,    &#13;
 in  quanto  precluderebbe  al  condannato di ottenere - provando - il    &#13;
 beneficio dell'estinzione del reato;                                     &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che la  questione  venga  dichiarata  infondata,  in    &#13;
 quanto  la  prova  della avvenuta commissione di un reato può essere    &#13;
 desunta solo dal passaggio in giudicato della  relativa  sentenza,  e    &#13;
 non anche dall'esistenza di procedimenti eventualmente risultanti dai    &#13;
 certificati dei carichi pendenti;                                        &#13;
     che  pertanto, ad avviso dell'Avvocatura generale dello Stato, al    &#13;
 fine di ottenere la dichiarazione di  estinzione  del  reato  sarebbe    &#13;
 sufficiente la produzione del certificato giudiziale.                    &#13;
   Considerato   che  il  rimettente  muove  dal  duplice  presupposto    &#13;
 interpretativo  che  l'elemento  ostativo   alla   dichiarazione   di    &#13;
 estinzione  del  reato consista nella mera commissione di un reato e,    &#13;
 quindi, nella semplice esistenza di un procedimento penale pendente a    &#13;
 carico del condannato e che l'onere di provarne  l'inesistenza  gravi    &#13;
 sullo stesso condannato;                                                 &#13;
     che entrambi i presupposti sono palesemente erronei;                 &#13;
     che  è del tutto incontroverso, anche alla luce del principio di    &#13;
 cui all'art. 27,  secondo  comma,  Cost.,  che  l'effetto  preclusivo    &#13;
 dell'estinzione  del  reato  non  consegue  al  mero  fatto  di avere    &#13;
 commesso  un  delitto  entro  il   termine   di   cinque   anni,   ma    &#13;
 all'accertamento  della  responsabilità  contenuto  in  una sentenza    &#13;
 irrevocabile di condanna;                                                &#13;
     che il rimettente, per contro, non tiene conto della  distinzione    &#13;
 tra  i  due  momenti  della commissione del reato entro il termine di    &#13;
 cinque anni prescritto dall'art. 445, comma  2,  cod.  proc.  pen.  e    &#13;
 dell'accertamento giudiziale della colpevolezza, che può intervenire    &#13;
 anche dopo la scadenza di tale termine;                                  &#13;
     che  tale  distinzione  sta  alla base della disciplina riservata    &#13;
 all'istituto, affine  a  quello  oggetto  del  presente  giudizio  di    &#13;
 costituzionalità,  dell'estinzione  del reato per il quale sia stata    &#13;
 concessa la sospensione condizionale della pena (art. 167 cod. pen.),    &#13;
 e della revoca di diritto  della  sospensione  nel  caso  in  cui  il    &#13;
 condannato  commetta  un  delitto  ovvero  una  contravvenzione della    &#13;
 stessa indole nei termini stabiliti (art. 168, comma primo, numero 1,    &#13;
 cod. pen.); istituto nei  cui  confronti  giurisprudenza  e  dottrina    &#13;
 pacificamente   distinguono  tra  commissione  del  nuovo  delitto  e    &#13;
 relativo accertamento giudiziale, ricollegando solo a quest'ultimo la    &#13;
 revoca della sospensione condizionale (e della conseguente estinzione    &#13;
 del reato), a nulla rilevando che la sentenza irrevocabile sia  stata    &#13;
 pronunciata oltre il termine stabilito;                                  &#13;
     che   è   comunque   erroneo   anche   il   secondo  presupposto    &#13;
 interpretativo su cui  si  basa  il  rimettente,  assumendo  che  nel    &#13;
 procedimento  di  esecuzione  sarebbe  posto  a carico del condannato    &#13;
 l'onere  di  provare  l'inesistenza  di  elementi  impeditivi   della    &#13;
 estinzione del reato;                                                    &#13;
     che, a contrario l'art. 666, comma 5, cod. proc. pen. attribuisce    &#13;
 al  giudice  dell'esecuzione  il  potere  di  chiedere alle autorità    &#13;
 competenti tutti i documenti e le informazioni di cui abbia bisogno;     &#13;
     che al fine di provvedere sulla richiesta di  dichiarare  estinto    &#13;
 il  reato  ex  art.  445,  comma  2,  cod. proc. pen. il giudice può    &#13;
 limitarsi ad acquisire il certificato del casellario giudiziale,  per    &#13;
 verificare  se  siano  o  meno  intervenute  sentenze irrevocabili di    &#13;
 condanna relative a delitti commessi entro il termine di cinque anni;    &#13;
     che pertanto la questione deve essere  dichiarata  manifestamente    &#13;
 infondata.                                                               &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  445,  comma  2,  del  codice  di procedura    &#13;
 penale, sollevata, in riferimento all'art. 24 della Costituzione, dal    &#13;
 Tribunale di Montepulciano, con l'ordinanza in epigrafe.                 &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 26 marzo 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore: Neppi Modona                          &#13;
                     Il cancelliere: Neppi Modona                         &#13;
   Depositata in cancelleria il 6 aprile 1998.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
