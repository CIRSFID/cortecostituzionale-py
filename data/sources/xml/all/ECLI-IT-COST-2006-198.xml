<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>198</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:198</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Romano Vaccarella</relatore_pronuncia>
    <redattore_pronuncia>Romano Vaccarella</redattore_pronuncia>
    <data_decisione>03/05/2006</data_decisione>
    <data_deposito>11/05/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 78 del codice di procedura civile, promosso con ordinanza del 14 maggio 2005 dal Tribunale di Asti, nel procedimento civile vertente tra Orlando Canio Donato e Pata Bartolo ed altra, iscritta al n. 484 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 40, prima serie speciale, dell'anno 2005. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 5 aprile 2006 il Giudice relatore Romano Vaccarella. &#13;
      Ritenuto che, con ordinanza del 14 maggio 2005, il Tribunale di Asti – nel corso di un giudizio in cui, comparsa personalmente in udienza una parte convenuta rimasta contumace, il giudice istruttore ne aveva personalmente acclarato le gravi condizioni di demenza psichica, e quindi la completa incapacità di intendere e di volere – ha sollevato, in riferimento agli artt. 3, comma primo, e 24, comma secondo, della Costituzione, questione di legittimità costituzionale dell'art. 78 del codice di procedura civile, nella parte in cui, secondo il «diritto vivente», non prevede la nomina di un curatore speciale anche per l'incapace naturale; &#13;
      che, quanto alla rilevanza della questione, segnala il rimettente che egli si trova nella condizione di dover pronunciare i provvedimenti necessari per l'ulteriore corso del processo, senza avere alcuna possibilità di adottare un rimedio giudiziale a protezione della parte, in quanto, «nonostante sia pienamente dimostrato lo stato di incapacità naturale» della stessa, l'ordinamento nulla prevede a tutela di chi non sia in grado, a causa di patologia psichica non ancora giudizialmente accertata, di seguire consapevolmente il giudizio; &#13;
      che la nomina di un curatore speciale, ex art. 78 cod. proc. civ., è collegata all'esistenza di uno stato di incapacità legale, mentre l'ipotesi disciplinata dall'art. 4, comma 5, secondo periodo, della legge 1° dicembre 1970, n. 898 (Disciplina dei casi di scioglimento del matrimonio), costituisce normativa speciale, inapplicabile, in quanto tale, al di fuori del particolare ambito cui si riferisce;  &#13;
      che, quanto alla non manifesta infondatezza del dubbio, rileva il giudice a quo che l'impossibilità, in cui versa l'incapace naturale, di valutare le scelte da compiere e di determinarsi  conseguentemente, si risolvono in una palese violazione dell'art. 24 della Costituzione, mentre la disparità di trattamento tra l'interdetto legale e l'incapace naturale, ovvero tra il cittadino malato di mente, cui il presidente del tribunale nomini un curatore speciale ai sensi dell'art. 4, comma 5, secondo periodo, della legge n. 898 del 1970, e il cittadino che, nella stessa situazione psichica, sia parte di un processo civile, concreta una lesione del principio di uguaglianza sancito dall'art. 3, comma primo, della Costituzione;  &#13;
      che, considerata anche l'insufficienza del rimedio della segnalazione (nella specie già effettuata) al PM dello stato d'incapacità della convenuta contumace – stante la non retroattività degli effetti dei mezzi di tutela che questi riterrà eventualmente di promuovere –, si pone la questione di legittimità costituzionale dell'art. 78 cod. proc. civ., nella parte in cui, secondo il «diritto vivente», non ne è prevista l'applicazione anche all'incapace naturale, in riferimento agli artt. 3, comma primo, e 24, comma secondo, della Costituzione;  &#13;
      che, è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto alla Corte di disporre la restituzione degli atti al giudice a quo, in considerazione dello jus superveniens, costituito dalla legge 9 gennaio 2004, n. 6 (Introduzione nel libro primo, titolo XII, del codice civile, del capo I, relativo all'istituzione dell'amministrazione di sostegno e modifica degli artt. 388, 414, 417, 418, 426, 427 e 429 del codice civile, in materia di interdizione e di inabilitazione, nonché relative norme di attuazione, di coordinamento e finali). &#13;
      Considerato che il Tribunale di Asti dubita della legittimità costituzionale, in riferimento agli artt. 3, comma primo, e 24, comma secondo, della Costituzione, dell'art. 78 del codice di procedura civile nella parte in cui, secondo il «diritto vivente», non prevede la nomina di un curatore speciale anche per l'incapace naturale; &#13;
      che la questione è manifestamente infondata, dal momento che, da un lato, una norma certamente eccezionale (dettata in tema di comparizione personale dei coniugi nel giudizio di scioglimento del matrimonio) non può costituire parametro di riferimento per una disciplina generale, e, dall'altro lato, l'ordinamento prevede – specie a seguito della legge 9 gennaio 2004, n. 6 – forme di protezione dell'incapace naturale, che, attesa l'estrema varietà di ipotesi nelle quali tale forma di incapacità può darsi (sentenza n. 468 del 1992; ordinanza n. 206 del 1995), prendono già in considerazione – anche attraverso provvedimenti provvisori – l'esigenza che tale protezione consegua ad un procedimento adeguato alla gravità di un provvedimento che incide sulla capacità di agire, anche processuale, del soggetto che appare affetto da incapacità naturale. &#13;
      Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 78 del codice di procedura civile sollevata, in riferimento agli artt. 3, comma primo, e 24, comma secondo, della Costituzione, dal Tribunale di Asti con l'ordinanza in epigrafe. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 maggio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Romano VACCARELLA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria l'11 maggio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
