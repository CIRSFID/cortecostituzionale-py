<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>116</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:116</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/11/1967</data_decisione>
    <data_deposito>23/11/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. &#13;
 LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale  dell'art.    3,  primo  &#13;
 comma,  del  D.P.R. 9 agosto 1966, n. 869, recante "Norme di attuazione  &#13;
 dello Statuto speciale della Regione Friuli-Venezia Giulia  in  materia  &#13;
 di  igiene e sanità, assistenza sanitaria ed ospedaliera, recupero dei  &#13;
 minorati  fisici  e  mentali",  promosso  con  ricorso  della   Regione  &#13;
 Friuli-Venezia  Giulia  notificato  il  25 novembre 1966, depositato in  &#13;
 cancelleria il 28 successivo ed iscritto al n. 25 del Registro  ricorsi  &#13;
 1966.                                                                    &#13;
     Visto  l'atto  di  costituzione  del  Presidente  del Consiglio dei  &#13;
 Ministri;                                                                &#13;
     udita nell'udienza pubblica del 18 ottobre 1967  la  relazione  del  &#13;
 Giudice Giovanni Cassandro;                                              &#13;
     uditi l'avv. Raffaele Oriani, per la Regione Friuli-Venezia Giulia,  &#13;
 e  il  sostituto  avvocato generale dello Stato Francesco Agrò, per il  &#13;
 Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ricorso notificato al Presidente  del  Consiglio    il  25  &#13;
 novembre  1966  e  depositato  nella  cancelleria di questa Corte il 28  &#13;
 novembre successivo, la Regione Friuli-Venezia Giulia, rappresentata  e  &#13;
 difesa  dall'avv.  Raffaele Oriani, ha impugnato le norme contenute nel  &#13;
 primo comma dell'art. 3 del D.P.R. 9 agosto  1966,  n.  869,  le  quali  &#13;
 stabiliscono  che  "sono  riservate al Ministero della sanità all'alta  &#13;
 sorveglianza sugli enti  sanitari  e  le  attribuzioni  in  materia  di  &#13;
 classificazione  degli  ospedali  nonché  i  provvedimenti  intesi  ad  &#13;
 assicurare in tutto il territorio  nazionale  una  adeguata  assistenza  &#13;
 ospedaliera".                                                            &#13;
     Secondo  la  Regione queste norme sarebbero in contrasto con l'art.  &#13;
 5, n. 16, dello  Statuto  approvato  con  la  legge  costituzionale  31  &#13;
 gennaio  1963,  n.  1,  il  quale  assegna  alla  Regione  una potestà  &#13;
 legislativa secondaria e concorrente in materia di "igiene  e  sanità,  &#13;
 di assistenza sanitaria e ospedaliera, nonché il recupero dei minorati  &#13;
 fisici e mentali".                                                       &#13;
     Il   contrasto  sorgerebbe,  in  primo  luogo,  per  ragione  della  &#13;
 disposizione, che riserva al Ministero della sanità le attribuzioni in  &#13;
 materia di classificazione degli ospedali.   La  difesa  della  Regione  &#13;
 argomenta  che, mediante codesta riserva, alla Regione sarebbe vietato,  &#13;
 pur nell'osservanza dei principi fondamentali posti dalla  legge  dello  &#13;
 Stato  (nel  caso  il R.D. 30 settembre 1938, n.  1631), di "rapportare  &#13;
 alle concrete esigenze e situazioni  locali  la  classificazione  degli  &#13;
 ospedali";  e  di  esercitare  di  conseguenza  una  qualsiasi funzione  &#13;
 amministrativa ex  art.  8  del  medesimo  Statuto,  che  alla  Regione  &#13;
 competerebbe,  invece, anche nell'ipotesi che la legislazione regionale  &#13;
 si limitasse a  ripetere  la  legislazione  statale  o  addirittura  si  &#13;
 astenesse    dal   legiferare.   Né   potrebbe   obiettarsi   che   la  &#13;
 classificazione  degli  ospedali  esuli   dal   campo   dell'assistenza  &#13;
 ospedaliera,  dato anzi che essa la influenza e la condiziona a tutti i  &#13;
 livelli.  In secondo luogo, il contrasto sorgerebbe per  ragione  della  &#13;
 disposizione  che  riserva  al Ministero della sanità "i provvedimenti  &#13;
 intesi ad assicurare in tutto  il  territorio  nazionale  una  adeguata  &#13;
 assistenza ospedaliera".                                                 &#13;
     Secondo  la  difesa,  infatti,  questa  disposizione  per  la   sua  &#13;
 ampiezza toglierebbe ogni potere alla Regione nel settore  ospedaliero,  &#13;
 con  gravissimo  pregiudizio degli interessi della collettività che la  &#13;
 norma  de  qua  vorrebbe  tutelare.  Né  si  potrebbe  argomentare  in  &#13;
 contrario  che  l'assistenza  ospedaliera è materia tale da richiedere  &#13;
 uniformità di criteri per tutto il territorio nazionale.    La  difesa  &#13;
 regionale,  pur non contestando la priorità della legislazione statale  &#13;
 circa la determinazione dei  criteri,  che  debbono  essere  alla  base  &#13;
 dell'assistenza  ospedaliera,  contesta  che  alla Regione possa essere  &#13;
 tolta, nel settore, ogni ragione di intervento e richiama al  proposito  &#13;
 la  sentenza  n.  15 del 26 gennaio 1957 di questa Corte, che dichiarò  &#13;
 l'incostituzionalità dell'art. 19 del D.P.R. 19 maggio 1950,  n.  327,  &#13;
 contenente norme di attuazione dello Statuto regionale della Sardegna.   &#13;
     Se  lo  Stato,  prosegue  la  difesa,  deve  assicurare un'adeguata  &#13;
 assistenza ospedaliera, ne discende che  ogni intervento della  Regione  &#13;
 nel  settore  sarebbe  "un  di    più"  e  non potrebbe nemmeno essere  &#13;
 validamente  e  giustificatamente  finanziato  con   fondi   regionali.  &#13;
 Nemmeno   può   farsi   richiamo  alla  progettata  riforma  sanitaria  &#13;
 nazionale, perché essa è di quelle    riforme  economico-sociali  che  &#13;
 limitano  la  competenza    legislativa  regionale e non troverebbe mai  &#13;
 ostacolo alla sua applicazione in preesistenti strutture  di  carattere  &#13;
 regionale.                                                               &#13;
     La  Regione  richiama  poi  le  difficoltà  di  ordine pratico che  &#13;
 l'attribuzione allo Stato e alla Regione  di    competenze  frazionarie  &#13;
 nella  medesima  materia  comporterebbe,  tanto  che la Costituzione ha  &#13;
 cercato di evitarle,  prevedendo  che  lo  Stato  possa  delegare  alla  &#13;
 Regione  l'esercizio  di  proprie   funzioni amministrative (art.   119  &#13;
 della Costituzione e art. 10 dello Statuto Friuli- Venezia  Giulia);  e  &#13;
 lamenta  che  non  sia stato tenuto in  alcuna considerazione il parere  &#13;
 della commissione paritetica  prevista  dall'art.  65  dello    Statuto  &#13;
 speciale  né  quelle  dei  ministeri   interpellati. Fa istanza, anzi,  &#13;
 perché venga disposta  dalla Corte l'acquisizione di  questo  e  degli  &#13;
 altri  pareri    dei  ministeri  interessati, nonché dei verbali delle  &#13;
 riunioni della commissione paritetica.                                   &#13;
     2.  -  Resiste al ricorso il Presidente del Consiglio dei Ministri,  &#13;
 rappresentato e difeso dall'Avvocatura dello  Stato, che, nell'atto  di  &#13;
 deduzioni depositato il 15 dicembre 1966 e in una memoria depositata il  &#13;
 22  marzo  1967,  chiede  che  sia  dichiarata non fondata la sollevata  &#13;
 questione di costituzionalità.                                          &#13;
     L'Avvocatura si richiama alla sentenza n. 51  del  1965  di  questa  &#13;
 Corte,   che,   relativamente  alla  Regione  Trentino-Alto  Adige,  ha  &#13;
 dichiarato infondata la tesi dell'appartenenza alla Regione del  potere  &#13;
 normativo  e  quindi amministrativo in materia di classificazione degli  &#13;
 ospedali, dovendosi questa ispirare a  criteri  uniformi  in  tutto  il  &#13;
 territorio  nazionale.                                                   &#13;
     L'altra  competenza  riservata  allo Stato deve intendersi, secondo  &#13;
 l'Avvocatura, limitata ad assicurare in tutto il  territorio  nazionale  &#13;
 un'adeguata  assistenza  ospedaliera,    non  già  estesa  al punto di  &#13;
 impedire alla Regione di  concorrere a livello regionale ad  assicurare  &#13;
 codesta   assistenza.   D'altra   parte,   l'indagine   se   lo  Stato,  &#13;
 nell'esercizio della potestà  amministrativa che gli spetta in  questa  &#13;
 materia,  possa  invadere  la  sfera  delle  attribuzioni regionali, è  &#13;
 un'indagine  da  compiere  in  concreto  caso  per  caso,  in  sede  di  &#13;
 regolamento  di  competenza  ai sensi dell'art. 39 della legge 11 marzo  &#13;
 1953, n. 87.                                                             &#13;
     L'Avvocatura sostiene infine che  la  disposizione  impugnata  mira  &#13;
 anche ad assicurare la futura generale riforma ospedaliera e sanitaria,  &#13;
 che si fonda su alcune attribuzioni statali definite dalla Costituzione  &#13;
 (art.    32,  primo  comma, 38, primo e secondo comma), che non possono  &#13;
 trovare  ostacolo  in  situazioni  giuridiche  particolari  a   singole  &#13;
 regioni.  Anche  su  questo  punto l'Avvocatura si richiama alla citata  &#13;
 sentenza di questa Corte.                                                &#13;
     3. - All'udienza del 18 ottobre  1967  le  parti  hanno  brevemente  &#13;
 riassunto le proprie tesi e insistito nelle conclusioni già prese.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - L'art. 3, primo comma, delle norme di attuazione dello Statuto  &#13;
 per  il  Friuli-  Venezia Giulia contenute nel D.P.R. 9 agosto 1966, n.  &#13;
 869, riserva allo Stato l'alta sorveglianza  sugli  enti  sanitari;  le  &#13;
 attribuzioni   in   materia   di   classificazione  degli  ospedali;  i  &#13;
 provvedimenti intesi ad assicurare in tutto il territorio nazionale una  &#13;
 adeguata assistenza ospedaliera.                                         &#13;
     Di queste tre competenze la Regione rivendica come sue la seconda e  &#13;
 la terza, non già la prima che, in una fattispecie  analoga,  un'altra  &#13;
 Regione  a  statuto  speciale,  il  Trentino-Alto Adige, aveva ritenuto  &#13;
 sottratta  illegittimamente  alla  propria  sfera  di  competenza.   La  &#13;
 questione  di  legittimità  costituzionale  resta  perciò  limitata a  &#13;
 quelle due norme; ma, pure  in  tali  limiti,  deve  essere  dichiarata  &#13;
 infondata.                                                               &#13;
     2.  -  Che  la  classificazione degli ospedali sia competenza dello  &#13;
 Stato, legittimamente esercitata dal medico provinciale, è stato  già  &#13;
 affermato  dalla Corte nei confronti della Regione Trentino Alto Adige,  &#13;
 nonostante che a questa Regione sia  stata  attribuita  in  materia  di  &#13;
 assistenza  sanitaria e ospedaliera potestà legislativa primaria (art.  &#13;
 4, n. 12: assistenza sanitaria  e  ospedaliera)  e  nonostante  che  le  &#13;
 relative  norme  di    attuazione  non  facciano espressa riserva della  &#13;
 competenza statale nella materia della quale si controverte (D.P.R.  18  &#13;
 febbraio 1958, n. 307).                                                  &#13;
     Le  ragioni  che  persuasero  in  quella  circostanza  la  Corte  a  &#13;
 respingere il ricorso della Regione (cfr. sentenza  n.  51  del  1965),  &#13;
 argomentando  dal  maggiore al minore, sono ancora più valide nel caso  &#13;
 presente della Regione Friuli-Venezia Giulia che, secondo l'art. 5,  n.  &#13;
 16,  dello  Statuto (approvato con la legge costitituzionale 31 gennaio  &#13;
 1963, n. 1), possiede in materia di assistenza sanitaria e  ospedaliera  &#13;
 potestà legislativa secondaria o concorrente, e rispetto alla quale le  &#13;
 norme  di  attuazione,  come  è  stato  riferito, hanno esplicitamente  &#13;
 riservato  alle  Stato  la  classificazione  degli  ospedali.  Si  può  &#13;
 aggiungere  a  chiarimento  e  a  integrazione  che  la classificazione  &#13;
 ospedaliera si fonda, oltre che sulle funzioni svolte  dagli  ospedali,  &#13;
 sulla  struttura  e  organizzazione  loro, che ne costituisce, anzi, il  &#13;
 presupposto. E poiché è evidente che la struttura fondamentale  degli  &#13;
 ospedali  deve  essere nelle sue linee essenziali unitaria per tutto il  &#13;
 territorio nazionale, perché  ne  discendono  conseguenze  valide  per  &#13;
 l'intero  ordinamento  statale  in  questo settore, lo stesso carattere  &#13;
 unitario deve presentare la classificazione che su quella struttura  si  &#13;
 fonda,  come  è,  del  resto,  confermato  dalla  circostanza  che  le  &#13;
 disposizioni relative si trovano in capite  alle  "Norme  generali  per  &#13;
 l'ordinamento  dei  servizi  sanitari  e  del personale sanitario degli  &#13;
 ospedali" (R.D. 30 settembre  1938,  n.  1631),  e  dominano  tutta  la  &#13;
 materia.  Tanto l'assistenza ospedaliera quanto la struttura sanitaria,  &#13;
 che sono tra loro connesse, non  possono  mutare,  nell'essenziale,  da  &#13;
 regione a regione.                                                       &#13;
     Né  ha  valore  l'obiezione  mossa  dalla  difesa regionale che il  &#13;
 riconoscimento della  riserva  statale  comporti  un  frazionamento  di  &#13;
 competenze  che  il sistema della Costituzione e degli Statuti speciali  &#13;
 ha voluto evitare, tanto che è  ipotizzata  a  tal  fine  finanche  la  &#13;
 delegazione  alla  Regione, mediante legge, di competenze proprie dello  &#13;
 Stato (art. 118 della Costituzione). Quel sistema, viceversa,  conforma  &#13;
 le  competenze amministrative alla potestà legislativa della Regione e  &#13;
 tiene quelle nei limiti di questa; tanto che si potrebbe dire che  alla  &#13;
 Regione  spetti  tanto  di  amministrazione  quanto di legislazione. La  &#13;
 divisione e insieme il coordinamento  delle  competenze  legislative  e  &#13;
 amministrative  è perciò un momento essenziale di un ordinamento che,  &#13;
 pur nella presenza di autonomie regionali, resta unitario, e postula in  &#13;
 conseguenza un coordinamento e una collaborazione tra Stato  e  Regione  &#13;
 sia  a  presidio dell'unità dello Stato, sia a garanzia di un armonico  &#13;
 svolgimento dei rapporti tra i due Enti.                                 &#13;
     3. - La riserva allo Stato dei provvedimenti intesi  ad  assicurare  &#13;
 in  tutto  il territorio nazionale una adeguata assistenza ospedaliera,  &#13;
 trova, ad avviso della Corte, il suo fondamento, oltre che nel rispetto  &#13;
 dell'interesse  nazionale,  nell'obbligo  che  ogni   Regione   ha   di  &#13;
 osservare,  senza esclusione delle materie per le quali è riconosciuta  &#13;
 una competenza legislativa primaria, le riforme economico-sociali della  &#13;
 Repubblica, alla quale la Costituzione impone  di  tutelare  la  salute  &#13;
 come    fondamentale   diritto   dell'individuo   e   interesse   della  &#13;
 collettività e di garantire cure gratuite agli  indigenti  (art.  32),  &#13;
 nonché  di  assicurare a ogni cittadino inabile al lavoro e sprovvisto  &#13;
 dei mezzi necessari per vivere l'assistenza sociale (art.  38), che, in  &#13;
 questa sede, comprende, com'è ovvio,  l'assistenza  ospedaliera.    La  &#13;
 interpretazione  corretta della impugnata norma di attuazione altro non  &#13;
 comporta perciò, se non che lo Stato deve poter assicurare su tutto il  &#13;
 territorio nazionale un  eguale  standard  di  assistenza  ospedaliera,  &#13;
 integrando  o sostituendo quella regionale là dove sia insufficiente o  &#13;
 carente. Il principio ora richiamato della puntuale corrispondenza  tra  &#13;
 potestà  legislativa  e  potestà  amministrativa che vale ovviamente,  &#13;
 come nei confronti della Regione,  così  nei  confronti  dello  Stato,  &#13;
 garantisce  che  i  "provvedimenti"  dello  Stato  in questa materia si  &#13;
 terranno  nei  limiti  della  competenza  statale.  E,  nel   caso   di  &#13;
 sconfinamenti,  non  manca  il  giudice che possa reprimerli in sede di  &#13;
 conflitti di attribuzione.                                               &#13;
     La preoccupazione della Regione  che  la  formula  delle  norme  di  &#13;
 attuazione  sacrifichi affatto la sua competenza in materia sanitaria e  &#13;
 il timore, che essa manifesta, di non poter iscrivere somme in bilancio  &#13;
 destinate all'assistenza ospedaliera, sono perciò del tutto infondati.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la  questione,  sollevata  con  ricorso  della  &#13;
 Regione Friuli- Venezia Giulia, sulla legittimità costituzionale delle  &#13;
 norme  contenute  nel primo comma dell'art. 3 del D.P.R. 9 agosto 1966,  &#13;
 n. 869 ("Norme di  attuazione  dello  Statuto  speciale  della  Regione  &#13;
 Friuli-Venezia  Giulia  in  materia  di  igiene  e  sanità, assistenza  &#13;
 sanitaria e ospedaliera, recupero dei minorati fisici e  mentali"),  in  &#13;
 riferimento  all'art.  5,  n.  16, dello Statuto speciale della Regione  &#13;
 Friuli-Venezia Giulia.                                                   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 15 novembre 1967.                             &#13;
                                   GASPARE  AMBROSINI - ANTONINO PAPALDO  &#13;
                                   - NICOLA JAEGER - GIOVANNI  CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
