<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1962</anno_pronuncia>
    <numero_pronuncia>72</numero_pronuncia>
    <ecli>ECLI:IT:COST:1962:72</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Castelli Avolio</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/06/1962</data_decisione>
    <data_deposito>26/06/1962</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Dott. &#13;
 MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. GIUSEPPE &#13;
 CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. &#13;
 GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - &#13;
 Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - &#13;
 Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  10,  terzo  &#13;
 comma, del D.P.R. 26 aprile 1957, n. 818, promosso con ordinanza emessa  &#13;
 il  14  dicembre  1960  dal  Tribunale  di Roma nel procedimento civile  &#13;
 vertente tra Cardelli Filippo e l'Istituto nazionale  della  previdenza  &#13;
 sociale,  iscritta  al  n.  29 del Registro ordinanze 1961 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 83 del 1 aprile 1961.       &#13;
     Vista la dichiarazione di intervento del Presidente  del  Consiglio  &#13;
 dei Ministri:                                                            &#13;
     udita  nell'udienza  pubblica  del  6  giugno 1962 la relazione del  &#13;
 Giudice Giuseppe Castelli Avolio;                                        &#13;
     uditi l'avvocato Vittorio  Santoro,  per  il  Cardelli,  l'avvocato  &#13;
 Guido  Nardone, per l'Istituto nazionale della previdenza sociale, e il  &#13;
 sostituto avvocato generale dello Stato Valente Simi, per il Presidente  &#13;
 del Consiglio dei Ministri.                                              &#13;
 Ritenuto che nel giudizio vertente dinanzi al  Tribunale  di  Roma  fra  &#13;
 Cardelli  Filippo,  già ammesso al godimento di pensione ordinaria per  &#13;
 il servizio prestato presso le Ferrovie dello Stato, e  l'I.N.P.S.,  al  &#13;
 quale  il Cardelli aveva chiesto la pensione di invalidità e vecchiaia  &#13;
 in base ai contributi riferentisi al periodo precedente di avventiziato  &#13;
 presso l'Amministrazione ferroviaria, è stata sollevata  questione  di  &#13;
 legittimità  costituzionale del terzo comma dell'art. 10 del D.P.R. 26  &#13;
 aprile 1957, n. 818, che non consente il computo dei  contributi  così  &#13;
 detti  figurativi per il tempo del servizio militare, quando essi siano  &#13;
 stati   computati   o   siano   computabili   per   altri   trattamenti  &#13;
 pensionistici:                                                           &#13;
     che  si  assumeva  in  giudizio,  dalla  difesa  del  Cardelli,  la  &#13;
 illegittimità  costituzionale  di  tale  disposizione  in  riferimento  &#13;
 all'art.   76  della  Costituzione,  per  eccesso  dai  limiti  segnati  &#13;
 all'esercizio della delega, di cui all'art. 37  della  legge  4  aprile  &#13;
 1952, n. 218;                                                            &#13;
     che  il  Tribunale,  ritenendo  di  dover  escludere  che  la norma  &#13;
 impugnata, col porre il divieto del computo del servizio militare  agli  &#13;
 effetti  di due diversi trattamenti pensionistici, costituisse norma di  &#13;
 attuazione della legge delegante, oppure di  coordinamento  con  questa  &#13;
 della  legislazione  vigente,  ammise  che la questione sollevata fosse  &#13;
 rilevante e non manifestamente infondata,  epperò,  con  ordinanza  14  &#13;
 dicembre 1960, ne rinviava la risoluzione alla Corte costituzionale;     &#13;
     che,   nel   procedimento   seguitone   dinanzi   a  questa  Corte,  &#13;
 l'Avvocatura generale dello Stato, costituitasi in  rappresentanza  del  &#13;
 Presidente  del Consiglio dei Ministri, oltre a notare che il Tribunale  &#13;
 aveva omesso di rilevare l'applicabilità  o  meno  della  disposizione  &#13;
 impugnata  al  caso sottoposto al suo esame, dato che la domanda per la  &#13;
 pensione dell'I.N.P.S. era stata presentata dal Cardelli fin dal  1956,  &#13;
 ha pregiudizialmente opposto il difetto della rilevanza della questione  &#13;
 sollevata  in quanto la disposizione del decreto delegato sarebbe stata  &#13;
 assorbita e fatta propria da un disposto di legge, e cioè dall'art. 10  &#13;
 della legge 20 febbraio 1958, n. 55;                                     &#13;
     che la difesa dell'I.N.P.S. ha, dal canto suo,  insistito  su  tale  &#13;
 rilievo,   sostenendo   anzi  che  la  norma  impugnata  sarebbe  stata  &#13;
 tacitamente abrogata con la citata legge n. 55 del 1958, e  all'udienza  &#13;
 di  discussione  ha  chiesto  che la Corte dichiari la cessazione della  &#13;
 materia del contendere in ordine alla sollevata questione;               &#13;
 Considerato che questa Corte non può entrare nell'esame  dei  riflessi  &#13;
 che  l'art.  10  della  legge 20 febbraio 1958, n. 55, può avere sulla  &#13;
 risoluzione della controversia vertente  fra  le  parti;  e  la  stessa  &#13;
 questione,    presentata    quale   pregiudiziale   a   questa   Corte,  &#13;
 dell'affermato assorbimento da parte  della  nuova  legge  della  norma  &#13;
 impugnata  di  illegittimità  o  della sua assunta tacita abrogazione,  &#13;
 implica un accertamento ed una dichiarazione che esulano dal  controllo  &#13;
 di legittimità costituzionale a questa Corte demandato, trattandosi di  &#13;
 questioni  che  vanno  svolte  e  decise su piano diverso e con diverso  &#13;
 effetto (sentenza 5 giugno 1956, n. 1);                                  &#13;
     che, peraltro, la Corte costituzionale ha ritenuto che la questione  &#13;
 di legittimità costituzionale  allora  può  dirsi  rilevante  per  la  &#13;
 decisione  di  merito  quando  il  giudizio  può effettivamente essere  &#13;
 definito in base alla norma impugnata, e che perciò  vanno  restituiti  &#13;
 gli  atti  al giudice a quo se egli non abbia preliminarmente accertato  &#13;
 che  la  norma  impugnata  è  applicabile  al   rapporto   controverso  &#13;
 (ordinanza  6 luglio 1959, n. 40); che nel caso in esame, appunto sotto  &#13;
 il profilo della rilevanza, manca nell'ordinanza del Tribunale di  Roma  &#13;
 qualsiasi esame o anche un accenno sui riflessi che può avere, ai fini  &#13;
 della  risoluzione  della controversia vertente fra le parti, l'art. 10  &#13;
 della legge 20 febbraio 1958, n. 55,  sul  disposto  dell'art.  10  del  &#13;
 precedente decreto delegato, dei quali articoli si afferma la identità  &#13;
 di  contenuto,  giacché  il  Tribunale  solo  sul  primo decreto si è  &#13;
 soffermato, e della  disposizione  contenuta  nella  successiva  legge,  &#13;
 avente  ovviamente  carattere formale, non si è affatto occupato; che,  &#13;
 occorrendo, pertanto, un nuovo esame sulla  rilevanza,  vanno  rinviati  &#13;
 gli atti al Tribunale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina che gli atti siano restituiti al Tribunale di Roma.           &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 7 giugno 1962.                                &#13;
                                   GASPARE  AMBROSINI  - MARIO COSATTI -  &#13;
                                   FRANCESCO   PANTALEO    GABRIELI    -  &#13;
                                   GIUSEPPE  CASTELLI  AVOLIO - ANTONINO  &#13;
                                   PAPALDO - NICOLA  JAEGER  -  GIOVANNI  &#13;
                                   CASSANDRO   -   BIAGIO  PETROCELLI  -  &#13;
                                   ANTONIO  MANCA  -  ALDO  SANDULLI   -  &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
