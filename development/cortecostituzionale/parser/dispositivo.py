from akomantoso.dom.elements import Decision, Block, P, Point, Paragraph
from akomantoso.dom.meta import TLCPerson

from cortecostituzionale.parser.parser import parser
from cortecostituzionale.parser.text_cleanup import cleanup
from akomantoso.utils import to_camelCase
import re


def _normalize_date(date_match):
    if date_match is None:
        return None
    month_map = {"gen": "01", "feb": "02", "mar": "03", "apr": "04", "mag": "05", "giu": "06", "lug": "07",
                 "ago": "08", "set": "09", "ott": "10", "nov": "11", "dic": "12"}
    year = date_match.group("year").value
    day = date_match.group("day").value
    if len(day) == 1:
        day = f"0{day}"
    month_group = date_match.group("month")
    if month_group:
        month = month_group.value
        if len(month) == 1:
            month = f"0{month}"
    else:
        month = month_map[date_match.group("month_name").value[:3].lower()]
    return f"{year}-{month}-{day}"


def _parse_conclusions(conclusions):
    n_dates = 0
    for element in conclusions.iter():
        inlines = []
        if element.text is not None:
            for m in parser.parse(element.text, "date", "signatures", "cancellieri"):
                if m.type == "date":
                    iso_date = _normalize_date(m)
                    if n_dates == 0:
                        inlines.append(("date", (m.start, m.end), dict(refersTo="#decisionDate", date=iso_date)))
                        n_dates += 1
                    elif n_dates == 1:
                        inlines.append(("date", (m.start, m.end), dict(refersTo="#publicationDate", date=iso_date)))
                        n_dates += 1
                    else:
                        inlines.append(("date", (m.start, m.end), dict(refersTo="", date=iso_date)))
                        n_dates += 1
                elif m.type == "signatures":
                    judge = m.group("judges")
                    value = " ".join(judge.value.split()).strip()
                    eid = to_camelCase(value)
                    tlc = TLCPerson(eid, href=f"/akn/ontology/person/it/{eid}", show_as=value)
                    js = judge.start - m.start
                    je = js + len(judge.value)
                    attributes = dict(refersTo=tlc)
                    subreferences = [("judge", (0, je), attributes)]
                    role = m.group("role")
                    if role:
                        role = role.first()
                        _rs = role.start - m.start
                        _re = role.end - m.start
                        subreferences.append(("role", (_rs, _re), {"refersTo": f"#{role.key}"}))
                        attributes["as"] = f"#{role.key}"
                    inlines.append(
                        (
                            "signature",
                            (m.start, m.start + len(m.value.strip())),
                            {"$subreferences": subreferences}
                        )
                    )
                elif m.type == "cancellieri":
                    name = m.group("name")
                    cancelliere = m.group("cancelliere")
                    value = " ".join(name.value.strip().split())
                    eid = to_camelCase(value)
                    tlc = TLCPerson(eid, href=f"/akn/ontology/person/it/{eid}", show_as=value)
                    name_start = name.start - m.start
                    name_end = name_start + len(name.value.strip())
                    canc_start = cancelliere.start - m.start
                    canc_end = canc_start + len(cancelliere.value)
                    inlines.append(
                        (
                            "signature",
                            (m.start, m.start + len(m.value.strip())),
                            {"$subreferences": [
                                (
                                    "person",
                                    (name_start, name_end),
                                    {"refersTo": tlc, "as": "#cancelliere"}
                                ),
                                (
                                    "role",
                                    (canc_start, canc_end),
                                    {"refersTo": "#cancelliere"}
                                )
                            ]}
                        )
                    )
            element.add_inlines(*inlines)


def parse_dispositivo(dispositivo, provvedimento):
    conclusions_start = dispositivo.text.lower().find("così deciso")
    if conclusions_start >= 0:
        paragraphs = cleanup(dispositivo.text[0:conclusions_start]) + cleanup(dispositivo.text[conclusions_start:])
    else:
        paragraphs = cleanup(dispositivo.text)
    decision = Decision()
    target = decision
    provvedimento.body.append(decision)
    default = Paragraph
    elements = []
    for t in paragraphs:
        res = parser.search(t, "dispositivo")
        if res is not None:
            m = res.first()
            if m.key == "formula_rito":
                block = Block(name="formulaRito", text=t)
                target.append(block)
                m = re.search(r"la corte costituzionale", block.text, re.I)
                if m is not None:
                    block.add_inline(("docAuthority", (m.start(), m.end()), {}))
            elif m.key == "corte":
                corte = P(text=t)
                corte.add_inline(("docAuthority", (0, len(t)), {}))
                target.append(corte)
            elif m.key == "conclusions":
                target = provvedimento.conclusions
                target.append(Block(name="formulaFinale", text=t))
                elements = []
                default = P
        else:
            m = parser.search(t, "paragraphs")
            if m is not None and default == Paragraph:
                num_match = m.first()
                num = num_match.value
                text = t[num_match.end:]
                if num_match.key == "point_num" and len(elements):
                    elements[-1].append(Point(text=text, num=num))
                else:
                    par = default(text=text, num=num)
                    target.append(par)
                    elements.append(par)
            else:
                par = default(text=t)
                target.append(par)
                elements.append(par)

    _parse_conclusions(provvedimento.conclusions)
