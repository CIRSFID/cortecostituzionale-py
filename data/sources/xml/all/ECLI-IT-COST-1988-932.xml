<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>932</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:932</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1988</data_decisione>
    <data_deposito>28/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel   giudizio   di   legittimità  costituzionale  degli  artt.  80,    &#13;
 tredicesimo comma, e 80-  bis del  d.P.R.  15  giugno  1959,  n.  393    &#13;
 (Testo  unico  delle  norme  sulla circolazione stradale), introdotto    &#13;
 dall'art.  142 della legge 24 novembre 1981,  n.  689  (Modifiche  al    &#13;
 sistema  penale), promosso con ordinanza emessa il 21 maggio 1986 dal    &#13;
 Pretore di Fermo nel procedimento penale a carico di  Mulè   Santino,    &#13;
 iscritta  al  n.  25  del  registro ordinanze 1988 e pubblicata nella    &#13;
 Gazzetta Ufficiale della  Repubblica  n.  7,  prima  serie  speciale,    &#13;
 dell'anno 1988.                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  9 giugno 1988 il Giudice    &#13;
 relatore Giovanni Conso.                                                 &#13;
   Ritenuto che il Pretore di Fermo, con ordinanza del 21 maggio 1986,    &#13;
 ha sollevato, in riferimento agli artt.3, 4, 16,35, 36  e  42,  terzo    &#13;
 comma, della Costituzione, "questione di legittimità" degli artt.80,    &#13;
 tredicesimo comma, e 80-  bis del d.P.R.  15  giugno  1959,  n.  393,    &#13;
 introdotto  dall'art.142 della legge 24 novembre 1981, n. 689, "nella    &#13;
 parte in cui, secondo  la  giurisprudenza  costante  della  Corte  di    &#13;
 cassazione,  colui  che guida durante il periodo di sospensione della    &#13;
 patente di guida disposta dal  Prefetto  per  brevissima  durata,  è    &#13;
 soggetto  alle  stesse  sanzioni penali ed alla stessa sanzione della    &#13;
 confisca rispetto a colui  che  guida  senza  mai  aver  ottenuto  la    &#13;
 patente  di  guida  e con patente di guida revocata e sospesa a tempo    &#13;
 indeterminato";                                                          &#13;
    Considerato  che  il  giudice  a quo ha, in realtà, sollevato due    &#13;
 distinte questioni di legittimità costituzionale:  l'una  avente  ad    &#13;
 oggetto  l'art.80,  tredicesimo  comma, del d.P.R. 15 giugno 1959, n.    &#13;
 393, denunciato, in riferimento al solo art.  3  della  Costituzione,    &#13;
 nella  parte  in  cui  prevede,  per  chi guida durante il periodo di    &#13;
 sospensione della patente disposta "per brevissima durata", la stessa    &#13;
 misura  della  pena comminata a chi guida senza avere mai ottenuto la    &#13;
 patente di guida o con patente di guida revocata o  sospesa  a  tempo    &#13;
 indeterminato;  l'altra avente ad oggetto l'art.80-  bis dello stesso    &#13;
 d.P.R. 15 giugno 1959, n. 393, introdotto dall'art. 142  della  legge    &#13;
 24 novembre 1981, n. 689, denunciato, in riferimento agli artt. 3, 4,    &#13;
 16, 35, 36 e 42, terzo comma, della Costituzione, nella parte in  cui    &#13;
 prevede,  per  chi  guida  durante  il  periodo  di sospensione della    &#13;
 patente disposta "per brevissima  durata",  la  misura  di  sicurezza    &#13;
 della  confisca  obbligatoria del veicolo, senza differenziare la sua    &#13;
 posizione da quella di chi guida senza avere mai ottenuto la  patente    &#13;
 di  guida  o  con  patente  di  guida  revocata  o  sospesa  a  tempo    &#13;
 indeterminato;                                                           &#13;
      e   che   entrambe   le   questioni   risultano   manifestamente    &#13;
 inammissibili,  richiedendosi  sostanzialmente  dal  giudice  a   quo    &#13;
 l'apprestamento,  da parte di questa Corte, di un'apposita disciplina    &#13;
 volta a differenziare -  sia sul piano della comminatoria della  pena    &#13;
 sia  sul  piano  della  comminatoria  della misura di sicurezza della    &#13;
 confisca -  le fattispecie del tipo di quella sottoposta al suo esame    &#13;
 (e,  cioè, la "guida durante il periodo di sospensione della patente    &#13;
 disposta dal Prefetto per brevissima durata",  nozione  per  di  più    &#13;
 vaga  e  generica) da altre fattispecie di asserita maggiore gravità    &#13;
 (e, cioè, la guida "senza mai aver ottenuto la patente  di  guida  e    &#13;
 con  patente  di guida revocata e sospesa a tempo indeterminato"), il    &#13;
 che implica scelte discrezionali riservate al  solo  legislatore  (v.    &#13;
 ordinanze n. 604 del 1987, n. 48 del 1987, n. 290 del 1986).             &#13;
    Visti gli artt.26, secondo comma, della legge 11 marzo 1953, n.87,    &#13;
 e 9, secondo comma, delle norme integrative  per  i  giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   1)  dichiara  la  manifesta  inammissibilità  della  questione  di    &#13;
 legittimità costituzionale  dell'art.  80,  tredicesimo  comma,  del    &#13;
 d.P.R.  15  giugno  1959,  n.  393  (Testo  unico  delle  norme sulla    &#13;
 circolazione stradale), sollevata, in riferimento  all'art.  3  della    &#13;
 Costituzione, dal Pretore di Fermo con ordinanza del 21 maggio 1986;     &#13;
    2)  dichiara  la  manifesta  inammissibilità  della  questione di    &#13;
 legittimità costituzionale dell'art. 80- bis del  d.P.R.  15  giugno    &#13;
 1959,  n.  393 (Testo unico delle norme sulla circolazione stradale),    &#13;
 introdotto dall'art.  142  della  legge  24  novembre  1981,  n.  689    &#13;
 (Modifiche  al  sistema penale), sollevata, in riferimento agli artt.    &#13;
 3, 4, 16, 35, 36 e 42, terzo comma, della Costituzione,  dal  Pretore    &#13;
 di Fermo con ordinanza del 21 maggio 1986.                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 luglio 1988                                  &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: CONSO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 28 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
