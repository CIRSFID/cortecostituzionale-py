<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>181</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:181</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FRAGALI</presidente>
    <relatore_pronuncia>Giuseppe Chiarelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/11/1971</data_decisione>
    <data_deposito>17/11/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. MICHELE FRAGALI, Presidente - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio promosso con  ricorso  del  Presidente  della  Regione  &#13;
 siciliana, notificato il 19 febbraio 1971, depositato in cancelleria il  &#13;
 22  successivo  ed  iscritto  al  n.3  del  registro  ricorsi 1971, per  &#13;
 conflitto di attribuzione sorto a seguito dei  decreti  rispettivamente  &#13;
 in  data  18,  24, 28, 30 novembre, 3 e 20 dicembre 1970, con i quali i  &#13;
 Direttori  degli  Uffici  provinciali  del  lavoro  e   della   massima  &#13;
 occupazione  di  Agrigento,  Caltanissetta,  Catania,  Enna, Siracusa e  &#13;
 Ragusa hanno costituito i Comitati provinciali dell'Istituto  nazionale  &#13;
 della  previdenza sociale previsti dall'art. 34 del decreto legislativo  &#13;
 30 aprile 1970, n. 639.                                                  &#13;
     Visto l'atto di  costituzione  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 13 ottobre 1971 il Giudice relatore  &#13;
 Giuseppe Chiarelli;                                                      &#13;
     uditi  gli  avvocati  Salvatore  Villari e Antonino Sansone, per la  &#13;
 Regione siciliana,  ed  il  sostituto  avvocato  generale  dello  Stato  &#13;
 Michele Savarese, per il Presidente del Consiglio dei ministri.          &#13;
     Ritenuto  che,  con  ricorso notificato al Presidente del Consiglio  &#13;
 dei ministri il 19 febbraio 1971, il Presidente della Regione siciliana  &#13;
 ha sollevato  conflitto  di  attribuzione  in  riferimento  ai  decreti  &#13;
 rispettivamente  in data 18, 24, 28, 30 novembre, 3 e 20 dicembre 1970,  &#13;
 con i quali i Direttori degli Uffici provinciali  del  lavoro  e  della  &#13;
 massima   occupazione   di  Agrigento,  Caltanissetta,  Catania,  Enna,  &#13;
 Siracusa e Ragusa hanno costituito i Comitati provinciali dell'Istituto  &#13;
 nazionale della previdenza sociale previsti dall'art.  34  del  decreto  &#13;
 legislativo 30 aprile 1970, n. 639;                                      &#13;
     che  col  ricorso  si  chiede che sia accertato in via preliminare,  &#13;
 quale mezzo al fine, che gli artt. 33, 34 e  35  del  predetto  decreto  &#13;
 legislativo   sono   viziati   da   illegittimità  costituzionale  per  &#13;
 violazione degli artt. 17, lett. f, e  20  dello  Statuto  regionale  e  &#13;
 degli  artt.  1, 2 e 4 delle Norme di attuazione, approvate con decreto  &#13;
 presidenziale 25 giugno 1952, n. 1138, e che, comunque, sia  dichiarato  &#13;
 che  spetta  alla  Regione  siciliana,  in base alle predette norme, il  &#13;
 diritto a che suoi  rappresentanti  siano  chiamati  a  far  parte  dei  &#13;
 predetti  Comitati  provinciali  dell'I.N.P.S. e siano conseguentemente  &#13;
 annullati i decreti sopra indicati;                                      &#13;
     che il Presidente del Consiglio dei ministri si  è  costituito  in  &#13;
 giudizio,  con  atto  notificato al Presidente della Regione l'11 marzo  &#13;
 1971, eccependo in  via  preliminare  l'inammissibilità  del  ricorso,  &#13;
 perché   con  esso  si  sarebbe  prospettato  un  conflitto  meramente  &#13;
 virtuale, in quanto con la lamentata omissione, nei decreti  impugnati,  &#13;
 della nomina dei rappresentanti della Regione non si sarebbe concretata  &#13;
 una  invasione  di  competenza  regionale,  e inoltre perché i decreti  &#13;
 stessi sarebbero attuativi dell'art. 34 del citato decreto  legislativo  &#13;
 n. 639 del 1970, non impugnato a suo tempo;                              &#13;
     che  nel merito la difesa del Presidente del Consiglio ha sostenuto  &#13;
 che l'invocata disposizione  dell'art.  4  delle  Norme  di  attuazione  &#13;
 sarebbe  attualmente  inoperante,  non  essendo state ancora emanate le  &#13;
 norme integrative previste dal secondo comma del medesimo art. 4;        &#13;
     che nella discussione orale le due  difese  hanno  insistito  nelle  &#13;
 rispettive tesi.                                                         &#13;
     Considerato  che il giudizio per conflitto di attribuzione è stato  &#13;
 validamente proposto perché i  decreti  di  cui  trattasi  sono  stati  &#13;
 impugnati  come  atti autonomi e lesivi della competenza della Regione,  &#13;
 in quanto, con l'uniformarsi, nel  costituire  i  Comitati  provinciali  &#13;
 dell'I.N.P.S.,  esclusivamente  all'art.  34  del  decreto  legislativo  &#13;
 citato,  non  hanno  dato   applicazione   all'art.   4   del   decreto  &#13;
 presidenziale  n. 1138 del 1952, violando cosi gli artt. 17, lett. f, e  &#13;
 20 dello Statuto, di cui il detto art. 4 è norma di attuazione;         &#13;
     che, ai fini  della  decisione  del  conflitto,  si  presenta  come  &#13;
 preliminare  e  strumentale la risoluzione della questione se l'art. 34  &#13;
 del decreto legislativo n. 639 del 1970 non abbia violato  le  predette  &#13;
 norme  statutarie  e  di attuazione col non prevedere la rappresentanza  &#13;
 della Regione siciliana nei Comitati destinati a operare nella  Regione  &#13;
 medesima;                                                                &#13;
     che  tale  questione è pertanto rilevante nel presente giudizio, e  &#13;
 non è manifestamente infondato  il  dubbio  che  nella  partecipazione  &#13;
 della  Regione  al  procedimento  di  nomina  dei componenti gli organi  &#13;
 locali degli Istituti di previdenza e nella  partecipazione  alla  loro  &#13;
 attività  mediante  la rappresentanza prevista dall'art. 4 delle norme  &#13;
 di attuazione possa ravvisarsi, in relazione ai detti Comitati, il modo  &#13;
 di esercizio della  competenza  in  materia  di  legislazione  sociale,  &#13;
 attribuita alla Regione dagli artt. 17, lett. f, e 20 dello Statuto;     &#13;
     che ricorrono pertanto gli estremi perché la Corte, riservata ogni  &#13;
 pronuncia  sul  merito  del  ricorso,  sollevi  davanti  a se stessa la  &#13;
 questione di legittimità costituzionale, ai sensi  degli  artt.  23  e  &#13;
 seguenti della legge 11 marzo 1953, n. 87.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1)  dispone  la  trattazione innanzi a se stessa della questione di  &#13;
 legittimità costituzionale dell'art. 34  del  decreto  legislativo  30  &#13;
 aprile  1970, n. 639, in riferimento agli artt. 17, lett. f, e 20 dello  &#13;
 Statuto per la Regione siciliana e all'art. 4 delle norme di attuazione  &#13;
 (d.P.R. 26 giugno 1952, n. 1138), nella parte in cui non è prevista la  &#13;
 rappresentanza della Regione  nei  Comitati  provinciali  dell'Istituto  &#13;
 nazionale della previdenza sociale;                                      &#13;
     2) ordina il rinvio del presente giudizio perché esso possa essere  &#13;
 trattato  congiuntamente  alla questione di legittimità costituzionale  &#13;
 di cui al numero precedente;                                             &#13;
     3) ordina che a cura della cancelleria la  presente  ordinanza  sia  &#13;
 notificata  al  Presidente  del  Consiglio dei ministri e al Presidente  &#13;
 della Regione siciliana e sia comunicata ai Presidenti delle due Camere  &#13;
 del Parlamento;                                                          &#13;
     4) ordina che la presente ordinanza sia pubblicata  nella  Gazzetta  &#13;
 Ufficiale  della  Repubblica  e  nella Gazzetta Ufficiale della Regione  &#13;
 siciliana;                                                               &#13;
     5) assegna alle parti il termine di venti giorni  decorrenti  dalla  &#13;
 pubblicazione  della  presente ordinanza nella Gazzetta Ufficiale della  &#13;
 Repubblica  per  il  deposito  delle  deduzioni  sulla   questione   di  &#13;
 legittimità costituzionale di cui al numero 1.                          &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 10 novembre 1971.                             &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
