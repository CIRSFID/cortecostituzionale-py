<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>200</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:200</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/07/1986</data_decisione>
    <data_deposito>18/07/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI &#13;
 - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO - Prof. RENATO &#13;
 DELL'ANDRO - Prof. GABRIELE PESCATORE - Avv. UGO SPAGNOLI - Prof. &#13;
 FRANCESCO PAOLO CASAVOLA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  387,  terzo  &#13;
 comma, del codice di procedura penale, promosso con ordinanza emessa il  &#13;
 9  maggio  1984  dalla  Corte  di  cassazione  sui  ricorsi proposti da  &#13;
 Ippolito Francesco e Luberti Francesco, iscritta al n. 928 del registro  &#13;
 ordinanze 1984 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 287 dell'anno 1984.                                                   &#13;
     Udito  nella  camera  di  consiglio  del  23 aprile 1986 il Giudice  &#13;
 relatore Giovanni Conso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  sentenza  del  25  ottobre  1983  il  Giudice  istruttore  del  &#13;
 Tribunale di Roma dichiarava non doversi procedere  nei  confronti  del  &#13;
 dott.  Francesco  Ippolito  e  dell'avv.    Francesco Luberti, entrambi  &#13;
 componenti del Consiglio Superiore della  magistratura,  in  ordine  al  &#13;
 reato  di  abuso  di  ufficio  aggravato (così modificata l'originaria  &#13;
 imputazione di interesse privato in atti d'ufficio), "perché fatti non  &#13;
 sono previsti dalla legge  come  reato  sussistendo  la  causa  di  non  &#13;
 punibilità di cui all'art. 5 della legge 1981, n. 1".                   &#13;
     Entrambi prosciolti proponevano ricorso per cassazione avverso tale  &#13;
 sentenza,   chiedendone   l'annullamento,   con  o  senza  rinvio,  per  &#13;
 molteplici motivi; in via subordinata eccepivano, in  riferimento  agli  &#13;
 artt.  3 e 24 della Costituzione, l'illegittimità dell'art. 387, terzo  &#13;
 comma, del codice di procedura penale, "nella parte in cui  esclude  il  &#13;
 diritto  dell'imputato  di  proporre  appello  avverso  la sentenza del  &#13;
 giudice istruttore che ha  dichiarato  non  doversi  procedere  perché  &#13;
 fatti  non  sono  previsti  come  reato  sussistendo  una  causa di non  &#13;
 punibilità".                                                            &#13;
     Il Procuratore Generale concludeva richiedendo  il  proscioglimento  &#13;
 degli  imputati  perché il fatto non sussiste: sarebbe stato, infatti,  &#13;
 possibile "correggere vizi" della sentenza impugnata così da  adeguare  &#13;
 la  formula  di  proscioglimento  alla  verità  processuale,  donde la  &#13;
 "perdita di rilevanza" della questione di legittimità costituzionale.   &#13;
     Con ordinanza del 9 maggio 1984, la Corte di  cassazione,  premesso  &#13;
 che  l'esame  circa  la  fondatezza  delle affermazioni contenute nella  &#13;
 sentenza impugnata  in  ordine  alla  ritenuta  sussistenza  dei  fatti  &#13;
 integranti  il  reato di abuso d'ufficio, attenendo "in definitiva alla  &#13;
 valutazione  delle  prove",  doveva  ritenersi  precluso  in  sede   di  &#13;
 legittimità  e che, pur tuttavia, non poteva disconoscersi "l'esigenza  &#13;
 in sede di impugnazione di un  più  approfondito  riesame  del  merito  &#13;
 della   sentenza",   ha   dichiarato  rilevante  e  non  manifestamente  &#13;
 infondata, in riferimento agli artt. 3,  primo  comma,  e  24,  secondo  &#13;
 comma,  della Costituzione, la questione di legittimità dell'art. 387,  &#13;
 terzo comma, del codice  di  procedura  penale,  "nella  parte  in  cui  &#13;
 esclude  il  diritto  dell'imputato  di  proporre  appello  avverso  la  &#13;
 sentenza del giudice istruttore che ha dichiarato non doversi procedere  &#13;
 perché il fatto non costituisce reato per la presenza di una causa  di  &#13;
 non punibilità (nella specie, la causa di non punibilità specifica di  &#13;
 cui all'art. 32 - bis della legge 24 marzo 1958, n. 195)".               &#13;
     In   punto  di  rilevanza,  si  osserva  che  l'accoglimento  della  &#13;
 questione di legittimità costituzionale consentirebbe agli imputati di  &#13;
 ottenere un riesame del fatto ben più ampio di quello ammesso in  sede  &#13;
 di  legittimità  (con  la  possibilità di conseguire un provvedimento  &#13;
 più  favorevole);  si  aggiunge  che   la   norma   impugnata   incide  &#13;
 concretamente  -  data  la  sua  natura  processuale  -  sul  corso del  &#13;
 processo, sul compimento e  sull'efficacia  degli  atti  del  medesimo,  &#13;
 nonché  sulla sfera dei poteri e dei doveri delle parti e del giudice,  &#13;
 perché il giudizio non può proseguire se non viene prima identificato  &#13;
 il giudice competente a conoscere dell'impugnazione avverso la sentenza  &#13;
 istruttoria che applichi una "causa di non punibilità".                 &#13;
     Sotto il profilo della non  manifesta  infondatezza,  la  Corte  di  &#13;
 cassazione  richiama  la  sentenza  costituzionale  n.    224 del 1983,  &#13;
 cogliendo precise analogie fra l'ipotesi  ivi  decisa  e  quella  della  &#13;
 sentenza  istruttoria,  che  "riconosciuta  la  presenza degli elementi  &#13;
 positivi oggettivi e soggettivi del reato, proscioglie in virtù  della  &#13;
 presenza di una causa di giustificazione".                               &#13;
     Dalla  sentenza n. 224 del 1983, oltre che dalle sentenze n. 70 del  &#13;
 1975, n. 73 del 1978, n. 72 del 1979, n. 53 del 1981, con le  quali  la  &#13;
 Corte    ha   già   notevolmente   circoscritto   limiti   all'appello  &#13;
 dell'imputato avverso sentenze di proscioglimento  per  estinzione  del  &#13;
 reato,  sarebbe  ricavabile  il  principio  che  "vi  è violazione dei  &#13;
 diritti della difesa se all'imputato,  prosciolto  dopo  un  esame  del  &#13;
 merito  che  ne abbia sostanzialmente accertato la responsabilità, non  &#13;
 è riconosciuto il diritto di appellare al fine di ottenere un completo  &#13;
 riesame  del  merito,  e  che  vi  è  disparità  di  trattamento  tra  &#13;
 l'imputato  al  quale  l'appello  non  è consentito e il P.M. al quale  &#13;
 l'appello è consentito  in  ogni  caso".  Enunciato  in  relazione  al  &#13;
 proscioglimento  per  estinzione  del reato, tale principio sembrerebbe  &#13;
 "ugualmente se non maggiormente valido anche con riferimento  ad  altri  &#13;
 casi  di  proscioglimento caratterizzati da un previo riconoscimento di  &#13;
 colpevolezza, come appunto nei casi  in  cui  il  giudice  abbia,  dopo  &#13;
 l'esame  del  merito, accertato la sussistenza di tutti gli elementi di  &#13;
 un reato, ma si sia astenuto dal rinviare a giudizio per la presenza di  &#13;
 una causa di non punibilità":  ipotesi  che  si  è  verificata  nella  &#13;
 specie, dato che il giudice istruttore, dopo una complessa istruttoria,  &#13;
 ha  ritenuto  sussistente  un  reato  diverso  da  quello enunciato nel  &#13;
 mandato di comparizione, affermando testualmente che  "i  comportamenti  &#13;
 di entrambi consiglieri... si prestano ad essere considerati, sul piano  &#13;
 oggettivo...  come  abuso  di  ufficio,  e  sul piano soggettivo, quali  &#13;
 rivelatori dell'intento di nuocere. Pertanto  la  formula  decisoria...  &#13;
 che  si attaglia alle risultanze istruttorie non può essere ampiamente  &#13;
 liberatoria bensì quella contemplata dall'art. 5 della legge 1981 n. 1  &#13;
 con riferimento all'ipotesi di cui all'art. 323 cod. pen.".              &#13;
     In conclusione, sembrerebbe innegabile anche in casi come quello di  &#13;
 specie l'interesse dell'imputato ad ottenere il proscioglimento con una  &#13;
 formula più favorevole.  Di  qui  il  contrasto  con  parametri  sopra  &#13;
 indicati   conseguente   alla  preclusione  dell'appello:  infatti,  la  &#13;
 sentenza che dichiara non doversi procedere  per  la  presenza  di  una  &#13;
 causa  di  non  punibilità  presuppone necessariamente un esame ed una  &#13;
 valutazione in  senso  sfavorevole  del  merito  e  l'esclusione  della  &#13;
 possibilità di proscioglimento con formula più favorevole.             &#13;
     L'ordinanza,   ritualmente   notificata   e  comunicata,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale n. 287 del 17 ottobre 1984.          &#13;
     Nel giudizio non  si  sono  costituite  le  parti  private  né  ha  &#13;
 spiegato intervento la Presidenza del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - L'ordinanza della Corte di cassazione sottopone a controllo di  &#13;
 legittimità  costituzionale  l'art.  387,  terzo  comma, del codice di  &#13;
 procedura penale, in riferimento agli  artt.  3,  primo  comma,  e  24,  &#13;
 secondo  comma,  della  Costituzione,  "nella  parte  in cui esclude il  &#13;
 diritto dell'imputato di  proporre  appello  avverso  la  sentenza  del  &#13;
 giudice  istruttore  che ha dichiarato non doversi procedere perché il  &#13;
 fatto non costituisce reato  per  la  presenza  di  una  causa  di  non  &#13;
 punibilità".                                                            &#13;
     2.  -  Una puntualizzazione si rende necessaria in via preliminare,  &#13;
 per tenere nel debito conto le modificazioni  apportate  al  codice  di  &#13;
 procedura  penale  dalla legge 18 giugno 1955, n. 517. Se prima di tali  &#13;
 modificazioni in nessun caso era prevista  l'appellabilità,  da  parte  &#13;
 dell'imputato,  della  sentenza  dichiarativa  di non doversi procedere  &#13;
 perché il fatto non costituisce reato per la presenza di una causa  di  &#13;
 non punibilità, dopo di esse la situazione si è presentata in termini  &#13;
 sensibilmente  diversi.   Fra l'altro, l'art. 19 della legge n. 517 del  &#13;
 1955 ha innovato l'art. 387,  terzo  comma,  del  codice  di  procedura  &#13;
 penale  nel  senso  che,  "se l'imputazione riguardava un delitto o una  &#13;
 contravvenzione punibile con  l'arresto"  (limite  non  in  discussione  &#13;
 nella  presente sede, oltretutto per ragioni di rilevanza), "l'imputato  &#13;
 può appellare alla sezione istruttoria contro la sentenza del  giudice  &#13;
 istruttore"  non solo "quando è stato dichiarato non doversi procedere  &#13;
 per insufficienza di prove o per concessione del  perdono  giudiziale",  &#13;
 ma  anche  quando  è  stato  dichiarato non doversi procedere "perché  &#13;
 trattasi di persona non imputabile o di persona non punibile perché il  &#13;
 fatto  non  costituisce  reato,  se  è  stata  applicata  o  può  con  &#13;
 provvedimento successivo essere applicata una misura di sicurezza".      &#13;
     Pertanto,  la  questione dedotta ha per oggetto, più precisamente,  &#13;
 la parte in cui il comma in esame esclude il diritto  dell'imputato  di  &#13;
 proporre  appello  avverso  la  sentenza  del giudice istruttore che ha  &#13;
 dichiarato non doversi  procedere  "perché  trattasi  di  persona  non  &#13;
 punibile perché il fatto non costituisce reato", qualora non sia stata  &#13;
 applicata né possa, con provvedimento successivo, essere applicata una  &#13;
 misura di sicurezza.                                                     &#13;
     3.  - Una seconda puntualizzazione concerne significati e contenuti  &#13;
 della formula di proscioglimento che contraddistingue le  sentenze  con  &#13;
 le  quali  viene  dichiarato non doversi procedere "perché trattasi di  &#13;
 persona non punibile perché il fatto non costituisce reato".            &#13;
     Posto che tale formula  non  risulta  usata  in  modo  univoco  dal  &#13;
 legislatore,  occorre  distinguere la portata che essa assume nell'art.  &#13;
 387, terzo comma, del codice di procedura penale (come  negli  analoghi  &#13;
 artt.  399,  primo  comma,  512  n.  2  e 513 n. 2 dello stesso codice,  &#13;
 dedicati alle impugnazioni delle sentenze di proscioglimento emanate da  &#13;
 giudici  diversi  dal   giudice   istruttore)   dalla   portata,   più  &#13;
 circoscritta, che la stessa formula riveste nell'art. 378, primo comma,  &#13;
 del  codice  di  procedura  penale (attinente, come l'analogo art. 479,  &#13;
 primo comma, dello stesso codice,  alla  pronuncia  delle  sentenze  di  &#13;
 proscioglimento),  ove  si  parla  di "persona non punibile, perché il  &#13;
 fatto non costituisce reato o  per  un'altra  ragione".  Dal  raffronto  &#13;
 emerge  chiaramente  che  quanto  dall'art.  378,  primo  comma,  viene  &#13;
 ricompreso nell'ambito del "non punibile  per  un'altra  ragione"  deve  &#13;
 ritenersi  sussunto  dall'art.  387, terzo comma, nel l'ambito del "non  &#13;
 punibile perché il fatto non costituisce reato".                        &#13;
     Non meno significativo si rivela il raffronto con l'art.   152  del  &#13;
 codice  di  procedura  penale, che, nel disciplinare in via generale la  &#13;
 "declaratoria di determinate cause di non  punibilità",  non  contiene  &#13;
 nei  suoi  due  commi  alcun  cenno  alla formula "perché il fatto non  &#13;
 costituisce reato" (e nemmeno alla formula "perché la persona  non  è  &#13;
 imputabile",  normalmente  affiancata  ad  essa),  utilizzando, invece,  &#13;
 entrambe le volte la formula "perché il fatto non è  preveduto  dalla  &#13;
 legge  come reato", a sua volta del tutto assente tanto negli artt. 387  &#13;
 (399,512 e 513) quanto negli artt. 378 (e 479). Ciò ha indotto  questa  &#13;
 Corte  (sentenza  n.    175  del  1971)  a  recepire  l'interpretazione  &#13;
 giurisprudenziale e dottrinale che  con  larga  prevalenza  attribuisce  &#13;
 alla  formula  "perché  il  fatto  non  è  preveduto dalla legge come  &#13;
 reato", quale  utilizzata  dall'art.  152,  "un  significato  generico,  &#13;
 comprensivo  non  solo delle ipotesi del difetto di una qualsiasi norma  &#13;
 penale cui possa ricondursi il fatto imputato, ma anche  di  quelle  di  &#13;
 mancanza  delle condizioni di imputabilità o di punibilità rispetto a  &#13;
 cui il fatto, pur se astrattamente previsto dalla legge penale, risulta  &#13;
 giuridicamente  irrilevante  al  fine  dell'applicabilità di questa, e  &#13;
 quindi del tutto equivalente all'altra".                                 &#13;
     Rispetto alle formule "perché il fatto non  sussiste"  e  "perché  &#13;
 l'imputato    non   l'ha   commesso",   dirette   a   rimarcare   l'una  &#13;
 l'insussistenza  materiale  del  fatto  storico  e  l'altra  la  totale  &#13;
 estraneità dell'imputato, la formula "perché il fatto non costituisce  &#13;
 reato"  si caratterizza in quanto proscioglimento che, pur riconoscendo  &#13;
 la  sussistenza  della  materialità  del  fatto  storico  e   la   sua  &#13;
 riferibilità  all'imputato,  esclude  la  punibilità  per la mancanza  &#13;
 dell'elemento soggettivo  oppure  per  la  presenza  di  una  causa  di  &#13;
 esclusione  dell'antigiuridicità  o  di  una causa di esclusione della  &#13;
 punibilità.                                                             &#13;
     4. - Ad avviso della Corte di cassazione, il  tipo  di  formula  di  &#13;
 proscioglimento  così  individuata  non  può  mai  dirsi  "ampiamente  &#13;
 liberatoria", qualunque sia la particolare causa che di volta in  volta  &#13;
 ne  comporti  l'adozione,  compresa  ovviamente quella (v. in proposito  &#13;
 sentenza n. 148  del  1983)  ravvisata  nella  specie  dalla  pronuncia  &#13;
 istruttoria   sottoposta   a   ricorso:   "sembra   perciò  innegabile  &#13;
 l'interesse dell'imputato ad ottenere" il proscioglimento con  la  più  &#13;
 favorevole   formula   "perché  il  fatto  non  sussiste"  o  "perché  &#13;
 l'imputato non l 'ha commesso". Pertanto, "la preclusione  dell'appello  &#13;
 si  risolverebbe  non  solo  in  una  non  giustificata  disparità  di  &#13;
 trattamento tra due parti del  processo,  P.  M.  e  imputato  (art.  3  &#13;
 Cost.),  ma  anche in un impedimento all'esercizio della difesa in ogni  &#13;
 stato e grado del procedimento (art.  24,  secondo  comma,  Cost.):  il  &#13;
 tutto  sulla  base del principio - da questa Corte più volte enunciato  &#13;
 in relazione al proscioglimento per estinzione del reato - "che  vi  è  &#13;
 violazione dei diritti della difesa se all'imputato, prosciolto dopo un  &#13;
 esame   del   merito   che   ne   abbia  sostanzialmente  accertato  la  &#13;
 responsabilità, non è riconosciuto il diritto di appellare al fine di  &#13;
 ottenere un completo riesame del merito, e  che  vi  è  disparità  di  &#13;
 trattamento  tra  l'imputato  al quale l'appello non è consentito e il  &#13;
 P.M. al quale l'appello è consentito in ogni caso". Tale  principio  -  &#13;
 che  ha,  fra  l'altro,  comportato  la  declaratoria di illegittimità  &#13;
 costituzionale dello stesso  art.  387,  terzo  comma,  del  codice  di  &#13;
 procedura  penale, "nella parte in cui esclude il diritto dell'imputato  &#13;
 di pro porre appello, ai fini e nei limiti di cui all'art.  152,  comma  &#13;
 secondo,  cod.  proc. pen., avverso la sentenza del Giudice istruttore,  &#13;
 che lo abbia  prosciolto  per  estinzione  del  reato  per  amnistia  o  &#13;
 prescrizione"  (sentenza  n.  224  del  1983)  - sarebbe, ad avviso del  &#13;
 giudice a quo, ugualmente, se  non  maggiormente,  valido  riguardo  ai  &#13;
 "casi  in  cui  il Giudice abbia, dopo l'esame del merito, accertato la  &#13;
 sussistenza di tutti gli elementi di un reato, ma si sia  astenuto  dal  &#13;
 rinviare a giudizio per la presenza di una causa di non punibilità".    &#13;
     La questione è fondata.                                             &#13;
     5.  -  Come questa Corte ha ripetutamente affermato (sentenze n. 70  &#13;
 del 1975, n. 73 del 1978, n. 72 del 1979, n.  53 del 1981, n.  224  del  &#13;
 1983)  ed  ancora di recente ribadito (sentenze n. 299 del 1985, n. 110  &#13;
 del 1986), le norme  processuali  penali  che  negano  all'imputato  il  &#13;
 diritto di proporre appello contro provvedimenti suscettibili di essere  &#13;
 appellati  dal  pubblico  ministero violano congiuntamente gli artt. 3,  &#13;
 primo comma, e 24, secondo comma, della Costituzione, quando si  tratti  &#13;
 di  provvedimenti  dei  quali  pure  l'imputato  possa avere ragione di  &#13;
 lamentarsi: la disparità di trattamento, non potendo giustificarsi né  &#13;
 con  "la  peculiare posizione istituzionale e la funzione assegnata" al  &#13;
 pubblico  ministero  né  con  "le  esigenze  connesse  alla   corretta  &#13;
 amministrazione  della giustizia e di rilievo costituzionale" (sentenze  &#13;
 n. 190 del 1970,  n.  155  del  1974,  n.  110  del  1986),  "turba  il  &#13;
 necessario  equilibrio  del contraddittorio ed in tal senso viola anche  &#13;
 il principio del diritto di difesa" (sentenza n. 224 del 1983). Da ciò  &#13;
 "la necessità di ristabilire la par condicio tra imputato  e  pubblica  &#13;
 accusa"  (sentenze n. 62 del 1981, n. 110 del 1986), nell'ottica di "un  &#13;
 " equo processo " fondato, tra l'altro, sulla uguaglianza delle  parti,  &#13;
 sulla "egalité des armes "" (sentenza n. 62 del 1981).                  &#13;
     In  particolare,  l'interesse  dell'imputato  a  dolersi  anche per  &#13;
 motivi di merito, con conseguente diritto ad un riesame dei fatti, più  &#13;
 ampio di quello che può compiere il giudice di legittimità, è  stato  &#13;
 riconosciuto   nei   confronti   delle   sentenze   di  proscioglimento  &#13;
 istruttorio per estinzione  del  reato  a  seguito  di  amnistia  o  di  &#13;
 prescrizione,  dato che esse "possono arrecare agli imputati pregiudizi  &#13;
 di ordine morale e di ordine giuridico" (sentenza n. 224 del 1983).      &#13;
     Le stesse considerazioni e, quindi, le stesse conseguenze sarebbero  &#13;
 estensibili, secondo il giudice a quo,  alle  sentenze  istruttorie  di  &#13;
 proscioglimento  "perché  trattasi  di persona non punibile perché il  &#13;
 fatto non costituisce reato", stante  la  loro  idoneità  ad  arrecare  &#13;
 pregiudizio all'imputato.                                                &#13;
     6.  -  Poiché l'esistenza di un concreto interesse ad impugnare è  &#13;
 sempre il risultato di una valutazione demandata al giudice  ordinario,  &#13;
 e   più  precisamente  al  giudice  investito  dell'impugnazione,  cui  &#13;
 compete, fra l'altro, di dichiararne l'inammissibilità "quando risulta  &#13;
 che l'impugnazione fu proposta da chi non vi aveva interesse" (art. 209  &#13;
 del codice di procedura  penale,  in  relazione  all'art.  190,  quarto  &#13;
 comma,  dello  stesso  codice),  l'avere  il  giudice  a  quo affermato  &#13;
 l'esistenza di un interesse dell'imputato a dolersi  della  formula  di  &#13;
 proscioglimento   in   esame   dovrebbe   comportare   l'illegittimità  &#13;
 costituzionale della norma che non consente  all'imputato  di  proporre  &#13;
 nei  confronti  della  relativa  pronuncia  di  non  doversi  procedere  &#13;
 l'appello consentito, invece, al pubblico ministero.                     &#13;
     Si potrebbe, anzi, affermare, in termini più generali,  che,  ogni  &#13;
 qualvolta  la  Corte  di  cassazione  ritenga  ammissibile  il  ricorso  &#13;
 proposto dall'imputato contro una sentenza di primo  grado  appellabile  &#13;
 soltanto  dal  pubblico  ministero,  l'implicito  riconoscimento  così  &#13;
 operato di un  interesse  dell'imputato  a  dolersi  del  provvedimento  &#13;
 assoggettato    a    ricorso   dovrebbe   comportare   l'illegittimità  &#13;
 costituzionale della specifica norma che  non  estende  il  diritto  di  &#13;
 appellare anche all'imputato.                                            &#13;
     Ad  ulteriore  conferma  vi sarebbe, infine, la considerazione che,  &#13;
 tutte le  volte  in  cui  un  provvedimento  di  primo  grado  soltanto  &#13;
 ricorribile    per    cassazione    da    parte    dell'imputato    sia  &#13;
 contemporaneamente  appellato  dal  pubblico  ministero,   il   ricorso  &#13;
 dell'imputato  si intende convertito in appello, non potendosi di certo  &#13;
 escludere l'interesse all'appello, una volta riconosciuto l'interesse a  &#13;
 proporre il ricorso per cassazione e, quindi, ad impugnare.              &#13;
     L'ordinanza  di  rimessione  non  si  è,  peraltro,  limitata   ad  &#13;
 affermare  l'esistenza di un interesse dell'imputato a proporre appello  &#13;
 contro la sentenza di proscioglimento "perché il fatto non costituisce  &#13;
 reato", ma ne ha dato  motivazione,  ravvisando  tale  interesse  nella  &#13;
 possibilità  di  ottenere  il  proscioglimento  con  "una formula più  &#13;
 favorevole" - quale, appunto, quella "perché il fatto non sussiste"  o  &#13;
 quella  "perché  l'imputato non l'ha commesso" - in grado di escludere  &#13;
 la sussistenza materiale del  fatto  storico  o  la  sua  riferibilità  &#13;
 all'imputato.                                                            &#13;
     7.  -  Tutto  ciò  trova  larga eco nella giurisprudenza di questa  &#13;
 Corte: dalla più generale affermazione che la gerarchia delle  formule  &#13;
 di  proscioglimento  è una gerarchia "da determinare in considerazione  &#13;
 dell'interesse dell'imputato a venire assolto con l'impiego  di  quella  &#13;
 fra   esse   che   risulti   produttiva  degli  effetti  per  lui  meno  &#13;
 pregiudizievoli"  (sentenza  n.  175  del  1971)  alla  più  specifica  &#13;
 constatazione  che  "in tutte" le ipotesi di proscioglimento - "escluse  &#13;
 le pronunce emesse perché  il  fatto  non  sussiste  o  non  è  stato  &#13;
 commesso  dal  prevenuto",  le  uniche  per cui manca ogni interesse ad  &#13;
 impugnare - il legislatore "attribuisce all'imputato un  fatto,  o  non  &#13;
 esclude  l'attribuzione  di  un fatto, che può non costituire reato ma  &#13;
 tuttavia essere  giudicato  sfavorevolmente  dall'opinione  pubblica  o  &#13;
 comunque dalla coscienza sociale" (sentenza n.  151 del 1967).           &#13;
     Proprio  sulla base del rilievo che in tali casi il proscioglimento  &#13;
 istruttorio "può ferire la dignità del  cittadino  allo  stesso  modo  &#13;
 d'una   pronuncia   di  rinvio  a  giudizio",  per  giunta  escludendo,  &#13;
 diversamente da quest'ultima, "una seconda fase nella quale,  entro  lo  &#13;
 stesso  grado  del  giudizio,  si  possa porre immediato rimedio a quel  &#13;
 male", la sentenza da ultimo richiamata (in proposito v. anche sentenza  &#13;
 n.  224  del  1983)  era  pervenuta   a   dichiarare   l'illegittimità  &#13;
 costituzionale  dell'art.  376 del codice di procedura penale, nonché,  &#13;
 ex  art.  27  della  legge  11  marzo  1953,  n.  87,  l'illegittimità  &#13;
 costituzionale degli artt. 395, quarto comma, e 398, terzo comma, dello  &#13;
 stesso  codice,  nelle  rispettive  parti  in  cui  non  si "prevede la  &#13;
 contestazione del fatto e l'interrogatorio dell'imputato  ai  fini  del  &#13;
 proscioglimento con formula diversa da quella che il fatto non sussiste  &#13;
 o non sia stato commesso dall'imputato", l'unica "appagante l'interesse  &#13;
 morale   dell'imputato"  (sentenza  n.  5  del  1975).  Come  è  stato  &#13;
 efficacemente  rimarcato  anche  in  dottrina,  soltanto  nei  casi  di  &#13;
 proscioglimento con formula pienamente liberatoria in fatto si potrebbe  &#13;
 essere sicuri della mancanza di ogni pregiudizio (attuale o potenziale)  &#13;
 per il prosciolto.                                                       &#13;
     8.  -  Si  deve,  quindi,  concludere nel senso dell'illegittimità  &#13;
 costituzionale dell'art. 387, terzo  comma,  del  codice  di  procedura  &#13;
 penale,  nella  parte  in  cui non riconosce all'imputato il diritto di  &#13;
 proporre appello contro la sentenza del giudice  istruttore  che  abbia  &#13;
 dichiarato  non  doversi  procedere  "perché  trattasi  di persona non  &#13;
 punibile perché il fatto non costituisce reato", qualora non sia stata  &#13;
 applicata né possa, con provvedimento successivo, essere applicata una  &#13;
 misura di sicurezza.                                                     &#13;
     La conseguente equiparazione, ai fini dell'appellabilità anche  da  &#13;
 parte  dell'imputato,  di tutte le sentenze istruttorie pronunciate con  &#13;
 la formula "perché trattasi di persona non punibile perché  il  fatto  &#13;
 non  costituisce  reato",  sia  stata  o no applicata e possa o no, con  &#13;
 provvedimento successivo, essere applicata  una  misura  di  sicurezza,  &#13;
 così  superando  la distinzione introdotta dall'art. 19 della legge 18  &#13;
 giugno 1955, n. 517 (v.  punto  2),  richiama  alla  memoria  le  prime  &#13;
 iniziative  parlamentari  per  l'aggiornamento  del codice di procedura  &#13;
 penale  (proposta  di  legge  d'iniziativa  dei deputati Leone, Riccio,  &#13;
 Bellavista ed Amatucci, annunziata il 13  marzo  1952,  n.  2258  della  &#13;
 legislatura;  proposta di legge d'iniziativa dei deputati Leone, Riccio  &#13;
 ed Amatucci, annunziata il 25 luglio 1953, n. 30 della II  legislatura,  &#13;
 poi  trasfusa,  per la parte che qui interessa, nel disegno governativo  &#13;
 n.  3008,  approvato  all'unanimità  in  sede  legislativa  dalla   3ª  &#13;
 commissione  della  Camera dei deputati nella seduta del 25 marzo 1953,  &#13;
 secondo il testo coordinato da un apposito comitato e concordato con il  &#13;
 Governo). Tali iniziative avevano, infatti, perseguito la  sostituzione  &#13;
 dell'originario  art.  387, terzo comma, del codice di procedura penale  &#13;
 nel senso di stabilire, fra l'altro,  che  "l'imputato  può  appellare  &#13;
 alla  sezione  istruttoria contro la sentenza del giudice istruttore...  &#13;
 quando è stato dichiarato non doversi procedere perché trattasi... di  &#13;
 persona non punibile perché il fatto non costituisce                    &#13;
  reato".                                                                 &#13;
     Il condizionamento di tale disposto alla concomitante  applicazione  &#13;
 o  alla  successiva  applicabilità  di  una  misura  di  sicurezza  fu  &#13;
 successivamente inserito (v. disegno di legge presentato  dal  Ministro  &#13;
 di  grazia  e giustizia De Pietro alla Camera dei deputati nella seduta  &#13;
 del 3 agosto 1954, n. 1121 della  II  legislatura)  per  la  dichiarata  &#13;
 preoccupazione  di  evitare  "eventuali  ripercussioni  sfavorevoli sui  &#13;
 diritti dei terzi": una preoccupazione ormai  comunque  superata  dalle  &#13;
 pronunce  di  illegittimità costituzionale contenute nelle sentenze n.  &#13;
 55 del 1971, n.  99 del 1973 e n. 165 del 1975.                          &#13;
     9. - Una volta dichiarata l'illegittimità costituzionale dell'art.  &#13;
 387, terzo comma, del codice di procedura penale nei termini dei  quali  &#13;
 si  è  detto,  deve essere dichiarata d'ufficio, ai sensi dell'art. 27  &#13;
 della legge 11 marzo 1953,  n.    87,  l'illegittimità  costituzionale  &#13;
 dell'art.  399,  primo  comma,  del  codice  di procedura penale, quale  &#13;
 sostituito dapprima ad opera dell'art. 19 della legge 18  giugno  1955,  &#13;
 n. 517, e poi ad opera dell'art. 11 della legge 31 luglio 1984, n. 400,  &#13;
 nella  parte  in cui, del tutto analogamente all'art. 387, terzo comma,  &#13;
 del codice di procedura penale, riconosce all'imputato  il  diritto  di  &#13;
 appellare   contro  la  sentenza  istruttoria  del  pretore  che  abbia  &#13;
 dichiarato non doversi  procedere  "perché  trattasi  di  persona  non  &#13;
 punibile  perché il fatto non costituisce reato" soltanto "se è stata  &#13;
 applicata o può con  provvedimento  successivo  essere  applicata  una  &#13;
 misura di sicurezza".                                                    &#13;
     10.  -  Occorre, altresì, far luogo all'applicazione dell'art.  27  &#13;
 della legge 11 marzo 1953, n. 87:  A) nei confronti dell'art. 512 n.  2  &#13;
 del  codice  di  procedura  penale,  quale sostituito dapprima ad opera  &#13;
 dell'art. 19 della legge 18 giugno 1955, n. 517, poi ad opera dell'art.  &#13;
 134 della legge 24 novembre 1981, n. 689, ed infine ad opera  dell'art.  &#13;
 3  della  legge  31  luglio 1984, n. 400, nella parte in cui, del tutto  &#13;
 analogamente all'art. 387, terzo comma, del codice di procedura penale,  &#13;
 riconosce all'imputato il diritto di appellare contro la  sentenza  del  &#13;
 pretore  che  al  termine  del  giudizio l'abbia prosciolto "perché si  &#13;
 tratta di persona non punibile perché il fatto non costituisce  reato"  &#13;
 soltanto  "se  è stata applicata o può, con provvedimento successivo,  &#13;
 essere applicata una misura di sicurezza".                               &#13;
     B) nei confronti dell'art. 513 n. 2 del codice di procedura penale,  &#13;
 quale sostituito dapprima ad opera dell'art. 19 della legge  18  giugno  &#13;
 1955,  n. 517, poi ad opera dell'art. 135 della legge 24 novembre 1981,  &#13;
 n. 689, ed infine ad opera dell'art. 4 della legge 31 luglio  1984,  n.  &#13;
 400,  nella  parte  in  cui, del tutto analogamente all'art. 387, terzo  &#13;
 comma, del  codice  di  procedura  penale,  riconosce  all'imputato  il  &#13;
 diritto  di appellare contro la sentenza del tribunale o della corte di  &#13;
 assise che l'abbia prosciolto con la  formula  "perché  si  tratta  di  &#13;
 persona  non  punibile perché il fatto non costituisce reato" soltanto  &#13;
 "se è stata applicata o può,  con  provvedimento  successivo,  essere  &#13;
 applicata una misura di sicurezza".</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a)  dichiara  l'illegittimità  costituzionale dell'art. 387, terzo  &#13;
 comma, del codice di procedura penale (nel testo  sostituito  ad  opera  &#13;
 dell'art.  19  della  legge 18 giugno 1955, n. 517), nella parte in cui  &#13;
 riconosce  all'imputato  il  diritto  di  proporre  appello  contro  la  &#13;
 sentenza  del  giudice  istruttore  che  abbia  dichiarato  non doversi  &#13;
 procedere "perché trattasi di persona non punibile  perché  il  fatto  &#13;
 non costituisce reato" limitatamente alle ipotesi nelle quali sia stata  &#13;
 applicata  o  possa, con provvedimento successivo, essere applicata una  &#13;
 misura di sicurezza;                                                     &#13;
     b) dichiara d'ufficio, in applicazione dell'art. 27 della legge  11  &#13;
 marzo 1953, n. 87, l'illegittimità costituzionale dell'art. 399, primo  &#13;
 comma, del codice di procedura penale (nel testo sostituito dapprima ad  &#13;
 opera  dell'art.  19 della legge 18 giugno 1955, n. 517, e poi ad opera  &#13;
 dell'art. 11 della legge 31 luglio 1984, n. 400), nella  parte  in  cui  &#13;
 riconosce  all'imputato  il  diritto  di  proporre  appello  contro  la  &#13;
 sentenza  del  pretore  che  abbia  dichiarato  non  doversi  procedere  &#13;
 "perché  trattasi  di  persona  non  punibile  perché  il  fatto  non  &#13;
 costituisce reato" limitatamente alle ipotesi  nelle  quali  sia  stata  &#13;
 applicata  o  possa, con provvedimento successivo, essere applicata una  &#13;
 misura di sicurezza;                                                     &#13;
     c) dichiara d'ufficio, in applicazione dell'art. 27 della legge  11  &#13;
 marzo  1953,  n. 87, l'illegittimità costituzionale dell'art. 512 n. 2  &#13;
 del codice di procedura penale (nel testo dapprima sostituito ad  opera  &#13;
 dell'art. 19 della legge 18 giugno 1955, n. 517, poi ad opera dell'art.  &#13;
 134  della legge 24 novembre 1981, n. 689, ed infine ad opera dell'art.  &#13;
 3 della legge 31 luglio 1984, n. 400), nella  parte  in  cui  riconosce  &#13;
 all'imputato  il  diritto  di  proporre  appello contro la sentenza del  &#13;
 pretore che l'abbia  prosciolto  "perché  si  tratta  di  persona  non  &#13;
 punibile  perché  il  fatto  non costituisce reato" limitatamente alle  &#13;
 ipotesi nelle quali sia  stata  applicata  o  possa,  con  procedimento  &#13;
 successivo, essere applicata una misura di sicurezza;                    &#13;
     d)  dichiara d'ufficio, in applicazione dell'art. 27 della legge 11  &#13;
 marzo 1953, n. 87, l'illegittimità costituzionale dell'art. 513  n.  2  &#13;
 del  codice di procedura penale (nel testo dapprima sostituito ad opera  &#13;
 dell'art. 19 della legge 18 giugno 1955, n. 517, poi ad opera dell'art.  &#13;
 135 della legge 24 novembre 1981, n. 689, ed infine ad opera  dell'art.  &#13;
 4  della  legge  31  luglio 1984, n. 400), nella parte in cui riconosce  &#13;
 all'imputato il diritto di proporre  appello  contro  la  sentenza  del  &#13;
 tribunale  o  della  corte  d'assise che l'abbia prosciolto "perché si  &#13;
 tratta di persona non punibile perché il fatto non costituisce  reato"  &#13;
 limitatamente alle ipotesi nelle quali sia stata applicata o possa, con  &#13;
 provvedimento successivo, essere applicata una misura di sicurezza.      &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 1 luglio 1986.          &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - GIOVANNI  CONSO  -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   - GABRIELE PESCATORE - UGO SPAGNOLI -  &#13;
                                   FRANCESCO PAOLO CASAVOLA.              &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
