<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>467</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:467</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/10/2000</data_decisione>
    <data_deposito>03/11/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Annibale &#13;
 MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'art. 33, comma 2, del    &#13;
 codice  di  procedura  penale in relazione all'art. 1, secondo comma,    &#13;
 della   legge   7 maggio   1981,  n. 180  (Modifiche  all'ordinamento    &#13;
 giudiziario  militare  di  pace),  agli  artt. 7-bis  e  97 del regio    &#13;
 decreto  30 gennaio  1941, n. 12 (Ordinamento giudiziario) e al regio    &#13;
 decreto 9 settembre 1941, n. 1022 (Ordinamento giudiziario militare),    &#13;
 promossi  con  ordinanze  emesse il 19 novembre 1999 (n. 5 ordinanze)    &#13;
 dal  tribunale  militare  di  Verona  e  il  13 gennaio  2000  (n. 10    &#13;
 ordinanze)   dal   giudice  dell'udienza  preliminare  del  tribunale    &#13;
 militare  di  Padova,  rispettivamente iscritte ai nn. da 13 a 17, da    &#13;
 264  a  272  e  310  del  registro  ordinanze 2000 e pubblicate nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  nn. 6,  22  e  24,  1ª  serie    &#13;
 speciale, dell'anno 2000.                                                &#13;
     Visti  gli  atti  di  intervento del presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella camera di consiglio del 28 settembre 2000 il giudice    &#13;
 relatore Cesare Mirabelli.                                               &#13;
     Ritenuto   che  il  tribunale  militare  di  Verona,  con  cinque    &#13;
 ordinanze  emesse  il  19 novembre 1999 (reg. ord. nn. da 13 a 17 del    &#13;
 2000),  ed il giudice dell'udienza preliminare del tribunale militare    &#13;
 di  Padova,  con dieci ordinanze emesse il 13 gennaio 2000 (reg. ord.    &#13;
 nn. da  264  a  272 e 310 del 2000), nel corso di procedimenti penali    &#13;
 nei  quali  erano  stati  adottati,  secondo  quanto  riferiscono  le    &#13;
 ordinanze  di  rimessione,  provvedimenti  di supplenza senza seguire    &#13;
 criteri   obiettivi  e  predeterminati  e  senza  motivazione,  hanno    &#13;
 sollevato,  in  riferimento  agli  artt. 3  e  25, primo comma, della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art. 33,    &#13;
 comma  2,  cod.  proc.  pen., in relazione all'art. 1, secondo comma,    &#13;
 della   legge   7 maggio   1981,  n. 180  (Modifiche  all'ordinamento    &#13;
 giudiziario  militare  di  pace),  agli  artt. 7-bis  e  97 del regio    &#13;
 decreto  30 gennaio 1941, n. 12 (Ordinamento giudiziario), modificati    &#13;
 dalla  legge  4 maggio  1998,  n. 133, e al regio decreto 9 settembre    &#13;
 1941, n. 1022 (Ordinamento giudiziario militare);                        &#13;
     che,  ad  avviso  dei  giudici  rimettenti,  queste  disposizioni    &#13;
 consentirebbero  provvedimenti  di applicazione e supplenza del tutto    &#13;
 discrezionali  e  privi  di  motivazione,  senza  che si determini la    &#13;
 nullità   per   inosservanza   delle   disposizioni  concernenti  le    &#13;
 condizioni  di capacità del giudice; esse sarebbero in contrasto con    &#13;
 il  principio  del giudice naturale precostituito per legge (art. 25,    &#13;
 primo  comma,  Cost.),  diretto a garantire che anche la composizione    &#13;
 degli  organi  giudiziari  sia  sottratta  ad  ogni  possibilità  di    &#13;
 arbitrio,  e  con  il  principio  di  eguaglianza (art. 3 Cost.), per    &#13;
 l'ingiustificata  disparità  di  trattamento  nella disciplina delle    &#13;
 supplenze  e delle applicazioni per la magistratura militare rispetto    &#13;
 alla disciplina prevista per la magistratura ordinaria;                  &#13;
     che  in  tutti  i  giudizi  dinanzi  alla Corte è intervenuto il    &#13;
 Presidente   del  Consiglio  dei  Ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura  generale dello Stato, chiedendo che la questione sia    &#13;
 dichiarata  infondata,  in  quanto il problema della disciplina delle    &#13;
 supplenze di magistrati nell'ordinamento giudiziario militare sarebbe    &#13;
 da risolvere in via interpretativa, estendendo ai magistrati militari    &#13;
 le  stesse  norme che disciplinano stato giuridico e indipendenza dei    &#13;
 magistrati ordinari.                                                     &#13;
     Considerato  che  la  questione  di legittimità costituzionale -    &#13;
 sollevata  con  ordinanze  di  identico  contenuto  ed  i cui giudizi    &#13;
 possono,  pertanto,  essere  riuniti  -  investe  la disciplina delle    &#13;
 supplenze    di   magistrati   nei   tribunali   militari,   giacché    &#13;
 l'ordinamento  per essi previsto consentirebbe, ad avviso dei giudici    &#13;
 rimettenti,  al presidente della corte militare d'appello di adottare    &#13;
 provvedimenti di sostituzione di magistrati, in caso di loro mancanza    &#13;
 o   impedimento,  per  la  composizione  di  collegi  giudicanti  con    &#13;
 magistrati   di   altri  uffici  giudiziari,  senza  seguire  criteri    &#13;
 precostituiti  con  le  tabelle  degli  uffici giudicanti e senza una    &#13;
 motivazione  dei  provvedimenti  che permetta di verificare i criteri    &#13;
 seguiti per la sostituzione;                                             &#13;
     che,  successivamente  alle  ordinanze  di  rimessione,  identica    &#13;
 questione è stata dichiarata non fondata (sentenza n. 392 del 2000),    &#13;
 non potendo esserne condivisa la premessa interpretativa;                &#13;
     che, infatti, si deve ritenere - come del resto ha già affermato    &#13;
 il  Consiglio  della  magistratura  militare fondandosi proprio sulla    &#13;
 equiparazione  dello stato giuridico dei magistrati militari a quelli    &#13;
 ordinari,  stabilita  dall'art. 1  della  legge n. 180 del 1981 - che    &#13;
 nell'ambito  dell'ordinamento  giudiziario  militare operino tutte le    &#13;
 norme   dell'ordinamento  giudiziario  comune,  comprese  quelle  che    &#13;
 disciplinano le applicazioni e le supplenze dei magistrati mancanti o    &#13;
 impediti:  anche  nell'ordinamento  militare applicazioni e supplenze    &#13;
 possono,  quindi,  essere disposte solo seguendo criteri prefissati e    &#13;
 con   provvedimenti   motivati,   che  consentano  di  verificare  la    &#13;
 rispondenza  di ogni singolo provvedimento adottato ai presupposti ed    &#13;
 ai  criteri  obiettivi  indicati  dal  Consiglio  della  magistratura    &#13;
 militare;                                                                &#13;
     che,  pertanto,  non  prospettando  le  ordinanze  di  rimessione    &#13;
 profili od argomenti nuovi rispetto a quelli già esaminati da questa    &#13;
 Corte, la questione deve essere dichiarata manifestamente infondata.     &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87  e  9,  secondo  comma,  delle  norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti giudizi,                                                     &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 33, secondo comma, del codice    &#13;
 di  procedura  penale,  in relazione all'art. 1, secondo comma, della    &#13;
 legge  7 maggio  1981,  n. 180 (Modifiche all'ordinamento giudiziario    &#13;
 militare di pace), agli artt. 7-bis e 97 del regio decreto 30 gennaio    &#13;
 1941,   n. 12   (Ordinamento  giudiziario),  modificati  dalla  legge    &#13;
 4 maggio  1998,  n. 133, e al regio decreto 9 settembre 1941, n. 1022    &#13;
 (Ordinamento  giudiziario  militare),  sollevata, in riferimento agli    &#13;
 artt. 3 e 25, primo comma, della Costituzione, dal tribunale militare    &#13;
 di  Verona  e  dal  giudice  dell'udienza  preliminare  del tribunale    &#13;
 militare di Padova con le ordinanze indicate in epigrafe.                &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 ottobre 2000.                              &#13;
                 Il Presidente e redattore: Mirabelli                     &#13;
                       Il cancelliere: Di Paola                           &#13;
       Depositata in cancelleria il 3 novembre 2000.                      &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
