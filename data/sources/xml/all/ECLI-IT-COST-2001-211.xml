<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>211</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:211</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/07/2001</data_decisione>
    <data_deposito>04/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, CarloMEZZANOTTE, Fernanda CONTRI, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE,Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 195, quarto &#13;
comma,  del  regio  decreto  16 marzo  1942,  n. 267  (Disciplina del &#13;
fallimento,    del    concordato   preventivo,   dell'amministrazione &#13;
controllata e della liquidazione coatta amministrativa), promosso con &#13;
ordinanza  emessa  il  7 ottobre  1999  dal  tribunale  di  Udine nel &#13;
procedimento  civile  proposto  dal  Centro  regionale  servizi (CRS) &#13;
contro l'Istituto nazionale della previdenza sociale (INPS) ed altri, &#13;
iscritta  al  n. 162  del  registro ordinanze 2000 e pubblicata nella &#13;
Gazzetta   Ufficiale  della  Repubblica  n. 17,  1a  serie  speciale, &#13;
dell'anno 2000. &#13;
    Visto l'atto di costituzione dell'INPS; &#13;
    Udito   nell'udienza  pubblica  del  22 maggio  2001  il  giudice &#13;
relatore Annibale Marini.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Nel corso di un giudizio di opposizione contro una sentenza &#13;
dichiarativa  dello  stato  di insolvenza, il tribunale di Udine, con &#13;
ordinanza emessa il 7 ottobre 1999, ha sollevato, in riferimento agli &#13;
artt. 3   e   24   della   Costituzione,  questione  di  legittimità &#13;
costituzionale   dell'art. 195,   quarto  comma,  del  regio  decreto &#13;
16 marzo  1942,  n. 267  (Disciplina  del  fallimento, del concordato &#13;
preventivo,  dell'amministrazione  controllata  e  della liquidazione &#13;
coatta amministrativa), nella parte in cui dispone che il termine per &#13;
fare  opposizione  contro  la  sentenza  che  dichiara  lo  stato  di &#13;
insolvenza    di    un'impresa   soggetta   a   liquidazione   coatta &#13;
amministrativa decorre, anche per l'impresa, dalla data di affissione &#13;
invece che da quella di notificazione della sentenza. &#13;
    Premessa  la  rilevanza della questione - in quanto il giudizio a &#13;
quo  ha  ad oggetto una opposizione proposta dall'impresa della quale &#13;
è  stato  dichiarato  lo  stato  di insolvenza entro i trenta giorni &#13;
dalla  notificazione  della  sentenza  ma  oltre il trentesimo giorno &#13;
dalla  sua affissione - il rimettente osserva come l'affissione della &#13;
sentenza  è  un  mezzo  di pubblicità che crea una mera presunzione &#13;
legale  di conoscenza dell'atto. La previsione di notificazione della &#13;
sentenza,  pur  contenuta nel terzo comma dello stesso art. 195 della &#13;
legge  fallimentare,  non  offrirebbe, d'altro canto, certezza che il &#13;
debitore   sia  posto  in  condizione  di  conoscere  tempestivamente &#13;
l'avvenuta  dichiarazione  dello  stato  di insolvenza, in quanto nel &#13;
sistema  della legge non è prescritta l'anteriorità o simultaneità &#13;
di detta notificazione rispetto all'affissione. &#13;
    Il   ricorso   all'affissione,   quale   mezzo  di  comunicazione &#13;
dell'atto,  troverebbe,  dunque,  giustificazione  -  ad  avviso  del &#13;
rimettente  -  nei  soli  casi  in  cui,  per il rilevante numero dei &#13;
destinatari  o  per  la  difficoltà di individuarli tutti, si riveli &#13;
concretamente   impossibile  il  ricorso  a  forme  di  comunicazione &#13;
diretta,  ma non certo quando il destinatario sia, come nella specie, &#13;
sicuramente ed agevolmente identificabile. &#13;
    La  norma  impugnata risulterebbe, pertanto, in contrasto sia con &#13;
il   principio   di   eguaglianza   di   cui  all'art. 3  Cost.,  per &#13;
l'ingiustificata  equiparazione, ai fini della decorrenza del termine &#13;
per  l'opposizione, tra l'impresa di cui viene dichiarato lo stato di &#13;
insolvenza e qualsiasi altro interessato, genericamente indicato, sia &#13;
con  il  diritto  di  difesa  tutelato  dall'art. 24  Cost.,  per  il &#13;
pregiudizio  che  ne  deriva  all'effettività  del diritto di difesa &#13;
dell'impresa medesima. &#13;
    Ricorda,  da  ultimo,  il  rimettente  come  la  Corte abbia già &#13;
dichiarato  l'illegittimità  costituzionale  di numerose altre norme &#13;
della  legge  fallimentare,  proprio  nella parte in cui prevedevano, &#13;
anche  per il debitore, il decorso di termini decadenziali dalla data &#13;
di affissione del provvedimento oggetto di impugnazione. &#13;
    2.  -  Si  è  costituito  in giudizio l'Istituto nazionale della &#13;
previdenza  sociale  (INPS), creditore istante convenuto nel giudizio &#13;
di opposizione, concludendo per la declaratoria di inammissibilità o &#13;
di infondatezza della questione. &#13;
    Ad  avviso  della  parte,  la  questione  sarebbe irrilevante nel &#13;
giudizio  a  quo  in  quanto, essendo stata la sentenza notificata il &#13;
giorno  successivo  a quello di affissione, l'impresa avrebbe potuto, &#13;
con  l'ordinaria  diligenza,  accertare  tempestivamente  la  data di &#13;
affissione ai fini della proposizione dell'opposizione. &#13;
    La  questione  sarebbe, comunque, infondata nel merito essendo il &#13;
termine,  di  trenta  giorni,  fissato  per l'opposizione dalla norma &#13;
impugnata,  talmente  ampio  da  evitare qualsiasi compromissione del &#13;
diritto  di difesa, considerato anche che il terzo comma dello stesso &#13;
art. 195  della  legge  fallimentare  comunque prevede - a differenza &#13;
dell'art. 18  della  medesima  legge, dichiarato illegittimo in parte &#13;
qua  con  sentenza  n. 151  del  1980  - unitamente all'affissione la &#13;
notificazione della sentenza.<diritto>Considerato in diritto</diritto>1. -  Il tribunale di Udine dubita, in riferimento agli artt. 3 e &#13;
24    della    Costituzione,    della   legittimità   costituzionale &#13;
dell'art. 195,  quarto comma, del regio decreto 16 marzo 1942, n. 267 &#13;
(Disciplina    del    fallimento,    del    concordato    preventivo, &#13;
dell'amministrazione   controllata   e   della   liquidazione  coatta &#13;
amministrativa),  nella  parte  in  cui  dispone  che  il termine per &#13;
proporre  opposizione  contro  la  sentenza  che dichiara lo stato di &#13;
insolvenza    di    un'impresa   soggetta   a   liquidazione   coatta &#13;
amministrativa decorre, anche per l'impresa, dalla data di affissione &#13;
della sentenza invece che da quella di notificazione. &#13;
    2. -   L'Istituto   nazionale  della  previdenza  sociale  (INPS) &#13;
eccepisce  preliminarmente  l'inammissibilità  della  questione, per &#13;
difetto   di  rilevanza,  in  quanto  -  nel  caso  di  specie  -  la &#13;
notificazione  all'impresa della sentenza dichiarativa dello stato di &#13;
insolvenza   sarebbe  avvenuta  il  giorno  successivo  a  quello  di &#13;
affissione,  cosicché  l'impresa  stessa  avrebbe  potuto, usando la &#13;
normale  diligenza,  accertare tempestivamente la data di affissione, &#13;
dies a quo per la proposizione dell'opposizione. &#13;
    L'eccezione   è   priva  di  fondamento,  in  quanto  la  natura &#13;
decadenziale  del termine per proporre opposizione priva all'evidenza &#13;
di   qualsiasi   rilievo   lo   stato  soggettivo  dell'opponente  e, &#13;
conseguentemente, la sua eventuale negligenza nell'accertamento della &#13;
(data di) affissione della sentenza. &#13;
    3. - Nel merito, la questione è fondata. &#13;
    3.1. -  L'art. 195  della  legge fallimentare, pur prevedendo, al &#13;
terzo  comma,  che la sentenza dichiarativa dello stato di insolvenza &#13;
di un'impresa soggetta a liquidazione coatta amministrativa venga non &#13;
solo  affissa ma anche notificata, al successivo comma dispone che il &#13;
termine di trenta giorni per la proposizione dell'opposizione decorre &#13;
per  ogni interessato, compresa quindi l'impresa della quale è stato &#13;
dichiarato lo stato di insolvenza, dalla data di affissione. &#13;
    Al  riguardo  questa  Corte,  scrutinando altre norme della legge &#13;
fallimentare  contenenti  analoga previsione di decorrenza di termini &#13;
decadenziali    dalla    data    di   affissione   di   provvedimenti &#13;
giurisdizionali,  ha  già  avuto  modo  di  affermare  che la scelta &#13;
dell'affissione,  quale  atto  idoneo  a far decorrere il termine per &#13;
l'impugnazione  di  un  atto,  può  essere  giustificata  solo dalla &#13;
difficoltà  di  individuare  coloro  che  possono  avere interesse a &#13;
proporre l'impugnazione stessa (sentenze n. 273 del 1987 e n. 153 del &#13;
1980),  risultando  priva  di  razionale  giustificazione  quando  si &#13;
tratti,  invece,  di  soggetti  già  individuati  (sentenze n. 152 e &#13;
n. 151 del 1980, n. 255 del 1974). &#13;
    Si  è, infatti, osservato che l'affissione non può considerarsi &#13;
un equipollente della notificazione in quanto essa determina una mera &#13;
presunzione   di   conoscibilità  dell'atto,  peraltro  insuperabile &#13;
(sentenza  n. 255 del 1974), compatibile con il diritto di difesa del &#13;
destinatario  nei  soli casi in cui l'individuazione di questi, ed il &#13;
conseguente  ricorso  a  mezzi  di  comunicazione  diretta  dell'atto &#13;
stesso, risultino impossibili o estremamente difficoltosi. &#13;
    La   norma   denunciata,   disponendo   che  il  termine  per  la &#13;
proposizione  dell'opposizione  decorra,  anche  per  l'impresa della &#13;
quale è stato dichiarato lo stato di insolvenza, così come per ogni &#13;
altro  interessato,  dalla  data di affissione della sentenza (la cui &#13;
conoscenza  richiede  una specifica attività di accertamento) invece &#13;
che  da  quella della pur prevista notificazione, si pone pertanto in &#13;
contrasto  sia  con  il  principio  di  eguaglianza di cui all'art. 3 &#13;
Cost.,   in  quanto  assoggetta  ad  identica  disciplina  situazioni &#13;
significativamente   diverse,   sia   con   il   diritto   di  difesa &#13;
dell'impresa,     tutelato     dall'art. 24    Cost.,    in    quanto &#13;
rende maggiormente    difficoltosa,    senza    alcuna    ragionevole &#13;
giustificazione,  la  proposizione  dell'opposizione.  E va, per tale &#13;
aspetto, dichiarata costituzionalmente illegittima.</testo>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  l'illegittimità  costituzionale  dell'art. 195, quarto &#13;
comma,  del  regio  decreto  16 marzo  1942,  n. 267  (Disciplina del &#13;
fallimento,    del    concordato   preventivo,   dell'amministrazione &#13;
controllata  e della liquidazione coatta amministrativa), nella parte &#13;
in  cui  prevede  che  il  termine per proporre opposizione contro la &#13;
sentenza  che  dichiara  lo stato di insolvenza di impresa soggetta a &#13;
liquidazione  coatta  amministrativa  decorre,  anche  per l'impresa, &#13;
dall'affissione invece che dalla notificazione della sentenza. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 2 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                        Il redattore: Marini &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 4 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
