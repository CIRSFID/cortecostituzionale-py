<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>96</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:96</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Vezio Crisafulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/05/1972</data_decisione>
    <data_deposito>18/05/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  34  del  &#13;
 decreto legislativo 30 aprile 1970, n. 639  (attuazione  delle  deleghe  &#13;
 conferite  al Governo con gli artt. 27 e 29 della legge 30 aprile 1969,  &#13;
 n. 153, concernente revisione degli ordinamenti pensionistici  e  norme  &#13;
 in  materia  di sicurezza sociale), promosso con ordinanza emessa il 10  &#13;
 novembre 1971 dalla Corte costituzionale in giudizio per  conflitto  di  &#13;
 attribuzione tra la Regione siciliana e lo Stato, iscritta al n.451 del  &#13;
 registro  ordinanze  1971  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 323 del 22 dicembre 1971.                                  &#13;
     Visti gli atti  di  costituzione  della  Regione  siciliana  e  del  &#13;
 Presidente del Consiglio dei ministri;                                   &#13;
     udito  nell'udienza  pubblica del 22 marzo 1972 il Giudice relatore  &#13;
 Vezio Crisafulli;                                                        &#13;
     uditi gli avvocati Salvatore Villari e  Antonino  Sansone,  per  la  &#13;
 Regione  siciliana,  ed  il  sostituto  avvocato  generale  dello Stato  &#13;
 Michele Savarese, per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo>Ritenuto:                                                                &#13;
     che nel  corso  di  un  giudizio  per  conflitto  di  attribuzione,  &#13;
 proposto  dal  Presidente  della  Regione siciliana nei confronti dello  &#13;
 Stato,  che  traeva  origine  dai  decreti  di  nomina   dei   comitati  &#13;
 provinciali  dell'INPS  nell'Isola,  nei  quali  non era incluso nessun  &#13;
 rappresentante della Regione, questa Corte, con ordinanza n. 181 del 10  &#13;
 novembre 1971 ha sollevato  questione  di  legittimità  costituzionale  &#13;
 dell'art.  34  del  d.l.  30 aprile 1970,   n. 639, in riferimento agli  &#13;
 artt. 17, lett. f), e 20 dello Statuto siciliano, in relazione all'art.  &#13;
 4 delle norme di attuazione emanate con d.P.R.    26  giugno  1952,  n.  &#13;
 1138;                                                                    &#13;
     che il dubbio di costituzionalità sta nella circostanza che l'art.  &#13;
 34  omette  di  prevedere  la rappresentanza della Regione nei comitati  &#13;
 provinciali anzidetti.Considerato:                                                             &#13;
     che gli artt. 17, lett.  f),  e  20  dello  Statuto  della  Regione  &#13;
 siciliana  attribuiscono  alla  Regione  stessa  potestà legislativa e  &#13;
 amministrativa in materia di "previdenza ed assistenza sociale";         &#13;
     che, in ottemperanza a tali norme statutarie, l'art. 4  del  d.P.R.  &#13;
 25 giugno 1952, n. 1138, nell'operare il trasferimento alla Regione del  &#13;
 concreto  esercizio  delle  funzioni  amministrative  in detta materia,  &#13;
 stabilisce   espressamente   che   l'Amministrazione   regionale    sia  &#13;
 rappresentata  negli  organi  locali  degli  enti  ed istituti pubblici  &#13;
 esplicanti attività in quella rientranti;                               &#13;
     che  tra  questi  enti  è  compreso  l'Istituto  nazionale   della  &#13;
 previdenza sociale, in ordine al quale, tuttavia, l'art. 34 del decreto  &#13;
 legislativo  n.  639,  nel  disciplinare  la  composizione dei relativi  &#13;
 Comitati  provinciali,  omette  di  prevedere  in  seno  ad   essi   la  &#13;
 rappresentanza della Regione;                                            &#13;
     che  tale  rappresentanza  non  potrebbe  ritenersi  in  alcun modo  &#13;
 assicurata, secondo una tesi dubitativamente prospettata  dalla  difesa  &#13;
 dello  Stato, dai direttori degli Uffici provinciali del lavoro e della  &#13;
 massima occupazione in quanto chiamati ad  esercitare,  nella  Regione,  &#13;
 anche  funzioni  regionali  ed  all'uopo  posti  alle  dipendenze della  &#13;
 Regione, giacché, a prescindere da ogni altra considerazione di ordine  &#13;
 più generale, nella specie i direttori degli  Uffici  provinciali  del  &#13;
 lavoro  partecipano  ai  comitati  nella loro naturale ed istituzionale  &#13;
 figura di organi dell'Amministrazione statale di cui fanno  valere  gli  &#13;
 interessi,  che  potrebbero  anche, in ipotesi, essere in conflitto con  &#13;
 quelli dell'Amministrazione regionale;                                   &#13;
     che deve pertanto essere dichiarata l'illegittimità costituzionale  &#13;
 dell'art. 34 del decreto legislativo n.  639  del  1970,  limitatamente  &#13;
 alla  sua  applicazione  nella  Regione  siciliana e nella parte in cui  &#13;
 omette di prevedere, e  quindi  esclude,  che  la  Regione  stessa  sia  &#13;
 rappresentata  nei  Comitati  provinciali  dell'INPS, e la pronuncia va  &#13;
 estesa, in applicazione dell'art. 27 della legge 11 marzo 1953, n.  87,  &#13;
 al  precedente  art. 33, relativo al Comitato regionale per la Sicilia,  &#13;
 che, per identici motivi ed entro i medesimi limiti, contrasta del pari  &#13;
 con  la  competenza  costituzionalmente  attribuita  in  materia   alla  &#13;
 Regione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a) dichiara l'illegittimità costituzionale dell'art. 34 del d.P.R.  &#13;
 30   aprile   1970,  n.  639,  nella  parte  in  cui  non  prevede  che  &#13;
 l'Amministrazione regionale siciliana sia  rappresentata  nei  Comitati  &#13;
 provinciali dell'INPS di quella Regione;                                 &#13;
     b) in applicazione dell'art. 27, ultima parte, della legge 11 marzo  &#13;
 1953,  n. 87, dichiara l'illegittimità costituzionale dell'art. 33 del  &#13;
 d.P.R. 30 aprile 1970, n.   639, nella parte in  cui  non  prevede  che  &#13;
 l'Amministrazione  regionale  siciliana  sia rappresentata nel Comitato  &#13;
 regionale dell'INPS.                                                     &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 10 maggio 1972.                               &#13;
                                   GIUSEPPE  CHIARELLI - MICHELE FRAGALI  &#13;
                                   -  COSTANTINO  MORTATI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
