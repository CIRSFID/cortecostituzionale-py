<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>118</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:118</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/04/1986</data_decisione>
    <data_deposito>30/04/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO - Prof. RENATO &#13;
 DELL'ANDRO - Prof. GABRIELE PESCATORE - Avv. UGO SPAGNOLI - Prof. &#13;
 FRANCESCO PAOLO CASAVOLA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art.  10, quinto  &#13;
 comma, del d.P.R. 30 giugno 1965 n.  1124 (T.U. delle disposizioni  per  &#13;
 l'assicurazione  obbligatoria  contro  gli  infortuni  sul  lavoro e le  &#13;
 malattie professionali) promosso con l'ordinanza  emessa  il  3  maggio  &#13;
 1984  dal  Tribunale  di  Bergamo  nel procedimento civile vertente tra  &#13;
 Nicoli Luigi e la Società C.A.M.  S.p.a.,  iscritta  al  n.  1119  del  &#13;
 registro  ordinanze  1984  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 53 bis dell'anno 1985.                                     &#13;
     Visto l'atto di costituzione di Nicoli Luigi;                        &#13;
     udito nell'udienza pubblica del 4 marzo 1986  il  Giudice  relatore  &#13;
 Giuseppe Borzellino;                                                     &#13;
     udito l'avv. Franco Agostini per Nicoli Luigi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nella  causa  promossa  da  Nicoli  Luigi contro la Società C.A.M.  &#13;
 S.p.a. di  Trescore  Balneario,  datrice  di  lavoro  dell'attore,  per  &#13;
 risarcimento dei danni subiti a seguito di un infortunio sul lavoro, il  &#13;
 Tribunale  di  Bergamo  il  3  maggio  1984  ha sollevato "questione di  &#13;
 legittimità costituzionale dell'art. 10, quinto comma, del  d.P.R.  30  &#13;
 giugno  1965  n.  1124  nella  parte  in  cui  non consente che ai fini  &#13;
 dell'esercizio  dell'azione  sulle  conseguenze  civili  dell'eventuale  &#13;
 reato  da  parte dell'infortunato, l'accertamento del fatto reato possa  &#13;
 essere  compiuto  dal  giudice  civile  anche  nel  caso  in   cui   il  &#13;
 procedimento  penale  a carico del datore di lavoro si sia concluso con  &#13;
 un provvedimento di archiviazione, in relazione agli artt. 3 e 24 della  &#13;
 Costituzione".                                                           &#13;
     Da  parte della società convenuta erasi pregiudizialmente eccepita  &#13;
 l'improponibilità della domanda in relazione al disposto dell'art.  10  &#13;
 d.P.R.    n.  1124  del  1965, poiché il Pretore di Grumello del Monte  &#13;
 aveva concluso con decreto  di  archiviazione  il  procedimento  penale  &#13;
 aperto   a   seguito   dei   fatti  di  causa,  non  ravvisando  alcuna  &#13;
 responsabilità a carico di soggetti diversi dall'infortunato.           &#13;
     L'attore  richiamava  la  sentenza  102  del   1981   della   Corte  &#13;
 costituzionale,  con  la  quale  è  stata dichiarata la illegittimità  &#13;
 costituzionale del quinto comma, appunto, dell'art. 10  d.P.R.  citato,  &#13;
 nella parte in cui non consente che, ai fini dell'esercizio del diritto  &#13;
 di  regresso  dell'INAIL,  a  seguito  sempre  di  infortunio  occorso,  &#13;
 l'accertamento del fatto possa essere compiuto dal giudice civile anche  &#13;
 nei casi in cui il procedimento penale  nei  confronti  del  datore  di  &#13;
 lavoro  o  di  un suo dipendente si sia concluso con proscioglimento in  &#13;
 sede istruttoria o vi sia provvedimento di archiviazione.                &#13;
     Il  Tribunale,  considerato  che  gli  effetti   della   menzionata  &#13;
 pronuncia  non  si  estendono all'ipotesi dell'esercizio dell'azione di  &#13;
 danni da parte del dipendente infortunato, ha tuttavia osservato che le  &#13;
 motivazioni della Corte "hanno un contenuto di carattere più  generale  &#13;
 riferibile,  in  astratto,  sia  all'azione di regresso dell'INAIL, sia  &#13;
 all'azione in sede civile del dipendente infortunato".                   &#13;
     Conseguentemente all'assunto, l'ordinanza riporta le argomentazioni  &#13;
 svolte  dalla  Corte  nella  sentenza  102  del  1981  ai  fini   della  &#13;
 dichiarazione  di  incostituzionalità  della  norma  che,  precludendo  &#13;
 all'INAIL   l'esercizio   dell'azione    sulle    conseguenze    civili  &#13;
 dell'eventuale  reato  anche  in  caso  di  sentenza di proscioglimento  &#13;
 istruttorio o di decreto di archiviazione, veniva segnatamente a  porsi  &#13;
 in  "contrasto  con  le  esigenze  di tutela del diritto di azione e di  &#13;
 difesa garantite dall'art. 24 Cost.".                                    &#13;
     Nel giudizio si è  costituito  Nicoli  Luigi,  a  mezzo  dell'avv.  &#13;
 Franco  Agostini,  chiedendo  che  la  norma  denunciata sia dichiarata  &#13;
 incostituzionale.  Sarebbe  infatti  irrazionale   e   illegittima   la  &#13;
 preclusione  del  giudice  civile all'accertamento del fatto come reato  &#13;
 disposto  dall'art.  10  d.P.R.  n.  1124  del   1965,   essendo   tale  &#13;
 accertamento  possibile  "in  tutti  i  casi in cui manchi una sentenza  &#13;
 penale e quindi anche quando sia intervenuto,  come  nella  specie,  un  &#13;
 decreto di archiviazione".<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Con sentenza n. 102 del 19 giugno 1981 questa Corte dichiarò  &#13;
 l'illegittimità costituzionale del comma quinto dell'art. 10 d.P.R. 30  &#13;
 giugno 1965, n. 1124 (testo unico sugli  infortuni  sul  lavoro)  nella  &#13;
 parte  in  cui  non consente che, ai fini dell'esercizio del diritto di  &#13;
 regresso dell'INAIL, l'accertamento del fatto  di  reato  possa  essere  &#13;
 compiuto  dal  giudice  civile  anche  nei  casi in cui il procedimento  &#13;
 penale nei confronti del datore di lavoro o di un suo dipendente si sia  &#13;
 concluso con proscioglimento in sede istruttoria o vi sia provvedimento  &#13;
 di archiviazione.                                                        &#13;
     L'odierno giudice remittente ravvisa che in identica situazione  si  &#13;
 versi  - per i fini dell'esercizio dell'azione sulle conseguenze civili  &#13;
 dell'eventuale  reato  promosso  da   parte   dell'infortunato   -   in  &#13;
 fattispecie conclusasi con provvedimento di archiviazione.               &#13;
     2. - La questione è fondata.                                        &#13;
     Lo  scopo  perseguito,  per  l'accertamento  delle   responsabilità  &#13;
 civili a seguito di infortunio sul lavoro, si pone identico ex  art.  3  &#13;
 Cost.  sia per l'azione di regresso dell'INAIL nei confronti del datore  &#13;
 di lavoro - il che ebbe a  formare  oggetto  della  citata,  precedente  &#13;
 sentenza  102/1981  -  sia  per  il  caso, di cui or qui si discute, di  &#13;
 eventuale analoga azione esperibile  dall'infortunato  medesimo,  anche  &#13;
 qui   opponendosi  -  per  l'accertamento  del  fatto  di  reato  -  il  &#13;
 provvedimento di archiviazione.                                          &#13;
     Consegue che in tale seconda ipotesi l'attuale  disposto  dell'art.  &#13;
 10,  comma quinto, d.P.R. n.  1124/1965 viene a contrastare, come posto  &#13;
 in luce nella sentenza 102, con le esigenze di tutela  del  diritto  di  &#13;
 azione e di difesa garantite dall'art. 24 Cost.                          &#13;
     Va  pertanto  dichiarata  l'illegittimità costituzionale del comma  &#13;
 quinto dell'art. 10 d.P.R. n. 1124 del 1965, nella  parte  in  cui  non  &#13;
 consente   che,   ai   fini   dell'esercizio   dell'azione   da   parte  &#13;
 dell'infortunato,  l'accertamento  del  fatto  di  reato  possa  essere  &#13;
 compiuto  dal  giudice  civile anche nel caso in cui, non essendo stata  &#13;
 promossa l'azione penale nei confronti del datore di lavoro o di un suo  &#13;
 dipendente, vi sia provvedimento di archiviazione.                       &#13;
     Ex art. 27 legge n. 87 del  1953  la  pronuncia  di  illegittimità  &#13;
 costituzionale  -  tenuta  presente la identità di situazioni rilevate  &#13;
 con la precedente sentenza n. 102/1981 -  va  estesa  sempre  al  comma  &#13;
 quinto  del  ridetto  art.  10  d.P.R.  n. 1124, nella parte in cui non  &#13;
 consente che, ai fini dell'esercizio del diritto  di  azione  da  parte  &#13;
 dell'infortunato,  l'accertamento  del  fatto  di  reato  possa  essere  &#13;
 compiuto dal giudice civile anche  nel  caso  in  cui  il  procedimento  &#13;
 penale nei confronti del datore di lavoro o di un suo dipendente si sia  &#13;
 concluso con proscioglimento in sede istruttoria.                        &#13;
     È  a  ribadirsi  tuttavia  che  a  salvaguardia  del  principio di  &#13;
 prevalenza della giustizia penale  sussiste,  comunque,  il  meccanismo  &#13;
 processuale   della   sospensione   del  processo  civile  in  caso  di  &#13;
 sopravvenuta riapertura di quello penale (o di  inizio  della  relativa  &#13;
 azione)  sui  fatti costituenti il presupposto della domanda nella sede  &#13;
 civile.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale del comma quinto dell'art.  &#13;
 10 d.P.R. n. 1124 del 1965, nella parte in cui  non  consente  che,  ai  &#13;
 fini    dell'esercizio    dell'azione    da   parte   dell'infortunato,  &#13;
 l'accertamento del fatto di reato possa  essere  compiuto  dal  giudice  &#13;
 civile  anche  nel  caso  in  cui,  non essendo stata promossa l'azione  &#13;
 penale nei confronti del datore di lavoro o di un  suo  dipendente,  vi  &#13;
 sia provvedimento di archiviazione;                                      &#13;
     dichiara  ex  art.  27  legge  n.  87  del  1953  la illegittimità  &#13;
 costituzionale del comma quinto dell'art. 10 d.P.R. n. 1124  del  1965,  &#13;
 nella parte in cui non consente che, ai fini dell'esercizio dell'azione  &#13;
 da  parte  dell'infortunato,  l'accertamento  del  fatto di reato possa  &#13;
 essere  compiuto  dal  giudice  civile  anche  nel  caso  in   cui   il  &#13;
 procedimento  penale,  nei  confronti  del datore di lavoro o di un suo  &#13;
 dipendente, si sia concluso con proscioglimento in sede istruttoria.     &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 aprile 1986.         &#13;
                                   F.to:   LIVIO   PALADIN   -  VIRGILIO  &#13;
                                   ANDRIOLI   -   GIUSEPPE   FERRARI   -  &#13;
                                   FRANCESCO  SAJA  -  GIOVANNI  CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   -   RENATO   DELL'ANDRO   -  GABRIELE  &#13;
                                   PESCATORE - UGO SPAGNOLI -  FRANCESCO  &#13;
                                   PAOLO CASAVOLA.                        &#13;
                                   ROLANDO GALLI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
