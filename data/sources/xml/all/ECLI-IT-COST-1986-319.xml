<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>319</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:319</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1986</data_decisione>
    <data_deposito>31/12/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI; prof. Giuseppe FERRARI; dott. &#13;
 Francesco SAJA; prof. Giovanni CONSO; prof. Aldo CORASANITI; prof. &#13;
 Giuseppe BORZELLINO; dott. Francesco GRECO; prof. Gabriele &#13;
 PESCATORE; avv. Ugo SPAGNOLI; prof. Francesco P. CASAVOLA; prof. &#13;
 Antonio BALDASSARRE; prof. Vincenzo CAIANIELLO.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  5, quarto    &#13;
 comma,  del  d.P.R.  21  agosto  1971  n.   1275   (Regolamento   per    &#13;
 l'esecuzione  della  legge  2  aprile  1968,  n.  475,  recante norme    &#13;
 concernenti il servizio farmaceutico) promosso con  ordinanza  emessa    &#13;
 il  6  luglio  1979  dal  Tribunale  Amministrativo  Regionale per la    &#13;
 Lombardia - sezione staccata di Brescia  -  su  ricorso  proposto  da    &#13;
 Antonio  Lanzarini  contro  Regione Lombardia ed altri iscritta al n.    &#13;
 169 del registro ordinanze 1980 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n.131 dell'anno 1980;                                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 9 dicembre 1986 il Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto  che,  con  l'ordinanza  in epigrafe, emessa nel giudizio    &#13;
 promosso  da  Antonio  Lanzarini  per  ottenere  l'annullamento   del    &#13;
 provvedimento  del Presidente della Giunta regionale Lombardia con il    &#13;
 quale era stata approvata la graduatoria di merito del  concorso  per    &#13;
 il  conferimento  di  una  delle  sedi  farmaceutiche  vacanti  nella    &#13;
 provincia di Mantova, dei verbali della  Commissione  giudicatrice  e    &#13;
 del provvedimento con il quale il medico provinciale di Mantova aveva    &#13;
 assegnato al dott. Renzo Cavicchiolo la sede farmaceutica  n.  2  del    &#13;
 comune di Bagnolo San Vito, il Tribunale Amministrativo Regionale per    &#13;
 la Lombardia - sezione staccata di Brescia - ha  sollevato  questione    &#13;
 di  legittimità  costituzionale dell'art. 5, quarto comma, d.P.R. 21    &#13;
 agosto 1971, n. 1275 in quanto, ai fini del conferimento  delle  sedi    &#13;
 farmaceutiche,  pur attribuendo valore all'attività di collaboratore    &#13;
 scientifico espletata dai concorrenti presso  società  farmaceutiche    &#13;
 private,  non  prevede  i  congegni attraverso i quali il concorrente    &#13;
 interessato possa offrire la documentazione dell'attività espletata;    &#13;
      che  nell'ordinanza  di  rimessione  si  afferma  infatti che la    &#13;
 disposizione impugnata contrasta con gli artt. 3 e 97 Cost. in quanto    &#13;
 determina una situazione di sostanziale diversità di trattamento tra    &#13;
 soggetti in possesso di  titoli  diversi  ma  riconosciuti  parimenti    &#13;
 utili  ai  fini della procedura concorsuale e prevede un sistema che,    &#13;
 non consentendo di attribuire  rilevanza  a  servizi  che  lo  stesso    &#13;
 legislatore  ordinario riconosce in altro punto significativi ai fini    &#13;
 della procedura  concorsuale,  non  risponde  ai  principi  di  buona    &#13;
 amministrazione;                                                         &#13;
      che  è  intervenuto  il  Presidente  del Consiglio dei ministri    &#13;
 rappresentato dall'Avvocatura Generale dello Stato chiedendo  che  la    &#13;
 Corte  dichiari  inammissibile  la  proposta  impugnativa  in  quanto    &#13;
 rivolta a denunciare la pretesa illegittimità costituzionale di  una    &#13;
 norma  regolamentare,  priva  della  forza  e del valore propri della    &#13;
 legge e degli atti ad essa equiparati;                                   &#13;
      che  la  predetta eccezione deve essere accolta in quanto l'atto    &#13;
 impugnato è privo di forza di legge come si desume dal fatto che  si    &#13;
 autoqualifica  regolamento di esecuzione, che non vi è traccia nella    &#13;
 legge per la esecuzione della quale è stato  emanato  di  un  potere    &#13;
 legislativo  delegato  e  che  è  stato  emesso dal Presidente della    &#13;
 Repubblica, previa delibera del  Consiglio  dei  ministri,  udito  il    &#13;
 parere del Consiglio di Stato;                                           &#13;
      che,  tenuto  conto  della  posizione  assunta in simili casi da    &#13;
 questa Corte (v. da ultimo le ordinanze n. 17 del 1985;  n.  176  del    &#13;
 1984;  n.  11  del  1984; n. 10 del 1984; n. 334 del 1983; n. 179 del    &#13;
 1983)  la  questione  di  legittimità  costituzionale,   in   quanto    &#13;
 concernente un atto non avente forza di legge, non è suscettibile di    &#13;
 formare oggetto del giudizio di legittimità costituzionale;             &#13;
    Visti  gli artt. 26, comma 2, legge 11 marzo 1953, n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 dichiara   la   manifesta   inammissibilità   della   questione   di    &#13;
 legittimità costituzionale dell'art.  5,  comma  quarto,  d.P.R.  21    &#13;
 agosto  1971,  n.  1275  (Regolamento  per l'esecuzione della legge 2    &#13;
 aprile  1968,  n.  475  recante   norme   concernenti   il   servizio    &#13;
 farmaceutico)  sollevata, in riferimento agli artt. 3 e 97 Cost., dal    &#13;
 Tribunale  Amministrativo  Regionale  per  la  Lombardia  -   sezione    &#13;
 staccata di Brescia - con l'ordinanza in epigrafe indicata.              &#13;
    Così  deciso  in  Roma  in  camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 19 dicembre 1986.       &#13;
                     Il Presidente: LA PERGOLA                            &#13;
                     Il redattore: BALDASSARRE                            &#13;
    Depositata in cancelleria il 31 dicembre 1986.                        &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
