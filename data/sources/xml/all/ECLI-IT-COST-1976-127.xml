<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>127</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:127</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Angelo de Marco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/05/1976</data_decisione>
    <data_deposito>20/05/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE &#13;
 STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale della legge approvata  &#13;
 dal Consiglio regionale del Molise il 5 marzo 1975 e riapprovata il  23  &#13;
 aprile 1975, recante "Concessione di un assegno ai coltivatori diretti,  &#13;
 mezzadri  e  coloni  per  il  periodo di inabilità temporanea assoluta  &#13;
 dovuta ad infortunio sul lavoro o derivante da malattia professionale e  &#13;
 ai lavoratori autonomi per il periodo di ricovero ospedaliero, a titolo  &#13;
 di contributo assistenziale per le loro famiglie", promosso con ricorso  &#13;
 del Presidente del Consiglio dei  ministri,  notificato  il  12  maggio  &#13;
 1975,  depositato in cancelleria il 17 successivo ed iscritto al n.  12  &#13;
 del registro ricorsi 1975.                                               &#13;
     Udito nell'udienza pubblica del 7 aprile 1976 il  Giudice  relatore  &#13;
 Angelo De Marco;                                                         &#13;
     udito  il sostituto avvocato generale dello Stato Giorgio Azzariti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il Consiglio regionale del Mouse, nella seduta del 23 aprile  1975,  &#13;
 riapprovava  nel  medesimo testo, già rinviato dal Governo, un disegno  &#13;
 di legge con il quale si concede ai  coltivatori  diretti,  mezzadri  e  &#13;
 coloni  un  assegno  giornaliero  di  lire 2.000, per un massimo di 180  &#13;
 giorni, per inabilità temporanea assoluta derivante da  infortuni  sul  &#13;
 lavoro o da malattia  professionale, purché essi non siano titolari di  &#13;
 un reddito superiore a lire 1.200.000 annue.                             &#13;
     Avverso  tale  disegno  di  legge  la  Presidenza del Consiglio dei  &#13;
 ministri, debitamente autorizzata, ha proposto ricorso a questa  Corte,  &#13;
 chiedendo che ne venga dichiarata l'illegittimità costituzionale per i  &#13;
 seguenti motivi                                                          &#13;
     1)   violazione  dell'art.  117  in  relazione  all'art.  38  della  &#13;
 Costituzione, in quanto l'assegno accordato  s'inquadra  nella  materia  &#13;
 delle  assicurazioni  sociali  per  invalidità  o  infortunio, che non  &#13;
 rientra  fra  quelle  attribuite  alle  Regioni  a  statuto   ordinario  &#13;
 dall'art.  117 della Costituzione, sibbene, a norma del penultimo comma  &#13;
 dell'art.  38  della  Costituzione  stessa  "agli  organi  ed  istituti  &#13;
 predisposti o integrati dallo Stato".                                    &#13;
     D'altra  parte  il limite di reddito non superiore a lire 1.200.000  &#13;
 annue, non può valere a far rientrare  il  concesso  contributo  nella  &#13;
 materia  dell'assistenza  e  beneficenza,  per  la  quale la Regione è  &#13;
 competente, dato che in una economia prevalentemente e  sostanzialmente  &#13;
 povera,  qual  è quella del Molise, come ammette la stessa Regione, un  &#13;
 reddito fino a lire 1.200.000 annue non può significare uno  stato  di  &#13;
 indigenza.                                                               &#13;
     2)  Violazione dell'art. 81 della Costituzione, in quanto manca una  &#13;
 valida copertura per gli oneri derivanti nell'esercizio in corso ed  in  &#13;
 quelli  successivi,  non  potendosi  ritenere  tale  la  previsione  di  &#13;
 utilizzazione delle disponibilità del fondo di cui  all'art,  9  della  &#13;
 legge  finanziaria  regionale  n.  281  del  1970  che appare meramente  &#13;
 fittizia.                                                                &#13;
     Dopo gli adempimenti di legge, il ricorso viene ora alla cognizione  &#13;
 della Corte, sulla base delle conclusioni della sola  Avvocatura  dello  &#13;
 Stato, non essendovi stata costituzione da parte della Regione Molise.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il ricorso è fondato.                                               &#13;
     Come  già  questa Corte ha affermato, dichiarando illegittima, con  &#13;
 la recente sentenza n. 92 del 21 aprile 1976, la  legge  della  Regione  &#13;
 Abruzzo,  approvata  il  5  marzo 1975 e riapprovata il 23 aprile 1975,  &#13;
 recante "Concessione di un assegno ai coltivatori diretti,  mezzadri  e  &#13;
 coloni,  per  il  periodo  di  inabilità temporanea assoluta dovuta ad  &#13;
 infortunio sul lavoro  o  derivante  da  malattia  professionale  e  ai  &#13;
 lavoratori autonomi per il periodo di ricovero ospedaliero, a titolo di  &#13;
 contributo  assistenziale per le loro famiglie" un siffatto assegno non  &#13;
 può  che  inquadrarsi  nella  materia  dell'assistenza  sociale,   non  &#13;
 contemplata dall'art. 117 della Costituzione fra quelle trasferite alla  &#13;
 competenza legislativa delle Regioni a statuto ordinario.                &#13;
     Ovviamente altrettanto deve dirsi per l'assegno, del tutto analogo,  &#13;
 contemplato  dalla  legge  della  Regione  Molise, oggetto del presente  &#13;
 giudizio, anche in conformità  con  i  criteri  differenziali  tra  le  &#13;
 materie  "beneficenza" ed "assistenza sociale" enunciati nella sentenza  &#13;
 di questa Corte n. 139 del 1972 e riconfermati con la sentenza di  pari  &#13;
 data n. 126.                                                             &#13;
     Conseguentemente,  senza  che occorra passare all'esame del secondo  &#13;
 motivo, manifestamente assorbito, il ricorso deve essere accolto.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale della legge della  Regione  &#13;
 Molise  approvata  il  5  marzo  1975  e riapprovata il 23 aprile 1975,  &#13;
 recante: "Concessione di un assegno ai coltivatori diretti, mezzadri  e  &#13;
 coloni  per  il  periodo  di  inabilità  temporanea assoluta dovuta ad  &#13;
 infortunio sul lavoro  o  derivante  da  malattia  professionale  e  ai  &#13;
 lavoratori autonomi per il periodo di ricovero ospedaliero, a titolo di  &#13;
 contributo   assistenziale   per   le  loro  famiglie",  impugnata  dal  &#13;
 Presidente del  Consiglio  dei  ministri  con  il  ricorso  di  cui  in  &#13;
 epigrafe.                                                                &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 7 maggio 1976.                                &#13;
                                   F.to:  PAOLO  ROSSI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE ROCCHETTI  -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  -  GUIDO  ASTUTI - MICHELE  &#13;
                                   ROSSANO - ANTONINO DE STEFANO.         &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
