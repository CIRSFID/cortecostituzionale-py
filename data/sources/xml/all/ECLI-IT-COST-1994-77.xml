<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>77</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:77</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/02/1994</data_decisione>
    <data_deposito>10/03/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi di legittimità costituzionale degli artt. 392 e 393 del    &#13;
 codice di procedura penale, promossi con n. 2 ordinanze emesse  il  2    &#13;
 aprile  ed  il 18 giugno 1993 dal Giudice per le indagini preliminari    &#13;
 presso il Tribunale di Prato nei  procedimenti  penali  a  carico  di    &#13;
 Niccoli  Stefano  ed altri e Sparacino Giuseppe ed altri, iscritte ai    &#13;
 nn. 414 e 478 del registro ordinanze 1993 e pubblicate nelle Gazzetta    &#13;
 Ufficiale della  Repubblica  nn.  35  e  37,  prima  serie  speciale,    &#13;
 dell'anno 1993;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 12  gennaio  1994  il  Giudice    &#13;
 relatore Ugo Spagnoli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel corso di due procedimenti penali dei quali gli imputati    &#13;
 avevano avuto notizia solo con la notifica della richiesta di  rinvio    &#13;
 a  giudizio,  in  quanto nel corso delle indagini preliminari non era    &#13;
 stata loro inviata  alcuna  informazione  di  garanzia,  i  difensori    &#13;
 chiedevano,   all'udienza   preliminare,  l'espletamento  di  perizie    &#13;
 mediante incidente probatorio.                                           &#13;
    Ritenendo  tale  richesta  non  accoglibile,  il  Giudice  per  le    &#13;
 indagini  preliminari  presso il Tribunale di Prato ha sollevato, con    &#13;
 due ordinanze di tenore analogo del 2 aprile e 18 giugno  1993  (r.o.    &#13;
 nn.  414  e  478/1993),  una questione di legittimità costituzionale    &#13;
 degli artt. 392 e 393 del codice di procedura penale, assumendone  il    &#13;
 contrasto con gli artt. 3 e 24 Cost.                                     &#13;
    In  mancanza  di  una  informazione  di  garanzia  anteriore  alla    &#13;
 richiesta di rinvio a giudizio - osserva sinteticamente il giudice  a    &#13;
 quo   -  alla  difesa  non  è  consentito  di  chiedere  l'incidente    &#13;
 probatorio  né   nelle   indagini   preliminari   né   nell'udienza    &#13;
 preliminare,  sicché  l'indagato  non sarebbe posto in condizioni di    &#13;
 uguaglianza con la  pubblica  accusa  né  sarebbe  tutelato  il  suo    &#13;
 diritto di difesa.                                                       &#13;
    2.  -  Il  Presidente  del Consiglio dei ministri, rappresentato e    &#13;
 difeso dall'Avvocatura  Generale  dello  Stato,  è  intervenuto  nei    &#13;
 predetti  giudizi  con due memorie di contenuto parzialmente analogo,    &#13;
 chiedendo che la questione sia dichiarata inammissibile o infondata.     &#13;
    Sotto il primo profilo,  l'Avvocatura  osserva  che  la  doglianza    &#13;
 andrebbe  rivolta  alle  norme di cui agli artt. 416 e ss. cod. proc.    &#13;
 pen. e non a quelle impugnate, delle quali  il  giudice  dell'udienza    &#13;
 preliminare  non  deve fare applicazione; e che, comunque, la censura    &#13;
 proposta  riguarderebbe  semmai, la disposizione (art. 369 cod. proc.    &#13;
 pen.  )  che  disciplina  l'informazione  di  garanzia,   in   quanto    &#13;
 l'inconveniente  lamentato  potrebbe  essere  adeguatamente eliminato    &#13;
 soltanto ancorando l'obbligo di trasmissione dell'informazione ad una    &#13;
 fase antecedente a quella considerata dal legislatore.                   &#13;
    La questione sarebbe, inoltre, irrilevante perché  il  giudice  a    &#13;
 quo,  da  un  lato non ha dato conto della ricorrenza dei requisiti -    &#13;
 modificazione non evitabile della cosa  e  particolare  durata  della    &#13;
 perizia  (art.  392,  commi  1,  lettera f) e 2) - cui è subordinato    &#13;
 l'espletamento di questa mediante incidente  probatorio;  dall'altro,    &#13;
 non  ha  considerato  la  possibilità  di far refluire del materiale    &#13;
 conoscitivo nell'udienza preliminare (art. 422 cod. proc. pen.).         &#13;
    Nel merito,  comunque,  la  questione  è,  secondo  l'Avvocatura,    &#13;
 infondata.  Posto, infatti, che la ratio dell'incidente probatorio è    &#13;
 di evitare il pericolo di  dispersione  delle  prove  a  causa  della    &#13;
 durata  delle  indagini  preliminari, esso non ricorre più quando si    &#13;
 sia pervenuti all'udienza preliminare, dato che restano solo i  tempi    &#13;
 brevi  della  fissazione  dell'udienza dibattimentale, che può anche    &#13;
 essere anticipata per giustificati motivi (art. 455 cod.  proc.  pen.    &#13;
 ).<diritto>Considerato in diritto</diritto>1.  -  Con  le  due  ordinanze,  di  analogo  tenore,  indicate in    &#13;
 epigrafe, il Giudice per le indagini preliminari presso il  Tribunale    &#13;
 di  Prato  dubita della legittimità costituzionale degli artt. 392 e    &#13;
 393 del codice di procedura penale, nella parte in  cui,  stabilendo,    &#13;
 rispettivamente,  che  l'incidente  probatorio  può essere richiesto    &#13;
 "nel corso delle indagini preliminari" ed "entro i  termini"  per  la    &#13;
 loro  conclusione,  impedisce  che  esso  (nei  casi  di  specie, una    &#13;
 perizia) possa essere espletato nella fase dell'udienza  preliminare:    &#13;
 e  ciò,  particolarmente,  con riferimento al caso dell'indagato che    &#13;
 prima di tale udienza abbia avuto notizia del procedimento  penale  a    &#13;
 suo   carico   mediante  comunicazione  di  garanzia  anteriore  alla    &#13;
 richiesta di rinvio a giudizio.                                          &#13;
    Ciò darebbe luogo, ad avviso del rimettente, a contrasto con  gli    &#13;
 artt.  3  e  24  Cost.,  dato  che ne deriverebbe una menomazione del    &#13;
 diritto di difesa dell'indagato e un deteriore  trattamento  rispetto    &#13;
 alla   pubblica   accusa   nell'attività   probatoria   utilizzabile    &#13;
 nell'udienza preliminare.                                                &#13;
    2. - Le eccezioni di  inammissibilità  sollevate  dall'Avvocatura    &#13;
 non possono essere accolte.                                              &#13;
    La  preclusione  all'espletamento  dell'incidente probatorio nella    &#13;
 fase dell'udienza  preliminare  deriva  dallo  sbarramento  temporale    &#13;
 posto  dalla  norma  impugnata,  e  non  da  quelle  che disciplinano    &#13;
 l'udienza preliminare (artt. 416  e  ss.)  ovvero  l'informazione  di    &#13;
 garanzia  (art.  369).  E  poiché le perizie richieste nei giudizi a    &#13;
 quibus hanno oggetto tale da far presumere che il  loro  espletamento    &#13;
 possa  richiedere  più  di sessanta giorni, la circostanza che nelle    &#13;
 ordinanze di rimessione non sia stata esplicitata la  ricorrenza  del    &#13;
 requisito di cui all'art. 392, comma 2 non può tradursi in motivo di    &#13;
 irrilevanza delle questioni con esse sollevate.                          &#13;
    3. - Nel merito, la questione è fondata.                             &#13;
    Nel   vigente   sistema   processuale,  l'istituto  dell'incidente    &#13;
 probatorio  è  preordinato  a  consentire  alle   parti   principali    &#13;
 l'assunzione  delle  prove non rinviabili al dibattimento (art. 2, n.    &#13;
 40 della legge delega n. 81 del  1987),  e  cioè  di  quelle  che  -    &#13;
 secondo  l'elencazione dell'art. 392 cod. proc. pen. - si prevede che    &#13;
 non siano differibili al dibattimento per le condizioni della persona    &#13;
 da esaminare o perché soggette a perdita di genuinità  (lettere  da    &#13;
 a)  a  e)),  o  perché  il loro oggetto è inevitabilmente esposto a    &#13;
 modificazione (lettera f)), o perché ricorrono  particolari  ragioni    &#13;
 di   urgenza   (lettera   g))  o,  infine,  perché  il  loro  rinvio    &#13;
 pregiudicherebbe la concentrazione del dibattimento (comma 2).           &#13;
    Ove tali  circostanze  ricorrano,  l'anticipata  assunzione  della    &#13;
 prova  si  appalesa  indispensabile per l'acquisizione al processo di    &#13;
 elementi - in tesi -  necessari  all'accertamento  dei  fatti  e  per    &#13;
 garantire  l'effettività  del  diritto  delle  parti alla prova, che    &#13;
 sarebbe altrimenti irrimediabilmente perduta.                            &#13;
    Tale esigenza concerne il diritto alla prova  tanto  del  pubblico    &#13;
 ministero  che dell'imputato e prescinde, per quest'ultimo, dal fatto    &#13;
 che  egli  abbia  avuto  o  meno  la  possibilità  -  attraverso  la    &#13;
 comunicazione  giudiziaria - di chiedere l'incidente probatorio nella    &#13;
 fase delle indagini preliminari, dato che le evenienze  in  questione    &#13;
 (si pensi a quella di cui all'art. 392, lettera a)) possono insorgere    &#13;
 per la prima volta dopo la richiesta di rinvio a giudizio.               &#13;
    Di  ciò,  del  resto,  il  legislatore si è mostrato consapevole    &#13;
 disponendo che,  nei  casi  previsti  dall'art.  392,  le  prove  non    &#13;
 rinviabili  possano  essere  assunte  dal  presidente del collegio, a    &#13;
 richiesta di parte, nella fase degli atti preliminari al dibattimento    &#13;
 (art. 467 cod. proc. pen.).                                              &#13;
    Tale previsione è  già  di  per  sé  sufficiente  a  dimostrare    &#13;
 l'infondatezza della tesi - avanzata dall'Avvocatura - secondo cui la    &#13;
 preclusione   dell'incidente   probatorio   nella  fase  dell'udienza    &#13;
 preliminare sarebbe giustificata dalla prossimità del  dibattimento:    &#13;
 tesi  che  peraltro  -  anche  a  prescindere dalle conseguenze della    &#13;
 soppressione della regola dell'"evidenza" di cui  all'art.  425  cod.    &#13;
 proc.  pen.  (art.  1  legge 8 aprile 1993, n. 105) - è contraddetta    &#13;
 dalla possibile dilatazione di tale udienza, ai sensi dell'art. 422.     &#13;
    Sotto    il    profilo    sistematico,     poi,     l'interruzione    &#13;
 nell'acquisibilità  di  prove  non rinviabili appare contraddittoria    &#13;
 con la continuità che il legislatore ha assicurato all'attività  di    &#13;
 indagine prevedendo che essa possa proseguire anche dopo la richiesta    &#13;
 di  rinvio  a  giudizio  (art.  419,  comma  3) e dopo il decreto che    &#13;
 dispone il giudizio (art. 430), ben  potendo  darsi  che  per  taluno    &#13;
 degli  elementi  in tal modo acquisiti insorgano le situazioni di non    &#13;
 differibilità della prova previste dall'art. 392.                       &#13;
    La preclusione  all'esperimento  dell'incidente  probatorio  nella    &#13;
 fase  dell'udienza  preliminare  si  rivela,  pertanto, priva di ogni    &#13;
 ragionevole giustificazione e lesiva del  diritto  delle  parti  alla    &#13;
 prova  e,  quindi, dei diritti di azione e di difesa. Di conseguenza,    &#13;
 le   norme   impugnate   vanno,   per   questa   parte,    dichiarate    &#13;
 costituzionalmente illegittime.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi, dichiara l'illegittimità costituzionale degli    &#13;
 artt. 392 e 393 del codice di procedura penale, nella  parte  in  cui    &#13;
 non   consentono   che,   nei  casi  previsti  dalla  prima  di  tali    &#13;
 disposizioni,  l'incidente  probatorio  possa  essere  richiesto   ed    &#13;
 eseguito anche nella fase dell'udienza preliminare.                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 febbraio 1994.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: SPAGNOLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 marzo 1994.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
