<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>280</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:280</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1998</data_decisione>
    <data_deposito>17/07/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 1753 del codice    &#13;
 civile  promosso con ordinanza emessa l'11 aprile 1997 dal pretore di    &#13;
 Bassano del Grappa sul ricorso proposto da Stocco  Armando  ed  altro    &#13;
 contro l'agenzia generale SAI di Vidale Giovanni Battista e Minicelli    &#13;
 Giorgio  S.a.s.  ed  altro, iscritta al n. 508 del registro ordinanze    &#13;
 1997 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  36,    &#13;
 prima serie speciale, dell'anno 1997;                                    &#13;
   Udito  nella  camera  di  consiglio  del  6  maggio 1998 il giudice    &#13;
 relatore Massimo Vari;                                                   &#13;
   Ritenuto che il pretore di Bassano del Grappa con ordinanza  emessa    &#13;
 l'11  aprile  1997 ed iscritta al r.o. n. 508 del 1997 - nel corso di    &#13;
 un giudizio civile promosso da due  subagenti  di  assicurazione  per    &#13;
 ottenere  dal preponente il pagamento dell'indennità sostitutiva del    &#13;
 preavviso e dell'indennità di cessazione del rapporto - ha sollevato    &#13;
 questione di legittimità costituzionale dell'art.  1753  del  codice    &#13;
 civile,  per  contrasto con l'art. 3 della Costituzione, "nella parte    &#13;
 in cui consente alle norme collettive, agli usi  o  alle  pattuizioni    &#13;
 individuali  di  derogare  all'art.  1751 codice civile a sfavore del    &#13;
 subagente di assicurazione";                                             &#13;
     che, ad avviso del rimettente, la disposizione denunciata,  nello    &#13;
 stabilire  che  le  norme  dettate  in  materia di agenzia dal codice    &#13;
 civile si applicano anche agli agenti di  assicurazione,  "in  quanto    &#13;
 non  siano  derogate  dalle norme corporative o dagli usi e in quanto    &#13;
 siano compatibili con la natura dell'attività assicurativa", avrebbe    &#13;
 invertito "l'ordine di precedenza stabilito dagli  artt.  1,  7  e  8    &#13;
 delle   preleggi",   ponendo   in   posizione  subordinata  le  norme    &#13;
 codicistiche e, conseguentemente, "declassando" l'art. 1751 cod. civ.    &#13;
 da norma imperativa a dispositiva, tanto da consentirne la deroga  da    &#13;
 parte   di   disposizioni   collettive,   di  usi  e  di  pattuizioni    &#13;
 individuali;                                                             &#13;
     che, secondo il giudice a quo,  essendo  l'art.  1753  cod.  civ.    &#13;
 direttamente  applicabile  al  rapporto  di subagenzia assicurativa e    &#13;
 venendo in rilievo anche per tale rapporto "la ratio  sottostante  al    &#13;
 riconoscimento  legislativo  di  un'indennità  di  scioglimento  del    &#13;
 contratto", ne deriverebbe, proprio in virtù  dei  rilevati  effetti    &#13;
 discriminatori  della  disposizione  denunciata,  una  ingiustificata    &#13;
 disparità di trattamento del subagente di assicurazione rispetto  ad    &#13;
 ogni altro agente o subagente;                                           &#13;
     che,  a  suo  avviso,  tale  disparità di trattamento sarebbe da    &#13;
 "ritenersi giustificata" nei confronti dell'agente di  assicurazione,    &#13;
 "figura  che  presenta  peculiarità  che  la differenziano da quella    &#13;
 degli agenti operanti in diversi settori", mentre,  "non  altrettanto    &#13;
 può invece dirsi per i subagenti assicurativi".                         &#13;
   Considerato  che  il  giudice  a  quo nell'assumere, da un lato, la    &#13;
 applicabilità del denunciato art. 1753 cod. civ. anche ai  subagenti    &#13;
 di assicurazione nega, poi, contraddittoriamente, la sussistenza, per    &#13;
 questi   ultimi,   di   quella  eadem  ratio  che  dovrebbe,  invece,    &#13;
 sorreggerne l'affermata applicabilità;                                  &#13;
     che, in questi termini, quella prospettata dal giudice a  quo  si    &#13;
 pone,  in  realtà,  come mera questione interpretativa, da risolvere    &#13;
 sulla base dei canoni ermeneutici generali,  e  che,  quindi,  rimane    &#13;
 estranea  al  sindacato  affidato  alla Corte, alla quale non spetta,    &#13;
 infatti, valutare l'incertezza  in  ordine  all'applicabilità  delle    &#13;
 norme, bensì  eliminare le norme eventualmente viziate;                 &#13;
     che,  quindi,  la  questione  di  costituzionalità sollevata dal    &#13;
 pretore di Bassano del Grappa è manifestamente inammissibile.           &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità  costituzionale  dell'art.  1753  del   codice   civile,    &#13;
 sollevata,  in riferimento all'art. 3 della Costituzione, dal pretore    &#13;
 di Bassano del Grappa con l'ordinanza in epigrafe indicata.              &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 7 luglio 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                          Il redattore: Vari                              &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 17 luglio 1998.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
