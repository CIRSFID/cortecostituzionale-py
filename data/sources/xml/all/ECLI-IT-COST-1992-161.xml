<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>161</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:161</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/03/1992</data_decisione>
    <data_deposito>02/04/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 566,  quinto  e    &#13;
 sesto  comma,  del  codice di procedura penale promosso con ordinanza    &#13;
 emessa il 10 luglio 1991 dal Pretore  di  Cagliari  nel  procedimento    &#13;
 penale  a  carico  di Gianfranco Pedditzi ed altri iscritta al n. 679    &#13;
 del registro ordinanze 1991 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 45, prima serie speciale, dell'anno 1991;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di  consiglio  del  4  marzo  1992  il  Giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
    Ritenuto  che  nel  procedimento  penale  a  carico  di Gianfranco    &#13;
 Pedditzi ed altri, imputati del delitto di cui agli artt.  110,  624,    &#13;
 625,  nn.  2,  5  e  7 del codice penale, il Pretore di Cagliari, con    &#13;
 ordinanza del 10 luglio 1991 (R.O. n. 679 del 1991), ha  sollevato  -    &#13;
 in  riferimento  all'art.  25,  primo  comma,  della  Costituzione  -    &#13;
 questione di legittimità costituzionale nei confronti dell'art. 566,    &#13;
 quinto e sesto comma, del codice di procedura penale "nella parte  in    &#13;
 cui  non  impone  al  Pretore  adito per la celebrazione del giudizio    &#13;
 direttissimo tipico, che non abbia emesso provvedimenti  restrittivi,    &#13;
 di  restituire  gli atti al pubblico ministero perché proceda con le    &#13;
 forme ordinarie";                                                        &#13;
      che - secondo il giudice a quo - il pubblico ministero, ai sensi    &#13;
 dell'art. 121 del decreto legislativo 28 luglio 1989, n. 271, recante    &#13;
 "Norme  di  attuazione,  di coordinamento e transitorie del codice di    &#13;
 procedura penale", ha l'obbligo di adire il pretore designato per  la    &#13;
 celebrazione dei giudizi direttissimi solo ove ricorra il presupposto    &#13;
 dell'applicabilità di misure coercitive, mentre nessuna norma prevede    &#13;
 che la mancata applicazione di misure coercitive da parte del giudice    &#13;
 adito   con  le  forme  del  procedimento  direttissimo  comporti  la    &#13;
 trasformazione del rito  con  restituzione  degli  atti  al  pubblico    &#13;
 ministero;                                                               &#13;
      che,  in  base  alle  disposizioni  contenute nei commi quinto e    &#13;
 sesto  dell'art.  566  del  codice  di  procedura  penale,   per   la    &#13;
 celebrazione del giudizio direttissimo è necessaria e sufficiente la    &#13;
 sola  convalida  dell'arresto,  con  la  conseguenza  che il giudizio    &#13;
 speciale, una volta instaurato, deve essere proseguito comunque,  sia    &#13;
 che  il  giudice  accerti l'inapplicabilità di misure coercitive sia    &#13;
 che la pubblica accusa non chieda, come accade talvolta  in  pratica,    &#13;
 l'adozione di misure cautelari;                                          &#13;
      che  - sempre ad avviso del giudice remittente - le disposizioni    &#13;
 dettate dai  commi  quinto  e  sesto  dell'art.  566  del  codice  di    &#13;
 procedura,  così interpretate, sarebbero in contrasto con l'art. 25,    &#13;
 primo   comma,    della    Costituzione    perché    comporterebbero    &#13;
 l'impossibilità per il giudice di controllare, sotto il profilo della    &#13;
 scelta  del rito, le valutazioni del pubblico ministero, determinando    &#13;
 "l'indiscriminata sottrazione dell'imputato sia al suo giusto giudice    &#13;
 del procedimento incidentale  di  convalida  ...  sia  a  quello  del    &#13;
 dibattimento";                                                           &#13;
      che  nel  giudizio  dinanzi alla Corte ha spiegato intervento il    &#13;
 Presidente  del  Consiglio  dei  ministri,  rappresentato  e   difeso    &#13;
 dall'Avvocatura  generale dello Stato, chiedendo che la questione sia    &#13;
 dichiarata infondata.                                                    &#13;
    Considerato che, secondo il giudice  remittente,  le  disposizioni    &#13;
 impugnate  violerebbero  l'art.  25, primo comma, della Costituzione,    &#13;
 perché rimetterebbero la scelta del rito a valutazioni del  pubblico    &#13;
 ministero  non controllabili da parte del giudice, determinando quale    &#13;
 conseguenza la possibilità di una sottrazione dell'imputato  al  suo    &#13;
 giudice naturale;                                                        &#13;
      che,  in  base  all'art.  566,  sesto comma, del nuovo codice di    &#13;
 procedura penale, l'instaurazione  del  rito  direttissimo  è  stata    &#13;
 condizionata alla presenza di un provvedimento giudiziale, cioè alla    &#13;
 convalida   dell'arresto   da  parte  del  giudice,  il  che  esclude    &#13;
 l'esistenza di una scelta insindacabile  del  pubblico  ministero  in    &#13;
 ordine al rito ed al giudice del dibattimento;                           &#13;
      che,  secondo  la costante giurisprudenza di questa Corte, si ha    &#13;
 violazione del principio del giudice naturale, sancito dall'art.  25,    &#13;
 primo  comma,  della Costituzione quando il giudice venga designato a    &#13;
 posteriori  in  relazione   ad   una   determinata   controversia   o    &#13;
 direttamente  dal  legislatore  in  via  di  eccezione singolare alle    &#13;
 regole generali ovvero attraverso atti di altri soggetti ai quali  la    &#13;
 legge  attribuisca  tale  potere  al  di là dei limiti imposti dalla    &#13;
 riserva di legge (sent. n. 446 del 1990; ord. n. 902 del 1988;  sent.    &#13;
 n. 127 del 1979);                                                        &#13;
      che,  nella  fattispecie  condotta  all'esame  della Corte, tale    &#13;
 violazione  non  sussiste,  dal  momento   che   risulta   rispettata    &#13;
 l'esigenza  della  precostituzione  del  giudice,  quale  garanzia di    &#13;
 imparzialità dell'organo giudiziario;                                   &#13;
      che   pertanto   la   questione   va  dichiarata  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 566, quinto e sesto  comma,  del  codice  di    &#13;
 procedura   penale   sollevata,  in  riferimento  all'art.  25  della    &#13;
 Costituzione, dal Pretore di Cagliari  con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 marzo 1992.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: CHELI                             &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 2 aprile 1992.                           &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
