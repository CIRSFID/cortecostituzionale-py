<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1964</anno_pronuncia>
    <numero_pronuncia>20</numero_pronuncia>
    <ecli>ECLI:IT:COST:1964:20</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Nicola Jaeger</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/02/1964</data_decisione>
    <data_deposito>14/03/1964</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  12,  primo  &#13;
 comma, del D.P.R. 26 aprile 1957, n. 818, promosso con ordinanza emessa  &#13;
 il  28  dicembre  1962  dal Tribunale di Matera nel procedimento civile  &#13;
 vertente tra Di Tursi Filomena e l'Istituto nazionale della  previdenza  &#13;
 sociale,  iscritta  al  n. 126 del Registro ordinanze 1963 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica, n. 187 del 13 luglio 1963.    &#13;
     Visto l'atto di costituzione in  giudizio  dell'Istituto  nazionale  &#13;
 della previdenza sociale;                                                &#13;
     udita  nell'udienza pubblica dell'11 dicembre 1963 la relazione del  &#13;
 Giudice Nicola Jaeger;                                                   &#13;
     udito  l'avv.  Guido  Nardone,  per  l'Istituto   nazionale   della  &#13;
 previdenza sociale.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  atto  di  citazione  notificato il 30 dicembre 1959 la signora  &#13;
 Filomena Di Tursi conveniva davanti al Tribunale di  Matera  l'Istituto  &#13;
 nazionale  della  previdenza  sociale,  esponendo  che  questo  si  era  &#13;
 rifiutato di accreditarle i contributi figurativi  per  il  periodo  di  &#13;
 interruzione  del lavoro determinato da gravidanza e puerperio e durato  &#13;
 dal 6 novembre 1956 al 25 febbraio 1957, e  chiedendo  che  esso  fosse  &#13;
 dichiarato tenuto a provvedere all'accreditamento.                       &#13;
     L'Istituto,  costituitosi  in  giudizio,  si opponeva alla domanda,  &#13;
 osservando  che  il  provvedimento  definitivo  di  rigetto  era  stato  &#13;
 motivato dal fatto che l'attrice risultava obbligatoriamente assicurata  &#13;
 per  il periodo, per il quale era chiesta l'attribuzione dei contributi  &#13;
 figurativi, e che pertanto questi non potevano esserle attribuiti.       &#13;
     Rispondeva la difesa dell'attrice insistendo nella tesi del diritto  &#13;
 di questa a che si aggiungesse al numero dei contributi  corrispondenti  &#13;
 alle   giornate   di   lavoro   attribuite  il  numero  dei  contributi  &#13;
 corrispondenti al periodo  di  gravidanza  e  di  puerperio,  affinché  &#13;
 l'astensione  involontaria dal lavoro non si risolvesse in un danno per  &#13;
 l'assicurata. Nell'ipotesi  che  non  fosse  accolta  l'interpretazione  &#13;
 proposta dell'art. 10, primo comma, del decreto presidenziale 26 aprile  &#13;
 1957,  n.    818, in relazione all'art. 56, n. 3, del decreto 4 ottobre  &#13;
 1935,  n.  1827,  essa  chiedeva  che  il  Tribunale  dichiarasse   non  &#13;
 manifestamente infondata la questione di legittimità costituzionale di  &#13;
 quella norma, rimettendo gli atti alla Corte costituzionale.             &#13;
     Il  Tribunale, riconosciuta la rilevanza della questione, osservava  &#13;
 che essa non poteva  essere  dichiarata  manifestamente  infondata,  in  &#13;
 quanto  l'art.  12,  primo  comma,  del decreto presidenziale 26 aprile  &#13;
 1957, interpretato in relazione all'art. 10, primo comma, apportava una  &#13;
 evidente limitazione  del  diritto  all'accreditamento  dei  contributi  &#13;
 fittizi  per  i periodi di interruzione obbligatoria del lavoro durante  &#13;
 lo stato di gravidanza e di puerperio ed  era  quindi  ipotizzabile  un  &#13;
 eccesso di delega in riferimento all'art. 37 della legge 4 aprile 1952,  &#13;
 n.  218,  ed  all'art.  76  della Costituzione.   Pertanto rimetteva la  &#13;
 questione all'esame della Corte costituzionale con ordinanza emessa  il  &#13;
 28  dicembre  1962,  ma  pervenuta  alla  cancelleria  della Corte, con  &#13;
 inescusabile  ritardo,  solo  l'11  giugno  1963,  dopo  essere   stata  &#13;
 notificata  alle  parti  e  al  Presidente del Consiglio dei Ministri e  &#13;
 comunicata ai Presidenti del Senato e della Camera dei  Deputati  il  4  &#13;
 aprile  1963,  e  pubblicata  nella  Gazzetta  Ufficiale, n. 187 del 13  &#13;
 luglio 1963.                                                             &#13;
     Nel  presente  giudizio  si  è  costituito   soltanto   l'Istituto  &#13;
 nazionale  della  previdenza  sociale,  depositando  tempestivamente le  &#13;
 proprie deduzioni, nelle quali esso fa sostanzialmente richiamo  a  due  &#13;
 recenti  sentenze della Corte, n. 4 del 12 febbraio 1963 e n. 78 dell'8  &#13;
 giugno 1963, ed afferma che la questione ora  proposta  trova  già  in  &#13;
 esse  la  più adeguata e corretta soluzione, posto che il dubbio sulla  &#13;
 legittimità della norma denunciata è stato sollevato in relazione  al  &#13;
 contenuto  del  primo  comma  dell'art. 10 del decreto presidenziale 26  &#13;
 aprile 1957, n.  818.                                                    &#13;
     Tali conclusioni sono state  ribadite  dalla  difesa  dell'Istituto  &#13;
 all'udienza di discussione della causa.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il richiamo, che la difesa dell'Istituto nazionale della previdenza  &#13;
 sociale  ha  fatto  nelle  proprie  deduzioni  scritte e ripetuto nella  &#13;
 discussione  orale,  alla  recente  giurisprudenza  della  Corte  nella  &#13;
 materia  che  costituisce  oggetto  del  presente giudizio, è esatto e  &#13;
 rilevante.                                                               &#13;
     La  questione  sottoposta  alla  Corte  dal  Tribunale  di   Matera  &#13;
 nell'ordinanza  del  28  dicembre 1962 concerne infatti la legittimità  &#13;
 costituzionale di disposizioni,  le  quali  escludono  in  taluni  casi  &#13;
 l'accreditamento  dei  contributi  cosiddetti  fittizi per i periodi di  &#13;
 interruzione obbligatoria del lavoro durante lo stato di  gravidanza  e  &#13;
 di  puerperio.  Senonché  il  Tribunale  non  ha  preso  nella  dovuta  &#13;
 considerazione un aspetto essenziale  del  regolamento  della  materia,  &#13;
 che,  se  sottoposto  ad  un  attento  esame,  avrebbe potuto indurlo a  &#13;
 decidere senz'altro il merito  della  controversia  sottoposta  al  suo  &#13;
 giudizio.                                                                &#13;
     I  principi informatori del sistema della pensione di invalidità e  &#13;
 vecchiaia  si  ricollegano,  come  questa   Corte   ha   esplicitamente  &#13;
 riconosciuto già nella sentenza n. 34 del 24 maggio 1960, ad uno stato  &#13;
 di  bisogno  del  lavoratore,  al  quale  la legislazione previdenziale  &#13;
 intende  garantire  il  soddisfacimento  delle  necessità  vitali   al  &#13;
 verificarsi  di quegli eventi, che ne annullino o riducano le capacità  &#13;
 di lavoro e di guadagno. E nel sistema si inserisce  il  beneficio  dei  &#13;
 contributi  figurativi,  in  virtù  del  quale  si  computano  ai fini  &#13;
 assicurativi anche determinati periodi, durante i quali sia venuta meno  &#13;
 la possibilità di versare i contributi a causa di eventi meritevoli di  &#13;
 speciale considerazione, e comunque non imputabili né  al  lavoratore,  &#13;
 né al datore di lavoro.                                                 &#13;
     Questi  eventi - osservava la Corte nella successiva sentenza n. 78  &#13;
 del 25 maggio 1963 -  attengono  tutti  a  situazioni  straordinarie  o  &#13;
 patologiche  della  vita del lavoratore, quali il servizio militare, le  &#13;
 malattie di  una  certa  durata,  la  gravidanza  e  il  puerperio,  la  &#13;
 disoccupazione,  il  cui particolare rilievo le rende degne di speciale  &#13;
 tutela.                                                                  &#13;
     Aggiungeva però, testualmente: "Deriva  logicamente,  e  ciò  non  &#13;
 può  non avere rilevanza sul terreno giuridico, che la concessione dei  &#13;
 contributi figurativi, intesi - come si è  detto  -  ad  eliminare  le  &#13;
 conseguenze  dannose  dei  fatti  accennati riguardo alla realizzazione  &#13;
 della più completa difesa sociale contro l'invalidità e la vecchiaia,  &#13;
 non ha ragione di  permanere  qualora  i  soggetti  che  ne  dovrebbero  &#13;
 beneficiare   si   trovino   ad   avere   assicurato   un   trattamento  &#13;
 previdenziale,  uguale  a  quello  dell'assicurazione  obbligatoria  in  &#13;
 questione,  che  li pone al coperto dalle conseguenze della diminuita o  &#13;
 cessata capacità di guadagno, garantendo un minimo vitale".             &#13;
     Nel settore del lavoro agricolo, in  particolare,  venuta  meno  la  &#13;
 norma  dell'art.  17,  primo comma, del D.P.R.  26 aprile 1957, n. 818,  &#13;
 che poneva un limite al numero dei contributi computabili in favore dei  &#13;
 lavoratori agricoli giornalieri per il conseguimento del  diritto  alla  &#13;
 pensione  di invalidità o di vecchiaia o per i superstiti, per effetto  &#13;
 della sentenza n. 84 dell'8 giugno 1963 di  questa  Corte,  che  ne  ha  &#13;
 dichiarato  la  illegittimità costituzionale, i diritti dei lavoratori  &#13;
 agricoli sono regolati dall'art. 9 del R.D.L. 14 aprile 1939,  n.  636,  &#13;
 modificato  dall'art.  2  della  legge  4  aprile  1952,  n.  218, e si  &#13;
 considerano utili ai fini dei requisiti richiesti per il  conseguimento  &#13;
 della  pensione tanti contributi giornalieri quante sono le giornate di  &#13;
 lavoro attribuite dalla Commissione provinciale di cui all'art.  2  del  &#13;
 R.D.  24 settembre 1940, n. 1949.                                        &#13;
     Si avvera pertanto in siffatti casi proprio la situazione descritta  &#13;
 sopra,  e  prevista dalla citata sentenza n. 78 del 1963, nella quale i  &#13;
 soggetti che dovrebbero beneficiare della assicurazione  conseguono  un  &#13;
 trattamento  previdenziale  uguale a quello previsto, anche nei casi di  &#13;
 forzata assenza dal lavoro;</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  12,  primo  comma,  del  D.P.R.   26 aprile 1957, n. 818, in  &#13;
 relazione all'art. 77 della Costituzione.                                &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 26 febbraio 1964.                             &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
