<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>184</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:184</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/06/1975</data_decisione>
    <data_deposito>08/07/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. FNZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt.  59,  primo  &#13;
 comma,  della  legge  10  agosto 1950, n. 648, e 47, primo comma, della  &#13;
 legge  18  marzo  1968,  n.  313  (Riordinamento   della   legislazione  &#13;
 pensionistica  di  guerra),  promosso con ordinanza emessa il 21 maggio  &#13;
 1973 dalla Corte dei conti - sezione I pensioni di guerra - sul ricorso  &#13;
 di Larino Anna, iscritta  al  n.  41  del  registro  ordinanze  1974  e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 69 del 13 marzo  &#13;
 1974.                                                                    &#13;
     Udito  nella  Camera  di  consiglio  del  24 aprile 1975 il Giudice  &#13;
 relatore Michele Rossano.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con atto in data 1 febbraio 1964 Anna Larino propose  ricorso  alla  &#13;
 Corte  dei  conti avverso il decreto n. 2020554 del 23 agosto 1963, con  &#13;
 il quale il Ministro  del  Tesoro  le  aveva  negato  il  diritto  alla  &#13;
 pensione  privilegiata  -  chiesta  quale  vedova di Domenico Vanacore,  &#13;
 deceduto per fatto di guerra - perché la domanda era stata  presentata  &#13;
 dopo  il  passaggio  alle  seconde  nozze  e, quindi, non sussisteva la  &#13;
 condizione di vedovanza richiesta dagli artt. 55 e 59 legge  10  agosto  &#13;
 1950, n. 648.                                                            &#13;
     La  Corte  dei  conti  -  sez. I giurisdizionale - con ordinanza 21  &#13;
 maggio 1973, ha  ritenuto  rilevante  ai  fini  della  definizione  del  &#13;
 giudizio  e non manifestamente infondata la questione - sollevata dalla  &#13;
 Larino - di legittimità  costituzionale  dell'art.  59,  primo  comma,  &#13;
 legge  10  agosto  1950,  n.  648, e del corrispondente art.  47, primo  &#13;
 comma, legge 18 marzo 1968, n. 313, nella parte in cui stabiliscono che  &#13;
 la vedova che passi ad altre nozze perde la  pensione,  in  riferimento  &#13;
 all'art. 3 Costituzione.                                                 &#13;
     Ha  osservato  che, in base alle norme citate, si ha una disparità  &#13;
 di trattamento tra la vedova ed il vedovo  di  guerra  che  passino  ad  &#13;
 altre  nozze,  in  quanto, mentre per la prima la perdita del diritto a  &#13;
 pensione si verifica per il solo fatto del  matrimonio,  a  prescindere  &#13;
 dall'accertamento  delle  condizioni  economiche del marito, il secondo  &#13;
 decade da tale diritto solo nel caso in cui  contragga  matrimonio  con  &#13;
 donna  che fruisca di reddito assoggettabile all'imposta complementare.  &#13;
 L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n.  69 del  13  &#13;
 marzo 1974.                                                              &#13;
     Poiché  non  vi è stata costituzione di parti, la Corte ha deciso  &#13;
 in camera di consiglio ai sensi dell'art. 26 legge n.  87 del 1953.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La questione, sollevata dalla  Corte  dei  conti,  investe  la  &#13;
 legittimità   costituzionale,   in  riferimento  all'art.    3  Cost.,  &#13;
 dell'art. 59, primo  comma,  legge  10  agosto  1950,  n.  648,  e  del  &#13;
 corrispondente art. 47, primo comma, legge 18 marzo 1968, n. 313, nella  &#13;
 parte  in cui stabiliscono che perde la pensione la vedova che passi ad  &#13;
 altre nozze.                                                             &#13;
     2. Secondo la giurisprudenza della Corte dei conti le nuove  nozze,  &#13;
 per   quanto   riguarda   il   vedovo,  non  possono  essere  prese  in  &#13;
 considerazione in sé, ma solo  in  quanto  influiscono  sulle  di  lui  &#13;
 condizioni  economiche,  come  esplicitamente previsto dal quinto comma  &#13;
 dell'art. 62 legge 18 marzo 1968, n. 313, il quale  stabilisce  che  il  &#13;
 vedovo  che  passa a nuove nozze perde il diritto a pensione se contrae  &#13;
 matrimonio con donna che fruisce di  reddito  in  misura  superiore  ai  &#13;
 limiti previsti dall'art. 20.                                            &#13;
     La   disciplina   differenziata  prevista  per  la  vedova  non  è  &#13;
 giustificata da una  diversità  obiettiva  delle  situazioni  dei  due  &#13;
 coniugi  e  neppure  dalla  situazione conseguente alle eventuali nuove  &#13;
 nozze per ciò che ciascun coniuge, qualunque sia il sesso,  è  tenuto  &#13;
 nei  confronti  dell'altro, agli stessi obblighi sul piano patrimoniale  &#13;
 (sentenza n.   133  del  1970);  e  pertanto  essa  disciplina  risulta  &#13;
 costituzionalmente illegittima.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 59, primo comma,  &#13;
 legge  10  agosto 1950, n. 648, "Riordinamento delle disposizioni sulle  &#13;
 pensioni di guerra" e del corrispondente art. 47, primo comma, legge 18  &#13;
 marzo 1968, n. 313 "Riordinamento della legislazione  pensionistica  di  &#13;
 guerra",  nella  parte  in  cui stabiliscono che la vedova che passi ad  &#13;
 altre nozze perde la pensione per il solo fatto del matrimonio anche se  &#13;
 il  marito  non  fruisce  di  reddito   assoggettabile   alla   imposta  &#13;
 complementare.                                                           &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 giugno 1975.         &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   -   NICOLA  REALE  -  PAOLO  ROSSI  -  &#13;
                                   LEONETTO AMADEI - GIULIO GIONFRIDA  -  &#13;
                                   EDOARDO  VOLTERRA  -  GUIDO  ASTUTI -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere.</dispositivo>
  </pronuncia_testo>
</pronuncia>
