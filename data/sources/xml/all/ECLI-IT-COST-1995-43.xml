<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>43</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:43</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/02/1995</data_decisione>
    <data_deposito>13/02/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco &#13;
 GUIZZI, prof. Cesare MIRABELLI, dott. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  12, terzo    &#13;
 comma, del decreto-legge 10  marzo  1994,  n.  169  (Disposizioni  in    &#13;
 materia  di riutilizzo dei residui derivanti da cicli di produzione o    &#13;
 di  consumo  in  un  processo  produttivo  o  in   un   processo   di    &#13;
 combustione),  promosso  con  ordinanza  emessa il 22 aprile 1994 dal    &#13;
 Pretore di Verona, sezione distaccata di  Legnago,  nel  procedimento    &#13;
 penale  a  carico  di  Luciano Spedo, iscritta al n. 423 del registro    &#13;
 ordinanze 1994 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 29, prima serie speciale, dell'anno 1994;                             &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 25 gennaio 1995 il Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Ritenuto che nel corso di  un  procedimento  penale  a  carico  di    &#13;
 Luciano  Spedo  -  imputato  della  contravvenzione prevista e punita    &#13;
 dall'art. 25, primo comma, del d.P.R. 10 settembre 1982, n. 915,  per    &#13;
 avere effettuato attività di trasporto di inerti destinati ad essere    &#13;
 riutilizzati   nell'ambito  di  un  cantiere  senza  l'autorizzazione    &#13;
 prevista dall'art. 6, lettera d), dello stesso decreto -  il  Pretore    &#13;
 di   Verona,   sezione   distaccata  di  Legnago,  ha  sollevato,  in    &#13;
 riferimento agli artt. 3, 25 e 117 della Costituzione,  questione  di    &#13;
 legittimità  costituzionale  dell'art. 12, terzo comma, del decreto-legge 10 marzo 1994, n. 169 (Disposizioni in  materia  di  riutilizzo    &#13;
 dei  residui  derivanti  da  cicli  di  produzione o di consumo in un    &#13;
 processo produttivo o in un processo di combustione);                    &#13;
      che la norma denunciata stabilisce che non è punibile chiunque,    &#13;
 prima della data di entrata in vigore del decreto-legge, ha  commesso    &#13;
 un  fatto previsto come reato dal d.P.R. n. 915 del 1982 e successive    &#13;
 modifiche ed integrazioni, nell'esercizio  di  attività  qualificate    &#13;
 come  operazioni  di  raccolta e trasporto, stoccaggio, trattamento o    &#13;
 pretrattamento, recupero o riutilizzo di residui, nei modi e nei casi    &#13;
 previsti ed in conformità alle disposizioni del decreto del Ministro    &#13;
 dell'ambiente 26 gennaio 1990 ovvero di norme regionali;                 &#13;
      che, ad avviso  del  giudice  rimettente,  questa  disposizione,    &#13;
 condizionando  il  venir  meno  della  rilevanza  penale  del  fatto,    &#13;
 disposto in via generale dal successivo quarto comma, al rispetto del    &#13;
 decreto ministeriale 26  gennaio  1990  ovvero  di  leggi  regionali,    &#13;
 contrasterebbe  con  vari parametri costituzionali: con l'art. 25, in    &#13;
 quanto il principio di non ultrattività del precetto penale abrogato    &#13;
 esigerebbe che vadano esenti da pena tutte le condotte di smaltimento    &#13;
 dei  rifiuti  che,   secondo   il   decreto-legge   posteriore,   non    &#13;
 costituiscono  più  reato;  con  l'art.  3, giacché diverse sono le    &#13;
 indicazioni contenute nel decreto ministeriale 26 gennaio  1990  (tra    &#13;
 l'altro   annullato   da   questa  Corte  in  sede  di  conflitto  di    &#13;
 attribuzione  con  la  sentenza  n.  512  del  1990)  rispetto   agli    &#13;
 adempimenti prescritti dal decreto-legge; con l'art. 117, perché far    &#13;
 dipendere  la  non punibilità dei comportamenti di smaltimento delle    &#13;
 materie  prime  secondarie   dall'osservanza   di   leggi   regionali    &#13;
 violerebbe  il  principio  di  riserva  di  legge  statale in materia    &#13;
 penale;                                                                  &#13;
      che nel giudizio dinanzi alla Corte è intervenuto il Presidente    &#13;
 del Consiglio dei ministri, rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale  dello Stato, che ha chiesto che la questione sia dichiarata    &#13;
 inammissibile o, comunque, non fondata;                                  &#13;
    Considerato  che  il  decreto-legge  10  marzo 1994, n. 169 non è    &#13;
 stato convertito in legge entro il termine di sessanta  giorni  dalla    &#13;
 sua  pubblicazione,  come  risulta  dal  comunicato  pubblicato nella    &#13;
 Gazzetta Ufficiale della Repubblica n. 107, serie  generale,  del  10    &#13;
 maggio 1994;                                                             &#13;
      che  pertanto,  secondo  la  giurisprudenza  di questa Corte (da    &#13;
 ultimo ordinanze n. 426 e 322 del 1994), la questione di legittimità    &#13;
 costituzionale deve essere dichiarata manifestamente inammissibile;      &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 12, terzo comma, del decreto-legge 10 marzo 1994, n. 169 (Disposizioni in  materia  di  riutilizzo    &#13;
 dei  residui  derivanti  da  cicli  di  produzione o di consumo in un    &#13;
 processo produttivo o in un processo di combustione),  sollevata,  in    &#13;
 riferimento agli artt. 3, 25 e 117 della Costituzione, dal Pretore di    &#13;
 Verona,  sezione  distaccata  di  Legnago, con ordinanza emessa il 22    &#13;
 aprile 1994.                                                             &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 6 febbraio 1995.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 13 febbraio 1995.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
