<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>242</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:242</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/05/1992</data_decisione>
    <data_deposito>03/06/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale, in relazione all'art.  3    &#13;
 della  Costituzione,  dell'art. 69 della legge 27 luglio 1978, n. 392    &#13;
 (Disciplina delle locazioni di immobili  urbani),  promosso  con  due    &#13;
 ordinanze  emesse  il  23  ottobre  1991 dal Tribunale di Spoleto nei    &#13;
 procedimenti civili vertenti tra:                                        &#13;
      1) Massaroni Gaetano, ed altro, e Trincia Enrico;                   &#13;
      2) Massaroni Gaetano e Bianconi Sergio, iscritte rispettivamente    &#13;
 ai nn. 3 e 4 del registro ordinanze 1992 e pubblicate nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  5,  prima serie speciale, dell'anno    &#13;
 1992;                                                                    &#13;
    Udito nella camera di consiglio  del  6  maggio  1992  il  Giudice    &#13;
 relatore Cesare Mirabelli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Tribunale  di  Spoleto  con  due ordinanze emesse il 23    &#13;
 ottobre 1991 nel corso di procedimenti di appello proposti da Gaetano    &#13;
 Massaroni avverso due sentenze con  le  quali  il  Pretore  lo  aveva    &#13;
 condannato  a  corrispondere  ad  Enrico Trincia ed a Sergio Bianconi    &#13;
 somme di danaro a titolo di indennità per avviamento commerciale  in    &#13;
 relazione alla locazione ad uso non abitativo di due immobili siti in    &#13;
 Norcia,  ha sollevato, con riferimento all'art. 3 della Costituzione,    &#13;
 questione  di legittimità costituzionale dell'art. 69 della legge 27    &#13;
 luglio 1978, n. 392, e successive modificazioni, nella parte  in  cui    &#13;
 "pone  a carico del locatore di immobile adibito ad uso non abitativo    &#13;
 l'obbligazione  di  corrispondere  al  conduttore  l'indennità   per    &#13;
 avviamento  commerciale  anche nel caso in cui un provvedimento della    &#13;
 P.A. abbia ordinato  l'evacuazione  totale  dell'immobile,  facendone    &#13;
 così  venir  meno la utilizzabilità economica". Il giudice di primo    &#13;
 grado aveva ritenuto irrilevante la causa di cessazione del  rapporto    &#13;
 di  locazione,  costituita  dalla ordinanza di evacuazione totale del    &#13;
 fabbricato emessa dal sindaco  di  Norcia,  a  seguito  degli  eventi    &#13;
 sismici del 1979.                                                        &#13;
    2. - Le ordinanze di rimessione rilevano che l'art. 34 della legge    &#13;
 n. 392 del 1978, che determina i casi in cui il conduttore ha diritto    &#13;
 alla  indennità per la perdita dell'avviamento commerciale, è stato    &#13;
 dichiarato costituzionalmente illegittimo "nella  parte  in  cui  non    &#13;
 prevede  i  provvedimenti della pubblica Amministrazione tra le cause    &#13;
 di cessazione del rapporto che escludono il  diritto  del  conduttore    &#13;
 alla  indennità per la perdita dell'avviamento" (sentenza n. 542 del    &#13;
 1989). La fattispecie all'esame del giudice a quo concerne  contratti    &#13;
 di  locazione  sorti  anteriormente alla legge n. 392 del 1978, per i    &#13;
 quali  trova  applicazione  l'art.  69  della  stessa  legge.  Questa    &#13;
 disposizione  disciplina  il  diritto  di prelazione in caso di nuova    &#13;
 locazione  e  la  indennità  per  l'avviamento  commerciale,   senza    &#13;
 prevedere  la  esclusione  della indennità stessa quando il rapporto    &#13;
 cessa per provvedimento della pubblica Amministrazione. Il  Tribunale    &#13;
 di   Spoleto   ha   sollevato   pertanto  questione  di  legittimità    &#13;
 costituzionale dell'art. 69 della legge 392 del 1978, che è chiamato    &#13;
 ad applicare, in quanto la indennità di  avviamento  spetterebbe  al    &#13;
 conduttore  in un caso di "mancata prosecuzione della locazione di un    &#13;
 immobile per la  cui  sopravvenuta  inabitabilità  è  stata  emessa    &#13;
 ordinanza di evacuazione totale".                                        &#13;
    Il  giudice rimettente ha ritenuto la questione non manifestamente    &#13;
 infondata in relazione  all'art.  3  della  Costituzione,  in  quanto    &#13;
 l'art. 69 della legge n. 392 del 1978 disciplina i casi di esclusione    &#13;
 del  diritto  del conduttore all'indennità di avviamento in modo del    &#13;
 tutto analogo all'art. 34 della stessa legge, senza che vi sia alcuna    &#13;
 fondata ragione che  giustifichi  una  differenza  di  trattamento  a    &#13;
 seconda   che   l'indennità   concerna  i  contratti  soggetti  alla    &#13;
 disciplina  transitoria  ovvero  quelli  soggetti   alla   disciplina    &#13;
 ordinaria.                                                               &#13;
    3.  -  Le  due  ordinanze  sono  state  pubblicate  sulla Gazzetta    &#13;
 Ufficiale n. 5, prima serie speciale, del 29 gennaio 1992.<diritto>Considerato in diritto</diritto>1. - Il  Tribunale  di  Spoleto,  con  due  ordinanze  di  analogo    &#13;
 contenuto,  ha sollevato questione di legittimità costituzionale, in    &#13;
 relazione all'art. 3 della Costituzione, dell'art. 69 della legge  27    &#13;
 luglio  1978,  n. 392 (Disciplina delle locazioni di immobili urbani)    &#13;
 nella parte in cui pone a carico del locatore di immobile adibito  ad    &#13;
 uso  non  abitativo  l'obbligazione  di  corrispondere  al conduttore    &#13;
 l'indennità di avviamento commerciale  anche  nel  caso  in  cui  un    &#13;
 provvedimento   della   pubblica   Amministrazione   abbia   ordinato    &#13;
 l'evacuazione  totale  dell'immobile,  facendone  così  venir   meno    &#13;
 l'utilizzabilità economica.                                             &#13;
    Il  giudice rimettente sottolinea che la disciplina dettata per il    &#13;
 regime transitorio dall'art. 69 della  legge  n.  392  del  1978,  da    &#13;
 applicare  ai  contratti  di  locazione  stipulati anteriormente alla    &#13;
 entrata in vigore della legge  stessa,  è  del  tutto  analoga  alla    &#13;
 disciplina   prevista   dall'art.   34   per  i  contratti  stipulati    &#13;
 successivamente alla entrata in vigore della legge. Questa disciplina    &#13;
 è stata già dichiarata costituzionalmente illegittima  nella  parte    &#13;
 in  cui  l'art.  34  della  legge n. 392 del 1978 non prevede, tra le    &#13;
 cause di esclusione dell'obbligo  di  corresponsione  dell'indennità    &#13;
 per  l'avviamento  commerciale,  i  provvedimenti amministrativi che,    &#13;
 impedendo la utilizzabilità economica dell'immobile, escludono  ogni    &#13;
 possibile arricchimento del locatore.                                    &#13;
    I  due  giudizi,  riferiti  alla stessa disposizione e di identico    &#13;
 contenuto, sono evidentemente connessi e  possono  essere  riuniti  e    &#13;
 decisi congiuntamente.                                                   &#13;
    2. - La questione è fondata.                                         &#13;
    Questa  Corte  ha più volte affermato che è conforme ai precetti    &#13;
 costituzionali  una  disciplina  diretta  a   tutelare   l'avviamento    &#13;
 commerciale  mediante  la  previsione,  a  favore  del conduttore che    &#13;
 rilascia l'immobile, di una speciale indennità  compensatrice  della    &#13;
 perdita  che  egli  subisce e che, secondo una valutazione tipica del    &#13;
 legislatore, tiene conto dell'arricchimento del locatore per  effetto    &#13;
 dell'incremento di valore incorporatosi nell'immobile per l'attività    &#13;
 svolta dal conduttore (sentenze n. 300 del 1983, n. 882 del 1988 e n.    &#13;
 116 del 1989).                                                           &#13;
    L'esigenza di ripristinare l'equilibrio dei soggetti del rapporto,    &#13;
 bilanciando   la   perdita   dell'avviamento   per  il  conduttore  e    &#13;
 l'arricchimento senza causa propria del locatore, viene meno quando a    &#13;
 porre fine  al  rapporto  di  locazione,  senza  responsabilità  del    &#13;
 locatore,  è  un  provvedimento  della  pubblica Amministrazione che    &#13;
 vieta sine die l'utilizzazione dell'immobile ed esclude ogni  effetto    &#13;
 lucrativo  della  cessata relazione contrattuale. Questo principio è    &#13;
 stato già affermato nella sentenza n. 542 del 1989 che ha dichiarato    &#13;
 la illegittimità costituzionale dell'art. 34 della legge  27  luglio    &#13;
 1978,  n.  392, nella parte in cui, disciplinando l'indennità per la    &#13;
 perdita dell'avviamento, non prevede i provvedimenti  della  pubblica    &#13;
 Amministrazione  tra le cause di cessazione del rapporto di locazione    &#13;
 che escludono il diritto del conduttore alla indennità.  Difatti  è    &#13;
 stato   ritenuto  irragionevole  non  riconoscere  a  tale  causa  di    &#13;
 cessazione del rapporto gli stessi effetti che  si  verificano  negli    &#13;
 altri  casi  di  cessazione  del  rapporto che, secondo la previsione    &#13;
 legislativa, non danno titolo alla indennità di avviamento.             &#13;
    Lo stesso principio  deve  trovare  applicazione  per  la  analoga    &#13;
 disciplina  dettata  dall'art.  69  della legge n. 392 del 1978 per i    &#13;
 rapporti sorti anteriormente alla  entrata  in  vigore  della  legge.    &#13;
 Anche  per questi rapporti la cessazione della locazione o la mancata    &#13;
 prosecuzione di essa determinata da  un  provvedimento  autoritativo,    &#13;
 adottato  senza  responsabilità  del  locatore  e che vieta sine die    &#13;
 l'uso dell'immobile, esclude ogni  effetto  lucrativo  derivante  dal    &#13;
 cessato  rapporto  di  locazione.  Ne segue la irragionevolezza, già    &#13;
 valutata per la analoga disciplina dettata dall'art. 34  della  legge    &#13;
 392  del  1978  e  che  si  estende  anche  alla ipotesi ora presa in    &#13;
 considerazione.                                                          &#13;
    Deve  essere pertanto dichiarata la illegittimità costituzionale,    &#13;
 in riferimento all'art. 3  della  Costituzione,  dell'art.  69  della    &#13;
 legge  27 luglio 1978, n. 392 (Disciplina delle locazioni di immobili    &#13;
 urbani) nella parte in cui non prevede che l'obbligo per il  locatore    &#13;
 di   corrispondere  al  conduttore  la  indennità  per  l'avviamento    &#13;
 commerciale non ricorre  quando  causa  di  cassazione  del  rapporto    &#13;
 locativo  è  un  provvedimento  della  pubblica Amministrazione che,    &#13;
 senza che vi abbia dato  causa  il  locatore,  esclude  sine  die  la    &#13;
 utilizzazione   economica   dell'immobile  e  non  determina  per  il    &#13;
 conduttore  alcun  effetto  lucrativo  per   la   cessata   relazione    &#13;
 contrattuale.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  illegittimità  costituzionale    &#13;
 dell'art. 69 della legge 27 luglio 1978,  n.  392  (Disciplina  delle    &#13;
 locazioni  di  immobili  urbani)  nella  parte in cui non prevede che    &#13;
 l'obbligo del locatore di corrispondere al conduttore  la  indennità    &#13;
 per  l'avviamento  commerciale non ricorre quando causa di cessazione    &#13;
 del rapporto è un provvedimento della pubblica  Amministrazione  che    &#13;
 esclude indefinitamente la utilizzazione economica dell'immobile.        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 maggio 1992.                               &#13;
                       Il Presidente: BORZELLINO                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 3 giugno 1992.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
