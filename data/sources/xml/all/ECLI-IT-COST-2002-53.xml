<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>53</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:53</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>27/02/2002</data_decisione>
    <data_deposito>15/03/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
FLICK, Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità  costituzionale della legge 11 ottobre &#13;
1995,  n. 423  (Norme  in materia di soprattasse e di pene pecuniarie &#13;
per  omesso,  ritardato  o insufficiente versamento delle imposte), e &#13;
dell'art. 6   del   d.lgs.  18 dicembre  1997,  n. 472  (Disposizioni &#13;
generali  in  materia di sanzioni amministrative per le violazioni di &#13;
norme  tributarie,  a  norma  dell'art. 3,  comma  133,  della  legge &#13;
23 dicembre  1996,  n. 662),  promossi  con  due  ordinanze emesse il &#13;
30 novembre  2000  dalla  Commissione  tributaria  di  primo grado di &#13;
Trento,  iscritte,  rispettivamente,  ai  nn. 92  e  93  del registro &#13;
ordinanze 2001 e pubblicate nella Gazzetta Ufficiale della Repubblica &#13;
n. 7, 1ª serie speciale, dell'anno 2001. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 16 gennaio 2002 il giudice &#13;
relatore Valerio Onida. &#13;
    Ritenuto  che,  con  due ordinanze del medesimo tenore, emesse il &#13;
30 novembre  2000  e  pervenute a questa Corte il 22 gennaio 2001, la &#13;
Commissione   tributaria  di  primo  grado  di  Trento  ha  sollevato &#13;
questione  di legittimità costituzionale, senza espressa indicazione &#13;
della  norma  costituzionale  che  si  assume  violata,  della  legge &#13;
11 ottobre  1995,  n. 423  (Norme in materia di soprattasse e di pene &#13;
pecuniarie  per  omesso,  ritardato  o insufficiente versamento delle &#13;
imposte),   e   dell'art. 6   del  d.lgs.  18 dicembre  1997,  n. 472 &#13;
(Disposizioni  generali  in materia di sanzioni amministrative per le &#13;
violazioni di norme tributarie, a norma dell'art. 3, comma 133, della &#13;
legge   23 dicembre   1996,   n. 662),   "in   quanto   non  tutelano &#13;
sufficientemente  e  ragionevolmente,  in  tema  di  riscossione  dei &#13;
tributi ed accessori, il contribuente vittima di "consulente infedele &#13;
"; &#13;
        che  il  remittente  premette che il contribuente contesta un &#13;
accertamento  di imposta sul reddito delle persone fisiche, ritenendo &#13;
di  dover  pagare  solo  la  minore  somma  risultante  da un atto di &#13;
accertamento con adesione, non perfezionato perché il consulente del &#13;
medesimo   contribuente   aveva  omesso  di  effettuare  i  pagamenti &#13;
devolvendo a proprio profitto gli importi da corrispondere; e chiede, &#13;
in sede cautelare, la sospensione della riscossione; &#13;
        che,  secondo  il  giudice a quo mancherebbe una norma che in &#13;
via  generale  rimetta  il  contribuente  nei termini incolpevolmente &#13;
scaduti,  ed  in  particolare  mantenga  la  possibilità  di  fruire &#13;
dell'agevolazione  conseguente all'accertamento con adesione quando i &#13;
relativi  atti  non  si  siano  perfezionati  a  causa  dell'illecito &#13;
commesso dal professionista: ciò darebbe luogo ad una "irragionevole &#13;
disparità  di trattamento fra il regime dedicato alle sanzioni ed il &#13;
regime di riscossione del tributo", che renderebbe non manifestamente &#13;
infondato  il  dubbio  di legittimità costituzionale della normativa &#13;
denunciata; &#13;
        che,  quanto  alla  rilevanza  della questione, il remittente &#13;
afferma  che la verifica del fumus boni juris ai fini della richiesta &#13;
tutela  cautelare  appare  influenzata  dal  dubbio  di  legittimità &#13;
costituzionale sollevato; &#13;
        che  è  intervenuto nel giudizio il Presidente del Consiglio &#13;
dei ministri, osservando che l'art. 1, comma 6-bis della legge n. 423 &#13;
del  1995  prevede  la  possibilità  per  l'ufficio  finanziario  di &#13;
sospendere, fra l'altro nella ipotesi di omesso versamento di tributi &#13;
dovuto a fatto penalmente illecito, e denunciato, del professionista, &#13;
la  riscossione  del  tributo,  per  un  biennio,  nei  confronti del &#13;
contribuente per il quale sussistano comprovate difficoltà di ordine &#13;
economico,  e  che  offra idonea garanzia: onde il giudice tributario &#13;
avrebbe  potuto accordare la richiesta sospensione, ovvero, se avesse &#13;
ritenuto   quest'ultima   di   esclusiva   competenza  degli  uffici, &#13;
prospettare la questione di legittimità costituzionale riferendola a &#13;
più puntuale oggetto e ad altri parametri; &#13;
        che  pertanto,  secondo l'interveniente, la questione sarebbe &#13;
inammissibile  per  irrilevanza nella fase cautelare nel giudizio, in &#13;
quanto  preordinata  alla  decisione  sul  merito della controversia, &#13;
nonché   per  difetto  di  individuazione  della  norma  sostanziale &#13;
relativa   alla   risoluzione   o   alla   decadenza   dai   benefici &#13;
dell'accertamento  con  adesione;  sarebbe, ancora, inammissibile per &#13;
insufficiente esplicitazione delle ragioni della denuncia; e sarebbe, &#13;
comunque,  manifestamente  infondata  ove  riferita  ad  una asserita &#13;
preclusione  alla  sospensione  della  riscossione  del tributo nella &#13;
ipotesi  di omesso versamento d'imposta per fatto penalmente illecito &#13;
del terzo. &#13;
    Considerato che le due ordinanze sollevano la medesima questione, &#13;
onde i giudizi vanno riuniti per essere decisi con unica pronunzia; &#13;
        che  la censura mossa dal giudice remittente la cui rilevanza &#13;
nella  specie  è  dal medesimo, non implausibilmente, collegata alla &#13;
valutazione  del  fumus  boni  juris  ad esso demandata ai fini della &#13;
richiesta   tutela  cautelare  riguarda  la  mancata  estensione  dei &#13;
benefici  riconosciuti  dall'art. 1,  commi  1, 2 e 6-bis della legge &#13;
n. 423  del  1995  nel caso di omesso versamento di tributi quando la &#13;
violazione consegua alla condotta illecita, penalmente rilevante, del &#13;
professionista  in  dipendenza del mandato professionale (sospensione &#13;
della  riscossione  delle  somme  dovute a titolo di soprattassa e di &#13;
pena   pecuniaria;   sospensione   per   un   biennio   e  successiva &#13;
rateizzazione del debito relativo al versamento del tributo, nel caso &#13;
di  comprovate  difficoltà  di ordine economico del contribuente), e &#13;
dall'art. 6  del  d.lgs. n. 472 del 1997 nel caso in cui il pagamento &#13;
del tributo non sia stato eseguito per fatto denunciato all'autorità &#13;
giudiziaria    e    addebitabile    esclusivamente   a   terzi   (non &#13;
assoggettabilità   del  contribuente  alle  sanzioni  amministrative &#13;
previste)  alla  ipotesi in cui l'omesso versamento a causa del fatto &#13;
illecito   del   professionista   abbia   impedito  il  perfezionarsi &#13;
dell'accertamento  con  adesione, previsto dal d.lgs. 19 giugno 1997, &#13;
n. 218;  e  mira  ad  ottenere,  attraverso  la  richiesta  pronuncia &#13;
additiva  di  questa Corte, la rimessione del contribuente in termini &#13;
per  l'effettuazione  del pagamento che condiziona il perfezionamento &#13;
dell'accertamento  con  adesione,  ai  sensi dell'art. 9 del medesimo &#13;
d.lgs. n. 218 del 1997; &#13;
        che,  benché l'ordinanza di remissione ometta la indicazione &#13;
espressa  del  parametro,  la  censura  di  disparità di trattamento &#13;
appare  riconducibile all'art. 3 della Costituzione, parametro dunque &#13;
implicitamente ricavabile dalla motivazione dell'ordinanza medesima; &#13;
        che   il   beneficio   che  si  vorrebbe  far  conseguire  al &#13;
contribuente  attraverso  la  richiesta  pronuncia  di illegittimità &#13;
costituzionale  si  colloca  su  di  un  piano  diverso  dai benefici &#13;
derivanti   dalle   norme  invocate:  non  riguarderebbe  infatti  la &#13;
sospensione  (e  poi  lo  sgravio) del debito per le soprattasse e le &#13;
pene  pecuniarie,  e la non applicabilità delle sanzioni, nonché la &#13;
sospensione  temporanea  e  la  rateizzazione  del debito tributario, &#13;
destinato  però  a  rimanere  invariato  nel  suo  ammontare, bensì &#13;
inciderebbe  sull'ammontare  del  debito  tributario medesimo, che si &#13;
vorrebbe     ridotto     o    riducibile    all'entità    risultante &#13;
dall'accertamento  con  adesione,  pur quando quest'ultimo non si sia &#13;
perfezionato per il mancato tempestivo versamento delle somme dovute; &#13;
        che,  pertanto,  la questione sollevata appare manifestamente &#13;
infondata  per  inidoneità  del  tertium  comparationis  invocato  e &#13;
disomogeneità delle situazioni messe a raffronto; &#13;
        che  l'accertamento  con adesione è procedimento, apprestato &#13;
dal  legislatore  in  base  ad  una  scelta  discrezionale,  volto  a &#13;
consentire  una  più  rapida definizione dei rapporti tributari e la &#13;
riduzione   del  contenzioso,  e  ragionevolmente  legato  dunque  ad &#13;
adempimenti  del  contribuente da espletarsi entro termini perentori; &#13;
onde  appartiene  alla  discrezionalità  del legislatore l'eventuale &#13;
introduzione  di ipotesi di riapertura dei termini per la definizione &#13;
del rapporto tributario, in casi come quelli evocati dal remittente; &#13;
        che,   peraltro,   fermo   restando  l'ammontare  del  debito &#13;
tributario  come  definito  in  base  alle  procedure di accertamento &#13;
previste,  e  alle  eventuali  determinazioni  di  merito del giudice &#13;
tributario  tempestivamente  adito,  il  contribuente,  il  quale sia &#13;
vittima    della   condotta   illecita   penalmente   rilevante   del &#13;
professionista,   può   sempre   usufruire,  ove  ne  sussistano  le &#13;
condizioni,  dei  benefici  riconosciuti,  per  questa ipotesi, dalle &#13;
norme  legislative  sopra ricordate, concernenti la sospensione della &#13;
riscossione  delle  sanzioni  e la non applicabilità delle medesime, &#13;
nonché  la  sospensione  temporanea  e  la  rateizzazione del debito &#13;
tributario,  oltre  che  della  normale  tutela  cautelare in sede di &#13;
giudizio   tributario   promosso  avverso  l'accertamento:  a  parte, &#13;
evidentemente,  il diritto nei confronti dell'autore dell'illecito al &#13;
risarcimento del danno subito. &#13;
    Visti  gli articoli 26, secondo comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi; &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale dell'art. 1 della legge 11 ottobre 1995, &#13;
n. 423  (Norme  in  materia  di  soprattasse e di pene pecuniarie per &#13;
omesso,  ritardato  o  insufficiente  versamento  delle  imposte),  e &#13;
dell'art. 6   del   d.lgs.  18 dicembre  1997,  n. 472  (Disposizioni &#13;
generali  in  materia di sanzioni amministrative per le violazioni di &#13;
norme  tributarie,  a  norma  dell'art. 3,  comma  133,  della  legge &#13;
23 dicembre  1996, n. 662), sollevata dalla Commissione tributaria di &#13;
primo grado di Trento con le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 27 febbraio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 15 marzo 2002. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
