<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>322</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:322</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1994</data_decisione>
    <data_deposito>20/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  del  decreto-legge  17    &#13;
 gennaio  1994,  n. 33 recante: "Disciplina della proroga degli organi    &#13;
 amministrativi",  promosso  con  ricorso   della   Regione   Calabria    &#13;
 notificato  il  14  febbraio  1994  ed iscritto al n. 19 del registro    &#13;
 ricorsi 1994;                                                            &#13;
    Visto l'atto di costituzione  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  6 luglio 1994 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto che la Regione Calabria ha impugnato, in riferimento agli    &#13;
 artt. 77, ultimo comma, 117, 118, 121, 122 e 123 della  Costituzione,    &#13;
 il  decreto-legge  17  gennaio  1994, n. 33 (Disciplina della proroga    &#13;
 degli  organi   amministrativi)   nel   presupposto   interpretativo,    &#13;
 ipotizzato  come  eventuale,  che  l'art.  9,  comma  1, del medesimo    &#13;
 decreto-legge - secondo cui "le disposizioni .. (del decreto) operano    &#13;
 direttamente nei riguardi delle regioni a statuto  ordinario  fino  a    &#13;
 quando esse non avranno adeguato i rispettivi ordinamenti ai principi    &#13;
 generali  ivi  contenuti"  -  sia  idoneo  a  determinare l'immediata    &#13;
 abrogazione della preesistente normativa regionale in  materia  (che,    &#13;
 nella  specie,  è  contenuta  nella  legge  della Regione Calabria 5    &#13;
 agosto 1992, n. 13), nel qual caso la disciplina denunziata  sarebbe,    &#13;
 secondo    la    ricorrente,    invasiva    della    sua   competenza    &#13;
 costituzionalmente garantita ed in particolare:                          &#13;
       a) l'attribuzione ai presidenti  degli  organi  collegiali,  in    &#13;
 caso  di  inerzia  di  questi ultimi, della competenza in ordine alla    &#13;
 designazione e alla nomina dei titolari degli  organi  amministrativi    &#13;
 scaduti, contenuta nell'art. 4, comma 2, violerebbe sia le competenze    &#13;
 regionali  in  materia  di  ordinamento  degli uffici (art. 117 della    &#13;
 Costituzione),  sia  le  competenze  statutarie   (art.   123   della    &#13;
 Costituzione),  sia,  infine,  se riferita a nomine di competenza del    &#13;
 Consiglio regionale, la configurazione del presidente  del  Consiglio    &#13;
 regionale  come  organo  privo  di rilevanza esterna (artt. 121 e 122    &#13;
 della Costituzione);                                                     &#13;
       b) la disciplina  della  proroga  degli  organi  amministrativi    &#13;
 scaduti  e  degli  atti da questi emanati, nel limitare la competenza    &#13;
 degli organi prorogati agli atti di ordinaria amministrazione nonché    &#13;
 agli  atti  urgenti  e  indifferibili  (art.  3),  inciderebbe  sulla    &#13;
 competenza   regionale   in   materia,   violando  l'art.  117  della    &#13;
 Costituzione; questa censura sarebbe da estendere al collegato art. 6    &#13;
 del decreto-legge, che prevede la nullità degli atti compiuti  dagli    &#13;
 organi decaduti;                                                         &#13;
       c)  la  previsione  (art.  8)  della  conferma  degli  atti  di    &#13;
 ricostituzione degli organi scaduti  adottati  dai  presidenti  degli    &#13;
 organi  collegiali  sulla  base  della  disciplina vigente al momento    &#13;
 della loro adozione (e cioè sulla base dei decreti-legge  che  hanno    &#13;
 preceduto  quello  impugnato  nel  presente  giudizio) violerebbe sia    &#13;
 l'art. 77, ultimo  comma,  della  Costituzione,  in  relazione  anche    &#13;
 all'art.  15,  comma  2, lett. d) della legge 23 agosto 1988, n. 400,    &#13;
 sia le competenze regionali in materia di organizzazione di uffici ed    &#13;
 enti (artt. 117 e 123 della Costituzione), precludendo  alle  regioni    &#13;
 di  revocare gli atti illegittimi dei loro presidenti e di provvedere    &#13;
 diversamente in ordine agli organi scaduti;                              &#13;
      che si è costituito in giudizio il Presidente del Consiglio dei    &#13;
 Ministri, tramite l'Avvocatura generale dello Stato, che ha  concluso    &#13;
 per l'inammissibilità o per l'infondatezza delle questioni.             &#13;
    Considerato  che  il  decreto-legge  17 gennaio 1994, n. 33 non è    &#13;
 stato convertito in legge nel termine prescritto,  come  risulta  dal    &#13;
 comunicato  pubblicato  nella  Gazzetta  Ufficiale n. 65 del 19 marzo    &#13;
 1994;                                                                    &#13;
      che, pertanto, in  conformità  alla  giurisprudenza  di  questa    &#13;
 Corte  (v.,  da  ultimo,  l'ordinanza  n. 186 del 1994), le questioni    &#13;
 sollevate   dalla   Regione   Calabria   devono   essere   dichiarate    &#13;
 manifestamente inammissibili.                                            &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   delle  questioni  di    &#13;
 legittimità costituzionale degli artt. 3, 4, secondo comma, 6, 8 e 9    &#13;
 del decreto-legge 17 gennaio 1994, n. 33  (Disciplina  della  proroga    &#13;
 degli  organi  amministrativi),  sollevate, in riferimento agli artt.    &#13;
 77, ultimo comma, 117, 118, 121, 122 e 123 della Costituzione,  dalla    &#13;
 Regione Calabria con il ricorso indicato in epigrafe.                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 7 luglio 1994.                                   &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 20 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
