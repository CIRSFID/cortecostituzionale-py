<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>295</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:295</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/11/1985</data_decisione>
    <data_deposito>13/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 80 bis d.P.R.  &#13;
 15  giugno  1959,  n.  393 ("Testo unico delle norme sulla circolazione  &#13;
 stradale"), in relazione all'art. 83 dello stesso d.P.R., promosso  con  &#13;
 l'ordinanza  emessa  il 20 luglio 1983 dal Pretore di Caltanissetta nel  &#13;
 procedimento penale a carico di Russo Filippo iscritta al  n.  940  del  &#13;
 registro  ordinanze  1983  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 88 dell'anno 1984.                                         &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del  9 ottobre 1985 il Giudice  &#13;
 relatore prof. Antonio La Pergola.                                       &#13;
     Ritenuto che il Pretore di Caltanissetta, con l'ordinanza  indicata  &#13;
 in   epigrafe,   ha   sollevato,   in   riferimento  all'art.  3  della  &#13;
 Costituzione, questione di legittimità costituzionale dell'art. 80 bis  &#13;
 del Codice della Strada (introdotto con  legge  24  novembre  1981,  n.  &#13;
 689),  in  relazione  all'art.  83  dello  stesso codice, in quanto non  &#13;
 prevede la  pena  accessoria  della  confisca  del  veicolo  anche  nei  &#13;
 confronti  del  soggetto  attivo del reato di esercitazione alla guida,  &#13;
 senza avere a fianco, in funzione di istruttore, persona  provvista  di  &#13;
 patente valida per la stessa categoria del veicolo pilotato;             &#13;
     che,  ad avviso del giudice a quo, la mancata previsione della pena  &#13;
 accessoria nel caso indicato determina una disparità di trattamento in  &#13;
 relazione ad ipotesi di reato "tra loro sostanzialmente identiche",  la  &#13;
 quale non sarebbe "sorretta da alcuna ragionevolezza";                   &#13;
     che  si  è  costituito in giudizio il Presidente del Consiglio dei  &#13;
 ministri, rappresentato e difeso dall'Avvocatura dello Stato, la quale,  &#13;
 in primo  luogo,  eccepisce  la  irrilevanza  della  questione  per  la  &#13;
 inapplicabilità  della  normativa  impugnata  al  caso  di specie (che  &#13;
 ricade, si soggiunge, sotto il disposto dell'art. 83 del  Codice  della  &#13;
 Strada: norma, peraltro, più favorevole per il giudicabile), e deduce,  &#13;
 comunque,  l'infondatezza della questione stessa nel merito, in quanto,  &#13;
 pur essendo equiparata, ai fini della pena principale, la posizione  di  &#13;
 chi   guida   senza  patente  a  quella  di  chi,  autorizzato  per  la  &#13;
 esercitazione, non abbia al fianco, in funzione di istruttore,  persona  &#13;
 provvista  di  patente,  "rientra  tuttavia, nella discrezionalità del  &#13;
 legislatore prevedere, nell'un caso e non nell'altro, la confisca".      &#13;
     Considerato che identica questione, sollevata dallo stesso giudice,  &#13;
 è  stata  dichiarata  manifestamente  inammissibile  dalla  Corte  con  &#13;
 l'ordinanza n. 251 del 1984,  sulla  base  del  rilievo  che,  anche  a  &#13;
 prescindere     dall'eccezione     di     inammissibilità    sollevata  &#13;
 dall'Avvocatura,  veniva  richiesto  alla  Corte  di  pronunziare   una  &#13;
 sentenza,   la  quale  avrebbe  avuto  il  risultato  di  estendere  la  &#13;
 previsione delle sanzioni accessorie oltre i casi  che  il  legislatore  &#13;
 penale, nel dettare la normativa censurata, ha contemplato; mentre ciò  &#13;
 era  precluso  alla Corte dal fondamentale ed inderogabile principio di  &#13;
 legalità, consacrato nell'art. 25 della Costituzione,  oltre  che  nel  &#13;
 codice penale;                                                           &#13;
     che   identiche   considerazioni  valgono  anche  per  il  presente  &#13;
 giudizio.                                                                &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e 9, secondo comma, delle Norme integrative per i giudizi innanzi  alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   la   manifesta   inammissibilità   della  questione  di  &#13;
 legittimità costituzionale dell'art. 80 bis del  Codice  della  Strada  &#13;
 (introdotto  con legge n. 689 del 1981), in relazione all'art. 83 dello  &#13;
 stesso codice, sollevata, in riferimento all'art. 3 Cost., dal  Pretore  &#13;
 di Caltanissetta con l'ordinanza in epigrafe.                            &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 novembre 1985.       &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   RENATO DELL'ANDRO.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
