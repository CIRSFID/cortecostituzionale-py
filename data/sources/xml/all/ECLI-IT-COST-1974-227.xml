<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>227</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:227</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/07/1974</data_decisione>
    <data_deposito>10/07/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA Prof. EDOARDO &#13;
 VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale degli artt. 4, 7, 19, 21 e  &#13;
 22 della Convenzione per la concessione da parte dello Stato  alla  RAI  &#13;
 del  servizio  di radioaudizione e televisione circolare e del servizio  &#13;
 di telediffusione su filo (approvata con d.P.R.  26  gennaio  1952,  n.  &#13;
 180), e correlative norme del codice postale (r.d. 27 febbraio 1936, n.  &#13;
 645),  promosso  con ordinanza emessa il 9 dicembre 1972 dal pretore di  &#13;
 Trasacco nel procedimento civile vertente tra le società SEDI  e  SAIE  &#13;
 contro  le  società  RAI-TV  e  SIPRA, iscritta al n. 106 del registro  &#13;
 ordinanze 1973 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 133 del 23 maggio 1973.                                               &#13;
     Visti  gli  atti di costituzione delle società SEDI, SAIE, RAITV e  &#13;
 SIPRA;                                                                   &#13;
     udito nell'udienza pubblica del 29 maggio 1974 il Giudice  relatore  &#13;
 Giuseppe Verzì;                                                         &#13;
     uditi l'avv. Dario Di Gravio, per le società SEDI e SAIE, e l'avv.  &#13;
 Antonio Sorrentino, per le società RAI-TV e SIPRA.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  atto  depositato il 7 dicembre 1972 le società SEDI (società  &#13;
 editrice distributrice internazionale) e SAIE chiedevano al pretore  di  &#13;
 Trasacco,  ai  sensi  dell'art.  700  del codice di procedura civile di  &#13;
 ordinare alle società RAI Radiotelevisione Italiana p.a. e SIPRA p.a.,  &#13;
 in via principale, di astenersi dal trattare o concludere  accordi  per  &#13;
 la  pubblicità sulla stampa; in subordine, di sospendere le trattative  &#13;
 tra loro in corso in tale materia  e  di  depositare  gli  incartamenti  &#13;
 relativi  al volume della pubblicità sulla stampa. Eccepivano altresì  &#13;
 la illegittimità costituzionale del  monopolio  radiotelevisivo  della  &#13;
 pubblicità commerciale.                                                 &#13;
     Con  ordinanza  9  dicembre  1972  il suddetto pretore, accogliendo  &#13;
 l'eccezione  delle  società  istanti,  ha  sollevato,  in  riferimento  &#13;
 all'art.,43  Cost.,  la  questione  di  legittimità costituzionale del  &#13;
 d.P.R. 26 gennaio 1952, n. 180 (e correlative norme del codice postale)  &#13;
 "nella parte in cui approva e rende esecutivi gli artt. 4, 7, 19, 21  e  &#13;
 22   della   concessione  fra  lo  Stato  e  la  società  per  a.  RAI  &#13;
 Radiotelevisione Italiana", in  quanto  tali  articoli  considerano  la  &#13;
 pubblicità  radiotelevisiva  "servizio pubblico essenziale", "fonte di  &#13;
 energia" o "situazione  di  monopolio"  con  "carattere  di  preminente  &#13;
 interesse generale".                                                     &#13;
     Nel  giudizio  conseguito presso questa Corte si sono costituite le  &#13;
 cennate società nonché la RAI Radiotelevisione Italiana.               &#13;
     È soprattutto dall'atto di costituzione delle società che possono  &#13;
 desumersi i  termini  della  questione  prospettata  dall'ordinanza  di  &#13;
 rimessione;  la  quale,  del resto, si richiama espressamente ai motivi  &#13;
 indicati dalla SEDI e dalla SAIE nel ricorso.                            &#13;
     Secondo queste ultime, pur non essendo il  decreto  del  Presidente  &#13;
 della  Repubblica n. 180 del 1952 né una legge in senso formale né un  &#13;
 atto avente forza di legge, ma  un  regolamento  esterno  o  giuridico,  &#13;
 cioè   una   legge   in   senso   materiale,  è  tuttavia  come  tale  &#13;
 assoggettabile al giudizio di costituzionalità. Ne sarebbero  conferma  &#13;
 sia il contenuto, che consiste in una materia di interesse generale che  &#13;
 riguarda rapporti giuridici esterni della pubblica Amministrazione, sia  &#13;
 la  procedura  di emanazione, rispondente in pieno alle disposizioni di  &#13;
 legge. Fra  di  esse,  in  particolare,  la  previa  deliberazione  del  &#13;
 Consiglio dei ministri.                                                  &#13;
     Nel merito si rileva, in via generale, che non possono condividersi  &#13;
 le  argomentazioni addotte dalla Corte costituzionale nelle sentenze n.  &#13;
 59 del 1960, n. 81 del 1973 e n. 58 del  1965,  a  giustificazione  del  &#13;
 monopolio  statuale  dei  servizi  di radiodiffusione circolari e della  &#13;
 concessione di tali servizi ad un ente  privato  quale  la  RAI.  E  al  &#13;
 riguardo si ripetono le critiche mosse in dottrina a dette sentenze.     &#13;
     Più  analiticamente, si fa presente che l'art. 4 della Convenzione  &#13;
 Stato-RAI  del  1952,  prevede  che  la  gestione   della   pubblicità  &#13;
 radiotelevisiva   possa  essere  affidata  ad  una  società  separata,  &#13;
 fissando, altresì,  alcune  norme  in  ordine  alla  ripartizione  del  &#13;
 capitale  azionario,  ma  nulla disponendo quanto al campo di attività  &#13;
 della società medesima. E la RAI, profittando della carenza normativa,  &#13;
 ne ha affidato la gestione alla SIPRA, onde  assicurarsi  una  maggiore  &#13;
 libertà di azione che le sarebbe mancata ove avesse gestito in proprio  &#13;
 il settore pubblicitario.                                                &#13;
     Ma   vi   è   da  dubitare  -  si  osserva  -  della  legittimità  &#13;
 costituzionale del monopolio della impresa pubblicitaria televisiva  in  &#13;
 rapporto all'art. 43 della Costituzione.                                 &#13;
     I  presupposti  di  fatto cui detto articolo condiziona la facoltà  &#13;
 dello Stato di servirsi di un determinato  settore  non  ricorrono  per  &#13;
 l'impresa di pubblicità televisiva: non si tratta di fonte di energia,  &#13;
 né di servizio pubblico, né di impresa monopolistica.                  &#13;
     La  illegittimità deriverebbe altresì dal contrasto con l'art. 41  &#13;
 della Costituzione, in quanto non si verifica per la  ripetuta  impresa  &#13;
 nessuna   delle   ragioni  che,  a  norma  di  tale  articolo,  possono  &#13;
 determinare una limitazione della libertà di iniziativa privata.        &#13;
     L'art. 4 della Convenzione prevede - si chiarisce - la facoltà per  &#13;
 la RAI di gestire la pubblicità radiotelevisiva a  mezzo  di  separata  &#13;
 società;  il precedente art. 1 conclude affermando che l'esercizio dei  &#13;
 servizi elencativi (radio, telediffusione,  etc.)  e  di  quanto  altro  &#13;
 serve  esclusivamente al loro sviluppo e potenziamento, deve costituire  &#13;
 lo scopo sociale esclusivo della RAI.                                    &#13;
     Da un  punto  di  vista  interpretativo,  poiché  l'art.  1  nella  &#13;
 disposizione  ad hoc, definisce il campo esclusivo di azione della RAI,  &#13;
 il successivo art. 4 disponendo su di un diverso  argomento,  non  può  &#13;
 tacitamente derogare alla esplicita disposizione dell'art. 1.            &#13;
     Ciò  implica  che anche la società SIPRA è tenuta al rispetto di  &#13;
 quella norma, ossia a svolgere esclusivamente le attività di  gestione  &#13;
 della  pubblicità  televisiva,  con  esclusione  della  trattazione di  &#13;
 affari pubblicitari in altri settori. Al  contrario,  la  SIPRA  tratta  &#13;
 affari  pubblicitari  nel  campo  della  editoria,  violando così, nel  &#13;
 contempo, i doveri di monopolista ex  lege  e  quelli  di  imprenditore  &#13;
 privato nei confronti degli operatori economici concorrenti.             &#13;
     Secondo  la  difesa  della  RAI, nell'ordinanza di rimessione manca  &#13;
 ogni motivazione sulla rilevanza della risoluzione della  questione  di  &#13;
 legittimità  costituzionale per la definizione del giudizio principale  &#13;
 e, per giunta,  l'omissione  dell'esame  di  detta  rilevanza  riguarda  &#13;
 proprio  "le  correlative  norme del codice postale" alle quali il vice  &#13;
 pretore ha  esteso  di  ufficio  il  dubbio  di  costituzionalità.  E,  &#13;
 peraltro,  tali  norme  non  è  dato  individuare. Ma, soprattutto, il  &#13;
 d.P.R. n. 180 del 1952 non è né legge,  né  legge  delegata,  ma  un  &#13;
 comune  atto  amministrativo;  e  come  tale,  sfugge  al  sindacato di  &#13;
 legittimità della Corte.                                                &#13;
     Nel merito, la RAI osserva che se la censura del pretore si rivolge  &#13;
 al fatto che la pubblicità radiotelevisiva  sia  stata  attribuita  in  &#13;
 esclusiva  alla  RAI,  si  trascura  un dato essenziale; e cioè che la  &#13;
 pubblicità viene fatta con i  mezzi  che  fanno  parte  del  legittimo  &#13;
 monopolio dello Stato per il servizio pubblico delle trasmissioni e che  &#13;
 non  si  potrebbe  pretendere che la pubblicità in casa dell'esercente  &#13;
 tale servizio sia fatta da chicchessia. Se, poi, si vuole censurare che  &#13;
 la RAI-TV abbia dato in esclusiva il servizio alla SIPRA,  è  evidente  &#13;
 che  ciò  -  illegittimo  o  legittimo che sia - non va apprezzato dal  &#13;
 giudice di costituzionalità delle leggi.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'ordinanza  del  pretore  di  Trasacco  solleva  la  questione  di  &#13;
 legittimità  costituzionale del d.P.R. 26 gennaio 1952, n.  180, nella  &#13;
 parte in cui approva e rende esecutivi gli artt. 4,  7,  19'  21  e  22  &#13;
 della  Convenzione per la concessione da parte dello Stato alla RAI dei  &#13;
 servizi di radio audizione e telediffusioni  circolari,  assumendo  che  &#13;
 tali  norme sono in contrasto con l'art. 43 della Costituzione perché:  &#13;
 1) la pubblicità  televisiva  è  stata  riservata  in  monopolio  pur  &#13;
 mancando  il fine di utilità generale, e la qualificazione di pubblico  &#13;
 servizio essenziale o di fonti di energia; 2)  le  stesse  disposizioni  &#13;
 consentono  che il servizio venga gestito in monopolio, in concreto, da  &#13;
 soggeti diversi dallo Stato, enti pubblici, comunità di lavoratori  od  &#13;
 utenti.                                                                  &#13;
     La questione è inammissibile.                                       &#13;
     Il  d.P.R.  26 gennaio 1952, n. 180, che è stato impugnato, non è  &#13;
 né legge né atto avente forza di legge, per i  quali  la  Corte  può  &#13;
 giudicare   della   costituzionalità   a  sensi  dell'art.  134  della  &#13;
 Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 del d.P.R. 26 gennaio 1952, n. 180 (Approvazione ed esecutorietà della  &#13;
 Convenzione per la concessione alla Radio Audizione Italia società per  &#13;
 azioni  del  servizio  di  radioaudizioni e televisione circolare e del  &#13;
 servizio di  telediffusione  su  filo),  sollevata  con  ordinanza  del  &#13;
 pretore di Trasacco del 9 dicembre 1972.                                 &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 9 luglio 1974.                                &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ  -  LUIGI  OGGIONI - ANGELO DE  &#13;
                                   MARCO  -  ERCOLE  ROCCHETTI  -   ENZO  &#13;
                                   CAPALOZZA - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA EDOARDO VOLTERRA -  &#13;
                                   GUIDO ASTUTI.                          &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
