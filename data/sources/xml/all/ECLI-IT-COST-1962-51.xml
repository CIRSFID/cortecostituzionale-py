<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1962</anno_pronuncia>
    <numero_pronuncia>51</numero_pronuncia>
    <ecli>ECLI:IT:COST:1962:51</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Chiarelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/06/1962</data_decisione>
    <data_deposito>14/06/1962</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Dott. &#13;
 MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. GIUSEPPE &#13;
 CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 60 del T.U.  &#13;
 delle leggi  per  la  elezione  dei  Consigli  comunali  nella  Regione  &#13;
 siciliana, approvato con decreto del Presidente della Regione 20 agosto  &#13;
 1960,  n.  3,  promosso  con  deliberazione  5  giugno  1961 emessa dal  &#13;
 Consiglio comunale di Venetico  su  ricorso  di  La  Guidara  Giuseppe,  &#13;
 iscritta  al  n.  111  del  Registro  ordinanze 1961 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 218 del 2 settembre 1961 e nella  &#13;
 Gazzetta Ufficiale della Regione siciliana n. 60 del 28 ottobre 1961.    &#13;
     Vista la dichiarazione di intervento del Presidente  della  Regione  &#13;
 siciliana:                                                               &#13;
     udita  nell'udienza  pubblica  dell'11 aprile 1962 la relazione del  &#13;
 Giudice Giuseppe Chiarelli;                                              &#13;
     udito il sostituto avvocato generale dello Stato Giuseppe Guglielmi  &#13;
 per il Presidente della Regione siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il Consiglio comunale di Venetico, in sede di decisione di  ricorsi  &#13;
 avverso  la  convalida di alcuni consiglieri comunali, approvava, nella  &#13;
 seduta del 5  giugno  1961,  la  proposta  del  consigliere  Romano  di  &#13;
 sollevare  la questione di legittimità costituzionale dell'art. 60 del  &#13;
 T.U. reg. 20 agosto 1960, n.  3,  il  quale  contempla  il  potere  del  &#13;
 Consiglio comunale di giudicare su ricorsi in materia di eleggibilità.  &#13;
 Il  proponente rilevava come la Regione siciliana, a norma dell'art. 14  &#13;
 dello  Statuto  speciale,  non  abbia  il  potere  di costituire organi  &#13;
 giurisdizionali, perché, non solo la materia è devoluta  allo  Stato,  &#13;
 ma  è  in  aperto  conflitto  con  l'art.   102 della Costituzione. Il  &#13;
 Consiglio comunale, ritenuta la validità della convocazione,  che  era  &#13;
 stata  contestata da alcuni consiglieri, deliberava nel senso della non  &#13;
 manifesta infondatezza della questione e ordinava la trasmissione degli  &#13;
 atti a questa Corte.                                                     &#13;
     Si è costituito in giudizio il Presidente della Regione siciliana,  &#13;
 rappresentato e difeso dall'Avvocatura generale dello Stato, la  quale,  &#13;
 con  atto  10  luglio  1961,  ha  dedotto  che la Regione stessa non ha  &#13;
 istituito  alcun  giudice  speciale,  né  ha  legiferato  in   materia  &#13;
 giudiziaria,  in  quanto,  riunendo  in  testo  unico  norme  statali e  &#13;
 regionali, ha integralmente  riprodotto,  menzionandone  la  fonte,  le  &#13;
 norme  delle  leggi  nazionali  sul contenzioso elettorale. L'impugnato  &#13;
 art. 60, infatti, è identico all'art. 60 del T.U.  reg. 7 aprile 1960,  &#13;
 n. 1, emanato in base all'art. 7 della legge regionale 9 marzo 1959, n.  &#13;
 3, e riproduce letteralmente l'art. 43 della legge nazionale  23  marzo  &#13;
 1956,  n.  136  (ora  art.  82  T.U.  16  maggio 1960, n.   570). Esso,  &#13;
 pertanto, come il detto art. 82 del T.U.  nazionale, non contrasta  con  &#13;
 l'art.  102  della  Costituzione,  perché  non  istituisce  un giudice  &#13;
 speciale, che preesisteva all'entrata in vigore della Costituzione, né  &#13;
 con la VI disp. trans., dato il riconosciuto carattere ordinatorio  del  &#13;
 termine  ivi  previsto.  La  difesa  della  Regione rileva anche che la  &#13;
 potestà degli organi elettivi (politici o amministrativi) di giudicare  &#13;
 dei  titoli  di  ammissione  dei  propri  componenti,  e  delle   cause  &#13;
 sopraggiunte  di  ineleggibilità  e incompatibilità, corrisponde a un  &#13;
 tradizionale  principio  democratico,  enunciato  per   il   Parlamento  &#13;
 dall'art.  66  della  Costituzione,  ed  è espressione della autonomia  &#13;
 delle Regioni e degli enti locali (artt. 115 e 128 della Costituzione).  &#13;
 Conclude, quindi, per la dichiarazione di infondatezza della questione.  &#13;
     In una memoria depositata il 2 marzo 1962 l'Avvocatura dello  Stato  &#13;
 ha ribadito la tesi secondo la quale la Regione non ha esorbitato dalla  &#13;
 sua  competenza essendosi limitata a riprodurre, nella formulazione del  &#13;
 T.U., le  norme  di  legge  statale  che  disciplinano  il  contenzioso  &#13;
 elettorale.  Quanto  all'altro  profilo  della  sollevata  questione di  &#13;
 legittimità costituzionale, relativo a un asserito  contrasto  tra  la  &#13;
 norma  impugnata  e  l'art.  102  della  Costituzione,  la difesa della  &#13;
 Regione  eccepisce  la  inammissibilità  del  motivo,  in  quanto   il  &#13;
 denunciato  art.  60  non contiene una norma di legge regionale, bensì  &#13;
 una norma di legge statale, avente  vigore  in  Sicilia:  la  questione  &#13;
 doveva  perciò  essere  promossa  nei  confronti  del  Presidente  del  &#13;
 Consiglio e la deliberazione doveva  essere  notificata  ai  Presidenti  &#13;
 delle   Camere.   Comunque,  soggiunge  la  memoria,  la  questione  è  &#13;
 manifestamente infondata per le ragioni  già  esposte  nel  precedente  &#13;
 atto d'intervento, le cui conclusioni si confermano.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  deliberazione  del Consiglio comunale di Venetico ha proposto a  &#13;
 questa Corte la questione della legittimità costituzionale della norma  &#13;
 contenuta nell'art. 60 del T.U. approvato con  decreto  del  Presidente  &#13;
 della Regione siciliana 20 agosto 1960, n. 3, corrispondente alla norma  &#13;
 di  cui  all'art.    43  della legge dello Stato 23 marzo 1956, n. 136,  &#13;
 considerando la norma impugnata come norma di legge regionale.  Se  non  &#13;
 che alla detta norma non può riconoscersi tale natura.                  &#13;
     A  prescindere  dalle  questioni  sul  valore  dei  testi  unici in  &#13;
 generale, nel caso specifico appare fuori dubbio che  la  riproduzione,  &#13;
 nel  Testo  unico  approvato  dal Presidente della Regione, della norma  &#13;
 contenuta nell'art. 43 della citata legge dello Stato, non  può  avere  &#13;
 avuto   efficacia  innovativa  della  norma  statale,  con  conseguente  &#13;
 trasformazione di essa in norma regionale. Infatti, tale trasformazione  &#13;
 non sarebbe potuta avvenire, in ipotesi, che con legge della Regione  o  &#13;
 con  suo  atto  avente  forza  di  legge;  nella  specie,  con  decreto  &#13;
 legislativo, in quanto tale natura si riconoscesse al  testo  unico  in  &#13;
 parola.  Ma,  a parte che, come questa Corte ha già avuto occasione di  &#13;
 affermare (sent. n. 32 del 1961), la potestà legislativa della Regione  &#13;
 non può esplicarsi nella forma del decreto legislativo, i caratteri di  &#13;
 questo non si riscontrano nel Testo unico in oggetto, nel quale  si  è  &#13;
 proceduto  a una semplice compilazione con modifiche puramente formali,  &#13;
 delle disposizioni in materia di  elezioni  comunali,  contenute  nelle  &#13;
 leggi  dello  Stato  e  in  precedenti  leggi  della  Regione. Né alla  &#13;
 trascrizione in tale Testo delle norme di leggi statali puo attribuirsi  &#13;
 efficacia  di  recezione  di  queste  nella   legislazione   regionale,  &#13;
 giacché, a parte l'inidoneità del mezzo adoperato, è ben noto che le  &#13;
 leggi  dello  Stato  non hanno bisogno di recezione per essere efficaci  &#13;
 nella Regione, con la conseguenza che i poteri del  Consiglio  comunale  &#13;
 in  materia  di  contenzioso  elettorale  traggono direttamente il loro  &#13;
 titolo dalla legge dello Stato.                                          &#13;
     Dalle ragioni innanzi esposte si deduce che oggetto della  presente  &#13;
 impugnativa  è  un  atto  della  Regione che non ha forza di legge; la  &#13;
 proposta  questione  di  legittimità  costituzionale   è,   pertanto,  &#13;
 inammissibile.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 60 del Testo unico delle leggi per la elezione  dei  Consigli  &#13;
 comunali  nella Regione siciliana, approvato con decreto del Presidente  &#13;
 della Regione 20 agosto 1960, n. 3, proposta con deliberazione 5 giugno  &#13;
 1961, n. 8, dal Consiglio comunale di Venetico.                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 5 giugno 1962.                                &#13;
                                   GASPARE  AMBROSINI  - MARIO COSATTI -  &#13;
                                   FRANCESCO   PANTALEO    GABRIELI    -  &#13;
                                   GIUSEPPE  CASTELLI  AVOLIO - ANTONINO  &#13;
                                   PAPALDO - GIOVANNI CASSANDRO - BIAGIO  &#13;
                                   PETROCELLI -  ANTONIO  MANCA  -  ALDO  &#13;
                                   SANDULLI  - GIUSEPPE BRANCA - MICHELE  &#13;
                                   FRAGALI  -   COSTANTINO   MORTATI   -  &#13;
                                   GIUSEPPE CHIARELLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
