<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>115</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:115</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ZAGREBELSKY</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>05/04/2004</data_decisione>
    <data_deposito>08/04/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 337 del codice di procedura penale, promosso, nell'ambito di un procedimento penale, dal Tribunale di Monza con ordinanza in data 8 novembre 2002, iscritta al n. 192 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 15, prima serie speciale, dell'anno 2003. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 25 febbraio 2004 il Giudice relatore Guido Neppi Modona.  &#13;
    Ritenuto che il Tribunale di Monza ha sollevato, in riferimento agli artt. 2, 3 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 337 del codice di procedura penale, nella parte in cui prescrive che la querela, ove recapitata da un incaricato o spedita per posta in piego raccomandato, debba essere corredata della «sottoscrizione autentica» del querelante, in quanto «preclude al giudice di ritenere comunque provata l'originaria volontà della persona offesa di sporgere querela»; &#13;
    che, quanto alla rilevanza, il giudice a quo, che procede per reati perseguibili a querela, premette: &#13;
- che la querela è stata recapitata per conto del querelante da un incaricato, a norma del secondo periodo del  comma 1 dell'art. 337 cod. proc. pen.; &#13;
- che la sottoscrizione del querelante non è autenticata;  &#13;
- che in dibattimento la persona offesa ha confermato la propria volontà di perseguire l'autore del fatto illecito, ma la difesa dell'imputato ha chiesto di pronunciare sentenza ai sensi dell'art. 469 cod. proc. pen. perché l'azione penale non poteva essere iniziata per difetto di valida querela; &#13;
    che, alla luce della giurisprudenza di legittimità, che ha affermato che l'espressione «sottoscrizione autentica» va intesa nel senso di «sottoscrizione autenticata», il Tribunale rimettente ravvisa la ratio della disposizione censurata nell'esigenza di garantire «la provenienza dell'atto dal titolare del diritto di sporgere querela, all'evidente fine di evitare l'inutile attivazione della giurisdizione penale», e rileva che tale ratio non ricorre quando il procedimento sia già pervenuto alla fase processuale «e il giudice possa evincere con sicurezza la volontà della persona offesa di perseguire penalmente l'autore dell'illecito»; &#13;
    che il giudice a quo ritiene di non poter adottare una soluzione interpretativa conforme alla predetta ratio, sia perché «confliggente prima facie col tenore della disposizione», sia perché «nulla assicurerebbe che altri giudicanti condividano detta interpretazione, con il conseguente rischio di trattamenti difformi da processo a processo»; &#13;
    che la disposizione in esame sarebbe perciò in contrasto con l'art. 3 Cost., perché, mentre in presenza di una querela validamente proposta, con sottoscrizione autenticata, nulla impedisce al giudice di dubitare «che l'atto esprima la reale volontà della persona offesa di perseguire penalmente l'autore dell'illecito», in mancanza della autenticazione della sottoscrizione al giudice è irragionevolmente precluso ogni accertamento circa l'effettiva sussistenza della volontà punitiva; &#13;
    che la disciplina censurata contrasterebbe inoltre con gli artt. 2 e 24 Cost., perché pregiudica l'interesse del querelante ad agire in giudizio «per chiedere giustizia a fronte del torto subito»; &#13;
    che ad avviso del rimettente il contrasto con gli indicati parametri costituzionali sussisterebbe quindi solo nell'ipotesi in cui l'originaria volontà della persona offesa, già risultante dalla sottoscrizione (pur non autenticata) della querela, risulti provata nel corso del procedimento; &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile, risolvendosi in un problema interpretativo, e comunque infondata, perché la disposizione censurata risponde a un'esigenza di certezza in ordine alla provenienza dell'atto sottratta ad ogni valutazione discrezionale da parte del giudice. &#13;
    Considerato che il Tribunale di Monza dubita, in riferimento agli artt. 2, 3 e 24 della Costituzione, della legittimità costituzionale dell'art. 337 del codice di procedura penale, nella parte in cui stabilisce che la querela, ove recapitata da un incaricato o spedita per posta in piego raccomandato, debba essere corredata dalla «sottoscrizione autentica» del querelante; &#13;
    che il rimettente - rilevato che alla luce di un consolidato orientamento giurisprudenziale per sottoscrizione autentica deve intendersi sottoscrizione autenticata - ritiene che la disciplina censurata violi l'art. 3 Cost. in quanto irragionevolmente «preclude al giudice di ritenere comunque provata l'originaria volontà della persona offesa di sporgere querela» anche nel caso in cui tale volontà, sia pure espressa mediante un atto recante una sottoscrizione non autenticata, risulti poi effettivamente confermata in dibattimento, mentre in presenza di una querela proposta validamente al giudice è consentito verificare che «l'atto esprima la reale volontà della persona offesa di perseguire penalmente l'autore dell'illecito»;  &#13;
    che risulterebbero violati anche gli artt. 2 e 24 Cost., perché verrebbe sacrificato il diritto del querelante di agire in giudizio per la tutela dei propri interessi; &#13;
    che l'interpretazione a cui aderisce il ricorrente circa l'autenticazione della sottoscrizione dell'atto di querela è stata condivisa da questa Corte con la sentenza n. 287 del 1995, che ha dichiarato infondata analoga questione di legittimità costituzionale sollevata in riferimento agli artt. 24 e 112 Cost., in base al rilievo che il legislatore ha inteso evitare che la giurisdizione penale, in mancanza di qualsiasi verifica circa l'autenticità della sottoscrizione del querelante, «possa mettersi inutilmente in movimento», e che la disposizione censurata introduce quindi una «ragionevole cautela resa necessaria dal mancato contatto tra il querelante e gli uffici deputati alla ricezione dell'atto»; &#13;
    che il rimettente, nel prospettare la questione anche in riferimento all'art. 3 Cost., pone erroneamente sullo stesso piano i requisiti formali della querela, tra i quali, allorché l'atto non venga presentato personalmente dal querelante, rientra la sottoscrizione autenticata, e il suo contenuto sostanziale, che deve appunto esprimere la volontà della persona offesa di perseguire penalmente l'autore del reato, volontà il cui accertamento presuppone che l'atto sia conforme ai requisiti formali richiesti dalla legge; &#13;
    che, essendo privi di fondamento i motivi posti a sostegno della presunta irragionevolezza della disciplina censurata e non avendo questa Corte ragione di discostarsi dalle conclusioni cui è pervenuta con la sentenza n. 287 del 1995, la questione deve essere dichiarata manifestamente infondata in relazione a tutti i parametri evocati dal rimettente. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 337 del codice di procedura penale, sollevata, in riferimento agli artt. 2, 3 e 24 della Costituzione, dal Tribunale di Monza, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 aprile 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria l'8 aprile 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
