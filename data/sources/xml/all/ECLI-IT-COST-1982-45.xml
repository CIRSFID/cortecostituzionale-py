<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>45</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:45</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1982</data_decisione>
    <data_deposito>16/02/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - &#13;
 Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 4, 5, 6,  &#13;
 lett. a, 8, ultimo comma, e 22, terzo  comma,  della  legge  22  maggio  &#13;
 1978,   n.  194  (Norme  per  la  tutela  sociale  della  maternità  e  &#13;
 sull'interruzione volontaria della gravidanza), promosso con  ordinanza  &#13;
 emessa  il  5  ottobre  1978 dal Tribunale di Firenze, nel procedimento  &#13;
 penale a carico di D'Alessandro Felice ed altri, iscritta al n. 959 del  &#13;
 registro ordinanze 1979 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 57 del 27 febbraio 1980.                                   &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del  22  ottobre  1981  il  Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
     Ritenuto  che  il  Tribunale  di  Firenze con ordinanza emessa il 5  &#13;
 ottobre 1978 nel procedimento penale a carico di D'Alessandro Felice ed  &#13;
 altri, solleva questione di legittimità costituzionale degli artt. 4 e  &#13;
 5, 8, ultimo comma, 22, terzo comma, della legge  22  maggio  1978,  n.  &#13;
 194,  in  riferimento agli artt. 2, 3, 29, 31 e 37 Cost., deducendo che  &#13;
 dette  norme  -  concernenti  il  trattamento   penale   degli   aborti  &#13;
 infratrimestrali,  commessi  prima dell'entrata in vigore della attuale  &#13;
 normativa - vulnerano gli invocati precetti costituzionali,  posti,  si  &#13;
 assume, a tutela del diritto alla vita del concepito;                    &#13;
     che  analogo  vizio di illegittimità è prospettato nell'ordinanza  &#13;
 di  rimessione  con  riguardo  al  disposto  dell'art.  6,  lettera  al  &#13;
 richiamato  dall'art.  22  in  relazione alla non punibilità dei fatti  &#13;
 progressi, posti in essere dopo il novantesimo  giorno  di  gestazione:  &#13;
 affermando  il  giudice  a quo a questo riguardo che la liceità penale  &#13;
 dell'aborto,  nella  fase  ultratrimestrale  della  gravidanza,   viene  &#13;
 subordinata  all'accertata  sussistenza  di  un periodo configurato dal  &#13;
 legislatore come grave per la vita  della  madre,  ma  non  anche  come  &#13;
 altrimenti  inevitabile,  laddove  ciò  sarebbe richiesto dai precetti  &#13;
 costituzionali  che  si  assumono  lesi,  e  dalle  prescrizioni  della  &#13;
 sentenza n. 27/1975 di questa Corte;                                     &#13;
     che - sotto altro angolo visuale, diverso da quello testé indicato  &#13;
 -  il  giudice  a quo censura poi l'art. 22 in riferimento al principio  &#13;
 costituzionale di eguaglianza. Si assume invero che il fatto progresso,  &#13;
 al quale il legislatore attribuisce perdurante rilevanza penale (sempre  &#13;
 che non risulti  accertata  la  sussistenza  delle  condizioni  di  non  &#13;
 punibilità  stabilite, secondo lo stadio della gestazione, nell'art. 4  &#13;
 ovvero art. 6), debba essere escluso dall'ambito applicativo  dell'art.  &#13;
 19,  che  sanziona  penalmente  le  infrazioni  della presente legge, e  &#13;
 rimanga così soggetto al più severo trattamento dettato dall'art. 546  &#13;
 del c.p. per il reato di aborto di  donna  consenziente:  denunciandosi  &#13;
 come  ingiustificata  la  detta  disparità  di  disciplina  dei  fatti  &#13;
 pregressi rispetto a quelli attuali;                                     &#13;
     ritenuto altresì che il Presidente del Consiglio, costituitosi  in  &#13;
 giudizio   per   il   tramite   dell'Avvocatura   dello  Stato,  deduce  &#13;
 l'irrilevanza delle censure concernenti gli artt.  5 e 8, ultimo comma,  &#13;
 della legge 22 maggio 1978, n.  194,  eccependo  l'inapplicabilità  di  &#13;
 tali  disposizioni nel caso di specie, e l'infondatezza delle questioni  &#13;
 di legittimità che investono le altre norme censurate;                  &#13;
     considerato che la questione, in quanto concerne gli artt.  5 e  8,  &#13;
 ultimo  comma,  della  legge  n.  194,  va dichiarata inammissibile per  &#13;
 difetto di rilevanza, come dedotto dall'Avvocatura  dello  Stato  (cfr.  &#13;
 sentenza n. 108/1981);                                                   &#13;
     che le questioni di legittimità costituzionale dell'art. 22, terzo  &#13;
 comma,  nonché  dell'art. 4 della legge, ivi richiamato, sono state in  &#13;
 altro giudizio dichiarate inammissibili, in quanto l'esame di  esse  è  &#13;
 precluso  dal  principio  di legalità consacrato nell'art. 25, secondo  &#13;
 comma,  Cost.,  comportando  un'eventuale   pronuncia   di   fondatezza  &#13;
 l'estensione  al  fatto  progresso  infratrimestrale  del  distinto  ed  &#13;
 autonomo regime previsto per l'interruzione  della  gravidanza  dopo  i  &#13;
 primi  novanta giorni di gestazione, e così l'insorgenza di una regola  &#13;
 incriminatrice nuova, la cui produzione è riservata al legislatore;     &#13;
     che  identica  ratio  decidendi  impone   di   ritenere   parimenti  &#13;
 inammissibile  la  questione prospettata nel presente caso in relazione  &#13;
 ai fatti progressi ultratrimestrali: invero, la  statuizione  dell'art.  &#13;
 6,  lett.  a, viene denunciata dal giudice a quo solamente in quanto il  &#13;
 legislatore  definisce  come  grave,  ma  non  anche  come   altrimenti  &#13;
 inevitabile, il pericolo per la vita della madre, la cui sussistenza va  &#13;
 accertata  prima  che il fatto pregresso risulti non punibile: per modo  &#13;
 che - dove, anche qui, fosse ritenuta la fondatezza della  questione  -  &#13;
 la  pronuncia  della Corte implicherebbe necessariamente la modifica (e  &#13;
 l'ampliamento)  della  fattispecie  delittuosa,  laddove  proprio  tale  &#13;
 risultato  resta  necessariamente  precluso  dal precetto dell'art. 25,  &#13;
 secondo comma, Cost.;                                                    &#13;
     che la questione proposta per denunciare il  contrasto  tra  l'art.  &#13;
 22,  terzo  comma, e l'art. 3, primo comma, Cost. è inammissibile alla  &#13;
 stregua dei  criteri  adottati  dalla  Corte  in  analoghi  casi  (cfr.  &#13;
 sentenza  n. 108/1981), avendo il giudice a quo trascurato di delibarne  &#13;
 e motivarne la rilevanza come esige il vigente ordinamento;              &#13;
     che la Corte non ravvisa ragioni per discostarsi dalle soluzioni in  &#13;
 precedenza adottate.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  manifestamente inammissibile la questione di legittimità  &#13;
 costituzionale degli artt. 4 e 5, 8, ultimo  comma,  22,  terzo  comma,  &#13;
 della  legge  22  maggio  1978,  n.  194,  sollevata  con  ordinanza in  &#13;
 epigrafe, in riferimento agli artt.  2, 3, 29, 31 e 37 Cost.             &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 22 gennaio 1982.        &#13;
                                   F.to:   LEOPOLDO   ELIA   -   EDOARDO  &#13;
                                   VOLTERRA - MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO -  GUGLIELMO  ROEHRSSEN  -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI.                      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
