<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2019</anno_pronuncia>
    <numero_pronuncia>190</numero_pronuncia>
    <ecli>ECLI:IT:COST:2019:190</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LATTANZI</presidente>
    <relatore_pronuncia>Franco Modugno</relatore_pronuncia>
    <redattore_pronuncia>Franco Modugno</redattore_pronuncia>
    <data_decisione>03/07/2019</data_decisione>
    <data_deposito>18/07/2019</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Giorgio LATTANZI; Giudici : Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Luca ANTONINI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, commi 4 e 5; 2, comma 2; 3 e 4, comma 1, della legge della Provincia autonoma di Bolzano 20 settembre 2012, n. 15 (Istituzione del repertorio toponomastico provinciale e della consulta cartografica provinciale), promosso dal Presidente del Consiglio dei ministri, con ricorso notificato il 26-29 novembre 2012, depositato in cancelleria il 4 dicembre 2012, iscritto al n. 182 del registro ricorsi 2012 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 3, prima serie speciale, dell'anno 2013. &#13;
 Visto l'atto di costituzione della Provincia autonoma di Bolzano; &#13;
 udito nella camera di consiglio del 3 luglio 2019 il Giudice relatore Franco Modugno.&#13;
 Ritenuto che con ricorso notificato il 26-29 novembre 2012 e depositato in cancelleria il 4 dicembre 2012 (r.r. n. 182 del 2012), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso, ai sensi dell'art. 127 della Costituzione, questioni di legittimità costituzionale degli artt. 1, commi 4 e 5; 2, comma 2; 3 e 4, comma 1, della legge della Provincia autonoma di Bolzano 20 settembre 2012, n. 15 (Istituzione del repertorio toponomastico provinciale e della consulta cartografica provinciale), per contrasto: con gli artt. 3; 16; 117, primo comma, e 120, primo comma, della Costituzione, con gli artt. 1, numero 2) (recte: art. 2); 8, numero 2); 56, 99; 101 e 102 del d.P.R. 31 agosto 1972, n. 670 (Approvazione del testo unico delle leggi costituzionali concernenti lo statuto speciale per il Trentino-Alto Adige); con l'art. 7 del d.P.R. 22 marzo 1974, n. 279 (Norme di attuazione dello statuto speciale per la regione Trentino-Alto Adige in materia di minime proprietà colturali, caccia e pesca, agricoltura e foreste); nonché, infine, con l'art. 4, comma 4, del d.P.R. 15 luglio 1988, n. 574 (Norme di attuazione dello Statuto speciale per la regione Trentino-Alto Adige in materia di uso della lingua tedesca e della lingua ladina nei rapporti con la pubblica amministrazione e nei procedimenti giudiziari);&#13;
 che il ricorrente rileva che l'art. 1, comma 4, della legge provinciale impugnata, prevedendo che «[o]gni toponimo è raccolto nelle versioni in lingua tedesca, italiana e ladina, in quanto in uso in ciascuna di tali lingue a livello di comunità comprensoriale», consente che «in futuro alcuni toponimi possano essere solamente monolingui e, in particolare, che quelli in lingua italiana già previsti dalla legislazione statale in vigore possano essere eliminati dalla toponomastica ufficiale» sulla base del criterio dell'uso a livello di comunità comprensoriale; &#13;
 che la disposizione impugnata si porrebbe, in tal modo, in contrasto con il principio del separatismo linguistico previsto dallo statuto speciale e riconosciuto anche dalla giurisprudenza di questa Corte (si richiamano le sentenze n. 159 del 2009 e n. 188 del 1987), oltre che con l'art. 117, primo comma, Cost. e con l'art. 1, comma 2, lettera b), dell'Accordo di Parigi del 5 settembre 1946 (Allegato n. IV del Trattato di pace di Parigi del 10 febbraio 1947);&#13;
 che la predetta disposizione sarebbe illegittima perché ricorre al criterio dell'uso anche ai fini del riconoscimento dei toponimi ufficiali italiani, mentre tale criterio sarebbe contemplato dallo statuto speciale esclusivamente per l'accertamento dell'esistenza e l'approvazione della dizione, da parte della legge provinciale, dei toponimi in lingua tedesca;&#13;
 che l'impugnato art. 1, comma 4, anche laddove lo si interpretasse nel senso che i toponimi italiani «rimangono comunque intangibili», sarebbe in contrasto con l'art. 8 dello statuto speciale e gli altri parametri evocati, nella parte in cui restringe la verifica dell'uso al livello di comunità comprensoriali, le quali, invece, non avrebbero alcuna competenza in materia di toponomastica;&#13;
 che, inoltre, la disposizione oggetto di censura, adottando il livello di comunità comprensoriale quale criterio di ricognizione dell'uso toponomastico, contrasterebbe con gli artt. 8, 101 e 102 dello statuto speciale, i quali prevedono la bilinguità sull'intero territorio della Provincia autonoma di Bolzano e non soltanto su parte di esso;&#13;
 che le censure formulate nei confronti dell'art. 1, comma 4, sono estese anche all'art. 1, comma 5, della legge prov. Bolzano n. 15 del 2012, che attribuisce al consiglio delle comunità comprensoriali il potere di trasmettere al comitato cartografico, di cui all'art. 3 della medesima legge provinciale, le proposte di inserimento nel repertorio dei toponimi: competenza, questa, che risulterebbe eccedente le funzioni istituzionali delle comunità comprensoriali, rivelandosi pertanto irragionevole e «contraria al principio di inscindibilità del territorio provinciale ai fini toponomastici» enunciato nello statuto;&#13;
 che il ricorrente censura l'art. 1, commi 4 e 5, anche sotto il profilo della violazione degli artt. 16 e 120, primo comma, Cost., in quanto l'istituzione di un nuovo sistema di ricognizione della toponomastica, fondato sul criterio dell'uso comprensoriale, renderebbe «il sistema instabile», creando ostacoli alla libera circolazione delle persone e barriere territoriali a livello provinciale e infraprovinciale; &#13;
 che il Presidente del Consiglio dei ministri ha, altresì, impugnato l'art. 3 della legge prov. Bolzano n. 15 del 2012, poiché la funzione di accertare e approvare i toponimi, che la disposizione impugnata attribuisce al consiglio comprensoriale e al comitato cartografico, sarebbe riservata, dall'art. 101 dello statuto speciale, alla legge provinciale;&#13;
 che la suddetta disposizione è censurata anche perché viola il principio di parità dei gruppi linguistici e di salvaguardia delle rispettive caratteristiche etniche e culturali, sancito dall'art. 2 dello statuto speciale;&#13;
 che, infatti, la composizione del comitato cartografico e la modalità di assunzione delle relative decisioni non potrebbero considerarsi paritarie, da un lato, perché, in combinazione con il principio maggioritario stabilito per l'adozione delle deliberazioni, consentirebbero la formazione di alleanze tra due gruppi linguistici; dall'altro, perché la previsione per cui le adunanze sono valide con la presenza della maggioranza dei componenti e le decisioni sono adottate a maggioranza assoluta dei presenti renderebbe sufficiente la presenza dei rappresentanti di due gruppi linguistici per assumere decisioni che possono riguardare anche (o soltanto) il terzo gruppo;&#13;
 che, infine, il Presidente del Consiglio dei ministri censura gli artt. 2, comma 2, e 4, comma 1, della legge prov. Bolzano n. 15 del 2012, i quali, nel prevedere che «l'ordine di precedenza [con cui sono registrati i toponimi] è dato dalla consistenza dei gruppi linguistici nei luoghi di pertinenza, risultante dall'ultimo censimento generale della popolazione», violerebbero l'art. 4, comma 4, del d.P.R. n. 574 del 1988, che vieta qualsiasi ordine gerarchico o di precedenza tra le indicazioni nelle due (o tre) lingue;&#13;
 che, con atto depositato in cancelleria il 28 dicembre 2012, si è costituita in giudizio la Provincia autonoma di Bolzano, chiedendo che sia dichiarata la manifesta inammissibilità o la manifesta infondatezza di tutte le questioni di legittimità costituzionale proposte;&#13;
 che, dopo diversi rinvii dell'udienza pubblica richiesti dalle parti, in ragione dell'accordo fra queste ultime volto a superare i profili di incostituzionalità lamentati dal ricorrente, la legge provinciale impugnata è stata abrogata dalla legge della Provincia autonoma di Bolzano 23 aprile 2019, n. 1 (Abrogazione della legge provinciale 20 settembre 2012, n. 15, "Istituzione del repertorio toponomastico provinciale e della consulta cartografica provinciale" e altre disposizioni).&#13;
 Considerato che, in data 24 giugno 2019, il Presidente del Consiglio dei ministri, su conforme deliberazione del Consiglio dei ministri del 19 giugno 2019, ha depositato atto di rinuncia al ricorso, «avendo la Provincia autonoma di Bolzano abrogato con legge provinciale n. 1/2019 la legge provinciale impugnata»;&#13;
 che, in data 2 luglio 2019, la Provincia autonoma di Bolzano, su conforme deliberazione della Giunta provinciale del 25 giugno 2019, ha depositato atto di accettazione della predetta rinuncia al ricorso;&#13;
 che la rinuncia al ricorso accettata dalla controparte costituita determina, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.&#13;
 Visti l'art. 26, secondo comma, della legge 11 marzo 1953, n. 87, e gli artt. 9, comma 2, e 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara estinto il processo.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 luglio 2019.&#13;
 F.to:&#13;
 Giorgio LATTANZI, Presidente&#13;
 Franco MODUGNO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 18 luglio 2019.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
