<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>638</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:638</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/1988</data_decisione>
    <data_deposito>10/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, &#13;
 prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 77 della legge    &#13;
 24 novembre 1981, n. 689 (Modifiche al sistema penale), promosso  con    &#13;
 ordinanza emessa l'8 marzo 1985 dal Pretore di Asolo nel procedimento    &#13;
 penale a carico  di  Giromella  Giuliano,  iscritta  al  n.  166  del    &#13;
 registro  ordinanze  1986 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 86, prima serie speciale dell'anno 1986;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 20 aprile 1988 il Giudice    &#13;
 relatore Ettore Gallo.                                                   &#13;
    Ritenuto che il Pretore di Asolo, con ord. 8 marzo 1985, sollevava    &#13;
 questione di legittimità costituzionale dell'art. 77 della legge  24    &#13;
 novembre 1981, n. 689, nella parte in cui - a suo avviso - renderebbe    &#13;
 inapplicabili le sanzioni sostitutive ai reati  punibili  a  querela,    &#13;
 oppure,  qualora invece le si ritenga applicabili, nella parte in cui    &#13;
 - sempre a suo avviso - aggraverebbe la posizione della parte civile,    &#13;
 in  quanto  il giudice dovrebbe limitarsi ad una generica condanna al    &#13;
 risarcimento  del  danno,  e  al  più  alla   concessione   di   una    &#13;
 provvisionale,  non potendo proseguire il giudizio per l'accertamento    &#13;
 della integrale consistenza  del  danno:  situazioni  che  -  secondo    &#13;
 l'ordinanza - violerebbero gli articoli 3, 24 e 25 Cost.                 &#13;
    Considerato  che la questione è manifestamente inammissibile già    &#13;
 per  il  modo  in  cui  viene  proposta   in   quanto,   prospettando    &#13;
 l'illegittimità  in  via alternativa, non viene individuato il thema    &#13;
 decidendi, come invece si esige per costante giurisprudenza di questa    &#13;
 Corte  (cfr.  da  ultimo sent. 28 gennaio 1983 n. 30), determinandosi    &#13;
 un'ambivalenza della questione che non può essere superata in questa    &#13;
 sede;                                                                    &#13;
      che  a  tale  pronuncia, pertanto, devesi allo stato addivenire,    &#13;
 prescindendo  dalla  pure  manifesta  infondatezza  della  questione,    &#13;
 innanzitutto  perché  il  pretore  non  dice  donde abbia attinto la    &#13;
 strana   interpetrazione   dell'inapplicabilità    delle    sanzioni    &#13;
 sostitutive  quando  si proceda a querela di parte: tesi smentita dal    &#13;
 dato testuale proprio in tema di lesioni colpose, dato che l'art.  60    &#13;
 della  legge  impugnata  esclude soltanto i reati di lesioni colpose,    &#13;
 previsti dal secondo e terzo comma dell'art.  590,  limitatamente  ai    &#13;
 fatti   commessi  con  violazione  delle  norme  per  la  prevenzione    &#13;
 degl'infortuni sul lavoro o relative all'igiene del lavoro, e  sempre    &#13;
 che  abbiano  prodotto le gravi conseguenze ivi richiamate: fatti che    &#13;
 peraltro integrano  proprio  le  ipotesi  per  le  quali  si  procede    &#13;
 d'ufficio;                                                               &#13;
      che  conseguentemente  in  ogni  altro  caso di lesioni colpose,    &#13;
 ricorrendo le condizioni di cui all'art. 53 della legge, le  sanzioni    &#13;
 sostitutive  sono  evidentemente applicabili, pur potendosi procedere    &#13;
 per detti reati soltanto  a  querela  di  parte,  come  espressamente    &#13;
 dispone l'u.p. dell'art. 590 cod. pen.;                                  &#13;
      che  d'altronde  anche  la  seconda alternativa d'illegittimità    &#13;
 sarebbe manifestamente  infondata  perché  l'interesse  della  parte    &#13;
 civile  non  può mai compromettere i diritti di difesa dell'imputato    &#13;
 nel processo penale: tanto più  poi  che  la  condanna  generica  al    &#13;
 risarcimento  del  danno  e  la  concessione  di una provvisionale è    &#13;
 quanto frequentemente si verifica senza che sia stato mai considerato    &#13;
 compromissorio  dei  diritti  della  parte civile. La quale inserisce    &#13;
 eccezionalmente l'azione civile  nel  processo  penale,  senza  però    &#13;
 poter   pretendere   di  alternare  il  corso  previsto  dalla  legge    &#13;
 processuale  penale,  dato  che  a  sua  disposizione   c'è   sempre    &#13;
 l'autonoma e principale azione civile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art.  77  della  legge  24  novembre    &#13;
 1981,  n.  689 (Modifiche al sistema penale) sollevata dal Pretore di    &#13;
 Asolo, con ord. 8 marzo 1985, con riferimento agli articoli 3,  24  e    &#13;
 25 Cost.                                                                 &#13;
    Così  deciso  in  Roma,  in Camera di Consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta,                            &#13;
 l'8 giugno 1988.                                                         &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 10 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
