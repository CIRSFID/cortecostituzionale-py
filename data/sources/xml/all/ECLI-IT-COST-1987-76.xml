<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>76</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:76</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/02/1987</data_decisione>
    <data_deposito>05/03/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, prof. Ettore GALLO, dott. &#13;
 Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 73 del regio    &#13;
 decreto 9 luglio 1939, n. 1238 (Ordinamento dello  stato  civile),  e    &#13;
 degli  articoli  6,  143-bis,  236,  237, secondo comma, 262, secondo    &#13;
 comma, del codice civile, promosso con ordinanza emessa il 21 gennaio    &#13;
 1985  dal  Tribunale di Lucca nel procedimento instaurato da Montauti    &#13;
 Amelia ed altro, iscritta al n. 177 del  registro  ordinanze  1985  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n. 149-bis    &#13;
 dell'anno 1985.                                                          &#13;
    Udito  nella  camera  di  consiglio del 28 gennaio 1987 il Giudice    &#13;
 relatore Giovanni Conso.                                                 &#13;
    Ritenuto  che  il  Tribunale  di Lucca, con decreto del 1° ottobre    &#13;
 1984, emesso su  ricorso  proposto  dai  coniugi  Amelia  Montauti  e    &#13;
 Giuseppe Zumbo per ottenere l'autorizzazione all'aggiunta del cognome    &#13;
 materno al cognome dei loro legittimi  figli  minori  Gian  Paolo  ed    &#13;
 Alessandro,   aveva   ordinato   all'ufficiale  di  stato  civile  di    &#13;
 rettificare l'atto di nascita dei detti  minori  con  l'aggiunta  del    &#13;
 cognome "Montauti";                                                      &#13;
      che,  a  seguito  di ricorso del Procuratore della Repubblica di    &#13;
 Lucca, la Corte d'appello di Firenze, con sentenza  del  1°  dicembre    &#13;
 1984,  aveva  dichiarato  la  "nullità  (inesistenza) della sentenza    &#13;
 emessa  dal  Tribunale  di  Lucca  in  forma  di  decreto",   perché    &#13;
 "sottoscritta   dal   solo   Presidente,  e  non  anche  dal  giudice    &#13;
 estensore", rimettendo le parti davanti allo stesso Tribunale a norma    &#13;
 degli articoli 353 e 354 del codice di procedura civile;                 &#13;
      che il Tribunale di Lucca, con ordinanza del 21 gennaio 1985, ha    &#13;
 denunciato,  in  riferimento  agli  articoli  2,   3   e   29   della    &#13;
 Costituzione,  l'illegittimità  dell'art.  73  del  regio  decreto 9    &#13;
 luglio 1939, n.  1238  (Ordinamento  dello  stato  civile),  e  degli    &#13;
 articoli 6, 143-bis, 236, 237, secondo comma, 262, secondo comma, del    &#13;
 codice civile, in quanto non prevedono per  il  figlio  legittimo  la    &#13;
 facoltà  di  assumere  anche  il  cognome  materno e per la madre la    &#13;
 facoltà di trasmettere ai figli legittimi il proprio cognome;           &#13;
    Considerato  che,  non  essendosi provveduto alla riassunzione del    &#13;
 processo ai sensi del combinato disposto degli  articoli  354,  primo    &#13;
 comma,  e  353,  secondo  comma,  del  codice di procedura civile, il    &#13;
 giudizio di legittimità costituzionale risulta promosso prima che il    &#13;
 Tribunale  di  Lucca  fosse reinvestito della causa, in pendenza, per    &#13;
 giunta, del termine per il ricorso in cassazione avverso la  sentenza    &#13;
 emessa dalla Corte d'appello di Firenze;                                 &#13;
      e  che,  pertanto, la questione appare proposta in via del tutto    &#13;
 eventuale e, comunque, prematuramente, con  conseguente  difetto  del    &#13;
 requisito  della  rilevanza  (v. sentenze n. 300 del 1983, n. 117 del    &#13;
 1984, n. 140 del 1984, n. 254 del 1985).                                 &#13;
    Visti  gli  articoli 26, secondo comma, della legge 11 marzo 1953,    &#13;
 n. 87, e 9 delle norme integrative per i giudizi davanti  alla  Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 dichiara   la   manifesta   inammissibilità   della   questione   di    &#13;
 legittimità costituzionale dell'art. 73 del regio decreto  9  luglio    &#13;
 1939,  n. 1238 (Ordinamento dello stato civile ), e degli articoli 6,    &#13;
 143-bis, 236, 237, secondo comma, e 262, secondo  comma,  del  codice    &#13;
 civile,  sollevata,  in  riferimento  agli  articoli  2, 3 e 29 della    &#13;
 Costituzione, dal Tribunale di Lucca con l'ordinanza in epigrafe.        &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 26 febbraio 1987.       &#13;
                       Il Presidente: LA PERGOLA                          &#13;
                       Il Redattore: CONSO                                &#13;
    Depositata in cancelleria il 5 marzo 1987.                            &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
