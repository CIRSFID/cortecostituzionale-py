<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>145</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:145</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/04/1998</data_decisione>
    <data_deposito>23/04/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. &#13;
 Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale &#13;
 MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 10 e 14 della    &#13;
 legge 14 ottobre 1974, n. 497 (Nuove norme contro  la  criminalità),    &#13;
 promosso  con  ordinanza  emessa  il  12 giugno 1997 dal tribunale di    &#13;
 Casale Monferrato, iscritta al n. 645 del registro ordinanze  1997  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 41, prima    &#13;
 serie speciale, dell'anno 1997.                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di consiglio dell'11 febbraio 1998 il giudice    &#13;
 relatore Carlo Mezzanotte;                                               &#13;
   Ritenuto che, nel corso di un  procedimento  a  carico  di  persona    &#13;
 imputata  del  reato di cui agli artt. 10 e 14 della legge 14 ottobre    &#13;
 1974,  n.  497  (Nuove  norme  contro  la  criminalità),  per  avere    &#13;
 illegalmente  detenuto  nella  propria abitazione un fucile monocanna    &#13;
 calibro 24 ed una carabina ad aria compressa calibro 4, il  tribunale    &#13;
 di  Casale  Monferrato,  con  ordinanza  in  data  12 giugno 1997, ha    &#13;
 sollevato, in riferimento agli artt. 3, 27, terzo comma, e 97,  primo    &#13;
 comma,  della  Costituzione, questione di legittimità costituzionale    &#13;
 dei citati artt. 10 e  14,  nella  parte  in  cui  non  prevedono  un    &#13;
 trattamento  sanzionatorio  differenziato, nel minimo e nella specie,    &#13;
 per  chi  illegalmente  detiene,  nello  stesso  luogo,   armi   già    &#13;
 denunciate  da  altri  e a lui pervenute iure successionis o ad altro    &#13;
 titolo,                                                                  &#13;
     che il remittente premette che, secondo il costante  orientamento    &#13;
 della  giurisprudenza  di legittimità, l'obbligo di denuncia incombe    &#13;
 anche su chi erediti un'arma già denunciata dal de cuius sicché  la    &#13;
 fattispecie  sottoposta  al suo esame (omessa ripetizione di denuncia    &#13;
 di armi pervenute iure  successionis  e  già  denunciate  dal  dante    &#13;
 causa)   ricade   nell'ambito   di  applicazione  delle  disposizioni    &#13;
 censurate;                                                               &#13;
     che, ad avviso  del  giudice  a  quo,  nel  caso  di  specie  "il    &#13;
 disvalore  sociale  della condotta" sarebbe "minimo in relazione alla    &#13;
 finalità cui è preordinata la  norma  incriminatrice,  identificata    &#13;
 dalla giurisprudenza nella possibilità per l'autorità di polizia di    &#13;
 conoscere  il  luogo  in  cui  le armi si trovano e le persone che ne    &#13;
 hanno la disponibilità, in modo da  rendere  agevoli  gli  opportuni    &#13;
 controlli e facilitare l'esecuzione di ordini di consegna per ragioni    &#13;
 di  sicurezza", poiché, quando l'arma continua a permanere nel luogo    &#13;
 già indicato nella denuncia e nella disponibilità di coabitanti del    &#13;
 denunciante, tale esigenza sarebbe comunque soddisfatta;                 &#13;
     che, secondo il remittente,  l'irragionevolezza  della  normativa    &#13;
 censurata quanto alla specie ed all'entità della pena (reclusione da    &#13;
 uno  a  otto  anni  e  multa  da  lire quattrocentomila a tremilioni,    &#13;
 ridotte di un terzo per le armi comuni da sparo) sarebbe  ancor  più    &#13;
 evidente  "ove  la si raffronti all'ipotesi della mancata ripetizione    &#13;
 della denuncia in caso di trasferimento delle armi,  già  denunciate    &#13;
 ai  sensi  dell'art.  38  del  testo  unico  delle  leggi di pubblica    &#13;
 sicurezza, da una località all'altra del  territorio  dello  Stato",    &#13;
 fattispecie  che,  secondo la più recente giurisprudenza della Corte    &#13;
 di cassazione, integra la contravvenzione  di  cui  all'art.  58  del    &#13;
 regolamento per l'esecuzione del citato testo unico;                     &#13;
     che  le  disposizioni  denunciate contrasterebbero, altresì, con    &#13;
 l'art. 27, terzo comma, della Costituzione, in quanto la sproporzione    &#13;
 fra l'entità della sanzione  penale  e  il  disvalore  dell'illecito    &#13;
 commesso   vanificherebbe   la   finalità  rieducativa  della  pena,    &#13;
 ingenerando   sentimenti   di   sfiducia   nella    legislazione    e    &#13;
 nell'autorità chiamata ad applicarla;                                   &#13;
     che,  infine,  ad  avviso  del  giudice a quo, le norme impugnate    &#13;
 violerebbero l'art. 97, primo comma, della Costituzione,  perché  il    &#13;
 minimo  e la specie della pena edittale comminata non consentirebbero    &#13;
 l'applicazione di sanzioni sostitutive pecuniarie ed  ostacolerebbero    &#13;
 la  definizione del procedimento in sede predibattimentale, imponendo    &#13;
 la  celebrazione  del  processo   con   i   conseguenti   costi   per    &#13;
 l'amministrazione della giustizia e per la collettività;                &#13;
     che  è  intervenuto  in giudizio il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, concludendo per l'infondatezza della questione;                   &#13;
   Considerato  che,  come  questa Corte ha già rilevato (sentenza n.    &#13;
 166  del  1982),  la  ratio  della  normativa   denunciata   consiste    &#13;
 nell'esigenza  che l'autorità locale di pubblica sicurezza sia posta    &#13;
 in grado di conoscere quali e quante armi si trovino  nel  territorio    &#13;
 di  sua  competenza,  nonché i luoghi in cui sono custodite e i nomi    &#13;
 dei detentori;                                                           &#13;
     che, in effetti, un reale ed efficace controllo in tema di  armi,    &#13;
 munizioni  e  materie  esplodenti  presuppone  adeguati  strumenti di    &#13;
 conoscenza dei luoghi in cui si trovano le armi e le persone  che  ne    &#13;
 hanno la disponibilità;                                                 &#13;
     che,  se  questo  è  il fine della disciplina, deve escludersene    &#13;
 l'irrazionalità e si deve riconoscere che  la  determinazione  delle    &#13;
 pene,  per  le  ipotesi  in  cui  la  possibilità  di  controllo sia    &#13;
 vanificata a causa della omissione in cui  sia  comunque  incorso  il    &#13;
 detentore  delle  armi, rientra pienamente nella discrezionalità che    &#13;
 spetta al legislatore;                                                   &#13;
     che, d'altra parte, è consentito al legislatore includere in uno    &#13;
 stesso modello di genere una pluralità di  fattispecie  diverse  per    &#13;
 struttura  e  disvalore:  in  questi  casi  sarà  il  giudice a fare    &#13;
 emergere la differenza tra le varie sottospecie in ragione  del  loro    &#13;
 diverso disvalore oggettivo ed a graduare su questa base, nell'ambito    &#13;
 delle pene edittali, quelle da irrogare in concreto (sentenza n.  285    &#13;
 del 1991);                                                               &#13;
     che,  sotto  questo aspetto, le disposizioni censurate permettono    &#13;
 una assai flessibile graduazione delle pene,  sia  per  il  rilevante    &#13;
 divario  tra il massimo e il minimo edittale, sia per la possibilità    &#13;
 di diminuirle in misura non eccedente i  due  terzi,  quando  per  la    &#13;
 quantità  o  per  la  qualità  delle  armi,  delle munizioni, degli    &#13;
 esplosivi o degli aggressivi chimici  il  fatto  debba  ritenersi  di    &#13;
 lieve   entità   (art.   5  della  legge  2  ottobre  1967,  n.  895    &#13;
 "Disposizioni per il controllo delle armi");                             &#13;
     che, escluso il vizio di irrazionalità, resta superata anche  la    &#13;
 denunciata  violazione dell'art. 27, terzo comma, della Costituzione,    &#13;
 non risultando  alcuna  sproporzione  tra  l'entità  della  sanzione    &#13;
 penale e il disvalore dell'illecito commesso;                            &#13;
     che,  per  quanto  riguarda  la supposta violazione dell'art. 97,    &#13;
 primo comma, della Costituzione, questa Corte ha più volte affermato    &#13;
 che il principio del buon andamento si riferisce  anche  agli  organi    &#13;
 dell'amministrazione   della   giustizia  esclusivamente  per  quanto    &#13;
 riguarda  l'ordinamento  degli   uffici   giudiziari   ed   il   loro    &#13;
 funzionamento   sotto   l'aspetto  amministrativo,  ma  non  riguarda    &#13;
 l'esercizio della funzione giurisdizionale  nel  suo  complesso  e  i    &#13;
 diversi  provvedimenti  che  ne  costituiscono  espressione (cfr., da    &#13;
 ultimo, sentenze nn. 385 e 122 del 1997; ordinanze nn. 189, 168,  103    &#13;
 e 7 del 1997);                                                           &#13;
     che, pertanto, la questione è manifestamente infondata;             &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 10 e 14 della legge                           &#13;
     14 ottobre 1974, n. 497 (Nuove norme contro la criminalità),        &#13;
                              sollevata,                                  &#13;
   in riferimento agli artt. 3, 27, terzo comma, e 97, primo comma,       &#13;
      della Costituzione, dal tribunale di Casale Monferrato con          &#13;
                              l'ordinanza                                 &#13;
                         indicata in epigrafe.                            &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 aprile 1998.                               &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Mezzanotte                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 aprile 1998.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
