<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>538</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:538</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/12/1995</data_decisione>
    <data_deposito>29/12/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale art. 19 legge 6 marzo    &#13;
 1987, n. 74 (Nuove norme sulla disciplina dei casi di scioglimento di    &#13;
 matrimonio), promosso con ordinanza emessa il 9 dicembre  1994  dalla    &#13;
 Commissione  tributaria di primo grado di Milano sul ricorso proposto    &#13;
 da Sacco Rosina contro l'Ufficio del Registro di Milano, iscritta  al    &#13;
 n.  168  del  registro  ordinanze  1995  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 14,  prima  serie  speciale,  dell'anno    &#13;
 1995;                                                                    &#13;
   Visto l'atto di costituzione di Sacco Rosina;                          &#13;
   Udito  nella  udienza  pubblica  del  12  dicembre  1995 il Giudice    &#13;
 relatore Renato Granata;                                                 &#13;
   Udito l'avv. Giovanni M. Migliori per Sacco Rosina;                    &#13;
   Ritenuto che la Commissione tributaria di primo grado di  Milano  -    &#13;
 nel procedimento promosso da Sacco Rosina con impugnativa dell'avviso    &#13;
 di  liquidazione  dell'Ufficio  del registro di Milano concernente le    &#13;
 imposte di registro, bollo, trascrizione, catasto e Invim relative al    &#13;
 trasferimento di proprietà di un immobile  in  sede  di  separazione    &#13;
 consensuale  tra  la  ricorrente  ed  il  coniuge - ha sollevato (con    &#13;
 ordinanza del 9 dicembre 1994) questione incidentale di  legittimità    &#13;
 costituzionale  dell'art.  19  della  legge 6 marzo 1987, n. 74 nella    &#13;
 parte in cui, in violazione degli artt. 3, 29, 31  e  53  Cost.,  non    &#13;
 comprende  nell'esenzione  dal  tributo anche le imposte di registro,    &#13;
 bollo, trascrizione, catasto e INVIM relative ad atti e documenti del    &#13;
 giudizio di separazione personale dei coniugi;                           &#13;
     che la Commissione in  particolare  evidenzia  -  richiamando  la    &#13;
 sentenza  di questa Corte n.176 del 1992 - le "profonde analogie e la    &#13;
 complementarità funzionale dei due procedimenti di  separazione  dei    &#13;
 coniugi  e di divorzio" e quindi ritiene che la perdurante differenza    &#13;
 di regime tributario quanto alle imposte oggetto del  giudizio  violi    &#13;
 il  principio di eguaglianza unitamente ai precetti costituzionali di    &#13;
 cui agli artt. 29, 31 e 53 Cost.;                                        &#13;
    Considerato che l'ordinanza di rimessione, in  ordine  all'oggetto    &#13;
 del  giudizio  a  quo, si limita a riferire trattarsi di "ricorso ...    &#13;
 avverso l'avviso  di  liquidazione  ...  concernente  le  imposte  di    &#13;
 registro,   bollo,   trascrizione,   catasto  ed  INVIM  relative  al    &#13;
 trasferimento di proprietà di un immobile  in  sede  di  separazione    &#13;
 consensuale  dei  coniugi  ...", e che, in relazione alla fattispecie    &#13;
 così descritta, ritiene "non manifestamente infondata e rilevante ai    &#13;
 fini della decisione la questione di costituzionalità  dell'art.  19    &#13;
 della  legge  6  marzo  1987, n. 74 nella parte in cui, in violazione    &#13;
 degli artt. 3, 29, 31 e 53 Cost., non comprende nella  esenzione  del    &#13;
 tributo  anche le imposte di registro, bollo, trascrizione, catasto e    &#13;
 INVIM relative ad  atti  e  documenti  del  giudizio  di  separazione    &#13;
 personale dei coniugi";                                                  &#13;
     che  in tal guisa argomentando, senza ulteriori delucidazioni sul    &#13;
 punto della  rilevanza,  il  giudice  a  quo  chiaramente  mostra  di    &#13;
 presupporre come dato certo che il "trasferimento di proprietà di un    &#13;
 immobile"  della cui tassazione si tratta, se posto in essere in sede    &#13;
 di giudizio di divorzio, avrebbe fruito della esenzione  dai  tributi    &#13;
 la  cui esazione costituisce oggetto della pretesa fiscale contestata    &#13;
 davanti alla Commissione rimettente;                                     &#13;
     che pertanto il giudice a quo, ancora per  implicito,  presuppone    &#13;
 che  il  precetto  normativo  dettato dall'art. 19 impugnato riguardi    &#13;
 ogni tipo di atto, in senso generale, di natura sia  processuale  sia    &#13;
 sostanziale   comunque   correlato  al  procedimento  di  "divorzio",    &#13;
 comprese le attribuzioni di beni da un coniuge all'altro qualunque ne    &#13;
 sia lo stato di proprietà - tanto, cioè, se in proprietà esclusiva    &#13;
 quanto se in comunione sia legale che ordinaria fra i coniugi - e che    &#13;
 il precetto stesso si riferisca ad ogni tipo di tributo: e ciò senza    &#13;
 affatto motivare in ordine a tale latissima ed onnicomprensiva  -  ma    &#13;
 per  nulla pacifica - lettura della citata disposizione, pur tuttavia    &#13;
 assunta a presupposto  della  assertiva  valutazione  positiva  della    &#13;
 rilevanza della questione proposta;                                      &#13;
     che  quindi l'ordinanza di rimessione si appalesa del tutto priva    &#13;
 di motivazione sul punto della rilevanza;                                &#13;
   Visti gli artt. 26, secondo comma, della legge 11 marzo 1953,  n.87    &#13;
 e  9,  secondo  comma,  delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara  la  manifesta  inammissibilità   della   questione   di    &#13;
 legittimità costituzionale dell'art. 19 della legge 6 marzo 1987, n.    &#13;
 74  (Nuove  norme  sulla  disciplina  dei  casi  di  scioglimento  di    &#13;
 matrimonio), sollevata, in riferimento agli artt.  3,  29,  31  e  53    &#13;
 della  Costituzione,  dalla  Commissione tributaria di primo grado di    &#13;
 Milano con l'ordinanza indicata in epigrafe.                             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 15 dicembre 1995.                             &#13;
  Il Presidente:  Ferri                                                   &#13;
  Il redattore:  Granata                                                  &#13;
  Il cancelliere:  Di Paola                                               &#13;
   Depositata in cancelleria il 29 dicembre 1995.                         &#13;
  Il direttore di cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
