<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>270</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:270</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/06/1994</data_decisione>
    <data_deposito>30/06/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 3, primo e    &#13;
 secondo comma, della legge 18 febbraio  1983,  n.  47  (Riordinamento    &#13;
 della  prosecuzione  volontaria  dell'assicurazione  obbligatoria per    &#13;
 l'invalidità, la vecchiaia ed i superstiti), promosso con  ordinanza    &#13;
 emessa il 10 maggio 1993 dal Pretore di Lucca nel procedimento civile    &#13;
 vertente  tra  Del Seppia Walter e l'I.N.P.S., iscritta al n. 356 del    &#13;
 registro ordinanze 1993 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 28, prima serie speciale, dell'anno 1993;                  &#13;
    Visti   gli   atti   di   costituzione  di  Del  Seppia  Walter  e    &#13;
 dell'I.N.P.S.;                                                           &#13;
    Udito nell'udienza pubblica del 24 maggio 1994 il Giudice relatore    &#13;
 Luigi Mengoni;                                                           &#13;
    Udito l'avv. Piero d'Amelio per il Del Seppia Walter;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un giudizio civile promosso da Walter Del Seppia    &#13;
 contro  l'INPS  per  ottenere   l'accertamento   del   diritto   alla    &#13;
 prosecuzione  volontaria  dell'assicurazione  per  l'invalidità e la    &#13;
 vecchiaia in base ai contributi versati alla Cassa di previdenza  dei    &#13;
 dipendenti  degli  enti  locali  (CPDEL),  il  Pretore  di Lucca, con    &#13;
 ordinanza del 10 maggio 1993, ha sollevato questione di  legittimità    &#13;
 costituzionale:  a)  in  linea  principale, dell'art. 3, primo comma,    &#13;
 della legge 18 febbraio 1983, n. 47, per contrasto con gli artt.  35,    &#13;
 36 e 38 Cost., in quanto esclude la compatibilità della prosecuzione    &#13;
 volontaria     dell'assicurazione     generale    obbligatoria    per    &#13;
 l'invalidità, la vecchiaia e  i  superstiti  con  l'iscrizione  alle    &#13;
 gestioni  speciali  INPS  per  i  lavoratori autonomi o alle gestioni    &#13;
 previdenziali, comunque denominate, dei liberi professionisti; b)  in    &#13;
 linea  subordinata,  in  riferimento  all'art.  3 Cost., dell'art. 3,    &#13;
 secondo comma, della citata legge n. 47 del 1983, nella parte in  cui    &#13;
 non prevede l'inapplicabilità del divieto di cui al comma precedente    &#13;
 anche  agli assicurati che alla data di pubblicazione della legge non    &#13;
 fossero in  grado  di  ottenere  l'autorizzazione  alla  prosecuzione    &#13;
 volontaria   a   causa  del  ritardo  frapposto  dall'amministrazione    &#13;
 competente al trasferimento dei contributi all'INPS.                     &#13;
    Il giudice remittente premette, in fatto, che il ricorrente,  dopo    &#13;
 avere  prestato servizio alle dipendenze del Comune di Viareggio fino    &#13;
 al 31 gennaio 1958, accreditandosi un'anzianità contributiva  presso    &#13;
 la  CPDEL  di  12 anni e un mese, ed essersi successivamente iscritto    &#13;
 alla Cassa di previdenza degli avvocati e procuratori legali, solo in    &#13;
 data 11 agosto 1989 ha ricevuto formale  comunicazione  dell'avvenuto    &#13;
 trasferimento,  dal  Ministero  del tesoro all'INPS, dei contributi a    &#13;
 suo tempo versati alla  CPDEL.  La  domanda  di  autorizzazione  alla    &#13;
 prosecuzione   volontaria,   presentata  non  appena  pervenuta  tale    &#13;
 comunicazione, è stata respinta dall'INPS ai sensi della legge n. 47    &#13;
 del 1983 sopravvenuta nel frattempo.                                     &#13;
    In relazione alla prima questione l'ordinanza richiama le sentenze    &#13;
 nn. 35 del 1960 e 243 del 1976, che hanno dichiarato l'illegittimità    &#13;
 costituzionale per eccesso di delega dell'art. 16, primo  comma,  del    &#13;
 d.P.R. 26 aprile 1957, n. 818, e dell'art. 5, primo comma, del d.P.R.    &#13;
 31  dicembre  1971,  n.  1432,  i quali prevedevano, rispettivamente,    &#13;
 l'incompatibilità della prosecuzione  volontaria  dell'assicurazione    &#13;
 generale   obbligatoria   con  l'iscrizione  a  forme  di  previdenza    &#13;
 sostitutive o alle  gestioni  speciali  per  i  lavoratori  autonomi.    &#13;
 Sebbene  pronunciate in riferimento esclusivo all'art. 76 Cost., tali    &#13;
 sentenze avrebbero valore di  precedente  in  quanto  individuano  un    &#13;
 principio  di  "favore  verso  la  libera  previdenza del cittadino".    &#13;
 Perciò, il divieto di cumulo, reintrodotto dalla norma impugnata con    &#13;
 estensione  anche  ai  liberi  professionisti,  sarebbe  di  per  sé    &#13;
 costituzionalmente  illegittimo  per  contrasto con i parametri sopra    &#13;
 indicati.                                                                &#13;
    In relazione alla seconda questione,  il  giudice  a  quo  ritiene    &#13;
 l'art.  3,  secondo  comma,  della  legge  n.  47 del 1983 lesivo del    &#13;
 principio di eguaglianza perché discrimina  ingiustificatamente  gli    &#13;
 assicurati  che  non hanno potuto ottenere l'autorizzazione dell'INPS    &#13;
 prima della data di entrata in vigore della legge a causa del mancato    &#13;
 trasferimento da  parte  del  Ministero  del  tesoro  dei  contributi    &#13;
 precedentemente versati alla CPDEL.                                      &#13;
    2.  -  Nel  giudizio  davanti  alla  Corte  costituzionale  si  è    &#13;
 costituito l'assicurato, il  quale  riprende,  sviluppandole  in  una    &#13;
 memoria  aggiunta,  le  argomentazioni dell'ordinanza di rimessione e    &#13;
 conclude per una dichiarazione di illegittimità costituzionale delle    &#13;
 norme impugnate.                                                         &#13;
    In  ordine  alla questione sub b) la parte privata prospetta anche    &#13;
 la possibilità di una  interpretazione  "adeguatrice"  dell'art.  3,    &#13;
 secondo  comma,  della  legge  n. 47 del 1983, la quale comprenda nel    &#13;
 campo di applicazione della deroga al divieto del primo comma "coloro    &#13;
 che, come il ricorrente, alla data di entrata in vigore  della  legge    &#13;
 non  avevano  avuto  la  possibilità  tecnica  di una autorizzazione    &#13;
 dell'INPS per il mancato trasferimento dei contributi".                  &#13;
    3. - Si è pure costituito l'INPS chiedendo che le questioni siano    &#13;
 dichiarate infondate.                                                    &#13;
    L'Istituto contesta la pretesa  contrarietà  dell'art.  3,  primo    &#13;
 comma,  della  legge  n. 47 del 1983 agli artt. 35, 36 e 38 Cost.: la    &#13;
 garanzia costituzionale della tutela previdenziale  è  adeguatamente    &#13;
 adempiuta dall'assicurazione obbligatoria, tanto più dopo la legge 5    &#13;
 marzo  1990,  n.  45,  che  ha  concesso  ai liberi professionisti la    &#13;
 facoltà di ricongiunzione presso la rispettiva cassa  di  previdenza    &#13;
 di  tutti  i  periodi  di  contribuzione  precedentemente accreditati    &#13;
 presso forme di previdenza obbligatoria per lavoratori  dipendenti  o    &#13;
 autonomi.  La  possibilità di cumulo dell'assicurazione obbligatoria    &#13;
 in qualità di libero professionista con la  prosecuzione  volontaria    &#13;
 della  cessata  assicurazione  generale  presso  l'INPS è materia di    &#13;
 scelta discrezionale del legislatore, dipendente da  valutazioni  che    &#13;
 possono variare nel tempo.                                               &#13;
    Quanto  all'art.  3, secondo comma, l'INPS osserva che esso è una    &#13;
 conseguenza della tendenza del  legislatore  a  evitare  una  duplice    &#13;
 assicurazione.<diritto>Considerato in diritto</diritto>1.  -  Il  Pretore di Lucca ha sollevato questione di legittimità    &#13;
 costituzionale:                                                          &#13;
       a) in riferimento agli artt. 35, 36 e 38  Cost.,  dell'art.  3,    &#13;
 primo  comma,  della legge 18 febbraio 1983, n. 47, in quanto esclude    &#13;
 la compatibilità della  prosecuzione  volontaria  dell'assicurazione    &#13;
 generale  obbligatoria per l'invalidità, la vecchiaia e i superstiti    &#13;
 con l'iscrizione alle gestioni speciali dell'INPS  per  i  lavoratori    &#13;
 autonomi  o  alle  gestioni  previdenziali,  comunque denominate, dei    &#13;
 liberi professionisti;                                                   &#13;
       b) in riferimento all'art. 3 Cost., dell'art. 3, secondo comma,    &#13;
 della citata legge n. 47 del 1983, nella parte  in  cui  non  prevede    &#13;
 l'inapplicabilità  del divieto di cui al comma precedente anche agli    &#13;
 assicurati che alla data di pubblicazione della legge non fossero  in    &#13;
 grado  di  ottenere  l'autorizzazione  alla prosecuzione volontaria a    &#13;
 causa  del  ritardo  frapposto  dall'amministrazione  competente   al    &#13;
 trasferimento dei contributi all'INPS.                                   &#13;
    2. - Entrambe le questioni sono infondate.                            &#13;
    Non  possono  esse  invocate come precedente le sentenze di questa    &#13;
 Corte nn. 35 del 1960 e 243 del 1972. Gli  artt.  16  del  d.P.R.  26    &#13;
 aprile  1957,  n. 818, e 5, primo comma, del d.P.R. 31 dicembre 1971,    &#13;
 n. 1432, che vietavano il cumulo, in via di prosecuzione  volontaria,    &#13;
 dell'assicurazione  generale  obbligatoria  con l'iscrizione ad altre    &#13;
 forme  di  previdenza,  sono  stati   dichiarati   costituzionalmente    &#13;
 illegittimi  esclusivamente  per  violazione  dell'art.  76  Cost. Il    &#13;
 "favore verso la libera previdenza del cittadino"  è  addotto  dalle    &#13;
 sentenze   citate  come  principio  ostativo  a  una  interpretazione    &#13;
 estensiva  della delega legislativa conferita dall'art. 35, lett. b),    &#13;
 n. 1, della  legge  30  aprile  1969,  n.  153,  non  come  principio    &#13;
 costituzionale preclusivo del divieto di cumulo.                         &#13;
    L'inversione della linea di politica legislativa, decisa su questo    &#13;
 punto  dall'art.  3  della  legge  n. 47 del 1983, rientra pienamente    &#13;
 nella discrezionalità del legislatore, senza alcun limite  da  parte    &#13;
 dell'art. 38, secondo comma, Cost., e tanto meno da parte degli artt.    &#13;
 35 e 36 Cost.                                                            &#13;
    3.  -  La  questione  subordinata  sub  b)  non si giustifica alla    &#13;
 stregua di un'interpretazione corretta della legge.                      &#13;
    Ai fini dell'applicazione del divieto di cumulo -  decorrente  dal    &#13;
 giorno  di pubblicazione della legge (25 febbraio 1983), anziché dal    &#13;
 1 gennaio 1983 secondo la regola generale dell'art.  4  -  l'art.  3,    &#13;
 secondo  comma,  non discrimina gli assicurati in funzione della data    &#13;
 del provvedimento autorizzativo della prosecuzione volontaria, bensì    &#13;
 in funzione della decorrenza dell'autorizzazione,  la  quale  dipende    &#13;
 dalla  data  di  presentazione  della domanda all'INPS (art. 7, primo    &#13;
 comma, del d.P.R. n. 1432 del 1971):  la  domanda  di  autorizzazione    &#13;
 deve  risultare  presentata in tempo utile affinché l'autorizzazione    &#13;
 possa avere decorrenza anteriore al momento di entrata in vigore  del    &#13;
 divieto.                                                                 &#13;
    Così   precisato  il  significato  della  norma  transitoria,  la    &#13;
 tardività, non imputabile  all'assicurato,  del  trasferimento,  dal    &#13;
 Ministero  del  tesoro  all'INPS,  dei  contributi versati alla CPDEL    &#13;
 appare una circostanza irrilevante, trattandosi di un presupposto del    &#13;
 provvedimento autorizzativo - che non può  essere  concesso  se  non    &#13;
 dopo   che  la  posizione  assicurativa  dell'interessato  sia  stata    &#13;
 ricostituita presso l'INPS - non di un presupposto di  ammissibilità    &#13;
 della  domanda  di  autorizzazione. Se, per ipotesi, la domanda fosse    &#13;
 stata  presentata  tempestivamente  (cioè  almeno  un  giorno  prima    &#13;
 dell'ultimo  sabato  antecedente il 25 febbraio 1983), l'Istituto non    &#13;
 potrebbe  respingerla  adducendo   il   tardivo   trasferimento   dei    &#13;
 contributi accreditati all'assicurato presso la CPDEL.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondate  le questioni di legittimità costituzionale    &#13;
 dell'art. 3, primo e secondo comma, della legge 18 febbraio 1983,  n.    &#13;
 47       (Riordinamento       della      prosecuzione      volontaria    &#13;
 dell'assicurazioneobbligatoria per l'invalidità, la vecchiaia  ed  i    &#13;
 superstiti),  sollevate,  in  riferimento  agli  artt. 3, 35, 36 e 38    &#13;
 della Costituzione, dal Pretore di Lucca con l'ordinanza in epigrafe.    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 22 giugno 1994.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 30 giugno 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
