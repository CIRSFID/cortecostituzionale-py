<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>171</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:171</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/06/1997</data_decisione>
    <data_deposito>05/06/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio sull'ammissibilità del conflitto  di  attribuzione  tra    &#13;
 poteri  dello  Stato  sollevato  da Bernardini Rita, Fiori Raffaella,    &#13;
 Sabatano Mauro,  nella  qualità  di  promotori  e  presentatori  dei    &#13;
 referendum  abrogativi  in  tema di Ordine dei giornalisti, incarichi    &#13;
 extragiudiziari dei magistrati, carriera  dei  magistrati,  esercizio    &#13;
 della  caccia, obiezione di coscienza e "golden share", nei confronti    &#13;
 della  Commissione  parlamentare  per  l'indirizzo  generale   e   la    &#13;
 vigilanza  dei  servizi radiotelevisivi, del Parlamento, della Camera    &#13;
 dei deputati, del Senato della Repubblica  e  del  Governo,  sorto  a    &#13;
 seguito  del regolamento adottato il 20 maggio 1997 dalla Commissione    &#13;
 parlamentare per l'indirizzo generale  e  la  vigilanza  dei  servizi    &#13;
 radiotelevisivi  che  fissa  le  regole  cui  la  concessionaria  del    &#13;
 servizio   pubblico   radiotelevisivo    dovrà    attenersi    nella    &#13;
 predisposizione  delle trasmissioni da effettuarsi in occasione della    &#13;
 campagna referendaria riguardante i  referendum  indetti  per  il  15    &#13;
 giugno  1997, con ricorso depositato il 24 maggio 1997 ed iscritto al    &#13;
 n. 74 del registro ammissibilità conflitti.                             &#13;
   Udito nella camera di  consiglio  del  2  giugno  1997  il  giudice    &#13;
 relatore Carlo Mezzanotte.                                               &#13;
   Ritenuto che Rita Bernardini, Raffaella Fiori e Mauro Sabatano, con    &#13;
 ricorso  depositato il 24 maggio 1997, nella qualità di presentatori    &#13;
 e promotori  dei  referendum  abrogativi  concernenti  la  disciplina    &#13;
 dell'Ordine  dei giornalisti, degli incarichi extragiudiziari e della    &#13;
 carriera  dei magistrati, dell'esercizio della caccia, dell'obiezione    &#13;
 di coscienza e della "golden share" indetti per il 15 giugno 1997 con    &#13;
 d.P.R. 15  aprile  1997,  sollevano  conflitto  di  attribuzione  nei    &#13;
 confronti  della  Commissione parlamentare per l'indirizzo generale e    &#13;
 la vigilanza  dei  servizi  radiotelevisivi,  del  Parlamento,  della    &#13;
 Camera  dei  deputati,  del Senato della Repubblica e del Governo, in    &#13;
 riferimento al regolamento adottato in data 20 maggio  1997,  con  il    &#13;
 quale la Commissione parlamentare ha fissato i criteri e le modalità    &#13;
 per  la  trasmissione  delle  tribune  referendarie  da  parte  della    &#13;
 concessionaria del  servizio  pubblico  radiotelevisivo,  chiedendone    &#13;
 l'annullamento limitatamente all'art.  2, comma 1, lettere a) e b);      &#13;
     che tale regolamento, ad avviso dei ricorrenti, oltre ad un ciclo    &#13;
 di confronti per ciascuno dei quesiti referendari e ad uno di appelli    &#13;
 ai  votanti,  entrambi  riservati ai comitati promotori e ai comitati    &#13;
 per il NO, ne prevederebbe un altro di  quattro  dibattiti  al  quale    &#13;
 potrebbero   partecipare   i   soli  gruppi  parlamentari,  anche  se    &#13;
 costituiti in un solo ramo del Parlamento  e  non  anche  i  comitati    &#13;
 promotori;                                                               &#13;
     che  tali  disposizioni  sarebbero  lesive delle attribuzioni dei    &#13;
 ricorrenti, perché sarebbe affidato ai soli gruppi  parlamentari  il    &#13;
 potere  di  rappresentanza  delle posizioni referendarie in occasione    &#13;
 dei citati dibattiti, in contrasto  con  l'art.  52  della  legge  25    &#13;
 maggio  1970,  n.  352,  che,  in materia di propaganda referendaria,    &#13;
 riconosce  le  medesime  facoltà  ai  partiti  o   gruppi   politici    &#13;
 rappresentati in Parlamento e ai promotori del referendum considerati    &#13;
 questi ultimi come gruppo unico;                                         &#13;
     che,  secondo  i  ricorrenti,  la  partecipazione dei soli gruppi    &#13;
 parlamentari ai dibattiti di cui all'art. 2, comma 1, lettera a)  del    &#13;
 regolamento    impugnato,    si    baserebbe   "sull'indimostrato   e    &#13;
 presumibilmente erroneo presupposto" che tali gruppi si  ripartiscano    &#13;
 equamente  tra  i  SI  ed  i NO, con conseguente violazione della par    &#13;
 condicio tra i sostenitori  dei  due  schieramenti,  e  non  terrebbe    &#13;
 comunque  conto  del  fatto che i gruppi ammessi sono espressione del    &#13;
 Parlamento, il quale, in quanto titolare della potestà  legislativa,    &#13;
 si  porrebbe,  nel  sistema costituzionale, in posizione antitetica a    &#13;
 quella del comitato promotore;                                           &#13;
     che  un'ulteriore  lesione  delle  attribuzioni  dei   ricorrenti    &#13;
 andrebbe   individuata  nel  ritardo  con  il  quale  la  Commissione    &#13;
 parlamentare  ha  approvato   la   regolamentazione   delle   tribune    &#13;
 referendarie,  in  relazione  alla  data  di  inizio  del  periodo di    &#13;
 campagna referendaria,  con  conseguente  restrizione  dei  tempi  di    &#13;
 questa sulle reti radiofoniche e televisive del servizio pubblico;       &#13;
     che,  ad  avviso  dei  ricorrenti, la compressione della campagna    &#13;
 referendaria, in un contesto di  assoluta  mancanza  di  informazione    &#13;
 nelle  precedenti  fasi  della  procedura,  si  ripercuoterebbe sulla    &#13;
 formazione della volontà di coloro che sono chiamati ad esprimere il    &#13;
 proprio voto il 15 giugno 1997 e, di conseguenza, sulle  attribuzioni    &#13;
 garantite al comitato promotore dall'art. 75 della Costituzione;         &#13;
     che  i ricorrenti, in considerazione dell'asserito ritardo con il    &#13;
 quale la Commissione parlamentare ha adottato il regolamento e  della    &#13;
 esigenza di non vanificare la garanzia costituzionale della tutela in    &#13;
 sede  di  conflitto  tra poteri dello Stato, chiedono che, dichiarata    &#13;
 l'ammissibilità del conflitto proposto, questa Corte emetta, sentite    &#13;
 le  parti,  in  applicazione analogica della disposizione relativa ai    &#13;
 conflitti tra Stato e regioni e tra  regioni  (art.  28  delle  norme    &#13;
 integrative   per  i  giudizi  davanti  alla  Corte  costituzionale),    &#13;
 ordinanza  cautelare  con  la  quale  venga  sospesa   l'applicazione    &#13;
 dell'art.    2, comma 1, lettere a) e b), del regolamento adottato il    &#13;
 20  maggio  1997  dalla  Commissione  parlamentare  per   l'indirizzo    &#13;
 generale e la vigilanza dei servizi radiotelevisivi.                     &#13;
   Considerato  che  questa  Corte  è  chiamata  a decidere, ai sensi    &#13;
 dell'art.  37, commi terzo e quarto, della legge 11  marzo  1953,  n.    &#13;
 87,  con  ordinanza in camera di consiglio, in via delibativa e senza    &#13;
 contraddittorio, se  esista  la  materia  di  un  conflitto,  la  cui    &#13;
 soluzione spetti alla sua competenza con riferimento alla sussistenza    &#13;
 dei requisiti soggettivi e oggettivi di ammissibilità richiamati dal    &#13;
 primo comma dello stesso articolo;                                       &#13;
     che,  per quanto concerne i requisiti soggettivi, questa Corte ha    &#13;
 già più volte riconosciuto la qualità di potere dello  Stato  alla    &#13;
 frazione  del  corpo  elettorale,  titolare  del potere di iniziativa    &#13;
 referendaria ex art. 75  della  Costituzione,  e  la  competenza  dei    &#13;
 promotori  della  richiesta  di  referendum  abrogativo a dichiararne    &#13;
 definitivamente la volontà ai sensi dell'art. 37 della legge  n.  87    &#13;
 del 1953;                                                                &#13;
     che   la   legittimazione   attiva   è  stata,  in  particolare,    &#13;
 riconosciuta ai promotori in riferimento  a  restrizioni  poste  alla    &#13;
 propaganda  referendaria  che possano incidere sulla formazione della    &#13;
 volontà di coloro che sono  chiamati  al  voto  nella  consultazione    &#13;
 popolare (sentenza n. 161 del 1995);                                     &#13;
     che,  ancora  sotto  il  profilo soggettivo, deve riconoscersi la    &#13;
 legittimazione passiva della Commissione parlamentare per l'indirizzo    &#13;
 generale e la vigilanza dei  servizi  radiotelevisivi,  quale  organo    &#13;
 competente  a dichiarare definitivamente la volontà della Camera dei    &#13;
 deputati e del Senato della Repubblica in una materia che, come nella    &#13;
 specie, attiene direttamente alla informazione e alla  propaganda  in    &#13;
 relazione ai procedimenti di referendum abrogativo;                      &#13;
     che, quanto al requisito oggettivo del conflitto, le modalità di    &#13;
 svolgimento della campagna referendaria sono suscettibili di influire    &#13;
 sulla  formazione  dell'opinione  pubblica,  e  il  conflitto  stesso    &#13;
 concerne un atto di indirizzo delle Camere diretto ad  assicurare  la    &#13;
 realizzazione  del  principio  del  pluralismo  nel servizio pubblico    &#13;
 radiotelevisivo (sentenze n. 420 del 1994 e n. 112 del 1993), sicché    &#13;
 ogni  limitazione  della  facoltà  di   partecipare   ai   dibattiti    &#13;
 televisivi  sui referendum potrebbe, in astratto, ledere l'integrità    &#13;
 delle attribuzioni dei comitati promotori;                               &#13;
     che,  pertanto,  in  questa  fase  delibativa,  il   ricorso   va    &#13;
 dichiarato  ammissibile  nei confronti della Commissione parlamentare    &#13;
 per l'indirizzo generale e la vigilanza dei servizi  radiotelevisivi,    &#13;
 salva  e  impregiudicata  la  pronuncia  definitiva  anche  sul punto    &#13;
 relativo alla ammissibilità;                                            &#13;
     che il ricorso deve essere conseguentemente  notificato  a  detta    &#13;
 Commissione  ma  non  anche al Governo, non venendo in considerazione    &#13;
 alcuna sua competenza;                                                   &#13;
     che, quanto alla richiesta di  provvedimento  cautelare  avanzata    &#13;
 dai  ricorrenti  -  impregiudicata  ogni  valutazione  in ordine alla    &#13;
 configurabilità, nel giudizio sui conflitti tra poteri dello  Stato,    &#13;
 dell'istituto  della  sospensione  dell'atto  impugnato  -  non  v'è    &#13;
 ragione  di  far  luogo  alla  sollecitata  misura  extra ordinem nei    &#13;
 confronti di un atto che prevede eguale ripartizione del tempo tra le    &#13;
 opposte indicazioni di voto, nel contesto di una  programmazione  che    &#13;
 assicura la complessiva presenza dei comitati promotori durante tutto    &#13;
 l'arco delle previste trasmissioni.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  ammissibile  il conflitto di attribuzione in epigrafe nei    &#13;
 confronti della Commissione parlamentare per l'indirizzo  generale  e    &#13;
 la vigilanza dei servizi radiotelevisivi;                                &#13;
   Dispone  che la cancelleria della Corte dia immediata comunicazione    &#13;
 della presente ordinanza ai ricorrenti e che,   a cura  degli  stessi    &#13;
 ricorrenti,  il ricorso e la presente ordinanza siano notificati alla    &#13;
 Commissione parlamentare per  l'indirizzo generale e la vigilanza dei    &#13;
 servizi radiotelevisivi  entro  il  termine  di  dieci  giorni  dalla    &#13;
 comunicazione.                                                           &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 giugno 1997.                                &#13;
                         Il Presidente: Granata                           &#13;
                        Il redattore: Mezzanotte                          &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 5 giugno 1997.                            &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
