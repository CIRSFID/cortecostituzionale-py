<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>147</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:147</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1982</data_decisione>
    <data_deposito>27/07/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - &#13;
 Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale dell'art.  121, comma  &#13;
 terzo, del d.P.R. 15 giugno 1959, n. 393 (Testo unico delle norme sulla  &#13;
 circolazione stradale), come modificato dalla legge 5 maggio  1976,  n.  &#13;
 313 promossi  con otto ordinanze emesse il 2 ottobre 1981 e con quattro  &#13;
 ordinanze emesse il 9 ottobre 1981 dal Pretore di  Pontedecimo,  e  con  &#13;
 ordinanza emessa il 12 ottobre 1981 dal Tribunale di Bergamo iscritte ai  &#13;
 nn.    da 759 a 769, 776 e 783 del registro ordinanze 1981 e pubblicate  &#13;
 nella Gazzetta Ufficiale della Repubblica nn. 68 e 75 del 1982.          &#13;
     Udito nella camera di  consiglio  del  6  maggio  1982  il  Giudice  &#13;
 relatore Alberto Malagugini.                                             &#13;
     Ritenuto  che  le  dodici  ordinanze  del  Pretore  di  Pontedecimo  &#13;
 indicate in epigrafe propongono, in relazione agli artt. 3 e 27  Cost.,  &#13;
 le  medesime  questioni  di  legittimità costituzionale dell'art. 121,  &#13;
 terzo comma, del T. U. delle  norme  concernenti  la  disciplina  della  &#13;
 circolazione stradale, approvato con d.P.R. 15 giugno 1959, n. 393, nel  &#13;
 testo  sostituito  dall'art. 5 della legge 5 maggio 1976, n. 313, nella  &#13;
 parte in cui punisce con l'ammenda di L. 800.000 e con quindici  giorni  &#13;
 di  arresto  chiunque circoli con un veicolo che superi di oltre trenta  &#13;
 quintali il peso complessivo consentito, già dichiarate non fondate da  &#13;
 questa Corte con sentenza n. 50 del 1980 e manifestamente infondate con  &#13;
 ordinanze nn. 147, 167, 169 e 195 del 1980, 66, 82, 83, 84, 135, 136  e  &#13;
 158 del 1981, 4 del 1982;                                                &#13;
     considerato  che,  peraltro,  dopo  l'emanazione  delle  suindicate  &#13;
 ordinanze, il predetto testo dell'art. 121 del T. U. delle norme  sulla  &#13;
 circolazione  stradale  è  stato integralmente sostituito dall'art. 12  &#13;
 della legge 10 febbraio 1982, n.  38  il  quale  tra  l'altro:  prevede  &#13;
 l'applicazione  della  sanzione  amministrativa,  in  luogo  di  quella  &#13;
 penale, per la circolazione con veicoli che superino di oltre il 5  per  &#13;
 cento  il  peso  complessivo consentito; commina sanzioni distinte e di  &#13;
 importo gradualmente crescente per diverse fasce di eccedenza di peso a  &#13;
 seconda, cioè, che questo ecceda di 10, di 20, di 30  o  di  oltre  30  &#13;
 quintali  quello  indicato  sul  documento  di  circolazione; consente,  &#13;
 infine, di graduare tali sanzioni stabilendo  per  ciascuna  un  limite  &#13;
 minimo ed uno massimo;                                                   &#13;
     ritenuto,  altresì,  che  l'ordinanza  del  Tribunale  di  Bergamo  &#13;
 indicata in epigrafe propone l'ulteriore questione secondo  cui  l'art.  &#13;
 121 citato, prevedendo espressamente la responsabilità penale del solo  &#13;
 conducente  del  veicolo  (terzo  comma)  ed  escludendo implicitamente  &#13;
 quella  del  conducente  e  del  proprietario  in  quanto  li  dichiara  &#13;
 civilmente   responsabili   in  solido  (quarto  comma),  concreterebbe  &#13;
 un'ingiustificata disparità di trattamento in  danno  del  primo,  con  &#13;
 violazione dell'art. 3 Cost.;                                            &#13;
     considerato  che  -  come  questa  Corte  ha  già  precisato,  con  &#13;
 l'ordinanza n. 167 del 1980 - tale  questione  poggia  su  un  equivoco  &#13;
 interpretativo,  in  quanto  la partecipazione sia dolosa che colposa a  &#13;
 reati  contravvenzionali,  secondo  la  comune  opinione  dottrinale  e  &#13;
 giurisprudenziale,  assume  rilevanza  penale  in  forza  dei  principi  &#13;
 generali sul concorso di persone;                                        &#13;
     che, peraltro, il sopracitato art. 12 della legge n.  38 del  1982,  &#13;
 entrata  in  vigore  dopo  l'emanazione  dell'ordinanza  in  questione,  &#13;
 stabilisce  ora  espressamente,  al  decimo  comma,  che  le   sanzioni  &#13;
 amministrative  previste  a carico di "chiunque circoli" con veicoli di  &#13;
 peso  complessivo  superiore  al  consentito  "si  applicano   sia   al  &#13;
 conducente  che  al  proprietario  del veicolo, nonché al committente,  &#13;
 quando si tratti di trasporto eseguito per suo conto esclusivo";         &#13;
     ritenuto che, di conseguenza, si rende necessario che i  giudici  a  &#13;
 quibus  procedano  ad un nuovo esame della rilevanza delle questioni di  &#13;
 legittimità  costituzionale  sollevate,  tenendo  conto  delle   norme  &#13;
 sopravvenute.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione  degli atti, rispettivamente al Pretore di  &#13;
 Pontedecimo ed al Tribunale di Bergamo.                                  &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'8 luglio 982.            &#13;
                                   F.to:  LEOPOLDO  ELIA  -  ANTONINO DE  &#13;
                                   STEFANO - GUGLIELMO ROEHRSSEN  ORONZO  &#13;
                                   REALE  - BRUNETTO BUCCIARELLI DUCCI -  &#13;
                                   ALBERTO MALAGUGINI - LIVIO PALADIN  -  &#13;
                                   ANTONIO   LA   PERGOLA   -   VIRGILIO  &#13;
                                   ANDRIOLI - GUSEPPE FERRARI  FRANCESCO  &#13;
                                   SAJA - GIOVANNI CONSO.                 &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
