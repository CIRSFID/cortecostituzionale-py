<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2021-05-31/114/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2021-05-31/114"/>
          <FRBRalias value="ECLI:IT:COST:2021:114" name="ECLI"/>
          <FRBRdate date="2021-05-31" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="114"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2021-05-31/114/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2021-05-31/114/ita@"/>
          <FRBRdate date="2021-05-31" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2021-05-31/114/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2021-05-31/114/ita@.xml"/>
          <FRBRdate date="2021-05-31" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2021-04-27" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2021-05-31" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CORAGGIO" href="/akn/ontology/person/it/CORAGGIO" showAs="CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2021</akn4cc:anno>
        <akn4cc:numero>114</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2021:114</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>CORAGGIO</akn4cc:presidente>
        <akn4cc:relatore>Giovanni Amoroso</akn4cc:relatore>
        <akn4cc:data_decisione>2021-04-27</akn4cc:data_decisione>
        <akn4cc:data_deposito>2021-05-31</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>; Giudici : <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 589, secondo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, promosso dal Giudice per l'udienza preliminare presso il Tribunale ordinario di Treviso nel procedimento penale a carico di S. B., M. P. e M. T., con ordinanza del 24 dicembre 2019, iscritta al n. 72 del registro ordinanze 2020 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 26, prima serie speciale, dell'anno 2020.
 Visti l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> di S. B., nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nell'udienza pubblica del 27 aprile 2021 il Giudice relatore Giovanni Amoroso;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>uditi l'avvocato Stefano Pietrobon per S. B. e l'avvocato dello Stato Andrea Fedeli per il Presidente del Consiglio dei ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>deliberato nella camera di consiglio del 27 aprile 2021.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Giudice per l'udienza preliminare presso il Tribunale ordinario di Treviso, con ordinanza del 24 dicembre 2019 (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/72/!main">ord. n. 72 del 2020</ref>) ha sollevato, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>, questione di legittimità costituzionale dell'art. 589, secondo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, nella parte in cui non prevede una diminuzione di pena nel caso in cui «l'evento non sia esclusivamente conseguenza dell'azione o dell'omissione del colpevole».
 Il rimettente riferisce di procedere nei confronti di S. B., M. P. e M. T., per il reato di omicidio colposo commesso con la violazione delle norme per la prevenzione degli infortuni sul lavoro, ai sensi degli artt. 40, 113 e 589, secondo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>.
 Dal capo di imputazione risulta che gli imputati, in qualità di legali rappresentanti della B. snc, e S. B., anche come responsabile del servizio di protezione e prevenzione, per colpa consistita in negligenza, imprudenza, imperizia, nonché in violazione delle previsioni dell'art. 71, comma 3, in riferimento al punto 3.1.3 dell'Allegato VI al <ref href="/akn/it/act/decretoLegislativo/stato/2008-04-09/81/!main">decreto legislativo 9 aprile 2008, n. 81</ref> (Attuazione dell'<ref href="/akn/it/act/legge/stato/2007-08-03/123/!main#art_1">articolo 1 della legge 3 agosto 2007, n. 123</ref>, in materia di tutela della salute e della sicurezza nei luoghi di lavoro), omettendo, in particolare, di adottare adeguate misure tecniche e organizzative affinché le attrezzature per la movimentazione dei carichi fossero utilizzate in condizioni tali da garantire la stabilità dell'attrezzatura stessa e del carico trasportato, causavano la morte di V. H., autista, dipendente di una società slovacca, il quale, incaricato del trasporto e consegna di un carico di balle di terriccio, imballate con film plastico e poste sui bancali, presso la sede della società dei predetti imputati, veniva travolto e schiacciato da una di tali balle (del peso di 1000 chilogrammi circa) a causa dello sbilanciamento del carico del carrello elevatore, utilizzato per l'operazione di scarico.
 Il rimettente dà atto che, all'udienza del 3 ottobre 2019, il difensore degli imputati ha eccepito l'illegittimità costituzionale dell'art. 589, secondo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>. nella parte «in cui non riconosce, come fa invece nel comma 7 dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis c. p.</ref>, una diminuzione di pena nel caso in cui la condotta colposa dell'infortunato abbia contribuito a causare l'evento dannoso».
 Sciogliendo la riserva in ordine all'eccezione di illegittimità costituzionale, il giudice a quo afferma che la questione è rilevante e non è manifestamente infondata.
 In particolare, il rimettente evidenzia che l'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref>, introdotto dall'<ref href="/akn/it/act/legge/stato/2016-03-23/41/!main#art_1__para_1">art. 1, comma 1, della legge 23 marzo 2016, n. 41</ref> (Introduzione del reato di omicidio stradale e del reato di lesioni personali stradali, nonché disposizioni di coordinamento al <ref href="/akn/it/act/decretoLegislativo/stato/1992-04-30/285/!main">decreto legislativo 30 aprile 1992, n. 285</ref>, e al <ref href="/akn/it/act/decretoLegislativo/stato/2000-08-28/274/!main">decreto legislativo 28 agosto 2000, n. 274</ref>), prevede, come pena base, la reclusione da due a sette anni, per chi si rende responsabile del reato di omicidio stradale, e al settimo comma contempla una diminuzione di pena fino alla metà qualora l'evento non sia «esclusiva conseguenza dell'azione o dell'omissione del colpevole».
 Anche l'art. 589, secondo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., prevede la pena da anni due ad anni sette di reclusione per colui che si rende responsabile di omicidio colposo aggravato dalla violazione delle norme per la prevenzione degli infortuni sul lavoro, ma non contempla, invece, una circostanza attenuante analoga a quella di cui all'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>.
 Il giudice a quo rileva come entrambe le fattispecie astratte di cui agli artt. 589, secondo comma, e 589-bis, primo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>. sanzionino condotte caratterizzate da colpa specifica consistente, nel primo caso, nella violazione della normativa posta a tutela della sicurezza dei luoghi di lavoro e dei lavoratori, e, nel secondo caso, nella violazione della normativa tesa a tutelare la sicurezza degli utenti delle strade.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In entrambe le fattispecie, «il bene oggetto di tutela è l'integrità fisica delle persone».
 Le due norme sarebbero «sostanzialmente identiche, se non sovrapponibili tra loro», quantomeno in relazione alla loro funzione, tanto che le violazioni delle stesse erano sanzionate, e con la stessa pena, nel secondo comma dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589">art. 589 cod. pen.</ref>, sino alla introduzione del reato di omicidio stradale.
 Osserva il rimettente che, tuttavia, manca nella previsione dell'art. 589, secondo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., una norma che preveda la diminuzione di pena qualora la condotta della vittima sia stata tale da contribuire al verificarsi dell'evento.
 Pertanto, sussisterebbe una ingiustificata disparità di trattamento tra le due fattispecie di omicidio colposo, con conseguente violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>
 In punto di rilevanza, il rimettente riferisce che se esistesse una disposizione analoga a quella di cui all'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., si «alleggerirebbe la pena eventualmente infliggenda agli imputati nel processo che ne riguarda ove fosse riconosciuta la loro responsabilità ma, nello stesso tempo, fosse riconosciuta una condotta imprudente da parte dell'infortunato. Circostanza, questa, che il difensore intende dimostrare attraverso l'acquisizione della relazione dello SPISAL intervenuto sul luogo dell'infortunio e attraverso l'escussione dei testi».
 2.- Con atto depositato il 14 luglio 2020, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, è intervenuto nel presente giudizio di costituzionalità chiedendo alla Corte di dichiarare la questione inammissibile e, comunque, non fondata.
 La difesa statale osserva che il giudice rimettente ha sollevato la questione di legittimità costituzionale in sede di udienza preliminare, e quindi, nella fase deputata al vaglio dell'ipotesi accusatoria ai soli fini della decisione circa la necessità o non del rinvio a giudizio degli imputati.
 Dagli atti di causa non risulta che gli imputati abbiano scelto di definire il giudizio con riti alternativi.
 In particolare - osserva la difesa statale - nella fase dell'udienza preliminare il giudice non è chiamato a decidere sulla responsabilità dell'imputato e ad applicare la circostanza attenuante, la cui mancata previsione è oggetto di censura, sicché la questione sarebbe priva di rilevanza.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Comunque, nel merito, la questione sarebbe in ogni caso non fondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In particolare, la difesa statale osserva ulteriormente che, con le modifiche apportate dalla <ref href="/akn/it/act/legge/stato/2016/41/!main">legge n. 41 del 2016</ref>, al reato di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589">art. 589 cod. pen.</ref>, il legislatore, nell'esercizio della sua ampia discrezionalità, ha inteso assicurare le esigenze di maggior protezione, come quelle connesse alle frequenti violazioni del codice della strada, foriere di eventi lesivi o mortali, e, quindi, all'allarme sociale suscitato dal fenomeno ricorrente delle "vittime della strada".
 Più nello specifico, la difesa statale richiama la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/88/!main">sentenza n. 88 del 2019</ref> in cui questa Corte ha affermato che il legislatore, nel rendere autonoma la fattispecie dell'omicidio stradale, ha operato un tipico esercizio di discrezionalità legislativa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Infine evidenzia che, se è vero che l'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589">art. 589 cod. pen.</ref> non contempla un'attenuante speciale analoga a quella prevista dall'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., ciò però non impedisce al giudice di valutare l'eventuale concorrente condotta della persona offesa al fine del riconoscimento delle circostanze attenuanti generiche, suscettibili di bilanciamento con le altre aggravanti.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Infatti, non essendo previsto un divieto analogo a quello di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_590quater">art. 590-quater cod pen.</ref>, la pena può essere suscettibile di una sensibile riduzione in caso di prevalenza delle circostanze attenuanti generiche.
 3.- Con atto pervenuto il 15 luglio 2020, si è costituito in giudizio uno degli imputati (S. B.).
 In punto di rilevanza, la parte riferisce che l'evento delittuoso si è determinato anche con il contributo causale della condotta colposa della vittima, come emergerebbe dalla relazione dello SPISAL (Servizio per la prevenzione, l'igiene e la sicurezza negli ambienti di lavoro) intervenuto sul luogo dell'infortunio.
 Osserva che, in particolare, la vittima si sarebbe portata nel raggio d'azione del carrello elevatore, ponendosi in una situazione di rischio per la propria incolumità, così violando le prescrizioni di sicurezza impartitegli dagli imputati.
 Pone in rilievo che, pendendo il procedimento nella fase dell'udienza preliminare, «[l]addove il B. chieda di procedere nelle forme del rito abbreviato, egli sarà giudicato sulla base degli atti ivi contenuti e dallo stesso Giudice odierno remittente (Giudice dell'Udienza Preliminare) il quale non potrà evidentemente esimersi dal valutare il rilievo causale della condotta tenuta dalla vittima».
 Nel merito, in ordine alla denunciata violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, la parte sostiene che vi è una piena corrispondenza tra il reato di omicidio colposo aggravato di cui al secondo comma dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589">art. 589 cod. pen.</ref> e il reato di omicidio stradale di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref>
 Quindi, le due norme in comparazione disciplinerebbero situazioni giuridiche omogenee.
 Argomenta la parte che «la condotta integrativa dei due delitti è identica sul piano della struttura della fattispecie», anche quanto all'elemento soggettivo della colpa consistente nella violazione di norme precauzionali specifiche; in particolare per entrambe le fattispecie è previstala pena della reclusione da due a sette anni.
 Pertanto - conclude la parte - la questione di legittimità costituzionale sollevata dal giudice rimettente sarebbe fondata.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Giudice per l'udienza preliminare presso il Tribunale ordinario di Treviso, ha sollevato, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>, questione di legittimità costituzionale dell'art. 589, secondo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, nella parte in cui non prevede una circostanza attenuante analoga a quella contemplata dall'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., secondo cui la pena per chi si rende responsabile del reato di omicidio stradale è diminuita fino alla metà qualora l'evento non sia «esclusiva conseguenza dell'azione o dell'omissione del colpevole».
 Il giudice a quo sostiene che le due fattispecie - quella di omicidio colposo aggravato dalla violazione delle norme per la prevenzione degli infortuni sul lavoro e quella di omicidio stradale - sarebbero «sostanzialmente identiche, se non  sovrapponibili tra loro» e che pertanto risulterebbe una ingiustificata disparità di trattamento (<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>) in ragione della mancata previsione, anche nell'art. 589, secondo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., della diminuzione di pena qualora la condotta della vittima sia stata tale da aver contribuito al verificarsi dell'evento.
 2.- Giova premettere - quanto al quadro normativo di riferimento - che il parallelismo tra le due fattispecie, evocato dal giudice rimettente, trova origine nell'<ref href="/akn/it/act/legge/stato/1966-05-11/296/!main#art_1">art. 1 della legge 11 maggio 1966, n. 296</ref>, recante «Modifiche degli articoli 589 (omicidio colposo) e 590 (lesioni personali colpose) del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">Codice penale</ref>», che, nel riformulare l'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589">art. 589 cod. pen.</ref> (omicidio colposo), ha previsto, al secondo comma di tale disposizione, che «[s]e il fatto è commesso con violazione delle norme sulla disciplina della circolazione stradale o di quelle per la prevenzione degli infortuni sul lavoro la pena è della reclusione da uno a cinque anni».
 Per entrambe le ipotesi di omicidio colposo aggravato la pena è stata dapprima elevata nel minimo (da uno a due anni di reclusione) dall'<ref href="/akn/it/act/legge/stato/2006-02-21/102/!main#art_2__para_1">art. 2, comma 1, della legge 21 febbraio 2006, n. 102</ref> (Disposizioni in materia di conseguenze derivanti da incidenti stradali) e poi anche nel massimo (da cinque a sette anni di reclusione) dall'<ref href="/akn/it/act/decretoLegge/stato/2008-05-23/92/!main#art_1__para_1__point_c">art. 1, comma 1, lettera c), del decreto-legge 23 maggio 2008, n. 92</ref> (Misure urgenti in materia di sicurezza pubblica), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2008-07-24/125/!main">legge 24 luglio 2008, n. 125</ref>.
 Successivamente, però, l'allarme sociale causato dai numerosi e ricorrenti casi di "vittime della strada" ha indotto il legislatore a elevare, nel complesso, il regime sanzionatorio di chi, violando le norme sulla circolazione stradale, abbia cagionato la morte o lesioni gravi o gravissime ad altri.
 In particolare, il legislatore ha isolato la fattispecie di omicidio colposo con violazione delle norme sulla disciplina della circolazione stradale trasformandola nell'autonomo e distinto reato di omicidio stradale di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref> (<ref href="/akn/it/act/legge/stato/2016-03-23/41/!main#art_1__para_1">art. 1, comma 1, della legge 23 marzo 2016, n. 41</ref>, recante «Introduzione del reato di omicidio stradale e del reato di lesioni personali stradali, nonché disposizioni di coordinamento al <ref href="/akn/it/act/decretoLegislativo/stato/1992-04-30/285/!main">decreto legislativo 30 aprile 1992, n. 285</ref>, e al <ref href="/akn/it/act/decretoLegislativo/stato/2000-08-28/274/!main">decreto legislativo 28 agosto 2000, n. 274</ref>»).
 In riferimento a tale fattispecie di reato la stessa disposizione (<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref>) - in simmetria con il reato di lesioni stradali (<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_590bis">art. 590-bis cod. pen.</ref>) - ha previsto da una parte plurime aggravanti a effetto speciale cosiddette privilegiate, perché non suscettibili di bilanciamento con circostanze attenuanti ai sensi dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_590quater">art. 590-quater cod. pen.</ref>; dall'altra ha contemplato un'attenuante anch'essa a effetto speciale «qualora l'evento non sia esclusiva conseguenza dell'azione o dell'omissione del colpevole» (settimo comma dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref>) al fine di «moderare il notevole maggior rigore della risposta sanzionatoria» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/88/!main">sentenza n. 88 del 2019</ref>).
 Invece la fattispecie dell'omicidio colposo aggravato dalla violazione delle norme per la prevenzione degli infortuni sul lavoro è rimasta invariata.
 Il giudice rimettente censura proprio la mancata introduzione, anche per tale reato, della stessa attenuante prevista dall'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>. in ragione dell'asserito perdurante parallelismo con la fattispecie dell'omicidio stradale.
 3.- Ciò premesso, la questione di legittimità costituzionale è inammissibile per plurimi motivi.
 4.- Innanzitutto, il rimettente ha sollevato tale questione nel corso dell'udienza preliminare, omettendo di indicare se gli imputati avessero formulato la richiesta di definizione del giudizio con il rito abbreviato di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_438">art. 438 del codice di procedura penale</ref> o con il cosiddetto patteggiamento, ai sensi dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_444">art. 444 cod. proc. pen.</ref>
 L'assenza di una domanda di definizione del processo allo stato degli atti ovvero di applicazione della pena a richiesta risulta riconosciuta dalla stessa difesa dell'imputato, costituitosi nel presente giudizio di legittimità costituzionale, nonché trova riscontro anche nel fascicolo processuale del giudizio principale (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2009/58/!main">sentenza n. 58 del 2009</ref>; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2018/57/!main">ordinanza n. 57 del 2018</ref>).
 Pertanto il giudice rimettente non è chiamato a decidere sulla responsabilità degli imputati e quindi neppure, in ipotesi, a riconoscere la circostanza attenuante, la cui mancata previsione è oggetto di censura.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ciò rende meramente eventuale e ipotetica - nonché comunque prematura - l'odierna questione.
 Per costante orientamento di questa Corte, infatti, la questione incidentale è irrilevante e, dunque, inammissibile se l'applicazione della norma censurata è solo eventuale e successiva (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/139/!main">is, sentenze n. 139 del 2020</ref> e n. 217 del 2019; <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/210/!main">ordinanze n. 210</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/42/!main">42 del 2020</ref></mref>).
 5.- Inoltre, il giudice rimettente non precisa le ragioni per cui, nel caso sottoposto al suo giudizio, sarebbe ravvisabile un'ipotesi analoga a quella disciplinata dall'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., sì da poter ritenere che «l'evento non sia esclusiva conseguenza dell'azione o dell'omissione del colpevole».
 In particolare, il rimettente, limitandosi ad indicare il capo di imputazione, omette di prendere posizione sulla ricostruzione dei fatti con riferimento sia alla responsabilità degli imputati, sia soprattutto alla ipotizzata sussistenza di una condotta colposa della vittima, che avrebbe contribuito a causare l'evento morte; condotta questa che non viene affatto descritta.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Essendo contestata agli imputati la cooperazione, ai sensi dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_113">art. 113 cod. pen.</ref>, nel reato di omicidio colposo, il giudice rimettente inoltre non specifica neppure il distinto apporto causale di ciascuno di essi.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Tali lacune nella descrizione della fattispecie, oggetto del giudizio principale, determinano - come più volte affermato da questa Corte (<mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/147/!main">ordinanze n. 147</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/108/!main">108 del 2020</ref></mref>, n. 203 e n. 64 del 2019) - l'inammissibilità della questione di legittimità costituzionale, in quanto non consentono di verificarne l'effettiva rilevanza.
 6.- Sotto altro concorrente profilo, deve altresì osservarsi che l'ordinanza di rimessione è carente anche quanto alla motivazione della ritenuta non manifesta infondatezza della questione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Infatti il rimettente, nell'evocare la disciplina sul trattamento sanzionatorio dell'omicidio stradale quale tertium comparationis, si è limitato a indicare genericamente che le fattispecie a confronto prevedono la medesima pena della reclusione da due a sette anni rispettivamente per l'ipotesi aggravata del secondo comma dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589">art. 589 cod. pen.</ref> e per l'ipotesi base del primo comma dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref>
 In tal modo, però, il rimettente, non confrontandosi con il complessivo e più articolato quadro normativo sopra richiamato, non spiega adeguatamente le ragioni della asserita omogeneità delle fattispecie in comparazione, da cui dovrebbe derivare l'illegittimità costituzionale della mancata introduzione - anche per l'omicidio colposo aggravato dalla violazione delle norme per la prevenzione degli infortuni sul lavoro - della stessa attenuante ad effetto speciale di cui all'art. 589-bis, settimo comma, <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">cod. pen</ref>., prevista per il solo reato di omicidio stradale.
 Per costante giurisprudenza di questa Corte, l'insufficiente motivazione in punto di non manifesta infondatezza determina l'inammissibilità della questione di legittimità costituzionale (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/265/!main">sentenze n. 265 del 2019</ref> e n. 182 del 2018), così come anche la determina l'«incompleta ricostruzione della normativa di riferimento» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/102/!main">sentenza n. 102 del 2019</ref>).
 7.- Pertanto, nel complesso, la sollevata questione di legittimità costituzionale è inammissibile.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara inammissibile la questione di legittimità costituzionale dell'art. 589, secondo comma, del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, sollevata, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>, dal Giudice per l'udienza preliminare presso il Tribunale ordinario di Treviso, con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2021-04-27">27 aprile 2021</date>.
 F.to:
 <signature><judge refersTo="#giancarloCoraggio" as="#presidente">Giancarlo CORAGGIO</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giovanniAmoroso" as="#relatore">Giovanni AMOROSO</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2021-05-31">31 maggio 2021</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
