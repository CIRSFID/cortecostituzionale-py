<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>129</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:129</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Giuseppe Ferrari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/04/1987</data_decisione>
    <data_deposito>15/04/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, &#13;
 prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, &#13;
 prof. Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 53, secondo    &#13;
 comma, d.P.R. 16 maggio 1960, n. 570 (Testo unico delle leggi per  la    &#13;
 composizione   e  la  elezione  degli  organi  delle  amministrazioni    &#13;
 comunali), promosso con  ordinanza  emessa  il  14  aprile  1983  dal    &#13;
 Tribunale  amministrativo  regionale  per  la  Campania,  Sezione  di    &#13;
 Salerno, sul ricorso proposto da Colucci Raffaele  e  Amministrazione    &#13;
 Provinciale  di  Salerno  ed  altri,  iscritta al n. 879 del registro    &#13;
 ordinanze 1983 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 60 dell'anno 1984;                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza  pubblica  del  25  novembre  1986  il Giudice    &#13;
 relatore Giuseppe Ferrari;                                               &#13;
    Udito  l'Avvocato  dello  Stato Paolo di Tarsia di Belmonte per il    &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Tribunale  amministrativo  regionale  per  la Campania,    &#13;
 sezione di Salerno, con  ordinanza  emessa  il  14  aprile  1983  nel    &#13;
 giudizio  promosso  da  Raffaele  Colucci  per  l'annullamento  delle    &#13;
 operazioni elettorali del giorno 8 e 9 giugno  1980  e  dell'atto  di    &#13;
 proclamazione   degli   eletti  relativi  al  rinnovo  del  Consiglio    &#13;
 provinciale  di  Salerno,   collegio   di   Pontecagnano-Faiano,   ha    &#13;
 sollevato,   su   istanza   di   parte,   questione  di  legittimità    &#13;
 costituzionale,  in  riferimento  all'art.  3  Cost.,  dell'art.  53,    &#13;
 secondo comma, d.P.R. 16 maggio 1960, n. 570 (Testo unico delle leggi    &#13;
 per la composizione e la elezione degli organi delle  amministrazioni    &#13;
 comunali).                                                               &#13;
    Premesso  che  a  fondamento  della domanda il ricorrente adduceva    &#13;
 l'omessa vidimazione delle liste elettorali prima che fosse  iniziato    &#13;
 lo  spoglio  dei  voti  in  taluni  seggi,  secondo  quanto  previsto    &#13;
 dall'art. 53  del  d.P.R.  citato,  il  giudice  a  quo  osserva  che    &#13;
 l'articolo  2,  lettera  c), del decreto legge 3 maggio 1976, n. 161,    &#13;
 convertito in legge, con modificazioni, con legge 14 maggio 1976,  n.    &#13;
 240, nel prescrivere tutti gli adempimenti da effettuarsi prima dello    &#13;
 spoglio  dei  voti,  nulla  più  dispone  sulla   necessità   della    &#13;
 vidimazione delle liste. La portata innovativa di tale disposizione -    &#13;
 e, correlativamente, abrogativa  della  precedente  -  deve  peraltro    &#13;
 ritenersi   limitata  al  caso  di  contemporaneo  svolgimento  delle    &#13;
 elezioni politiche ed amministrative, dato  l'inequivoco  riferimento    &#13;
 in  tal  senso  operato dal titolo del testo legislativo in questione    &#13;
 che, tra l'altro, consente chiaramente  di  individuare  quali  norme    &#13;
 sono  in  ogni  caso  applicabili  (come,  ad es., l'art. 1) e quali,    &#13;
 invece, solo allorché le consultazioni siano contemporanee (come gli    &#13;
 artt.  2,  3,  4  e 5). Da qui una diversità di disciplina in ordine    &#13;
 alla necessità della vidimazione delle liste da parte del presidente    &#13;
 del  seggio  e  di  due  scrutatori  - alla cui omissione consegue la    &#13;
 nullità delle votazioni - del tutto priva  di  ragionevolezza  posto    &#13;
 che  la (scarsa) rilevanza garantistica dal legislatore evidentemente    &#13;
 attribuita a quell'adempimento formale nel caso in  cui  le  elezioni    &#13;
 politiche  ed  amministrative si svolgano in unico contesto temporale    &#13;
 non giustifica più una diversa disciplina allorché le consultazioni    &#13;
 elettorali amministrative avvengano separatamente.                       &#13;
   La  sollevata  questione  -  conclude  il  giudice a quo - non può    &#13;
 infine considerarsi già risolta dalla Corte costituzionale che,  con    &#13;
 sentenza  n.  43 del 1961, ritenne bensì la norma costituzionalmente    &#13;
 legittima ma in riferimento all'art. 48, secondo comma, Cost.; e che,    &#13;
 in   quella   sede,   operò   inoltre   il  raffronto  tra  elezioni    &#13;
 amministrative e politiche, concludendo che la non prevista  nullità    &#13;
 delle  votazioni  per  omessa  vidimazione  delle  liste  è,  per le    &#13;
 elezioni  politiche,  giustificata  dalla   considerazione   che   le    &#13;
 eventuali  invalidità  delle  operazioni elettorali sono deferite al    &#13;
 giudizio del Parlamento.                                                 &#13;
    2.  -  Il  Presidente  del  Consiglio dei ministri, intervenuto in    &#13;
 giudizio  tramite  l'Avvocatura  dello  Stato,  ha  chiesto  che   la    &#13;
 questione  venga  dichiarata  manifestamente infondata siccome basata    &#13;
 sull'erroneo presupposto che l'art. 2, lettera c), del  d.l.  n.  161    &#13;
 del  1976,  abbia  abrogato  la  norma  denunciata nella parte in cui    &#13;
 prevede  la  vidimazione  delle  liste,  con  riguardo  al  caso   di    &#13;
 contemporaneo  svolgimento delle elezioni amministrative e politiche.    &#13;
 All'omesso richiamo, da parte del d.l. n. 161 del 1976,  delle  norme    &#13;
 sulla  vidimazione  e  di  tutte quelle non direttamente connesse con    &#13;
 l'abbinamento delle elezioni amministrative con quelle politiche  non    &#13;
 può  invero  attribuirsi  alcuna  portata  abrogativa,  posto che il    &#13;
 provvedimento normativo mirava soltanto a coordinare  lo  svolgimento    &#13;
 contemporaneo   delle   operazioni  elettorali,  apportando  le  sole    &#13;
 modifiche necessarie per  stabilire  l'ordine  cronologico  dei  vari    &#13;
 adempimenti.  Della  immanente  vigenza della disposizione denunciata    &#13;
 costituirebbe poi un sintomo la circostanza che, pur  dopo  l'entrata    &#13;
 in  vigore  del  d.l.  n.  161  del  1976,  l'adempimento delle liste    &#13;
 elettorali continua ad essere previsto negli stampati predisposti per    &#13;
 i  seggi elettorali anche nel caso di contemporaneo svolgimento delle    &#13;
 elezioni politiche ed amministrative.<diritto>Considerato in diritto</diritto>1.  -  L'art.  53,  secondo  comma,  d.P.R. 16 maggio 1960, n. 570    &#13;
 prevede che, al termine delle votazioni per l'elezione  degli  organi    &#13;
 delle amministrazioni comunali, le liste degli elettori "prima che si    &#13;
 inizi lo spoglio dei voti, devono essere, a pena  di  nullità  della    &#13;
 votazione,  vidimate  in  ciascun  foglio  dal  presidente  e  da due    &#13;
 scrutatori e chiuse in piego sigillato...".                              &#13;
    Il 20 giugno 1976 si svolsero contemporaneamente le elezioni della    &#13;
 Camera dei  deputati  e  del  Senato  della  Repubblica  nonché  dei    &#13;
 consigli  delle  regioni  a  statuto ordinario, delle assemblee e dei    &#13;
 consigli delle regioni a statuto speciale, dei consigli provinciali e    &#13;
 dei consigli comunali.                                                   &#13;
    Nell'imminenza  di  tali  molteplici  consultazioni  ed in ragione    &#13;
 della loro concomitanza venne emanato il d.l. 3 maggio 1976, n.  161,    &#13;
 convertito  con  l.  14  maggio  1976,  n.  240, il cui art. 2 - alla    &#13;
 lettera c) - prevede che, in caso  di  contemporaneo  svolgimento  di    &#13;
 elezioni   politiche   ed  amministrative,  il  seggio,  ultimate  le    &#13;
 operazioni di riscontro dei voti, "procede alla formazione dei plichi    &#13;
 contenenti  gli  atti  relativi  a  tali operazioni nonché le schede    &#13;
 avanzate".                                                               &#13;
    Il   giudice  a  quo  rileva  che  quest'ultima  disposizione  non    &#13;
 contempla più l'adempimento della vidimazione  e  ne  fa  discendere    &#13;
 l'intervenuta  abrogazione dell'art. 53 del d.P.R. n. 570 del 1960 in    &#13;
 tutti i casi  in  cui  le  elezioni  amministrative  e  politiche  si    &#13;
 svolgano simultaneamente.                                                &#13;
    In  tale  ipotesi, l'omessa vidimazione delle liste, in quanto non    &#13;
 espressamente prescritta, non sarebbe più causa di nullità, in ciò    &#13;
 evidenziandosi  una  irrazionale  difformità  di disciplina rispetto    &#13;
 alle  consultazioni  elettorali   amministrative   che   si   tengono    &#13;
 indipendentemente dalle altre. Tale diversità di regime integrerebbe    &#13;
 il denunciato vizio di illegittimità costituzionale.                    &#13;
    2.  -  Dai lavori preparatori della l. 14 maggio 1976, n. 240, che    &#13;
 ha convertito il d.l. 3  maggio  1976,  n.  161,  risulta  la  natura    &#13;
 tecnica  del  provvedimento  legislativo,  finalizzato  ad uniformare    &#13;
 taluni  adempimenti   organizzativi,   in   precedenza   diversamente    &#13;
 disciplinati  a  seconda  del  tipo  di  elezione, onde consentire il    &#13;
 contemporaneo svolgimento di molteplici  consultazioni:  non  a  caso    &#13;
 l'art.  2  della  citata  l.  n.  240  del  1976  rinvia  alla futura    &#13;
 emanazione di un testo unico la sistemazione organica della  materia.    &#13;
 Il  procedimento  elettorale  è  risultato  semplificato,  ma nessun    &#13;
 intervento abrogativo è stato effettuato circa  l'adempimento  della    &#13;
 vidimazione delle liste.                                                 &#13;
    Questa  consiste  nella  sottoscrizione di ciascun foglio da parte    &#13;
 del presidente e di  due  scrutatori;  è  tradizionalmente  prevista    &#13;
 nell'ordinamento  sin  dal  T.U.  n.  5821  del  1889:  mediante tale    &#13;
 attività  il  seggio  attesta  di  aver  identificato  l'elettore  e    &#13;
 certificato  il  suo  voto  proprio  su  quella  lista  che era stata    &#13;
 autenticata dalla Commissione elettorale mandamentale. La vidimazione    &#13;
 ha  perciò finalità di accertamento, al pari di quella, ad esempio,    &#13;
 che la legge richiede al notaio per  il  libro-giornale  dell'impresa    &#13;
 (art.  2216  c.c.)  o  per i fogli sui quali è redatto il testamento    &#13;
 olografo  (art.  620  c.c.),  nonché  al  pretore  mandamentale  per    &#13;
 ciascuna  pagina  dei  registri  dello  stato civile (art. 20, r.d. 9    &#13;
 luglio 1939, n. 1238).                                                   &#13;
    Con  tale adempimento, in conclusione, si tende ad impedire che si    &#13;
 verifichino sostituzioni o manipolazioni delle liste  successivamente    &#13;
 alla  conclusione  delle  votazioni:  la  vidimazione  garantisce  la    &#13;
 certezza e  regolarità  delle  liste  medesime  onde  consentire  le    &#13;
 operazioni  di  riscontro  dei  voti  e  si  colloca perciò prima di    &#13;
 queste.                                                                  &#13;
    Viceversa l'art. 2 d.l. n. 161 del 1976, come ha statuito il Cons.    &#13;
 giust. amm. reg. Sicilia nella sentenza n. 170 del 5  dicembre  1984,    &#13;
 ha  riguardo  al  momento  -  cronologicamente successivo - dell'iter    &#13;
 procedimentale elettorale in cui, già effettuato il riscontro, sulla    &#13;
 base  di  liste perciò vidimate, devono essere formati i plichi, dei    &#13;
 quali, per le anzidette esigenze di coordinamento, è  prescritta  la    &#13;
 confezione  in unico contesto anziché, dispersivamente, alla fine di    &#13;
 ogni singola consultazione.                                              &#13;
    Deve  perciò  escludersi  che  la  norma  del  1976 abbia portata    &#13;
 abrogatrice delle disposizioni che prevedono  modalità  tipiche  dei    &#13;
 singoli  procedimenti  elettorali  ed  in  particolare  dell'art. 53,    &#13;
 d.P.R. n. 570 del 1960. L'esigenza accertativa  delle  liste  permane    &#13;
 comunque  anche in caso di elezioni abbinate: essa è sempre tutelata    &#13;
 attraverso l'obbligo della  vidimazione,  operazione  essenziale  per    &#13;
 ogni tipo di elezione che non può mai venire meno, rileva il giudice    &#13;
 sopra citato, sia che si tratti di consultazioni separate, sia che si    &#13;
 tratti di consultazioni contestuali.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 dichiara  non  fondata  la  questione  di legittimità costituzionale    &#13;
 dell'art. 53, secondo comma, d.P.R. 16 maggio 1960,  n.  570  ("Testo    &#13;
 unico  delle  leggi  per  la  composizione e la elezione degli organi    &#13;
 delle amministrazioni comunali"), sollevata in riferimento all'art. 3    &#13;
 Cost. dal T.A.R. per la Campania, sezione di Salerno, con l'ordinanza    &#13;
 in epigrafe.                                                             &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta,                            &#13;
 l'8 aprile 1987.                                                         &#13;
                       Il Presidente: LA PERGOLA                          &#13;
                       Il Redattore: FERRARI                              &#13;
    Depositata in cancelleria il 15 aprile 1987.                          &#13;
                        Il cancelliere: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
