import chardet
import functools
import json
import os

import requests

from akomantoso.dom import Judgment
from akomantoso.dom.elements import Base
from akomantoso.dom.meta import TLCPerson, TLCRole
from akomantoso.schema import validate
from akomantoso.serializer import serialize
from akomantoso.serializer.common import NS_MAP, AKN4CC
from akomantoso.utils import to_camelCase
from lxml import etree
from tqdm import tqdm

from cortecostituzionale import logger
from cortecostituzionale.parser.body import parse_body
from cortecostituzionale.parser.collegio import parse_collegio
from cortecostituzionale.parser.dispositivo import parse_dispositivo
from cortecostituzionale.parser.epigrafe import parse_epigrafe
from cortecostituzionale.parser.references import parse_references
from cortecostituzionale.settings import DATA_DIR
from cortecostituzionale.settings import EXIST_DB
from cortecostituzionale.db_manager import get_conn
from cortecostituzionale import __version__
from argparse import ArgumentParser
import multiprocessing as mp

tqdm.monitor_interval = 0

NS_MAP["akn4cc"] = AKN4CC

meta_map = dict(
    anno="year",
    numero="number",
    tipo="docname",
    data_deposito="publication_date",
    data_decisione="decision_date"
)


SOURCE_DIR = os.path.join(DATA_DIR, "sources", "xml", "all")
OUT_DIR = os.path.join(DATA_DIR, "out")
VALID_DIR = os.path.join(OUT_DIR, "valid")
INVALID_DIR = os.path.join(OUT_DIR, "invalid")
os.makedirs(VALID_DIR, exist_ok=True)
os.makedirs(INVALID_DIR, exist_ok=True)


def get_meta(pronuncia_testata):
    ecli = pronuncia_testata.find("ecli").text
    try:
        meta = dict(
            anno=int(pronuncia_testata.find("anno_pronuncia").text),
            numero=int(pronuncia_testata.find("numero_pronuncia").text),
            ecli=pronuncia_testata.find("ecli").text,
            tipo=pronuncia_testata.find("tipologia_pronuncia").text,
            presidente=pronuncia_testata.find("presidente").text,
            relatore=pronuncia_testata.find("relatore_pronuncia").text,
            data_decisione="-".join(reversed(pronuncia_testata.find("data_decisione").text.split("/"))),
            data_deposito="-".join(reversed(pronuncia_testata.find("data_deposito").text.split("/")))
        )
        try:
            meta["tipo_procedimento"] = pronuncia_testata.find("tipo_procedimento").text
            meta["dispositivo"] = pronuncia_testata.find("dispositivo").text
        except:
            meta["tipo_procedimento"] = None
            meta["dispositivo"] = None
        return meta
    except:
        msg = f"Fatal Error for {ecli}"
        logger.exception(msg)
        return None


def init_provvedimento(doc):
    meta = get_meta(doc.find("pronuncia_testata"))
    assert meta is not None, "Could not find meta"
    akn_meta = {meta_map.get(k, k): str(v) for k, v in meta.items() if meta_map.get(k, k) != k}
    akn_meta.update(dict(
        authority="Corte costituzionale",
        docname="sentenza" if akn_meta["docname"].lower() == "s" else "ordinanza",
        exist_db=EXIST_DB,
        ecli=meta["ecli"]
    ))
    provvedimento = Judgment(**akn_meta)
    meta.update(parser_version=__version__)
    for k, v in meta.items():
        if v is None:
            continue
        pe = Base(k, namespace="akn4cc")
        pe._text = str(v)
        provvedimento.add_proprietary(pe)
    dispositivo, tipo_procedimento = meta.get("dispositivo") or "unknown", meta.get("tipo_procedimento") or "unknown"
    other_analysis = Base("otherAnalysis", source="#corteCostituzionale")
    oa_dis = Base("dispositivo", namespace="akn4cc")
    oa_dis._text = dispositivo
    other_analysis.append(oa_dis)
    oa_proc = Base("tipo_procedimento", namespace="akn4cc")
    oa_proc._text = tipo_procedimento
    other_analysis.append(oa_proc)
    provvedimento.add_analysis(other_analysis)

    presidente_eid = to_camelCase(meta["presidente"])
    provvedimento.add_tlc(
        TLCPerson(eid=presidente_eid, href=f"/akn/ontology/person/it/{presidente_eid}", show_as=meta["presidente"])
    )
    relatore_eid = to_camelCase(meta["relatore"])
    provvedimento.add_tlc(
        TLCPerson(eid=relatore_eid, href=f"/akn/ontology/person/it/{relatore_eid}", show_as=meta["relatore"])
    )
    provvedimento.add_tlc(
        TLCRole(eid="presidenteCollegio", href=f"/akn/ontology/role/it/presidenteCollegio",
                show_as="Presidente del Collegio")
    )
    provvedimento.add_tlc(
        TLCRole(eid="relatore", href=f"/akn/ontology/role/it/relatore", show_as="Relatore")
    )
    provvedimento.add_tlc(
        TLCRole(eid="cancelliere", href=f"/akn/ontology/role/it/relatore", show_as="Cancelliere")
    )

    return provvedimento, meta


def add_tlcs(provvedimento):
    for name, component in provvedimento.components.items():
        for element in component.iter():
            for inline in element.inlines:
                reference = inline[2].get("refersTo")
                if reference is not None and not isinstance(reference, str):
                    provvedimento.add_tlc(reference, overwrite=True)
                subreferences = inline[2].get("$subreferences", [])
                for subref in subreferences:
                    subref = subref[2].get("refersTo")
                    if subref is not None and not isinstance(subref, str):
                        provvedimento.add_tlc(subref, overwrite=True)


def parse_doc(docstring):
    doc = etree.fromstring(docstring)
    provvedimento, meta = init_provvedimento(doc)
    pronuncia_testo = doc.find("pronuncia_testo")
    collegio = parse_collegio(pronuncia_testo.find("collegio"))
    provvedimento.header.append(collegio)
    introduction = parse_epigrafe(pronuncia_testo.find("epigrafe"))
    provvedimento.body.append(introduction)
    testo = pronuncia_testo.find("testo")
    if testo is not None:
        parse_body(testo, provvedimento.body)
    dispositivo = pronuncia_testo.find("dispositivo")
    parse_dispositivo(dispositivo, provvedimento)
    parse_references(provvedimento)
    add_tlcs(provvedimento)
    return provvedimento, meta


def get_rows_to_parse(cur, override):
    sql = "SELECT ecli FROM pronunce"
    if not override:
        sql += " WHERE parsed=? OR parser_version !=?"
        return [r["ecli"] for r in cur.execute(sql, (False, __version__,)).fetchall()]
    else:
        return [r["ecli"] for r in cur.execute(sql).fetchall()]


def parse_one(ecli, push=False):
    tipo_map = {"o": "ordinanza", "s": "sentenza"}
    try:
        filename = ecli.replace(":", "-") + ".xml"
        filepath = os.path.join(SOURCE_DIR, filename)
        with open(filepath, "rb") as f:
            b = f.read()
            enc = chardet.detect(b)["encoding"]
            text = b.decode(encoding=enc).replace("&amp;#8210;", "-")
            provvedimento, meta = parse_doc(text)
        akn_string = serialize(provvedimento, as_string=True)
        if push:
            try:
                provvedimento.push(silent=True)
            except Exception as e:
                tqdm.write(f"Could not push {ecli}: {e}")
                logger.exception(f"Could not push {ecli}: {e}")
        isvalid, validation = validate(akn_string)
        anno, tipo = meta["anno"], tipo_map[meta["tipo"].lower()]
        if isvalid:
            outdir = os.path.join(VALID_DIR, str(anno), tipo)
            os.makedirs(outdir, exist_ok=True)
            with open(os.path.join(outdir, filename), "w") as f:
                f.write(akn_string)
        else:
            outdir = os.path.join(INVALID_DIR, str(anno), tipo)
            os.makedirs(outdir, exist_ok=True)
            with open(os.path.join(outdir, filename), "w") as f:
                f.write(akn_string)
            with open(os.path.join(outdir, filename.replace(".xml", "-validation.json")), "w") as f:
                json.dump(validation, f)
        return int(isvalid), ecli
    except Exception as e:
        msg = f"Fatal Error for {ecli}: {e}"
        tqdm.write(msg)
        logger.exception(msg)
        return -1, ecli


def parse_all(override=False, push=False):
    logger.info("PARSING ALL")
    conn = get_conn()
    cur = conn.cursor()
    pronunce = get_rows_to_parse(cur, override)
    total_valid = 0
    total_invalid = 0
    total_fatal = 0
    pool = mp.Pool(mp.cpu_count())
    parse_func = functools.partial(parse_one, push=push)
    for valid, ecli in tqdm(pool.imap_unordered(parse_func, pronunce), total=len(pronunce)):
        if valid == 1:
            total_valid += 1
        elif valid == 0:
            total_invalid += 1
        else:
            total_fatal += 1
        if valid > -1:
            with conn:
                cur.execute(
                    """
                    UPDATE pronunce
                    SET parsed=?, valid=?, parser_version=?
                    WHERE ecli=?
                    """, (True, valid, __version__, ecli)
                )
    pool.close()
    pool.join()
    stats = f"Total: {total_valid + total_invalid + total_fatal}\n" \
        f"Valid: {total_valid}\nInvalid: {total_invalid}\n" \
        f"Fatal: {total_fatal}"
    logger.info(f"ALL PARSED\n{stats}")
    if total_valid + total_invalid > 0:
        requests.get(
            "http://bach.cirsfid.unibo.it:8081/exist/rest/db/portal-corte-costituzionale/countDocsTmp.xq?full_refresh=true",
            auth=(EXIST_DB["user"], EXIST_DB["password"])
        )
    print("\n", stats)


def test(anno, numero):
    filepath = f"/Users/biagio/repos/CIRSFID/cortecostituzionale-py/data/sources/xml/all/ECLI-IT-COST-{anno}-{numero}.xml"
    with open(filepath, "rb") as f:
        b = f.read()
        enc = chardet.detect(b)["encoding"]
        text = b.decode(encoding=enc).replace("&amp;#8210;", "-")
        provvedimento, meta = parse_doc(text)
    with open(provvedimento.ecli.replace(":", "-") + ".xml", "w") as f:
        akn_string = serialize(provvedimento, as_string=True)
        provvedimento.push(silent=True)
        f.write(akn_string)
    print(validate(akn_string))


if __name__ == '__main__':
    # test(1957, 27)
    # test(2019, 208)
    # exit()
    argparser = ArgumentParser()
    argparser.add_argument("--override", action="store_true", default=False, help="Overrides existing docs")
    argparser.add_argument("--push", action="store_true", default=False, help="Push to eXistDB")
    args = argparser.parse_args()
    parse_all(override=args.override, push=args.push)
