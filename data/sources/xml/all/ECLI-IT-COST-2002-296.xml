<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>296</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:296</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VARI</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>19/06/2002</data_decisione>
    <data_deposito>28/06/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Massimo VARI; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, &#13;
Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco &#13;
AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 242 e 245 del &#13;
decreto  legislativo  28 luglio 1989, n. 271 (Norme di attuazione, di &#13;
coordinamento  e transitorie del codice di procedura penale) promosso &#13;
con  ordinanza  emessa  il  13 luglio  2001  dalla Corte di assise di &#13;
Palermo  nel procedimento penale a carico di G.A., iscritta al n. 786 &#13;
del  registro  ordinanze  2001  e pubblicata nella Gazzetta Ufficiale &#13;
della Repubblica, 1ª serie speciale, n. 40 dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio  del 13 marzo 2002 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto che, con ordinanza emessa il 13 luglio 2001, la Corte di &#13;
assise di Palermo ha sollevato, in riferimento agli artt. 3, 24 e 111 &#13;
della  Costituzione,  questione  di legittimità costituzionale degli &#13;
artt. 242 e 245 del decreto legislativo 28 luglio 1989, n. 271 (Norme &#13;
di attuazione, di coordinamento e transitorie del codice di procedura &#13;
penale),  nella  parte  in  cui  prevedono,  a date condizioni, che i &#13;
procedimenti penali in corso alla data di entrata in vigore del nuovo &#13;
codice  di  procedura  penale proseguano, anche dopo le modificazioni &#13;
apportate  all'art. 111 della Costituzione dalla legge costituzionale &#13;
n. 2  del  1999,  con  l'osservanza delle norme vigenti anteriormente &#13;
all'entrata in vigore del nuovo codice di procedura penale; &#13;
        che  il  rimettente - dopo aver premesso che, in esito ad una &#13;
pronuncia  del  17 febbraio  1998  della Corte di assise d'appello di &#13;
Palermo,   era  stata  dichiarata  la  nullità,  limitatamente  alla &#13;
posizione  di  un  imputato,  della  sentenza di un giudizio di primo &#13;
grado definito applicando le norme del codice di rito del 1930; e che &#13;
era  stata  nuovamente  disposta  la  citazione  a  giudizio di detto &#13;
imputato   dinanzi   ad  esso  rimettente  -  ha  osservato  come  il &#13;
dibattimento,  così  instaurato,  debba ritenersi "assoggettato alla &#13;
disciplina  transitoria  di cui al titolo III delle norme transitorie &#13;
del  codice  di  procedura  penale,  di cui al d.lgs. 28 luglio 1989, &#13;
n. 271":  con  conseguente applicazione delle disposizioni del codice &#13;
abrogato, relativamente alla formazione ed utilizzazione delle prove; &#13;
        che  in particolare, a parere del giudice a quo, risulterebbe &#13;
inapplicabile  la  disciplina  transitoria  dettata dal decreto-legge &#13;
7 gennaio   2000,   n. 2   (Disposizioni   urgenti  per  l'attuazione &#13;
dell'art. 2  della  legge  costituzionale  23 novembre 1999, n. 2, in &#13;
materia  di  giusto  processo),  convertito, con modificazioni, nella &#13;
legge  25 febbraio 2000, n. 35: ciò in quanto il comma 2 dell'art. 2 &#13;
del  citato  decreto-legge n. 2 del 2000, nel prevedere l'eccezionale &#13;
utilizzabilità  delle  dichiarazioni  rese  nel corso delle indagini &#13;
preliminari  da  chi, per libera scelta, si è sempre volontariamente &#13;
sottratto  all'esame  dell'imputato  o  del  suo  difensore,  se già &#13;
acquisite  al  fascicolo  del  dibattimento  al momento di entrata in &#13;
vigore  del decreto in questione, non potrebbe applicarsi - se non in &#13;
via  analogica  e,  dunque,  illegittimamente  -  ai procedimenti che &#13;
continuano  secondo  il  "vecchio  rito"  e  per  i  quali non si sia &#13;
verificata  la  condizione di una sottrazione volontaria e per libera &#13;
scelta del dichiarante al confronto con l'accusato; &#13;
        che  inoltre,  sempre  ad  avviso  del  rimettente, il regime &#13;
transitorio  del  decreto-legge  n. 2  del 2000 risulterebbe comunque &#13;
inapplicabile  nel  giudizio  a quo, essendo previsto (art. 1), quale &#13;
suo  "preciso  termine  finale  di  efficacia", quello della "data di &#13;
entrata   in   vigore   della   legge   che  disciplina  l'attuazione &#13;
dell'art. 111  della Costituzione"; questa disciplina, introdotta con &#13;
la  legge  1  marzo  2001, n. 63, non contempla tuttavia, nella norma &#13;
transitoria   dell'art. 26,   "alcuna   disposizione   riferibile  ai &#13;
procedimenti  in  corso,  che  proseguono - o dovrebbero proseguire - &#13;
secondo il vecchio rito": con l'impossibilità di desumere, peraltro, &#13;
"in via interpretativa, una sorta di ultrattività della norma di cui &#13;
al comma 6 dell'art. 1 del decreto-legge n. 2 del 2000"; &#13;
        che  tuttavia,  a  parere  del  giudice  a  quo,  proprio  la &#13;
circostanza  che "l'unica disciplina transitoria tuttora vigente" sia &#13;
quella  sancita  nella normativa censurata, comporta l'applicabilità &#13;
delle  norme  processuali  in  materia di formazione della prova già &#13;
contenute  nel codice del 1930, le quali - consentendo, attraverso il &#13;
meccanismo  delle letture dibattimentali, la piena utilizzabilità di &#13;
tutte   le   prove   assunte  nel  corso  dell'istruzione  formale  - &#13;
determinerebbero  la  violazione del principio della formazione della &#13;
prova nel contraddittorio tra le parti e del diritto dell'imputato di &#13;
interrogare  o far interrogare le persone che rendono dichiarazioni a &#13;
suo carico, espressi, entrambi, nell'art. 111 Cost.; &#13;
        che,  sotto  analogo profilo, risulterebbero altresì violati &#13;
gli artt. 3 e 24 della Costituzione, per la ingiustificata disparità &#13;
di  trattamento  tra  imputati  di  procedimenti in corso, in ragione &#13;
della "prosecuzione in conformità alle norme del codice abrogato per &#13;
la  materia  che  concerne la formazione e le modalità di assunzione &#13;
delle  prove",  con  violazione  anche  del  diritto dell'imputato di &#13;
esercitare  il  proprio  diritto  di difesa nel contraddittorio delle &#13;
parti; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato, che ha concluso per l'infondatezza della questione proposta. &#13;
    Considerato  che  le  censure  prospettate dal rimettente muovono &#13;
dalla  ritenuta inapplicabilità della disciplina transitoria dettata &#13;
dal    decreto-legge    7 gennaio   2000,   n. 2,   convertito,   con &#13;
modificazioni,  dalla  legge 25 febbraio 2000, n. 35, ai procedimenti &#13;
penali  che,  a  mente  degli artt. 242 e 245 del decreto legislativo &#13;
28 luglio  1989,  n. 271, proseguono con l'osservanza delle norme del &#13;
codice di rito abrogato; &#13;
        che,  tuttavia, tale presupposto ermeneutico appare inesatto, &#13;
poiché  lo  stesso  legislatore  costituzionale  ha  sancito,  nella &#13;
medesima legge di riforma sul "giusto processo", che "la legge regola &#13;
l'applicazione   dei   principi   contenuti   nella   presente  legge &#13;
costituzionale  ai  procedimenti  penali in corso alla data della sua &#13;
entrata  in  vigore"  (art. 2,  legge  cost. 23 novembre 1999, n. 2): &#13;
demandando,  dunque,  al legislatore ordinario l'approntamento di una &#13;
disciplina  transitoria  "atta  a  modulare  l'applicazione  di  quei &#13;
principi  nei  processi  in  corso di celebrazione, secondo una linea &#13;
tesa  chiaramente  a  tracciare  un  "ponte"  normativo  destinato  a &#13;
mitigare  una  drastica applicazione della regola tempus regit actum" &#13;
(v. sentenza n. 381 del 2001); e tale disciplina è stata realizzata, &#13;
appunto,  con  il  citato decreto-legge n. 2 del 2000, il cui art. 1, &#13;
comma  6,  espressamente sancisce l'applicabilità delle disposizioni &#13;
di  cui ai commi precedenti anche "ai procedimenti che proseguono con &#13;
le norme del codice di procedura penale anteriormente vigente"; &#13;
        che   pertanto   se,   per   un  verso,  appare  evidente  la &#13;
riferibilità  ai  soli  processi  di  "nuovo  rito"  dello specifico &#13;
termine,  enunciato  dal  comma  2  dell'art. 1 del più volte citato &#13;
decreto-legge  n. 2  del  2000,  individuato  con  il  richiamo  alla &#13;
intervenuta acquisizione al fascicolo per il dibattimento dei verbali &#13;
contenenti   le   dichiarazioni   rese   nel   corso  delle  indagini &#13;
preliminari;  sotto  altro profilo, deve sottolinearsi come non possa &#13;
affatto  ritenersi  incompatibile,  con  i  procedimenti  in corso di &#13;
celebrazione  secondo  le  norme  previste  dal  codice  abrogato, la &#13;
previsione  -  ivi enunciata - secondo la quale le dichiarazioni rese &#13;
da  chi  si  è  sempre  sottratto  al contraddittorio debbano essere &#13;
valutate  "solo  se  la  loro  attendibilità  è confermata da altri &#13;
elementi  di  prova, assunti o formati con diverse modalità": regola &#13;
di  valutazione  probatoria, quella testé rammentata, la cui portata &#13;
ed il cui risalto questa Corte ha già avuto modo di scrutinare nella &#13;
richiamata sentenza n. 381 del 2001; &#13;
        che,  per  altro  verso,  neppure  pertinente  si  rivela  il &#13;
richiamo  ad una pretesa "ultrattività" della previsione dettata dal &#13;
comma  6  dell'art. 1  del  medesimo d.l. n. 2 del 2000; infatti - in &#13;
considerazione   della  profonda  diversità  che  caratterizzava  il &#13;
modello  processuale  delineato dal codice abrogato - la pendenza dei &#13;
procedimenti  che  proseguivano  con  l'osservanza  delle  previsioni &#13;
dettate   da  quello  stesso  codice  non  poteva  che  produrre  una &#13;
"cristallizzazione"  del regime transitorio suddetto, proprio perché &#13;
fondato su un unico fascicolo processuale e sulla assenza di una fase &#13;
delle  indagini  preliminari:  ciò  rende  dunque evidente che per i &#13;
procedimenti  richiamati dallo stesso comma 6, la relativa disciplina &#13;
non doveva "combinarsi" con quella di attuazione dell'art. 111 Cost., &#13;
poi introdotta ad opera della legge n. 63 del 2001; &#13;
        che,  pertanto, la questione proposta, muovendo da un erroneo &#13;
presupposto interpretativo, è da ritenersi manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87  e  9,  secondo  comma,  delle  norme integrative per i giudizi &#13;
avanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità   costituzionale  degli  artt. 242  e  245  del  decreto &#13;
legislativo   28 luglio   1989,   n. 271  (Norme  di  attuazione,  di &#13;
coordinamento   e   transitorie  del  codice  di  procedura  penale), &#13;
sollevata,   in   riferimento   agli  articoli  3,  24  e  111  della &#13;
Costituzione,  dalla  Corte  di  assise  di  Palermo, con l'ordinanza &#13;
indicata in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 19 giugno 2002. &#13;
                         Il Presidente: Vari &#13;
                         Il redattore: Flick &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 28 giugno 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
