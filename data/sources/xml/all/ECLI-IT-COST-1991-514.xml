<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>514</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:514</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1991</data_decisione>
    <data_deposito>30/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 33 della  legge    &#13;
 24  novembre 1981, n. 689 (Modifiche al sistema penale), promosso con    &#13;
 ordinanza emessa il 5  dicembre  1990  dal  Pretore  di  Sassari  nel    &#13;
 procedimento penale a carico di Valentino Saba iscritta al n. 471 del    &#13;
 registro  ordinanze  1991 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 88, prima serie speciale, dell'anno 1991;                  &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 4 dicembre il Giudice relatore    &#13;
 Enzo Cheli;                                                              &#13;
    Ritenuto che nel procedimento penale a carico di  Valentino  Saba,    &#13;
 imputato  di  violazione  degli  artt.  16  e 17 del regio decreto 14    &#13;
 luglio 1898, n. 404, per aver omesso di custodire convenientemente il    &#13;
 bestiame  bovino  di  sua  proprietà,  il  Pretore  di  Sassari   ha    &#13;
 dichiarato  rilevante e non manifestamente infondata - in riferimento    &#13;
 all'art.  3  della  Costituzione  -  la  questione  di   legittimità    &#13;
 costituzionale  dell'art.  33  della  legge  24 novembre 1981, n. 689    &#13;
 (Modifiche al sistema penale), nella  parte  in  cui  circoscrive  la    &#13;
 depenalizzazione  solo  alla norma contenuta nell'art. 672 del codice    &#13;
 penale (omessa custodia e malgoverno di animali) e mantiene  in  vita    &#13;
 la  sanzione  penale  per  "la  condotta  sostanzialmente uguale o al    &#13;
 limite meno grave sotto il profilo della tutela di beni rilevanti per    &#13;
 l'ordinamento" contemplata negli artt. 16 e 17 del regio  decreto  14    &#13;
 luglio  1898, n. 404 (Approvazione del regolamento per la repressione    &#13;
 dell'abigeato e del pascolo abusivo in Sardegna);                        &#13;
      che nella ordinanza di rinvio si espone  che,  a  seguito  della    &#13;
 legge  n. 689 del 1981, la contravvenzione prevista dall'art. 672 del    &#13;
 codice penale per l'omessa custodia ed il malgoverno di  animali  non    &#13;
 costituisce   più   reato   ed   è   soggetta  alla  sola  sanzione    &#13;
 amministrativa pecuniaria, mentre - in forza degli artt. 16 e 17  del    &#13;
 regio  decreto  n.  404 del 1898 - il soggetto che, nei propri fondi,    &#13;
 non custodisca il bestiame di sua proprietà in modo da  evitare  che    &#13;
 sia  danneggiata la proprietà altrui "è punito o con l'arresto fino    &#13;
 a tre mesi o con l'ammenda fino a. 400.000 se si ritiene  che  l'art.    &#13;
 434  del codice penale abrogato sia stato sostituito dal vigente art.    &#13;
 650 del codice penale oppure fino alla pena massima dell'art. 636 del    &#13;
 codice penale";                                                          &#13;
      che, ad avviso del giudice a quo, tale situazione  darebbe  vita    &#13;
 ad  una ingiustificata "disparità di trattamento tra i cittadini che    &#13;
 vivono nelle regioni ove trova applicazione il regio decreto  n.  404    &#13;
 del 1898 e quelli che vivono altrove" giacché, per questi ultimi, la    &#13;
 omessa  custodia  di  bestiame o rappresenta una condotta irrilevante    &#13;
 per l'ordinamento o è punita con la  sola  sanzione  amministrativa,    &#13;
 mentre  le fattispecie regolate dagli artt. 16 e 17 del regio decreto    &#13;
 n. 404 del 1898 rendono applicabile una più grave sanzione di natura    &#13;
 penale;                                                                  &#13;
      che nel giudizio dinanzi alla Corte ha  spiegato  intervento  il    &#13;
 Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura generale dello Stato, chiedendo che la questione  sia    &#13;
 dichiarata inammissibile o infondata.                                    &#13;
    Considerato   che   nelle  tre  ipotesi  di  illecito  contemplate    &#13;
 nell'art. 672 del codice penale  l'oggetto  della  tutela  penale  è    &#13;
 costituito  dalla  incolumità  pubblica,  che  può essere esposta a    &#13;
 pericolo dalla omessa custodia o dal malgoverno di animali o da altri    &#13;
 comportamenti che rendano pericolosi gli animali per le persone;         &#13;
      che, invece, negli artt. 16 e 17 del  regio  decreto  14  luglio    &#13;
 1898,  n.  404,  l'oggetto della tutela penale è rappresentato dalla    &#13;
 proprietà fondiaria che si intende salvaguardare da sconfinamenti  e    &#13;
 danneggiamenti ad opera di animali privi di adeguata custodia;           &#13;
      che,  in  ragione  della  diversità  dei  beni protetti, le due    &#13;
 normative   poste   a   confronto   dal   giudice   remittente   sono    &#13;
 insuscettibili di comparazione sotto il profilo del differente regime    &#13;
 sanzionatorio   previsto  per  la  loro  violazione  e  della  scelta    &#13;
 discrezionalmente compiuta dal legislatore di depenalizzare  solo  la    &#13;
 contravvenzione prevista dall'art. 672 del codice penale;                &#13;
      che,  pertanto - a parte ogni considerazione sulla sopravvivenza    &#13;
 delle disposizioni dettate dagli artt. 16 e 17 del regio  decreto  n.    &#13;
 404  del 1898 - la questione di legittimità costituzionale dell'art.    &#13;
 33 della legge 24 novembre 1981, n. 689,  sollevata,  in  riferimento    &#13;
 all'art.  3  della Costituzione, dal Pretore di Sassari va dichiarata    &#13;
 manifestamente infondata.                                                &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 33 della legge  24  novembre  1981,  n.  689    &#13;
 (Modifiche al sistema penale), nella parte in cui non comprende tra i    &#13;
 casi  di depenalizzazione le fattispecie contemplate negli artt. 16 e    &#13;
 17 del regio  decreto  14  luglio  1898,  n.  404  (Approvazione  del    &#13;
 regolamento per la repressione dell'abigeato e del pascolo abusivo in    &#13;
 Sardegna),  sollevata,  in riferimento all'art. 3 della Costituzione,    &#13;
 dal Pretore di Sassari con la ordinanza di cui in epigrafe.              &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 19 dicembre 1991.                             &#13;
                       Il presidente: CORASANITI                          &#13;
                          Il redattore: CHELI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 30 dicembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
