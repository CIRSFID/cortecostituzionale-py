<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>129</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:129</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>27/03/2003</data_decisione>
    <data_deposito>16/04/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 521-bis, comma 1, del codice di procedura penale, promosso, nell'ambito di un procedimento penale, dal Tribunale di Pistoia con ordinanza del 20 settembre 2002, iscritta al n. 506 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 47, prima serie speciale, dell'anno 2002. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 12 marzo 2003 il Giudice relatore Guido Neppi Modona.  &#13;
    Ritenuto che il Tribunale di Pistoia in composizione collegiale ha sollevato questione di legittimità costituzionale dell'art. 521-bis, comma 1, del codice di procedura penale, nella parte in cui non prevede la trasmissione degli atti al pubblico ministero in tutte le ipotesi in cui, a seguito della modifica dell'imputazione ex art. 516, commi 1-bis e 1-ter, cod. proc. pen. o della contestazione di un reato concorrente ex art. 517 cod. proc. pen., in relazione a fatti che già risultavano dagli atti di indagine, il reato è attribuito alla cognizione del tribunale in composizione collegiale anziché monocratica;  &#13;
    che il rimettente premette che in esito all'udienza preliminare era stato disposto il giudizio davanti al tribunale in composizione monocratica per il reato di bancarotta semplice (art. 217 del r.d. 16 marzo 1942, n. 267) e che nel corso del dibattimento il pubblico ministero aveva modificato l'imputazione in quella di bancarotta fraudolenta documentale (art. 216, primo comma, n. 2) ed aveva contestato altresì il reato concorrente di bancarotta fraudolenta patrimoniale (art. 216, primo comma, n. 1), in relazione ad un fatto - distrazione di somme di denaro - già risultante dagli atti di indagine; &#13;
    che a norma dell'art. 33-septies, comma 1, cod. proc. pen. era stata disposta la trasmissione degli atti al tribunale in composizione collegiale, e cioè all'attuale rimettente, che, invece, individuava nell'art. 521-bis cod. proc. pen. la disposizione applicabile nel caso di specie, concernente la modifica della composizione del giudice a seguito di nuove contestazioni e non già a seguito di una originaria, erronea individuazione del giudice; &#13;
    che ad avviso del giudice a quo tale disposizione, limitando i casi di trasmissione degli atti al pubblico ministero ai soli giudizi con citazione diretta, sacrifica la possibilità dell'imputato di accedere ai riti alternativi e, segnatamente, al giudizio abbreviato «nella sede appropriata e in relazione alla complessiva, corretta imputazione»; &#13;
    che, al riguardo, il rimettente richiama la sentenza n. 265 del 1994, con la quale la Corte ha dichiarato, in riferimento agli artt. 3 e 24 Cost., la illegittimità costituzionale degli artt. 516 e 517 cod. proc. pen., nella parte in cui non prevedono la facoltà dell'imputato di richiedere al giudice del dibattimento l'applicazione della pena ex art. 444 cod. proc. pen. relativamente al fatto diverso o al reato concorrente contestato in dibattimento, quando la nuova contestazione concerne un fatto che già risultava dagli atti di indagine al momento dell'esercizio dell'azione penale; &#13;
    che il rimettente dà atto che con la stessa sentenza la Corte ha dichiarato inammissibile analoga questione concernente il giudizio abbreviato, in quanto, stante l'inconciliabilità di tale rito con la struttura del dibattimento, non era ravvisabile una soluzione costituzionalmente obbligata, ma erano possibili più opzioni rientranti nella sfera della discrezionalità del legislatore;  &#13;
    che, ad avviso del giudice a quo, «tale motivazione può essere ora rivista»  in quanto in tutti i casi in cui le nuove contestazioni trovino fondamento in elementi di prova raccolti nel corso delle indagini preliminari, la restituzione degli atti al pubblico ministero consentirebbe all'imputato di presentare richiesta di giudizio abbreviato nell'udienza preliminare; &#13;
    che, infine, il giudice a quo ritiene che la questione è rilevante «a prescindere dal fatto che l'imputato o il suo difensore in forza di procura speciale abbia chiesto o meno […] il giudizio abbreviato per le nuove contestazioni, poiché in questa sede è certo che nessuna richiesta di tal genere sarebbe ammissibile»; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata non fondata. &#13;
    Considerato che il rimettente lamenta che l'art. 521-bis del codice di procedura penale non prevede la trasmissione degli atti al pubblico ministero in tutte le ipotesi in cui, a seguito della modifica dell'imputazione (art. 516, commi 1-bis e 1-ter, cod. proc. pen.) o della contestazione di un reato concorrente (art. 517 cod. proc. pen.) in relazione a fatti che già risultavano dagli atti di indagine, il reato è attribuito alla cognizione del tribunale in composizione collegiale anziché monocratica; &#13;
    che, in effetti, la disposizione censurata prevede la trasmissione degli atti al pubblico ministero nell'ipotesi in cui, a seguito delle nuove contestazioni, il reato risulti tra quelli attribuiti alla cognizione del tribunale per cui è prevista l'udienza preliminare e questa non si è tenuta, e non anche nella situazione - ricorrente nel caso di specie - in cui il reato originariamente contestato rientri tra quelli attribuiti alla cognizione del tribunale in composizione monocratica per i quali è prevista l'udienza preliminare, e questa si è regolarmente tenuta; &#13;
    che il giudice a quo non indica espressamente i parametri in riferimento ai quali solleva la questione di legittimità costituzionale, ma questi possono essere indirettamente individuati, attraverso il richiamo alla sentenza n. 265 del 1994 contenuto nell'ordinanza di rimessione, negli artt. 3 e 24 Cost., posto che il rimettente si duole del fatto che il diritto di difesa dell'imputato rimanga sacrificato da una disciplina che irragionevolmente gli preclude di chiedere il giudizio abbreviato in relazione alle nuove imputazioni; &#13;
    che il rimettente dà atto che l'imputato non ha presentato alcuna richiesta di giudizio abbreviato, ma ritiene che tale mancanza non incida sulla rilevanza della questione, in quanto la richiesta avanzata nella sede dibattimentale sarebbe stata comunque inammissibile; &#13;
    che, tuttavia, dipendendo il rito alternativo in esame dalla iniziativa della parte, ed essendo escluso ogni potere officioso del giudice, in mancanza di una concreta manifestazione di volontà dell'imputato di accedere al giudizio abbreviato difetta il necessario requisito della pregiudizialità della questione rispetto alla definizione del giudizio a quo; &#13;
    che, contrariamente a quanto ritiene il Tribunale, ove l'imputato avesse formulato una simile richiesta, questa non avrebbe dovuto essere dichiarata inammissibile, atteso che rispetto a tale decisione si sarebbe posta appunto come pregiudiziale la questione di costituzionalità di una disciplina che, stando alla stessa prospettazione del rimettente, priverebbe l'imputato della facoltà di accedere al giudizio abbreviato; &#13;
    che pertanto la questione si rivela del tutto ipotetica, in quanto la sua rilevanza è condizionata all'eventualità non solo futura, ma anche del tutto incerta che, in caso di accoglimento, l'imputato presenti effettivamente richiesta di giudizio abbreviato; &#13;
    che la questione va pertanto dichiarata manifestamente inammissibile.  &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 521-bis del codice di procedura penale, sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dal Tribunale di Pistoia, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 27 marzo 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 16 aprile 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
