<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>80</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:80</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/03/1998</data_decisione>
    <data_deposito>26/03/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità  costituzionale  dell'art.  633,  ultimo    &#13;
 comma,  del codice di procedura civile, promosso con ordinanza emessa    &#13;
 il 28 marzo  1997  dal  pretore  di  Ancona,  sezione  distaccata  di    &#13;
 Fabriano,   nel   procedimento   civile  vertente  tra  Fad  Fabriano    &#13;
 Autoadesivi S.p.a.  e Garoufalis Dimitrios, iscritta al  n.  401  del    &#13;
 registro  ordinanze  1997 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica, n.27, prima serie speciale, dell'anno 1997.                  &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  dell'11  marzo 1998 il giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
   Ritenuto che - a séguito  di un ricorso per decreto ingiuntivo  nei    &#13;
 confronti  di  una  società  avente  la  propria sede in Grecia - il    &#13;
 pretore di Ancona, sezione  distaccata  di  Fabriano,  con  ordinanza    &#13;
 emessa  il  28  marzo  1997,  ha  sollevato questione di legittimità    &#13;
 costituzionale dell'art. 633, ultimo comma, del codice  di  procedura    &#13;
 civile,  secondo  cui  l'ingiunzione  di  pagamento  non  può essere    &#13;
 pronunciata se la  notificazione  all'intimato  deve  avvenire  fuori    &#13;
 dalla Repubblica;                                                        &#13;
     che - dopo aver affermato di non poter disapplicare la denunciata    &#13;
 norma  in  relazione alle affermazioni contenute nella sentenza della    &#13;
 Corte di giustizia delle comunità europee del 13 luglio 1995  (nella    &#13;
 causa  C-474/1993),  la  quale  si  limita  a statuire in ordine alla    &#13;
 compatibilità del procedimento monitorio italiano  con  i  princi'pi    &#13;
 comunitari  -  il rimettente rileva come, stante l'equiparabilità in    &#13;
 tutto e per  tutto  del  procedimento  monitorio  a  qualsiasi  altro    &#13;
 provvedimento  giurisdizionale  nell'a'mbito comunitario, l'operatore    &#13;
 economico  interno  risulta  fortemente  penalizzato,   non   potendo    &#13;
 adeguatamente  tutelare  il  suo  credito  allorquando il debitore si    &#13;
 trovi all'estero;                                                        &#13;
     che, pertanto, la denunciata disposizione violerebbe:  a)  l'art.    &#13;
 3  della Costituzione, ponendo un'incongrua disparità di trattamento    &#13;
 con lo  straniero  appartenente  ad  uno  degli  Stati  membri  della    &#13;
 comunità,  che  si  avvalga  di una procedura sommaria equipollente,    &#13;
 ovvero con chi utilizzi  qualsiasi  altra  procedura  giurisdizionale    &#13;
 civile,  italiana  o meno, nonché con chi richieda in corso di causa    &#13;
 un'ordinanza  ex   art.   186-ter   cod.   proc.   civ.,   alla   cui    &#13;
 pronunciabilità  non  sono opposte reclusioni di sorta; b) l'art. 24    &#13;
 della Costituzione, in quanto non permette all'interessato  di  agire    &#13;
 per   la   tutela   del  suo  credito  con  tutti  i  mezzi  previsti    &#13;
 dall'ordinamento,  escludendo  senza  idonea   ragione   proprio   il    &#13;
 procedimento  monitorio,  che  appare tipicamente previsto in caso di    &#13;
 fornitura di merci non pagate; c) l'art. 41  della  Costituzione,  in    &#13;
 quanto viene di fatto imposta una restrizione all'attività economica    &#13;
 -   disincentivando   i   rapporti  commerciali  con  l'estero  -  in    &#13;
 conseguenza  dell'immotivato  difetto  di  tutela  dei  rapporti   di    &#13;
 interscambio che hanno raggiunto un'elevata frequenza;                   &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 concludendo per l'infondatezza della sollevata questione.                &#13;
   Considerato  che  questa  Corte,  in  un  precedente  scrutinio  di    &#13;
 costituzionalità della stessa  disposizione,  allora  censurata  con    &#13;
 riferimento  all'art.    10 della Costituzione, ha già affermato che    &#13;
 nei patti comunitari non si configurano princi'pi generali  incidenti    &#13;
 nella  materia  del  processo,  lasciata  alla disciplina del diritto    &#13;
 interno degli Stati, e che il divieto di notificazione all'estero del    &#13;
 decreto ingiuntivo determina solo una causa di inammissibilità della    &#13;
 speciale  tutela  monitoria  e  non  un  difetto  di   giurisdizione,    &#13;
 potendosi agire in sede ordinaria (ordinanza n. 364 del 1989);           &#13;
     che  proprio la possibilità per il creditore di utilizzare tutti    &#13;
 gli altri  strumenti  processuali  offertigli  in  sede  ordinaria  e    &#13;
 cautelare  - ivi compresi i provvedimenti anticipatori di condanna di    &#13;
 cui agli artt. 186-bis e segg. cod. proc. civ. - onde far  valere  il    &#13;
 proprio  diritto,  esclude  la  paventata  menomazione del diritto di    &#13;
 difesa e di azione  dello  stesso,  nonché,  conseguentemente  e  di    &#13;
 riflesso,  la  restrizione  della  sua  piena  libertà di iniziativa    &#13;
 economica;                                                               &#13;
     che, per quanto riguarda la denunciata disparità di trattamento,    &#13;
 mentre si palesa inconfigurabile l'ipotizzata comparabilità  con  la    &#13;
 posizione  dello  straniero  che  si  avvalga di non meglio precisate    &#13;
 procedure sommarie equipollenti approntate da altri Stati comunitari,    &#13;
 va ribadito quanto la Corte ha  già  ripetutamente  sottolineato  in    &#13;
 ordine  alla non omogeneità, in termini di natura e di funzione, del    &#13;
 provvedimento monitorio rispetto all'ordinanza disciplinata dall'art.    &#13;
 186-ter cod. proc. civ., la quale dunque non può essere  chiamata  a    &#13;
 fungere  da tertium comparationis nella prospettazione di un'asserita    &#13;
 illegittimità  costituzionale  della   non   simmetrica   disciplina    &#13;
 applicativa  di  quel  provvedimento  (v. sentenze n. 65 e n. 200 del    &#13;
 1996);                                                                   &#13;
     che, più in generale, va altresì ribadito come  resti  comunque    &#13;
 affidata  alla  discrezionalità  del legislatore la differenziazione    &#13;
 delle condizioni di accesso alla tutela giurisdizionale,  tanto  più    &#13;
 nella  specie,  dove la domandata pronuncia caducatoria implicherebbe    &#13;
 imprescindibilmente  un  articolato  coordinamento  dello   specifico    &#13;
 contesto  processuale,  sia  per  quanto  riguarda  in  particolare i    &#13;
 termini di notifica  del  decreto  ingiuntivo  e  gli  effetti  della    &#13;
 mancata notifica dello stesso, sia con riferimento alla instaurazione    &#13;
 del giudizio di opposizione;                                             &#13;
     che  dunque all'attenzione proprio e solo del legislatore sarebbe    &#13;
 semmai da segnalare la necessità di rimuovere  -  così  come  hanno    &#13;
 fatto  altri Stati della Comunità europea - il limite previsto dalla    &#13;
 denunciata norma,  ove  si  supponesse  ormai  superata  l'originaria    &#13;
 ragione giustificativa di esso;                                          &#13;
     che, pertanto, la questione appare manifestamente infondata.         &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 633, ultimo comma, del codice  di  procedura    &#13;
 civile,  sollevata,  in  riferimento  agli  artt.  3,  24  e 41 della    &#13;
 Costituzione, dal pretore di Ancona, sezione distaccata di  Fabriano,    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 marzo 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                        Il cancelliere: Malvica                           &#13;
   Depositata in cancelleria il 26 marzo 1998.                            &#13;
                        Il cancelliere: Malvica</dispositivo>
  </pronuncia_testo>
</pronuncia>
