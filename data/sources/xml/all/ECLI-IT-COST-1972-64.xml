<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>64</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:64</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Francesco Paolo Bonifacio</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/04/1972</data_decisione>
    <data_deposito>19/04/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.     357,  &#13;
 capoverso,  in  relazione  all'art.  304  bis,  del codice di procedura  &#13;
 penale, promosso con ordinanza emessa  l'11  maggio  1970  dal  giudice  &#13;
 istruttore  del tribunale di Torino nel procedimento penale a carico di  &#13;
 Nicolotto Giuseppe ed altri, iscritta al n. 315 del registro  ordinanze  &#13;
 1970  e  pubblicata  nella  Gazzetta  Ufficiale della Repubblica n. 286  &#13;
 dell'11 novembre 1970.                                                   &#13;
     Udito nella camera di consiglio del 10  febbraio  1972  il  Giudice  &#13;
 relatore Francesco Paolo Bonifacio.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Nel  corso  di  un  procedimento penale a carico di Giuseppe  &#13;
 Nicolotto, pendente innanzi al  giudice  istruttore  del  tribunale  di  &#13;
 Torino, la difesa dell'imputato, dolendosi di non aver potuto assistere  &#13;
 alla  deposizione  di  una testimone assunta a futura memoria (art. 357  &#13;
 cpv. cod.  proc.  pen.),  ed  al  successivo  confronto  fra  questa  e  &#13;
 l'imputato, eccepì l'illegittimità costituzionale degli artt. 304 bis  &#13;
 e 303 del codice di procedura penale in relazione agli artt.  357 e 364  &#13;
 dello  stesso  codice  ed  in riferimento all'art.   24, secondo comma,  &#13;
 della Costituzione.                                                      &#13;
     2. - Con ordinanza  dell'11  maggio  1970  il  giudice  istruttore,  &#13;
 pronunciandosi sulle suddette eccezioni, ha in primo luogo ritenuto che  &#13;
 sia  manifestamente  infondata  quella  concernente  l'art. 303 c.p.p.,  &#13;
 osservando in proposito che la Costituzione non presuppone  affatto  un  &#13;
 principio  di  parità  fra  pubblico ministero e difesa dell'imputato.  &#13;
 Ciò premesso, il giudice a quo-rilevato che in relazione  all'art.  24  &#13;
 Cost.  occorre di volta in volta verificare, con riferimento ai singoli  &#13;
 atti   processuali,   se  la  mancata  previsione  dell'assistenza  del  &#13;
 difensore sia in contrasto con la norma costituzionale - ritiene che il  &#13;
 diritto  di  difesa  sia violato dall'art. 304 bis c.p.p., in relazione  &#13;
 agli artt.  357 e 364, giacché l'assunzione  dei  testimoni  a  futura  &#13;
 memoria   avviene   in   previsione  dell'eventuale  impossibilità  di  &#13;
 escuterli nuovamente nel futuro corso del  procedimento,  verificandosi  &#13;
 la  quale  il  contraddittorio  non potrà attuarsi nella sua pienezza.  &#13;
 Dopo aver precisato che la censura di  incostituzionalità  si  rivolge  &#13;
 non  già  all'imposizione  del  giuramento, sibbene all'esclusione del  &#13;
 diritto  del  difensore  di  assistere  a  siffatte  testimonianze   ed  &#13;
 all'eventuale  confronto,  l'ordinanza osserva che per rendere operante  &#13;
 il rispetto dell'art. 24 Cost.  "occorrerebbe non tanto eliminare parte  &#13;
 di una norma quanto piuttosto integrare una  disposizione  incompleta":  &#13;
 ricorda,  peraltro,  che  le difficoltà tecniche da ciò nascenti sono  &#13;
 state già superate in altre occasioni.                                  &#13;
     Sulla base  di  tali  premesse  il  giudice  a  quo  propone,  come  &#13;
 rilevante  e non manifestamente infondata, la questione di legittimità  &#13;
 costituzionale concernente "il contrasto tra  la  norma  dell'art.  357  &#13;
 cpv.  c.p.p.  in relazione all'art. 304 bis e l'art. 24, secondo comma,  &#13;
 della Costituzione".                                                     &#13;
     3. - Innanzi a questa Corte non c'è stata costituzione di parti  e  &#13;
 non  è  intervenuto il Presidente del Consiglio dei ministri. La causa  &#13;
 viene pertanto decisa in camera di consiglio  ai  sensi  dell'art.  26,  &#13;
 secondo comma, della legge 11 marzo 1953, n. 87.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  L'oggetto  del presente giudizio - quale risulta dalla parte  &#13;
 finale  dell'ordinanza   di   rimessione,   interpretata   nel   quadro  &#13;
 dell'intera  motivazione della stessa - è costituito dall'art. 304 bis  &#13;
 del codice di procedura penale, nella parte in cui questa disposizione,  &#13;
 in contrasto col principio sancito nell'art.  24, secondo comma,  della  &#13;
 Costituzione,   esclude  il  diritto  del  difensore  dell'imputato  di  &#13;
 assistere, nella fase istruttoria, all'esame  dei  testimoni  a  futura  &#13;
 memoria  (art.  357 cpv.) ed al confronto fra questi e l'imputato (art.  &#13;
 364).                                                                    &#13;
     2. - Con sentenza (n. 63 del 1972), depositata  in  data  di  oggi,  &#13;
 questa  Corte ha accertato che non viola il diritto di difesa garantito  &#13;
 dalla Costituzione la norma, desumibile dall'art. 304  bis  cod.  proc.  &#13;
 pen.,  secondo  la  quale  al  difensore non è consentito di assistere  &#13;
 all'esame dei testi escussi nel corso dell'istruttoria penale.  A  tale  &#13;
 conclusione  la  Corte  è  pervenuta  in  base alla considerazione che  &#13;
 siffatta disciplina,  mentre  da  un  canto  trova  giustificazione  in  &#13;
 esigenze  inerenti  al  sistema  dell'istruttoria delineato dal vigente  &#13;
 ordinamento, non sacrifica  definitivamente  la  difesa  dell'imputato,  &#13;
 giacché   la   prova  testimoniale,  di  regola,  sarà  ripetuta  nel  &#13;
 successivo  dibattimento,  salvo  che  pubblico   ministero   e   parti  &#13;
 consentano  la semplice lettura del verbale istruttorio: di modo che il  &#13;
 contraddittorio si dispiegherà,  in  quella  sede,  in  tutta  la  sua  &#13;
 pienezza,  il difensore sarà in grado di richiedere che al teste siano  &#13;
 rivolte tutte le domande  pertinenti  all'oggetto  della  testimonianza  &#13;
 (art. 467) ed il giudice potrà decidere tenendo presenti le risultanze  &#13;
 acquisite  al  processo  col  dialettico intervento dell'accusa e della  &#13;
 difesa.                                                                  &#13;
     Queste  ragioni,  che  giustificano  l'esclusione   del   difensore  &#13;
 dall'assistenza  alla  normale prova testimoniale, non sussistono, come  &#13;
 è ovvio, nel caso della testimonianza a futura memoria.                 &#13;
     Questa,  infatti, viene assunta con le speciali modalità stabilite  &#13;
 nel secondo comma dell'art. 357  -  fra  le  quali  è  di  particolare  &#13;
 rilievo   l'imposizione   del   giuramento   -  proprio  in  previsione  &#13;
 dell'impossibilità che il teste, a causa  di  infermità  o  di  altro  &#13;
 grave  impedimento,  sia riesaminato in giudizio: al che puntualmente e  &#13;
 razionalmente  corrisponde  l'inclusione  della  deposizione  a  futura  &#13;
 memoria  fra gli atti dei quali è consentita la lettura del verbale in  &#13;
 dibattimento (art. 462, secondo comma).                                  &#13;
     La legge, dunque, esclude che il difensore possa  assistere  ad  un  &#13;
 atto  istruttorio  che  essa stessa prevede come irripetibile e, in tal  &#13;
 modo statuendo, consente che sia definitivamente acquisita al  processo  &#13;
 una  prova  sulla quale la difesa non ha potuto, né potrà in seguito,  &#13;
 interloquire con quei mezzi (domande, contestazioni, ecc.) che, invece,  &#13;
 essa è in grado di dispiegare a proposito della normale testimonianza,  &#13;
 quando questa vien ripetuta in dibattimento.                             &#13;
     È evidente  che  il  contrasto  fra  la  norma  denunziata  ed  il  &#13;
 principio costituzionale di raffronto non è attenuato né dall'obbligo  &#13;
 del  giuramento, imposto al teste per renderlo più sensibile al dovere  &#13;
 di obiettività e di verità, né dalla possibilità  di  registrazione  &#13;
 meccanica  della  esposizione,  introdotta  dall'art.  2  della legge 6  &#13;
 dicembre 1965, n. 1369: queste  modalità  di  assunzione  della  prova  &#13;
 dimostrano,  certo, che il legislatore ha avvertito la sua peculiarità  &#13;
 rispetto alla normale testimonianza, ma non valgono  a  legittimare  il  &#13;
 divieto  dell'assistenza  del  difensore. Giova, al contrario, rilevare  &#13;
 che lo stesso ordinamento processuale offre elementi per  una  positiva  &#13;
 valutazione dell'attuale questione di legittimità costituzionale.       &#13;
     Ed  invero  dall'art. 304 bis, nelle sue disposizioni originarie ed  &#13;
 in quelle ora risultanti dalla ricordata sentenza n. 63  del  1972,  si  &#13;
 deduce che al difensore dell'imputato è consentito di assistere (oltre  &#13;
 che  all'interrogatorio,  per  il  quale  valgono  le  particolarissime  &#13;
 ragioni esposte nella sentenza n. 190 del 1970) a quegli atti che sarà  &#13;
 impossibile  ripetere  in  dibattimento,  quanto  meno   nelle   stesse  &#13;
 condizioni  di  tempo  e  di luogo in cui essi vennero compiuti durante  &#13;
 l'istruttoria: la dichiarazione di illegittimità costituzionale  della  &#13;
 norma ora in esame elimina una ingiustificabile disarmonia del sistema.  &#13;
     3.  -  Le  ragioni  fin  qui  esposte valgono anche a dimostrare la  &#13;
 fondatezza della questione nella parte  relativa  alla  esclusione  del  &#13;
 diritto  del  difensore  di assistere al confronto fra imputato e teste  &#13;
 assunto a futura memoria. Vero è che nella sentenza  n.  63  è  stata  &#13;
 ritenuta  non  illegittima  tale  esclusione  nel confronto fra testi e  &#13;
 imputato o fra più imputati: ma è chiaro che nel caso di cui  ora  ci  &#13;
 si  occupa  determinante  per  l'opposta  conclusione  è la previsione  &#13;
 dell'irripetibilità del confronto in sede dibattimentale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità  costituzionale  dell'art.  304  bis  del  &#13;
 codice  di procedura penale, limitatamente alla parte in cui esclude il  &#13;
 diritto del difensore dell'imputato di assistere alla  testimonianza  a  &#13;
 futura  memoria  (art.  357  cpv.)  ed  al  con  fronto  fra imputato e  &#13;
 testimone esaminato a futura memoria (art. 364).                         &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 13 aprile 1972.         &#13;
                                   GIUSEPPE  CHIARELLI - MICHELE FRAGALI  &#13;
                                   -  COSTANTINO  MORTATI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
