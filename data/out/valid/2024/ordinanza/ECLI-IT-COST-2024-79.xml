<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-05-07/79/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-05-07/79"/>
          <FRBRalias value="ECLI:IT:COST:2024:79" name="ECLI"/>
          <FRBRdate date="2024-05-07" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="79"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-05-07/79/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-05-07/79/ita@"/>
          <FRBRdate date="2024-05-07" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-05-07/79/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-05-07/79/ita@.xml"/>
          <FRBRdate date="2024-05-07" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2024-04-16" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2024-05-07" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>cessata materia del contendere</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="BARBERA" href="/akn/ontology/person/it/BARBERA" showAs="BARBERA"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2024</akn4cc:anno>
        <akn4cc:numero>79</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2024:79</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>BARBERA</akn4cc:presidente>
        <akn4cc:relatore>Giulio Prosperetti</akn4cc:relatore>
        <akn4cc:data_decisione>2024-04-16</akn4cc:data_decisione>
        <akn4cc:data_deposito>2024-05-07</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>cessata materia del contendere</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta da:
 Presidente: <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>; Giudici : <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI, Giovanni PITRUZZELLA, Antonella SCIARRONE ALIBRANDI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 9 della legge della Regione Siciliana 22 febbraio 2023, n. 2 (Legge di stabilità regionale 2023-2025), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 29 aprile 2023, depositato in cancelleria il 2 maggio 2023, iscritto al n. 17 del registro ricorsi 2023 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 21, prima serie speciale, dell'anno 2023.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Siciliana;
 udito nell'udienza pubblica del 16 aprile 2024 il Giudice relatore Giulio Prosperetti;
 uditi l'avvocato dello Stato Giammario Rocchitta per il Presidente del Consiglio dei ministri e l'avvocato Nicola Dumas per la Regione Siciliana;
 deliberato nella camera di consiglio del 16 aprile 2024.
 Ritenuto che, con il ricorso indicato in epigrafe (reg. ric. n. 17 del 2023), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato, tra gli altri, l'art. 9 della legge della Regione Siciliana 22 febbraio 2023, n. 2 (Legge di stabilità regionale 2023-2025), per violazione degli artt. 97, quarto comma, e 117, secondo comma, lettere g) ed l), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, nonché per violazione delle competenze attribuite alla Regione Siciliana dal regio <ref href="/akn/it/act/decretoLegislativo/stato/1946-05-15/455/!main">decreto legislativo 15 maggio 1946, n. 455</ref> (Approvazione dello statuto della Regione siciliana), convertito in legge costituzionale 26 febbraio 1948, n. 2.
 che la disposizione impugnata stabilisce: «[a]ll'art. 64 della legge regionale 12 agosto 2014, n. 21 e successive modificazioni, dopo il comma 4 è aggiunto il seguente: "4bis. Le disposizioni di cui al comma 4 trovano applicazione anche nei confronti degli enti pubblici regionali e delle aziende sanitarie ed ospedaliere siciliane"»;
 che, secondo il ricorrente, la riferita disposizione regionale, estendendo alle aziende sanitarie ed ospedaliere siciliane la disposizione dell'art. 64, comma 4, della legge della Regione Siciliana 12 agosto 2014, n. 21 (Assestamento del bilancio della Regione per l'anno finanziario 2014. Variazioni al bilancio di previsione della Regione per l'esercizio finanziario 2014 e modifiche alla legge regionale 28 gennaio 2014, n. 5 "Disposizioni programmatiche e correttive per l'anno 2014. Legge di stabilità regionale. Disposizioni varie"), violerebbe l'art. 97, quarto comma, Cost., in relazione all'<ref href="/akn/it/act/decretoLegislativo/stato/2001-03-30/165/!main#art_35">art. 35 del decreto legislativo 30 marzo 2001, n. 165</ref> (Norme generali sull'ordinamento del lavoro alle dipendenze delle amministrazioni pubbliche) e alle disposizioni del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1997-12-10/483/!main">decreto del Presidente della Repubblica 10 dicembre 1997, n. 483</ref> (Regolamento recante la disciplina concorsuale per il personale dirigenziale del Servizio sanitario nazionale), e del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-03-27/220/!main">decreto del Presidente della Repubblica 27 marzo 2001, n. 220</ref> (Regolamento recante disciplina concorsuale del personale non dirigenziale del Servizio sanitario nazionale), eludendo la regola del concorso pubblico quale criterio generale  non derogabile se non nei casi espressamente indicati dalla legge per peculiari e straordinarie esigenze di interesse pubblico  di accesso ai pubblici impieghi;
 che sarebbe altresì violato l'art. 117, secondo comma, lettere g) ed l), Cost., in relazione agli <ref href="/akn/it/act/decretoLegislativo/stato/2016-08-19/175/!main#art_19">artt. 19 del decreto legislativo 19 agosto 2016, n. 175</ref> (Testo unico in materia di società a partecipazione pubblica) e 11, comma 1, del <ref href="/akn/it/act/decretoLegge/stato/2019-04-30/35/!main">decreto-legge 30 aprile 2019, n. 35</ref> (Misure emergenziali per il servizio sanitario della Regione Calabria e altre misure urgenti in materia sanitaria), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2019-06-25/60/!main">legge 25 giugno 2019, n. 60</ref>, in quanto la disposizione impugnata, intervenendo sulla disciplina del rapporto di lavoro del personale sanitario, disciplinerebbe le materie «ordinamento e organizzazione amministrativa dello Stato e degli enti pubblici nazionali» e «ordinamento civile», riservate come tali alla potestà legislativa esclusiva dello Stato, eccedendo dalle competenze legislative della Regione Siciliana, come delineate dall'art. 14 statuto reg. Siciliana;
 che, con atto depositato il 6 giugno 2023, si è costituita in giudizio la Regione Siciliana che, con riferimento all'impugnativa avente ad oggetto l'art. 9 della legge reg. Siciliana n. 2 del 2023, si è limitata ad evidenziare l'avvenuta presentazione di una proposta di legge regionale abrogativa della disposizione impugnata;
 che, in data 26 marzo 2024, in prossimità dell'udienza, la resistente ha depositato una memoria nella quale ha rilevato che, successivamente alla proposizione del ricorso con il quale è stata promossa la presente questione di legittimità costituzionale, è stata approvata la legge della Regione Siciliana 21 novembre 2023, n. 25 (Disposizioni finanziarie varie. Modifiche di norme), il cui art. 28, comma 4, ha abrogato, a decorrere dal 24 novembre 2023, ai sensi del successivo art. 30, comma 1, della medesima legge reg. Siciliana n. 25 del 2023, l'art. 9 della legge reg. Siciliana n. 2 del 2023;
 che la difesa regionale, con la detta memoria integrativa, ha prodotto in giudizio la nota 15 febbraio 2024, n. 10784, con cui la Ragioneria generale della Regione Siciliana attestava che «le norme impugnate della legge regionale 22 febbraio 2023, n. 2 di cui all'allegato elenco, ad eccezione degli articoli 36, 38 e 48 [...], non hanno trovato applicazione medio tempore sotto il profilo contabile dalla data della rispettiva emanazione alla data della relativa abrogazione apportata dall'articolo 28, comma 4, della legge regionale 21 novembre 2023, n. 25»;
 che la Regione Siciliana, sulla base di tali elementi, ha domandato la dichiarazione di cessazione della materia del contendere;
 che, nel corso dell'udienza pubblica del 16 aprile 2024, l'Avvocatura generale dello Stato ha preso atto della richiesta della resistente, non contestando la mancata applicazione della disposizione impugnata.
 Considerato che, nelle more del presente giudizio, l'art. 28, comma 4, della legge reg. Siciliana n. 25 del 2023 ha abrogato, a decorrere dal 24 novembre 2023, ai sensi del successivo art. 30, comma 1, della medesima legge reg. Siciliana n. 25 del 2023, la disposizione impugnata;
 che il 26 marzo 2024 la resistente ha depositato memoria integrativa con cui ha chiesto che sia dichiarata cessata la materia del contendere, producendo in giudizio la nota della Ragioneria generale della Regione Siciliana 15 febbraio 2024, n. 10784, attestante che la disposizione impugnata non ha trovato attuazione nel periodo in cui è stata vigente;
 che, secondo la costante giurisprudenza di questa Corte, l'abrogazione della disposizione impugnata, ove non abbia trovato medio tempore applicazione, determina la cessazione della materia del contendere (ex plurim<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/200/!main">is, sentenze n. 200</ref>, n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/117/!main">117</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/78/!main">78 del 2020</ref></mref>; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2020/101/!main">ordinanza n. 101 del 2020</ref>);
 che la Regione Siciliana ha dichiarato che la disposizione impugnata non ha trovato applicazione, producendo in giudizio la nota conforme della Ragioneria generale della Regione Siciliana 15 febbraio 2024, prot. n. 10784, senza che l'Avvocatura generale dello Stato abbia mosso obiezioni in proposito;
 che sussistono, pertanto, i presupposti per dichiarare cessata la materia del contendere limitatamente alle questioni oggetto del presente giudizio.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 riservata a separate pronunce la decisione delle ulteriori questioni di legittimità costituzionale promosse con il ricorso indicato in epigrafe;
 dichiara cessata la materia del contendere in ordine alle questioni di legittimità costituzionale dell'art. 9 della legge della Regione Siciliana 22 febbraio 2023, n. 2 (Legge di stabilità regionale 2023-2025), promosse, in riferimento agli artt. 97, quarto comma, e 117, secondo comma, lettere g) ed l), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> e alle competenze attribuite alla Regione Siciliana dal regio <ref href="/akn/it/act/decretoLegislativo/stato/1946-05-15/455/!main">decreto legislativo 15 maggio 1946, n. 455</ref> (Approvazione dello statuto della Regione siciliana), convertito in legge costituzionale 26 febbraio 1948, n. 2, dal Presidente del Consiglio dei ministri con il ricorso indicato in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2024-04-16">16 aprile 2024</date>.
 F.to:
 <signature><judge refersTo="#augustoAntonioBarbera" as="#presidente">Augusto Antonio BARBERA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulioProsperetti" as="#relatore">Giulio PROSPERETTI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2024-05-07">7 maggio 2024</date>
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
