<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>357</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:357</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>25/10/2006</data_decisione>
    <data_deposito>07/11/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO PER CONFLITTO DI ATTRIBUZIONE TRA ENTI</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione sorto a seguito degli artt. 10, 11 e 12 del decreto del Presidente della Giunta della Regione Toscana del 1° dicembre 2004, n. 69/R (Regolamento di attuazione, di cui all'articolo 15, comma 3, della legge regionale 29 dicembre 2003, n. 67, concernente “Organizzazione delle attività del sistema regionale della protezione civile in emergenza”), promosso con ricorso del Presidente del Consiglio dei ministri, notificato l'8 febbraio 2005, depositato in cancelleria il 15 febbraio 2005 ed iscritto al n. 11 del registro conflitti 2005. &#13;
    Visto l'atto di costituzione della Regione Toscana; &#13;
    udito nell'udienza pubblica del 26 settembre 2006 il Giudice relatore Maria Rita Saulle; &#13;
    udito l'avvocato dello Stato Franco Favara per il Presidente del Consiglio dei ministri, nonché l'avvocato Mario Loria per la Regione Toscana. &#13;
    Ritenuto che, con ricorso notificato l'8 febbraio 2005, depositato il successivo 15 febbraio, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha sollevato conflitto di attribuzione nei confronti della Regione Toscana, in relazione agli artt. 10, 11 e 12 del decreto del Presidente della Giunta regionale della Toscana 1° dicembre 2004, n. 69/R (Regolamento di attuazione, di cui all'articolo 15, comma 3, della legge regionale 29 dicembre 2003, n. 67, concernente “Organizzazione delle attività del sistema regionale della protezione civile in emergenza”); &#13;
    che, secondo il ricorrente, le norme impugnate violerebbero gli artt. 117, secondo comma, lettere g) e m), terzo comma, e 118 della Costituzione, in quanto, nell'istituire in ambito comunale e provinciale strutture denominate unità di crisi, prevedono che di esse possano fare parte, previa intesa, soggetti estranei all'ente territoriale e, in particolare, rappresentanti dei vigili del fuoco, delle forze di polizia e della prefettura; &#13;
    che la difesa erariale, premesso che la materia della protezione civile rientra tra quelle indicate dall'art. 117, terzo comma, della Costituzione, rileva che l'art. 107 del decreto legislativo 31 marzo 1998, n. 112 (Conferimento di funzioni e compiti amministrativi dello Stato alle regioni ed agli enti locali, in attuazione del capo I della legge 15 marzo 1997, n. 59), ha mantenuto allo Stato il «soccorso tecnico urgente» e le relative attribuzioni inerenti all'impiego delle strutture operative; &#13;
    che tale ultima disposizione, sempre a parere del ricorrente, trova la sua ratio nell'esigenza di garantire determinati standard minimi di salvaguardia della vita umana, la cui determinazione l'art. 117, secondo comma, lettera m), della Costituzione riserva alla competenza esclusiva dello Stato; &#13;
    che, comunque, l'art. 118, terzo comma, della Costituzione, nei casi di necessario coordinamento tra Stato e regioni in materia di «ordine pubblico e sicurezza», attribuisce allo Stato l'esercizio delle funzioni amministrative sulla base dei principi di sussidiarietà e adeguatezza; &#13;
    che, infine, a parere della difesa erariale, le norme censurate sarebbero in contrasto con l'art. 117, secondo comma, lettera g), della Costituzione, in quanto la Regione, nel disciplinare forme di collaborazione e di coordinamento in materia di protezione civile, avrebbe attribuito, unilateralmente, nuovi compiti ad organi dello Stato nell'ambito di strutture di enti regionali o locali;  &#13;
    che si è costituita in giudizio la Regione Toscana chiedendo che il ricorso sia dichiarato inammissibile e, comunque, infondato; &#13;
    che, a parere della Regione, le disposizioni contestate non esorbiterebbero dalle proprie competenze normative, limitandosi a dare attuazione all'art. 15 della legge della Regione Toscana n. 67 del 2003 (Ordinamento del sistema regionale della protezione civile e disciplina della relativa attività), e a prevedere forme di intese e di accordo con i rappresentanti degli organi statali; &#13;
    che, in prossimità dell'udienza, la Regione Toscana ha depositato memoria con la quale ha documentato l'adozione, successivamente alla proposizione del conflitto, del decreto del Presidente della Giunta regionale 12 settembre 2006, n. 44/R (Modifiche al Regolamento regionale emanato con decreto del Presidente della Giunta regionale 1° dicembre 2004, n. 69/R), che ha espressamente modificato i tre articoli oggetto del giudizio, tenendo conto di tutti i motivi del ricorso; &#13;
    che, nel corso dell'udienza pubblica, la difesa erariale ha dichiarato di rinunciare al ricorso e la Regione Emilia-Romagna, a sua volta, ha accettato la anzidetta rinuncia. &#13;
    Considerato che, ai sensi dell'art. 27, comma 8, delle norme integrative per i giudizi dinanzi a questa Corte, la rinuncia al ricorso, seguita dall'accettazione della parte resistente, comporta l'estinzione del processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara estinto il processo.  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 25 ottobre 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 7 novembre 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
