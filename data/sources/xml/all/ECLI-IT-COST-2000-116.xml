<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>116</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:116</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/04/2000</data_decisione>
    <data_deposito>21/04/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale del combinato disposto    &#13;
 degli artt. 663-bis del codice penale, 5 della legge 8 febbraio 1948,    &#13;
 n. 47  (Disposizioni  sulla stampa) e 45 della legge 3 febbraio 1963,    &#13;
 n. 69  (Ordinamento  della  professione di giornalista), promosso con    &#13;
 ordinanza  emessa  il  24  marzo  1999  dal  pretore  di  Livorno nel    &#13;
 procedimento  penale  a  carico di Impallazzo Alessandra, iscritta al    &#13;
 n. 314  del  registro  ordinanze  1999  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 23,  prima serie speciale, dell'anno    &#13;
 1999.                                                                    &#13;
     Visto  l'atto  di  costituzione di Impallazzo Alessandra, nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei Ministri;          &#13;
     Udito  nella  camera  di  consiglio  del 22 marzo 2000 il giudice    &#13;
 relatore Fernando Santosuosso.                                           &#13;
     Ritenuto  che nel corso di un procedimento penale a carico di una    &#13;
 imputata   del   reato  di  divulgazione  di  stampa  clandestina  ex    &#13;
 art. 663-bis  cod.  pen., il pretore di Livorno, con ordinanza del 24    &#13;
 marzo 1999, ha sollevato questione di legittimità costituzionale del    &#13;
 combinato  disposto  degli  artt. 663-bis  cod. pen., 5 della legge 8    &#13;
 febbraio 1948,  n. 47  (Disposizioni sulla stampa) e 45 della legge 3    &#13;
 febbraio  1963,  n. 69 (Ordinamento della professione di giornalista)    &#13;
 in riferimento agli artt. 21, 3, 4 e 18 della Costituzione;              &#13;
         che,  a giudizio del rimettente, in ordine alla rilevanza, il    &#13;
 reato  contestato sarebbe ipotizzabile ove manchi uno dei presupposti    &#13;
 richiesti   dalla   legge   sulla  stampa  e  in  particolare  quello    &#13;
 dell'iscrizione    del    direttore   responsabile   all'ordine   dei    &#13;
 giornalisti;                                                             &#13;
         che,  nel  merito, le norme denunciate violerebbero l'art. 21    &#13;
 della  Costituzione, che garantisce la libertà di manifestazione del    &#13;
 pensiero in genere e la libertà di stampa in particolare;               &#13;
         che  sarebbe  altresì  violato  il  principio di uguaglianza    &#13;
 (art. 3  Cost.),  in quanto la profonda evoluzione degli strumenti di    &#13;
 informazione  avrebbe  determinato  l'obsolescenza  della legge sulla    &#13;
 stampa  che detta disposizioni sulla pubblicazione e sulla diffusione    &#13;
 dei periodici;                                                           &#13;
         che  l'obbligo  di  iscrizione  all'albo violerebbe l'art. 18    &#13;
 della  Costituzione  atteso  che  la  libertà  di  associazione, per    &#13;
 giurisprudenza  di  questa  Corte,  si  riferisce  sia  al diritto di    &#13;
 associarsi liberamente, sia a quello di non associarsi; le previsioni    &#13;
 della   legge  sulla  stampa,  quindi,  negherebbero  il  diritto  di    &#13;
 associazione, perché imporrebbero ai giornalisti professionisti e al    &#13;
 direttore   responsabile   l'obbligo   di  associarsi  ove  intendano    &#13;
 esercitare il diritto di informazione;                                   &#13;
         che,  infine,  le  disposizioni  di  cui  sopra  violerebbero    &#13;
 l'art. 4 della Costituzione, che nel riconoscere il diritto al lavoro    &#13;
 prescriverebbe  che,  per  il  relativo esercizio, non possono essere    &#13;
 imposti  dalla  legge  ordinaria  limiti  se  non  in relazione ad un    &#13;
 interesse  costituzionalmente  protetto  quale  potrebbe  essere, nel    &#13;
 sistema   disegnato   dalla   legge  sulla  stampa,  la  libertà  di    &#13;
 manifestazione   del   pensiero,   sotto   la   specie   del  diritto    &#13;
 all'informazione ex art. 21 Cost.;                                       &#13;
         che  si è costituita la parte privata, nel presente giudizio    &#13;
 di  legittimità  costituzionale, insistendo per l'accoglimento della    &#13;
 questione;                                                               &#13;
         che  nel  presente giudizio di legittimità costituzionale è    &#13;
 intervenuto il Presidente del Consiglio dei Ministri, rappresentato e    &#13;
 difeso  dall'Avvocatura  generale  dello  Stato,  che ha concluso per    &#13;
 l'infondatezza della questione.                                          &#13;
     Considerato  che, successivamente all'ordinanza di rimessione, è    &#13;
 intervenuto   il   decreto   legislativo  30  dicembre  1999,  n. 507    &#13;
 (Depenalizzazione   dei   reati   minori   e   riforma   del  sistema    &#13;
 sanzionatorio,  ai  sensi  dell'art. 1  della  legge  25 giugno 1999,    &#13;
 n. 205)  che  all'art. 47  ha depenalizzato la contravvenzione di cui    &#13;
 all'art.  663-bis  del  codice  penale  in  attuazione  della  delega    &#13;
 legislativa  contenuta  nell'art. 7,  comma  1, lett. c) della citata    &#13;
 legge n. 205 del 1999, anch'essa successiva all'ordinanza del giudice    &#13;
 a quo;                                                                   &#13;
         che  della  sopravvenuta  disposizione richiamata deve essere    &#13;
 valutata  l'incidenza  nel giudizio che ha dato origine alla presente    &#13;
 questione  di  costituzionalità  anche  alla  luce dell'art. 100 del    &#13;
 decreto  legislativo  n. 507 del 1999 che disciplina l'applicabilità    &#13;
 delle  sanzioni amministrative alle violazioni commesse anteriormente    &#13;
 all'entrata in vigore del citato decreto;                                &#13;
         che,  pertanto,  gli atti devono essere restituiti al giudice    &#13;
 rimettente per una nuova valutazione della rilevanza della questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Ordina la restituzione degli atti al pretore di Livorno.             &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 aprile 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 21 aprile 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
