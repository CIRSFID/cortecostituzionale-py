<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>123</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:123</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Alfonso Quaranta</relatore_pronuncia>
    <redattore_pronuncia>Alfonso Quaranta</redattore_pronuncia>
    <data_decisione>20/03/2006</data_decisione>
    <data_deposito>24/03/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 200 e 201 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso con ordinanza del 28 aprile 2005 dal Giudice di pace di Palermo, nel procedimento civile vertente tra Ganci Francesco e il Prefetto di Palermo, iscritta al n. 348 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 29, prima serie speciale, dell'anno 2005. &#13;
    Udito nella camera di consiglio del 22 febbraio 2006 il Giudice relatore Alfonso Quaranta.  &#13;
 &#13;
 &#13;
    Ritenuto che il Giudice di pace di Palermo, con ordinanza emessa il 28 aprile 2005, ha sollevato questione di legittimità costituzionale – in riferimento agli artt. 3 e 24 della Costituzione – degli artt. 200 e 201 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada); &#13;
    che il rimettente – nel premettere di essere chiamato a decidere dell'opposizione, proposta ex art. 205 del codice della strada dal coobbligato in solido per il pagamento della sanzione pecuniaria, avverso ordinanza-ingiunzione prefettizia – assume che «la mancata notifica del verbale di accertamento all'obbligato in solido» renderebbe impossibile, per costui, di «venire a conoscenza, immediatamente, del fatto contestato», e quindi di «poter esercitare il proprio diritto di difesa nelle varie fasi del procedimento di opposizione»; &#13;
    che su tali basi, pertanto, reputa «non manifestamente infondata l'eccezione di incostituzionalità sollevata dal ricorrente», in relazione agli artt. 3 e 24 della Costituzione, avente ad oggetto i predetti artt. 200 e 201 del codice della strada, «nella parte in cui non prevedono, in caso di contestazione immediata al trasgressore, anche la necessaria notifica all'obbligato in solido»; &#13;
    che, difatti, le norme impugnate – in ragione della descritta omissione – non riserverebbero ai «soggetti obbligati in solido alla sanzione amministrativa» né la medesima «parità di trattamento», né le «stesse possibilità di tutelare i propri diritti e gli interessi legittimi», riconosciute invece al «conducente-trasgressore», giacché, in particolare, l'omessa notifica del verbale di accertamento al proprietario del mezzo, coobbligato in solido per il pagamento della sanzione amministrativa pecuniaria, determinerebbe «la mancata possibilità per costui di esercitare il proprio diritto di difesa». &#13;
    Considerato che il Giudice di pace di Palermo ha sollevato questione di legittimità costituzionale – in riferimento agli artt. 3 e 24 della Costituzione – degli artt. 200 e 201 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada); &#13;
    che il rimettente – dopo aver riferito di essere chiamato a decidere dell'opposizione ex art. 205 del codice della strada, proposta dal coobbligato in solido per il pagamento della sanzione pecuniaria, avverso ordinanza-ingiunzione prefettizia – nulla aggiunge in merito alla vicenda processuale sottoposta al suo giudizio;  &#13;
    che, secondo costante giurisprudenza di questa Corte, «l'insufficiente descrizione della fattispecie oggetto del giudizio a quo comporta la manifesta inammissibilità della questione sollevata dal rimettente» (da ultimo, con riferimento a questione concernente una disposizione del codice della strada, v. ordinanza n. 396 del 2005); &#13;
    che la descritta carenza si risolve, difatti, nell'impossibilità per questa Corte di vagliare la rilevanza oltre che la non manifesta infondatezza del prospettato dubbio di costituzionalità (ex multis, ordinanze n. 297, n. 236 e n. 140 del 2005); &#13;
    che, pertanto, non sussistono le condizioni per addivenire ad un esame nel merito della sollevata questione, e ciò a prescindere dal rilievo in ordine al carattere meramente “assertivo” del denunciato vizio di legittimità costituzionale, giacché il rimettente non ha precisato in quale misura l'omessa notificazione del verbale di contestazione dell'infrazione stradale al coobbligato in solido per il pagamento della sanzione pecuniaria (che, peraltro, l'impugnato art. 201 del codice della strada comunque prescrive, quando l'effettivo trasgressore «non sia stato identificato e si tratti di violazione commessa dal conducente di un veicolo a motore, munito di targa») determini «la mancata possibilità per costui di esercitare il proprio diritto di difesa»; &#13;
    che, pertanto, la questione deve essere dichiarata manifestamente inammissibile. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli artt. 200 e 201 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata – in riferimento agli artt. 3 e 24 della Costituzione – dal Giudice di pace di Palermo, con l'ordinanza di cui in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  &#13;
il 20 marzo 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 24 marzo 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
