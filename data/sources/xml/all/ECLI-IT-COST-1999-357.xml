<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>357</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:357</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/07/1999</data_decisione>
    <data_deposito>22/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale  dell'art.  4,  comma  2,    &#13;
 della  legge  8  novembre 1991, n. 362 (Norme di riordino del settore    &#13;
 farmaceutico), promosso con ordinanza emessa il 20  maggio  1998  dal    &#13;
 T.A.R. per la Basilicata sul ricorso proposto da Coiro Antonio contro    &#13;
 Regione  Basilicata, iscritta al n. 710 del registro ordinanze 1998 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  40,  prima    &#13;
 serie speciale, dell'anno 1998.                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di consiglio  del  7  luglio  1999  il  giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
   Ritenuto  che,  nel  corso  di  un  giudizio amministrativo diretto    &#13;
 all'annullamento di un bando di concorso per titoli ed esami  per  il    &#13;
 conferimento di sedi farmaceutiche vacanti o di nuova istituzione, il    &#13;
 Tribunale  amministrativo  regionale  della Basilicata, con ordinanza    &#13;
 del  20  maggio  1998,  ha  sollevato   questione   di   legittimità    &#13;
 costituzionale  dell'art. 4, comma 2, della legge 8 novembre 1991, n.    &#13;
 362 (Norme di riordino del settore farmaceutico) in riferimento  agli    &#13;
 artt.  3,  4  e  35  della  Costituzione,  in  quanto la disposizione    &#13;
 denunciata prevede un limite massimo di età (cinquantanove anni) per    &#13;
 essere ammesso al concorso pubblico  per  titoli  ed  esami,  per  il    &#13;
 conferimento di sedi farmaceutiche vacanti o di nuova istituzione;       &#13;
     che  detta  limitazione  secondo  il  rimettente  - costituirebbe    &#13;
 un'innovazione  nel  sistema   normativo   riguardante   il   settore    &#13;
 farmaceutico,  non  prevista  dall'art.  12,  secondo e ottavo comma,    &#13;
 della legge 2 aprile 1968, n.  475  (Norme  concernenti  il  servizio    &#13;
 farmaceutico)  che  disciplina  il trasferimento della titolarità di    &#13;
 farmacie  per  atto  inter  vivos  a  titolo  oneroso  a  favore  del    &#13;
 farmacista  che  abbia  già  conseguito  la  titolarità  o  che sia    &#13;
 risultato  idoneo  in  un  precedente   concorso.      Ciò   sarebbe    &#13;
 irragionevole   e   determinerebbe  un'ingiustificata  disparità  di    &#13;
 trattamento tra chi può acquistare a qualunque età una  farmacia  e    &#13;
 chi,  invece,  non  può  partecipare  al concorso per acquisirne una    &#13;
 perché ultracinquantanovenne;                                           &#13;
     che la disposizione  denunciata  non  troverebbe  giustificazione    &#13;
 neanche  dal  raffronto  con quella per l'accesso al pubblico impiego    &#13;
 atteso che, anche per quest'ultima, il  limite  di  età  fissato  in    &#13;
 quaranta anni dalla legge 27 gennaio 1989, n. 25 (Norme sui limiti di    &#13;
 età  per  la partecipazione ai pubblici concorsi) e poi aumentato di    &#13;
 un anno dall'art. 1, comma 4, della legge 28 dicembre 1995, n.    549    &#13;
 (Misure   di  razionalizzazione  della  finanza  pubblica)  è  stato    &#13;
 definitivamente abolito dall'art. 3, comma 6, della legge  15  maggio    &#13;
 1997,  n.  127  (Misure  urgenti  per  lo  snellimento dell'attività    &#13;
 amministrativa e dei procedimenti di decisione e di controllo);          &#13;
     che la disposizione denunciata si porrebbe altresì in  contrasto    &#13;
 con  gli  artt.  4 e 35 della Costituzione che tutelano il diritto al    &#13;
 lavoro in tutte le sue forme e applicazioni;                             &#13;
     che nel giudizio è intervenuto il Presidente del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  il  quale  ha  chiesto  che  la  questione  sollevata   venga    &#13;
 dichiarata inammissibile o manifestamente infondata, in ragione della    &#13;
 diversità  e  non comparabilità delle situazioni poste a raffronto,    &#13;
 atteso che con la  disposizione  denunciata  il  legislatore  avrebbe    &#13;
 inteso  disciplinare  il  conferimento  della  titolarità delle sedi    &#13;
 farmaceutiche  risultanti  disponibili  attraverso  la   regola   del    &#13;
 concorso  pubblico  per  individuare  gli  aspiranti più meritevoli,    &#13;
 appartenendo alla discrezionalità del legislatore la fissazione  dei    &#13;
 requisiti necessari per la partecipazione alla procedura concorsuale;    &#13;
     che  a  diverse  finalità  sarebbe  improntata  la  disposizione    &#13;
 dell'art.  12 della legge n. 475  del  1968,  come  modificato  dalla    &#13;
 legge  n.  892  del  1984,  in  quanto  tale  normativa  concerne  la    &#13;
 differente ipotesi del  trasferimento  di  aziende  tra  privati:  un    &#13;
 settore  di  natura  commerciale e sorretto dall'autonomia privata, a    &#13;
 fronte del quale l'intervento del legislatore fissa altre  condizioni    &#13;
 a tutela degli interessi pubblici relativi al servizio farmaceutico.     &#13;
   Considerato  che, in base alla consolidata giurisprudenza di questa    &#13;
 Corte, il raffronto tra  due  fattispecie  al  fine  di  valutare  la    &#13;
 ragionevolezza  del  loro  trattamento  differenziato  è ammissibile    &#13;
 soltanto se esse  siano  identiche,  o,  quantomeno,  sostanzialmente    &#13;
 omogenee  (v. per tutte, le sentenze n. 431 del 1997, n. 65 del 1996,    &#13;
 n. 237 del 1995, n. 139 del 1984 e l'ordinanza n. 297 del 1998);         &#13;
     che, inoltre,  rientra  nella  discrezionalità  del  legislatore    &#13;
 stabilire  i  requisiti  d'età  per  l'accesso  ai pubblici impieghi    &#13;
 purché i detti requisiti non siano determinati in modo arbitrario  o    &#13;
 irragionevole (v., per tutte, le sentenze n. 466 del 1997, n. 412 del    &#13;
 1988);                                                                   &#13;
     che,  nella specie, il legislatore con la disposizione denunciata    &#13;
 ha stabilito,  con  una  scelta  immune  da  irragionevolezza  e  non    &#13;
 arbitraria,  come  si  evince dai lavori preparatori, di: "evitare in    &#13;
 futuro le gestioni provvisorie nell'esercizio  delle  farmacie  e  le    &#13;
 ripetute  sanatorie  che  hanno  caratterizzato i periodi trascorsi",    &#13;
 dettando regole cogenti sia nel fissare i  requisiti  soggettivi  dei    &#13;
 concorrenti,  sia nel disciplinare gli altri aspetti del procedimento    &#13;
 concorsuale;                                                             &#13;
     che le due fattispecie indicate dal giudice a  quo  pur  relative    &#13;
 entrambe  al  conseguimento  di  un  esercizio farmaceutico, non sono    &#13;
 omogenee, essendo diversa la fonte della  titolarità:  (il  concorso    &#13;
 pubblico ex art. 4 della legge n. 362 del 1991 e il contratto ex art.    &#13;
 12  della  legge  n.  475  del  1968)  e  che,  pertanto,  la seconda    &#13;
 fattispecie, costituendo una deroga alla  generale  prescrizione  del    &#13;
 pubblico   concorso,   può   considerarsi  una  ipotesi  diversa  ed    &#13;
 eccezionale;                                                             &#13;
     che, pertanto, il tertium comparationis indicato  dal  giudice  a    &#13;
 quo,  non  può  essere  raffrontato  in modo pertinente con la norma    &#13;
 impugnata, e la disciplina stabilita da quest'ultima, di per sé, non    &#13;
 risulta irragionevole e non determina  ingiustificate  disparità  di    &#13;
 trattamento;                                                             &#13;
     che,  infine,  risulta  manifestamente infondato il richiamo agli    &#13;
 artt.  4  e  35  della  Costituzione  che  concernono   precipuamente    &#13;
 l'accesso  al  mercato  del  lavoro  (v.  sent. n. 293 del 1997) e la    &#13;
 protezione di tale diritto nelle sue varie forme; invero va  rilevato    &#13;
 che  dal riconoscimento dell'importanza costituzionale del lavoro non    &#13;
 deriva  l'impossibilità  di  prevedere  condizioni  e   limiti   per    &#13;
 l'esercizio del relativo diritto (v. sentenza n. 103 del 1977), anche    &#13;
 attraverso  la  fissazione di un limite massimo di età, quale quello    &#13;
 previsto dalla disposizione censurata, posto a tutela di altri valori    &#13;
 costituzionalmente  garantiti,  purché  sempre  nel  rispetto  della    &#13;
 ragionevolezza dei requisiti soggettivi di partecipazione ai concorsi    &#13;
 pubblici.                                                                &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art. 4, comma 2, della legge 8 novembre 1991, n.    &#13;
 362 (Norme di  riordino  del  settore  farmaceutico),  sollevata,  in    &#13;
 riferimento  agli  artt.  3, 4 e 35 della Costituzione, dal Tribunale    &#13;
 amministrativo regionale della Basilicata, con  l'ordinanza  indicata    &#13;
 in epigrafe.                                                             &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 luglio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 luglio 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
