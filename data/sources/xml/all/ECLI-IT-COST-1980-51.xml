<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1980</anno_pronuncia>
    <numero_pronuncia>51</numero_pronuncia>
    <ecli>ECLI:IT:COST:1980:51</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Edoardo Volterra</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/04/1980</data_decisione>
    <data_deposito>14/04/1980</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - &#13;
 Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA &#13;
 - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  341  del  &#13;
 codice  penale,  promosso  con  ordinanza emessa il 15 gennaio 1975 dal  &#13;
 Pretore di  Prato,  nel  procedimento  penale  a  carico  di  Pecchioli  &#13;
 Marileno,  iscritta  al n. 121 del registro ordinanze 1975 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 145 del 4 giugno 1975.      &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  19  dicembre  1979  il  Giudice  &#13;
 relatore Edoardo Volterra;                                               &#13;
     udito  l'avvocato  dello  Stato  Giuseppe  Angelini  Rota  per   il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Nel  corso  del  procedimento  penale  a  carico di Marileno  &#13;
 Pecchioli, imputato del reato di oltraggio  a  pubblico  ufficiale,  il  &#13;
 Pretore di Prato, con ordinanza emessa il 15 gennaio 1975, ha sollevato  &#13;
 questione  di  legittimità  costituzionale  dell'art. 341 cod. pen. in  &#13;
 riferimento all'art. 3 della Costituzione.                               &#13;
     Il giudice a quo ritiene non manifestamente infondato il  contrasto  &#13;
 tra la norma denunziata ed il principio invocato, sotto due profili.     &#13;
     Quanto  al  primo,  a detta del Pretore, sarebbe viziata da sofisma  &#13;
 idoneo a vanificare il principio d'eguaglianza  l'argomentazione  della  &#13;
 Corte,  contenuta nella sentenza n. 109 del 1968, decisione in cui, nel  &#13;
 respingere analoga questione, si osservava che la  speciale  disciplina  &#13;
 dell'art.  341  cod.    pen.  è  giustificata dal fatto che "la tutela  &#13;
 penale dell'onore della persona fisica titolare del  pubblico  ufficio,  &#13;
 è assorbita in quella del prestigio della pubblica amministrazione che  &#13;
 in  essa s'incarna, prestigio il quale viene colpito nel momento stesso  &#13;
 in cui la sua autorità si fa  concretamente  valere,  e  pertanto  dà  &#13;
 luogo  ad  una  nuova  e  diversa  fattispecie legale". In questo modo,  &#13;
 sempre secondo il giudice a quo, si  consentirebbe  al  legislatore  di  &#13;
 eludere  a  suo arbitrio il principio di uguaglianza e di pari dignità  &#13;
 sociale dei cittadini costruendo "fattispecie nuove  e  diverse"  sulla  &#13;
 base  della  diversa  e  specifica  rilevanza attribuita alle diverse e  &#13;
 specifiche condizioni o qualifiche personali o sociali dei cittadini.    &#13;
     Né,  aggiunge,  il  Pretore,  si  può  ritenere  che  una  tutela  &#13;
 differenziata  dell'onorabilità del pubblico ufficiale e di quella del  &#13;
 comune cittadino sia giustificata dal  fatto  che  il  prestigio  della  &#13;
 pubblica  amministrazione  rappresenta un valore maggiore, per esempio,  &#13;
 del prestigio della proprietà imprenditoriale, o del culto  religioso,  &#13;
 o   dell'insegnamento   o   della  professione  medica  o  forense  che  &#13;
 rispettivamente s'incarnano nell'imprenditore, nel ministro del  culto,  &#13;
 nel  docente,  nel medico, e nell'avvocato. Il prestigio della pubblica  &#13;
 amministrazione  non  sarebbe   fatto   oggetto   di   nessuna   tutela  &#13;
 costituzionale  e  perciò  nulla  autorizzerebbe ad assumerlo a valore  &#13;
 privilegiato fino al punto da consentire  la  deroga  in  suo  nome  al  &#13;
 principio costituzionale di uguaglianza.                                 &#13;
     Quanto  al  secondo  profilo,  l'art.  341  sembra  al  Pretore  in  &#13;
 manifesto contrasto con l'art.  3,  primo  comma,  della  Costituzione,  &#13;
 giacché  esso  comporta,  sul  piano  processuale,  una  disparità di  &#13;
 trattamento tra pubblici ufficiali e comuni cittadini che si risolve in  &#13;
 una discriminazione e in un privilegio odioso  a  danno  delle  persone  &#13;
 titolari   di   pubblici   uffici.     Queste  infatti  a  causa  della  &#13;
 procedibilità d'ufficio del  reato  di  oltraggio,  sono  private  del  &#13;
 potere  di  proporre  (o  di  non proporre) e di rimettere la querela a  &#13;
 tutela della loro personale onorabilità  e  perciò  del  personale  e  &#13;
 privato  interesse  da  questa  costituito.  In  tal  modo  i  pubblici  &#13;
 ufficiali non hanno come gli altri cittadini la facoltà di  deliberare  &#13;
 discrezionalmente a tutela della loro personale onorabilità e del loro  &#13;
 interesse  alla riservatezza, in ordine all'opportunità di provocare o  &#13;
 invece di sottrarsi ad un processo penale.                               &#13;
     Onde ovviare a questa ingiustificata disparità di trattamento,  la  &#13;
 Corte costituzionale dovrebbe quanto meno, con sentenza interpretativa,  &#13;
 stabilire  che,  ove  dal  giudizio  risulti che il fatto è diretto ad  &#13;
 offendere esclusivamente e specificamente' l'onore e  il  decoro  della  &#13;
 persona  fisica  del  pubblico  ufficiale pur nell'esercizio ed a causa  &#13;
 delle  sue  funzioni  e  non  anche   il   prestigio   della   pubblica  &#13;
 amministrazione,   esso  dovrebbe  essere  qualificato  e  punito  come  &#13;
 ingiuria anziché  come  oltraggio  e  fosse  perciò  perseguibile  su  &#13;
 querela della parte offesa.                                              &#13;
     2.  -  L'ordinanza  è  stata regolarmente notificata, comunicata e  &#13;
 pubblicata nella Gazzetta Ufficiale.                                     &#13;
     Dinanzi alla Corte costituzionale è intervenuto il Presidente  del  &#13;
 Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale  &#13;
 dello Stato.                                                             &#13;
     Nel  chiedere  che  la  questione  venga  dichiarata manifestamente  &#13;
 infondata, l'avvocatura osserva che il Pretore  di  Prato  contesta  la  &#13;
 esattezza delle affermazioni contenute nella sentenza n. 109 del 1968 e  &#13;
 che  la sua critica è in sostanza rivolta al legislatore ordinario, il  &#13;
 quale accorda una tutela  differenziata  al  prestigio  della  pubblica  &#13;
 amministrazione  rispetto  a  quello  dei  privati  cittadini, anche se  &#13;
 imprenditori,  ministri  di  culto,  docenti,  medici  o  avvocati.  Si  &#13;
 tratterebbe,  peraltro,  di  una critica del tutto infondata perché la  &#13;
 tutela differenziata della pubblica amministrazione corrisponde ad  una  &#13;
 necessità  insopprimibile dello Stato democratico, mentre l'attuazione  &#13;
 di  tale  tutela  attraverso  la  particolare  sanzione  del  reato  di  &#13;
 oltraggio si adegua esattamente al principio costituzionale secondo cui  &#13;
 "i  pubblici  impiegati sono al servizio esclusivo della Nazione" (art.  &#13;
 98, primo comma).                                                        &#13;
     Il criterio che ispira l'art. 341  del  codice  penale  sarebbe  lo  &#13;
 stesso  per  cui,  dato  il  prevalente  rilievo  costituzionale  della  &#13;
 Magistratura, l'oltraggio al magistrato in udienza corrisponde  da  una  &#13;
 fattispecie  più  grave di reato, sanzionata dall'art. 343 cod. pen. e  &#13;
 per cui, essendo  il  Presidente  della  Repubblica  il  rappresentante  &#13;
 dell'unità  nazionale  (art.  67  Cost.),  l'offesa  al  suo  onore  e  &#13;
 prestigio  costituisce  reato  ancor  più  grave,  quello   sanzionato  &#13;
 dall'art.  278  stesso  codice,  considerato  come  delitto  contro  la  &#13;
 personalità dello Stato.                                                &#13;
     È quindi evidente  che  in  tali  diverse  ipotesi  l'offesa  alla  &#13;
 persona,  seppure costituisce l'elemento materiale costante dei diversi  &#13;
 reati, non può giustificare una disciplina uniforme dell'oltraggio  al  &#13;
 Presidente  della  Repubblica, o al magistrato in udienza pubblica o al  &#13;
 pubblico ufficiale e dell'ingiuria a privati,  onde  non  può  neanche  &#13;
 determinare  una  ingiusta  disparità  di  trattamento  fra le diverse  &#13;
 persone offese da tali reati.                                            &#13;
     L'avvocatura ricorda che tutto  ciò  è  già  stato  ribadito  da  &#13;
 questa Corte con le sentenze 28 novembre 1972, n. 165 e 26 giugno 1974,  &#13;
 n.  192,  nonché  con ulteriori ordinanze dichiarative della manifesta  &#13;
 infondatezza della stessa questione.                                     &#13;
     Infine, secondo l'avvocatura, le stesse considerazioni  valgono  ad  &#13;
 escludere  la  violazione  del  principio di uguaglianza anche sotto il  &#13;
 secondo profilo prospettato dall'ordinanza di rimessione, in ordine  al  &#13;
 quale  rileva  comunque  che  la perseguibilità d'ufficio del reato di  &#13;
 oltraggio non diminuisce, ma rafforza la tutela del pubblico ufficiale,  &#13;
 quale  incarnazione   della   Pubblica   Amministrazione   alla   quale  &#13;
 appartiene.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Pur mostrandosi a conoscenza delle numerose decisioni con cui  &#13;
 questa Corte ha respinto la questione  di  legittimità  costituzionale  &#13;
 dell'art.  341 del codice penale promossa in relazione all'art. 3 della  &#13;
 Costituzione,  il  Pretore  di  Prato  ha  ritenuto  non manifestamente  &#13;
 infondato il dubbio di incompatibilità della norma  in  esame  con  il  &#13;
 principio di eguaglianza sotto un duplice profilo. Da un lato la tutela  &#13;
 differenziata  dell'onore  e  del  prestigio del pubblico ufficiale non  &#13;
 corrisponderebbe  a  un   valore   privilegiato   dalla   Costituzione,  &#13;
 dall'altro  la procedibilità d'ufficio in ordine al reato di oltraggio  &#13;
 priverebbe il pubblico ufficiale del  potere  di  proporre  (o  di  non  &#13;
 proporre)  e  di  rimettere  la  querela  a  tutela della sua personale  &#13;
 onorabilità, con disparità di trattamento rispetto a tutti gli  altri  &#13;
 cittadini.                                                               &#13;
     2. - La questione non è fondata.                                    &#13;
     Come già avvertito nella sentenza n.  109 del 1968, l'articolo 341  &#13;
 del  codice  penale appresta una tutela che trascende la persona fisica  &#13;
 del  titolare  dell'ufficio,  per  risolversi  nella   protezione   del  &#13;
 prestigio  della pubblica amministrazione impersonata da quel titolare.  &#13;
 Nonostante la contraria opinione del giudice a quo il perseguimento  di  &#13;
 un   simile  valore  da  parte  del  legislatore  ordinario  (alla  cui  &#13;
 insindacabile discrezionalità, ove non  trasmodi  in  arbitrio,  vanno  &#13;
 rimesse  le  modalità  attuative concrete), corrisponde alla finalità  &#13;
 del  buon  andamento  amministrativo  prevista   dall'art.   97   della  &#13;
 Costituzione.   Finalità che non si riferisce esclusivamente alla fase  &#13;
 organizzativa iniziale della pubblica amministrazione, ma ne investe il  &#13;
 complesso  funzionamento  (cfr.    sentenza n. 22 del 1966). Di qui una  &#13;
 duplice  conseguenza:  da   un   lato   ragionevolmente   nella   norma  &#13;
 sull'oltraggio  viene  previsto  un  trattamento  penale  più grave di  &#13;
 quello riservato all'ingiuria e dall'altro se  il  pubblico  ufficiale,  &#13;
 privato  del  potere  di  querela, si trova in situazione di disparità  &#13;
 rispetto ai comuni cittadini, tale  disparità  è  giustificata  dalla  &#13;
 protezione di un interesse che supera quello della persona fisica e che  &#13;
 trova fondamento nella Carta costituzionale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 341 del codice penale promossa con l'ordinanza  in  epigrafe,  &#13;
 in riferimento all'art. 3 della Costituzione.                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 2 aprile 1980.                                &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE   STEFANO   -   LEOPOLDO   ELIA  -  &#13;
                                   GUGLIELMO ROEHRSSEN - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
