<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1964</anno_pronuncia>
    <numero_pronuncia>17</numero_pronuncia>
    <ecli>ECLI:IT:COST:1964:17</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/02/1964</data_decisione>
    <data_deposito>14/03/1964</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 10, ultimo  &#13;
 comma, del D.P.R. 26 aprile 1957, n. 818, promosso con ordinanza emessa  &#13;
 il 20 febbraio 1963 dal Tribunale dell'Aquila nel  procedimento  civile  &#13;
 vertente tra Del Cimmuto Angelo e l'Istituto nazionale della previdenza  &#13;
 sociale,  iscritta  al  n.  64 del Registro ordinanze 1963 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 94 del 6 aprile 1963.       &#13;
     Visti gli atti di costituzione in giudizio di Del Cimmuto Angelo  e  &#13;
 dell'Istituto nazionale della previdenza sociale;                        &#13;
     udita  nell'udienza  pubblica  del 6 novembre 1963 la relazione del  &#13;
 Giudice Giovanni Battista Benedetti.                                     &#13;
     Ritenuto che nel giudizio vertente dinanzi al Tribunale dell'Aquila  &#13;
 fra Del Cimmuto Angelo, già ammesso al godimento di pensione ordinaria  &#13;
 per il servizio prestato alle dipendenze del Comune di Pescocostanzo, e  &#13;
 l'Istituto nazionale della previdenza sociale, al  quale  l'interessato  &#13;
 aveva  chiesto  la  pensione  di  invalidità e vecchiaia sulla base di  &#13;
 contributi  già  versati  e  previo  riconoscimento   dei   contributi  &#13;
 figurativi riguardanti il servizio militare da lui prestato nel periodo  &#13;
 1915-1920,  è stata sollevata questione di legittimità costituzionale  &#13;
 dell'ultimo comma dell'art. 10 del D.P.R. 26 aprile 1957, n.  818,  che  &#13;
 non  consente  il  riconoscimento dei contributi figurativi quando essi  &#13;
 siano computabili per altri trattamenti pensionistici;                   &#13;
     che si  assumeva  in  giudizio  dalla  difesa  di  Del  Cimmuto  la  &#13;
 illegittimità  costituzionale  di  tale  disposizione  in  riferimento  &#13;
 all'art. 76 della Costituzione perché eccedente i limiti della  delega  &#13;
 conferita con l'art.  37 della legge 4 aprile 1952, n. 218;              &#13;
     che  il  Tribunale,  dopo  avere escluso che la norma impugnata col  &#13;
 porre il  divieto  della  computabilità  del  servizio  militare  agli  &#13;
 effetti   di  due  diversi  trattamenti  pensionistici  potesse  essere  &#13;
 considerata norma  di  attuazione  della  legge  delegante,  oppure  di  &#13;
 coordinamento  tra  questa  e  la  legislazione  vigente  in materia di  &#13;
 assicurazioni sociali,  ha  ritenuto  rilevante  e  non  manifestamente  &#13;
 infondata   la   dedotta   questione   rinviandone  in  conseguenza  la  &#13;
 risoluzione alla Corte costituzionale;                                   &#13;
     che  nel  procedimento dinanzi a questa Corte si sono costituite le  &#13;
 parti e non è intervenuto il Presidente del Consiglio dei Ministri;     &#13;
     che la difesa dell'I.N.P.S., dopo aver  rilevato  come  la  materia  &#13;
 disciplinata dalla norma impugnata sia stata successivamente sostituita  &#13;
 e  confermata  dall'art.  10  della  legge  20 febbraio 1958, n. 55, ha  &#13;
 osservato che si rendeva  necessaria  la  restituzione  degli  atti  al  &#13;
 giudice  a  quo  per  un nuovo esame della rilevanza della questione di  &#13;
 legittimità dell'art. 10, ultimo comma, del D.P.R. n.  818 del 1957 in  &#13;
 relazione alla sopravvenuta norma dell'art. 10 della legge  n.  55  del  &#13;
 1958;                                                                    &#13;
     che  la  difesa del Del Cimmuto ha, dal canto suo, sostenuto che il  &#13;
 problema  della  legittimità  costituzionale  della  norma   impugnata  &#13;
 permane   ancorché   la   materia   da   essa  considerata  sia  stata  &#13;
 successivamente regolata dalla nuova norma di legge;                     &#13;
     Considerato  che  la  rilevanza  della  questione  di  legittimità  &#13;
 costituzionale in tanto sussiste in quanto il giudizio non possa essere  &#13;
 definito  indipendentemente dalla risoluzione di detta questione e che,  &#13;
 pertanto, deve essere disposta la restituzione degli atti al giudice  a  &#13;
 quo  se  questi  non  abbia  preliminarmente  accertato  che  la  norma  &#13;
 impugnata è applicabile al rapporto controverso;                        &#13;
     che nel caso in esame tale accertamento è  mancato  in  quanto  il  &#13;
 Tribunale  ha  omesso  di  esaminare  se la controversia potesse essere  &#13;
 decisa in base alla disposizione contenuta  nell'art.  10  della  legge  &#13;
 ordinaria  20  febbraio  1958, n. 55, disposizione che si assume essere  &#13;
 identica a quella dell'art. 10, ultimo comma,  del  precedente  decreto  &#13;
 legislativo delegato;                                                    &#13;
     che,   in  conseguenza,  gli  atti  debbono  essere  restituiti  al  &#13;
 Tribunale per l'esame della rilevanza;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina che gli atti siano restituiti al Tribunale dell'Aquila.       &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 25 febbraio 1964.                             &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
