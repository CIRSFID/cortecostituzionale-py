<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1964</anno_pronuncia>
    <numero_pronuncia>85</numero_pronuncia>
    <ecli>ECLI:IT:COST:1964:85</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/10/1964</data_decisione>
    <data_deposito>12/11/1964</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha deliberato in camera di consiglio la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 79, ultimo  &#13;
 comma, del T. U. delle leggi per  la  elezione  dei  Consigli  comunali  &#13;
 nella  Regione  siciliana,  approvato  con decreto del Presidente della  &#13;
 Regione 20 agosto 1960, n. 3, e dell'art. 102, ultimo comma, del T.  U.  &#13;
 delle  leggi  per  la  composizione  e  la  elezione degli organi delle  &#13;
 Amministrazioni comunali, approvato con decreto  del  Presidente  della  &#13;
 Repubblica  16 maggio 1960, n. 570, promosso con ordinanza emessa il 23  &#13;
 gennaio 1964 dal Pretore di Mineo nel procedimento penale a  carico  di  &#13;
 Cantone  Gioacchino  ed altri, iscritta al n. 43 del Registro ordinanze  &#13;
 1964 e pubblicata nella Gazzetta  Ufficiale  della  Repubblica,  n.  91  &#13;
 dell'11 aprile 1964 e nella Gazzetta Ufficiale della Regione siciliana,  &#13;
 n.  15 del 28 marzo 1964.                                                &#13;
     Udita  nella  camera  di consiglio del 22 ottobre 1964 la relazione  &#13;
 del Giudice Giovanni Cassandro;                                          &#13;
     Ritenuto che con ordinanza del 23 gennaio 1964 il Pretore di Mineo,  &#13;
 in accoglimento dell'istanza presentata dalla difesa degli imputati, ha  &#13;
 sollevato la  questione  di  legittimità  costituzionale  delle  norme  &#13;
 contenute  nell'art.  79,  ultimo  comma,  del T. U. delle leggi per la  &#13;
 elezione dei Consigli comunali nella Regione siciliana,  approvato  con  &#13;
 D.  P.  Reg.  20  agosto  1960, n. 3, in relazione all'art. 102, ultimo  &#13;
 comma, del T. U. delle leggi per la composizione e  la  elezione  degli  &#13;
 organi  delle Amministrazioni locali, approvato con D.  P. R. 16 maggio  &#13;
 1960, n. 570, perché tali norme, escludendo l'applicabilità ai  reati  &#13;
 elettorali  delle  disposizioni  relative alla sospensione condizionale  &#13;
 della pena e alla non  menzione  della  condanna  nel  certificato  del  &#13;
 casellario  giudiziale,  sarebbero  in contrasto con la norma del terzo  &#13;
 comma dell'art. 27 della Costituzione, la quale dispone che le pene non  &#13;
 possono consistere in trattamenti  contrari  al  senso  di  umanità  e  &#13;
 devono tendere alla rieducazione del condannato;                         &#13;
     che  davanti  alla Corte è intervenuto il Presidente della Regione  &#13;
 siciliana, rappresentato e difeso dall'Avvocatura generale dello Stato,  &#13;
 con atto depositato il 16 marzo  1964,  concludendo  perché  la  Corte  &#13;
 dichiari   infondata   la   questione  di  legittimità  costituzionale  &#13;
 dell'art. 102 del T. U. 16 maggio 1960, n.   570, e  l'inammissibilità  &#13;
 della questione sollevata nei confronti dell'art. 79, ultimo comma, del  &#13;
 T. U. della Regione siciliana 20 agosto 1960, n. 3;                      &#13;
     Considerato  che  con  sentenza  n.  51 del 5 giugno 1962, la Corte  &#13;
 costituzionale ha dichiarato inammissibile la questione di legittimità  &#13;
 costituzionale dell'art.  60  del  citato  testo  unico  della  Regione  &#13;
 siciliana,  perché  il testo unico che quella norma contiene non è un  &#13;
 atto avente forza di legge;                                              &#13;
     che tale decisione è stata ribadita con ordinanza 4 febbraio 1964,  &#13;
 n. 8;                                                                    &#13;
     che, per conseguenza, pure inammissibile deve essere dichiarata  la  &#13;
 questione  di  costituzionalità dell'art. 79 che fa parte del medesimo  &#13;
 testo unico;                                                             &#13;
     che con sentenza n. 48 del 29 maggio 1962 la  Corte  costituzionale  &#13;
 ha  dichiarato  non fondata la questione di legittimità costituzionale  &#13;
 dell'art. 102, ultimo comma, del T. U.  16  maggio  1960,  n.  570,  in  &#13;
 riferimento all'art. 27, terzo comma, della Costituzione;                &#13;
     che i motivi esposti nell'ordinanza non sono diversi nella sostanza  &#13;
 da  quelli  già  esaminati  dalla  Corte nel precedente giudizio e non  &#13;
 possono perciò indurre a una diversa decisione;                         &#13;
     Visti gli artt. 26, comma secondo, e 29 della legge 11 marzo  1953,  &#13;
 n. 87, e l'art. 9, comma secondo, delle Norme integrative per i giudizi  &#13;
 davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 79, ultimo comma, del T. U. delle leggi per la  elezione  dei  &#13;
 consigli  comunali nella Regione siciliana, approvato con D. P. Reg. 20  &#13;
 agosto 1960, n. 3,                                                       &#13;
     dichiara manifestamente  infondata  la  questione  di  legittimità  &#13;
 costituzionale  dell'art.  102, ultimo comma, del T. U. delle leggi per  &#13;
 la composizione  e  la  elezione  degli  organi  delle  Amministrazioni  &#13;
 comunali,  approvato  con D.P.R. 16 maggio 1960, n. 570, in riferimento  &#13;
 all'art. 27, terzo comma, della Costituzione.                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 ottobre 1964.        &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO</dispositivo>
  </pronuncia_testo>
</pronuncia>
