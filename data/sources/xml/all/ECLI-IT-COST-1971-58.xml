<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>58</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:58</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Vincenzo Trimarchi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1971</data_decisione>
    <data_deposito>22/03/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di  legittimità  costituzionale degli artt.  &#13;
 1751, primo  comma,  del  codice  civile  e  8  dell'accordo  economico  &#13;
 collettivo  del  30  giugno  1938,  per  la  disciplina del rapporto di  &#13;
 agenzia  e  rappresentanza  commerciale,  promossi  con   le   seguenti  &#13;
 ordinanze:                                                               &#13;
     1)  ordinanza  emessa  il 21 novembre 1969 dalla Corte d'appello di  &#13;
 Cagliari nel procedimento civile vertente tra Randone Mario e la s.r.l.  &#13;
 "Alce", iscritta al n. 247 del registro  ordinanze  1970  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 235 del 16 settembre 1970;  &#13;
     2)  ordinanza  emessa il 5 aprile 1970 dal tribunale di Palermo nel  &#13;
 procedimento civile vertente tra Di Giorgi Antonino e Dagnino  Guido  e  &#13;
 l'ENASARCO, iscritta al n. 280 del registro ordinanze 1970 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 267 del 21 ottobre 1970.    &#13;
     Udito  nella  camera  di  consiglio del 10 febbraio 1971 il Giudice  &#13;
 relatore Vincenzo Michele Trimarchi.                                     &#13;
     Ritenuto che la prima delle  due  ordinanze  indicate  in  epigrafe  &#13;
 solleva  in  riferimento all'art. 36 della Costituzione la questione di  &#13;
 legittimità costituzionale degli artt. 1751 ; comma primo, del  codice  &#13;
 civile  e  8  dell'accordo  economico  collettivo  del  30  giugno 1938  &#13;
 "convalidato con decreto presidenziale  26  dicembre  1960,  n.  1842",  &#13;
 nella  parte  in  cui  codesti  articoli  dispongono  che non è dovuta  &#13;
 l'indennità per lo scioglimento del contratto di  agenzia  determinato  &#13;
 da fatto imputabile all'agente;                                          &#13;
     che   la  seconda  ordinanza  solleva  la  questione  limitatamente  &#13;
 all'art. 1751 ; comma primo, e però in riferimento  anche  all'art.  5  &#13;
 della Costituzione;                                                      &#13;
     che i giudizi possono essere riuniti e decisi con unica ordinanza;   &#13;
     che  nei  due giudizi nessuna delle parti si è costituita e non ha  &#13;
 spiegato intervento il Presidente del Consiglio dei ministri.            &#13;
     Considerato che con  sentenza  n.  75  del  1970  questa  Corte  ha  &#13;
 dichiarato  non  fondata  la  questione  di legittimità costituzionale  &#13;
 dell'art. 1751 ; comma primo, in riferimento agli articoli 3,  4  e  36  &#13;
 della Costituzione;                                                      &#13;
     che  relativamente  a detta questione non sono stati addotti motivi  &#13;
 nuovi o tali che possano indurre a modificare la precedente decisione;   &#13;
     che, avendo  il  d.P.R.  26  dicembre  1960,  n.  1842,  attribuito  &#13;
 efficacia  erga  omnes  solo  all'accordo  economico  collettivo del 13  &#13;
 ottobre 1958, la questione di legittimità costituzionale  dell'art.  8  &#13;
 dell'accordo  economico  collettivo del 30 giugno 1938 è inammissibile  &#13;
 perché sollevata a proposito di una norma facente parte di un atto che  &#13;
 non ha forza di legge.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     avvalendosi della procedura in camera di consiglio consentita dagli  &#13;
 artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9; secondo  &#13;
 comma, delle Norme integrative, dichiara la manifesta  inammissibilità  &#13;
 della questione di legittimità costituzionale dell'art. 8 dell'accordo  &#13;
 economico  collettivo 30 giugno 1938 (per la disciplina del rapporto di  &#13;
 agenzia  e  rappresentanza  commerciale),  sollevata,  con  l'ordinanza  &#13;
 indicata in epigrafe, dalla Corte d'appello di Cagliari, in riferimento  &#13;
 all'art. 36 della Costituzione;                                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale  dell'art.  1751  ;  comma  primo,  del  codice  civile,  &#13;
 sollevata  con  l'anzidetta  ordinanza  nonché  con  quella,  del pari  &#13;
 indicata in epigrafe, del tribunale di  Palermo,  in  riferimento  agli  &#13;
 artt.  5  e  36  della  Costituzione, e già dichiarata non fondata con  &#13;
 sentenza n. 75 del 20 maggio 1970.                                       &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'11 marzo 1971.           &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE    VERZÌ   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
