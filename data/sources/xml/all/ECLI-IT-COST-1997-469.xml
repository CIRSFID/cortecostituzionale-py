<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>469</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:469</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/12/1997</data_decisione>
    <data_deposito>30/12/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  sull'ammissibilità  del conflitto di attribuzione tra    &#13;
 poteri dello Stato sollevato dal tribunale di Palermo  nei  confronti    &#13;
 del  Senato della Repubblica a seguito della delibera con la quale il    &#13;
 Senato della Repubblica in  data  20  settembre  1995  ha  dichiarato    &#13;
 l'insindacabilità  delle  opinioni  espresse  dal  senatore  Carmine    &#13;
 Mancuso; conflitto promosso con ricorso depositato il 23 giugno  1997    &#13;
 ed iscritto al n. 78 del registro ammissibilità conflitti;              &#13;
   Udito  nella  camera  di  consiglio del 10 dicembre 1997 il giudice    &#13;
 relatore Riccardo Chieppa;                                               &#13;
   Ritenuto che, nel corso di una trasmissione  televisiva  andata  in    &#13;
 onda   il  12  maggio  1993,  Carmine  Mancuso,  senatore  nella  XII    &#13;
 legislatura, pronunciava  nei  confronti  del  questore  dott.  Bruno    &#13;
 Contrada   delle   espressioni   ritenute   gravemente   diffamatorie    &#13;
 dall'interessato, che perciò sporgeva querela;                          &#13;
     che, con ordinanza del 10 maggio 1994, il giudice per le indagini    &#13;
 preliminari presso il tribunale di Palermo dichiarava  manifestamente    &#13;
 infondata  l'eccezione  di  insindacabilità ex art. 68, primo comma,    &#13;
 della Costituzione, e,  conseguentemente,  ordinava  la  restituzione    &#13;
 degli atti al pubblico ministero;                                        &#13;
     che,  con  lettera  in  data  25 novembre 1994, il presidente del    &#13;
 tribunale di Palermo provvedeva a trasmettere  copia  della  predetta    &#13;
 ordinanza  al  presidente  del  Senato  della  Repubblica,  ai  sensi    &#13;
 dell'art.  3, comma 2, del d.-l. 9 novembre 1994, n.  627,  all'epoca    &#13;
 vigente;                                                                 &#13;
     che,  intervenuta  una  nuova disciplina (d.-l. 13 marzo 1995, n.    &#13;
 69, poi reiterato da successivi decreti-legge, tutti,  peraltro,  non    &#13;
 convertiti),  la Giunta delle elezioni e delle immunità parlamentari    &#13;
 del Senato deliberava di richiedere copia degli atti del procedimento    &#13;
 penale de quo;                                                           &#13;
     che, con deliberazione adottata nella  seduta  del  20  settembre    &#13;
 1995,  l'Assemblea  del  Senato approvava la proposta della Giunta di    &#13;
 dichiarare insindacabili, ai sensi dell'art. 68, primo  comma,  della    &#13;
 Costituzione, le opinioni espresse dal senatore Carmine Mancuso;         &#13;
     che, con ordinanza del 16 ottobre 1995, il tribunale di Palermo -    &#13;
 premesso che l'art. 68, primo comma, della Costituzione configura una    &#13;
 condizione   di  punibilità,  e  non,  come  il  secondo  comma,  di    &#13;
 procedibilità,  con  conseguente  inapplicabilità   del   combinato    &#13;
 disposto  degli  artt.    129  e  469  cod.  proc.  pen., i quali non    &#13;
 contemplano,  tra  le   ipotesi   di   proscioglimento,   prima   del    &#13;
 dibattimento,   la  non  punibilità  dell'imputato  -  disponeva  di    &#13;
 "procedersi   al   dibattimento,   dovendo  peraltro  trovare  tutela    &#13;
 l'eventuale interesse dell'imputato all'accertamento della sua totale    &#13;
 estraneità ai fatti di reato";                                          &#13;
     che,  a  seguito  di  tale  provvedimento,  il  Senato  sollevava    &#13;
 conflitto di attribuzione nei confronti del tribunale di Palermo;        &#13;
     che questa Corte, con sentenza n. 129 del 1996, ha dichiarato che    &#13;
 non  spettava  al  predetto  tribunale  disporre  la celebrazione del    &#13;
 dibattimento nel processo  penale  pendente  a  carico  del  senatore    &#13;
 Mancuso,  annullandone,  pertanto, la citata ordinanza del 16 ottobre    &#13;
 1995;                                                                    &#13;
     che, con successiva ordinanza, emessa in data 13 maggio 1997,  lo    &#13;
 stesso tribunale ha sollevato conflitto di attribuzione nei confronti    &#13;
 del  Senato della Repubblica in relazione alla predetta deliberazione    &#13;
 del 20 settembre  1995,  con  la  quale  quest'ultimo  ha  dichiarato    &#13;
 insindacabili,   ai   sensi   dell'art.   68,   primo   comma,  della    &#13;
 Costituzione, le opinioni espresse dal senatore Mancuso;                 &#13;
     che il tribunale di Palermo ritiene che il Senato non abbia fatto    &#13;
 corretto  uso  del  proprio  potere,  in  quanto   il   comportamento    &#13;
 addebitato  all'imputato  nel  processo  de quo non sarebbe rientrato    &#13;
 nell'esercizio delle funzioni parlamentari, "attenendo, al contrario,    &#13;
 a  vicende  personali  dello  stesso  senatore  ed  ai  rapporti  tra    &#13;
 quest'ultimo, il proprio genitore ed il querelante";                     &#13;
     che,  pertanto, ad avviso del predetto tribunale, il Senato della    &#13;
 Repubblica,  con  la  deliberazione  di  cui   si   tratta,   avrebbe    &#13;
 illegittimamente compresso i poteri ad esso attribuiti dalla legge;      &#13;
   Considerato  che, a norma dell'art. 37, terzo e quarto comma, della    &#13;
 legge 11 marzo 1953, n. 87, la Corte, in questa fase, è  chiamata  a    &#13;
 deliberare,  senza contraddittorio, se il ricorso sia ammissibile, in    &#13;
 quanto esista "la materia di un conflitto la cui  risoluzione  spetti    &#13;
 alla   sua   competenza",   restando  impregiudicata  ogni  ulteriore    &#13;
 decisione;                                                               &#13;
     che, in linea preliminare, la  forma  dell'ordinanza,  utilizzata    &#13;
 dal  tribunale di Palermo per proporre il ricorso di cui all'art.  37    &#13;
 della legge n. 87 del 1953, deve  ritenersi  idonea  per  una  valida    &#13;
 instaurazione  del  conflitto, come ripetutamente affermato da questa    &#13;
 Corte (cfr., da ultimo, ordinanze nn. 325 del 1997 e 339 del 1996);      &#13;
     che deve essere riconosciuta la legittimazione del  tribunale  di    &#13;
 Palermo  a  sollevare  conflitto  secondo il costante insegnamento di    &#13;
 questa  Corte,  tenuto  conto  della  posizione  dei  singoli  organi    &#13;
 giurisdizionali,  che svolgono le loro funzioni in posizione di piena    &#13;
 indipendenza  costituzionalmente  garantita  (cfr.,  tra  le   altre,    &#13;
 ordinanze  nn.  325 del 1997; 6, 269 e 339 del 1996; sentenze nn. 265    &#13;
 del 1997, 129 del 1996);                                                 &#13;
     che, parimenti, il Senato  della  Repubblica  è  legittimato  ad    &#13;
 essere  parte  del  presente  conflitto,  quale  organo  competente a    &#13;
 dichiarare   definitivamente   la   propria   volontà   in    ordine    &#13;
 all'applicabilità  dell'art.  68,  primo  comma,  della Costituzione    &#13;
 (cfr., tra le altre, ordinanze n. 325 del 1997, nn. 6 e 339 del 1996;    &#13;
 sentenze nn. 265 del 1997, 129 del 1996);                                &#13;
     che, per quanto attiene al profilo oggettivo  del  conflitto,  il    &#13;
 ricorrente  lamenta la lesione di una attribuzione costituzionalmente    &#13;
 garantita, assumendo che il Senato abbia illegittimamente compresso i    &#13;
 suoi poteri.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
   Dichiara  ammissibile,  ai  sensi dell'art. 37 della legge 11 marzo    &#13;
 1953, n. 87, il conflitto di attribuzione sollevato dal tribunale  di    &#13;
 Palermo  nei  confronti del Senato della Repubblica con il ricorso in    &#13;
 epigrafe;                                                                &#13;
   Dispone:                                                               &#13;
     che la cancelleria della Corte dia immediata comunicazione  della    &#13;
 presente ordinanza al tribunale di Palermo, ricorrente;                  &#13;
     che,  a  cura  del ricorrente, il ricorso e la presente ordinanza    &#13;
 siano notificati al Senato  della  Repubblica,  in  persona  del  suo    &#13;
 Presidente, entro il termine di trenta giorni dalla comunicazione;       &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 16 dicembre 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Chieppa                            &#13;
                      Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 30 dicembre 1997.                         &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
