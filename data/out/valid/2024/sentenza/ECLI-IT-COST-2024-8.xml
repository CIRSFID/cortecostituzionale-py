<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2024-01-23/8/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2024-01-23/8"/>
          <FRBRalias value="ECLI:IT:COST:2024:8" name="ECLI"/>
          <FRBRdate date="2024-01-23" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="8"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2024-01-23/8/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2024-01-23/8/ita@"/>
          <FRBRdate date="2024-01-23" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2024-01-23/8/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2024-01-23/8/ita@.xml"/>
          <FRBRdate date="2024-01-23" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2023-12-05" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2024-01-23" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="BARBERA" href="/akn/ontology/person/it/BARBERA" showAs="BARBERA"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="valeriaEmma" href="/akn/ontology/person/it/valeriaEmma" showAs="Valeria EMMA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2024</akn4cc:anno>
        <akn4cc:numero>8</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2024:8</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>BARBERA</akn4cc:presidente>
        <akn4cc:relatore>Luca Antonini</akn4cc:relatore>
        <akn4cc:data_decisione>2023-12-05</akn4cc:data_decisione>
        <akn4cc:data_deposito>2024-01-23</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta da:
 Presidente: <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>; Giudici : <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Marco D'ALBERTI, Giovanni PITRUZZELLA, Antonella SCIARRONE ALIBRANDI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 8, comma 3, della legge della Regione Puglia 3 aprile 1995, n. 14 (Modalità di attuazione della <ref href="/akn/it/act/legge/stato/1992-01-15/21/!main">legge 15 gennaio 1992, n. 21</ref> «Legge-quadro per il trasporto di persone mediante autoservizi pubblici non di linea»), promosso dal Consiglio di Stato, sezione quinta, nel procedimento vertente tra G. S. e la Camera di commercio, industria, artigianato e agricoltura di Taranto, con ordinanza del 27 marzo 2023, iscritta al n. 78 del registro ordinanze 2023 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 24, prima serie speciale, dell'anno 2023.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> di G. S.;
 udito nell'udienza pubblica del 5 dicembre 2023 il Giudice relatore Luca Antonini;
 udito l'avvocato Giorgia Calella per G. S.;
 deliberato nella camera di consiglio del 5 dicembre 2023.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 27 marzo 2023 (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2023/78/!main">ord. n. 78 del 2023</ref>), il Consiglio di Stato, sezione quinta, ha sollevato - in riferimento agli artt. 3, 41 e 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> - questioni di legittimità costituzionale dell'art. 8, comma 3, della legge della Regione Puglia 3 aprile 1995, n. 14 (Modalità di attuazione della <ref href="/akn/it/act/legge/stato/1992-01-15/21/!main">legge 15 gennaio 1992, n. 21</ref> «Legge-quadro per il trasporto di persone mediante autoservizi pubblici non di linea»), nella parte in cui richiede, quale requisito di ammissione all'esame d'idoneità all'esercizio dei servizi di taxi e di noleggio con conducente (NCC), «l'assenza di carichi pendenti».
 2.- Il citato art. 8, al comma 1, dispone che i soggetti interessati a sostenere il suddetto esame devono presentare domanda alla commissione provinciale appositamente costituita presso le Camere di commercio, industria, artigianato e agricoltura (CCIAA) della Regione Puglia.
 Quindi, al censurato comma 3, stabilisce che a tale «domanda, a pena di esclusione, [...], deve essere allegata una dichiarazione sostitutiva dell'atto di notorietà attestante», tra l'altro, «l'assenza di carichi pendenti».
 3.- Le questioni sono sorte nel corso del giudizio instaurato da G. S. per ottenere la caducazione del provvedimento con cui la CCIAA di Taranto ha annullato d'ufficio, in via di autotutela, «il superamento dell'esame di idoneità» da questi sostenuto al fine della successiva iscrizione nel ruolo provinciale dei conducenti dei veicoli e dei natanti adibiti ad autoservizi pubblici non di linea, in quanto, all'esito dei controlli in seguito effettuati, è emersa la pendenza nei suoi confronti di due carichi penali.
 4.- Il rimettente è investito dell'appello avverso la sentenza di prime cure, che ha respinto il ricorso ritenendo che la CCIAA avesse correttamente applicato la disposizione censurata, sulla cui asserita illegittimità costituzionale è fondato uno dei motivi di gravame.
 Il provvedimento oggetto del processo principale trae, infatti, origine dalla previsione recata dalla disposizione sospettata, alla cui stregua la sola esistenza di un qualsiasi carico penale pendente impedirebbe l'ammissione al suddetto esame d'idoneità.
 Le questioni sollevate, pertanto, sarebbero rilevanti, poiché l'ambita pronuncia ablativa di questa Corte farebbe venir meno il requisito in discorso e condurrebbe all'accoglimento dell'impugnazione, il cui esito, d'altra parte, dipenderebbe unicamente dalla soluzione dei prospettati dubbi di legittimità costituzionale, essendo prive di fondamento le ulteriori doglianze dell'appellante.
 5.- Quanto alla non manifesta infondatezza, il giudice a quo ritiene che la disposizione sospettata violi, in primo luogo, l'art. 117, terzo comma, Cost., in relazione alla materia «professioni», perché si porrebbe in contrasto con la norma interposta di cui all'<ref href="/akn/it/act/legge/stato/1992-01-15/21/!main#art_6">art. 6 della legge 15 gennaio 1992, n. 21</ref> (Legge quadro per il trasporto di persone mediante autoservizi pubblici non di linea).
 Quest'ultima disposizione non contemplerebbe, infatti, l'inesistenza di carichi pendenti quale «requisito di accesso all'iscrizione nel ruolo della figura professionale in esame», limitandosi, da un lato, a prevedere la necessità dell'iscrizione nel ruolo dei conducenti di veicoli o natanti adibiti ad autoservizi pubblici non di linea istituito presso le CCIAA ai fini del rilascio della licenza per l'esercizio del servizio di taxi e dell'autorizzazione per l'esercizio del servizio di NCC (comma 5); dall'altro, a subordinare la suddetta iscrizione esclusivamente al possesso del certificato di abilitazione professionale alla guida dei veicoli (comma 2) e al superamento dell'esame finalizzato ad accertare l'idoneità all'esercizio dei servizi in questione. Accertamento da compiere, tuttavia, sottolinea il rimettente, «con particolare riferimento» alla sola «conoscenza geografica e toponomastica» (comma 3).
 Di qui il dedotto vulnus, considerato che la disciplina in esame sarebbe ascrivibile alla materia delle professioni e che in tale ambito materiale, secondo la consolidata giurisprudenza di questa Corte (sono citate, fra le altre, le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/209/!main">sentenze n. 209 del 2020</ref> e n. 98 del 2013), spetterebbe allo Stato l'individuazione, non solo delle figure professionali, ma anche dei relativi titoli abilitanti, senza che le regioni possano introdurre requisiti ulteriori, «non potendosi avere», con riferimento alla «professione de qua, una disciplina differenziata».
 5.1.- Il giudice a quo ritiene, altresì, che la disposizione regionale censurata «contrasti con il canone di ragionevolezza e proporzionalità, sotteso all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, sotto vari punti di vista».
 5.1.1- Rispetto all'esigenza di assicurare l'affidabilità morale dei futuri conduttori, la disposizione denunciata, infatti, non terrebbe in considerazione, in particolare, la gravità e l'«eventuale riflesso sulla professione che si intende esercitare» delle fattispecie di reato «che possono essere ricomprese nella previsione dei "carichi pendenti"», e, per di più, prescinderebbe da una «valutazione da parte della [...] Commissione» deputata all'accertamento dei requisiti d'idoneità professionale.
 Il principio di ragionevolezza sarebbe, inoltre, compromesso perché la preclusione in parola, essendo ancorata alla sola pendenza del carico, opererebbe in virtù del «mero esercizio dell'azione penale da parte del P.M., ex <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_60">art. 60 c.p.p.</ref>, in assenza di qualsivoglia vaglio da parte dell'organo giudicante, anche di tipo sommario - quale quello espresso in sede di rinvio a giudizio o in sede di adozione di una misura cautelare personale - ovvero in assenza di una sentenza di condanna anche di primo grado».
 La disposizione sospettata - evidenzia, infine, il rimettente - nemmeno terrebbe conto della pena massima edittale irrogabile in relazione al reato oggetto del carico pendente e, quindi, dell'applicabilità della pena accessoria dell'interdizione dai pubblici uffici, mentre l'alinea successivo a quello censurato condiziona la rilevanza ostativa delle condanne proprio alla condizione che sia stata inflitta tale pena accessoria. Ciò determinerebbe il paradossale risultato che, in presenza di reati che non potrebbero comportarla, la preclusione in esame scatterebbe comunque per la sola pendenza del carico e non opererebbe, invece, una volta intervenuta la condanna.
 5.2.- I medesimi rilievi conducono il Consiglio di Stato a ritenere conclusivamente violato anche l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_41">art. 41 Cost.</ref>, con riferimento alla libertà di iniziativa economica «cui si correla l'esercizio delle professioni».
 Non sarebbe, difatti, ravvisabile una ragionevole e proporzionata correlazione tra la preclusione della possibilità di svolgere la professione de qua in virtù della «mera pendenza di un carico penale, riferibile a qualsivoglia fattispecie di reato», e le esigenze di tutela espresse dall'evocato parametro costituzionale.
 6.- Si è costituito in giudizio il ricorrente nel processo principale, chiedendo l'accoglimento delle questioni sollevate in forza di argomentazioni analoghe a quelle addotte dal giudice a quo e insistendo, nella memoria depositata in prossimità dell'udienza, nelle conclusioni già rassegnate.
 7.- La Regione Puglia non è intervenuta in giudizio.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 27 marzo 2023 (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2023/78/!main">ord. n. 78 del 2023</ref>), il Consiglio di Stato, sezione quinta, dubita della legittimità costituzionale dell'art. 8, comma 3, della legge reg. Puglia n. 14 del 1995, nella parte in cui richiede, quale requisito di ammissione all'esame d'idoneità all'esercizio dei servizi di taxi e di noleggio con conducente, «l'assenza di carichi pendenti».
 2.- Questi servizi, ai sensi dell'<ref href="/akn/it/act/legge/stato/1992/21/!main#art_1__para_2">art. 1, comma 2, della legge n. 21 del 1992</ref>, costituiscono autoservizi pubblici non di linea e la suddetta legge regionale stabilisce che la domanda di ammissione al suddetto esame d'idoneità (di cui agli artt. 6, comma 3, e 8) deve essere corredata, a pena di esclusione, da una dichiarazione sostitutiva dell'atto di notorietà attestante, tra l'altro, «l'assenza di carichi pendenti» (art. 8, comma 3, primo alinea).
 3.- Secondo il Consiglio di Stato quest'ultima previsione violerebbe, in primo luogo, l'art. 117, terzo comma, Cost., in relazione alla materia «professioni», perché, posto che le discipline regionali, secondo la costante giurisprudenza di questa Corte, non potrebbero porsi in contrasto con «i principi fissati dalla legge quadro statale», imporrebbe un requisito di accesso alla professione, concernente l'esercizio dei servizi di taxi e di noleggio con conducente, non contemplato dalla norma interposta di cui all'<ref href="/akn/it/act/legge/stato/1992/21/!main#art_6">art. 6 della legge n. 21 del 1992</ref>.
 In secondo luogo, il rimettente ritiene che la disposizione sospettata «contrasti con il canone di ragionevolezza e proporzionalità, sotteso all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, sotto vari punti di vista».
 La norma pugliese, infatti, precluderebbe l'ammissione all'esame d'idoneità a prescindere dalla gravità e dall'«eventuale riflesso sulla professione che si intende esercitare» delle fattispecie di reato «che possono essere ricomprese nella previsione dei "carichi pendenti"».
 Inoltre, l'effetto ostativo risulterebbe connesso «al mero esercizio dell'azione penale da parte del P.M.», in assenza di qualsivoglia vaglio da parte dell'organo giudicante, anche di tipo sommario.
 Infine, il suddetto effetto si produrrebbe in mancanza di ogni motivata valutazione da parte delle commissioni provinciali, costituite presso le CCIAA, deputate allo svolgimento dell'esame di idoneità.
 Peraltro, prescindendo dall'applicabilità della pena accessoria dell'interdizione dai pubblici uffici, alla cui irrogazione l'alinea successivo a quello censurato subordina, invece, la rilevanza ostativa delle condanne, il filtro di ammissione in esame darebbe altresì luogo a un'irragionevole contraddizione: in presenza di reati che non comportano la suddetta interdizione, la preclusione de qua scatterebbe comunque, per effetto della mera pendenza del carico, mentre non opererebbe una volta intervenuta la condanna.
 Alla luce delle medesime considerazioni sarebbe, infine, violato l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_41">art. 41 Cost.</ref>, dal momento che l'ostatività della «mera pendenza di un carico penale, riferibile a qualsivoglia fattispecie di reato», determinerebbe un'irragionevole e sproporzionata compressione della libertà di iniziativa economica privata.
 4.- La questione di legittimità costituzionale sollevata in riferimento all'art. 117, terzo comma, Cost. deve, in via preliminare, essere dichiarata inammissibile.
 Il Consiglio di Stato assume che la disposizione regionale denunciata sia riconducibile alla materia «professioni» e, pertanto, risulti censurabile per contrasto con l'evocato parametro costituzionale nella formulazione vigente, derivante dalla riforma di cui alla legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>).
 Sennonché, il giudice a quo non considera che la norma pugliese è anteriore a tale riforma costituzionale e non ha subito modifiche.
 Questa Corte ha, invece, più volte affermato «la necessità che lo scrutinio sia riferito ai parametri in vigore al momento dell'emanazione della normativa regionale (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/130/!main">is, sentenze n. 130 del 2015</ref> e n. 62 del 2012) e ha ritenuto inammissibili questioni sollevate senza motivare "in ordine alle ragioni per le quali [si] ritiene di dover evocare parametri sopravvenuti all'adozione della legge regionale" (ex plurim<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2016/247/!main">is, ordinanza n. 247 del 2016</ref>)» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/52/!main">sentenza n. 52 del 2022</ref>).
 Il rimettente, non adducendo alcun argomento al riguardo, pregiudica la motivazione posta a sostegno della non manifesta infondatezza e ciò si ripercuote sull'ammissibilità della questione in esame.
 5.- Nel merito, la censura di violazione del principio di proporzionalità e di quello di ragionevolezza, tutelati dall'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, è fondata.
 5.1.- Come si è chiarito, la disposizione regionale sospettata condiziona all'assenza di carichi pendenti l'ammissione all'esame d'idoneità professionale, il cui superamento è funzionale all'iscrizione nel ruolo dei conducenti e al conseguimento della licenza per l'esercizio del servizio di taxi e dell'autorizzazione all'esercizio del servizio di NCC.
 In questi termini, essa impedisce la partecipazione al suddetto esame in virtù della mera pendenza di un qualsiasi carico penale: ogni ipotesi di reato prevista dalla legislazione, una volta oggetto d'imputazione, finisce, quindi, per determinare tale effetto ostativo.
 5.2.- Il vulnus al principio di proporzionalità, in tal caso, non attiene alla legittimità del fine che il legislatore regionale sembra essersi prefissato, che, in astratto, potrebbe apparire funzionale a garantire un adeguato svolgimento di servizi pubblici, quali quelli di taxi e di NCC, che si svolgono a stretto contatto con gli utenti.
 Riguarda, piuttosto, il macroscopico difetto, in concreto, di una connessione razionale tra il mezzo predisposto dal legislatore pugliese e il fine che questi intende perseguire, perché la disposizione censurata finisce per intercettare, con effetto ostativo, una vastissima gamma di possibili violazioni alla legislazione penale che nulla hanno a che vedere con l'affidabilità dei soggetti che ambiscono ad essere ammessi all'esame in questione. Qualsiasi ipotesi di reato, infatti, impedisce, contrassegnando la persona con un abnorme stigma sociale, la possibilità di svolgere un'attività lavorativa quale quella in oggetto.
 Nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/161/!main">sentenza n. 161 del 2018</ref>, questa Corte, del resto, ha sì escluso il contrasto con il principio di proporzionalità di norme che, in sostanza, precludono, a coloro che abbiano subito una condanna penale per determinati reati, il mantenimento (e, a monte, il conseguimento) dell'autorizzazione allo svolgimento della professione di autotrasportatore di cose per conto terzi e di viaggiatori con autobus. Ma ciò proprio in quanto non si trattava di un'elencazione «casuale», perché dettata «ora dall'oggettiva gravità della violazione, ora dalla relazione fra questa e l'attività svolta dall'interessato».
 L'art. 8, comma 3, della legge reg. Puglia n. 14 del 1995 non effettua, invece, alcuna selezione e produce, in tal modo, un effetto interdittivo del tutto sproporzionato, operando, come detto, anche in relazione a molteplici fattispecie che non manifestano alcuna correlazione causale tra il requisito in parola e lo scopo cui esso stesso dovrebbe essere funzionale.
 5.2.1.- La preclusione stabilita dalla norma regionale, oltretutto, sorge per effetto della mera pendenza del carico penale e, quindi - in virtù del combinato disposto degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2002-11-14/313/!main#art_2__para_1__point_b">artt. 2, comma 1, lettera b)</ref>, e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2002-11-14/313/!main#art_2__para_1__point_6">6</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2002-11-14/313/!main#art_2__para_1__point_a">comma 1, lettera a), del d.P.R. 14 novembre 2002, n. 313</ref></mref>, recante «Testo unico delle disposizioni legislative e regolamentari in materia di casellario giudiziale, di casellario giudiziale europeo, di anagrafe delle sanzioni amministrative dipendenti da reato e dei relativi carichi pendenti. (Testo A)» -, sin dal momento dell'assunzione della qualità di imputato ai sensi dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_60">art. 60 del codice di procedura penale</ref>.
 Questa Corte, invece, ha precisato che la «linea tendenziale dell'ordinamento» è quella di ritenere che lo specifico presupposto di operatività di effetti extrapenali - analoghi a quelli previsti dalla disposizione censurata - debba essere «che l'accertamento della responsabilità penale sia stato oggetto di un primo vaglio giudiziario», sicché sia ravvisabile «un nesso affidabile - quale riflesso del diritto dell'indagato a non essere considerato colpevole, nel procedimento penale, sino all'emanazione di un provvedimento irrevocabile di condanna - tra la possibile responsabilità penale e l'idoneità a svolgere determinate attività richiedenti particolari requisiti di moralità» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/152/!main">sentenza n. 152 del 2022</ref>).
 Del resto, lo stesso legislatore statale ha oggi chiarito in via generale, all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_335bis">art. 335-bis cod. proc. pen.</ref>, che «[l]a mera iscrizione nel registro di cui all'articolo 335 non può, da sola, determinare effetti pregiudizievoli di natura civile o amministrativa per la persona alla quale il reato è attribuito».
 5.2.2.- In definitiva, omettendo di delimitare, all'interno dell'intera area del diritto penale, gli eventuali reati ostativi pertinenti rispetto all'attività da espletare e addirittura prescindendo da qualsiasi vaglio dell'imputazione da parte del giudice, la disposizione in esame non può superare il test di proporzionalità.
 5.3.- L'art. 8, comma 3, della legge reg. Puglia n. 14 del 1995, inoltre, all'alinea successivo a quello censurato, disciplina specificamente anche le condanne, circoscrivendone, tuttavia, l'efficacia ostativa a quelle che comportano l'interdizione dai pubblici uffici.
 Ne consegue che, mentre la sola formulazione dell'imputazione per un reato, il cui accertamento non conduce all'irrogazione di tale pena accessoria, impedisce comunque, stante la mera pendenza del carico penale, la partecipazione all'esame d'idoneità, questa, paradossalmente, non è invece preclusa dalla condanna per quello stesso reato.
 Ciò che determina, sotto questo profilo, anche la violazione del principio di ragionevolezza.
 5.4.- Rimane assorbito l'ulteriore profilo di censura dedotto dal rimettente in ordine alla violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, inerente all'assenza di una motivata valutazione delle commissioni provinciali costituite presso le CCIAA.
 6.- L'art. 8, comma 3, censurato, prescrivendo il requisito dell'insussistenza di carichi pendenti, per quanto esposto finisce anche per comprimere irragionevolmente la libertà di iniziativa economica privata di cui all'art. 41, primo comma, Cost., perché si traduce in «una indebita barriera all'ingresso nel mercato» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021/7/!main">sentenza n. 7 del 2021</ref>) dei servizi in questione, già, peraltro, caratterizzato, come più volte ha rimarcato l'Autorità garante della concorrenza e del mercato (da ultimo, mediante segnalazione del 3 novembre 2023, rif. n. S4778), da una inadeguata apertura all'ingresso di nuovi soggetti.
 Del resto, la necessità di evitare ingiustificate barriere nello specifico settore del trasporto di persone mediante il servizio di NCC è stata di recente precisata anche dalla Corte di giustizia dell'Unione europea, per la quale restrizioni alla libertà di stabilimento possono essere ammesse purché rispettino, tra l'altro, «il principio di proporzionalità, il che implica che esse siano idonee a garantire, in modo coerente e sistematico, la realizzazione dell'obiettivo perseguito e non eccedano quanto necessario per conseguirlo» (sentenza 8 giugno 2023, in causa C-50/21, Prestige and Limousine SL).
 7.- Va quindi dichiarata l'illegittimità costituzionale - per violazione degli artt. 3, primo comma, e 41, primo comma, Cost. - dell'art. 8, comma 3, della legge reg. Puglia n. 14 del 1995, nella parte in cui prevede che la dichiarazione sostitutiva dell'atto di notorietà che deve essere allegata alla domanda di ammissione all'esame d'idoneità all'esercizio dei servizi di taxi e di NCC attesti «l'assenza di carichi pendenti».</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 1) dichiara l'illegittimità costituzionale dell'art. 8, comma 3, della legge della Regione Puglia 3 aprile 1995, n. 14 (Modalità di attuazione della <ref href="/akn/it/act/legge/stato/1992-01-15/21/!main">legge 15 gennaio 1992, n. 21</ref> «Legge-quadro per il trasporto di persone mediante autoservizi pubblici non di linea»), nella parte in cui prevede che la dichiarazione sostitutiva dell'atto di notorietà che deve essere allegata alla domanda di ammissione all'esame d'idoneità all'esercizio dei servizi di taxi e di noleggio con conducente attesti «l'assenza di carichi pendenti»;
 2) dichiara inammissibile la questione di legittimità costituzionale dell'art. 8, comma 3, della legge reg. Puglia n. 14 del 1995, sollevata, in riferimento all'art. 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, dal Consiglio di Stato, sezione quinta, con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2023-12-05">5 dicembre 2023</date>.
 F.to:
 <signature><judge refersTo="#augustoAntonioBarbera" as="#presidente">Augusto Antonio BARBERA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#lucaAntonini" as="#relatore">Luca ANTONINI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#valeriaEmma" as="#cancelliere">Valeria EMMA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2024-01-23">23 gennaio 2024</date>
 Il Cancelliere
 F.to: Valeria EMMA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
