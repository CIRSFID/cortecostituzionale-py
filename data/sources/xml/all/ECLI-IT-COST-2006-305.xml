<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>305</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:305</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Alfonso Quaranta</relatore_pronuncia>
    <redattore_pronuncia>Alfonso Quaranta</redattore_pronuncia>
    <data_decisione>05/07/2006</data_decisione>
    <data_deposito>20/07/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 53, comma 1, del decreto legislativo 8 giugno 2001, n. 325 (Testo unico delle disposizioni legislative in materia di espropriazione per pubblica utilità – Testo B), promossi con due ordinanze del 6 giugno 2005 del Tribunale amministrativo regionale della Calabria, sui ricorsi proposti rispettivamente da Gioia Ugo contro il Comune di Laino Borgo e da Sculco Cesira Maria Emilia ed altri contro l'Anas s.p.a. ed altri, iscritte ai n. 463 e 464 del registro ordinanze 2005 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 39, prima serie speciale, dell'anno 2005. &#13;
      Visto l'atto di costituzione di Sculco Cesira Maria Emilia ed altri; &#13;
      udito nell'udienza pubblica del 4 luglio 2006 il Giudice relatore Alfonso Quaranta. &#13;
 Ritenuto che con ordinanza del 6 giugno 2005 (r.o. n. 463 del 2005) il Tribunale amministrativo regionale della Calabria ha sollevato questione di legittimità costituzionale dell'art. 53, comma 1, del decreto legislativo 8 giugno 2001, n. 325 (Testo unico delle disposizioni legislative in materia di espropriazione per pubblica utilità – Testo B) – al quale è conforme l'art. 53, comma 1, del d.P.R. 8 giugno 2001, n. 327 (Testo unico delle disposizioni legislative e regolamentari in materia di espropriazione per pubblica utilità – Testo A) – per contrasto con l'art. 103 della Costituzione, nella parte in cui prevede la devoluzione alla giurisdizione esclusiva del giudice amministrativo delle controversie concernenti i comportamenti delle amministrazioni pubbliche, e dei soggetti equiparati, in materia di espropriazione per pubblica utilità; &#13;
    che con altra ordinanza in pari data (r.o. n. 464 del 2005) lo stesso Tribunale amministrativo regionale della Calabria ha sollevato questione di legittimità costituzionale del medesimo art. 53, comma 1, del d.lgs. n. 325 del 2001, sempre per violazione dell'art. 103 della Costituzione;  &#13;
    che, in relazione alla prima ordinanza (r.o. n. 463 del 2005), il dubbio di costituzionalità è stato prospettato nel corso di un giudizio proposto, nei confronti del Comune di Laino Borgo, dal proprietario di un terreno, oggetto di un provvedimento di occupazione d'urgenza finalizzato alla espropriazione del fondo e alla realizzazione, sullo stesso, di un mercato coperto;  &#13;
    che il ricorrente aveva chiesto al Tribunale amministrativo regionale della Calabria la condanna del convenuto al ristoro dei danni subiti a seguito della illegittima occupazione del terreno;  &#13;
    che il rimettente qualifica tale domanda come volta ad ottenere il risarcimento del pregiudizio per la perdita del diritto di proprietà conseguente all'avvenuto perfezionamento di una fattispecie di occupazione acquisitiva;  &#13;
    che, in relazione alla seconda ordinanza (r.o. n. 464 del 2005), la questione di legittimità costituzionale è stata sollevata nel corso di un giudizio proposto dai proprietari di un terreno, oggetto di decreto di occupazione d'urgenza finalizzato alla realizzazione di lavori di ammodernamento della strada statale n. 106;  &#13;
    che decorsi i termini fissati nel decreto senza che fosse stato emesso il provvedimento di espropriazione, i ricorrenti avevano adito il Tribunale amministrativo regionale della Calabria, deducendo che, nonostante il completamento dell'opera pubblica, non poteva ritenersi operante il meccanismo dell'occupazione acquisitiva, istituto che la Corte europea dei diritti dell'uomo, nella decisione in data 30 maggio 2000, aveva ritenuto contrario ai principi di cui all'art. 1 del Protocollo addizionale della Convenzione europea per la salvaguardia dei diritti dell'uomo;  &#13;
    che, sulla base di tali premesse, avevano quindi chiesto la condanna dell'impresa esecutrice delle opere e dell'Anas, stazione appaltante, alla restituzione dell'area, nonché al risarcimento dei danni subiti a seguito dell'illecita occupazione della stessa;  &#13;
    che, in entrambe le ordinanze di rimessione, il Tribunale rimettente svolge le medesime argomentazioni a sostegno della illegittimità costituzionale della norma impugnata;  &#13;
    che, in punto di non manifesta infondatezza, si osserva che gli argomenti che hanno condotto questa Corte a dichiarare, con la sentenza n. 204 del 2004, la parziale illegittimità dell'art. 34 del decreto legislativo 31 marzo 1998, n. 80 (Nuove disposizioni in materia di organizzazione e di rapporti di lavoro nelle amministrazioni pubbliche, di giurisdizione nelle controversie di lavoro e di giurisdizione amministrativa, emanate in attuazione dell'articolo 11, comma 4, della legge 15 marzo 1997, n. 59), ben potrebbero essere impiegati per pervenire al medesimo risultato con riferimento alla disposizione impugnata;  &#13;
    che, in particolare, nella citata sentenza la Corte ha ritenuto non conforme all'art. 103 della Costituzione l'art. 34 del d.lgs. n. 80 del 1998, nella parte in cui estendeva la giurisdizione esclusiva in materia di urbanistica ed edilizia, anche ai comportamenti, così allargando l'ambito di tale giurisdizione a fattispecie in cui la pubblica amministrazione non esercita neppure mediatamente un pubblico potere;  &#13;
    che, in ordine alla rilevanza della questione, osserva il rimettente che entrambi i giudizi, concernenti una fattispecie acquisitiva perfezionatasi prima dell'entrata in vigore del nuovo testo unico in materia di espropriazioni, avvenuta il 30 giugno 2003, sono stati introdotti successivamente a tale data, risultando, rispettivamente, il ricorso relativo all'ordinanza n. 463 del 2005 notificato nel maggio del 2004 e depositato il successivo 1° giugno ed il ricorso relativo alla ordinanza n. 464 del 2005 notificato nel novembre del 2003 e depositato il giorno 25 dello stesso mese;  &#13;
    che, pertanto, ai sensi dell'art. 5 del codice procedura civile, secondo il rimettente non possono esservi dubbi sull'applicabilità, alle fattispecie dedotte nei due giudizi, della norma sospettata di illegittimità.  &#13;
    Considerato che, attesa la sostanziale identità delle questioni sollevate, deve essere disposta la riunione dei due giudizi; &#13;
    che, successivamente all'emanazione di entrambe le ordinanze di rimessione, questa Corte, con la sentenza n. 191 del 2006, ha dichiarato la parziale illegittimità costituzionale dell'art. 53, comma 1, del decreto legislativo 8 giugno 2001, n. 325 (Testo unico delle disposizioni legislative in materia di espropriazione per pubblica utilità – Testo B), trasfuso nell'art. 53, comma 1, del decreto del Presidente della Repubblica 8 giugno 2001, n. 327 (Testo unico delle disposizioni legislative e regolamentari in materia di espropriazione per pubblica utilità – Testo A);  &#13;
    che, in particolare, questa Corte, richiamando i principi già enunciati nella sentenza n. 204 del 2004, ha affermato che la norma impugnata utilizzando l'ampia locuzione “comportamenti”, senza provvedere ad una specifica qualificazione degli stessi, abbia inteso attribuire alla giurisdizione esclusiva del giudice amministrativo le controversie nelle quali sia parte la pubblica amministrazione, facendo così del giudice amministrativo il giudice dell'amministrazione piuttosto che l'organo di garanzia della giustizia nell'amministrazione (sentenza n. 191 del 2006);  &#13;
    che la giurisdizione esclusiva del giudice amministrativo si giustifica, invece, soltanto in presenza di “comportamenti” – nella specie, la realizzazione dell'opera – che «costituiscono esecuzione di atti o provvedimenti amministrativi (dichiarazione di pubblica utilità e/o di indifferibilità e urgenza)» e che siano quindi riconducibili all'esercizio, ancorché viziato da illegittimità, del pubblico potere dell'amministrazione (citata sentenza n. 191 del 2006); &#13;
    che, alla luce dei principi sopra esposti, questa Corte ha ritenuto che «deve ritenersi conforme a Costituzione la devoluzione alla giurisdizione esclusiva del giudice amministrativo delle controversie relative a “comportamenti” (di impossessamento del bene altrui) collegati all'esercizio, pur se illegittimo, di un pubblico potere, laddove deve essere dichiarata costituzionalmente illegittima la devoluzione alla giurisdizione esclusiva di “comportamenti” posti in essere in carenza di potere ovvero in via di mero fatto» (citata sentenza n. 191 del 2006); &#13;
    che, pertanto, alla luce delle considerazioni che precedono, gli atti vanno restituiti al Tribunale rimettente affinché valuti se le sollevate questioni di legittimità costituzionale siano tuttora rilevanti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi,  &#13;
    ordina la restituzione degli atti al Tribunale amministrativo regionale della Calabria. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 5 luglio 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 20 luglio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
