<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>729</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:729</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/06/1988</data_decisione>
    <data_deposito>30/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1 della legge 4    &#13;
 agosto 1984, n. 424 (Inasprimento  delle  sanzioni  amministrative  a    &#13;
 carico  dei  trasgressori  delle  norme  in  difesa  dei boschi dagli    &#13;
 incendi), promosso con ricorso della Regione Liguria notificato il  7    &#13;
 settembre  1984, depositato in cancelleria il 15 settembre successivo    &#13;
 ed iscritto al n. 29 del registro ricorsi 1984.                          &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza pubblica dell'8 marzo 1988 il Giudice relatore    &#13;
 Aldo Corasaniti;                                                         &#13;
    Uditi  l'avv.  Ludovico  Villani  per  la Regione Liguria e l'avv.    &#13;
 dello Stato Giorgio Azzariti per  il  Presidente  del  Consiglio  dei    &#13;
 ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ricorso notificato il 7 settembre 1984 la Regione Liguria    &#13;
 ha promosso giudizio di legittimità costituzionale,  per  violazione    &#13;
 dell'art.  117  Cost.  in  relazione agli artt. 66 e 69 del d.P.R. 24    &#13;
 luglio 1977, n. 616, dell'art. 1 della legge 4 agosto  1984,  n.  424    &#13;
 recante  "Inasprimento  delle  sanzioni  amministrative  a carico dei    &#13;
 trasgressori delle norme  in  materia  di  difesa  dei  boschi  dagli    &#13;
 incendi".                                                                &#13;
    Espone  la  ricorrente che la disposizione denunciata ha inasprito    &#13;
 le sanzioni pecuniarie per le infrazioni alle prescrizioni in materia    &#13;
 di boschi e terreni montani richiamate dall'art. 10 della legge primo    &#13;
 marzo 1975, n. 47 recante "Norme integrative per la difesa dei boschi    &#13;
 dagli  incendi". Tale norma aveva attribuito natura contravvenzionale    &#13;
 ad ogni infrazione punita con l'ammenda ai sensi degli artt. 24,  26,    &#13;
 54  e  135  del  R.D.L.  30  dicembre 1923, n. 3267 ("Riordinamento e    &#13;
 riforma  della  legislazione  in  materia  di  boschi  e  di  terreni    &#13;
 montani").                                                               &#13;
    Per  effetto  dell'art. 32 della legge 24 novembre 1981, n. 689 le    &#13;
 infrazioni venivano depenalizzate riacquistando  natura  di  illeciti    &#13;
 amministrativi.                                                          &#13;
    La  tutela dei boschi, attesa la sua stretta inerenza alla materia    &#13;
 "agricoltura e foreste", inclusa  nell'elenco  dall'art.  117  Cost.,    &#13;
 anche   a   seguito   del   trasferimento   delle  relative  funzioni    &#13;
 amministrative realizzato dall'art. 69 del d.P.R. 24 luglio 1977,  n.    &#13;
 616,  è indubbiamente compresa nella sfera di competenza legislativa    &#13;
 delle Regioni, cui va riconosciuta la potestà di introdurre sanzioni    &#13;
 amministrative    per    comportamenti   trasgressivi   di   precetti    &#13;
 disciplinanti la materia.                                                &#13;
    La  Regione  Liguria  aveva  pertanto  predisposto,  con  la legge    &#13;
 regionale 16 aprile 1984, n. 22, una regolamentazione organica  della    &#13;
 materia  forestale,  con  un  generale  riordinamento  delle sanzioni    &#13;
 amministrative per i trasgressori delle norme in  materia  di  tutela    &#13;
 delle  foreste,  sia  mediante  la  introduzione di nuove fattispecie    &#13;
 d'illecito, sia mediante un sistematico inasprimento delle pene.         &#13;
    Lamenta  la  Regione  che  la legge statale 4 agosto 1984, n. 424,    &#13;
 ignorando  la  competenza  regionale,  sopravveniva  prescrivendo  il    &#13;
 raddoppio  della sanzioni amministrative per le infrazioni richiamate    &#13;
 dalla legge n. 47 del 1975. La disposizione censurata, fondata  sulla    &#13;
 premessa   del  persistente  vigore  della  legislazione  statale  in    &#13;
 materia, se è idonea, come norma di dettaglio,  a  produrre  effetti    &#13;
 nelle  Regioni  che  hanno  conservato  la  regolamentazione statale,    &#13;
 astenendosi sino ad ora dal dettare una  propria  disciplina  in  una    &#13;
 materia enumerata dall'art. 117 Cost., è tuttavia inapplicabile alla    &#13;
 Regione Liguria, che ha già provveduto, come detto, ad  abrogare  le    &#13;
 disposizioni ora aggiornate.                                             &#13;
    Ove si ritenesse nondimeno, osserva la ricorrente, che la legge n.    &#13;
 424 del 1984, omettendo ogni accenno alle competenze regionali, abbia    &#13;
 abgrogato  la  legge ligure, disciplinando il settore non già con la    &#13;
 sola prefissione di  princìpi,  ma  individuando  esaustivamente  le    &#13;
 sanzioni  relative  alla  singola fattispecie, essa concreterebbe una    &#13;
 violazione dell'art. 117 Cost. in relazione agli artt. 66  e  69  del    &#13;
 d.P.R. 24 luglio 1977, n. 616.                                           &#13;
    2.  -  Si  è costituito il Presidente del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura dello Stato,  concludendo  per    &#13;
 l'infondatezza della questione.                                          &#13;
    Osserva   l'Avvocatura   che  l'incontestabile  devoluzione  della    &#13;
 materia alla competenza regionale non preclude allo Stato di  dettare    &#13;
 norme  con  valore  di  princìpio,  anche  al  fine di coordinare ed    &#13;
 armonizzare le varie  legislazioni  regionali,  e  di  supplire  alla    &#13;
 inerzia  di  singole  Regioni.  Restituendo  efficacia  ad un sistema    &#13;
 sanzionatorio di scarsa capacità dissuasiva per la sensibile perdita    &#13;
 del  potere  di  acquisto  della  moneta,  il  legislatore statale ha    &#13;
 determinato la soglia minima di efficacia del sistema.                   &#13;
    La  disposizione  denunciata  ha  dunque  anche valore di norma di    &#13;
 princìpio, in quanto diretta ad armonizzare i  sistemi  sanzionatori    &#13;
 delle  singole  Regioni  "nella  misura della pena". Ne deriva che la    &#13;
 legge statale non  pregiudica  le  normative  regionali  che  abbiano    &#13;
 fissato sanzioni non inferiori.                                          &#13;
    Nella fattispecie, rileva infine l'Avvocatura, la norma denunciata    &#13;
 non importa alcun riflesso sul sistema sanzionatorio disegnato  dalla    &#13;
 Regione  Liguria,  in  quanto  di  efficacia  non  minore  di  quella    &#13;
 richiesta dal legislatore statale.<diritto>Considerato in diritto</diritto>1. - Il ricorso in epigrafe solleva in via principale questione di    &#13;
 legittimità costituzionale dell'art. 1 della legge 4 agosto 1984, n.    &#13;
 424   ("Inasprimento  delle  sanzioni  amministrative  a  carico  dei    &#13;
 trasgressori delle norme  in  materia  di  difesa  dei  boschi  dagli    &#13;
 incendi")  per violazione dell'art. 117 Cost. in relazione agli artt.    &#13;
 66 e 69 del d.P.R. 24 luglio 1977 n. 616.                                &#13;
    La  norma,  secondo la regione, non si limita a fissare princìpi,    &#13;
 ma individua - sia pure per relationem - sanzioni relative a  singole    &#13;
 fattispecie  in  tema  di  tutela dei boschi, disponendo così in una    &#13;
 materia ("agricoltura e foreste")  di  competenza  legislativa  delle    &#13;
 regioni.  Nel caso in esame, la Regione Liguria aveva dettato, con la    &#13;
 legge  16  aprile  1984,  n.  22  (legge  forestale  regionale),  una    &#13;
 disciplina organica della materia.                                       &#13;
    2. - La questione non è fondata.                                     &#13;
    Le  norme  impugnate,  racchiuse nell'art. 1 della legge n.424 del    &#13;
 1984, dispongono:                                                        &#13;
       a)  al  comma primo che le sanzioni amministrative previste per    &#13;
 le infrazioni richiamate nell'art. 10 della legge 1° marzo  1975,  n.    &#13;
 47  (Norme  integrative  per  la difesa dei boschi dagli incendi), ad    &#13;
 eccezione  delle   sanzioni   amministrative   previste   da   alcune    &#13;
 disposizioni  del  R.D.L.30  dicembre  1923, n. 3267 (Riordinamento e    &#13;
 riforma della legislazione in materia di boschi e  terreni  montani),    &#13;
 sono  ulteriormente  raddoppiate,  "dopo aver considerato gli aumenti    &#13;
 previsti dalla legge 24 novembre 1981, n.  689" (Modifiche al sistema    &#13;
 penale).                                                                 &#13;
       b)  al  comma  secondo, che le sanzioni amministrative previste    &#13;
 per le infrazioni richiamate nell'art. 11 della legge n. 47 del 1975,    &#13;
 sono quintuplicate.                                                      &#13;
    Le infrazioni previste dall'art. 10 della suddetta legge n. 47 del    &#13;
 1975, aventi  natura  di  illecito  penale  ("...costituiscono  reato    &#13;
 contravvenzionale...  e  sono  punite  con  l'ammenda"),  erano state    &#13;
 depenalizzate per effetto dell'art. 32 della legge n.  689  del  1981    &#13;
 (quelle  previste  dall'art.  11  della  legge n. 47 del 1975 avevano    &#13;
 già, secondo la legge, natura di illecito amministrativo).              &#13;
    Da  quanto finora esposto appar chiaro che la normativa denunciata    &#13;
 si limita  ad  aumentare  le  sanzioni  per  illeciti  amministrativi    &#13;
 previsti dalla legislazione statale espressamente richiamata. Perciò    &#13;
 stesso tale normativa mostra di non potersi applicare all'ipotesi che    &#13;
 la  disciplina statale sia stata derogata da normative regionali, con    &#13;
 le quali, nell'esercizio della competenza in tema  di  agricoltura  -    &#13;
 radicatasi  per  effetto  dei  trasferimenti delle funzioni (cfr., in    &#13;
 materia di agricoltura, il d.P.R. 15 gennaio 1972, n.  11,  e  quindi    &#13;
 gli  artt.  66  e segg. del d.P.R. 24 luglio 1977, n. 616), ed estesa    &#13;
 alle  sanzioni  amministrative  a  seguito   della   depenalizzazione    &#13;
 disposta  dalla  legge n. 689 del 1981 - le regioni abbiano, come nel    &#13;
 caso della Liguria, dettato una disciplina organica.                     &#13;
    Se così è, la regione non ha motivo di dolersi di una disciplina    &#13;
 che non si applica nel suo territorio, salvo che per le  disposizioni    &#13;
 di  princìpio  da essa desumibili, dalle quali, peraltro, secondo la    &#13;
 difesa della Presidenza del Consiglio, non si sarebbe  discostata  la    &#13;
 legge  forestale  regionale  n.  22  del  1984, la quale anzi avrebbe    &#13;
 apprestato un sistema sanzionatorio di efficacia non minore di quella    &#13;
 indicata dal legislatore statale con le norme denunciate.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 1 della legge 4 agosto 1984, n.  424  ("Inasprimento  delle    &#13;
 sanzioni  amministrative  a  carico  dei  trasgressori delle norme in    &#13;
 materia di difesa dei boschi dagli incendi"), in riferimento all'art.    &#13;
 117  Cost. in relazione agli artt. 66 e 69 del d.P.R. 24 luglio 1977,    &#13;
 n. 616, sollevata dalla Regione Liguria con il  ricorso  indicato  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 giugno 1988.                               &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CORASANITI                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 30 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
