<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>61</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:61</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/03/1986</data_decisione>
    <data_deposito>24/03/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO Dott. FRANCESCO GRECO - Prof. RENATO &#13;
 DELL'ANDRO - Prof. GABRIELE PESCATORE - Avv. UGO SPAGNOLI - Prof. &#13;
 FRANCESCO PAOLO CASAVOLA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi riuniti di legittimità costituzionale degli artt. 25,  &#13;
 26, 28 e 30 l. 3 maggio 1982 n. 203 (conversione dei  contratti  agrari  &#13;
 di  mezzadria  in  affitto)  promossi  con undici ordinanze emesse il 7  &#13;
 novembre e il 19 dicembre 1983 dal Tribunale di Modena nei procedimenti  &#13;
 civili vertenti tra  Caragnani  Paolo  e  Lanzarini  Franco,  Marchetti  &#13;
 Renato  e  Golinelli  Alfredo,  Marchetti  Renato  e  Barbieri Augusto,  &#13;
 Baraldi Anna Lucia e Tassinari Renzo, Freschi Giovanni e Soli  Paolino,  &#13;
 Amici   Germinia  e  Parenti  Nerio,  Badiali  Francesco  e  Cassanelli  &#13;
 Vittorino, Reggianini Ada e Del Carlo Gino, Colombini  Mario  e  Trenti  &#13;
 Marino,   Bernardi  Giuseppe  e  Rossi  Lorenzo,  Morselli  Vittorio  e  &#13;
 Bellentani Valentino, iscritte  ai  nn.  da  718  a  728  del  registro  &#13;
 ordinanze  1985  e  pubblicate  tutte  nella  Gazzetta  Ufficiale della  &#13;
 Repubblica n. 306 bis dell'anno 1985:                                    &#13;
     udito nella camera  di  consiglio  del  5  marzo  1986  il  Giudice  &#13;
 relatore Francesco Saja.                                                 &#13;
     Ritenuto  che  nel  corso  di  giudizi  civili aventi ad oggetto la  &#13;
 conversione del contratto di mezzadria in affitto,  disciplinata  dagli  &#13;
 artt. 25 e segg. l. 3 maggio 1982 n. 203, il Tribunale di Modena con le  &#13;
 ordinanze  indicate  in  epigrafe  sollevava  questione di legittimità  &#13;
 costituzionale delle norme disciplinanti il detto istituto;              &#13;
     che precisamente il Tribunale  impugnava  gli  artt.  25  e  30  ed  &#13;
 indicava  anche,  genericamente  e  solo per corroborare la denuncia di  &#13;
 incostituzionalità, gli artt. 26 e 28 l. cit.;                          &#13;
     che esso faceva riferimento alle seguenti norme della Costituzione:  &#13;
     - art. 3, in quanto la conversione è rimessa al mero arbitrio  dei  &#13;
 concessionari, che vengono Così posti in una ingiustificata situazione  &#13;
 di privilegio di fronte ai concedenti;                                   &#13;
     - ancora art. 3, in quanto la conversione lascia in vita le imprese  &#13;
 mezzadrili  più  deboli,  chiamate  dall'art.  31  l.  cit.    "unità  &#13;
 produttive insufficienti", e  sacrifica  quelle  più  prospere,  Così  &#13;
 palesando una intrinseca irragionevolezza;                               &#13;
     -  art.  4,  in  quanto  l'iniziativa  del  mezzadro  può porre il  &#13;
 concedente - imprenditore nella necessità di  abbandonare  la  propria  &#13;
 attività professionale;                                                 &#13;
     -  art.  41,  in  quanto  la  conversione  annulla  la  libertà di  &#13;
 iniziativa economica del concedente, intesa come libertà di  destinare  &#13;
 un capitale a fini produttivi;                                           &#13;
     - ancora art. 41, in quanto l'affidamento della conversione al mero  &#13;
 arbitrio  del  mezzadro contrasta con la riserva di legge in materia di  &#13;
 valutazione dell'"utilità sociale", quale  condizione  necessaria  del  &#13;
 sacrificio dell'iniziativa economica privata;                            &#13;
     -  artt.  42  e  44,  in  quanto  il  concedente è sostanzialmente  &#13;
 espropriato  del  suo  fondo,  ossia  della  propria   azienda,   senza  &#13;
 indennizzo e senza che l'espropriazione possa servire a realizzare più  &#13;
 equi rapporti sociali.                                                   &#13;
     Considerato   che  tutti  i  giudizi  vanno  riuniti  per  la  loro  &#13;
 identità;                                                               &#13;
     che le questioni sono state già decise con sentenza 7 maggio  1984  &#13;
 n. 138;                                                                  &#13;
     che  in  essa  la Corte ha premesso come il legislatore, attraverso  &#13;
 l'istituto della  trasformazione  della  mezzadria  in  affitto,  abbia  &#13;
 confermato  il  disfavore,  già  espresso  nella precedente normativa,  &#13;
 verso il primo tipo di contratto agrario ed abbia perseguito la duplice  &#13;
 finalità di incrementare  la  produzione  ed  evitare  la  persistente  &#13;
 conflittualità tra le parti del rapporto, considerando con particolare  &#13;
 favore   la   posizione   del   mezzadro,  il  quale  all'attività  di  &#13;
 condirezione dell'impresa unisce il lavoro manuale e perciò ha un più  &#13;
 intenso e diretto vincolo con il fondo;                                  &#13;
     che l'istituto della cosiddetta conversione  -  come  la  Corte  ha  &#13;
 precisato  -  consiste  in  effetti  in  un  mutamento più formale che  &#13;
 sostanziale del tipo  di  contratto,  da  associativo  in  commutativo,  &#13;
 giacché  il  legislatore  si  è  limitato  a  prendere atto che nella  &#13;
 generalità dei casi la collaborazione tra concedente  e  mezzadro  era  &#13;
 solo  apparente, essendo l'impresa mezzadrile gestita solo dal secondo,  &#13;
 mentre il primo non era che un puro percettore di reddito;               &#13;
     che le giustificazioni sopra dette, e in particolare  l'assenteismo  &#13;
 del  concedente  -  riscontrabile,  secondo l'espressione contenuta nei  &#13;
 lavori preparatori della legge, nella grande  maggioranza  dei  casi  -  &#13;
 escludono in linea di principio la violazione degli artt. 3 e 41 Cost.;  &#13;
     che  la  non  ravvisabilità  di  una  fattispecie espropriativa ha  &#13;
 indotto la Corte ad escludere altresl' la violazione degli artt.  42  e  &#13;
 44 Cost.;                                                                &#13;
     che,  tuttavia,  poiché l'assenteismo del concedente non è sempre  &#13;
 riscontrabile, la Corte ha ritenuto che l'art. 25 l.  cit.    contrasti  &#13;
 con gli artt. 41 e 44 Cost. nella parte in cui prevede che, nel caso di  &#13;
 concedente imprenditore a titolo principale ai sensi dell'art. 12 l. n.  &#13;
 153  del  1975  o  che  comunque  abbia  dato  un adeguato apporto alla  &#13;
 condirezione dell'impresa di cui ai contratti associativi previsti  nel  &#13;
 primo  comma  dello stesso art. 25, la conversione abbia luogo senza il  &#13;
 consenso del concedente stesso;                                          &#13;
     che  in  conseguenza  la  Corte  ha   dichiarato   l'illegittimità  &#13;
 costituzionale,  oltre che, parzialmente, dell'art. 25, anche dell'art.  &#13;
 30 l. cit.;                                                              &#13;
     che anche la censura relativa all'art. 4 Cost. ha  formato  oggetto  &#13;
 della sent. n. 138 del 1984;                                             &#13;
     che  in  conclusione,  le questioni, in quanto già decise, debbono  &#13;
 essere dichiarate manifestamente infondate.                              &#13;
     Visti  gli  artt.  26  l.  11  marzo  1953  n.  87  e 9 delle Norme  &#13;
 integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riuniti i giudizi,                                                   &#13;
     dichiara manifestamente non fondate le  questioni  di  legittimità  &#13;
 costituzionale  degli  artt.  25,  26,  28, 30 l. 3 maggio 1982 n. 203,  &#13;
 sollevate in riferimento agli artt.  3,  4,  41,  42  e  44  Cost.  dal  &#13;
 Tribunale  di  Modena  con le ordinanze indicate in epigrafe, in quanto  &#13;
 già decise con la sent. n. 138 del 1984.                                &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 18 marzo 1986.          &#13;
                                   F.to:   LIVIO   PALADIN   -  VIRGILIO  &#13;
                                   ANDRIOLI   -   GIUSEPPE   FERRARI   -  &#13;
                                   FRANCESCO  SAJA  -  GIOVANNI  CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   -   RENATO   DELL'ANDRO   -  GABRIELE  &#13;
                                   PESCATORE - UGO SPAGNOLI -  FRANCESCO  &#13;
                                   PAOLO CASAVOLA.                        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
