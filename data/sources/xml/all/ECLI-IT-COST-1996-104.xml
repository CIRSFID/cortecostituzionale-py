<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>104</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:104</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/03/1996</data_decisione>
    <data_deposito>04/04/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nei   giudizi  di  legittimità  costituzionale  dell'art.  1,  comma    &#13;
 secondo, della legge 9 gennaio 1963, n. 9 (Elevazione dei trattamenti    &#13;
 minimi  di  pensione  e  riordinamento  delle  norme  in  materia  di    &#13;
 previdenza dei coltivatori diretti e dei coloni e mezzadri), promossi    &#13;
 con ordinanze emesse:                                                    &#13;
     1)  il  21  febbraio  1995  dal pretore di Udine nel procedimento    &#13;
 civile vertente tra Anna Maria Petrucci e I.N.P.S.,  iscritta  al  n.    &#13;
 220 del registro ordinanze 1995 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 17, prima serie speciale, dell'anno 1995;            &#13;
     2)  il  5  maggio  1995 dal pretore di La Spezia nei procedimenti    &#13;
 civili riuniti vertenti tra Principe Ravecca e I.N.P.S., iscritta  al    &#13;
 n.  403  del  registro  ordinanze  1995  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 27,  prima  serie  speciale,  dell'anno    &#13;
 1995;                                                                    &#13;
   Visti gli atti di costituzione dell'I.N.P.S.;                          &#13;
   Udito  nella  udienza  pubblica  del  20  febbraio  1996 il Giudice    &#13;
 relatore Massimo Vari.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ordinanza emessa il 21 febbraio  1995  (r.o.  n.  220  del    &#13;
 1995)  il  pretore  di  Udine,  in funzione di giudice del lavoro, ha    &#13;
 sollevato  questione  incidentale  di   legittimità   costituzionale    &#13;
 dell'art.    1,  secondo  comma,  della  legge  9  gennaio 1963, n. 9    &#13;
 (Elevazione dei trattamenti minimi di pensione e riordinamento  delle    &#13;
 norme in materia di previdenza dei coltivatori diretti e dei coloni e    &#13;
 mezzadri), in riferimento all'art. 3 della Costituzione.                 &#13;
   Premesso  che il giudizio a quo si è instaurato con ricorso contro    &#13;
 l'Istituto nazionale della  previdenza  sociale  (I.N.P.S.)  di  Anna    &#13;
 Maria   Petrucci,   titolare  dal  1  gennaio  1983  di  pensione  di    &#13;
 riversibilità dello Stato, nonché, dalla stessa data,  di  pensione    &#13;
 di   riversibilità   I.N.P.S.  (SR)  non  integrata  al  minimo,  il    &#13;
 rimettente osserva che la fattispecie in esame  "non  risulta  essere    &#13;
 stata  esaminata dalla Corte costituzionale in altre sentenze su casi    &#13;
 analoghi e neppure nella sentenza n. 69 del 22 febbraio 1990", che ha    &#13;
 dichiarato l'illegittimità  della  stessa  norma  denunciata  "nella    &#13;
 parte  in cui escludeva il diritto all'integrazione della pensione SR    &#13;
 a carico dell'I.N.P.S. per chi era già titolare di pensione  diretta    &#13;
 dello Stato".                                                            &#13;
   Sussistendo,  nella  specie, a giudizio del rimettente, le medesime    &#13;
 ragioni di contrasto con l'art.  3  della  Costituzione  poste  dalla    &#13;
 Corte  costituzionale  a fondamento di precedenti pronunce, si reputa    &#13;
 rilevante e non manifestamente infondata la questione proposta.          &#13;
   2.   -   Costituendosi  in  giudizio,  l'Istituto  nazionale  della    &#13;
 previdenza sociale si è rimesso alla decisione della  Corte,  attesa    &#13;
 la declaratoria di illegittimità della norma denunciata, di cui alla    &#13;
 sentenza  n.    69  del 1990, nonché la copiosa giurisprudenza della    &#13;
 stessa  Corte  che  "ha  perseguito  l'intento  di   eliminare   ogni    &#13;
 preclusione  all'integrazione al trattamento minimo per i titolari di    &#13;
 più pensioni", rendendo così  possibile  "la  titolarità  di  più    &#13;
 integrazioni" fino all'entrata in vigore del decreto-legge n. 463 del    &#13;
 1983, convertito, con modificazioni, nella legge n. 638 del 1983.        &#13;
   3.  -  Con ordinanza emessa il 5 maggio 1995 (r.o. n. 403 del 1995)    &#13;
 il pretore di La Spezia,  in  funzione  di  giudice  del  lavoro,  ha    &#13;
 sollevato,  in  riferimento  agli  artt.  3  e 38 della Costituzione,    &#13;
 questione incidentale di  legittimità  costituzionale  del  medesimo    &#13;
 art.  1, secondo comma, della legge 9 gennaio 1963, n. 9 "nella parte    &#13;
 in cui non consente l'integrazione al minimo legale della pensione di    &#13;
 riversibilità erogata dall'I.N.P.S.  -  Fondo  speciale  coltivatori    &#13;
 diretti, coloni e mezzadri - in caso di cumulo con pensione diretta a    &#13;
 carico del Fondo speciale addetti ai pubblici servizi di trasporto".     &#13;
   Rilevato  che  la  fattispecie di causa presenta caratteri omogenei    &#13;
 con  quelli  esaminati  in  precedenti  decisioni  della  Corte,   il    &#13;
 rimettente  reputa la questione non manifestamente infondata, nonché    &#13;
 rilevante, atteso  che  la  rimozione  del  divieto  di  integrazione    &#13;
 determinerebbe   l'accoglibilità  della  domanda,  "quanto  meno  in    &#13;
 relazione alle differenze sui  ratei  maturate  sino  all'entrata  in    &#13;
 vigore"  del decreto-legge n. 463 del 1983, convertito nella legge n.    &#13;
 638 del 1983.                                                            &#13;
   4.  -  Costituendosi  in  giudizio,  l'Istituto   nazionale   della    &#13;
 previdenza sociale si è rimesso alla decisione della Corte.<diritto>Considerato in diritto</diritto>1.  -  I  giudici rimettenti prospettano l'illegittimità dell'art.    &#13;
 1, secondo comma, della legge 9 gennaio 1963, n.  9  (Elevazione  dei    &#13;
 trattamenti minimi di pensione e riordinamento delle norme in materia    &#13;
 di  previdenza  dei coltivatori diretti e dei coloni e mezzadri), con    &#13;
 riguardo a due distinte ipotesi di preclusione  dell'integrazione  al    &#13;
 minimo  della  pensione  di  riversibilità  a  carico  del Fondo per    &#13;
 coltivatori  diretti,  mezzadri  e  coloni   in   caso   di   cumulo,    &#13;
 rispettivamente,  con  pensione  di  riversibilità dello Stato e con    &#13;
 pensione diretta a carico del  Fondo  speciale  addetti  ai  pubblici    &#13;
 servizi di trasporto.                                                    &#13;
   2. - Le questioni, che possono essere trattate congiuntamente, sono    &#13;
 entrambe fondate.                                                        &#13;
   La  norma  denunciata  è  stata  già più volte dichiarata, sotto    &#13;
 altri profili, illegittima in applicazione del principio che  esclude    &#13;
 -   sino   alla   data   del   1  ottobre  1983  -  ogni  preclusione    &#13;
 dell'integrazione  al  minimo  in  caso  di   titolarità   di   più    &#13;
 trattamenti  (cfr.  sentenze nn. 547, 70, 69 del 1990, 1144 e 184 del    &#13;
 1988 e 102 del 1982), anche con riguardo alla  medesima  pensione  di    &#13;
 riversibilità a carico del Fondo per coltivatori diretti, mezzadri e    &#13;
 coloni (da ultimo con le sentenze nn. 438 e 165 del 1992).               &#13;
   Sussistendo identiche ragioni, anche nei casi in esame va eliminata    &#13;
 la residua area di operatività della norma.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi:                                                     &#13;
     a)  dichiara l'illegittimità costituzionale dell'art. 1, secondo    &#13;
 comma, della legge 9 gennaio 1963, n. 9 (Elevazione  dei  trattamenti    &#13;
 minimi  di  pensione  e  riordinamento  delle  norme  in  materia  di    &#13;
 previdenza dei coltivatori diretti e dei coloni  e  mezzadri),  nella    &#13;
 parte  in  cui  preclude  l'integrazione  al minimo della pensione di    &#13;
 riversibilità a carico del Fondo per coltivatori diretti, mezzadri e    &#13;
 coloni in caso di cumulo con pensione di riversibilità dello Stato;     &#13;
     b) dichiara l'illegittimità costituzionale della norma  suddetta    &#13;
 nella  parte  in  cui  preclude l'integrazione al minimo del medesimo    &#13;
 trattamento di riversibilità in caso di cumulo con pensione  diretta    &#13;
 erogata dal Fondo speciale addetti ai pubblici servizi di trasporto.     &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 marzo 1996.                                &#13;
                         Il Presidente:  Ferri                            &#13;
                          Il redattore:  Vari                             &#13;
                       Il cancelliere:  Di Paola                          &#13;
   Depositata in cancelleria il 4 aprile 1996.                            &#13;
               Il direttore della cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
