<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>47</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:47</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1997</data_decisione>
    <data_deposito>20/02/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale  del  combinato  disposto    &#13;
 degli  artt.  8, secondo e terzo comma, della legge 15 dicembre 1972,    &#13;
 n. 772 (Norme per il riconoscimento dell'obiezione  di  coscienza)  e    &#13;
 163 e seguenti del codice penale, promosso con ordinanza emessa il 19    &#13;
 giugno  1995  dal  giudice  per  le  indagini  preliminari  presso il    &#13;
 tribunale militare di Roma nel procedimento penale a carico di  Rombi    &#13;
 Gennaro,  iscritta al n. 529 del registro ordinanze 1995 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  39,  prima   serie    &#13;
 speciale, dell'anno 1995;                                                &#13;
   Visto  l'atto  di  costituzione  di Rombi Gennaro nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
   Udito  nell'udienza  pubblica  del  10  dicembre  1996  il  giudice    &#13;
 relatore Gustavo Zagrebelsky;                                            &#13;
   Uditi  gli  avvocati  Mauro  Mellini  e Roberto Lorenzini per Rombi    &#13;
 Gennaro e l'Avvocato dello Stato Stefano Onufrio  per  il  Presidente    &#13;
 del Consiglio dei Ministri;                                              &#13;
   Ritenuto che, con ordinanza del 19 giugno 1995 (r.o. 529 del 1995),    &#13;
 il  giudice  per le indagini preliminari presso il tribunale militare    &#13;
 di Roma ha sollevato questione di legittimità  costituzionale  della    &#13;
 norma  risultante  dal  combinato  disposto  degli artt. 8, secondo e    &#13;
 terzo comma, della legge 15 dicembre  1972,  n.  772  (Norme  per  il    &#13;
 riconoscimento  dell'obiezione  di  coscienza)  e  163 e seguenti del    &#13;
 codice penale, con riferimento all'art. 3 della Costituzione  nonché    &#13;
 al  principio  della finalità rieducativa della pena, nella parte in    &#13;
 cui prevede  "che,  a  fronte  della  concessione  di  ufficio  della    &#13;
 sospensione  condizionale  della  pena  nel primo giudizio, l'esonero    &#13;
 (dalla prestazione del servizio militare di leva)  consegua  soltanto    &#13;
 all'espiazione della pena inflitta per il secondo reato";                &#13;
     che  la  questione  è  stata  sollevata nel corso di un processo    &#13;
 penale a carico di persona precedentemente condannata  alla  pena  di    &#13;
 cinque   mesi   e   dieci   giorni  di  reclusione,  con  sospensione    &#13;
 condizionale della stessa, per  il  reato  di  "rifiuto"  totale  del    &#13;
 servizio  militare  di  leva per motivi di coscienza (art. 8, secondo    &#13;
 comma, della legge n. 772 del 1972), persona  imputata  dello  stesso    &#13;
 delitto  in  relazione  a  una  nuova  chiamata  alle armi, anch'essa    &#13;
 disattesa per i medesimi motivi, dopo il passaggio in giudicato della    &#13;
 prima sentenza ma  prima  del  decorso  del  periodo  di  sospensione    &#13;
 condizionale;                                                            &#13;
     che   il   giudice  rimettente  -  respinte  come  manifestamente    &#13;
 infondate  diverse  e  più  ampie  questioni  di  costituzionalità,    &#13;
 prospettate  dalla  difesa  dell'imputato - ha investito questa Corte    &#13;
 della questione di  legittimità  costituzionale,  esclusivamente  in    &#13;
 relazione  all'ipotesi di nuovo processo per il reato di cui all'art.    &#13;
 8, secondo comma, della legge n. 772 del 1972, nel caso di precedente    &#13;
 condanna per il medesimo reato,  con  pena  condizionalmente  sospesa    &#13;
 senza che vi sia stata a tal fine richiesta dell'imputato;               &#13;
     che  in relazione alla specifica eventualità da ultimo indicata,    &#13;
 si sostiene nell'ordinanza che - qualora la persona  che  rifiuta  il    &#13;
 servizio  militare  perseveri  nel suo atteggiamento - la sospensione    &#13;
 condizionale  si  traduce  in  un  danno  per  il   condannato,   non    &#13;
 riconducibile  alla  sua  condotta:  danno  consistente  in ciò, che    &#13;
 l'esonero dalla prestazione del servizio militare (previsto dall'art.    &#13;
 8, terzo  comma,  della  legge  n.  772  del  1972  come  conseguenza    &#13;
 dell'espiazione  della  pena inflitta con la sentenza di condanna per    &#13;
 il rifiuto del servizio militare), a causa  della  sospensione  della    &#13;
 pena, non potrebbe operare in conseguenza della condanna per il primo    &#13;
 rifiuto,  ma  solo in seguito a una seconda sentenza di condanna che,    &#13;
 irrogando una nuova pena, disponesse per conseguenza anche la  revoca    &#13;
 del beneficio precedentemente concesso;                                  &#13;
     che,  in tal modo, l'effetto dell'esonero, previsto dall'art.  8,    &#13;
 terzo comma, per evitare la "spirale delle  condanne",  conseguirebbe    &#13;
 contraddittoriamente  dopo  più  sentenze  di  condanna aventi, come    &#13;
 effetto, il cumulo delle pene;                                           &#13;
     che tale disciplina dell'esonero dalla prestazione  del  servizio    &#13;
 militare  di  leva,  in  relazione alla specifica ipotesi dedotta, è    &#13;
 censurata dal giudice a quo  per  violazione  a)  dell'art.  3  della    &#13;
 Costituzione,  sotto  il profilo della ragionevolezza, in quanto essa    &#13;
 presupporrebbe, ai fini  dell'esonero,  un'ulteriore  condanna  e  un    &#13;
 ulteriore  prolungamento  della  pena  -  rispetto  al caso in cui il    &#13;
 beneficio  della  sospensione  condizionale  non  fosse  concesso  -,    &#13;
 nonché  b)  degli  artt. 3 e 27, terzo comma, della Costituzione, in    &#13;
 quanto il prolungamento della pena necessario  ai  fini  dell'esonero    &#13;
 sarebbe  ingiustificato  anche  dal  punto  di  vista della finalità    &#13;
 rieducativa;                                                             &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dell'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che la  questione  sia  dichiarata  inammissibile  o    &#13;
 infondata;                                                               &#13;
     che si è costituita in giudizio la parte privata, Gennaro Rombi,    &#13;
 il   cui   patrocinio,   nell'atto   di   costituzione,   sviluppando    &#13;
 argomentazioni già svolte nel giudizio principale - ma non  recepite    &#13;
 nella  prospettazione  dell'ordinanza  di  rimessione - ha variamente    &#13;
 indicato possibilità interpretative della normativa in  vigore  tali    &#13;
 da  condurre  all'impossibilità  di una ripetizione di processi e di    &#13;
 condanne per lo stesso titolo  di  reato  in  argomento,  concludendo    &#13;
 pertanto,  in  via principale, per l'irrilevanza della questione - in    &#13;
 quanto già risolta, nel senso detto, nell'affermata  irripetibilità    &#13;
 del  reato  -  e,  solo in via subordinata, per l'incostituzionalità    &#13;
 della   disciplina   impugnata   in   quanto   consente   l'accennata    &#13;
 ripetizione;                                                             &#13;
     che  nel  corso del riferito giudizio di costituzionalità questa    &#13;
 Corte, con ordinanza n. 183 del 27 maggio 1996 (r.o. 614  del  1996),    &#13;
 ha  disposto  la  trattazione  innanzi a se stessa della questione di    &#13;
 legittimità costituzionale dell'art. 8, secondo e terzo comma, della    &#13;
 legge  n.  772  del  1972,  nella  parte  in cui consente la ripetuta    &#13;
 sottoponibilità a procedimento penale del  medesimo  soggetto,  già    &#13;
 condannato  per  il  reato  di  rifiuto  del  servizio  militare  ivi    &#13;
 previsto, e che persista nel rifiuto, in riferimento agli artt. 2, 3,    &#13;
 19 e 21 della Costituzione;                                              &#13;
     che all'udienza pubblica del 10 dicembre 1996 l'Avvocatura  dello    &#13;
 Stato  e  il  patrocinio  della  parte  privata hanno insistito nelle    &#13;
 rispettive conclusioni;                                                  &#13;
   Considerato   che   questa   Corte,   decidendo   sulla   questione    &#13;
 pregiudiziale  sollevata dinanzi a se stessa, ricordata in narrativa,    &#13;
 con sentenza n. 43 del 1997, depositata in pari data,  ha  dichiarato    &#13;
 l'illegittimità  costituzionale  dell'art. 8, secondo e terzo comma,    &#13;
 della citata legge n. 772 del 1972, nella parte in cui non esclude la    &#13;
 possibilità di più di una condanna per il reato di chi, al di fuori    &#13;
 dei casi di ammissione ai benefici  previsti  dalla  suddetta  legge,    &#13;
 rifiuta in tempo di pace, prima di assumerlo, il servizio militare di    &#13;
 leva, adducendo i motivi di cui all'art. 1 della medesima legge;         &#13;
     che   con  la  pronuncia  di  illegittimità  costituzionale  ora    &#13;
 richiamata è stata espunta in radice dall'ordinamento la  norma  che    &#13;
 rende  possibile  irrogare  ulteriori condanne e pene per un fatto di    &#13;
 reato di rifiuto del servizio militare previsto dall'art. 8,  secondo    &#13;
 comma,  della  legge  n. 772 del 1972, quando, per un motivo previsto    &#13;
 dall'ordinamento,  l'espiazione  totale   o   parziale   della   pena    &#13;
 precedentemente  inflitta  per il medesimo reato di rifiuto non abbia    &#13;
 avuto luogo;                                                             &#13;
     che  l'anzidetta  declaratoria  di  incostituzionalità,  facendo    &#13;
 venir  meno  la  previsione  denunciata  di  incostituzionalità  dal    &#13;
 giudice  penale  militare,  rende  manifestamente  inammissibile   la    &#13;
 questione sollevata dallo stesso giudice (v., da ultimo, ordinanza n.    &#13;
 142 del 1996);                                                           &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  del  combinato  disposto degli artt. 8,    &#13;
 secondo e terzo comma, della legge 15 dicembre 1972,  n.  772  (Norme    &#13;
 per  il  riconoscimento dell'obiezione di coscienza) e 163 e seguenti    &#13;
 del codice penale, sollevata, in riferimento agli artt. 3 e 27, terzo    &#13;
 comma, della Costituzione, dal giudice per  le  indagini  preliminari    &#13;
 presso  il  tribunale  militare  di Roma, con l'ordinanza indicata in    &#13;
 epigrafe.                                                                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 10 febbraio 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 20 febbraio 1997.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
