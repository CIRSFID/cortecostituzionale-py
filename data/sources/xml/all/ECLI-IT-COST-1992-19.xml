<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:19</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1992</data_decisione>
    <data_deposito>24/01/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 3 della legge    &#13;
 23 ottobre 1985, n. 595 (Norme per la programmazione sanitaria e  per    &#13;
 il  piano sanitario triennale 1986-88), promosso con ordinanza emessa    &#13;
 il 9 gennaio 1991 dal  Pretore  di  Milano  nel  procedimento  civile    &#13;
 vertente  tra  Marta Cappellini e la U.S.L. n. 74 di Milano, iscritta    &#13;
 al n. 492 del registro ordinanze 1991  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  33, prima serie speciale, dell'anno    &#13;
 1991;                                                                    &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 4 dicembre 1991 il Giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
    Ritenuto che nel corso di un giudizio  civile  promosso  da  Marta    &#13;
 Cappellini  contro  la  U.S.L. n. 74 di Milano al fine di ottenere il    &#13;
 rimborso della spesa sostenuta per un trattamento sanitario  ricevuto    &#13;
 da  una  struttura  privata  non convenzionata - trattamento ritenuto    &#13;
 necessario  dal  medico  curante,  ma  non  suscettibile  di   essere    &#13;
 prestato,  da  alcuna struttura pubblica o privata convenzionata - il    &#13;
 Pretore di Milano, con ordinanza in data 9 gennaio 1991, ha sollevato    &#13;
 questione di legittimità costituzionale, in  relazione  all'art.  32    &#13;
 della  Costituzione,  dell'art. 3 della legge 23 ottobre 1985, n. 595    &#13;
 (Norme per la programmazione  sanitaria  e  per  il  piano  sanitario    &#13;
 triennale 1986-88) "nella parte in cui omette di vincolare le Regioni    &#13;
 a  fornire positivamente le prestazioni terapeutiche, ove necessarie,    &#13;
 anche in forma di rimborso  qualora  esse  non  siano,  o  non  siano    &#13;
 ancora, erogabili in forma diretta o convenzionata";                     &#13;
      che  nell'ordinanza in questione viene richiamata la sentenza di    &#13;
 questa Corte n. 992 del 1988, al fine di richiedere l'estensione  del    &#13;
 principio  affermato  con  tale sentenza - relativo al rimborso delle    &#13;
 spese sostenute per prestazioni di diagnostica specialistica ad  alto    &#13;
 costo  eseguite  presso strutture private non convenzionate dotate in    &#13;
 esclusiva delle apparecchiature necessarie - anche al rimborso  delle    &#13;
 spese   sostenute  presso  strutture  private  non  convenzionate  in    &#13;
 relazione a prestazioni terapeutiche indispensabili, ma non erogabili    &#13;
 da parte di strutture pubbliche o private convenzionate;                 &#13;
      che nel giudizio si è costituito il  Presidente  del  Consiglio    &#13;
 dei ministri, rappresentato dall'Avvocatura generale dello Stato, per    &#13;
 chiedere  che  venga  dichiarata  l'inammissibilità o l'infondatezza    &#13;
 della questione proposta;                                                &#13;
      che ad avviso  della  difesa  statale  l'inammissibilità  della    &#13;
 questione   dovrebbe,   in  particolare,  discendere  dal  fatto  che    &#13;
 nell'ordinanza  relativa  si  avanza  alla  Corte  costituzionale  la    &#13;
 richiesta  di  una  sentenza  "addittiva"  pur in assenza "di uno dei    &#13;
 presupposti necessari perché una pronuncia addittiva possa aversi, e    &#13;
 cioè  per  mancanza  della  unicità  della  soluzione raggiungibile    &#13;
 mediante  lo   strumento   del   giudizio   costituzionale",   mentre    &#13;
 l'infondatezza   della  stessa  questione  dovrebbe  in  primo  luogo    &#13;
 derivare dal fatto che "la domanda di prestazioni  sanitarie  per  le    &#13;
 sue  dimensioni,  per  la  sua  variabilità e per la sempre maggiore    &#13;
 onerosità delle nuove  tecnologie  non  può  oggettivamente  essere    &#13;
 integralmente   soddisfatta   dai   servizi  sanitari  offerti  dalla    &#13;
 collettività organizzata", non risultando, d'altra parte,  garantito    &#13;
 dall'art.  32  della Costituzione un diritto soggettivo a prestazioni    &#13;
 sanitarie illimitate;                                                    &#13;
    Considerato che la questione in esame difetta di rilevanza ai fini    &#13;
 della decisione del processo a quo, dal momento che detta  questione,    &#13;
 nei  termini  in  cui  viene  prospettata, si collega, comunque, alla    &#13;
 necessità di un  successivo  intervento  normativo  da  parte  della    &#13;
 Regione  diretto  a  disciplinare  le  modalità  per  accedere  alla    &#13;
 prestazione  terapeutica  e  per  ottenere  il  rimborso,  totale   o    &#13;
 parziale, della spesa sostenuta;                                         &#13;
      che  la  previsione di tali modalità, anche nell'ipotesi in cui    &#13;
 venisse introdotto - così come richiesto dall'ordinanza di rinvio  -    &#13;
 un  vincolo  a  provvedere a carico della Regione, non è tale da dar    &#13;
 luogo ad una soluzione univoca in ordine  ai  modi,  ai  tempi,  alle    &#13;
 misure ed ai controlli connessi al rimborso, ma offre la possibilità    &#13;
 di  soluzioni  differenziate, nel cui ambito la scelta deve ritenersi    &#13;
 riservata alla discrezionalità del legislatore regionale;               &#13;
      che,  pertanto,  la   questione   si   presenta   manifestamente    &#13;
 inammissibile  per  difetto  di  rilevanza  ed  in  quanto  diretta a    &#13;
 ottenere una sentenza  di  tipo  addittivo  pur  in  assenza  di  una    &#13;
 soluzione obbligata;                                                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara manifestamente inammissibile la questione di  legittimità    &#13;
 costituzionale dell'art. 3 della legge 23 ottobre 1985, n. 595 (Norme    &#13;
 per  la  programmazione  sanitaria e per il piano sanitario triennale    &#13;
 1986-88), nella parte in cui omette di vincolare le Regioni a fornire    &#13;
 positivamente le prestazioni terapeutiche, ove necessarie,  anche  in    &#13;
 forma  di  rimborso,  qualora  esse  non  siano,  o non siano ancora,    &#13;
 erogabili in forma diretta o convenzionata, questione sollevata,  con    &#13;
 riferimento all'art. 32 della Costituzione, dal Pretore di Milano con    &#13;
 l'ordinanza di cui in epigrafe.                                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 gennaio 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: CHELI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 24 gennaio 1992.                         &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
