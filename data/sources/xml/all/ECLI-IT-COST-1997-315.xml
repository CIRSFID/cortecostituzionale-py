<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>315</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:315</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1997</data_decisione>
    <data_deposito>22/10/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 34, comma 2,    &#13;
 del codice di procedura penale, promosso con ordinanza emessa  il  10    &#13;
 giugno  1996  dal  giudice  per  le  indagini  preliminari  presso il    &#13;
 tribunale di Catanzaro nel procedimento penale a carico di  Catanzaro    &#13;
 Carmine, iscritta al n. 1268 del registro ordinanze 1996 e pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  47,  prima  serie    &#13;
 speciale, dell'anno 1996;                                                &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  18 giugno 1997 il giudice    &#13;
 relatore Gustavo Zagrebelsky;                                            &#13;
   Ritenuto che con ordinanza del 10 giugno 1996  il  giudice  per  le    &#13;
 indagini  preliminari  presso il tribunale di Catanzaro ha sollevato,    &#13;
 in riferimento agli artt. 3, 24, secondo comma, e 27, secondo  comma,    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art.   34, comma 2, cod. proc. pen.,  nella  parte  in  cui  non    &#13;
 prevede  che  non  possa  partecipare  al giudizio "nella forma della    &#13;
 declaratoria con sentenza dell'estinzione del reato  per  intervenuta    &#13;
 oblazione"  il  giudice  per  le  indagini  preliminari  che abbia in    &#13;
 precedenza provveduto sia in ordine alla convalida del fermo  sia  in    &#13;
 ordine  alla  richiesta di adozione di una misura cautelare personale    &#13;
 da parte del pubblico ministero;                                         &#13;
     che, richiamando le sentenze n. 432 del 1995 e nn. 131 e 155  del    &#13;
 1996  di  questa  Corte,  il giudice rimettente osserva che, come nei    &#13;
 casi in esse affrontati, anche in quello ora prospettato  si  delinea    &#13;
 la  possibile  menomazione  dell'imparzialità  del giudice che, dopo    &#13;
 aver valutato i profili  indiziari  e  cautelari,  è  chiamato  alla    &#13;
 pronuncia  di  estinzione del reato per oblazione; pronuncia che, pur    &#13;
 non  implicando  la  piena  verifica  della  fondatezza  dell'accusa,    &#13;
 involge  pur  sempre  un apprezzamento sulla inapplicabilità di più    &#13;
 favorevoli formule di proscioglimento nel merito, secondo  la  regola    &#13;
 dettata dall'art.  129 cod. proc. pen.;                                  &#13;
     che,   inoltre,   il   giudice  rimettente  prospetta,  in  forma    &#13;
 ipotetica, ulteriori possibili rilievi di  incostituzionalità  della    &#13;
 norma  denunciata  con  riguardo ad altre ipotesi - non riferibili al    &#13;
 giudizio a quo - accomunabili, in  sintesi,  nelle  numerose  diverse    &#13;
 interrelazioni  che  possono  essere  ravvisabili  tra  una funzione,    &#13;
 pregiudicata, di "giudizio" (comprensivo delle varie forme in cui  si    &#13;
 estrinseca:   dibattimento,   giudizio   abbreviato,  patteggiamento,    &#13;
 emissione   del   decreto   penale  di  condanna)  e  una  precedente    &#13;
 valutazione  sul  profilo  della  libertà  personale  (pronunce   de    &#13;
 libertate nella fase predibattimentale, giudizio di riesame o appello    &#13;
 ex  artt.  309  e  310 cod. proc. pen., decisione sulla convalida del    &#13;
 fermo o dell'arresto);                                                   &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, che ha concluso per l'inammissibilità o l'infondatezza  delle    &#13;
 questioni sollevate;                                                     &#13;
   Considerato  che, relativamente alla questione di costituzionalità    &#13;
 posta con riguardo alla dedotta incompatibilità tra  una  precedente    &#13;
 valutazione  in  ordine  a  una  misura  cautelare  personale  e  una    &#13;
 successiva  funzione  di  definizione  del  processo  attraverso   la    &#13;
 declaratoria  di  estinzione  del  reato  per  intervenuta oblazione,    &#13;
 risulta, dall'ordinanza di rinvio, che, a seguito  di  opposizione  a    &#13;
 decreto  penale  di condanna e contestuale richiesta ex art. 162 cod.    &#13;
 pen., il giudice rimettente ha ammesso l'imputato  all'oblazione,  ed    &#13;
 è  stato  "ritualmente"  effettuato il deposito della somma dovuta a    &#13;
 tal fine, cosicché sussistono tutti  i  presupposti  per  l'adozione    &#13;
 della declaratoria di intervenuta estinzione del reato;                  &#13;
     che,   in  tale  quadro,  assume  puntuale  rilievo  l'eccezione,    &#13;
 formulata dall'Avvocatura  dello  Stato,  di  inammissibilità  della    &#13;
 questione  per difetto di rilevanza, poiché, una volta esercitato da    &#13;
 parte del giudice il potere di verifica delle condizioni legali e dei    &#13;
 presupposti di  ammissione  all'istituto,  e  una  volta  intervenuto    &#13;
 altresì  il  versamento dell'importo stabilito dalla legge, l'ambito    &#13;
 valutativo  che  si  assume  potenzialmente  "pregiudicato"  -  dalla    &#13;
 precedente  determinazione  sul  tema  cautelare - è in realtà già    &#13;
 consumato, residuando solo la limitata cognitio dell'adozione di  una    &#13;
 pronuncia dichiarativa dell'estinzione del reato, già verificatasi a    &#13;
 norma  dell'art.  162,  secondo  comma,  cod.  pen.,  per effetto del    &#13;
 pagamento della somma a titolo di oblazione;                             &#13;
     che l'affermazione della rilevanza della questione da  parte  del    &#13;
 giudice  a  quo,  nell'assunto della possibilità di revocare in ogni    &#13;
 tempo l'ordinanza  ammissiva  dell'oblazione,  non  risulta  pertanto    &#13;
 plausibile,  perché  essa  si riferisce all'ipotesi di una pronuncia    &#13;
 giurisprudenziale resa in tema di oblazione "discrezionale"  ex  art.    &#13;
 162-bis  cod. pen. e dunque relativa a un istituto, diverso da quello    &#13;
 di cui è stata fatta  applicazione,  che  prevede  ipotesi  ostative    &#13;
 soggettive  e  oggettive  e  che  inoltre affida al giudice ambiti di    &#13;
 valutazione  ampi  e  non  predeterminati  (come  l'incidenza   della    &#13;
 "gravità  del  fatto");  elementi,  questi,  che  non figurano nella    &#13;
 disciplina dell'oblazione "comune" regolata dall'art. 162 cod.  pen.,    &#13;
 che è quella che viene in rilievo nel giudizio a quo;                   &#13;
     che   neppure  è  ravvisabile  un  ipotetico  e  residuo  ambito    &#13;
 delibativo concernente l'esattezza della qualificazione giuridica del    &#13;
 fatto, stante l'espressa indicazione  dell'ordinanza  di  rinvio  nel    &#13;
 senso   della  globale  legittimità  della  procedura  di  oblazione    &#13;
 esperita;                                                                &#13;
     che pertanto la questione in esame, in quanto concernente profili    &#13;
 che non sono più ricompresi  nella  cognizione  del  giudice  a  quo    &#13;
 risulta  sollevata  in  via  astratta  ed  è  priva  del  necessario    &#13;
 requisito  della  pregiudizialità,  cosicché   essa   deve   essere    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
     che  alla  medesima  conclusione  si deve pervenire relativamente    &#13;
 alle ulteriori questioni indicate in narrativa, sollevate in  termini    &#13;
 ipotetici,  palesemente  estranee alla concreta vicenda processuale e    &#13;
 perciò - del  resto  dichiaratamente  -  manifestamente  irrilevanti    &#13;
 rispetto ad essa;                                                        &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   delle   questioni   di    &#13;
 legittimità  costituzionale  dell'art.  34,  comma  2, del codice di    &#13;
 procedura penale, sollevate, in riferimento agli artt. 3, 24, secondo    &#13;
 comma, e 27, secondo comma, della Costituzione, dal  giudice  per  le    &#13;
 indagini   preliminari   presso   il   tribunale  di  Catanzaro,  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1997.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 ottobre 1997.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
