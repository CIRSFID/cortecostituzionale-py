<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>220</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:220</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/05/1994</data_decisione>
    <data_deposito>08/06/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 34, secondo    &#13;
 comma,  del  regio  decreto  legislativo  31  maggio  1946,  n.   511    &#13;
 (Guarentige  della Magistratura), promosso con ordinanza emessa il 24    &#13;
 settembre 1993 dal Consiglio Superiore della Magistratura  -  Sezione    &#13;
 disciplinare  nel  procedimento  disciplinare  relativo  a Conigliaro    &#13;
 Giovanni, iscritta al n. 781 del registro ordinanze 1993 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 4, prima serie speciale,    &#13;
 dell'anno 1994;                                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 27 aprile 1994 il Giudice    &#13;
 relatore Cesare Ruperto;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un procedimento in cui  l'incolpato,  affermando    &#13;
 di  non  aver  reperito  un  magistrato disposto ad assisterlo, aveva    &#13;
 chiesto alla  Sezione  disciplinare  del  Consiglio  Superiore  della    &#13;
 Magistratura  di  sollevare  questione di legittimità costituzionale    &#13;
 dell'art. 34, secondo comma, del regio decreto legislativo 31  maggio    &#13;
 1946, n. 511 (Guarentigie della Magistratura), nella parte in cui non    &#13;
 consente  la  nomina  di  un  avvocato  quale  difensore,  la Sezione    &#13;
 medesima, con ordinanza  emessa  il  24  settembre  1993,  dopo  aver    &#13;
 respinto  tale  prospettazione,  ha  viceversa sollevato questione di    &#13;
 legittimità  costituzionale  della  norma  citata,  in   riferimento    &#13;
 all'art. 24 della Costituzione, sotto due diversi e distinti profili.    &#13;
    Premette  la  Sezione  disciplinare  che il diritto di difesa può    &#13;
 essere  articolato  in  forme  e  modalità  diverse  e  non  implica    &#13;
 necessariamente  l'apporto  tecnico di un avvocato. Tuttavia la norma    &#13;
 non si sottrarrebbe al dubbio di  legittimità  nella  parte  in  cui    &#13;
 rimette  alla scelta discrezionale dell'incolpato se avvalersi o meno    &#13;
 di un difensore. La stessa facoltatività della difesa  in  sostanza,    &#13;
 soprattutto  quando  sia  lecito  dubitare della piena consapevolezza    &#13;
 dell'incolpato, comporterebbe il denunciato vulnus.                      &#13;
    Alla luce poi dell'interpretazione della norma  impugnata  che  la    &#13;
 stessa   Sezione   remittente   afferma   di  seguire  costantemente,    &#13;
 escludendo che essa consenta di procedere alla nomina di un difensore    &#13;
 d'ufficio - ancorché magistrato - nei casi in cui  l'incolpato  "non    &#13;
 intenda  o  non  possa"  nominare  un  collega,  si  profilerebbe  un    &#13;
 ulteriore aspetto d'illegittimità, concretantesi in tale preclusione    &#13;
 alla nomina d'ufficio.                                                   &#13;
    2. - È intervenuto il  Presidente  del  Consiglio  dei  ministri,    &#13;
 rappresentato  e  difeso dall'Avvocatura dello Stato, che ha rilevato    &#13;
 come la difesa tecnica da parte di un collega -  anche  alla  stregua    &#13;
 delle  affermazioni  della  Suprema  Corte  - non violi il diritto di    &#13;
 difesa nel procedimento disciplinare nei  confronti  del  magistrato.    &#13;
 Altrettanto   potrebbe  sostenersi  per  l'autodifesa  allorché  non    &#13;
 intervenga la  nomina  di  un  altro  magistrato:  tale  possibilità    &#13;
 risulterebbe   coerente   "con  il  principio  di  autogoverno  e  di    &#13;
 autodichia della magistratura e con le peculiarità del  procedimento    &#13;
 disciplinare",    anche    per    la    particolare    qualificazione    &#13;
 dell'incolpato. Comunque la presenza  di  un  difensore  non  sarebbe    &#13;
 sempre  ed  in assoluto costituzionalmente richiesta, dovendosi avere    &#13;
 riguardo alla funzione oggettiva cui la difesa è preordinata  e  non    &#13;
 alla scelta soggettiva dell'interessato.                                 &#13;
    L'Avvocatura  ha  altresì proposto un'interpretazione adeguatrice    &#13;
 della norma, nel senso che essa, per non restare "priva di  contenuto    &#13;
 o  quanto  meno claudicante", dovrebbe comportare il potere, da parte    &#13;
 della Sezione, di nominare un difensore d'ufficio.<diritto>Considerato in diritto</diritto>1.  -  L'art.  34, secondo comma, del regio decreto legislativo 31    &#13;
 maggio 1946, n. 511, è  sospettato  d'illegittimità  costituzionale    &#13;
 dalla Sezione disciplinare del Consiglio Superiore della Magistratura    &#13;
 sotto  due profili: là dove prevede la possibilità per l'incolpato,    &#13;
 di farsi assistere da altro magistrato come una facoltà e  non  già    &#13;
 come  un  obbligo ed in secondo luogo nella parte in cui non consente    &#13;
 la nomina di un magistrato-difensore  di  ufficio  nei  casi  in  cui    &#13;
 l'incolpato "non intenda o non possa nominare un difensore".             &#13;
    Specifica  il  giudice  a  quo che nella specie "l'incolpato si è    &#13;
 presentato al dibattimento privo  di  difensore,  nonostante  la  sua    &#13;
 volontà  di  avvalersi  della  difesa  di  un  altro magistrato, non    &#13;
 avendo,  a  suo  dire,  potuto  reperire  un  collega   disposto   ad    &#13;
 assisterlo".  "Per costante interpretazione", conclude la remittente,    &#13;
 si esclude che l'impugnato art. 34 consenta la nomina di  ufficio  di    &#13;
 un magistrato difensore.                                                 &#13;
    2.  -  Tale  premessa  di fatto palesa l'irrilevanza del primo dei    &#13;
 dedotti profili d'illegittimità costituzionale, non venendo  qui  in    &#13;
 evidenza  l'aspetto  dell'obbligatorietà  della  difesa,  posto  che    &#13;
 l'incolpato ha già espresso la propria intenzione di valersi  di  un    &#13;
 difensore, sì che il momento della discrezionalità della scelta tra    &#13;
 assistenza  ed  autodifesa  risulta ormai superato nella dinamica del    &#13;
 procedimento.                                                            &#13;
    La relativa questione deve quindi essere dichiarata inammissibile.    &#13;
    3.1. - Il secondo profilo va altresì scrutinato con riguardo alla    &#13;
 sola ipotesi rilevante, quella dell'incolpato che non possa  nominare    &#13;
 un  magistrato  difensore  e  non  già  che  non intenda farlo. Tale    &#13;
 seconda eventualità riconduce infatti  alla  prospettazione  sub  2,    &#13;
 cioè  ad  un problema di alternativa tra l'assistenza obbligatoria e    &#13;
 l'assistenza facoltativa, che esula dal procedimento a quo.              &#13;
    Così circoscritta  nella  sua  rilevanza  e  precisata  nei  suoi    &#13;
 termini, la questione è fondata.                                        &#13;
    3.2.  - Questa Corte ha costantemente ritenuto che il procedimento    &#13;
 disciplinare  nei  confronti  dei  magistrati  è  strutturalmente  e    &#13;
 funzionalmente  diverso  da  quello  previsto per gli impiegati dello    &#13;
 Stato.  Nel  suo  carattere  giurisdizionale  ed  in  una  serie   di    &#13;
 peculiarità,  l'intera  vicenda  disciplinare  riflette  il proprium    &#13;
 dell'Ordine  giudiziario  e  le  implicazioni   che   essa   comporta    &#13;
 nell'esercizio delle funzioni giurisdizionali (sentenza n. 289/1992).    &#13;
    A tale specificità si ricollega la particolare previsione dettata    &#13;
 dalla  norma  impugnata,  che  limita  all'àmbito  dei  magistrati i    &#13;
 soggetti legittimati a difendere l'incolpato e rimette a quest'ultimo    &#13;
 l'opzione tra autodifesa ed assistenza del collega.                      &#13;
    Più in generale, questa Corte ha chiarito, quanto a modalità  di    &#13;
 esercizio del diritto di difesa, che esso può essere dal legislatore    &#13;
 "diversamente  regolato e adattato alle speciali esigenze dei singoli    &#13;
 procedimenti, purché  non  ne  siano  pregiudicati  lo  scopo  e  le    &#13;
 funzioni"   (sentenze  nn.  159/1972,  119/1974,  62/1975).  Scopo  e    &#13;
 funzioni che si concretizzano, in primo luogo, nella garanzia  di  un    &#13;
 effettivo    contraddittorio    e    di    un'assistenza    di   tipo    &#13;
 tecnico-professionale. Al riguardo la Corte ha  sottolineato  che  il    &#13;
 diritto di difesa può dirsi assicurato, come regola, nella misura in    &#13;
 cui   si  dia  all'interessato  la  possibilità  di  partecipare  ad    &#13;
 un'effettiva   dialettica   processuale,   non   realizzabile   senza    &#13;
 l'intervento del difensore (sentenza n. 190/1970).                       &#13;
    Ora, nel caso in esame, è vero che  il  legislatore  ha  ritenuto    &#13;
 l'assistenza  in  parola  surrogabile  con  il  bagaglio  culturale e    &#13;
 l'esperienza  professionale  di  cui  il  magistrato  è  normalmente    &#13;
 portatore,  così  giustificando  la  previsione dell'autodifesa e la    &#13;
 limitazione dell'assistenza ai soli colleghi. Ma è  altresì  chiaro    &#13;
 che,  nel momento in cui la scelta dell'incolpato si sia compiuta nel    &#13;
 senso  di  valersi  dell'opera  di  un  collega,   la   garanzia   di    &#13;
 effettività  del  diritto  di  quella  difesa  che l'interessato non    &#13;
 ritiene di potersi assicurare da solo, postula la necessità che tale    &#13;
 assistenza venga comunque resa possibile.                                &#13;
    Gli stessi argomenti che sono  alla  base  della  possibilità  di    &#13;
 difendersi    solo    personalmente,    accreditando    all'incolpato    &#13;
 preparazione tecnica e discernimento sufficienti a decidere se  farsi    &#13;
 assistere o meno da un collega, militano a favore della necessità di    &#13;
 una nomina d'ufficio ove tale opera difensiva non si concretizzi.        &#13;
    In  altri  e  conclusivi  termini,  il precetto di cui all'art. 24    &#13;
 della Costituzione non consente che ragioni di carattere oggettivo  -    &#13;
 ostative  all'individuazione  e  nomina  di  un  collega  difensore -    &#13;
 vengano a comprimere in danno dell'incolpato quella  possibilità  di    &#13;
 consapevole  ed  attiva  partecipazione  al  procedimento,  in cui si    &#13;
 sostanzia l'effettività della difesa.                                   &#13;
    Il dato  testuale  dell'art.  34  non  permette  l'interpretazione    &#13;
 adeguatrice suggerita dall'Autorità intervenuta, sì che la norma va    &#13;
 dichiarata  illegittima nella parte in cui non prevede esplicitamente    &#13;
 la  possibilità,  per  la  Sezione  disciplinare,  di  nominare   un    &#13;
 magistrato  difensore  d'ufficio  all'incolpato  che  abbia scelto di    &#13;
 farsi assistere da un collega: analogamente, del resto, a  quanto  è    &#13;
 legislativamente   previsto   per   altre   ipotesi  di  procedimenti    &#13;
 disciplinari (cfr. ad es. gli artt. 17, comma 4,  delle  disposizioni    &#13;
 d'attuazione  C.P.P. e 15, secondo comma, della legge 11 luglio 1978,    &#13;
 n. 382 "Norme di principio sulla disciplina militare").</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  dell'art.  34,  secondo    &#13;
 comma,   del  regio  decreto  legislativo  31  maggio  1946,  n.  511    &#13;
 (Guarentige della Magistratura), nella parte in cui non consente alla    &#13;
 Sezione disciplinare del Consiglio Superiore  della  Magistratura  di    &#13;
 disporre d'ufficio la nomina di un magistrato difensore;                 &#13;
    Dichiara  inammissibile  la  ulteriore  questione  di legittimità    &#13;
 costituzionale della medesima disposizione, sollevata in  riferimento    &#13;
 all'art.   24  della  Costituzione  dalla  Sezione  disciplinare  del    &#13;
 Consiglio Superiore della Magistratura  con  l'ordinanza  di  cui  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 maggio 1994.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: RUPERTO                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria l'8 giugno 1994.                            &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
