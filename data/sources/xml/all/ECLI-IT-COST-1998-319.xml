<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>319</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:319</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/07/1998</data_decisione>
    <data_deposito>22/07/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, &#13;
 avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 81 e 113  del    &#13;
 decreto  legislativo 25 febbraio 1995, n. 77 (Ordinamento finanziario    &#13;
 e  contabile  degli  enti  locali),  come  modificato   dal   decreto    &#13;
 legislativo  11 giugno 1996, n. 336, promosso con ordinanza emessa il    &#13;
 9 gennaio  1998  dal  pretore  di  Cosenza  nel  procedimento  civile    &#13;
 vertente  tra  Biasi Renato contro il comune di Fiumefreddo Bruzio ed    &#13;
 altra iscritta al n. 100 del registro  ordinanze  1998  e  pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 9, prima serie speciale,    &#13;
 dell'anno 1998.                                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di consiglio  del  1  luglio  1998  il  giudice    &#13;
 relatore Annibale Marini.                                                &#13;
                               Ordinanza                                  &#13;
   Ritenuto   che   nel   corso  di  un  procedimento  di  opposizione    &#13;
 all'esecuzione, il pretore di Cosenza, con ordinanza  del  9  gennaio    &#13;
 1998,  ha  sollevato  -  in  riferimento agli artt. 3, 24 e 113 della    &#13;
 Costituzione - questione di legittimità costituzionale  degli  artt.    &#13;
 81 e 113 del decreto legislativo 25 febbraio 1995, n. 77 (Ordinamento    &#13;
 finanziario  e  contabile  degli  enti  locali),  come modificato dal    &#13;
 decreto legislativo 11 giugno 1996, n. 336;                              &#13;
     che, ad avviso del rimettente, la prima delle norme  censurate  -    &#13;
 nel  prevedere  una  forma  di procedura esecutiva collettiva di tipo    &#13;
 concorsuale nei confronti di un ente locale che  versi  in  stato  di    &#13;
 dissesto  -  violerebbe  l'art.  113  della  Costituzione  in  quanto    &#13;
 l'intera procedura si svolgerebbe  "in  ambito  amministrativo  senza    &#13;
 alcun   controllo  giurisdizionale  se  non  quello  eventuale  della    &#13;
 magistratura amministrativa con la conseguenza di una  ingiustificata    &#13;
 degradazione  di  un  diritto  soggettivo  perfetto,  quale quello di    &#13;
 credito, ad un mero interesse legittimo";                                &#13;
     che  la   suddetta   procedura   consentirebbe   al   commissario    &#13;
 liquidatore  di  transigere, secondo il proprio arbitrio, vertenze in    &#13;
 atto  o  pretese  in  corso   senza   alcuna   forma   di   controllo    &#13;
 giurisdizionale,  pregiudicando  in  tal  modo  la  realizzazione del    &#13;
 principio della par condicio creditorum;                                 &#13;
     che, secondo il giudice a quo, l'art. 113 del  citato  d.lgs.  n.    &#13;
 77  del  1995  -  nel disporre la non assoggettabilità ad esecuzione    &#13;
 forzata delle somme di pertinenza degli enti  locali  destinate,  con    &#13;
 atto   amministrativo,  all'espletamento  dei  servizi  essenziali  -    &#13;
 renderebbe impossibile al creditore procedente la tutela del  proprio    &#13;
 credito   a  fronte  di  una  dichiarazione  negativa  del  tesoriere    &#13;
 dell'ente locale;                                                        &#13;
     che, in tal modo, si determinerebbe,  con  la  creazione  di  due    &#13;
 diverse  categorie  di  creditori,  una  deroga  al  principio  della    &#13;
 responsabilità patrimoniale di cui all'art. 2740 del codice  civile,    &#13;
 priva  di  giustificazione  sotto  il  profilo della ragionevolezza e    &#13;
 lesiva del principio di eguaglianza;                                     &#13;
     che la stessa disposizione - attribuendo rilevanza, nei  rapporti    &#13;
 privatistici,  ad un atto dell'ente esecutato avente natura meramente    &#13;
 previsionale  e  perciò  di  carattere  generico  ed  impedendo   al    &#13;
 creditore  la  verifica  dell'effettiva  destinazione  delle  somme -    &#13;
 violerebbe  il  diritto  del  creditore  procedente  a  resistere  in    &#13;
 giudizio, tutelato dall'art. 24 della Costituzione;                      &#13;
     che nel giudizio davanti alla Corte costituzionale è intervenuto    &#13;
 il  Presidente  del  Consiglio  dei  Ministri, rappresentato e difeso    &#13;
 dall'Avvocatura generale dello Stato, chiedendo che la questione  sia    &#13;
 dichiarata inammissibile e infondata;                                    &#13;
     che,  in  prossimità  della  camera  di  consiglio, l'Avvocatura    &#13;
 generale dello Stato ha depositato memoria insistendo  nelle  proprie    &#13;
 richieste  ed  in particolare rilevando come l'attuale disciplina del    &#13;
 risanamento finanziario degli enti locali dissestati di cui al d.lgs.    &#13;
 n. 77  del  1995,  come  modificato  dal  d.lgs.  n.  336  del  1996,    &#13;
 "riprodurrebbe   dal  punto  di  vista  strutturale"  quella  di  cui    &#13;
 all'abrogato art. 21 del d.-l. 18 gennaio 1993 n. 8, convertito nella    &#13;
 legge 19 marzo 1993 n. 68, che questa  Corte  avrebbe  già  ritenuto    &#13;
 costituzionalmente legittima.                                            &#13;
   Considerato che difetta nell'ordinanza di rimessione la motivazione    &#13;
 della  rilevanza  della  questione di legittimità costituzionale nel    &#13;
 giudizio principale e che non  è  neppure  possibile  desumere  tale    &#13;
 rilevanza da una descrizione, pur sommaria, della fattispecie oggetto    &#13;
 della controversia sottoposta alla decisione del giudice rimettente;     &#13;
     che  la  diversità  delle  due  norme  congiuntamente denunciate    &#13;
 (delle quali, mentre l'una presuppone l'intervenuta dichiarazione  di    &#13;
 dissesto  dell'ente  locale  e  dispone  l'apertura  di una procedura    &#13;
 concorsuale di liquidazione, l'altra stabilisce, in relazione ad  una    &#13;
 esecuzione  individuale, l'impignorabilità delle somme di pertinenza    &#13;
 degli  enti  locali  destinate   a   determinati   servizi   pubblici    &#13;
 essenziali)  non  consente di ricostruire, neppure induttivamente, la    &#13;
 fattispecie sottoposta all'esame del giudice a quo;                      &#13;
     che, pertanto, la questione deve essere dichiarata manifestamente    &#13;
 inammissibile.                                                           &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  dinanzi    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità  costituzionale  degli  artt.  81  e  113  del   decreto    &#13;
 legislativo  25  febbraio  1995,  n.  77  (Ordinamento  finanziario e    &#13;
 contabile degli enti locali), come modificato dal decreto legislativo    &#13;
 11 giugno 1996, n. 336, sollevata, in riferimento agli artt. 3, 24  e    &#13;
 113  della  Costituzione,  dal  pretore  di  Cosenza  con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 9 luglio 1998.                                &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Marini                             &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 22 luglio 1998.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
