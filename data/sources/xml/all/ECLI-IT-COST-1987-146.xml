<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>146</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:146</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/04/1987</data_decisione>
    <data_deposito>23/04/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, dott. Francesco SAJA, prof. &#13;
 Giovanni CONSO, prof. Ettore GALLO, dott. Aldo CORASANITI, prof. &#13;
 Giuseppe BORZELLINO, dott. Francesco GRECO, prof. Renato &#13;
 DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  di  legittimità  costituzionale  degli artt. 44, primo    &#13;
 comma, del r.d. 26  giugno  1924,  n.  1054  (t.u.  delle  leggi  sul    &#13;
 Consiglio  di  Stato) 26 r.d. 17 agosto 1907, n. 642 (Regolamento per    &#13;
 la procedura dinanzi alle Sezioni giurisdizionali  del  Consiglio  di    &#13;
 Stato)   e  7  1°  comma,  della  legge  6  dicembre  1971,  n.  1034    &#13;
 (Istituzione dei tribunali amministrativi regionali) promossi con  le    &#13;
 seguenti ordinanze:                                                      &#13;
      1)  ordinanza  emessa  il 26 giugno 1979 dal t.a.r. per l'Umbria    &#13;
 sul ricorso proposto da  Gasperini  Virgilio  contro  comune  di  San    &#13;
 Giustino  iscritta al n. 129 del registro ordinanze 1980 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 124 dell'anno 1980;         &#13;
      2) ordinanza emessa il 10 giugno 1980 dal t.a.r. per il Piemonte    &#13;
 sul ricorso proposto da Baro Domenico contro Ente autonomo del teatro    &#13;
 regio  di  Torino  iscritta  al  n. 728 del registro ordinanze 1980 e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 345 dell'anno    &#13;
 1980;                                                                    &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Uditi  nell'udienza pubblica del 7 aprile 1987 il giudice relatore    &#13;
 Virgilio Andrioli e l'avvocato  dello  Stato  Paolo  D'Amico  per  il    &#13;
 Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con ordinanza emessa il 26 giugno 1979 (pervenuta alla Corte    &#13;
 il 18 febbraio 1980; notificata il 7 e comunicata l'8  gennaio  1980;    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  n.  124  del  7 maggio 1980 e    &#13;
 iscritta al n. 129 r.o. 1980) sul ricorso,  con  il  quale  Gasperini    &#13;
 Virgilio,  già dipendente del comune di San Giustino, aveva chiesto,    &#13;
 previo annullamento della delibera comunale n. 77 del 5 maggio  1976,    &#13;
 il  pagamento  della  indennità  sostitutiva dei riposi festivi e la    &#13;
 corresponsione degli emolumenti dovuti per prestazioni  svolte  oltre    &#13;
 il  normale  orario  di  lavoro,  il  t.a.r.  Umbria,  dopo avere con    &#13;
 sentenza  in  pari  data  dichiarato  fondata  la  prima  domanda   e    &#13;
 constatato   che   sulla  base  dei  documenti  acquisiti  a  seguito    &#13;
 dell'esperimento  dei  mezzi  istruttori  consentiti   nel   processo    &#13;
 amministrativo  non  risultavano  provati  i  fatti costitutivi della    &#13;
 pretesa posta a base della seconda domanda, ritenne rilevante  e,  in    &#13;
 riferimento  agli  artt.  3,  24,  103  e 113 della Costituzione, non    &#13;
 manifestamente infondata la questione di legittimità  costituzionale    &#13;
 degli artt. 44 del r.d. 26 giugno 1924, n. 1054 (t.u. delle leggi sul    &#13;
 Consiglio di Stato) e 26 del r.d. 17 agosto 1907, n. 642 (Regolamento    &#13;
 per  la  procedura dinanzi alle sezioni giurisdizionali del Consiglio    &#13;
 di Stato) nella  parte  in  cui  non  prevedono  la  possibilità  di    &#13;
 esperire  dinanzi al giudice amministrativo, in sede di giurisdizione    &#13;
 esclusiva e per controversie  attinenti  a  diritti  soggettivi,  gli    &#13;
 altri  mezzi  di  prova  previsti  per il processo dinanzi al giudice    &#13;
 ordinario.                                                               &#13;
    2.  -  Avanti la Corte non si è costituita alcuna delle parti del    &#13;
 giudizio a quo; ha spiegato intervento il  Presidente  del  Consiglio    &#13;
 dei  Ministri  con  atto  depositato  il 27 maggio 1980, con il quale    &#13;
 l'Avvocatura generale dello Stato ha rilevato che  il  t.a.r.  Umbria    &#13;
 non  avrebbe motivato sulla rilevanza della questione e, pertanto, ne    &#13;
 ha concluso per l'inammissibilità e, in ipotesi, per l'infondatezza.    &#13;
    3.  -  Con  ordinanza emessa il 10 giugno 1980 (notificata il 25 e    &#13;
 comunicata il 31 del successivo  luglio;  pubblicata  nella  Gazzetta    &#13;
 Ufficiale n. 345 del 17 dicembre 1980 e iscritta al n. 728 r.o. 1980)    &#13;
 sul ricorso, con il quale Baro Domenico, assunto  dall'Ente  autonomo    &#13;
 del teatro regio di Torino con decorrenza 1° aprile 1974 come operaio    &#13;
 di   prima   categoria   addetto   alla   centrale   termica   e   di    &#13;
 condizionamento,  aveva  - sul riflesso che, a decorrere da circa due    &#13;
 anni dalla data di assunzione, l'ente lo  aveva  adibito  a  svariate    &#13;
 mansioni  (verniciatore,  scaricatore,  addetto a manutenzioni varie)    &#13;
 con ordine di non più occuparsi dell'incarico originario - lamentato    &#13;
 la  violazione  dell'art.  31 del d.P.R. 10 gennaio 1957, n. 3 e più    &#13;
 specificamente dell'art. 12 c.c.n. di categorie del  1971,  il  quale    &#13;
 vieta  di  assegnare,  anche temporaneamente, l'operaio a mansioni di    &#13;
 categoria  diverse  rispetto  a  quelle  di  categoria  qualora  ciò    &#13;
 comporti   mutamento  sostanziale  della  sua  posizione  morale  nei    &#13;
 confronti dell'ente e, pertanto, aveva chiesto dichiararsi  l'obbligo    &#13;
 dell'ente  di assegnargli mansioni di addetto alla centrale termica e    &#13;
 di condizionamento nell'ambito di prima categoria e mansioni  per  le    &#13;
 quali   fu  assunto,  il  t.a.r.  Piemonte,  disattese  le  eccezioni    &#13;
 d'inammissibilità  e  irricevibilità  opposte  dall'ente,   ritenne    &#13;
 rilevante  e,  in relazione agli artt. 3 e 24, primo commo, e 2 della    &#13;
 Costituzione, giudicò non manifestamente infondata la  questione  di    &#13;
 legittimità  costituzionale degli artt. 44, primo comma, del r.d. 26    &#13;
 giugno 1924, n. 1054, 26 del r.d. 17 agosto 1907, n. 642 e 19,  primo    &#13;
 comma,  della  legge  6  dicembre  1971,  n.  1034  (Istituzione  dei    &#13;
 tribunali  amministrativi  regionali),  in  riferimento  all'art.  7,    &#13;
 secondo comma, della legge n. 1034/1971.                                 &#13;
    4.1.  -  Avanti la Corte nessuna delle parti del giudizio a quo si    &#13;
 è costituita; ha spiegato intervento il Presidente del Consiglio dei    &#13;
 Ministri  con  atto  depositato  il  6  gennaio  1981,  con  il quale    &#13;
 l'Avvocatura generale dello  Stato  ha  riassunto  le  argomentazioni    &#13;
 esposte   nel   precedente   atto  d'intervento  concludendo  per  la    &#13;
 inammissibilità o per la infondatezza della proposta questione.         &#13;
    4.2.  -  Nella  udienza pubblica del 7 aprile 1987, nella quale il    &#13;
 giudice Andrioli ha svolto congiunta  relazione  sui  due  incidenti,    &#13;
 l'avv.  Stato  D'Amico  ha illustrato la deduzione d'inammissibilità    &#13;
 dei due incidenti.<diritto>Considerato in diritto</diritto>5.1.  -  La  connessione,  se  non la identità, dei due incidenti    &#13;
 induce a disporne la riunione ai fini di unitaria deliberazione.         &#13;
    Non  ha  fondamento  l'eccezione d'inammissibilità per difetto di    &#13;
 rilevanza, su cui si è diffusa l'Avvocatura erariale, perché non è    &#13;
 lecito  imporre  alla parte l'onere di chiedere l'assunzione di mezzi    &#13;
 istruttori  la  cui  ammissibilità  dipende  dalla  declaratoria  di    &#13;
 incostituzionalità  di disposizioni sottordine che non la prevedono.    &#13;
    5.2.  -  Il rispetto del canone della corrispondenza tra chiesto e    &#13;
 pronunciato  induce  a  limitare  lo  scrutinio  della  questione  di    &#13;
 costituzionalità,  sollevata dai giudici a quibus, a controversie di    &#13;
 impiego di dipendenti dello Stato e di enti pubblici, riservate  alla    &#13;
 giurisdizione esclusiva dei t.a.r. e, in secondo grado, del Consiglio    &#13;
 di Stato, e alla individuazione dei  mezzi  istruttori  che  ai  fini    &#13;
 dell'accertamento dei fatti possono essere disposti.                     &#13;
   Così  circoscritta, la questione d'incostituzionalità degli artt.    &#13;
 44, primo comma, del r.d. 26 giugno 1924, n. 1054 (t.u.  delle  leggi    &#13;
 sul  Consiglio  di  Stato)  e  26  del  r.d.  17  agosto 1907, n. 642    &#13;
 (Regolamento per la procedura dinanzi  alle  sezioni  giurisdizionali    &#13;
 del  Consiglio  di  Stato),  e  7 primo comma, della legge 6 dicembre    &#13;
 1971, n. 1034 (Istituzione dei  tribunali  amministrativi  regionali)    &#13;
 nei  limiti  in  cui  li  richiama, violano gli artt. 3 e 24, primo e    &#13;
 secondo comma, della Costituzione  perché  è  contrario  vuoi  alla    &#13;
 direttiva   di   razionalità   vuoi,   e  soprattutto,  alla  tutela    &#13;
 dell'azione in giudizio e alla garanzia  del  diritto  di  difesa  la    &#13;
 limitazione  della ricerca della verità nelle controversie de quibus    &#13;
 ai mezzi istruttori descritti nell'art. 44, primo comma, del r.d.  n.    &#13;
 1054/1924 e 26, primo comma, del r.d. n. 642/1907.                       &#13;
    Le  normative che la legge istitutiva dei t.a.r. ha avuto il torto    &#13;
 di non richiamare, sono non già le disposizioni  del  secondo  libro    &#13;
 del codice di procedura civile sulla istruzione probatoria (artt. 191    &#13;
 e 262), sibbene gli artt. 421, comma 2 a 4,  422,  424  e  425  dello    &#13;
 stesso, novellati in virtù della legge 11 agosto 1973, n. 533.          &#13;
    Così  decidendo,  la  Corte  segue la via segnata con la sent. 28    &#13;
 giugno 1985, n. 190, dichiarativa dell'incostituzionalità  dell'art.    &#13;
 21,  ultimo  comma,  della  legge  n.  1034/1971  nella parte in cui,    &#13;
 limitando l'intervento  d'urgenza  del  giudice  amministrativo  alla    &#13;
 sospensione  dell'esecutività  dell'atto  impugnato, non consente al    &#13;
 giudice  stesso  di  adottare,  nelle  controversie  patrimoniali  in    &#13;
 materia   di  pubblico  impiego  sottoposte  alla  sua  giurisdizione    &#13;
 esclusiva,  i  provvedimenti  urgenti   che   appaiono   secondo   le    &#13;
 circostanze  più  idonei  ad assicurare provvisoriamente gli effetti    &#13;
 della decisione sul  merito  le  quante  volte  il  ricorrente  abbia    &#13;
 fondato  motivo  di  temere  che  durante  il  tempo  necessario alla    &#13;
 prolazione della provincia di merito il suo diritto sia minacciato da    &#13;
 un  pregiudizio  imminente  e  irreparabile,  e con la sent. 31 marzo    &#13;
 1987, n. 89, dichiarativa dell'incostituzionalità dell'art. 2, primo    &#13;
 comma, n. 3 del d.P.R. 5 gennaio 1950, n. 180, nella parte in cui, in    &#13;
 contrasto con l'art. 545, quarto comma, del c.p.c.,  non  prevede  la    &#13;
 pignorabilità   e  la  sequestrabilità  degli  stipendi,  salari  e    &#13;
 retribuzioni corrisposti  da  altri  enti  diversi  dallo  Stato,  da    &#13;
 aziende  ed  imprese di cui all'art. 1 dello stesso d.P.R., fino alla    &#13;
 concorrenza di un quinto per ogni credito vantato nei  confronti  del    &#13;
 personale.                                                               &#13;
    Via   che   non   ha   mancato   di   battere  l'art.  31  (Tutela    &#13;
 giurisdizionale) del  d.P.R.  24  marzo  1981,  n.  145  (Ordinamento    &#13;
 dell'Azienda  autonoma  di  assistenza  al volo per il traffico aereo    &#13;
 generale) per  il  quale,  pur  essendo  le  controversie  di  lavoro    &#13;
 relative   al   personale   comunque  in  servizio  presso  l'Azienda    &#13;
 attribuite alla esclusiva  giurisdizione  dei  t.a.r.,  si  applicano    &#13;
 l'art.  28,  della  legge  20  maggio  1970,  n.  300,  e,  in quanto    &#13;
 applicabili, le disposizioni di cui alla legge  11  agosto  1973,  n.    &#13;
 533.                                                                     &#13;
    De  futuro: prevede l'art. 28, primo comma, della legge quadro sul    &#13;
 pubblico impiego 29 marzo 1983, n.  93  che  "In  sede  di  revisione    &#13;
 dell'ordinamento  della  giurisdizione  amministrativa si provvederà    &#13;
 alla  emanazione  di  norme  che   si   ispirino,   per   la   tutela    &#13;
 giurisdizionale  del  pubblico  impiego,  ai principi contenuti nelle    &#13;
 leggi 20 maggio 1970, n. 300 e 11 agosto 1973, n. 533".</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 riuniti  gli  incidenti  iscritti ai nn 129/1980 e 728/1980, dichiara    &#13;
 l'illegittimità costituzionale degli artt. 44, primo comma, del r.d.    &#13;
 26  giugno  1924,  n. 1054 e 26 del r.d. 17 agosto 1907, n. 642, e 7,    &#13;
 primo comma, della legge 6 dicembre 1971, n. 1034 nei limiti  in  cui    &#13;
 li  richiama,  nella  parte  in cui, nelle controversie di impiego di    &#13;
 dipendenti dello  Stato  e  di  enti,  riservate  alla  giurisdizione    &#13;
 esclusiva  amministrativa,  non  consentono  l'esperimento  dei mezzi    &#13;
 istruttori previsti negli artt. 421, comma 2 a 4, 422, 424 e 425, del    &#13;
 c.p.c. novellati in virtù della legge 11 agosto 1973, n. 533.           &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 palazzo della Consulta, il 10 aprile 1987                                &#13;
                       Il Presidente: LA PERGOLA                          &#13;
                       Il Redattore: ANDRIOLI                             &#13;
    Depositata in cancelleria il 23 aprile 1987.                          &#13;
                        Il cancelliere: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
