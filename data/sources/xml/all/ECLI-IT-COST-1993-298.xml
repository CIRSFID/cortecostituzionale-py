<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>298</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:298</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1993</data_decisione>
    <data_deposito>01/07/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 51, n. 3, del    &#13;
 codice di procedura civile,  promosso  con  ordinanza  emessa  il  16    &#13;
 ottobre  1992  dal  Pretore di Trapani - sezione distaccata di Alcamo    &#13;
 nel procedimento civile vertente tra Catalano  Giuseppe  ed  altre  e    &#13;
 Gruppuso Giuseppe, n. q. di curatore del fallimento di Calamia Rocco,    &#13;
 iscritta  al  n.  20  del  registro ordinanze 1993 e pubblicata nella    &#13;
 Gazzetta Ufficiale della  Repubblica  n.  5,  prima  serie  speciale,    &#13;
 dell'anno 1993;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  del  5  maggio  1993  il  Giudice    &#13;
 relatore Renato Granata;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - In un giudizio civile pendente innanzi al Pretore di Trapani,    &#13;
 sezione distaccata di Alcamo, l'avv. Francesco Paolo Ruisi, difensore    &#13;
 di  una  delle  parti in causa, presentava istanza di ricusazione del    &#13;
 giudice  investito  della  controversia  (nei  cui  confronti   aveva    &#13;
 proposto  domanda  di  risarcimento  del  danno  davanti  al  giudice    &#13;
 conciliatore  di  Alcamo  per  alcune   affermazioni   contenute   in    &#13;
 un'ordinanza  emessa  in un precedente giudizio), istanza subordinata    &#13;
 al mancato esercizio da parte del medesimo giudice del  potere-dovere    &#13;
 di astenersi.                                                            &#13;
    Il  pretore adito ha sollevato, con ordinanza del 16 ottobre 1992,    &#13;
 questione incidentale di legittimità costituzionale dell'art. 51, n.    &#13;
 3, c.p.c. - in riferimento agli artt. 3, 97, 101 e 105 Cost. -  nella    &#13;
 parte  in  cui  non  prevede la possibilità di valutare la manifesta    &#13;
 inammissibilità o la manifesta infondatezza della  domanda  proposta    &#13;
 nei  confronti  di  un  giudice  prima  che  sia  operante per questo    &#13;
 l'obbligo di astensione.                                                 &#13;
    Ad avviso del pretore rimettente l'automaticità  dell'obbligo  di    &#13;
 astensione    espone    il    giudice   ad   iniziative   giudiziarie    &#13;
 strumentalmente preordinate ad ottenere la sostituzione  del  giudice    &#13;
 non   gradito   ed   inoltre   può   determinare  gravi  difficoltà    &#13;
 organizzative dell'intero ufficio. Tali inconvenienti non  sussistono    &#13;
 nell'ipotesi di azione diretta a far valere la responsabilità civile    &#13;
 del  magistrato  per fatti commessi nell'esercizio delle sue funzioni    &#13;
 giacché (oltre alla esclusiva legittimazione passiva dello Stato) è    &#13;
 prevista  (dalla  legge  13  aprile  1988  n.  117)  una  valutazione    &#13;
 preliminare  in  camera di consiglio sulla non manifesta infondatezza    &#13;
 della domanda; invece nessuna  analoga  valutazione  è  prevista  in    &#13;
 tutti i casi in cui il giudice sia stato citato in giudizio per fatti    &#13;
 che  non  attengono  all'esercizio  della giurisdizione ovvero - come    &#13;
 nella specie - sia stato  citato,  ancorché  per  fatti  riguardanti    &#13;
 l'attività  giurisdizionale,  innanzi  ad  un organo giudiziario non    &#13;
 previsto dalla speciale normativa in materia (legge n. 117/88 cit.).     &#13;
    Pertanto - conclude il giudice rimettente  -  la  norma  censurata    &#13;
 confliggerebbe  con  i  principi  dell'indipendenza e della autonomia    &#13;
 della funzione giurisdizionale (art. 101 Cost.), peraltro ostacolando    &#13;
 la funzione di autogoverno della magistratura per  ciò  che  attiene    &#13;
 alla  distribuzione  degli  affari  all'interno  dello stesso ufficio    &#13;
 (art. 105 Cost.); violerebbe il  principio  di  non  irragionevolezza    &#13;
 (art.     3    Cost.);    comprometterebbe    il    buon    andamento    &#13;
 dell'amministrazione della giustizia (art. 97 Cost.).                    &#13;
    2. - È intervenuto  il  Presidente  del  Consiglio  dei  Ministri    &#13;
 rappresentato e difeso dall'Avvocatura Generale dello Stato chiedendo    &#13;
 che la questione sollevata sia dichiarata non fondata in quanto muove    &#13;
 da  un'inammissibile  equiparazione  tra  causa di responsabilità ex    &#13;
 lege n. 117/88 e procedimento di astensione.                             &#13;
    Osserva  poi  che  il  procedimento  di  astensione  ha  carattere    &#13;
 amministrativo e ad esso le parti rimangono estranee, laddove un sub-procedimento  di  delibazione della fondatezza o ammissibilità della    &#13;
 domanda proposta dal (o nei confronti del)  magistrato  non  potrebbe    &#13;
 non   coinvolgere   la  posizione  delle  parti  stesse  della  causa    &#13;
 "pregiudicante".<diritto>Considerato in diritto</diritto>1. - È stata sollevata questione di  legittimità  costituzionale    &#13;
 dell'art. 51, n. 3, c.p.c. in riferimento agli artt. 3, 97, 101 e 105    &#13;
 Cost.  nella  parte in cui non prevede la possibilità di valutare la    &#13;
 manifesta inammissibilità o la manifesta infondatezza della  domanda    &#13;
 proposta  nei  confronti  di  un  giudice  prima che sia operante per    &#13;
 questo l'obbligo di astensione; la norma è sospettata di contrastare    &#13;
 con il principio di non irragionevolezza e di comportare  la  lesione    &#13;
 dei   principi   di   indipendenza  e  di  autonomia  della  funzione    &#13;
 giurisdizionale, nonché di buon andamento dell'amministrazione della    &#13;
 giustizia.                                                               &#13;
    2. - Giova premettere che la disposizione censurata, nel prevedere    &#13;
 l'obbligo di astensione del giudice civile quando egli stesso  od  il    &#13;
 coniuge  abbiano  una causa pendente con una delle parti o alcuno dei    &#13;
 suoi difensori, persegue la finalità di assicurare  la  mancanza  di    &#13;
 ogni  (pur  minima)  interferenza  sulla  posizione  di terzietà del    &#13;
 giudice  stesso  per  preservare  la  indipendenza   della   funzione    &#13;
 giurisdizionale  quale  strumentale  presidio del diritto di agire in    &#13;
 giudizio. La preminente esigenza di tutela di tale valore di  rilievo    &#13;
 costituzionale  -  già evidenziata da questa Corte (sent. n. 390 del    &#13;
 1991) che ha ritenuto  prevalenti  "la  garanzia  della  serenità  e    &#13;
 obiettività  dei  giudizi"  e  "la  imparzialità e la terzietà del    &#13;
 giudice" - rende  pienamente  coerente  una  rigorosa  e  dettagliata    &#13;
 disciplina  delle  ipotesi di astensione del giudice (ed in genere di    &#13;
 chi sia chiamato ad operare  in  posizione  di  terzietà,  quale  il    &#13;
 consulente  tecnico  d'ufficio  che può essere ricusato nelle stesse    &#13;
 ipotesi di astensione del giudice), potendo il  legislatore  prendere    &#13;
 in  considerazione anche situazioni in cui il rischio di interferenza    &#13;
 sia  minimo.  Non  sarebbe  quindi  censurabile,  sotto  alcuno   dei    &#13;
 parametri  invocati,  il fatto che il legislatore, nel prevedere come    &#13;
 ipotesi di astensione la pendenza di una causa tra il giudice adito e    &#13;
 la parte od il suo difensore,  non  abbia  enucleato  la  fattispecie    &#13;
 della  causa  manifestamente  infondata  (o  addirittura  temeraria),    &#13;
 omettendo di prevedere un meccanismo di  "filtro"  per  escludere  in    &#13;
 questo  caso  l'obbligo  di  astensione,  atteso  anche  che  in tale    &#13;
 evenienza potrebbe non diminuire affatto il rischio  di  interferenza    &#13;
 sulla  serenità  del  giudizio,  se  non  addirittura risultare esso    &#13;
 accentuato in ragione della  consapevolezza  del  giudice  di  essere    &#13;
 stato ingiustamente chiamato in giudizio.                                &#13;
    3.  -  Né rileva la disciplina dettata dall'art. 5 della legge n.    &#13;
 117 del 1988 sulla responsabilità civile dei magistrati e richiamata    &#13;
 dal giudice rimettente. Le situazioni non sono comparabili perché la    &#13;
 previa delibazione ivi prevista circa la  eventuale  inammissibilità    &#13;
 per  manifesta  infondatezza  della domanda di risarcimento dei danni    &#13;
 assertivamente    provocati    nell'esercizio     della     attività    &#13;
 giurisdizionale  trova  la  sua  ragione  d'essere nella peculiare ed    &#13;
 autonoma esigenza di evitare che la possibilità di un indiscriminato    &#13;
 ingresso di pretese risarcitorie (seppur nei confronti  dello  Stato,    &#13;
 ma  con  azione di rivalsa nei confronti del giudice) induca remore o    &#13;
 timori nell'esercizio dell'attività giurisdizionale per  il  rischio    &#13;
 di azioni temerarie od intimidatorie (sent. n. 468 del 1990).            &#13;
    4.  - La fattispecie della causa manifestamente infondata è però    &#13;
 presa  in  considerazione  dall'ordinanza  del   giudice   rimettente    &#13;
 essenzialmente  sotto  un profilo del tutto particolare che è quello    &#13;
 di una artificiosa preordinazione della pendenza della  lite  proprio    &#13;
 al  fine di predisporre un'ipotesi di automatica astensione (e quindi    &#13;
 anche  di  possibile  ricusazione)  di  un  determinato  giudice  non    &#13;
 gradito.  In  tale  evenienza  in effetti non è dato rinvenire nella    &#13;
 disciplina processuale un meccanismo di  preventiva  valutazione  che    &#13;
 valga ad evitare l'automatismo dell'astensione, salvo considerare che    &#13;
 -  ove  di  fatto  il giudice non si astenga proprio per il carattere    &#13;
 fittizio della lite (e sempre che ciò possa essere valutato in  sede    &#13;
 disciplinare per escludere ogni responsabilità del giudice stesso) -    &#13;
 la  causa di astensione ridonda in causa di ricusazione, per la quale    &#13;
 è invece prevista  dagli  artt.  53  e  54  c.p.c.  una  delibazione    &#13;
 preliminare  che può condurre all'inammissibilità del ricorso della    &#13;
 parte ove risulti la fittizietà della lite ad arte provocata.           &#13;
    Non di meno però deve riconoscersi che esiste un qualche  rischio    &#13;
 di  strumentalizzazione  dell'ipotesi  di  astensione obbligatoria in    &#13;
 esame,  non  sufficientemente  bilanciato   dalla   possibilità   di    &#13;
 qualificare  un tale comportamento vuoi come violazione del dovere di    &#13;
 lealtà processuale delle parti (art. 88 c.p.c.), vuoi come  condotta    &#13;
 non   conforme   alla  deontologia  professionale  forense  e  quindi    &#13;
 suscettibile di sanzioni disciplinari; rischio che ridonderebbe - pur    &#13;
 senza considerare la possibile incidenza sul  principio  del  giudice    &#13;
 naturale,   in  quanto  non  evocato  dal  giudice  rimettente  -  in    &#13;
 vulnerazione dei  principi  di  indipendenza  e  di  autonomia  della    &#13;
 funzione     giurisdizionale,     nonché     di    buon    andamento    &#13;
 dell'amministrazione della giustizia. Ma la costruzione di  una  fase    &#13;
 di  delibazione  preliminare  analoga a quella prevista per l'ipotesi    &#13;
 della  ricusazione  è  compito  del  legislatore   che   nella   sua    &#13;
 discrezionalità può variamente disegnarla sia in ordine all'atto di    &#13;
 impulso,   sia  al  procedimento  e  all'individuazione  del  giudice    &#13;
 competente ad operare tale delibazione, sia all'idoneità, o meno, di    &#13;
 tale fase incidentale a sospendere il giudizio.                          &#13;
    L'ordinanza di rimessione chiede quindi alla Corte  una  pronuncia    &#13;
 additiva che non si presenta affatto a rime obbligate essendo plurime    &#13;
 le  ipotesi  di  disciplina  del meccanismo di preventiva valutazione    &#13;
 della causa manifestamente infondata, preordinata a precostituire  un    &#13;
 motivo  di astensione o ricusazione del giudice; sicché la questione    &#13;
 di costituzionalità va dichiarata inammissibile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 51, n. 3, del codice  di  procedura  civile  sollevata,  in    &#13;
 riferimento  agli artt. 3, 97, 101 e 105 della Costituzione, dal Pretore di  Trapani,  sezione  distaccata  di  Alcamo,  con  l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 giugno 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: GRANATA                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 1° luglio 1993.                          &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
