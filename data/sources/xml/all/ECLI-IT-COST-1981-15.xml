<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>15</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:15</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Arnaldo Maccarone</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/01/1981</data_decisione>
    <data_deposito>10/02/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIOVANNI FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  28  della  &#13;
 legge  17 agosto 1942, n. 1150, come modificato dall'art. 8 della legge  &#13;
 6 agosto 1967, n. 765 (legge urbanistica) promosso con ordinanze emesse  &#13;
 il 28 ottobre 1975 ed il 12 ottobre 1977 rispettivamente dai  tribunali  &#13;
 di  Mantova  e  di  Piacenza - sezioni agrarie, nei procedimenti civili  &#13;
 vertenti tra Moretti Argia in Minelli e Sproccati Corrado e  tra  Rizzi  &#13;
 Virgilio  ed  altra  e Brizzolara Giovanni ed altri, iscritte al n. 161  &#13;
 del registro ordinanze 1976 ed al n. 521 del registro ordinanze 1977  e  &#13;
 pubblicate  nella  Gazzetta Ufficiale della Repubblica n. 92 del 1976 e  &#13;
 n. 25 del 1978.                                                          &#13;
     Udito nella camera di consiglio del 27  novembre  1980  il  Giudice  &#13;
 relatore Arnaldo Maccarone.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza  emessa  il  28 ottobre 1975 nel procedimento civile  &#13;
 vertente tra Moretti Argia e Sproccati Corrado  avente  ad  oggetto  il  &#13;
 rilascio  di  fondo  rustico ceduto in affito, il tribunale di Mantova,  &#13;
 sezione specializzata agraria, ha sollevato questione  di  legittimità  &#13;
 costituzionale   dell'art.  8  della  legge  6  agosto  1967,  n.  765,  &#13;
 concernente la disciplina della procedura relativa  alla  lottizzazione  &#13;
 di   aree   con   destinazione  edilizia,  in  quanto  non  prevede  la  &#13;
 corresponsione di un indennizzo a favore del conduttore di terreni  nei  &#13;
 confronti  del quale, come nella specie, venga pronunciata la decadenza  &#13;
 dal diritto alla proroga del contratto di  affitto  perché  i  terreni  &#13;
 stessi sono stati compresi in un piano di lottizzazione convenzionale.   &#13;
     Secondo  l'ordinanza  si  concreterebbe in tal modo una irrazionale  &#13;
 disparità di trattamento rispetto a quanto previsto  invece  dall'art.  &#13;
 17  della  legge 22 ottobre 1971, n. 865, che attribuisce un indennizzo  &#13;
 al coltivatore diretto che debba cessare  dalla  conduzione  del  fondo  &#13;
 espropriato  per  pubblica  utilità  ai  sensi  della  legge stessa, e  &#13;
 sarebbe così violato il principio di eguaglianza garantito dall'art. 3  &#13;
 Cost.                                                                    &#13;
     L'ordinanza  notificata  e  comunicata  come  per  legge,  è stata  &#13;
 pubblicata nella Gazzetta Ufficiale n. 92 del 7 aprile 1976.             &#13;
     Questione analoga è stata sollevata  dal  tribunale  di  Piacenza,  &#13;
 sezione  specializzata  agraria,  nel  procedimento  vertente tra Rizzi  &#13;
 Virgilio ed altra e  Brizzolara  Giovanni  ed  altri,  pure  avente  ad  &#13;
 oggetto  il  rilascio di terreni ceduti in affitto e compresi poi in un  &#13;
 piano di lottizzazione.                                                  &#13;
     Il giudice a quo a sostegno della  censura,  rivolta  espressamente  &#13;
 contro  "l'art.  28  della  legge  17  agosto 1942, n. 1150, così come  &#13;
 modificato dall'art. 8 della legge n. 765 del 1967"  si  richiama  alla  &#13;
 giurisprudenza   di   questa   Corte   con   cui  è  stata  dichiarata  &#13;
 l'illegittimità dell'articolo unico della legge 13 giugno 1961, n. 527  &#13;
 e dell'art. 1 d.l.  C.P.S.  1  aprile  1947,  n.  273,  in  quanto  non  &#13;
 prevedevano  un  indennizzo a favore del conduttore dichiarato decaduto  &#13;
 dalla proroga  del  contratto  di  affitto  nel  caso  di  radicali  ed  &#13;
 immediate  trasformazioni agrarie del fondo da parte del concedente che  &#13;
 risultino incompatibili con la continuazione del rapporto.  La  ragione  &#13;
 fondamentale  posta  alla  base  della menzionata pronunzia, afferma il  &#13;
 giudice a quo, risiede nella riconosciuta  esigenza  di  attribuire  un  &#13;
 ristoro  all'affittuario  costretto  ad  abbandonare  il  fondo  per il  &#13;
 suddetto motivo, in  osservanza  del  principio  sancito  dall'art.  44  &#13;
 Cost.,  secondo cui devono essere garantiti equi rapporti sociali nella  &#13;
 regolamentazione della proprietà terriera.  E  tale  esigenza  sarebbe  &#13;
 indubbiamente valida anche nell'ipotesi in esame in cui, come in quella  &#13;
 esaminata  dalla Corte, il coltivatore deve rilasciare il fondo perché  &#13;
 la sua permanenza è incompatibile con la nuova destinazione  economica  &#13;
 del bene.                                                                &#13;
     Non manifestamente infondata sarebbe altresì la questione sotto il  &#13;
 profilo   della   violazione   del   principio   di   eguaglianza,  per  &#13;
 l'ingiustificata disparità di trattamento che  conseguirebbe  sia  dal  &#13;
 riconoscimento  del  diritto  all'indennizzo  nella  situazione  cui si  &#13;
 riferisce la menzionata  pronunzia  della  Corte  e,  viceversa,  dalla  &#13;
 esclusione  del  diritto stesso nella del tutto analoga situazione oggi  &#13;
 in esame, che  come  tale  richiederebbe  eguale  disciplina,  sia  dal  &#13;
 contrasto  con  l'art.  17  della  legge  22 ottobre 1971, n. 865, già  &#13;
 rilevato con la citata ordinanza del tribunale di Mantova.               &#13;
     L'ordinanza, notificata e  comunicata  come  per  legge,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale n. 25 del 25 gennaio 1978.           &#13;
     Non  risultando costituzione di parti, per entrambe le questioni è  &#13;
 stata fissata la discussione in camera di consiglio.<diritto>Considerato in diritto</diritto>:                          &#13;
  Le due cause, riguardando questioni analoghe, possono essere riunite e  &#13;
 decise con unica sentenza.                                               &#13;
     Le censure sollevate si riferiscono al preteso  contrasto  con  gli  &#13;
 artt.  3  e 44 Costituzione dell'art. 28 della legge 17 agosto 1942, n.  &#13;
 1150, modificato dall'art.  8 della legge 6 agosto 1967, n. 765.         &#13;
     Dette norme  riguardano  la  disciplina  del  procedimento  per  la  &#13;
 lottizzazione  di aree a scopo edilizio, e fissano i modi, i tempi e le  &#13;
 condizioni per le autorizzazioni e per  le  concessioni  delle  licenze  &#13;
 edilizie nell'ambito dei singoli lotti.                                  &#13;
     I  giudici  a  quibus lamentano la carenza, in queste norme, di una  &#13;
 disposizione che attribuisca un indennizzo al conduttore  di  un  fondo  &#13;
 rustico, il quale venga dichiarato decaduto dalla proroga del contratto  &#13;
 perché il fondo stesso è stato compreso in un piano di lottizzazione.  &#13;
     Le questioni sono inammissibili.                                     &#13;
     Nel  caso  di  specie  si  versa in una ipotesi di cessazione della  &#13;
 proroga non prevista espressamente dalle leggi che regolano la materia,  &#13;
 ma individuata dalla giurisprudenza in base al principio che  le  leggi  &#13;
 di  proroga  dei  contratti  agrari  presuppongono,  ai fini della loro  &#13;
 applicabilità, il riferimento a terreni destinati all'agricoltura, per  &#13;
 cui,  se  un  terreno  perde  tale  qualità,  come  nel   caso   della  &#13;
 lottizzazione  per  destinazione  edilizia,  il contratto agrario viene  &#13;
 meno come tale e non può essere considerato soggetto a proroga.         &#13;
     I giudici a  quibus  hanno  individuato  la  sedes  materiae  della  &#13;
 lamentata  omissione  nelle  norme  regolanti  la  lottizzazione, ma è  &#13;
 evidente che gli articoli impugnati, riguardando, come si è  detto,  i  &#13;
 tempi,  i  modi e le condizioni delle autorizzazioni e delle licenze in  &#13;
 materia, con riferimento esclusivo al rapporto fra il proprietario e la  &#13;
 pubblica amministrazione, investono  un  campo  del  tutto  diverso  ed  &#13;
 autonomo  rispetto  a  quello cui si riferisce la censura, che riguarda  &#13;
 una pretesa disarmonia nel bilanciamento degli interessi del  lavoro  e  &#13;
 del capitale in agricoltura, nell'ambito della disciplina dei contratti  &#13;
 agrari e delle relative proroghe, e con stretto riferimento al rapporto  &#13;
 privato fra concedente e conduttore.                                     &#13;
     La  norma  impugnata,  pertanto, non rileva di per sé in ordine al  &#13;
 problema prospettato nelle ordinanze di rinvio ed i giudici del merito,  &#13;
 per decidere le  controversie,  non  debbono  fare  applicazione  degli  &#13;
 articoli  di legge impugnati, i quali non rappresentano quindi la sedes  &#13;
 materiae idonea a dar luogo ad una  pronunzia  di  questa  Corte  sulla  &#13;
 questione   di   costituzionalità   prospettata.   Ciò   comporta  in  &#13;
 conformità di quanto già deciso da questa Corte con la  sent.  35/80,  &#13;
 in  analoga  fattispecie,  l'inammissibilità  per difetto di rilevanza  &#13;
 della questione sollevata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  inammissibilità  delle  questioni  di   legittimità  &#13;
 costituzionale  dell'art.  8  della  legge  6  agosto  1967,  n. 765, e  &#13;
 dell'art. 28 della legge 17 agosto 1942,  n.  1150,  sollevate  con  le  &#13;
 ordinanze  di  cui  in  epigrate in riferimento agli artt. 3 e 44 della  &#13;
 Costituzione.                                                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 29 gennaio 1981.        &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   - LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI.                      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
