<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>304</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:304</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/05/1989</data_decisione>
    <data_deposito>25/05/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 69, 10° comma,    &#13;
 legge 27 luglio 1978 n. 392, nella nuova  formulazione  di  cui  alla    &#13;
 legge   6   febbraio   1987   n.  15,  ("Conversione  in  legge,  con    &#13;
 modificazioni del decreto-legge 9  dicembre  1986,  n.  382,  recante    &#13;
 misure  urgenti  in  materia  di  contratti  di locazione di immobili    &#13;
 adibiti ad uso diverso da quello di  abitazione");  dell'art.  2,  4°    &#13;
 comma legge 6 febbraio 1987 n. 15 promosso con ordinanza emessa il 14    &#13;
 novembre 1988 dal Pretore di Napoli nel procedimento civile  vertente    &#13;
 tra  S.p.A. ICE SNEI e Fiorillo Salvatore ed altro iscritta al n. 812    &#13;
 del registro ordinanze 1988 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 3 prima serie speciale dell'anno 1989;               &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 12 aprile 1989 il Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto che nel corso di un processo civile promosso dal locatore    &#13;
 di un immobile ad uso non abitativo contro il conduttore per ottenere    &#13;
 "la   determinazione  giudiziale  del  canone  e  dell'indennità  di    &#13;
 occupazione" per il  periodo  successivo  alla  scadenza  del  regime    &#13;
 transitorio,  il  Pretore  di  Napoli,  con ordinanza del 14 novembre    &#13;
 1988,  ha  sollevato,  in  riferimento  agli  artt.  3  e  42   della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art. 69,    &#13;
 decimo comma, della legge 27 luglio 1978, n. 392, nel testo stabilito    &#13;
 dalla  legge  6 febbraio 1987, n. 15, e dell'art. 2, quarto comma, di    &#13;
 quest'ultima  legge,  "nella  parte  in  cui  si  stabilisce  che  il    &#13;
 conduttore  di  immobili ad uso diverso da abitazione, per il periodo    &#13;
 di occupazione successivo alla scadenza del vecchio contratto e  fino    &#13;
 alla  data  di  rilascio  fissata dal giudice o alla decorrenza di un    &#13;
 nuovo contratto, non è tenuto a  corrispondere  se  non  il  canone,    &#13;
 maggiorato di un aumento in misura prefissata dalla legge e neppure a    &#13;
 risarcire il danno ex art. 1591 cod. civ.";                              &#13;
      che nel giudizio davanti alla Corte è intervenuto il Presidente    &#13;
 del  Consiglio  dei  Ministri,  rappresentato  dall'Avvocatura  dello    &#13;
 Stato,  chiedendo che la questione sia dichiarata inammissibile o, in    &#13;
 subordine, infondata;                                                    &#13;
    Considerato  che nell'indicazione della norma impugnata il giudice    &#13;
 a quo è caduto in una falsa  demostratio,  tale  norma  non  essendo    &#13;
 contenuta  nel  combinato  disposto dell'art. 69, decimo comma, della    &#13;
 legge n. 392 del 1978, modificato dall'art.  1  del  decreto-legge  9    &#13;
 dicembre 1986, n. 832, convertito nella legge 6 febbraio 1987, n. 15,    &#13;
 e dell'art. 2, quarto comma, del  medesimo  decreto  n.  832,  bensì    &#13;
 nell'art.  2  del decreto-legge 25 settembre 1987, n. 393, convertito    &#13;
 nella legge 25 novembre 1987, n. 478;                                    &#13;
      che   la  questione  è  già  stata,  con  argomenti  analoghi,    &#13;
 sottoposta a questa Corte, che l'ha dichiarata  non  fondata  con  la    &#13;
 sentenza n. 22 del 1989;                                                 &#13;
    Visti  gli  artt.  26  della  legge 11 marzo 1953 n. 87, e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 dell'art. 2 del decreto-legge 25 settembre 1987, n.  393,  convertito    &#13;
 nella  legge  25 novembre 1987, n. 478 (Norme in materia di locazioni    &#13;
 di  immobili  ad  uso  non  abitativo,  nonché  di  cessione  e   di    &#13;
 assegnazione   di  alloggi  di  edilizia  agevolata-convenzionata)  -    &#13;
 erroneamente indicato come art. 69,  decimo  comma,  della  legge  27    &#13;
 luglio  1987,  n. 392, modificato dalla legge 6 febbraio 1987, n. 15,    &#13;
 combinato con  l'art.  2,  quarto  comma,  di  quest'ultima  legge  -    &#13;
 sollevata,  in  riferimento agli artt. 3 e 42 della Costituzione, dal    &#13;
 Pretore di Napoli con l'ordinanza indicata in epigrafe.                  &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 17 maggio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 25 maggio 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
