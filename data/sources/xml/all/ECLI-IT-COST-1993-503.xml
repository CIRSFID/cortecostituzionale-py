<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>503</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:503</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/12/1993</data_decisione>
    <data_deposito>31/12/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA prof. Giuliano VASSALLI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 1, 2, 7 e 8    &#13;
 del decreto-legge 15 maggio 1993, n. 143 (Disposizioni in materia  di    &#13;
 legittimità  dell'azione amministrativa), promosso con ricorso della    &#13;
 Regione  autonoma  Valle  d'Aosta,  notificato  il  15  giugno  1993,    &#13;
 depositato  in  cancelleria il 23 successivo ed iscritto al n. 31 del    &#13;
 registro ricorsi 1993;                                                   &#13;
    Visto l'atto di costituzione  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 3 novembre 1993 il Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto che la Regione autonoma della Valle d'Aosta, con  ricorso    &#13;
 notificato il 15 giugno 1993 e depositato il successivo 23 giugno, ha    &#13;
 sollevato  questione di legittimità costituzionale degli artt. 1, 2,    &#13;
 7 e 8 del decreto-legge 15  maggio  1993,  n.  143  (Disposizioni  in    &#13;
 materia  di  legittimità  dell'azione  amministrativa), deducendo la    &#13;
 violazione  degli  artt.  77,  100,  103,  108,  116  e   125   della    &#13;
 Costituzione,  nonché  degli  artt.  2, 3, 4, 29, 38, 43 e 46, primo    &#13;
 comma, del proprio Statuto (legge costituzionale 26 febbraio 1948, n.    &#13;
 4);                                                                      &#13;
      che,  in  particolare,  le  disposizioni  impugnate   concernono    &#13;
 l'istituzione delle sezioni regionali della Corte dei conti (art. 1),    &#13;
 l'individuazione   dell'organo  incaricato  dello  svolgimento  delle    &#13;
 funzioni di pubblico ministero  presso  le  sezioni  regionali  della    &#13;
 Corte  dei  conti (art. 2), la individuazione degli atti, anche delle    &#13;
 regioni, da sottoporre al controllo della Corte  dei  conti,  nonché    &#13;
 dei  modi  e  dei  contenuti  del  controllo  successivo (art. 7), e,    &#13;
 infine, la istituzione dei servizi di controllo interno in  tutte  le    &#13;
 amministrazioni  pubbliche,  e le relative modalità di funzionamento    &#13;
 (art. 8);                                                                &#13;
      che, ad  avviso  della  ricorrente,  le  disposizioni  impugnate    &#13;
 sarebbero  variamente  lesive  delle proprie competenze, dei principi    &#13;
 costituzionali in materia di controllo sugli atti delle regioni e  di    &#13;
 tutela   delle   minoranze  linguistiche  riconosciute,  nonché  dei    &#13;
 principi che regolano la decretazione d'urgenza e la riserva di legge    &#13;
 formale, prevista dagli  artt.  100,  secondo  e  terzo  comma,  103,    &#13;
 secondo comma, e 108 della Costituzione;                                 &#13;
      che  si  è costituito il Presidente del Consiglio dei ministri,    &#13;
 chiedendo  che  il  ricorso  sia  dichiarato   inammissibile   ovvero    &#13;
 infondato;                                                               &#13;
    Considerato  che  il  decreto-legge  15 maggio 1993, n. 143 non è    &#13;
 stato convertito in legge nel termine di sessanta  giorni  dalla  sua    &#13;
 pubblicazione,  come risulta dal comunicato pubblicato nella Gazzetta    &#13;
 Ufficiale n. 166 del 17 luglio 1993;                                     &#13;
      che, pertanto, in  conformità  alla  giurisprudenza  di  questa    &#13;
 Corte  (v.,  da ultimo, l'ordinanza n. 470 del 1993), le questioni di    &#13;
 legittimità costituzionale sollevate  dalla  Regione  Valle  d'Aosta    &#13;
 devono essere dichiarate manifestamente inammissibili;                   &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   delle  questioni  di    &#13;
 legittimità costituzionale degli artt. 1, 2, 7 e 8 del decreto-legge    &#13;
 15 maggio 1993, n.  143  (Disposizioni  in  materia  di  legittimità    &#13;
 dell'azione  amministrativa),  sollevate dalla Regione autonoma della    &#13;
 Valle d'Aosta, con il ricorso indicato in  epigrafe,  per  violazione    &#13;
 degli  artt. 77, 100, 103, 108, 116 e 125 della Costituzione, nonché    &#13;
 degli artt. 2, 3, 4, 29, 38, 43 e  46,  primo  comma,  dello  Statuto    &#13;
 speciale approvato con legge costituzionale 26 febbraio 1948, n. 4.      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 29 dicembre 1993.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: BALDASSARRE                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 31 dicembre 1993.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
