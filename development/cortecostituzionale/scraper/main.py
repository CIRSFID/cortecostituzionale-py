import hashlib
import json
import os
import re
from zipfile import ZipFile
import csv

import requests
from bs4 import BeautifulSoup
from lxml import etree
from tqdm import tqdm

from cortecostituzionale import logger
from cortecostituzionale.db_manager import get_conn
from cortecostituzionale.parser.main import get_meta
from cortecostituzionale.settings import DATA_DIR

tqdm.monitor_interval = 0

__here__ = os.path.abspath(os.path.dirname(__file__))
urls_path = os.path.join(__here__, "urls.json")

MAIN_URL = "https://dati.cortecostituzionale.it/Scarica_i_dati/Scarica_i_dati"
SOURCES_DIR = os.path.join(DATA_DIR, "sources")
SPARQL_ENDPOINT = "https://dati.cortecostituzionale.it/sparql/endpoint"

conn = get_conn()
cur = conn.cursor()


def etree_to_string(xml):
    return etree.tostring(xml, encoding="utf-8", method="xml", pretty_print=True).decode("utf-8")


def load_urls():
    logger.info("FETCHING URLS")
    r = requests.get(MAIN_URL)
    try:
        assert r.status_code == 200, f"Cannot reach {MAIN_URL}"
    except Exception as e:
        logger.exception(f"Cannot reach {MAIN_URL} ({e})")
        raise
    soup = BeautifulSoup(r.text, "html.parser")
    urls = []
    for a in soup.find_all("a"):
        url = a.get("href", "")
        if re.search(r"CC_OpenPronunce\w+\.zip$", url):
            urls.append(url)
    return urls


def needs_download(filename):
    if "oggi" in filename.lower():
        return True
    if cur.execute("SELECT * FROM downloaded_archives WHERE filename=?", (filename,)).fetchone() is None:
        return True
    return False


def download_archives():
    urls = load_urls()
    archive_pardir = os.path.join(SOURCES_DIR, "xml")
    os.makedirs(archive_pardir, exist_ok=True)
    logger.info("DOWNLOADING ARCHIVES")
    for url in urls:
        _, filename = os.path.split(url)
        if needs_download(filename):
            try:
                print(f"Fetching {url} ", flush=True, end="")
                r = requests.get(url)
                assert r.status_code == 200, f"Cannot fetch archive ({url})"
                filepath = os.path.join(archive_pardir, filename)
                with open(filepath, "wb") as f:
                    f.write(r.content)
                print("Ok.")
                hexdigest = hashlib.sha256(r.content).hexdigest()
                with conn:
                    cur.execute(
                        """
                        INSERT OR IGNORE INTO downloaded_archives (
                            filename, sha256_hexdigest
                        ) VALUES (?, ?)
                        """, (filename, hexdigest,)
                    )
            except Exception as e:
                print(e)
                logger.exception(f"Fatal error while downloading archives: {e}")
    logger.info("ARCHIVES DOWNLOADED")


def download_sparql_meta():
    print("Downloading SPARQL meta...", end="", flush=True)
    logger.info("DOWNLOADING SPARQL META")
    query = """
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
    PREFIX dcc: <https://dati.cortecostituzionale.it/ontology/>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX d2r: <http://sites.wiwiss.fu-berlin.de/suhl/bizer/d2r-server/config.rdf#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX schema-org: <http://schema.org/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX meta: <http://www4.wiwiss.fu-berlin.de/bizer/d2r-server/metadata#>
    PREFIX time: <http://www.w3.org/2006/time#>
    PREFIX map: <https://dati.cortecostituzionale.it/ontology/#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    PREFIX sioc: <http://rdfs.org/sioc/ns#>
    select distinct ?pronuncia ?numero ?anno ?tipo_procedimento ?nome_presidente ?nome_relatore ?tipo ?dispositivo where {
       ?pronuncia a dcc:Pronuncia ;
                    dbpedia-owl:number ?numero ;
                    time:year ?anno;
                    dcc:tipoProcedimento ?tipo_procedimento;
                    dc:type ?tipo_pronuncia;
                    dcc:tipoDispositivo ?tipo_dispositivo;
                    dcc:presidentePronuncia ?presidente ;
                    dcc:relatorePronuncia ?relatore .
      ?tipo_pronuncia rdfs:label ?tipo .
      ?presidente foaf:name ?nome_presidente .
      ?relatore foaf:name ?nome_relatore .
      ?tipo_dispositivo rdfs:label ?dispositivo .
    }
    ORDER BY ASC(?numero)
    """
    r = requests.post(SPARQL_ENDPOINT, data={"query": query, "output": "json"})
    if r.status_code == 200:
        sparql_meta = json.loads(r.text)
        with open(os.path.join(SOURCES_DIR, "sparql_meta.json"), "w") as f:
            json.dump(sparql_meta, f, ensure_ascii=False)
            logger.info("OK")
            print("Ok.")
            return sparql_meta
    else:
        raise ConnectionError(f"Could not get meta from SPARQL: {r.status_code}")


def cleanup_sparql_meta(sparql_meta):
    bindings = sparql_meta["results"]["bindings"]
    clean_sparql_meta = {}
    for binding in tqdm(bindings, desc="Cleaning up meta"):
        anno = binding["anno"]["value"]
        numero = binding["numero"]["value"]
        clean_sparql_meta[f"{anno}:{numero}"] = dict(
            tipo_procedimento=binding["tipo_procedimento"]["value"],
            dispositivo=binding["dispositivo"]["value"]
        )
    return clean_sparql_meta


def enrich_meta(meta, testata, sparql_meta):
    anno, numero = meta["anno"], meta["numero"]
    key = f"{anno}:{numero}"
    meta.update(sparql_meta.get(key, {}))
    if meta.get("tipo_procedimento"):
        tipo_procedimento = etree.Element("tipo_procedimento")
        tipo_procedimento.text = meta.get("tipo_procedimento", "SCONOSCIUTO")
        testata.append(tipo_procedimento)
    if meta.get("dispositivo"):
        dispositivo = etree.Element("dispositivo")
        dispositivo.text = meta.get("dispositivo", "SCONOSCIUTO")
        testata.append(dispositivo)


def split_docs(xml_string, sparql_meta):
    doc = etree.fromstring(xml_string)
    dest_dir = os.path.join(SOURCES_DIR, "xml", "all")
    os.makedirs(dest_dir, exist_ok=True)
    with conn:
        for pronuncia in tqdm(doc.findall("pronuncia")):
            meta = get_meta(pronuncia.find("pronuncia_testata"))
            if meta is not None:
                ecli = meta["ecli"]
                enrich_meta(meta, pronuncia.find("pronuncia_testata"), sparql_meta)
                try:
                    cur.execute(
                        """
                        INSERT OR IGNORE INTO pronunce (
                            ecli, anno, numero, tipo, presidente, relatore, data_decisione, data_deposito, tipo_procedimento, dispositivo
                        ) VALUES (
                            :ecli, :anno, :numero, :tipo, :presidente, :relatore, :data_decisione, :data_deposito, :tipo_procedimento, :dispositivo
                        )
                        """, meta
                    )
                    filename = meta["ecli"].replace(":", "-") + ".xml"
                    filepath = os.path.join(dest_dir, filename)
                    with open(filepath, "w") as f:
                        f.write(etree_to_string(pronuncia))
                except:
                    msg = f"Fatal Error for {ecli}"
                    tqdm.write(msg)
                    logger.exception(msg)
            else:
                ecli = pronuncia.find("pronuncia_testata").find("ecli").text
                logger.info(f"COULD NOT FIND META FOR {ecli}")
                corrupted_path = os.path.join(dest_dir, "corrupted")
                os.makedirs(corrupted_path, exist_ok=True)
                filepath = os.path.join(corrupted_path, ecli.replace(":", "-") + ".xml")
                with open(filepath, "w") as f:
                    f.write(etree_to_string(pronuncia))
                msg = f"COULD NOT FIND META FOR {ecli}. Manual intervention needed for {filepath}"
                logger.info(msg)
                tqdm.write(msg)


def needs_extraction(filename):
    row = cur.execute(
        """
        SELECT extracted FROM downloaded_archives WHERE filename=?
        """, (filename,)
    ).fetchone()
    if row is not None:
        extracted = row["extracted"]
        return not extracted
    return


def extract_archives(sparql_meta=None):
    xml_dir = os.path.join(DATA_DIR, "sources", "xml")
    logger.info("EXTRACTING ARCHIVES")
    if sparql_meta is None:
        with open(os.path.join(SOURCES_DIR, "sparql_meta.json"), "r") as f:
            sparql_meta = cleanup_sparql_meta(json.load(f))
    for zip_name in tqdm([_ for _ in os.listdir(xml_dir) if _.endswith(".zip")], desc="extracting"):
        if "oggi" not in zip_name:
            continue
        main_zip_filepath = os.path.join(xml_dir, zip_name)
        main_zip_dir = main_zip_filepath.replace(".zip", "")
        with ZipFile(main_zip_filepath, "r") as main_zip:
            main_zip.extractall(main_zip_dir)
        for sub_zip_name in tqdm([_ for _ in os.listdir(main_zip_dir) if _.endswith(".zip")]):
            sub_zip_filepath = os.path.join(main_zip_dir, sub_zip_name)
            with ZipFile(sub_zip_filepath, "r") as sub_zip:
                for filename in sub_zip.namelist():
                    if filename.lower().endswith("xml"):
                        with sub_zip.open(filename) as f:
                            split_docs(f.read(), sparql_meta)
    logger.info("EXTRACTION COMPLETE")


def scrape():
    download_archives()
    sparql_meta = download_sparql_meta()
    clean_sparql_meta = cleanup_sparql_meta(sparql_meta)
    extract_archives(clean_sparql_meta)


def add_missing():
    missing_filepath = os.path.join(SOURCES_DIR, "missing.csv")
    dictobj = csv.DictReader(open(missing_filepath))
    with conn:
        for meta in dictobj:
            cur.execute(
                """
                INSERT OR IGNORE INTO pronunce (
                    ecli, anno, numero, tipo, presidente, relatore, data_decisione, data_deposito
                ) VALUES (
                    :ecli, :anno, :numero, :tipo, :presidente, :relatore, :data_decisione, :data_deposito
                )
                """, dict(meta)
            )


if __name__ == '__main__':
    scrape()
    # add_missing()
    # extract_archives()
