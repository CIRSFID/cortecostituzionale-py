<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>69</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:69</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/03/1971</data_decisione>
    <data_deposito>05/04/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Prof. FRANCESCO PAOLO BONIFACIO - Dott. &#13;
 LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. &#13;
 ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale dell'art. 12 bis  &#13;
 dei testo unico sulla caccia 5 giugno 1939, n. 1016 (aggiunto dall'art.  &#13;
 3  della legge 2 agosto 1967, n. 799), promossi con ordinanza emessa il  &#13;
 30 giugno 1969 dal pretore di  Conegliano  nel  procedimento  penale  a  &#13;
 carico  di Donati Ugo, iscritta al n. 351 del registro ordinanze 1969 e  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  269  del  22  &#13;
 ottobre  1969,  e  con cinque ordinanze emesse il 4 febbraio 1970 dallo  &#13;
 stesso pretore in altrettanti  procedimenti  penali  rispettivamente  a  &#13;
 carico  di  Modolo Mario, De Polo Antonio, Benedetti Fortunato, Soldera  &#13;
 Gildo e Basei Luciano, iscritte ai nn. 168, 169, 170,  171  e  172  del  &#13;
 registro  ordinanze  1970  e  pubblicate nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 143 del 10 giugno 1970.                                    &#13;
     Visti gli  atti  d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  10  febbraio  1971  il  Giudice  &#13;
 relatore Paolo Rossi;                                                    &#13;
     udito il sostituto avvocato generale dello Stato Franco  Chiarotti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di  sei distinti procedimenti penali a carico di taluni  &#13;
 cacciatori imputati, tra l'altro, della contravvenzione all'art. 12 bis  &#13;
 del testo unico sulla caccia, per aver esercitato l'arte  venatoria  in  &#13;
 località  sottoposte  al  regime di caccia controllata, sprovvisti del  &#13;
 tesserino  prescritto   dal   regolamento   deliberato   dal   Comitato  &#13;
 provinciale  della caccia, il pretore di Conegliano sollevava questione  &#13;
 di legittimità costituzionale del predetto art. 12 bis, per  contrasto  &#13;
 con  il principio di legalità di cui all'art. 25, secondo comma, della  &#13;
 Costituzione, con ordinanze del 30 giugno 1969 e del 4  febbraio  1970,  &#13;
 di  identico contenuto. Il giudice a quo rileva che la norma impugnata,  &#13;
 delineata la nozione di caccia controllata,  stabilisce  la  soggezione  &#13;
 dei cacciatori ad una serie di limiti all'esercizio venatorio, relativi  &#13;
 all'epoca,  al  luogo,  alla  specie e al numero dei capi di selvaggina  &#13;
 stanziale  da  abbattere, ed ammette i titolari di licenza di caccia ad  &#13;
 esercitare la caccia controllata "osservando  le  condizioni  stabilite  &#13;
 dal  regolamento deliberato dal Comitato provinciale della caccia sulla  &#13;
 scorta di un regolamento tipo nazionale". L'ultimo  comma  della  norma  &#13;
 impugnata commina ai contravventori la pena dell'ammenda.                &#13;
     Il  pretore  di  Conegliano  ritiene  che  la  norma  suddetta,  in  &#13;
 violazione della riserva di legge stabilita dall'invocato art. 25 della  &#13;
 Carta, ricolleghi la pena dell'ammenda alla inosservanza di un precetto  &#13;
 che non è formulato nella legge stessa, ma la  cui  determinazione  è  &#13;
 attribuita  al  regolamento  emanando ad opera del Comitato provinciale  &#13;
 per la caccia.                                                           &#13;
     Si è costituito  in  giudizio  il  Presidente  del  Consiglio  dei  &#13;
 ministri,  rappresentato e difeso dall'Avvocatura generale dello Stato,  &#13;
 con atti di intervento depositati il 14 ottobre 1969 ed  il  26  maggio  &#13;
 1970, chiedendo dichiararsi l'infondatezza della questione proposta.     &#13;
     Rileva   l'Avvocatura   che,   anche   secondo   la   più  recente  &#13;
 giurisprudenza della Corte costituzionale, non  si  avrebbe  violazione  &#13;
 dell'art.  25,  secondo  comma,  della  Costituzione,  quando la legge,  &#13;
 nell'attribuire al  regolamento  il  potere  di  fissare,  per  singoli  &#13;
 elementi,   o   nella   loro   interezza,   ipotesi   criminose,  abbia  &#13;
 predeterminato, come  nella  specie,  con  sufficiente  rigore,  questa  &#13;
 fissazione.<diritto>Considerato in diritto</diritto>:                          &#13;
     Tutte  le  ordinanze di remissione propongono la medesima questione  &#13;
 che va quindi definita con unica sentenza.                               &#13;
     La Corte costituzionale è chiamata a decidere se l'art. 12 bis del  &#13;
 t.u. 5 giugno 1939, n. 1016,  che  punisce  con  l'ammenda  l'attività  &#13;
 venatoria   svolta   in   località  sottoposte  al  regime  di  caccia  &#13;
 controllata senza osservare le  condizioni  stabilite  dal  regolamento  &#13;
 deliberato dal Comitato provinciale per la caccia, contrasti o meno con  &#13;
 il  principio  di  legalità  di  cui all'art. 25, secondo comma, della  &#13;
 Costituzione, per il dubbio che la norma legislativa abbia in tal  modo  &#13;
 rinviato all'emanando regolamento l'integrale formulazione del precetto  &#13;
 penale.                                                                  &#13;
     Questa  Corte  ha  avuto  occasione  di rilevare più volte come il  &#13;
 principio di legalità della pena esiga, da un lato, che sia proprio un  &#13;
 atto avente forza di legge ad indicare "con sufficiente  specificazione  &#13;
 i  presupposti, i caratteri, il contenuto ed i limiti dei provvedimenti  &#13;
 dell'autorità non  legislativa,  alla  trasgressione  dei  quali  deve  &#13;
 seguire  la  pena";  d'altro  canto che sia sempre ed esclusivamente la  &#13;
 legge  a  determinare  con  quale  misura  debba  venire  repressa   la  &#13;
 trasgressione  dei  precetti che essa vuole sanzionati penalmente (cfr.  &#13;
 sentenze n. 26 del 1966 e n. 61 del 1969).                               &#13;
     Il principio ora enunciato  appare  rispettato  anche  dalla  norma  &#13;
 impugnata. Invero l'art. 12 bis del vigente t.u. sulla caccia (aggiunto  &#13;
 dall'art.  3  della  legge  2  agosto 1967, n. 799) definisce nelle sue  &#13;
 caratteristiche fondamentali  il  regime  di  caccia  controllata  come  &#13;
 quello  secondo  cui l'esercizio venatorio è soggetto a limitazioni di  &#13;
 tempo, di luogo, di specie e di numero di capi di selvaggina  stanziale  &#13;
 protetta   da  abbattere.  I  vari  regolamenti  emanati  dai  Comitati  &#13;
 provinciali  della  caccia,  sulla  scorta  di  un   regolamento   tipo  &#13;
 nazionale,   sono   stati   previsti   dalla   legge  per  specificare,  &#13;
 principalmente per la necessità di adattare  alle  diverse  condizioni  &#13;
 dei   luoghi,  quelle  caratteristiche  limitative  già  fissate,  con  &#13;
 sufficiente precisione, ad opera della legge.                            &#13;
     Pertanto  la  norma  impugnata  non ha violato l'invocato principio  &#13;
 costituzionale, rimettendo alla fonte regolamentare  la  specificazione  &#13;
 di elementi predeterminati dalla legge.                                  &#13;
     Qualora, poi, in ipotesi, gli emanati regolamenti, nel precisare le  &#13;
 condizioni  da  osservarsi  nell'esercizio  della  caccia  controllata,  &#13;
 avessero  stabilito  oneri   o   limitazioni   non   consentite   dalla  &#13;
 formulazione  legislativa, essi risulterebbero evidentemente affetti da  &#13;
 vizio di illegittimità, con  il  conseguente  dovere  del  giudice  di  &#13;
 disapplicarli,  e con la possibilità per l'interessato di ricorrere ai  &#13;
 comuni strumenti di tutela giurisdizionale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  12  bis  del testo unico sulla caccia 5 giugno 1939, n. 1016  &#13;
 (aggiunto dall'art. 3 della legge 2 agosto 1967, n. 799), sollevata, in  &#13;
 riferimento all'art. 25, secondo  comma,  della  Costituzione,  con  le  &#13;
 ordinanze in epigrafe indicate.                                          &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 25 marzo 1971.                                &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI  -   GIUSEPPE    VERZÌ'   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
