<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>13</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:13</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Brunetto Bucciarelli Ducci</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1986</data_decisione>
    <data_deposito>28/01/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - &#13;
 Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE &#13;
 FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE &#13;
 GALLO - Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. &#13;
 FRANCESCO GRECO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di  legittimità  costituzionale  dell'art.  1,  &#13;
 ultimo  comma,  d.lgt. 8 marzo 1945, n. 90 (Modificazioni delle imposte  &#13;
 sulle successioni e sulle donazioni) e  dell'articolo  unico  legge  20  &#13;
 novembre  1955, n. 1123 (Equiparazione dei diritti dei figli adottivi a  &#13;
 quelli  legittimi  in  materia  fiscale)  promossi  con   le   seguenti  &#13;
 ordinanze:                                                               &#13;
     1)  ordinanza  emessa  il 28 febbraio 1977 dalla Corte d'appello di  &#13;
 Roma nel  procedimento  civile  vertente  tra  l'Amministrazione  delle  &#13;
 Finanze  dello  Stato e Marsili Adriana iscritta al n. 415 del registro  &#13;
 ordinanze 1977 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 293 dell'anno 1977;                                                   &#13;
     2) ordinanza emessa il 21 aprile 1978 dalla Corte di cassazione sul  &#13;
 ricorso  proposto dall'Amministrazione delle Finanze dello Stato contro  &#13;
 Negri Clementi Maria Sidonia ed altri iscritta al n. 565  del  registro  &#13;
 ordinanze  1978  e pubblicata nella Gazzetta Ufficiale della Repubblica  &#13;
 n. 31 dell'anno 1979.                                                    &#13;
     Visto l'atto  di  costituzione  di  Negri  Clementi  Maria  Sidonia  &#13;
 nonché l'atto di intervento del Presidente del Consiglio dei ministri;  &#13;
     udito  nell'udienza  pubblica  del  19  novembre  1985  il  Giudice  &#13;
 relatore Brunetto Bucciarelli Ducci;                                     &#13;
     uditi l'avv. Franco Salvucci per Negri Clementi e l'Avvocato  dello  &#13;
 Stato Carlo Salimei per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.-  Con  ordinanza del 28 febbraio 1977, n. 415 r.o. 1977 la Corte  &#13;
 d'appello di  Roma  sollevava  questione  incidentale  di  legittimità  &#13;
 costituzionale, in riferimento all'art. 3 della Costituzione, dell'art.  &#13;
 1,  ultimo  comma,  d.lgt.  8  marzo  1945,  n.  90  e  in correlazione  &#13;
 dell'articolo unico legge 20 novembre 1955, n. 1123, nella parte in cui  &#13;
 dispongono un trattamento fiscale per i discendenti dei figli  adottivi  &#13;
 del  de  cuius,  che  succedono  a  questo  per  rappresentazione, più  &#13;
 sfavorevole rispetto a quello previsto  per  i  discendenti  dei  figli  &#13;
 legittimi.  Ritiene, infatti, il giudice a quo che tale norma determini  &#13;
 una ingiustificato discriminazione tra discendenti di figli legittimi e  &#13;
 discendenti   di   figli   adottivi   con   violazione   del  principio  &#13;
 costituzionale d'eguaglianza. Malgrado  il  legislatore  -  osserva  la  &#13;
 Corte  d'appello  -  estenda  gli  effetti  dell'adozione  ai  rapporti  &#13;
 ereditari  tra  l'adottante  ed  i  discendenti  legittimi  dei   figli  &#13;
 adottivi,  riconoscendo  a  detti  discendenti  lo  stesso  diritto  di  &#13;
 rappresentazione spettante ai discendenti dei figli  legittimi,  l'art.  &#13;
 1, ultimo comma, d.lgt. n. 90/1945 stabilisce per i successori chiamati  &#13;
 per   rappresentazione   un   sistema  di  tassazione  in  ragione  non  &#13;
 dell'immediatezza della loro  chiamata,  ma  del  grado  dell'eventuale  &#13;
 parentela  con  il  de  cuius.  Sicché,  mentre  viene  assicurato  ai  &#13;
 discendenti dei figli legittimi un determinato trattamento  fiscale  in  &#13;
 dipendenza  della  parentela  diretta  con  l'autore della successione,  &#13;
 altrettanto non si verifica  per  i  discendenti  dei  figli  adottivi,  &#13;
 mancando un identico rapporto di parentela.                              &#13;
     Identica questione è stata sollevata dalla Corte di cassazione con  &#13;
 ordinanza del 21 aprile 1978, n. 565 r.o.  1978.                         &#13;
     2.  -  Nel  primo giudizio è intervenuta con atto 10 novembre 1977  &#13;
 l'amministrazione delle Finanze, in persona del Ministro  pro  tempore,  &#13;
 rappresentato e difeso dall'Avvocatura generale dello Stato, segnalando  &#13;
 in  primo luogo che la vertenza, oggetto del giudizio de quo, era stata  &#13;
 definita in base alle norme sul condono in materia tributaria di cui al  &#13;
 d.l. 5 novembre 1973, n. 660 (convertito nella legge 19 dicembre  1973,  &#13;
 n.  823),  cosicché il giudizio stesso si era estinto, con conseguente  &#13;
 sopravvenuta irrilevanza della questione sollevata.  In  secondo  luogo  &#13;
 l'Avvocatura  ricordava  che  comunque  la medesima questione era stata  &#13;
 già dichiarata non fondata con sentenza di questa Corte n.  71  dell'8  &#13;
 aprile 1976.                                                             &#13;
     Nel  secondo  giudizio  è  intervenuto  invece  il  Presidente del  &#13;
 Consiglio   dei    ministri,    concludendo    nello    stesso    senso  &#13;
 dell'infondatezza e richiamando la citata sentenza della Corte.          &#13;
     3. - Nel secondo procedimento (r.o. n. 565/1978) si sono costituite  &#13;
 altresl' le parti private Maria Sidonia Negri Clementi e Irene Mercedes  &#13;
 Negri  Clementi, rappresentate e difese dall'avv. Francesco Salvucci di  &#13;
 Roma, con atto del 21 dicembre  1978,  concludendo  per  la  fondatezza  &#13;
 della questione sollevata dalla Corte di cassazione.                     &#13;
     Con  successive  memorie  del 25 settembre e del 5 novembre 1985 la  &#13;
 stessa parte privata Maria  Sidonia  Negri  Clementi  ha  ulteriormente  &#13;
 illustrato la propria tesi a favore della illegittimità costituzionale  &#13;
 della norma impugnata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  L'identità  della  questione  sollevata  nei due giudizi ne  &#13;
 consiglia la riunione e la definizione con unica sentenza.               &#13;
     La  questione  sulla  quale  la  Corte  deve  pronunciarsi  è   se  &#13;
 contrastino  o  meno  con  l'art. 3 della Costituzione l'art. 1, ultimo  &#13;
 comma, d.lgt. 8 marzo 1945, n. 90 e l'articolo  unico  della  legge  20  &#13;
 novembre 1955, n. 1123, nella parte in cui, per i discendenti dei figli  &#13;
 adottivi  del  de  cuius,  che succedano a questo per rappresentazione,  &#13;
 dispongono un trattamento fiscale più sfavorevole  rispetto  a  quanto  &#13;
 previsto per i discendenti dei figli legittimi.                          &#13;
     In  effetti  l'ultimo  comma  dell'art.  1  del decreto legislativo  &#13;
 luogotenenziale n. 90 del 1945 stabilisce per i successori chiamati per  &#13;
 rappresentazione   un   sistema   di   tassazione   in   ragione    non  &#13;
 dell'immediatezza  della  loro  chiamata,  ma  del grado dell'eventuale  &#13;
 parentela  con  il  de cuius. Cosicché mentre ai discendenti dei figli  &#13;
 legittimi  viene  assicurato  un  determinato  trattamento  fiscale  in  &#13;
 dipendenza  della  parentela diretta con l'autore della successione, ai  &#13;
 discendenti dei figli adottivi viene  applicata  invece  l'aliquota  di  &#13;
 imposta prevista per la successione tra estranei, in quanto mancherebbe  &#13;
 un identico rapporto di parentela.                                       &#13;
     Né vale a superare tale disparità - come si legge nelle ordinanze  &#13;
 di rimessione - l'articolo unico della legge n.  1123 del 1955 che, pur  &#13;
 segnando una tappa significativa nell'equiparazione tra figli legittimi  &#13;
 e  adottivi  in  materia  fiscale,  ha  una  portata  circoscritta alla  &#13;
 successione  da  adottante  ad  adottato  e  non  può  quindi  trovare  &#13;
 applicazione  al di fuori di tale ipotesi. Tale è del resto il diritto  &#13;
 vivente alla luce della giurisprudenza ordinaria.                        &#13;
     Ritengono i giudici a quibus che la disparità ora  descritta  leda  &#13;
 il  principio  di uguaglianza, discriminando arbitrariamente in materia  &#13;
 fiscale tra discendenti dei figli legittimi  e  discendenti  dei  figli  &#13;
 adottivi,   quando   dal   punto  di  vista  del  diritto  civile  tale  &#13;
 discriminazione non ha più luogo.                                       &#13;
     2. - La questione è fondata.                                        &#13;
     Rettamente le ordinanze di rimessione, nel  valutare  l'adeguatezza  &#13;
 delle disposizioni impugnate al parametro costituzionale di riferimento  &#13;
 e  quindi  al  principio  di uguaglianza, pongono l'accento sul sistema  &#13;
 normativo che regola l'istituto della successione per rappresentazione,  &#13;
 cogliendo l'antinomia in cui si pone il legislatore nel disciplinare la  &#13;
 materia tributaria in tema di successione  -  quando  essa  attenga  ai  &#13;
 discendenti  dei figli adottivi - in modo contraddittorio rispetto alla  &#13;
 regolamentazione della successione stessa ed alla logica giuridica  che  &#13;
 ad essa presiede.                                                        &#13;
     L'art. 468 c.c. infatti chiama alla successione, assieme agli altri  &#13;
 soggetti   espressamente  indicati,  anche  i  discendenti  del  figlio  &#13;
 adottivo che  non  possa  o  non  voglia  accettare  l'eredità,  senza  &#13;
 discriminazione alcuna rispetto ai discendenti del figlio legittimo. La  &#13;
 funzione  dell'istituto  è  quella  di  tutelare  gli  interessi della  &#13;
 famiglia del mancato erede o  legatario  diretto,  impedendo  che  essa  &#13;
 venga  privata  dei  beni del de cuius, solo perché il genitore non ha  &#13;
 potuto o non ha voluto accettarli.                                       &#13;
     Non può quindi negarsi l'esistenza, ai fini della rappresentazione  &#13;
 ereditaria,  di  un  rapporto  civile  tra  adottante   e   discendenti  &#13;
 dell'adottato,   cui  l'ordinamento  attribuisce  un  preciso  rilievo,  &#13;
 derivante dall'adozione ed equivalente a quello di parentela.            &#13;
     Se tale è la logica giuridica del sistema, improntato alla  tutela  &#13;
 dell'adottato  e  dei  suoi  discendenti alla stessa stregua del figlio  &#13;
 legittimo, del tutto arbitraria si rivela la discriminazione introdotta  &#13;
 dal legislatore tributario tra i discendenti del  mancato  crede-figlio  &#13;
 legittimo e quelli del mancato erede-figlio adottivo, con il sottoporre  &#13;
 il   patrimonio  ereditato  da  quest'ultimo  ad  una  tassazione  più  &#13;
 sfavorevole, quale quella prevista per gli estranei.                     &#13;
     Una volta quindi che la legge civile parifica la tutela successoria  &#13;
 della famiglia del figlio adottivo a quella della famiglia  del  figlio  &#13;
 legittimo,  la  mancata  estensione  di  tale parificazione nel diritto  &#13;
 tributario assume il significato di  un  irragionevole  arbitrio,  come  &#13;
 tale lesivo del principio di uguaglianza.                                &#13;
     Restano  Così superati gli argomenti deducibili dall'art. 300 c.c.  &#13;
 che furono posti a base della sentenza di questa Corte n. 71  del  1976  &#13;
 per respingere la medesima questione ora proposta.                       &#13;
     Del resto, occorre osservare che la conclusione allora raggiunta è  &#13;
 oggi  contrastata dalla giurisprudenza ordinaria, la quale ha ritenuto,  &#13;
 anche alla  luce  del  nuovo  clima  normativo,  che  l'esclusione  del  &#13;
 rapporto  civile derivante dal vincolo di adozione tra l'adottante e la  &#13;
 famiglia dell'adottato riguardi unicamente la famiglia  di  origine  di  &#13;
 quest'ultimo e non anche quella da lui costituita (cfr. le ordinanze di  &#13;
 rimessione della Corte di cassazione e della Corte d'appello di Roma).   &#13;
     Va  pertanto dichiarata l'illegittimità costituzionale delle norme  &#13;
 impugnate in parte qua.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 1, ultimo comma,  &#13;
 d.lgt. 8 marzo 1945, n. 90 e  dell'articolo  unico  legge  20  novembre  &#13;
 1955, n. 1123, nella parte in cui, per i discendenti dei figli adottivi  &#13;
 del  de  cuius, che succedono a questo per rappresentazione, dispongono  &#13;
 un trattamento fiscale più sfavorevole rispetto a quello previsto  per  &#13;
 i discendenti dei figli legittimi.                                       &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 22 gennaio 1986.                              &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   BRUNETTO   BUCCIARELLI   -   DUCCI  -  &#13;
                                   ALBERTO  MALAGUGINI  -   ANTONIO   LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO.   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
