<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>292</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:292</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/05/1989</data_decisione>
    <data_deposito>25/05/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 31 della legge    &#13;
 7 agosto 1982, n. 516 (Conversione in legge, con  modificazioni,  del    &#13;
 decreto-legge   10   luglio  1982,  n.  429,  recante  norme  per  la    &#13;
 repressione dell'evasione in materia di imposte  sui  redditi  e  sul    &#13;
 valore  aggiunto  e  per  agevolare  la definizione delle pendenze in    &#13;
 materia tributaria), promosso con ordinanza emessa il 2 dicembre 1987    &#13;
 dalla  Commissione tributaria di secondo grado di Caserta sul ricorso    &#13;
 proposto da Montini  Pietro  -  legale  rappresentante  della  S.p.a.    &#13;
 "Menyanthes"  -  contro  l'Ufficio  del  Registro  di  Sessa  Arunca,    &#13;
 iscritta al n. 554 del registro ordinanze  1988  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  43,  prima serie speciale    &#13;
 dell'anno 1988.                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  dell'8  marzo 1989 il Giudice    &#13;
 relatore Vincenzo Caianiello.                                            &#13;
    Ritenuto  che  nel corso di un giudizio avente ad oggetto l'avviso    &#13;
 di  liquidazione  con  cui  l'Ufficio  del  Registro,  a  seguito  di    &#13;
 un'istanza  di  condono,  aveva  richiesto  il pagamento dell'imposta    &#13;
 complementare Invim per decorso decennio, la  Commissione  tributaria    &#13;
 di  secondo  grado di Caserta ha sollevato, in riferimento all'art. 3    &#13;
 della  Costituzione,   questione   di   legittimità   costituzionale    &#13;
 dell'art.  31,  comma  secondo, del D.L. 10 luglio 1982, n. 429, come    &#13;
 modificato dalla legge di conversione 7 agosto 1982, n. 516;             &#13;
      che  la  norma impugnata viene censurata nella parte in cui, nel    &#13;
 disciplinare la liquidazione delle istanze di  condono  tributario  -    &#13;
 prevedendo,  in  riferimento  ad alcuni tributi fra cui l'I.N.V.I.M.,    &#13;
 che "l'incremento imponibile complessivamente assoggettato ad imposta    &#13;
 non  può  comunque  essere inferiore al 20% del valore dichiarato" -    &#13;
 modifica  il  criterio  di  liquidazione   previsto   dall'originario    &#13;
 decreto-legge,  così  determinando  un'ingiustificata  disparità di    &#13;
 trattamento "tra coloro che hanno  avuto  occasione  di  definire  la    &#13;
 pendenza    tributaria   vigente   la   disposizione   prevista   dal    &#13;
 decreto-legge e  quelli  che  hanno  definito  la  vertenza  dopo  la    &#13;
 conversione in legge del decreto medesimo";                              &#13;
      che,   ad  avviso  del  giudice  a  quo,  gli  effetti  di  tale    &#13;
 discriminazione sarebbero aggravati dall'art. 32 dello  stesso  testo    &#13;
 legislativo   che,  prevedendo  l'irrevocabilità  della  domanda  di    &#13;
 condono, non consente, a chi l'aveva  presentata  nella  vigenza  del    &#13;
 decreto-legge,  di  ritirarla,  una volta modificati - dalla legge di    &#13;
 conversione - i criteri di liquidazione;                                 &#13;
      che  è  intervenuta l'Avvocatura generale dello Stato chiedendo    &#13;
 che la questione venga dichiarata infondata.                             &#13;
    Considerato  che  il  carattere  naturalmente  provvisorio  di  un    &#13;
 decreto-legge che, come nella specie, preveda taluni benefici  per  i    &#13;
 soggetti  che  dichiarino  di  volersene avvalere, non esclude che la    &#13;
 legge di  conversione  possa  apportarvi  modifiche  meno  favorevoli    &#13;
 rispetto alle previsioni del decreto originario;                         &#13;
      che,   in   dipendenza   di   tale   eventualità,   non  appare    &#13;
 irragionevole che colui il quale - conformemente ad un decreto  legge    &#13;
 non  ancora  convertito - si sia avvalso della facoltà di presentare    &#13;
 la dichiarazione integrativa  per  la  definizione  di  una  pendenza    &#13;
 tributaria  debba  soggiacere,  poi,  alla  disciplina, anche se più    &#13;
 svantaggiosa, introdotta dalla legge  di  conversione,  allorché  la    &#13;
 definizione  della  vertenza  avvenga  dopo  l'entrata  in  vigore di    &#13;
 quest'ultima;                                                            &#13;
      che,   proprio   in   tale   prospettiva,  la  previsione  della    &#13;
 irrevocabilità   della   dichiarazione   integrativa,   in    quanto    &#13;
 espressamente  indicata  nel  testo  originario  del decreto legge in    &#13;
 esame (art. 32), lungi "dal privilegiare la posizione - nel  rapporto    &#13;
 tributario  -  dello  Stato",  come sostiene invece il giudice a quo,    &#13;
 già consentiva al contribuente di  valutare  l'eventualità  che  la    &#13;
 definizione  del  procedimento,  ove fosse avvenuta dopo l'entrata in    &#13;
 vigore della legge di conversione, sarebbe  stata  assoggettata  alla    &#13;
 disciplina  risultante  dalle  modifiche  eventualmente introdotte da    &#13;
 quest'ultima legge;                                                      &#13;
      che   tale   carattere   provvisorio   del   decreto-legge,  che    &#13;
 costituisce immediata conseguenza del tipo di procedimento  formativo    &#13;
 della   norma,  esclude  che  possa  considerarsi  ingiustificata  la    &#13;
 denunciata disparità  di  trattamento  rispetto  a  coloro  nei  cui    &#13;
 confronti  la  definizione  sia  eventualmente  avvenuta  prima della    &#13;
 conversione;                                                             &#13;
      che,  difatti,  -  a  parte che si è in presenza di una ipotesi    &#13;
 alquanto improbabile nel  caso  di  specie  tenuto  conto  del  breve    &#13;
 periodo  di  tempo  intercorso  per  la  conversione,  (come posto in    &#13;
 evidenza  dall'Avvocatura  generale  dello  Stato)  -  la  disparità    &#13;
 costituisce  in  ogni  caso  una conseguenza di mero fatto, collegata    &#13;
 alla diversità dei momenti di attuazione;                               &#13;
      che,  altrimenti,  si verrebbe ad affermare che un decreto legge    &#13;
 non possa avere mai immediata applicazione, in vista delle  possibili    &#13;
 modifiche   della  legge  di  conversione  oppure  che  la  legge  di    &#13;
 conversione,   ove   disponga   modifiche,   debba    necessariamente    &#13;
 assoggettarvi anche i rapporti esauritisi sotto l'imperio del decreto    &#13;
 legge, conseguenze, queste,  che  non  discendono  dai  principi  che    &#13;
 regolano la materia;                                                     &#13;
      che  proprio tali principi, salvo espressa previsione contraria,    &#13;
 consentono  che  alcuni  rapporti  vengano  definiti  in  base   alle    &#13;
 previsioni  del  decreto  legge  ed  altri  da  quelle della legge di    &#13;
 conversione,  senza  che  ciò  appaia  irragionevole,  essendosi  in    &#13;
 presenza di situazioni riferite a tempi diversi, il che ne giustifica    &#13;
 la differente disciplina (vedi, fra le altre,  sentenze  n.  159  del    &#13;
 1987 e n. 1019 del 1988);                                                &#13;
      che   la   questione   va   pertanto  dichiarata  manifestamente    &#13;
 infondata.                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 31,  secondo  comma  del  decreto  legge  10    &#13;
 luglio  1982,  n.  429  (Norme  per  la repressione della evasione in    &#13;
 materia di imposte sui redditi e sul valore aggiunto e per  agevolare    &#13;
 la  definizione delle pendenze in materia tributaria) come modificato    &#13;
 con legge di  conversione  7  agosto  1982,  n.  516,  sollevata,  in    &#13;
 riferimento   all'art.   3   della  Costituzione,  dalla  Commissione    &#13;
 tributaria di secondo grado di Caserta, con l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 17 maggio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 25 maggio 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
