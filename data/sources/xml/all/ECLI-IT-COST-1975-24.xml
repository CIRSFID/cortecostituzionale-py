<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>24</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:24</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/01/1975</data_decisione>
    <data_deposito>05/02/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. EDOARDO &#13;
 VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 186, primo  &#13;
 comma, del r.d.21 febbraio 1895, n.70 (Testo unico  delle  leggi  sulle  &#13;
 pensioni civili e militari), modificato dall'art.  11 del d.l.C.P.S. 13  &#13;
 agosto  1947,  n.  833  (Miglioramenti  sui trattamenti di quiescenza),  &#13;
 promosso con ordinanza emessa il 29 marzo 1971 dalla Corte dei conti  -  &#13;
 sezione  IV,  pensioni  militari  -  sul  ricorso  di  Fiorano Susanna,  &#13;
 iscritta al n. 251 del  registro  ordinanze  1973  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 205 dell'8 agosto 1973.           &#13;
     Udito  nella  camera  di  consiglio del 19 dicembre 1974 il Giudice  &#13;
 relatore Ercole Rocchetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel procedimento relativo al ricorso promosso da  Fiorano  Susanna,  &#13;
 vedova  dell'ex  ufficiale  Tito  Checchia,  a  suo tempo incorso nella  &#13;
 perdita del diritto a pensione ai sensi dell'art.   183,  primo  comma,  &#13;
 lett. b), del t.u. 21 febbraio 1895, n. 70, la Corte dei conti, sezione  &#13;
 quarta  giurisdizionale, con sentenza non definitiva emessa il 29 marzo  &#13;
 1971, dichiarava che all'interessata, in possesso di tutti i  requisiti  &#13;
 prescritti  dalla legge, spettava il trattamento pensionistico previsto  &#13;
 dall'art.  186, primo comma, del citato t.u. n. 70 del 1895, nel  testo  &#13;
 modificato  dall'art.  11  del  d.l.C.P.S.  13  agosto  1947, n. 833, e  &#13;
 disponeva che si provvedesse alla liquidazione  provvisoria  secondo  i  &#13;
 criteri dettati da quest'ultima disposizione, alla stregua dei quali la  &#13;
 pensione  di  riversibilità, di coloro che siano incorsi nella perdita  &#13;
 del diritto a pensione, "è liquidata sulla base che  sarebbe  spettata  &#13;
 all'impiegato,  al militare o al salariato con la riduzione del quarto,  &#13;
 prevista dall'art. 187" del t.u. delle leggi sulle  pensioni  civili  e  &#13;
 militari.                                                                &#13;
     Con  ordinanza  emessa  in  pari  data,  la stessa sezione riteneva  &#13;
 rilevante e non manifestamente infondata, con riferimento  all'art.  36  &#13;
 della   Costituzione,   la  questione  di  legittimità  costituzionale  &#13;
 dell'art. 11 del d.l.C.P.S. 13 agosto 1947, n. 833, nella parte in  cui  &#13;
 prevede  "la  riduzione  del quarto prevista dall'art. 187". Secondo la  &#13;
 Corte dei conti, poiché il trattamento  di  quiescenza  va  riguardato  &#13;
 alla  stregua  di una retribuzione differita, meritevole, come tale, di  &#13;
 speciale protezione, la speciale norma che  prevede  la  riduzione  del  &#13;
 quarto  è  in contrasto con l'art. 36, primo comma, della Costituzione  &#13;
 che  assicura  al  lavoratore  e  alla  sua  famiglia  il   trattamento  &#13;
 conquistato mediante l'attività lavorativa.                             &#13;
     L'ordinanza   è   stata   ritualmente   notificata,  comunicata  e  &#13;
 pubblicata.                                                              &#13;
     Nel giudizio dinanzi alla Corte  non  v'è  stata  costituzione  di  &#13;
 parte, né intervento del Presidente del Consiglio dei ministri.         &#13;
     La  causa,  pertanto,  ai sensi dell'art. 26, comma secondo.  della  &#13;
 legge 11 marzo 1953, n. 87, viene decisa con la procedura di camera  di  &#13;
 consiglio.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte dei conti - sezione IV giurisdizionale (pensioni militari)  &#13;
 sottopone  a  questa  Corte  la  questione se non siano da considerarsi  &#13;
 costituzionalmente illegittimi gli artt. 186 del t.u.   sulle  pensioni  &#13;
 civili  e  militari  del  1895, n. 70, e 11 del d.l.C.P.S. del 1947, n.  &#13;
 833, nella parte in cui dispongono che la pensione che sarebbe spettata  &#13;
 a quei dipendenti statali che,  per  condanna  penale  o  provvedimento  &#13;
 disciplinare,  siano  incorsi  nella  perdita del diritto a percepirla,  &#13;
 venga corrisposta alla moglie o alla prole, ma con la riduzione  di  un  &#13;
 quarto.                                                                  &#13;
     La  denuncia  di illegittimità concerne tale riduzione ed è posta  &#13;
 in riferimento all'art. 36, primo comma, della Costituzione,  il  quale  &#13;
 prescrive   che   il   lavoratore   ha   diritto  ad  una  retribuzione  &#13;
 proporzionata alla quantità e qualità del suo lavoro e, in ogni caso,  &#13;
 sufficiente ad assicurare a sé e alla famiglia un'esistenza  libera  e  &#13;
 dignitosa.                                                               &#13;
     È innanzi tutto da osservare che le citate disposizioni dei 1895 e  &#13;
 del  1947,  come ogni altra che prevedeva la perdita, la riduzione o la  &#13;
 sospensione del diritto del  pubblico  dipendente  al  godimento  della  &#13;
 pensione o di equivalente indennità, sono state abrogate dalla legge 8  &#13;
 giugno 1966, n. 424.                                                     &#13;
     Poiché  però, per l'art. 2 di essa, il ripristino del trattamento  &#13;
 di quiescenza del quale il dipendente era stato  privato,  avviene  dal  &#13;
 primo  giorno del mese successivo a quello dell'entrata in vigore della  &#13;
 legge,  e  quindi  solo  per  l'avvenire,  la  proposta  questione   di  &#13;
 costituzionalità  resta  rilevante per il passato, e cioè in rapporto  &#13;
 al periodo in cui permangono gli effetti delle  norme  abrogate  ed  in  &#13;
 base  alle  quali le disposte menomazioni relative al detto trattamento  &#13;
 sono tuttora operanti.                                                   &#13;
     In tali limiti la questione, oltre che rilevante, è anche fondata.  &#13;
     Non può infatti disconoscersi che, se il lavoratore ha diritto  ad  &#13;
 una  retribuzione  che sia proporzionata alla qualità e alla quantità  &#13;
 del  suo  lavoro,  una  diminuzione,  per  qualsiasi  causa,  del   suo  &#13;
 trattamento  retributivo (al quale si collega quello pensionistico), da  &#13;
 presumersi con tale criterio calcolata, rompa quella proporzionalità e  &#13;
 infranga quindi la norma costituzionale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  illegittimità  costituzionale  dell'art.  186, primo  &#13;
 comma, del t.u. sulle pensioni civili e militari 21 febbraio  1895,  n.  &#13;
 70,  modificato  dall'art.  11  del  d.l.C.P.S. 13 agosto 1947. n. 833,  &#13;
 nella parte in cui riduce di un quarto la  pensione  da  corrispondersi  &#13;
 alla  moglie  e alla prole dei dipendenti pubblici che hanno perduto il  &#13;
 diritto a percepirla direttamente.                                       &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 gennaio 1975.        &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI - MICHELE ROSSANO.      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
