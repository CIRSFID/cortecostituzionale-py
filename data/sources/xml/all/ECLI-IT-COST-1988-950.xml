<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>950</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:950</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1988</data_decisione>
    <data_deposito>29/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 10, lett. g),    &#13;
 del d.P.R. 29  settembre  1987,  n.  597  (Istituzione  e  disciplina    &#13;
 dell'imposta  sul  reddito  delle  persone  fisiche), come modificato    &#13;
 dall'art. 5 della legge 13 aprile 1977, n.  114  (Modificazioni  alla    &#13;
 disciplina  dell'imposta sul reddito delle persone fisiche), promosso    &#13;
 con ordinanza emessa il 16 aprile 1987 dalla  Commissione  Tributaria    &#13;
 di I grado di Sanremo sul ricorso proposto da Corsaro Concetto contro    &#13;
 l'Ufficio  II.DD.  di  Sanremo,  iscritta  al  n.  387  del  registro    &#13;
 ordinanze 1987 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 37, prima serie speciale, dell'anno 1987;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  6 luglio 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che la Commissione Tributaria di I grado di Sanremo, nel    &#13;
 corso di un procedimento iniziato da Corsaro Concetto  ed  avente  ad    &#13;
 oggetto  la deducibilità dal reddito percepito nel 1982 dell'assegno    &#13;
 annuo corrisposto al coniuge separato per il mantenimento dei  figli,    &#13;
 ha  sollevato,  con  ordinanza  del  16 aprile 1987 (R.O. n. 387/87),    &#13;
 questione di legittimità costituzionale dell'art. 10,  primo  comma,    &#13;
 lett. g), del d.P.R. 29 settembre 1973, n. 597, così come modificato    &#13;
 dall'art. 5, primo comma, della legge 13 aprile 1977, n.  114,  nella    &#13;
 parte  in  cui non riconosce la detraibilità degli assegni destinati    &#13;
 al mantenimento dei figli;                                               &#13;
      che  la  Commissione,  nel lamentare detta limitazione, ha fatto    &#13;
 riferimento agli artt. 3, 29, 30 e 53  Cost.  per  la  disparità  di    &#13;
 trattamento  che si verifica in relazione alla prevista deducibilità    &#13;
 dell'assegno corrisposto all' ex coniuge ed alla  indetraibilità  di    &#13;
 quello  corrisposto  per i figli assegnati all'altro coniuge; nonché    &#13;
 per la violazione dei  diritti  primari  della  famiglia,  risultando    &#13;
 agevolata  la  situazione  di carenza di figli nati dal matriminio in    &#13;
 quanto il coniuge separato o divorziato  senza  prole  può  detrarre    &#13;
 l'assegno  di  mantenimento  mentre  quello  con prole non ha analoga    &#13;
 possibilità; ed, infine, per il rilievo che la  norma  in  questione    &#13;
 non  terrebbe conto dell'effettiva capacità contributiva del coniuge    &#13;
 separato o divorziato, tenuto alla corresponsione di assegno  per  la    &#13;
 prole,  in  contrasto  con il trattamento di esenzione di cui gode il    &#13;
 coniuge separato o divorziato senza figli;                               &#13;
      che   l'Avvocatura   Generale   dello   Stato,   intervenuta  in    &#13;
 rappresentanza del Presidente del Consiglio dei ministri, ha concluso    &#13;
 per l'infondatezza della questione;                                      &#13;
    Considerato  che,  secondo  la  giurisprudenza di questa Corte, la    &#13;
 detrazione degli oneri  e  delle  spese  incontrate  e  gravanti  sul    &#13;
 reddito  tassato  a  fini IRPEF è affidata alla discrezionalità del    &#13;
 legislatore,   che   rimane    insindacabile    nel    giudizio    di    &#13;
 costituzionalità a meno che non trasmodi in arbitrio;                   &#13;
      che  non  sussiste  disparità  di  trattamento  tributario  tra    &#13;
 l'assegno periodico al coniuge rispetto a quello di mantenimento  dei    &#13;
 figli  in quanto l'uno costituisce una perdita economica del soggetto    &#13;
 erogatore  mentre  l'altro  rappresenta  adempimento  di  un  obbligo    &#13;
 sancito  dagli  artt.  147  e  148 cod. civ. per i figli a carico dei    &#13;
 genitori, che non viene meno a seguito di  separazione  legale  o  di    &#13;
 scioglimento del matrimonio;                                             &#13;
      che  non sussiste violazione degli artt. 29 e 30 Cost. in quanto    &#13;
 la norma censurata non contrasta né con i doveri del matrimonio  né    &#13;
 con  quello  dei genitori di mantenere, istruire ed educare la prole,    &#13;
 mentre   proprio   la   detraibilità   dell'assegno   in   questione    &#13;
 risulterebbe  di  danno  all'unità familiare, permanendo la quale il    &#13;
 soggetto tenuto al  dovere  suddetto  non  gode  della  detraibilità    &#13;
 stessa;                                                                  &#13;
      che  non sussiste violazione dell'art. 53 Cost., in quanto, come    &#13;
 più volte ritenuto da questa Corte (v. sentt. nn. 97 del 1968  e  91    &#13;
 del  1982),  per  capacità  contributiva  si intende l'idoneità del    &#13;
 soggetto contribuente a  corrispondere  la  prestazione  imposta,  da    &#13;
 porre  in  relazione non alla concreta capacità di ciascun soggetto,    &#13;
 ma al presupposto al quale la prestazione stessa è collegata e  agli    &#13;
 elementi essenziali dell'obbligazione tributaria;                        &#13;
      che,   pertanto,   la   questione  sollevata  è  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 10, primo comma, lett.  g),  del  d.P.R.  29    &#13;
 settembre  1973,  n.  597  (Istituzione e disciplina dell'imposta sul    &#13;
 reddito delle persone fisiche), come  modificato  dall'art.  5  della    &#13;
 legge   13   aprile  1977,  n.  114  (Modificazioni  alla  disciplina    &#13;
 dell'imposta  sul  reddito  delle  persone  fisiche),  sollevata,  in    &#13;
 riferimento  agli  artt.  3,  29,  30  e  53 Cost., dalla Commissione    &#13;
 Tributaria di I grado di Sanremo con l'ordinanza in epigrafe.            &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, l'8 luglio 1988.           &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 29 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
