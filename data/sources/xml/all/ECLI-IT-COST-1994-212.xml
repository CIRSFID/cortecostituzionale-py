<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>212</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:212</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PESCATORE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/05/1994</data_decisione>
    <data_deposito>02/06/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Gabriele PESCATORE; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  promosso  con ricorso del Presidente del Consiglio dei    &#13;
 ministri notificato l'8 gennaio 1994, depositato in Cancelleria il 18    &#13;
 successivo, per conflitto  di  attribuzione  sorto  a  seguito  della    &#13;
 "dichiarazione  di  intenti" sottoscritta a Palermo il 20 maggio 1993    &#13;
 dall'Assessore regionale al turismo della  Regione  Siciliana  e  dal    &#13;
 Ministro  del  turismo e dell'artigianato della Repubblica di Tunisia    &#13;
 ed iscritto al n. 2 del registro conflitti 1994;                         &#13;
    Udito nell'udienza pubblica del 10 maggio 1994 il Giudice relatore    &#13;
 Cesare Mirabelli;                                                        &#13;
    Udito l'avvocato dello Stato Giorgio Zagari per il Presidente  del    &#13;
 Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con ricorso notificato l'8 gennaio 1994 e depositato il successivo    &#13;
 18  gennaio  il  Presidente  del  Consiglio  dei ministri ha proposto    &#13;
 conflitto di attribuzione nei confronti della Regione Siciliana,  per    &#13;
 far  dichiarare  che  non  spetta ad essa stipulare un accordo con la    &#13;
 Repubblica di Tunisia in materia di turismo,  senza  avere  informato    &#13;
 preventivamente  il  Governo per acquisirne la necessaria intesa o il    &#13;
 consenso. Di conseguenza  il  ricorrente  chiede  che  sia  annullata    &#13;
 l'adesione della Regione alla "dichiarazione di intenti" sottoscritta    &#13;
 a Palermo il 20 maggio 1993 dall'Assessore regionale al turismo e dal    &#13;
 Ministro  del  turismo  della  Repubblica  di  Tunisia.  Questo  atto    &#13;
 invaderebbe competenze riservate allo Stato in  materia  di  politica    &#13;
 estera  e  di  stipulazione di accordi con altri Stati. La lesione si    &#13;
 manifesterebbe sia in aspetti attinenti alla procedura ed alla forma,    &#13;
 che nel contenuto dell'atto.                                             &#13;
    Quanto al primo aspetto, il Governo è venuto a  conoscenza  della    &#13;
 sottoscrizione  dell'accordo solo a seguito di informazioni richieste    &#13;
 alla Regione e da questa trasmesse con nota datata 3  novembre  1993,    &#13;
 pervenuta  alla  Presidenza del Consiglio dei ministri il 10 novembre    &#13;
 successivo. Inoltre la "dichiarazione di intenti" sarebbe stata posta    &#13;
 in essere dalla Regione con un ente non omologo e sottoscritta da  un    &#13;
 assessore, organo a ciò non abilitato.                                  &#13;
    Quanto   al   contenuto,   la   dichiarazione   di  intenti  muove    &#13;
 espressamente "nell'ambito dei rapporti di  amicizia  e  cooperazione    &#13;
 tra  la  Repubblica  di  Tunisia  e  la  Repubblica  Italiana"  ed è    &#13;
 orientata a promuovere ed incoraggiare gli scambi turistici, le borse    &#13;
 di studio, l'apertura di rappresentanze in questo settore, i contatti    &#13;
 tra  operatori  turistici, l'integrazione delle stazioni portuali nei    &#13;
 circuiti delle crociere  e  lo  sviluppo  del  turismo  nautico,  con    &#13;
 aspetti  che  il  Governo  ritiene  vengano ad incidere nella materia    &#13;
 dell'immigrazione. La dichiarazione di intenti  toccherebbe,  quindi,    &#13;
 competenze riservate in via esclusiva allo Stato.<diritto>Considerato in diritto</diritto>1.   -  Il  Presidente  del  Consiglio  dei  ministri,  proponendo    &#13;
 conflitto di attribuzione nei confronti della Regione  Siciliana,  ha    &#13;
 chiesto  l'annullamento della "dichiarazione di intenti" sottoscritta    &#13;
 a Palermo il 20 maggio 1993 dall'Assessore regionale al turismo e dal    &#13;
 Ministro del turismo della Repubblica di Tunisia, in  quanto:  a)  è    &#13;
 stata negoziata e sottoscritta senza informare previamente il Governo    &#13;
 e  senza  averne ottenuto la necessaria intesa, accordo o assenso; b)    &#13;
 è stata stipulata con un ente non omologo alla Regione, superando  i    &#13;
 limiti  propri  degli  "atti  di  mero rilievo internazionale"; c) è    &#13;
 stata sottoscritta da un organo regionale, l'Assessore al turismo,  a    &#13;
 ciò non abilitato; d) toccherebbe una materia di competenza statale.    &#13;
    2. - Il ricorso è fondato.                                           &#13;
    La  giurisprudenza  costituzionale  ha più volte affermato che la    &#13;
 sottoscrizione di accordi con organi  o  enti  esteri  senza  che  la    &#13;
 Regione  abbia  preventivamente informato il Governo, quindi senza la    &#13;
 necessaria intesa o assenso, è di per  sé  lesiva  della  sfera  di    &#13;
 attribuzioni statali (sentenze n. 204 e 290 del 1993).                   &#13;
    La   "dichiarazione   di   intenti"   in  questione,  sottoscritta    &#13;
 dall'Assessore al turismo della Regione Siciliana senza la preventiva    &#13;
 intesa o assenso del Governo, è lesiva della sfera  di  attribuzioni    &#13;
 dello    Stato,   indipendentemente   da   ogni   valutazione   della    &#13;
 riconducibilità delle materie trattate dalla "dichiarazione"  stessa    &#13;
 alla sfera di attribuzioni regionali.                                    &#13;
    La  "dichiarazione  di  intenti"  deve essere pertanto, per quanto    &#13;
 concerne gli atti posti in essere dalla Regione Siciliana, annullata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara che non spetta alla Regione  il  potere  di  stipulare  la    &#13;
 "dichiarazione  di  intenti" sottoscritta a Palermo il 20 maggio 1993    &#13;
 dall'Assessore al turismo della Regione Siciliana, e  di  conseguenza    &#13;
 annulla tale atto.                                                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 maggio 1994.                               &#13;
                       Il Presidente: PESCATORE                           &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 2 giugno 1994.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
