<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>286</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:286</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>19/06/2002</data_decisione>
    <data_deposito>26/06/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
FLICK, Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 263 del codice &#13;
di  procedura penale, promosso con ordinanza emessa il 15 maggio 2001 &#13;
dal Tribunale sez. per il riesame di Palermo sull'appello proposto da &#13;
B.F.,  iscritta  al  n. 828  del registro ordinanze 2001 e pubblicata &#13;
nella  Gazzetta  Ufficiale della Repubblica n. 42, 1ª serie speciale, &#13;
dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 10 aprile 2002 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto che con ordinanza emessa il 15 maggio 2001, il Tribunale &#13;
di  Palermo  ha  sollevato,  in  riferimento  agli artt. 3 e 24 della &#13;
Costituzione,  questione di legittimità costituzionale dell'art. 263 &#13;
del  codice  di procedura penale, nella parte in cui non prevede che, &#13;
dopo  la  chiusura  delle indagini preliminari e fino al passaggio in &#13;
giudicato  della  sentenza o del decreto di condanna, gli interessati &#13;
possano   proporre   opposizione   avverso   l'ordinanza  di  rigetto &#13;
dell'istanza   di   restituzione   di  cose  sottoposte  a  sequestro &#13;
probatorio  "innanzi  al  medesimo giudice a norma dell'art. 127 cod. &#13;
proc. pen."; &#13;
        che   il   giudice  a  quo  premette  di  essere  chiamato  a &#13;
pronunciare  sull'appello  proposto dall'imputato avverso l'ordinanza &#13;
con la quale, dopo la chiusura delle indagini preliminari, il giudice &#13;
competente  aveva  respinto l'istanza di restituzione di beni oggetto &#13;
di sequestro probatorio; &#13;
        che  il rimettente rileva come l'impugnazione dovrebbe essere &#13;
dichiarata inammissibile, dato che nessuna norma prevede l'appello al &#13;
"tribunale  della  libertà"  contro  i  provvedimenti  in materia di &#13;
restituzione  di  cose  sottoposte  a  sequestro probatorio (e ciò a &#13;
differenza  che  per  il  sequestro  preventivo, ex art. 322-bis cod. &#13;
proc. pen.); &#13;
        che  i predetti provvedimenti, d'altro canto, ove pronunciati &#13;
(come  nella  specie)  dopo  la chiusura delle indagini preliminari e &#13;
prima  del passaggio in giudicato della sentenza o del decreto penale &#13;
di   condanna,   non   sarebbero   neppure   impugnabili   -  secondo &#13;
l'orientamento  giurisprudenziale  al quale il giudice a quo dichiara &#13;
di aderire - tramite il diverso rimedio dell'opposizione; &#13;
        che  la  norma impugnata determinerebbe, peraltro, sotto tale &#13;
profilo, una ingiustificata disparità di trattamento fra il soggetto &#13;
che  chiede la restituzione nell'arco temporale dianzi indicato ed il &#13;
soggetto  che  formula  la medesima istanza nella fase delle indagini &#13;
preliminari,  ovvero  dopo  il passaggio in giudicato della sentenza: &#13;
ipotesi,  queste ultime, nelle quali i commi 5 e 6 dell'art. 263 cod. &#13;
proc.   pen. consentono   viceversa   all'interessato   di   proporre &#13;
opposizione   avverso   il   provvedimento   di   rigetto  (adottato, &#13;
rispettivamente,    dal    pubblico    ministero    e   dal   giudice &#13;
dell'esecuzione), sia pure con diverse procedure di tipo camerale; &#13;
        che  ne  deriverebbe  anche  una  violazione  del  diritto di &#13;
difesa,  venendo  negata all'interessato quella facoltà di sostenere &#13;
la  propria istanza di restituzione mediante proposizione di gravame, &#13;
che  gli  è  riconosciuta,  invece,  tanto nella fase delle indagini &#13;
preliminari che dopo il passaggio in giudicato della sentenza; &#13;
        che  la  piena  conformità  ai  parametri costituzionali del &#13;
procedimento  di  restituzione  delle  cose sequestrate resterebbe di &#13;
contro assicurata - ad avviso del rimettente - ove venisse attribuita &#13;
all'interessato,  nell'ipotesi  in questione, la facoltà di proporre &#13;
opposizione   dinanzi   al   medesimo  giudice  che  ha  adottato  il &#13;
provvedimento  di  rigetto, con applicazione della procedura camerale &#13;
prevista dall'art. 127 cod. proc. pen; &#13;
        che  la  questione - secondo il rimettente - sarebbe altresì &#13;
rilevante  nel  giudizio a quo giacché, in caso di suo accoglimento, &#13;
l'impugnazione   proposta,   anziché  essere  dichiarata  "meramente &#13;
inammissibile,  potrebbe  essere  riqualificata e trasmessa, ai sensi &#13;
del comma 5 dell'art. 568 cod. proc. pen., al giudice competente, per &#13;
il giudizio di opposizione"; &#13;
        che  nel  giudizio  di  costituzionalità  è  intervenuto il &#13;
Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso &#13;
dall'Avvocatura  generale dello Stato, chiedendo che la questione sia &#13;
dichiarata inammissibile o infondata. &#13;
    Considerato che - conformemente a quanto già affermato da questa &#13;
Corte  in  riferimento  a  quesiti  di  costituzionalità  del  tutto &#13;
analoghi  (cfr.  ordinanza  n. 409 del 2001) - la questione sollevata &#13;
dal Tribunale di Palermo risulta affatto irrilevante nel procedimento &#13;
a quo; &#13;
        che   il   Tribunale  rimettente  risulta  infatti  investito &#13;
dell'appello  de libertate erroneamente proposto - secondo il modello &#13;
stabilito  dall'art. 322-bis  cod.  proc.  pen. in  rapporto  al solo &#13;
sequestro   preventivo   -   avverso   il  provvedimento  di  rigetto &#13;
dell'istanza   di   restituzione   di  beni  sottoposti  a  sequestro &#13;
probatorio:  appello  che - come lo stesso giudice a quo non dubita - &#13;
è da ritenere dunque inammissibile; &#13;
        che,   d'altro  canto,  la  sentenza  additiva  invocata  dal &#13;
rimettente  non  varrebbe  a  devolvere  al  rimettente  medesimo  la &#13;
cognizione   del  gravame,  ma  introdurrebbe  una  nuova  figura  di &#13;
"opposizione"  davanti ad un diverso, e cioè allo stesso giudice che &#13;
ha adottato il provvedimento reiettivo dell'istanza di restituzione; &#13;
        che, pertanto, il quesito di costituzionalità verte su norma &#13;
della quale il giudice a quo non è e non sarebbe comunque chiamato a &#13;
fare applicazione; &#13;
        che  è  inconferente,  in  senso  contrario,  il rilievo del &#13;
rimettente,   secondo  il  quale,  nel  caso  di  accoglimento  della &#13;
questione,   il   tribunale   -   anziché  limitarsi  alla  semplice &#13;
declaratoria    di    inammissibilità    dell'appello   -   potrebbe &#13;
"riqualificare"  l'impugnazione  come "opposizione", trasmettendo gli &#13;
atti  al  giudice  competente:  giacché  l'epilogo  sarebbe comunque &#13;
l'inapplicabilità della norma impugnata da parte del giudice a quo; &#13;
        che   la   questione   va  dunque  dichiarata  manifestamente &#13;
inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  dell'art. 263  del  codice di procedura &#13;
penale,   sollevata,   in   riferimento   agli  artt. 3  e  24  della &#13;
Costituzione, dal Tribunale di Palermo con l'ordinanza in epigrafe. &#13;
    Così  deciso,  in  Roma,  nella sede della Corte Costituzionale, &#13;
Palazzo della Consulta, il 19 giugno 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 26 giugno 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
