<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>432</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:432</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1996</data_decisione>
    <data_deposito>30/12/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale degli artt. 1, 2,  3  e  6    &#13;
 del  d.-l.  17  marzo  1995,  n.  79 (Modifiche alla disciplina degli    &#13;
 scarichi delle pubbliche fognature e degli  insediamenti  civili  che    &#13;
 non    recapitano    in   pubbliche   fognature),   convertito,   con    &#13;
 modificazioni, nella legge 17 maggio 1995, n. 172, dell'art. 1  della    &#13;
 legge  17  maggio  1995,  n.  172 e dell'art. 21, quinto comma, della    &#13;
 legge lo maggio 1976,  n.  319  (Norme  per  la  tutela  delle  acque    &#13;
 dall'inquinamento), promossi con ordinanze emesse il 29 gennaio 1996,    &#13;
 l'8  febbraio  1996,  il  13 febbraio 1996, il 9 febbraio 1996, il 19    &#13;
 gennaio 1996, il 18 gennaio 1996 e il 19 gennaio 1996 dal giudice per    &#13;
 le indagini preliminari presso la pretura di Udine, il 6  marzo  1996    &#13;
 dal   giudice   per   le   indagini  preliminari  presso  la  pretura    &#13;
 circondariale di Prato e il  15  marzo  1996  dal  pretore  di  Pisa,    &#13;
 sezione  distaccata  di  San Miniato, rispettivamente iscritte ai nn.    &#13;
 377, 378, 379, 380, 381, 382, 383, 485 e 628 del  registro  ordinanze    &#13;
 1996  e  pubblicate nella Gazzetta Ufficiale della Repubblica nn. 19,    &#13;
 22 e 28, prima serie speciale, dell'anno 1996;                           &#13;
   Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
   Udito nella camera di consiglio del  16  ottobre  1996  il  giudice    &#13;
 relatore Cesare Mirabelli.                                               &#13;
   Ritenuto  che  con  nove ordinanze, emesse nel corso di altrettanti    &#13;
 procedimenti penali promossi per violazioni delle norme per la tutela    &#13;
 delle acque dall'inquinamento (art. 21, primo e  terzo  comma,  della    &#13;
 legge  10  maggio  1976,  n. 319) , sono state sollevate questioni di    &#13;
 legittimità costituzionale di alcune disposizioni del  decreto-legge    &#13;
 17  marzo 1995, n. 79 (Modifiche alla disciplina degli scarichi delle    &#13;
 pubbliche fognature e degli insediamenti civili che non recapitano in    &#13;
 pubbliche fognature), convertito, con modificazioni, nella  legge  17    &#13;
 maggio  1995, n. 172; ovvero questioni di legittimità costituzionale    &#13;
 della medesima legge di conversione, o, ancora, della legge 10 maggio    &#13;
 1976, n. 319 (Norme per la  tutela  delle  acque  dall'inquinamento),    &#13;
 come modificata dal citato decreto-legge;                                &#13;
     che,  in  particolare,  il  giudice  per  le indagini preliminari    &#13;
 presso la pretura circondariale di Udine,   con  sette  ordinanze  di    &#13;
 identico  contenuto emesse il 18 gennaio (reg. ord. n. 382 del 1996),    &#13;
 il 19 gennaio (reg. ord. nn. 381 e 383 del 1996), il 29 gennaio (reg.    &#13;
 ord.  n. 377 del 1996), l'8 febbraio (reg. ord. n. 378 del 1996),  il    &#13;
 9  febbraio  (reg. ord. n. 380 del 1996) ed il 13 febbraio 1996 (reg.    &#13;
 ord. n. 379 del 1996), ha sollevato, in riferimento agli artt. 3,  9,    &#13;
 secondo  comma,  32,  10, 25, secondo comma, e 77 della Costituzione,    &#13;
 questioni di legittimità costituzionale dell'art. 3, comma 1,  prima    &#13;
 parte,  del  decreto-legge  n.  79  del  1995.  Il giudice rimettente    &#13;
 ritiene che la norma denunciata, non configurando più come reato, ma    &#13;
 come  illecito  amministrativo,  il   superamento   dei   limiti   di    &#13;
 accettabilità  stabiliti  dalle  Regioni  con i piani di risanamento    &#13;
 delle acque  per  gli  scarichi  diversi  da  quelli  provenienti  da    &#13;
 insediamenti produttivi, contrasterebbe:                                 &#13;
      a)  con  il  principio  di  eguaglianza e ragionevolezza (art. 3    &#13;
 della   Costituzione),   giacché   la   diversità    di    sanzioni    &#13;
 (amministrative  per gli scarichi civili e delle pubbliche fognature,    &#13;
 penali per gli scarichi da insediamenti produttivi)  sarebbe  fondata    &#13;
 non  sulla  diversa gravità dei fatti, ma sulla differente qualifica    &#13;
 di chi li effettua;                                                      &#13;
      b) con la tutela del paesaggio (art.  9,  secondo  comma,  della    &#13;
 Costituzione)  e della salute (art. 32 della Costituzione), in quanto    &#13;
 la  depenalizzazione  di   alcuni   comportamenti,   che   egualmente    &#13;
 determinano  inquinamento idrico, ridurrebbe il livello di protezione    &#13;
 della salubrità dell'ambiente;                                          &#13;
      c) con l'obbligo di adeguamento al diritto  comunitario,  ed  in    &#13;
 particolare alla direttiva 91/271/CEE (art. 10 della Costituzione);      &#13;
      d) con gli artt. 25, secondo comma, e 77 della Costituzione, per    &#13;
 la   mancanza  degli  indispensabili  requisiti  della  necessità  e    &#13;
 dell'urgenza, in una materia, quella penale, nella  quale  l'uso  del    &#13;
 decreto-legge  dovrebbe  essere del tutto eccezionale, per evitare il    &#13;
 rischio di sottrarre al Parlamento la funzione ad  esso  riservata  e    &#13;
 che dà corpo alla riserva di legge;                                     &#13;
     che  il  giudice  per  le  indagini preliminari presso la pretura    &#13;
 circondariale di Prato, con ordinanza emessa il 6  marzo  1996  (reg.    &#13;
 ord.  n.  485  del  1996)  ,  ha  sollevato questione di legittimità    &#13;
 costituzionale dell'art. 6, comma 2,  del  decreto-legge  n.  79  del    &#13;
 1995;  dell'art.    1  della  legge  n. 172 del 1995, che converte il    &#13;
 decreto-legge n. 79 del 1995; dell'art. 21, quinto comma, della legge    &#13;
 n.  319 del 1976, aggiunto dall'art. 6, comma 2, del decreto-legge n.    &#13;
 79 del 1995, prospettando la violazione degli artt. 3,  primo  comma,    &#13;
 9,  secondo  comma, 32, 25, secondo comma, e 77, secondo comma, della    &#13;
 Costituzione in termini analoghi a quelli in precedenza indicati.  Il    &#13;
 giudice  rimettente  dubita  della  costituzionalità della norma che    &#13;
 colpisce con sanzione pecuniaria amministrativa  (da  dieci  a  cento    &#13;
 milioni   di   lire),  anziché  con  l'originaria  sanzione  penale,    &#13;
 l'apertura o l'effettuazione di scarichi  civili  e  delle  pubbliche    &#13;
 fognature  senza avere richiesto la prescritta autorizzazione, ovvero    &#13;
 dopo che l'autorizzazione sia stata negata o revocata;                   &#13;
     che il pretore di Pisa, sezione distaccata di  San  Miniato,  con    &#13;
 ordinanza  emessa  il  15 marzo 1996 (reg. ord. n. 628 del 1996),  ha    &#13;
 sollevato questione di legittimità costituzionale degli artt. 1,  2,    &#13;
 3   e   6  della  legge  n.  172  del  1995  (più  esattamente:  del    &#13;
 decreto-legge n. 79 del 1995, convertito,  con  modificazioni,  nella    &#13;
 legge  n.  172  del  1995), là dove, per gli scarichi civili e delle    &#13;
 pubbliche fognature, depenalizzano sia il superamento dei  limiti  di    &#13;
 accettabilità  sia l'apertura o l'effettuazione degli scarichi senza    &#13;
 avere richiesto la prescritta  autorizzazione,  ovvero  dopo  che  la    &#13;
 prescritta  autorizzazione  sia  stata  negata o revocata. Il giudice    &#13;
 rimettente denuncia, in termini analoghi a quelli  prospettati  dalle    &#13;
 precedenti   ordinanze   di   rimessione,  la  lesione  dei  principi    &#13;
 costituzionali di parità di trattamento e di ragionevolezza (art.  3    &#13;
 della   Costituzione),   come   pure   la   violazione   delle  norme    &#13;
 costituzionali di tutela  del  paesaggio  e  della  salute  (art.  9,    &#13;
 secondo  comma,  e 32 della Costituzione). Lo stesso giudice denuncia    &#13;
 anche la violazione della libertà di  iniziativa  economica  privata    &#13;
 (art.  41  della Costituzione), sia perché questa non può svolgersi    &#13;
 in contrasto con l'utilità sociale,  alla  quale  è  da  ricondurre    &#13;
 anche   il   principio  "chi  inquina  paga"  posto  dalla  normativa    &#13;
 comunitaria, sia perché sarebbero penalizzate le imprese  che  hanno    &#13;
 affrontato  rilevanti  investimenti per adeguare gli scarichi che non    &#13;
 recapitano in pubbliche fognature alla normativa in vigore;              &#13;
     che in tutti i giudizi, tranne in quello promosso con l'ordinanza    &#13;
 del  Giudice  per  le  indagini   preliminari   presso   la   Pretura    &#13;
 circondariale di Prato (reg. ord. n. 485 del 1996), è intervenuto il    &#13;
 Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura generale dello  Stato,  chiedendo  che  le  questioni    &#13;
 siano dichiarate inammissibili o non fondate;                            &#13;
   Considerato che i dubbi di legittimità costituzionale investono le    &#13;
 innovazioni  alle  norme  per la tutela delle acque dall'inquinamento    &#13;
 (legge 10 maggio 1976, n. 319), introdotte con  il  decreto-legge  17    &#13;
 marzo  1995,  n.  79  (Modifiche alla disciplina degli scarichi delle    &#13;
 pubbliche fognature e degli insediamenti civili che non recapitano in    &#13;
 pubbliche fognature), convertito, con modificazioni, nella  legge  17    &#13;
 maggio 1995, n. 172;                                                     &#13;
     che   tutte   le  ordinanze  di  rimessione  sollevano  questioni    &#13;
 identiche o analoghe,  sicché  i  relativi  giudizi  possono  essere    &#13;
 riuniti e decisi con unica pronuncia;                                    &#13;
     che  i  dubbi  di  legittimità  costituzionale,  prospettati  in    &#13;
 riferimento a vari  parametri,  si  riferiscono  alla  configurazione    &#13;
 dell'illecito   come   amministrativo,   anziché   penale,  ed  alla    &#13;
 disciplina  delle  sanzioni   per   gli   scarichi   provenienti   da    &#13;
 insediamenti    civili   o   da   pubbliche   fognature;   disciplina    &#13;
 differenziata rispetto a quella prevista per gli scarichi provenienti    &#13;
 da insediamenti produttivi senza autorizzazione o con superamento dei    &#13;
 limiti di accettabilità;                                                &#13;
     che le  questioni  sollevate  sono  manifestamente  inammissibili    &#13;
 (sentenza  n.  330  del  1996;  ordinanza  n. 332 del 1996), giacché    &#13;
 tendono a reintrodurre figure di reato, chiedendo una  pronuncia  che    &#13;
 esula  dai  poteri  spettanti  a questa Corte, in quanto il potere di    &#13;
 creare fattispecie penali o di aggravare le  pene  è  esclusivamente    &#13;
 riservato al legislatore, in forza del principio di stretta legalità    &#13;
 dei  reati  e  delle pene, sancito dall'art. 25, secondo comma, della    &#13;
 Costituzione (tra le molte, da ultimo, sentenza n. 411  del  1995  e,    &#13;
 nella  materia  della  tutela  delle  acque dall'inquinamento idrico,    &#13;
 sentenze nn. 314 e 226 del 1983; ordinanze nn. 132 e 25 del 1995);       &#13;
     che, in ogni caso, per i vizi  denunciati  con  riferimento  alla    &#13;
 mancanza  dei  presupposti  straordinari  di  necessità  ed urgenza,    &#13;
 oggetto  del  giudizio   di   legittimità   costituzionale   è   il    &#13;
 decreto-legge  n.    79 del 1995, per il quale, a prescindere da ogni    &#13;
 valutazione relativa all'avvenuta conversione in legge,  va  rilevato    &#13;
 che  la  Corte  ha  già ritenuto che di quei presupposti non ricorre    &#13;
 l'evidente mancanza (sentenza n. 330 del 1996);                          &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi, dichiara  la  manifesta  inammissibilità  delle    &#13;
 questioni  di legittimità costituzionale degli artt. 1, 2, 3 e 6 del    &#13;
 d.-l. 17 marzo 1995, n. 79 (Modifiche alla disciplina degli  scarichi    &#13;
 delle  pubbliche  fognature  e  degli  insediamenti  civili  che  non    &#13;
 recapitano in pubbliche fognature),  convertito,  con  modificazioni,    &#13;
 nella legge 17 maggio 1995, n. 172, dell'art. 1 della legge 17 maggio    &#13;
 1995,  n.    172  e dell'art. 21, quinto comma, della legge 10 maggio    &#13;
 1976, n.  319 (Norme per la tutela  delle  acque  dall'inquinamento),    &#13;
 sollevate,  in  riferimento  agli  artt. 3, 9, secondo comma, 10, 25,    &#13;
 secondo comma, 32, 41 e 77 della Costituzione,  dai  Giudici  per  le    &#13;
 indagini  preliminari  presso  le Preture circondariali di Udine e di    &#13;
 Prato e dal Pretore di Pisa, sezione distaccata di San  Miniato,  con    &#13;
 le ordinanze indicate in epigrafe.                                       &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 dicembre 1996.                             &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Mirabelli                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 30 dicembre 1996.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
