<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>61</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:61</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Paolo Maddalena</relatore_pronuncia>
    <redattore_pronuncia>Paolo Maddalena</redattore_pronuncia>
    <data_decisione>10/02/2003</data_decisione>
    <data_deposito>28/02/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 4, comma 1, della legge 6 ottobre 1986, n. 656 (Modifiche ed integrazioni alla normativa sulle pensioni di guerra), promosso con ordinanza emessa il 14 novembre 2001 dal giudice unico delle pensioni della Corte dei conti - sezione giurisdizionale per la Regione Veneto sul ricorso proposto da Rampinelli Maria contro il Ministero del tesoro, del bilancio e della programmazione economica, iscritta al n. 65 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 8, prima serie speciale, dell'anno 2002. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 29 gennaio 2003 il Giudice relatore Paolo Maddalena. &#13;
    Ritenuto che, con ordinanza del 14 novembre 2001 (r.o. n. 65 del 2002), il giudice unico delle pensioni della Corte dei conti - sezione giurisdizionale per la Regione Veneto ha sollevato questione di legittimità costituzionale dell'art. 4, comma 1, della legge 6 ottobre 1986, n. 656 (Modifiche ed integrazioni alla normativa sulle pensioni di guerra) - che sostituisce, con decorrenza dal 1° gennaio 1985, l'art. 9 del d.P.R. 30 dicembre 1981, n. 834 (Definitivo riordinamento delle pensioni di guerra, in attuazione della delega prevista dall'art. 1 della legge 23 settembre 1981, n. 533) che, a sua volta, inserisce, dopo il terzo, tre commi all'art. 38 del d.P.R. 23 dicembre 1978, n. 915 (Testo unico delle norme in materia di pensioni di guerra) -, per contrasto con l'art. 3 della Costituzione, nella parte in cui prevede la convivenza quale presupposto per l'erogazione dell'assegno supplementare previsto dall'art. 3 del d.P.R. n. 834 del 1981; &#13;
    che, secondo il giudice a quo, la scelta legislativa di ancorare la concessione dell'assegno supplementare in questione al requisito della convivenza sarebbe illegittima, comportando una discriminazione tra situazioni soggettive identiche, poiché entrambe qualificate da rapporti di coniugio; infatti, il legislatore avrebbe riservato un trattamento più favorevole a chi percepisce la pensione ordinaria di reversibilità, rispetto a chi è titolare di pensione di reversibilità di guerra; &#13;
    che, inoltre, la “convivenza” intesa come coabitazione, finisce con il “disconoscere il diritto di ciascuno dei coniugi di fissare liberamente la propria residenza”; &#13;
    che il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha sostenuto l'infondatezza della questione sollevata con l'ordinanza in epigrafe; &#13;
    che la difesa erariale ha ritenuto, in primo luogo, la diversa natura dei due assegni principali, pensione ordinaria e pensione di guerra, avendo l'una carattere di retribuzione differita e l'altra carattere risarcitorio e che, inoltre, il fondamento giuridico, che rende evidente la ratio della norma, è costituito non solo dal normale rapporto affettivo, ma anche dalla costante assistenza al mutilato e all'invalido di guerra di prima categoria, garantita, appunto, dalla convivenza. &#13;
    Considerato che, a ben vedere, il remittente pone a raffronto situazioni tra loro non omogenee, poiché, come questa Corte ha avuto più volte modo di affermare (sentenze n. 186 del 1985 e n. 70 del 1999), la pensione di reversibilità ordinaria ha natura retributiva, mentre quella di guerra ha natura risarcitoria; sicché, data la disomogeneità delle situazioni poste a raffronto, il remittente ha, nella specie, fatto riferimento ad un tertium comparationis assolutamente improponibile; &#13;
    che non è irragionevole ed ingiustificata la ratio della disciplina legislativa in questione, la quale tiene conto di una situazione completamente diversa da quella relativa alla reversibilità delle pensioni ordinarie e persegue lo scopo di assicurare un ristoro economico ulteriore a chi abbia prestato una costante e diuturna assistenza al mutilato o all'invalido di guerra, circostanza che può maturare soltanto in una situazione di convivenza, il cui accertamento è rimesso alla valutazione del giudice di merito; &#13;
    che, comunque, data la ratio della disposizione censurata, la convivenza intesa come situazione fattuale di coabitazione, e non invece quale formale coincidenza anagrafica, non coarta la libertà del coniuge di scegliere liberamente la propria residenza, ma costituisce soltanto un ragionevole presupposto per la concessione di un ulteriore beneficio economico;  &#13;
    che, pertanto, la prospettata questione di legittimità costituzionale riferita al citato art. 4, comma 1, della legge 6 ottobre 1986, n. 656, per contrasto con l'art. 3 della Costituzione, appare manifestamente infondata. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 4, comma 1, della legge 6 ottobre 1986, n. 656 (Modifiche ed integrazioni alla normativa sulle pensioni di guerra) - che sostituisce, con decorrenza dal 1° gennaio 1985, l'art. 9 del d.P.R. 30 dicembre 1981, n. 834 (Definitivo riordinamento delle pensioni di guerra, in attuazione della delega prevista dall'art. 1 della legge 23 settembre 1981, n. 533) che, a sua volta, inserisce, dopo il terzo, tre commi all'art. 38 del d.P.R. 23 dicembre 1978, n. 915 (Testo unico delle norme in materia di pensioni di guerra) - sollevata, in riferimento all'art. 3 della Costituzione, dal giudice unico delle pensioni della Corte dei conti - sezione giurisdizionale per la Regione Veneto, con l'ordinanza in epigrafe. &#13;
  &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 febbraio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Paolo MADDALENA, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 28 febbraio 2003. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
