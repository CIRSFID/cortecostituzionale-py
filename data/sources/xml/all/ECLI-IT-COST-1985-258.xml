<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>258</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:258</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/10/1985</data_decisione>
    <data_deposito>04/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di  legittimità  costituzionale  dell'art.  2,  &#13;
 quinto  comma,  seconda parte della legge 30 aprile 1976, n.  159 così  &#13;
 come modificato  dall'art.  3  della  legge  8  ottobre  1976,  n.  689  &#13;
 (Conversione  in  legge  del  decreto-legge  10  agosto  1976,  n. 543,  &#13;
 concernente modifica dell'art. 2 della legge 30 aprile  1976,  n.  159,  &#13;
 nella  quale è stato convertito, con modificazioni, il decreto-legge 4  &#13;
 marzo 1976,  n.  31,  contenente  disposizioni  penali  in  materia  di  &#13;
 infrazioni  valutarie.  Ulteriori  modifiche  al  decreto-legge 4 marzo  &#13;
 1976, n. 31, e alla legge 30 aprile 1976, n.  159),  promossi  con  due  &#13;
 ordinanze  emesse  il  6  dicembre  1983  dalla Corte di Cassazione sui  &#13;
 ricorsi proposti da Gambarino Carlo e Oliviero Antonio, iscritte ai nn.  &#13;
 945 e 946 del registro  ordinanze  1984  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica nn. 19 bis e 13 bis dell'anno 1985.           &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio  del  9  ottobre  1985  il  Giudice  &#13;
 relatore Alberto Malagugini.                                             &#13;
     Ritenuto  che  con  le  ordinanze  indicate in epigrafe la Corte di  &#13;
 Cassazione dubita della legittimità costituzionale dell'art.  2, primo  &#13;
 comma, lett. d) e quinto comma, della legge 30 aprile 1976, n. 159, nel  &#13;
 testo sostituito dall'art. 3 della legge 8 ottobre  1976,  n.  689,  in  &#13;
 quanto tale norma da un lato impone ai cittadini italiani possessori di  &#13;
 natanti   non   iscritti   in  pubblici  registri  nazionali  l'obbligo  &#13;
 penalmente sanzionato di dichiararne il possesso e di  venderli  ovvero  &#13;
 importarli  e  nazionalizzarli  a  proprio nome, e dall'altro ricollega  &#13;
 all'osservanza di  tali  prescrizioni  l'esonero  dalle  sole  sanzioni  &#13;
 amministrative  previste  dalle  norme  valutarie e fiscali e non anche  &#13;
 dalle sanzioni penali;                                                   &#13;
     che ad avviso della Corte rimettente tale disciplina contrasterebbe  &#13;
 con l'art. 24, secondo comma, Cost., sostenendosi che l'esecuzione  dei  &#13;
 predetti  adempimenti  darebbe necessariamente luogo ad un'autodenuncia  &#13;
 per il delitto di contrabbando doganale del natante e  che  perciò  il  &#13;
 mancato  esonero dalle relative sanzioni comprometterebbe il diritto di  &#13;
 difesa, che non consente che alcuno sia tenuto  a  confessare  di  aver  &#13;
 commesso un reato;                                                       &#13;
     che  l'Avvocatura dello Stato, intervenuta in entrambi i giudizi in  &#13;
 rappresentanza del Presidente del Consiglio dei  ministri,  ha  chiesto  &#13;
 che la questione, ove ritenuta ammissibile, sia dichiarata non fondata,  &#13;
 essendo stata già decisa con la sentenza n. 236 del 1984.               &#13;
     Considerato  che  in  effetti  tale  questione,  già sollevata nei  &#13;
 medesimi termini e rispetto a fattispecie analoghe da altri giudici, è  &#13;
 stata da questa Corte dichiarata inammissibile con la citata  sentenza:  &#13;
 e  ciò  essenzialmente  sul  rilievo - peraltro condiviso dalla stessa  &#13;
 Corte rimettente (ord. 945/84) -  che  non  vi  è  alcun  rapporto  di  &#13;
 conseguenzialità  necessaria  tra  il  possesso  illecito  del natante  &#13;
 all'estero da parte del residente ed il reato di contrabbando doganale,  &#13;
 il  quale  rappresenta  invece  una  semplice  eventualità  di   fatto  &#13;
 conseguente    all'introduzione    dell'imbarcazione    ed   alla   sua  &#13;
 utilizzazione nel territorio  dello  Stato  in  epoca  precedente  alla  &#13;
 dichiarazione  di  cui all'art. 2, quarto comma della legge n.  159 del  &#13;
 1976;                                                                    &#13;
     che, conseguentemente, le dichiarazioni e gli  adempimenti  di  cui  &#13;
 all'art.  3  della  legge  n. 689 del 1976 attengono a fatti diversi da  &#13;
 quelli costitutivi del delitto di contrabbando  e  non  possono  quindi  &#13;
 essere  considerati,  di  per  sé,  autodenuncia  del  reato medesimo,  &#13;
 eventualmente commesso in epoca precedente;                              &#13;
     che non essendo gli imputati, nei  giudizi  a  quibus,  chiamati  a  &#13;
 rispondere  del reato omissivo di cui al quinto comma, secondo periodo,  &#13;
 dell'art. 2 della legge n. 159 del 1976, nel testo sostituito dall'art.  &#13;
 3 della legge n. 689 del 1976, la Corte rimettente non  è  chiamata  a  &#13;
 fare  applicazione  delle norme da essa censurate, sicché la questione  &#13;
 dedotta si appalesa irrilevante;                                         &#13;
     che pertanto essa va dichiarata manifestamente  inammissibile,  non  &#13;
 essendo  state addotte argomentazioni o profili nuovi rispetto a quelli  &#13;
 già esaminati nella precedente decisione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  manifesta   inammissibilità   della   questione   di  &#13;
 legittimità costituzionale dell'art. 2, primo comma, lett. d) e quinto  &#13;
 comma  della  legge  30  aprile  1976,  n.  159,  nel  testo sostituito  &#13;
 dall'art.  3  della  legge  8  ottobre  1976,  n.  689,  sollevata   in  &#13;
 riferimento   all'art.  24,  secondo  comma,  Cost.    dalla  Corte  di  &#13;
 Cassazione con le due ordinanze in data 6  dicembre  1983  indicate  in  &#13;
 epigrafe.                                                                &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 25 ottobre 1985.        &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   RENATO DELL'ANDRO.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
