<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>325</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:325</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/03/1988</data_decisione>
    <data_deposito>17/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 31, ultimo    &#13;
 comma, del codice di procedura penale, nel testo introdotto dall'art.    &#13;
 1  della  legge  31 luglio 1984, n. 400 (Nuove norme sulla competenza    &#13;
 penale e sull'appello contro le sentenze del pretore),  promosso  con    &#13;
 ordinanza  emessa  il 26 ottobre 1985 dal Pretore di Urbino, iscritta    &#13;
 al n. 865 del registro ordinanze 1985  e  pubblicata  sulla  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  23  prima serie speciale, dell'anno    &#13;
 1986;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  Camera  di consiglio del 10 febbraio 1988 il giudice    &#13;
 relatore Ettore Gallo;                                                   &#13;
    Ritenuto,  in  fatto, che con ordinanza 26 ottobre 1985 il Pretore    &#13;
 di  Urbino  sollevava  questione   di   legittimità   costituzionale    &#13;
 dell'art.  31,  ult.  co.,  cod.  proc.  pen.  , nel testo introdotto    &#13;
 dall'art. 1 legge 31 luglio 1984 n. 400, con riferimento all'art.  3,    &#13;
 primo co., Cost.;                                                        &#13;
      che,   secondo   il   pretore,  l'attribuzione  alla  competenza    &#13;
 pretorile di taluni reati,  che  appartenevano  alla  competenza  del    &#13;
 tribunale, ha determinato una situazione di disuguaglianza per coloro    &#13;
 che hanno commesso il fatto dopo  l'entrata  in  vigore  della  nuova    &#13;
 legge sotto più di un profilo;                                          &#13;
      che  l'incompatibilità  rispetto  al  principio di cui al primo    &#13;
 comma dell'art. 3 Cost. si verificherebbe, infatti, perché  imputati    &#13;
 di  reati  anche  meno gravi, o almeno di pari gravità, e coloro che    &#13;
 hanno commesso il fatto prima  dell'entrata  in  vigore  della  nuova    &#13;
 legge,  restano  affidati  alla  superiore  competenza  di  un organo    &#13;
 collegiale, che offre garenzie di maggiore ponderatezza del giudizio,    &#13;
 e  di  terzietà,  in quanto restano distinte in quel procedimento le    &#13;
 funzioni requirenti da quelle  giudicanti,  a  differenza  di  quanto    &#13;
 accade nel procedimento pretoreo dove lo stesso magistrato giudica al    &#13;
 dibattimento dopo avere magari compiuto atti di istruttoria;             &#13;
      che  si  è  costituito  nel  giudizio  innanzi  alla  Corte  il    &#13;
 Presidente del Consiglio dei ministri, rappresentato  dall'Avvocatura    &#13;
 Generale  dello  Stato,  la  quale  ha chiesto che la questione venga    &#13;
 dichiarata non fondata;                                                  &#13;
    Considerato,   in   diritto,   che   nella   specie   trattasi  di    &#13;
 maltrattamenti nei confronti del coniuge, accertati  dai  carabinieri    &#13;
 in istato di quasi flagranza dell'ultimo episodio, per essere accorsi    &#13;
 a seguito di segnalazione telefonica dei vicini, trovando  tracce  di    &#13;
 violenza  recentissima  sul  mobilio  e sul corpo della moglie, dalla    &#13;
 quale avevano ricevuto denunzia dell'abituale  reiterazione  di  tali    &#13;
 episodi;                                                                 &#13;
      che, pertanto, non sembra sussistessero nella specie particolari    &#13;
 problemi di prova - come si asserisce teoricamente nell'ordinanza- in    &#13;
 guisa da doversi invocare le maggiori garenzie dell'organo collegiale    &#13;
 e dell'istruttoria attribuita ad organo diverso;                         &#13;
      che,  comunque,  questa  Corte  ha  già  valutato gli argomenti    &#13;
 addotti  dal  rimettente  dichiarandoli  non  fondati  con  sent.  15    &#13;
 dicembre 1986 n. 268;                                                    &#13;
      che,  peraltro,  non  sembrano  irrazionali  i criteri di scelta    &#13;
 utilizzati dal legislatore nell'attribuire al pretore  la  competenza    &#13;
 in  ordine a taluni delitti, prescindendo dalla misura della pena, in    &#13;
 quanto è stato dato  rilievo  alla  minore  gravità  sociale  delle    &#13;
 conseguenze,  ed  è  ormai  notorio  che legge-delega e progetto del    &#13;
 nuovo   codice   processuale   prevedono    espressamente    siffatta    &#13;
 attribuzione,  che,  del resto, discende dai poteri discrezionali del    &#13;
 legislatore;                                                             &#13;
      che,  infine, come pure è stato rilevato nella citata sentenza,    &#13;
 non vi può essere diversa alternativa,  quanto  alla  data,  se  non    &#13;
 quella  di far discendere gli effetti voluti a far epoca dall'entrata    &#13;
 in vigore della legge che dispone  un  diverso  assetto  processuale:    &#13;
 salva  l'ipotesi  di  un  diritto  transitorio  per i procedimenti in    &#13;
 corso, che però dipende esclusivamente da quei poteri  discrezionali    &#13;
 del legislatore che in questa sede non possono essere censurati;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 31, ult. co., cod.  proc.  pen.,  nel  testo    &#13;
 introdotto  dall'art.  1 della legge 31 luglio 1984 n. 400, sollevata    &#13;
 dal pretore di Urbino con ordinanza 26 ottobre 1985,  in  riferimento    &#13;
 all'art. 3, primo co., Cost.                                             &#13;
    Così  deciso  in  Roma,  in Camera di Consiglio, nella sede della    &#13;
 Corte Costituzionale, palazzo della Consulta il 10 marzo 1988.           &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 17 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
