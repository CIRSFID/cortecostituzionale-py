<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>12</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:12</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1994</data_decisione>
    <data_deposito>26/01/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 avv Massimo VARI, dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2126 del codice    &#13;
 civile,  promosso  con  ordinanza  emessa  il  17  dicembre  1992 dal    &#13;
 Tribunale amministrativo regionale per l'Abruzzo - Sezione distaccata    &#13;
 di Pescara sul ricorso proposto da  Lizza  Mario  contro  la  USL  di    &#13;
 Pescara,  iscritta al n. 551 del registro ordinanze 1993 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  39,  prima   serie    &#13;
 speciale, dell'anno 1993;                                                &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 12  gennaio  1994  il  Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto  che,  nel  corso di un giudizio promosso dal dott. Mario    &#13;
 Lizza, ispettore sanitario, contro la USL di Pescara per ottenere  la    &#13;
 differenza  di  trattamento  economico  spettantegli in ragione delle    &#13;
 mansioni superiori di responsabile del servizio  di  medicina  legale    &#13;
 svolte dall'8 dicembre 1981 al 31 luglio 1982, il TAR per l'Abruzzo -    &#13;
 Sezione  distaccata  di  Pescara,  con ordinanza del 17 dicembre 1992    &#13;
 (pervenuta alla Corte costituzionale l'11 agosto 1993), ha sollevato,    &#13;
 in riferimento agli artt. 3, 4, 32, 36, 97 e 98  della  Costituzione,    &#13;
 questione  di legittimità costituzionale dell'art. 2126 cod. civ. in    &#13;
 quanto applicabile anche all'impiego pubblico, almeno nella parte  in    &#13;
 cui non prevede per tale settore limiti di operatività temporale;       &#13;
      che,  ad  avviso  del  giudice  remittente,  la norma sarebbe in    &#13;
 contrasto con: a) il principio di eguaglianza e il diritto al  lavoro    &#13;
 perché,  non  prevedendo  alcun  limite temporale di applicabilità,    &#13;
 consente abusi che si traducono  in  arbitrari  favoritismi;  b)  col    &#13;
 principio  di tutela della salute, in sé e coordinato con l'art. 97,    &#13;
 primo comma, Cost., perché nel settore  della  sanità  consente  di    &#13;
 affidare  la  salute  dei cittadini a prestatori di lavoro di cui non    &#13;
 sono  accertate  le  occorrenti  attitudini  professionali;  c)   col    &#13;
 principio  di  proporzionalità  della retribuzione alle qualità del    &#13;
 lavoro prestato, pure coordinato  col  principio  di  buon  andamento    &#13;
 dell'amministrazione,    perché   consente   di   corrispondere   la    &#13;
 retribuzione relativa a qualifiche superiori a personale di qualifica    &#13;
 inferiore  privo  di  idoneità  a  mansioni  più  elevate;  d)  col    &#13;
 principio dell'avanzamento di carriera per pubblico concorso, perché    &#13;
 favorisce  lo  svolgimento  di  carriere  di  fatto senza la garanzia    &#13;
 prevista dall'art. 97, terzo comma, Cost.; e) col principio che  pone    &#13;
 i pubblici dipendenti al servizio esclusivo della Nazione, perché si    &#13;
 presta ad asservirli "a privati interessi distorti";                     &#13;
      che   nel   giudizio   davanti   alla  Corte  costituzionale  è    &#13;
 intervenuto il Presidente del Consiglio dei  ministri,  rappresentato    &#13;
 dall'Avvocatura   dello   Stato,   chiedendo  che  la  questione  sia    &#13;
 dichiarata manifestamente infondata in conformità dell'ord.  n.  337    &#13;
 del  1993, che ha deciso una questione analoga sollevata dallo stesso    &#13;
 giudice in relazione  all'art.  29,  secondo  comma,  del  d.P.R.  20    &#13;
 dicembre  1979,  n.  761,  "quale  risulta essere a seguito della sua    &#13;
 integrazione con gli artt. 36 Cost. e 2126 cod. civ.";                   &#13;
    Considerato che l'art. 2126 cod. civ., affererente alla disciplina    &#13;
 dei rapporti privati di  lavoro,  è  applicabile  ai  prestatori  di    &#13;
 lavoro dipendenti da enti pubblici, quali il personale delle USL, non    &#13;
 per  virtù propria, bensì in forza e nei limiti dell'art. 2129 cod.    &#13;
 civ.,  di  guisa   che   l'ordinanza   appare   viziata   da   errata    &#13;
 identificazione della norma impugnanda;                                  &#13;
      che  inoltre  la  questione  è  prospettata  "in  astratto", in    &#13;
 ragione della pretesa potenzialità lesiva  dei  richiamati  principi    &#13;
 costituzionali  attribuita  dal giudice remittente all'art. 2126 cod.    &#13;
 civ. in quanto applicabile anche ai  rapporti  di  pubblico  impiego,    &#13;
 senza   alcuna   verifica  della  concreta  pregiudizialità  per  la    &#13;
 definizione del giudizio principale ai sensi  dell'art.  23,  secondo    &#13;
 comma, della legge 11 marzo 1953, n. 87;                                 &#13;
    Visti  gli  artt.  26,  secondo comma, della legge ora citata e 9,    &#13;
 secondo comma, delle Norme integrative per  i  giudizi  davanti  alla    &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 2126 cod. civ.,  sollevata,  in    &#13;
 riferimento  agli artt. 3, 4, 32, 36, 97 e 98 della Costituzione, dal    &#13;
 Tribunale  amministrativo  per  l'Abruzzo  -  Sezione  distaccata  di    &#13;
 Pescara con l'ordinanza in epigrafe.                                     &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 gennaio 1994.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 gennaio 1994.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
