<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>1119</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:1119</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1988</data_decisione>
    <data_deposito>20/12/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>Nel  giudizio di legittimità costituzionale dell'art. 2 del d.-l.    &#13;
 25 maggio 1987, n. 206 ("Norme in materia di locazione di immobili ad    &#13;
 uso  non  abitativo,  di  alloggi di edilizia agevolata e di prestiti    &#13;
 emessi dalle Ferrovie dello Stato, nonché interventi per il  settore    &#13;
 distributivo")  promosso  con  ordinanza emessa il 24 luglio 1987 dal    &#13;
 Pretore di Fiorenzuola d'Arda nel procedimento  civile  vertente  tra    &#13;
 Grignaffini  Bernardo  e  Merli  Beniamino,  iscritta  al  n. 829 del    &#13;
 registro ordinanze 1987 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 54, prima serie speciale, dell'anno 1988.                  &#13;
    Visto  l'atto  di  costituzione  di  Grignaffini  Bernardo nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
    Udito  nella camera di consiglio del 9.11.1988 il Giudice relatore    &#13;
 Luigi Mengoni.                                                           &#13;
                               ORDINANZA                                  &#13;
    Ritenuto  che,  con  ordinanza  del  24 luglio 1987, il Pretore di    &#13;
 Fiorenzuola d'Arda ha sollevato, in riferimento agli  artt.  3  e  42    &#13;
 Cost., questione di legittimità costituzionale dell'art. 2 del d.-l.    &#13;
 25 maggio 1987 n. 206, in materia di locazione di immobili ad uso non    &#13;
 abitativo;                                                               &#13;
      che nel giudizio davanti alla Corte è intervenuto il Presidente    &#13;
 del  Consiglio  dei  Ministri,  rappresentato  dall'Avvocatura  dello    &#13;
 Stato,  chiedendo  che  la  questione  sia  dichiarata inammissibile,    &#13;
 siccome avente per oggetto una norma contenuta in  un  decreto  legge    &#13;
 non  convertito,  e  quindi  privato  di  efficacia  ex tunc ai sensi    &#13;
 dell'art. 77, terzo comma, Cost.;                                        &#13;
      che  la  parte  locatrice,  costituita  in giudizio, ha prodotto    &#13;
 "note   d'udienza"   nelle   quali   contesta   l'eccezione   opposta    &#13;
 dall'Avvocatura,  rilevando  che gli effetti della norma in questione    &#13;
 sono stati fatti salvi, e quindi  riportati  in  vita,  dall'art.  1,    &#13;
 secondo comma, della legge 25 novembre 1987 n. 478, che ha convertito    &#13;
 il d-l. 25 settembre 1987 n. 393, nel cui art. 2  è  riprodotta  una    &#13;
 disposizione identica a quella impugnata.                                &#13;
    Considerato  che,  in  conseguenza  della  mancata  conversione in    &#13;
 legge, il d.-l. n. 206 del 1987  deve  ormai  considerarsi  come  mai    &#13;
 esistito quale fonte di diritto a livello legislativo;                   &#13;
      che  non  giova  a  mantenerlo  in  vita  la successiva legge 25    &#13;
 novembre 1987 n. 478, che ne ha ripetuto  parzialmente  il  contenuto    &#13;
 con  salvezza  dei  rapporti  in  base ad esso insorti, in quanto una    &#13;
 norma di convalida ai sensi dell'art. 77,  terzo  comma,  Cost.,  non    &#13;
 forma  un  idoneo equipollente della legge di conversione (cfr. Corte    &#13;
 cost. n. 59 del 1982, n. 307 del 1983).                                  &#13;
    Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953 n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 2 del d.-l. 25 maggio  1987  n.    &#13;
 206 ("Norme in materia di locazione di immobili ad uso non abitativo,    &#13;
 di alloggi di edilizia agevolata e di prestiti emessi dalle  Ferrovie    &#13;
 dello  Stato,  nonché  interventi  per  il  settore  distributivo"),    &#13;
 sollevata dal Pretore di Fiorenzuola d'Arda con l'ordinanza  indicata    &#13;
 in epigrafe.                                                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 dicembre 1988.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 20 dicembre 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
