<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>30</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:30</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/01/1985</data_decisione>
    <data_deposito>30/01/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO &#13;
 LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO &#13;
 CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.    16  del  &#13;
 d.P.R.  26  ottobre  1972,  n.  636  (Revisione  della  disciplina  del  &#13;
 contenzioso tributario); degli artt. 1, 5 e 7 della  legge  2  dicembre  &#13;
 1975,  n.  576  (Disposizioni in materia di imposte sui redditi e sulle  &#13;
 successioni), promosso con ordinanza emessa il 12  gennaio  1977  dalla  &#13;
 Commissione tributaria di primo grado di Torino sul ricorso proposto da  &#13;
 Valentini  Liliana ed altri, iscritta al n.  340 del registro ordinanze  &#13;
 1977 e pubblicata, nella Gazzetta Ufficiale  della  Repubblica  n.  258  &#13;
 dell'anno 1977.                                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del  4  dicembre  1984  il  Giudice  &#13;
 relatore Aldo Corasaniti.                                                &#13;
     Ritenuto che la Commissione tributaria di primo grado di Torino - a  &#13;
 seguito  di  ricorso proposto da Valentini Liliana ed altri, diretto ad  &#13;
 accertare, in prevenzione di eventuali atti impositivi, l'insussistenza  &#13;
 dell'obbligo  del  cumulo  dei  redditi  dei  coniugi,  nonché   della  &#13;
 responsabilità  solidale dei medesimi per le imposte rispettivamente e  &#13;
 autonomamente dovute - ha  sollevato  con  ordinanza  12  gennaio  1977  &#13;
 questione di legittimità costituzionale:                                &#13;
     a)  dell'art.  16  d.P.R.  26  ottobre  1972,  n.  636  in  quanto,  &#13;
 sussistendo incertezza sull'ammissibilità,  nel  processo  tributario,  &#13;
 dell'azione  di accertamento negativo dell'obbligazione tributaria, non  &#13;
 sembra garantita la tutela preventiva  dei  diritti  del  cittadino  in  &#13;
 materia tributaria, con violazione dell'art. 24 della Costituzione;      &#13;
     b)  degli  artt.  1,  5  e  7  della legge 2 dicembre 1975, n. 576,  &#13;
 contenenti  norme  sul  cumulo  dei  redditi  dei   coniugi   e   sulla  &#13;
 responsabilità  solidale dei medesimi, in riferimento agli artt. 3, 4,  &#13;
 29, 31, 35, 37 e 53 Cost. in base agli argomenti  addotti  dalla  Corte  &#13;
 costituzionale con la sentenza n. 179 del 1976.                          &#13;
     Considerato  che,  successivamente all'ordinanza di rimessione, gli  &#13;
 artt. da 1 a 7 della legge n.  576/1975 sono stati abrogati dall'art. 1  &#13;
 della legge 13 aprile 1977,  n.  114  (Modificazioni  della  disciplina  &#13;
 dell'imposta sul reddito delle persone fisiche), e che, inoltre, l'art.  &#13;
 19  della  legge n. 114/1977 ha dettato norme transitorie per l'imposta  &#13;
 relativa  ai redditi posseduti dai coniugi nell'anno 1975 (da ritenere,  &#13;
 in relazione alla natura preventiva dell'azione dei contribuenti ed  al  &#13;
 tempo  di  presentazione  del  ricorso,  oggetto di contestazione nella  &#13;
 specie);                                                                 &#13;
     che, pertanto, soppresso mediante ius superveniens  l'istituto  del  &#13;
 "cumulo",   all'esclusione   della  cui  applicazione  era  strumentale  &#13;
 l'azione di  accertamento  negativo,  si  determina  la  necessità  di  &#13;
 restituzione degli atti al giudice a quo per il riesame della rilevanza  &#13;
 sia,  in ragione dell'ius superveniens, della questione sub b), che, in  &#13;
 ragione della cennata strumentalità, della questione sub a).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina la restituzione degli atti alla  Commissione  tributaria  di  &#13;
 primo grado di Torino.                                                   &#13;
     Così    deciso  in  Roma, in camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 gennaio 1985.        &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
