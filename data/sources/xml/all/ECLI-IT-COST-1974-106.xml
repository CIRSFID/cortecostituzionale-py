<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>106</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:106</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Vezio Crisafulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/04/1974</data_decisione>
    <data_deposito>18/04/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale  dell'art.  113,  &#13;
 quinto comma, del r.d. 18 giugno 1931, n.  773 (Testo unico delle leggi  &#13;
 di pubblica sicurezza) e degli artt. 2 e 4 della legge 23 gennaio 1941,  &#13;
 n. 166 (Norme integrative della disciplina delle pubbliche affissioni),  &#13;
 promossi con le seguenti ordinanze:                                      &#13;
     1)  ordinanza  emessa  il  19 maggio 1972 dal pretore di Modena nel  &#13;
 procedimento penale a carico di Piccinini Maurizio ed  altri,  iscritta  &#13;
 al  n.  265  del  registro  ordinanze  1972 e pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 247 del 20 settembre 1972;                 &#13;
     2) ordinanza emessa il 4 maggio 1973  dal  pretore  di  Modena  nel  &#13;
 procedimento  penale  a  carico di Bonavita Massimo, iscritta al n. 311  &#13;
 del registro ordinanze 1973 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 236 del 12 settembre 1973.                                 &#13;
     Udito nella camera di consiglio del  10  gennaio  1974  il  Giudice  &#13;
 relatore Vezio Crisafulli.                                               &#13;
     Ritenuto  che con due ordinanze, emesse dal pretore di Modena il 19  &#13;
 maggio 1972 nel corso di un procedimento penale a carico  di  Piccinini  &#13;
 Maurizio  ed  altro  ed  il  4 maggio 1973 nel corso di un procedimento  &#13;
 penale a carico di Bonavita Massimo, è stata sollevata, in riferimento  &#13;
 agli articoli 21 e 3, secondo comma, della Costituzione,  questione  di  &#13;
 legittimità  costituzionale  dell'art.  113, comma quinto, del r.d. 18  &#13;
 giugno 1931, n. 773 (t.u. delle leggi di pubblica  sicurezza)  e  degli  &#13;
 artt.  2  e  4  della  legge  23  gennaio  1941, n. 166, recante "Norme  &#13;
 integrative della disciplina delle pubbliche  affissioni",  nel  dubbio  &#13;
 che  tali norme che vietano le affissioni di scritti, disegni, stampati  &#13;
 e manoscritti fuori dei luoghi  destinati  dalla  competente  autorità  &#13;
 amministrativa  siano  in  contrasto  con la libertà costituzionale di  &#13;
 manifestazione del pensiero, regolandone  limitativamente  il  concreto  &#13;
 esercizio,  introducendo una sostanziale censura sul pensiero scritto e  &#13;
 demandando  all'autorità  amministrativa,  anziché  alla  legge,   la  &#13;
 fissazione  dei criteri prati ci e la determinazione degli spazi su cui  &#13;
 eseguire le affissioni, e violino altresì il principio  costituzionale  &#13;
 di  eguaglianza,  ponendo  un  ostacolo  di  ordine  economico  sociale  &#13;
 all'esercizio di una libertà primaria ed essenziale al pieno  sviluppo  &#13;
 della   persona  umana  ed  all'effettiva  partecipazione  di  tutti  i  &#13;
 lavoratori alla organizzazione politica, economica e sociale del Paese;  &#13;
     che nessuno si è costituito in giudizio.                            &#13;
     Considerato che le  medesime  questioni  o  questioni  strettamente  &#13;
 analoghe  sono  state  dichiarate  non  fondate  da questa Corte con le  &#13;
 sentenze 5 giugno 1956, n. 1, 3 luglio 1957, n. 121, 20 giugno 1961, n.  &#13;
 38, 4 giugno 1964, n. 48, 16 giugno 1965, n. 49, e 24 giugno  1970,  n.  &#13;
 129,  e  manifestamente  infondate con l'ordinanza 16 dicembre 1965, n.  &#13;
 97;                                                                      &#13;
     che in tali occasioni, questa Corte,  con  particolare  riferimento  &#13;
 alla  libertà  di  manifestazione del pensiero, ha più volte chiarito  &#13;
 che la disciplina delle  modalità  di  esercizio  di  un  diritto  non  &#13;
 costituisce per se stessa lesione del diritto medesimo e non è perciò  &#13;
 costituzionalmente  vietata anche se possa derivarne indirettamente una  &#13;
 qualche limitazione  sempre  che  il  diritto  stesso  non  ne  risulti  &#13;
 snaturato  o  non  ne sia reso più difficile o addirittura impossibile  &#13;
 l'esercizio; che le norme che regolano  l'affissione  di  stampati,  di  &#13;
 giornali  murali  e  di  manifesti  di  propaganda  durante la campagna  &#13;
 elettorale non instaurano né direttamente, né  indirettamente  alcuna  &#13;
 forma  di  censura;  che, infine, una certa sfera di discrezionalità -  &#13;
 specie se limitata e controllata e, per alcuni aspetti,  tecnica  -  si  &#13;
 deve  riconoscere  all'autorità  amministrativa,  perché  le leggi, e  &#13;
 tanto meno la Costituzione, non possono prevedere e disciplinare  tutte  &#13;
 le mutevoli situazioni di fatto, né graduare in astratto e in anticipo  &#13;
 le limitazioni poste all'esercizio dei diritti;                          &#13;
     che  in  alcune di tali decisioni questa Corte ha altresì chiarito  &#13;
 in  relazione  al  principio  costituzionale  di  eguaglianza,  e   con  &#13;
 particolare  riferimento  alla propaganda elettorale, con argomento che  &#13;
 può e deve estendersi,  per  identità  di  ragioni,  anche  a  quanto  &#13;
 riguarda  più largamente il regime delle affissioni in genere, che una  &#13;
 rigorosa disciplina in materia  tende  proprio  ad  assicurare  che  lo  &#13;
 svolgimento  della vita democratica sia garantito a tutti in condizioni  &#13;
 di parità e non sia di fatto ostacolato da  situazioni  economiche  di  &#13;
 svantaggio o politiche di minoranza;                                     &#13;
     che   nelle  suddette  ordinanze  di  rinvio  non  vengono  addotti  &#13;
 argomenti nuovi che possano  indurre  a  discostarsi  dalle  precedenti  &#13;
 decisioni.                                                               &#13;
     Visti  gli  artt.  26, secondo comma, della legge 11 marzo I953, n.  &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi innanzi a  &#13;
 questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza delle questioni di legittimità  &#13;
 costituzionale dell'art. 113, comma quinto, del r.d. 18 giugno 1931, n.  &#13;
 773 (t.u. delle leggi di pubblica sicurezza) e degli artt. 2 e 4  della  &#13;
 legge  23  gennaio  1941,  n.  166,  recante  "Norme  integrative della  &#13;
 disciplina delle pubbliche affissioni", sollevate, in riferimento  agli  &#13;
 artt.  21  e  3, secondo comma, della Costituzione, con le ordinanze in  &#13;
 epigrafe indicate.                                                       &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 4 aprile 1974.          &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
