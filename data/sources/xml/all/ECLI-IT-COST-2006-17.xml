<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>17</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:17</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>11/01/2006</data_decisione>
    <data_deposito>20/01/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Annibale MARINI; Giudici: Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli articoli 97 e 108 del codice di procedura penale, promosso con ordinanza del 10 giugno 2004 dalla Corte d'appello di Caltanissetta, nel procedimento penale a carico di P.G.C., iscritta al n. 871 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 45, prima serie speciale, dell'anno 2004. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 14 dicembre 2005 il Giudice relatore Giovanni Maria Flick.  &#13;
    Ritenuto che, con l'ordinanza in epigrafe la Corte d'appello di Caltanissetta – dopo aver premesso che il difensore dell'appellante, designato «d'ufficio» in udienza a norma dell'art. 97 del codice di procedura penale, ha chiesto la concessione di un termine a difesa, nel corso di un dibattimento di appello - ha sollevato, in riferimento agli artt. 3 e 24, secondo comma, della Costituzione, questione di legittimità costituzionale degli artt. 97 e 108 del codice di procedura penale, «nella parte in cui non prevedono che anche il difensore designato di ufficio all'imputato che deve essere giudicato nella fase dibattimentale il quale non abbia nominato un difensore di fiducia, o sia comunque privo dell'assistenza difensiva all'udienza fissata per la celebrazione del relativo giudizio, abbia diritto, qualora lo richieda, di usufruire della concessione di un termine “per prendere cognizione degli atti e per informarsi sui fatti oggetto del procedimento”»; &#13;
    che a tal proposito la Corte rimettente sottolinea come non risulti formalmente previsto dall'art. 108 del codice di rito che il difensore designato d'ufficio, a norma dell'art. 97 del medesimo codice, possa in ogni caso chiedere la concessione di un termine allo scopo di acquisire – come recita il richiamato art. 108 cod. proc. pen. – una compiuta “cognizione degli atti e per informarsi sui fatti oggetto del procedimento”, al pari di quanto è invece testualmente previsto per il difensore designato di ufficio nelle ipotesi di “rinuncia, di revoca, di incompatibilità, e nel caso di abbandono di difesa”; &#13;
    che la rilevata lacuna normativa risulterebbe lesiva del diritto di difesa presidiato dall'art. 24, secondo comma, Cost., in quanto la menomazione delle facoltà difensive, che scaturisce dalla negazione del termine a difesa, finisce ineluttabilmente per compromettere la stessa «efficacia della assistenza difensiva, rendendola sostanzialmente inutile»; &#13;
    che risulterebbe violato anche il principio di uguaglianza sostanziale, sancito dall'art. 3 della Carta fondamentale, non potendosi rinvenire – secondo il giudice a quo – ragioni di ordine sistematico tali da giustificare l'esclusione del difensore d'ufficio, designato ai sensi dell'art. 97 cod. proc. pen., «dall'esercizio della facoltà espressamente prevista dall'art. 108» del medesimo codice; &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dalla Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile e comunque infondata. &#13;
    Considerato che questione del tutto analoga è già stata dichiarata non fondata da questa Corte con sentenza n. 450 del 1997 e manifestamente infondata con ordinanza n. 162 del 1998; &#13;
    che in tali pronunce – ignorate dal giudice rimettente e dalle quali non v'è ragione di discostarsi, in difetto di elementi o profili nuovi e diversi, rispetto a quelli da esse valutati – questa Corte ha sottolineato come l'art. 108 cod. proc. pen., nel disciplinare l'istituto del termine a difesa, si concentri su una tassativa elencazione di ipotesi (rinuncia, revoca, incompatibilità e abbandono di difesa), la cui ratio comune è rappresentata dalla circostanza che, in ognuna delle situazioni prese in considerazione, l'imputato rimane definitivamente privo di difensore: una condizione di fatto e di diritto, dunque, assai diversa da quella della semplice assenza del difensore, di fiducia o di ufficio, la quale – ha puntualizzato questa Corte (v. la sentenza n. 450 del 1997) – «può risalire ai più diversi motivi ed essere espressiva di situazioni assai diverse tra loro», ed è d'altronde specificamente disciplinata, nelle ipotesi di assenza “sorretta da un legittimo impedimento”, dall'art. 420-ter cod. proc. pen.; &#13;
    che, pertanto, «l'avvocato che interviene come sostituto del difensore (di fiducia come d'ufficio) da questo nominato (ex art. 102) o immediatamente designato dal magistrato appena verificatasi l'assenza del difensore (art. 97, comma 4) è investito del compito di rappresentare colui che è e resta il difensore dell'imputato», ed è «figura del tutto diversa da quella del nuovo difensore designato nelle ipotesi di rinuncia, revoca, incompatibilità e abbandono di difesa» : con l'ovvia conseguenza che una proiezione, in capo al sostituto, del medesimo diritto di un termine a difesa specificamente attribuito a chi rivesta la qualità di “nuovo” (e stabile) difensore dell'imputato, finirebbe per costituire soluzione davvero eccentrica, perequando fra loro situazioni – come si osservò nelle richiamate pronunce – del tutto eterogenee; &#13;
    che, d'altra parte - essendo la presenza un diritto e non un obbligo del difensore, salvo le ipotesi espressamente previste dalla legge (art. 294, comma 4, secondo periodo, cod. proc. pen.) - il mancato riconoscimento del termine a difesa, per il difensore designato in sostituzione (“estemporanea ed episodica”, si sottolineò nella sentenza n. 450 del 1997) di quello “stabilmente” officiato dall'imputato o per l'imputato, appare conseguenza del tutto ragionevole nel quadro di un sistema che necessariamente mira a bilanciare le contrapposte esigenze di prevedere comunque una presenza difensiva, ma di non compromettere al tempo stesso la indispensabile funzionalità del processo e la relativa ragionevole durata, altrimenti perturbata da differimenti reiterati per ciascuno dei difensori che intervengano come sostituti e che ne facciano richiesta. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 97 e 108 del codice di procedura penale, sollevata, in riferimento agli artt. 3 e 24, secondo comma, della Costituzione, dalla Corte d'appello di Caltanissetta con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 gennaio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 20 gennaio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
