<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>437</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:437</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/11/1992</data_decisione>
    <data_deposito>13/11/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 1 e 2 della    &#13;
 legge della Regione Lombardia 25 novembre 1991, n. 28 (Norme  per  lo    &#13;
 stoccaggio  provvisorio  dei rifiuti tossici e nocivi presso il luogo    &#13;
 di produzione), promosso con ordinanza emessa il 17 dicembre 1991 dal    &#13;
 Pretore di Bergamo, Sezione distaccata di  Grumello  del  Monte,  nel    &#13;
 procedimento  penale a carico di Archetti Alberto, iscritta al n. 255    &#13;
 del registro ordinanze 1992 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 20, prima serie speciale, dell'anno 1992;            &#13;
    Udito  nella  camera  di  consiglio del 21 ottobre 1992 il Giudice    &#13;
 relatore Francesco Greco;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Il Pretore di Bergamo, Sezione distaccata di Grumello  del  Monte,    &#13;
 nel  procedimento  penale  a carico di Archetti Alberto, imputato del    &#13;
 reato di cui all'art. 26  del  D.P.R.  n.  915  del  1982,  per  aver    &#13;
 effettuato  lo  stoccaggio  provvisorio  di  quattro fusti contenenti    &#13;
 solventi  clorurati  esausti  per  un  peso   complessivo   di   1168    &#13;
 chilogrammi  senza essere in possesso della prescritta autorizzazione    &#13;
 regionale, con ordinanza del 17 dicembre 1991 (R.O. n. 255 del 1992),    &#13;
 ha sollevato questione di legittimità costituzionale degli artt. 1 e    &#13;
 2 della legge regionale della Lombardia 25 novembre 1991, n.  28,  in    &#13;
 riferimento agli artt. 117 e 25 della Costituzione.                      &#13;
    Ha  osservato che la normativa censurata prevede, in via generale,    &#13;
 un'autorizzazione  preventiva  per  lo  stoccaggio  provvisorio   dei    &#13;
 rifiuti   tossici  e  nocivi  all'interno  del  singolo  insediamento    &#13;
 produttivo  di  origine,  ove  siano  soddisfatti  alcuni   requisiti    &#13;
 tassativamente indicati (art. 1), previa comunicazione alla provincia    &#13;
 territorialmente  competente  della  tipologia  e della quantità dei    &#13;
 rifiuti entro 60 giorni dalla data di entrata in vigore della legge o    &#13;
 di formazione di nuovi rifiuti (art. 2).                                 &#13;
    Tali disposizioni, a  parere  del  giudice  a  quo,  sarebbero  in    &#13;
 contrasto  con gli artt. 6 e 16 del D.P.R. n. 915 del 1982, in quanto    &#13;
 l'autorizzazione preventiva generale  non  rientra  nelle  competenze    &#13;
 regionali  così  come  previste  dal  punto  f)  del citato art. 6 e    &#13;
 verrebbe posta nel nulla l'autorizzazione richiesta dall'art. 16  del    &#13;
 D.P.R.  n.  915  del 1982 per ogni fase dello smaltimento dei rifiuti    &#13;
 tossici e nocivi e, quindi, anche per lo stoccaggio provvisorio.         &#13;
    In sostanza, il disposto regionale  consente  che  l'attività  in    &#13;
 questione  si  svolga  senza  alcuna  pronunzia dell'Amministrazione,    &#13;
 lasciando poi alla iniziativa della provincia il controllo successivo    &#13;
 della   sussistenza   delle   condizioni   legittimanti.    Pertanto,    &#13;
 risulterebbero violati:                                                  &#13;
      a)  l'art.  117  della  Costituzione,  perché  la materia dello    &#13;
 smaltimento dei rifiuti non  rientra  tra  quelle  per  le  quali  è    &#13;
 riconosciuta  la  potestà  legislativa  concorrente  delle Regioni e    &#13;
 perché principi fondamentali diversi sono posti dalla legge  statale    &#13;
 che, peraltro, attua direttive C.E.E.;                                   &#13;
      b)  l'art.  25  della  Costituzione  perché  la legge regionale    &#13;
 impugnata  renderebbe  lecita  una  fattispecie   che,   invece,   è    &#13;
 penalmente  sanzionata dalla legge statale (art. 26 d.P.R. n. 915 del    &#13;
 1982).<diritto>Considerato in diritto</diritto>La Corte deve verificare se gli artt. 1 e 2 della legge  regionale    &#13;
 della  Lombardia  n. 28 del 1991, nella parte in cui prevedono in via    &#13;
 generale un'autorizzazione preventiva per lo  stoccaggio  provvisorio    &#13;
 di  rifiuti  tossici  e  nocivi  all'interno del singolo insediamento    &#13;
 produttivo  di  origine,  ove  siano  soddisfatti  alcuni   requisiti    &#13;
 tassativamente  indicati,  tra  cui  l'obbligo  a carico del titolare    &#13;
 dell'impianto di comunicare entro 60 giorni dalla data di entrata  in    &#13;
 vigore  della legge o di formazione dei nuovi rifiuti, alla provincia    &#13;
 territorialmente competente la tipologia e la quantità dei  rifiuti,    &#13;
 violino  gli  artt.  117 e 25 della Costituzione in quanto la Regione    &#13;
 non ha competenza legislativa in  materia;  non  sarebbero  osservati    &#13;
 principi  fondamentali  posti  dalla legge statale (d.P.R. n. 915 del    &#13;
 1982) attuativa  di  direttive  comunitarie  e,  infine,  perché  si    &#13;
 renderebbe   lecita   una  fattispecie  che,  invece,  è  penalmente    &#13;
 sanzionata.                                                              &#13;
    2. - La questione è fondata.                                         &#13;
    L'art. 1 della  legge  della  Regione  Lombardia  dispone  che  lo    &#13;
 stoccaggio  provvisorio  di  rifiuti tossici e nocivi all'interno del    &#13;
 singolo stabilimento produttivo di origine, si intende autorizzato se    &#13;
 siano soddisfatte alcune condizioni (lett. a, b, c, d,).                 &#13;
    L'art. 2 stabilisce che il titolare  dell'impianto  di  stoccaggio    &#13;
 provvisorio  così  autorizzato è tenuto a comunicare alla provincia    &#13;
 territorialmente competente la tipologia e la quantità  dei  rifiuti    &#13;
 nocivi  prodotti  entro sessanta giorni dalla entrata in vigore della    &#13;
 legge ovvero dalla data di formazione di nuovi rifiuti. Deve altresì    &#13;
 tenere i registri di carico e scarico.                                   &#13;
    Le norme impugnate prevedono, quindi,  una  autorizzazione  tacita    &#13;
 sebbene  sottoposta  all'osservanza di determinate condizioni, la cui    &#13;
 sussistenza, però, deve, per  la  legge  statale,  essere  accertata    &#13;
 preventivamente al rilascio della autorizzazione specifica.              &#13;
    3.  -  Questa  Corte ha già ritenuto in materia (sent. n. 306 del    &#13;
 1992) che la disciplina specifica di cui al d.P.R. n. 915  del  1982,    &#13;
 attuativo  di  alcune  direttive  C.E.E.,  richiede per lo stoccaggio    &#13;
 provvisorio, come per tutte le  fasi  dello  smaltimento  di  rifiuti    &#13;
 tossici  e  nocivi,  una  espressa  e  puntuale  autorizzazione  dopo    &#13;
 l'accertamento  della  sussistenza  di  alcuni  requisiti  specifici,    &#13;
 affinché sia garantita l'eliminazione di ogni pericolo per la salute    &#13;
 e il degrado ambientale.                                                 &#13;
    Secondo  il  prevalente  indirizzo  giurisprudenziale, è altresì    &#13;
 esclusa la possibilità di autorizzazioni tacite o generiche, nonché    &#13;
 il  ricorso  al  silenzio-assenso  proprio  per   le   finalità   da    &#13;
 raggiungersi  e  per  la  necessità di predisporre garanzie idonee e    &#13;
 sufficientemente certe e sicure.                                         &#13;
    3.1.  -  È  stato,  inoltre, più volte affermato che la potestà    &#13;
 legislativa  regionale  deve  attuarsi  in  armonia  con  i  precetti    &#13;
 costituzionali e con i principi fondamentali dell'ordinamento, con la    &#13;
 garanzia della osservanza degli obblighi internazionali assunti dallo    &#13;
 Stato,  e  che  è  destinata  a  cedere di fronte alla attuazione di    &#13;
 direttive comunitarie  per  le  quali  lo  Stato  ha  contratto  tali    &#13;
 obblighi.                                                                &#13;
    Resta  assorbita  la  censura sollevata in riferimento all'art. 25    &#13;
 della Costituzione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la illegittimità costituzionale degli artt. 1 e  2  della    &#13;
 legge  della  Regione Lombardia n. 28 del 25 novembre 1991 (Norme per    &#13;
 lo stoccaggio provvisorio dei rifiuti  tossici  e  nocivi  presso  il    &#13;
 luogo di produzione).                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 2 novembre 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 13 novembre 1992.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
