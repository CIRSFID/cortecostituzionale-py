<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>175</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:175</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Antonino de Stefano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/10/1982</data_decisione>
    <data_deposito>10/11/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale degli artt.  43, lett.  &#13;
 d, della legge 12 febbraio 1968, n. 132 (Enti ospedalieri e  assistenza  &#13;
 ospedaliera)  e  133 del d.P.R.  27 marzo 1969, n. 130 (Stato giuridico  &#13;
 dei dipendenti degli enti ospedalieri) promosso con ordinanza emessa il  &#13;
 16 novembre  1976  dal  pretore  di  Torino,  nel  procedimento  civile  &#13;
 vertente  tra  Borsari Osanna e Calderini Paolo, iscritta al n. 764 del  &#13;
 registro ordinanze 1976 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 51 del 23 febbraio 1977.                                   &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 12 gennaio 1982 il Giudice relatore  &#13;
 Antonino De Stefano;                                                     &#13;
     udito l'avvocato dello Stato Franco Chiarotti per il Presidente del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Il pretore di Torino, nel  procedimento  civile  vertente  tra  &#13;
 Borsari  Osanna  e  Calderini  Paolo,  in  data  16  novembre  1976  ha  &#13;
 pronunziato  un'ordinanza  con  la  quale,  ritenuta   l'eccezione   di  &#13;
 illegittimità  costituzionale  sollevata dal convenuto rilevante e non  &#13;
 manifestamente infondata, ha disposto  la  trasmissione  degli  atti  a  &#13;
 questa   Corte   per   la   decisione   in   ordine  alla  legittimità  &#13;
 costituzionale, in relazione agli artt. 3, 4 e 32  della  Costituzione,  &#13;
 degli  artt.  133 del d.P.R. 27 marzo 1969, n. 130 (Stato giuridico dei  &#13;
 dipendenti degli enti ospedalieri), e  43,  lett.  d.  della  legge  12  &#13;
 febbraio 1968, n. 132 (Enti ospedalieri e assistenza ospedaliera).       &#13;
     Nella  motivazione del provvedimento, riguardo ai fatti oggetto del  &#13;
 giudizio  a  quo,  il  pretore  riferisce  che,  a   quanto   affermato  &#13;
 dall'attrice  Borsari,  ella,  sofferente  di  tumore, aveva chiesto di  &#13;
 venire operata dall'aiuto chirurgo prof.  Calderini  presso  l'ospedale  &#13;
 San  Giovanni Battista in Torino; ma l'operazione, data la gravità del  &#13;
 male e l'urgenza dell'intervento, fu  poi  eseguita,  non  in  ospedale  &#13;
 (dove al momento, per il gran numero di ricoverati in attesa, le si era  &#13;
 obiettato  che  non sarebbe stato possibile) ma in una clinica privata.  &#13;
 Secondo l'attrice, tuttavia, il " dirottamento  dall'ospedale  pubblico  &#13;
 alla clinica privata" era stato abusivo da parte del professionista, ed  &#13;
 illegittimo, "perché impostole in chiaro contrasto con le disposizioni  &#13;
 che  disciplinano  l'attività  a tempo definito dei medici". E poiché  &#13;
 tutto ciò le aveva causato un notevole aggravio di spese, oltre  a  un  &#13;
 ritardo  pericoloso  per  la  sua  salute,  chiedeva  la  condanna  del  &#13;
 Calderini al risarcimento dei danni. Dal canto suo, il  convenuto,  pur  &#13;
 ammettendo  la realtà dei fatti lamentati dall'attrice, replicava che,  &#13;
 prestando servizio a tempo  definito  presso  l'ospedale  San  Giovanni  &#13;
 Battista,  e non offrendo questo la disponibilità di appositi ambienti  &#13;
 idonei  per  il  libero   esercizio,   presso   lo   stesso   ospedale,  &#13;
 dell'attività  libero  -  professionale,  egli  veniva  a  subire, per  &#13;
 effetto delle denunciate norme, delle limitazioni  contrastanti  con  i  &#13;
 precetti degli artt.  3, 4 e 32 della Costituzione.                      &#13;
     Ciò  premesso  il  giudice  a  quo  osserva  che  le  questioni di  &#13;
 legittimità costituzionale sono pregiudiziali e rilevanti,  in  quanto  &#13;
 il  comportamento  del  Calderini ha sortito conseguenze economicamente  &#13;
 gravose per l'attrice, che ne fu vittima, e  se  riconosciuto  illecito  &#13;
 darebbe  diritto  alla  stessa di pretendere il risarcimento del danno,  &#13;
 mentre la presunta illiceità cadrebbe, e il convenuto andrebbe  esente  &#13;
 da  ogni  responsabilità,  se  venisse  dichiarata  la  illegittimità  &#13;
 costituzionale delle norme che si ritengono da lui violate.              &#13;
     In punto di non manifesta infondatezza delle questioni, il  pretore  &#13;
 osserva, poi, riguardo agli artt.  43, lett. d. della legge 12 febbraio  &#13;
 1968,  n.  132,  e  133 del d.P.R. 27 marzo 1969, n. 130, che mentre il  &#13;
 primo di  tali  articoli  sembra  da  interpretarsi  nel  senso  che  a  &#13;
 decorrere dal 1 gennaio 1976, una volta che siano state messe in opera,  &#13;
 nell'ambito   dell'ospedale,   le   previste   apposite   attrezzature,  &#13;
 necessarie per l'esercizio dell'attività professionale  "intramurale",  &#13;
 alla  regola della libera attività nelle case di cura private dovrebbe  &#13;
 subentrare  per  i  sanitari  ospedalieri  a  tempo  definito,  quella,  &#13;
 opposta,  dell'incompatibilità,  il  secondo,  almeno  se  inteso alla  &#13;
 lettera, sembra statuire che, scaduto il termine del 31 dicembre  1975,  &#13;
 l'attività  libera,  siano  o  meno  apprestati gli "ambienti idonei",  &#13;
 sarà in ogni caso preclusa. A giudizio del  pretore,  perciò,  l'art.  &#13;
 133   del   d.P.R.  n.  130  del  1969,  "unitamente  alla  particolare  &#13;
 interpretazione dell'art. 43, lett. d. della legge  n.  132  del  1968"  &#13;
 contrasta:                                                               &#13;
     a) con l'art. 3 della Costituzione, poiché i medici che esercitano  &#13;
 specialità per cui sono indispensabili attrezzature particolari (negli  &#13;
 ospedali in cui esercitano, mancanti), vedrebbero paralizzato per metà  &#13;
 l'esercizio  della  professione,  in  situazione di disparità rispetto  &#13;
 agli altri medici ospedalieri, ai quali (poiche negli ospedali, in  cui  &#13;
 essi lavorano, le suddette attrezzature invece esistono) l'esercizio di  &#13;
 specialità  condizionate  all'uso  di  quelle  attrezzature  è invece  &#13;
 consentito,  e  rispetto  altresì  agli  altri medici che, non dovendo  &#13;
 servirsi, per la natura della loro attività, di speciali attrezzature,  &#13;
 possono svolgere interamente la loro libera attività nei propri  studi  &#13;
 professionali;                                                           &#13;
     b) con l'art. 4 della Costituzione, giacché i medici ospedalieri a  &#13;
 tempo  definito,  che  non abbiano di fatto la possibilità di svolgere  &#13;
 all'interno dell'ospedale l'attività cui hanno diritto,  verrebbero  a  &#13;
 subire una ingiustificata limitazione del loro diritto al lavoro;        &#13;
     c)  con l'art. 32 della Costituzione, non essendoci dubbio che, nei  &#13;
 casi suddetti, il diritto alla  salute  da  esso  garantito  rimarrebbe  &#13;
 compresso  in  una delle sue estrinsecazioni più importanti, quella di  &#13;
 farsi curare  dal  medico  di  propria  fiducia;  ciò  che  avverrebbe  &#13;
 inevitabilmente  ogni  qual  volta  il  medico  prescelto  fosse medico  &#13;
 ospedaliero e l'ospedale (cosa non infrequente)  non  disponesse  delle  &#13;
 strutture prescritte.                                                    &#13;
     2.  -  Trasmessi  a  questa  Corte  gli  atti  del  giudice a quo e  &#13;
 adempiute le formalità di rito, con atto depositato il 7 marzo 1977 è  &#13;
 intervenuta, per il Presidente del Consiglio dei ministri, l'Avvocatura  &#13;
 dello Stato, chiedendo che le questioni di legittimità  costituzionale  &#13;
 proposte dal pretore di Torino siano dichiarate infondate.               &#13;
     Riportandosi,   per   quanto  riguarda  la  questione  proposta  in  &#13;
 riferimento all'art. 3  della  Costituzione  (identica  a  quella  già  &#13;
 prospettata  con  altre  ordinanze),  alle  deduzioni svolte negli atti  &#13;
 d'intervento presentati nei giudizi  relativi  (risoltisi  poi  con  la  &#13;
 sentenza  di  questa  Corte  n.  103 del 1977), riguardo alle questioni  &#13;
 formulate  in  riferimento  agli  artt.  4  e  32  della  Costituzione,  &#13;
 l'Avvocatura  osserva,  anzitutto, che il senso della norma dell'art. 4  &#13;
 della Costituzione esclude che l'efficacia di essa si  estenda  fino  a  &#13;
 coprire  situazioni  del  tipo  di  quella  che  è  oggetto  di  esame  &#13;
 nell'ordinanza di rinvio.  Quanto poi al richiamo  dell'art.  32  della  &#13;
 Costituzione, all'Avvocatura non sembra possa affermarsi che il diritto  &#13;
 alla  salute  da  esso sancito comprenda anche il diritto di scelta del  &#13;
 medico di propria  fiducia.  In  tal  modo,  infatti,  si  verrebbe  ad  &#13;
 escludere  che  il  diritto  proclamato dall'art. 32 della Costituzione  &#13;
 trovi tutela negli ospedali pubblici, cui tutti  possono  accedere,  ma  &#13;
 nei quali la scelta del medico è ben raramente consentita al paziente,  &#13;
 senza che con ciò possa dirsi che il diritto alla salute non riceva in  &#13;
 tal caso per il paziente stesso congrua tutela.                          &#13;
     3.  -  Inclusa,  in  un  primo tempo, fra le cause da esaminare, in  &#13;
 camera di consiglio, il 28 febbraio 1980, la causa,  con  ordinanza  n.  &#13;
 145  dello  stesso  anno,  è stata rinviata a nuovo ruolo. All'udienza  &#13;
 pubblica  del  12  gennaio  1982,  successivamente   fissata   per   la  &#13;
 discussione,  il  giudice  Antonino De Stefano ha svolto la relazione e  &#13;
 l'avvocato  dello  Stato  Franco  Chiarotti   ha   insistito   per   la  &#13;
 dichiarazione di non fondatezza.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  pretore di Torino, con l'ordinanza indicata in epigrafe,  &#13;
 prospetta questione di  legittimità  costituzionale  degli  artt.  43,  &#13;
 lett.  d.  della  legge  12  febbraio 1968, n. 132, e 133 del d.P.R. 27  &#13;
 marzo 1969, n. 130, interpretati nel senso  che  il  divieto,  da  essi  &#13;
 disposto  per  i  sanitari  ospedalieri  con rapporto di lavoro a tempo  &#13;
 definito, di esercitare attività libero - professionale presso case di  &#13;
 cura private, abbia carattere perentorio ed operi comunque, dopo il  31  &#13;
 dicembre   1975,  anche  se  l'amministrazione  ospedaliera  non  abbia  &#13;
 assicurato  la  disponibilità  di  appositi  ambienti qualitativamente  &#13;
 idonei  per  l'esercizio   dell'attività   professionale   all'interno  &#13;
 dell'ospedale.                                                           &#13;
     Le    denunciate   norme   contrasterebbero   con   vari   principi  &#13;
 costituzionali. Innanzitutto con il principio dell'uguaglianza, sancito  &#13;
 dall'art. 3 della Costituzione, sotto un duplice profilo. Ad avviso del  &#13;
 pretore, infatti, una prima disparità di trattamento  per  i  sanitari  &#13;
 che,  in  ragione  della  propria specializzazione, abbiano bisogno per  &#13;
 l'esercizio della professione di particolari attrezzature, scaturirebbe  &#13;
 dalle norme in questione, avendo il divieto una  diversa  incidenza,  a  &#13;
 seconda  che  essi dipendano da enti ospedalieri che abbiano apprestato  &#13;
 ambienti idonei all'esercizio dell'attività libero -  professionale  o  &#13;
 da  enti  che,  invece,  tali  ambienti  non  abbiano  potuto  o voluto  &#13;
 attrezzare.  Una seconda disparità di  trattamento  si  concreterebbe,  &#13;
 per   gli  stessi  sanitari,  rispetto  ai  loro  colleghi,  egualmente  &#13;
 dipendenti ospedalieri a tempo definito, che,  non  avendo  bisogno  di  &#13;
 particolari  attrezzature, possono esercitare la libera professione nei  &#13;
 propri studi privati, per i quali ultimi il divieto non opera,  vigendo  &#13;
 soltanto per le case di cura.                                            &#13;
     Le  norme  impugnate  sarebbero,  poi,  in  contrasto  anche con il  &#13;
 principio  enunciato  dall'art.  4  della  Costituzione,  in  quanto  i  &#13;
 sanitari  ospedalieri a tempo definito, non avendo la possibilità, per  &#13;
 le  ragioni  sopra  esposte,  di  svolgere  all'interno   dell'ospedale  &#13;
 l'attività  libero  -  professionale  cui  hanno diritto, verrebbero a  &#13;
 subire una ingiustificata limitazione del loro diritto al lavoro.        &#13;
     Infine, il giudice a quo  denuncia  il  contrasto  delle  anzidette  &#13;
 norme  con  l'art.  32  della  Costituzione, atteso che il diritto alla  &#13;
 salute, da questo garantito, rimarrebbe compresso in una delle sue più  &#13;
 importanti estrinsecazioni,  quella  di  farsi  curare  dal  medico  di  &#13;
 propria  fiducia,  "ogni  qual  volta  il medico prescelto fosse medico  &#13;
 ospedaliero e l'ospedale... non disponesse delle strutture prescritte".  &#13;
     2. - La sollevata questione, nei cennati  profili  con  riferimento  &#13;
 agli  invocati parametri degli artt. 3 e 4 della Costituzione, è stata  &#13;
 già dichiarata non fondata da questa Corte con la sentenza n. 103  del  &#13;
 1977;  e  manifestamente  infondata,  in relazione al solo art. 3 della  &#13;
 Costituzione, con l'ordinanza n. 7 del 1979.    Pertanto,  non  essendo  &#13;
 addotti  argomenti  atti  ad  indurre  la Corte a modificare la propria  &#13;
 giurisprudenza, la  questione,  come  prospettata  con  riferimento  ai  &#13;
 menzionati artt. 3 e 4 della Costituzione, va dichiarata manifestamente  &#13;
 infondata.                                                               &#13;
     3.  -  Del  pari  non  fondata è la questione, come prospettata in  &#13;
 relazione all'art. 32 della Costituzione.                                &#13;
     Nella richiamata sentenza n. 103 del  1977  questa  Corte  ebbe  ad  &#13;
 affermare  che "in attuazione del principio del supremo interesse della  &#13;
 collettività alla tutela della salute,  consacrata  come  fondamentale  &#13;
 diritto  dell'individuo dall'art. 32 della Costituzione (sentenze n. 21  &#13;
 del  1964  e  n.  149  del  1969),  l'infermo  assurge,  nella  novella  &#13;
 concezione  dell'assistenza  ospedaliera,  alla  dignità  di legittimo  &#13;
 utente di un pubblico servizio, cui ha pieno e incondizionato  diritto,  &#13;
 e  che  gli  vien  reso,  in  adempimento  di un inderogabile dovere di  &#13;
 solidarietà  umana  e  sociale,  da  apparati  di   personale   e   di  &#13;
 attrezzature  a  ciò strumentalmente preordinati e che in ciò trovano  &#13;
 la loro stessa ragion d'essere". E nella successiva sentenza n. 88  del  &#13;
 1979  la  Corte ribadì che il bene afferente alla salute va ricompreso  &#13;
 tra le posizioni soggettive direttamente tutelate dalla Costituzione.    &#13;
     In  siffatta  prospettiva  non  v'ha  dubbio  che  razionalmente si  &#13;
 colloca il diritto del cittadino alla libera scelta del  medico  e  del  &#13;
 luogo  di cura. Ovvio, peraltro, che la tutela di siffatto diritto vada  &#13;
 assicurata  "nei  limiti  oggettivi  dell'organizzazione  dei   servizi  &#13;
 sanitari",  come appunto sancisce l'art. 19, comma secondo, della legge  &#13;
 23 dicembre 1978, n. 833, istitutiva del servizio sanitario nazionale.   &#13;
     Per quanto in particolare concerne l'assistenza ospedaliera, l'art.  &#13;
 25 della citata legge n. 833 del 1978 si richiama al  "principio  della  &#13;
 libera  scelta del cittadino al ricovero presso gli ospedali pubblici e  &#13;
 gli altri istituti convenzionati",  contemplando,  peraltro,  ai  commi  &#13;
 ottavo  e  nono,  apposita  disciplina,  a seconda che i nosocomi siano  &#13;
 ubicati nel territorio della regione di residenza dell'utente  o  fuori  &#13;
 di  esso.  In  tale  ambito la scelta del medico di fiducia può essere  &#13;
 realizzata anche facendo ricorso  all'attività  libero-  professionale  &#13;
 consentita  ai  sanitari ospedalieri dalle denunciate norme e da quelle  &#13;
 successive (art. 12 della legge 29 giugno 1977, n. 349; art. 47,  comma  &#13;
 terzo, n.  4 della citata legge n. 833 del 1978; art. 35 del d.P.R.  20  &#13;
 dicembre  1979,  n.  761),  che  fanno  riferimento anche a modalità e  &#13;
 limiti previsti da leggi regionali.  Ma la disciplina di tale  rapporto  &#13;
 resta pur sempre subordinata, come ben rileva l'Avvocatura dello Stato,  &#13;
 alle  preminenti  esigenze  organizzative  e funzionali delle strutture  &#13;
 ospedaliere. I limiti posti al riguardo dalle impugnate norme -  limiti  &#13;
 la  cui  fondamentale ratio è stata da questa Corte, con la più volte  &#13;
 richiamata sentenza n. 103  del  1977,  ravvisata  nella  tutela  degli  &#13;
 stessi  principi  posti,  soprattutto nell'interesse del malato, a base  &#13;
 della riforma ospedaliera non appaiono  perciò  lesivi  del  principio  &#13;
 sancito dall'invocato art. 32 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1)  dichiara  manifestamente infondata la questione di legittimità  &#13;
 costituzionale, sollevata, in  riferimento  agli  artt.  3  e  4  della  &#13;
 Costituzione,  con  l'ordinanza  emessa  in  data  16 novembre 1976 dal  &#13;
 pretore di Torino (R.O.   n. 764 del 1976) degli  artt.  43,  lett.  d.  &#13;
 della  legge  12  febbraio  1968, n. 132 (Enti ospedalieri e assistenza  &#13;
 ospedaliera) e 133 del d.P.R. 27 marzo 1969, n.  130  (Stato  giuridico  &#13;
 dei  dipendenti degli enti ospedalieri):  questione già dichiarata non  &#13;
 fondata dalla Corte costituzionale con sentenza n. 103 del 1977;         &#13;
     2) dichiara non fondata la questione di legittimità costituzionale  &#13;
 sollevata,  in  riferimento  all'art.  32   della   Costituzione,   con  &#13;
 l'ordinanza emessa in data 16 novembre 1976 dal pretore di Torino (R.O.  &#13;
 n.  764  del  1976),  degli  artt. 43, lett. d. della legge 12 febbraio  &#13;
 1968, n. 132 (Enti ospedalieri, e assistenza  ospedaliera)  e  133  del  &#13;
 d.P.R. 27 marzo 1969, n. 130 (Stato giuridico dei dipendenti degli enti  &#13;
 ospedalieri).                                                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 25 ottobre 1982.                              &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   -   ANTONINO DE STEFANO  -  GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - GIUSEPPE FERRARI  &#13;
                                   - FRANCESCO SAJA.                      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
