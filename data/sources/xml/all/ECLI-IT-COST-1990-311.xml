<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>311</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:311</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/06/1990</data_decisione>
    <data_deposito>22/06/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 5, in relazione    &#13;
 all'art. 4,  della  legge  della  Regione  Lazio  riapprovata  il  14    &#13;
 febbraio   1990   dal   Consiglio   regionale,  avente  per  oggetto:    &#13;
 "Disciplina del sistema informativo regionale", promosso con  ricorso    &#13;
 del  Presidente  del  Consiglio  dei ministri, notificato il 19 marzo    &#13;
 1990, depositato in cancelleria il 26 successivo ed iscritto al n. 20    &#13;
 del registro ricorsi 1990;                                               &#13;
     Visto l'atto di costituzione della Regione Lazio;                    &#13;
    Udito nell'udienza pubblica del 22 maggio 1990 il Giudice relatore    &#13;
 Francesco Greco;                                                         &#13;
    Uditi l'Avvocato dello Stato Giorgio D'Amato, per il ricorrente, e    &#13;
 l'avv. Achille Chiappetti per la Regione.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Consiglio della Regione Lazio, con deliberazione n. 297    &#13;
 del 20 dicembre 1986, approvava lo  studio  di  fattibilità  per  la    &#13;
 realizzazione  di  un  sistema  informativo  automatizzato presentato    &#13;
 dalla ditta ISED S.p.A. e, nella seduta  del  6  dicembre  1989,  una    &#13;
 legge recante la disciplina del sistema informativo regionale.           &#13;
    Dopo  avere  precisato  all'art.  4  che  i  criteri  e  le  linee    &#13;
 programmatiche  per  l'attuazione  del  sistema  informativo   e   le    &#13;
 caratteristiche  progettuali  di  massima erano indicate nel suddetto    &#13;
 studio, la citata legge, all'art. 5 stabiliva che, in attuazione  del    &#13;
 progetto  di  cui  al  precedente art. 4, la Giunta regionale avrebbe    &#13;
 deliberato   annualmente   un   programma   operativo   che   avrebbe    &#13;
 individuato: a) i settori e le aree di attività; b) gli interventi e    &#13;
 i soggetti tenuti a realizzarlo; c) le modalità e gli  strumenti  di    &#13;
 attuazione; d) i piani di formazione del personale interessato; e) le    &#13;
 risorse finanziarie.                                                     &#13;
     Il  Governo  rinviava  la  legge  al  Consiglio  regionale per il    &#13;
 riesame osservando che l'art. 5 della stessa, autorizzando la  Giunta    &#13;
 regionale  a deliberare un programma operativo annuale, attribuiva ad    &#13;
 essa una potestà regolamentare di spettanza del Consiglio  ai  sensi    &#13;
 dell'art. 121, secondo comma, della Costituzione.                        &#13;
     Il  Consiglio  regionale,  nella  seduta  del  14  febbraio 1990,    &#13;
 riapprovava la legge apportandovi alcune modifiche.                      &#13;
     Il  Presidente  del  Consiglio  dei  ministri, con ricorso del 17    &#13;
 marzo 1990, ha impugnato l'art.  5  in  relazione  all'art.  4  della    &#13;
 stessa legge, nella formulazione successiva al rilievo.                  &#13;
     Deduce  che il Consiglio regionale si è limitato ad apportare al    &#13;
 testo dell'art.  5  modifiche  solamente  formali,  sostituendo  alla    &#13;
 denominazione  "programma  operativo"  quella  di  "previsione  degli    &#13;
 interventi operativi ed attuativi" lasciando,  però,  inalterata  la    &#13;
 sostanza  delle  deliberazioni  attribuite  alla Giunta regionale, le    &#13;
 quali deliberazioni costituiscono esercizio di potestà regolamentare    &#13;
 e ciò in contrasto con l'art. 121 della Costituzione.                   &#13;
     Aggiunge  che  il  contenuto  della  deliberazione  consiliare è    &#13;
 l'approvazione di un semplice studio  di  fattibilità  recante  solo    &#13;
 indicazioni  di  larga  massima.  Sono,  invece, affidate alla Giunta    &#13;
 attività di pianificazione  e  programmazione  implicanti  rilevanti    &#13;
 scelte  strategiche  e sostanziali; previsioni di carattere normativo    &#13;
 in ordine agli obiettivi da perseguire ed i vincoli da rimuovere;  la    &#13;
 deliberazione  di  piani  di  definizione dei settori e delle aree di    &#13;
 attività  da  informatizzare;  la  scelta  degli  interventi  e  dei    &#13;
 soggetti  tenuti  a realizzarli; delle modalità e degli strumenti di    &#13;
 attuazione nonché delle risorse finanziarie.                            &#13;
    Trattasi   di  attività  normativa  di  spettanza  del  Consiglio    &#13;
 regionale. Tanto che le determinazioni inerenti  al  subsistema  già    &#13;
 ipotizzato  nel  citato  studio di fattibilità sono state effettuate    &#13;
 con deliberazione consiliare (n. 363 del 28 maggio 1987).                &#13;
     2.  -  Nel  giudizio  è  intervenuto  il Presidente della Giunta    &#13;
 regionale. Egli ha osservato che i  compiti  attribuiti  alla  Giunta    &#13;
 regionale, secondo quanto risulta dalla stessa legge e dall'art. 5 in    &#13;
 particolare, hanno  natura  esecutiva  ed  operativa  così  come  le    &#13;
 attività ad essa demandate.                                             &#13;
     Peraltro,  in  base  alle  successive  disposizioni  della stessa    &#13;
 legge, le dette attività devono svolgersi secondo i precisi  criteri    &#13;
 stabiliti  dal Consiglio regionale (artt. 7, 8, 9, 10, 11, 12), sotto    &#13;
 il suo costante controllo (artt. 6, ultimo comma, 8, secondo comma) e    &#13;
 nei limiti di spesa da esso stabiliti.                                   &#13;
     Ha  concluso  per  la  infondatezza  o  la inammissibilità della    &#13;
 questione.<diritto>Considerato in diritto</diritto>1.  -  La  Corte  è  chiamata  a decidere se l'art. 5 della legge    &#13;
 regionale del Lazio, approvata il 6 dicembre 1989 e riapprovata il 14    &#13;
 febbraio   1990,   il   quale   demanda   alla  Giunta  regionale  le    &#13;
 deliberazioni annuali degli interventi  operativi  ed  attuativi  del    &#13;
 sistema  informativo  regionale  in  conformità  dei criteri e delle    &#13;
 linee programmatiche di cui all'art. 4 della stessa legge  e  secondo    &#13;
 lo  studio  di  fattibilità  approvato dal Consiglio regionale il 20    &#13;
 dicembre 1986 (deliberazione n. 297), attribuisca o meno  alla  detta    &#13;
 Giunta  regionale potestà regolamentare, in contrasto con l'art. 121    &#13;
 della Costituzione.                                                      &#13;
     2. - La questione non è fondata.                                    &#13;
     Gli  interventi  operativi ed attuativi dei criteri e delle linee    &#13;
 programmatiche per la realizzazione del sistema informativo regionale    &#13;
 nonché   delle   caratteristiche   progettuali  di  massima  per  la    &#13;
 introduzione  dei  processi  di  informazione,  già  deliberati  dal    &#13;
 Consiglio  regionale,  hanno natura meramente esecutiva. Lo si evince    &#13;
 con assoluta certezza dall'esame valutativo delle attività demandate    &#13;
 alla  Giunta  regionale  elencate  nell'art.  5  ora  impugnato. Esse    &#13;
 consistono  nella  individuazione  dei  settori  e  delle   aree   di    &#13;
 attività;   nella  scelta  dei  soggetti  tenuti  a  realizzare  gli    &#13;
 interventi necessari; nella determinazione delle  modalità  e  degli    &#13;
 strumenti  di  attuazione; nella compilazione dei piani di formazione    &#13;
 del personale;  nell'utilizzazione  delle  risorse  finanziarie  già    &#13;
 individuate  e specificate nella stessa legge regionale (art. 14). La    &#13;
 Giunta regionale, in definitiva, predispone ed appresta le  strutture    &#13;
 umane   e  materiali  e  ogni  altro  mezzo  per  attuare  e  rendere    &#13;
 funzionante il sistema informativo  scelto  e  indicato  nelle  linee    &#13;
 fondamentali  dal  Consiglio  regionale.  Essa  opera  in una visione    &#13;
 organica ed integrata tenendo anche conto delle disposizioni e  degli    &#13;
 indirizzi  dello  Stato in materia. Sono predeterminati dal Consiglio    &#13;
 regionale non solo i criteri da osservare ma  anche  i  traguardi  da    &#13;
 raggiungere  e  sono inoltre previsti appositi controlli dello stesso    &#13;
 Consiglio regionale  al  quale  la  Giunta  presenta  ogni  anno  una    &#13;
 relazione  sullo  stato  di  attuazione  dei progetti e dei programmi    &#13;
 operativi  (artt.  6  e  8,  legge  regionale  cit.).  Alla  Regione,    &#13;
 peraltro,  è  riservata  la  promozione  delle  attività degli enti    &#13;
 regionali strumentali, delle  unità  sanitarie  locali,  degli  enti    &#13;
 territoriali  e  di  qualsiasi  altro  ente  locale  anche per quanto    &#13;
 riguarda i dati utili e necessari per lo svolgimento  delle  funzioni    &#13;
 di programmazione e di attuazione. La stessa Regione tiene i rapporti    &#13;
 con gli enti e con lo Stato. In verità la legge regionale  è  molto    &#13;
 complessa  ed  articolata nella determinazione delle competenze della    &#13;
 Giunta e del Consiglio regionale che tiene ben distinte. Alla  Giunta    &#13;
 non  è  affatto conferita una potestà regolamentare ma è demandato    &#13;
 il  compimento  soltanto  di  atti  esecutivi  ed  attuativi.   Ciò,    &#13;
 peraltro, è conforme a quanto dispone lo Statuto della Regione Lazio    &#13;
 secondo cui (art. 22, nn. 1 e 2, legge 22 maggio 1971, n. 346) spetta    &#13;
 alla  Giunta  regionale  l'attuazione  dei  programmi  approvati  dal    &#13;
 Consiglio regionale e la esecuzione delle deliberazioni  prese  dallo    &#13;
 stesso.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 5  della  legge  della  Regione  Lazio  riapprovata  il  14    &#13;
 febbraio 1990 recante "Disciplina del sistema informativo regionale",    &#13;
 in relazione all'art. 4 della stessa legge sollevata, in  riferimento    &#13;
 all'art.  121  della  Costituzione,  dal Presidente del Consiglio dei    &#13;
 Ministri con il ricorso indicato in epigrafe.                            &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 giugno 1990.                               &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 22 giugno 1990.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
