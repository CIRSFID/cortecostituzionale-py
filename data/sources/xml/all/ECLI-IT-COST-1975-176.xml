<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>176</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:176</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Leonetto Amadei</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/06/1975</data_decisione>
    <data_deposito>03/07/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 18 del r.d.  &#13;
 1  maggio  1930,  n.  680,  e  11  della  legge  6 luglio 1939, n. 1035  &#13;
 (trattamento di quiescenza dei sanitari), promosso con ordinanza emessa  &#13;
 l'11 marzo 1972 dalla Corte dei conti - sezione III pensioni  civili  -  &#13;
 sul  ricorso di Gualteroni Mario contro l'Istituto di previdenza presso  &#13;
 il Ministero del tesoro, iscritta al n. 193 del registro ordinanze 1973  &#13;
 e pubblicata nella Gazzetta Ufficiale della Repubblica  n.  198  del  1  &#13;
 agosto 1973.                                                             &#13;
     Visto l'atto di costituzione di Gualteroni Mario;                    &#13;
     udito  nell'udienza  pubblica del 6 maggio 1975 il Giudice relatore  &#13;
 Leonetto Amadei;                                                         &#13;
     udito l'avv. Enrico Schiavone, per il Gualteroni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con decreto del 31 gennaio 1970 la  direzione  generale  degli  &#13;
 Istituti  di  previdenza  concedeva  al  dottor  Mario  Gualteroni, con  &#13;
 decorrenza dal 1 gennaio  1969,  la  pensione  normale  diretta  di  L.  &#13;
 1.000.O72  annue.  Dal computo del servizio utile per il trattamento di  &#13;
 quiescenza l'amministrazione concedente  escludeva  il  periodo  dal  1  &#13;
 novembre  1936  al 31 gennaio 1969 durante il quale il Gualteroni aveva  &#13;
 prestato servizio, come medico condotto, presso l'opera  pia  Azzanelli  &#13;
 Cedrelli  di  Bergamo  simultaneamente  a  quello prestato nelle locali  &#13;
 carceri giudiziarie.                                                     &#13;
     Contro  il  provvedimento  di  liquidazione l'interessato proponeva  &#13;
 ricorso alla Corte dei conti per ottenere il  riconoscimento,  ai  fini  &#13;
 pensionistici,  anche del periodo suindicato ancorché per esso l'opera  &#13;
 pia Azzanelli Cedrelli non avesse  provveduto  ad  apposita  iscrizione  &#13;
 alla   Cassa  per  le  pensioni  ai  sanitari,  essendo  già  in  atto  &#13;
 l'iscrizione alla predetta Cassa per l'attività sanitaria svolta nello  &#13;
 stesso periodo presso le carceri giudiziarie.                            &#13;
     La terza sezione giurisdizionale della Corte dei conti, preso  atto  &#13;
 che,  in forza dell'art. 18 del testo unico approvato con r.d. 1 maggio  &#13;
 1930, n. 680, il cui  contenuto  è  stato  sostanzialmente  riprodotto  &#13;
 nell'art.  11  del  nuovo  ordinamento di previdenza per le pensioni ai  &#13;
 sanitari (legge 6 luglio 1939, n. 1035), le  istituzioni  pubbliche  di  &#13;
 assistenza  e  beneficenza sarebbero esonerate da ogni contributo per i  &#13;
 medici in servizio appartenenti  a  categorie  per  le  quali  leggi  e  &#13;
 regolamenti  prevedano  un  trattamento  di  quiescenza  obbligatorio o  &#13;
 facoltativo,  ha  sollevato,  con  ordinanza  dell'11  marzo  1972,  la  &#13;
 questione  di  legittimità  costituzionale  dei  precitati articoli di  &#13;
 legge, in riferimento all'art. 36,  primo  comma,  della  Costituzione.  &#13;
 Nella  motivazione  dell'ordinanza  si rileva che la mancata iscrizione  &#13;
 alla Cassa per le pensioni dei sanitari in base  alle  norme  impugnate  &#13;
 comporterebbe   per   il   sanitario  la  non  utilizzazione,  ai  fini  &#13;
 pensionistici, del servizio prestato presso un Istituto di assistenza e  &#13;
 beneficenza, qualora tale prestazione sia  stata  simultanea  ad  altro  &#13;
 servizio  che  tale  iscrizione  comporti.  Per  la Corte dei conti una  &#13;
 siffatta situazione realizzerebbe senza altro una ipotesi di  contrasto  &#13;
 con  l'art.  36,  primo  comma,  della Costituzione che garantirebbe ai  &#13;
 lavoratori una retribuzione proporzionata alla quantità e qualità del  &#13;
 lavoro  svolto  in  quanto,  in  base  all'insegnamento   della   Corte  &#13;
 costituzionale  (sentenze  n.  3 del 1966; n. 78 del 1967 e n.  112 del  &#13;
 1968), la pensione avrebbe carattere di retribuzione differita.          &#13;
     2. - Nel giudizio davanti alla Corte si è regolarmente  costituito  &#13;
 il  dottor  Gualteroni,  rappresentato  e difeso dagli avvocati Lorenzo  &#13;
 Suardi ed Enrico Schiavone.                                              &#13;
     La difesa, riprendendo e sviluppando le motivazioni dell'ordinanza,  &#13;
 si riporta anch'essa  alla  interpretazione  che  dell'art.  36,  primo  &#13;
 comma,  della  Costituzione  avrebbe  dato la Corte costituzionale e in  &#13;
 forza della quale sarebbero da ravvisarsi  nella  norma  costituzionale  &#13;
 due    principi,   quello   della   retribuzione   proporzionata,   che  &#13;
 comporterebbe un trattamento retributivo adeguato al  lavoro  prestato,  &#13;
 in  modo  da  impedire  uno  sfruttamento antisociale del lavoratore, e  &#13;
 quello della retribuzione sufficiente. Conseguirebbe da ciò che  nella  &#13;
 valutazione quantitativa e qualitativa della retribuzione differita - e  &#13;
 tale  sarebbe  la  pensione  -  si  dovrebbe  tener  conto dei principi  &#13;
 costituzionali senza distinzione alcuna o limiti particolari, quali che  &#13;
 siano state le modalità delle prestazioni lavorative purché risultino  &#13;
 continuative e riconducibili sul piano  giuridico  ad  un  rapporto  di  &#13;
 lavoro  o  di impiego. Contro tali principi sarebbe in palese contrasto  &#13;
 la disposizione dell'art. 18 del testo unico riprodotto sostanzialmente  &#13;
 nell'art. 11 della legge n.  1035 del 1939.                              &#13;
     In effetti tali norme, indubbiamente disposte a favore delle  opere  &#13;
 pie  al  fine di sgravarle dall'onere del pagamento delle contribuzioni  &#13;
 sociali, non potrebbero essere rivolte in danno del prestatore  d'opera  &#13;
 contenendone e limitandone i diritti in quanto ciò si risolverebbe nel  &#13;
 riversare  su  di  lui  l'onere  di  elargizioni  le quali, in casi del  &#13;
 genere, dovrebbero gravare su chi le dispone, ossia sullo Stato.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con  l'ordinanza  indicata  in  epigrafe viene sollevata, in  &#13;
 riferimento all'art. 36, primo comma, della Costituzione, la  questione  &#13;
 di  legittimità  costituzionale dell'art. 18 del r.d.l. 1 maggio 1930,  &#13;
 n. 680, e dell'art. 11 della legge  6  luglio  1939,  n.  1035, i quali  &#13;
 consentono  che il sanitario, già iscritto alla "cassa per le pensioni  &#13;
 ai  sanitari"  per  una  data  prestazione  professionale  a  carattere  &#13;
 continuativo,    venga    privato    della    parte   differita   della  &#13;
 retribuzione-pensione relativa al simultaneo servizio  prestato  presso  &#13;
 un  Istituto di assistenza e beneficenza, esonerato dall'iscrizione del  &#13;
 sanitario stesso alla Cassa.                                             &#13;
     2. - La questione è fondata nei limiti in prosieguo precisati.      &#13;
     L'art. 18 del r.d. 1 maggio 1930, n. 680  (approvazione  del  testo  &#13;
 unico  delle  leggi  sulla  Cassa  di  previdenza  per  le pensioni dei  &#13;
 sanitari), stabilisce che le istituzioni di beneficenza sono  esonerate  &#13;
 da  ogni  contributo  quando  si  valgano  di medici già iscritti alla  &#13;
 Cassa. Tale disposizione di legge è stata ripresa dall'art.  11  della  &#13;
 legge 6 luglio 1939, n. 1035 (approvazione dell'ordinamento della Cassa  &#13;
 di  previdenza  per  le  pensioni dei sanitari), il quale, a sua volta,  &#13;
 esonera le istituzioni di assistenza e beneficenza da  ogni  contributo  &#13;
 per  i  medici  in  servizio già provvisti di pensione, che non sia di  &#13;
 guerra  né  privilegiata  ordinaria,  o  che  appartengono  a   quelle  &#13;
 categorie  per le quali leggi e regolamenti prevedono un trattamento di  &#13;
 quiescenza obbligatorio o facoltativo.                                   &#13;
     Questa Corte con  la  sentenza  n.  3  del  1966  ha  affermato  il  &#13;
 principio,   ribadito  in  successive  sentenze,  che  "in  riferimento  &#13;
 all'art. 36 della Costituzione la retribuzione  dei  lavoratori,  tanto  &#13;
 quella  corrisposta  nel  corso  del  rapporto di lavoro, quanto quella  &#13;
 differita, a fini previdenziali, alla cessazione di  tale  rapporto,  e  &#13;
 corrisposta sotto forma di trattamento di liquidazione o di quiescenza,  &#13;
 a  seconda  dei  casi,  allo  stesso lavoratore o ai suoi aventi causa,  &#13;
 rappresenta nel vigente ordinamento costituzionale (che,  tra  l'altro,  &#13;
 l'art.  1  della  Costituzione definisce fondato su lavoro) una entità  &#13;
 fatta  oggetto,  sul  piano  morale  e  su  quello   patrimoniale,   di  &#13;
 particolare protezione".                                                 &#13;
     È  da  rilevare  che  tale  principio,  conforme  al  dettato  del  &#13;
 precitato art. 36 della Costituzione che garantisce il diritto  per  il  &#13;
 lavoratore  ad una retribuzione proporzionata alla quantità e qualità  &#13;
 del suo lavoro, trova piena applicazione anche nel caso di specie,  una  &#13;
 volta riconosciuto che la pensione deve essere considerata una forma di  &#13;
 retribuzione  differita, direttamente legata alla natura e agli aspetti  &#13;
 del rapporto di lavoro prestato.                                         &#13;
     Sussiste,  pertanto,  una  incompatibilità  con  l'art.  36  della  &#13;
 Costituzione,  nelle  limitazioni  che  le  norme  impugnate pongono al  &#13;
 diritto al trattamento economico che dovrebbe  competere  al  sanitario  &#13;
 alla  cessazione  del  rapporto di prestazione d'opera, per il semplice  &#13;
 fatto della coesistenza con altro rapporto intercorso con  un  ente  di  &#13;
 beneficenza o di assistenza.                                             &#13;
     Il trattamento preferenziale che il legislatore ha inteso riservare  &#13;
 agli   Istituti   di   assistenza   e   beneficenza,  esonerandoli,  in  &#13;
 considerazione  delle  finalità  sociali  da  essi   perseguiti,   dal  &#13;
 pagamento  dei  contributi  assicurativi  per i sanitari che si trovino  &#13;
 nelle  particolari  condizioni   previste   dalla   legge,   non   può  &#13;
 ragionevolmente riversare i propri effetti a danno dei sanitari stessi.  &#13;
 La posizione retributiva di questi deve essere commisurata, a tutti gli  &#13;
 effetti  e, quindi, anche a quelli conseguenti al collocamento a riposo  &#13;
 o, comunque, alla cessazione del rapporto  di  lavoro,  al  trattamento  &#13;
 economico   goduto   in  attività  di  servizio  considerato  nel  suo  &#13;
 complesso.                                                               &#13;
     Né varrebbe obiettare che il secondo comma  dell'art.    11  della  &#13;
 legge  n.  1035  del 1939 offre la possibilità al sanitario di potersi  &#13;
 iscrivere facoltativamente alla  Cassa  di  previdenza  corrispondendo,  &#13;
 oltre  al proprio, anche il contributo dell'ente, per cui egli verrebbe  &#13;
 garantito in ordine al conseguimento di un  trattamento  di  quiescenza  &#13;
 superiore  a  quello  che  gli  spetterebbe  se  non si avvalesse della  &#13;
 riconosciuta facoltà.                                                   &#13;
     Il porre a carico del prestatore d'opera  la  corresponsione  anche  &#13;
 dei  tributi propri del datore di lavoro contrasta con quella che è la  &#13;
 struttura del sistema previdenziale e rappresenta una  imposizione  che  &#13;
 costituisce,  all'atto  pratico,  una  riduzione  non  ragionevole  del  &#13;
 trattamento economico. Se, pertanto, ben può rientrare nei compiti del  &#13;
 legislatore regolare, in casi particolari socialmente apprezzabili, con  &#13;
 aspetti e forme derogatrici dei criteri generalmente  seguiti,  l'onere  &#13;
 delle  contribuzioni  assicurative  che  fanno  capo  ad enti pubblici,  &#13;
 tuttavia ciò egli potrà farlo solo nei limiti in cui restino salvi  i  &#13;
 diritti che la Costituzione garantisce ai prestatori d'opera in tema di  &#13;
 retribuzione, qualunque siano gli aspetti che questa assume.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità costituzionale degli artt. 18 del r.d.l.  &#13;
 lo maggio 1930, n. 680, e 11  della  legge  6  luglio  1939,  n.  1035,  &#13;
 limitatamente  alle  parti  in  cui  escludono  per  il sanitario, già  &#13;
 iscritto  alla  Cassa  per  le  pensioni  ai  sanitari  per  una   data  &#13;
 prestazione  professionale,  il  trattamento  pensionistico relativo al  &#13;
 simultaneo  servizio  prestato  presso  un  Istituto  di  assistenza  e  &#13;
 beneficenza   esonerato  dalla  iscrizione  del  sanitario  alla  Cassa  &#13;
 predetta.                                                                &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  Costituzionale,  &#13;
 Palazzo della Consulta, il 18 giugno 1975.                               &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRTSAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI - MICHELE ROSSANO.      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
