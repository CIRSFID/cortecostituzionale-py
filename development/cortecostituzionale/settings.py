import os

from dotenv import load_dotenv

__here__ = os.path.abspath(os.path.dirname(__file__))

load_dotenv()

DATA_DIR = os.environ.get(
    "CORTE_COST_DATA_DIR",
    os.path.abspath(os.path.join(__here__, os.pardir, os.pardir, "data"))
)

os.makedirs(DATA_DIR, exist_ok=True)

DB_PATH = os.environ.get(
    "CORTE_COST_DB_PATH",
    os.path.join(DATA_DIR, "cortecost.db")
)

EXIST_DB = {
    "host": os.environ.get("EXIST_DB_HOST", "localhost"),
    "port": os.environ.get("EXIST_DB_PORT", 8080),
    "user": os.environ.get("EXIST_DB_USER", "admin"),
    "password": os.environ.get("EXIST_DB_PASSWORD", ""),
    "default_collection": os.environ.get("EXIST_DB_COLLECTION", "corte-costituzionale"),
}

if __name__ == '__main__':
    print(__here__)
    print(DATA_DIR)
    print(DB_PATH)
