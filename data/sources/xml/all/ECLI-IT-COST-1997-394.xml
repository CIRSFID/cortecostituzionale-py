<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>394</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:394</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/11/1997</data_decisione>
    <data_deposito>11/12/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. &#13;
 Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale &#13;
 MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 13 della  legge    &#13;
 26  luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e sulla    &#13;
 esecuzione delle  misure  privative  e  limitative  della  libertà),    &#13;
 promosso  con  ordinanza  emessa  il 5 dicembre 1996 dal Tribunale di    &#13;
 sorveglianza di Perugia, iscritta al n. 102  del  registro  ordinanze    &#13;
 1997  e  pubblicata  nella Gazzetta Ufficiale della Repubblica n. 12,    &#13;
 prima serie speciale, dell'anno 1997.                                    &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del 29 ottobre 1997 il giudice    &#13;
 relatore Valerio Onida;                                                  &#13;
   Ritenuto che, con ordinanza emessa il 5 dicembre 1996, pervenuta  a    &#13;
 questa  Corte  il  25  febbraio 1997, il Tribunale di sorveglianza di    &#13;
 Perugia ha sollevato questione  di  legittimità  costituzionale,  in    &#13;
 riferimento  agli  artt.  27,  terzo  comma,  e 3 della Costituzione,    &#13;
 dell'art.  13  della  legge   26   luglio   1975,   n.   354   (Norme    &#13;
 sull'ordinamento   penitenziario  e  sulla  esecuzione  delle  misure    &#13;
 privative e limitative della libertà);                                  &#13;
     che il Tribunale remittente, chiamato a decidere su  due  istanze    &#13;
 di liberazione anticipata di un detenuto, afferma di non essere nelle    &#13;
 condizioni  di  poter  valutare se sia da concedersi o meno la misura    &#13;
 richiesta, e cioè se l'instante abbia profittato delle  opportunità    &#13;
 di  trattamento, e sia meritevole della misura, in quanto l'equipe di    &#13;
 trattamento ha riferito che il detenuto, essendo sottoposto a  regime    &#13;
 differenziato  in base a provvedimento ministeriale adottato ai sensi    &#13;
 dell'art. 41-bis, comma 2,  dell'ordinamento  penitenziario,  non  ha    &#13;
 potuto fruire del trattamento individualizzato previsto dall'art.  13    &#13;
 dell'ordinamento  penitenziario,  né ha potuto essere effettuata nei    &#13;
 suoi confronti l'osservazione scientifica della personalità prevista    &#13;
 dal medesimo art. 13;                                                    &#13;
     che secondo il giudice a quo tale situazione non sarebbe conforme    &#13;
 a  legge  e  ai  principi  costituzionali  citati,  costituendo  anzi    &#13;
 "violazione di precise norme costituzionali con conseguente  denegata    &#13;
 giustizia";                                                              &#13;
     che  è  intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 Ministri, chiedendo che la questione sia dichiarata non fondata;         &#13;
     che l'interveniente  rileva  che  il  giudice  a  quo  ha  errato    &#13;
 nell'identificare  le  norme da sottoporre a sindacato, che avrebbero    &#13;
 semmai  dovuto  essere  gli  artt.  41-bis  e   54   dell'ordinamento    &#13;
 penitenziario,   nella   parte  in  cui  si  sostiene  precludano  la    &#13;
 concessione  della  liberazione  anticipata  al  detenuto   nei   cui    &#13;
 confronti   sia   stata  disposta  la  sospensione  delle  regole  di    &#13;
 trattamento, e non l'art. 13, che si limita a stabilire, in positivo,    &#13;
 i principi di individualizzazione del trattamento e  di  osservazione    &#13;
 della  personalità;  e che, in ogni caso, la pretesa preclusione non    &#13;
 sussiste, in quanto, ai  fini  della  concessione  della  liberazione    &#13;
 anticipata,  in  mancanza di un programma individuale di trattamento,    &#13;
 il giudice dovrebbe basarsi sulla regolarità del  comportamento  del    &#13;
 detenuto,  ai  sensi  dell'art.  94  del  regolamento  di  esecuzione    &#13;
 dell'ordinamento penitenziario, di cui al d.P.R. 29 aprile  1976,  n.    &#13;
 431.                                                                     &#13;
   Considerato  che  la  lamentata impossibilità di valutare se debba    &#13;
 concedersi  la  liberazione  anticipata,  a  causa  dell'assenza   di    &#13;
 trattamento  e  di  osservazione  della  personalità  del  detenuto,    &#13;
 sottoposto al regime differenziato di cui all'art. 41-bis,  comma  2,    &#13;
 dell'ordinamento penitenziario (impossibilità peraltro da escludere,    &#13;
 quando   si   interpreti   quest'ultima  norma  in  modo  conforme  a    &#13;
 Costituzione: sent.  n.  376  del  1997),  non  può  in  alcun  modo    &#13;
 ricondursi  alla impugnata disposizione dell'art. 13 dell'ordinamento    &#13;
 penitenziario  che  stabilisce  in  via  generale   i   principi   di    &#13;
 individualizzazione   del   trattamento   e   di  osservazione  della    &#13;
 personalità   nel    corso    dell'esecuzione    bensì,    casomai,    &#13;
 all'applicazione  del  medesimo  art. 41-bis, comma 2, e dell'art. 54    &#13;
 dell'ordinamento penitenziario in materia di liberazione anticipata;     &#13;
     che  pertanto  la  questione  è  posta  nei  riguardi   di   una    &#13;
 disposizione  del  tutto  estranea alla situazione e alle conseguenze    &#13;
 lamentate dal remittente, onde  essa  deve  ritenersi  manifestamente    &#13;
 inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 13 della legge 26 luglio  1975,    &#13;
 n. 354 (Norme sull'ordinamento penitenziario e sulla esecuzione delle    &#13;
 misure   privative   e  limitative  della  libertà),  sollevata,  in    &#13;
 riferimento agli artt.  27, terzo comma, e 3 della Costituzione,  dal    &#13;
 tribunale  di  sorveglianza  di  Perugia, con l'ordinanza indicata in    &#13;
 epigrafe.                                                                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 27 novembre 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                          Il redattore: Onida                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria l'11 dicembre 1997.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
