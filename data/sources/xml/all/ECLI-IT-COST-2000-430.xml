<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>430</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:430</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/10/2000</data_decisione>
    <data_deposito>19/10/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Cesare RUPERTO, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 38 del d.P.R.    &#13;
 29  settembre  1973,  n. 602  (Disposizioni  sulla  riscossione delle    &#13;
 imposte sul reddito), promosso con ordinanza emessa il 12 luglio 1999    &#13;
 dalla  Commissione  tributaria  regionale di Roma sui ricorsi riuniti    &#13;
 proposti  da  Marziale  Michele  ed  altri c/ la D.R.E. per il Lazio,    &#13;
 iscritta  al  n. 745  del  registro ordinanze 1999 e pubblicata nella    &#13;
 Gazzetta   Ufficiale   della  Repubblica  n. 4,  1ª  serie  speciale,    &#13;
 dell'anno 2000.                                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella camera di consiglio del 27 settembre 2000 il giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
     Ritenuto  che,  nel corso di giudizi riuniti d'appello - promossi    &#13;
 da contribuenti avverso sentenze di primo grado che avevano respinto,    &#13;
 per  intervenuta  decadenza  ex art. 38 del d.P.R. 29 settembre 1973,    &#13;
 n. 602  (Disposizioni  sulla  riscossione delle imposte sul reddito),    &#13;
 domande di rimborso di ritenute IRPEF operate e versate dal datore di    &#13;
 lavoro  su  componenti  delle  retribuzioni  corrisposte  nel periodo    &#13;
 1974-1990   -  la  Commissione  tributaria  regionale  di  Roma,  con    &#13;
 ordinanza  emessa  il  12  luglio  1999,  ha  sollevato  questione di    &#13;
 legittimità costituzionale di tale norma "nella parte in cui pone un    &#13;
 termine   decadenziale   al  potere  di  richiedere  il  rimborso  di    &#13;
 illegittime  ritenute  alla  fonte  e  ne  sancisce la decorrenza del    &#13;
 termine breve dal versamento eseguito dal sostituto d'imposta";          &#13;
         che, a premessa dei prospettati dubbi di incostituzionalità,    &#13;
 la  Commissione rimettente puntualizza, in fatto, che la controversia    &#13;
 tributaria  al  suo  esame  è  stata  promossa  nel 1995 con ricorsi    &#13;
 avverso il silenzio-rifiuto sulle istanze amministrative di rimborso,    &#13;
 dagli  interessati  avanzate  (in  quello  stesso  anno)  dopo che le    &#13;
 Sezioni  unite  della  Corte di cassazione - risolvendo (con sentenza    &#13;
 n. 1436   del  1994)  questione  di  giurisdizione  sorta  nel  corso    &#13;
 dell'originario giudizio di risarcimento danni, instaurato davanti al    &#13;
 giudice  ordinario nel 1985 dai dipendenti contro il datore di lavoro    &#13;
 per  le  errate ritenute - aveva affermato il carattere pregiudiziale    &#13;
 del giudizio sulla legittimità della ritenuta fiscale;                  &#13;
         che,  secondo  il  rimettente, il così detto favor fisci non    &#13;
 appare  più  rispondente alle esigenze di giustizia sostanziale, né    &#13;
 può  continuare  a  giustificare  disparità di trattamento a favore    &#13;
 dell'erario,  come  dimostrato  da  recenti  modifiche  normative (in    &#13;
 particolare  quella  introdotta  dall'art. 1, comma 5, della legge 13    &#13;
 maggio  1999, n. 133, che ha modificato il termine de quo, elevandolo    &#13;
 per  il futuro a quarantotto mesi), nonché da sopravvenuti mutamenti    &#13;
 giurisprudenziali in tema di rimborso delle imposte riscosse mediante    &#13;
 versamento diretto;                                                      &#13;
         che  dunque la norma censurata, nello stabilire un termine di    &#13;
 decadenza  di  diciotto  mesi  per la domanda di rimborso, si pone in    &#13;
 contrasto:                                                               &#13;
           a)    con    l'art. 3   Cost.,   per   l'irragionevole   ed    &#13;
 ingiustificata  disparità  di  trattamento riservata al contribuente    &#13;
 attore  in  condictio  indebiti rispetto al ben più ampio termine ad    &#13;
 agire accordato all'Amministrazione per l'accertamento tributario (ex    &#13;
 artt. 43  del  d.P.R.  29  settembre 1973, n. 600, e 57 del d.P.R. 26    &#13;
 ottobre 1972, n. 633);                                                   &#13;
           b)  ancora  con  l'art. 3  Cost.,  sotto  il  profilo della    &#13;
 disparità di trattamento rispetto a quanto previsto, con riferimento    &#13;
 ad  analoga  situazione,  dall'art. 37 dello stesso d.P.R. n. 602 del    &#13;
 1973, che stabilisce il diverso termine prescrizionale di dieci anni,    &#13;
 uguale a quello sancito in generale per i crediti dal codice civile;     &#13;
           c)  conseguentemente  anche  con l'art. 53 Cost., poiché -    &#13;
 nella  impossibilità  per  il  contribuente sostituito di monitorare    &#13;
 consapevolmente   la   regolarità   delle  operazioni  demandate  al    &#13;
 sostituto e l'esattezza dei versamenti da questo operati all'erario -    &#13;
 la previsione di un così breve termine si risolve in una sostanziale    &#13;
 espropriazione, senza "giustificazione contributiva", delle somme che    &#13;
 il sostituito ha diritto di ripetere;                                    &#13;
           d)  con  l'art. 24  Cost., giacché - avuto riguardo, da un    &#13;
 lato,  alla  mancata  previsione  di  un  obbligo  del  sostituto  di    &#13;
 attivarsi per il recupero della ritenuta erroneamente operata (le cui    &#13;
 conseguenze sostanziali riguardano il solo sostituito) e, dall'altro,    &#13;
 alla rilevata ed inderogabile pregiudizialità della causa tributaria    &#13;
 sulla  legittimità  della  ritenuta  stessa,  rispetto all'azione di    &#13;
 risarcimento   esperibile  dal  contribuente  -  la  fissazione  d'un    &#13;
 identico  termine  per  la  domanda  di  rimborso,  tanto se proposta    &#13;
 dall'autore   del   versamento   quanto   se   avanzata  dal  diretto    &#13;
 interessato,  praticamente viene a sancire (per il combinato disposto    &#13;
 della  norma  censurata  con  l'art. 2  del  d.lgs. 31 dicembre 1992,    &#13;
 n. 546)  la  irresponsabilità  del  primo  privando il secondo della    &#13;
 possibilità di azionare la pretesa risarcitoria;                        &#13;
         che  è intervenuto il Presidente del Consiglio dei Ministri,    &#13;
 rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato,    &#13;
 concludendo   per   l'irrilevanza   della   questione   sollevata  in    &#13;
 riferimento  all'art. 24 Cost., e per la non fondatezza relativamente    &#13;
 agli altri parametri evocati.                                            &#13;
     Considerato  che  la denunciata norma - già sottoposta al vaglio    &#13;
 di legittimità costituzionale, con riferimento agli stessi parametri    &#13;
 -  è stata da questa Corte più volte ritenuta immune dai denunciati    &#13;
 vizi;                                                                    &#13;
         che,  in  particolare, relativamente al profilo dell'asserita    &#13;
 ingiustificata  disparità  di  trattamento  rispetto alla disciplina    &#13;
 dell'art. 37  dello  stesso  d.P.R. n. 602 del 1973 (dove è fissato,    &#13;
 per la presentazione dell'istanza di rimborso delle ritenute dirette,    &#13;
 il  termine  di  prescrizione  di  dieci  anni),  è  sufficiente qui    &#13;
 riaffermare   l'eterogeneità  delle  situazioni  poste  a  raffronto    &#13;
 (ordinanze n. 305 del 1985, n. 545 del 1987, n. 145 del 1990);           &#13;
         che,  infatti,  gli  artt. 37 e 38 del d.P.R. n. 602 del 1973    &#13;
 afferiscono  a  meccanismi di riscossione del tributo aventi spiccata    &#13;
 autonomia  e  caratteristiche  del  tutto peculiari, nel contesto dei    &#13;
 quali  la  procedura di rimborso viene a trarre origine, nell'un caso    &#13;
 (ritenuta  diretta) da un comportamento erroneo riconducibile al solo    &#13;
 ente  creditore  del  tributo,  senza alcun concorso del debitore, il    &#13;
 quale perciò ha diritto di ripetere quanto indebitamente trattenuto;    &#13;
 nell'altro caso (versamento diretto), da un comportamento ascrivibile    &#13;
 allo  stesso  contribuente  (eventualmente  a  mezzo  di un sostituto    &#13;
 d'imposta),   sul   quale  grava  dunque  l'onere  di  richiedere  la    &#13;
 restituzione  di  quanto  non  dovuto  entro un termine di decadenza,    &#13;
 così operandosi un contemperamento del diritto alla restituzione con    &#13;
 l'interesse  pubblicistico di garantire la necessaria celerità di un    &#13;
 gettito fiscale certo;                                                   &#13;
         che,   pertanto,   l'assenza   nelle   due   fattispecie   di    &#13;
 caratteristiche  di  omogeneità  che  ne  impongano  una  disciplina    &#13;
 unitaria,  porta immediatamente ad escludere la comparabilità tra di    &#13;
 esse,  al  fine  di individuare un vulnus al principio di uguaglianza    &#13;
 (laddove la diversità di natura, di presupposti e di struttura delle    &#13;
 fattispecie   stesse  non  viene  attenuata  dal  carattere  generale    &#13;
 dell'a'mbito  di  operatività  riconosciuto  dalla giurisprudenza ai    &#13;
 rimedi disciplinati dalle norme in esame);                               &#13;
         che  altrettanto inidonee ad assurgere a tertia comparationis    &#13;
 risultano  le  richiamate discipline degli artt. 43 del d.P.R. n. 600    &#13;
 del  1973  e  57  del d.P.R. n. 633 del 1972, le quali attengono alle    &#13;
 procedure  di accertamento dei tributi da parte dell'amministrazione,    &#13;
 rispondenti  ad esigenze affatto diverse (ordinanze n. 871 del 1988 e    &#13;
 n. 322 del 1992);                                                        &#13;
         che,  riguardo  poi  alla  dedotta lesione dell'art. 24 della    &#13;
 Costituzione  - anche a prescindere dalla dubbia ammissibilità della    &#13;
 questione, la quale (nei termini prospettati) sembrerebbe estranea al    &#13;
 thema  decidendum  del  giudizio  tributario  a  quo assumendo semmai    &#13;
 rilevanza   nell'eventuale   diversa   controversia,   di  competenza    &#13;
 dell'A.G.O.,   avente   ad   oggetto   la  domanda  risarcitoria  del    &#13;
 contribuente  sostituito  nei confronti del sostituto d'imposta, - va    &#13;
 ancora  una  volta  ribadito,  in  via  del  tutto assorbente, che al    &#13;
 legislatore  è consentito di determinare, in relazione alle esigenze    &#13;
 dei  singoli  procedimenti,  le modalità di esercizio del diritto di    &#13;
 difesa,  il  quale  non  risulta  menomato dal sistema previsto dalla    &#13;
 norma  impugnata, stante la già affermata congruità del termine, in    &#13;
 armonia col sistema tributario (sentenza n. 494 del 1991 ed ordinanza    &#13;
 n. 5 del 1996);                                                          &#13;
         che  costituisce  appunto esercizio di detta discrezionalità    &#13;
 il successivo intervento attuato con la legge 13 maggio 1999, n. 133,    &#13;
 la  quale  (all'art. 1, comma 5, non applicabile ratione temporis nel    &#13;
 giudizio a quo), ha elevato da diciotto a quarantotto mesi il termine    &#13;
 previsto  dalla  norma  impugnata,  conservandone  peraltro la natura    &#13;
 decadenziale;                                                            &#13;
         che   anche   con   riferimento  alla  denunciata  violazione    &#13;
 dell'art. 53  Cost.,  va  ancora  una volta ribadito che il principio    &#13;
 della  capacità  contributiva,  riguardando l'idoneità del soggetto    &#13;
 dell'obbligazione  d'imposta, attiene alla garanzia sostanziale della    &#13;
 proporzionalità  dell'imposta stessa alla capacità del contribuente    &#13;
 e  non  può,  quindi, riferirsi alla materia del processo tributario    &#13;
 (cfr. sentenza n. 18 del 2000 ed ordinanza n. 322 del 1992);             &#13;
         che,  pertanto,  la  sollevata  questione  è  manifestamente    &#13;
 infondata, con riferimento a tutti i parametri evocati.                  &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 38  del  d.P.R.  29 settembre    &#13;
 1973,  n. 602  (Disposizioni  sulla  riscossione  delle  imposte  sul    &#13;
 reddito),  sollevata,  in  riferimento  agli  artt. 3, 24 e 53 Cost.,    &#13;
 dalla  Commissione  tributaria  regionale  di  Roma,  con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 ottobre 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Ruperto                            &#13;
                        Il cancelliere: Malvica                           &#13;
     Depositata in cancelleria il 19 ottobre 2000.                        &#13;
                        Il Cancelliere: Malvica</dispositivo>
  </pronuncia_testo>
</pronuncia>
