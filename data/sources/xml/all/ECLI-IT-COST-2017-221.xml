<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2017</anno_pronuncia>
    <numero_pronuncia>221</numero_pronuncia>
    <ecli>ECLI:IT:COST:2017:221</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GROSSI</presidente>
    <relatore_pronuncia>Giorgio Lattanzi</relatore_pronuncia>
    <redattore_pronuncia>Giorgio Lattanzi</redattore_pronuncia>
    <data_decisione>27/09/2017</data_decisione>
    <data_deposito>25/10/2017</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 553 e 554 del codice di procedura penale, promosso dal Giudice per le indagini preliminari del Tribunale ordinario di Tivoli, nel procedimento penale a carico di A. C. ed altro, con ordinanza del 12 aprile 2016, iscritta al n. 138 del registro ordinanze 2016 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 33, prima serie speciale, dell'anno 2016. &#13;
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 27 settembre 2017 il Giudice relatore Giorgio Lattanzi.&#13;
 Ritenuto che il Giudice per le indagini preliminari del Tribunale ordinario di Tivoli, con ordinanza del 12 aprile 2016 (r.o. n. 138 del 2016), ha sollevato questioni di legittimità costituzionale degli artt. 553 e 554 del codice di procedura penale, in riferimento agli artt. 3, 25, primo comma, e 111 della Costituzione;&#13;
 che l'art. 553 cod. proc. pen. è censurato «nella parte in cui non impone al Pubblico ministero la notificazione del decreto di citazione a giudizio entro un termine prestabilito ovvero nella parte in cui non impone, allo stesso Pubblico ministero, di provvedere all'immediata trasmissione degli atti al Giudice per il dibattimento prima che venga curata la notificazione del decreto»;&#13;
 che l'art. 554 cod. proc. pen. è censurato «nella parte in cui prevede la competenza cautelare del giudice per le indagini preliminari senza che ricorrano presupposti di urgenza o circostanze eccezionali»;&#13;
 che il rimettente riferisce di avere trasmesso al giudice competente per il giudizio una domanda di dissequestro di un bene, dopo che il pubblico ministero aveva già emesso il decreto di citazione a giudizio;&#13;
 che il Presidente del Tribunale in composizione collegiale competente per il giudizio aveva ritrasmesso il fascicolo al Giudice per le indagini preliminari, ritenendone la competenza a provvedere sulla domanda di dissequestro;&#13;
 che in base all'art. 554 cod. proc. pen. il giudice per le indagini preliminari è competente ad assumere gli atti urgenti a norma dell'art. 467 cod. proc. pen. e a provvedere sulle misure cautelari fino a quando il decreto di citazione a giudizio, unitamente al fascicolo per il dibattimento, non sia trasmesso al giudice competente per il giudizio ai sensi dell'art. 553 cod. proc. pen.;&#13;
 che in base a quest'ultima disposizione il pubblico ministero deve trasmettere al giudice competente per il giudizio il fascicolo e il decreto di citazione «immediatamente dopo» la notificazione del decreto all'imputato, al suo difensore e alla parte offesa, come prevede l'art. 552, comma 3, cod. proc. pen.;&#13;
 che il giudice a quo, preso atto che le disposizioni censurate gli attribuiscono la competenza a provvedere in merito al dissequestro, ha sostenuto che esse sono lesive degli artt. 3, 25, primo comma, e 111 Cost.;&#13;
 che l'art. 553 cod. proc. pen. non reca alcun termine entro il quale il pubblico ministero è tenuto a notificare il decreto di citazione a giudizio e quindi, secondo il rimettente, potrebbe intercorrere un lungo lasso di tempo durante il quale il pubblico ministero, omettendo di procedere alla notificazione e di formare il fascicolo dibattimentale, impedisce che si creino le condizioni legali perché il giudice competente per il giudizio acquisisca altresì la competenza sugli atti urgenti, che resta invece radicata presso il giudice per le indagini preliminari;&#13;
 che il rimettente ritiene che tale assetto sia in contrasto con il principio di precostituzione per legge del giudice naturale (art. 25, primo comma, Cost.), perché la competenza a provvedere sugli atti urgenti dipenderebbe da una scelta del pubblico ministero sul tempo di trasmissione degli atti, per quanto determinata da «esigenze interne di funzionamento dei carichi e dei flussi di lavoro»;&#13;
 che inoltre sarebbe violato il principio di uguaglianza (art. 3 Cost.), perché «l'individuazione del cd. giudice della domanda cautelare non soggiace ad alcun criterio obiettivo e verificabile, ma è rimessa all'iniziativa del solo Pubblico ministero»;&#13;
 che per identiche ragioni sarebbero violati anche i «precetti costituzionali in tema di giusto processo (art. 111 Cost.)»;&#13;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni siano dichiarate inammissibili, e, nel merito, infondate;&#13;
 che l'Avvocatura dello Stato ha eccepito l'inammissibilità delle questioni, perché il rimettente non avrebbe, né descritto la fattispecie, né motivato sulla rilevanza, e nel merito ha sostenuto che eventuali ritardi del pubblico ministero nella formazione del fascicolo e nella sua trasmissione al giudice competente per il giudizio rileverebbero solo ai fini disciplinari ma non potrebbero incidere sulla competenza a provvedere sugli atti urgenti;&#13;
 che tale competenza resterebbe invece determinata senza ambiguità dalle disposizioni di legge in questione e non da valutazioni della pubblica accusa.&#13;
 Considerato che il Giudice per le indagini preliminari del Tribunale ordinario di Tivoli, in riferimento agli artt. 3, 25, primo comma, e 111 della Costituzione, ha sollevato questioni di legittimità costituzionale dell'art. 553 del codice di procedura penale, «nella parte in cui non impone al Pubblico ministero la notificazione del decreto di citazione a giudizio entro un termine prestabilito ovvero nella parte in cui non impone, allo stesso Pubblico ministero, di provvedere all'immediata trasmissione degli atti al Giudice per il dibattimento prima che venga curata la notificazione del decreto»;&#13;
 che le questioni così formulate sono manifestamente inammissibili a causa del loro carattere ancipite, dato che il rimettente le ha prospettate in termini alternativi, senza indicare quale soluzione ritiene prioritariamente imposta dalla Costituzione (ex plurimis, sentenze n. 22 del 2016 e n. 248 del 2014), «ciò che devolverebbe a questa Corte l'impropria competenza di scegliere tra esse» (ordinanza n. 130 del 2017);&#13;
 che, in riferimento agli stessi articoli 3, 25, primo comma, e 111 Cost., il Giudice per le indagini preliminari ha censurato anche l'art. 554 cod. proc. pen., «nella parte in cui prevede la competenza cautelare del giudice per le indagini preliminari senza che ricorrano presupposti di urgenza o circostanze eccezionali»;&#13;
 che le questioni sono manifestamente inammissibili per l'assoluta carenza di motivazione a sostegno della loro asserita non manifesta infondatezza;&#13;
 che tali profili assorbono ogni ulteriore causa di inammissibilità.&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale degli artt. 553 e 554 del codice di procedura penale, sollevate, in riferimento agli artt. 3, 25, primo comma, e 111 della Costituzione, dal Giudice per le indagini preliminari del Tribunale ordinario di Tivoli, con l'ordinanza indicata in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 27 settembre 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Giorgio LATTANZI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 25 ottobre 2017.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
