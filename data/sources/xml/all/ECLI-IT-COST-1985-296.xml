<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>296</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:296</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/11/1985</data_decisione>
    <data_deposito>13/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale degli artt. 12, primo  &#13;
 comma, e 24, primo comma, della legge della  Provincia  di  Bolzano  20  &#13;
 agosto  1972,  n.  15  ("Legge  di  riforma dell'edilizia abitativa") e  &#13;
 successive modificazioni, promossi con ordinanze emesse rispettivamente  &#13;
 il 15 maggio, il 26 giugno, il 12 giugno, il 17 aprile (n. 2 ord.),  19  &#13;
 giugno  e 3 luglio 1984 dalla Corte di Appello di Trento ed iscritte ai  &#13;
 nn. 996, 1056, 1057, 1058, 1059, 1060 e  1061  del  registro  ordinanze  &#13;
 1984 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 32 bis,  &#13;
 34 bis, 38 bis e 47 bis dell'anno 1985;                                  &#13;
     udito  nella  camera  di  consiglio  del  9 ottobre 1985 il Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
                              Ritenuto che:                               &#13;
     1.1 - la Corte d'Appello di Trento, con le sette ordinanze indicate  &#13;
 in epigrafe, ha  sollevato  questione  di  legittimità  costituzionale  &#13;
 dell'art.  12,  primo  e  terzo  comma,  della legge della Provincia di  &#13;
 Bolzano 20 agosto 1972, n. 15 e  successive  modificazioni  ("Legge  di  &#13;
 riforma  dell'edilizia  abitativa"),  in  riferimento agli artt. 3 e 42  &#13;
 Cost.;                                                                   &#13;
     1.2 - il giudice rimettente, premesso che nei  giudizi  di  merito,  &#13;
 tutti  concernenti  opposizione  alla determinazione dell'indennità di  &#13;
 esproprio,  vengono  in  considerazione  aree   dotate   di   vocazione  &#13;
 edificatoria  (donde la rilevanza della questione), deduce che la norma  &#13;
 censurata  impone,  nel  dettare  i  criteri  per   la   determinazione  &#13;
 dell'indennità,  di  considerare  i terreni espropriati come agricoli,  &#13;
 prescindendo dalle effettive caratteristiche del bene  ablato,  con  il  &#13;
 risultato  di  offendere  sia  la  previsione costituzionale del "serio  &#13;
 ristoro" (art. 42 Cost.), sia lo stesso principio di eguaglianza  (art.  &#13;
 3 Cost.), giacché la lamentata disparità di trattamento conseguirebbe  &#13;
 a valori indennitari che possono essere "i più vari e i meno logici";   &#13;
     2.1  -  nelle  ordinanze  nn.  996,  1058, 1059, 1060 e 1061/84, il  &#13;
 giudice a quo,  solleva,  in  via  subordinata  all'accoglimento  della  &#13;
 principale,  una  seconda  questione  di  legittimità  costituzionale,  &#13;
 denunciando, sempre in riferimento agli artt. 3 e 42 Cost., l'art.  24,  &#13;
 primo comma, della medesima legge provinciale n. 15 del 1972, il quale,  &#13;
 nell'ambito  della  normativa concernente le aree di edilizia agevolata  &#13;
 nelle zone di espansione, disciplina l'acquisizione di  dette  aree  da  &#13;
 parte del comune;                                                        &#13;
     2.2  - la Corte rimettente rileva che, se il valore urbanistico dei  &#13;
 beni ablati tornasse ad avere rilevanza ai  fini  della  determinazione  &#13;
 dell'indennità,  non  potrebbe  non tenersi conto del fatto che, nelle  &#13;
 zone di espansione, il terreno della comunione coatta, che residua allo  &#13;
 stralcio di quello destinato all'edilizia  abitativa  agevolata  ed  è  &#13;
 restituito ai proprietari, viene perciò stesso ad acquistare un pregio  &#13;
 più   elevato,   in   quanto   affrancato  dal  rischio  di  ulteriori  &#13;
 espropriazioni e destinato all'edilizia residenziale  privata:  con  la  &#13;
 conseguente  illegittimità costituzionale della norma censurata, nella  &#13;
 parte in cui non detrae dall'indennizzo,  a  titolo  di  compensazione,  &#13;
 l'ingiustificata locupletazione conseguita dall'espropriato mediante la  &#13;
 restituzione del suolo non utilizzato per fini pubblici;                 &#13;
     3.  -  nei giudizi instaurati con le ordinanze in esame non si sono  &#13;
 costituite  parti  private,  né  è  intervenuto  il  Presidente   del  &#13;
 Consiglio dei ministri;                                                  &#13;
     4.  -  i  giudizi possono, data l'identità delle questioni, essere  &#13;
 riuniti e congiuntamente decisi.                                         &#13;
     Considerato che le medesime questioni sono  state  già  esaminate,  &#13;
 sotto gli stessi profili, dalla Corte costituzionale.  Questo Collegio,  &#13;
 con sentenza n. 231, del 1984, ha infatti dichiarato:                    &#13;
     a)   l'illegittimità  costituzionale  -  limitatamente  al  regime  &#13;
 dell'indennità di esproprio previsto per le aree comprese  nel  centro  &#13;
 edificato   o   altrimenti   provviste,  in  relazione  alle  oggettive  &#13;
 caratteristiche  del  bene  ablato,  dell'attitudine   edificatoria   -  &#13;
 dell'art.  12, primo comma, della legge della Provincia di Bolzano n.15  &#13;
 del 1972 e successive modificazioni, al quale  andava  circoscritta  la  &#13;
 questione  sollevata  -  in  quel giudizio, come nelle ordinanze ora in  &#13;
 esame - dalla stessa Corte d'Appello di Trento, anche nei confronti del  &#13;
 terzo comma dell'art. 12;                                                &#13;
     b) l'inammissibilità della questione relativa all'art.  24,  primo  &#13;
 comma,  della  stessa  legge  provinciale,  in  quanto  le  conseguenze  &#13;
 ipotizzate dal  giudice  a  quo  non  si  verificherebbero,  come  egli  &#13;
 ritiene,  necessariamente,  bensì  solo  se e in quanto il legislatore  &#13;
 altoatesino,  nel  ridefinire,   in   conseguenza   dell'illegittimità  &#13;
 costituzionale  allora pronunziata, il regime indennitario, non tenesse  &#13;
 conto del trattamento di favore che nelle ordinanze  di  remissione  si  &#13;
 assumeva  riservato  ai terreni e ai soggetti espropriati nelle zone di  &#13;
 espansione;                                                              &#13;
     che non vi sono  motivi  per  discostarsi  da  tali  decisioni:  il  &#13;
 giudice  a  quo  prospetta  le questioni con argomentazioni identiche a  &#13;
 quelle contenute nelle ordinanze di cui ai giudizi decisi con la citata  &#13;
 sentenza n. 231/84.                                                      &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e 9, secondo comma, delle Norme integrative per i giudizi dinanzi  alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a)   dichiara   la   manifesta   infondatezza  della  questione  di  &#13;
 legittimità costituzionale dell'art.  12,  primo  comma,  della  legge  &#13;
 della  Provincia  di  Bolzano  20 agosto 1972, n. 15 ("Legge di riforma  &#13;
 dell'edilizia abitativa") e  successive  modificazioni,  sollevata,  in  &#13;
 riferimento  agli  artt.  3 e 42 Cost., dalla Corte d'Appello di Trento  &#13;
 con tutte le ordinanze indicate in epigrafe;                             &#13;
     b)  dichiara  la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità  costituzionale  dell'art.  24,  primo  comma, della legge  &#13;
 della Provincia di Bolzano 20 agosto 1972,  n.    15,  sollevata  dalla  &#13;
 Corte di Appello di Trento, in riferimento agli artt. 3 e 42 Cost., con  &#13;
 le ordinanze nn. 996, 1058, 1059, 1060 e 1061 del 1984.                  &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 novembre 1985.       &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   RENATO DELL'ANDRO.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
