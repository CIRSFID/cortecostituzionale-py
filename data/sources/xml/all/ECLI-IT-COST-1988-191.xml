<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>191</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:191</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1988</data_decisione>
    <data_deposito>18/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della delibera regionale,    &#13;
 riapprovata  il  12  marzo  1987  dal  Consiglio  regionale  con   la    &#13;
 maggioranza  assoluta  dei  componenti e comunicata al Rappresentante    &#13;
 del Governo il 14 marzo 1987, recante "Istituzione della  Commissione    &#13;
 speciale  di  indagine sulla ripresa della criminalità in Sardegna",    &#13;
 promosso con ricorso  del  Presidente  del  Consiglio  dei  ministri,    &#13;
 notificato  il  30  marzo 1987, depositato in cancelleria il 3 aprile    &#13;
 successivo ed iscritto al n. 7 del registro ricorsi 1987;                &#13;
    Visto l'atto di costituzione della Regione Sardegna;                  &#13;
    Udito  nell'udienza  pubblica  del  12  gennaio  1988  il  Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Uditi  l'Avvocato  dello Stato Franco Favara, per il ricorrente, e    &#13;
 l'avvocato Sergio Panunzio per la Regione;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ricorso notificato il 30 marzo 1987, il Presidente del    &#13;
 Consiglio  dei  ministri  ha  sollevato  questione  di   legittimità    &#13;
 costituzionale  della  legge  della  Regione  Sardegna approvata il 3    &#13;
 febbraio 1987, riapprovata il 12 marzo 1987 e comunicata il 14  marzo    &#13;
 successivo,  concernente  "Istituzione  della Commissione speciale di    &#13;
 indagine sulla ripresa della criminalità in Sardegna".                  &#13;
    Premette  il  ricorrente  che  la  legge attribuisce (art. 2) alla    &#13;
 Commissione speciale il  compito  di  verificare  l'attuazione  delle    &#13;
 conclusioni   espresse  dalla  Commissione  parlamentare  d'inchiesta    &#13;
 istituita con legge 27 ottobre 1969, n.  755,  nonché  lo  stato  di    &#13;
 attuazione   degli   interventi   proposti   da   detta   Commissione    &#13;
 parlamentare, e di formulare proposte di intervento per  prevenire  e    &#13;
 reprimere  manifestazioni  di  criminalità. L'art. 3 attribuisce poi    &#13;
 alla Commissione  consiliare  talune  facoltà  nei  confronti  della    &#13;
 giunta   regionale   e   il   potere   di  predisporre  audizioni  di    &#13;
 amministratori di enti locali, di operatori pubblici e privati  e  di    &#13;
 altre persone che riterrà opportune sentire.                            &#13;
    Il  ricorrente  deduce che il potere di inchiesta non si configura    &#13;
 come funzione autonoma,  bensì  strumentale  rispetto  all'esercizio    &#13;
 delle  funzioni  istituzionalmente  spettanti al Consiglio regionale:    &#13;
 pertanto, l'inchiesta può essere disposta soltanto in quelle materie    &#13;
 per  le  quali  l'organo che la dispone è attributario di specifiche    &#13;
 competenze e potestà decisionali.                                       &#13;
    Nella   fattispecie,   l'inchiesta  riguarda  argomenti  -  ordine    &#13;
 pubblico, criminalità, operato degli organi chiamati eventualmente a    &#13;
 dar  seguito alle conclusioni dell'inchiesta parlamentare chiaramente    &#13;
 esulanti dalle competenze regionali, ai sensi degli artt.  3, 4  e  5    &#13;
 dello Statuto speciale.                                                  &#13;
    2.  -  La Regione Sardegna, costituitasi oltre il termine indicato    &#13;
 nell'art. 23 delle Norme integrative per i giudizi davanti alla Corte    &#13;
 costituzionale, conclude per l'infondatezza della questione.             &#13;
    3.  -  Nel Bollettino Ufficiale della Regione Sardegna n. 32 del 4    &#13;
 agosto 1987 è stata pubblicata la legge regionale 27 luglio 1987, n.    &#13;
 33  (concernente  "Istituzione  di  una  commissione speciale per una    &#13;
 indagine conoscitiva sulla condizione economica e sociale delle  zone    &#13;
 della  Sardegna interessate da particolari fenomeni di criminalità e    &#13;
 di violenza"), la quale, ai sensi  dell'art.  5,  sostituisce  quella    &#13;
 impugnata.<diritto>Considerato in diritto</diritto>Come detto in narrativa, la legge della Regione Sardegna 27 luglio    &#13;
 1987, n. 33, recante "Istituzione di una commissione speciale per una    &#13;
 indagine  conoscitiva sulla condizione economica e sociale delle zone    &#13;
 della Sardegna interessate da particolari fenomeni di criminalità  e    &#13;
 di violenza", pubblicata nel Bollettino Ufficiale della regione n. 32    &#13;
 del 4 agosto 1987, ha sostituito la legge impugnata (art. 5).            &#13;
    Ne  deriva  che  deve  essere  dichiarata  cessata  la materia del    &#13;
 contendere del presente giudizio (da ult., sent. n. 434 del 1987).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  cessata  la  materia  del contendere in ordine al ricorso    &#13;
 indicato in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte Costituzionale,    &#13;
 palazzo della Consulta il 10 febbraio 1988.                              &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 febbraio 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
