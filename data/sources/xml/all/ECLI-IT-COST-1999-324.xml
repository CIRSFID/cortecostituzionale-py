<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>324</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:324</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1999</data_decisione>
    <data_deposito>16/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art.  13,  comma  5,    &#13;
 lettera  c),  della  legge  23  dicembre  1994,  n.  724  (Misure  di    &#13;
 razionalizzazione della finanza pubblica), e dell'art. 1,  comma  31,    &#13;
 primo periodo, della legge 8 agosto 1995, n. 335 (Riforma del sistema    &#13;
 pensionistico  obbligatorio  e complementare), promosso con ordinanza    &#13;
 emessa  l'11  febbraio   1998   dalla   Corte   dei   conti   sezione    &#13;
 giurisdizionale  per  la  Regione  Veneto,  sul  ricorso  proposto da    &#13;
 Trevisan Lucia  contro  il  Provveditorato  agli  studi  di  Venezia,    &#13;
 iscritta  al  n.  327  del registro ordinanze 1998 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  19,  prima  serie  speciale,    &#13;
 dell'anno 1998;                                                          &#13;
   Visto l'atto di costituzione di Trevisan Lucia;                        &#13;
   Udito  nella  camera  di  consiglio  del  12 maggio 1999 il giudice    &#13;
 relatore Francesco Guizzi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - La Corte dei conti, sezione  giurisdizionale  per  la  regione    &#13;
 Veneto,  ha  sollevato, in riferimento agli articoli 3, 36 e 38 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art.  13,    &#13;
 comma 5, lettera c), della legge 23 dicembre 1994, n. 724 (Misure  di    &#13;
 razionalizzazione   della  finanza  pubblica),  nella  parte  in  cui    &#13;
 differisce al 1 gennaio 1997 la corresponsione della pensione per  il    &#13;
 personale  collocato  a  riposo per dimissioni, nonché dell'art.  1,    &#13;
 comma 31, primo periodo, della legge 8 agosto 1995, n.  335  (Riforma    &#13;
 del  sistema pensionistico obbligatorio e complementare), nella parte    &#13;
 in cui fa salva l'efficacia del citato art. 13, comma 5, lettera c).     &#13;
   Secondo il Collegio  rimettente,  le  due  disposizioni  menzionate    &#13;
 risulterebbero  illegittime  alla  luce  dei principi enunciati dalla    &#13;
 giurisprudenza di questa Corte; in  particolare,  nella  sentenza  n.    &#13;
 439  del  1994  è stata censurata una norma (introdotta dal d.-l. 19    &#13;
 settembre 1992, n. 384, convertito, con modificazioni, nella legge 14    &#13;
 novembre  1992,  n.  438)  che  differiva  al  1  gennaio   1994   la    &#13;
 corresponsione  della  pensione  per  il  personale  della scuola, in    &#13;
 violazione dell'art.  3 della Costituzione,  perché  il  legislatore    &#13;
 non  avrebbe  considerato  la specifica posizione del personale della    &#13;
 scuola, determinando un'irrazionalità nella legislazione di settore.    &#13;
   Anche la disposizione  citata  recherebbe  lesione  all'art.  3,  e    &#13;
 sarebbe  in  contrasto,  altresì,  con  gli  artt.  36  e  38  della    &#13;
 Costituzione, perché i dipendenti della scuola che hanno  presentato    &#13;
 le  dimissioni sono privati dello stipendio e della pensione, l'uno e    &#13;
 l'altra mezzi indispensabili - conclude la  Corte  dei  conti  -  per    &#13;
 provvedere ai bisogni essenziali della vita.                             &#13;
   2.  -  Ha  presentato tardivamente memoria di costituzione la parte    &#13;
 privata.<diritto>Considerato in diritto</diritto>1. - La Corte dei conti, sezione  giurisdizionale  per  la  Regione    &#13;
 Veneto,  ha  sollevato, in riferimento agli articoli 3, 36 e 38 della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art.  13,    &#13;
 comma 5, lettera c), della legge 23 dicembre 1994, n. 724 (Misure  di    &#13;
 razionalizzazione   della  finanza  pubblica),  nella  parte  in  cui    &#13;
 differisce al 1 gennaio 1997 la corresponsione della pensione per  il    &#13;
 personale  collocato  a  riposo per dimissioni, nonché dell'art.  1,    &#13;
 comma 31, primo periodo, della legge 8 agosto 1995, n.  335  (Riforma    &#13;
 del  sistema pensionistico obbligatorio e complementare), nella parte    &#13;
 in cui fa salva l'efficacia del citato art. 13, comma 5, lettera c).     &#13;
   Il Collegio rimettente  invoca  quale  precedente  la  sentenza  di    &#13;
 questa  Corte  n.  439  del  1994, che ha dichiarato l'illegittimità    &#13;
 costituzionale della  norma  di  "blocco"  introdotta  dal  d.-l.  19    &#13;
 settembre  1992, n.   384, convertito, con modificazioni, nella legge    &#13;
 14 novembre 1992, n. 438. Pure in questo caso vi  sarebbe  violazione    &#13;
 dell'art.  3  della  Costituzione, perché il legislatore non avrebbe    &#13;
 considerato  la specifica posizione del personale della scuola, dando    &#13;
 luogo a un'incongruenza all'interno della  legislazione  di  settore.    &#13;
 L'irrazionalità  già  censurata da questa Corte sarebbe ancora più    &#13;
 evidente, nella fattispecie ora in esame, sol  che  si  consideri  il    &#13;
 lungo  spazio  di  tempo  (sedici mesi) che intercorre fra la data di    &#13;
 cessazione dal servizio e quella  della  decorrenza  del  trattamento    &#13;
 pensionistico  per  il  personale  di  cui  alla  lettera  c),  prima    &#13;
 menzionata.                                                              &#13;
   Insieme con l'art. 3, il Collegio rimettente invoca gli articoli 36    &#13;
 e 38 della Costituzione, dal momento che il personale della scuola è    &#13;
 privato, per sedici mesi, dello stipendio e della pensione.              &#13;
   2. - L'ordinanza di rimessione muove dunque dall'assunto che vi sia    &#13;
 stretta analogia fra le norme di "blocco" censurate da questa Corte e    &#13;
 la lettera c) del citato art. 13, sì che varrebbe la medesima  ratio    &#13;
 decidendi    della sentenza n. 439 del 1994, alla quale è seguita la    &#13;
 sentenza n. 347 del 1997, che riguardava l'art. 13, comma 5,  lettera    &#13;
 b), della legge n. 724 del 1994.                                         &#13;
   Ma siffatta impostazione, a ben vedere, non può essere condivisa.     &#13;
   Le    due    pronunce    richiamate,    entrambe   d'illegittimità    &#13;
 costituzionale, ricordano che per il comparto della scuola vi  è  un    &#13;
 regime  specifico per l'accettazione delle dimissioni volontarie e il    &#13;
 collocamento a riposo, che decorre dal  1  settembre  di  ogni  anno.    &#13;
 Peculiarità che nelle due norme censurate non era stata valutata; di    &#13;
 modo  che  l'applicazione  del "blocco" - previsto per la generalità    &#13;
 dei dipendenti pubblici - determinava, a danno  del  personale  della    &#13;
 scuola,  un  vuoto  di  quattro  mesi  fra  cessazione dal servizio e    &#13;
 corresponsione del trattamento  pensionistico.                           &#13;
   La disposizione denunciata, malgrado  l'apparente  similitudine  di    &#13;
 formulazione rispetto alla lettera b) dell'art. 13, riguarda invero i    &#13;
 dipendenti  che non hanno ancora maturato un'anzianità contributiva,    &#13;
 o di servizio, pari a 31 anni, e pone nei loro confronti  una  regola    &#13;
 "dissuasiva"  di  una  certa severità: il trattamento pensionistico,    &#13;
 infatti, potrà essere conseguito solo a partire dal 1 gennaio  1997.    &#13;
 Questa  misura  trova  evidente  giustificazione  nella  non  elevata    &#13;
 anzianità raggiunta da tali dipendenti, come risulta  dal  confronto    &#13;
 fra  le lettere a), b) e c) dell'art. 13, comma 5, della legge n. 724    &#13;
 del 1994: la lettera a) pone la decorrenza del 1 luglio 1995 per  chi    &#13;
 abbia un'anzianità non inferiore a 37 anni; la lettera b) riguarda i    &#13;
 dipendenti  con  anzianità non inferiore a 31 anni (per costoro vale    &#13;
 il termine del 1 gennaio 1996) e infine con la  lettera  c),  qui  in    &#13;
 esame,  il  dato  obiettivo della ridotta anzianità (inferiore ai 31    &#13;
 anni)  porta  a  un  più  incisivo  differimento   del   trattamento    &#13;
 pensionistico,  temperato  dalla  disposizione  di  cui al successivo    &#13;
 comma 8, che  ammette  la  revoca  delle  domande  di  pensionamento,    &#13;
 ancorché accettate dagli enti di appartenenza.                          &#13;
   3.   -  Non  bisogna  dimenticare  che  queste  misure,  di  natura    &#13;
 eccezionale, sono valse a fronteggiare  una  situazione  economica  e    &#13;
 finanziaria  di  notevole  gravità  e  si  inseriscono  in un quadro    &#13;
 composito di provvedimenti tesi a contenere  la  spesa  pubblica  nel    &#13;
 settore  della previdenza.   Tale finalità di risanamento ha imposto    &#13;
 l'adozione di norme restrittive che hanno  inciso  sulle  aspettative    &#13;
 che erano maturate con riferimento alla legislazione  previgente.        &#13;
   Nel  caso  in  esame,  l'obiettivo  di riequilibrio del bilancio si    &#13;
 coniuga con il chiaro disfavore per il  pensionamento  volontario  di    &#13;
 dipendenti   ancora   lontani   da   una   significativa   anzianità    &#13;
 contributiva;  né  si  può  invocare  a  sostegno  del  dubbio   di    &#13;
 legittimità costituzionale la necessità di garantire la continuità    &#13;
 delle   prestazioni  durante  l'anno  scolastico,  perché  la  norma    &#13;
 denunciata opera sulla decorrenza del trattamento  pensionistico,  al    &#13;
 fine  di  scoraggiare  l'esodo dei più giovani, e non sull'efficacia    &#13;
 delle dimissioni nell'ambito dell'anno scolastico.                       &#13;
   Non vale dunque  il  richiamo  alle  peculiarità  dell'ordinamento    &#13;
 scolastico,  nei  termini  suggeriti  dal  rimettente,  e  tanto meno    &#13;
 convince il riferimento agli articoli 36  e  38  della  Costituzione,    &#13;
 giacché l'effetto economico negativo subito dagli interessati deriva    &#13;
 da  un  loro  atto  volontario, che poteva essere revocato su istanza    &#13;
 degli stessi dipendenti, avendo costoro la facoltà  di  ritirare  la    &#13;
 domanda   di   pensionamento,  ancorché  accettata,  secondo  quanto    &#13;
 disposto dall'art.  13, comma 8, della citata legge n. 724.              &#13;
   La questione deve essere quindi dichiarata non fondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  13,  comma 5, lettera c), della legge 23 dicembre 1994, n.    &#13;
 724 (Misure di razionalizzazione  della  finanza  pubblica),  nonché    &#13;
 dell'art.  1,  comma 31, primo periodo, della legge 8 agosto 1995, n.    &#13;
 335 (Riforma del sistema pensionistico obbligatorio e complementare),    &#13;
 sollevata,  in  riferimento  agli  articoli  3,   36   e   38   della    &#13;
 Costituzione,  dalla  Corte dei conti, sezione giurisdizionale per la    &#13;
 regione Veneto, con l'ordinanza in epigrafe.                             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 7 luglio 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Guizzi                             &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 16 luglio 1999.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
