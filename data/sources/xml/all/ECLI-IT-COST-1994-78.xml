<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>78</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:78</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/02/1994</data_decisione>
    <data_deposito>10/03/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 39 del d.P.R.    &#13;
 11 luglio 1980, n. 382 (Riordinamento  della  docenza  universitaria,    &#13;
 relativa fascia di formazione nonché sperimentazione organizzativa e    &#13;
 didattica),  promosso  con  ordinanza  emessa  il  19  marzo 1992 dal    &#13;
 Tribunale amministrativo regionale della Liguria sul ricorso proposto    &#13;
 da Acaccia Gabriella ed altri contro il Ministero dell'Università  e    &#13;
 Ricerca  Scientifica  e  Tecnologica  ed  Università  degli studi di    &#13;
 Genova, iscritta al n. 393 del registro ordinanze 1993  e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  29,  prima  serie    &#13;
 speciale, dell'anno 1993;                                                &#13;
    Visto l'atto di costituzione di Acaccia Gabriella ed altri;           &#13;
    Udito  nell'udienza  pubblica  dell'11  gennaio  1994  il  Giudice    &#13;
 relatore Francesco Guizzi;                                               &#13;
    Udito l'avv. Carlo Raggi per Acaccia Gabriella ed altri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Tribunale amministrativo regionale della Liguria, prima    &#13;
 sezione, ha sollevato,  in  riferimento  agli  artt.  3  e  38  della    &#13;
 Costituzione,  questione  di legittimità costituzionale dell'art. 39    &#13;
 del d.P.R. 11  luglio  1980,  n.  382  (Riordinamento  della  docenza    &#13;
 universitaria,  relativa fascia di formazione nonché sperimentazione    &#13;
 organizzativa   e   didattica),  nella  parte  in  cui  dichiara  non    &#13;
 pensionabile  l'assegno   aggiuntivo   riconosciuto   ai   professori    &#13;
 universitari che hanno optato per il regime di impegno a tempo pieno.    &#13;
    Il  giudice a quo osserva che l'emolumento, istituito dall'art. 39    &#13;
 del citato d.P.R. n. 382, ha subito una radicale  modifica  ad  opera    &#13;
 del   decreto-legge   11   gennaio   1985,   n.  2,  convertito,  con    &#13;
 modificazioni, nella legge 8 marzo 1985, n. 72. L'assegno  in  parola    &#13;
 ha  le  caratteristiche  di un emolumento fisso e continuativo, ed è    &#13;
 parte  significativa  della  retribuzione  complessiva  dei   docenti    &#13;
 universitari  che  hanno  optato  per il regime a tempo pieno; non è    &#13;
 stata però  modificata  la  norma,  presente  nel  testo  originario    &#13;
 dell'art.  39,  che  lo  configura  come non pensionabile. Al caso in    &#13;
 esame si dovrebbe applicare il principio enunciato  da  questa  Corte    &#13;
 nella  sentenza  n.  126  del 1981, che ha dichiarato, per violazione    &#13;
 dell'art. 38 della Costituzione, l'illegittimità delle norme che non    &#13;
 prevedevano la computabilità, a fini previdenziali e  assistenziali,    &#13;
 dell'indennità  attribuita  al  personale  medico  universitario con    &#13;
 funzioni di assistenza sanitaria:   anche  per  l'assegno  aggiuntivo    &#13;
 percepito  dai  professori  universitari  a  tempo  pieno vi sarebbe,    &#13;
 dunque, quel carattere di "corrispettività"  che  è  proprio  degli    &#13;
 elementi  costitutivi  della  retribuzione,  sì che l'esclusione dal    &#13;
 computo della pensione sarebbe in contrasto con la  menzionata  norma    &#13;
 costituzionale.                                                          &#13;
    Il   collegio  rimettente  ricorda,  poi,  che  la  giurisprudenza    &#13;
 amministrativa ha riconosciuto la computabilità  dell'indennità  di    &#13;
 tempo  pieno  prevista per il personale medico degli enti ospedalieri    &#13;
 (e,  successivamente,  delle  unità  sanitarie  locali).  La   norma    &#13;
 denunziata risulterebbe perciò in contrasto anche con l'art. 3 della    &#13;
 Costituzione  per  l'ingiustificata  disparità  di trattamento tra i    &#13;
 professori universitari a tempo pieno e i dipendenti  pubblici  prima    &#13;
 indicati,  ai  quali  è  data  la  possibilità  di optare tra i due    &#13;
 regimi.                                                                  &#13;
    2. - Sono intervenute in  giudizio  le  parti  private,  adducendo    &#13;
 argomenti  in  adesione  a  quelli  dell'ordinanza  di  rimessione  e    &#13;
 sottolineando, in particolare, come  il  rapporto  a  tempo  pieno  -    &#13;
 incompatibile  con  lo  svolgimento  di  attività professionali e di    &#13;
 consulenza  esterna  e  con  l'assunzione   di   qualsiasi   incarico    &#13;
 retribuito, secondo quanto previsto dall'art. 11, quinto comma, lett.    &#13;
 a),  del  d.P.R.  n. 382 del 1980 - comporti una prestazione diversa,    &#13;
 sotto il profilo qualitativo, rispetto al rapporto a tempo  definito;    &#13;
 ciò  per l'impegno esclusivo richiesto ai docenti a tempo pieno, che    &#13;
 giustifica  la  riserva,  a  loro  favore,  di  determinate  funzioni    &#13;
 universitarie (art. 11, quarto comma, lett. a) del d.P.R. n. 382). Si    &#13;
 rileva,  infine,  come  la  sostanziale differenza fra le prestazioni    &#13;
 connesse ai due regimi di servizio  sia  stata  già  considerata  da    &#13;
 questa Corte nella sent. n. 1019 del 1988.                               &#13;
    3.  -  In  una  memoria depositata nell'imminenza dell'udienza, le    &#13;
 parti  private  hanno  ribadito  il  carattere  di   corrispettività    &#13;
 dell'assegno   aggiuntivo   (che   avrebbe   una   funzione   analoga    &#13;
 all'indennità  di  tempo  pieno  attribuita  al   personale   medico    &#13;
 ospedaliero), richiamando altresì la sentenza di questa Corte n. 302    &#13;
 del  1983,  che  ha  dichiarato,  per  violazione  dell'art. 36 della    &#13;
 Costituzione,   l'illegittimitàcostituzionale   del   primo    comma    &#13;
 dell'art.  18  della  legge  5  dicembre  1959,  n.  1077,  in quanto    &#13;
 consentiva   alla   C.p.d.e.l.  di  valutare  solo  in  parte,  nella    &#13;
 determinazione  della  base  pensionabile,  l'"indennità  di   toga"    &#13;
 corrisposta agli avvocati del comune.<diritto>Considerato in diritto</diritto>1.  -  Il  Tribunale amministrativo regionale della Liguria, prima    &#13;
 sezione, dubita, in riferimento agli artt. 3 e 38 della Costituzione,    &#13;
 della legittimità costituzionale dell'art. 39 del d.P.R.  11  luglio    &#13;
 1980,  n. 382, nella parte in cui dichiara non pensionabile l'assegno    &#13;
 aggiuntivo riconosciuto ai professori  universitari  a  tempo  pieno:    &#13;
 costoro  sarebbero  discriminati rispetto a pubblici dipendenti per i    &#13;
 quali  analoga  indennità  è  pensionabile;  l'assegno   aggiuntivo    &#13;
 previsto  dall'art.  39 del d.P.R. n. 382 del 1980 è, infatti, parte    &#13;
 del complessivo trattamento economico dei professori universitari che    &#13;
 optano per il tempo pieno e come tale - conclude il rimettente - deve    &#13;
 essere valutato ai fini del trattamento  di  quiescenza,  secondo  il    &#13;
 principio  che  questa Corte avrebbe enunciato nella sent. n. 126 del    &#13;
 1981, desumendolo dall'art. 38 della Costituzione.                       &#13;
    2. - Conviene, preliminarmente, ricostruire la storia dell'assegno    &#13;
 aggiuntivo in esame.                                                     &#13;
    Il decreto-legge 1  ottobre  1973,  n.  580  (Misure  urgenti  per    &#13;
 l'università),   convertito,   con  modificazioni,  nella  legge  30    &#13;
 novembre 1973,  n.  766,  all'art.  12  disciplinava  il  trattamento    &#13;
 economico  del  personale  docente universitario, al quale attribuiva    &#13;
 due assegni.                                                             &#13;
    Il primo, regolato dai primi tre commi dell'art. 12, consisteva in    &#13;
 un "assegno annuo pensionabile"  corrisposto  a  tutto  il  personale    &#13;
 insegnante   delle   università,   compreso  quello  fuori  ruolo  e    &#13;
 incaricato, che sostituiva l'"indennità di ricerca scientifica" già    &#13;
 prevista  dall'art.  22  della  legge  26  gennaio  1962,  n.  16,  e    &#13;
 successive  modificazioni.  Il  secondo  - regolato dai commi quarto,    &#13;
 quinto, sesto e settimo  del  medesimo  art.  12  -  veniva  definito    &#13;
 "assegno  speciale": non era pensionabile; non spettava ai professori    &#13;
 che   intendevano    svolgere    privatamente    "libera    attività    &#13;
 professionale";  ed  era,  dunque,  un incentivo ante litteram per il    &#13;
 "tempo pieno".                                                           &#13;
    Il riordinamento della docenza universitaria operato dal d.P.R. n.    &#13;
 382,  nell'istituire  le  due  fasce  dei  professori,   ordinari   e    &#13;
 associati,  ha  previsto  per  entrambi un incremento del trattamento    &#13;
 economico, pari al 40 per cento, nel caso di opzione per il regime di    &#13;
 impegno a tempo pieno (secondo quanto disposto  dall'art.  36,  sesto    &#13;
 comma,  del  d.P.R. n. 382, e già dall'art. 4, lett. c), della legge    &#13;
 delega  21  febbraio  1980,  n.  28).  Tale  maggiore  emolumento  è    &#13;
 pensionabile  nelle  modalità di cui all'art. 40 dello stesso d.P.R.    &#13;
 n. 382.                                                                  &#13;
    L'art. 39 più volte richiamato,  nell'innovare  quanto  stabilito    &#13;
 dai  commi  quarto,  quinto, sesto, settimo e ottavo dell'art. 12 del    &#13;
 decreto-legge n. 580 del 1973, sostituiva  l'"assegno  speciale"  non    &#13;
 pensionabile,  di  cui  si  è detto, con l'"assegno aggiuntivo", che    &#13;
 rappresenta un'ulteriore incentivazione dell'opzione per il regime di    &#13;
 tempo pieno. Nella stesura originaria dell'art. 39 del d.P.R. n. 382,    &#13;
 l'assegno aggiuntivo spettava anche ai professori a  tempo  definito,    &#13;
 ma  ridotto  del  50  per  cento,  e  se  ne  prevedeva  comunque  il    &#13;
 riassorbimento, con  i  futuri  miglioramenti  economici,  in  misura    &#13;
 differenziata  per  i  due regimi (ultimo capoverso dell'art. 39, nel    &#13;
 testo originario).                                                       &#13;
    3.  -  Il  decreto-legge n. 2 del 1985 ha abrogato il terzultimo e    &#13;
 l'ultimo capoverso dell'art. 39 del d.P.R.  n.  382:  l'ordinanza  di    &#13;
 rimessione  e  le parti private insistono quindi sull'abrogazione del    &#13;
 penultimo capoverso, che prevedeva la corresponsione  dell'  assegno,    &#13;
 sia  pur  ridotto  del  50  per  cento,  anche  ai professori a tempo    &#13;
 definito; e il fatto che esso spetti, ora, soltanto ai  professori  a    &#13;
 tempo  pieno  dimostrerebbe  che  l'assegno  assolve  la funzione - e    &#13;
 rivela la natura - di componente  essenziale  della  retribuzione  in    &#13;
 forme  del tutto analoghe, si potrebbe aggiungere, alla maggiorazione    &#13;
 stipendiale del 40 per cento, pensionabile, ai sensi dell'art. 36 del    &#13;
 d.P.R. n. 382. Di  qui,  la  denunciata  illegittimità  della  norma    &#13;
 ancora presente nell'art. 39 del d.P.R. n. 382.                          &#13;
    Questa Corte ha tuttavia chiarito che non basta addurre la "natura    &#13;
 retributiva"  e,  più  esattamente,  il carattere di "componente del    &#13;
 normale trattamento economico" di un'indennità  per  stabilirne,  in    &#13;
 via  di principio, la pensionabilità: occorre infatti provare che la    &#13;
 non  computabilità  dell'indennità  ai   fini   pensionistici   sia    &#13;
 "manifestamente  incongrua  o irragionevole" (sent. n. 119 del 1991).    &#13;
 Il che deve escludersi, in questo caso, alla luce di quanto segue.       &#13;
    4.  -  Già  l'assegno  speciale   previsto   dall'art.   12   del    &#13;
 decreto-legge  n.  580  del 1973 non era pensionabile; ed è un dato,    &#13;
 questo,  che  dimostra  come  il  legislatore,  confermando  siffatta    &#13;
 caratteristica  con riguardo al nuovo "assegno aggiuntivo", non abbia    &#13;
 agito arbitrariamente. Né vale  obiettare  che  è,  così,  offerta    &#13;
 scarsa  tutela  all'opzione  per il tempo pieno in contraddizione con    &#13;
 l'esigenza, apprezzata da questa Corte, di "privilegiare sempre  più    &#13;
 tale  scelta"  (sent.  n.  1019 del 1988): una differenziazione assai    &#13;
 rilevante fra i professori a tempo pieno e quelli a tempo definito è    &#13;
 invero assicurata, anche ai fini pensionistici, dal d.P.R. n. 382,  e    &#13;
 in  particolare  dal  combinato disposto dell'art. 36, sesto comma, e    &#13;
 dell'art.  40,  giacché  nel  trattamento  di  quiescenza  è   già    &#13;
 computata la maggiorazione del 40 per cento.                             &#13;
    Non sussiste, dunque, il vulnus all'art. 38 della Costituzione nei    &#13;
 termini  prospettati  dal  giudice  a  quo,  né  si palesa motivo di    &#13;
 irrazionalità   conseguente   alle   modificazioni   apportate   dal    &#13;
 decreto-legge  n.  2  del 1985, come convertito nella legge n. 72 del    &#13;
 1985, dal  momento  che  il  risultato  finale  di  detta  disciplina    &#13;
 fornisce,  pure  ai  fini  del  trattamento di quiescenza, una tutela    &#13;
 sufficiente - anche se perfettibile - dell'opzione per il  regime  di    &#13;
 impegno  a tempo pieno:  l'art. 38 della Costituzione non garantisce,    &#13;
 d'altronde, una integrale corrispondenza tra retribuzione e  pensione    &#13;
 (v.,  nella  giurisprudenza più recente di questa Corte, la sent. n.    &#13;
 449 del 1993).                                                           &#13;
    5. - Neppure ha pregio la censura mossa con riguardo alla presunta    &#13;
 disparità di trattamento che si sarebbe determinata a seguito di  un    &#13;
 indirizzo  giurisprudenziale che, in assenza di contraria statuizione    &#13;
 legislativa,  ha  disposto  il  computo,  ai  fini   di   quiescenza,    &#13;
 dell'indennità  del  personale sanitario ospedaliero. Tale indirizzo    &#13;
 non esprime un  principio,  generale,  di  necessario  computo  delle    &#13;
 indennità  ai fini di quiescenza, che abbia fondamento in specifiche    &#13;
 disposizioni e, comunque, in valori tutelati dalla Costituzione.         &#13;
    Anche  a  voler  superare, in ipotesi, le obiezioni sull'idoneità    &#13;
 del  termine  di  raffronto  evocato,  va  infine   considerato   che    &#13;
 l'intervento  della  Corte, richiesto dal giudice a quo, risulterebbe    &#13;
 invasivo della sfera riservata  alle  valutazioni  discrezionali  del    &#13;
 legislatore,  al  quale spetta, eventualmente, una nuova ponderazione    &#13;
 della materia.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  39  del d.P.R. 11 luglio 1980, n. 382 (Riordinamento della    &#13;
 docenza  universitaria,  relativa  fascia   di   formazione   nonché    &#13;
 sperimentazione  organizzativa  e  didattica),  nella  parte  in  cui    &#13;
 dichiara  non  pensionabile  l'assegno  aggiuntivo  riconosciuto   ai    &#13;
 professori  universitari  che optano per il regime di impegno a tempo    &#13;
 pieno,  sollevata,  in  riferimento  agli  artt.   3   e   38   della    &#13;
 Costituzione,  dal  Tribunale amministrativo regionale della Liguria,    &#13;
 prima sezione, con l'ordinanza indicata in epigrafe.                     &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 23 febbraio 1994.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 marzo 1994.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
