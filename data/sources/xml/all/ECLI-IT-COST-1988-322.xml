<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>322</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:322</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/03/1988</data_decisione>
    <data_deposito>17/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 80, tredicesimo    &#13;
 comma, del d.P.R. 15 giugno 1959, n. 393 (Codice  della  strada),  in    &#13;
 relazione  all'art.  48 del codice penale e all'art. 26 della legge 6    &#13;
 giugno  1974  n.   298   (Istituzione   dell'albo   nazionale   degli    &#13;
 autotrasportatori   di   cose   per  conto  terzi,  disciplina  degli    &#13;
 atotrasporti di cose  e  istituzione  di  un  sistema  di  tariffe  a    &#13;
 forcella  per i trasporti di merci su strada), promosso con ordinanza    &#13;
 emessa il 2 maggio 1984 dal Pretore di Grumello Del  Monte,  iscritta    &#13;
 al  n.  1291  del registro ordinanze 1984 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 113- bis dell'anno 1985;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 febbraio 1988 il Giudice    &#13;
 relatore Ettore Gallo;                                                   &#13;
   Ritenuto  che  il  Pretore  di  Grumello del Monte, con ordinanza 2    &#13;
 maggio  1984,  sollevava  questione  di  legittimità  costituzionale    &#13;
 dell'art.  80,  co. tredicesimo, d.P.R. 15 giugno 1959 n. 383 (codice    &#13;
 della strada), modificato dall'art. 2 della l. 14  febbraio  1974  n.    &#13;
 62, con riferimento agli artt. 3 e 27 Cost.;                             &#13;
      che nella specie si trattava di conducente di un pulmino adibito    &#13;
 al trasporto di alunni: il quale conducente, pur essendo in  possesso    &#13;
 di  patente  di  guida,  era  però  sprovvisto  del  "certificato di    &#13;
 abilitazione professionale" richiesto dalla legge  per  la  guida  di    &#13;
 tali  veicoli,  in  particolare  per  quelli  adibiti al trasporto di    &#13;
 scolari;                                                                 &#13;
      che  - ad avviso del Pretore - essendo il contenuto della prova,    &#13;
 per il conseguimento del certificato, limitato  all'apprendimento  di    &#13;
 nozioni  che  presuppongono  ed  integrano quelle ben più importanti    &#13;
 acquisite con l'abilitazione alla guida, sarebbe  irrazionale  punire    &#13;
 con  la  stessa pena chi guida senza patente e chi, la patente avendo    &#13;
 conseguita, è soltanto sprovvisto del  certificato  di  abilitazione    &#13;
 professionale;                                                           &#13;
      che  tale  irrazionalità  sarebbe tanto più evidente quando si    &#13;
 consideri che l'art. 348 cod.pen. prevede la pena  alternativa  della    &#13;
 sola  multa  per  chi  esercita  abusivamente una professione, per la    &#13;
 quale è richiesta una speciale abilitazione dello  Stato,  mentre  a    &#13;
 chi  guida  senza  il  certificato  di  abilitazione professionale è    &#13;
 riservata una pena congiunta di arresto ed ammenda;                      &#13;
      che  altrettanto  dovrebbe  dirsi  in relazione all'art. 26 l. 6    &#13;
 giugno 1974 n. 298 che punisce chi esercita l'autotrasporto  di  cose    &#13;
 per  conto  terzi,  senza  essere  iscritto  all'albo  nazionale  dei    &#13;
 trasportatori, con le pene di cui all'art. 348 cod.pen. ;                &#13;
      che,  per  tal  modo,  non  venendo  rispettato  il principio di    &#13;
 proporzionalità fra la pena ed  il  disvalore  dell'illecito,  anche    &#13;
 l'art.  27  Cost.  resterebbe pregiudicato nella finalizzazione della    &#13;
 pena alla risocializzazione del reo;                                     &#13;
      che è intervenuto nel giudizio innanzi alla Corte il Presidente    &#13;
 del Consiglio dei ministri,  rappresentato  dall'Avvocatura  Generale    &#13;
 dello  Stato, la quale ha concluso per la declaratoria d'infondatezza    &#13;
 della sollevata questione;                                               &#13;
    Considerato    innanzitutto   che,   nella   nuova   formulazione,    &#13;
 conseguente alle modifiche apportate dalla l. 14 febbraio 1974 n. 62,    &#13;
 il  comma  impugnato  dell'art.  80  del d.P.R. 15 giugno 1959 n. 383    &#13;
 dev'essere rettificato, non essendo più  il  tredicesimo  ma  bensì    &#13;
 l'undicesimo quello che contiene la disposizione nei cui confronti è    &#13;
 stato sollevato il dubbio d'illegittimità costituzionale;               &#13;
      che  non  sembra  del tutto ininfluente il contenuto della prova    &#13;
 previsto dal D.M. 3 ottobre 1979  ai  fini  della  valutazione  della    &#13;
 razionalità della comminazione della stessa pena per colui che guidi    &#13;
 senza avere sostenuto quella prova e  conseguito  il  certificato  di    &#13;
 abilitazione professionale, e colui che guidi senza patente;             &#13;
      che,  infatti,  a  parte  la  conoscenza  che  si pretende sulla    &#13;
 legislazione nazionale applicabile al trasporto di persone, dove sono    &#13;
 previste  precise prescrizioni di comportamento indispensabili per la    &#13;
 sicurezza dei trasporti, la prova esige  anche  la  conoscenza  della    &#13;
 responsabilità   che   il   conducente   assume  nel  trasporto  dei    &#13;
 viaggiatori;                                                             &#13;
      che  tutte  tali nozioni, unite alle altre elencate dal decreto,    &#13;
 rappresentano requisiti indispensabili ad  una  professionalità  che    &#13;
 pone  nelle  mani del guidatore in una sola volta la vita di numerose    &#13;
 persone trasportate, e particolarmente quella degli  scolari  che,  a    &#13;
 causa  dell'inesperienza  propria della giovanissima età, richiedono    &#13;
 nel   trasportatore   coscienza    scrupolosa    della    particolare    &#13;
 responsabilità che si assume;                                           &#13;
      che,   perciò,   non   può   essere  ritenuta  irrazionale  la    &#13;
 disposizione che sottopone il guidatore sprovvisto del certificato di    &#13;
 abilitazione  professionale  alla  stessa pena comminata per la guida    &#13;
 senza patente: e non senza ragione, del resto, questo certificato  di    &#13;
 abilitazione   professionale,   così   chiamato   per   allineamento    &#13;
 terminologico alla  legislazione  internazionale,  era  definito,  in    &#13;
 realtà,  dalla  interna  normativa  -  come  la  stessa ordinanza di    &#13;
 rimessione ricorda - "patente di guida ad uso pubblico";                 &#13;
      che,  esclusa  l'irrazionalità,  il  resto  è  prerogativa del    &#13;
 legislatore che non può essere in questa sede censurata: e,  d'altra    &#13;
 parte,  non appare convincente il confronto con la mancata iscrizione    &#13;
 all'albo per l'esercizio dell'autotrasporto di cose per conto  terzi,    &#13;
 trattandosi  di  situazione  diversa,  questa sì semmai paragonabile    &#13;
 all'esercizio di professione abusiva per mancata iscrizione  all'albo    &#13;
 professionale e, perciò, punita con la stessa pena;                     &#13;
      che,  in  effetti,  altro è il conseguimento di un'abilitazione    &#13;
 (esame di patente,  esame  di  stato,  esame  abilitativo),  e  altro    &#13;
 l'iscrizione all'albo professionale, giacché quest'ultimo presuppone    &#13;
 l'abilitazione ma con essa non s'identifica;                             &#13;
      che  quanto  sopra  supera  altresì  il riferimento all'art. 27    &#13;
 Cost., anche a prescindere dalla ripetuta  giurisprudenza  di  questa    &#13;
 Corte che limita l'operatività del terzo co. dell'art. 27 Cost. alla    &#13;
 fase esecutiva;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 80, co. 13 (rectius  11)  d.P.R.  15  giugno    &#13;
 1959  n.  383 (codice della strada) così come modificato dall'art. 2    &#13;
 della l. 14 febbraio 1974 n. 62, sollevata dal  Pretore  di  Grumello    &#13;
 del  Monte con ordinanza 2 maggio 1984, in riferimento agli artt. 3 e    &#13;
 27 Cost.                                                                 &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 10 marzo 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 17 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
