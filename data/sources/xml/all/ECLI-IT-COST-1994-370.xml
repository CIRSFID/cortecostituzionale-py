<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>370</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:370</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PESCATORE</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/07/1994</data_decisione>
    <data_deposito>27/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Gabriele PESCATORE; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco &#13;
 GUIZZI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  15,  quarto    &#13;
 comma  septies,  della legge 19 marzo 1990, n. 55 (Nuove disposizioni    &#13;
 per la prevenzione della delinquenza di tipo mafioso e di altre gravi    &#13;
 forme  di  manifestazione  di  pericolosità   sociale),   introdotto    &#13;
 dall'art.  1  della legge 18 gennaio 1992, n. 16 (Norme in materia di    &#13;
 elezioni e nomine presso le Regioni e gli enti locali), promosso  con    &#13;
 ordinanza  emessa  il  12  ottobre  1993 dal Tribunale amministrativo    &#13;
 regionale per la Sicilia - Sezione staccata di Catania - sul  ricorso    &#13;
 proposto  da  Coco  Mario  contro  il  Comando Generale dell'Arma dei    &#13;
 Carabinieri ed altro, iscritta al n. 151 del registro ordinanze  1994    &#13;
 e  pubblicata  nella Gazzetta Ufficiale della Repubblica n. 13, prima    &#13;
 serie speciale, dell'anno 1994.                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  6 luglio 1994 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto che il Tribunale amministrativo regionale per la Sicilia,    &#13;
 con ordinanza del 12 ottobre 1993, ha denunciato, in riferimento agli    &#13;
 artt.  3,  4,  35,  36  e  97  della  Costituzione,  l'illegittimità    &#13;
 dell'art.  15,  quarto  comma  septies, della l. 19 marzo 1990, n. 55    &#13;
 introdotto dall'art. 1 della l. 18 gennaio 1992, n. 16 nella parte in    &#13;
 cui  prevede  la  sospensione  del  pubblico  dipendente  che   abbia    &#13;
 riportato  sentenza  di condanna per i delitti indicati nelle lettere    &#13;
 a), b), c) e d) di cui al precedente  primo  comma,  ovvero  nei  cui    &#13;
 confronti sussistano le condizioni di cui alle lettere e) ed f) dello    &#13;
 stesso primo comma;                                                      &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata infondata;              &#13;
    Considerato  che  la questione - sollevata negli stessi termini in    &#13;
 riferimento agli artt. 3,  primo  comma  e  97,  primo  comma,  della    &#13;
 Costituzione dal Tribunale amministrativo regionale per il Piemonte -    &#13;
 è  stata  dichiarata non fondata da questa Corte con sentenza n. 184    &#13;
 del 1994;                                                                &#13;
      che secondo tale sentenza la ratio della  l.  n.  16  del  1992,    &#13;
 consiste  nell'"esigenza di rafforzare la disciplina già posta dalla    &#13;
 l. n. 55 del 1990,  estendendone  talune  qualificanti  previsioni  -    &#13;
 inizialmente  riferite  ai  soggetti  legati alla P.A. da rapporto di    &#13;
 servizio onorario, elettivo o non - a pubblici dipendenti legati alla    &#13;
 stessa da  rapporto  di  servizio,  che  possono  talora  versare  in    &#13;
 condizione  di  potenziale  maggiore  pericolosità e, quindi, essere    &#13;
 fonte di possibili maggiori danni";                                      &#13;
      che, pertanto, "il trattamento omogeneo delle due  categorie  è    &#13;
 stato   determinato   razionalmente  dalla  legge,  identici  essendo    &#13;
 finalità e mezzi di tutela rispetto alla pericolosità eventuale  di    &#13;
 comportamenti decisionali ed operativi potenzialmente pregiudizievoli    &#13;
 per la pubblica amministrazione";                                        &#13;
      che  alla  luce  di  questa  prospettiva,  "la  diversità delle    &#13;
 posizioni e delle funzioni non comporta  diversità  di  disciplina",    &#13;
 essendo   quest'ultima   volta   "alla   salvaguardia   di  interessi    &#13;
 fondamentali dello Stato";                                               &#13;
      che, avuto  riguardo  alle  considerazioni  suesposte  risultano    &#13;
 assorbite  le  censure sollevate in riferimento agli artt. 4, 35 e 36    &#13;
 della Costituzione le quali sono prive di autonomo svolgimento;          &#13;
      che, pertanto, nell'ordinanza di  rimessione  non  sono  dedotti    &#13;
 nuovi profili che suggeriscono un riesame della proposta questione;      &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 15, comma quarto  septies,  della  legge  19    &#13;
 marzo  1990,  n.  55  (Nuove  disposizioni  per  la prevenzione della    &#13;
 delinquenza di tipo mafioso e di altre gravi forme di  manifestazione    &#13;
 di  pericolosità  sociale),  introdotto  dall'art.  1 della legge 18    &#13;
 gennaio 1992, n. 16 (Norme in materia di elezioni e nomine presso  le    &#13;
 Regioni e gli enti locali) sollevata, in riferimento agli artt. 3, 4,    &#13;
 35,   36  e  97  della  Costituzione,  dal  Tribunale  amministrativo    &#13;
 regionale per la Sicilia con ordinanza emessa il 12 ottobre 1993.        &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 19 luglio 1994.                               &#13;
                 Il Presidente e redattore: PESCATORE                     &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 27 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
