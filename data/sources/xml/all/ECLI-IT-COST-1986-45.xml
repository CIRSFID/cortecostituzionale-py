<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>45</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:45</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/02/1986</data_decisione>
    <data_deposito>03/03/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - &#13;
 Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - &#13;
 Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO &#13;
 GRECO - Prof. RENATO DELL'ANDRO - Prof. GABRIELE PESCATORE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di ammissibilità del conflitto di attribuzione fra  i  &#13;
 poteri  dello  Stato  sollevato  dal giudice delegato presso la Sezione  &#13;
 fallimentare del Tribunale di  Roma  nei  confronti  del  Ministero  di  &#13;
 Grazia  e  Giustizia,  sorto  a  seguito  della relazione ispettiva del  &#13;
 Ministero di Grazia e Giustizia ricevuta il 6 maggio 1981  ed  iscritto  &#13;
 al n. 23 del registro ammissibilità ricorso conflitti.                  &#13;
     Udito  nella  camera  di  consiglio  del 22 gennaio 1986 il Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
     Ritenuto che il giudice delegato, presso  la  Sezione  fallimentare  &#13;
 del  Tribunale  di Roma, al fallimento della s.p.a.  INVIM ha sollevato  &#13;
 conflitto di attribuzioni nei  confronti  del  Ministero  di  Grazia  e  &#13;
 Giustizia,   il   quale   avrebbe   indebitamente   interferito   nelle  &#13;
 attribuzioni ad esso giudice riservate;                                  &#13;
     che il ricorso è in sostanza Così motivato:                        &#13;
     a) in una relazione ispettiva del Ministero di Grazia  e  Giustizia  &#13;
 dall'anzidetto  giudice  ricevuta  con nota 6 maggio 1981, si esprimono  &#13;
 giudizi sulla  qualificazione  giuridica,  di  coadiutore-perito  o  di  &#13;
 delegato,  e  si  sostiene  che  il  compenso al coadiutore deve essere  &#13;
 liquidato a vocazione, invece  che  sulla  base  del  diverso  criterio  &#13;
 adottato  dal  ricorrente,  laddove  nella giurisprudenza della Suprema  &#13;
 Corte di Cassazione vi sarebbe contrasto sull'assetto della materia;     &#13;
     b) l'adozione di alcun altro eventuale  provvedimento  del  giudice  &#13;
 ricorrente   in   difformità   dai  rilievi  formulati  nell'anzidetta  &#13;
 relazione costituirebbe  oggetto  di  ulteriori  censure  nella  "nuova  &#13;
 ispezione attualmente in corso" presso la Sezione fallimentare, nonché  &#13;
 di "prevedibili azioni disciplinari";                                    &#13;
     c)  l'attività svolta dal giudice delegato al fallimento ha natura  &#13;
 giurisdizionale, "salvo a stabilire quale parte  di  essa  spetti  alla  &#13;
 giurisdizione   contenziosa   e   quale  a  quella  volontaria",  e  la  &#13;
 magistratura, anche nel settore fallimentare, si configura come  ordine  &#13;
 autonomo   e  pertanto  sciolto  da  qualsiasi  dipendenza  dal  potere  &#13;
 esecutivo:  del  resto,  la  legge  12  ottobre  1962,  n.  1311,   nel  &#13;
 disciplinare    l'organizzazione   e   funzionamento   dell'Ispettorato  &#13;
 Generale, si limiterebbe a disporre - in conformità  dell'esigenza  di  &#13;
 tutelare  l'indipendenza e il prestigio del giudice - che gli ispettori  &#13;
 rilevano soltanto dati statistici e possono su tale base esclusivamente  &#13;
 riferire   circa  l'entità  e  tempestività  del  lavoro  svolto  dai  &#13;
 magistrati;                                                              &#13;
     d) pur essendo prevista la possibilità di  indagare  sul  servizio  &#13;
 prestato   dai  magistrati,  sulla  loro  capacità  e  condotta  nelle  &#13;
 situazioni che giustificano l'apertura  di  inchieste  disciplinari  ai  &#13;
 sensi  dell'art. 12 della citata legge n.  1311 del 1962, gli ispettori  &#13;
 procedenti non sarebbero stati nella specie  investiti  del  potere  di  &#13;
 inchiesta    ma    semplicemente   abilitati   ad   agire   nell'ambito  &#13;
 dell'ispezione ordinaria, rimanendo  loro  in  ogni  caso  preclusa  la  &#13;
 valutazione   del   merito   dei  provvedimenti  adottati  dal  giudice  &#13;
 ricorrente.                                                              &#13;
                           Ritenuto altresl':                             &#13;
     che il conflitto Così prospettato è promosso  nei  confronti  del  &#13;
 Ministero  di Grazia e Giustizia, in quanto la relazione ispettiva, con  &#13;
 la  quale  si  assume  posta  in  essere  la  denunciata   interferenza  &#13;
 nell'esercizio   della   giurisdizione,  è  stata  fatta  propria  dal  &#13;
 Ministro,  che  l'ha  trasmessa  sia  al  Consiglio   Superiore   della  &#13;
 Magistratura  sia al Presidente del Tribunale di Roma e ha disposto una  &#13;
 nuova inchiesta.                                                         &#13;
     Considerato che, a norma dell'art. 37, terzo e quarto comma,  legge  &#13;
 n.  87  del  1953,  la  Corte, in questa fase, è chiamata a deliberare  &#13;
 senza contraddittorio se il ricorso sia ammissibile, in  quanto  esista  &#13;
 "la  materia  di  un  conflitto  la  cui  risoluzione  spetti  alla sua  &#13;
 competenza", rimanendo perciò impregiudicata, ove la pronuncia sia  di  &#13;
 ammissibilità, la facoltà delle parti di proporre nel corso ulteriore  &#13;
 del giudizio, anche su questo punto, istanze ed eccezioni;               &#13;
     che  per  l'ammissibilità del conflitto è prescritto, a norma del  &#13;
 citato art. 37,  il  concorso  dei  seguenti  requisiti  soggettivi  ed  &#13;
 oggettivi: il conflitto deve sorgere tra organi competenti a dichiarare  &#13;
 definitivamente   la  volontà  del  potere  cui  essi  appartengono  e  &#13;
 riguardare  la  delimitazione  della  relativa  sfera  di  attribuzione  &#13;
 secondo le norme costituzionali;                                         &#13;
     che,  nel caso in esame, il ricorso è proposto di fronte a rilievi  &#13;
 formulati nella relazione dagli organi ispettivi  del  Ministero  della  &#13;
 Giustizia,  in  assenza di alcun provvedimento, disciplinare o di altra  &#13;
 natura, del Ministro:  il quale ultimo, ad avviso del giudice  delegato  &#13;
 al    fallimento,    sarebbe,    in    quanto    organo    di   vertice  &#13;
 dell'Amministrazione, legittimato passivo come  parte  nel  prospettato  &#13;
 conflitto;                                                               &#13;
     che  difetta in conseguenza il possibile oggetto della controversia  &#13;
 instaurata avanti la Corte: l'avere il Ministro - come si  afferma  dal  &#13;
 ricorrente, sempre nel presupposto della legittimazione passiva di tale  &#13;
 organo  -  inviato  l'anzidetta  relazione al Consiglio Superiore della  &#13;
 Magistratura e al Presidente del Tribunale di Roma,  o  disposto  nuova  &#13;
 ispezione   in   vista   di   ulteriori,  e  soltanto  eventuali,  suoi  &#13;
 provvedimenti, costituisce, infatti,  un  adempimento  procedurale  nel  &#13;
 corso  dell'attività  ispettiva,  che  non  concreta,  nemmeno  in via  &#13;
 preparatoria, gli estremi di alcun  atto  idoneo  a  ledere  l'invocata  &#13;
 sfera  del  potere  giurisdizionale  o  ad  interferire  altrimenti nel  &#13;
 relativo esercizio;                                                      &#13;
     che questo rilievo determina  l'inammissibilità  del  conflitto  e  &#13;
 assorbe ogni altro profilo da esaminare in questa sede.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile  il  ricorso  per conflitto di attribuzione  &#13;
 indicato in epigrafe.                                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 26 febbraio 1986.                             &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO -  &#13;
                                   GABRIELE PESCATORE.                    &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
