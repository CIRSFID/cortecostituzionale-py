<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>174</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:174</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giulio Gionfrida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/06/1974</data_decisione>
    <data_deposito>19/06/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI - Avv. LEONETTO AMADEI - Dott. GUIDO GIONFRIDA - Prof. EDOARDO &#13;
 VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti promossi con tre ricorsi del  Presidente  della  &#13;
 Regione  Campania,  notificati  il  19  settembre e il 18 ottobre 1973,  &#13;
 depositati in cancelleria il 22 settembre  e  il  24  ottobre  1973  ed  &#13;
 iscritti  ai  nn.  10,  11  e  12  del  registro 1973, per conflitti di  &#13;
 attribuzione sorti a seguito dei decreti 9 aprile 1973, n.  10754,  del  &#13;
 Prefetto  di  Napoli  e  10  maggio  1973,  n.  50565,  del Prefetto di  &#13;
 Benevento,  concernenti  la   nomina   dei   commissari   straordinari,  &#13;
 rispettivamente,  dell'asilo  infantile  "Croce  Rossa" di San Giuseppe  &#13;
 Vesuviano e dell'Ente comunale di assistenza  di  Arpaia,  nonché  del  &#13;
 decreto 9 giugno 1973, numero 11008, del Prefetto di Napoli, con cui si  &#13;
 è  provveduto  alla  ricostituzione  del  Consiglio di amministrazione  &#13;
 dell'ospedale civile "Albano Francescano" di Procida.                    &#13;
     Visti gli atti di costituzione del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza pubblica del 30 aprile 1974 il Giudice relatore  &#13;
 Giulio Gionfrida;                                                        &#13;
     udito il sostituto avvocato generale dello Stato Michele  Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  - Con due ricorsi, notificati entrambi il 19 settembre 1973, il  &#13;
 Presidente  della  Regione   Campania   ha   sollevato   conflitto   di  &#13;
 attribuzione  nei  confronti  del Presidente del Consiglio dei ministri  &#13;
 impugnando i provvedimenti del Prefetto di Napoli in data 9 aprile 1973  &#13;
 e del Prefetto di Benevento in data 10 maggio 1973, con i quali  si  è  &#13;
 provveduto alla nomina di commissari straordinari, rispettivamente, per  &#13;
 l'Opera  pia  "Asilo  infantile Croce Rossa" di S. Giuseppe Vesuviano e  &#13;
 per l'Ente comunale di assistenza di Arpaia.                             &#13;
     Lamenta la Regione la  violazione  degli  artt.  117  e  118  della  &#13;
 Costituzione,  in  relazione  anche al d.P.R. 15 gennaio 1972, n. 9, in  &#13;
 quanto la materia dell'assistenza  e  beneficenza  dovrebbe  intendersi  &#13;
 integralmente  trasferita  alle  Regioni  e  con  essa  anche  i poteri  &#13;
 "politici" di supremazia sugli enti operanti nel  settore:    tra  cui,  &#13;
 appunto,  il  potere  di  esercitare,  nei  confronti  di tali enti, il  &#13;
 controllo, non solo sugli atti, ma anche sugli organi.                   &#13;
     Eccepisce,  preliminarmente  il costituito Presidente del Consiglio  &#13;
 dei ministri (che, per altro, resiste la tesi della Regione  anche  nel  &#13;
 merito)   che   i  ricorsi  sopradetti  sarebbero,  comunque,  entrambi  &#13;
 inammissibili: in quanto notificati oltre il termine di 60 giorni dalla  &#13;
 notificazione, pubblicazione o conoscenza dell'atto impugnato,  di  cui  &#13;
 all'art.  39  della  legge  11 marzo 1953, n. 87; senza che possa nella  &#13;
 specie invocarsi la legge 7 ottobre 1969, n. 742, sulla sospensione dei  &#13;
 termini  feriali,  non  trovando  questa  applicazione   nel   processo  &#13;
 costituzionale.                                                          &#13;
     2.  -  Con  altro  ricorso,  notificato  il  18  ottobre  1973,  il  &#13;
 Presidente della Regione Campania ha anche impugnato, per conflitto  di  &#13;
 attribuzione,  il  decreto del Prefetto di Napoli in data 9 giugno 1973  &#13;
 con  cui  si  è  provveduto  alla  ricostituzione  del  Consiglio   di  &#13;
 amministrazione  dell'Opera pia ospedale civile "Albano Francescano" di  &#13;
 Procida.                                                                 &#13;
     Nelle proprie deduzioni, rileva preliminarmente la Regione che  "la  &#13;
 natura  dell'ente  in  questione appare non definita", in quanto questo  &#13;
 "sebbene costituito come opera  pia"  di  fatto  esercitando  attività  &#13;
 sanitaria, si presenta come un "ente ospedaliero".                       &#13;
     Ciò  per  altro  non  inciderebbe sulla attribuzione del potere di  &#13;
 controllo sostitutivo sull'ente stesso, che, in ogni caso,  spetterebbe  &#13;
 alla  Regione:  in  base  alla  legge  ospedaliera 1968, n. 132, ove si  &#13;
 evinca la natura dell'ente dalle funzioni effettivamente esercitate, od  &#13;
 in base agli artt. 117, 118 della Costituzione e al d.P.R.  1972, n. 9,  &#13;
 ove si consideri l'ente tra quelli di natura assistenziale.              &#13;
     Di conseguenza il provvedimento prefettizio impugnato -  in  quanto  &#13;
 comunque invasivo di competenze regionali - andrebbe annullato.          &#13;
     Anche  di  tale  ricorso  il  Presidente  del Consiglio costituito,  &#13;
 eccepisce, preliminarmente,  l'inammissibilità  per  tardività  della  &#13;
 notificazione:  mentre  nel  merito,  ne contesta la fondatezza, per la  &#13;
 ragione che il controllo sugli organi - quale  nella  specie  viene  in  &#13;
 discussione  -  competerebbe,  anche relativamente agli enti locali, in  &#13;
 ogni caso all'autorità statale, ex art. 130 della Costituzione.<diritto>Considerato in diritto</diritto>:                          &#13;
     I  ricorsi,  che   possono   essere   riuniti,   vanno   dichiarati  &#13;
 inammissibili  perché  notificati  oltre il termine di sessanta giorni  &#13;
 previsto dall'art. 39 della legge 11 marzo 1953, n. 87.                  &#13;
     Il decreto del Prefetto di Napoli, emesso il  9  aprile  1973,  era  &#13;
 stato  notificato  alla Regione Campania il 6 giugno 1973, e il decreto  &#13;
 del Prefetto di Benevento del 10 maggio 1973 le era stato comunicato il  &#13;
 5 giugno successivo.                                                     &#13;
     Entrambi, come si è detto, sono stati impugnati dalla Regione  con  &#13;
 ricorsi  notificati  il  19  settembre  1973, e quindi oltre il termine  &#13;
 predetto di decadenza.                                                   &#13;
     Lo stesso è a dire rispetto al decreto del Prefetto di  Napoli  in  &#13;
 data  9  giugno 1973, che era stato comunicato alla Regione il 5 luglio  &#13;
 1973 ed è stato impugnato con ricorso notificato il 18 ottobre 1973.    &#13;
     Né, per ritenersi la tempestività dei ricorsi, potrebbe invocarsi  &#13;
 la legge  7  ottobre  1969,  n.  742,  sulla  sospensione  dei  termini  &#13;
 processuali  nel  periodo feriale, giacché la giurisprudenza di questa  &#13;
 Corte è ferma nell'escludere l'applicabilità di tale  disciplina  nel  &#13;
 processo costituzionale (sent. n. 30 del 1973).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibili  per  tardività  i  ricorsi proposti dalla  &#13;
 Regione Campania con atti notificati il  19  settembre  1973  e  il  18  &#13;
 ottobre  1973  per la risoluzione di conflitto di attribuzione relativo  &#13;
 ai decreti prefettizi indicati in epigrafe.                              &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 12 giugno 1974.                               &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VEZIO  CRISAFULLI  -  NICOLA  REALE -  &#13;
                                   PAOLO  ROSSI  -  LEONETTO  AMADEI   -  &#13;
                                   GIULIO GIONFRIDA - EDOARDO VOLTERRA -  &#13;
                                   GUIDO ASTUTI.                          &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
