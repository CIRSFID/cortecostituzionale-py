<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>46</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:46</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1997</data_decisione>
    <data_deposito>20/02/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 696, primo    &#13;
 comma, del cod. proc. civ.,  promosso  con  ordinanza  emessa  il  23    &#13;
 gennaio  1996  dal  pretore  di  Torino nel procedimento vertente tra    &#13;
 Pietro Corino e FIAT AUTO s.p.a, iscritta  al  n.  237  del  registro    &#13;
 ordinanze 1996 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 12, prima serie speciale, dell'anno 1996;                             &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 27  novembre  1996  il  giudice    &#13;
 relatore Cesare Mirabelli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ordinanza  emessa  il  23  gennaio 1996 nel corso di un    &#13;
 procedimento di istruzione preventiva promosso  per  far  verificare,    &#13;
 con   un   accertamento   tecnico   prima   del  giudizio,  la  causa    &#13;
 dell'incendio di un'autovettura ed in particolare se  esso  derivasse    &#13;
 da  un  vizio di fabbricazione, il pretore di Torino ha sollevato, in    &#13;
 riferimento agli artt.  3  e  24  della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale dell'art.   696, primo comma, cod. proc.    &#13;
 civ., nella parte in cui non prevede che il  giudice  possa  disporre    &#13;
 l'accertamento tecnico preventivo anche sulla causa dei danni.           &#13;
   Il  pretore ricorda che la disposizione denunciata limita tale atto    &#13;
 di istruzione preventiva alla verifica dello stato dei luoghi o della    &#13;
 qualità o condizione di cose, senza che possano essere formulati  al    &#13;
 consulente   tecnico,   secondo   l'interpretazione  della  Corte  di    &#13;
 cassazione,  quesiti  sulle  cause  e  sull'entità  dei  danni,   in    &#13;
 previsione  di  un'azione  di  risarcimento; ma un accertamento così    &#13;
 ristretto sarebbe di scarsa utilità e menomerebbe  il  diritto  alla    &#13;
 prova,   considerato  nucleo  essenziale  del  diritto  di  agire  in    &#13;
 giudizio, garantito dall'art.  24 della Costituzione.                    &#13;
   Ad   avviso   del   giudice   rimettente,   la  mancata  estensione    &#13;
 dell'accertamento   tecnico   preventivo   alle   cause   del   danno    &#13;
 determinerebbe  anche,  in contrasto con l'art. 3 della Costituzione,    &#13;
 un'irragionevole disparità  di  trattamento.  Difatti,  quando  tale    &#13;
 accertamento  sia stato esteso dal giudice o dal consulente, sia pure    &#13;
 in contrasto con quanto prevede la legge, sino a comprendere le cause    &#13;
 e  l'entità  dei  danni,  l'accertamento  stesso   potrebbe   essere    &#13;
 successivamente  acquisito,  in  mancanza  di opposizione della parte    &#13;
 controinteressata,  al  giudizio  di  merito;   mentre,   rispettando    &#13;
 l'ambito  previsto per questo atto di istruzione preventiva dall'art.    &#13;
 696 cod. proc. civ., l'accertamento, egualmente utile nel giudizio di    &#13;
 merito, non potrebbe essere effettuato.                                  &#13;
   2. - Nel giudizio dinanzi alla Corte è intervenuto  il  Presidente    &#13;
 del  Consiglio  dei  Ministri, rappresentato e difeso dall'Avvocatura    &#13;
 generale dello Stato, chiedendo  che  la  questione  di  legittimità    &#13;
 costituzionale sia dichiarata inammissibile o non fondata.               &#13;
   L'Avvocatura  rileva  che l'ordinanza di rimessione non considera i    &#13;
 limiti  propri  dell'accertamento  tecnico  preventivo,  che  è   un    &#13;
 procedimento   sommario   destinato   ad   impedire  il  venire  meno    &#13;
 dell'oggetto di una prova, ritenuta rilevante per un futuro  giudizio    &#13;
 di merito. Estendere anche in questo procedimento l'accertamento alla    &#13;
 causa  del  danno  significherebbe ampliare il contenuto dell'atto di    &#13;
 istruzione preventiva, per sua natura limitato, essendo  connesso  ad    &#13;
 una  situazione  dalla  quale può eventualmente sorgere una pretesa,    &#13;
 che  dovrà  essere  fatta  valere  in  un  futuro  giudizio.  Questa    &#13;
 particolarità  dimostra,  ad avviso dell'Avvocatura, che non sarebbe    &#13;
 posta a rischio la tutela del diritto nel futuro  processo,  giacché    &#13;
 solo  nel  successivo  giudizio  di  merito la causa del danno potrà    &#13;
 essere riconosciuta, mediante una valida consulenza tecnica.             &#13;
   Non sarebbe neppure violato l'art. 3  della  Costituzione,  perché    &#13;
 l'accertamento,  disposto  dal giudice o effettuato dal consulente in    &#13;
 contrasto con i limiti posti dalla  norma  denunciata,  costituirebbe    &#13;
 una  patologia processuale e non potrebbe essere considerato un utile    &#13;
 termine di confronto.<diritto>Considerato in diritto</diritto>1. -   La  questione  di  legittimità  costituzionale  investe  la    &#13;
 disciplina  dell'accertamento tecnico nell'ambito dei procedimenti di    &#13;
 istruzione preventiva.                                                   &#13;
   Il pretore di Torino ritiene che  l'art.  696,  primo  comma,  cod.    &#13;
 proc.  civ., nel prevedere che chi ha urgenza di far verificare prima    &#13;
 del giudizio lo stato  dei  luoghi  o  la  condizione  di  cose  può    &#13;
 chiedere  che  sia  disposto un accertamento tecnico, non consenta di    &#13;
 accertare la causa e l'entità dei danni, in vista di un giudizio  di    &#13;
 risarcimento.    Ciò determinerebbe una lesione del diritto di agire    &#13;
 in  giudizio,  garantito  dall'art.  24   della   Costituzione,   che    &#13;
 comprenderebbe,  nel  suo  nucleo  essenziale,  anche il diritto alla    &#13;
 prova. Inoltre si determinerebbe, in  violazione  dell'art.  3  della    &#13;
 Costituzione, una ingiustificata disparità di trattamento rispetto a    &#13;
 chi  abbia  ottenuto  dal  giudice  o  dal  consulente,  sia  pure in    &#13;
 contrasto con la  disciplina  legislativa,  un  accertamento  tecnico    &#13;
 esteso  alla  causa  del  danno,  giacché  in  tal  caso i risultati    &#13;
 dell'accertamento  potrebbero  essere  acquisiti,  in   mancanza   di    &#13;
 opposizione della parte controinteressata, nel successivo giudizio di    &#13;
 merito.                                                                  &#13;
   2.  -  La  questione,  proposta  con  riferimento  al  principio di    &#13;
 eguaglianza (art. 3 Cost.), è inammissibile.                            &#13;
   Difatti  l'ordinanza  di  rimessione  assume,  quale   termine   di    &#13;
 comparazione  per  chiedere  il superamento di un limite legislativo,    &#13;
 una situazione che  la  stessa  ordinanza  considera  patologica  nel    &#13;
 processo,  in  quanto  determinata dalla violazione del limite che si    &#13;
 vuole rimuovere. Mentre il presupposto per la  corretta  proposizione    &#13;
 di un giudizio di comparazione, necessario per valutare la parità di    &#13;
 trattamento   di  situazioni  che  si  assumono  eguali,  è  che  la    &#13;
 situazione indicata quale termine di raffronto non costituisca,  come    &#13;
 invece   avviene   nella  prospettazione  offerta  dall'ordinanza  di    &#13;
 rimessione, la violazione di una norma, rispettando la quale  non  si    &#13;
 determinerebbe   la   dedotta  ineguaglianza.     D'altra  parte,  la    &#13;
 disparità   di   trattamento,   che   viene   riferita   ai   limiti    &#13;
 dell'accertamento  tecnico  preventivo,  sarebbe  piuttosto collegata    &#13;
 alle successive vicende dell'acquisizione della prova nel processo  e    &#13;
 risponde,  quindi,  ad  altri  e  diversi profili, che attengono alla    &#13;
 logica del principio dispositivo, al quale si  conforma  il  processo    &#13;
 civile.                                                                  &#13;
   3. - Con riferimento all'art. 24 della Costituzione la questione è    &#13;
 infondata.                                                               &#13;
   Il  potere  di  agire in giudizio per la tutela dei propri diritti,    &#13;
 garantito dalla Costituzione, trova gli strumenti per la sua concreta    &#13;
 esplicazione nella disciplina  del  processo,  secondo  le  modalità    &#13;
 proprie  di  esso,  attraverso  una  molteplicità  di  istituti. Nel    &#13;
 processo  civile,  e  per  la  configurazione  delle  prove  in  esso    &#13;
 previste,  la  garanzia  dell'azione  e  del giudizio, per non essere    &#13;
 compressa se non addirittura  vanificata,  implica  che,  quando  sia    &#13;
 presente  un  rischio  di dispersione degli elementi necessari per la    &#13;
 formazione della prova, sia  resa  possibile  la  ricognizione  e  la    &#13;
 raccolta  di  tali  elementi,  mediante  procedure e provvedimenti di    &#13;
 urgenza destinati ad evitare che la modifica delle situazioni  e  gli    &#13;
 eventi che si possono verificare prima del giudizio ne impediscano la    &#13;
 tempestiva acquisizione. Altrimenti potrebbero essere definitivamente    &#13;
 compromessi  gli  accertamenti  e  le  valutazioni  che devono essere    &#13;
 effettuati nel processo di merito e la domanda verrebbe  privata  del    &#13;
 suo supporto probatorio.                                                 &#13;
   La   necessità   di  prevedere  e  disciplinare  atti  urgenti  di    &#13;
 istruzione  preventiva,  che  rendano  possibile  l'accertamento  del    &#13;
 proprio diritto anche in caso di dispersione degli elementi di prova,    &#13;
 non  deve  tuttavia  estendersi,  perché  si possa dire garantito il    &#13;
 diritto  di  agire  in  giudizio,  sino  a  comprendere  nella   sede    &#13;
 preventiva  le ulteriori valutazioni tecniche che, anche sulla scorta    &#13;
 degli elementi acquisiti in via di  urgenza,  possano  essere  svolte    &#13;
 successivamente,  con  la  consulenza  nel  processo  di  merito,  in    &#13;
 rapporto ad una azione già  esercitata  e  nel  rispetto  del  pieno    &#13;
 contraddittorio tra le parti.                                            &#13;
   La  garanzia  del  diritto di agire in giudizio non impone, dunque,    &#13;
 che l'atto di istruzione preventiva sia configurato come  sostanziale    &#13;
 anticipazione,   in  via  di  urgenza,  dell'apprezzamento  che  deve    &#13;
 formarsi nel giudizio, mentre necessaria è solo una  misura  diretta    &#13;
 all'acquisizione  di  tutti  gli elementi di fatto, anche connotativi    &#13;
 delle cause del danno, che rischiano di essere dispersi.                 &#13;
   A  questa  esigenza  è diretto a rispondere l'accertamento tecnico    &#13;
 preventivo disciplinato dall'art. 696 cod. proc. civ., la cui portata    &#13;
 va interpretata in coerenza con il sistema ed alla luce dei  principi    &#13;
 costituzionali  che  garantiscono  la  tutela in giudizio del proprio    &#13;
 diritto. La disposizione processuale  denunciata  consente,  con  una    &#13;
 espressione  generica  e  dal  contenuto  molto  ampio, la preventiva    &#13;
 verifica dello stato dei luoghi e  della  qualità  o  condizione  di    &#13;
 cose.  Il  termine  "verifica" comprende difatti, nel suo significato    &#13;
 letterale, l'esame, la  constatazione,  il  controllo,  la  prova  di    &#13;
 quanto  viene  sottoposto  all'attività  di  accertamento in sede di    &#13;
 istruzione preventiva, come tale preordinata al futuro  giudizio.  La    &#13;
 stessa  disposizione denunciata può, dunque, essere interpretata nel    &#13;
 senso che  il  previsto  accertamento  tecnico  comprenda  tutti  gli    &#13;
 elementi  conoscitivi  considerati  necessari  per le valutazioni che    &#13;
 dovranno essere effettuate nel giudizio di merito ed includa, quindi,    &#13;
 ogni acquisizione  preordinata  alla  successiva  valutazione,  anche    &#13;
 tecnica,  che in quel giudizio si dovrà esprimere per determinare la    &#13;
 causa del danno o l'entità di esso.   In tal  modo  non  si  ha  una    &#13;
 impropria  anticipazione  del  giudizio,  ma  non manca un'anticipata    &#13;
 raccolta di ogni elemento di fatto necessario per il giudizio.           &#13;
   Questo discrimine tra atti consentiti ed atti non previsti in  sede    &#13;
 di  accertamento  tecnico  preventivo,  desumibile  da  una  adeguata    &#13;
 interpretazione dell'art.  696  cod.  proc.  civ.,  coerente  con  il    &#13;
 sistema  ed  orientata  dal  principio  di  garanzia del diritto alla    &#13;
 prova, supera il dubbio di  legittimità  costituzionale  prospettato    &#13;
 dal pretore di Torino.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara:                                                              &#13;
     inammissibile   la   questione   di  legittimità  costituzionale    &#13;
 dell'art.  696, primo comma, cod. proc. civ., sollevata  dal  pretore    &#13;
 di   Torino,  in  riferimento  all'art.  3  della  Costituzione,  con    &#13;
 l'ordinanza indicata in epigrafe;                                        &#13;
     non fondata la questione di legittimità costituzionale dell'art.    &#13;
 696, primo comma, cod. proc. civ., sollevata dallo stesso giudice  in    &#13;
 riferimento all'art. 24 della Costituzione.                              &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 10 febbraio 1997.                             &#13;
                         Il Presidente: Granata                           &#13;
                        Il redattore: Mirabelli                           &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 20 febbraio 1997.                         &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
