<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>27</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:27</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/02/1999</data_decisione>
    <data_deposito>11/02/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  per  conflitto  di  attribuzione sorto a seguito della    &#13;
 sentenza del Consiglio di Stato, sezione IV,  n.  625  del  6  giugno    &#13;
 1997, con la quale è stato respinto l'appello avverso l'annullamento    &#13;
 del  provvedimento  del  Presidente della Provincia in data 10 luglio    &#13;
 1986 che aveva disposto la sospensione della licenza di un  esercizio    &#13;
 pubblico,  promosso con ricorso della Provincia di Trento, notificato    &#13;
 il 29 agosto 1997, depositato in Cancelleria il 4 settembre  1997  ed    &#13;
 iscritto al n. 47 del registro ricorsi (recte: conflitti) 1997.          &#13;
   Visto  l'atto  di  costituzione  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
   Udito nell'udienza pubblica del 21 aprile 1998 il giudice  relatore    &#13;
 Gustavo Zagrebelsky;                                                     &#13;
   Uditi  l'avvocato  Giandomenico Falcon per la Provincia di Trento e    &#13;
 l'avvocato dello Stato  Giuseppe  O.  Russo  per  il  Presidente  del    &#13;
 Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Con ricorso regolarmente notificato e depositato la Provincia    &#13;
 autonoma  di  Trento  ha  sollevato  conflitto  di  attribuzione  nei    &#13;
 confronti  dello  Stato,  per  violazione  dell'art. 20, primo comma,    &#13;
 dello statuto speciale per la Regione Trentino-Alto Adige (d.P.R.  31    &#13;
 agosto  1972,  n.  670)  e  relative  norme  di  attuazione (d.P.R. 1    &#13;
 novembre 1973, n.  686, recante "Norme di  attuazione  dello  statuto    &#13;
 speciale  per  la  Regione  Trentino-Alto  Adige concernente esercizi    &#13;
 pubblici e spettacoli pubblici"), secondo  cui  "i  Presidenti  delle    &#13;
 Giunte provinciali esercitano le attribuzioni spettanti all'autorità    &#13;
 di pubblica sicurezza previste dalle leggi vigenti, in materia di ...    &#13;
 esercizi  pubblici" (art. 20 citato), tra le quali dovrebbe ritenersi    &#13;
 inclusa quella relativa  alla  sospensione  della  licenza  di  detti    &#13;
 esercizi,  prevista  dall'art.    100  del testo unico delle leggi di    &#13;
 pubblica sicurezza (r.d. 18 giugno 1931, n. 773), in riferimento alla    &#13;
 sentenza con la quale il Consiglio di Stato (sez. IV, 6 giugno  1997,    &#13;
 n.  625)  ha  confermato  la  pronuncia  del  Tribunale  regionale di    &#13;
 giustizia  amministrativa  di  Trento  che  aveva  annullato  -   per    &#13;
 incompetenza  dell'organo  provinciale  a  emanare  provvedimenti  in    &#13;
 materia di ordine pubblico, riservati, invece, agli organi statali in    &#13;
 base all'art. 21 dello statuto speciale - l'ordinanza di  sospensione    &#13;
 della   autorizzazione   all'apertura  di  un  esercizio  commerciale    &#13;
 adottata dal Presidente della Giunta provinciale di Trento.              &#13;
   Premesso che l'impugnata sentenza sarebbe "erronea,  arbitraria  ed    &#13;
 illegittimamente invasiva", la difesa della Provincia autonoma rileva    &#13;
 che  il  Consiglio di Stato muove da una interpretazione non corretta    &#13;
 della competenza provinciale in materia di esercizi pubblici  di  cui    &#13;
 all'art. 20 dello statuto, competenza che il giudice ritiene limitata    &#13;
 agli  aspetti relativi alla "regolarità commerciale" dell'esercizio,    &#13;
 mentre sulla  base  della  normativa  statutaria  e  delle  norme  di    &#13;
 attuazione  il  riparto  di  competenza  tra  Presidente della Giunta    &#13;
 provinciale e questore  dovrebbe  avvenire  per  materia,  e  non  in    &#13;
 relazione  ai  differenti  interessi perseguiti, o ai diversi tipi di    &#13;
 provvedimento.  Il  Consiglio  di  Stato   avrebbe   qualificato   il    &#13;
 provvedimento  adottato  dal  Presidente  della Giunta provinciale ai    &#13;
 sensi dell'art. 100 del testo unico delle leggi di pubblica sicurezza    &#13;
 come provvedimento "per l'ordine pubblico":   attraverso una  erronea    &#13;
 lettura  degli artt. 9, numero 7, 20, terzo comma, e 21 dello statuto    &#13;
 speciale,  nonché un improprio richiamo al d.P.R. 24 luglio 1977, n.    &#13;
 616,  concernente  le  regioni  a  statuto  ordinario,  la   pubblica    &#13;
 sicurezza  sarebbe  stata  inclusa nella nozione di ordine pubblico e    &#13;
 ricondotta per intero alla competenza statale.                           &#13;
   2.  -  Si  è  costituito  nel  giudizio  di  fronte   alla   Corte    &#13;
 costituzionale    il   Presidente   del   Consiglio   dei   Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo  che  il  ricorso  sia dichiarato inammissibile. Secondo la    &#13;
 costante giurisprudenza costituzionale, il conflitto di  attribuzione    &#13;
 tra   Stato  e  Regione  in  relazione  ad  atti  giurisdizionali  è    &#13;
 ammissibile in quanto la Regione contesti in radice la spettanza  del    &#13;
 potere  all'organo  giurisdizionale, mentre non è ammissibile quando    &#13;
 essa si limiti a censurare il modo in  cui  la  giurisdizione  si  è    &#13;
 concretamente  esplicata,  denunziando eventuali errori in iudicando.    &#13;
 Nel caso di specie il giudice amministrativo si  sarebbe  limitato  a    &#13;
 dirimere, nell'ambito dei suoi indiscussi poteri giurisdizionali, una    &#13;
 questione    di    competenza    tra    organi   "accidentalmente   e    &#13;
 ininfluentemente" appartenenti a soggetti diversi.                       &#13;
   3. - In prossimità dell'udienza la difesa della Provincia autonoma    &#13;
 di Trento ha depositato una memoria sostenendo  l'ammissibilità  del    &#13;
 ricorso,  in  quanto il giudice amministrativo, individuando i poteri    &#13;
 spettanti alla Provincia, avrebbe oltrepassato i  confini  della  sua    &#13;
 giurisdizione.  La  negazione di poteri che la ricorrente esercita in    &#13;
 virtù  delle  proprie  attribuzioni  statutarie   richiederebbe   un    &#13;
 intervento  della  Corte  costituzionale, tanto più che, nel caso di    &#13;
 atti  di  organi  giurisdizionali,   il   vulnus   alle   prerogative    &#13;
 costituzionali sarebbe, altrimenti, irrimediabile.<diritto>Considerato in diritto</diritto>1.   -  La  Provincia  autonoma  di  Trento  propone  conflitto  di    &#13;
 attribuzione contro il Presidente  del  Consiglio  dei  Ministri,  in    &#13;
 riferimento  alla sentenza del Consiglio di Stato, sezione IV, n. 625    &#13;
 del 6 giugno 1997, che ha  confermato  una  pronuncia  del  Tribunale    &#13;
 regionale  di giustizia amministrativa di Trento di annullamento, per    &#13;
 incompetenza, dell'ordinanza del Presidente della Giunta  provinciale    &#13;
 di  sospensione  dell'autorizzazione  all'apertura  di  un  esercizio    &#13;
 commerciale. Sarebbero violati l'art. 20, primo comma, dello  statuto    &#13;
 speciale  per  la Regione Trentino-Alto Adige (d.P.R. 31 agosto 1972,    &#13;
 n. 670) e il d.P.R. 1  novembre  1973,  n.  686  (recante  "Norme  di    &#13;
 attuazione  dello statuto speciale per la Regione Trentino-Alto Adige    &#13;
 concernente esercizi  pubblici  e  spettacoli  pubblici").  La  norma    &#13;
 statutaria  -  confermata  dall'art. 3, primo comma, delle menzionate    &#13;
 norme di attuazione -  stabilisce  che  "i  Presidenti  delle  Giunte    &#13;
 provinciali  esercitano  le  attribuzioni  spettanti all'autorità di    &#13;
 pubblica sicurezza  previste  dalle  leggi  vigenti",  tra  le  quali    &#13;
 dovrebbe  ritenersi  inclusa  quella  relativa alla sospensione della    &#13;
 licenza degli esercizi pubblici, prevista  dall'art.  100  del  testo    &#13;
 unico  delle  leggi  di  pubblica  sicurezza (r.d. 18 giugno 1931, n.    &#13;
 773).                                                                    &#13;
   Ad avviso della ricorrente, la decisione del giudice amministrativo    &#13;
 che ha dato  origine  al  conflitto  disconoscerebbe  una  competenza    &#13;
 provinciale  avente  fondamento  nello statuto speciale, in quanto il    &#13;
 Consiglio di Stato  ha  qualificato  il  provvedimento  adottato  dal    &#13;
 Presidente  della Giunta provinciale ai sensi dell'art. 100 del testo    &#13;
 unico delle  leggi  di  pubblica  sicurezza  come  provvedimento  per    &#13;
 l'ordine  pubblico,  di  competenza dello Stato, a norma dell'art. 21    &#13;
 dello statuto speciale.                                                  &#13;
   2. - Il conflitto non è ammissibile.                                  &#13;
   2.1. - Atti di giurisdizione, secondo consolidata giurisprudenza di    &#13;
 questa Corte, possono essere a base di conflitto di attribuzione  tra    &#13;
 Regioni  e  Stato,  oltre  che  tra  poteri  dello  Stato, purché il    &#13;
 conflitto medesimo non si risolva in mezzo improprio di  censura  del    &#13;
 modo  di  esercizio  della funzione giurisdizionale (sentenze nn. 289    &#13;
 del 1974, 98 e 183 del 1981, 70 del 1985, 285 del 1990, 99 e 175  del    &#13;
 1991, 357 del 1996).                                                     &#13;
   Contro   gli   errori   in   iudicando  di  diritto  sostanziale  o    &#13;
 processuale, infatti, valgono i rimedi  consueti  riconosciuti  dagli    &#13;
 ordinamenti  processuali  delle  diverse  giurisdizioni;  non vale il    &#13;
 conflitto di  attribuzione.  A  ritenere  diversamente,  il  giudizio    &#13;
 presso la Corte costituzionale si trasformerebbe inammissibilmente in    &#13;
 un  nuovo  grado  di  giurisdizione  avente  portata  tendenzialmente    &#13;
 generale. Avendo infatti per lo più le situazioni  soggettive  delle    &#13;
 Regioni   base   diretta   o  almeno  indiretta  in  norme  di  rango    &#13;
 costituzionale attributive di competenza, la gran parte dei motivi di    &#13;
 doglianza da parte  delle  stesse  contro  decisioni  giurisdizionali    &#13;
 finirebbe  per  potersi  trasformare  automaticamente  in  motivo  di    &#13;
 ricorso per conflitto di attribuzione,  con  evidente  forzatura  dei    &#13;
 caratteri  propri  di  quest'ultimo e alterazione dei rapporti tra la    &#13;
 giurisdizione  costituzionale  e  quella   riconosciuta   a   istanze    &#13;
 giurisdizionali non costituzionali.                                      &#13;
   Invece,  ancora  secondo  la  giurisprudenza  di questa Corte sopra    &#13;
 ricordata, il rimedio del conflitto di attribuzione relativamente  ad    &#13;
 atti   di   giurisdizione  è  configurabile  quando  sia  contestata    &#13;
 radicalmente la riconducibilità dell'atto che determina il conflitto    &#13;
 alla funzione giurisdizionale (cfr., ad esempio, sentenze nn. 150 del    &#13;
 1981 e 283 del 1986) ovvero sia messa in questione l'esistenza stessa    &#13;
 del potere giurisdizionale nei confronti del soggetto ricorrente.  In    &#13;
 tutti  questi  casi, il conflitto verrebbe infatti a configurarsi non    &#13;
 come controllo sul contenuto dell'attività giurisdizionale, ma  come    &#13;
 garanzia  di sfere di attribuzioni che si vogliono costituzionalmente    &#13;
 protette da interferenze da parte di organi della giurisdizione o che    &#13;
 si vogliono riservare al controllo di altra istanza costituzionale.      &#13;
   2.2. - Nella specie, la Provincia autonoma ricorrente non  contesta    &#13;
 l'esistenza    del    potere   giurisdizionale   relativamente   alla    &#13;
 legittimità dei provvedimenti di  sospensione  della  autorizzazione    &#13;
 all'apertura   degli   esercizi  commerciali.  Essa  contesta  invece    &#13;
 l'argomentazione contenuta nella  sentenza  del  Consiglio  di  Stato    &#13;
 secondo  la  quale  tali  provvedimenti  sarebbero  da ascrivere alla    &#13;
 difesa dell'ordine pubblico e non invece  della  sicurezza  pubblica,    &#13;
 con  la  conseguente affermazione della competenza statale invece che    &#13;
 regionale.                                                               &#13;
   Trattasi quindi  di  una  controversia  che,  avendo  base  in  una    &#13;
 questione  di interpretazione del diritto vigente che influisce sulla    &#13;
 decisione  del  giudice  che  si  vorrebbe  censurare,  non   attiene    &#13;
 all'esistenza  della  giurisdizione  in  quanto  tale. Il ricorso per    &#13;
 conflitto   di   attribuzione   deve   pertanto   essere   dichiarato    &#13;
 inammissibile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara inammissibile il conflitto di attribuzione sollevato dalla    &#13;
 Provincia  autonoma di Trento nei confronti dello Stato, in relazione    &#13;
 alla sentenza del Consiglio di Stato, sezione IV, n. 625 del 6 giugno    &#13;
 1997, con il ricorso indicato in epigrafe.                               &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 febbraio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria l'11 febbraio 1999.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
