<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>98</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:98</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/04/1974</data_decisione>
    <data_deposito>18/04/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Dott. ANGELO &#13;
 DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA &#13;
 REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 53, primo  &#13;
 comma,  del  r.d.l.  4  ottobre  1935,  n.  1827   (Perfezionamento   e  &#13;
 coordinamento  legislativo  della  previdenza  sociale),  promosso  con  &#13;
 ordinanza emessa il 10 novembre  1971  dal  tribunale  di  Ravenna  nel  &#13;
 procedimento civile vertente tra Missiroli Nello e l'Istituto nazionale  &#13;
 della  previdenza sociale, iscritta al n. 9 del registro ordinanze 1972  &#13;
 e pubblicata nella Gazzetta Ufficiale della Repubblica  n.  50  del  23  &#13;
 febbraio 1972.                                                           &#13;
     Visti l'atto d'intervento del Presidente del Consiglio dei ministri  &#13;
 e di costituzione dell'Istituto nazionale della previdenza sociale;      &#13;
     udito nell'udienza pubblica del 6 febbraio 1974 il Giudice relatore  &#13;
 Ercole Rocchetti;                                                        &#13;
     uditi gli avvocati Antonio Giorgi e Sergio Traverso, per l'INPS, ed  &#13;
 il  sostituto  avvocato  generale  dello  Stato  Renato  Carafa, per il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza emessa il 10  novembre  1971,  nel  procedimento  &#13;
 civile  vertente  tra  Missiroli  Nello  e  l'Istituto  nazionale della  &#13;
 previdenza sociale, il tribunale di Ravenna ha  proposto  questione  di  &#13;
 legittimità  costituzionale  dell'art.  53,  primo comma, del r.d.l. 4  &#13;
 ottobre 1935, n.  1827,  con  riferimento  agli  artt.  3  e  23  della  &#13;
 Costituzione.                                                            &#13;
     Secondo  il  giudice  a quo, la norma impugnata, nella parte in cui  &#13;
 prevede, nel caso di omesso  o  irregolare  versamento  dei  contributi  &#13;
 previdenziali,   la   facoltà  assolutamente  discrezionale  dell'ente  &#13;
 assicuratore di rinunciare al pagamento della somma aggiuntiva prevista  &#13;
 dall'art. 111, n. 2, del decreto n. 1827 del  1935,  contrasta  con  il  &#13;
 principio  di  eguaglianza e con il principio della riserva di legge in  &#13;
 materia di prestazioni imposte.                                          &#13;
     Il  tribunale  di Ravenna, dopo essersi richiamato all'orientamento  &#13;
 della Corte costituzionale, che ha  affermato  la  natura  di  sanzione  &#13;
 amministrativa  della somma aggiuntiva prevista dal citato art. 111, n.  &#13;
 2, ritiene che il reale contenuto della norma impugnata non sia  quello  &#13;
 di  attribuire  una  scelta  all'istituto assicuratore tra due forme di  &#13;
 risarcimento  (pagamento  della  somma  aggiuntiva  e  pagamento  degli  &#13;
 interessi  di  mora,  di cui all'art. 53), ma quello di attribuirgli la  &#13;
 potestà  di  rinunciare  ad  libitum  all'applicazione  di  una   pena  &#13;
 amministrativa.                                                          &#13;
     L'ordinanza   è   stata   ritualmente   notificata,  comunicata  e  &#13;
 pubblicata.                                                              &#13;
     2. - Nel giudizio dinanzi alla Corte è intervenuto  il  Presidente  &#13;
 del  Consiglio  dei  ministri,  a  mezzo dell'Avvocatura generale dello  &#13;
 Stato,  e  si  è  costituito  l'Istituto  nazionale  della  previdenza  &#13;
 sociale,  i  quali  chiedono  che  la dedotta questione di legittimità  &#13;
 costituzionale sia dichiarata infondata.                                 &#13;
     La  difesa  dello  Stato,  dopo  aver  ricordato   che   la   Corte  &#13;
 costituzionale,  con  sentenza n. 76 del 1966, ha tra l'altro affermato  &#13;
 la legittimità costituzionale degli artt.  111  e  112  del  r.d.l.  4  &#13;
 ottobre  1935,  n.    1827,  osserva  che  la  questione  sollevata dal  &#13;
 tribunale  di  Ravenna,  relativamente  alla  facoltà   dell'Ente   di  &#13;
 rinunziare  al pagamento della somma aggiuntiva prevista dall'art. 111,  &#13;
 n. 2, in realtà investe proprio quest'ultima norma e non il denunciato  &#13;
 art. 53, perché quel potere non trova fondamento nella norma impugnata  &#13;
 ma appunto nell'art. 111 del decreto n. 1827 del 1935.                   &#13;
     Tale questione, prosegue l'Avvocatura, è già stata  affrontata  e  &#13;
 risolta dalla Corte con la citata sentenza n. 76 del 1966 e proprio nel  &#13;
 punto in cui si è riconosciuto che la eventuale riduzione della misura  &#13;
 della   somma   aggiuntiva   rientra  nell'ambito  di  una  ragionevole  &#13;
 elasticità volta alla soluzione del conflitto di interessi,  soluzione  &#13;
 che giova sia all'Ente sia alle controparti.                             &#13;
     Anche   la  difesa  dell'Istituto  assicuratore  si  richiama  alla  &#13;
 sentenza n. 76 del 1966, in cui la Corte costituzionale, esaminando  il  &#13;
 problema  delle  sanzioni civili, ha chiarito che, proprio in relazione  &#13;
 alla funzione assegnata  all'INPS,  trova  la  sua  giustificazione  il  &#13;
 potere  dell'Ente  di  valutare  con  una  certa elasticità le diverse  &#13;
 situazioni concrete.   In particolare,  la  circostanza  che  la  legge  &#13;
 consente all'Istituto di sanzionare in modo diverso (con l'applicazione  &#13;
 delle  sanzioni  civili  o  dei  semplici interessi di mora) uno stesso  &#13;
 fatto (e cioè la omissione e il ritardo nel versamento dei contributi)  &#13;
 potrebbe, in linea di fatto, dar luogo a una disparità di  trattamento  &#13;
 tra  due situazioni identiche: ma la possibilità di tali inconvenienti  &#13;
 non giustifica la  dichiarazione  di  illegittimità  della  norma,  ma  &#13;
 bensì  quegli  altri  diversi  rimedi  (ricorsi amministrativi, tutela  &#13;
 giurisdizionale) che, caso per caso, il legislatore ha previsto, oltre,  &#13;
 naturalmente, le  garanzie  che  derivano  dagli  organi  di  controllo  &#13;
 dell'Istituto.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Il tribunale di Ravenna deferisce alla Corte una questione di  &#13;
 costituzionalità concernente l'art. 53,  primo  comma,  del  r.d.l.  4  &#13;
 ottobre  1935,  n.  1827,  sulla  previdenza sociale, articolo il quale  &#13;
 dispone che: "nei casi  di  tardivo  versamento  dei  contributi  delle  &#13;
 assicurazioni   obbligatorie,   per   i   quali  non  si  faccia  luogo  &#13;
 all'applicazione  della  disposizione  di cui al n. 2 dell'art. 111, il  &#13;
 datore di lavoro è tenuto al contemporaneo pagamento  degli  interessi  &#13;
 di  mora,  nella  misura  stabilita  per  l'interesse legale in materia  &#13;
 civile".                                                                 &#13;
     Secondo il giudice a quo, il detto articolo autorizzerebbe  l'INPS,  &#13;
 in  sede  di applicazione di penalità per omesso o ritardato pagamento  &#13;
 dei contributi, ad operare una scelta fra  due  tipi  di  sanzioni:  il  &#13;
 pagamento degli interessi di mora, ovvero quello della somma aggiuntiva  &#13;
 prevista  dall'art. 111 dello stesso decreto e consistente in una somma  &#13;
 pari a quella dei contributi omessi.  E  poiché  tale  scelta  sarebbe  &#13;
 puramente  discrezionale,  l'art.  53,  che concederebbe la facoltà di  &#13;
 compierla, violerebbe sia l'art. 23 che l'art.  3  della  Costituzione.  &#13;
 Ciò  in  quanto  il  detto  art.  53, pur autorizzando una prestazione  &#13;
 imposta, non ne determinerebbe  i  criteri  di  imposizione  e  non  ne  &#13;
 stabilirebbe  i controlli, violando con ciò il principio della riserva  &#13;
 di legge e, per gli arbitrii cui potrebbe dar luogo,  anche  quello  di  &#13;
 eguaglianza.                                                             &#13;
     2 - La questione non è fondata.                                     &#13;
     Risulta  del  tutto  gratuita  e  contraria  alla realtà normativa  &#13;
 l'affermazione che l'art. 53 denunziato autorizzi una scelta fra le due  &#13;
 sanzioni.                                                                &#13;
     In realtà quell'articolo si limita a disporre che, quando  non  si  &#13;
 fa  luogo  all'applicazione  della maggior sanzione del pagamento della  &#13;
 somma aggiuntiva, prevista dall'art. 111, debbono pagarsi gli interessi  &#13;
 di mora. Ma la previsione che concerne l'obbligo di pagare, come quello  &#13;
 di non pagare, la somma aggiuntiva non è contenuta - contrariamente  a  &#13;
 quanto  afferma  il  giudice  a  quo  -  nell'art.  53,  ma  è  bensì  &#13;
 contemplata nel 111, al quale articolo la somma escussa dall'INPS aveva  &#13;
 infatti  riferito,  nel   giudizio   di   merito,   la   questione   di  &#13;
 costituzionalità,  che  lo  stesso  giudice  ha  invece dichiarata non  &#13;
 fondata.                                                                 &#13;
     È infatti nell'art. 111 che si  fa  carico  al  datore  di  lavoro  &#13;
 inadempiente  all'obbligo  del versamento dei contributi, di pagare una  &#13;
 somma aggiuntiva, ma si dispone anche nell'ultimo comma che,  ove  egli  &#13;
 presenti domanda di oblazione, il Comitato esecutivo dell'Istituto può  &#13;
 ridurre  l'importo  di quella somma, e può farlo, non essendo previsto  &#13;
 alcun limite, sino alla sua totale ablazione.                            &#13;
     Deve perciò ritenersi che è nell'art. 111, regolante i poteri del  &#13;
 detto organo dell'Istituto, che deve  rinvenirsi  quello,  in  presenza  &#13;
 dell'inadempimento,  di dar corso o non dar corso, in tutto o in parte,  &#13;
 al pagamento della somma aggiuntiva. E non  è  inutile  ricordare  che  &#13;
 un'analoga  questione di costituzionalità del detto articolo, proposta  &#13;
 in  riferimento  agli  artt.  3,  23,  24,  primo  comma,  e  53  della  &#13;
 Costituzione,  fu  dalla Corte, con sentenza n. 76 del 1966, dichiarata  &#13;
 non fondata.                                                             &#13;
     3. - L'art. 53 denunziato, che  sancisce  l'obbligo  del  pagamento  &#13;
 degli  interessi  di  mora,  non autorizza dunque nessuna scelta, ma ne  &#13;
 presuppone una, fra il doversi o no pagare la somma aggiuntiva, che  è  &#13;
 stata già operata, in forza dell'articolo 111, dal Comitato esecutivo.  &#13;
     E,  ove  si  tenga  conto  che gli interessi di mora sono dovuti in  &#13;
 forza del disposto dell'art. 1124 del codice civile, si concluderà che  &#13;
 l'art.  53  esprime  in  forma  impropriamente  positiva  un   concetto  &#13;
 sostanzialmente  negativo,  e  cioè  che gli interessi di mora (di cui  &#13;
 all'art. 1124 c.c.) non sono dovuti quando si è  tenuti  al  pagamento  &#13;
 (perché  il  Comitato  esecutivo,  in  forza  dell'art.  111, così ha  &#13;
 disposto) della somma aggiuntiva, in qualsiasi misura.                   &#13;
     La norma denunziata  ha  dunque  un  contenuto  diverso  da  quello  &#13;
 attribuitogli  dal  giudice  a  quo  ed  al  quale è stata riferita la  &#13;
 questione di costituzionalità Questa deve pertanto  essere  dichiarata  &#13;
 non fondata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 53, primo comma, del r.d.l. 4  ottobre  1935,  n.  1827,  sul  &#13;
 coordinamento legislativo della previdenza sociale; questione proposta,  &#13;
 con  l'ordinanza  in epigrafe, dal tribunale di Ravenna, in riferimento  &#13;
 agli artt. 23 e 3 della Costituzione.                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 4 aprile 1974.                                &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI.                        &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
