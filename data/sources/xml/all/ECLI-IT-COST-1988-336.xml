<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>336</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:336</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1988</data_decisione>
    <data_deposito>24/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, &#13;
 dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, &#13;
 prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 25 della legge    &#13;
 30 aprile 1969, n. 153 (Revisione degli ordinamenti  pensionistici  e    &#13;
 norme  in  materia  di sicurezza sociale) e 18 della legge 26 ottobre    &#13;
 1957,  n.  1047  (Estensione  dell'assicurazione  per  invalidità  e    &#13;
 vecchiaia  ai  coltivatori  diretti, mezzadri e coloni), promossi con    &#13;
 ordinanze emesse il 2  febbraio  1981  dal  Pretore  di  Genova,  l'8    &#13;
 febbraio  1981  ed  il  23 aprile 1982 dal Pretore di Camerino, il 28    &#13;
 gennaio 1983 dal Pretore di  Oristano  e  il  10  febbraio  1984  dal    &#13;
 Pretore  di  Firenze, iscritte rispettivamente al n. 264 del registro    &#13;
 ordinanze 1981, ai nn. 179 e 385 del registro ordinanze 1982,  al  n.    &#13;
 229  del  registro ordinanze 1983 e al n. 1346 del registro ordinanze    &#13;
 1984 e pubblicate nella Gazzetta Ufficiale della  Repubblica  n.  255    &#13;
 dell'anno 1981, nn. 248 e 310 dell'anno 1982, n. 212 dell'anno 1983 e    &#13;
 n. 119- bis dell'anno 1985;                                              &#13;
    Visti gli atti di costituzione dell'I.N.P.S.;                         &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  i Pretori di Genova (R.O. n. 264/1981), di Camerino    &#13;
 (R.O. nn. 179 e  385/1982),  di  Oristano  (R.O.   n.  229/1983),  di    &#13;
 Firenze   (R.O.   n.   1346/1984),   hanno   sollevato  questione  di    &#13;
 legittimità costituzionale degli artt. 25, primo comma, della  legge    &#13;
 30  aprile  1969  n.  153 e 18, secondo comma, della legge 26 ottobre    &#13;
 1957 n. 1047 nella parte in cui escludono il diritto delle vedove  di    &#13;
 coltivatori  diretti  alla pensione di riversibilità nel caso in cui    &#13;
 l'assicurato abbia maturato il diritto a pensione anteriormente al  1    &#13;
 gennaio  1970  e  la  vedova fruisca di pensione diretta nella stessa    &#13;
 gestione speciale, in riferimento agli artt. 3, 35, 38 Cost.  per  la    &#13;
 disparità  di  trattamento  ai  fini  della  tutela  assicurativa  e    &#13;
 previdenziale tra lavoratori dipendenti e lavoratori autonomi e della    &#13;
 disciplina  del cumulo tra pensioni dirette e pensioni indirette o di    &#13;
 riversibilità;                                                          &#13;
      che,  come già affermato da questa Corte (sent. n. 33/1975), la    &#13;
 diversità dei regimi assicurativi e  previdenziali  previsti  per  i    &#13;
 lavoratori  autonomi  e  i  lavoratori  dipendenti  trova  adeguata e    &#13;
 ragionevole giustificazione nella diversità dei rapporti di lavoro e    &#13;
 che  la  parificazione  e  l'ampliamento delle tutele costituisce una    &#13;
 scelta  di  politica  legislativa   che   postula   una   ragionevole    &#13;
 gradualità in relazione anche alle disponibilità finanziarie e alle    &#13;
 esigenze di bilancio per la spesa e l'aggravio che importa;              &#13;
      che, pertanto, la questione è manifestamente infondata;            &#13;
    Visti gli artt. 26, secondo comma, legge 11 marzo 1953, n. 87 e 9,    &#13;
 secondo comma, delle Norme integrative per  i  giudizi  davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale degli artt. 25 della legge 30 aprile 1969 n. 153 e  18    &#13;
 della  legge  26 ottobre 1957 n. 1047, sollevata, in riferimento agli    &#13;
 artt. 3, 35 e 38 Cost., dai Pretori di Genova, Camerino,  Oristano  e    &#13;
 Firenze con le ordinanze in epigrafe.                                    &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte Costituzionale, Palazzo della Consulta,                            &#13;
 l'11 marzo 1988.                                                         &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 24 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
