<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>22</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:22</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/01/1994</data_decisione>
    <data_deposito>03/02/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 10 e  11  del    &#13;
 d.P.R.  30  giugno  1965, n. 1124 (Testo unico delle disposizioni per    &#13;
 l'assicurazione obbligatoria contro gli infortuni  sul  lavoro  e  le    &#13;
 malattie  professionali),  promosso con ordinanza emessa l'8 febbraio    &#13;
 1993 dal Pretore di  Milano  nel  procedimento  civile  vertente  tra    &#13;
 l'I.N.A.I.L.  e la s.p.a. Industrie Grafiche Vallardi, iscritta al n.    &#13;
 151 del registro ordinanze 1993 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 15, prima serie speciale, dell'anno 1993;            &#13;
    Visti gli atti di  costituzione  dell'I.N.A.I.L.  e  della  s.p.a.    &#13;
 Industria   Grafiche   Vallardi  nonché  l'atto  di  intervento  del    &#13;
 Presidente del Consiglio dei ministri;                                   &#13;
    Udito  nell'udienza  pubblica  dell'11  gennaio  1994  il  Giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
    Uditi l'avvocato Michele Fiscella per la s.p.a. Industrie Grafiche    &#13;
 Vallardi,  l'avvocato  Adriana Pignataro per l'I.N.A.I.L e l'Avvocato    &#13;
 dello Stato  Antonio  Bruno  per  il  Presidente  del  Consiglio  dei    &#13;
 ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Nel corso di un giudizio ordinario, il Pretore di Milano, in    &#13;
 qualità di giudice del lavoro, ha  sollevato,  in  riferimento  agli    &#13;
 artt.  3,  38  e  41  della  Costituzione,  questione di legittimità    &#13;
 costituzionale degli artt. 10 ed 11 del d.P.R.  30  giugno  1965,  n.    &#13;
 1124 (Testo unico delle disposizioni per l'assicurazione obbligatoria    &#13;
 contro  gli  infortuni sul lavoro e le malattie professionali), nella    &#13;
 parte in cui, nell'ipotesi di  più  infortuni  occorsi  al  medesimo    &#13;
 lavoratore  e  verificatisi  nella  pendenza  di  rapporti  di lavoro    &#13;
 succedutisi  nel  tempo,  impongono  all'I.N.A.I.L.   di   esercitare    &#13;
 l'azione  di  regresso nei confronti dell'ultimo datore di lavoro per    &#13;
 la quota-parte di invalidità ad esso  imputabile  penalmente,  anche    &#13;
 quando  la  quota-parte  stessa,  che  di  per  sé  sola non sarebbe    &#13;
 sufficiente a raggiungere la soglia di indennizzabilità del  10  per    &#13;
 cento, lo diviene in forza dei precedenti infortuni.                     &#13;
    Rileva  il  giudice a quo che nel procedimento da cui ha presso le    &#13;
 mosse la presente questione, il ricorrente I.N.A.I.L. aveva  esperito    &#13;
 l'azione  di  regresso,  prevista  dalle  predette  disposizioni, nei    &#13;
 confronti della Società Industrie Grafiche  Vallardi,  in  relazione    &#13;
 alla  rendita  erogata  a tale Ferrari Bruno, dipendente della stessa    &#13;
 società, in conseguenza dell'infortunio da questi subito.               &#13;
    Per l'evento dannoso di cui  sopra,  il  Ferrari  aveva  riportato    &#13;
 un'invalidità  quantificata nella misura dell'8 per cento e, quindi,    &#13;
 inferiore alla soglia di  indennizzabilità   stabilita   dalla  legge    &#13;
 nella  misura  del  10  per  cento.  Tuttavia, precedentemente a tale    &#13;
 infortunio, il medesimo lavoratore ne aveva riportato un  altro,  per    &#13;
 il  quale  gli  era  stata  riconosciuta un'invalidità pari al 6 per    &#13;
 cento.                                                                   &#13;
    Su tali premesse, l'I.N.A.I.L. perveniva ad una determinazione, in    &#13;
 favore del Ferrari, del  14  per  cento  di  invalidità,  rendendolo    &#13;
 beneficiario della corrispondente rendita. Esperiva, quindi, l'azione    &#13;
 di  regresso  per  il  valore  capitale  della rendita corrisposta al    &#13;
 lavoratore, facendola  valere  contro  il  responsabile  del  secondo    &#13;
 infortunio.  Nel relativo giudizio, il Pretore di Milano ha sollevato    &#13;
 d'ufficio incidente di costituzionalità.                                &#13;
    In punto di  rilevanza,  il  giudice  rimettente  osserva  che  il    &#13;
 processo  non  può concludersi se non con l'applicazione delle norme    &#13;
 della cui legittimità costituzionale  si  dubita;  né  può  essere    &#13;
 accolta   l'eccezione  di  prescrizione,  avanzata  dalla  resistente    &#13;
 Società Vallardi, essendo intervenuti tempestivi atti interruttivi.     &#13;
    In punto di non  manifesta  infondatezza,  il  giudice  rimettente    &#13;
 ritiene  non  giustificato un accollo al datore di lavoro di un onere    &#13;
 che non troverebbe fondamento né in una responsabilità  per  colpa,    &#13;
 né nella tutela dell'invalido, né nell'utilità sociale. Nella specie,  infatti,  l'azione  di  regresso viene esercitata limitatamente    &#13;
 all'8 per cento dell'invalidità, addebitando l'evento al  datore  di    &#13;
 lavoro  presso la cui impresa si è verificato il secondo infortunio,    &#13;
 anche nel caso in cui la responsabilità per l'infortunio  precedente    &#13;
 non fosse addebitabile al medesimo datore di lavoro.                     &#13;
    Sostiene inoltre il giudice a quo che, se è vero che l'I.N.A.I.L.    &#13;
 ha esperito l'azione soltanto limitatamente alla quota di invalidità    &#13;
 pari  all'8  per  cento,  e  cioè per la quota-parte imputabile alla    &#13;
 resistente, è pur vero  che  solo  aggiungendo  l'altra  quota-parte    &#13;
 relativa    al    primo   infortunio   si   supera   la   soglia   di    &#13;
 indennizzabilità, stabilita dalla legge  nella  misura  del  10  per    &#13;
 cento,  ed  al  di sotto della quale l'azione di regresso non avrebbe    &#13;
 potuto essere esperita. Il che, ad  avviso  del  giudice  rimettente,    &#13;
 comporta  che  il  secondo datore di lavoro finisce con il rispondere    &#13;
 senza titolo, e, nel caso che il primo infortunio fosse  addebitabile    &#13;
 ad  altro  datore  di  lavoro,  il secondo risponderebbe per un fatto    &#13;
 altrui.                                                                  &#13;
    Conclude il giudice ritenendo che l'unica  soluzione  possibile  e    &#13;
 costituzionalmente  legittima  è  quella  che  escluda  l'azione  di    &#13;
 regresso da parte dell'I.N.A.I.L. nelle ipotesi in cui il  datore  di    &#13;
 lavoro   sia   responsabile   di  un  infortunio  cui  è  conseguita    &#13;
 un'invalidità specifica inferiore al 10 per cento.                      &#13;
    2.  -  Nel  giudizio  dinanzi  alla   Corte   si   è   costituito    &#13;
 l'I.N.A.I.L.,   concludendo   per  la  manifesta  infondatezza  della    &#13;
 questione.                                                               &#13;
    Nel motivare tale richiesta, la difesa  dell'I.N.A.I.L.  sostiene,    &#13;
 in  primo  luogo, che il datore di lavoro risponde in via di regresso    &#13;
 ex artt. 10 e 11 del d.P.R. n.  1124  del  1965  soltanto  in  quanto    &#13;
 risulti penalmente responsabile.                                         &#13;
    Detta  responsabilità  è  poi limitata alla entità del danno da    &#13;
 lui medesimo causato, e non anche dell'infortunio  causato  da  fatto    &#13;
 (illecito)  di chi era datore di lavoro nell'ipotesi di un infortunio    &#13;
 precedente.                                                              &#13;
    Occorre  inoltre  tener   presente,   ad   avviso   della   difesa    &#13;
 dell'I.N.A.I.L.,  che  l'azione  di  regresso  di cui si discute può    &#13;
 essere esercitata soltanto quando l'infortunio sia indennizzato;  non    &#13;
 potrebbe invece interessare il datore di lavoro qualora l'infortunato    &#13;
 abbia  avuto  precedenti  eventi  lesivi  di  natura  lavorativa  non    &#13;
 indennizzati in quanto inferiori al minimo.                              &#13;
    Allo stesso modo, risulterebbe indifferente che sia stata  o  meno    &#13;
 esperita  azione  di  regresso nei confronti del precedente datore di    &#13;
 lavoro, in quanto tutti i datori di lavoro rispondono unicamente  del    &#13;
 fatto  proprio  e  nei  limiti di questo, sempre che l'infortunio sia    &#13;
 stato indennizzato dall'I.N.A.I.L.; e ciò  nel  pieno  rispetto  del    &#13;
 principio d'eguaglianza di cui all'art. 3 della Costituzione.            &#13;
    Neppure sarebbero violati gli artt. 38 e 41 della Costituzione, in    &#13;
 quanto    l'infortunato   riceverebbe   piena   tutela   assicurativa    &#13;
 indipendentemente dall'azione di regresso dell'I.N.A.I.L.                &#13;
    3. -  Si  è  costituita  anche  la  Società  Industrie  Grafiche    &#13;
 Vallardi,  concludendo  per l'accoglimento della questione sulla base    &#13;
 di argomentazioni già contenute nell'ordinanza di rimessione.           &#13;
    In particolare, la difesa della Società sostiene che gli artt. 10    &#13;
 ed 11 del d.P.R. n. 1124 del 1965 violano gli artt. 3, 38 e 41  della    &#13;
 Costituzione,  in  quanto,  per un medesimo infortunio, si verrebbe a    &#13;
 creare una disparità di trattamento tra imprenditori e tra questi  e    &#13;
 gli  organi preposti alla tutela del cittadino lavoratore che subisca    &#13;
 un infortunio,  con  il  rischio  di  realizzare  per  tale  via  una    &#13;
 discriminazione anche tra lavoratori.                                    &#13;
    4.  -  È  intervenuto  il  Presidente del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 concludendo  per  la  inammissibilità ovvero la non fondatezza della    &#13;
 questione.                                                               &#13;
    5.  -  In prossimità dell'udienza, gli intervenienti I.N.A.I.L. e    &#13;
 Presidente del Consiglio hanno presentato ulteriori memorie.<diritto>Considerato in diritto</diritto>1. - Il Pretore di Milano, in qualità di giudice del  lavoro,  ha    &#13;
 sollevato,  in  riferimento agli artt. 3, 38 e 41 della Costituzione,    &#13;
 questione di legittimità costituzionale degli artt.  10  ed  11  del    &#13;
 d.P.R.  30  giugno  1965, n. 1124 (Testo unico delle disposizioni per    &#13;
 l'assicurazione obbligatoria contro gli infortuni  sul  lavoro  e  le    &#13;
 malattie  professionali),  nella  parte  in cui, nell'ipotesi di più    &#13;
 infortuni  occorsi  al  medesimo  lavoratore  e  verificatisi   nella    &#13;
 pendenza  di  rapporti  di  lavoro  succedutisi  nel tempo, impongono    &#13;
 all'I.N.A.I.L. di  esercitare  l'azione  di  regresso  nei  confronti    &#13;
 dell'ultimo  datore  di  lavoro  per la quota-parte di invalidità ad    &#13;
 esso imputabile penalmente, anche quando la quota-parte  stessa,  che    &#13;
 di  per  sé  sola non sarebbe sufficiente a raggiungere la soglia di    &#13;
 indennizzabilità  del  10  per  cento,  lo  diviene  in  forza   dei    &#13;
 precedenti infortuni.                                                    &#13;
    La   questione  sollevata  d'ufficio  dal  Pretore  di  Milano  è    &#13;
 rilevante poiché detto giudice, chiamato a decidere sulla domanda di    &#13;
 regresso  esercitata  dall'I.N.A.I.L.,  deve  in   ogni   caso   fare    &#13;
 applicazione delle norme di cui sospetta l'incostituzionalità. Ma la    &#13;
 questione non è fondata.                                                &#13;
    In  sintesi,  il  giudice  a  quo ritiene che, essendo presupposto    &#13;
 dell'azione   di   regresso   il   superamento   della   soglia    di    &#13;
 indennizzabilità del 10 per cento di invalidità del lavoratore, ove    &#13;
 tale  soglia  sia superata in base alla sommatoria di più infortuni,    &#13;
 la rivalsa nei confronti dei singoli datori di lavoro in  proporzione    &#13;
 delle  rispettive  colpe  per  i  diversi  infortuni  non  troverebbe    &#13;
 legittimo fondamento costituzionale né in  una  responsabilità  per    &#13;
 colpa (determinando peraltro un trattamento deteriore per i datori di    &#13;
 lavoro  i quali assumono lavoratori che hanno già subìto precedenti    &#13;
 infortuni per colpa di altri), né nella tutela  dell'invalido  (art.    &#13;
 38  Cost.),  né  nell'utilità  sociale  (art.  41 Cost.). Nel caso,    &#13;
 quindi, in cui ciascuno dei più infortuni subi'ti dal lavoratore non    &#13;
 superi la predetta soglia di indennizzabilità,  secondo  il  Pretore    &#13;
 rimettente,  "l'unica ipotesi possibile è che l'I.N.A.I.L. eroghi le    &#13;
 prestazioni in favore del lavoratore, ma poi non possa recuperare  in    &#13;
 sede di rivalsa la somma sborsata".                                      &#13;
    L'eccezione  di  incostituzionalità  non  è  condivisibile sotto    &#13;
 diversi  profili,  ma  soprattutto  per  il  motivo  che  il  sistema    &#13;
 disciplinato  dal testo unico dell'assicurazione contro gli infortuni    &#13;
 sul lavoro (d.P.R. n. 1124 del 1965), per  un  verso,  nel  prevedere    &#13;
 (art.  74)  l'obbligo  dell'I.N.A.I.L. di indennizzare le conseguenze    &#13;
 degli infortuni quando il  lavoratore  abbia  subito  una  inabilità    &#13;
 superiore  al  10  per  cento,  giustamente  non  distingue  se  tale    &#13;
 percentuale sia stata superata a seguito di uno o più infortuni; per    &#13;
 altro verso, prevede (art. 11) che l'istituto ha "diritto di regresso    &#13;
 per le somme pagate a titolo di indennità e per le spese  accessorie    &#13;
 contro le persone civilmente responsabili", ma non limita l'esercizio    &#13;
 di  tale  diritto  alla sola ipotesi che, nel caso di più infortuni,    &#13;
 ciascuno di questi abbia determinato un'invalidità superiore  al  10    &#13;
 per cento.                                                               &#13;
    Inoltre,   l'art.  10  del  citato  testo  unico  stabilisce  che,    &#13;
 nonostante  l'assicurazione,  permane  la  responsabilità  civile  a    &#13;
 carico  di coloro cui sia imputabile l'infortunio, senza condizionare    &#13;
 tale responsabilità nei confronti dell'I.N.A.I.L. al  superamento  -    &#13;
 per  ogni  singolo  infortunio - di un certo grado di invalidità del    &#13;
 lavoratore,  e  senza  escludere  la  responsabilità  stessa  quando    &#13;
 l'infortunio  non  sia quello verificatosi per primo. Anzi, lo stesso    &#13;
 testo unico (art. 80, terzo comma) contempla espressamente  l'ipotesi    &#13;
 della  indennizzabilità dell'inabilità permanente quando, a seguito    &#13;
 di più infortuni, essa superi "complessivamente" la percentuale  del    &#13;
 10 per cento.                                                            &#13;
    Il  sistema  assicurativo  previsto,  quindi,  se limita l'obbligo    &#13;
 dell'istituto di indennizzare alla sola ipotesi in cui il  lavoratore    &#13;
 si   trovi   in   uno   stato   di   inabilità,   oggettivamente   e    &#13;
 complessivamente, accertato nella misura superiore al 10  per  cento,    &#13;
 riconosce  il  diritto-dovere dell'istituto stesso di rivalersi delle    &#13;
 somme pagate nei confronti delle persone civilmente responsabili, e -    &#13;
 ove  vi  siano  pluralità  di  colpevoli  -  in  proporzione   delle    &#13;
 rispettive responsabilità.                                              &#13;
    E  ciò  a prescindere dalla considerazione che, nella specie, pur    &#13;
 essendosi verificati entrambi gli infortuni durante  il  rapporto  di    &#13;
 lavoro  con  lo  stesso  imprenditore,  l'istituto  abbia  chiesto in    &#13;
 rivalsa solo le somme relative al secondo infortunio.                    &#13;
    Da quanto finora esposto risulta inesatto quanto assume il giudice    &#13;
 a quo, che "il secondo datore  di  lavoro  finirebbe  col  rispondere    &#13;
 senza  titolo,  e nel caso che il primo infortunio fosse addebitabile    &#13;
 ad un altro datore di  lavoro  finirebbe  col  rispondere  per  fatto    &#13;
 altrui".   Né  appare  decisivo  per  la  soluzione  della  presente    &#13;
 questione rilevare, come fa l'ordinanza di rimessione, le  differenze    &#13;
 fra l'azione di regresso prevista dal testo unico sugli infortuni sul    &#13;
 lavoro  e  l'azione di surroga ex art. 1916 del codice civile; e ciò    &#13;
 considerando anche che il limite previsto  dall'art.  74  del  citato    &#13;
 testo  unico - come si è notato - non riguarda le responsabilità di    &#13;
 cui agli artt. 10 e 11 dello stesso d.P.R.,  e  che  ogni  datore  di    &#13;
 lavoro,    chiamato    in   regresso   proporzionalmente   alla   sua    &#13;
 responsabilità, può comunque evitare  di  corrispondere  la  stessa    &#13;
 somma,  una  volta direttamente al lavoratore ed una seconda volta in    &#13;
 via  di  regresso   all'Istituto.   Tanto   meno   merita   specifica    &#13;
 confutazione l'affermazione dell'ordinanza di rimessione, secondo cui    &#13;
 "sul  piano  sostanziale importa che se il lavoratore bene o male non    &#13;
 esperisce l'azione sua propria, il datore di lavoro vede  alleggerita    &#13;
 la sua posizione".                                                       &#13;
    Considerato  pertanto che ciascun datore di lavoro deve rispondere    &#13;
 una sola volta delle conseguenze dell'infortunio a lui imputabile, ed    &#13;
 in proporzione dell'inabilità da  esso  derivante,  può  affermarsi    &#13;
 conclusivamente  che  non  è  ravvisabile  disparità di trattamento    &#13;
 normativo tra il datore di lavoro che risponde del  primo  infortunio    &#13;
 da cui derivi una inabilità superiore al 10 per cento ed i datori di    &#13;
 lavoro  che rispondono proporzionalmente di diversi infortuni, le cui    &#13;
 conseguenze  dannose  siano  complessivamente   superiori   a   detta    &#13;
 percentuale, se tra loro sommate.                                        &#13;
    Parimenti  devono  ritenersi  non fondate le censure di violazione    &#13;
 degli  artt.  38  e  41  della  Costituzione,  dal  momento  che,  al    &#13;
 contrario,  solo  l'interpretazione di detto sistema assicurativo qui    &#13;
 accolta soddisfa  sia  l'esigenza  costituzionale  della  tutela  dei    &#13;
 lavoratori  sia  quella  della  utilità  sociale  nello  svolgimento    &#13;
 dell'iniziativa economica. Ed invero,  recuperando  le  somme  anticipate,  l'I.N.A.I.L. è in condizione di continuare ad indennizzare i    &#13;
 lavoratori infortunati quando il datore di lavoro non provvede subito    &#13;
 o non è coperto da assicurazione,  così  salvaguardandosi  anche  i    &#13;
 limiti entro i quali può esercitarsi l'iniziativa economica privata,    &#13;
 individuati   dalla   Costituzione   nell'utilità  sociale  e  nella    &#13;
 sicurezza umana.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 degli  artt. 10 ed 11 del d.P.R. 30 giugno 1965, n. 1124 (Testo unico    &#13;
 delle  disposizioni  per  l'assicurazione  obbligatoria  contro   gli    &#13;
 infortuni  sul  lavoro  e  le  malattie  professionali) sollevata, in    &#13;
 riferimento agli artt. 3, 38 e 41 della Costituzione, dal Pretore  di    &#13;
 Milano con l'ordinanza indicata in epigrafe.                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 gennaio 1994.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: SANTOSUOSSO                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 3 febbraio 1994.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
