<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>105</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:105</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/06/1973</data_decisione>
    <data_deposito>05/07/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZI - Dott. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  580  del  &#13;
 codice  civile,  promosso  con  ordinanza  emessa il 13 aprile 1971 dal  &#13;
 tribunale di Messina  nel  procedimento  civile  vertente  tra  Fiorino  &#13;
 Giuseppe  ed  altri e Orlando Concetta ed altri, iscritta al n. 482 del  &#13;
 registro ordinanze 1971 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 37 del 9 febbraio 1972.                                    &#13;
     Udito  nella  camera  di  consiglio  del  17 maggio 1973 il Giudice  &#13;
 relatore Giovanni Battista Benedetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con citazione notificata il 27 dicembre  1962  Fiorino  Giuseppe  e  &#13;
 Gioacchino  fu  Carmelo  e  Fiorino  Pietro e Carmelo, figli dell'altro  &#13;
 germano defunto Bartolo, convenivano in giudizio davanti  al  tribunale  &#13;
 di  Messina  Orlando  Concetta, nella sua qualità di madre e legittima  &#13;
 rappresentante dei figli minori Orlando Ida Maria, Virginia, Carmela  e  &#13;
 Carmelo,  nonché  Filippini  Rosaria,  nella  sua  qualità  di  erede  &#13;
 universale testamentaria di Murale Eugenia chiedendo: la  dichiarazione  &#13;
 della  nullità  del  testamento,  che  si pretendeva olografo, dell'11  &#13;
 novembre  1952  di  Fiorino  Tommaso,  defunto  il  10  gennaio   1953,  &#13;
 l'apertura  della  successione  per  legge  in favore degli attori e di  &#13;
 Murale Eugenia e una nuova perizia tecnica  per  accertare  l'olografia  &#13;
 del testamento dianzi menzionato qualora non fosse ritenuta sufficiente  &#13;
 l'efficacia  delle  prove  raccolte in un precedente giudizio, per fare  &#13;
 dichiarare la nullità del testamento, successivamente estinto.          &#13;
     Esperiti  gli  opportuni  mezzi  istruttori,   con   sentenza   non  &#13;
 definitiva  del  17  maggio  1966  il  tribunale  dichiarava  nullo  il  &#13;
 testamento olografo di Fiorino Tommaso; aperta, quindi, la  successione  &#13;
 per legge in favore dei fratelli germani Fiorino Giuseppe, Gioacchino e  &#13;
 Bartolo  nonché della vedova Murale Eugenia, dichiarava che a costei e  &#13;
 per essa alla sua erede Filippini Rosaria spettava in proprietà  metà  &#13;
 del patrimonio ereditario; dichiarava inoltre che ai minori Orlando Ida  &#13;
 Maria,   Virginia,   Carmela   e  Carmelo,  quali  figli  naturali  non  &#13;
 riconoscibili di Fiorino Tommaso, spettava un assegno vitalizio, per la  &#13;
 cui determinazione, rinviava, con separata ordinanza, le parti  davanti  &#13;
 all'istruttore.                                                          &#13;
     Esperita  la  consulenza tecnica per la determinazione dell'assegno  &#13;
 vitalizio, il g.i. rimetteva le parti davanti al  Collegio,  il  quale,  &#13;
 con  ordinanza  in  data 13 aprile 1971, ha ritenuto di dover sollevare  &#13;
 d'ufficio la questione di legittimità costituzionale dell'art. 580 del  &#13;
 codice  civile  per  violazione  dell'art.  3,   comma   primo,   della  &#13;
 Costituzione.                                                            &#13;
     Secondo  il  giudice  a  quo  la norma impugnata non considera come  &#13;
 eredi bensì come semplici legatari i figli naturali non riconosciuti o  &#13;
 non riconoscibili giacché ad essi attribuisce, nel caso di successione  &#13;
 legittima, soltanto un assegno alimentare proporzionato  alle  sostanze  &#13;
 ereditarie  e al numero e qualità degli eredi e comunque non superiore  &#13;
 all'ammontare  della  rendita  della  quota  a  cui  i  figli  naturali  &#13;
 avrebbero   diritto   se   la   filiazione  fosse  stata  dichiarata  o  &#13;
 riconosciuta.                                                            &#13;
     Questa  grave  limitazione  della  loro  capacità   di   succedere  &#13;
 contrasterebbe  col  principio  di  uguaglianza  enunciato  dal  citato  &#13;
 precetto costituzionale  poiché  a  questi  figli  naturali  viene  in  &#13;
 sostanza riservato un trattamento giuridico deteriore rispetto a quello  &#13;
 di  altri soggetti (ascendenti, collaterali) i quali, non facendo parte  &#13;
 della famiglia legittima, devono essere considerati degli  estranei.  A  &#13;
 questi  soggetti,  come  pure ai parenti entro il sesto grado, la legge  &#13;
 riconosce una piena capacità di succedere, sia  come  eredi  che  come  &#13;
 legatari;  per i figli non riconosciuti o non riconoscibili, invece, la  &#13;
 capacità di succedere è limitata non potendo ricevere una  quota  del  &#13;
 patrimonio  del  genitore  naturale  defunto  né in proprietà, né in  &#13;
 usufrutto.                                                               &#13;
     Detta  disparità   di   trattamento   non   troverebbe   razionale  &#13;
 giustificazione  nel  contenuto  e  nelle finalità dell'istituto della  &#13;
 successione legittima, anzi sarebbe in aperto contrasto con essi  posto  &#13;
 che  il  criterio  cui  la  legge  si  ispira  nella determinazione dei  &#13;
 successibili ex legge e delle rispettive  quote  ereditarie  è  quello  &#13;
 della intensità del vincolo che unisce i vari congiunti al defunto: un  &#13;
 criterio,  quindi,  che ha come fondamento la solidarietà familiare. E  &#13;
 non è dubbio che il vincolo di sangue che unisce i figli naturali  non  &#13;
 riconosciuti  o  non  riconoscibili al genitore sia almeno pari, se non  &#13;
 superiore, al  vincolo  che  unisce  altre  categorie  di  successibili  &#13;
 (ascendenti, collaterali, coniuge e parenti entro il sesto grado) al de  &#13;
 cuius stesso.                                                            &#13;
     Sulla  base  delle  considerazioni  anzidette  e alla stregua delle  &#13;
 statuizioni contenute in precedenti decisioni della Corte (sentenze nn.  &#13;
 205 del 1970; 54 del 1960; 79 del 1969; 126 del 1968 e 147 del 1969) il  &#13;
 tribunale  ha  ritenuto  non  manifestamente  infondata   la   proposta  &#13;
 questione.                                                               &#13;
     Nel presente giudizio nessuno si è costituito.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  La  questione  di  legittimità costituzionale sollevata dal  &#13;
 tribunale di Messina con l'ordinanza del 13 aprile 1971 ha  ad  oggetto  &#13;
 l'art.  580  del codice civile ritenuto costituzionalmente illegittimo,  &#13;
 per violazione del principio  di  uguaglianza  enunciato  dall'art.  3,  &#13;
 comma  primo,  della  Costituzione.  Si  assume che la norma impugnata,  &#13;
 attribuendo ai figli naturali non riconosciuti o non riconoscibili, nel  &#13;
 caso  di  successione  legittima,  soltanto   un   assegno   alimentare  &#13;
 proporzionato  alle  sostanze  ereditarie  e al numero e qualità degli  &#13;
 eredi, comporterebbe una grave limitazione della capacità di succedere  &#13;
 di  tali  soggetti,  non  considerati  come eredi, bensì come semplici  &#13;
 legatari; ad essi in sostanza il  codice  riserberebbe  un  trattamento  &#13;
 giuridico  deteriore  rispetto  a  quello  assicurato agli ascendenti e  &#13;
 collaterali che, non facendo parte  della  famiglia  legittima,  devono  &#13;
 essere considerati degli estranei.                                       &#13;
     2.  -  Non  è dato alla Corte scendere all'esame di tale questione  &#13;
 dovendosene, in via preliminare, rilevare l'evidente inammissibilità.   &#13;
     Come si desume dal testo dell'ordinanza di rinvio, il giudice a quo  &#13;
 ha proposto l'eccezione di incostituzionalità dopo aver già  statuito  &#13;
 in  ordine  alla  applicazione  della  norma ora impugnata nel giudizio  &#13;
 dinanzi ad esso pendente. Ed, invero, con sentenza 17 maggio  1966,  il  &#13;
 tribunale,  pronunciando  non  definitivamente  sulle  domande proposte  &#13;
 dalle parti in causa, dichiarò, tra  l'altro,  aperta  la  successione  &#13;
 legittima  di  Fiorino  Tommaso, deceduto il 10 gennaio 1953, in favore  &#13;
 dei fratelli germani dello stesso e della vedova e dichiarò, altresì,  &#13;
 che ai minori figli naturali  non  riconoscibili  di  Fiorino  Tommaso,  &#13;
 rappresentati   dalla   madre  Orlando  Concetta,  "spetta  un  assegno  &#13;
 vitalizio per la cui determinazione sarà provveduto  con  la  sentenza  &#13;
 definitiva".                                                             &#13;
     In  puntuale  adempimento  di quest'ultima parte della decisione il  &#13;
 giudice a quo, con separata ordinanza di pari data, dispose  la  nomina  &#13;
 di un consulente tecnico per far valutare la consistenza del patrimonio  &#13;
 del de cuius.                                                            &#13;
     Da  quel che precede risulta quindi chiaro che la statuizione della  &#13;
 sentenza relativa al diritto all'assegno vitalizio spettante  ai  figli  &#13;
 naturali  non riconoscibili è preclusiva dell'esame della questione di  &#13;
 legittimità costituzionale successivamente sollevata,  non  ricorrendo  &#13;
 più  il requisito della necessaria pregiudizialità per la definizione  &#13;
 del giudizio richiesto dall'art. 23 della legge 11 marzo 1953, n. 87.    &#13;
     Nessuna efficacia potrebbe, invero, spiegare l'eventuale  pronuncia  &#13;
 della  Corte  nella  controversia  ora  all'esame  del  giudice  a quo,  &#13;
 giacché proprio sullo specifico punto formante oggetto del  dubbio  di  &#13;
 legittimità  costituzionale  ha  già  statuito, in modo definitivo, -  &#13;
 come dianzi rilevato - il tribunale con la ricordata sua sentenza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile, per difetto di rilevanza, la  questione  di  &#13;
 legittimità  costituzionale dell'art. 580 del codice civile, sollevata  &#13;
 dal tribunale di Messina, con  l'ordinanza  indicata  in  epigrafe,  in  &#13;
 riferimento all'art. 3, comma primo, della Costituzione.                 &#13;
     Così  deciso in Roma, in camera di consiglio, nella sede del Corte  &#13;
 costituzionale, Palazzo della Consulta, il 26 giugno 1973.               &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - LUIGI OGGIONI - ANGELO DE  MARCO  -  &#13;
                                   ERCOLE  ROCCHETTI  - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
