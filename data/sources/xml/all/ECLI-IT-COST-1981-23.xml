<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>23</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:23</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Antonino de Stefano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/02/1981</data_decisione>
    <data_deposito>10/02/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Avv. ALBERTO MALAGUGINI - Prof. LIVIO &#13;
 PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  sull'ammissibilità,  ai  sensi  dell'art.  2, comma  &#13;
 primo, legge cost. 11 marzo 1953 n. 1, della richiesta di    referendum  &#13;
 popolare  per  l'abrogazione  degli  articoli  17,    comma  primo n. 2  &#13;
 (l'ergastolo) e 22 del codice penale   approvato con regio  decreto  19  &#13;
 ottobre 1930 n. 1398 e  successive modificazioni (n. 15 reg. ref.).      &#13;
     Vista  l'ordinanza  2 dicembre 1980 con la quale l'Ufficio centrale  &#13;
 per il  referendum  presso  la  Corte  di  cassazione    ha  dichiarato  &#13;
 legittima la suddetta richiesta;                                         &#13;
     udito,  nella  camera di consiglio del 14 gennaio 1981, il  Giudice  &#13;
 relatore Antonino De Stefano;                                            &#13;
     udito  l'avv.  Mauro  Mellini  per  il   Comitato   promotore   del  &#13;
 referendum.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     L'Ufficio  centrale  per il referendum, costituito presso la  Corte  &#13;
 di cassazione, ha esaminato, in applicazione della    legge  25  maggio  &#13;
 1970,  n.  352, e successive  modificazioni, la richiesta di referendum  &#13;
 popolare,  presentata il 26 giugno 1980 da Rippa Giuseppe,    Cherubini  &#13;
 Laura,  Passeri  Maria  Grazia,  Pergameno Silvio e Vigevano Paolo, sul  &#13;
 seguente quesito: "Volete   voi che siano  abrogati  gli  articoli  17,  &#13;
 comma  primo  n. 2   (l'ergastolo) e 22 del codice penale approvato con  &#13;
 regio     decreto  10  ottobre   1930,   n.      1398,   e   successive  &#13;
 modificazioni?".                                                         &#13;
     Con  ordinanza  del  2  dicembre  1980,  depositata in pari   data,  &#13;
 l'Ufficio centrale ha dato atto che la richiesta è    stata  preceduta  &#13;
 dall'attività  di  promozione conforme ai   requisiti di legge, che è  &#13;
 stata presentata da soggetti che  vi erano legittimati, che il deposito  &#13;
 è avvenuto nel   termine di tre mesi dalla  data  di  vidimazione  dei  &#13;
 fogli,    che  la  richiesta  di abrogazione delle su indicate norme è  &#13;
 stata regolarmente formulata e trascritta nella facciata  contenente le  &#13;
 firme di ciascun foglio, che il numero  definitivo delle sottoscrizioni  &#13;
 regolari supera  quello  di    500.000  voluto  dalla  Costituzione;  e  &#13;
 considerato  che  è    indubbio  il  carattere  legislativo  dell'atto  &#13;
 normativo   sottoposto  a  referendum,  e  che  al  riguardo  non  sono  &#13;
 intervenuti  atti  di  abrogazione,  né  pronunce  di   illegittimità  &#13;
 costituzionale, ha dichiarato legittima la  richiesta anzidetta.         &#13;
     Ricevuta comunicazione dell'ordinanza, il  Presidente  di    questa  &#13;
 Corte ha fissato per la conseguente deliberazione  il giorno 14 gennaio  &#13;
 1981,  dandone  a  sua  volta    comunicazione  ai  presentatori  della  &#13;
 richiesta ed al   Presidente  del  Consiglio  dei  ministri,  ai  sensi  &#13;
 dell'art. 33,  comma secondo, della legge n. 352 del 1970.               &#13;
     In  data  10 gennaio 1981, il Comitato promotore del  referendum in  &#13;
 esame ha presentato una memoria. In essa,  ribadite le riserve riguardo  &#13;
 al criterio della "omogeneità"  dei quesiti, affermato da questa Corte  &#13;
 con la sentenza n.   16 del  1978,  si  osserva  non  potersi  comunque  &#13;
 dubitare    che  la  richiesta di abolizione delle due norme del codice  &#13;
 penale che prevedono, rispettivamente,  la  pena  dell'ergastolo  e  ne  &#13;
 stabiliscono  le  modalità,  dia  luogo  ad    un  quesito "omogeneo".  &#13;
 Rilevato  che  nessuno  degli  altri      principi   limitativi   della  &#13;
 ammissibilità  del  referendum,    enunciati  nella citata sentenza di  &#13;
 questa Corte, viene nel  caso in discussione, nella memoria si aggiunge  &#13;
 che la  soppressione delle norme sull'ergastolo, di cui attraverso   il  &#13;
 referendum  si  propone  l'abrogazione, verrebbe a dare   attuazione al  &#13;
 principio, proclamato dall'art. 27 della   Costituzione,  che  le  pene  &#13;
 debbono  tendere  alla  rieducazione  del  reo:  ad  un fine cioè che,  &#13;
 proiettandosi  oltre la loro durata, presuppone che la esecuzione delle  &#13;
 pene sia limitata nel tempo.                                             &#13;
     Nessuna memoria è stata presentata da parte  dell'Avvocatura dello  &#13;
 Stato.<diritto>Considerato in diritto</diritto>:                          &#13;
     Oggetto della  richiesta  di  referendum  abrogativo,    dichiarata  &#13;
 legittima con ordinanza del 2 dicembre 1980,  in applicazione dell'art.  &#13;
 32  della  legge  25 maggio 1970,  n. 352, dall'Ufficio centrale per il  &#13;
 referendum, costituito    presso  la  Corte  di  cassazione,  sono  gli  &#13;
 articoli  17, comma  primo, n. 2, e 22 del codice penale, approvato con  &#13;
 r.d. 19  ottobre 1930, n. 1398, e successive modificazioni. L'art.  17,  &#13;
 nell'elencare le pene principali, indica, al n. 2  del    primo  comma,  &#13;
 l'ergastolo.  Il  successivo art. 22, come modificato dall'art. 1 della  &#13;
 legge 25 novembre 1962, n.  1634, sancisce che "La pena  dell'ergastolo  &#13;
 è  perpetua,  ed    è  scontata  in  uno  degli  stabilimenti  a ciò  &#13;
 destinati, con  l'obbligo del lavoro e con  l'isolamento  notturno.  Il  &#13;
 condannato all'ergastolo può essere ammesso al lavoro  all'aperto".     &#13;
     La  Corte, in sede di cognizione dell'ammissibilità del referendum  &#13;
 - compito che le è attribuito dall'art. 2 della  legge  costituzionale  &#13;
 11 marzo 1953, n. 1 e dagli artt. 32,  comma secondo, e 33 della citata  &#13;
 legge  n. 352 del 1970 -  non riscontra nella richiesta in esame alcuna  &#13;
 ragione  d'inammissibilità, e pertanto la dichiara ammissibile.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  ammissibile  la  richiesta  di  referendum  popolare  per  &#13;
 l'abrogazione degli articoli 17, comma primo, n. 2  (l'ergastolo), e 22  &#13;
 del  codice  penale,  approvato  con  r.d.   19 ottobre 1930 n. 1398, e  &#13;
 successive modificazioni,   dichiarata legittima con  ordinanza  del  2  &#13;
 dicembre  1980    dall'Ufficio  centrale  per il referendum, costituito  &#13;
 presso  la Corte di cassazione.                                          &#13;
     Così deciso in Roma, in camera di consiglio,  nella  sede    della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 9  febbraio 1981.       &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   -   LEOPOLDO      ELIA   -  GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO   REALE -  ALBERTO  &#13;
                                   MALAGUGINI - LIVIO  PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO    ANDRIOLI    -    GIUSEPPE  &#13;
                                   FERRARI.                               &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
