<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>222</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:222</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/07/1976</data_decisione>
    <data_deposito>03/08/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 330 e 340  &#13;
 del codice penale, promosso con ordinanza emessa il  14  novembre  1972  &#13;
 dal  pretore  di  Tivoli  nel procedimento penale a carico di Meccarini  &#13;
 Giacomo ed altri, iscritta al n. 417  del  registro  ordinanze  1974  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale della Repubblica n.   309 del 27  &#13;
 novembre 1974.                                                           &#13;
     Udito nella camera di consiglio  del  15  giugno  1976  il  Giudice  &#13;
 relatore Ercole Rocchetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza  emessa il 14 novembre 1972 (pervenuta alla Corte in  &#13;
 data 3 ottobre 1974), il pretore di Tivoli  ha  proposto  questione  di  &#13;
 legittimità  costituzionale,  con  riferimento  agli  artt. 3, 39 e 40  &#13;
 della Costituzione, degli  artt.  330  e  340  del  codice  penale  che  &#13;
 contemplano,  rispettivamente,  l'abbandono collettivo e l'interruzione  &#13;
 di un ufficio  o  servizio  pubblico  o  di  un  servizio  di  pubblica  &#13;
 necessità.                                                              &#13;
     Secondo  il  giudice  a  quo,  le norme impugnate, anche nella loro  &#13;
 attuale formulazione a seguito della sentenza  n.  31  del  1969  della  &#13;
 Corte   costituzionale,  negano  ai  dipendenti  di  imprese  esercenti  &#13;
 pubblici servizi o servizi di pubblica necessità, e in particolare  ai  &#13;
 dipendenti degli istituti manicomiali pubblici e privati, il diritto di  &#13;
 sciopero.  Tale  divieto sarebbe assoluto in quanto, alla stregua delle  &#13;
 disposizioni che regolano la attività  e  le  funzioni  del  personale  &#13;
 ospedaliero  manicomiale,  non  potrebbe essere contestata la esistenza  &#13;
 del requisito della essenzialità  globale  dei  servizi  demandati  ai  &#13;
 manicomi.  Da  tale  situazione deriverebbe una "evidente alterazione e  &#13;
 menomazione del principio della parità dei diritti che è fondamentale  &#13;
 in materia sindacale (art. 39), del principio di eguaglianza (art. 3) e  &#13;
 del diritto di sciopero (art. 40), spettante a tutti indistintamente  i  &#13;
 lavoratori,  senza  che  nessuna  legge  regolatrice dello sciopero sia  &#13;
 intervenuta a fissarne limiti, vincoli e condizioni".                    &#13;
     L'ordinanza  è   stata   ritualmente   notificata   comunicata   e  &#13;
 pubblicata.                                                              &#13;
     Dinanzi alla Corte nessuna delle parti si è costituita in giudizio  &#13;
 né  è intervenuto il Presidente del Consiglio dei ministri. La causa,  &#13;
 pertanto, viene decisa in camera di consiglio ai  sensi  dell'art.  26,  &#13;
 secondo comma, della legge 11 marzo 1953, n. 87.<diritto>Considerato in diritto</diritto>:                          &#13;
     Viene  proposta alla Corte questione di legittimità costituzionale  &#13;
 dell'art. 330 del codice penale che contempla l'abbandono collettivo di  &#13;
 pubblici uffici, impieghi. servizi  o  lavori,  e  dell'art.  340,  che  &#13;
 concerne  l'interruzione  di  un  servizio pubblico o di un servizio di  &#13;
 pubblica necessità.                                                     &#13;
     Con sentenza n. 31 del 1969, l'art. 330, nei primi due commi, venne  &#13;
 dichiarato parzialmente illegittimo  "limitatamente  all'applicabilità  &#13;
 allo sciopero economico che non comprometta funzioni o servizi pubblici  &#13;
 essenziali,  aventi carattere di preminente interesse generale ai sensi  &#13;
 della Costituzione".                                                     &#13;
     Veniva così affermato il principio - che non  può  non  importare  &#13;
 riflessi  anche sull'art. 340 - che l'esercizio del diritto di sciopero  &#13;
 è garantito anche se implichi la  interruzione  di  servizi  pubblici,  &#13;
 eccetto  che  non  si  tratti  di  servizi  il cui funzionamento sia da  &#13;
 considerarsi essenziale, e cioè indispensabile, alla collettività.     &#13;
     La individuazione di detti servizi, da ritenersi appunto essenziali  &#13;
 e tali da escludere, o almeno  limitare,  l'esercizio  del  diritto  di  &#13;
 sciopero,  veniva  così  rimessa  ai  giudici,  ad opera dei quali, in  &#13;
 questi ultimi anni, si è formata sul tema una notevole giurisprudenza.  &#13;
     Ma il pretore di Tivoli, chiamato a giudicare per i  reati  di  cui  &#13;
 agli  artt.  330  e  340 codice penale i componenti del personale di un  &#13;
 ospedale psichiatrico che si erano posti  in  sciopero  in  proporzione  &#13;
 tale  da  incidere  pesantemente  sulla  regolarità  dei  servizi,  ha  &#13;
 osservato che in un nosocomio, in cui vengano ospitati e curati  malati  &#13;
 di  mente,  tutti i servizi sono correlati e connessi, tanto da doversi  &#13;
 parlare di una loro essenzialità globale.                               &#13;
     Dal che dovrebbe trarsi la conclusione che,  per  il  personale  di  &#13;
 tali  istituti,  l'esercizio del diritto di sciopero dovrebbe ritenersi  &#13;
 totalmente escluso; conclusione questa che contrasterebbe col  disposto  &#13;
 degli artt. 3, 39 e 40 della Costituzione.                               &#13;
     La questione non è fondata.                                         &#13;
     La  interdipendenza  e  la correlazione tra i servizi costituiscono  &#13;
 l'espressione di un fatto organizzatorio caratteristico di ogni tipo di  &#13;
 comunità, da cui, tuttavia, non può trarsi la conclusione che tutti i  &#13;
 servizi abbiano uguale grado  di  importanza  e  di  indispensabilità.  &#13;
 Certo,  tutti  sono necessari e tra loro in qualche modo complementari,  &#13;
 quando la complessa attività cui dà luogo la vita della comunità  si  &#13;
 svolge in regime di normalità.                                          &#13;
     Ma,   quando   ragioni   di   necessità,   impongono  di  ridurre,  &#13;
 eventualmente anche  al  minimo,  l'appagamento  delle  esigenze  della  &#13;
 collettività  o  di  una  più  ristretta comunità sociale, è sempre  &#13;
 possibile individuare tra i servizi quelli che  debbono  conservare  la  &#13;
 necessaria  efficienza  -  e  che sono poi quelli essenziali - e quelli  &#13;
 suscettibili di essere sospesi o ridotti.                                &#13;
     Orbene,  quando  ricorrono i presupposti perché uno sciopero venga  &#13;
 proclamato ed attuato, quei servizi essenziali debbono essere mantenuti  &#13;
 in  efficienza,  ed  in  nessun  caso  possono  essere  trasgredite  le  &#13;
 specifiche norme inderogabili eventualmente al riguardo esistenti.       &#13;
     Per quanto riguarda la fattispecie presa in esame dal giudice a quo  &#13;
 e  relativa  ad  un istituto destinato al ricovero di malati affetti da  &#13;
 alienazione mentale, ritiene la Corte che particolare attenzione  debba  &#13;
 essere  rivolta  alla  norma contenuta nell'art. 2, quarto comma, della  &#13;
 legge 18 marzo 1968, n. 431, la quale prescrive che "dovrà  essere  in  &#13;
 ogni  caso  assicurato  il rapporto di un infermiere per ogni tre posti  &#13;
 letto e di una assistente sanitaria o  sociale  per  ogni  cento  posti  &#13;
 letto".                                                                  &#13;
     I   rilievi   che   precedono  consentono  pertanto  di  affermare,  &#13;
 contrariamente a quanto assume il giudice a  quo,  che  non  difettano,  &#13;
 nella  disciplina  della  materia,  norme valide a fornire, ai soggetti  &#13;
 interessati prima e all'interprete poi, i criteri atti ad  individuare,  &#13;
 anche  sul  piano  concreto,  quali  servizi  debbano  essere  ritenuti  &#13;
 essenziali e quali esigenze debbano essere in ogni  caso  salvaguardate  &#13;
 dal  personale  addetto  ai  pubblici servizi che intenda avvalersi del  &#13;
 diritto di sciopero.                                                     &#13;
     Dagli stessi rilievi discende che le limitazioni che  tale  diritto  &#13;
 riceve  nella  fattispecie  in  esame,  dirette come sono ad assicurare  &#13;
 interessi  costituzionalmente  protetti  attinenti  alla  tutela  della  &#13;
 integrità  fisica dei malati e di coloro che li assistono, non possono  &#13;
 implicare alcuna violazione degli  artt.  39  e  40  che  quel  diritto  &#13;
 proclamano  e  garantiscono,  nel rispetto dei principi fondamentali di  &#13;
 uno Stato democratico, fondato sul lavoro, ma che, tuttavia,  "richiede  &#13;
 l'adempimento   dei   doveri  inderogabili  di  solidarietà  politica,  &#13;
 economica e sociale" (art. 2 Costituzione).                              &#13;
     Né, per i medesimi motivi, è  configurabile  una  violazione  del  &#13;
 principio  di eguaglianza, dal momento che questo è tutelabile solo in  &#13;
 presenza  di  situazioni  identiche  o  simili  e  non  di   condizioni  &#13;
 oggettivamente e razionalmente differenziate.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondate, nei sensi di cui in motivazione, le questioni  &#13;
 di legittimità costituzionale degli artt. 330 e 340 del codice penale,  &#13;
 proposte,  con  l'ordinanza  indicata  in epigrafe, in riferimento agli  &#13;
 artt. 3, 39 e 40 della Costituzione.                                     &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 15 luglio 1976.         &#13;
                                   F.to:  PAOLO  ROSSI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE ROCCHETTI  -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE STEFANO - LEOPOLDO ELIA.            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
