<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>201</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:201</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VARI</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia>Carlo Mezzanotte</redattore_pronuncia>
    <data_decisione>09/05/2002</data_decisione>
    <data_deposito>16/05/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Massimo VARI; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 204 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso con ordinanza emessa il 31 maggio 2001 dal Giudice di pace di Vercelli, iscritta al n. 692 del registro ordinanze 2001 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2001. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 13 marzo 2002 il Giudice relatore Carlo Mezzanotte. &#13;
    Ritenuto che, nel corso di un giudizio di opposizione ad ordinanza-ingiunzione prefettizia con la quale si intimava il pagamento di una somma a titolo di sanzione amministrativa per violazione di norme del codice della strada, il Giudice di pace di Vercelli, con ordinanza emessa in data 31 maggio 2001, ha sollevato, in riferimento agli articoli 24, 97, 111 e 113 della Costituzione, questione di legittimità costituzionale dell'art. 204 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), “nella parte in cui non prevede un termine entro il quale deve essere notificato il provvedimento prefettizio dell'ordinanza-ingiunzione”; &#13;
    che il remittente, dopo aver riferito che nella specie l'ordinanza prefettizia, emessa in data 21 giugno 2000, era stata notificata all'opponente in data 28 dicembre 2000, e cioè dopo oltre un anno dal deposito del ricorso proposto in data 13 (o 23) dicembre 1999, rileva che la disposizione censurata, nella parte in cui non prevede un termine per la notificazione dell'ordinanza-ingiunzione, sarebbe in contrasto con l'art. 24 della Costituzione, in quanto, consentendo che detta notifica abbia luogo anche “dopo un lungo periodo di tempo” (entro il termine della prescrizione quinquennale decorrente dalla violazione), renderebbe difficoltosa la tutela giurisdizionale del cittadino, a cui non sarebbe consentito apprendere con ragionevole tempestività gli esiti del ricorso; &#13;
    che, ad avviso del giudice a quo, la norma impugnata violerebbe anche l'art. 97 della Costituzione, in quanto determinerebbe “un vuoto temporaneo sia nelle casse dello Stato che in quelle dei singoli comuni”, e contrasterebbe con il principio della ragionevole durata del processo, sancito dall'art. 111 della Costituzione, poiché il procedimento, per quanto riguarda la sua fase amministrativa, potrebbe durare fino a cinque anni e a questi dovrebbero aggiungersi “i tempi di durata per il I grado avanti alla autorità giudiziaria ed un eventuale supplemento di tempo per il ricorso in Cassazione”; &#13;
    che sarebbe, infine, violato l'art. 113 della Costituzione, in quanto, una volta individuati nelle prefetture gli “organi decidenti l'annullamento degli effetti delle applicate sanzioni amministrative”, non sarebbe legittimo consentire che la conoscenza di tali effetti possa verificarsi dopo così lungo tempo, lasciando in sospeso situazioni giuridiche in capo ai ricorrenti; &#13;
    che il remittente, nel sollevare la questione di legittimità costituzionale, prospetta come “giusto ed equo” un termine per la notifica pari a trenta giorni a decorrere dall'emissione dell'ordinanza prefettizia; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, e ha chiesto, in via preliminare, che la questione sia dichiarata inammissibile per difetto di rilevanza, poiché la indeterminatezza di durata del procedimento amministrativo non inciderebbe sulla successiva fase giurisdizionale, se non in senso favorevole all'amministrato; &#13;
    che, ad avviso dell'Avvocatura dello Stato, la questione sarebbe comunque infondata, in quanto il termine per la notifica non spiegherebbe alcun effetto sulla esistenza o sulla validità dell'atto, ma rileverebbe soltanto in riferimento all'attività processuale successiva, determinando decadenze o inammissibilità, in base a scelte insindacabili del legislatore; &#13;
    che, in particolare, la difesa erariale rileva che la mancata previsione di un autonomo termine per la notificazione comporterebbe unicamente che la notifica stessa, per essere efficace, non possa essere effettuata oltre il termine di prescrizione per la riscossione delle somme dovute, con conseguente esclusione di qualunque violazione dell'art. 97 Cost., poiché anche nell'ipotesi in esame sussisterebbe un limite massimo temporale per l'attività dell'amministrazione e sarebbe prevista una sanzione per l'inerzia che dovesse prolungarsi oltre il termine predetto; &#13;
    che - prosegue ancora l'Avvocatura dello Stato - inerzie o ritardi potrebbero solo giovare e mai nuocere all'amministrato, il quale, peraltro, potrebbe sempre ricorrere direttamente al giudice senza il previo esperimento del ricorso al prefetto; &#13;
    che, in ogni caso, i parametri di costituzionalità di cui agli artt. 24, 111 e 113 atterrebbero soltanto al momento giurisdizionale e non sarebbero applicabili ad un procedimento amministrativo quale è quello dinanzi al prefetto. &#13;
    Considerato che il Giudice di pace di Vercelli ha sollevato, in relazione agli articoli 24, 97, 111 e 113 della Costituzione, questione di legittimità costituzionale dell'art. 204 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), “nella parte in cui non prevede un termine entro il quale deve essere notificato il provvedimento prefettizio dell'ordinanza-ingiunzione”; &#13;
    che il remittente riferisce che in data 13 dicembre 1999 era stata proposta opposizione avverso l'ordinanza-ingiunzione emessa il 21 giugno 2000 e notificata il 28 dicembre 2000; &#13;
    che dal complessivo contesto dell'ordinanza di rimessione si intende che in data 13 dicembre 1999 era stato proposto ricorso al prefetto, il quale aveva deciso con l'ordinanza-ingiunzione emessa il 21 giugno 2000 e notificata il 28 dicembre 2000; &#13;
    che sempre dall'ordinanza di rimessione e dagli atti del fascicolo di causa emerge che l'opponente aveva eccepito la tardività del provvedimento prefettizio; &#13;
    che in tale situazione di fatto, per motivare la rilevanza della questione di legittimità costituzionale, lo stesso remittente non avrebbe potuto ignorare che, ai sensi del censurato art. 204, comma 1, del codice della strada, il prefetto è tenuto ad emettere l'ordinanza-ingiunzione entro un termine che originariamente era di sessanta giorni, che l'art. 68, comma 4, della legge 23 dicembre 1999, n. 488 (in vigore dal 1° gennaio 2000) ha portato a centottanta giorni (poi ridotti a novanta ad opera dell'art. 18 della legge 24 novembre 2000, n. 340), e che, secondo un consolidato orientamento giurisprudenziale, la violazione di tale termine rende illegittimo il provvedimento sanzionatorio; &#13;
    che l'omissione di qualsiasi motivazione su questi decisivi elementi di fatto e di diritto rende la questione manifestamente inammissibile. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 204 del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata, in riferimento agli articoli 24, 97, 111 e 113 della Costituzione, dal Giudice di pace di Vercelli con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 maggio 2002. &#13;
F.to: &#13;
Massimo VARI, Presidente &#13;
Carlo MEZZANOTTE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 16 maggio 2002. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
