<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>33</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:33</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>OGGIONI</presidente>
    <relatore_pronuncia>Enzo Capolozza</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/02/1976</data_decisione>
    <data_deposito>19/02/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. LUIGI OGGIONI, Presidente - Avv. &#13;
 ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA &#13;
 REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. EDOARDO &#13;
 VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO &#13;
 DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 409, n. 3,  &#13;
 del codice di procedura civile, nel testo  sostituito  dalla  legge  11  &#13;
 agosto  1973,  n.  533, promosso con ordinanza emessa il 1 ottobre 1974  &#13;
 dal pretore di Savona nella  causa  di  lavoro  vertente  tra  Sardella  &#13;
 Iolanda  e  la  Compagnia  assicuratrice UNIPOL, iscritta al n. 451 del  &#13;
 registro ordinanze 1974 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 324 dell'11 dicembre 1974.                                 &#13;
     Visti  gli  atti  d'intervento  del  Presidente  del  Consiglio dei  &#13;
 ministri e di costituzione della Compagnia assicuratrice UNIPOL;         &#13;
     udito  nell'udienza  pubblica  del  10  dicembre  1975  il  Giudice  &#13;
 relatore Enzo Capalozza;                                                 &#13;
     uditi  l'avv.  Renato Scognamiglio, per la Compagnia assicuratrice,  &#13;
 ed il sostituto avvocato generale dello Stato  Renato  Carafa,  per  il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza 1 ottobre 1974, emessa nel corso del procedimento di  &#13;
 lavoro iniziato da  Jolanda  Sardella  nei  confronti  della  Compagnia  &#13;
 assicuratrice  UNIPOL, il pretore di Savona ha ritenuto rilevante e non  &#13;
 manifestamente infondato, in riferimento all'art. 3 della Costituzione,  &#13;
 il dubbio di legittimità  costituzionale  dell'art.  409,  n.  3,  del  &#13;
 codice  di procedura civile, nel testo sostituito dalla legge 11 agosto  &#13;
 1973, n. 533.                                                            &#13;
     Ad avviso del giudice a quo, la norma denunziata,  sottoponendo  al  &#13;
 rito speciale delle controversie individuali di lavoro i rapporti degli  &#13;
 agenti  che  si  concretino  in  prestazioni prevalentemente personali,  &#13;
 darebbe luogo ad un'ingiusta sperequazione tra essi e le  società  che  &#13;
 hanno ad oggetto il disbrigo degli affari di agenzia.                    &#13;
     Nel  giudizio  dinanzi  a questa Corte si è costituita la società  &#13;
 UNIPOL ed è intervenuto il  Presidente  del  Consiglio  dei  ministri,  &#13;
 rappresentato dall'Avvocatura generale dello Stato.                      &#13;
     La difesa della società, richiamando sostanzialmente gli argomenti  &#13;
 dell'ordinanza,  chiede  che la norma sia dichiarata costituzionalmente  &#13;
 illegittima.                                                             &#13;
     L'Avvocatura  generale,  invece,  conclude per l'infondatezza della  &#13;
 questione.                                                               &#13;
     Al riguardo deduce che  la  disposizione  denunziata,  seguendo  la  &#13;
 medesima   ratio   che   ha   indotto  il  legislatore  a  privilegiare  &#13;
 l'attuazione giurisdizionale del diritto del lavoratore subordinato, ha  &#13;
 assoggettato al nuovo rito rapporti di lavoro autonomo che, per il loro  &#13;
 carattere  continuativo,  coordinato   e   prevalentemente   personale,  &#13;
 realizzino  una  non  occasionale  dipendenza  del prestatore di lavoro  &#13;
 dall'impresa del preponente,  analoga  alla  dipendenza  economica  del  &#13;
 prestatore  subordinato.  Tale comune caratteristica sarebbe confermata  &#13;
 dal rilievo che l'attività professionale degli agenti e rappresentanti  &#13;
 di commercio, prima di essere disciplinata dalla legge 12  marzo  1968,  &#13;
 n.  316, ha formato oggetto di accordi economici collettivi, alcuni dei  &#13;
 quali sono stati resi obbligatori erga omnes, in attuazione della legge  &#13;
 14 luglio 1959, n. 741,  al  pari  di  altri  contratti  collettivi  di  &#13;
 lavoratori subordinati.                                                  &#13;
     Deriverebbe  da  tutto  ciò che l'assunta sperequazione tra agenti  &#13;
 puri e semplici e società costituite per il disbrigo degli  affari  di  &#13;
 agenzia  si  profilerebbe  quale  applicazione,  anziché  deroga,  del  &#13;
 principio di eguaglianza.                                                &#13;
     La difesa della società UNIPOL ha depositato memoria nella  quale,  &#13;
 insistendo  nelle  sue  conclusioni,  esprime  l'avviso  che  la tutela  &#13;
 privilegiata del lavoratore  subordinato  sarebbe  stata  ingiustamente  &#13;
 estesa  al  rapporto  di  lavoro  autonomo  degli agenti che sarebbero,  &#13;
 invece, in ogni caso, imprenditori.                                      &#13;
     L'illegittimità  sussisterebbe  anche   perché   non   potrebbero  &#13;
 avvalersi del nuovo rito né gli altri imprenditori non "agenti", né i  &#13;
 lavoratori autonomi non compresi nella norma denunziata.                 &#13;
     Non  sarebbe,  poi,  possibile fare distinzione nell'ambito interno  &#13;
 della categoria degli agenti per sottoporre alla speciale procedura del  &#13;
 lavoro solo  quelli  che  esplichino  una  prestazione  prevalentemente  &#13;
 personale,  per  essere  tale  requisito  comune  ad  ogni attività di  &#13;
 agenzia.                                                                 &#13;
     Per quanto specificamente concerne gli agenti di assicurazione,  la  &#13;
 stessa difesa fa, infine, presente che, di regola, la loro attività è  &#13;
 organizzata in imprese di medie o grandi dimensioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Il  pretore di Savona esprime l'avviso che l'art. 409, n. 3,  &#13;
 del codice di procedura civile, nel testo  sostituito  dalla  legge  11  &#13;
 agosto  1973, n. 533, sia in contrasto con l'art. 3 della Costituzione:  &#13;
 ciò in quanto "può creare e crea  sperequazioni  tra  agenti  puri  e  &#13;
 semplici  e  società  costituite per il disbrigo di affari d'agenzia".  &#13;
 Dalla  laconica  esposizione  dell'ordinanza   sembra   desumersi   che  &#13;
 l'eventuale  accoglimento  della  censura porterebbe all'esclusione del  &#13;
 rito speciale nei confronti degli uni e delle altre.                     &#13;
     2. - Modificando la generica impostazione del  giudice  a  quo,  la  &#13;
 difesa  della  società  UNIPOL  prospetta  che  la  discriminazione vi  &#13;
 sarebbe perché dal rito speciale  sono  esclusi  imprenditori  diversi  &#13;
 dagli agenti (o rappresentanti), come pure altri lavoratori autonomi.    &#13;
     Aggiunge  che un'ulteriore disparità vi sarebbe per la distinzione  &#13;
 che si opera nell'ambito della stessa categoria  degli  agenti,  per  i  &#13;
 quali   non   può   essere  elemento  differenziatore  la  prestazione  &#13;
 prevalentemente personale,  perché  tale  carattere,  richiesto  dalla  &#13;
 norma denunziata, mancherebbe proprio negli agenti di assicurazione (di  &#13;
 cui  trattasi nel giudizio di merito), che sarebbero degli imprenditori  &#13;
 di medie  e  grandi  dimensioni  e  mai  potrebbero  configurarsi  come  &#13;
 imprenditori che operano personalmente.                                  &#13;
     3.  -  Nei  limiti della prospettazione dell'ordinanza, va rilevato  &#13;
 che la norma denunziata estende il  medesimo  trattamento  processuale,  &#13;
 previsto per i lavoratori subordinati, ad ampie categorie di lavoratori  &#13;
 autonomi  che  abbiano  una  dipendenza  non  occasionale ed esplichino  &#13;
 prestazioni coordinate e prevalentemente personali, e, pertanto, affida  &#13;
 al magistrato la valutazione dei requisiti richiesti.                    &#13;
     Di tal che sarà il giudice investito della controversia,  nel  suo  &#13;
 apprezzamento,  ad  accertare  se la specifica natura delle prestazioni  &#13;
 dell'agente di assicurazione, nelle singole fattispecie, le collochi  o  &#13;
 meno  nella sfera di applicabilità della norma in esame, cioè se esse  &#13;
 siano meramente ausiliarie dell'attività imprenditoriale altrui oppure  &#13;
 se integrino quell'autonomia di impresa che è estranea  all'ambito  di  &#13;
 applicazione della norma stessa.                                         &#13;
     La questione è infondata.                                           &#13;
     E,  invero,  in questa sede è sufficiente osservare che il sistema  &#13;
 accolto ha come ratio la tutela non  solo  del  lavoro  subordinato  (e  &#13;
 agricolo  anche  non  subordinato), ma altresì del lavoro autonomo che  &#13;
 graviti attorno all'impresa, in quanto - ripetesi -  si  estrinseca  in  &#13;
 una  prestazione  di  opera  continuativa e coordinata, prevalentemente  &#13;
 personale: è una scelta, sul piano  processuale,  parallela  a  quella  &#13;
 che,  sul  piano  sostanziale,  è  stata operata dalla legge 14 luglio  &#13;
 1959, n. 741, la quale, all'art. 2, ha previsto che i decreti  delegati  &#13;
 per  l'efficacia  dei  contratti  collettivi  e degli accordi economici  &#13;
 concernano, oltreché rapporti di lavoro, di associazione  agraria,  di  &#13;
 affitto   a  coltivatore  diretto,  quelli  di  collaborazione  che  si  &#13;
 concretino in prestazioni d'opera nel senso su indicato.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  409,  n.  3,  del  codice  di  procedura  civile  nel  testo  &#13;
 risultante dall'art. 1 della legge 11  agosto 1973, n. 533  (Disciplina  &#13;
 delle  controversie  individuali  di  lavoro  e  delle  controversie in  &#13;
 materia di previdenza e  di  assistenza  obbligatoria),  sollevata,  in  &#13;
 riferimento all'art. 3 della Costituzione, con l'ordinanza in epigrafe.  &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 12 febbraio 1976.                             &#13;
                                   F.to: LUIGI OGGIONI - ANGELO DE MARCO  &#13;
                                   - ERCOLE ROCCHETTI - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI -  LEONETTO  AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO - ANTONINO DE STEFANO.         &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
