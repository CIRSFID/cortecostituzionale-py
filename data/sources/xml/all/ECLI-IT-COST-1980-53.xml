<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1980</anno_pronuncia>
    <numero_pronuncia>53</numero_pronuncia>
    <ecli>ECLI:IT:COST:1980:53</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/04/1980</data_decisione>
    <data_deposito>14/04/1980</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - &#13;
 Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 2 terdecies,  &#13;
 comma prima,  legge  4  agosto  1971,  n.  592  (interventi  in  favore  &#13;
 dell'agricoltura)  promosso con ordinanza emessa il 15 gennaio 1975 dal  &#13;
 Tribunale di Cosenza, nel procedimento civile vertente tra l'Opera  per  &#13;
 la  valorizzazione  della  Sila e Solima Luigi ed altri, iscritta al n.  &#13;
 222 del registro ordinanze 1975 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n. 188 del 16 luglio 1975.                              &#13;
     Visti  gli  atti  di  costituzione dell'Opera per la valorizzazione  &#13;
 della Sila e di Solima Luigi ed altri;                                   &#13;
     visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  13  febbraio  1980  il  Giudice  &#13;
 relatore Virgilio Andrioli;                                              &#13;
     udito l'avvocato dello Stato Renato Carafa per l'Opera Sila  e  per  &#13;
 il Presidente del Consiglio dei ministri;                                &#13;
     ritenuto  che  con  d.P.R.  18  dicembre  1951, n.   1410 (G. U. 31  &#13;
 dicembre 1951, n. 299, suppl.   ord.)  si  dispose  l'approvazione  del  &#13;
 piano  particolareggiato di espropriazione, compilato dall'Opera per la  &#13;
 valorizzazione della Sila,  per  i  terreni  ricadenti  nel  Comune  di  &#13;
 Bisignano  della  superficie  di  ettari  166.01.10 nei confronti della  &#13;
 ditta Vincenzo Solima fu Rosalbino e  se  ne  trasferì  la  proprietà  &#13;
 all'Opera  Sila  ordinandosene  la immediata occupazione; che con altro  &#13;
 d.P.R. 18 dicembre 1951, n. 1423, pubblicato nello  stesso  supplemento  &#13;
 della  G.  U., si dispose l'approvazione del piano particolareggiato di  &#13;
 espropriazione, compilato dalla Opera Sila per i terreni ricadenti  nel  &#13;
 Comune  di  S.  Sofia  Epiro  della  superficie  di ettari 26.80.30 nei  &#13;
 confronti della ditta Vincenzo e Francesco Solima fu Rosalbino e se  ne  &#13;
 trasferì  la  proprietà  alla  Opera  Sila  ordinandosene l'immediata  &#13;
 occupazione. Il tutto sebbene in data 26 aprile 1951 fosse deceduto, ab  &#13;
 intestato, Vincenzo Solima, alla cui successione erano stati chiamati i  &#13;
 figli Angela, Rosario, Luigi e Marco (o Marcantonio);                    &#13;
     che  con  atto  21 gennaio 1954, Rosario e Luigi Solima fu Vincenzo  &#13;
 convennero avanti il Tribunale di Cosenza la Opera Sila  perché  fosse  &#13;
 condannata,  con  sentenza  provvisoriamente  esecutiva, "all'immediato  &#13;
 rilascio di tutti i beni" (sia di quelli compresi nei due decreti,  sia  &#13;
 degli  altri,  nel  cui  possesso  la  Opera  Sila,  pur  non essendovi  &#13;
 compresi,  si  sarebbe  immessa),  nonché  "ai   frutti   dalla   data  &#13;
 dell'arbitraria occupazione ed ai danni tutti, niuno escluso, spiegando  &#13;
 che  tali  danni  debbono  comprendere  anche il valore venale dei beni  &#13;
 qualora si ritenesse l'Opera  esente  dall'obbligo  di  restituirli  in  &#13;
 natura.  Con gli interessi sul capitale di condanna del 5% e col favore  &#13;
 delle spese ed onorari di lite";                                         &#13;
     che   a   sostegno   della   domanda   gli   attori    denunciavano  &#13;
 l'incostituzionalità  della  legge  12  maggio 1950, n. 230, in virtù  &#13;
 della quale i decreti presidenziali erano stati adottati,  nonché  dei  &#13;
 due  decreti  perché  a)  la  espropriazione  era  stata  disposta nei  &#13;
 confronti del deceduto Vincenzo  Solima,  b)  comunque,  nessuno  degli  &#13;
 eredi  di  Vincenzo  era  proprietario  di  più di trecento ettari per  &#13;
 ciascuno, e, pertanto, versava nelle condizioni previste nella legge n.  &#13;
 230/1950, c) né,  infine,  i  terreni,  colpiti  da  esproprio,  erano  &#13;
 suscettibili   di  miglioramenti  maggiori  di  quelli  realizzati  dai  &#13;
 proprietari.  Con  specifico  riferimento,  poi,  al  d.P.R.  n.  1410,  &#13;
 riflettente   i   terreni   ricadenti   nel  Comune  di  Bisignano,  ne  &#13;
 denunciavano la incostituzionalità perché l'espropriazione era  stata  &#13;
 disposta in danno del solo Vincenzo Solima laddove il piano di scorporo  &#13;
 era  stato  compilato  nei  confronti  anche  di  Francesco  Solima, e,  &#13;
 pertanto, beni comuni erano stati espropriati nei confronti di uno solo  &#13;
 dei comproprietari,                                                      &#13;
     che con ordinanza 9 luglio 1958, l'adito  Tribunale  dichiarò  non  &#13;
 manifestamente  infondate le questioni di costituzionalità rinviandone  &#13;
 l'esame a questa Corte, che con sentenza  9  luglio  1959,  n.  41,  le  &#13;
 dichiarò non fondate in riferimento alla legge n. 230/1950 e al d.P.R.  &#13;
 18  dicembre  1951, n. 1423, attinente ai terreni situati nel Comune di  &#13;
 S. Sofia Epiro:  più precisamente, in ordine agli artt. 5  e  8  della  &#13;
 legge  n.  230/1950  ribadì  gli  orientamenti giurisprudenziali della  &#13;
 Corte e della Cassazione in virtù dei  quali  a)  i  provvedimenti  di  &#13;
 scorporo  adottati  dal  Governo in attuazione dell'art. 5 hanno natura  &#13;
 formalmente e sostanzialmente legislativa e non meramente esecutiva  e,  &#13;
 pertanto, non sussiste la elusione del precetto contenuto nell'art. 113  &#13;
 Cost.,  il  quale  riguarda  gli atti amministrativi, e b) il pagamento  &#13;
 dell'indennizzo in titoli anziché in denaro e la redimibilità di tali  &#13;
 titoli solo ad una certa epoca non implicano  violazione  dell'art.  42  &#13;
 Cost.;  ritenne  poi  insussistenti  i  due  eccessi  di delega, che, a  &#13;
 giudizio del Tribunale di  Cosenza,  avrebbero  viziato  il  d.P.R.  n.  &#13;
 1423/1951,  in  quanto,  in  base  al ripetuto art. 2, il provvedimento  &#13;
 doveva essere intestato a Vincenzo Solima, proprietario al 15  novembre  &#13;
 1949  e  al  20  maggio  1950, sebbene egli fosse deceduto il 26 aprile  &#13;
 1951, ed i figli di lui, proprietari alla  data  della  espropriazione,  &#13;
 mai avessero individualmente posseduto, fino al momento dello scorporo,  &#13;
 una  quantità  di terreni superiore a trecento ettari. Con riferimento  &#13;
 invece, al d.P.R. 18 dicembre 1951, n. 1410,  con  ordinanza  9  luglio  &#13;
 1959,  n.   42, dispose che gli atti fossero restituiti al Tribunale di  &#13;
 Cosenza perché chiarisse 1) se e a chi fosse  intestato  il  piano  di  &#13;
 scorporo  dei  terreni  ricadenti  nel  Comune  di  Bisignano, 2) quali  &#13;
 fossero le complessive consistenze terriere, al 14  novembre  1949,  di  &#13;
 ciascuno   dei   germani   Vincenzo   e  Francesco  Solima,  3)  a  chi  &#13;
 effettivamente  appartenessero  e a chi fossero intestati in catasto al  &#13;
 14 novembre 1949 i terreni espropriati  con  il  d.P.R.  n.  1410/1951.  &#13;
 Riassunta  avanti il giudice a quo la causa, nella quale intervennero i  &#13;
 due altri eredi di Vincenzo e cioè  Angela  e  Marco  (o  Marcantonio)  &#13;
 Solima,  ed esperita consulenza ad Opera dell'ing. Italo Caracciolo, il  &#13;
 quale, tra l'altro, precisò che, a suo avviso, l'Opera Sila non  aveva  &#13;
 occupato,  in  danno  dei  Solima, terreni che non fossero compresi nei  &#13;
 decreti presidenziali  di  esproprio,  il  Tribunale  di  Cosenza,  con  &#13;
 ordinanza  del  16  aprile 1963, rimise di bel nuovo le parti avanti la  &#13;
 Corte costituzionale, che, con sentenza 23 maggio 1964, n. 41,  ritenne  &#13;
 insussistenti  i  due  eccessi  di  delega,  sostanzialmente identici a  &#13;
 quelli già disattesi in riguardo al d.P.R. n. 1423/1951, ma  dichiarò  &#13;
 l'illegittimità, per eccesso di delega rispetto agli artt. 4 e 5 della  &#13;
 legge  n.    230/1950 e in riferimento agli artt. 76 e 77, comma primo,  &#13;
 Cost., del d.P.R.  n.  1410/1951  perché  emesso  in  danno  del  solo  &#13;
 comproprietario Vincenzo anche per la quota di comproprietà, di cui il  &#13;
 piano di scorporo aveva previsto l'espropriazione in danno di Francesco  &#13;
 Solima;                                                                  &#13;
     che,  riassunto  il  giudizio  di  merito  con atto 12 giugno 1964,  &#13;
 l'adito Tribunale, con sentenza  non  definitiva  27  gennaio  1  marzo  &#13;
 1965,1)  respinse  i  capi  della  domanda  attrice,  relativi  ai beni  &#13;
 espropriati con il d.P.R. n. 1423/1951, siti nel  Comune  di  S.  Sofia  &#13;
 Epiro,  2)  in  accoglimento, invece, della domanda attrice nella parte  &#13;
 riflettente i beni ricadenti nel Comune  di  Bisignano,  dichiarò  che  &#13;
 detti  beni  non erano mai usciti dal patrimonio dei germani Vincenzo e  &#13;
 Francesco Solima e, pertanto, 3) condannò l'Opera Sila al risarcimento  &#13;
 dei danni subiti dagli attori e intervenienti eredi di Vincenzo  Solima  &#13;
 in  conseguenza  della  mancata restituzione dei beni stessi nei limiti  &#13;
 precisati in motivazione;                                                &#13;
     che, con separata ordinanza designò consulente  il  dottor  Nicola  &#13;
 Catanzaro  cui  affidò  l'incarico di determinare i danni rimettendo le  &#13;
 parti, che avevano formulato riserva di appello avverso la sentenza non  &#13;
 definitiva, avanti il giudice istruttore.    Depositata  la  relazione,  &#13;
 alla  quale  mossero  critiche tutte le parti, e restituita la causa al  &#13;
 Collegio, questo, con sentenza definitiva 25 gennaio - 5 febbraio 1969,  &#13;
 condannò la Opera Sila al pagamento, in  favore  dei  germani  Solima,  &#13;
 della  somma  di  lire  559.736.883,  con  gli  interessi legali dal 21  &#13;
 gennaio 1967 al soddisfo, e a  tre  quarti  delle  spese  di  giudizio,  &#13;
 respingendo le altre domande dei Solima e compensando l'altro quarto di  &#13;
 spese;                                                                   &#13;
     che, spiegati tempestivamente appelli principale della Opera Sila e  &#13;
 incidentale  dei  germani Solima, con sentenza 27 gennaio - 18 febbraio  &#13;
 1971, notificata, ad istanza dei germani Solima alla Opera Sila, il  13  &#13;
 marzo  1971  (e,  quindi,  passata  in giudicato il 12 maggio 1971), 1)  &#13;
 dichiarò  cessata  la  materia  del  contendere  relativa  ai   motivi  &#13;
 d'impugnazione    della   Opera   Sila,   riferentisi   alla   parziale  &#13;
 illegittimità del d.P.R. n. 1410/1951, per avvenuta rinuncia da  parte  &#13;
 dell'opera  stessa la quale riconobbe che la Corte costituzionale aveva  &#13;
 dichiarato l'illegittimità del decreto n.  1410/1951 nel suo  insieme,  &#13;
 e  confermò  per  il  resto  la sentenza non definitiva 27 gennaio - 1  &#13;
 marzo 1965 del Tribunale di  Cosenza,  2)  in  parziale  riforma  della  &#13;
 sentenza  definitiva  del Tribunale condannò l'Opera Sila al pagamento  &#13;
 della somma di lire 271.856.200, oltre gli interessi  legali  su  detta  &#13;
 somma  dal  21  gennaio 1954 sino all'effettivo pagamento; 3) dichiarò  &#13;
 interamente  compensate  tra  le parti le spese del giudizio di secondo  &#13;
 grado; che, avverso l'atto di precetto, intimato, sotto la data del  13  &#13;
 ottobre  1971,  da Luigi Solima in proprio e in qualità di procuratore  &#13;
 generale dei tre suoi germani, per il pagamento  della  sorte  e  degli  &#13;
 interessi portati dalla sentenza della Corte d'appello di Catanzaro nel  &#13;
 frattempo  passata  in  giudicato, con detrazione di lire cento milioni  &#13;
 corrisposte il 22 giugno 1971 dalla Opera Sila, spiegò opposizione  la  &#13;
 Opera Sila con atto, notificato il 15 ottobre 1971, deducendo che nella  &#13;
 Gazzetta  Ufficiale  n.  205 del 14 agosto 1971 era stata pubblicata la  &#13;
 legge 4 agosto 1971, n. 592 (entrata, quindi, in vigore  il  29  agosto  &#13;
 1971),  per l'art. 2, terdecies, primo comma, della quale "al pagamento  &#13;
 di  somme  dovute  in  forza  sia  di  sentenze,  sia  di   transazioni  &#13;
 conseguenti  a  decisioni  della Corte costituzionale, in dipendenza di  &#13;
 espropriazioni disposte ai sensi delle leggi di riforma  fondiaria,  si  &#13;
 provvede  mediante  rilascio  di  titoli  del  prestito  per la riforma  &#13;
 fondiaria redimibile 5 per cento, di cui alla legge 21 ottobre 1950, n.  &#13;
 841, da emettere con l'osservanza delle modalità stabilite con decreto  &#13;
 del Ministro per  il  tesoro  del  28  giugno  1951,  pubblicato  nella  &#13;
 Gazzetta  Ufficiale  n.    146 del 30 successivo", e chiedendo, quindi,  &#13;
 dichiararsi più non essere tenuta essa Opera al pagamento.  I  Solima,  &#13;
 costituitisi   mediante   comparsa   di   risposta  16  novembre  1971,  &#13;
 obiettarono che non il giudice ordinario, ma il Consiglio di  Stato  ai  &#13;
 sensi  dell'art.  27,  n.  4, t.u.   del 1924 era competente a statuire  &#13;
 sulla vicenda, che l'art. 2, terdecies, comma  primo,  della  legge  n.  &#13;
 592/1971 era inapplicabile per essere la sentenza della Corte d'appello  &#13;
 di  Catanzaro, mandata ad esecuzione, già passata in giudicato, che la  &#13;
 ripetuta disposizione,  ove  la  si  fosse  ritenuta  applicabile,  era  &#13;
 affetta  da  illegittimità  per  contrasto  con  gli artt. 24,42 e 113  &#13;
 Cost., che,  comunque,  la  Opera  Sila  era  tenuta  a  promuovere  la  &#13;
 emissione dei titoli della riforma fondiaria e a procurarne la consegna  &#13;
 ai  creditori, nonché a corrispondere gli interessi in moneta contante  &#13;
 e la differenza tra il prezzo di  mercato  e  il  tasso  di  emissione;  &#13;
 pertanto,  chiesero  il  pagamento  in  contanti  della  somma  di lire  &#13;
 54.160.000,  oltre  gli  interessi  legali  dal  23  febbraio  1973  al  &#13;
 soddisfo, previa declaratoria d'illegittimità costituzionale dell'art.  &#13;
 2, terdecies, comma primo, legge n.  592/1971;                           &#13;
     che  successivamente  la  Opera  Sila  comunicò  ai  Solima che il  &#13;
 Ministero dell'Agricoltura e delle Foreste, con suo decreto 18 dicembre  &#13;
 1971, aveva autorizzato il pagamento della somma capitale liquidata  in  &#13;
 lire 271.856.209, degli interessi al 21 dicembre 1971 liquidati in lire  &#13;
 243.925.780 e delle spese al netto in lire 13.075.000, da corrispondere  &#13;
 in  titoli  del  prestito della riforma fondiaria, depositati presso la  &#13;
 Cassa di Risparmio di Calabria e Lucania, ed aveva ordinato che tutti i  &#13;
 detti titoli venissero svincolati a favore dei Solima.    Costoro,  con  &#13;
 ogni  riserva, presentarono al Tribunale di Cosenza istanza di svincolo  &#13;
 di detti titoli limitatamente a  lire  404.899.000;  istanza,  accolta,  &#13;
 nella  misura  di  lire  402.707.000,  con  decreto  7 ottobre 1972 del  &#13;
 Tribunale, a seguito del quale la Cassa di risparmio li  acquistò   per  &#13;
 la  somma  complessiva  di  lire  379.940.767,  per modo che precetto e  &#13;
 opposizione rimasero in piedi per il conseguimento della differenza fra  &#13;
 il valore nominale ed il prezzo ricavato  dalla  vendita  al  corso  di  &#13;
 borsa e per i relativi interessi;                                        &#13;
     che,  dal  suo  canto,  la Opera Sila, nella conclusionale 8 aprile  &#13;
 1974, negò applicarsi l'art. 27,  n.  4,  del  t.u.  delle  leggi  sul  &#13;
 Consiglio  di  Stato  perché trattavasi non già di dare esecuzione ad  &#13;
 una sentenza, sibbene di  valutare  l'incidenza,  su  di  un  giudicato  &#13;
 perfezionatosi,  di  disposizione normativa sopravvenuta; soggiunse che  &#13;
 con l'art. 2, terdecies, comma primo, della legge n.   592/1971 si  era  &#13;
 verificata  una  sorta  di  novazione  soggettiva  ex  lege; contestò,  &#13;
 comunque, di detta norma la illegittimità;                              &#13;
     che è andato, invece, in diverso avviso  il  Tribunale  adito,  il  &#13;
 quale,   con  ordinanza  15  gennaio  1975  regolarmente  comunicata  e  &#13;
 notificata, e pubblicata nella Gazzetta Ufficiale n. 188 del 16  luglio  &#13;
 1975  (n.  222  R.O. 1975), ha ritenuto non manifestamente infondata la  &#13;
 questione di legittimità dell'art. 2, terdecies,  comma  primo,  della  &#13;
 legge 4 agosto 1971, n.  592, in riferimento agli artt. 3, 24, 42 e 113  &#13;
 Cost., sospendendo il giudizio;                                          &#13;
     che  avanti  la  Corte  si  è costituito Luigi Solima in proprio e  &#13;
 quale procuratore generale, giusta procura con firma autenticata  il  4  &#13;
 marzo  1974  per  notaio  Marranghello  di Napoli, dei germani Rosario,  &#13;
 Angelo e Marco (o Marcantonio), rappresentato, in  virtù  di  mandato,  &#13;
 dall'avv.  Cesare  Gabriele  (poi  mancato ai vivi il 9 luglio 1976), e  &#13;
 mediante comparsa di costituzione depositata il 5 maggio 1975,  in  cui  &#13;
 si   è   concluso   dichiararsi   l'irrilevanza   della  questione  di  &#13;
 legittimità dell'art. 2 terdecies, comma primo, della legge  4  agosto  &#13;
 1971, n. 592, perché inapplicabile a sentenza, passata in giudicato al  &#13;
 momento  della  sua  entrata in vigore, e, in subordine, dichiararne la  &#13;
 fondatezza. È intervenuta per la Presidenza del Consiglio dei ministri  &#13;
 e si è costituita per l'Opera Sila l'Avvocatura generale  dello  Stato  &#13;
 mediante  atto  depositato  l'11 giugno 1975, in cui ha concluso per la  &#13;
 dichiarazione   d'infondatezza   della   prospettata    questione    di  &#13;
 costituzionalità;                                                       &#13;
     che  i  Solima  hanno illustrato le già prese conclusioni mediante  &#13;
 memoria sottoscritta dall'avv.  Carlo  Martuccelli  (difensore,  giusta  &#13;
 procura  autenticata  il 24 ottobre 1979 per notaio Maddalena, di Luigi  &#13;
 Solima in proprio e quale  procuratore  generale  dei  fratelli  Angela  &#13;
 giusta  procura  20 agosto 1960 per notaio Maddalena, e Rosario e Marco  &#13;
 giusta procura 2 marzo 1962 per dott. Durante,  coadiutore  del  notaio  &#13;
 Maddalena) e depositata il 31 gennaio 1980;                              &#13;
     che  all'udienza  pubblica del 13 febbraio 1980, cui la trattazione  &#13;
 della questione era  stata  rinviata  dalla  pubblica  udienza  del  21  &#13;
 novembre  1979,  l'Avvocatura  dello Stato ha illustrato le conclusioni  &#13;
 già formulate;                                                          &#13;
     che nella ordinanza 15 gennaio 1975  il  Tribunale  di  Cosenza  ha  &#13;
 detto  non  manifestamente  infondata la questione di costituzionalità  &#13;
 sollevata in subordine dai Solima "perché in tema di successione delle  &#13;
 norme giuridiche nel tempo vige  il  principio  della  irretroattività  &#13;
 enunciato  nell'art.  11 delle disposizioni sulla legge in generale, il  &#13;
 quale  preclude  l'applicazione  della  legge  nuova  non  soltanto  ai  &#13;
 rapporti giuridici esauriti prima della sua entrata in vigore, ma anche  &#13;
 a  quelli  sorti anteriormente ed ancora in vita, quando l'applicazione  &#13;
 della  nuova  legge  importi  il  disconoscimento  degli  effetti  già  &#13;
 verificati  del  fatto  passato o tolga efficacia, in tutto o in parte,  &#13;
 alle conseguenze attuali  o  future  del  fatto  medesimo  e  che  tale  &#13;
 principio cardine dell'ordinamento giuridico trova conferma negli artt.  &#13;
 73  della  Costituzione  e  10  delle predette disposizioni, per cui la  &#13;
 norma dell'art.  2 terdecies, comma primo, della legge n. 592 del 1971,  &#13;
 se  applicabile,  precluderebbe o limiterebbe l'autorità del giudicato  &#13;
 posto a fondamento del precetto opposto, già eseguito,  peraltro,  con  &#13;
 pagamento  parziale  in  contanti".  Prosegue  il Tribunale, cui non è  &#13;
 stata presente la sent. 9 luglio 1959, n. 41, resa da questa Corte  nel  &#13;
 corso  del  giudizio  definito  con la sentenza 25 gennaio - 5 febbraio  &#13;
 1969 della Corte d'appello di Catanzaro, che la ripetuta  disposizione,  &#13;
 consentendo   il   pagamento  di  una  somma  di  denaro  a  titolo  di  &#13;
 risarcimento danni  per  espropriazione  illegittima  in  titoli  della  &#13;
 riforma  fondiaria di valore notoriamente inferiore appare in contrasto  &#13;
 sia con l'art. 3 Cost. che sancisce la parità dei cittadini di  fronte  &#13;
 alla  legge,  sia  con  l'art.  24  circa la tutela giurisdizionale dei  &#13;
 propri diritti, specie se assistiti da un giudicato, sia con l'art.  42  &#13;
 circa la statuita indennizzabilità effettiva di un esproprio specie se  &#13;
 riconosciuto,  come  nel caso, illegittimo, sia con l'art. 113 circa la  &#13;
 tutela giurisdizionale dei  diritti  contro  gli  atti  della  pubblica  &#13;
 Amministrazione e senza alcuna limitazione per determinate categorie di  &#13;
 atti.     Né,  sempre  a  giudizio  del  Tribunale,  la  questione  di  &#13;
 legittimità verrebbe meno a causa dell'art. 8 della  legge  12  maggio  &#13;
 1950,  n.  230  perché,  "altrimenti, la parte soggetta all'esproprio,  &#13;
 specie se riconosciuto illegittimo, non avrebbe, come nella generalità  &#13;
 dei casi, il diritto all'integrale  risarcimento  del  danno  sofferto,  &#13;
 donde  un  ulteriore  ipotetico  contrasto  con  il citato art. 3 della  &#13;
 Costituzione";                                                           &#13;
     che nell'atto, depositato  l'11  giugno  1975,  l'Avvocatura  dello  &#13;
 Stato  obietta  che, ove si riguardi la pretesa dei Solima come diretta  &#13;
 al risarcimento del danno,  non  si  può  elevare  a  parametro  della  &#13;
 questione  di  legittimità  il precetto costituzionale che sancisce un  &#13;
 indennizzo in caso di espropriazione della proprietà privata,  che  la  &#13;
 difesa  dei  diritti,  esercitata  dalle  parti private anche avanti la  &#13;
 Corte, esclude la violazione in concreto degli artt. 24 e 113 Cost.,  e  &#13;
 che,  infine,  l'applicabilità  al  caso  della disposizione normativa  &#13;
 impugnata non implica violazione dell'art. 73 Cost., nonché  dell'art.  &#13;
 10  delle  disposizioni  sull'applicazione della legge in generale. Né  &#13;
 infine, sempre  ad  avviso  dell'Avvocatura,  riuscirebbe  dalla  norma  &#13;
 impugnata offeso l'art. 3 non solo perché, più che di risarcimento di  &#13;
 danno  provocato  da  illecito,  è  da  far parola di aestimatio della  &#13;
 diminuzione patrimoniale provocata dalla mancata restituzione dei  beni  &#13;
 espropriati   in  virtù  di  un  decreto  legislativo  successivamente  &#13;
 dichiarato illegittimo, ma anche perché il pagamento in  contanti  non  &#13;
 è  qualcosa  di  giuridica  mente  diverso  dal  pagamento  con titoli  &#13;
 commerciabili in borsa,  che  fruiscono  dell'interesse  annuo  del  5%  &#13;
 netto.  Nel  corso  della  discussione  orale,  infine, l'Avvocatura ha  &#13;
 richiamato la sentenza 8 luglio 1975, n. 183, con qui questa  Corte  ha  &#13;
 ritenuto  non  fondata,  in  riferimento  all'art.  3,  comma primo, la  &#13;
 questione di legittimità dell'art. 2 terdecies, comma primo,  1  della  &#13;
 legge 4 agosto 1971, n. 592, in atto impugnato;                          &#13;
     che,  dal  loro  canto,  i  Solima, nelle deduzioni depositate il 5  &#13;
 maggio  1975,  ripetono  che  il  Tribunale  avrebbe  dovuto   reputare  &#13;
 irrilevante  la  questione  per  essere  il  primo  comma dell'art.   2  &#13;
 terdecies inapplicabile alla specie visto che la sentenza, con  cui  la  &#13;
 Corte  di  Catanzaro  aveva  condannato  la Opera Sila al pagamento dei  &#13;
 danni, era passata in giudicato il 12 marzo 1971, mentre la  legge  era  &#13;
 stata promulgata il 4 agosto 1971 e pubblicata nella Gazzetta Ufficiale  &#13;
 il  successivo  14;  nel  merito della prospettata questione si rifanno  &#13;
 alla   motivazione  della  ordinanza  di  rimessione.    Nella  memoria  &#13;
 depositata il 31 gennaio 1980, insistono nella eccezione  d'irrilevanza  &#13;
 delle   questioni   perché  della  norma  impugnata  non  è  prevista  &#13;
 l'efficacia retroattiva e ribadiscono la diversità di  posizioni,  che  &#13;
 si  converte  in offesa dell'art. 3, dell'espropriato in forme legali e  &#13;
 di chi sia stato privato del suo bene  in  virtù  di  atto  dichiarato  &#13;
 illegittimo;  contestano,  infine,  l'argomentazione esposta per ultima  &#13;
 dall'Avvocatura sul riflesso che  la  norma  impugnata  impone  ad  una  &#13;
 limitata  categoria  di  creditori  di accettare in pagamento titoli, i  &#13;
 quali, per loro natura e nell'attuale contesto economico, hanno  valore  &#13;
 inferiore  al  normale,  e  non  prevede  un conguaglio che consenta di  &#13;
 conseguire il risarcimento nella misura esatta  nella  quale  è  stato  &#13;
 liquidato;                                                               &#13;
     considerato  che oggetto delle conclusioni ultime dei Solima avanti  &#13;
 il Tribunale di Cosenza era il pagamento in  contanti  della  somma  di  &#13;
 lire  54.160.000  oltre  gli  interessi  legali dal 23 febbraio 1973 al  &#13;
 soddisfo;                                                                &#13;
     che la eccezione d'irrilevanza, in cui insistono  anche  in  questa  &#13;
 sede  i  Solima,  non  impedisce  alla  Corte  di sciogliere il dubbio,  &#13;
 sollevato dal Tribunale nella motivazione ma non emerso nel dispositivo  &#13;
 della ordinanza di rimessione, sul se il principio di  irretroattività  &#13;
 tragga conferma non solo dall'art. 10 disp. sulla legge in generale, ma  &#13;
 anche  dall'art. 73 Cost.; dubbio che, se risolto affermativamente, non  &#13;
 consentirebbe alla norma impugnata di precludere  o,  quanto  meno,  di  &#13;
 violare l'autorità del giudicato;                                       &#13;
     che  il  dubbio,  in  tali  termini  prospettato, non ha ragione di  &#13;
 esistere perché questa Corte ha costantemente negato  all'art.  73  il  &#13;
 significato  di  imprimere  al  principio di irretroattività rilevanza  &#13;
 costituzionale fuori della materia penale, nella quale, con il rispetto  &#13;
 dell'eccezione  imposta  dal  favor  rei  che  persuade  ad   assegnare  &#13;
 efficacia  retroattiva  alla  legge  più favorevole al reo, l'art. 25,  &#13;
 comma secondo, statuisce che nessuno può essere punito se non in forza  &#13;
 di una legge che sia entrata in vigore prima del fatto commesso (sentt.  &#13;
 n. 9/1959; 23/1967; 19/1970). Pertanto l'art. 73 Cost. non  impedirebbe  &#13;
 alla   norma   impugnata  di  prevalere  sull'autorità  del  giudicato  &#13;
 promanante dalla sentenza della Corte d'appello  di  Catanzaro  con  la  &#13;
 conseguenza  che i rapporti tra la Opera Sila e i Solima rinvenirebbero  &#13;
 la loro disciplina non nella sentenza, sibbene nella  norma  impugnata,  &#13;
 ma  è  appena  il  caso  di  soggiungere che lo scrutinare se l'art. 2  &#13;
 terdecies, comma primo, interpretato  a  stregua  dei  criteri  dettati  &#13;
 nell'art.    12  disp.  sulla  legge  in generale, si applichi anche ai  &#13;
 rapporti, che pur  abbiano  formato  oggetto  di  sentenza  passata  in  &#13;
 giudicato,  è  operazione che, estranea ai compiti di questa Corte, è  &#13;
 riservata al  magistero  del  giudice  del  merito;  magistero  che  il  &#13;
 Tribunale  non  ha  esercitato  perché,  senza dare neppure atto della  &#13;
 exceptio iudicati, opposta dai Solima alle due deduzioni  di  novazione  &#13;
 soggettiva  e di modo di estinzione del debito accertato dalla Corte di  &#13;
 Catanzaro, dalla  Opera  Sila  basate  sulla  norma  impugnata,  si  è  &#13;
 limitato  ad  ipotizzare  la  prevalenza  della  legge sopravvenuta sul  &#13;
 preesistente  giudicato  laddove  l'interpretazione,  ai   fini   della  &#13;
 individuazione dei limiti temporali di sua applicabilità, dell'art. 2,  &#13;
 terdecies,  comma  primo,  tanto più si esigeva puntuale per quanto il  &#13;
 Tribunale  aveva  preso  le  mosse  dalla  affermazione  della  normale  &#13;
 inapplicabilità  della  legge  agli effetti non ancora verificatisi di  &#13;
 fatti  anteriori.  Ditalché  altra  alternativa non si apre alla Corte  &#13;
 all'infuori di restituire gli atti al Tribunale onde questo proceda  in  &#13;
 assoluta  libertà  di  giudizio  alla  valutazione  di rilevanza della  &#13;
 prospettata  questione,  o,  in  ipotesi,  conosca  del  merito   della  &#13;
 opposizione della Opera Sila al precetto intimatole dai Solima.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina la restituzione degli atti al Tribunale di Cosenza.           &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 2 aprile 1980.                                &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE   STEFANO   -   LEOPOLDO   ELIA  -  &#13;
                                   GUGLIELMO ROEHRSSEN - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
