<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>68</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:68</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Leonetto Amadei</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/03/1975</data_decisione>
    <data_deposito>25/03/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 1, secondo  &#13;
 comma, del contratto collettivo nazionale di lavoro 25 luglio 1959  per  &#13;
 gli  operai  addetti all'industria delle calzature, recepito nel d.P.R.  &#13;
 25 settembre 1960, n. 1433, promosso con ordinanza emessa il 23  giugno  &#13;
 1972 dal pretore di orvieto nel procedimento penale a carico di Menconi  &#13;
 Alessandro, iscritta al n. 282 del registro ordinanze 1972 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 247 del 20 settembre 1972.  &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica dell'8 gennaio 1975 il Giudice relatore  &#13;
 Leonetto Amadei;                                                         &#13;
     udito il sostituto avvocato generale dello  Stato  Giorgio  Zagari,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     In  data  10  febbraio 1972 l'ispettorato provinciale del lavoro di  &#13;
 Terni  elevava  contravvenzione  a  Menconi  Alessandro,  titolare  del  &#13;
 calzaturificio  "Etruria",  per  violazione  dell'art. 1 della legge 14  &#13;
 luglio 1959, n. 741, non avendo comunicato con lettera a 85  operai  la  &#13;
 loro avvenuta assunzione al lavoro.                                      &#13;
     Il   pretore   di  Orvieto  emetteva  decreto  penale  di  condanna  &#13;
 regolarmente opposto.                                                    &#13;
     Nel giudizio di opposizione  il  pretore  ha  sollevato  d'ufficio,  &#13;
 ritenendola  rilevante,  la  questione  di  legittimità costituzionale  &#13;
 dell'art. 1, secondo comma, del contratto collettivo nazionale per  gli  &#13;
 operai  dell'industria calzaturiera, recepito nel 25 settembre 1960, n.  &#13;
 1433, limitatamente all'avverbio  "normalmente",  in  riferimento  agli  &#13;
 artt.  3,  primo  comma, 25, secondo comma, e 101, secondo comma, della  &#13;
 Costituzione.                                                            &#13;
     La violazione del primo comma dell'art.  3  della  Costituzione  è  &#13;
 prospettata  sotto  il  profilo che l'eguaglianza dei cittadini davanti  &#13;
 alla legge non può considerarsi realizzata da norme positive che,  per  &#13;
 l'incertezza   del   comando,  richiedono  una  particolare  accortezza  &#13;
 nell'afferrare l'essenza e la portata del comando stesso; dell'art. 25,  &#13;
 secondo  comma, perché risulterebbe violato nella parte che "scolpisce  &#13;
 il principio di  legalità";  dell'art.  101,  secondo  comma,  infine,  &#13;
 perché  non  sarebbe  pensabile  che  il giudice resti soggetto ad una  &#13;
 legge  che  sia  tale  solo  "in  apparenza"  e  che  palesi   la   sua  &#13;
 insufficienza a tal punto da poter dare luogo "ad arbitrii".             &#13;
     Non  vi  è stata costituzione di parte. È intervenuto, invece, il  &#13;
 Presidente  del  Consiglio  dei  ministri,   rappresentato   e   difeso  &#13;
 dall'Avvocatura dello Stato.                                             &#13;
     Questa  contesta,  in  via preliminare, la rilevanza della proposta  &#13;
 questione, che sarebbe stata affermata e non sviluppata  nell'ordinanza  &#13;
 di rimessione. In sostanza il proponente avrebbe omesso di dare risalto  &#13;
 al nesso esistente tra la norma impugnata e la imputazione contestata e  &#13;
 alle   conseguenze  che  potrebbero  derivare  dalla  dichiarazione  di  &#13;
 illegittimità costituzionale di essa norma.  Nel  merito  l'Avvocatura  &#13;
 ritiene infondata la questione.                                          &#13;
     L'art.  1,  secondo  comma,  del contratto collettivo cui trattasi,  &#13;
 nello stabilire che "l'assunzione  verrà  normalmente  comunicata  per  &#13;
 iscritto   all'interessato",   non   farebbe  altro  che  applicare  il  &#13;
 principio, generalmente accolto nel nostro ordinamento giuridico, della  &#13;
 "libertà  di  forma  nella  manifestazione  di  volontà"  così  come  &#13;
 sancisce l'art. 1325, n. 4, del codice civile.                           &#13;
     Conseguirebbe   da   ciò   che  nessun  contrasto  esiste  tra  la  &#13;
 disposizione  contestata  e  le  norme  costituzionali,  richiamate  in  &#13;
 riferimento dalla ordinanza pretorile; l'avverbio "normalmente", per il  &#13;
 suo  carattere meramente indicativo, non attribuirebbe, pertanto, forza  &#13;
 cogente  alla  norma,  ma  solo  valore  di  indirizzo,   incapace   di  &#13;
 determinare "l'azione e gli inconvenienti prospettati nell'ordinanza di  &#13;
 rinvio".<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  L'ordinanza  del  pretore di orvieto pone la questione della  &#13;
 legittimità costituzionale dell'art. 1, secondo comma,  del  contratto  &#13;
 collettivo  nazionale per gli operai delle industrie calzaturiere, reso  &#13;
 obbligatorio  erga  omnes  col  d.P.R.  25  settembre  1960,  n.  1433,  &#13;
 limitatamente  all'avverbio "normalmente", in riferimento agli artt. 3,  &#13;
 primo  comma,  25,  secondo  comma,  e  101,   secondo   comma,   della  &#13;
 Costituzione.                                                            &#13;
     La questione è inammissibile.                                       &#13;
     2.  -  L'art.  1  della  legge  14  luglio  1959,  n.  741  - Norme  &#13;
 transitorie per garantire minimi salariali - concede delega al  Governo  &#13;
 di emanare norme giuridiche aventi forza di legge ai fini di assicurare  &#13;
 minimi  inderogabili di trattamento economico e normativo nei confronti  &#13;
 di tutti gli appartenenti ad una medesima categoria  e  stabilisce  che  &#13;
 nell'emanazione  delle  predette  norme  il  Governo deve uniformarsi a  &#13;
 tutte le clausole dei singoli accordi economici e contratti collettivi,  &#13;
 anche  intercategoriali,   stipulati   dalle   associazioni   sindacali  &#13;
 anteriormente alla data di entrata in vigore della legge.                &#13;
     L'art.  8 della stessa legge stabilisce che il datore di lavoro che  &#13;
 non adempie gli obblighi derivanti dalle norme di  cui  all'art.  1  è  &#13;
 punito con ammenda da lire 5.000 a lire 100.000 per ogni lavoratore cui  &#13;
 si riferisce la violazione.                                              &#13;
     Con  d.P.R.  25  settembre  1960,  n.  1433 - Norme sul trattamento  &#13;
 economico e normativo dei lavoratori dipendenti dalle imprese esercenti  &#13;
 la produzione  delle  calzature,  pantofole  e  tomaie  -  veniva  reso  &#13;
 esecutivo,  a  norma  della  legge  delega  surrichiamata, il contratto  &#13;
 collettivo nazionale di lavoro 25 luglio 1959 per  gli  operai  addetti  &#13;
 alle industrie calzaturiere.                                             &#13;
     L'art. 1 del precitato contratto stabilisce, nel secondo comma, che  &#13;
 l'assunzione  degli  operai  deve  essere  "normalmente  comunicata per  &#13;
 iscritto all'interessato...".                                            &#13;
     3. - La Corte osserva, sulla base della propria giurisprudenza, che  &#13;
 la questione di costituzionalità  della  disposizione  impugnata  deve  &#13;
 essere dichiarata inammissibile.                                         &#13;
     La  Corte,  infatti, ha statuito (v. sent. nn. 106 e 107 del 1962 e  &#13;
 per ultimo n. 120 del 1974) che l'inserzione nei decreti delegati,  che  &#13;
 hanno  reso esecutivi erga omnes i contratti collettivi, di clausole in  &#13;
 contrasto con norme  imperative  di  legge  e  a  maggior  ragione  con  &#13;
 precetti  costituzionali  si  deve considerare inoperante e incapace di  &#13;
 conferire ad esse forza di legge sulla base  della  legge  n.  741  del  &#13;
 1959.                                                                    &#13;
     Spetterà  pertanto al giudice ordinario stabilire se l'espressione  &#13;
 "normalmente" contenuta  nell'art.  l,  secondo  comma,  del  contratto  &#13;
 collettivo  di  lavoro  25  luglio  1959  sia stata o meno recepita nel  &#13;
 decreto delegato.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 dell'art.  l, secondo comma, del contratto collettivo nazionale per gli  &#13;
 operai delle industrie calzaturiere,  reso  esecutivo  erga  omnes  con  &#13;
 d.P.R.  25  settembre  1960,  n.  1433,  sollevata  con  l'ordinanza in  &#13;
 epigrate.                                                                &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 20 marzo 1975.                                &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI  - VEZIO CRISAFULLI - PAOLO  &#13;
                                   ROSSI -  LEONETTO  AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO.                               &#13;
                                   ARDUINO SALUSTRI Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
