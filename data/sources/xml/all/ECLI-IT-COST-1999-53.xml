<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>53</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:53</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/02/1999</data_decisione>
    <data_deposito>04/03/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  5,  settimo    &#13;
 (recte:   quinto)  comma,  della  legge  26  ottobre  1957,  n.  1047    &#13;
 (Estensione  dell'assicurazione  per  invalidità  e   vecchiaia   ai    &#13;
 coltivatori  diretti,  mezzadri  e  coloni),  promosso  con ordinanza    &#13;
 emessa il 5 marzo 1997 dal pretore di Treviso sul ricorso proposto da    &#13;
 Nichele Guido contro l'I.N.P.S., iscritta  al  n.  324  del  registro    &#13;
 ordinanze 1997 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 25, prima serie speciale, dell'anno 1997.                             &#13;
   Visto  l'atto di costituzione dell'I.N.P.S.;                           &#13;
   Udito nell'udienza pubblica del 12 gennaio 1999 il giudice relatore    &#13;
 Fernanda Contri;                                                         &#13;
   Udito l'avvocato Carlo De Angelis per l'I.N.P.S.                       &#13;
   Ritenuto  che,  nel  corso di un giudizio promosso da Guido Nichele    &#13;
 contro l'INPS, il pretore di Treviso, con ordinanza emessa il 5 marzo    &#13;
 1997, ha sollevato, in riferimento agli artt. 3 e 38, secondo  comma,    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art.    5, settimo (recte: quinto) comma, della legge 26 ottobre    &#13;
 1957, n.   1047  (Estensione  dell'assicurazione  per  invalidità  e    &#13;
 vecchiaia  ai coltivatori diretti, mezzadri e coloni), nella parte in    &#13;
 cui  non  consente  l'accreditamento  dei  contributi   versati   dal    &#13;
 lavoratore   che,  "dopo  aver  svolto  abitualmente  e  direttamente    &#13;
 attività manuale di coltivazione del fondo con  i  propri  familiari    &#13;
 per  la  maggior  parte  dell'anno,  non  risulta essere presente nel    &#13;
 nucleo familiare al  31  dicembre  dell'anno  cui  si  riferiscono  i    &#13;
 contributi";                                                             &#13;
     che  il  giudice  a  quo  afferma  la  rilevanza  della questione    &#13;
 sollevata;                                                               &#13;
     che, sotto  il  profilo  della  non  manifesta  infondatezza,  il    &#13;
 pretore  di  Treviso  prospetta  il  contrasto  con  l'art.  3  della    &#13;
 Costituzione  per  disparità  di  trattamento   derivante   da   una    &#13;
 circostanza   -  l'emigrazione  a  scopo  lavorativo  -  "causata  da    &#13;
 scarsità di lavoro in Italia e non riconducibile quindi ad  un  atto    &#13;
 meramente volitivo del ricorrente";                                      &#13;
     che,  in  merito  al  prospettato  contrasto  con l'art. 38 della    &#13;
 Costituzione, il giudice a quo osserva che la disposizione  impugnata    &#13;
 precluderebbe   senza   ragione   la   computabilità   -   ai   fini    &#13;
 dell'accredito dei contributi sulla singola posizione assicurativa  -    &#13;
 di periodi di lavoro effettivamente prestati dall'interessato e per i    &#13;
 quali i contributi sono stati versati;                                   &#13;
     che  nel giudizio davanti a questa Corte si è costituito l'INPS,    &#13;
 per chiedere che la questione sollevata dal pretore  di  Treviso  sia    &#13;
 dichiarata infondata;                                                    &#13;
     che,  ad avviso dell'INPS, la censura formulata dal giudice a quo    &#13;
 non può essere circoscritta all'esclusione della  computabilità  di    &#13;
 periodi di lavoro prestati da coloro che non risultavano presenti nel    &#13;
 nucleo  familiare  al  31  dicembre  dell'anno  cui  si  riferivano i    &#13;
 contributi, non garantendo la disciplina introdotta dall'art. 5 della    &#13;
 legge 26 ottobre  1957,  n.  1047,  la  corrispondenza  all'effettivo    &#13;
 impiego  di  mano  d'opera della "individuale spettanza di contributi    &#13;
 giornalieri",  e   dipendendo   l'attribuzione   di   questi   ultimi    &#13;
 dall'applicazione  dei  criteri  di  priorità  fra  i  componenti la    &#13;
 famiglia stabiliti dalla medesima legge.                                 &#13;
   Considerato che, in ordine  alla  fattispecie  concreta  sottoposta    &#13;
 alla  cognizione del giudice rimettente, dall'ordinanza di rimessione    &#13;
 si desume soltanto che oggetto del procedimento civile a quo  sarebbe    &#13;
 la  liquidazione  della  pensione  del  ricorrente,  emigrato a scopo    &#13;
 lavorativo nell'anno 1959;                                               &#13;
     che  il  giudice  a  quo  in  merito  alla  fattispecie  concreta    &#13;
 sottoposta  al  suo  esame,  non  fornisce  alcun elemento ulteriore,    &#13;
 omettendo,  in  particolare,  ogni  riferimento  all'iscrizione   del    &#13;
 ricorrente  negli  elenchi  di  categoria,  alla sua collocazione nel    &#13;
 nucleo familiare ai fini dell'applicazione dei criteri  di  priorità    &#13;
 di cui ai primi tre commi del denunciato art. 5, alla maturazione dei    &#13;
 requisiti   di  età  e  di  anzianità  contributiva,  all'eventuale    &#13;
 presentazione, da parte dell'interessato, a norma dell'art. 11  della    &#13;
 legge  2  agosto  1990, n. 233 (Riforma dei trattamenti pensionistici    &#13;
 dei lavoratori autonomi), della richiesta di riscatto dei  contributi    &#13;
 non   accreditati  per  effetto  del  secondo  comma  dell'art.  3  e    &#13;
 dell'impugnato art. 5 della legge n. 1047 del 1957;                      &#13;
     che   il   giudice   a  quo  non  si  sofferma  sulla  perdurante    &#13;
 applicabilità della disciplina impugnata,  abrogata  dalla  legge  9    &#13;
 gennaio  1963,  n. 9 (Elevazione dei trattamenti minimi di pensione e    &#13;
 riordinamento delle norme in materia di  previdenza  dei  coltivatori    &#13;
 diretti  e  dei  coloni  e  mezzadri),  né  -  in  presenza  di  una    &#13;
 giurisprudenza di merito sul  punto  non  univoca  -  fa  cenno  agli    &#13;
 effetti  eventualmente derivanti, sul piano dell'applicabilità della    &#13;
 norma censurata al caso dedotto nel  giudizio  principale,  dall'art.    &#13;
 4-ter  del  decreto-legge 15 gennaio 1993, n. 6 (Disposizioni urgenti    &#13;
 per il recupero degli introiti contributivi in materia previdenziale)    &#13;
 come modificato dalla legge 17 marzo  1993,  n.  63  (Conversione  in    &#13;
 legge,  con  modificazioni,  del decreto-legge 15 gennaio 1993, n. 6,    &#13;
 recante  disposizioni  urgenti  per  il   recupero   degli   introiti    &#13;
 contributivi in materia previdenziale);                                  &#13;
     che,   secondo   la  costante  giurisprudenza  di  questa  Corte,    &#13;
 l'insufficiente motivazione dell'ordinanza  di  rimessione  sotto  il    &#13;
 profilo   della   adeguata  descrizione  della  fattispecie  concreta    &#13;
 comporta   l'inammissibilità   della   questione   di   legittimità    &#13;
 costituzionale,  giacché  impedisce  di  valutarne  la rilevanza nel    &#13;
 procedimento a quo (ex plurimis ordinanze nn. 129 e 69 del 1998; 151,    &#13;
 69 e 62 del 1997);                                                       &#13;
     che, secondo la giurisprudenza di questa Corte, qualora le  norme    &#13;
 oggetto  di censura siano abrogate o comunque modificate, sui giudici    &#13;
 a quibus grava un onere di motivazione rigoroso e pieno,  dovendo  il    &#13;
 rimettente  indicare con sufficiente esaustività i concreti elementi    &#13;
 della fattispecie sottoposta al suo  esame  e  specificare  i  motivi    &#13;
 della  perdurante rilevanza della questione (da ultimo, ordinanze nn.    &#13;
 343 e 79 del 1998; 419 del 1997);                                        &#13;
     che i rilevati vizi  della  motivazione  impediscono  alla  Corte    &#13;
 qualsiasi  controllo  sul  requisito  della rilevanza della questione    &#13;
 sollevata ed anche sull'avvenuto apprezzamento di tale condizione  di    &#13;
 proponibilità da parte del giudice a quo;                               &#13;
     che,  pertanto, la questione dev'essere dichiarata manifestamente    &#13;
 inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità costituzionale dell'art. 5, quinto comma, della legge 26    &#13;
 ottobre  1957, n. 1047 (Estensione dell'assicurazione per invalidità    &#13;
 e vecchiaia ai coltivatori diretti, mezzadri e coloni), sollevata, in    &#13;
 riferimento agli artt. 3 e 38, secondo comma, della Costituzione, dal    &#13;
 pretore di Treviso con l'ordinanza in epigrafe.                          &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 24 febbraio 1999.                             &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 4 marzo 1999.                             &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
