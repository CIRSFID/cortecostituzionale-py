<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>102</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:102</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GALLO E.</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/02/1991</data_decisione>
    <data_deposito>11/03/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Ettore GALLO; &#13;
 Giudici: dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  566,  nono    &#13;
 comma,  del codice di procedura penale, promosso con ordinanza emessa    &#13;
 il 9 agosto 1990 dal Pretore di Grosseto nel  procedimento  penale  a    &#13;
 carico  di  Cazzarotto  Stefano,  iscritta  al  n.  682  del registro    &#13;
 ordinanze 1990 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 44, prima serie speciale, dell'anno 1990;                             &#13;
    Udito nella camera di consiglio del 30  gennaio  1991  il  Giudice    &#13;
 relatore Mauro Ferri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel  corso del procedimento penale a carico di Cazzarotto Stefano,    &#13;
 il Pretore di Grosseto ha sollevato, con ordinanza del 9 agosto 1990,    &#13;
 questione di legittimità costituzionale - in riferimento agli  artt.    &#13;
 3  e 24 della Costituzione - dell'art. 566, nono comma, del codice di    &#13;
 procedura penale (secondo cui "fuori  dei  casi  previsti  dai  commi    &#13;
 precedenti,  il  pubblico ministero procede a norma del titolo II del    &#13;
 presente libro"), in quanto esclude l'applicabilità al  procedimento    &#13;
 pretorile  della disciplina del rito direttissimo di cui all'art. 449    &#13;
 del  codice  di  procedura  penale,   con   particolare   riferimento    &#13;
 all'ipotesi   di   cui  al  quinto  comma  di  quest'ultimo  articolo    &#13;
 (confessione dell'imputato).                                             &#13;
    Osserva preliminarmente il giudice a quo che la  presunta  ragione    &#13;
 della limitazione del giudizio direttissimo nel processo pretorile al    &#13;
 solo  caso  di cui alla lettera a) della direttiva n. 43 della legge-delega  (giudizio  contestuale   alla   convalida   dell'arresto   in    &#13;
 flagranza)  -  ragione  individuata  nella  peculiarità del processo    &#13;
 pretorile stesso - non appare fondata, in quanto essa manterrebbe  la    &#13;
 sua  valenza  solo  ove fosse consentita la facoltà di scelta tra il    &#13;
 procedimento direttissimo di cui all'art. 566 del codice di procedura    &#13;
 penale e quello direttissimo cosiddetto ordinario regolato  dall'art.    &#13;
 449  dello  stesso  codice:  ma ciò è escluso dalla disposizione in    &#13;
 questione.                                                               &#13;
    Ciò  posto,  la  norma  impugnata  violerebbe,  ad   avviso   del    &#13;
 remittente,  i  sopra  indicati  parametri  costituzionali, in quanto    &#13;
 l'imputato di reato di competenza  pretorile,  in  caso  di  identico    &#13;
 comportamento,  quale  l'avere  reso  piena  confessione,  verrebbe a    &#13;
 soffrire  una  ingiustificata  disparità  di  trattamento   rispetto    &#13;
 all'imputato  di  reato  di  competenza  del  tribunale,  sia  per la    &#13;
 maggiore durata del giudizio in sé, da instaurare con rito ordinario    &#13;
 e quindi con termini più lunghi, sia per il protrarsi della custodia    &#13;
 cautelare, come avviene nel caso di specie.                              &#13;
    Quanto alla rilevanza, il  Pretore  osserva  che  dalla  decisione    &#13;
 della  proposta questione dipende la natura del giudizio con il quale    &#13;
 definire il procedimento.<diritto>Considerato in diritto</diritto>1.  -  Il  Pretore   di   Grosseto   dubita   della   legittimità    &#13;
 costituzionale  dell'art.  566,  nono  comma, del codice di procedura    &#13;
 penale,  in  quanto  esclude   l'applicabilità,   nel   procedimento    &#13;
 pretorile,   del  giudizio  direttissimo  nella  ipotesi  -  prevista    &#13;
 nell'art. 449,  quinto  comma,  dello  stesso  codice  in  ordine  al    &#13;
 procedimento davanti al tribunale - di confessione resa dall'imputato    &#13;
 nel  corso  dell'interrogatorio;  la  norma violerebbe, ad avviso del    &#13;
 remittente, gli artt. 3 e  24  della  Costituzione,  per  irrazionale    &#13;
 disparità  di  trattamento  tra  imputati confessi, a seconda che il    &#13;
 reato sia di competenza del tribunale o del  pretore,  in  quanto  in    &#13;
 quest'ultima   ipotesi,   dovendosi  procedere  col  rito  ordinario,    &#13;
 l'imputato verrebbe a soffrire tempi di durata maggiore del processo.    &#13;
    2. - La questione è fondata.                                         &#13;
    L'art. 566, nono comma, del codice di procedura penale, prevede  -    &#13;
 con   disposizione   specifica  che  esclude  l'applicabilità  della    &#13;
 generale norma di rinvio di cui all'art. 549 - che  "Fuori  dei  casi    &#13;
 previsti  dai commi precedenti, il pubblico ministero procede a norma    &#13;
 del  titolo  II  del  presente  libro"  (cioè  nei  modi  ordinari).    &#13;
 L'instaurazione  del  giudizio direttissimo dinanzi al pretore viene,    &#13;
 perciò, circoscritta alle sole  ipotesi  disciplinate  nel  medesimo    &#13;
 art.  566: vale a dire quella del giudizio contestuale alla convalida    &#13;
 dell'arresto in flagranza e quella (quinto  comma)  del  giudizio  su    &#13;
 consenso  dell'imputato  e  del pubblico ministero in caso di mancata    &#13;
 convalida.                                                               &#13;
    Rispetto  alla  disciplina  dettata  dall'art.  449  del codice di    &#13;
 procedura penale per il procedimento davanti  al  tribunale,  restano    &#13;
 pertanto   esclusi,  nel  processo  pretorile,  i  casi  di  giudizio    &#13;
 direttissimo previsti dai commi quarto e quinto  di  detto  articolo,    &#13;
 già   contemplati  rispettivamente  nelle  lettere  b)  e  c)  della    &#13;
 direttiva n. 43 della legge-delega 16 febbraio  1987,  n.  81:  cioè    &#13;
 quello  del  giudizio separato (entro quindici giorni dall'arresto in    &#13;
 flagranza)  dalla  convalida   dell'arresto   e   quello,   che   qui    &#13;
 particolarmente  interessa,  al  quale  il  pubblico  ministero  può    &#13;
 procedere - per una udienza non  successiva  al  quindicesimo  giorno    &#13;
 dalla iscrizione nel registro delle notizie di reato - "nei confronti    &#13;
 della persona che nel corso dell'interrogatorio ha reso confessione".    &#13;
    In  ordine  alla  descritta  disciplina  del rito direttissimo nel    &#13;
 procedimento davanti al pretore, dai lavori preparatori  risulta  che    &#13;
 le  ragioni  della  mancata  previsione in tale sede delle ipotesi da    &#13;
 ultimo  indicate  risiedono,  in  generale,  nella  peculiarità  del    &#13;
 procedimento   pretorile,   informato   a   criteri  di  snellezza  e    &#13;
 speditezza, con conseguente tendenza alla  massima  disincentivazione    &#13;
 del  dibattimento.  In  particolare,  per  il caso del giudizio entro    &#13;
 quindici giorni dall'arresto si è ritenuto che esso  sarebbe  finito    &#13;
 col   diventare   l'ipotesi  normale,  con  conseguente  arretramento    &#13;
 rispetto alla normativa del codice di procedura penale abrogato (art.    &#13;
 505) e mancato rispetto del principio della  massima  semplificazione    &#13;
 del  procedimento  di  pretura (direttiva n. 103 della legge-delega),    &#13;
 nel quale la regola è  invece  la  contestualità  tra  convalida  e    &#13;
 giudizio direttissimo. Quanto al caso di confessione, si è osservato    &#13;
 che  tale  forma  di  giudizio  direttissimo rimaneva "assorbita - se    &#13;
 così si può dire  -  dalle  forme  di  definizione  anticipata  del    &#13;
 procedimento,  che  nel processo di pretura saranno il rito ordinario    &#13;
 in  caso  appunto  di  evidenza  della   prova   o   di   confessione    &#13;
 dell'imputato",  e la sua adozione è parsa "non praticabile" poiché    &#13;
 "il  procedimento  pretorile  -  celere  e  snello  -   consente   di    &#13;
 raggiungere  risultati sostanzialmente analoghi" (cfr., in tal senso,    &#13;
 rispettivamente, le relazioni al  progetto  preliminare  e  al  testo    &#13;
 definitivo).                                                             &#13;
    3.  -  Premesso  che  il  nuovo  codice  di  procedura  penale  è    &#13;
 caratterizzato da un evidente favor nei  confronti  dei  procedimenti    &#13;
 differenziati, nell'ambito dei quali, in particolare, le possibilità    &#13;
 di  ricorso  al giudizio direttissimo sono state ampliate rispetto al    &#13;
 codice  abrogato,   anche   in   considerazione   della   "accentuata    &#13;
 caratterizzazione  (di  tale  giudizio)  sugli  schemi  del  processo    &#13;
 accusatorio" (v. relazione al progetto preliminare), va osservato che    &#13;
 l'esclusione nel processo di pretura del rito direttissimo in  alcune    &#13;
 ipotesi  andrebbe sorretta, onde evitare ingiuste discriminazioni, da    &#13;
 una adeguata ratio, strettamente  connessa  con  la  struttura  e  le    &#13;
 caratteristiche   del   processo  pretorile  volute  dal  legislatore    &#13;
 delegante. Ora, le motivazioni sopra riportate che hanno  indotto  il    &#13;
 legislatore delegato ad escludere in tale processo il ricorso al rito    &#13;
 direttissimo  nel  caso di confessione (che è la sola ipotesi qui in    &#13;
 discussione, anche sotto il profilo della rilevanza  nel  giudizio  a    &#13;
 quo)  si rivelano inadeguate allo scopo, anche perché basate in gran    &#13;
 parte su considerazioni meramente empiriche.                             &#13;
    In  realtà,  non  sono  rinvenibili  nel  sistema  valide ragioni    &#13;
 giustificative della disciplina impugnata. Il  giudizio  direttissimo    &#13;
 assomma  in  sé i vantaggi (che tali sono innanzitutto per lo stesso    &#13;
 imputato) della estrema celerità, dovendosi l'udienza dibattimentale    &#13;
 tenere - nel caso in esame - entro  quindici  giorni  dall'iscrizione    &#13;
 della  notizia  di reato nell'apposito registro, e della pubblicità:    &#13;
 l'aver precluso al pubblico ministero presso la pretura, nel caso  di    &#13;
 particolare   evidenza  della  prova  qual  è  quello  dell'avvenuta    &#13;
 confessione  dell'imputato,  la  possibilità  di  ottenere  un  tale    &#13;
 rapidissimo  sbocco dibattimentale del processo, nonché all'imputato    &#13;
 stesso  di  usufruirne,  è  una  scelta  che  non   può   ritenersi    &#13;
 razionalmente  sorretta dalla considerazione (addotta nella relazione    &#13;
 al progetto preliminare) secondo cui il rito  in  questione  verrebbe    &#13;
 assorbito  dalle forme di definizione anticipata del procedimento, le    &#13;
 quali presentano, invece, caratteristiche del  tutto  differenti.  La    &#13;
 norma    impugnata    si    rivela,    dunque,    irragionevole    ed    &#13;
 ingiustificatamente   discriminatoria   di    identiche    situazioni    &#13;
 processuali,   e   pertanto,   pur   prescindendo  dalla  sua  dubbia    &#13;
 compatibilità con le direttive  nn.  43  e  103  della  legge-delega    &#13;
 (questione  non prospettata dal giudice remittente), ne va dichiarata    &#13;
 l'illegittimità costituzionale in parte qua per violazione dell'art.    &#13;
 3 della Costituzione, con conseguente  assorbimento  del  profilo  di    &#13;
 censura relativo all'art. 24 della Costituzione stessa.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara l'illegittimità costituzionale dell'art. 566, nono comma,    &#13;
 del   codice   di  procedura  penale,  nella  parte  in  cui  esclude    &#13;
 l'applicabilità dell'art. 449, quinto comma, dello stesso codice.       &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 27 febbraio 1991.                             &#13;
                         Il Presidente: GALLO                             &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria l'11 marzo 1991.                            &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
