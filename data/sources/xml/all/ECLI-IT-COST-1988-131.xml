<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>131</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:131</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/01/1988</data_decisione>
    <data_deposito>02/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco P. CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 11 della legge    &#13;
 17 dicembre 1986, n. 880 (Revisione delle aliquote dell'imposta sulle    &#13;
 successioni e donazioni), promossi con ordinanze emesse il 23 gennaio    &#13;
 ed il 19 febbraio 1987 dalla Commissione Tributaria  di  I  grado  di    &#13;
 Vasto,  iscritte  ai  nn.  223  e  224  del registro ordinanze 1987 e    &#13;
 pubblicate nella Gazzetta Ufficiale della  Repubblica  n.  25,  prima    &#13;
 serie speciale dell'anno 1987;                                           &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 dicembre 1987 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  nel corso di un procedimento iniziato da Marchesani    &#13;
 Grazia ed avente ad oggetto l'accertamento di valore di  alcuni  beni    &#13;
 caduti  in  successione  in  data  28  agosto  1984,  la  Commissione    &#13;
 tributaria di primo grado di Vasto con ordinanza del 23 gennaio  1987    &#13;
 (reg.ord.  n.223 del 1987) sollevava, in riferimento agli artt. 3, 53    &#13;
 e 97 Cost., questione di legittimità costituzionale dell'art. 11, l.    &#13;
 17  dicembre  1986, n. 880, che esclude l'applicazione dei criteri di    &#13;
 determinazione del  valore  degli  immobili,  previsti  nella  stessa    &#13;
 legge, per le successioni aperte prima del 1° luglio 1986;               &#13;
      che  la  Commissione osservava come questi nuovi criteri fossero    &#13;
 previsti nell'art. 8, l. cit., modificativo dell'art.  26  d.P.R.  26    &#13;
 ottobre  1972,  n. 637, e fossero altresì analoghi a quelli previsti    &#13;
 nell'art. 52 d.P.R. 26 aprile 1986, n. 131 per l'imposta di registro;    &#13;
      che,  peraltro,  mentre per quest'ultima l'art. 79 d.P.R. n. 131    &#13;
 del 1986 stabiliva  doversi  applicare  le  nuove  disposizioni  più    &#13;
 favorevoli  ai  contribuenti  anche per atti anteriori all'entrata in    &#13;
 vigore  dello  stesso  d.P.R.  (e   purché   pendesse   ancora   una    &#13;
 controversia   o   l'Amministrazione  finanziaria  non  fosse  ancora    &#13;
 decaduta dall'azione), il denunciato art.  11  l.  n.  880  del  1986    &#13;
 escludeva,  ai  fini  dell'imposta  sulle successioni, tale efficacia    &#13;
 retroattiva. Ciò determinava, ad avviso della  Commissione:  a)  che    &#13;
 l'ammontare dell'imposta di successione variava a seconda del momento    &#13;
 in   cui   fosse   morto   il   de   cuius,   così   discriminandosi    &#13;
 accidentalmente,  e  perciò  irragionevolmente,  i  contribuenti (v.    &#13;
 artt. 3 e 53 Cost.); b) che l'Amministrazione finanziaria non  poteva    &#13;
 comportarsi  imparzialmente,  dovendo valutare diversamente lo stesso    &#13;
 bene a  seconda  che  si  trattasse  di  imposta  di  registro  o  di    &#13;
 successione  (art. 97 Cost.); c) che l'art. 51 d.P.R. n. 131 del 1986    &#13;
 disponeva, per la  valutazione  dei  beni  ai  fini  dell'imposta  di    &#13;
 registro,   doversi  aver  riguardo  "ai  trasferimenti  a  qualsiasi    &#13;
 titolo... anteriori di non oltre tre anni alla data dell'atto (scil.:    &#13;
 tassato)",  restando  così  incerto  se dovesse farsi riferimento ai    &#13;
 trasferimenti tra vivi o  a  causa  di  morte:  ciò  che  dimostrava    &#13;
 ulteriormente l'irragionevolezza della norma impugnata;                  &#13;
      che  le  stesse  questioni  venivano  sollevate  dalla  medesima    &#13;
 Commissione tributaria con ordinanza del 19 febbraio  1987  (reg.ord.    &#13;
 n. 224 del 1987), emessa nel procedimento iniziato da Rocchio Ada;       &#13;
      che  la  Presidenza  del  Consiglio dei ministri, intervenuta in    &#13;
 entrambi i giudizi, chiedeva  dichiararsi  la  non  fondatezza  delle    &#13;
 questioni;                                                               &#13;
    Considerato  che  per  l'identità delle questioni i giudizi vanno    &#13;
 riuniti;                                                                 &#13;
      che  nulla  impedisce al legislatore di valutare in modo diverso    &#13;
 lo stesso bene ai fini di imposizioni, quali quelle di registro e  di    &#13;
 successioni, aventi differenti presupposti e finalità;                  &#13;
      che,  come  questa  Corte ha già osservato, la discrezionalità    &#13;
 del legislatore è  tanto  più  ampia  quando  trattisi  di  dettare    &#13;
 disposizioni  transitorie  (sent.  n.  171  del  1985);  va  peraltro    &#13;
 osservato che l'impugnato art. 11 l. n.  880  del  1986  non  dispone    &#13;
 semplicemente  l'irretroattività  dei  criteri  di valutazione degli    &#13;
 immobili stabiliti  nella  stessa  legge,  ma  favorisce  altresì  i    &#13;
 contribuenti,  disponendo che per le successioni aperte anteriormente    &#13;
 al 1° luglio 1986, purché non vi  sia  accertamento  definitivo,  il    &#13;
 valore  dell'immobile  potrà essere determinato per adesione con una    &#13;
 riduzione pari al 30 per cento dell'accertato;                           &#13;
      che  il riferimento all'art. 51 d.P.R. n. 131 del 1986 - oltre a    &#13;
 non risultare motivato in punto di rilevanza - non considera l'intero    &#13;
 testo  dello  stesso  art.  51,  il  quale  non  impone  agli  uffici    &#13;
 finanziari  di  tener  conto  in  ogni  caso  e  automaticamente  dei    &#13;
 trasferimenti  avvenuti  nel  triennio anteriore all'atto tassato, ma    &#13;
 attribuisce loro ampia discrezionalità nella  valutazione  del  bene    &#13;
 trasferito, avendo riguardo "ad ogni altro elemento di valutazione";     &#13;
      che  in definitiva le questioni si rivelano tutte manifestamente    &#13;
 inammissibili;                                                           &#13;
    Visti  gli  artt.  26,  l.  11  marzo  1953,  n.87 e 9 delle Norme    &#13;
 integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i   giudizi,  dichiara  manifestamente  inammissibili  le    &#13;
 questioni di legittimità costituzionale dell'art. 11, l. 15 dicembre    &#13;
 1986,  n.  880,  sollevate in riferimento agli artt. 3, 53 e 97 Cost.    &#13;
 dalla Commissione tributaria di primo grado di Vasto con le ordinanze    &#13;
 indicate in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 febbraio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
