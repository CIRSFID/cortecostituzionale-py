<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>41</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:41</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/01/1987</data_decisione>
    <data_deposito>05/02/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. &#13;
 Giuseppe BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, &#13;
 prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>Nei  giudizi  di  legittimità costituzionale degli artt. 23, 29 e    &#13;
 243 del d.P.R. 29 gennaio 1958, n. 645 ("Approvazione del T.U.  delle    &#13;
 leggi  sulle  imposte  dirette"),  promossi  con ordinanze emesse l'8    &#13;
 maggio 1979 dalla Commissione tributaria di primo grado  di  Isernia,    &#13;
 il  29  ottobre  1980  dal Tribunale di Udine e il 5 marzo 1981 dalla    &#13;
 Commissione tributaria di secondo  grado  di  Imperia  (n.  3  ord.),    &#13;
 iscritte rispettivamente al n. 609 del registro ordinanze 1979, al n.    &#13;
 852 del registro ordinanze 1980 ed ai nn. 619, 620 e 621 del registro    &#13;
 ordinanze   1981;   e   pubblicate  nella  Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 310 del 13 novembre 1979; n. 56 del 25 febbraio 1981  e    &#13;
 n. 12 del 13 gennaio 1982;                                               &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  Camera di consiglio dell'11 dicembre 1986 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto che la Commissione tributaria di 1° grado di Isernia, con    &#13;
 ordinanza emessa l'8 maggio 1979 (Registro ordinanze n.  609/79),  ha    &#13;
 sollevato  questione  di legittimità costituzionale, con riferimento    &#13;
 all'art. 3 Cost., dell'art. 29, ultimo comma,  del  T.U.  29  gennaio    &#13;
 1958,  n.  645  ("Approvazione  del  T.U.  delle  leggi sulle imposte    &#13;
 dirette") in relazione agli artt. 21, 22, 23 e 33 dello stesso  T.U.,    &#13;
 nella  parte  in cui non limita gli effetti del tardivo inoltro della    &#13;
 dichiarazione  dei   redditi   all'ufficio   competente,   da   parte    &#13;
 dell'ufficio  II.DD.  cui  sia  stata irregolarmente presentata, alla    &#13;
 mera decorrenza del termine  per  la  rettifica  della  dichiarazione    &#13;
 medesima;                                                                &#13;
      che  il  Tribunale  di Udine, con ordinanza emessa il 29 ottobre    &#13;
 1980 (Reg. ord. n. 852/80), ha sollevato  questione  di  legittimità    &#13;
 costituzionale,  con riferimento agli artt. 3 e 27 Cost., degli artt.    &#13;
 29, ultimo comma, e 243 del medesimo d.P.R. 29 gennaio 1958, n.  645,    &#13;
 nella  parte  in cui disciplinano e sanzionano con le stesse pene sia    &#13;
 l'ipotesi di omessa denuncia dei  redditi  sia  ipotesi  di  denuncia    &#13;
 presentata   in   termine   ad   ufficio   incompetente  e  pervenuta    &#13;
 tardivamente a quello competente  per  motivi  non  collegabili  alla    &#13;
 volontà del contribuente;                                               &#13;
      che  la  Commissione tributaria di secondo grado di Imperia, con    &#13;
 tre ordinanze emesse il 5 marzo 1981 (Registro ordinanze nn. 619, 620    &#13;
 e   621/81),   ha   sollevato   analoga   questione  di  legittimità    &#13;
 costituzionale, con riferimento all'art. 3 Cost., degli artt. 23,  29    &#13;
 e  243  del  d.P.R.  29  gennaio  1958,  n.  645,  nella parte in cui    &#13;
 prevedono  lo  stesso  trattamento  fiscale  e  penale  sia  per   il    &#13;
 contribuente  che  presenta  nei  termini  ad ufficio incompetente la    &#13;
 dichiarazione dei redditi e  sia  per  il  contribuente  che  non  la    &#13;
 presenta affatto o la presenta dopo un mese dalla scadenza;              &#13;
    Considerato  che  per  l'identità  o connessione delle rispettive    &#13;
 questioni i giudizi debbono essere riuniti;                              &#13;
      che  la  legge  22  dicembre 1980, n. 882, recante "Sanatoria di    &#13;
 irregolarità formali e di minori infrazioni in  materia  tributaria"    &#13;
 commesse  sino  al  31 agosto 1980, ha tra l'altro disposto che "sono    &#13;
 considerate valide" le dichiarazioni dei redditi  considerate  omesse    &#13;
 perché  pervenute  all'ufficio  competente  oltre i termini previsti    &#13;
 dalla legge, a  condizione  che  siano  state  presentate,  anche  ad    &#13;
 ufficio  incompetente,  entro  il 31 agosto 1980 (art. 3, comma 1, n.    &#13;
 2);                                                                      &#13;
      che  spetta  alle  autorità  remittenti  stabilire  se la detta    &#13;
 sanatoria, benché testualmente riferita alle dichiarazioni  "di  cui    &#13;
 al  titolo  I  del d.P.R. 29 settembre 1973, n. 600", sia applicabile    &#13;
 anche alle infrazioni commesse in  vigenza  del  precedente  T.U.  29    &#13;
 gennaio  1958, n. 645, con l'effetto, in caso positivo, di rendere le    &#13;
 questioni irrilevanti;                                                   &#13;
      che,  di conseguenza, si rende necessario restituire gli atti ai    &#13;
 giudici a quibus  perché  accertino,  alla  stregua  della  predetta    &#13;
 normativa, se le questioni sollevate siano tuttora rilevanti;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 riuniti  i  giudizi, ordina la restituzione degli atti alle autorità    &#13;
 giudiziarie indicate in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 29 gennaio 1987.        &#13;
                     Il Presidente: LA PERGOLA                            &#13;
                      Il Redattore: DELL'ANDRO                            &#13;
    Depositata in cancelleria il 5 febbraio 1987.                         &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
