<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>334</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:334</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2000</data_decisione>
    <data_deposito>24/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO; Massimo VARI; &#13;
 Cesare RUPERTO; Riccardo CHIEPPA; Gustavo ZAGREBELSKY; Valerio &#13;
 ONIDA; Carlo MEZZANOTTE; Fernanda CONTRI; Guido NEPPI MODONA; Piero &#13;
 Alberto CAPOTOSTI; Annibale MARINI; Franco BILE; Giovanni Maria &#13;
 FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  per  conflitto  di  attribuzione  sorto  a seguito del    &#13;
   "comportamento"   assunto   dal   Governo   in   violazione   delle    &#13;
   attribuzioni  spettanti  alla  regione  Puglia  sulle funzioni già    &#13;
   esercitate  dall'Ente  autonomo  per  l'acquedotto  pugliese, ed in    &#13;
   particolare  della  approvazione  da parte del Ministro del tesoro,    &#13;
   del  bilancio  e della programmazione economica dello statuto della    &#13;
   Società  Acquedotto  pugliese  e della nomina di un amministratore    &#13;
   unico  della  predetta Società, promosso con ricorso della regione    &#13;
   Puglia,  notificato  il 6 agosto 1999, depositato in cancelleria il    &#13;
   25 successivo ed iscritto al n. 30 del registro conflitti 1999;        &#13;
     Udito nell'udienza pubblica del 9 maggio 2000 il giudice relatore    &#13;
   Piero Alberto Capotosti;                                               &#13;
     Udito  l'avvocato  Vincenzo  Caputi  Jambrenghi  per  la  regione    &#13;
   Puglia;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - La regione Puglia, con ricorso notificato il 6 agosto 1999,    &#13;
   ha  promosso conflitto di attribuzione nei confronti dello Stato in    &#13;
   relazione  al  "comportamento"  del  Presidente  del  Consiglio dei    &#13;
   Ministri,  del  Ministro  del  tesoro  e  del  Ministro  dei lavori    &#13;
   pubblici,  per violazione delle attribuzioni spettanti alla regione    &#13;
   Puglia  secondo  gli  artt.  117  e  118 della Costituzione, "sulla    &#13;
   attività,  le  funzioni  e la gestione esercitate sinora dall'Ente    &#13;
   autonomo acquedotto pugliese", e, in particolare, in relazione alla    &#13;
   "approvazione da parte del Ministro del tesoro dello statuto" della    &#13;
   Società  per  azioni  Acquedotto pugliese e della "nomina da parte    &#13;
   del medesimo Ministro di un amministratore unico".                     &#13;
     La  Regione chiede che la Corte dichiari non spettante allo Stato    &#13;
   ma  alla regione Puglia "l'adozione dei provvedimenti necessari per    &#13;
   organizzare  e  realizzare  la gestione delle risorse idriche anche    &#13;
   con   riferimento   specifico   a  quelle  dell'Ente  autonomo  per    &#13;
   l'acquedotto    pugliese"    e    conseguentemente    annulli   "la    &#13;
   determinazione amministrativa di approvazione da parte del Ministro    &#13;
   del   tesoro"  della  clausola  statutaria  che  riserva  tutte  le    &#13;
   attività  suddette  alla  Società  Acquedotto  pugliese;  nonché    &#13;
   annulli   la  nomina  da  parte  del  Ministro  del  tesoro  di  un    &#13;
   amministratore  unico  "anziché di un consiglio di amministrazione    &#13;
   che  avrebbe  potuto  ricomprendere  rappresentanti  della  regione    &#13;
   Puglia".                                                               &#13;
                                                                          &#13;
     2.  -  La  ricorrente espone che il decreto legislativo 11 maggio    &#13;
   1999, n. 141 - avverso il quale la regione ha promosso questione di    &#13;
   legittimità   costituzionale  -  ha  disposto  la  privatizzazione    &#13;
   dell'Ente  autonomo  per l'acquedotto pugliese, e che in attuazione    &#13;
   della  procedura ivi prevista il 2 luglio 1999 si è tenuta in Roma    &#13;
   la   prima   assemblea   della   costituenda  Società  "Acquedotto    &#13;
   pugliese". In tale occasione il Ministro del tesoro, nella qualità    &#13;
   di  unico  azionista  della società, ha provveduto ad approvare lo    &#13;
   statuto  della  società stessa, nonché ha deliberato la nomina di    &#13;
   un amministratore unico nella persona del commissario straordinario    &#13;
   del  disciolto  Ente  autonomo.  Secondo la ricorrente, che ricorda    &#13;
   come  la  struttura  acquedottistica in questione sia stata gestita    &#13;
   lungo  tutto  il  novecento  da  amministratori di emanazione dello    &#13;
   Stato e delle province originarie di Bari, Foggia e Lecce, nonché,    &#13;
   successivamente anche di Taranto, Brindisi e Potenza, nessuna norma    &#13;
   di  legge avrebbe previsto "l'estromissione completa" della regione    &#13;
   Puglia   dall'amministrazione   della   nuova   società,   con  la    &#13;
   conseguenza  che  il  Ministro  del tesoro, evitando di nominare un    &#13;
   Consiglio  di  amministrazione  al  quale avrebbero potuto prendere    &#13;
   parte   anche   rappresentanti  delle  autonomie  locali,  "avrebbe    &#13;
   sorpassato  i limiti imposti dall'ordinamento generale nel rapporto    &#13;
   Stato-regioni".                                                        &#13;
     Ulteriore  violazione  delle  prerogative  regionali  deriverebbe    &#13;
   inoltre   dalla   circostanza   che  nella  definizione  statutaria    &#13;
   dell'oggetto   della   nuova  società  farebbe  difetto  qualsiasi    &#13;
   riferimento  alla  legge  5  gennaio  1994,  n. 36 (Disposizioni in    &#13;
   materia  di  risorse  idriche),  con  la conseguenza che la regione    &#13;
   resterebbe  esposta  ad  ogni  decisione unilaterale della Società    &#13;
   nella  gestione  delle  acque  fuori  e dentro gli ambiti ottimali,    &#13;
   affidati  invece  alle  regioni  dal  predetto  testo  legislativo.    &#13;
   Inoltre  la  medesima  clausola  statutaria  conterrebbe  anche, ad    &#13;
   avviso  della ricorrente, una "riserva" a favore della società per    &#13;
   azioni  in  relazione  ai  compiti  già affidati al disciolto ente    &#13;
   acquedottistico,  fra i quali, in particolare, quelli relativi alla    &#13;
   "gestione  del servizio idrico integrato", e quelli riguardanti "la    &#13;
   captazione,   l'adduzione,   la  potabilizzazione,  l'accumulo,  la    &#13;
   distribuzione  e  vendita  di  acqua  ad  usi  civili, industriali,    &#13;
   commerciali  ed  agricoli",  la  quale  comporterebbe una ulteriore    &#13;
   violazione  delle  attribuzioni  regionali  predette  nonché degli    &#13;
   artt.  90.1  e  6 del trattato istitutivo della Comunità europea e    &#13;
   dell'art. 8  della  legge  1°  ottobre  1990,  n. 287 (Norme per la    &#13;
   tutela della concorrenza e del mercato).                               &#13;
                                                                          &#13;
     3.  -  In  prossimità dell'udienza, la regione ha depositato una    &#13;
   memoria  difensiva  nella  quale,  dopo  avere  ricordato  il ruolo    &#13;
   determinante  delle  istanze locali nella istituzione del Consorzio    &#13;
   per  l'acquedotto pugliese prima e del corrispondente Ente autonomo    &#13;
   poi,  ha  sottolineato  come  non  vi  sia  servizio  pubblico più    &#13;
   collegato  al  territorio  di  quello  che abbia ad oggetto l'acqua    &#13;
   potabile,  donde  deriverebbe la "ragione ultima della demanialità    &#13;
   dei  beni  acquedottistici".  L'estromissione  della regione Puglia    &#13;
   dalla  nuova  struttura organizzatoria dell'acquedotto pugliese, ad    &#13;
   avviso  della  ricorrente,  si  porrebbe quindi in contrasto con il    &#13;
   principio  del  federalismo amministrativo e con l'affidamento alle    &#13;
   regioni, da parte della legislazione più recente ed in particolare    &#13;
   del  decreto  legislativo 30 marzo 1999, n. 96, della competenza in    &#13;
   materia  "di  opere idrauliche di qualsiasi natura (...) e, più in    &#13;
   generale, di gestione del demanio idrico".<diritto>Considerato in diritto</diritto>1.  - Il conflitto di attribuzione sollevato dalla regione Puglia    &#13;
   nei  confronti dello Stato ha ad oggetto il "comportamento" assunto    &#13;
   dagli  organi di governo in violazione delle attribuzioni spettanti    &#13;
   alla  regione  Puglia  "sulla  attività, le funzioni e la gestione    &#13;
   esercitate  sinora  dall'Ente autonomo per l'acquedotto pugliese" e    &#13;
   in  particolare  in  relazione  alla  "approvazione  da  parte  del    &#13;
   Ministro del tesoro dello statuto della Società per azioni e della    &#13;
   nomina  da parte del medesimo Ministro di un amministratore unico",    &#13;
   in   occasione   della   prima  assemblea  della  stessa  società,    &#13;
   conseguente al procedimento di privatizzazione disposto dal decreto    &#13;
   legislativo n. 141 del 1999.                                           &#13;
     Secondo  la Regione ricorrente sarebbe in particolare censurabile    &#13;
   l'approvazione  della  clausola  dello  statuto  della Società per    &#13;
   azioni  Acquedotto  pugliese,  che  "riserva" ad essa le attività,    &#13;
   già  proprie  del  disciolto  Ente,  inerenti  alla  "gestione del    &#13;
   servizio  idrico  integrato",  nonché delle ulteriori clausole che    &#13;
   prevedono  rispettivamente la "riserva di azionariato" al Ministero    &#13;
   del  tesoro  e la nomina di un amministratore unico, anziché di un    &#13;
   consiglio  di amministrazione al quale avrebbero potuto partecipare    &#13;
   anche rappresentanti delle autonomie locali.                           &#13;
                                                                          &#13;
     2. - Il ricorso è inammissibile.                                    &#13;
     La    Regione   ricorrente   sostanzialmente   si   duole   della    &#13;
   "connotazione   giuridica   esclusivamente  statale"  della  S.p.a.    &#13;
   Acquedotto pugliese e della conseguente estromissione della regione    &#13;
   Puglia dall'amministrazione e dalla gestione della società stessa.    &#13;
   E,  in  questa  ottica,  in particolare articola le proprie censure    &#13;
   sulle   clausole  statutarie  che  rispettivamente  riservano  alla    &#13;
   società  stessa  i  compiti  relativi  alla "gestione del servizio    &#13;
   idrico integrato", nonché attribuiscono al Ministero del tesoro la    &#13;
   totalità delle azioni e la nomina di un amministratore unico.         &#13;
     Si tratta, però, di censure che riguardano propriamente non già    &#13;
   lo  statuto  della  Società Acquedotto pugliese, bensì il decreto    &#13;
   legislativo  11 maggio 1999, n. 141, che ha appunto disciplinato la    &#13;
   trasformazione  del  preesistente Ente autonomo acquedotto pugliese    &#13;
   in  società  per  azioni.  Ed  invero  è  l'art. 2  del decreto a    &#13;
   prevedere  espressamente  l'affidamento  alla  società "Acquedotto    &#13;
   pugliese"   delle  finalità  già  attribuite  al  disciolto  Ente    &#13;
   autonomo,  nonché  dei  compiti  relativi alla "gestione del ciclo    &#13;
   integrato dell'acqua e, in particolare, alla captazione, adduzione,    &#13;
   potabilizzazione, distribuzione di acqua a usi civili". È evidente    &#13;
   pertanto  che  la  censurata  clausola  4.1.  dello  statuto  della    &#13;
   società  non  rappresenta altro che una trascrizione del contenuto    &#13;
   normativo del predetto art. 2 del citato decreto.                      &#13;
     Allo  stesso  modo  è  l'art. 3 dello stesso decreto legislativo    &#13;
   che,  al  comma  2,  stabilisce  che  "le azioni sono attribuite al    &#13;
   Ministero   del   tesoro,   del  bilancio  e  della  programmazione    &#13;
   economica,  che  esercita  i diritti dell'azionista" e, al comma 4,    &#13;
   prevede  che  l'organo  gestorio  della  società  possa  essere un    &#13;
   consiglio   di   amministrazione  o  un  amministratore  unico.  La    &#13;
   censurata  clausola  statutaria  pertanto si mantiene rigorosamente    &#13;
   all'interno  di  un'opzione  già compiutamente prefigurata in modo    &#13;
   esplicito dal legislatore.                                             &#13;
     In  definitiva,  il  ricorso  in  esame prospetta censure che, in    &#13;
   realtà, riguardano norme di legge, delle quali i "comportamenti" e    &#13;
   le  determinazioni  impugnati  costituiscono applicazione. Essi non    &#13;
   hanno,  dunque,  di per sé, autonoma attitudine lesiva della sfera    &#13;
   di attribuzione regionale costituzionalmente garantita, dal momento    &#13;
   che  il  disposto  legislativo  predetermina,  come  si  è  detto,    &#13;
   presupposti, contenuto e forme applicative di essi (sentenze n. 277    &#13;
   del   1998   e   n. 467   del  1997).  Ma  secondo  la  consolidata    &#13;
   giurisprudenza  costituzionale,  si  deve  escludere che in sede di    &#13;
   conflitto  di  attribuzioni  tra  regione  e  Stato  sia  possibile    &#13;
   impugnare  atti  (o  "comportamenti"),  al solo scopo di far valere    &#13;
   pretese  violazioni della Costituzione da parte della legge, che è    &#13;
   a  fondamento  dei poteri svolti con gli atti (o i "comportamenti")    &#13;
   impugnati  (sentenze  n. 467  del 1997, n. 215 del 1996, n. 472 del    &#13;
   1995).</testo>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara  inammissibile  il  conflitto  di  attribuzione promosso    &#13;
   dalla  regione  Puglia  nei  confronti  dello  Stato con il ricorso    &#13;
   indicato in epigrafe.                                                  &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
   Palazzo della Consulta, il 12 luglio 2000.                             &#13;
                       Il Presidente: Mirabelli                           &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 24 luglio 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
