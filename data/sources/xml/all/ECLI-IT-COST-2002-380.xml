<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>380</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:380</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia>Gustavo Zagrebelsky</redattore_pronuncia>
    <data_decisione>10/07/2002</data_decisione>
    <data_deposito>23/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  per  conflitto  di  attribuzione sorto a seguito della &#13;
deliberazione  della Giunta della Regione Trentino-Alto Adige n. 2327 &#13;
del   27 ottobre  1994,  recante  "Recepimento  normativa  risultante &#13;
dall'accordo   sindacale  di  data  20 ottobre  1994  riguardante  il &#13;
personale  non  dirigente  della Regione e delle Camere di Commercio, &#13;
Industria,  Artigianato  e  Agricoltura  di  Trento  e  di  Bolzano e &#13;
contenente  prime  misure  di omogeneizzazione interna al comparto in &#13;
attuazione  dell'articolo 6 dell'accordo sindacale 3 febbraio 1994 in &#13;
attesa   della  futura  contrattazione",  promosso  con  ricorso  del &#13;
Presidente del Consiglio dei ministri, notificato il 28 gennaio 1995, &#13;
depositato  in  cancelleria il 3 febbraio 1995 e iscritto al n. 3 del &#13;
registro conflitti 1995. &#13;
    Visto l'atto di costituzione della Regione Trentino-Alto Adige; &#13;
    Udito nell'udienza pubblica del 4 giugno 2002 il Giudice relatore &#13;
Gustavo Zagrebelsky; &#13;
    Uditi  l'avvocato dello Stato Maurizio Fiorilli per il Presidente &#13;
del Consiglio dei ministri e gli avvocati Giandomenico Falcon e Luigi &#13;
Manzi per la Regione Trentino-Alto Adige. &#13;
    Ritenuto   che  con  ricorso  notificato  il  28 gennaio  1995  e &#13;
depositato  il successivo 3 febbraio, il Presidente del Consiglio dei &#13;
ministri  ha  sollevato conflitto di attribuzioni nei confronti della &#13;
Regione  Trentino-Alto  Adige,  in relazione alla deliberazione della &#13;
giunta  regionale  27 ottobre  1994,  n. 2327  (Recepimento normativa &#13;
risultante dall'accordo sindacale di data 20 ottobre 1994 riguardante &#13;
il personale non dirigente della Regione e delle Camere di Commercio, &#13;
Industria,  Artigianato  e  Agricoltura  di  Trento  e  di  Bolzano e &#13;
contenente  prime  misure  di omogeneizzazione interna al comparto in &#13;
attuazione  dell'articolo 6 dell'accordo sindacale 3 febbraio 1994 in &#13;
attesa  della futura contrattazione), assumendo il contrasto di detta &#13;
delibera  con  gli  artt. 15,  17,  18  e  19 del decreto legislativo &#13;
10 novembre   1993,   n. 470  (Disposizioni  correttive  del  decreto &#13;
legislativo   3 febbraio   1993,   n. 29,  recante  razionalizzazione &#13;
dell'organizzazione delle amministrazioni pubbliche e revisione della &#13;
disciplina   in   materia   di   pubblico  impiego),  e  chiedendone, &#13;
conseguentemente, l'annullamento; &#13;
        che  il  Governo  rileva che il provvedimento per il quale è &#13;
insorto  il conflitto è stato emanato sulla base di una legislazione &#13;
della  Regione  Trentino-Alto Adige in materia di ordinamento e stato &#13;
giuridico  ed  economico  del  personale  regionale  che non è stata &#13;
adeguata,  nei  modi e nei termini prescritti dal decreto legislativo &#13;
16 marzo 1992, n. 266 (Norme di attuazione dello statuto speciale per &#13;
il  Trentino-Alto  Adige concernenti il rapporto tra atti legislativi &#13;
statali  e leggi regionali e provinciali, nonché la potestà statale &#13;
di   indirizzo  e  coordinamento),  alla  legislazione  nazionale  di &#13;
principio, costituita (a) dalla legge 23 ottobre 1992, n. 421 (Delega &#13;
al  Governo  per la razionalizzazione e la revisione delle discipline &#13;
in  materia  di  sanità,  di  pubblico  impiego,  di previdenza e di &#13;
finanza  territoriale),  (b) dal decreto legislativo 3 febbraio 1993, &#13;
n. 29  (Razionalizzazione  dell'organizzazione  delle amministrazioni &#13;
pubbliche  e  revisione  della  disciplina  in  materia  di  pubblico &#13;
impiego),   nonché  (c)  dai  decreti  legislativi,  correttivi  del &#13;
precedente,  n. 470  del  1993  e 23 dicembre 1993, n. 546 (Ulteriori &#13;
modifiche al decreto legislativo 3 febbraio 1993, n. 29, sul pubblico &#13;
impiego); &#13;
        che,  in  particolare, il ricorrente, richiamata la procedura &#13;
prevista  per  la  contrattazione collettiva nel pubblico impiego con &#13;
l'intervento  della  Agenzia  per  la  rappresentanza negoziale delle &#13;
pubbliche  amministrazioni di cui all'art. 15 del decreto legislativo &#13;
n. 470  del  1993  -  che  prevede  che  i  contratti  concernenti  i &#13;
"comparti"  del  personale  regionale,  provinciale e comunale devono &#13;
essere  preceduti  da un contratto collettivo nazionale stipulato tra &#13;
l'Agenzia e le organizzazioni sindacali maggiormente rappresentative, &#13;
secondo  uno schema il cui fine principale è quello del contenimento &#13;
della spesa pubblica del personale regionale e degli enti locali, con &#13;
la   riserva   allo  Stato  della  competenza  alla  definizione  del &#13;
trattamento  economico  uniforme  di  detto  personale  su  tutto  il &#13;
territorio nazionale - e rilevato che tra le parti è stato stipulato &#13;
un  protocollo  d'intesa,  approvato  con  d.P.C.m.  28 aprile  1994, &#13;
osserva  (a)  che  la delibera per cui è promosso il conflitto rende &#13;
evidente che la contrattazione in essa considerata è disciplinata da &#13;
atti  amministrativi attuativi della legislazione regionale, la quale &#13;
però non è stata adeguata ai principi della legislazione statale in &#13;
materia,   (b)  che  per  questo  mancato  adeguamento  è  stato  in &#13;
precedenza promosso, dallo stesso Governo, separato ricorso (iscritto &#13;
al  reg.  ricorsi  n. 57  del  1994),  a  norma del citato art. 2 del &#13;
decreto  legislativo  n. 266 del 1992, (c) che l'"esito scontato" del &#13;
giudizio  per  mancato  adeguamento  renderebbe  "evidente"  come  la &#13;
Regione  Trentino-Alto  Adige,  con l'adozione del decreto impugnato, &#13;
abbia   travalicato   i  limiti  della  propria  competenza,  con  il &#13;
recepimento  delle clausole di un accordo stipulato in sede locale al &#13;
quale  la Giunta regionale avrebbe piuttosto dovuto negare efficacia, &#13;
(d)  che,  inoltre,  l'atto  per  cui  è  insorto il conflitto viola &#13;
l'art. 3    della    Costituzione,   introducendo   un'ingiustificata &#13;
disparità  di  trattamento tra i dipendenti pubblici, l'art. 2 della &#13;
Costituzione,  sottraendo  una  categoria  di dipendenti al dovere di &#13;
concorrere  all'interesse  generale,  e l'art. 95 della Costituzione, &#13;
interferendo  gravemente con la direzione politica generale del Paese &#13;
assegnata  al  Governo,  (e)  che  per  le suddette ragioni l'atto in &#13;
questione deve essere annullato; &#13;
        che  nel  giudizio così promosso si è costituita la Regione &#13;
Trentino-Alto  Adige,  chiedendo  il  rigetto  del  ricorso,  perché &#13;
inammissibile e infondato, osservando, in una memoria successivamente &#13;
depositata,  che,  secondo  l'art. 2  della  legge n. 421 del 1992, i &#13;
principi   e   criteri   direttivi   posti  al  legislatore  delegato &#13;
costituiscono,   per  le  regioni  e  le  province  autonome,  "norme &#13;
fondamentali di riforma economico-sociale della Repubblica", e che la &#13;
medesima  formulazione  è  ripetuta  anche nell'art. 1, comma 3, del &#13;
decreto  legislativo  n. 29  del 1993, con la conseguenza, secondo la &#13;
resistente,  che  in nessun momento il legislatore statale ha pensato &#13;
che si dovesse arrivare a una normativa uniforme dell'impiego statale &#13;
e  dell'impiego  nelle  regioni ad autonomia differenziata, dovendosi &#13;
piuttosto ragionare in termini di discipline autonome, nei limiti dei &#13;
principi   di   riforma  contenuti  nella  legge  di  delega,  e  con &#13;
l'ulteriore  conseguenza  che  la  costituzionalità  del  sistema di &#13;
contrattazione  collettiva  istituito  in  Trentino-Alto  Adige  deve &#13;
essere  valutata  con  esclusivo riferimento a quanto stabilito nella &#13;
legge  n. 421  del  1992,  restando estraneo - perché inoperante nei &#13;
confronti  della  Regione  resistente  -  il  contenuto  del  decreto &#13;
legislativo n. 29 del 1993 e relative integrazioni; &#13;
        che,   secondo   le   suddette   argomentazioni,  la  Regione &#13;
Trentino-Alto  Adige  non potrebbe dunque essere ricompresa nell'area &#13;
di  disciplina  del  sistema  di  contrattazione  in tema di pubblico &#13;
impiego  delineato  dalla  legislazione statale, poiché le regioni a &#13;
statuto  speciale,  sostiene  la  resistente, possono - non debbono - &#13;
avvalersi, nella contrattazione, dell'attività dell'Agenzia; &#13;
        che  la  difesa  della  Regione  Trentino-Alto Adige conclude &#13;
osservando  che l'atto regionale nel suo complesso non potrebbe dirsi &#13;
in  contrasto  con  alcun principio di riforma desumibile dalle norme &#13;
statali,  avendo  essa  "semplicemente  preferito,  nell'ambito della &#13;
propria autonomia costituzionale, una diversa via per dare disciplina &#13;
provvisoria a taluni aspetti economici del rapporto con il personale, &#13;
nell'attesa della disciplina definitiva"; &#13;
        che  con successiva memoria, depositata il 15 maggio 2002, la &#13;
Regione  resistente  ha  rilevato  che  nel  citato  giudizio  in via &#13;
principale  per  "mancato  adeguamento"  di  cui al ricorso n. 57 del &#13;
1994, obiettivamente connesso al conflitto in quanto avente a oggetto &#13;
la legislazione regionale sulla base della quale è stata adottata la &#13;
delibera  della  giunta oggetto del conflitto medesimo, il ricorrente &#13;
Presidente  del Consiglio dei ministri ha formulato atto di rinuncia, &#13;
con  il  che  il  giudizio di legittimità costituzionale per mancato &#13;
adeguamento  è comunque destinato a estinguersi, anche se precisa la &#13;
Regione  la  materia  del  contendere  dovrebbe  dirsi già cessata a &#13;
seguito  della  approvazione  della legge regionale del Trentino-Alto &#13;
Adige  21 luglio  2000, n. 3 (Norme urgenti in materia di personale), &#13;
con  la  quale  è  stata  dettata una normativa adeguata ai principi &#13;
della legge statale n. 421 del 1992; &#13;
        che con atto depositato il 20 maggio 2002, l'Avvocatura dello &#13;
Stato,  per il Presidente del Consiglio dei ministri, ha rilevato che &#13;
la  modifica  del  titolo  V  della  parte seconda della Costituzione &#13;
disposta  dalla legge costituzionale 18 ottobre 2001, n. 3 (Modifiche &#13;
al  titolo  V  della  parte seconda della Costituzione), ha mutato il &#13;
rapporto  tra  la  potestà  legislativa  dello  Stato e quella delle &#13;
Regioni,  in  particolare  nel  senso  che  l'interesse nazionale non &#13;
costituisce  più  un  limite generale all'esercizio delle competenze &#13;
legislative  regionali,  e che per questo il ricorso per conflitto di &#13;
attribuzioni,  in  quanto  "conseguenza  immediata del ricorso per il &#13;
mancato  adeguamento  della  legislazione regionale a quella statale" &#13;
fondato sul ricordato principio, "ha perso interesse"; &#13;
        che  pertanto  l'Avvocatura  dello Stato ha dichiarato per il &#13;
Presidente  del  Consiglio  dei ministri, previa conforme delibera di &#13;
quest'ultimo adottata in data 3 maggio 2002, di rinunciare al ricorso &#13;
per conflitto di attribuzioni; &#13;
        che  in  prossimità  dell'udienza  la  difesa  della Regione &#13;
Trentino-Alto  Adige  ha a sua volta depositato, su conforme delibera &#13;
della giunta regionale del 17 aprile 2002, atto di accettazione della &#13;
rinuncia. &#13;
    Considerato  che, a norma dell'art. 27, ultimo comma, delle norme &#13;
integrative  per  i  giudizi  davanti  alla  Corte costituzionale, la &#13;
rinuncia  al  ricorso,  seguita  dalla  relativa  accettazione  della &#13;
controparte, produce l'effetto di estinguere il processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara estinto il processo. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                      Il redattore: Zagrebelsky &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in Cancelleria il 23 luglio 2002. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
