<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>577</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:577</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/12/1987</data_decisione>
    <data_deposito>23/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 12 della legge    &#13;
 9 dicembre 1977, n.903 (Parità di trattamento tra uomini  e  donne),    &#13;
 promosso  con  ordinanza emessa il 2 maggio 1981 dal pretore di Siena    &#13;
 nel procedimento  civile  vertente  tra  Neri  Aldo  e  l'I.N.A.I.L.,    &#13;
 iscritta  al  n.  478  del registro ordinanze 1981 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica n. 297 dell'anno 1981;               &#13;
    Visti  gli atti di costituzione dell'avv. Franco Agostini per Neri    &#13;
 Aldo e dell'I.N.A.I.L., nonché l'atto di intervento  del  Presidente    &#13;
 del Consiglio dei ministri;                                              &#13;
    Udito  nell'udienza  pubblica  del  10  novembre  1987  il giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Uditi   gli   avv.ti   Enrico   Ruffini  e  Antonino  Catania  per    &#13;
 l'I.N.A.I.L. e l'avvocato dello Stato Paolo D'Amico per il Presidente    &#13;
 del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  ordinanza  emessa  il  2  maggio 1981 nel procedimento civile    &#13;
 vertente tra Neri Aldo e l'INAIL (n. 478  del  1981)  il  pretore  di    &#13;
 Siena   ha   sollevato   questione   di  legittimità  costituzionale    &#13;
 dell'art.12  della  l.  9  dicembre  1977,  n.   903,   limitatamente    &#13;
 all'inciso  "deceduto  (recte:  deceduta) posteriormente alla data di    &#13;
 entrata in vigore della presente legge", in riferimento agli artt.  3    &#13;
 e 29 della Costituzione.                                                 &#13;
    Secondo  il giudice a quo, resta limitata irrazionalmente la piena    &#13;
 eguaglianza dei coniugi, in materia di prestazioni erogate  nel  caso    &#13;
 di  infortuni  sul lavoro e malattie professionali, per le ipotesi in    &#13;
 cui il decesso della lavoratrice sia avvenuto anteriormente.             &#13;
    Nel  novembre  del  1981  si  è  costituito  l'INAIL difeso dagli    &#13;
 avvocati Vincenzo Cataldi, Carlo Graziani  ed  Antonino  Catania  per    &#13;
 sostenere  la  conformità  al  precetto  costituzionale  della norma    &#13;
 impugnata.                                                               &#13;
    Analogo l'intervento dell'Avvocatura generale dello Stato.<diritto>Considerato in diritto</diritto>L'ordinanza  si duole della irrazionalità - ex artt. 3 e 29 Cost.    &#13;
 - dell'art.12 legge 9 dicembre 1977, n. 903 (Parità  di  trattamento    &#13;
 tra  uomini e donne in materia di lavoro) per il limite temporale ivi    &#13;
 imposto, nel senso di applicabilità delle prestazioni ai superstiti,    &#13;
 solo  a  seguito di decesso della lavoratrice posteriore alla data di    &#13;
 entrata in vigore della legge.                                           &#13;
    Senonché  la  Corte  ha  già  affermato che le dette prestazioni    &#13;
 spettano, comunque, al marito quale che sia la data del decesso della    &#13;
 moglie   lavoratrice,  dichiarando  in  conseguenza  l'illegittimità    &#13;
 costituzionale  dell'art.12   sovrariferito,   quanto   alle   parole    &#13;
 "deceduta  posteriormente  alla  data  di  entrata  in  vigore  della    &#13;
 presente legge (sentenza n. 117 del 1986).                               &#13;
    La  questione  è  rimasta  priva, pertanto, del relativo supporto    &#13;
 normativo a va dichiarata inammissibile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 della legge 9 dicembre 1977,  n.  903  (Parità  di  trattamento  tra    &#13;
 uomini   e   donne):   art.  12  limitatamente  all'inciso  "deceduta    &#13;
 posteriormente alla data di entrata in vigore della presente  legge",    &#13;
 sollevata  dal  Pretore  di  Siena,  in riferimento agli artt. 3 e 29    &#13;
 Cost., con l'ordinanza in epigrafe.                                      &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta,                            &#13;
 l'11 dicembre 1987.                                                      &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: BORZELLINO                          &#13;
    Depositata in cancelleria il 23 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
