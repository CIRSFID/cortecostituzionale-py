<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>99</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:99</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/02/1990</data_decisione>
    <data_deposito>02/03/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, &#13;
 dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, &#13;
 prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 2, commi 5° ed    &#13;
 ultimo, della legge 3 gennaio  1981,  n.  6  ("Norme  in  materia  di    &#13;
 previdenza  per  gli  ingegneri  e gli architetti"), promossi con sei    &#13;
 ordinanze emesse il 6 giugno 1989 (nn. 4 ord.) dal Pretore di  Latina    &#13;
 e  il  20 luglio 1988 dal Pretore di Milano, iscritte rispettivamente    &#13;
 ai nn. 395, 488, 489, 490, 491 e 500 del registro  ordinanze  1989  e    &#13;
 pubblicate nella Gazzetta Ufficiale della Repubblica nn. 37, 43 e 44,    &#13;
 prima serie speciale, dell'anno 1989;                                    &#13;
    Visto l'atto di costituzione di Vigorelli Gianni;                     &#13;
    Udito  nell'udienza  pubblica  del  17  gennaio  1990  il  Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Udito l'avv. Renato D'Auria per Vigorelli Gianni;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel corso di un giudizio promosso dall'ing. Mario Giorgetti    &#13;
 contro la Cassa Nazionale di  previdenza  per  gli  ingegneri  e  gli    &#13;
 architetti  al fine di ottenere la liquidazione del supplemento della    &#13;
 pensione in misura piena, il Pretore di Udine, con  ordinanza  del  6    &#13;
 giugno   1989,   ha   sollevato,  in  riferimento  all'art.  3  della    &#13;
 Costituzione, questione di legittimità costituzionale  dell'art.  2,    &#13;
 ultimo  comma,  della  legge 3 gennaio 1981, n. 6, nella parte in cui    &#13;
 prevede che il supplemento, spettante dopo cinque anni di  iscrizione    &#13;
 e di contribuzione successivi al pensionamento, sia liquidato in base    &#13;
 alla metà dei coefficienti stabiliti dal primo e dal quinto  (recte:    &#13;
 quarto) comma per la determinazione della pensione.                      &#13;
    Il  giudice  a quo osserva che "il caso in esame appare in tutto e    &#13;
 per tutto  simile  a  quello  relativo  al  supplemento  di  pensione    &#13;
 previsto  per  gli avvocati dall'art. 2, ultimo comma, della legge n.    &#13;
 576 del 1980", caso già risolto dalla sentenza n. 1008 del  1988  di    &#13;
 questa  Corte nel senso dell'illegittimità della norma in questione.    &#13;
    2.  -  Con  quattro ordinanze di eguale tenore del 26 aprile 1989,    &#13;
 emesse nel corso di altrettanti  procedimenti  instaurati  contro  la    &#13;
 detta  Cassa  da  ingegneri  pensionati tuttora iscritti all'albo, il    &#13;
 Pretore di Latina ha sollevato,  in  riferimento  all'art.  3  Cost.,    &#13;
 questione  di  legittimità costituzionale dell'art. 2, quinto comma,    &#13;
 della legge n. 6 del 1981, che prevede la riduzione di un terzo della    &#13;
 pensione  di  vecchiaia  quando  il  titolare resti iscritto all'albo    &#13;
 professionale.                                                           &#13;
    Ad  avviso del giudice remittente la decurtazione è irragionevole    &#13;
 sia in se stessa sia in rapporto  al  diverso  trattamento  di  altre    &#13;
 categorie  professionale  e  all'interno della stessa categoria degli    &#13;
 ingegneri, in rapporto a quelli  che  esercitano  la  professione  in    &#13;
 costanza di un rapporto di lavoro alle dipendenze di un ente pubblico    &#13;
 o di un datore di lavoro privato. Costoro,  infatti,  quando  vengono    &#13;
 collocati  in  quiescenza per raggiunti limiti di età, non subiscono    &#13;
 alcuna diminuzione della pensione pur se rimangono iscritti all'albo.    &#13;
    L'ordinanza  osserva,  inoltre,  che  la  norma impugnata non può    &#13;
 giustificarsi nemmeno alla stregua  del  principio  di  solidarietà,    &#13;
 tenuto  conto  anche  dei rilievi svolti nella richiamata sentenza n.    &#13;
 1008 del 1988 di questa Corte in ordine all'identica norma  contenuta    &#13;
 nell'art. 2, sesto comma, della legge sulla previdenza forense.          &#13;
    3.  -  Sempre  in  riferimento  all'art.  3  Cost. la legittimità    &#13;
 costituzionale dell'art. 2, quinto comma, della legge n. 6  del  1981    &#13;
 è contestata anche dal Pretore di Milano con ordinanza del 20 luglio    &#13;
 1988, pervenuta alla Corte costituzionale  il  6  ottobre  1989.  Gli    &#13;
 argomenti sono analoghi a quelli del Pretore di Latina.                  &#13;
    Nel giudizio davanti alla Corte promosso da quest'ultima ordinanza    &#13;
 si è costituito il ricorrente ing. Gianni  Vigorelli.  Egli  insiste    &#13;
 soprattutto  sulla  disparità  di  trattamento  nei  confronti degli    &#13;
 ingegneri titolari di pensione derivante da un pregresso rapporto  di    &#13;
 lavoro,  e richiama l'attenzione sulla proposta di legge n. 490 del 2    &#13;
 luglio 1987 pendente davanti alla Camera dei deputati,  la  quale,  a    &#13;
 modifica  della  norma  impugnata,  prevede  la  corresponsione della    &#13;
 pensione della Cassa in misura intera anche agli ingegneri pensionati    &#13;
 che continuano l'esercizio della professione.<diritto>Considerato in diritto</diritto>1.  -  Il  Pretore di Udine ha sollevato questione di legittimità    &#13;
 costituzionale dell'art. 2, settimo  comma,  della  legge  3  gennaio    &#13;
 1981,  n.  6,  sulla  previdenza  per gli ingegneri e gli architetti,    &#13;
 nella parte  in  cui  dispone  che  il  supplemento  della  pensione,    &#13;
 spettante  a  coloro  che  continuano  l'attività  professionale per    &#13;
 almeno cinque anni dopo  il  pensionamento,  sia  calcolato  mediante    &#13;
 coefficienti  pari  alla metà di quelli previsti per la liquidazione    &#13;
 della pensione.                                                          &#13;
    Dai  Pretori  di  Latina e di Milano è contestata la legittimità    &#13;
 costituzionale anche del quinto comma del medesimo art.  2,  a  mente    &#13;
 del  quale  la pensione di vecchiaia è ridotta a due terzi quando il    &#13;
 titolare mantenga l'iscrizione all'albo professionale.                   &#13;
    L'identità  della  questione  oggetto  dei  giudizi  promossi dai    &#13;
 Pretori di Latina e di Milano ne impone la riunione. Per  ragioni  di    &#13;
 connessione  è  opportuno  disporre  altresì  la  riunione  di tale    &#13;
 questione con quella sollevata dal Pretore di  Udine,  in  guisa  che    &#13;
 entrambe siano decise con unica sentenza.                                &#13;
    2. - La prima questione è fondata.                                   &#13;
    Nel  sistema  della  legge  n. 6 del 1981, ricalcato sulla riforma    &#13;
 della previdenza forense attuata con la legge 20 settembre  1980,  n.    &#13;
 576,  il  principio di solidarietà non si sovrappone al principio di    &#13;
 proporzionalità della pensione ai contributi  personali  versati  (a    &#13;
 loro  volta  proporzionali  al  reddito professionale netto, entro un    &#13;
 limite massimo), ma introduce  un  correttivo  destinato  ad  operare    &#13;
 nella  misura  necessaria,  secondo  le circostanze, per assicurare a    &#13;
 tutti i membri della  categoria  professionale  una  pensione  minima    &#13;
 adeguata alle loro esigenze di vita.                                     &#13;
    Poiché  per i pensionati che continuano l'attività professionale    &#13;
 l'art.  9,  terzo  comma,  tiene  fermo  l'obbligo  di  contribuzione    &#13;
 personale  alla  Cassa  in misura intera, il criterio di correlazione    &#13;
 tra contribuzione e prestazione previdenziale deve valere  anche  per    &#13;
 la  determinazione  del supplemento della pensione. In quanto applica    &#13;
 percentuali di  calcolo  dimezzate  rispetto  a  quelle  con  cui  si    &#13;
 determina  l'ammontare  della  pensione,  la norma impugnata viola il    &#13;
 principio di eguaglianza. Né può essere giustificata richiamando il    &#13;
 principio  solidaristico,  sia  perché,  considerata  la  situazione    &#13;
 economica  della  Cassa,  l'imposizione  di  un  tale  sacrificio  ai    &#13;
 pensionati  ultrasettantenni  non  appare necessaria per garantire un    &#13;
 livello adeguato del trattamento minimo, sia  perché  gli  ingegneri    &#13;
 pensionati  sono  già gravati, come ogni altro iscritto all'albo, da    &#13;
 un obbligo di solidarietà nella  forma  di  un  contributo  a  fondo    &#13;
 perduto del tre per cento sul reddito eccedente i quaranta milioni di    &#13;
 lire.                                                                    &#13;
    3. - Pure la seconda questione è fondata.                            &#13;
    L'argomento  su cui i giudici remittenti insistono maggiormente fa    &#13;
 leva sulla differenza di  trattamento,  all'interno  della  categoria    &#13;
 degli ingegneri e degli architetti, tra gli iscritti all'albo che, in    &#13;
 virtù di un pregresso rapporto di lavoro, fruiscono di una  pensione    &#13;
 a  carico  dello  Stato  o  dell'Assicurazione  generale  INPS  e gli    &#13;
 ingegneri titolari di pensione erogata dalla Cassa: solo  i  secondi,    &#13;
 non  anche i primi, subiscono una decurtazione della pensione a causa    &#13;
 della continuazione dell'attività professionale.                        &#13;
    In  questi termini l'argomento non è producente perché prospetta    &#13;
 una differenza di trattamento derivante dalla titolarità di pensioni    &#13;
 erogate  da  sistemi  previdenziali diversi, ciascuno con una propria    &#13;
 autonoma disciplina. Tuttavia può essere  recuperato  riformulandolo    &#13;
 in  connessione  con la ratio sottesa alla norma impugnata, la quale,    &#13;
 essendo identica a quella contenuta nell'art. 2, sesto  comma,  della    &#13;
 legge   sulla   previdenza   forense,   ne  ripete  la  finalità  di    &#13;
 "disincentivare la prosecuzione del servizio professionale  da  parte    &#13;
 di  quei  professionisti  che già sono in pensione" (cfr. Camera dei    &#13;
 deputati, VIII legislatura, Commissioni riunite Giustizia  -  Lavoro,    &#13;
 seduta del 26 giugno 1980). Valutato da questo punto di vista, l'art.    &#13;
 2, quinto comma, della legge n. 6 del 1981 urta contro  il  principio    &#13;
 di  eguaglianza  in  quanto  discrimina  gli ingegneri affiliati alla    &#13;
 Cassa gravandoli di un disincentivo all'esercizio  della  professione    &#13;
 dopo  il  pensionamento, dal quale sono esenti gli ingegneri iscritti    &#13;
 ad altre forme di previdenza in dipendenza di un rapporto  di  lavoro    &#13;
 subordinato o di altra attività di lavoro autonomo.                     &#13;
    Del  resto  la  norma impugnata contrasta con l'art. 3 Cost. anche    &#13;
 sotto il profilo del principio di ragionevolezza, per motivi analoghi    &#13;
 a  quelli  esposti  nella sentenza n. 1008 del 1988 in relazione alla    &#13;
 norma corrispondente della legge sulla previdenza forense.               &#13;
    Anche  per  la  categoria degli ingegneri si può osservare che la    &#13;
 penalizzazione della  prosecuzione  dell'esercizio  professionale  da    &#13;
 parte   dei   titolari   di  pensioni  erogate  dalla  Cassa  non  è    &#13;
 giustificata né dal livello delle prestazioni, sempre modesto e  nel    &#13;
 caso  del  Pretore  di  Milano  addirittura  inferiore  alla pensione    &#13;
 sociale (tre milioni annui, ridotti a due ai sensi della disposizione    &#13;
 denunciata),  né  dalle condizioni di questo settore del mercato dei    &#13;
 servizi, le quali non sono tali che la  continuazione  dell'attività    &#13;
 professionale  da  parte  degli  ingegneri  pensionati a carico della    &#13;
 Cassa possa essere ritenuta un ostacolo all'accesso dei giovani  alla    &#13;
 professione,  né infine dalla situazione finanziaria della Cassa, la    &#13;
 quale  negli  ultimi  anni  si  è  arricchita  consentendo  che   il    &#13;
 contributo  sul  reddito  fino  a lire 40 milioni, già diminuito dal    &#13;
 dieci al nove  per  cento  a  partire  dal  1°  gennaio  1984,  fosse    &#13;
 ulteriormente  ridotto,  dal  1°  gennaio  1988, al sei per cento con    &#13;
 decreto del Ministro del lavoro 18 dicembre 1987, n. 548.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi:                                                     &#13;
      dichiara  l'illegittimità  costituzionale  dell'art. 2, settimo    &#13;
 comma, della legge 3  gennaio  1981,  n.  6  ("Norme  in  materia  di    &#13;
 previdenza  per  gli ingegneri e gli architetti"), nella parte in cui    &#13;
 prevede che il supplemento della pensione,  spettante  a  coloro  che    &#13;
 dopo la maturazione del diritto a pensione continuano per cinque anni    &#13;
 l'esercizio della professione, "è pari, per  ognuno  di  tali  anni,    &#13;
 alla  metà  delle  percentuali  di  cui al primo e al quinto (recte:    &#13;
 quarto)  comma,  riferite  alla  media  dei   redditi   professionali    &#13;
 risultanti dalle dichiarazioni successive a quelle considerate per il    &#13;
 calcolo del pensionamento", anziché alle percentuali intere;            &#13;
      dichiara  l'illegittimità  costituzionale  dell'art.  2, quinto    &#13;
 comma, della stessa legge 3 gennaio 1981, n. 6.                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 21 febbraio 1990.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 marzo 1990.                            &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
