<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>130</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:130</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Giulio Gionfrida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/05/1976</data_decisione>
    <data_deposito>26/05/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Dott. NICOLA REALE - &#13;
 Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA &#13;
 - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE &#13;
 STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  promosso  con  ricorso  del Presidente della Regione  &#13;
 Lombardia, notificato il 17 febbraio 1975, depositato in cancelleria il  &#13;
 7 marzo successivo ed iscritto al n. 8 del registro 1975, per conflitto  &#13;
 di attribuzione sorto per effetto del provvedimento 10 dicembre 1974 n.  &#13;
 8991/11701  della   Commissione   di   controllo   sull'amministrazione  &#13;
 regionale  della Lombardia, con cui è stata annullata la deliberazione  &#13;
 della  Giunta  regionale  del  19  novembre  1974  n.  10472,   recante  &#13;
 l'approvazione  della  costituzione  del  Consorzio  provinciale  per i  &#13;
 servizi sociali delle vacanze e dei soggiorni climatici di Pavia.        &#13;
     Udito  nell'udienza  pubblica  del  25  febbraio  1976  il  Giudice  &#13;
 relatore Giulio Gionfrida;                                               &#13;
     udito l'avv. Umberto Pototschnig, per la Regione Lombardia.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con provvedimento (n. 8991/11701) in data 10 dicembre 1974, la  &#13;
 Commissione   di   controllo   sulla  amministrazione  regionale  della  &#13;
 Lombardia ha annullato la delibera della Giunta regionale lombarda  (n.  &#13;
 10472)  del  19 novembre 1974 - recante approvazione della costituzione  &#13;
 del "Consorzio provinciale per i servizi sociali delle  vacanze  e  dei  &#13;
 soggiorni  climatici di Pavia" (ed approvazione del relativo statuto) -  &#13;
 in base al rilievo  che  la  istituzione  (al  pari  della  modifica  o  &#13;
 soppressione) di consorzi (come quello di specie) comprendenti  (anche)  &#13;
 enti   locali   territoriali   (e,  perciò,  inquadrabili  tra  quelli  &#13;
 disciplinati dagli artt. 156 ss. del t.u.   comunale  e    provinciale)  &#13;
 "costituisce  estrinsecazione  di  un potere   autonomo, attinente alla  &#13;
 materia  dell'ordinamento  comunale  e   provinciale",      come   tale  &#13;
 riconducibile,  "non alle competenze trasferite alla Regione" (anche se  &#13;
 nelle  correlative  materie  l'ente è destinato ad operare), sibbene a  &#13;
 quelle conservate allo Stato.                                            &#13;
     2. - Avverso il provvedimento di annullamento indicato ha  proposto  &#13;
 conflitto  di attribuzione, con ricorso notificato il 17 febbraio 1975,  &#13;
 la Regione Lombardia.                                                    &#13;
     La quale ha sostenuto che l'atto impugnato è lesivo delle  proprie  &#13;
 attribuzioni  costituzionalmente  garantite: in quanto - contrariamente  &#13;
 all'assunto dell'organo di  controllo  -  apparterrebbe  effettivamente  &#13;
 alla  Regione  (e  non  allo Stato) la competenza a costituire Consorzi  &#13;
 facoltativi fra Comuni, Province ed altri enti locali,  ogni  qualvolta  &#13;
 gli  scopi del Consorzio siano riconducibili alle materie di competenza  &#13;
 regionale ai sensi dell'art. 117 della Costituzione.                     &#13;
     3. - Nel giudizio innanzi alla Corte non vi è  stata  costituzione  &#13;
 del Presidente del Consiglio dei ministri.                               &#13;
     Con  memoria  successivamente depositata, la Regione ha ribadito ed  &#13;
 ulteriormente illustrato la propria tesi.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. -  Il  ricorso  della  Regione  Lombardia  riguarda  -  come  in  &#13;
 narrativa  detto - il provvedimento della Commissione di controllo, che  &#13;
 ha annullato la deliberazione della Giunta regionale approvativa  della  &#13;
 costituzione  del  "Consorzio  provinciale  per i servizi sociali delle  &#13;
 vacanze e dei soggiorni climatici di Pavia" (e del relativo statuto).    &#13;
     2.  -  Nella  motivazione  del  provvedimento  di  annullamento  è  &#13;
 sottolineata, preliminarmente, la natura di ente locale territoriale di  &#13;
 cinque  dei  sei  enti  consorziati  (il  sesto  essendo un istituto di  &#13;
 assistenza e beneficenza);  ed  in  relazione  a  tale  circostanza  è  &#13;
 formulato  il rilievo che i provvedimenti di istituzione, modificazione  &#13;
 e soppressione dei consorzi di cui al t.u. comunale e provinciale (r.d.  &#13;
 3 marzo 1934 n. 383) " costituiscono, in ogni caso, estrinsecazione  di  &#13;
 un  potere  autonomo attinente alla materia dell'ordinamento comunale e  &#13;
 provinciale",   riconducibile  "non  alle  competenze  trasferite  alla  &#13;
 Regione, bensì a quelle conservate allo Stato".                         &#13;
     3.  -  In  contrario  si  sostiene col ricorso della Regione che il  &#13;
 provvedimento  di  approvazione  della   costituzione   del   consorzio  &#13;
 suindicato   rientra   tra  gli  atti  di  esercizio  delle  competenze  &#13;
 attribuitele (ex artt.  117,  118  della  Costituzione)  nella  materia  &#13;
 dell'assistenza e beneficenza pubblica.                                  &#13;
     E  ciò in correlazione anche al disposto del decreto delegato n. 9  &#13;
 del 1972, che espressamente menziona, tra le funzioni  trasferite  alle  &#13;
 Regioni, quelle concernenti "l'assistenza estiva ed invernale in favore  &#13;
 dei minori" (art. 1 lett. f, del d.P.R. cit.).                           &#13;
     Nessun  fondamento avrebbe, d'altra parte, a dire della Regione, il  &#13;
 contrario  assunto  della  Commissione  di  controllo,  secondo  cui  i  &#13;
 provvedimenti  di istituzione, modificazione e soppressione di consorzi  &#13;
 tra  enti  locali  dovrebbero  ricondursi  ad  una  "supposta"  materia  &#13;
 dell'ordinamento  provinciale e comunale: "che non esiste come tale nel  &#13;
 testo costituzionale".                                                   &#13;
     Aggiunge che la competenza  della  Regione  a  costituire  consorzi  &#13;
 facoltativi  (anche  tra  Comuni  e Province), per il raggiungimento di  &#13;
 scopi relativi a materie di cui all'art. 117 della Costituzione,  trova  &#13;
 conferma  nell'ordinamento  statuale:  come  nel  caso, ad esempio, dei  &#13;
 "consorzi per i servizi di assistenza medico-chirurgica ed ostetrica" e  &#13;
 dei "consorzi di assistenza veterinaria" di cui all'art. 1 lett. i  del  &#13;
 d.P.R. 14 gennaio 1972 n. 4.                                             &#13;
     Sottolinea,  infine,  la già avvenuta emanazione ("senza obiezioni  &#13;
 da parte del Governo") dileggi regionali che testualmente demandano  al  &#13;
 Presidente  della  Giunta  regionale  di  approvare la "costituzione di  &#13;
 consorzi tra comuni e province" destinati  ad  operare  in  materia  di  &#13;
 competenza  regionale (legge reg. Piemonte 4 giugno 1975 n. 46, art. 1;  &#13;
 legge reg. Umbria 3 giugno 1975 n. 40, art. 11).                         &#13;
     4. - Il ricorso è fondato.                                          &#13;
     In considerazione del duplice elemento della  natura  "facoltativa"  &#13;
 del  consorzio  in  questione  e  della inerenza dell'oggetto della sua  &#13;
 attività istituzionale a materia ("assistenza e beneficenza") compresa  &#13;
 nell'elenco di cui all'art. 117 della Costituzione  (la  competenza  in  &#13;
 ordine  alla quale risulta, in particolare, trasferita alla Regione con  &#13;
 d.P.R. 1972 n. 9 cit.), deve riconoscersi che rientra nella  competenza  &#13;
 della  Regione  l'adozione  del  provvedimento  di  approvazione  della  &#13;
 costituzione del consorzio stesso.                                       &#13;
     Per vero, nel caso di consorzio facoltativo (sia pur tra  comuni  e  &#13;
 province)  -  in  quanto  la  vicenda della relativa creazione fa perno  &#13;
 sull'"accordo", derivante dall'incontro e fusione  delle  delibere  (di  &#13;
 spontanea  adesione) dei singoli enti partecipanti - il successivo atto  &#13;
 approvativo (del detto accordo), quale che sia  il  suo  più  corretto  &#13;
 inquadramento,  sotto  il  profilo  giuridico (come controllo atipico o  &#13;
 come atto del procedimento formativo dell'ente), costituisce certamente  &#13;
 esplicazione di una funzione, comunque, amministrativa.                  &#13;
     La quale - nel caso, poi che l'istituendo consorzio abbia scopi  in  &#13;
 particolare  riconducibili  ad una delle materie indicate nell'art. 117  &#13;
 della Costituzione - si specifica come funzione inerente  alla  materia  &#13;
 stessa: per tale via, appunto, devoluta alla competenza della Regione.   &#13;
     5.  - La conclusione così raggiunta non contraddice l'affermazione  &#13;
 - contenuta nella precedente sentenza della Corte n.  186  del  1974  -  &#13;
 circa  la  spettanza allo Stato dei "controlli sostitutivi sugli organi  &#13;
 di consorzi intercomunali o interprovinciali  operanti  nell'ambito  di  &#13;
 materie previste dall'art.  117 della Costituzione.                      &#13;
     La  titolarità  dello  Stato  -  relativamente  all'esercizio  dei  &#13;
 controlli indicati - è stata, invero, ritenuta in  considerazione  del  &#13;
 prevalente  profilo  strutturale  che  connota  tali  atti,  in  quanto  &#13;
 emanazione  di   un   potere   di   supremazia   e   di   "interferenza  &#13;
 nell'organizzazione".                                                    &#13;
     Il  quale  - ex art. 128 della Costituzione - resta necessariamente  &#13;
 riservato allo Stato, nel caso che l'organizzazione incisa  sia  quella  &#13;
 territoriale di Comuni, Province o loro proiezioni.                      &#13;
     Nella  diversa  ipotesi  di  costituzione di consorzio facoltativo,  &#13;
 l'atto di approvazione  dell'accordo  istitutivo  viene  invece  -  per  &#13;
 quanto detto - in rilievo sotto un profilo eminentemente funzionale.     &#13;
     Onde   è   coerente   l'affermazione  della  correlata  competenza  &#13;
 regionale, per il caso che sussista (come nella specie) connessione tra  &#13;
 la  funzione  amministrativa  esercitata  e  materia  rientrante  nella  &#13;
 previsione dell'art. 117 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  che  spetta alla Regione Lombardia il potere di approvare  &#13;
 la costituzione del "Consorzio provinciale per i servizi sociali, delle  &#13;
 vacanze e dei soggiorni climatici di Pavia":                             &#13;
     annulla  il  provvedimento (n. 8991/11701) in data 10 dicembre 1974  &#13;
 della Commissione di  controllo  sull'amministrazione  regionale  della  &#13;
 Lombardia.                                                               &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 19 maggio 1976.                               &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI  -  NICOLA REALE - LEONETTO  &#13;
                                   AMADEI - GIULIO GIONFRIDA  -  EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO - ANTONINO DE STEFANO.         &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
