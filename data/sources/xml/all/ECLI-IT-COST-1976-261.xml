<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>261</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:261</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/12/1976</data_decisione>
    <data_deposito>29/12/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Dott. NICOLA REALE - &#13;
 Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA &#13;
 - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE &#13;
 STEFANO - Prof. LEOPOLDO ELIA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 13, ultimo  &#13;
 comma, legge 6 dicembre 1962, n. 1643 (Istituzione dell'Ente  nazionale  &#13;
 per l'energia elettrica e trasferimento ad esso delle imprese esercenti  &#13;
 le  industrie  elettriche),  promosso con ordinanza emessa il 10 maggio  &#13;
 1974 dal pretore di Casacalenda, nel procedimento civile  vertente  tra  &#13;
 Pasquale Stella e l'Ente per l'energia elettrica (ENEL), iscritta al n.  &#13;
 365  del  registro ordinanze 1974 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 284 del 30 ottobre 1974.                             &#13;
     Visti gli atti di costituzione di Stella  Pasquale,  dell'Ente  per  &#13;
 l'energia  elettrica,  nonché  l'atto di intervento del Presidente del  &#13;
 Consiglio dei ministri;                                                  &#13;
     udito nell'udienza pubblica del 27 ottobre 1976 il Giudice relatore  &#13;
 Michele Rossano;                                                         &#13;
     uditi gli avvocati Enrico Piacitelli  e  Renato  Scognamiglio,  per  &#13;
 l'ENEL e il sostituto avvocato generale dello Stato Giorgio Zagari, per  &#13;
 il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel   corso  del  procedimento  civile  -  promosso  nei  confronti  &#13;
 dell'ENEL da Pasquale Stella, già dipendente della "Impresa  Elettrica  &#13;
 Riccillo Pietrantonio e C", trasferita all'ENEL, al fine di ottenere la  &#13;
 declaratoria  della  inefficacia  del  licenziamento,  intimatogli  con  &#13;
 lettera raccomandata del 12 giugno 1970, e la conseguente reintegra nel  &#13;
 posto di lavoro - il pretore di Casacalenda, con  ordinanza  10  maggio  &#13;
 1974,   ha   sollevato,   di  ufficio,  la  questione  di  legittimità  &#13;
 costituzionale dell'ultimo comma dell'art. 13 legge 6 dicembre 1962, n.  &#13;
 1643 (Istituzione dell'Ente per l'energia elettrica e trasferimento  ad  &#13;
 esso  delle  imprese esercenti le industrie elettriche), in riferimento  &#13;
 all'art. 3 della Costituzione.                                           &#13;
     L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n. 284 del  &#13;
 30 ottobre 1974.                                                         &#13;
     Nel giudizio davanti a questa Corte si sono  costituite  le  parti,  &#13;
 Pasquale Stella e l'ENEL, ed è intervenuto il Presidente del Consiglio  &#13;
 dei  ministri,  rappresentato  e  difeso  dall'Avvocato  generale dello  &#13;
 Stato.                                                                   &#13;
     Pasquale Stella, nelle deduzioni depositate il 25 luglio 1974, dopo  &#13;
 avere  dichiarato  che  aderiva  ai motivi dell'ordinanza di rinvio, ha  &#13;
 affermato che la norma sottoposta all'esame di legittimità viola anche  &#13;
 i principi proclamati dagli artt. 4, comma primo, e  35,  comma  primo,  &#13;
 della Costituzione.                                                      &#13;
     L'ENEL, con le deduzioni depositate il 30 maggio 1974, e l'Avvocato  &#13;
 generale  dello  Stato,  con  l'atto  di  intervento  depositato  il 19  &#13;
 novembre  1974,  hanno  chiesto  che  la  questione   di   legittimità  &#13;
 costituzionale,  sollevata  di  ufficio  dal pretore di Casacalenda sia  &#13;
 dichiarata non fondata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con l'ordinanza in  epigrafe  il  pretore  di  Casacalenda  ha  &#13;
 sollevato  di  ufficio  la  questione  di  legittimità  costituzionale  &#13;
 dell'ultimo comma dell'art.  13 della legge 6 dicembre  1962,  n.  1643  &#13;
 (Istituzione  dell'Ente per l'energia elettrica e trasferimento ad esso  &#13;
 delle  imprese  esercenti  le  industrie  elettriche),  in  riferimento  &#13;
 all'art. 3 della Costituzione.                                           &#13;
     Secondo  il  pretore  l'ultimo  comma dell'art. 13 legge n.1643 del  &#13;
 1962, che prevede il trasferimento all'ENEL  del  personale  dipendente  &#13;
 dalle  imprese  alla  data  del  1 gennaio 1962, addetto esclusivamente  &#13;
 all'esercizio di  attività  elettriche,  violerebbe  il  principio  di  &#13;
 eguaglianza  perché  attuerebbe "una inammissibile discriminazione fra  &#13;
 lavoratori, assicurando il trasferimento del rapporto di lavoro ai soli  &#13;
 dipendenti che   esercitano attività  esclusivamente  elettrica  senza  &#13;
 concedere  analogo  trattamento  al  lavoratore  che esegue prestazioni  &#13;
 principalmente di ordine elettrico".                                     &#13;
     2. - La questione non è fondata.                                    &#13;
     La ratio dell'art. 13 della legge 6 dicembre 1962,  n.    1643,  va  &#13;
 individuata  nella  esigenza  di assicurare all'ENEL, nello svolgimento  &#13;
 della  sua  attività  monopolistica  nel  settore   elettrico,   fonte  &#13;
 essenziale  di  energia  nell'economia  generale,  la disponibilità, di  &#13;
 personale adeguato, secondo un principio di economicità  di  gestione,  &#13;
 nel  rispetto  dei  diritti  dei lavoratori e di quelli dell'impresa. E  &#13;
 siffatta ratio esclude che la distinzione tra dipendenti addetti in via  &#13;
 esclusiva ad attività elettriche e  dipendenti  addetti  ad  attività  &#13;
 promiscue,  ancorché  prevalentemente  elettriche, sia censurabile, ai  &#13;
 sensi dell'art. 3 della Costituzione,  per  irrazionale,  irragionevole  &#13;
 discriminazione. Il trasferimento dei rapporti di lavoro dei dipendenti  &#13;
 addetti in via esclusiva all'esercizio di attività elettriche risponde  &#13;
 all'interesse  di  gestione  dell'Ente e, nel contempo, all'oggetto del  &#13;
 rapporto di lavoro, concernente  esclusivamente  attività  elettriche,  &#13;
 laddove   i  rapporti  di  lavoro  in  corso  per  attività  promiscue  &#13;
 continuano con le imprese originarie.                                    &#13;
     3.  -  Poiché  il  giudizio  di  legittimità  costituzionale   è  &#13;
 circoscritto  dai  confini  fissati  nell'ordinanza  di rimessione, non  &#13;
 possono  essere  prese  in  esame  le  altre  censure  concernenti   la  &#13;
 violazione  degli  artt.  4,  comma  primo,  e  35,  comma primo, della  &#13;
 Costituzione,  dedotte  dal  difensore  della  parte  privata  Pasquale  &#13;
 Stella.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 13, ultimo comma, legge 6 dicembre 1962, n. 1643 (Istituzione  &#13;
 dell'Ente per l'energia elettrica e trasferimento ad esso delle imprese  &#13;
 esercenti  le  industrie  elettriche),   sollevata   dal   pretore   di  &#13;
 Casacalenda,  con  ordinanza  10 maggio 1974, in riferimento all'art. 3  &#13;
 della Costituzione.                                                      &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 21 dicembre 1976.                             &#13;
                                   F.to:  PAOLO  ROSSI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE ROCCHETTI  -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - NICOLA REALE  -  LEONETTO  &#13;
                                   AMADEI  -  GIULIO GIONFRIDA - EDOARDO  &#13;
                                   VOLTERRA -  GUIDO  ASTUTI  -  MICHELE  &#13;
                                   ROSSANO   -  ANTONINO  DE  STEFANO  -  &#13;
                                   LEOPOLDO ELIA.                         &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
