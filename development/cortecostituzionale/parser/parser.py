import os

from replus import Engine

__here__ = os.path.abspath(os.path.dirname(__file__))
models_path = os.path.join(__here__, "models")

parser = Engine(models_path, *"i", ws_noise=r"( |\n|\r\n)*")
