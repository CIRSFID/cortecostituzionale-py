<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>387</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:387</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia>Carlo Mezzanotte</redattore_pronuncia>
    <data_decisione>22/11/2001</data_decisione>
    <data_deposito>06/12/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'articolo 14, commi 3, &#13;
4  e  5,  del decreto legislativo 25 luglio 1998, n. 286 (Testo unico &#13;
delle  disposizioni  concernenti  la  disciplina  dell'immigrazione e &#13;
norme sulla condizione dello straniero) e dell'articolo 20 del d.P.R. &#13;
31 agosto  1999,  n. 394 (Regolamento recante norme di attuazione del &#13;
testo    unico   delle   disposizioni   concernenti   la   disciplina &#13;
dell'immigrazione  e  norme sulla condizione dello straniero, a norma &#13;
dell'articolo  1,  comma  6,  del decreto legislativo 25 luglio 1998, &#13;
n. 286),  promossi  con n. 5 ordinanze emesse il 16 dicembre 2000 dal &#13;
tribunale  di  Milano,  in  composizione monocratica, rispettivamente &#13;
iscritte   dal  n. 192  al  n. 196  del  registro  ordinanze  2001  e &#13;
pubblicate  nella Gazzetta Ufficiale della Repubblica n. 12, 1ª serie &#13;
speciale, dell'anno 2001. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella camera di consiglio del 26 settembre 2001 il giudice &#13;
relatore Carlo Mezzanotte. &#13;
    Ritenuto  che, con cinque ordinanze di identico contenuto in data &#13;
16 dicembre  2000 (r.o. da n. 192 a n. 196 del 2001), il tribunale di &#13;
Milano,  in  composizione  monocratica,  ha sollevato, in riferimento &#13;
agli  articoli  3,  10, 13, 24 e 111 della Costituzione, questione di &#13;
legittimità  costituzionale  dell'articolo  14,  commi 3, 4 e 5, del &#13;
decreto   legislativo  25 luglio  1998,  n. 286  (Testo  unico  delle &#13;
disposizioni  concernenti  la  disciplina  dell'immigrazione  e norme &#13;
sulla  condizione  dello  straniero)  e  dell'articolo  20 del d.P.R. &#13;
31 agosto  1999,  n. 394 (Regolamento recante norme di attuazione del &#13;
testo    unico   delle   disposizioni   concernenti   la   disciplina &#13;
dell'immigrazione  e  norme sulla condizione dello straniero, a norma &#13;
dell'articolo  1,  comma  6,  del decreto legislativo 25 luglio 1998, &#13;
n. 286); &#13;
        che il remittente riferisce di essere chiamato a convalidare, &#13;
all'esito  di  udienza camerale trattata secondo il rito disciplinato &#13;
dagli  articoli  737  e  seguenti  del codice di procedura civile, il &#13;
provvedimento  di  trattenimento  presso  un  centro  di permanenza e &#13;
assistenza  temporanea  disposto  dal  questore  nei confronti di uno &#13;
straniero  destinatario  di decreto di espulsione con accompagnamento &#13;
alla frontiera; &#13;
        che,   sull'assunto   che  il  trattenimento  nei  centri  di &#13;
permanenza  temporanea  previsto  dall'art. 14  del d.lgs. n. 286 del &#13;
1998 costituisca una forma di "detenzione amministrativa" comparabile &#13;
alla  custodia  in  carcere,  il  tribunale  di  Milano  dubita della &#13;
legittimità costituzionale delle seguenti disposizioni: &#13;
        art. 14,  comma  4,  nella  parte  in  cui  dispone  che alla &#13;
convalida  del  trattenimento  presso  un  centro  di  permanenza  ed &#13;
assistenza    temporanea   dello   straniero   destinatario   di   un &#13;
provvedimento di espulsione si applichi la disciplina degli artt. 737 &#13;
e  ss.  cod.  proc. civ., in quanto la procedura ivi prevista sarebbe &#13;
"manifestamente    inidonea    ad    assicurare   la   pienezza   del &#13;
contraddittorio e dell'esplicazione delle difese"; &#13;
        art. 14, comma 4, nella parte in cui precluderebbe al giudice &#13;
ogni accertamento in ordine alla sussistenza delle condizioni addotte &#13;
dalla  autorità di polizia quale concreto impedimento all'esecuzione &#13;
immediata  dell'accompagnamento  alla  frontiera  e, sotto un diverso &#13;
profilo,  in  ordine  alla  fondatezza  delle  ragioni allegate dallo &#13;
straniero   circa   la  ricorrenza  di  una  ipotesi  di  divieto  di &#13;
espulsione; &#13;
        art. 14, comma 3, unitamente all'art. 20 del d.P.R. 31 agosto &#13;
1999,  n. 394,  nella  parte  in  cui non prevederebbero l'obbligo di &#13;
avviso  al  difensore,  d'ufficio  o di fiducia, contestualmente alla &#13;
comunicazione al giudice dell'inizio del trattenimento; &#13;
        art. 14,  comma  5,  nella  parte  in cui non prevederebbe un &#13;
limite  massimo  anche  per  il  cumulo di vari periodi successivi di &#13;
trattenimento   fondati   sul   medesimo  decreto  di  espulsione  e, &#13;
conseguentemente,  impedirebbe al giudice di accertare se quel limite &#13;
sia stato superato; &#13;
        art. 14, comma 4, nella parte in cui imporrebbe al giudice di &#13;
provvedere alla convalida senza attribuirgli il potere di determinare &#13;
il  ragionevole  termine  massimo, anche cumulato, del trattenimento, &#13;
tenendo  conto  delle  concrete  circostanze del caso e bilanciando i &#13;
contrapposti   interessi   della   tutela  delle  frontiere  e  della &#13;
salvaguardia della libertà personale dello straniero; &#13;
        che  in  tutte  le  ordinanze  di rimessione il giudice a quo &#13;
contestualmente   alla  proposizione  delle  anzidette  questioni  di &#13;
legittimità  costituzionale,  ha ordinato l'immediato rilascio dello &#13;
straniero, il cui trattenimento è oggetto del giudizio di convalida; &#13;
        che  è  intervenuto,  in  tutti i giudizi, il Presidente del &#13;
Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura &#13;
generale  dello  Stato,  e ha chiesto che la questione sia dichiarata &#13;
inammissibile proprio a causa dell'avvenuto rilascio degli stranieri, &#13;
che  avrebbe  comportato  l'esaurimento della funzione decisoria alla &#13;
quale il remittente, nei singoli giudizi, era chiamato; &#13;
        che,  quanto  alla  censura  che  si appunta sull'art. 20 del &#13;
d.P.R.  n. 394  del  1999,  la difesa erariale osserva che l'atto nel &#13;
quale  tale  disposizione  è  contenuta, di natura regolamentare, è &#13;
privo  del  requisito  della  forza  di  legge  e  pertanto  non può &#13;
costituire  oggetto  del  controllo di legittimità costituzionale ad &#13;
opera di questa Corte; &#13;
        che,  in  ogni  caso, ad avviso dell'Avvocatura, le questioni &#13;
sarebbero  infondate,  in  quanto  il  trattenimento  nei  centri  di &#13;
permanenza temporanea inciderebbe sulla libertà di circolazione e di &#13;
soggiorno  e  non  anche  sulla  libertà  personale  e  comunque  le &#13;
disposizioni  censurate  realizzerebbero  un  equo  bilanciamento tra &#13;
l'esigenza  di  contrastare  l'immigrazione  clandestina  e quella di &#13;
tutelare i diritti dello straniero; &#13;
        che,   sempre  secondo  l'Avvocatura  dello  Stato,  anche  a &#13;
ritenere  che la misura del trattenimento si attenga alla sfera della &#13;
libertà   personale,   il  procedimento  regolato  dal  testo  unico &#13;
sull'immigrazione  sarebbe  ricalcato  sul modello dell'art. 13 della &#13;
Costituzione  e  nessun addebito potrebbe essere mosso al legislatore &#13;
per aver adottato il rito camerale ex art. 737 e ss. cod. proc. civ., &#13;
che non risulterebbe inadeguato alle esigenze di tutela della persona &#13;
trattenuta. &#13;
    Considerato che le ordinanze propongono la medesima questione e i &#13;
relativi   giudizi   possono   essere   riuniti   per  essere  decisi &#13;
congiuntamente; &#13;
        che  in tutti gli atti instaurativi del presente giudizio, il &#13;
remittente,  nel promuovere questione di legittimità costituzionale, &#13;
ha   espressamente   disposto   l'immediato  rilascio  della  persona &#13;
trattenuta,  sicché  ricorre  la  medesima  fattispecie  sulla quale &#13;
questa  Corte,  con  ordinanza n. 297 del 2001, si è pronunciata nel &#13;
senso della manifesta inammissibilità; &#13;
        che  la censura rivolta nei confronti dell'art. 20 del d.P.R. &#13;
n. 394 del 1999 è inammissibile anche per l'ulteriore ragione che si &#13;
tratta   di   disposizione  regolamentare,  inidonea  a  radicare  la &#13;
competenza di questa Corte nel giudizio incidentale sulle leggi. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  dell'articolo  14,  commi 3, 4 e 5, del &#13;
decreto   legislativo  25 luglio  1998,  n. 286  (Testo  unico  delle &#13;
disposizioni  concernenti  la  disciplina  dell'immigrazione  e norme &#13;
sulla  condizione  dello  straniero)  e  dell'articolo  20 del d.P.R. &#13;
31 agosto  1999,  n. 394 (Regolamento recante norme di attuazione del &#13;
testo    unico   delle   disposizioni   concernenti   la   disciplina &#13;
dell'immigrazione  e  norme sulla condizione dello straniero, a norma &#13;
dell'articolo  1,  comma  6,  del decreto legislativo 25 luglio 1998, &#13;
n. 286),  sollevata, in riferimento agli articoli 3, 10, 13, 24 e 111 &#13;
della   Costituzione,   dal  tribunale  di  Milano,  in  composizione &#13;
monocratica, con le ordinanze indicate in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 22 novembre 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                      Il redattore: Mezzanotte &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 6 dicembre 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
