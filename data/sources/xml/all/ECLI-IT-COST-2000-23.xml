<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>23</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:23</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/01/2000</data_decisione>
    <data_deposito>27/01/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale &#13;
 MARINI, dott. Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di ammissibilità del conflitto tra poteri  dello  Stato    &#13;
 sorto a seguito della limitazione del controllo della Corte dei conti    &#13;
 sulla  gestione  finanziaria  dell'Istituto  nazionale di astrofisica    &#13;
 (INAF) ai soli "conti consuntivi" attuata con il decreto  legislativo    &#13;
 23  luglio  1999,  n.  296  (Istituzione  dell'Istituto  nazionale di    &#13;
 astrofisica  INAF  e  norme  relative  all'Osservatorio   vesuviano),    &#13;
 promosso  dalla  Corte  dei  conti,  con  ricorso  depositato  il  24    &#13;
 settembre  1999  ed  iscritto  al  n. 129 del registro ammissibilità    &#13;
 conflitti.                                                               &#13;
   Udito nella camera di consiglio del 15  dicembre  1999  il  giudice    &#13;
 relatore Gustavo Zagrebelsky.                                            &#13;
   Ritenuto  che  il Presidente della Corte dei conti, a seguito della    &#13;
 determinazione n. 52/1999 della Sezione competente al controllo sulla    &#13;
 gestione finanziaria degli enti a cui lo Stato  contribuisce  in  via    &#13;
 ordinaria,  ha  proposto  ricorso  per  conflitto di attribuzioni tra    &#13;
 poteri dello Stato nei riguardi del Governo della  Repubblica  e  dei    &#13;
 Ministri per l'università e la ricerca scientifica e per la funzione    &#13;
 pubblica, in relazione al decreto legislativo 23 luglio 1999, n.  296    &#13;
 (Istituzione  dell'Istituto  nazionale  di astrofisica - INAF e norme    &#13;
 relative all'Osservatorio vesuviano),  "nella  parte  in  cui  si  è    &#13;
 limitato   il  potere  di  controllo  della  Corte  dei  conti",  per    &#13;
 violazione degli artt. 76 e 100, secondo comma,  della  Costituzione,    &#13;
 chiedendo che questa Corte, "previo annullamento e/o dichiarazione di    &#13;
 illegittimità  costituzionale"  del  suddetto  decreto  legislativo,    &#13;
 dichiari che "spetta alla Corte dei conti - nella composizione  della    &#13;
 sezione del controllo sugli enti - l'esercizio del controllo previsto    &#13;
 dalla  legge  21  marzo  1958,  n.  259  sugli enti considerati dalle    &#13;
 richiamate norme";                                                       &#13;
     che il ricorrente,  facendo  riferimento  all'art.  100,  secondo    &#13;
 comma,  della  Costituzione  per  quanto  attiene  all'esercizio  del    &#13;
 controllo della Corte dei conti sulla gestione finanziaria degli enti    &#13;
 a cui lo Stato contribuisce in via ordinaria  nonché  ai  successivi    &#13;
 adempimenti  nei  confronti delle Camere "sul risultato del riscontro    &#13;
 eseguito",   osserva   che,   in   attuazione   di   tale   parametro    &#13;
 costituzionale,  è  stata emanata la legge 21 marzo 1958, n. 259, la    &#13;
 quale, ai fini del  controllo  di  cui  trattasi,  ha  istituito  una    &#13;
 speciale  Sezione  e  ha  dettato  le modalità per l'esercizio della    &#13;
 funzione di controllo;                                                   &#13;
     che nel ricorso si sostiene che la legge 15 marzo  1997,  n.  59,    &#13;
 nel   prevedere   (art.  11)  una  delega  per  il  riordinamento  di    &#13;
 particolari enti pubblici e privati, operanti in  specifici  settori,    &#13;
 non  ha  fatto  alcun  riferimento,  nell'indicazione  dei  necessari    &#13;
 criteri direttivi, alle funzioni di controllo della Corte  dei  conti    &#13;
 nei  riguardi  degli  enti medesimi, manifestando anzi, nell'art. 14,    &#13;
 con il richiamo alla legge 14  gennaio  1994,  n.  20,  l'intento  di    &#13;
 mantenere il sistema di controllo previgente;                            &#13;
     che,  inoltre, l'art. 100, secondo comma, della Costituzione, pur    &#13;
 rinviando alla legge ordinaria la determinazione  dei  casi  e  delle    &#13;
 forme del controllo, riferisce quest'ultimo a tutti gli enti a cui lo    &#13;
 Stato  contribuisce in via ordinaria senza porre alcuna distinzione e    &#13;
 pertanto  la  limitazione  di  tale   controllo,   ad   opera   delle    &#13;
 disposizioni  in  questione,  alla  "semplice  verifica  dei  bilanci    &#13;
 consuntivi con esclusione del controllo sugli atti di gestione e  con    &#13;
 esclusione perfino dell'esame della regolarità contabile" violerebbe    &#13;
 la  sfera di attribuzioni costituzionalmente assegnata alla Corte dei    &#13;
 conti medesima.                                                          &#13;
   Considerato che la Corte dei conti, in persona del suo  Presidente,    &#13;
 sulla  base  della determinazione n. 52/1999 della Sezione competente    &#13;
 al controllo sulla gestione finanziaria degli enti  a  cui  lo  Stato    &#13;
 contribuisce  in  via    ordinaria,  con  ricorso  depositato  il  24    &#13;
 settembre 1999, ha proposto  conflitto  di  attribuzioni  tra  poteri    &#13;
 dello    Stato  contro  il  Governo  della Repubblica, in persona del    &#13;
 Presidente del Consiglio  dei  Ministri,  e  contro  i  Ministri  per    &#13;
 l'università e la ricerca scientifica e per la funzione pubblica, in    &#13;
 relazione  al decreto legislativo 23 luglio 1999, n. 296 (Istituzione    &#13;
 dell'Istituto nazionale  di  astrofisica  -  INAF  e  norme  relative    &#13;
 all'Osservatorio  vesuviano),  per  violazione  degli  artt.  76,  in    &#13;
 riferimento alla legge 15 marzo 1997, n. 59, e  100,  secondo  comma,    &#13;
 della  Costituzione;                                                     &#13;
     che,  in  questa fase del giudizio, a norma dell'art. 37, terzo e    &#13;
 quarto comma, della legge 11 marzo  1953,  n.  87,  questa  Corte  è    &#13;
 chiamata  a  deliberare senza contraddittorio sull'ammissibilità del    &#13;
 ricorso  sotto  il  profilo  dell'esistenza  della  "materia  di   un    &#13;
 conflitto la cui risoluzione spetti alla sua competenza";                &#13;
     che,  dal  punto  di vista dei presupposti soggettivi, alla Corte    &#13;
 dei conti, nell'esercizio  della  sua  funzione  di  controllo  sulla    &#13;
 gestione finanziaria degli enti ai quali lo Stato contribuisce in via    &#13;
 ordinaria,    spetta   la   legittimazione   a   proporre   conflitto    &#13;
 costituzionale  di  attribuzioni  a   norma   dell'art.   134   della    &#13;
 Costituzione,  in quanto tale funzione, sia pure di natura ausiliare,    &#13;
 è caratterizzata, oltre che  dalla  sua  previsione  nell'art.  100,    &#13;
 secondo   comma,   della   Costituzione,  dalla  posizione  di  piena    &#13;
 indipendenza dell'organo chiamato a esercitarla (sentenza n. 466  del    &#13;
 1993 e ordinanza n. 323 del 1999, nonché, in relazione alla funzione    &#13;
 di controllo in generale, sentenze nn. 406 del 1989 e 302 del 1995);     &#13;
     che,  ancora  dal  punto di vista soggettivo, la legittimazione a    &#13;
 resistere  spetta  al  Governo,  rappresentato  dal  Presidente   del    &#13;
 Consiglio  dei  Ministri,  e  non  ai  singoli  Ministri, in quanto -    &#13;
 indipendentemente dalla  questione  dell'astratta  legittimazione  di    &#13;
 questi  ultimi  -  il  conflitto proposto riguarda norme contenute in    &#13;
 decreti  legislativi  delegati,  ascrivibili   ai   poteri   e   alla    &#13;
 responsabilità del Governo nel suo complesso;                           &#13;
     che,  con  riferimento  ai  presupposti  oggettivi, il ricorso è    &#13;
 indirizzato alla garanzia della sfera di attribuzioni determinata  da    &#13;
 norme   costituzionali,  in  quanto  la  lesione  lamentata  concerne    &#13;
 competenze della Corte dei conti configurate  dalla  legge  21  marzo    &#13;
 1958,  n.  259,  riconducibili alla previsione dell'art. 100, secondo    &#13;
 comma, della Costituzione;                                               &#13;
      che, circa il profilo dell'idoneità a determinare conflitto  di    &#13;
 atti  aventi  natura  legislativa, quali quelli  in questione, questa    &#13;
 Corte ha già dato una risposta affermativa con sentenza n.  457  del    &#13;
 1999 resa su analogo  conflitto;                                         &#13;
     che pertanto il ricorso deve essere dichiarato ammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  ammissibile,  ai  sensi dell'art. 37 della legge 11 marzo    &#13;
 1953, n. 87, nei confronti del Governo della Repubblica, il conflitto    &#13;
 di attribuzioni  proposto  dalla  Corte  dei  conti  con  il  ricorso    &#13;
 indicato in epigrafe;                                                    &#13;
   Dispone:                                                               &#13;
     a) che la cancelleria di questa Corte dia immediata comunicazione    &#13;
 della presente ordinanza all'organo  ricorrente;                         &#13;
     b) che, a cura del ricorrente, il ricorso e la presente ordinanza    &#13;
 siano   notificati  al  Governo  della  Repubblica,  in  persona  del    &#13;
 Presidente del Consiglio dei Ministri, entro il  termine  di  novanta    &#13;
 giorni  dalla comunicazione di cui sub-a), per essere successivamente    &#13;
 depositati  presso  la  cancelleria  di questa Corte entro il termine    &#13;
 fissato dall'art.  26, terzo comma, delle  norme  integrative  per  i    &#13;
 giudizi davanti alla Corte costituzionale.                               &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 17 gennaio 2000.                              &#13;
                        Il Presidente: Vassalli                           &#13;
                        Il relatore: Zagrebelsky                          &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 27 gennaio 2000.                          &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
