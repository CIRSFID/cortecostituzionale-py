<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2020</anno_pronuncia>
    <numero_pronuncia>69</numero_pronuncia>
    <ecli>ECLI:IT:COST:2020:69</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CARTABIA</presidente>
    <relatore_pronuncia>Giuliano Amato</relatore_pronuncia>
    <redattore_pronuncia>Giuliano Amato</redattore_pronuncia>
    <data_decisione>24/03/2020</data_decisione>
    <data_deposito>10/04/2020</data_deposito>
    <tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>ammissibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Marta CARTABIA; Giudici : Aldo CAROSI, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI, Stefano PETITTI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 9 gennaio 2019 (approvazione del doc. IV-ter, n. 5-A), promosso dal Giudice per l'udienza preliminare del Tribunale ordinario di Verona, con ricorso spedito per la notificazione il 15 novembre, depositato in cancelleria il 18 novembre 2019 e iscritto al n. 6 del registro conflitti tra poteri 2019, fase di ammissibilità.&#13;
 Udito nella camera di consiglio del 23 marzo 2020 il Giudice relatore Giuliano Amato;&#13;
 deliberato nella camera di consiglio del 24 marzo 2020.&#13;
 Ritenuto che, con ricorso spedito per la notificazione il 15 novembre e depositato il 18 novembre 2019, il Giudice per l'udienza preliminare del Tribunale ordinario di Verona ha promosso conflitto di attribuzione tra poteri dello Stato in riferimento alla deliberazione del 9 gennaio 2019 (approvazione del doc. IV-ter, n. 5-A), con la quale il Senato della Repubblica ha affermato che le dichiarazioni rese da A.C. B., senatrice all'epoca dei fatti, concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e ricadono, pertanto, nella garanzia di insindacabilità di cui all'art. 68, primo comma, della Costituzione;&#13;
 che, secondo quanto riferito dal giudice ricorrente, nel procedimento penale innanzi a esso pendente, l'on. A.C. B. è imputata dei reati di cui agli artt. 416, comma 1, e 318 del codice penale per avere, per l'esercizio delle sue funzioni e dei suoi poteri, in qualità di senatrice della Repubblica, accettato la promessa e ricevuto denaro e altre utilità dal direttore generale di un consorzio, per la promozione, il rafforzamento e l'appoggio politico al sodalizio criminoso costituito dallo stesso consorzio, da realizzarsi, in particolare, attraverso la presentazione di un emendamento a esso favorevole, nonché attraverso il concreto interessamento circa l'iter legislativo di tale emendamento;&#13;
 che, a seguito della richiesta di rinvio a giudizio, l'imputata ha sollevato eccezione di insindacabilità ai sensi dell'art. 68 Cost. e, con ordinanza del 26 aprile 2018, il Giudice dell'udienza preliminare ha sospeso il processo e trasmesso copia degli atti al Senato della Repubblica, ai sensi dell'art. 3, commi 3 e 4, della legge 20 giugno 2003, n. 140 (Disposizioni per l'attuazione dell'articolo 68 della Costituzione nonché in materia di processi penali nei confronti delle alte cariche dello Stato);&#13;
 che, riferisce ancora il ricorrente, con la deliberazione del 9 gennaio 2019 il Senato della Repubblica ha approvato la relazione con la quale la Giunta delle elezioni e delle immunità parlamentari ha proposto di deliberare che le dichiarazioni della senatrice A.C. B. costituiscono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e ricadono pertanto nell'ipotesi di cui all'art. 68, primo comma, Cost.;&#13;
 che nella deliberazione impugnata il Senato avrebbe ritenuto insussistente il reato di corruzione per mancanza dell'elemento soggettivo e, in particolare, della cosiddetta voluntas accipiendi; e che, in questo modo, il Senato avrebbe esercitato un sindacato sulla non manifesta implausibilità dell'accusa, attribuendosi un potere inesistente di valutarne il fondamento non rientrante nell'ambito delle attribuzioni della Camera di appartenenza del parlamentare, spettando esclusivamente all'autorità giudiziaria;&#13;
 che, d'altra parte, nel caso in cui si proceda nei confronti di un parlamentare per il reato di corruzione per l'esercizio della funzione, non potrebbe essere invocata la garanzia dell'insindacabilità; a questo riguardo, sono richiamate le sentenze della Corte di cassazione, sesta sezione penale, del 6 giugno 2017, n. 36769, e 11 settembre 2018, n. 40347, con le quali è stato ritenuto che l'immunità prevista dall'art. 68 Cost. non precluda la perseguibilità del delitto di corruzione per l'esercizio della funzione, di cui all'art. 318 cod. pen., il quale sarebbe configurabile anche nei confronti di un membro del Parlamento;&#13;
 che il ricorrente ha, quindi, chiesto a questa Corte di dichiarare che non spettava al Senato della Repubblica deliberare che i fatti per i quali è pendente procedimento penale nei confronti della senatrice A.C. B. concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, ai sensi dell'art. 68, primo comma, Cost.; ed è altresì richiesto l'annullamento della deliberazione di insindacabilità adottata dal Senato il 9 gennaio 2019.&#13;
 Considerato che, con ricorso depositato il 18 novembre 2019, il Giudice per l'udienza preliminare del Tribunale ordinario di Verona ha promosso conflitto di attribuzione tra poteri dello Stato in riferimento alla deliberazione del 9 gennaio 2019 (approvazione del doc. IV-ter, n. 5-A), con la quale il Senato della Repubblica ha affermato che le dichiarazioni rese da A.C. B., senatrice all'epoca dei fatti, concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e ricadono, pertanto, nella garanzia di insindacabilità di cui all'art. 68, primo comma, della Costituzione;&#13;
 che, in questa fase del giudizio, la Corte è chiamata a deliberare, in camera di consiglio e senza contraddittorio, in ordine alla sussistenza dei requisiti soggettivo e oggettivo prescritti dall'art. 37, primo comma, della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), ossia a decidere se il conflitto insorga tra organi competenti a dichiarare definitivamente la volontà del potere cui appartengono e per la delimitazione della sfera di attribuzioni determinata per i vari poteri da norme costituzionali, restando impregiudicata ogni ulteriore questione, anche in punto di ammissibilità;&#13;
 che, sotto il profilo del requisito soggettivo, va riconosciuta la legittimazione del Giudice per l'udienza preliminare presso il Tribunale di Verona a promuovere conflitto di attribuzione tra poteri dello Stato, in quanto organo giurisdizionale, in posizione di indipendenza costituzionalmente garantita, competente a dichiarare definitivamente, nell'esercizio delle funzioni attribuitegli, la volontà del potere cui appartiene (ex plurimis, ordinanze n. 139 del 2016, n. 25 del 2013 e n. 142 del 2011);&#13;
 che, parimenti, deve essere riconosciuta la legittimazione del Senato della Repubblica a essere parte del presente conflitto, quale organo competente a dichiarare in modo definitivo la propria volontà in ordine all'applicazione dell'art. 68, primo comma, Cost.;&#13;
 che, per quanto attiene al profilo oggettivo, il ricorrente lamenta la lesione della propria sfera di attribuzione, costituzionalmente garantita, in conseguenza di un esercizio ritenuto illegittimo, per inesistenza dei relativi presupposti, del potere spettante al Senato della Repubblica di dichiarare l'insindacabilità delle opinioni espresse da un membro di quel ramo del Parlamento ai sensi dell'art. 68, primo comma, Cost.;&#13;
 che, dunque, esiste la materia di un conflitto la cui risoluzione spetta alla competenza di questa Corte (ex plurimis, ordinanze n. 139 e n. 91 del 2016, n. 286, n. 161, n. 150 e n. 53 del 2014).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 1) dichiara ammissibile, ai sensi dell'articolo 37 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), il ricorso per conflitto di attribuzione tra poteri dello Stato indicato in epigrafe, proposto dal Giudice per l'udienza preliminare presso il Tribunale ordinario di Verona, nei confronti del Senato della Repubblica;&#13;
 2) dispone:&#13;
 a) che la Cancelleria di questa Corte dia immediata comunicazione della presente ordinanza al predetto giudice, che ha proposto il conflitto di attribuzione;&#13;
 b) che il ricorso e la presente ordinanza siano, a cura del ricorrente, notificati al Senato della Repubblica, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di trenta giorni previsto dall'art. 24, comma 3, delle Norme integrative per i giudizi davanti alla Corte costituzionale.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 marzo 2020.&#13;
 F.to:&#13;
 Marta CARTABIA, Presidente&#13;
 Giuliano AMATO, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 10 aprile 2020.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
