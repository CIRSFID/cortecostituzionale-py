from cirsfidparsers.legal_references import RiferimentiNormativiParser


ref_parser = RiferimentiNormativiParser()
ref_parser.default_corte = "consulta"


def parse_references(provvedimento):
    for name, component in provvedimento.components.items():
        for element in component.iter():
            if element.text is not None:
                references = ref_parser.parse(element.text)
                element.add_inlines(*references)
