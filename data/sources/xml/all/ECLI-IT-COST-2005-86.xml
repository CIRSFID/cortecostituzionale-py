<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>86</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:86</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONTRI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>23/02/2005</data_decisione>
    <data_deposito>02/03/2005</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Fernanda CONTRI; Giudici: Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), promossi, nell'ambito di diversi procedimenti penali, dal Giudice di pace di Vibo Valentia con ordinanze del 4 febbraio e 10 marzo 2004, rispettivamente iscritte ai numeri 742 e 743 del registro ordinanze 2004 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 39, prima serie speciale, dell'anno 2004. &#13;
    Udito nella camera di consiglio del 26 gennaio 2005 il Giudice relatore Guido Neppi Modona. &#13;
    Ritenuto che con due ordinanze testualmente identiche (r.o. n. 742 e n. 743 del 2004) il Giudice di pace di Vibo Valentia ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questione di legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui non prevede che nella citazione a giudizio davanti al giudice di pace sia dato avviso all'imputato, a pena di nullità, della «possibilità di estinguere il reato a mezzo di condotte riparatorie» ai sensi dell'art. 35 del medesimo decreto; &#13;
    che il giudice a quo, ritenuta la rilevanza della questione in quanto concernente «la validità della citazione in giudizio», osserva che l'art. 35 menzionato prevede al comma 1 che il giudice di pace, sentite le parti e l'eventuale persona offesa, dichiara con sentenza estinto il reato quando l'imputato dimostra di aver proceduto, prima dell'udienza di comparizione, alla riparazione del danno e di aver eliminato le conseguenze dannose o pericolose del reato, e che, inoltre, ai sensi del comma 3 il giudice di pace può disporre la sospensione del processo, per un periodo non superiore a tre mesi, se l'imputato chiede nell'udienza di comparizione di poter provvedere agli adempimenti di cui al comma 1 e dimostri di non averlo potuto fare in precedenza; &#13;
    che a giudizio del rimettente la mancata previsione nell'art. 20 della necessità di dare avviso all'imputato delle facoltà di cui ai commi 1 e 3 dell'art. 35 determina «una chiara violazione dei principi costituzionali ed in particolare degli artt. 3, 24 e 111 Cost., poiché l'imputato non viene posto nella condizione di poter scegliere di avvalersi della facoltà di estinguere il reato ai sensi dell'art. 35 citato, facoltà che deve essere esercitata prima dell'udienza di comparizione». &#13;
    Considerato che il rimettente dubita della legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), nella parte in cui non prevede che nella citazione a giudizio davanti al giudice di pace sia dato avviso all'imputato, a pena di nullità, della «possibilità di estinguere il reato a mezzo di condotte riparatorie» ai sensi dell'art. 35 del medesimo decreto; &#13;
    che, stante l'identità delle ordinanze di rimessione, deve essere disposta la riunione dei relativi giudizi; &#13;
    che analoga questione è già stata dichiarata manifestamente infondata con ordinanza n. 11 del 2004; &#13;
    che peraltro le ordinanze di rimessione difettano della descrizione delle fattispecie oggetto dei giudizi a quibus e sono del tutto carenti di motivazione in ordine alla rilevanza e alla non manifesta infondatezza delle questioni, essendo i parametri costituzionali invocati apoditticamente; &#13;
    che le questioni devono pertanto essere dichiarate manifestamente inammissibili (v. ordinanze numeri 349 e 333 del 2004, 1 del 2003 e 239 del 2002). &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 20 del decreto legislativo 28 agosto 2000, n. 274 (Disposizioni sulla competenza penale del giudice di pace, a norma dell'articolo 14 della legge 24 novembre 1999, n. 468), sollevate, in riferimento agli artt. 3, 24 e 111 della Costituzione, dal Giudice di pace di Vibo Valentia con le ordinanze in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 febbraio 2005. &#13;
    F.to: &#13;
    Fernanda CONTRI, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Maria Rosaria FRUSCELLA, Cancelliere &#13;
    Depositata in Cancelleria il 2 marzo 2005. &#13;
    Il Cancelliere &#13;
    F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
