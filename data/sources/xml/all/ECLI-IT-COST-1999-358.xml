<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>358</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:358</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/07/1999</data_decisione>
    <data_deposito>22/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'articolo 34, comma    &#13;
 2, del codice di procedura penale, promosso con ordinanza emessa il 4    &#13;
 maggio 1998 dalla  Corte  di  cassazione,  iscritta  al  n.  758  del    &#13;
 registro  ordinanze  1998 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 42, prima serie speciale, dell'anno 1998.                  &#13;
   Udito nella camera di  consiglio  del  7  luglio  1999  il  giudice    &#13;
 relatore Carlo Mezzanotte;                                               &#13;
   Ritenuto  che  con  ordinanza  in  data  4  maggio 1998 la Corte di    &#13;
 cassazione,  investita  di  un  ricorso  avverso   un'ordinanza   del    &#13;
 Tribunale   di   Belluno,  con  la  quale  era  stata  rigettata  una    &#13;
 dichiarazione di ricusazione proposta da un  imputato  nei  confronti    &#13;
 del  pretore di Belluno, sezione di Pieve di Cadore, ha sollevato, in    &#13;
 riferimento agli articoli 3 e 24  della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale dell'articolo 34, comma 2, del codice di    &#13;
 procedura penale, nella parte in cui non  prevede  l'incompatibilità    &#13;
 per il giudice del dibattimento che abbia precedentemente pronunciato    &#13;
 sentenza  di  applicazione della pena ex art. 444 cod. proc. pen. nei    &#13;
 confronti di altro soggetto, valutando lo stesso fatto;                  &#13;
     che  il  remittente  riferisce  che  il  ricusante   era   stato,    &#13;
 unitamente  ad  altri,  tratto a giudizio per rispondere del reato di    &#13;
 cui all'art.  221 del regio decreto 27 luglio 1934,  n.  1265  (Testo    &#13;
 unico  delle  leggi  sanitarie)  e  che  il pretore aveva pronunciato    &#13;
 sentenza di applicazione della pena  nei  confronti  di  alcuni  suoi    &#13;
 coimputati, previa separazione delle loro posizioni;                     &#13;
     che,   quanto   alla  rilevanza  della  questione,  la  Corte  di    &#13;
 cassazione osserva che il pretore con  la  sentenza  di  applicazione    &#13;
 della  pena emessa nei confronti dei coimputati avrebbe già valutato    &#13;
 la vicenda processuale, escludendo che ricorressero gli  estremi  per    &#13;
 applicare  l'art.  129  cod.  proc. pen. e, in particolare, ritenendo    &#13;
 ininfluente un certificato di abitabilità, la  cui  legittimità  ed    &#13;
 efficacia  sarebbe determinante ai fini del giudizio nei confronti di    &#13;
 tutti gli imputati, ivi compreso il ricusante;                           &#13;
     che il giudice remittente, pur dichiarandosi consapevole che  con    &#13;
 la  sentenza n. 186 del 1992 questa Corte ha escluso che la pronuncia    &#13;
 di una sentenza di applicazione della pena su richiesta nei confronti    &#13;
 di un coimputato determini incompatibilità a celebrare  il  giudizio    &#13;
 nei  confronti dei concorrenti negli stessi reati, afferma che questa    &#13;
 pronuncia  nella  sua  assolutezza  sarebbe  stata  "superata"  dalla    &#13;
 successiva   giurisprudenza   costituzionale,  e  segnatamente  dalla    &#13;
 sentenza  n.  371  del  1996,  con  la  quale  è  stata   dichiarata    &#13;
 l'illegittimità  costituzionale  dell'art.  34,  comma 2, cod. proc.    &#13;
 pen., nella parte in cui non prevede che  non  possa  partecipare  al    &#13;
 giudizio   nei   confronti  di  un  imputato  il  giudice  che  abbia    &#13;
 pronunciato o concorso a  pronunciare  una  precedente  sentenza  nei    &#13;
 confronti  di  altri  soggetti,  nella  quale  la posizione di quello    &#13;
 stesso imputato in ordine alla sua responsabilità  penale  sia  già    &#13;
 stata comunque valutata;                                                 &#13;
     che,  però,  ad  avviso  del giudice a quo quest'ultima sentenza    &#13;
 riguarderebbe esclusivamente le ipotesi di  concorso  necessario  nel    &#13;
 reato,  sicché  occorrerebbe  "ampliarne la sfera di applicazione in    &#13;
 relazione  all'avvenuto  esame  di  merito  di  tutto  il   materiale    &#13;
 probatorio comune ai diversi imputati";                                  &#13;
     che  in  difetto di una ulteriore dichiarazione di illegittimità    &#13;
 costituzionale in parte qua dell'art. 34,  comma  2,  del  codice  di    &#13;
 procedura  penale  verrebbe violato il principio del giusto processo,    &#13;
 poiché per il peculiare atteggiarsi della fattispecie  probatoria  -    &#13;
 fondandosi  la  responsabilità  degli imputati sugli stessi ed unici    &#13;
 elementi di fatto e quindi  sulla  medesima  ed  intera  prova  -  il    &#13;
 giudice  con  la  sentenza  emessa  ex  art.  444 cod. proc. pen. non    &#13;
 avrebbe potuto non formarsi un convincimento non soltanto sul  merito    &#13;
 dell'azione  penale svolta contro gli imputati che hanno richiesto ed    &#13;
 ottenuto l'applicazione "patteggiata" della pena, ma  anche,  seppure    &#13;
 incidentalmente,  sul  merito dell'intera vicenda di fatto e, quindi,    &#13;
 sulla posizione  del  residuo  coimputato  rimasto  estraneo  a  quel    &#13;
 processo  e  successivamente  sottoposto  al suo giudizio con il rito    &#13;
 ordinario;                                                               &#13;
     che, infine, a giudizio  della  Corte  remittente,  escludere  la    &#13;
 sussistenza   dell'incompatibilità   soltanto  in  virtù  del  dato    &#13;
 meramente formale dell'assenza nella sentenza  di  cui  all'art.  444    &#13;
 cod.  proc.    pen.  di  qualsiasi  accertamento  di responsabilità,    &#13;
 significherebbe negare l'evidenza e  trascurare  l'esistenza  di  una    &#13;
 valutazione  probatoria di analoga pregnanza rispetto a quella svolta    &#13;
 nel  giudizio  dibattimentale,  destinata  ad  incidere  sul   futuro    &#13;
 dibattimento.                                                            &#13;
   Considerato  che la Corte di cassazione dubita, in riferimento agli    &#13;
 articoli 3 e 24 della Costituzione, della legittimità costituzionale    &#13;
 dell'articolo 34, comma 2, del  codice  di  procedura  penale,  nella    &#13;
 parte   in   cui  non  prevede  l'incompatibilità  del  giudice  del    &#13;
 dibattimento  che  abbia  precedentemente  pronunciato  sentenza   di    &#13;
 applicazione  della pena ex art. 444 cod. proc. pen. nei confronti di    &#13;
 altri soggetti, valutando lo stesso fatto;                               &#13;
     che questa Corte nella  sentenza  n.  371  del  1996,  richiamata    &#13;
 nell'ordinanza    di    remissione,    ha    già    precisato    che    &#13;
 l'incompatibilità sussiste in tutti i casi in cui sia stata espressa    &#13;
 nella sentenza che definisce il giudizio, sia  pure  incidentalmente,    &#13;
 una  valutazione in ordine alla responsabilità penale di un terzo, e    &#13;
 successivamente  ha  puntualizzato  che  adottare  una  sentenza   di    &#13;
 applicazione  della pena su richiesta nei confronti di un concorrente    &#13;
 nel reato non significa necessariamente esprimere  valutazioni  circa    &#13;
 la  responsabilità  penale  degli  ulteriori concorrenti estranei al    &#13;
 processo (ordinanze nn. 281 e 127 del 1999),  come  d'altronde  avere    &#13;
 valutato  una  prova  nei  confronti  di  un  imputato  non vuol dire    &#13;
 necessariamente esprimere  una  valutazione  di  responsabilità  nei    &#13;
 confronti  di  terzi che non hanno partecipato al giudizio (ordinanza    &#13;
 n. 135 del 1999);                                                        &#13;
     che non vi è ragione di argomentare ulteriormente in ordine agli    &#13;
 anzidetti precedenti, sicché la  questione  deve  essere  dichiarata    &#13;
 manifestamente infondata.                                                &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'articolo  34,  comma  2, del codice di procedura    &#13;
 penale,  sollevata,  in  riferimento  agli  artt.  3   e   24   della    &#13;
 Costituzione,  dalla  Corte di cassazione con l'ordinanza indicata in    &#13;
 epigrafe.                                                                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 14 luglio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Mezzanotte                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 luglio 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
