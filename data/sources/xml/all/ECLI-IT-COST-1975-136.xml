<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>136</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:136</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Guido Astuti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/06/1975</data_decisione>
    <data_deposito>11/06/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  509 del  &#13;
 codice penale, promosso con ordinanza  emessa  il  6  luglio  1973  dal  &#13;
 pretore  di  Carini nel procedimento penale a carico di Puleo Caterina,  &#13;
 iscritta al n. 390 del  registro  ordinanze  1973  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 294 del 14 novembre 1973.         &#13;
     Udito  nella  camera  di  consiglio  del  6  marzo  1975 il Giudice  &#13;
 relatore Guido Astuti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso di un procedimento penale a  carico  di  Puleo  Caterina,  &#13;
 imputata  del  reato di cui all'art. 509 del codice penale per non aver  &#13;
 osservato gli obblighi derivanti dal contratto  collettivo  corporativo  &#13;
 per  il  personale  dipendente da esattorie e ricevitorie delle imposte  &#13;
 dirette,  stipulato  il  31  dicembre  1939,  il  pretore  di   Carini,  &#13;
 accogliendo   l'eccezione   proposta  dal  difensore  della  Puleo,  ha  &#13;
 sollevato, in riferimento all'art. 3 Cost., questione  di  legittimità  &#13;
 costituzionale  dell'art. 509 del codice penale. Tale disposizione, nel  &#13;
 configurare come delitto  punito  con  la  multa  l'inosservanza  degli  &#13;
 obblighi  derivanti  dai  contratti collettivi corporativi, sarebbe, ad  &#13;
 avviso del giudice a quo, in contrasto con il principio di eguaglianza,  &#13;
 atteso  che  per  l'art.  8  della  legge  14  luglio  1959,  n.   741,  &#13;
 l'inosservanza   degli  obblighi  derivanti  dai  contratti  collettivi  &#13;
 postcorporativi  costituisce  solo  una  contravvenzione,  punita   con  &#13;
 l'ammenda.                                                               &#13;
     L'ordinanza   è   stata   ritualmente   notificata,  comunicata  e  &#13;
 pubblicata.                                                              &#13;
     Nel giudizio innanzi a questa Corte non vi è stata costituzione di  &#13;
 parte, né intervento del Presidente  del  Consiglio  dei  ministri  e,  &#13;
 pertanto,  la  causa  viene  decisa  in  camera  di  consiglio ai sensi  &#13;
 dell'art. 9 delle norme integrative.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con l'ordinanza di rimessione viene sollevata la questione  di  &#13;
 legittimità   costituzionale  dell'art.  509  del  codice  penale,  in  &#13;
 riferimento all'art. 3 della Costituzione.  Il pretore a quo,  chiamato  &#13;
 a  giudicare  persona  imputata  del  reato previsto dall'art. 509, "in  &#13;
 relazione  agli  artt.  25  e  32 del contratto collettivo nazionale di  &#13;
 lavoro per il personale dipendente da  esattorie  e  ricevitorie  delle  &#13;
 imposte  dirette,  stipulato  il  31  dicembre  1939", ha ravvisato una  &#13;
 ingiustificata disparità di trattamento, raffrontando la  disposizione  &#13;
 dell'art.  509,  che  per  l'inosservanza  degli  obblighi derivanti ai  &#13;
 datori  di  lavoro  dai  contratti  collettivi  corporativi  "configura  &#13;
 un'ipotesi  delittuosa  punita  con  la  multa",  con  la  disposizione  &#13;
 dell'art. 8 della legge 14 luglio 1959, n. 741, che per  l'inosservanza  &#13;
 degli obblighi derivanti dai contratti collettivi postcorporativi, resi  &#13;
 efficaci erga omnes in base alla delega conferita al Governo con l'art.  &#13;
 1  della  stessa  legge,  "configura  semplicemente  un reato di natura  &#13;
 contravvenzionale, punito con l'ammenda".                                &#13;
     Sarebbero così puniti con sanzioni diverse comportamenti  illeciti  &#13;
 sostanzialmente   identici,  in  contrasto  con  il  principio  sancito  &#13;
 dall'art.  3  della  Costituzione,  che  "garantendo  la  parità   dei  &#13;
 cittadini  di  fronte  alla  legge,  postula  che  ad  uguale  condotta  &#13;
 antigiuridica consegua uguale trattamento".                              &#13;
     2. - La questione non è  fondata.  Le  due  disposizioni  poste  a  &#13;
 confronto   dal  giudice  a  quo  costituiscono  fattispecie  normative  &#13;
 diverse, anche  se  concernenti  situazioni  analoghe,  e  illeciti  di  &#13;
 carattere  parzialmente  identico.  L'art. 509, primo comma, del codice  &#13;
 penale (la  cui  legittimità,  sotto  altro  profilo,  in  riferimento  &#13;
 all'art.    39 della Costituzione, è già stata riconosciuta da questa  &#13;
 Corte con sentenza n. 55 del 1957), sanziona la inosservanza, da  parte  &#13;
 del  datore  di  lavoro  o  del lavoratore, delle norme disciplinanti i  &#13;
 rapporti di lavoro, con riferimento al sistema di fonti  caratteristico  &#13;
 già dell'ordinamento corporativo, soppresso con il decreto legislativo  &#13;
 luogotenenziale  23  novembre 1944, n. 369, il cui art. 43 ha mantenuto  &#13;
 tuttavia in vigore per i rapporti collettivi e  individuali,  salvo  le  &#13;
 successive  modifiche,  "le  norme  contenute nei contratti collettivi,  &#13;
 negli accordi economici, nelle sentenze della magistratura del lavoro e  &#13;
 nelle ordinanze corporative".  L'art. 8 della legge 14 luglio 1959,  n.  &#13;
 741,  punisce il datore di lavoro, inadempiente agli obblighi derivanti  &#13;
 dalle  norme  di  cui  all'art.  1  della  stessa  legge  (ossia  dalle  &#13;
 disposizioni  degli accordi economici e contratti collettivi di diritto  &#13;
 privato alle quali il Governo, con decreti  delegati,  abbia  conferito  &#13;
 efficacia  erga omnes), con un'ammenda da lire 5.000 a lire 100.000 per  &#13;
 ogni lavoratore cui si riferisce la violazione.                          &#13;
     Si tratta dunque di norme penali diverse, concernenti la violazione  &#13;
 di obblighi di diversa natura, con sanzioni diverse per contenuto e per  &#13;
 modalità di applicazione, comminate le prime tanto ai datori di lavoro  &#13;
 quanto ai lavoratori, le seconde invece ai soli datori di lavoro.        &#13;
     Anche considerandole limitatamente  agli  obblighi  dipendenti  dai  &#13;
 contratti  collettivi  corporativi,  formati  anteriormente  al 1944, e  &#13;
 rispettivamente dai successivi contratti collettivi di diritto privato,  &#13;
 oggetto delle norme transitorie della legge 14 luglio 1959, n. 741, non  &#13;
 è  possibile  istituire  un  puntuale  raffronto,  né  scorgere   una  &#13;
 disparità  di  trattamento,  tale da determinare l'incostituzionalità  &#13;
 dell'art. 509 del codice penale, per il fatto che il legislatore, nella  &#13;
 sua discrezionalità di apprezzamento, con la legge  n.  741  del  1959  &#13;
 abbia  ritenuto  di  colpire  con  sanzione  diversa l'inadempimento di  &#13;
 obblighi derivanti da rapporti formalmente e  sostanzialmente  diversi,  &#13;
 considerandolo come reato di natura contravvenzionale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 509 del codice penale, sollevata dal pretore  di  Carini  con  &#13;
 l'ordinanza  indicata  in  epigrafe,  in  riferimento  all'art. 3 della  &#13;
 Costituzione.                                                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 4 giugno 1975.          &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI - MICHELE ROSSANO.      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
