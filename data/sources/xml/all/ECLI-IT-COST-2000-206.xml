<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>206</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:206</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/2000</data_decisione>
    <data_deposito>16/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, Cesare &#13;
 RUPERTO, Riccardo CHIEPPA, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
 CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, &#13;
 Franco BILE, Giovanni Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 206 del decreto    &#13;
 legislativo  30  aprile  1992,  n. 285  (Nuovo  codice della strada),    &#13;
 promosso  con  ordinanza  emessa  il  17 luglio 1999 dal Tribunale di    &#13;
 Catania,  sezione  distaccata  di  Adrano,  nel  procedimento  civile    &#13;
 vertente  tra  C.R.  e il Servizio di riscossione tributi iscritta al    &#13;
 n. 550  del  registro  ordinanze  1999  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 41,  prima serie speciale, dell'anno    &#13;
 1999.                                                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio del 10 maggio 2000 il giudice    &#13;
 relatore Piero Alberto Capotosti.                                        &#13;
     Ritenuto  che  il  Tribunale  di  Catania,  sezione distaccata di    &#13;
 Adrano, con ordinanza in data 17 luglio 1999, solleva, in riferimento    &#13;
 agli  artt. 3  e  24  della  Costituzione,  questione di legittimità    &#13;
 costituzionale  dell'art. 206 del decreto legislativo 30 aprile 1992,    &#13;
 n. 285  (Nuovo codice della strada), nella parte in cui disciplina la    &#13;
 riscossione  delle  somme  dovute a titolo di sanzione amministrativa    &#13;
 pecuniaria  irrogata  per  violazione  delle  norme  del codice della    &#13;
 strada  mediante  rinvio  all'art. 27  della  legge 24 novembre 1981,    &#13;
 n. 689  (Modifiche  al sistema penale), il quale, a sua volta, rinvia    &#13;
 all'art. 54,  secondo  comma,  del  d.P.R.  29 settembre 1973, n. 602    &#13;
 (Disposizioni sulla riscossione delle imposte sul reddito);              &#13;
         che, secondo il rimettente, l'applicabilità alla riscossione    &#13;
 delle entrate in esame dell'art. 54, secondo comma, del d.P.R. n. 602    &#13;
 del  1973,  in virtù del richiamo contenuto nell'art. 27 della legge    &#13;
 n. 689  del  1981,  al  quale  rinvia  la norma impugnata, renderebbe    &#13;
 inammissibili le opposizioni ex artt. 615 e 617 cod. proc. civ.;         &#13;
         che,   ad   avviso   del   giudice   a  quo  l'applicabilità    &#13;
 dell'art. 54, secondo comma, del d.P.R. n. 602 del 1973 realizzerebbe    &#13;
 una  non  ragionevole limitazione del diritto di difesa del debitore,    &#13;
 dato che, in riferimento ad una entrata non tributaria qual è quella    &#13;
 in  esame,  non  sussisterebbe l'esigenza di garantire il programmato    &#13;
 afflusso  nelle casse dello Stato delle risorse indispensabili per la    &#13;
 spesa pubblica;                                                          &#13;
         che  è  intervenuto nel giudizio il Presidente del Consiglio    &#13;
 dei  Ministri, rappresentato dall'Avvocatura generale dello Stato, il    &#13;
 quale,  in  linea  preliminare,  ha  chiesto  che  la  questione  sia    &#13;
 dichiarata  inammissibile  per  sopravvenuto  difetto di rilevanza, a    &#13;
 seguito  delle  modificazioni  introdotte  dall'art. 16 del d.lgs. 26    &#13;
 febbraio  1999,  n. 46  -  le  quali renderebbero comunque necessario    &#13;
 ordinare  la  restituzione degli atti - e, nel merito, ha chiesto che    &#13;
 la  Corte la dichiari infondata in virtù delle argomentazioni svolte    &#13;
 nella sentenza n. 437 del 1995.                                          &#13;
     Considerato  che  il  Tribunale di Catania, sezione distaccata di    &#13;
 Adrano,  dubita  della  legittimità costituzionale dell'art. 206 del    &#13;
 d.lgs.  n. 285  del  1992,  nella  parte in cui, attraverso il rinvio    &#13;
 all'art. 27  della  legge  n. 689 del 1981, rendendo applicabile alla    &#13;
 riscossione  delle  entrate  in  esame  l'art. 54, secondo comma, del    &#13;
 d.P.R. n. 602 del 1973, realizzerebbe una non ragionevole limitazione    &#13;
 della tutela del debitore;                                               &#13;
         che,  in  data  anteriore  all'ordinanza  di  rimessione,  è    &#13;
 entrato  in  vigore  il  d.lgs.  26 febbraio 1999, n. 46, il quale ha    &#13;
 innovato la disciplina della riscossione coattiva;                       &#13;
         che,  in  particolare, l'art. 16 del d.lgs. n. 46 del 1999 ha    &#13;
 sostituito l'intero Titolo II del d.P.R. n. 602 del 1973, modificando    &#13;
 l'art. 54 e confermando, agli artt. 57 e 60, l'improponibilità delle    &#13;
 opposizioni  regolate dall'art. 615, cod. proc. civ., ad eccezione di    &#13;
 quelle  aventi  ad  oggetto  la  pignorabilità  dei  beni,  e  delle    &#13;
 opposizioni  disciplinate  dall'art. 617, cod. proc. civ. concernenti    &#13;
 la  regolarità  formale  e  la  notificazione  del titolo esecutivo,    &#13;
 disponendo  che  il  giudice  dell'esecuzione  non può sospendere il    &#13;
 processo  esecutivo,  salvo  che  ricorrano  fondati  motivi e vi sia    &#13;
 fondato pericolo di grave e irreparabile danno;                          &#13;
         che,  in  particolare,  l'art. 29  del  d.lgs. n. 46 del 1999    &#13;
 dispone   che,  ora,  per  le  entrate  non  tributarie,  il  giudice    &#13;
 competente  a  conoscere  le  controversie  concernenti il ruolo può    &#13;
 sospendere  la  riscossione  se  ricorrono  gravi  motivi  stabilendo    &#13;
 altresì  che  ad  esse  non  si  applica la disposizione del comma 1    &#13;
 dell'art. 57 del decreto del Presidente della Repubblica 29 settembre    &#13;
 1973,  n. 602  come sostituito dall'art. 16 del presente decreto e le    &#13;
 opposizioni all'esecuzione ed agli atti esecutivi si propongono nelle    &#13;
 forme  ordinarie  e  che  ad  esecuzione  iniziata  il  giudice  può    &#13;
 sospendere  la  riscossione  solo  in presenza dei presupposti di cui    &#13;
 all'art. 60  del decreto del Presidente della Repubblica 29 settembre    &#13;
 1973, n. 602, come sostituito dall'art. 16 del presente decreto;         &#13;
         che  il  predetto art. 29 riguarda anche le entrate derivanti    &#13;
 dall'irrogazione di sanzioni amministrative pecuniarie per infrazioni    &#13;
 alle  norme  del codice della strada, in quanto esse non hanno natura    &#13;
 tributaria;                                                              &#13;
         che,  nonostante  la  nuova  disciplina sia entrata in vigore    &#13;
 anteriormente  alla  data  dell'ordinanza  di  rimessione,  essa  non    &#13;
 risulta  presa in esame dal giudice a quo il quale, conseguentemente,    &#13;
 non   ha   esplicitato  se  il  mutamento  del  quadro  normativo  di    &#13;
 riferimento  abbia eventualmente inciso, ed entro quali limiti, sulla    &#13;
 fattispecie sottoposta al suo esame;                                     &#13;
         che  la mancanza di ogni specificazione al riguardo determina    &#13;
 la  carenza  della motivazione in ordine alle ragioni che, secondo il    &#13;
 rimettente, fanno ritenere la perdurante rilevanza della questione;      &#13;
         che,   pertanto,   la   questione   deve   essere  dichiarata    &#13;
 manifestamente inammissibile.                                            &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87  e  9,  secondo  comma,  delle  norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale dell'art. 206 del decreto legislativo 30    &#13;
 aprile  1992,  n. 285 (Nuovo codice della strada), nella parte in cui    &#13;
 disciplina  la  riscossione  delle  somme dovute a titolo di sanzione    &#13;
 amministrativa  pecuniaria  irrogata  per  violazione delle norme del    &#13;
 codice  della  strada,  sollevata,  in  riferimento agli artt. 3 e 24    &#13;
 della  Costituzione,  dal Tribunale di Catania, sezione distaccata di    &#13;
 Adrano, con l'ordinanza in epigrafe.                                     &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 giugno 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 16 giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
