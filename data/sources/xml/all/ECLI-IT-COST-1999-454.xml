<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>454</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:454</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/12/1999</data_decisione>
    <data_deposito>17/12/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio &#13;
 ONIDA, prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI, &#13;
 dott. Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 446, comma 1,    &#13;
 del codice di procedura penale, promosso con ordinanza emessa  il  18    &#13;
 giugno  1998 dalla Corte d'appello di Venezia nel procedimento penale    &#13;
 a carico di B. A. ed altro, iscritta al n. 821 del registro ordinanze    &#13;
 1998 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  45,    &#13;
 prima serie speciale, dell'anno 1998.                                    &#13;
   Visto  l'atto  di  costituzione  di  Harry  Richter in proprio e in    &#13;
 qualità di legale rappresentante della "ICI Italia" S.p.a;              &#13;
   Udito  nell'udienza  pubblica  del  23  novembre  1999  il  giudice    &#13;
 relatore Guido Neppi Modona;                                             &#13;
   Udito l'avvocato Perla Sciretti per Harry Richter.                     &#13;
   Ritenuto  che  la  Corte  di  appello  di  Venezia ha sollevato, in    &#13;
 riferimento agli artt.  3  e  24  della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale  dell'art.  446,  comma 1, del codice di    &#13;
 procedura penale, nella parte in cui non prevede la  possibilità  di    &#13;
 applicazione  della  pena su richiesta delle parti anche nel giudizio    &#13;
 di  appello,  quando  in  esso  si  proceda  alla  rinnovazione   del    &#13;
 dibattimento a norma dell'art. 604, comma 6, cod. proc. pen;             &#13;
     che  il  rimettente  premette  che nel giudizio di primo grado il    &#13;
 pretore, dopo aver  dichiarato  la  contumacia  di  un  imputato,  in    &#13;
 accoglimento   dell'eccezione   preliminare  della  difesa  di  altro    &#13;
 imputato,  a  cui  aveva  aderito  la  difesa  del  contumace,  aveva    &#13;
 pronunciato  in  limine  litis, prima dell'apertura del dibattimento,    &#13;
 sentenza di non doversi procedere ai sensi degli artt. 129 e 529 cod.    &#13;
 proc. pen. per mancanza di valida querela;                               &#13;
     che avverso  la  sentenza  aveva  proposto  appello  il  pubblico    &#13;
 ministero e che la Corte di appello, nella prima udienza del giudizio    &#13;
 di  impugnazione,  ritenuta la validità dell'atto di querela, si era    &#13;
 riservata di  disporre  la  rinnovazione  del  dibattimento  a  norma    &#13;
 dell'art. 604, comma 6, cod. proc. pen., previo esame delle eccezioni    &#13;
 preliminari delle parti;                                                 &#13;
     che  alla  successiva  udienza  la  difesa  dell'imputato rimasto    &#13;
 contumace  aveva  depositato  procura  speciale  con   richiesta   di    &#13;
 applicazione  della  pena,  sulla  quale ilprocuratore generale aveva    &#13;
 espresso il proprio consenso, mentre la parte civile si era  opposta,    &#13;
 osservando  che l'imputato era decaduto dalla facoltà di chiedere il    &#13;
 patteggiamento, in quanto l'art. 446, comma 1,  cod.  proc.  pen.  ne    &#13;
 prevede   l'esercizio   sino   alla  dichiarazione  di  apertura  del    &#13;
 dibattimento di primo grado;                                             &#13;
     che il giudice rimettente rileva che  la  formulazione  letterale    &#13;
 dell'art.  446, comma 1, cod. proc. pen. non consente interpretazioni    &#13;
 estensive  e,  comunque,  preclude  la   possibilità   di   chiedere    &#13;
 l'applicazione  della  pena  nel  giudizio  di  appello, anche quando    &#13;
 l'istruzione dibattimentale venga svolta per la prima volta  in  tale    &#13;
 fase ai sensi dell'art. 604, comma 6, cod. proc. pen;                    &#13;
     che  pertanto  la  richiesta di applicazione della pena formulata    &#13;
 per la prima volta nel giudizio di appello doveva ritenersi tardiva;     &#13;
     che la preclusione di cui all'art. 446, comma 1, cod. proc.  pen.    &#13;
 impedirebbe   all'imputato   di   usufruire   dei  vantaggi  connessi    &#13;
 all'istituto dell'applicazione della pena  solo  perché  nella  fase    &#13;
 degli  atti introduttivi al dibattimento il giudice di primo grado ha    &#13;
 emesso una erronea sentenza di proscioglimento;                          &#13;
     che in tale situazione - in cui, ad  avviso  del  rimettente,  il    &#13;
 mancato  esercizio  della  facoltà  di chiedere l'applicazione della    &#13;
 pena non è addebitabile in alcun modo "ad errata scelta  processuale    &#13;
 dell'imputato  o  a sua colposa inerzia", avendo questi esercitato il    &#13;
 diritto di proporre questioni  preliminari  circa  la  procedibilità    &#13;
 dell'azione  penale  prima della richiesta di cui all'art. 444, comma    &#13;
 1, cod. proc. pen.  -  verrebbe  a  determinarsi  una  ingiustificata    &#13;
 compressione  del  diritto  di  difesa,  derivante  da "un evento non    &#13;
 evitabile ed esterno alla volontà del prevenuto"  (al  riguardo,  il    &#13;
 rimettente  richiama  la  sentenza  di questa Corte n. 101 del 1993),    &#13;
 nonché una irragionevole disparità di trattamento nei confronti  di    &#13;
 quegli  imputati  ai  quali  sia precluso in appello, a seguito della    &#13;
 rinnovazione del dibattimento ai sensi dell'art. 604, comma  6,  cod.    &#13;
 proc. pen., di usufruire dei benefici del patteggiamento;                &#13;
     che  si  è  costituito  nel  presente giudizio Harry Ricther, in    &#13;
 proprio e in qualità di legale  rappresentante  della  "ICI  Italia"    &#13;
 S.p.a.,  parte  civile  nel procedimento penale davanti alla Corte di    &#13;
 appello di Venezia, rappresentato e difeso dagli avvocati Corso Bovio    &#13;
 e  Perla  Sciretti,  chiedendo  che  la  questione   sia   dichiarata    &#13;
 infondata;                                                               &#13;
     che,  in  particolare, la parte costituita rileva che all'esordio    &#13;
 del dibattimento di primo grado - e, dunque,  entro  il  termine  per    &#13;
 proporre  richiesta  di  applicazione  della  pena  -  il difensore e    &#13;
 procuratore  speciale  dell'imputato  contumace  aveva  "ritenuto  di    &#13;
 concentrare  la  difesa sull'obiettivo processuale" della sentenza di    &#13;
 improcedibilità per difetto di valida querela,  senza  coltivare  la    &#13;
 possibilità   di   presentare  anche  richiesta  di  patteggiamento;    &#13;
 richiesta  che  non  avrebbe  comunque  precluso   alla   difesa   di    &#13;
 sollecitare  anche  la  pronuncia  di  una  sentenza  di  non luogo a    &#13;
 procedere per difetto di valida querela, ai sensi degli artt.  129  e    &#13;
 444, comma 2, cod. proc. pen;                                            &#13;
     che,   pertanto,  la  sentenza  additiva  richiesta  dal  giudice    &#13;
 rimettente in realtà mirerebbe a  porre  rimedio  ad  una  omissione    &#13;
 della  difesa  dell'imputato,  mediante l'introduzione di un istituto    &#13;
 che verrebbe ad ampliare la disciplina del cosiddetto  patteggiamento    &#13;
 in appello previsto dall'art. 599 cod. proc. pen.                        &#13;
   Considerato  che  la  Corte  di  appello  di  Venezia  ha sollevato    &#13;
 questione di legittimità costituzionale dell'art. 446, comma 1, cod.    &#13;
 proc.   pen., nella parte in  cui  non  prevede  la  possibilità  di    &#13;
 formulare  richiesta di applicazione della pena anche nel giudizio di    &#13;
 appello quando in esso si proceda alla rinnovazione del  dibattimento    &#13;
 a  norma  dell'art.  604,  comma  6,  cod.  proc.  pen., in quanto la    &#13;
 disciplina censurata comporterebbe  una  ingiustificata  compressione    &#13;
 del  diritto  di  difesa,  in  violazione  degli  artt.  3 e 24 della    &#13;
 Costituzione;                                                            &#13;
     che il  giudice  rimettente,  nel  prospettare  la  questione  di    &#13;
 legittimità  costituzionale,  muove  dal  presupposto  che  l'omessa    &#13;
 presentazione della richiesta di applicazione  della  pena  entro  il    &#13;
 termine  previsto dalla norma censurata sia conseguenza di "un evento    &#13;
 non evitabile ed esterno alla volontà del prevenuto",  rappresentato    &#13;
 dalla sentenza di non doversi procedere per difetto di querela emessa    &#13;
 dal  pretore  nella  fase  degli atti introduttivi al dibattimento di    &#13;
 primo grado, poi ritenuta errata dal giudice di appello;                 &#13;
     che  tale  presupposto  non   trova   riscontro   nella   vicenda    &#13;
 processuale   su  cui  si  è  innestata  la  presente  questione  di    &#13;
 legittimità costituzionale;                                             &#13;
     che infatti, come emerge dalla stessa ordinanza di rimessione, la    &#13;
 sentenza di non doversi procedere, pronunciata ex  artt.  129  e  529    &#13;
 cod.  proc.  pen.  in  limine  litis  e  cioè  prima  della  formale    &#13;
 dichiarazione di apertura del dibattimento, era stata sollecitata  al    &#13;
 pretore,  subito dopo la costituzione del rapporto processuale, dalla    &#13;
 difesa di entrambi gli imputati;                                         &#13;
     che  l'anticipazione dell'epilogo dibattimentale e il conseguente    &#13;
 superamento del termine ultimo entro cui doveva essere presentata  la    &#13;
 richiesta  di  applicazione  della  pena sono dipesi dal fatto che in    &#13;
 primo  grado  il  difensore  dell'imputato  rimasto  contumace  aveva    &#13;
 aderito all'eccezione preliminare relativa alla regolarità dell'atto    &#13;
 di querela proposta dal difensore del coimputato, senza esercitare la    &#13;
 facoltà  di  presentare  contestualmente, e in subordine, tempestiva    &#13;
 richiesta di patteggiamento;                                             &#13;
     che  pertanto   l'omessa   presentazione   della   richiesta   di    &#13;
 applicazione  della  pena entro il termine di cui all'art. 446, comma    &#13;
 1, cod. proc.   pen. è dipesa dalla  scelta  difensiva,  liberamente    &#13;
 esercitata,   di   sollecitare  in  via  esclusiva  la  richiesta  di    &#13;
 proscioglimento  anticipato  per  un  supposto  vizio  dell'atto   di    &#13;
 querela;                                                                 &#13;
     che  non è quindi conferente il richiamo alla sentenza di questa    &#13;
 Corte  n.  101  del  1993,  che  si  riferisce  ad  un  caso  in  cui    &#13;
 l'inosservanza   del   termine   per   presentare   la  richiesta  di    &#13;
 applicazione della pena era stata effettivamente "determinata  da  un    &#13;
 evento  non  evitabile dall'interessato", e cioè dal suo legittimo e    &#13;
 assoluto impedimento, del quale era pervenuta in ritardo  notizia,  a    &#13;
 presenziare all'udienza dibattimentale;                                  &#13;
     che ove l'imputato, se presente al dibattimento di primo grado, o    &#13;
 il  suo  difensore, se munito di procura speciale, avesse esercitato,    &#13;
 subordinatamente alla richiesta di proscioglimento ex  art. 129  cod.    &#13;
 proc.  pen.,  la  facoltà di presentare tempestivamente richiesta di    &#13;
 applicazione della pena, il giudice di  appello  avrebbe  potuto,  in    &#13;
 applicazione  dell'art.  604,  comma  6, cod. proc. pen., pronunciare    &#13;
 sentenza   di   patteggiamento   in   riforma   della   sentenza   di    &#13;
 proscioglimento di primo grado;                                          &#13;
     che,  di conseguenza, la questione di legittimità costituzionale    &#13;
 deve essere dichiarata manifestamente infondata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  446,  comma  1,  del  codice  di procedura    &#13;
 penale,  sollevata,  in  riferimento  agli  artt.  3   e   24   della    &#13;
 Costituzione,  dalla  Corte di appello di Venezia, con l'ordinanza in    &#13;
 epigrafe.                                                                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 13 dicembre 1999.                             &#13;
                        Il Presidente: Vassalli                           &#13;
                       Il redattore: Neppi Modona                         &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 17 dicembre 1999.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
