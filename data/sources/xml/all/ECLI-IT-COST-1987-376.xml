<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>376</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:376</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1987</data_decisione>
    <data_deposito>04/11/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giovanni CONSO, prof. Ettore &#13;
 GALLO, dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele PESCATORE, &#13;
 avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 635, secondo    &#13;
 comma, del codice di procedura civile, promosso con ordinanza  emessa    &#13;
 il  25  giugno  1983 dal Pretore di La Spezia, iscritta al n. 215 del    &#13;
 registro ordinanze 1984 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 197 dell'anno 1984;                                        &#13;
    Visti  l'atto  di  costituzione  dell'I.N.P.S.  nonché  l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio del 14 ottobre 1987 il Giudice    &#13;
 relatore Virgilio Andrioli;                                              &#13;
    Ritenuto che con ordinanza emessa il 25 giugno 1983 (notificata il    &#13;
 12 e  comunicata  il  13  del  successivo  luglio;  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  n.  197  del 18 luglio 1984 e iscritta al n. 215    &#13;
 R.O. 1984) sulle opposizioni proposte nei confronti dell'I.N.P.S.  da    &#13;
 Bruno Teresa e da Del Vecchio Carmine e Di Staso Francesco ai decreti    &#13;
 ingiuntivi  emessi  sulla  base  delle  attestazioni  del   direttore    &#13;
 provinciale  dell'I.N.P.S.  di  La Spezia, il Pretore di La Spezia ha    &#13;
 giudicato rilevante e, in riferimento agli artt. 3 e  24  Cost.,  non    &#13;
 manifestamente  infondata la questione di legittimità costituzionale    &#13;
 dell'art.  635  co.  2°  c.p.c.  nei  limiti  in  cui   prevede   che    &#13;
 l'attestazione  di  un  funzionario  sia  documento  idoneo,  al fine    &#13;
 dell'emissione del decreto ingiuntivo, per i crediti  degli  Enti  di    &#13;
 Previdenza e Assistenza derivanti da omesso versamento dei contributi    &#13;
 (orientamento interpretativo accolto dalla Corte di Cassazione);         &#13;
      che   avanti   la   Corte  si  sono  costituiti,  nell'interesse    &#13;
 dell'I.N.P.S., gli avv.ti Gianni Romoli  e  Mario  Procaccio,  giusta    &#13;
 delega in calce all'atto depositato il 14 settembre 1984 argomentando    &#13;
 e concludendo per la dichiarazione di costituzionalità  della  norma    &#13;
 impugnata  e  che  ha  spiegato  intervento  per  il  Presidente  del    &#13;
 Consiglio dei ministri                                                   &#13;
 l'Avvocatura  generale  dello  Stato  con atto depositato il 7 agosto    &#13;
 1984 nel quale ha argomentato e concluso per  la  infondatezza  della    &#13;
 proposta questione;                                                      &#13;
    Considerato   che   le   attestazioni  dei  direttori  delle  sedi    &#13;
 provinciali dell'I.N.P.S.,  riguardanti  l'ammontare  dei  contributi    &#13;
 previdenziali dovuti dal datore di lavoro all'Istituto, devono essere    &#13;
 fondate sugli  accertamenti  eseguiti  dai  funzionari  dell'Istituto    &#13;
 stesso  e  che,  in occasione di tali accertamenti, ove correttamente    &#13;
 eseguiti, il datore di  lavoro  o  il  suo  rappresentante  hanno  la    &#13;
 possibilità di formulare osservazioni e contestazioni;                  &#13;
      che  l'attendibilità  riconosciuta,  nella  fase  di cognizione    &#13;
 sommaria, a tutta una serie di atti provenienti dallo Stato o da enti    &#13;
 pubblici creditori trova il suo fondamento nell'esigenza di garantire    &#13;
 a tutte le parti del processo sia attrici che convenute una  incisiva    &#13;
 tutela giudiziaria dei propri diritti;                                   &#13;
      che, per le suesposte ragioni, la norma impugnata non vulnera il    &#13;
 diritto di difesa, né può essere validamente posta  a  raffronto  -    &#13;
 stante  la  strutturale  diversità  di  situazioni regolate - con la    &#13;
 nuova procedura contenziosa introdotta con la legge 24 novembre 1981,    &#13;
 n. 689;                                                                  &#13;
      che  in  conclusione  la  questione  si  appalesa manifestamente    &#13;
 infondata;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 635, 2° comma  c.p.c.  nella  parte  in  cui    &#13;
 qualifica  prove  idonee  all'emanazione  di  decreti  ingiuntivi gli    &#13;
 accertamenti eseguiti dai funzionari degli enti di  previdenza  e  di    &#13;
 assistenza   sull'omesso  versamento  di  contributi,  sollevata  dal    &#13;
 Pretore di La Spezia con ordinanza del 25 giugno 1983  (n.  215  R.O.    &#13;
 1984).                                                                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1987.                              &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: ANDRIOLI                             &#13;
    Depositata in cancelleria il 4 novembre 1987.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
