<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>37</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:37</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>11/02/2008</data_decisione>
    <data_deposito>21/02/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>ammissibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica del 30 gennaio 2007 (Doc. IV-ter, n. 2-A), relativa alla insindacabilità  delle opinioni espresse dal senatore Raffaele Iannuzzi, nei confronti di Giancarlo Caselli, già Procuratore della Repubblica presso il Tribunale di Palermo, e di altri magistrati, promosso con ricorso del Giudice per le indagini preliminari del Tribunale di Milano, depositato in cancelleria il 24 luglio 2007 ed iscritto al n. 11 del registro conflitti tra poteri dello Stato 2007, fase di ammissibilità.  &#13;
    Udito nella camera di consiglio del 30 gennaio 2008 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che, con ricorso dell'8 maggio 2007, il Giudice per le indagini preliminari del Tribunale di Milano ha promosso conflitto di attribuzione tra poteri dello Stato nei confronti del Senato della Repubblica, in relazione alla delibera adottata il 30 gennaio 2007 (Doc. IV-ter, n. 2-A), con la quale - in conformità alla proposta della Giunta delle elezioni e delle immunità parlamentari - è stato dichiarato che i fatti per i quali il senatore Raffaele Iannuzzi è sottoposto a procedimento penale per il delitto di diffamazione a mezzo stampa riguardano opinioni espresse da quest'ultimo nell'esercizio delle sue funzioni parlamentari e sono, quindi, insindacabili ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    che il ricorrente osserva di essere chiamato a giudicare il predetto senatore per il reato sopra indicato commesso ai danni di Giancarlo Caselli, Guido Lo Forte, Roberto Scarpinato e Gioacchino Natoli, i quali, nelle rispettive qualità di Procuratore della Repubblica, Procuratori della Repubblica Aggiunti e Sostituto Procuratore presso il Tribunale di Palermo, hanno ritenuto che la loro reputazione fosse stata offesa da un articolo pubblicato il 23 ottobre 2003 dal quotidiano “Il Giornale”; &#13;
    che il ricorrente, dopo aver riportato il contenuto dell'articolo citato, con il quale l'imputato avrebbe denunciato presunti interessamenti politici sulla Procura di Palermo tesi ad orientarne l'attività investigativa antimafia per mezzo dei magistrati sopra indicati, ritiene che dalla relazione della Giunta delle elezioni e delle immunità parlamentari non sarebbe emerso alcun atto tipico della funzione parlamentare cui ricollegare le frasi oggetto di imputazione, ma solo un generico riferimento all'impegno politico del senatore R.I. sui temi della criminalità mafiosa e del suo contrasto; &#13;
    che, pertanto, sulla base della giurisprudenza costituzionale, nel caso di specie non ricorrerebbe alcun nesso funzionale, tra l'attività divulgativa esterna e l'attività parlamentare, idoneo a far operare la garanzia ex art. 68, primo comma, della Costituzione. &#13;
    Considerato che, in questa fase del giudizio, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, la Corte costituzionale è chiamata a deliberare, senza contraddittorio, circa l'esistenza o meno della «materia di un conflitto la cui risoluzione spetti alla sua competenza», restando impregiudicata ogni ulteriore decisione, anche in punto di ammissibilità; &#13;
    che nella fattispecie sussistono i requisiti soggettivo ed oggettivo del conflitto;  &#13;
    che, infatti, quanto al requisito soggettivo, devono ritenersi legittimati ad essere parte del presente conflitto sia il Giudice per le indagini preliminari del Tribunale di Milano, in quanto organo giurisdizionale, in posizione di indipendenza costituzionalmente garantita - competente a dichiarare definitivamente, per il procedimento di cui è investito, la volontà del potere cui appartiene -, sia il Senato della Repubblica, in quanto organo competente a dichiarare definitivamente la propria volontà in ordine all'applicabilità dell'art. 68, primo comma, della Costituzione; &#13;
    che, quanto al profilo oggettivo, sussiste la materia del conflitto, dal momento che il ricorrente lamenta la lesione della propria sfera di attribuzioni, costituzionalmente garantita, da parte della impugnata deliberazione del Senato della Repubblica; &#13;
    che, pertanto, esiste la materia di un conflitto, la cui risoluzione spetta alla competenza di questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi  &#13;
LA CORTE COSTITUZIONALE  &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dal Giudice per le indagini preliminari del Tribunale di Milano, nei confronti del Senato della Repubblica, con l'atto indicato in epigrafe; &#13;
    dispone: &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al Giudice per le indagini preliminari del Tribunale di Milano, ricorrente; &#13;
    b) che, a cura del ricorrente, l'atto introduttivo e la presente ordinanza siano notificati al Senato della Repubblica, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione, per essere successivamente depositati, con la prova dell'avvenuta notifica, presso la cancelleria della Corte entro il termine di venti giorni, previsto dall'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'11 febbraio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 21 febbraio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
