<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>16</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:16</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia>Carlo Mezzanotte</redattore_pronuncia>
    <data_decisione>16/01/2003</data_decisione>
    <data_deposito>30/01/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'articolo 13, commi 4, 5, 6 e 8 del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promossi con ordinanze del 22 gennaio 2001 dal Tribunale di Milano, in composizione monocratica, e del 24 aprile 2001 dal Giudice istruttore del Tribunale di Vicenza, iscritte al n. 340 e al n. 940 del registro ordinanze 2001 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 20 e n. 48, prima serie speciale, dell'anno 2001. &#13;
    Udito nella camera di consiglio del 4 dicembre 2002 il Giudice relatore Carlo Mezzanotte. &#13;
    Ritenuto che, con ordinanza in data 22 gennaio 2001 (r.o. n. 340 del 2001), il Tribunale di Milano, in composizione monocratica, ha sollevato, in riferimento all'articolo 13, terzo comma, della Costituzione, questione di legittimità costituzionale dell'art. 13, commi 4 e 5, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), “nella parte in cui non prevede che il provvedimento amministrativo che dispone l'accompagnamento immediato alla frontiera debba essere sottoposto al controllo dell'autorità giudiziaria entro le 48 ore dalla sua adozione”; &#13;
    che il remittente, nel rilevare che i primi tre commi dell'art. 13 del d. lgs. n. 286 del 1998 stabiliscono i casi in cui può essere emesso il decreto di espulsione nei confronti degli stranieri, mentre i commi 4 e 5 della citata disposizione fissano i casi in cui il decreto di espulsione deve o può essere eseguito mediante accompagnamento alla frontiera, osserva che “nessuna norma prevede un controllo sul provvedimento amministrativo [del questore, comma 4, lettera a), o del prefetto, negli altri casi] da parte dell'autorità giudiziaria”; &#13;
    che, ad avviso del remittente, la coazione necessaria per condurre materialmente una persona con un mezzo di trasporto al suo Stato di provenienza non potrebbe non comportare una restrizione della libertà personale rientrante tra quelle previste in generale dall'art. 13, secondo comma, della Costituzione, sicché in tali casi, la mancanza di qualsivoglia controllo da parte dell'autorità giudiziaria comporterebbe la violazione del terzo comma del menzionato art. 13; &#13;
    che, secondo il giudice a quo, al provvedimento di espulsione mediante accompagnamento alla frontiera non possono riferirsi le disposizioni dell'art. 14 del medesimo d.lgs. n. 286 del 1998, in quanto la convalida ivi prevista si riferisce unicamente al provvedimento di trattenimento presso i centri di permanenza e assistenza temporanea, mentre la mancata convalida non travolgerebbe il provvedimento di espulsione con accompagnamento, che continuerebbe a gravare sullo straniero; &#13;
    che, con ordinanza in data 24 aprile 2001 (r.o. n. 940 del 2001), il Giudice istruttore del Tribunale di Vicenza ha sollevato, in riferimento all'articolo 13 della Costituzione, questione di legittimità costituzionale dell'art. 13, commi 4, 5, 6 e 8 del decreto legislativo 25 luglio 1998, n. 286, “nella parte in cui non prevede che il provvedimento di accompagnamento coattivo alla frontiera sia comunicato alla autorità giudiziaria entro 48 ore e assoggettato a convalida entro le successive 48 ore”; &#13;
    che il remittente rileva che questa Corte, con la sentenza n. 105 del 2001, ha chiaramente affermato il principio per cui il sindacato del giudice della convalida investe non solo la misura del trattenimento, ma anche l'espulsione amministrativa nella sua specifica modalità di esecuzione consistente nell'accompagnamento coattivo alla frontiera, il quale, quindi, inciderebbe direttamente sulla libertà personale dello straniero destinatario del provvedimento e, pertanto, dovrebbe essere assistito dalle garanzie previste dall'art. 13 della Costituzione; &#13;
    che viceversa, si argomenta nell'ordinanza di rimessione, il giudice viene ad essere investito della questione solo se lo straniero destinatario del provvedimento proponga ricorso avverso il decreto di espulsione e non anche ogni volta in cui tale misura venga disposta ed eseguita; e comunque, nel caso di esecuzione mediante accompagnamento alla frontiera, il termine di 30 giorni per l'impugnazione del decreto di espulsione non si accorderebbe con quelli più brevi fissati dall'art. 13 della Costituzione; &#13;
    che in conclusione, secondo il giudice a quo, non potrebbe ricorrersi a una interpretazione adeguatrice, dal momento che la disposizione censurata non prevede alcun controllo giurisdizionale del provvedimento che dispone l'accompagnamento coattivo alla frontiera prima della esecuzione della misura e in ogni caso non contempla neppure un obbligo di comunicazione del provvedimento alla autorità giudiziaria da parte della autorità di polizia; &#13;
    che è intervenuto in entrambi i giudizi il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto, riportandosi alle deduzioni svolte in precedenti giudizi su analoghe questioni, che la questione sia dichiarata manifestamente inammissibile o infondata;  &#13;
    che con successivi atti l'Avvocatura dello Stato ha segnalato l'intervenuta adozione nella seduta del Consiglio dei ministri del 28 marzo 2002 di un decreto-legge che disciplinerebbe nuovamente la materia. &#13;
    Considerato che le ordinanze di rimessione propongono questioni analoghe e i relativi giudizi possono essere riuniti per essere decisi congiuntamente; &#13;
    che in esse si dubita, in riferimento all'articolo 13 della Costituzione, della legittimità costituzionale dell'art. 13, commi 4 e 5 (la censura è estesa ai commi 6 e 8 dal solo Giudice istruttore del Tribunale di Vicenza), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), in un caso, poiché tali disposizioni non prevedono che il provvedimento di accompagnamento coattivo alla frontiera debba essere sottoposto al controllo dell'autorità giudiziaria entro le 48 ore dalla sua adozione, e, nell'altro, anche per il fatto che non ne sia prevista la convalida entro le 48 ore successive; &#13;
    che, dopo le ordinanze di rimessione, la disciplina legislativa in tema di espulsione degli stranieri ha subito diverse modificazioni, dapprima ad opera del decreto-legge 4 aprile 2002, n. 51 (Disposizioni urgenti recanti misure di contrasto all'immigrazione clandestina e garanzie per soggetti colpiti da provvedimenti di accompagnamento alla frontiera), quindi della relativa legge di conversione 7 giugno 2002, n. 106, infine della legge 30 luglio 2002, n. 189 (Modifica alla normativa in materia di immigrazione e di asilo); &#13;
    che, pertanto, si rende necessaria la restituzione degli atti ai giudici remittenti affinché valutino se, alla luce delle sopravvenute modificazioni legislative che hanno interessato non solo le specifiche disposizioni censurate ma l'intero quadro normativo di riferimento, le questioni di legittimità costituzionale siano tuttora rilevanti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti al Tribunale di Milano, in composizione monocratica, e al Tribunale di Vicenza. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 16 gennaio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Carlo MEZZANOTTE, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 30 gennaio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
