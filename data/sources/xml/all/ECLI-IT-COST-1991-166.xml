<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>166</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:166</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/04/1991</data_decisione>
    <data_deposito>18/04/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO; avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 423, secondo    &#13;
 comma, del codice di procedura penale, promosso con ordinanza  emessa    &#13;
 il 15 novembre 1990 dal Giudice per le indagini preliminari presso il    &#13;
 Tribunale  militare  di La Spezia nel procedimento penale a carico di    &#13;
 Loiacono Salvatore, iscritta al n. 16 del registro ordinanze  1991  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 5, prima    &#13;
 serie speciale, dell'anno 1991;                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  20 marzo 1991 il Giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
    Ritenuto  che  nel  procedimento  penale  a  carico  di  Salvatore    &#13;
 Loiacono,  imputato  di  insubordinazione  con  minaccia  ed ingiuria    &#13;
 pluriaggravata, il giudice per  le  indagini  preliminari  presso  il    &#13;
 Tribunale  militare di La Spezia, con ordinanza del 15 novembre 1990,    &#13;
 ha sollevato d'ufficio - in riferimento agli  artt.  112  e  3  della    &#13;
 Costituzione  -  questione  di  legittimità costituzionale dell'art.    &#13;
 423, secondo comma, del nuovo codice di procedura penale "nella parte    &#13;
 in cui non prevede  un  meccanismo  processuale  idoneo  a  suscitare    &#13;
 l'esercizio  dell'azione  penale  da  parte  del pubblico ministero o    &#13;
 comunque a provocarne l'attivazione" in  ordine  ad  un  fatto  nuovo    &#13;
 emerso  nell'udienza  preliminare  e  per il quale si debba procedere    &#13;
 d'ufficio;                                                               &#13;
      che - secondo quanto esprime l'ordinanza di rinvio -  nel  corso    &#13;
 dell'udienza  preliminare  sono emersi, a carico dell'imputato, fatti    &#13;
 nuovi relativi ad ipotesi di reato di concorso in abbandono di  posto    &#13;
 aggravato  e di disobbedienza aggravata non enunciati nella richiesta    &#13;
 di rinvio a giudizio e per i  quali  è  prevista  la  procedibilità    &#13;
 d'ufficio;                                                               &#13;
      che   le  ipotesi  di  reato  non  contestate  all'imputato  non    &#13;
 risultano connesse, a norma dell'art. 12, primo comma, lett. b),  del    &#13;
 codice  di procedura penale, con il reato per cui è stato chiesto il    &#13;
 rinvio a giudizio ed il  pubblico  ministero  non  "ha  fatto  alcuna    &#13;
 richiesta  a  norma  dell'art.  423,  secondo  comma,  del  codice di    &#13;
 procedura penale o alcuna contestazione a norma del comma  primo  del    &#13;
 medesimo articolo";                                                      &#13;
      che,  ad  avviso  del  giudice  remittente, risulterebbe perciò    &#13;
 impossibile contestare all'imputato (e al concorrente) i fatti  nuovi    &#13;
 emersi  nel  corso  dell'udienza  preliminare  e  non sarebbe neppure    &#13;
 utilizzabile la norma di cui all'art. 331  del  codice  di  procedura    &#13;
 penale  "atteso  che  non  verrebbe sottoposto all'esame del pubblico    &#13;
 ministero nulla di più di quanto già da esso conosciuto  attraverso    &#13;
 la comunicazione di reato in ordine alla quale, peraltro, il pubblico    &#13;
 ministero ha già effettuato le proprie valutazioni definitive con la    &#13;
 richiesta di rinvio a giudizio";                                         &#13;
      che  -  secondo  il  giudice  a  quo  -  da  questi  presupposti    &#13;
 deriverebbe la possibilità, per il pubblico  ministero,  di  operare    &#13;
 una  sorta di archiviazione extra ordinem, sottratta al controllo del    &#13;
 giudice,  in   contrasto   con   il   principio   costituzionale   di    &#13;
 obbligatorietà   dell'azione  penale  sancito  dall'art.  112  della    &#13;
 Costituzione;                                                            &#13;
      che inoltre, sempre ad avviso del giudice  remittente,  analoghe    &#13;
 fattispecie  sarebbero  oggetto di una disciplina ingiustificatamente    &#13;
 differenziata, in violazione dell'art. 3 della Costituzione:  e  ciò    &#13;
 in  quanto la norma impugnata impedirebbe il controllo del giudice su    &#13;
 di una inattività del pubblico ministero in ordine  ai  fatti  nuovi    &#13;
 emersi  a  carico dell'imputato nel corso dell'udienza, mentre l'art.    &#13;
 409 del codice di procedura penale contempla  l'ipotesi  del  mancato    &#13;
 accoglimento  da  parte  del giudice della richiesta di archiviazione    &#13;
 del pubblico ministero e consente al giudice di emanare una ordinanza    &#13;
 che vincola il pubblico ministero a formulare l'imputazione;             &#13;
      che nel giudizio dinanzi alla Corte ha  spiegato  intervento  il    &#13;
 Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura generale dello Stato, chiedendo che la questione  sia    &#13;
 dichiarata infondata;                                                    &#13;
    Considerato  che  l'art.  423,  secondo comma, del nuovo codice di    &#13;
 procedura penale detta la disciplina delle "nuove contestazioni"  nel    &#13;
 corso  dell'udienza  preliminare, stabilendo che "se risulta a carico    &#13;
 dell'imputato un fatto nuovo non enunciato nella richiesta di  rinvio    &#13;
 a  giudizio, per il quale si debba procedere d'ufficio, il giudice ne    &#13;
 autorizza la contestazione se il pubblico ministero ne fa richiesta e    &#13;
 vi è il consenso dell'imputato;                                         &#13;
      che tale disposizione - come già affermato da questa Corte  con    &#13;
 l'ordinanza   n.   11   del   1991   -  non  viola  il  principio  di    &#13;
 obbligatorietà  dell'azione  penale  sancito  dall'art.  112   della    &#13;
 Costituzione,  poiché  il  pubblico  ministero  -  quando  nel corso    &#13;
 dell'udienza preliminare risulti  a  carico  dell'imputato  un  fatto    &#13;
 nuovo che configuri un reato perseguibile d'ufficio - è obbligato ad    &#13;
 esercitare  tale  azione e ad iscrivere la nuova notizia di reato nel    &#13;
 registro di cui  all'art.  335  del  codice  di  rito,  potendo  solo    &#13;
 scegliere   se   esercitare  per  tale  fatto  un'azione  separata  o    &#13;
 procedere, con il consenso dell'imputato,  alla  nuova  contestazione    &#13;
 nell'ambito  del  processo già in corso, con conseguente trattazione    &#13;
 unitaria delle due imputazioni;                                          &#13;
      che, non essendo violato, nella fattispecie  condotta  all'esame    &#13;
 di  questa  Corte,  l'art.  112  della Costituzione, risulta priva di    &#13;
 fondamento anche l'ulteriore censura di illegittimità costituzionale    &#13;
 formulata dal giudice a quo sul  presupposto  di  una  ingiustificata    &#13;
 diversità   -   sotto   il  profilo  del  controllo  giurisdizionale    &#13;
 sull'obbligatorio esercizio dell'azione penale -  tra  la  disciplina    &#13;
 dell'archiviazione  dettata  dall'art.  409  del  codice di procedura    &#13;
 penale  e  la  disciplina  delle  "nuove  contestazioni"  in  udienza    &#13;
 contenuta nella norma impugnata;                                         &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 423, secondo comma, del codice di  procedura    &#13;
 penale, in relazione agli artt. 112 e 3 della Costituzione, sollevata    &#13;
 dal  giudice per le indagini preliminari presso il Tribunale militare    &#13;
 di La Spezia con l'ordinanza di cui in epigrafe.                         &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 aprile 1991.                                 &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: CHELI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 aprile 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
