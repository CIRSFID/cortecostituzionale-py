<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>29</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:29</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/01/2000</data_decisione>
    <data_deposito>04/02/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI, dott. &#13;
 Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale  dell'art.  1-sexies  del    &#13;
 decreto-legge  27  giugno  1985,  n. 312 (Disposizioni urgenti per la    &#13;
 tutela delle zone di particolare interesse ambientale), convertito in    &#13;
 legge 8 agosto 1985, n. 431,  promosso  con  ordinanza  emessa  il  2    &#13;
 dicembre  1998 dal pretore di Latina nel procedimento penale a carico    &#13;
 di Caruso Daniela, iscritta al n. 261 del registro ordinanze  1999  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 20, prima    &#13;
 serie speciale, dell'anno 1999;                                          &#13;
   Udito nella camera di consiglio del 10  novembre  1999  il  giudice    &#13;
 relatore Riccardo Chieppa;                                               &#13;
   Ritenuto  che  il  pretore  di  Latina,  con  ordinanza emessa il 2    &#13;
 dicembre 1998 (r.o. n. 261 del 1999), nel corso  di  un  procedimento    &#13;
 penale  nel  quale  era  chiamato  ad  applicare, tra l'altro, l'art.    &#13;
 1-sexies del decreto-legge  27  giugno  1985,  n.  312  (Disposizioni    &#13;
 urgenti   per   la   tutela   delle  zone  di  particolare  interesse    &#13;
 ambientale), introdotto dalla legge di conversione 8 agosto 1985,  n.    &#13;
 431,  ha  sollevato  questione  di  legittimità costituzionale della    &#13;
 predetta norma;                                                          &#13;
     che, ad avviso del giudice a quo, essa si porrebbe  anzitutto  in    &#13;
 contrasto  con  gli artt. 42 e 97 della Costituzione, rimandando alla    &#13;
 nozione di aree protette quale desumibile dalla espressa  elencazione    &#13;
 normativa  di  cui  all'art.  1 dello stesso decreto-legge n. 312 del    &#13;
 1985, che individua i beni oggetto di tutela per categoria;              &#13;
     che siffatta elencazione sarebbe illegittima, non consentendo che    &#13;
 la individuazione dei beni  con  naturale  attitudine  al  vincolo  e    &#13;
 conseguenti  limitazioni  al  diritto  di godimento e di disposizione    &#13;
 avvenga nelle forme del giusto procedimento, sia al fine  di  rendere    &#13;
 conoscibili  le ragioni che connotano il particolare pregio del bene,    &#13;
 sia per consentire ai privati di introdurre le  proprie  osservazioni    &#13;
 ed istanze;                                                              &#13;
     che, inoltre, la norma in questione recherebbe vulnus all'art.  9    &#13;
 della      Costituzione,   giacché   il  valore  estetico  culturale    &#13;
 risulterebbe individuato e riconosciuto non in quanto  effettivamente    &#13;
 sussistente  in  relazione  alle  caratteristiche  proprie  del  bene    &#13;
 stesso,   ma   attraverso   la   utilizzazione   di   caratteri   e/o    &#13;
 qualificazioni meramente giuridiche;                                     &#13;
     che, nella prospettazione del giudice rimettente, l'art. 1-sexies    &#13;
 violerebbe,  altresì,  l'art. 25, secondo comma, della Costituzione,    &#13;
 per il contrasto con il principio  della  legalità,  avuto  riguardo    &#13;
 alla  indeterminatezza  della  pena  da  applicare,  oltre  che della    &#13;
 condotta  incriminata,  individuata  con  generico  riferimento  alla    &#13;
 violazione  delle  disposizioni dello stesso decreto-legge n. 312 del    &#13;
 1985;                                                                    &#13;
   Considerato  che  la  questione  di   legittimità   costituzionale    &#13;
 dell'art.    1-sexies del decreto-legge n. 312 del 1985 è già stata    &#13;
 risolta in riferimento ai medesimi parametri invocati nel senso della    &#13;
 manifesta infondatezza con ordinanza n. 68 del 1998  (v.  anche,  per    &#13;
 taluni profili, ordinanze nn. 158 e 316 del 1998);                       &#13;
     che, pertanto, essa va dichiarata manifestamente infondata.          &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 1-sexies del decreto-legge 27  giugno  1985,    &#13;
 n.  312 (Disposizioni urgenti per la tutela delle zone di particolare    &#13;
 interesse  ambientale),  introdotto  dall'art.  1  della   legge   di    &#13;
 conversione  8  agosto  1985,  n. 431, sollevata, in riferimento agli    &#13;
 artt. 9, 25, secondo comma, 42 e 97 della Costituzione,  dal  pretore    &#13;
 di Latina con l'ordinanza indicata in epigrafe.                          &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 gennaio 2000.                              &#13;
                        Il Presidente: Vassalli                           &#13;
                          Il relatore: Chieppa                            &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 4 febbraio 2000.                          &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
