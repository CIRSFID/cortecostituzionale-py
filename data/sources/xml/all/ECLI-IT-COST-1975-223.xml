<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>223</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:223</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giulio Gionfrida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1975</data_decisione>
    <data_deposito>17/07/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA- Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 10  del  r.d.  &#13;
 16  marzo  1942,  n.  267  (legge fallimentare), promosso con ordinanza  &#13;
 emessa il 20 marzo 1973 dal  tribunale  di  Civitavecchia  sul  ricorso  &#13;
 della ditta fratelli Federici ed altri per ottenere la dichiarazione di  &#13;
 fallimento  di  Muliello  Giuseppe,  iscritta  al  n.  201 del registro  &#13;
 ordinanze 1973 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 191 del 25 luglio 1973.                                               &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 18 giugno 1975 il Giudice  relatore  &#13;
 Giulio Gionfrida;                                                        &#13;
     udito il sostituto avvocato generale dello Stato Renato Carafa, per  &#13;
 il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  A  seguito  di ricorsi proposti da varie società e volti ad  &#13;
 ottenere il fallimento  di  Muliello  Giuseppe,  l'adito  tribunale  di  &#13;
 Civitavecchia  -  ritenuto  che  era  palese lo stato di insolvenza del  &#13;
 Muliello e che, d'altra parte, non era decorso l'anno dalla  cessazione  &#13;
 dell'impresa  commerciale del medesimo, onde sussistevano i presupposti  &#13;
 per far luogo alla chiesta  declaratoria  di  fallimento,  in  base  al  &#13;
 disposto  dell'art.  10  r.d.  1942,  n.    267  (secondo cui, appunto,  &#13;
 "l'imprenditore... può essere dichiarato fallito entro un  anno  dalla  &#13;
 cessazione    dell'impresa,   se   l'insolvenza   si   è   manifestata  &#13;
 anteriormente alla medesima o entro l'anno successivo"), con  ordinanza  &#13;
 20 marzo 1973, sospesa la decisione nel merito, ha sollevato, in quanto  &#13;
 a  suo  avviso  rilevante  e non manifestamente infondata, questione di  &#13;
 legittimità dell'art. 10 sopradetto, per contrasto con l'art. 3  della  &#13;
 Costituzione.                                                            &#13;
     2.  -  Ritualmente notificata, comunicata e pubblicata la ordinanza  &#13;
 de qua ed instaurato il giudizio  innanzi  alla  Corte,  è  in  questo  &#13;
 intervenuto  il  Presidente  del Consiglio dei ministri, per il tramite  &#13;
 dell'Avvocatura  generale  dello  Stato,  che  ha   concluso   per   la  &#13;
 declaratoria di infondatezza della sollevata questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Dispone  l'art.  10  del  r.d.  16 marzo 1942, n. 267 (legge  &#13;
 fallimentare) che "l'imprenditore, che per qualunque causa  ha  cessato  &#13;
 dall'esercizio  dell'impresa,  può  essere dichiarato fallito entro un  &#13;
 anno dalla cessazione dell'impresa se l'insolvenza  si  è  manifestata  &#13;
 anteriormente alla medesima o entro l'anno successivo".                  &#13;
     Il  giudice  a quo dubita - come detto - della legittimità di tale  &#13;
 norma e  ne  ipotizza  il  contrasto  con  il  precetto  costituzionale  &#13;
 dell'eguaglianza  di  cui  all'art.  3  della  Costituzione: in quanto,  &#13;
 invero, il legislatore - senza tener conto  della  diversa  "condizione  &#13;
 personale"   del  soggetto  non  più  imprenditore  -  parificherebbe,  &#13;
 arbitrariamente ed irrazionalmente la situazione di questo  con  quella  &#13;
 dell'esercente  l'impresa  commerciale, supponendo, in base ad una mera  &#13;
 finzione  contraria  alla  realtà,  la  persistenza   della   qualità  &#13;
 imprenditoriale entro l'anno dalla cessazione dell'impresa.              &#13;
     2. - La questione non è fondata.                                    &#13;
     La  norma  dell'art. 10 legge fallimentare (avente il suo immediato  &#13;
 precedente nella disposizione - formulata con criteri anzi  di  maggior  &#13;
 rigore  -  dell'art.  690  cod.  comm.  e,  comunque,  in linea con una  &#13;
 costante tradizione legislativa italiana, oltreché in armonia  con  le  &#13;
 legislazioni   di   molti   altri   Paesi)   trova   la  sua  razionale  &#13;
 giustificazione sotto diversi profili.                                   &#13;
     Da  un  lato  va  considerato  che  i  rapporti  posti  in   essere  &#13;
 dall'imprenditore  proiettano  normalmente i loro effetti anche dopo la  &#13;
 cessazione dell'attività commerciale,  per  un  periodo  più  o  meno  &#13;
 lungo;  cosicché  la  insolvenza  manifestatasi  entro l'anno (periodo  &#13;
 nella  specie   discrezionalmente   fissato   dal   legislatore)   può  &#13;
 ragionevolmente ritenersi ricollegabile alla gestione imprenditoriale.   &#13;
     D'altro  lato  -  e ciò vale anche per il caso che l'insolvenza si  &#13;
 sia  manifestata  prima  della  cessazione  dell'impresa  -,  se   tale  &#13;
 cessazione fosse idonea ad evitare automaticamente il fallimento, tutti  &#13;
 i debitori in difficoltà economiche potrebbero artatamente ricorrervi,  &#13;
 con  grave  danno,  oltre  che  della massa dei creditori, degli stessi  &#13;
 interessi pubblicistici che l'istituto  del  fallimento  è  diretto  a  &#13;
 tutelare.                                                                &#13;
     È    pertanto    pienamente    razionale    che    nei   confronti  &#13;
 dell'imprenditore cessato sia applicabile, per il predetto  periodo  di  &#13;
 tempo, la disciplina relativa alla dichiarazione di fallimento, sia per  &#13;
 quanto  attiene  agli  effetti patrimoniali sia per quelli di carattere  &#13;
 personale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art. 10 r.d. 16 marzo 1942, n. 267 (legge fallimentare) sollevata,  &#13;
 in  riferimento  all'art.  3  della  Costituzione,  con  l'ordinanza in  &#13;
 epigrafe indicata.                                                       &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, l'8 luglio 1975.                                 &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO  AMADEI - GIULIO GIONFRIDA -  &#13;
                                   EDOARDO VOLTERRA  -  GUIDO  ASTUTI  -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
