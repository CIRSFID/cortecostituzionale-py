<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2012</anno_pronuncia>
    <numero_pronuncia>196</numero_pronuncia>
    <ecli>ECLI:IT:COST:2012:196</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>QUARANTA</presidente>
    <relatore_pronuncia>Mario Rosario Morelli</relatore_pronuncia>
    <redattore_pronuncia>Mario Rosario Morelli</redattore_pronuncia>
    <data_decisione>20/06/2012</data_decisione>
    <data_deposito>19/07/2012</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 4 della legge 22 maggio 1978, n. 194 (Norme per la tutela sociale della maternità e sull'interruzione volontaria della gravidanza), promosso dal Giudice tutelare del Tribunale ordinario di Spoleto nel procedimento relativo a F.N., con ordinanza del 3 gennaio 2012, iscritta al n. 60 del registro ordinanze 2012 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 16, prima serie speciale, dell'anno 2012.&#13;
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 20 giugno 2012 il Giudice relatore Mario Rosario Morelli.</epigrafe>
    <testo>Ritenuto che - nel corso di un procedimento ex articolo 12 della legge 22 maggio 1978, n. 194 (Norme per la tutela sociale della maternità e sull'interruzione volontaria della gravidanza), attivato a seguito della richiesta, del locale Consultorio familiare, di autorizzare una minorenne a decidere l'interruzione volontaria della gravidanza senza informarne i genitori - l'adito Giudice tutelare del Tribunale ordinario di Spoleto ha sollevato, con ordinanza emessa il 3 gennaio 2012, questione di legittimità costituzionale dell'articolo 4 della predetta legge n. 194, nella parte in cui prevede la facoltà della donna, in presenza delle condizioni ivi stabilite, di procedere volontariamente alla interruzione volontaria della gravidanza entro i primi novanta giorni dal concepimento, per contrasto con gli articoli 2, 32, primo comma, 11 e 117, primo comma della Costituzione;&#13;
 che, presupposta la rilevanza di tal questione, il rimettente ne ha motivato la non manifesta infondatezza, con richiamo, in premessa, alla sentenza della Corte di giustizia dell'Unione europea del 18 ottobre 2011 - resa nel procedimento C-34/10, Brüstle contro Greenpeace e V., e recante l'interpretazione dell'articolo 6, numero 2, lettera c), della direttiva del Parlamento europeo e del Consiglio 98/44/CE, sulla protezione giuridica delle invenzioni biotecnologiche - la quale avrebbe, a suo avviso, attribuito, in modo inequivoco, assoluto rilievo giuridico all'embrione umano, non solo definendolo tale sin dalla fecondazione, ma considerandolo anche un "soggetto" di primario valore assoluto;&#13;
 che - in applicazione, sempre secondo il giudice a quo, di tale principio, cui egli attribuisce efficacia diretta e vincolante per tutti gli Stati membri - risulterebbero, conseguentemente, lesi, dalla norma denunciata, i precetti costituzionali: &#13;
 - dell'articolo 2, che garantisce il diritto alla vita anche nello stadio della sua formazione mediante il progressivo sviluppo delle cellule germinali;&#13;
 - dell'articolo 32, primo comma, poiché la volontaria distruzione dell'embrione umano recherebbe vulnus al diritto alla salute, parimenti riconosciuto a chiunque possieda una individualità giuridicamente rilevante;&#13;
 - degli articoli 11 e 117, primo comma, per elusione dei vincoli derivanti, al legislatore nazionale, dal principio dell'ordinamento comunitario in tema, appunto di tutela dell'embrione umano, come essere provvisto di autonoma soggettività giuridica;&#13;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, con il patrocinio dell'Avvocatura generale dello Stato, che ha concluso per la manifesta inammissibilità della questione, per irrilevanza; e, nel merito, per la sua non fondatezza per assoluta inconferenza del dictum della Corte di giustizia europea, non altro recante che una «definizione dell'embrione umano ai soli specifici, e limitati fini, della individuazione di cosa costituisce invenzione biotecnologica brevettabile ai sensi della citata direttiva 98/44/CE»;&#13;
 che, inoltre, secondo l'Avvocatura, «proprio dalla diversa natura dei valori che vengono coinvolti dal presente giudizio, che (...) altro non concerne se non il bilanciamento tra la libera autodeterminazione e la tutela della salute della donna e la tutela dell'aspettativa di vita del nascituro, discende l'impossibilità per il c.d. principio della primazia del diritto europeo di operare. Ciò in virtù della "teoria dei contro limiti", in base al[la] quale la "ritrazione" del diritto interno nei confronti del diritto comunitario non opera "in riferimento ai principi fondamentali del nostro ordinamento costituzionale e ai diritti inalienabili della persona umana"».&#13;
 Considerato che la riferita questione di costituzionalità è stata sollevata nel corso, come detto, di un procedimento di volontaria giurisdizione ex articolo 12 della citata legge n. 194 del 1978;&#13;
 che - a riguardo del provvedimento di "autorizzazione a decidere" che, in tal procedura, è chiesto al giudice tutelare di pronunciare - già nella sentenza n. 196 del 1987 questa Corte ha precisato che esso ha contenuto «unicamente di integrazione, della volontà della minorenne, per i vincoli gravanti sulla sua capacità d'agire», rimanendo quindi «esterno alla procedura di riscontro, nel concreto, dei parametri previsti dal legislatore per potersi procedere all'interruzione gravidica»; sicché, «una volta che i disposti accertamenti siansi identificati quale antefatto specifico e presupposto di carattere tecnico, al magistrato non sarebbe possibile discostarsene; intervenendo egli, come si è chiarito, nella sola generica sfera della capacità (o incapacità) del soggetto, tal quale viene a verificarsi per altre consimili fattispecie (per gli interdicendi, ad esempio, ai sensi dell'articolo 414 cod. civ.)»;&#13;
 che le medesime considerazioni si trovano ribadite nelle ordinanze n. 463 del 1988 e n. 293 del 1993;&#13;
 che, anche di recente, è stato ancora una volta riaffermato, nella ordinanza n. 126 del 2012, come, conformemente alla sopra identificata funzione del procedimento dinanzi al giudice tutelare, sia «attribuito a tale giudice - in tutti i casi in cui l'assenso dei genitori o degli esercenti la tutela non sia o non possa essere espresso - il compito di "autorizzazione a decidere", un compito che (alla stregua della stessa espressione usata per indicarlo dall'art. 12, secondo comma, della legge n. 194 del 1978) non può configurarsi come potestà co-decisionale, la decisione essendo rimessa - alle condizioni ivi previste - soltanto alla responsabilità della donna» (ordinanza n. 76 del 1996); e che «il provvedimento del giudice tutelare risponde ad una funzione di verifica in ordine alla esistenza delle condizioni nelle quali la decisione della minore possa essere presa in piena libertà morale» (ordinanza n. 514 del 2002);&#13;
 che, pertanto, non essendo il rimettente chiamato a decidere, o a codecidere, sull'an della interruzione della gravidanza, la denunciata norma dell'art. 4 della legge n. 194 del 1978, che tale interruzione consente, non viene in applicazione del giudizio a quo;&#13;
 che la correlativa questione di legittimità costituzionale è manifestamente, quindi, inammissibile per irrilevanza, assorbita rimanendo ogni altra eccezione dell'Avvocatura.&#13;
 	Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'articolo 4 della legge 22 maggio 1978, n. 194 (Norme per la tutela sociale della maternità e sull'interruzione volontaria della gravidanza), sollevata, in riferimento agli articoli 2, 32, primo comma, 11 e 117 della Costituzione, dal Giudice tutelare del Tribunale ordinario di Spoleto, con l'ordinanza in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 giugno 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 19 luglio 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
