<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>483</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:483</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/04/1988</data_decisione>
    <data_deposito>27/04/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 5 della legge    &#13;
 22 dicembre 1973, n. 903 (Istituzione del  Fondo  di  previdenza  del    &#13;
 clero  e  dei  ministri  di culto delle confessioni religiose diverse    &#13;
 dalla  cattolica  e  nuova  disciplina   dei   relativi   trattamenti    &#13;
 pensionistici),  promosso  con ordinanza emessa il 25 agosto 1982 dal    &#13;
 Pretore di Massa, iscritta al n. 860 del registro  ordinanze  1982  e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 135 dell'anno    &#13;
 1983;                                                                    &#13;
    Visti  gli  atti  di  costituzione di Palmieri Ido e dell'I.N.P.S.    &#13;
 nonché  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 13 gennaio 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  nel  procedimento promosso dal Rev.do Palmieri Ido,    &#13;
 francescano (clero regolare),  per  ottenere  il  riconoscimento  del    &#13;
 proprio  diritto, all'iscrizione al Fondo di previdenza per il clero,    &#13;
 negatagli dall'I.N.P.S., sebbene egli fosse investito delle  funzioni    &#13;
 di  parroco,  per  essere  l'iscrizione  medesima  riservata  ai soli    &#13;
 ministri del culto cattolico del clero secolare, l'adito  Pretore  di    &#13;
 Massa,  con  ordinanza  in  data  25  agosto  1982,  ha sollevato, in    &#13;
 riferimento   all'art.   3   Cost.,   questione    di    legittimità    &#13;
 costituzionale  dell'art.  5  della  legge  22 dicembre 1973, n. 903,    &#13;
 nella parte in cui esclude il diritto del religioso non  appartenente    &#13;
 al  clero  secolare,  investito  delle suddette funzioni, di ottenere    &#13;
 l'iscrizione al menzionato Fondo di previdenza;                          &#13;
      che,  ad  avviso  del  giudice  a quo, tale esclusione determina    &#13;
 un'arbitraria discriminazione in danno del religioso appartenente  al    &#13;
 clero  regolare, il quale si vede privato della tutela previdenziale,    &#13;
 essendogli inibita sia l'iscrizione al Fondo in questione, sia quella    &#13;
 all'assicurazione  generale  obbligatoria,  in  quanto  prestatore di    &#13;
 lavoro in favore di terzi "concordatari", quali la parrocchia;           &#13;
      che  si  sono  costituiti  l'I.N.P.S.  e  la parte privata ed è    &#13;
 intervenuto il Presidente del Consiglio dei ministri;                    &#13;
      che  la difesa dell'Istituto e quella dell'Autorità intervenuta    &#13;
 hanno concluso nel senso dell'infondatezza della questione, mentre di    &#13;
 opposto tenore sono state le conclusioni della parte privata;            &#13;
      che la questione è manifestamente infondata;                       &#13;
      che,  invero,  la  condizione del sacerdote del clero secolare -    &#13;
 avente diritto, in virtù di tale suo status, all'iscrizione al Fondo    &#13;
 di  cui alla legge n. 903 del 1973 - e quella del sacerdote del clero    &#13;
 regolare non sono assimilabili;                                          &#13;
      che   relativamente  a  quest'ultimo,  i  voti  di  carità,  di    &#13;
 obbedienza e di  povertà,  la  sua  appartenenza  ad  una  comunità    &#13;
 costituente  garanzia  di assistenza e di mantenimento, integrano uno    &#13;
 status complessivamente non comparabile con  quello  tenuto  presente    &#13;
 dal  legislatore  come  idoneo  a  legittimare ex se l'iscrizione del    &#13;
 sacerdote secolare al Fondo suddetto;                                    &#13;
      che, inoltre, è inesatto che il sacerdote appartenente al clero    &#13;
 regolare rimanga sprovvisto di tutela assicurativa, ove presti lavoro    &#13;
 a  favore  di  enti  "concordatari"  (come  la parrocchia), posto che    &#13;
 questa  Corte,  con  sentenza  n.  108  del   1977,   ha   dichiarato    &#13;
 l'illegittimità  costituzionale  dell'articolo  unico,  primo comma,    &#13;
 della legge 3 maggio 1956, n. 392, nella parte in cui escludeva dalla    &#13;
 soggezione  alle assicurazioni sociali obbligatorie il clero regolare    &#13;
 officiato di attività lavorativa retribuita alle dipendenze di  enti    &#13;
 concordatari  (e  ciò con specifico riferimento ad un caso analogo a    &#13;
 quello del giudizio a quo, perché concernente la tutela assicurativa    &#13;
 di un religioso investito delle funzioni di parroco);                    &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 5 della legge  22  dicembre  1973,  n.  903,    &#13;
 sollevata,  in riferimento all'art. 3 Cost., dal Pretore di Massa con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 20 aprile 1988.         &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 aprile 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
