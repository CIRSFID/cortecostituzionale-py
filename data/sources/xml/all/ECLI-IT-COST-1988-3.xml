<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>3</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:3</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/01/1988</data_decisione>
    <data_deposito>19/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art. 52, secondo    &#13;
 comma, del d.P.R. 29 settembre 1973, n. 597 "Istituzione e disciplina    &#13;
 dell'imposta sul reddito delle persone fisiche" in relazione all'art.    &#13;
 18 del d.P.R.  29  settembre  1973,  n.600  "Disposizioni  comuni  in    &#13;
 materia  di  accertamento delle imposte sui redditi", promossi con le    &#13;
 ordinanze emesse il 1° marzo 1984 dalla Commissione tributaria di  2°    &#13;
 grado  di Pescara nei ricorsi proposti da Danelli Francesco e Danelli    &#13;
 Virginia contro l'Ufficio delle Imposte Dirette di Pescara,  iscritte    &#13;
 ai  nn.  987  e  988  del  registro ordinanze 1984 e pubblicate nella    &#13;
 Gazzetta Ufficiale della Repubblica nn. 32 e 25- bis dell'anno 1985;     &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 10 dicembre 1987 il Giudice    &#13;
 relatore Francesco Saja.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel   corso   di   due  procedimenti  di  appello  concernenti  la    &#13;
 deducibilità - nella dichiarazione ai fini IRPEF - da parte di  soci    &#13;
 della  s.a.s. "ASTRA di Francesco Danelli e C.", di interessi passivi    &#13;
 afferenti beni immobili della società non strumentali  all'esercizio    &#13;
 dell'impresa,  la  Commissione  tributaria  di  II  grado  di Pescara    &#13;
 sollevava, con due ordinanze di identico contenuto emesse il 1° marzo    &#13;
 1984,  questione  di  legittimità  costituzionale  dell'art.  52, II    &#13;
 comma, del d.P.R. 29 settembre  1973,  n.  597  (in  relazione  anche    &#13;
 all'art.  18  del  d.P.R.  29 settembre 1973, n. 600), in riferimento    &#13;
 all'art. 3 Cost.                                                         &#13;
    Afferma il giudice rimettente che la s.a.s. ASTRA non aveva potuto    &#13;
 portare in detrazione gli interessi passivi in sede  di  compilazione    &#13;
 del  mod.  750/E  perché,  quale  impresa minore percettrice di soli    &#13;
 proventi  immobiliari,  aveva  dovuto  necessariamente   assumere   a    &#13;
 tassazione  tali  redditi  in base alle risultanze catastali e non in    &#13;
 base al reddito effettivo netto di gestione, e  ciò  per  la  stessa    &#13;
 struttura del quadro 750/E.                                              &#13;
    Ciò  premesso,  la  normativa censurata determinerebbe, ad avviso    &#13;
 del giudice a quo, un diverso trattamento tributario  tra  percettori    &#13;
 dello  stesso  reddito,  in quanto le imprese "maggiori" sarebbero, a    &#13;
 differenza  delle  "minori",  abilitate   a   dedurre   dai   redditi    &#13;
 immobiliari  gli  interessi  passivi,  e  ciò  anche  per la diversa    &#13;
 struttura del modello 750/A, che esse sono tenute a compilare.           &#13;
    Il  Presidente del Consiglio dei ministri, intervenuto in entrambi    &#13;
 i giudizi, conclude per l'infondatezza della questione, deducendo, in    &#13;
 primo  luogo, che l'art. 52 censurato è applicabile sia alle imprese    &#13;
 con contabilità semplificata, sia  a  quelle  soggette  alla  tenuta    &#13;
 della  contabilità  ordinaria,  e, in secondo luogo, che ai soggetti    &#13;
 ammessi  a  fruire  del  regime  di  contabilità   semplificata   è    &#13;
 riconosciuta  la facoltà di optare per il regime ordinario, restando    &#13;
 così effetto di  libera  scelta  di  convenienza  ogni  varietà  di    &#13;
 trattamento tributario che ne possa derivare.<diritto>Considerato in diritto</diritto>1.  -  I giudizi, per l'identità della questione sollevata, vanno    &#13;
 riuniti e decisi congiuntamente.                                         &#13;
    2.  -  La  norma  censurata (art. 52, secondo comma, del d.P.R. 29    &#13;
 settembre  1973,  n.  597,  concernente  "Istituzione  e   disciplina    &#13;
 dell'imposta  sul  reddito  delle persone fisiche") dispone che nella    &#13;
 determinazione degli utili netti - i quali, ai sensi del primo  comma    &#13;
 dello  stesso  art. 52, costituiscono il reddito d'impresa - " non si    &#13;
 tiene conto delle  perdite  relative  ai  cespiti  che  fruiscono  di    &#13;
 esenzione né dei proventi soggetti a ritenuta alla fonte a titolo di    &#13;
 imposta né dei proventi e dei costi relativi agli immobili  indicati    &#13;
 dell'art.  21  che non costituiscono beni strumentali per l'esercizio    &#13;
 dell'impresa. I redditi di tali  immobili  concorrono  a  formare  il    &#13;
 reddito di impresa nell'ammontare determinato secondo le disposizioni    &#13;
 del titolo II".                                                          &#13;
    La disposizione violerebbe, ad avviso della Commissione tributaria    &#13;
 rimettente, il principio  di  eguaglianza,  in  quanto  il  combinato    &#13;
 disposto  di essa e dell'art. 18 del d.P.R. 29 settembre 1973, n. 600    &#13;
 (che  individua  le  imprese   minori   ammesse   alla   contabilità    &#13;
 semplificata)   determinerebbe  una  disparità  di  trattamento  tra    &#13;
 imprese minori e imprese maggiori,  poiché  soltanto  queste  ultime    &#13;
 sarebbero  abilitate a computare i redditi immobiliari al netto degli    &#13;
 interessi passivi.                                                       &#13;
    3. - La questione non è fondata.                                     &#13;
    Va  innanzitutto  rilevato  che  la  norma impugnata, come dedotto    &#13;
 anche dall'Avvocatura dello Stato, detta  una  disciplina  comune  ad    &#13;
 ogni  tipo  di  impresa,  nel  senso  che  essa si applica ai redditi    &#13;
 d'impresa sia che  vengano  determinati  in  base  alla  contabilità    &#13;
 ordinaria,   sia   che   rientrino   nel  regime  della  contabilità    &#13;
 semplificata.                                                            &#13;
    Oltre a ciò peraltro, e anche prescindendo dall'ulteriore rilievo    &#13;
 che ai sensi del sesto comma dell'art. 18 del  d.P.R.  n.  600/73  il    &#13;
 contribuente  ammesso  alla  contabilità semplificata ha facoltà di    &#13;
 optare  per  il  regime  ordinario,   assume   valore   decisivo   la    &#13;
 considerazione  che  l'art.  72  del d.P.R. n. 597/73 dispone che nei    &#13;
 confronti delle imprese ammesse alla contabilità semplificata e  che    &#13;
 non  hanno  optato  per  il  regime  normale  il reddito d'impresa è    &#13;
 costituito dalla differenza tra l'ammontare complessivo  dei  ricavi,    &#13;
 delle  plusvalenze  patrimoniali  e  delle  sopravvenienze  attive  e    &#13;
 l'ammontare complessivo di una serie di costi, tra i quali  la  norma    &#13;
 prevede, al punto 7, gli interessi passivi.                              &#13;
   Pertanto,  la questione sollevata è infondata, in quanto, ai sensi    &#13;
 della normativa vigente, contrariamente  all'assunto  del  giudice  a    &#13;
 quo,  gli  interessi passivi sono previsti fra le componenti negative    &#13;
 di reddito detraibili per le imprese minori  anche  se  ammesse  alla    &#13;
 contabilità semplificata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata, nei sensi di cui in motivazione, la questione    &#13;
 di legittimità  costituzionale  dell'art.  52,  secondo  comma,  del    &#13;
 d.P.R.  29 settembre 1973, n. 597, in relazione anche all'art. 18 del    &#13;
 d.P.R. 29 settembre 1973, n. 600, sollevata, in riferimento  all'art.    &#13;
 3  Cost.,  dalla Commissione tributaria di II grado di Pescara con le    &#13;
 ordinanze indicate in epigrafe.                                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 gennaio 1988.                              &#13;
                    Il Presidente e redattore: SAJA                       &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
