<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>350</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:350</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1992</data_decisione>
    <data_deposito>20/07/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità  costituzionale  dell'art.  4-bis,  primo    &#13;
 comma,  prima  parte,  della  legge  26  luglio  1975,  n. 354 (Norme    &#13;
 sull'ordinamento  penitenziario  e  sulla  esecuzione  delle   misure    &#13;
 privative e limitative della libertà), introdotto dall'art. 1, primo    &#13;
 comma,  del  decreto-legge  13  maggio  1991,  n.  152 (Provvedimenti    &#13;
 urgenti  in  tema  di  lotta  alla  criminalità  organizzata  e   di    &#13;
 trasparenza   e   buon   andamento   dell'attività  amministrativa),    &#13;
 convertito dalla legge 12 luglio 1991, n. 203, promossi  con  n.  due    &#13;
 ordinanze emesse il 27 febbraio 1992 dal Tribunale di sorveglianza di    &#13;
 Ancona  nei  procedimenti  di  sorveglianza  relativi  ad  istanze di    &#13;
 liberazione anticipata nei confronti di Medaglia Francesco  e  Pagano    &#13;
 Guido,  ordinanze  iscritte  ai  nn. 201 e 223 del registro ordinanze    &#13;
 1992 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 18  e    &#13;
 19, prima serie speciale, dell'anno 1992;                                &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  17  giugno  1992  il  Giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
    Ritenuto  che  il  Tribunale di sorveglianza di Ancona ha, con due    &#13;
 ordinanze di contenuto sostanzialmente identico, entrambe  emesse  il    &#13;
 27  febbraio  1992  nel  corso  di  procedimenti  aventi  ad  oggetto    &#13;
 richieste di riduzione di pena per liberazione anticipata -  avanzate    &#13;
 la prima da Medaglia Francesco, condannato per i reati di concorso in    &#13;
 sequestro  di  persona  a scopo di estorsione, in rapina aggravata ed    &#13;
 altro, la seconda  da  Pagano  Guido,  condannato  per  il  reato  di    &#13;
 partecipazione  ad  associazione  per  delinquere  di  tipo mafioso -    &#13;
 sollevato, in riferimento agli artt.  3  e  27,  terzo  comma,  della    &#13;
 Costituzione, questione di legittimità dell'art. 4-bis, primo comma,    &#13;
 prima parte, della legge 26 luglio 1975, n. 354, introdotto dall'art.    &#13;
 1,  primo comma, del decreto-legge 13 maggio 1991, n. 152, convertito    &#13;
 dalla legge 12 luglio 1991, n. 203, per la parte in cui  prevede,  in    &#13;
 relazione alle istanze intese all'ottenimento della riduzione di pena    &#13;
 ai  fini della liberazione anticipata presentate dai condannati per i    &#13;
 delitti  commessi  per  finalità  di  terrorismo  o   di   eversione    &#13;
 dell'ordinamento  costituzionale,  per  delitti  commessi avvalendosi    &#13;
 delle condizioni previste dall'articolo 416-bis   del  codice  penale    &#13;
 ovvero  al  fine di agevolare l'attività delle associazioni previste    &#13;
 dallo stesso articolo, nonché per i delitti di cui agli artt. 416-bis    &#13;
 e   630   del   codice   penale  e all'articolo 74 del testo unico in    &#13;
 materia di  disciplina  delle  sostanze  stupefacenti  e  psicotrope,    &#13;
 repressione,   cura   e   riabilitazione   dei   relativi   stati  di    &#13;
 tossicodipendenza, approvato con il d.P.R. 9 ottobre  1990,  n.  309,    &#13;
 che  tali  istanze  possano  trovare accoglimento "solo se sono stati    &#13;
 acquisiti elementi tali da escludere l'attualità di collegamenti con    &#13;
 la criminalità organizzata o eversiva", previa  richiesta  da  parte    &#13;
 della magistratura di sorveglianza ai competenti comitati provinciali    &#13;
 per l'ordine e la sicurezza pubblica;                                    &#13;
      che,  in  punto  di rilevanza, il giudice a quo osserva che, nei    &#13;
 casi di specie, gli elementi ostativi alla riduzione della  pena  per    &#13;
 liberazione  anticipata  derivano tanto per il Medaglia quanto per il    &#13;
 Pagano dalle informazioni  acquisite  per  il  tramite  del  comitato    &#13;
 provinciale  per l'ordine e la sicurezza pubblica di Ascoli Piceno le    &#13;
 quali asseriscono l'insussistenza di  elementi  idonei  a  comprovare    &#13;
 l'attualità  di  collegamenti degli interessati "con la criminalità    &#13;
 organizzata (non la  presenza,  si  badi,  di  positivi  elementi  di    &#13;
 riscontro  atti a comprovare l'assenza di collegamenti attuali ovvero    &#13;
 l'intervenuta  recisione  di  collegamenti   passati)",   aggiungendo    &#13;
 ulteriori  emergenze  da  ritenere "apodittiche in quanto sfornite di    &#13;
 elementi  di  riscontro"  e  che,  considerata  la   presunzione   di    &#13;
 attualità  di collegamenti con la criminalità organizzata, appaiono    &#13;
 sufficienti "al fine di consustanziare  una  pronuncia  di  reiezione    &#13;
 dell'istanza",  senza  rendere  necessari ulteriori più approfonditi    &#13;
 accertamenti,  "siccome  sarebbe  viceversa  opportuno   laddove   la    &#13;
 disciplina legislativa fosse analoga a quella prevista per i soggetti    &#13;
 individuati  nella  seconda  parte  del primo comma dell'art. 4- bis"    &#13;
 dell'ordinamento penitenziario, nei  confronti  dei  quali  le  dette    &#13;
 informazioni  sono  ostative della concessione del beneficio "solo se    &#13;
 vi sono elementi tali da far ritenere la sussistenza di  collegamenti    &#13;
 con la criminalità organizzata o eversiva";                             &#13;
      che  in  entrambi  i  giudizi  è  intervenuto il Presidente del    &#13;
 Consiglio  dei  ministri,  rappresentato  e  difeso   dall'Avvocatura    &#13;
 Generale dello Stato, chiedendo che la questione venga dichiarata, in    &#13;
 via principale, inammissibile e, in subordine, non fondata;              &#13;
    Considerato  che,  stante  l'identità delle questioni proposte, i    &#13;
 giudizi devono essere riuniti;                                           &#13;
      che la questione di legittimità dell'art. 4- bis, primo  comma,    &#13;
 prima parte, della legge 26 luglio 1975, n. 354, introdotto dall'art.    &#13;
 1,  primo comma, del decreto-legge 13 maggio 1991, n. 152, convertito    &#13;
 dalla legge 12 luglio 1991, n. 203 - una  norma  peraltro  modificata    &#13;
 dall'art.  15  del  decreto-legge  8  giugno 1992, n. 306, non ancora    &#13;
 convertito  in  legge  -  è  stata  già  dichiarata  manifestamente    &#13;
 infondata da questa Corte con ordinanza n. 271 del 1992;                 &#13;
      che  nelle  ordinanze  di  rimessione non sono addotti argomenti    &#13;
 nuovi o diversi da quelli allora esaminati;                              &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 4- bis,  primo  comma,  prima  parte,  della    &#13;
 legge  26 luglio 1975, n. 354 (Norme sull'ordinamento penitenziario e    &#13;
 sulla esecuzione delle misure privative e limitative della libertà),    &#13;
 introdotto dall'art. 1, primo  comma,  del  decreto-legge  13  maggio    &#13;
 1991,   n.   152   (Provvedimenti  urgenti  in  tema  di  lotta  alla    &#13;
 criminalità  organizzata  e  di   trasparenza   e   buon   andamento    &#13;
 dell'attività  amministrativa),  convertito  dalla  legge  12 luglio    &#13;
 1991, n. 203, questione sollevata, in riferimento agli artt. 3 e  27,    &#13;
 terzo  comma,  della  Costituzione,  dal Tribunale di sorveglianza di    &#13;
 Ancona con le ordinanze in epigrafe.                                     &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 7 luglio 1992.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: VASSALLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
 Depositata in cancelleria il 20 luglio 1992.                             &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
