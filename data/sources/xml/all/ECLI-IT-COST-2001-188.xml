<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>188</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:188</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Franco Bile</redattore_pronuncia>
    <data_decisione>04/06/2001</data_decisione>
    <data_deposito>08/06/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale del combinato disposto &#13;
degli  artt. 76,  122  e 100 del codice di procedura penale, promosso &#13;
con  ordinanza emessa il 29 novembre 1999 dal Tribunale di Milano nel &#13;
procedimento  penale  a  carico di S. L. ed altro, iscritta al n. 823 &#13;
del  registro  ordinanze  2000  e pubblicata nella Gazzetta Ufficiale &#13;
della Repubblica n. 2, 1ª serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  della  S.I.A.E.  nonché l'atto di &#13;
intervento del Presidente del Consiglio dei ministri; &#13;
    Udito  nella  camera  di  consiglio del 26 aprile 2001 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto  che  il  tribunale  di  Milano, con ordinanza emessa il &#13;
29 novembre  1999  e pervenuta a questa Corte il 13 dicembre 2000, ha &#13;
sollevato,  in  riferimento  agli  artt. 3  e  24 della Costituzione, &#13;
questione di legittimità costituzionale del combinato disposto degli &#13;
artt. 76,  122  e  100 del codice di procedura penale, nella parte in &#13;
cui  prevedono  che "la procura speciale per la costituzione di parte &#13;
civile  debba  essere  fatta  per  atto  pubblico o scrittura privata &#13;
autenticata,  senza  dare possibilità al difensore di certificare la &#13;
genuinità  della  sottoscrizione dell'avente diritto, quando apposta &#13;
in  calce  o  a lato di un medesimo atto contenente sia la procura ad &#13;
actum  -  nomina di un procuratore che si costituisca parte civile in &#13;
nome  e  per  conto  del  rappresentato  -  sia il conferimento della &#13;
procura ad litem al difensore stesso"; &#13;
        che  a  tal  proposito  il giudice rimettente ha premesso, in &#13;
fatto,  che,  nel  corso  di un procedimento per reati previsti dalla &#13;
legge  n. 633  del  1941,  una delle parti offese si costituiva parte &#13;
civile,  con atto sottoscritto dal difensore ed in calce al quale era &#13;
stata apposta la nomina di due avvocati quali procuratori e difensori &#13;
dell'ente  costituendo,  con sottoscrizione del legale rappresentante &#13;
autenticata da uno dei due difensori; &#13;
        che,  accogliendo l'eccezione formulata dalle altre parti, il &#13;
tribunale  rimettente  dichiarava  inammissibile tale costituzione di &#13;
parte  civile,  in  quanto  - sottolinea il giudice a quo - mentre il &#13;
mandato  alle liti può essere autenticato dal difensore, tale potere &#13;
non  può  essergli  riconosciuto  in relazione alla procura ad actum &#13;
posto  che  l'art. 122  del  codice di rito espressamente richiede, a &#13;
pena  di  inammissibilità,  che tale procura sia rilasciata per atto &#13;
pubblico o scrittura privata autenticata; &#13;
        che  alla  stregua  di  tali  rilievi,  le norme impugnate si &#13;
porrebbero, dunque, in contrasto: &#13;
          con   l'art. 3  Cost.,  in  quanto  si  determinerebbe  una &#13;
ingiustificata  disparità di trattamento tra soggetti titolari di un &#13;
diritto  risarcitorio, a seconda che tale diritto sia fatto valere in &#13;
sede  civile  o in sede penale, sede, quest'ultima, ove maggiori sono &#13;
gli oneri e le difficoltà per ottenere il ristoro dei danni patiti; &#13;
          con  l'art. 24  Cost.,  giacché  i maggiori  oneri imposti &#13;
dalle  norme  processuali penali rispetto a quelle civili (obbligo di &#13;
comparire  personalmente  all'udienza, o, in alternativa, di nominare &#13;
un  procuratore  speciale  mediante atto pubblico o scrittura privata &#13;
autenticata),  "limitano  ingiustificatamente  il diritto di agire in &#13;
giudizio  per  la  tutela  dei  propri  diritti,  che  il danneggiato &#13;
troverebbe  se  invece di adire il giudice civile scegliesse di adire &#13;
il giudice penale mediante costituzione di parte civile"; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri, rappresentato e difeso dalla Avvocatura generale dello &#13;
Stato,  chiedendo  disporsi  la  restituzione degli atti al tribunale &#13;
rimettente  per  ius  superveniens  ovvero, in subordine, dichiararsi &#13;
infondata la proposta questione; &#13;
        che  ha  altresì  spiegato  atto di intervento l'ente la cui &#13;
dichiarazione  di costituzione di parte civile nel procedimento a quo &#13;
è stata dichiarata inammissibile. &#13;
    Considerato che, a prescindere dal mutamento del quadro normativo &#13;
intervenuto  dopo  la  pronuncia  della  ordinanza  di rimessione, la &#13;
questione  proposta  è  palesemente  inammissibile  per  difetto del &#13;
requisito  della  rilevanza,  giacché  il  giudice  a  quo  - avendo &#13;
espressamente  affermato  che  non  v'è  più  alcuno "spazio per un &#13;
ulteriore   esame   della   costituzione   di   parte   civile,  già &#13;
necessariamente dichiarata inammissibile", ma che si ritiene tuttavia &#13;
"importante"  sottoporre  il  quesito al vaglio di questa Corte - per &#13;
questa via attesta di aver già fatto definitiva applicazione proprio &#13;
delle norme della cui legittimità costituzionale ora dubita; &#13;
        che  gli  accennati  rilievi  consentono di ritenere superato &#13;
anche   il  problema  relativo  alla  legittimazione  all'intervento, &#13;
spiegato  nel presente giudizio, dall'ente non ammesso a rivestire la &#13;
qualità di parte privata nel procedimento a quo. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  degli artt. 76, 122 e 100 del codice di &#13;
procedura  penale,  sollevata, in riferimento agli artt. 3 e 24 della &#13;
Costituzione, dal tribunale di Milano con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 4 giugno 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Bile &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria l'8 giugno 2001 . &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
