<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>131</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:131</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>DE STEFANO</presidente>
    <relatore_pronuncia>Oronzo Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/05/1984</data_decisione>
    <data_deposito>04/05/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ANTONINO DE STEFANO, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. &#13;
 ETTORE GALLO - Dott. ALDO CORASANITI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt.   10  e  20  &#13;
 della legge 2 aprile 1968, n. 482 (Disciplina generale delle assunzioni  &#13;
 obbligatorie  presso le pubbliche amministrazioni e le aziende private)  &#13;
 promosso con ordinanza emessa il 1 marzo 1978 dal tribunale  di  Torino  &#13;
 nel  procedimento  civile  vertente  tra  Società Aspera e Bosco Paolo  &#13;
 iscritta al n. 286 del  registro  ordinanze  1978  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 20 dell'anno 1978.                &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 10 gennaio 1984 il giudice relatore  &#13;
 Oronzo Reale;                                                            &#13;
     udito  l'Avvocato  dello  Stato  Giuseppe  Angelini  Rota  per   il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza 1 marzo 1978 il tribunale di Torino solleva questione  &#13;
 incidentale  di  legittimità  costituzionale degli artt. 10 e 20 della  &#13;
 legge 2 aprile 1968, n. 482, nella  parte  in  cui  dette  norme  -  in  &#13;
 contrasto  con  gli artt. 3 e 24 della Costituzione - prevedono che gli  &#13;
 invalidi obbligatoriamente avviati al lavoro possano essere  licenziati  &#13;
 per sopravvenuta inidoneità, previo accertamento della perdita di ogni  &#13;
 capacità  lavorativa  o  di  un  aggravamento  di  invalidità tale da  &#13;
 determinare pregiudizio alla salute ed alla incolumità dei compagni di  &#13;
 lavoro, nonché è  alla  sicurezza  degli  impianti,  accertamento  da  &#13;
 effettuarsi   a   cura   del   Collegio   Medico   Provinciale,  organo  &#13;
 amministrativo nominato dal Prefetto.                                    &#13;
     Si osserva che avverso la  decisione  di  tale  collegio  non  sono  &#13;
 previsti  rimedi né in sede amministrativa né in sede giurisdizionale  &#13;
 e che il procedimento  che  ivi  si  segue  non  sarebbe  garantito  da  &#13;
 "adeguata  pubblicità":    che  il lavoratore, inoltre, non avrebbe il  &#13;
 diritto di farsi assistere da  un  patrocinatore  legale  in  grado  di  &#13;
 esercitare un controllo sulla legittimità formale del procedimento.     &#13;
     Si evidenzia altresì che il sindacato di legittimità spettante al  &#13;
 giudice  ordinario in merito al ricordato provvedimento sarebbe ridotto  &#13;
 ad una indagine "rivolta unicamente ad esaminare se fra i presupposti e  &#13;
 il dispositivo dell'atto  stesso  esiste  una  necessaria  correlazione  &#13;
 logica"  e  che perciò, poiché si tratta di un giudizio che incide su  &#13;
 di  un  diritto  soggettivo  del  lavoratore,  si   appaleserebbe   una  &#13;
 disparità  di trattamento con la procedura prevista dalla legge n. 604  &#13;
 del 1966, in tema di  giusta  causa  legittimante  la  risoluzione  del  &#13;
 rapporto  di  lavoro,  con  conseguente  violazione  dell'art.  3 della  &#13;
 Costituzione.                                                            &#13;
     Ancora, nell'ordinanza si sottolinea che nella specie sussisterebbe  &#13;
 "una  sostanziale  impossibilità  per  il  lavoratore  di   esercitare  &#13;
 congruamente  il proprio diritto di difesa", con conseguente violazione  &#13;
 dell'art. 24 della Costituzione, in quanto il fatto da cui trae origine  &#13;
 il licenziamento non sarebbe più valutabile nel merito  da  parte  del  &#13;
 giudice ordinario in sede di impugnazione del licenziamento.             &#13;
     In  punto  di rilevanza, nell'ordinanza si afferma che la questione  &#13;
 sollevata risponde a  tale  requisito  "potendo  incidere  direttamente  &#13;
 sulla legittimità del licenziamento impugnato".                         &#13;
     Ha spiegato intervento il Presidente del Consiglio dei ministri per  &#13;
 il  tramite  dell'Avvocatura  generale  dello  Stato,  chiedendo che la  &#13;
 proposta questione venga dichiarata infondata in quanto:                 &#13;
     a) la legge n. 482 del 1968 non introduce nuove o diverse cause  di  &#13;
 licenziamento  rispetto  alla  disciplina  ordinaria, ma limita ai casi  &#13;
 più  gravi  di  evoluzione   dell'invalidità   la   possibilità   di  &#13;
 giustificare  il recesso, in armonia con gli scopi e le finalità della  &#13;
 legge in questione;                                                      &#13;
     b) gli artt. 10 e 20 della ricordata  legge  non  hanno  fatto  che  &#13;
 anticipare  la  disciplina,  poi  introdotta in forza dell'art. 5 della  &#13;
 legge n. 300 del 1970, sottraendo al datore di lavoro l'accertamento di  &#13;
 fatto e demandandolo  ad  un  organo  pubblico,  in  grado  di  fornire  &#13;
 maggiori garanzie di obiettività e competenza;                          &#13;
     c)  il  fatto  che  sia  diverso  l'organo pubblico cui è commesso  &#13;
 l'accertamento, rispetto all'ipotesi generale,  è  giustificato  dalla  &#13;
 specialità  della  disciplina  in  questione,  che  richiede  un  più  &#13;
 complesso  e  accurato  controllo  e  non  crea  quindi  disparità  di  &#13;
 trattamento;                                                             &#13;
     d) l'ipotizzata violazione del diritto di difesa non sussisterebbe,  &#13;
 in   quanto,  in  caso  di  contestazione  relativamente  all'obiettiva  &#13;
 esistenza del fatto giustificante il recesso ex art. 10 citato, permane  &#13;
 nel giudice il potere  di  accertarne  la  sussistenza  o  meno,  senza  &#13;
 preclusioni,  mentre,  nella  fase  extra-giudiziale,  il lavoratore è  &#13;
 facultizzato a  farsi  assistere  da  un  medico  di  fiducia,  il  cui  &#13;
 intervento,  considerata  la  sede tecnica in cui si attua, appare più  &#13;
 utile che non la partecipazione di un patrocinatore legale.<diritto>Considerato in diritto</diritto>:                          &#13;
     La censura di incostituzionalità proposta dal tribunale di  Torino  &#13;
 con l'ordinanza riassunta in narrativa avrebbe fondamento se fosse vero  &#13;
 quanto  nell'ordinanza  stessa  si  afferma,  cioè  che  "a  norma del  &#13;
 combinato disposto di cui agli artt. 10 e 20 della citata legge (n. 482  &#13;
 del 1968) il giudizio  sulla  sopravvenuta  inidoneità  al  lavoro  è  &#13;
 demandato   al   Collegio  Medico  Provinciale,  organo  amministrativo  &#13;
 nominato dal prefetto, ed avverso  tale  decisione  non  sono  previsti  &#13;
 rimedi né in sede amministrativa, né in sede giurisdizionale".         &#13;
     Ma così non è.                                                     &#13;
     Essendo  in  gioco  non  un  interesse  legittimo,  ma  un  diritto  &#13;
 soggettivo del lavoratore alla tutela  del  suo  posto  di  lavoro  ove  &#13;
 esistano  le  condizioni  previste  dall'art. 20 della legge n. 482 del  &#13;
 1968, cioè che "la natura  ed  il  grado  dell'invalidità  non  possa  &#13;
 riuscire  di  pregiudizio alla salute o all'incolumità dei compagni di  &#13;
 lavoro ed  alla  sicurezza  degli  impianti",  deve  escludersi  -  nel  &#13;
 silenzio  della  norma  -  che  il  "referto"  del  collegio medico sia  &#13;
 sottratto al controllo di merito dell'autorità giudiziaria.             &#13;
     Come ha  ritenuto  la  Corte  di  Cassazione,  la  valutazione  del  &#13;
 collegio   medico   costituisce   soltanto   una   "sorta   di  perizia  &#13;
 stragiudiziale altamente qualificata, ma  liberamente  valutabile"  dal  &#13;
 giudice  ordinario  presso il quale è intatto il diritto di difesa del  &#13;
 lavoratore che  il  giudice  a  quo  reputa,  invece,  compresso  nella  &#13;
 procedura   di   accertamento   della  esistenza  o  inesistenza  delle  &#13;
 condizioni legittimanti il  licenziamento.  E  non  sussistendo  questo  &#13;
 pregiudizio al diritto di difesa, evidentemente non sussiste nemmeno la  &#13;
 denunciata  pregiudizievole  "disparità  di trattamento" rispetto alla  &#13;
 tutela giurisdizionale che al lavoratore viene accordata dalla legge n.  &#13;
 604 del 1966.                                                            &#13;
     La questione, pertanto, è infondata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata, nei sensi di cui in motivazione, la questione  &#13;
 di legittimità costituzionale degli artt. 10 e 20 della legge 2 aprile  &#13;
 1968, n. 482 sollevata dal tribunale di  Torino,  in  riferimento  agli  &#13;
 artt.  3  e 24 della costituzione, con l'ordinanza n. 278 del reg. ord.  &#13;
 1978 di cui in epigrafe.                                                 &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 2 maggio 1984.                                &#13;
                                   F.to: ANTONINO DE STEFANO - GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - GIUSEPPE FERRARI  &#13;
                                   - FRANCESCO SAJA - GIOVANNI  CONSO  -  &#13;
                                   ETTORE GALLO - ALDO CORASANITI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
