<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-03-20/54/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-03-20/54"/>
          <FRBRalias value="ECLI:IT:COST:2019:54" name="ECLI"/>
          <FRBRdate date="2019-03-20" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="54"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-03-20/54/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-03-20/54/ita@"/>
          <FRBRdate date="2019-03-20" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-03-20/54/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-03-20/54/ita@.xml"/>
          <FRBRdate date="2019-03-20" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2019-02-06" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2019-03-20" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="LATTANZI" href="/akn/ontology/person/it/LATTANZI" showAs="LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2019</akn4cc:anno>
        <akn4cc:numero>54</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2019:54</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>LATTANZI</akn4cc:presidente>
        <akn4cc:relatore>Giuliano Amato</akn4cc:relatore>
        <akn4cc:data_decisione>2019-02-06</akn4cc:data_decisione>
        <akn4cc:data_deposito>2019-03-20</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2006-01-10/4/!main#art_31">art. 31 del decreto-legge 10 gennaio 2006, n. 4</ref> (Misure urgenti in materia di organizzazione e funzionamento della pubblica amministrazione), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2006-03-09/80/!main">legge 9 marzo 2006, n. 80</ref>, promosso dalla Corte di appello di Roma, nel procedimento vertente tra Ente Autonomo Volturno srl (EAV srl), quale incorporante di Circumvesuviana srl, e il Ministero delle infrastrutture e dei trasporti, con ordinanza del 24 luglio 2017, iscritta al n. 168 del registro ordinanze 2017 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2017.
 Visti l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della società EAV srl, nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito nella udienza pubblica del 5 febbraio 2019 il Giudice relatore Giuliano Amato;
 uditi l'avvocato Michele Mascolo per l'EAV srl e l'avvocato dello Stato Gianni De Bellis per il Presidente del Consiglio dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 24 luglio 2017, la Corte di appello di Roma ha sollevato questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2006-01-10/4/!main#art_31">art. 31 del decreto-legge 10 gennaio 2006, n. 4</ref> (Misure urgenti in materia di organizzazione e funzionamento della pubblica amministrazione), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2006-03-09/80/!main">legge 9 marzo 2006, n. 80</ref>.
 La disposizione censurata prevede che «[l]e regolazioni debitorie dei disavanzi delle ferrovie concesse e in ex gestione commissariale governativa, comprensivi degli oneri di trattamento di fine rapporto maturati alla data del 31 dicembre 2000, previste dall'<ref href="/akn/it/act/legge/stato/2000-12-23/388/!main#art_145__para_30">articolo 145, comma 30, della legge 23 dicembre 2000, n. 388</ref>, si intendono definite nei termini delle istruttorie effettuate congiuntamente dal Ministero delle infrastrutture e dei trasporti e dal Ministero dell'economia e delle finanze a seguito delle comunicazioni effettuate e delle istanze formulate dalle aziende interessate entro il 31 agosto 2005».
 Ad avviso del giudice a quo, la disposizione in esame violerebbe, in primo luogo, l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref> per l'ingiustificata discriminazione che si determinerebbe tra quei soggetti che, pur non essendo vincolati al rispetto di alcun termine per la presentazione di istanze o comunicazioni ai fini del rimborso, per mera casualità le avessero presentate entro il termine previsto dalla disposizione censurata, e quei soggetti che, invece, non lo avessero fatto.
 Sarebbero violati anche l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> ed il principio di buon andamento ed imparzialità dell'azione amministrativa, per la lesione del legittimo affidamento sorto in capo agli aventi diritto.
 2.- Il giudice a quo è chiamato a decidere in ordine all'impugnazione della sentenza con cui il Tribunale ordinario di Roma ha respinto la domanda avanzata dalla Circumvesuviana srl, poi incorporata nell'Ente Autonomo Volturno srl (EAV srl), al fine di ottenere la condanna del Ministero delle infrastrutture e dei trasporti al rimborso di pagamenti effettuati dalla società attrice, subentrata nei rapporti passivi della gestione commissariale governativa, già gestita da Ferrovie dello Stato spa.
 In punto di fatto, il giudice a quo riferisce che il Ministero delle infrastrutture e trasporti ha negato il rimborso, ritenendo tardiva la richiesta avanzata il 15 maggio 2006 rispetto al termine del 31 agosto 2005, fissato dalla disposizione censurata. A sostegno dell'impugnazione, la parte appellante ha prospettato l'illegittimità costituzionale della previsione del termine.
 2.1.- La Corte di appello ritiene di non potere accedere ad un'interpretazione adeguatrice, tale da consentire un'applicazione della disposizione censurata conforme ai principi di eguaglianza, ragionevolezza e legittimo affidamento.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il rimettente evidenzia che questa interpretazione farebbe leva sulla distinzione, contenuta nella disposizione censurata, tra le «istanze», ossia le richieste di erogazione di somme, e le «comunicazioni», riferite invece ad ogni tipo di segnalazione in ordine alla regolazione dei disavanzi. Questa distinta considerazione sarebbe giustificata dall'esigenza di non pregiudicare ingiustamente le aziende che non avessero formulato alcuna istanza entro il 31 agosto 2005, allorché non era previsto alcun termine. Tuttavia, non risultando che la società appellante abbia inviato alcuna comunicazione circa i disavanzi da regolare, adeguata a consentirne la definizione, nel caso in esame tale interpretazione conforme a <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> sarebbe impraticabile.
 L'unica interpretazione possibile della disposizione censurata determinerebbe la rilevanza della questione di costituzionalità ai fini della risoluzione della controversia.
 2.2.- Quanto alla non manifesta infondatezza, la Corte di appello ritiene che la previsione di un termine di decadenza retroattivo, in quanto applicabile rispetto a diritti già maturati, si ponga in contrasto con i principi di cui agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, anche con riferimento all'art. 11 delle disposizioni preliminari al <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">codice civile</ref>.
 Si determinerebbe, infatti, un'ingiustificata discriminazione tra quei soggetti che, pur non essendo vincolati al rispetto di alcun termine per la presentazione di istanze o comunicazioni ai fini del rimborso, per mera casualità le avessero presentate entro il termine, e quei soggetti che, invece, non lo avessero fatto. Ad integrare la violazione dei principi costituzionali non sarebbe la retroattività in sé considerata, ma l'effetto che ne consegue.
 Sarebbe inoltre violato il principio di buon andamento ed imparzialità dell'azione amministrativa, per la lesione del legittimo affidamento sorto in capo agli aventi diritto.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>È richiamata la giurisprudenza costituzionale che ha riconosciuto un'ampia discrezionalità legislativa in materia di successione di leggi, salvo il limite imposto in materia penale dall'art. 25, secondo comma, Cost., e, comunque, purché la retroattività trovi adeguata e ragionevole giustificazione e non si ponga in contrasto con altri valori ed interessi costituzionalmente protetti. Questa stessa giurisprudenza ha altresì affermato che il principio del legittimo affidamento costituisce elemento fondamentale dello Stato di diritto e non può essere leso da disposizioni retroattive che trasmodino in regolamento irrazionale di situazioni fondate su leggi anteriori.
 3.- Nel giudizio dinanzi alla Corte si è costituita la società appellante EAV srl, quale avente causa della incorporata Circumvesuviana srl, chiedendo l'accoglimento della questione di legittimità costituzionale.
 3.1.- La parte costituita evidenzia che la disposizione censurata, entrata in vigore il 13 febbraio [recte: il 12 gennaio] 2006, avrebbe cancellato, con effetto dal 31 agosto 2005, il meccanismo di regolazione attraverso il quale il Ministero delle infrastrutture e dei trasporti provvedeva a rimborsare alle aziende subentrate alle gestioni commissariali governative gli oneri sostenuti per i disavanzi di queste ultime. In questo modo, le aziende che non abbiano presentato le proprie istanze entro il 31 agosto 2005 sarebbero discriminate rispetto a quelle che, per circostanze casuali, abbiano proposto le proprie istanze prima di tale data.
 La disposizione in esame sarebbe, inoltre, lesiva dell'affidamento riposto dalle imprese di trasporto in un sistema di regolazione, in vigore da cinque anni, che consentiva di attendere che i contenziosi ed i rapporti giuridici riferibili alle ex gestioni si concludessero e che i relativi esiti fossero inseriti nel consuntivo di cui chiedere il rimborso al Ministero.
 La disposizione censurata non rivestirebbe neppure natura premiale di condotte diligenti. Infatti, gran parte dei crediti in questione sarebbero derivati dal contenzioso promosso dai lavoratori, il quale avrebbe un iter processuale e una durata diversi a seconda del contesto territoriale.
 L'art. 31 in esame finirebbe paradossalmente per penalizzare proprio le società più virtuose, che si siano costituite in giudizio per contrastare le richieste dei lavoratori ricorrenti. Infatti, per tutta la durata del processo sarebbe impossibile rendicontare i relativi esborsi.
 La medesima disposizione si porrebbe inoltre in contrasto con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> e con il principio di buona amministrazione.
 La società costituita sottolinea che il diritto al rimborso trova la sua fonte legale nell'<ref href="/akn/it/act/decretoLegislativo/stato/1997-11-19/422/!main#art_8__para_6">art. 8, comma 6, del decreto legislativo 19 novembre 1997, n. 422</ref> (Conferimento alle regioni ed agli enti locali di funzioni e compiti in materia di trasporto pubblico locale, a norma dell'<ref href="/akn/it/act/legge/stato/1997-03-15/59/!main#art_4__para_4">articolo 4, comma 4, della legge 15 marzo 1997, n. 59</ref>) e nell'<ref href="/akn/it/act/legge/stato/2000-12-23/388/!main#art_145">art. 145 della legge 23 dicembre 2000, n. 388</ref>, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2001)», da cui scaturisce l'obbligo del Ministero di adempiere. In quanto rispondente ad un'obbligazione fondata sulla legge, ai sensi dell'<ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_1173">art. 1173 del codice civile</ref>, l'adempimento dell'amministrazione sarebbe attività vincolata e non discrezionale. Esso costituirebbe il soddisfacimento di un diritto perfetto del creditore, sul quale non potrebbe influire l'esercizio dei poteri amministrativi connessi all'erogazione della spesa.
 Dopo avere pagato gli importi di cui alle condanne giudiziali, la parte costituita li ha quindi inseriti nei rendiconti allegati alle proprie richieste di rimborso. Nell'istanza del 15 maggio 2006 era contenuto il prospetto di un contenzioso, già noto al Ministero, in cui erano riassunti pagamenti effettuati dal 1° gennaio 2005 al 31 dicembre 2005, per fatti relativi al periodo precedente al 31 dicembre 2000 e comunicati sin dal 2002. Si osserva come sarebbe stato impossibile inviare un'istanza di rimborso prima di potere rendicontare con esattezza gli esborsi sostenuti. Il credito nei confronti del Ministero risulta tuttora evidenziato nel bilancio di esercizio della società EAV srl, che ha previsto un apposito fondo svalutazione per fare fronte ai rischi connessi al mancato rimborso.
 La società appellante contesta l'interpretazione fatta propria dalla sentenza di primo grado, secondo la quale l'<ref href="/akn/it/act/decretoLegge/stato/2006/4/!main#art_31">art. 31 del d.l. n. 4 del 2006</ref> sarebbe una norma programmatica, giustificata dall'intento di porre fine al regime transitorio ed ai relativi strumenti di assistenza finanziaria nel subentro nelle gestioni commissariali, aprendo il settore alla concorrenza. La parte costituita fa rilevare che il sistema di regolazione del disavanzo non prevedeva alcun termine di decadenza per il recupero delle spese sostenute. I crediti derivanti da transazioni, liti giudiziarie, erogazioni di trattamenti di fine rapporto, sorti prima del 31 dicembre 2000, si sarebbero concretizzati successivamente, all'esito dei relativi giudizi. Del tutto inconferente ed indimostrato sarebbe poi il riferimento alla tutela della concorrenza, quale principio ispiratore della disposizione censurata.
 Dopo avere richiamato alcune pronunce della giurisprudenza ordinaria e amministrativa sul principio di legittimo affidamento, la parte costituita illustra la giurisprudenza costituzionale sul canone di ragionevolezza e sul sindacato di proporzionalità della legge, con particolare riferimento a disposizioni aventi efficacia retroattiva.
 Alla luce di tale giurisprudenza, si evidenzia, altresì, la disparità di trattamento derivante dall'avere regolato diversamente, ed in modo ingiustificato, situazioni giuridiche sostanziali identiche, assumendo come criterio distintivo la data di presentazione dell'istanza di rimborso.
 Quanto alla violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref>, la parte costituita sottolinea che l'impatto della norma retraoattiva sarebbe pregiudizievole rispetto all'attività procedimentale della pubblica amministrazione. Il censurato art. 31 avrebbe infatti vanificato lo svolgimento delle istruttorie già avviate con comunicazioni successive al 31 agosto 2005, ancorché anteriori alla sua entrata in vigore, il 12 gennaio 2006. Ciò avrebbe determinato uno spreco di risorse, in termini di personale ed attività, in palese contrasto con i principi di economicità, efficacia ed efficienza, che fanno da corollario al buon andamento della pubblica amministrazione. Peraltro, la lesione più grave all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> discenderebbe dalla violazione del principio di imparzialità, essendo irrimediabilmente compromesso l'affidamento nella certezza di rapporti giuridici, che costituisce principio cardine dell'ordinamento giuridico.
 Si osserva, d'altra parte, che, per salvaguardare i principi di uguaglianza, di buon andamento ed imparzialità, sarebbe stato sufficiente prevedere un termine ragionevole e posteriore, anziché antecedente all'entrata in vigore della legge. Inoltre, la responsabilità del Ministero delle infrastrutture e dei trasporti per fatti di gestione precedenti al subentro avrebbe potuto essere definita senza necessità di interventi legislativi. Al più, avrebbero potuto essere ottimizzati i tempi e le procedure di rimborso, ma senza comprimere il diritto al recupero di pagamenti già effettuati. La preclusione di tale rimborso trasmoderebbe, quindi, in un regolamento irrazionale di situazioni giuridiche fondate su un assetto stabilito da leggi anteriori, sul quale gli amministrati facevano affidamento.
 4.- Nel giudizio innanzi alla Corte, è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o comunque non fondata.
 4.1.- È eccepita, in primo luogo, l'inammissibilità della questione per l'omessa sperimentazione «di un'esegesi autenticamente adeguatrice del dato normativo». Il giudice a quo avrebbe dovuto verificare la possibilità di un'interpretazione diversa da quella, sinteticamente prospettata nell'ordinanza di rimessione, secondo la quale dall'inosservanza del termine discenderebbe l'estinzione del credito restitutorio.
 4.2.- Nel merito, l'Avvocatura generale dello Stato ritiene che l'interpretazione del giudice a quo contrasti con il tenore letterale della disposizione censurata, che non prevede espressamente il termine a pena di decadenza, nonché con il <ref href="/akn/it/act/decretoLegislativo/stato/1997/422/!main">d.lgs. n. 422 del 1997</ref> e con il successivo decreto del Presidente del Consiglio dei ministri 16 novembre 2000 (Individuazione e trasferimento alle regioni delle risorse per l'esercizio delle funzioni e compiti conferiti ai sensi degli <mref><ref href="/akn/it/act/decretoLegislativo/stato/1997-11-19/422/!main#art_8">articoli 8</ref> e <ref href="/akn/it/act/decretoLegislativo/stato/1997-11-19/422/!main#art_12">12 del D.Lgs. 19 novembre 1997, n. 422</ref></mref> in materia di trasporto pubblico locale), che pongono a carico dello Stato la copertura dei disavanzi maturati.
 L'Avvocatura generale dello Stato fa notare che, ai sensi dell'<ref href="/akn/it/act/legge/stato/2000/388/!main#art_145">art. 145 della legge n. 388 del 2000</ref>, cui la disposizione censurata rinvia, gli oneri destinati a rimanere a carico dello Stato includono i disavanzi maturati fino al 31 dicembre 2000, ivi compresi gli oneri per il trattamento di fine rapporto. Il termine quinquennale di prescrizione di tali obbligazioni decorre dalla cessazione del rapporto di lavoro (<ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_2948">art. 2948 cod. civ.</ref>). Era quindi possibile che, alla data del 31 agosto 2005, non fossero ancora stati azionati tutti i debiti per i trattamenti di fine rapporto, riferiti a periodi antecedenti il conferimento.
 Ad avviso dell'Avvocatura generale dello Stato, la disposizione impugnata si limiterebbe a dettare soltanto le modalità procedurali per la definizione delle istanze depositate fino al 31 agosto 2005. Essa non inciderebbe sul diritto delle aziende subentrate alle ex gestioni commissariali di ottenere il rimborso dei pagamenti anche successivi a tale data, riferibili ad oneri maturati nella gestione antecedente al 31 dicembre 2000.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- La Corte di appello di Roma ha sollevato questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2006-01-10/4/!main#art_31">art. 31 del decreto-legge 10 gennaio 2006, n. 4</ref> (Misure urgenti in materia di organizzazione e funzionamento della pubblica amministrazione), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2006-03-09/80/!main">legge 9 marzo 2006, n. 80</ref>.
 La disposizione censurata prevede che «Le regolazioni debitorie dei disavanzi delle ferrovie concesse e in ex gestione commissariale governativa, comprensivi degli oneri di trattamento di fine rapporto maturati alla data del 31 dicembre 2000, previste dall'<ref href="/akn/it/act/legge/stato/2000-12-23/388/!main#art_145__para_30">articolo 145, comma 30, della legge 23 dicembre 2000, n. 388</ref>, si intendono definite nei termini delle istruttorie effettuate congiuntamente dal Ministero delle infrastrutture e dei trasporti e dal Ministero dell'economia e delle finanze a seguito delle comunicazioni effettuate e delle istanze formulate dalle aziende interessate entro il 31 agosto 2005».
 Ad avviso del giudice a quo, tale disposizione violerebbe, in primo luogo, l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>, per l'ingiustificata discriminazione che si determinerebbe tra quei soggetti che, pur non essendo vincolati al rispetto di alcun termine per la presentazione di istanze o comunicazioni ai fini del rimborso, per mera casualità le avessero presentate entro il termine previsto dalla disposizione censurata, e quei soggetti che, invece, non lo avessero fatto.
 Sarebbero violati anche l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> ed il principio di buon andamento ed imparzialità dell'azione amministrativa, per la lesione del legittimo affidamento sorto in capo agli aventi diritto.
 2.- In via preliminare, va esaminata l'eccezione di inammissibilità formulata dall'Avvocatura generale dello Stato.
 Ad avviso di quest'ultima, il rimettente avrebbe omesso di esplorare la possibilità di dare un'interpretazione costituzionalmente orientata del censurato art. 31.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'eccezione non è fondata.
 Il rimettente si è fatto carico dell'onere di sperimentare un'interpretazione conforme, attribuendo rilievo, ai fini del rispetto del termine introdotto dalla disposizione censurata, oltre che a specifiche istanze di rimborso, anche alle generiche "comunicazioni" o "segnalazioni", comunque riferite alla regolazione dei disavanzi. Tuttavia, ad avviso del giudice a quo, tale considerazione non sarebbe risolutiva, poiché nel caso in esame prima della scadenza del termine non sono state inviate comunicazioni o segnalazioni riferite ai disavanzi da regolare, tali da consentirne la definizione.
 Non è praticabile, d'altra parte, l'interpretazione, suggerita dalla stessa Avvocatura generale dello Stato, secondo la quale dall'inosservanza del termine non discenderebbe l'estinzione del credito restitutorio. Essa contrasta sia con il tenore letterale della disposizione censurata («Le regolazioni debitorie [...] si intendono definite [...] a seguito delle comunicazioni effettuate e delle istanze formulate [...] entro il 31 agosto 2005»), sia con la dichiarata ratio di contenimento della spesa pubblica, risultante anche dalla relazione illustrativa al disegno di legge di conversione del <ref href="/akn/it/act/decretoLegge/stato/2006/4/!main">d.l. n. 4 del 2006</ref>. È significativo, a questo riguardo, che questa stessa interpretazione non sia stata condivisa né dal Ministero delle infrastrutture e dei trasporti, che sulla ritenuta tardività della richiesta ha fondato il rifiuto di procedere al rimborso, né dalla stessa Avvocatura generale dello Stato in entrambi i gradi del giudizio di merito, in cui si è opposta alla domanda attrice proprio sulla base della natura perentoria del termine.
 Appare pertanto corretta la premessa del rimettente, secondo il quale l'univoco tenore letterale della norma censurata preclude un'interpretazione adeguatrice, che deve pertanto cedere il passo al sindacato di legittimità costituzionale (ex plurim<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/258/!main">is, sentenze nn. 258</ref>, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/82/!main">82</ref> e <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/42/!main">42 del 2017</ref></mref>, n. 95 del 2016).
 3.- Nel merito, la questione è fondata in riferimento all'art. 3, primo comma, Cost.
 La disposizione censurata, in vigore dal 12 gennaio 2006, ha previsto la definizione dei disavanzi delle ferrovie concesse e in ex gestione commissariale governativa nei termini delle istruttorie effettuate a seguito delle comunicazioni e delle istanze formulate entro il 31 agosto 2005. Introducendo un termine per la presentazione delle istanze, è stato innovato il meccanismo attraverso il quale lo Stato aveva sino ad allora provveduto al ripianamento del disavanzo maturato alla data del conferimento di funzioni in materia di trasporto pubblico locale, realizzato in base al <ref href="/akn/it/act/decretoLegislativo/stato/1997-11-19/422/!main">decreto legislativo 19 novembre 1997, n. 422</ref> (Conferimento alle regioni ed agli enti locali di funzioni e compiti in materia di trasporto pubblico locale, a norma dell'<ref href="/akn/it/act/legge/stato/1997-03-15/59/!main#art_4__para_4">articolo 4, comma 4, della legge 15 marzo 1997, n. 59</ref>).
 Va inoltre evidenziato, che in base all'art. 8, comma 6, dello stesso <ref href="/akn/it/act/decretoLegislativo/stato/1997/422/!main">d.lgs. n. 422 del 1997</ref>, nell'ambito dei disavanzi maturati alla data del conferimento di funzioni erano ricompresi anche gli oneri per il trattamento di fine rapporto. Tale inclusione è stata poi confermata dall'<ref href="/akn/it/act/legge/stato/2000-12-23/388/!main#art_145__para_30">art. 145, comma 30, della legge 23 dicembre 2000, n. 388</ref>, recante «Disposizioni per la formazione del bilancio annuale e pluriennale dello Stato (legge finanziaria 2001)», e infine dalla disposizione censurata.
 In considerazione del termine quinquennale di prescrizione dei crediti per indennità di cessazione del rapporto di lavoro, decorrente appunto da tale cessazione, era pertanto del tutto verosimile che, alla data del 31 agosto 2005, obbligazioni relative a periodi precedenti al 2000 non fossero state ancora definite in sede giudiziale e, quindi, non fossero state ancora avanzate istanze di rimborso al Ministero.
 A partire dal 31 agosto 2005 la disposizione censurata preclude la possibilità di ottenere la copertura dei disavanzi derivanti dal subentro nelle funzioni. Tale previsione incide quindi su una fattispecie sorta nella vigenza della precedente disciplina e non ancora esaurita, in ragione del residuo spazio temporale in cui era ancora possibile avanzare le richieste di rimborso.
 Questa Corte ha costantemente riconosciuto che il valore del legittimo affidamento, che trova copertura costituzionale nell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, non esclude che il legislatore possa adottare disposizioni che modificano in senso sfavorevole agli interessati la disciplina di rapporti giuridici, «anche se l'oggetto di questi sia costituito da diritti soggettivi perfetti». Ciò può avvenire, tuttavia, a condizione «che tali disposizioni non trasmodino in un regolamento irrazionale, frustrando, con riguardo a situazioni sostanziali fondate sulle leggi precedenti, l'affidamento dei cittadini nella sicurezza giuridica, da intendersi quale elemento fondamentale dello Stato di diritto» (ex plurim<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/216/!main">is, sentenze n. 216</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/56/!main">56 del 2015</ref></mref>, n. 219 del 2014, n. 154 del 2014, n. 310 e n. 83 del 2013, n. 166 del 2012 e n. 302 del 2010; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/31/!main">ordinanza n. 31 del 2011</ref>).
 Nel caso in esame, l'assetto normativo preesistente alla disposizione denunciata era tale da far sorgere nelle aziende interessate la ragionevole fiducia nella possibilità di esercitare il diritto al rimborso, una volta conclusi gli eventuali giudizi, sino al termine ordinario di prescrizione di questo diritto. La fonte legale dell'obbligazione statale (<ref href="/akn/it/act/decretoLegislativo/stato/1997/422/!main#art_8__para_6">art. 8, comma 6, del d.lgs. n. 422 del 1997</ref> e <ref href="/akn/it/act/legge/stato/2000/388/!main#art_145__para_30">art. 145, comma 30, della legge n. 388 del 2000</ref>), l'anteriorità dei fatti generatori delle obbligazioni rispetto al conferimento delle funzioni ed il decorso di un quinquennio senza alcuna modifica di tale assetto hanno consolidato il legittimo affidamento circa la possibilità di ottenere il ripianamento di tali disavanzi, rendendo per ciò stesso censurabile l'intervento legislativo in esame.
 In particolare, con riguardo alla fissazione di termini per l'esercizio di singoli diritti, questa Corte ha riconosciuto che «il legislatore gode di ampia discrezionalità, con l'unico limite dell'eventuale irragionevolezza, qualora "[il termine] venga determinato in modo da non rendere effettiva la possibilità di esercizio del diritto cui si riferisce"» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/216/!main">sentenze n. 216 del 2015</ref> e n. 234 del 2008; nello stesso senso, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2005/192/!main">sentenze n. 192 del 2005</ref>, n. 197 del 1987 e n. 10 del 1970, <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2000/153/!main">ordinanza n. 153 del 2000</ref>). Nel caso in esame, l'introduzione di un termine di decadenza, già decorso al momento dell'entrata in vigore della disposizione che lo ha previsto, ha definitivamente precluso la possibilità di ottenere il rimborso di oneri già sostenuti.
 D'altra parte, l'intervento legislativo oggetto di censura non può trovare adeguata giustificazione nell'interesse dello Stato alla riduzione della spesa pubblica. Infatti, «[s]e l'obiettivo di ridurre il debito può giustificare scelte anche assai onerose e, sempre nei limiti della ragionevolezza e della proporzionalità, la compressione di situazioni giuridiche rispetto alle quali opera un legittimo affidamento, esso non può essere perseguito senza una equilibrata valutazione comparativa degli interessi in gioco e, in particolare, non può essere raggiunto trascurando completamente gli interessi dei privati, con i quali va invece ragionevolmente contemperato» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/216/!main">sentenza n. 216 del 2015</ref>). Nel caso in esame, non risulta che l'interesse alla riduzione della spesa pubblica sia stato adeguatamente bilanciato rispetto al sacrificio imposto alle imprese che, anche dopo il 31 agosto 2005, continuavano a vantare crediti. Infatti, la disposizione censurata comporta tout court l'estinzione di tali obbligazioni e non lascia alcun residuo margine temporale per esercitare il credito; con l'effetto, nel caso di specie, che esse continuano a gravare sul fondo svalutazione, costituito dalla società EAV srl per far fronte ai rischi connessi al mancato rimborso.
 D'altra parte, il tenore letterale della disposizione censurata non consente neppure di riferirla all'accertamento di eventuali sopravvenienze attive, cui lo Stato - in ipotesi - avrebbe rinunciato. Infatti, il suo ambito applicativo è espressamente individuato nelle «regolazioni debitorie dei disavanzi [...] comprensivi degli oneri di trattamento di fine rapporto [...]». Manca, pertanto, qualsiasi riferimento ad eventuali sopravvenienze attive e, a fortiori, alla relativa rinuncia da parte dello Stato, di cui si fa menzione soltanto nella relazione al disegno di legge di conversione del <ref href="/akn/it/act/decretoLegge/stato/2006/4/!main">d.l. n. 4 del 2006</ref>.
 Pertanto, va dichiarata l'illegittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2006/4/!main#art_31">art. 31 del d.l. n. 4 del 2006</ref>, come convertito, per violazione dei principi di tutela dell'affidamento e di ragionevolezza, di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, con assorbimento dell'ulteriore censura formulata in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref></p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2006-01-10/4/!main#art_31">art. 31 del decreto-legge 10 gennaio 2006, n. 4</ref> (Misure urgenti in materia di organizzazione e funzionamento della pubblica amministrazione), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2006-03-09/80/!main">legge 9 marzo 2006, n. 80</ref>.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2019-02-06">6 febbraio 2019</date>.
 F.to:
 <signature><judge refersTo="#giorgioLattanzi" as="#presidente">Giorgio LATTANZI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulianoAmato" as="#relatore">Giuliano AMATO</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2019-03-20">20 marzo 2019</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
