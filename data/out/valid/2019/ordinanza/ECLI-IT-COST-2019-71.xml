<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-03-29/71/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-03-29/71"/>
          <FRBRalias value="ECLI:IT:COST:2019:71" name="ECLI"/>
          <FRBRdate date="2019-03-29" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="71"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-03-29/71/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-03-29/71/ita@"/>
          <FRBRdate date="2019-03-29" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-03-29/71/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-03-29/71/ita@.xml"/>
          <FRBRdate date="2019-03-29" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2019-02-20" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2019-03-29" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="LATTANZI" href="/akn/ontology/person/it/LATTANZI" showAs="LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2019</akn4cc:anno>
        <akn4cc:numero>71</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2019:71</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>LATTANZI</akn4cc:presidente>
        <akn4cc:relatore>Francesco Viganò</akn4cc:relatore>
        <akn4cc:data_decisione>2019-02-20</akn4cc:data_decisione>
        <akn4cc:data_deposito>2019-03-29</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei giudizi di legittimità costituzionale degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">artt. 456</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_552__para_1__point_f">552, comma 1, lettera f), del codice di procedura penale</ref></mref>, promossi dal Tribunale ordinario di Bergamo e dal Tribunale ordinario di Bari, con ordinanze del 21 dicembre e del 3 aprile 2017, iscritte rispettivamente ai nn. 60 e 73 del registro ordinanze 2018 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 16 e 20, prima serie speciale, dell'anno 2018.
 Visti gli atti di intervento del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 20 febbraio 2019 il Giudice relatore Francesco Viganò.
 Ritenuto che il Tribunale ordinario di Bergamo, con ordinanza del 21 dicembre 2017, pervenuta a questa Corte il 27 marzo 2018 (r. o. n. 60 del 2018), ha sollevato, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">art. 24 della Costituzione</ref>, questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">art. 456 del codice di procedura penale</ref>, nella parte in cui non prevede che il decreto di giudizio immediato debba contenere l'avviso della facoltà dell'imputato di chiedere la sospensione del processo con messa alla prova, con la forma e i termini di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_458">art. 458 cod. proc. pen.</ref>;
 che il giudice rimettente premette di essere chiamato a pronunciarsi sulla colpevolezza di un soggetto «imputato, tra l'altro, del reato di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2000/74/!main#art_5">art. 5 decreto legislativo 74/2000</ref> [...], che prevede una pena massima non superiore a quattro anni, che in astratto legittima l'ammissione alla sospensione del processo con messa alla prova», a seguito dell'emissione da parte del giudice per le indagini preliminari di un decreto di giudizio immediato privo dell'avviso della facoltà dell'imputato di chiedere la sospensione del procedimento con messa alla prova;
 che, su istanza formulata dalla difesa dell'imputato prima dell'apertura del dibattimento, il giudice a quo ha ritenuto rilevante e non manifestamente infondata la questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">art. 456 cod. proc. pen.</ref> come sopra formulata;
 che la rilevanza della questione risiederebbe, secondo il rimettente, nel fatto che l'eventuale accoglimento della stessa determinerebbe la nullità del decreto di giudizio immediato, con la conseguente restituzione degli atti al giudice per le indagini preliminari per l'emissione di un nuovo decreto di giudizio immediato, corredato dell'avviso relativo alla facoltà per l'imputato di chiedere la sospensione del procedimento con messa alla prova;
 che, quanto alla non manifesta infondatezza della questione, il rimettente osserva che l'istituto della sospensione del processo con messa alla prova introdotto dalla <ref href="/akn/it/act/legge/stato/2014-04-28/67/!main">legge 28 aprile 2014, n. 67</ref> (Deleghe al Governo in materia di pene detentive non carcerarie e di riforma del sistema sanzionatorio. Disposizioni in materia di sospensione del procedimento con messa alla prova e nei confronti degli irreperibili), costituisca «a tutti gli effetti un nuovo rito alternativo», anche alla luce di quanto statuito da questa Corte nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/240/!main">sentenza n. 240 del 2015</ref>;
 che il rimettente osserva, altresì, che l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">art. 456 cod. proc. pen.</ref> prescrive - a pena di nullità - che il decreto di giudizio immediato contenga l'avviso per l'imputato di potersi avvalere dei riti alternativi del giudizio abbreviato e dell'applicazione della pena su richiesta delle parti, senza tuttavia fare menzione alcuna della facoltà di chiedere la sospensione con messa alla prova;
 che, a parere del giudice a quo, l'obbligo di avvisare l'imputato della facoltà di chiedere riti alternativi sarebbe strettamente connesso all'esercizio del diritto di difesa, tanto che, proprio con riguardo al nuovo istituto della sospensione del procedimento con messa alla prova, questa Corte ha dichiarato l'illegittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_460__para_1__point_e">art. 460, comma 1, lettera e), cod. proc. pen.</ref>, nella parte in cui non prevede che il decreto penale di condanna contenga l'avviso della facoltà dell'imputato di chiedere, mediante l'opposizione, la sospensione del procedimento con messa alla prova, proprio in quanto l'omissione dell'avviso può «determinare un pregiudizio irreparabile» al diritto di difesa di cui all'art. 24, secondo comma, Cost., essendo «nel procedimento per decreto il termine entro il quale chiedere la messa alla prova [...] anticipato rispetto al giudizio» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/201/!main">sentenza n. 201 del 2016</ref>);
 che, ad avviso del rimettente, le medesime ragioni dovrebbero condurre alla dichiarazione di illegittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">art. 456 cod. proc. pen.</ref>, dal momento che, ai sensi del combinato disposto degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_464bis__para_2">artt. 464-bis, comma 2</ref>, e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_464bis__para_458">458</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_464bis__para_1">comma 1, cod. proc. pen.</ref></mref>, la richiesta di riti alternativi deve essere formulata, a pena di decadenza, entro un termine anticipato rispetto al giudizio, e in particolare entro quindici giorni dalla data di notificazione del decreto di giudizio immediato;
 	che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'infondatezza della questione, in quanto sarebbe possibile dare alla disposizione censurata un'interpretazione costituzionalmente conforme, sì da imporre, a pena di nullità, che il decreto di giudizio immediato debba contenere l'avviso all'imputato della facoltà di chiedere la sospensione del procedimento con messa alla prova entro il termine e con le forme di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_458">art. 458 cod. proc. pen.</ref>;
 che il Tribunale ordinario di Bari, con ordinanza del 3 aprile 2017, pervenuta a questa Corte il 24 aprile 2018 (r. o. n. 73 del 2018), ha sollevato, in riferimento agli articoli 3, 24, secondo comma, e 111 Cost., questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_552__para_1__point_f">art. 552, comma 1, lettera f), cod. proc. pen.</ref>, nella parte in cui non prevede l'avviso che, qualora ne ricorrano i presupposti, l'imputato, fino alla dichiarazione di apertura del dibattimento di primo grado, può formulare la richiesta di sospensione del procedimento con messa alla prova, ai sensi degli artt. 168-bis e seguenti del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref> e 464-bis e seguenti <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main">cod. proc. pen</ref>.;
 che il rimettente espone di essere chiamato a giudicare sulla responsabilità penale di tre soggetti imputati del delitto di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_648">art. 648 cod. pen.</ref>, rinviati a giudizio con decreto di citazione diretta, nel quale non era contenuto alcun avviso circa la possibilità di chiedere la sospensione del procedimento con messa alla prova;
 che, a parere del rimettente, l'avviso all'imputato della facoltà di chiedere riti alternativi costituirebbe una garanzia essenziale per il godimento del diritto di difesa, come comprovato dalla sanzione della nullità, ex art. 178, comma 1, lettera e) (recte: lettera c), <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main">cod. proc. pen</ref>., in caso di omesso avviso quando è stabilito un termine di decadenza per la richiesta di riti alternativi (sono richiamate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2012/237/!main">sentenze n. 237 del 2012</ref>, n. 219 e n. 148 del 2004, n. 101 del 1997, n. 70 del 1996, n. 497 del 1995, n. 76 del 1993, nonché l'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2005/309/!main">ordinanza n. 309 del 2005</ref>);
 che l'istituto della messa alla prova avrebbe carattere di rito alternativo al giudizio ordinario;
 che la Corte di cassazione ha, invero, recentemente escluso che ricorra un caso di nullità derivante dal mancato avviso, nel decreto di citazione diretta a giudizio, della facoltà di chiedere la sospensione del processo con messa alla prova, tale omissione non comportando ad avviso della stessa Corte un effetto pregiudizievole per l'imputato, in quanto l'applicazione del beneficio può essere richiesta alla prima udienza (<ref href="/akn/it/judgment/sentenza/corteDiCassazione/2016-12-03/2PEN/!main">Corte di cassazione, sezione seconda penale, sentenza 23 dicembre 2016</ref>, n. 2379 [recte: 3864]);
 che tuttavia, secondo il giudice a quo, una tale possibilità da un lato non garantisce che l'imputato sia realmente consapevole della facoltà in parola, in ragione della frequente assenza di previ contatti tra costui e il difensore, specie se d'ufficio, e dall'altro non assicura che l'imputato abbia il tempo necessario per prendere contatto con l'Ufficio esecuzione penale esterna e per predisporre il programma da sottoporre all'approvazione del giudice;
 che tale situazione integrerebbe una violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, equiparando «soggetti che non possono accedere all'istituto in parola, per cui è del tutto indifferente l'avviso, ai soggetti che invece possono accedervi e che potrebbero vedersi limitati nel diritto di difesa a seguito dell'omissione»;
 che pregiudicati sarebbero, altresì, il diritto di difesa di cui all'art. 24, secondo comma, e 111 Cost., la disciplina oggi vigente non consentendo all'imputato di determinarsi tempestivamente su «quale sia la migliore difesa perseguibile con la scelta dell'istituto più appropriato»;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che le questioni vengano dichiarate inammissibili o comunque infondate;
 che, a parere dell'Avvocatura generale dello Stato, l'inammissibilità delle questioni discenderebbe anzitutto dall'assenza nell'ordinanza di rimessione di qualsiasi descrizione dei fatti oggetto di giudizio, non essendo neppure stati riportati i capi di imputazione e non avendo il rimettente indicato la fase processuale in cui sono state sollevate le questioni, con conseguente impossibilità di valutarne la rilevanza nel giudizio a quo;
 che il giudice remittente avrebbe potuto, peraltro, superare il dubbio di costituzionalità attraverso un'interpretazione della normativa nel senso di ritenere necessaria la restituzione degli atti al pubblico ministero, tenuto conto anche della circostanza che nell'avviso di conclusioni delle indagini preliminari, ex <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_415bis">art. 415-bis cod. proc. pen.</ref>, il pubblico ministero nel giudizio a quo aveva effettivamente formulato tale avviso.
 Considerato che il Tribunale ordinario di Bergamo, con ordinanza del 21 dicembre 2017, pervenuta a questa Corte il 27 marzo 2018 (r. o. n. 60 del 2018), ha sollevato, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">art. 24 della Costituzione</ref>, questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">art. 456 del codice di procedura penale</ref>, nella parte in cui non prevede che il decreto di giudizio immediato debba contenere l'avviso della facoltà dell'imputato di chiedere la sospensione del processo con messa alla prova, con la forma e i termini di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_458">art. 458 cod. proc. pen.</ref>;
 che il Tribunale ordinario di Bari, con ordinanza del 3 aprile 2017, pervenuta a questa Corte il 24 aprile 2018 (r. o. n. 73 del 2018), ha sollevato, in riferimento agli articoli 3, 24, secondo comma, e 111 Cost., questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_552__para_1__point_f">art. 552, comma 1, lettera f), cod. proc. pen.</ref>, nella parte in cui non prevede l'avviso che, qualora ne ricorrano i presupposti, l'imputato, fino alla dichiarazione di apertura del dibattimento di primo grado, può formulare la richiesta di sospensione del procedimento con messa alla prova, ai sensi degli artt. 168-bis e seguenti del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref> e 464-bis e seguenti <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main">cod. proc. pen</ref>.;
 che le questioni sollevate dal Tribunale ordinario di Bergamo e dal Tribunale ordinario di Bari, pur vertendo su disposizioni diverse, lamentano entrambe la mancata previsione dell'obbligatorietà dell'avviso della facoltà dell'imputato di chiedere la sospensione del procedimento con messa alla prova, rispettivamente nel decreto di giudizio immediato e nel decreto di citazione diretta a giudizio, di talché meritano di essere esaminate congiuntamente e decise con unica pronuncia;
 che l'ordinanza del Tribunale ordinario di Bergamo non contiene alcuna descrizione dei fatti oggetto del giudizio a quo, limitandosi a indicare la disposizione che prevede il reato contestato all'imputato, senza neppure riportare il relativo capo di imputazione;
 che già sotto questo aspetto, come correttamente eccepito dall'Avvocatura generale dello Stato, la questione deve essere ritenuta manifestamente inammissibile per omessa descrizione della fattispecie concreta e conseguente difetto di motivazione sulla rilevanza (ex mult<mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2018/85/!main">is, ordinanze n. 85</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2018/7/!main">7 del 2018</ref></mref>, n. 210 e n. 46 del 2017);
 che, inoltre, tra i reati contestati vi è quello previsto dall'<ref href="/akn/it/act/decretoLegislativo/stato/2000-03-10/74/!main#art_10">art. 10 del decreto legislativo 10 marzo 2000, n. 74</ref> (Nuova disciplina dei reati in materia di imposte sui redditi e sul valore aggiunto, a norma dell'<ref href="/akn/it/act/legge/stato/1999-06-25/205/!main#art_9">articolo 9 della legge 25 giugno 1999, n. 205</ref>), punito con la reclusione fino a sei anni, dunque oltre il limite edittale entro cui è ammessa, ai sensi dell'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_168bis">art. 168-bis del codice penale</ref>, la messa alla prova;
 che, come già rammentato da questa Corte nell'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2018/85/!main">ordinanza n. 85 del 2018</ref>, la Corte di cassazione ha escluso che, in tema di sospensione con messa alla prova, la sospensione possa essere disposta, previa separazione dei processi, soltanto per alcuni dei reati contestati per i quali sia possibile l'accesso al beneficio, in quanto la messa alla prova tende alla eliminazione completa delle tendenze antisociali del reo e sarebbe incompatibile con le finalità dell'istituto una rieducazione parziale (<ref href="/akn/it/judgment/sentenza/corteDiCassazione/2015-03-02/1PEN/!main">Corte di cassazione, sezione seconda penale, sentenza 12 marzo 2015</ref>, n. 14112);
 che da ciò discende un'ulteriore ragione di manifesta inammissibilità della questione prospettata dal Tribunale ordinario di Bergamo, non potendo in ogni caso l'imputato del procedimento a quo beneficiare della sospensione del processo con messa alla prova;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che anche le questioni sollevate dal Tribunale ordinario di Bari sono manifestamente inammissibili, come correttamente eccepito dall'Avvocatura generale dello Stato;
 che, infatti, anche in questo caso l'ordinanza di rimessione non contiene alcuna descrizione dei fatti oggetto del giudizio a quo, limitandosi a indicare la disposizione che prevede il reato contestato agli imputati, senza neppure riportare il relativo capo di imputazione;
 che, inoltre, l'ordinanza di rimessione non ha specificato se nell'udienza in cui sono state sollevate le questioni di legittimità costituzionale fosse già stata dichiarata l'apertura del dibattimento e se gli imputati avessero manifestato la volontà di richiedere la sospensione del procedimento con messa alla prova;
 che, come già rilevato da questa Corte nell'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2018/7/!main">ordinanza n. 7 del 2018</ref>, ai sensi dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_182__para_1">art. 182, comma 1, cod. proc. pen.</ref>, la nullità del decreto di citazione a giudizio non può essere eccepita da chi non ha interesse all'osservanza della disposizione violata;
 che il rimettente avrebbe dunque dovuto precisare se si fosse già verificata l'apertura del dibattimento, il che avrebbe precluso agli imputati la possibilità di chiedere la sospensione del procedimento con messa alla prova;
 che solo l'imputato nei cui confronti si sia verificata la preclusione conseguente all'apertura del dibattimento, e che abbia l'intenzione di chiedere la sospensione del procedimento con messa alla prova, può aver interesse alla declaratoria di nullità del decreto di citazione a giudizio che non contenga l'avvertimento relativo a tale facoltà;
 che l'insufficiente descrizione della fattispecie processuale, e in particolare dello stato in cui si trovava il giudizio, impedisce il necessario controllo in punto di rilevanza e rende le questioni manifestamente inammissibili (ex mult<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2018/7/!main">is, ordinanze n. 7 del 2018</ref>, n. 210 del 2017 e n. 237 del 2016).
 Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 riuniti i giudizi,
 1) dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_456">art. 456 del codice di procedura penale</ref>, sollevata, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">art. 24 della Costituzione</ref>, dal Tribunale ordinario di Bergamo con l'ordinanza indicata in epigrafe;
 2) dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_552__para_1__point_f">art. 552, comma 1, lettera f), cod. proc. pen.</ref>, sollevate, in riferimento agli artt. 3, 24, secondo comma, e 111 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, dal Tribunale ordinario di Bari con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2019-02-20">20 febbraio 2019</date>.
 F.to:
 <signature><judge refersTo="#giorgioLattanzi" as="#presidente">Giorgio LATTANZI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#francescoViganò" as="#relatore">Francesco VIGANÒ</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2019-03-29">29 marzo 2019</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
