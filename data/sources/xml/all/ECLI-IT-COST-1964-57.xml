<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1964</anno_pronuncia>
    <numero_pronuncia>57</numero_pronuncia>
    <ecli>ECLI:IT:COST:1964:57</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/06/1964</data_decisione>
    <data_deposito>23/06/1964</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 2, comma  &#13;
 terzo, della legge 26 aprile  1959,  n.  207,  promosso  con  ordinanza  &#13;
 emessa  il  2  ottobre  1963  dal Tribunale di Isernia nel procedimento  &#13;
 penale a carico di Di Claudio Armando, iscritta al n. 197 del  Registro  &#13;
 ordinanze  1963 e pubblicata nella Gazzetta Ufficiale della Repubblica,  &#13;
 n. 299 del 16 novembre 1963.                                             &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udita  nell'udienza  pubblica  del  3  giugno 1964 la relazione del  &#13;
 Giudice Costantino Mortati;                                              &#13;
     udito  il  sostituto  avvocato  generale   dello   Stato   Giuseppe  &#13;
 Guglielmi, per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza del 2 ottobre 1963 il Tribunale di Isernia, nel corso  &#13;
 di  procedimento  penale  in grado di appello contro Di Claudio Armando  &#13;
 imputato di contravvenzione all'art. 80 del T. U. 15  giugno  1959,  n.  &#13;
 393, sulla circolazione stradale, in accoglimento di eccezione proposta  &#13;
 dalla   difesa   di   costui,   sollevava   questione  di  legittimità  &#13;
 costituzionale, nella considerazione che il T.  U.  predetto  è  stato  &#13;
 emesso  in  virtù di delegazione conferita con l'art. 2 della legge 26  &#13;
 aprile 1959, n. 207,  la  quale,  in  contrasto  con  l'art.  76  della  &#13;
 Costituzione,  non conteneva alcuna indicazione di limiti temporali per  &#13;
 l'esercizio della medesima.                                              &#13;
     L'ordinanza, regolarmente notificata  e  comunicata  a  termini  di  &#13;
 legge,  è  stata  pubblicata  nella  Gazzetta Ufficiale, n. 299 del 16  &#13;
 novembre 1963.                                                           &#13;
     Nel giudizio avanti a questa Corte si è costituito  il  Presidente  &#13;
 del  Consiglio  dei  Ministri,  rappresentato  dall'Avvocatura generale  &#13;
 dello Stato, con deduzioni depositate il 25 ottobre 1963.  In  esse  si  &#13;
 osserva che l'art. 2 della legge n. 207 del 1959 non ha conferito alcun  &#13;
 potere di delegazione legislativa, essendosi limitato ad autorizzare il  &#13;
 Governo  all'esercizio  del normale potere di raccolta, coordinamento e  &#13;
 compilazione  in testo unico delle norme di legge vigenti in materia, e  &#13;
 pertanto  non  poteva  trovar  luogo  la  prefissione  di  termine  per  &#13;
 l'esercizio  del  potere prescritto dall'art. 76 della Costituzione. In  &#13;
 conseguenza  chiede  sia  dichiarata  l'infondatezza  della   questione  &#13;
 sollevata.                                                               &#13;
     Con  successiva  memoria,  in  data 11 maggio 1964, l'Avvocatura si  &#13;
 richiama alla distinzione accolta dalla Corte con la sentenza n. 54 del  &#13;
 1957 fra testi unici per la cui emanazione si rende necessaria apposita  &#13;
 delegazione e quelli per i quali basta la  semplice  autorizzazione  e,  &#13;
 riaffermato  che  quello  in  esame  rientra  nella  seconda  di  dette  &#13;
 categorie, insiste nelle conclusioni già prese.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  La  questione  sollevata  dal  Tribunale  di  Isernia  muove  &#13;
 evidentemente  dall'accoglimento  dell'opinione, rappresentata da larga  &#13;
 parte della dottrina, secondo la quale ogni specie di  testo  unico  la  &#13;
 cui  formazione  sia  avvenuta  in  virtù  di  apposita autorizzazione  &#13;
 legislativa, viene ad essere necessariamente rivestito della  forza  di  &#13;
 legge,  quale  che  sia  l'entità  degli  adattamenti e variazioni del  &#13;
 tenore originario delle norme da unificare, che ogni attività  rivolta  &#13;
 a  tale  unificazione  di  per sé implica, e quindi rimane subordinato  &#13;
 alle condizioni poste dalla Costituzione per la valida emanazione degli  &#13;
 atti governativi forniti di tale efficacia.                              &#13;
 La Corte con sue precedenti pronuncie (sentenze nn. 54 del  1957  e  24  &#13;
 del  1961)  ha  ritenuto  che tale orientamento dottrinale non fosse da  &#13;
 accogliere, e che invece forza di legge possono venire ad assumere solo  &#13;
 quelli fra i testi unici i quali non si limitino  ad  operare  un  mero  &#13;
 coordinamento  fra le norme da riunire, ma siano abilitati ad apportare  &#13;
 innovazioni o integrazioni alle norme stesse.                            &#13;
 La Corte non rinviene motivi che la inducono a  discostarsi  dalla  sua  &#13;
 costante  giurisprudenza,  tanto  più  in  presenza  di un testo, come  &#13;
 quello denunciato, che si è limitato alla  materiale  riproduzione  in  &#13;
 ogni  loro  parte,  delle  disposizioni del decreto delegato 27 ottobre  &#13;
 1958, n. 956, e pedissequamente delle modifiche di cui all'art. 1 della  &#13;
 legge 26  aprile  1959,  n.  207,  e  quindi  non  ha  reso  necessario  &#13;
 l'esercizio  neppure  di  quel  minimo di attività interpretativa, che  &#13;
 appare inseparabile dall'operazione del coordinamento.                   &#13;
     La rilevata mancanza della forza di legge dell'atto  denunciato  ha  &#13;
 per conseguenza l'inammissibilità della questione sollevata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 sollevata nei confronti dell'art. 2, comma terzo, della legge 26 aprile  &#13;
 1959,  n.  207,  in  relazione  all'art.    134,  primo  comma,   della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 9 giugno 1964.                                &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO - ANTONINO  PAPALDO  -  NICOLA  &#13;
                                   JAEGER  - GIOVANNI CASSANDRO - BIAGIO  &#13;
                                   PETROCELLI -  ANTONIO  MANCA  -  ALDO  &#13;
                                   SANDULLI  - GIUSEPPE BRANCA - MICHELE  &#13;
                                   FRAGALI  -   COSTANTINO   MORTATI   -  &#13;
                                   GIUSEPPE  CHIARELLI - GIUSEPPE  VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
