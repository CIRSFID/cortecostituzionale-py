<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>40</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:40</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/02/1986</data_decisione>
    <data_deposito>03/03/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - &#13;
 Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - &#13;
 Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO &#13;
 GRECO - Prof. RENATO DELL'ANDRO - Prof. GABRIELE PESCATORE, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, lett. b,  &#13;
 e  4  del  d.P.R.  31  marzo  1971,  n.  276 ("Assunzioni temporanee di  &#13;
 personale  presso  le  Amministrazioni  dello  Stato"),  promosso   con  &#13;
 ordinanza  emessa  il 29 marzo 1978 dal Pretore di La Spezia su ricorso  &#13;
 proposto da Fabbrini Federica ed altri contro Ministero delle  Finanze,  &#13;
 iscritta  al  n.    444  del registro ordinanze 1978 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica dell'anno 1978;                      &#13;
     visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del 4 febbraio 1986 il Giudice  &#13;
 relatore Livio Paladin.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Assunti  per  un  periodo  determinato,  con  scadenza  al  31  &#13;
 dicembre  1977,  e riassunti per ulteriori novanta giorni, con scadenza  &#13;
 al 31 marzo 1978, alcuni dipendenti dell'Amministrazione delle  finanze  &#13;
 hanno  proposto  ricorso al Pretore di La Spezia ai sensi dell'art. 700  &#13;
 cod.  proc.  civ.,  sostenendo  che   la   nuova   assunzione   dovesse  &#13;
 considerarsi  illegittima  e  che  nei loro confronti trovassero quindi  &#13;
 applicazione le  disposizioni  della  legge  n.  230  del  1962,  sulla  &#13;
 conversione del rapporto di lavoro a tempo indeterminato.                &#13;
     Ritenuta  la  propria  competenza,  il  Pretore ha invece sollevato  &#13;
 questione  di  legittimità  costituzionale  degli  artt.  1  lett.  b,  &#13;
 limitatamente  alla  frase  "al  compimento  dei  quali  il rapporto è  &#13;
 risolto di diritto", e 4 del d.P.R. 31 marzo 1971, n. 276. Il giudice a  &#13;
 quo rileva infatti che la pretesa dei ricorrenti a  veder  riconosciuta  &#13;
 l'esistenza  di  un rapporto di lavoro a tempo indeterminato troverebbe  &#13;
 ostacolo nelle norme impugnate, le quali dispongono la  risoluzione  di  &#13;
 diritto  dei  contratti  d'impiego  allo  scadere  di novanta giorni di  &#13;
 servizio, anche non continuativo, nell'anno solare, e la nullità delle  &#13;
 assunzioni temporanee effettuate in violazione della diciplina  dettata  &#13;
 dagli  artt.  1-3  del  predetto decreto presidenziale. Tale disciplina  &#13;
 sarebbe  sospetta  d'illegittimità   costituzionale   in   riferimento  &#13;
 all'art. 3 Cost., per la disparità di trattamento rispetto al rapporto  &#13;
 di   lavoro  a  termine  di  diritto  privato,  nell'ambito  del  quale  &#13;
 l'eventuale violazione dei limiti posti dalla  legge  sulle  assunzioni  &#13;
 conduce,  invece  che  alla nullità, alla conversione del contratto in  &#13;
 rapporto di lavoro a tempo indeterminato;  in  riferimento  all'art.  4  &#13;
 Cost.,   in   quanto   il  principio  del  diritto  al  lavoro  mal  si  &#13;
 concilierebbe con previsioni Così drastiche come quelle in  esame;  in  &#13;
 riferimento all'art. 35 Cost., perché le previsioni stesse urterebbero  &#13;
 "contro  il  principio  di  tutela  del lavoro, assunto come valore, in  &#13;
 tutte le sue forme"; ed in riferimento all'art. 97 Cost., giacché  "le  &#13;
 esigenze  di  tutela  della  Pubblica Amministrazione contro assunzioni  &#13;
 indiscriminate vanno soddisfatte a monte del contratto a termine e  non  &#13;
 possono essere addotte per legittimare una situazione in cui il ricorso  &#13;
 a tale forma di contratto sia divenuto sistematico".                     &#13;
     2.  -  L'intervenuto  Presidente del Consiglio dei ministri osserva  &#13;
 anzitutto che la proposta questione non  sarebbe  rilevante  o,  quanto  &#13;
 meno,  che  il  giudice  a  quo  non avrebbe adeguatamente motivato sul  &#13;
 punto. L'eventuale annullamento delle nonne  impugnate  non  renderebbe  &#13;
 applicabile la legge n. 230 del 1962, dal momento che nella fattispecie  &#13;
 non  era  ancora  scaduto il termine del rapporto, prorogato fino al 31  &#13;
 marzo 1978, in virtù di un parere del Consiglio di Stato.               &#13;
     Nel  merito,  comunque,  la   questione   sarebbe   infondata.   La  &#13;
 tendenziale  convergenza fra lo stato giuridico dei dipendenti pubblici  &#13;
 e di quelli privati non escluderebbe, infatti, le innegabili differenze  &#13;
 che ancora intercorrono tra le  due  forme  di  impiego  (Corte  cost.,  &#13;
 sentt.  nn. 209/1975, 49/1976, 118/1976, 43/1977). La ratio delle norme  &#13;
 impugnate andrebbe cioè ravvisata nella regola  dell'accesso  mediante  &#13;
 concorso  alle pubbliche amministrazioni: cui non si potrebbe derogare,  &#13;
 nella specie, se non in considerazione di esigenze straordinarie e  nei  &#13;
 previsti limiti di durata del rapporto.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Preliminarmente,  va respinta l'eccezione d'inammissibilità  &#13;
 promossa dall'Avvocatura dello Stato in base al rilievo che il  Pretore  &#13;
 di  La  Spezia  non  potrebbe  comunque  fare  applicazione delle norme  &#13;
 invocate dai ricorrenti, ossia della legge  18  aprile  1962,  n.  230,  &#13;
 sulla   disciplina   del  contratto  di  lavoro  a  tempo  determinato.  &#13;
 Affinché sussista la rilevanza di  una  impugnazione  incidentale,  è  &#13;
 infatti  sufficiente  che  la questione sollevata investa la norma o le  &#13;
 norme applicabili -  allo  stato  attuale  dell'ordinamento  -  per  la  &#13;
 risoluzione del giudizio a quo.                                          &#13;
     Né  la  conclusione  può  mutare,  d'altra  parte, in vista della  &#13;
 circostanza che il Pretore di La Spezia è stato adito  ex  art.    700  &#13;
 cod.  proc.  civ., mediante una serie di ricorsi proposti nei confronti  &#13;
 del Ministero delle finanze. Sul punto, infatti, il giudice  a  quo  ha  &#13;
 motivato  ampiamente.  E  la sentenza n.   190 del 1985, con cui questa  &#13;
 Corte ha riconosciuto al giudice amministrativo la potestà di adottare  &#13;
 provvedimenti d'urgenza nelle controversie patrimoniali in  materia  di  &#13;
 pubblico  impiego,  non basta a far ritenere inammissibile la questione  &#13;
 in esame, sebbene prospettata ad opera di un giudice civile.             &#13;
     2. -  Nondimeno,  l'impugnativa  non  si  dimostra  fondata,  sotto  &#13;
 nessuno degli aspetti sui quali ha insistito l'ordinanza di rimessione.  &#13;
     La giurisprudenza di questa Corte è tuttora costante nel senso che  &#13;
 non  vale  far  richiamo,  per  ipotizzare  illegittime  disparità  di  &#13;
 trattamento,  al  solo  dato  della  "tendenziale  convergenza"   delle  &#13;
 discipline rispettivamente riguardanti il lavoro privato ed il pubblico  &#13;
 impiego.  Certo  è  che tale convergenza s'è ulteriormente accentuata  &#13;
 negli ultimi anni, soprattutto in virtù della "legge quadro" 29  marzo  &#13;
 1983,   n.     93.  Ma,  anche  nel  periodo  successivo  all'emissione  &#13;
 dell'ordinanza predetta, la Corte ha messo in luce le  "differenze  che  &#13;
 tuttora  intercorrono  tra  impiego  privato  ed  impiego pubblico" (si  &#13;
 vedano le sentt. nn. 68 del 1980, 193 del 1981, 46  del  1983,  90  del  &#13;
 1984).                                                                   &#13;
     Ciò  che  più  conta,  nell'ambito  del  particolare  ordinamento  &#13;
 dell'impiego pubblico l'impugnato  d.P.R.  31  marzo  1971,  n.    276,  &#13;
 presenta  un  carattere spiccatamente derogatorio: come risulta sia dal  &#13;
 suo preambolo, là dove si sottolinea la temporaneità delle assunzioni  &#13;
 delle quali si tratta, in quanto destinate a  soddisfare  "esigenze  di  &#13;
 carattere  eccezionale",  sia  dalla  lett.  a  dell'art. 1, con cui si  &#13;
 ribadisce che "le assunzioni temporanee devono essere  giustificate  da  &#13;
 esigenze  indilazionabili  e  determinate  nella  durata". Il che rende  &#13;
 ancor meno sostenibile la  richiesta  del  giudice  a  quo,  intesa  ad  &#13;
 allargare  di  molto la portata di tale disciplina, alterando la stessa  &#13;
 natura dei rapporti da essa regolati, in vista  di  una  legge  di  cui  &#13;
 generalmente  la  dottrina  disconosce  la  presente  adeguatezza, come  &#13;
 quella recante il n. 230 del 1962.                                       &#13;
     Si consideri ancora che l'invocato art.  97  Cost.,  non  solo  non  &#13;
 concorre  a  sorreggere la proposta censura, ma anzi fornisce argomenti  &#13;
 per respingerla. Da un lato, lo stesso Pretore di La  Spezia  individua  &#13;
 esattamente la ragion d'essere del contestato art. 1, primo comma lett.  &#13;
 b,   sulla  risoluzione  di  diritto  del  rapporto  allo  scadere  del  &#13;
 prescritto termine, nell'opportunità di porre "un freno ad  assunzioni  &#13;
 senza  concorso  indiscriminante, clientelari, destinate a trasformarsi  &#13;
 in assunzioni a tempo indeterminato con  pregiudizio  per  la  Pubblica  &#13;
 Amministrazione   e   l'Erario".      D'altro   lato,  diversamente  ne  &#13;
 deriverebbero situazioni  di  privilegio  e  violazioni  del  principio  &#13;
 d'imparzialità,  in  netto  contrasto con quel sistema di elenchi e di  &#13;
 precedenze dei vari aspiranti, che è stato congegnato dall'art. 3  del  &#13;
 d.P.R.  n. 276, ai fini delle nuove assunzioni straordinarie.            &#13;
     Ciò   basta  ad  imporre  il  rigetto  dell'impugnativa.  E  nulla  &#13;
 aggiunge, a sostegno della conclusione opposta,  il  generico  richiamo  &#13;
 agli artt. 4 e 35 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 1, primo comma  lett.  b,  limitatamente  alle  parole  "al  &#13;
 compimento dei quali il rapporto è risolto di diritto", e 4 del d.P.R.  &#13;
 31  marzo  1971,  n. 276 ("Assunzioni temporanee di personale presso le  &#13;
 Amministrazioni dello Stato"), sollevata dal Pretore di La Spezia -  in  &#13;
 riferimento  agli  artt.  3,  4,  35  e  97  della  Costituzione  - con  &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 26 febbraio 1986.       &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO -  &#13;
                                   GABRIELE PESCATORE.                    &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
