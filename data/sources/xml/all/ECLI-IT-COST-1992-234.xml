<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>234</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:234</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/05/1992</data_decisione>
    <data_deposito>27/05/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 623  codice  di    &#13;
 procedura  civile  promosso  con ordinanza emessa il 27 novembre 1991    &#13;
 dal giudice istruttore presso il tribunale di Asti  nel  procedimento    &#13;
 civile  vertente tra Trifone Gino e S.a.s. "SIBO di Avallone Angelo e    &#13;
 C."  iscritta  al n. 7 del registro ordinanze 1992 e pubblicata nella    &#13;
 Gazzetta Ufficiale della  Repubblica  n.  5,  prima  serie  speciale,    &#13;
 dell'anno 1992;                                                          &#13;
    Udito  nell'udienza pubblica del 31 marzo 1992 il Giudice relatore    &#13;
 Renato Granata;                                                          &#13;
    Udito l'avv. Sergio Cersosimo per la  S.a.s.  "SIBO2  DI  Avallone    &#13;
 Angelo e C.";</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ordinanza  del  27 novembre 1991 il giudice istruttore    &#13;
 presso il tribunale di Asti, designato nella causa di  opposizione  a    &#13;
 precetto pendente tra Trifone Gino e la società SIBO S.a.s., dovendo    &#13;
 pronunciarsi  sull'istanza  di  quest'ultima  diretta  ad ottenere la    &#13;
 revoca dell'ordinanza di sospensione  dell'esecuzione,  ha  sollevato    &#13;
 questione  incidentale  di  legittimità costituzionale dell'art. 623    &#13;
 c.p.c. - in riferimento agli artt. 3 e 24 della Costituzione -  nella    &#13;
 parte  in  cui  non  prevede  che  il  giudice  adito  nella causa di    &#13;
 opposizione a precetto in relazione  ad  esecuzione  per  consegna  o    &#13;
 rilascio   possa,   prima   dell'inizio  dell'esecuzione,  sospendere    &#13;
 l'esecuzione medesima.                                                   &#13;
    In particolare il giudice  rimettente  premette  che  la  società    &#13;
 SIBO,  aggiudicataria  all'asta  pubblica  di  un immobile in sede di    &#13;
 esecuzione immobiliare, aveva agito in via esecutiva  azionando  come    &#13;
 titolo    il    decreto   di   trasferimento   emesso   dal   giudice    &#13;
 dell'esecuzione. Non  avendo  l'ufficiale  giudiziario  proceduto  al    &#13;
 rilascio  in sede di primo accesso perché l'immobile era detenuto da    &#13;
 terzi, tra cui il Trifone, la società - dopo aver ottenuto dal  Pretore  di  Asti  un  decreto  con  cui veniva disposta la prosecuzione    &#13;
 dell'esecuzione nei confronti  di  chiunque  occupasse  l'immobile  -    &#13;
 notificava  un  nuovo  atto  di  precetto  al Trifone per il rilascio    &#13;
 dell'immobile. Quest'ultimo proponeva opposizione a precetto  innanzi    &#13;
 al Pretore di Asti, chiedendo la sospensione dell'esecuzione, che gli    &#13;
 veniva  negata  dal  pretore  adito,  ma successivamente concessa dal    &#13;
 presidente del tribunale di Asti, al quale la causa era stata rimessa    &#13;
 per competenza  per  valore.  Designato  il  giudice  istruttore,  la    &#13;
 società   chiedeva   la  revoca  della  sospensione  dell'esecuzione    &#13;
 sostenendo, tra l'altro, che il giudice dell'opposizione a  precetto,    &#13;
 non  essendo  "giudice  davanti  al  quale  è  impugnato  il  titolo    &#13;
 esecutivo", (art. 623 c.p.c.),  non  ha  la  facoltà  di  sospendere    &#13;
 l'esecuzione.                                                            &#13;
    Il   giudice   istruttore  rimettente  -  dopo  aver  escluso  che    &#13;
 l'esecuzione potesse considerarsi già  iniziata  nei  confronti  del    &#13;
 Trifone  - rileva che, secondo la costante giurisprudenza della Corte    &#13;
 di cassazione,  il  giudice  dell'opposizione  a  precetto  non  può    &#13;
 sospendere  l'esecuzione, giacché l'inciso del primo comma dell'art.    &#13;
 623  c.p.c.  ,  che,  nell'attribuire   il   potere   di   sospendere    &#13;
 l'esecuzione al giudice dell'esecuzione, fa salva la possibilità che    &#13;
 tale  sospensione  sia  disposta  dal  giudice  innanzi  al  quale è    &#13;
 impugnato il titolo esecutivo,  si  riferisce  soltanto  ai  casi  di    &#13;
 impugnazioni  in  senso  proprio  (appello,  ricorso  per cassazione,    &#13;
 revocazione,  opposizione  di  terzo,  impugnazione  della   sentenza    &#13;
 arbitrale),  con  esclusione  dell'opposizione  a precetto. Quindi la    &#13;
 sospensione (erroneamente) disposta  sarebbe  da  revocare.  Ma  tale    &#13;
 ritenuta  impossibilità  di  sospendere  l'esecuzione  prima del suo    &#13;
 inizio  quando l'opposizione a precetto sia fondata su titolo diverso    &#13;
 dalla sentenza esecutiva impugnabile si presenta - secondo il giudice    &#13;
 rimettente  -  ingiustificatamente  penalizzante  per   il   debitore    &#13;
 esecutato  (e  quindi  in contrasto con l'art. 24 della Costituzione)    &#13;
 soprattutto nel caso dell'esecuzione  per  consegna  o  rilascio  che    &#13;
 avviene,  o  può  avvenire,  uno actu con l'attività dell'ufficiale    &#13;
 giudiziario di  consegna  della  cosa  mobile  o  di  immissione  nel    &#13;
 possesso   dell'immobile,  sicché  il  debitore,  il  quale  lamenti    &#13;
 l'illegittimità del titolo  esecutivo  e  contesti  il  diritto  del    &#13;
 creditore a procedere, è del tutto sprovvisto della tutela cautelare    &#13;
 costituita  dalla facoltà di chiedere la sospensione dell'esecuzione    &#13;
 senza che la  specialità  di  tale  esecuzione  diretta  giustifichi    &#13;
 questa limitazione di tutela.                                            &#13;
    Il giudice rimettente ravvisa inoltre un'ingiustificata disparità    &#13;
 di  trattamento  tra  l'esecutato  che,  dopo  un  primo  infruttuoso    &#13;
 accesso, può chiedere la sospensione dell'esecuzione  e  l'esecutato    &#13;
 che,  per una mera ed accidentale situazione di fatto, veda esaurirsi    &#13;
 l'esecuzione al primo atto dell'ufficiale giudiziario.                   &#13;
    2.  -  Si  è  costituita  la  società  SIBO  sostenendo  in  via    &#13;
 preliminare  l'inammissibilità  della questione di costituzionalità    &#13;
 sia  per  irritualità  dell'ordinanza  di  rimessione   in   ragione    &#13;
 dell'incertezza  nell'identificazione dell'organo rimettente, essendo    &#13;
 riferita  al  tribunale  di  Asti,  ma   sottoscritta   dal   giudice    &#13;
 istruttore; sia per difetto di rilevanza per essere l'esecuzione già    &#13;
 iniziata,  avendo ormai l'ufficiale giudiziario proceduto ad un primo    &#13;
 accesso nell'immobile, ed avendo comunque il pretore  autorizzato  la    &#13;
 prosecuzione dell'esecuzione nei confronti del Trifone.                  &#13;
    Nel  merito  la  società  SIBO sostiene che non vi è un indebito    &#13;
 sacrificio del diritto di difesa, né  violazione  del  principio  di    &#13;
 eguaglianza,   atteso   che   i   titoli   esecutivi   che  abilitano    &#13;
 all'esecuzione  per  il   rilascio   rivestono   caratteristiche   di    &#13;
 particolare  certezza  e  stabilità,  che  giustificano una speciale    &#13;
 tutela. Né d'altra parte la garanzia del diritto di  difesa  di  cui    &#13;
 all'art.  24  della  Costituzione  copre  sempre e comunque la tutela    &#13;
 cautelare.<diritto>Considerato in diritto</diritto>1. - È stata sollevata questione incidentale di costituzionalità    &#13;
 - in riferimento agli artt. 3 e 24 della Costituzione - dell'art. 623    &#13;
 c.p.c. nella parte in cui non prevede  che  il  giudice  adito  nella    &#13;
 causa  di  opposizione  al  precetto  in  relazione ad esecuzione per    &#13;
 consegna  o  rilascio  possa,  prima   dell'inizio   dell'esecuzione,    &#13;
 sospendere  l'esecuzione  medesima.  Ritiene  il  giudice  rimettente    &#13;
 violati sia il diritto di difesa in  giudizio  (perché  il  soggetto    &#13;
 esecutato,  che con l'opposizione a precetto lamenti essere il titolo    &#13;
 esecutivo inidoneo a legittimare l'esecuzione, non può domandare  la    &#13;
 sospensione   dell'esecuzione   stessa,   non  ancora  iniziata,  con    &#13;
 conseguente  definitiva  negazione  di  tale  tutela  cautelare   ove    &#13;
 l'esecuzione,  successivamente  iniziata con l'accesso dell'ufficiale    &#13;
 giudiziario, si perfezioni uno actu), sia il principio di parità  di    &#13;
 trattamento  (perché  si fa dipendere la possibilità per i soggetti    &#13;
 esecutati  di  accedere  alla  tutela  cautelare  della   sospensione    &#13;
 dell'esecuzione   dal   fatto,   meramente   contingente   e   quindi    &#13;
 ingiustificatamente discriminatorio, che l'esecuzione  si  compia,  o    &#13;
 meno, uno actu).                                                         &#13;
    2. - Va preliminarmente rilevato che il giudice rimettente ritiene    &#13;
 che  la  sua  decisione,  quale  giudice  istruttore,  dipende  dalla    &#13;
 sussistenza,  o  meno,  del  potere  di  concedere   la   sospensione    &#13;
 dell'esecuzione  da  parte  del  tribunale  e  quindi  si duole della    &#13;
 ritenuta illegittimità costituzionale  dell'art.  623  c.p.c.  nella    &#13;
 parte  in  cui tale potere - secondo la giurisprudenza della Corte di    &#13;
 cassazione - non è previsto; invoca pertanto una  sentenza  additiva    &#13;
 che  tale  potere attribuisca al giudice dell'opposizione a precetto.    &#13;
 Risulta però in tal modo una  prospettazione  perplessa  ed  ambigua    &#13;
 perché  non  è  dato  comprendere  se  l'attribuzione del potere di    &#13;
 sospendere l'esecuzione,  che  -  secondo  il  giudice  rimettente  -    &#13;
 ricondurrebbe   la   norma   censurata  a  conformità  ai  parametri    &#13;
 costituzionali  invocati,  sia  da  riferire,  in  caso   di   organo    &#13;
 collegiale, al giudice istruttore o al collegio; nel qual ultimo caso    &#13;
 il   giudice  istruttore  non  sarebbe  legittimato  a  sollevare  la    &#13;
 questione di costituzionalità de qua. Né può farsi ricorso  ad  un    &#13;
 canone   generale  che  attribuisca  sempre  e  comunque  al  giudice    &#13;
 istruttore tale potere di sospensione, atteso che una regola siffatta    &#13;
 non è rinvenibile nel vigente ordinamento  processuale.  Ed  infatti    &#13;
 mentre  è  previsto  che  sia  il  giudice  istruttore  a provvedere    &#13;
 sull'istanza di sospensione dell'esecuzione nel giudizio  di  appello    &#13;
 (art.   351  c.p.c.)  ed  in  quello  di  opposizione  a  decreto  di    &#13;
 ingiunzione  (art.  649  c.p.c.),  è  in  altri  casi  richiesta  la    &#13;
 decisione  collegiale;  così  in pendenza del giudizio di cassazione    &#13;
 (art. 373, secondo comma, c.p.c.), di revocazione (art. 401  c.p.c.),    &#13;
 di  opposizione  di  terzo  (art.  407 c.p.c.), di impugnazione della    &#13;
 sentenza arbitrale (art. 830, secondo comma, c.p.c.).                    &#13;
    Tale mancata puntualizzazione da parte del giudice rimettente  non    &#13;
 consente  la  verifica  della  rilevanza della sollevata questione di    &#13;
 costituzionalità e conseguentemente  è  causa  di  inammissibilità    &#13;
 della stessa.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 623 codice procedura civile, in riferimento agli artt. 3  e    &#13;
 24  della  Costituzione,  sollevata  dal giudice istruttore presso il    &#13;
 Tribunale di Asti con l'ordinanza in epigrafe.                           &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 maggio 1992.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: GRANATA                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 27 maggio 1992.                          &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
