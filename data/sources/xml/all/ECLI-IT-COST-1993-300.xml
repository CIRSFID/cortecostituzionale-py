<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>300</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:300</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1993</data_decisione>
    <data_deposito>01/07/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, &#13;
 prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, &#13;
 dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco &#13;
 GUIZZI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 12-  bis  della    &#13;
 legge  1° dicembre 1970, n. 898, (Disciplina dei casi di scioglimento    &#13;
 del matrimonio) introdotto dall'art. 16 della legge 6 marzo 1987,  n.    &#13;
 74  (Nuove  norme  sulla  disciplina  dei  casi  di  scioglimento  di    &#13;
 matrimonio), promosso con ordinanza emessa  il  24  giugno  1992  dal    &#13;
 Tribunale  di  Siena  nei  procedimenti  civili  riuniti vertenti tra    &#13;
 Ancillotti Lucia e Dari Enzo e  da  Dari  Enzo  e  Ancillotti  Lucia,    &#13;
 iscritta  al  n.  59  del  registro ordinanze 1993 e pubblicata nella    &#13;
 Gazzetta Ufficiale della  Repubblica  n.  9,  prima  serie  speciale,    &#13;
 dell'anno 1993;                                                          &#13;
    Visti  l'atto  di  costituzione  di  Dari  Enzo  nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito nella camera di consiglio  del  5  maggio  1993  il  Giudice    &#13;
 relatore Ugo Spagnoli;                                                   &#13;
    Ritenuto  che  con  ordinanza  del  24 giugno 1992, pervenuta alla    &#13;
 Corte il 3 febbraio 1993, il Tribunale di Siena ha proposto questione    &#13;
 di legittimità costituzionale, per contrasto con gli articoli 3 e 36    &#13;
 della Costituzione, dell'articolo 12- bis  della  legge  1°  dicembre    &#13;
 1970 n. 898, introdotto dall'articolo 17 recte 16 della legge 6 marzo    &#13;
 1987 n. 74, secondo cui il coniuge divorziato, se non passato a nuove    &#13;
 nozze  ed in quanto titolare dell'assegno divorzile, ha diritto al 40    &#13;
 per cento  dell'indennità  di  fine  rapporto  percepita  dall'altro    &#13;
 coniuge,  per la parte di tale indennità riferibile agli anni in cui    &#13;
 il rapporto di lavoro è coinciso con il matrimonio;                     &#13;
      che  il  giudice  a  quo  ritiene  che   tale   norma   parifica    &#13;
 irrazionalmente situazioni diverse, in quanto attribuisce la medesima    &#13;
 quota  dell'indennità  di fine rapporto, commisurata alla durata del    &#13;
 matrimonio, computando in  essa  anche  il  periodo  di  separazione,    &#13;
 prescindendo  dalla  durata di tale periodo, nonché dalle condizioni    &#13;
 personali ed economiche dei coniugi  divorziati,  dalle  ragioni  del    &#13;
 fallimento del matrimonio, dall'entità del contributo reciproco alla    &#13;
 conduzione  familiare  e alla formazione del patrimonio comune e, infine, dalla misura dell'assegno divorzile;                               &#13;
      che il giudice a quo ritiene  che,  in  ragione  delle  medesime    &#13;
 incongruenze,   la   norma   violi   altresì   l'articolo  36  della    &#13;
 Costituzione,  perché  incide  sul  diritto  del  lavoratore  ad  un    &#13;
 emolumento di natura sicuramente retributiva;                            &#13;
      che la parte privata costituita ha aderito all'eccezione, mentre    &#13;
 il  Presidente del Consiglio dei ministri, intervenuto nel giudizio a    &#13;
 mezzo dell'Avvocatura Generale dello Stato, ha chiesto che la  stessa    &#13;
 sia dichiarata inammissibile o manifestamente infondata;                 &#13;
    Considerato   che  questione  sostanzialmente  identica,  riferita    &#13;
 peraltro agli articoli 3 e 38 della Costituzione, è stata dichiarata    &#13;
 non fondata da questa Corte con sentenza n. 23  del  1991  e  che  il    &#13;
 giudice  a  quo  non  ha  prospettato profili nuovi rispetto a quelli    &#13;
 esaminati da tale pronunzia.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'articolo 12- bis della legge 1° dicembre 1970  n.    &#13;
 898  (Disciplina dei casi di scioglimento del matrimonio), introdotto    &#13;
 dall'articolo 16 della legge 6 marzo 1987 n.  74 (Nuove  norme  sulla    &#13;
 disciplina  dei  casi  di scioglimento di matrimonio), sollevata, con    &#13;
 riferimento agli articoli 3 e 36 della Costituzione, dal Tribunale di    &#13;
 Siena con ordinanza del 24 giugno 1992.                                  &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 24 giugno 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: SPAGNOLI                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 1° luglio 1993.                          &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
