<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>331</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:331</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1993</data_decisione>
    <data_deposito>21/07/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  del decreto-legge 19    &#13;
 marzo  1993,  n.  69   (Disciplina   della   proroga   degli   organi    &#13;
 amministrativi),   promosso   con   ricorso  della  regione  Calabria    &#13;
 notificato il 17 aprile 1993, depositato in cancelleria il 20  aprile    &#13;
 successivo ed iscritto al n. 28 del registro ricorsi 1993;               &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di consiglio  del  7  luglio  1993  il  Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto che la regione Calabria impugna il decreto-legge 19 marzo    &#13;
 1993,  n.  69,  recante  la  "disciplina  della  proroga degli organi    &#13;
 amministrativi", in riferimento agli articoli 77, 117, 118, 121,  122    &#13;
 e 123 della Costituzione;                                                &#13;
      che  la  regione  ricorrente,  dopo  aver illustrato la serie di    &#13;
 decreti-legge adottati in materia dal Governo (nn. 381 del 1992,  439    &#13;
 del  1992, 7 del 1993 e 69 del 1993), osserva che il decreto legge n.    &#13;
 69  del  1993  da  ultimo  impugnato  rappresenta,  in  sostanza,  la    &#13;
 riproduzione  dei precedenti provvedimenti d'urgenza, eccetto che per    &#13;
 la  formulazione  dell'art.   9,   relativo   all'adeguamento   della    &#13;
 disciplina regionale alle norme di principio del decreto;                &#13;
      che,    in   particolare,   la   regione   rileva   che,   prima    &#13;
 dell'emanazione  dei  ricordati  decreti-legge,  essa  stessa   aveva    &#13;
 adottato,  con  la  legge  regionale  5  agosto  1992,  n.  13, sulla    &#13;
 "disciplina delle nomine di competenza della  regione",  una  propria    &#13;
 regolamentazione  delle  scadenze  delle  nomine  e  designazioni  di    &#13;
 competenza regionale e  delle  relative  procedure  di  rinnovazione,    &#13;
 disciplina, questa, coerente con l'art. 97 della Costituzione e con i    &#13;
 principi  affermati  nella  sentenza  n.  208  del  1992  della Corte    &#13;
 costituzionale;                                                          &#13;
      che pertanto, ad avviso della regione, il  decreto-legge  n.  69    &#13;
 del  1993  "non  dovrebbe  incidere  sulla legge regionale", giacché    &#13;
 questa contiene norme di  dettaglio  bensì  diverse  da  quelle  del    &#13;
 decreto  ma  tuttavia  rispettose dei fondamentali principi posti dal    &#13;
 medesimo, rappresentati dalla cessazione delle funzioni alla scadenza    &#13;
 naturale del mandato e dalla previsione  di  un  periodo  massimo  di    &#13;
 proroga  nonché  di meccanismi sostitutivi rigidamente articolati in    &#13;
 caso di inerzia dell'organo competente alla ricostituzione;              &#13;
      che, però,  se  l'art.  9  del  decreto-legge  impugnato  fosse    &#13;
 interpretato come abrogativo della normativa regionale, la disciplina    &#13;
 denunziata  sarebbe, secondo la regione ricorrente, incostituzionale,    &#13;
 in quanto:                                                               &#13;
       a)  l'art.  4,  comma  2, del decreto-legge, che attribuisce la    &#13;
 competenza alle designazioni o nomine (per gli  organi  scaduti),  in    &#13;
 caso  di inerzia di organi collegiali, ai presidenti di detti organi,    &#13;
 violerebbe sia la competenza  regionale  in  materia  di  ordinamento    &#13;
 degli  uffici  ed  enti  dipendenti  dalle  regioni  (art.  117 della    &#13;
 Costituzione)  sia  la  competenza   statutaria   (art.   123   della    &#13;
 Costituzione),  in quanto incide sulle norme, statutarie e ordinarie,    &#13;
 che regolano le competenze degli organi collegiali, creando  ex  novo    &#13;
 una  competenza  dei presidenti e sottraendo ai collegi i correlativi    &#13;
 poteri; detta disposizione, inoltre, contrasterebbe con gli  articoli    &#13;
 121  e 122 della Costituzione, se riferita a nomine di competenza del    &#13;
 Consiglio regionale,  attesa  la  configurazione  costituzionale  del    &#13;
 presidente del Consiglio regionale, privo di rilevanza esterna;          &#13;
       b)  l'art.  3  del  decreto-legge,  sul regime di proroga degli    &#13;
 organi  amministrativi  scaduti  e  degli  atti  da  questi  emanati,    &#13;
 limitando  la  competenza  ai  soli  atti  urgenti  e  indifferibili,    &#13;
 inciderebbe sulla competenza regionale in  materia,  violando  l'art.    &#13;
 117 della Costituzione; la censura sarebbe da estendere al successivo    &#13;
 art.  6  che prevede la nullità di diritto degli atti compiuti dagli    &#13;
 organi scaduti;                                                          &#13;
       c) infine, sarebbe costituzionalmente illegittimo l'art. 8  del    &#13;
 decreto-legge,   che   convalida   e   mantiene  fermi  gli  atti  di    &#13;
 ricostituzione di organi scaduti adottati  da  presidenti  di  organi    &#13;
 collegiali,  anteriormente  all'entrata  in  vigore  del  decreto, in    &#13;
 sostituzione dei competenti  collegi;  questa  norma  violerebbe  sia    &#13;
 l'art.  77  della Costituzione, in relazione anche all'art. 15, comma    &#13;
 2, lett. d) della legge n. 400 del 1988, sia le competenze  regionali    &#13;
 in  materia di organizzazione di uffici ed enti: il decreto-legge non    &#13;
 può - afferma la  regione  -  convalidare  ciò  che  in  base  alla    &#13;
 Costituzione    è    invalido    e   non   può   dunque   sottrarre    &#13;
 all'amministrazione regionale il potere di qualificare come  invalidi    &#13;
 gli  atti già adottati in base a decreti-legge non convertiti, così    &#13;
 impedendo alle regioni di "revocare gli  illegittimi  atti  dei  loro    &#13;
 presidenti  e  di  provvedere  diversamente  in  ordine  agli  organi    &#13;
 scaduti";                                                                &#13;
      che si è costituito in giudizio il Presidente del Consiglio dei    &#13;
 Ministri, tramite l'Avvocatura generale dello Stato, concludendo  per    &#13;
 l'inammissibilità e l'infondatezza della questione;                     &#13;
    Considerato  che  il  decreto-legge  19  marzo 1993, n. 69, non è    &#13;
 stato convertito in legge entro il termine prescritto,  come  risulta    &#13;
 dal  comunicato  pubblicato  nella  Gazzetta  Ufficiale n. 116 del 20    &#13;
 maggio 1993;                                                             &#13;
      che, pertanto, in  conformità  alla  giurisprudenza  di  questa    &#13;
 Corte  (da  ultimo, ordinanze nn. 292, 229, 174 e 116 del 1993), deve    &#13;
 essere dichiarata la manifesta inammissibilità  della  questione,  e    &#13;
 che detto rilievo è logicamente preliminare rispetto al profilo di -    &#13;
 eventuale   -  inammissibilità,  dedotto  dall'Avvocatura  erariale,    &#13;
 basato sul rilievo per  cui  con  l'impugnativa  verrebbe  ad  essere    &#13;
 prospettata una mera questione interpretativa;                           &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale del decreto-legge 19 marzo  1993,  n.  69    &#13;
 (Disciplina  della  proroga  degli  organi  amministrativi), promossa    &#13;
 dalla regione Calabria con il ricorso indicato in epigrafe.              &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 7 luglio 1993.                                &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 21 luglio 1993.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
