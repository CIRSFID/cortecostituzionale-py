<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>336</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:336</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/10/1987</data_decisione>
    <data_deposito>22/10/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  3, secondo    &#13;
 comma, del d.l. 10 luglio 1982 n. 429 (Norme per la repressione della    &#13;
 evasione  in  materia  di imposte sui redditi e sul valore aggiunto e    &#13;
 per agevolare la definizione delle pendenze in  materia  tributaria),    &#13;
 convertito  in  l.  7 agosto 1982 n. 516, promosso con n. 6 ordinanze    &#13;
 emesse il 1° febbraio 1986 dal Giudice istruttore presso il Tribunale    &#13;
 di Foggia, iscritte ai nn. 299, 300, 301, 302, 303 e 304 del registro    &#13;
 ordinanze 1986 e pubblicate nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 35, prima serie speciale dell'anno 1986;                              &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera di consiglio del 30 settembre 1987 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che  nel  corso  di  un  procedimento penale a carico di    &#13;
 Calderisi Luigi, imputato di avere omesso di annotare sul  prescritto    &#13;
 registro 250 ricevute fiscali, il Giudice istruttore del Tribunale di    &#13;
 Foggia con ordinanza del 1° febbraio 1986 (reg. ord. n. 299 del 1986)    &#13;
 sollevava,   in   riferimento   all'art.   25   Cost.,  questione  di    &#13;
 legittimità costituzionale  dell'art.  3,  secondo  comma,  d.l.  10    &#13;
 luglio  1982  n.  429,  convertito  in legge 7 agosto 1982 n. 516, il    &#13;
 quale commina sanzioni penali per "chi stampa, fornisce,  acquista  o    &#13;
 detiene stampati per la compilazione dei documenti di accompagnamento    &#13;
 dei beni viaggianti o delle ricevute fiscali  senza  provvedere  alle    &#13;
 prescritte annotazioni";                                                 &#13;
      che,  secondo  il  giudice  rimettente  la  descrizione  di tale    &#13;
 fattispecie di reato, rinviando ad  atti  amministrativi  per  quanto    &#13;
 riguardava   le   "prescritte   annotazioni"   -   ossia  ai  decreti    &#13;
 ministeriali di attuazione della l. 10 maggio 1976 n. 249, contenente    &#13;
 tra  l'altro  norme  in  materia di documenti di accompagnamento e di    &#13;
 ricevute fiscali -, violava il  principio  di  riserva  di  legge  in    &#13;
 materia  penale,  di  cui  all'art.  25, secondo comma, Cost.: questa    &#13;
 norma infatti escludeva, sempre secondo  l'ordinanza  di  rimessione,    &#13;
 che  il  precetto penale, per effetto di un rinvio espresso o tacito,    &#13;
 venisse integrato da atti amministrativi;                                &#13;
      che ordinanze di identico contenuto venivano emesse dallo stesso    &#13;
 giudice nel corso dei procedimenti penali                                &#13;
 contro   Giordani   Francesco,   Marrone  Nicola,  Magnifico  Savino,    &#13;
 Barriello Lucia e Piscopo Maria Mattea (registro                         &#13;
 ord. nn. da 300 a 304 del 1986);                                         &#13;
    Considerato che, come risulta da numerose pronunce di questa Corte    &#13;
 (tra cui le sentt. nn. 26 del 1966, 168 del 1971, 58  del  1975),  il    &#13;
 principio  di  legalità  dei  reati  e  delle  pene  deve  ritenersi    &#13;
 soddisfatto quando nella norma primaria siano indicati i presupposti,    &#13;
 i  caratteri,  il  contenuto  ed  i limiti dei provvedimenti alla cui    &#13;
 trasgressione  deve  seguire  la  sanzione  penale,   potendo   così    &#13;
 ritenersi che il reato sia tassativamente determinato in tutti i suoi    &#13;
 elementi costitutivi;                                                    &#13;
      che  nella  specie  la norma impugnata descrive il comportamento    &#13;
 omissivo  illecito  con  sufficiente  precisione,  restando  altresì    &#13;
 chiara  la  ratio legis, che è quella di impedire la produzione o il    &#13;
 commercio  clandestini  di  stampati  idonei  ad  evitare  l'evasione    &#13;
 fiscale  e  rimanendo all'autorità amministrativa la sola emanazione    &#13;
 di prescrizioni di dettaglio;                                            &#13;
      che pertanto la questione si rivela manifestamente infondata;       &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953  n.  87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi, dichiara manifestamente infondata la questione    &#13;
 di legittimità costituzionale dell'art. 3, secondo  comma,  d.l.  10    &#13;
 luglio  1982  n.  429,  convertito  in  legge  7  agosto 1982 n. 516,    &#13;
 sollevata in riferimento all'art. 25 Cost. dal Giudice istruttore del    &#13;
 Tribunale di Foggia con le ordinanze indicate in epigrafe.               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 ottobre 1987.                              &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: SAJA                                 &#13;
    Depositata in cancelleria il 22 ottobre 1987.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
