#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    __init__.py
    ~~~~~~~~~~~

    no description available

    :copyright: (c) 2019 by CIRSFID.
    :license: see LICENSE for more details.
"""

__title__ = 'cortecostituzionale'
__version__ = '0.0.4'
__author__ = 'Biagio Distefano'


from cortecostituzionale.commons import setup_logger
from multiprocessing_logging import install_mp_handler

logger = setup_logger("cortecost")
install_mp_handler()
logger.info("STARTED CORTE COSTITUZIONALE")
