<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-02-23/26/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-02-23/26"/>
          <FRBRalias value="ECLI:IT:COST:2023:26" name="ECLI"/>
          <FRBRdate date="2023-02-23" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="26"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-02-23/26/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-02-23/26/ita@"/>
          <FRBRdate date="2023-02-23" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2023-02-23/26/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2023-02-23/26/ita@.xml"/>
          <FRBRdate date="2023-02-23" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2023-01-10" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2023-02-23" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="SCIARRA" href="/akn/ontology/person/it/SCIARRA" showAs="SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2023</akn4cc:anno>
        <akn4cc:numero>26</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2023:26</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>SCIARRA</akn4cc:presidente>
        <akn4cc:relatore>Luca Antonini</akn4cc:relatore>
        <akn4cc:data_decisione>2023-01-10</akn4cc:data_decisione>
        <akn4cc:data_deposito>2023-02-23</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>; Giudici : <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 15, comma 5, secondo periodo, della legge della Regione Calabria 19 marzo 2004, n. 11 (Piano Regionale per la Salute 2004/2006), promosso dalla Corte di cassazione, sezione lavoro, nel procedimento vertente tra C.S. B. e l'Azienda sanitaria provinciale di Crotone e altri, con ordinanza del 18 ottobre 2021, iscritta al n. 1 del registro ordinanze 2022 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 6, prima serie speciale, dell'anno 2022.
 	Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Calabria e di C.S. B.;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica del 10 gennaio 2023 il Giudice relatore Luca Antonini;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>uditi gli avvocati Marco Marazza per C.S. B. e Franceschina Talarico per la Regione Calabria;
 	deliberato nella camera di consiglio del 10 gennaio 2023.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 18 ottobre 2021 (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2022/1/!main">ord. n. 1 del 2022</ref>), la Corte di cassazione, sezione lavoro, ha sollevato questioni di legittimità costituzionale dell'art. 15, comma 5, secondo periodo, della legge della Regione Calabria 19 marzo 2004, n. 11 (Piano Regionale per la Salute 2004/2006), il quale stabilisce che gli incarichi di direttore sanitario e di direttore amministrativo delle aziende del servizio sanitario regionale «hanno comunque termine ed i relativi rapporti di lavoro sono risolti di diritto, nell'ipotesi di cessazione, per revoca, decadenza, dimissioni o qualsiasi altra causa, del direttore generale».
 2.- Il rimettente è investito del ricorso con il quale il direttore amministrativo di un'azienda sanitaria calabrese ha impugnato la sentenza d'appello che, per quanto qui interessa, ha ritenuto la legittimità della decadenza del direttore generale e della risoluzione di diritto del rapporto di lavoro del medesimo direttore amministrativo che ne è conseguita in forza della disposizione denunciata.
 2.1.- In punto di rilevanza, il giudice a quo, ritenuti preliminarmente inammissibili i motivi di gravame concernenti la decadenza del direttore generale, osserva che la decisione sulle domande in merito alla suddetta risoluzione dipende dall'applicazione della norma censurata, sulla cui asserita illegittimità costituzionale è basato uno dei motivi di ricorso: da tale norma è, infatti, derivata la risoluzione medesima e su di essa il giudice d'appello ha fondato il rigetto delle pretese azionate in giudizio.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Pertanto, aggiunge il rimettente, se l'art. 15, comma 5, secondo periodo, della legge reg. Calabria n. 11 del 2004 fosse caducato, la risoluzione oggetto della controversia non sarebbe consentita da alcuna norma.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>2.- In punto di non manifesta infondatezza, il giudice a quo dubita della compatibilità della norma denunciata con gli artt. 97, secondo comma, e 98, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, per ragioni che rinviene nella giurisprudenza costituzionale.
 Premette il rimettente che con la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2006/233/!main">sentenza n. 233 del 2006</ref> questa Corte ha escluso la violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> ad opera dell'art. 14, comma 3, della legge della Regione Calabria 17 agosto 2005, n. 13 - recante «Provvedimento generale, recante norme di tipo ordinamentale e finanziario (collegato alla manovra di assestamento di bilancio per l'anno 2005 ai sensi dell'art. 3, comma 4, della legge regionale 4 febbraio 2002, n. 8)» - nella parte in cui prevede la decadenza dei direttori amministrativi e sanitari delle aziende ospedaliere e delle aziende sanitarie locali in concomitanza con la nomina dei direttori generali delle aziende medesime.
 Nell'occasione, infatti, questa Corte ha osservato che il citato art. 14, comma 3, è funzionale ad assicurare il principio del buon andamento dell'azione amministrativa, giacché non regola un rapporto fiduciario tra l'organo politico che conferisce l'incarico e colui che lo riceve, non prevedendo quindi un meccanismo di spoils system, ma riguarda l'organizzazione amministrativa sanitaria, mirando a garantire la consonanza d'impostazione gestionale fra il direttore generale, da un lato, e quello amministrativo e sanitario, dall'altro.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ad avviso del rimettente, tuttavia, i principi successivamente enunciati da questa Corte condurrebbero a ritenere costituzionalmente illegittima la norma oggetto dell'odierno scrutinio.
 Nelle <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2011/228/!main">sentenze n. 228 del 2011</ref> e n. 224 del 2010, infatti, questa Corte, alla luce dell'orientamento nel frattempo consolidatosi in tema di spoils system (sono citate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/34/!main">sentenze n. 34 del 2010</ref>, n. 351 e n. 161 del 2008, n. 104 e n. 103 del 2007), avrebbe affermato che sarebbero lesive dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> disposizioni recanti meccanismi di decadenza automatica relativi ai rapporti, non solo tra organi politici e amministrativi, ma anche tra gli stessi organi amministrativi, se concernenti figure dirigenziali non apicali, ovvero titolari di uffici per la cui scelta l'ordinamento non attribuisce rilievo, esclusivo o prevalente, al criterio della personale adesione del nominato all'orientamento politico del soggetto conferente: anche in queste ipotesi, infatti, l'automatismo della decadenza contrasterebbe con il principio del buon andamento, pregiudicando la continuità dell'azione amministrativa e privando il soggetto dichiarato decaduto delle garanzie del giusto procedimento, nell'ambito del quale accertare i risultati conseguiti nello svolgimento dell'incarico.
 Di qui la declaratoria di illegittimità costituzionale di norme regionali che disponevano la cessazione dall'incarico del direttore amministrativo e di quello sanitario a seguito della nomina del nuovo direttore generale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>A parere del rimettente, anche la norma sospettata, che lega la risoluzione di diritto alla cessazione dall'incarico del direttore generale, si porrebbe in contrasto con i suddetti principi.
 Infatti, una volta sorto, a seguito della scelta, pur ampiamente discrezionale, del direttore generale, il rapporto di lavoro con il direttore amministrativo e con quello sanitario, verrebbero in rilievo sia l'esigenza di assicurare la continuità dell'espletamento delle funzioni conferite loro, sia quella di ancorare l'interruzione del rapporto stesso - contrariamente a quanto previsto dalla disposizione censurata - alla sussistenza di ragioni, da valutare con le garanzie del giusto procedimento, legate alle relative modalità di svolgimento.
 Il denunciato meccanismo di risoluzione automatica risulterebbe lesivo anche dell'art. 98, primo comma, Cost., che impone ai pubblici impiegati un dovere di neutralità.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>- Si è costituita in giudizio la Regione Calabria, parte nel giudizio a quo, eccependo l'inammissibilità delle questioni sollevate per difetto di rilevanza e sostenendo, comunque, la loro non fondatezza.
 3.1.- L'eccezione di inammissibilità è basata sull'affermazione del rimettente che, se la norma ora in esame fosse espunta dall'ordinamento, la risoluzione di diritto del rapporto oggetto del processo principale, in ogni caso, non sarebbe consentita da altre disposizioni.
 Secondo la difesa regionale occorrerebbe infatti considerare che: a) la detta modalità di risoluzione sarebbe stata prevista in un'apposita clausola del contratto sottoscritto dal ricorrente nel processo principale, che avrebbe quindi «prodotto e ormai esaurito i propri effetti», a prescindere della disciplina legislativa censurata; b) la norma oggetto di doglianza avrebbe «trovato conferma» nel sopra citato art. 14, comma 3, della legge reg. Calabria n. 13 del 2005; c) ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/1992-12-30/502/!main#art_3bis__para_8">art. 3-bis, comma 8, del decreto legislativo 30 dicembre 1992, n. 502</ref> (Riordino della disciplina in materia sanitaria, a norma dell'<ref href="/akn/it/act/legge/stato/1992-10-23/421/!main#art_1">articolo 1 della legge 23 ottobre 1992, n. 421</ref>), i contratti stipulati dai direttori sanitari e amministrativi delle aziende sanitarie sarebbero riconducibili ai contratti d'opera di cui agli artt. 2222 e seguenti del <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">codice civile</ref>, sicché, anche in assenza di una specifica normativa regionale, la fattispecie oggetto del giudizio a quo risulterebbe disciplinata da tali norme codicistiche.
 3.2.- Nel merito, le questioni sarebbero destituite di fondamento.
 Al riguardo, la Regione osserva che la norma denunciata - introdotta in attuazione del citato art. 3-bis, comma 8, che demanda alle regioni la disciplina delle cause di risoluzione del rapporto con i direttori amministrativi e sanitari - si prefiggerebbe di assicurare la permanenza del vincolo fiduciario tra il direttore generale e i suoi più stretti collaboratori al fine di realizzare un «programma gestionale comune»: in questa prospettiva, pertanto, essa, diversamente da quanto sostenuto dal rimettente, garantirebbe proprio il canone del buon andamento, come del resto affermato da questa Corte nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2006/233/!main">sentenza n. 233 del 2006</ref>.
 La norma censurata non riguarderebbe, dunque, i rapporti tra la politica e l'amministrazione, ma esclusivamente un legame intercorrente tra organi amministrativi diretto ad assicurare, nella logica del principio simul stabunt, simul cadent, la consonanza di gestione tra essi.
 D'altra parte, precisa sul punto la difesa regionale, l'<ref href="/akn/it/act/decretoLegislativo/stato/1992/502/!main#art_3__para_1quinquies">art. 3, comma 1-quinquies, del d.lgs. n. 502 del 1992</ref> stabilisce che i direttori amministrativi e sanitari partecipano, unitamente al direttore generale, alla direzione dell'azienda e concorrono alla formazione delle decisioni della direzione generale, ciò che richiederebbe necessariamente un'azione coordinata dei tre direttori al fine del raggiungimento degli obiettivi aziendali.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>I rilievi che precedono determinerebbero, in definitiva, la non fondatezza della questione sollevata in riferimento all'art. 97, secondo comma, Cost.
 Anche la censura di violazione dell'art. 98, primo comma, Cost. sarebbe priva di pregio, poiché, come detto, la norma sospettata non inciderebbe sul rapporto degli organi amministrativi con la politica.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>- Si è costituito in giudizio il ricorrente nel processo principale, chiedendo la declaratoria di illegittimità costituzionale dell'art. 15, comma 5, secondo periodo, della legge reg. Calabria n. 11 del 2004.
 Questi, premesso, in ordine alla rilevanza delle questioni, che la definizione del giudizio a quo dipenderebbe dall'applicazione di tale norma, nel merito sostiene che questa Corte avrebbe ritenuto compatibili con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> meccanismi di decadenza analoghi a quello oggetto di censura solo se concernenti i titolari di uffici di diretta collaborazione con gli organi di governo o figure apicali: ipotesi, queste, che non ricorrerebbero nella specie.
 4.1.- In prossimità dell'udienza, la parte privata ha depositato una memoria illustrativa con cui ha insistito nelle conclusioni già rassegnate e ha replicato all'eccezione di inammissibilità sollevata ex adverso, al riguardo osservando, tra l'altro, che nel contratto individuale da essa sottoscritto non sarebbe stata trasfusa la clausola evocata dalla difesa regionale.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ordinanza del 18 ottobre 2021 (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2022/1/!main">ord. n. 1 del 2022</ref>), la Corte di cassazione, sezione lavoro, dubita della legittimità costituzionale dell'art. 15, comma 5, secondo periodo, della legge reg. Calabria n. 11 del 2004.
 La disposizione censurata stabilisce che gli incarichi di direttore sanitario e di direttore amministrativo delle aziende sanitarie regionali «hanno comunque termine ed i relativi rapporti di lavoro sono risolti di diritto, nell'ipotesi di cessazione, per revoca, decadenza, dimissioni o qualsiasi altra causa, del direttore generale».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ad avviso del giudice a quo, questa norma lederebbe il principio del buon andamento di cui all'art. 97, secondo comma, Cost., poiché pregiudicherebbe l'esigenza di assicurare con continuità l'espletamento delle funzioni affidate al direttore sanitario e a quello amministrativo, ancorando l'interruzione anticipata dei relativi rapporti alla cessazione del direttore generale e dunque prescindendo dalla sussistenza di ragioni, da valutare con le garanzie del giusto procedimento, legate alle concrete modalità di svolgimento degli incarichi.
 Il denunciato meccanismo di sostanziale decadenza automatica violerebbe, inoltre, l'art. 98, primo comma, Cost., che impone ai pubblici impiegati un dovere di neutralità.
 2.- Deve essere preliminarmente esaminata l'eccezione d'inammissibilità sollevata dalla Regione Calabria.
 Le questioni sarebbero, infatti, irrilevanti, perché il rimettente non avrebbe considerato: a) che la risoluzione di diritto del rapporto de quo sarebbe stata prevista anche in un'apposita clausola del contratto sottoscritto dal ricorrente nel processo principale, la quale avrebbe «prodotto e ormai esaurito i propri effetti», a prescindere dalla norma denunciata; b) che, analogamente a quanto stabilito dalla norma censurata, anche l'art. 14, comma 3, della legge reg. Calabria n. 13 del 2005 disporrebbe la decadenza dagli incarichi del direttore sanitario e di quello amministrativo, sebbene in caso di nomina del nuovo direttore generale; c) che i contratti del direttore sanitario e di quello amministrativo sarebbero regolati dalle norme del <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">codice civile</ref> sui contratti di prestazione d'opera, sicché, se pure la disposizione censurata fosse espunta dall'ordinamento, la fattispecie oggetto del giudizio a quo sarebbe disciplinata da tali norme codicistiche.
 2.1.- L'eccezione non è suscettibile di accoglimento, sotto alcuno dei profili in cui è articolata.
 2.1.1.- I descritti rilievi della difesa regionale non possono, infatti, essere condivisi, dal momento che: a) dell'ipotizzata clausola contrattuale, il cui specifico contenuto non è stato, in ogni caso, descritto dalla Regione, non vi è cenno nell'ordinanza di rimessione, né la difesa regionale ha dedotto che essa appartenesse al thema decidendum devoluto al rimettente; b) la decadenza di cui all'art. 14, comma 3, della legge reg. Calabria n. 13 del 2005 è legata alla nomina del nuovo direttore generale, sicché si è al cospetto di una fattispecie distinta dalla risoluzione di diritto che ha dato origine al giudizio a quo; c) la Regione non ha indicato quale delle richiamate norme del <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">codice civile</ref> disciplinerebbe tale risoluzione e comunque - se pure si volesse ritenere che intendesse fare riferimento all'<ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main#art_2227">art. 2227 cod. civ.</ref>, che attribuisce al committente la facoltà di recesso ad nutum nei contratti di prestazione d'opera - non ha dedotto di aver fatto ricorso a tale facoltà dopo la risoluzione medesima (ciò che, a tacer d'ogni altra considerazione, si risolverebbe peraltro in un posterius che non inciderebbe sul giudizio di rilevanza).
 2.1.2.- Del resto, nell'ordinanza di rimessione è chiaramente spiegato che la risoluzione di diritto del rapporto con il direttore amministrativo oggetto del processo principale è conseguita alla decadenza del direttore generale per effetto della norma denunciata; è in forza di quest'ultima, inoltre, che la sentenza d'appello impugnata con ricorso per cassazione ha disatteso le pretese azionate dal direttore amministrativo; infine, è proprio sull'asserita illegittimità costituzionale della medesima norma che si è fondato uno dei motivi di gravame.
 Poiché gli ulteriori motivi di censura della sentenza impugnata sono stati dichiarati inammissibili, è evidente che il rimettente deve fare applicazione della disposizione sospettata e che l'esito dell'odierno incidente di legittimità costituzionale influisce sulla sua decisione: tanto è sufficiente a radicare la rilevanza delle questioni sollevate, secondo il costante orientamento di questa Corte (ex plurim<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/193/!main">is, sentenze n. 193</ref>, n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/183/!main">183</ref>, n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/167/!main">167</ref>, n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/143/!main">143</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/139/!main">139 del 2022</ref></mref>).
 3.- Nel merito, è fondata la censura di violazione dell'art. 97, secondo comma, Cost.
 Le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2011/228/!main">sentenze n. 228 del 2011</ref> e n. 224 del 2010 hanno puntualizzato le affermazioni della più risalente <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2006/233/!main">sentenza n. 233 del 2006</ref>, evocata dalla difesa della Regione Calabria, anche con riguardo alla specifica considerazione, in essa contenuta, del principio di buon andamento dell'azione amministrativa.
 In particolare, la richiamata <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/224/!main">sentenza n. 224 del 2010</ref>, anche alla luce dell'evoluzione medio tempore intervenuta nella giurisprudenza costituzionale sui meccanismi di spoils system, ha voluto valorizzare maggiormente, rispetto a quel precedente del 2006, altri profili che ha ritenuto più strettamente connessi al principio di buon andamento della pubblica amministrazione.
 Infatti, con riguardo a una normativa della Regione Lazio che prevedeva una regola che legava in modo automatico, secondo il principio simul stabunt, simul cadent, la sorte di una figura non apicale, quale il direttore amministrativo, a quella del direttore generale, ha dato risalto, da un lato, alle «esigenze dell'Amministrazione ospedaliera concernenti l'espletamento con continuità delle funzioni dirigenziali proprie del direttore amministrativo, e, dall'altro lato, alla tutela giudiziaria, costituzionalmente protetta, delle situazioni soggettive dell'interessato, inerenti alla carica».
 All'interno di un contesto normativo regionale che oltretutto stabiliva «un particolare iter procedimentale di garanzia per il direttore amministrativo», la sentenza in esame ha pertanto ritenuto del tutto ingiustificata, e in fondo anche contraddittoria, l'omessa considerazione delle medesime esigenze da parte della norma censurata, che stabiliva invece un mero meccanismo di decadenza automatica.
 Quest'ultimo è stato quindi ritenuto in contrasto con il principio di buon andamento dell'azione amministrativa sancito dall'art. 97, secondo comma, Cost.
 3.1.- Tali conclusioni non possono che essere ribadite anche con riferimento alla norma censurata.
 Questa, facendo decorrere la cessazione degli incarichi del direttore amministrativo e di quello sanitario dalla cessazione del vecchio direttore generale, stride, ancor più sotto tale profilo, con l'esigenza di continuità dell'azione amministrativa.
 Infatti, in forza della specifica modalità con cui è strutturato il principio simul stabunt, simul cadent dall'art. 15, comma 5, secondo periodo, della legge reg. Calabria n. 11 del 2004, l'ente risulta esposto al rischio di subire un periodo di discontinuità gestionale, in ipotesi anche prolungato, in cui il vacuum finisce addirittura per riguardare tutti i tre i direttori preposti, secondo le loro rispettive competenze, al governo dell'ente stesso.
 3.2.- La norma censurata, inoltre, con l'effetto automatico che determina, «non àncora l'interruzione del rapporto di ufficio in corso a ragioni "interne" a tale rapporto» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/224/!main">sentenza n. 224 del 2010</ref>), legate alle modalità di svolgimento delle funzioni del direttore amministrativo e di quello sanitario.
 Essa, infatti, pretermette del tutto una fase valutativa dei comportamenti tenuti dall'interessato, in cui al dirigente sia consentita la possibilità di fare valere le proprie ragioni, sulla base dei risultati delle prestazioni rese e delle competenze esercitate in concreto nella gestione dei servizi amministrativi a lui affidati.
 L'interruzione automatica del rapporto stabilita dalla norma censurata esclude quindi ogni possibilità di valutazione qualitativa dell'operato del direttore amministrativo e di quello sanitario.
 In presenza invece di tale possibilità, il nuovo direttore generale, per fare cessare dall'incarico il direttore amministrativo e quello sanitario, sarebbe tenuto a specificare le ragioni, connesse alle pregresse modalità di svolgimento delle funzioni dirigenziali da parte dell'interessato, «idonee a fare ritenere sussistenti comportamenti di quest'ultimo suscettibili di integrare la violazione delle direttive ricevute o di determinare risultati negativi nei servizi di competenza e giustificare, dunque, il venir meno della necessaria consonanza di impostazione gestionale» (ancora, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2011/228/!main">sentenza n. 228 del 2011</ref>).
 Anche da questo punto di vista, quindi, l'automatismo della disciplina in discorso, con la mancata previsione di una fase procedurale che faccia dipendere «la decadenza da pregressa responsabilità del dirigente, comporta una vera e propria "discontinuità della gestione"», che, risultando priva di una motivata giustificazione, si pone in contrasto con il principio del buon andamento dell'azione amministrativa di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">art. 97 Cost.</ref> (ancora, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/224/!main">sentenza n. 224 del 2010</ref>).</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>3.- Sulla scorta delle argomentazioni che precedono, deve dichiararsi l'illegittimità costituzionale dell'art. 15, comma 5, secondo periodo, della legge reg. Calabria n. 11 del 2004.
 Resta assorbita l'ulteriore censura prospettata dal rimettente.
 4.- Va, infine, precisato che in situazioni di commissariamento statale del servizio sanitario regionale - quale quello attualmente vigente nella Regione Calabria, di cui questa Corte «ha registrato la straordinaria lunghezza e difficoltà della gestione» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2022/228/!main">sentenza n. 228 del 2022</ref>) - quanto qui affermato, a regime, in ordine all'illegittimità costituzionale dell'automatismo della decadenza degli incarichi dei direttori sanitari e amministrativi non interferisce con la particolare prospettiva in cui, in tali contesti, viene in causa il principio del buon andamento della pubblica amministrazione, e in particolare con quanto chiarito nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021/168/!main">sentenza n. 168 del 2021</ref>, per cui il potere sostitutivo, «in situazioni estreme come quella in oggetto, non può essere certo attuato attraverso il mero avvicendamento del vertice, senza considerare l'inefficienza dell'intera struttura sulla quale tale vertice è chiamato a operare in nome dello Stato».</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'art. 15, comma 5, secondo periodo, della legge della Regione Calabria 19 marzo 2004, n. 11 (Piano Regionale per la Salute 2004/2006).</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2023-01-10">10 gennaio 2023</date>.</block>
      <p>F.to:
 <signature><judge refersTo="#silvanaSciarra" as="#presidente">Silvana SCIARRA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#lucaAntonini" as="#relatore">Luca ANTONINI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2023-02-23">23 febbraio 2023</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</p>
    </conclusions>
  </judgment>
</akomaNtoso>
