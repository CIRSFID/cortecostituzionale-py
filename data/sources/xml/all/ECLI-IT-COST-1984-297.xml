<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>297</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:297</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1984</data_decisione>
    <data_deposito>19/12/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - &#13;
 Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. GIUSEPPE &#13;
 BORZELLINO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità  costituzionale  dell'art.  12,  &#13;
 primo  e  terzo comma, della legge della Provincia di Bolzano 20 agosto  &#13;
 1972, n. 15 (Legge di riforma  dell'edilizia  abitativa)  e  successive  &#13;
 modificazioni  e  dell'art. 24, primo comma, stessa legge, promossi con  &#13;
 13 ordinanze emesse il 5 ottobre, 9, 16, 23 novembre, 14 e 15  dicembre  &#13;
 1982,  11,  18,  25 gennaio (n. 2 ordinanze), 26 aprile, 17 maggio 1983  &#13;
 dalla Corte d'appello di Trento, iscritte ai nn. 821  e  949  del  reg.  &#13;
 ord. 1982, 14, 37, 93, 170, 171, 209, 210, 242, 310, 600 e 871 del reg.  &#13;
 ord.  1983  e  pubblicate nella Gazzetta Ufficiale della Repubblica nn.  &#13;
 108, 156, 163, 177, 184, 205, 219, 232, 246, 255 dell'anno 1983  e  nn.  &#13;
 4 e 67 dell'anno 1984.                                                   &#13;
     Visto l'atto di costituzione di Waldner Luigi;                       &#13;
     udito  nella  camera  di  consiglio  del 30 ottobre 1984 il Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
 Ritenuto che:                                                            &#13;
     1.1. - la Corte d'appello  di  Trento,  con  le  tredici  ordinanze  &#13;
 indicate   in   epigrafe,   ha   sollevato  questione  di  legittimità  &#13;
 costituzionale dell'art. 12, commi primo e  terzo,  della  legge  della  &#13;
 Provincia  di  Bolzano 20 agosto 1972, n. 15 e successive modificazioni  &#13;
 (Legge di riforma dell'edilizia abitativa), in riferimento agli artt. 3  &#13;
 e 42 Cost.;                                                              &#13;
     1.2. - il giudice remittente, premesso che nei  giudizi  a  quibus,  &#13;
 tutti  concernenti  opposizione  alla determinazione dell'indennità di  &#13;
 esproprio,  vengono  in  considerazione  aree   dotate   di   vocazione  &#13;
 edificatoria  (donde la rilevanza della questione), deduce che la norma  &#13;
 censurata,  imponendo,  nel  dettare  i  criteri  per la determinazione  &#13;
 dell'indennità, di considerare i terreni  espropriati  come  agricoli,  &#13;
 prescinderebbe  dalle  effettive caratteristiche del bene ablato, così  &#13;
 offendendo sia la previsione costituzionale del "serio  ristoro"  (art.  &#13;
 42  Cost.),  che  lo stesso principio di eguaglianza (art. 3 Cost.), in  &#13;
 relazione  alla  disparità  di  trattamento   conseguente   a   valori  &#13;
 indennitari che possono essere i più vari e i meno logici;              &#13;
     2.1.  -  in  alcune ordinanze di rimessione R.O.  949/82, 170, 171,  &#13;
 209, 310 e 600/83)  il  giudice  a  quo  solleva,  in  via  subordinata  &#13;
 all'accoglimento   della   principale,   una   seconda   questione   di  &#13;
 legittimità costituzionale, denunciando,  sempre in  riferimento  agli  &#13;
 artt.  3  e  42  Cost.,  l'art.  24,  primo comma, della medesima legge  &#13;
 provinciale n. 15 del  1972,  il  quale,  nell'ambito  della  normativa  &#13;
 concernente  le  aree  di  edilizia agevolata nelle zone di espansione,  &#13;
 disciplina l'acquisizione di dette aree da parte del comune;             &#13;
     2.2. - la Corte remittente rileva che, se il valore urbanistico dei  &#13;
 beni ablati tornasse ad avere rilevanza ai  fini  della  determinazione  &#13;
 dell'indennità,  non  potrebbe  non tenersi conto del fatto che, nelle  &#13;
 zone di espansione, il terreno della comunione coatta che residua  allo  &#13;
 stralcio  di  quello  destinato  all'edilizia abitativa agevolata ed è  &#13;
 restituito ai proprietari, viene perciò stesso ad acquistare un pregio  &#13;
 più  elevato,  in  quanto  affrancato   dal   rischio   di   ulteriori  &#13;
 espropriazioni  e  destinato  all'edilizia residenziale privata; con la  &#13;
 conseguente illegittimità costituzionale della norma censurata,  nella  &#13;
 parte  in  cui  non  detrae  dall'indennizzo,  a  mo' di compensazione,  &#13;
 l'ingiustificata locupletazione conseguita dall'espropriato mediante la  &#13;
 restituzione del suolo non utilizzato per fini pubblici;                 &#13;
     3. - in nessuno dei giudizi instaurati con le ordinanze in esame si  &#13;
 sono costituite parti private, né è  intervenuto  il  Presidente  del  &#13;
 Consiglio dei ministri;                                                  &#13;
     4.  -  i  giudizi possono, data l'identità delle questioni, essere  &#13;
 riuniti e congiuntamente decisi.                                         &#13;
     Considerato che le medesime questioni sono  state  già  esaminate,  &#13;
 sotto  gli stessi profili, dalla Corte costituzionale che, con sentenza  &#13;
 n. 231 del 1984, ha dichiarato:                                          &#13;
     a)  l'illegittimità  costituzionale  -  limitatamente  al   regime  &#13;
 dell'indennità  di  esproprio previsto per le aree comprese nel centro  &#13;
 edificato  o  altrimenti  provviste,  in   relazione   alle   oggettive  &#13;
 caratteristiche   del   bene  ablato,  dell'attitudine  edificatoria  -  &#13;
 dell'art. 12, primo comma, della legge della Provincia di Bolzano n. 15  &#13;
 del 1972 e successive modificazioni, al quale  andava  circoscritta  la  &#13;
 questione  sollevata  -  allora,  come nelle ordinanze in esame - dalla  &#13;
 stessa Corte di appello di Trento, anche nei confronti del terzo  comma  &#13;
 dell'art.  12;                                                           &#13;
     b)  l'inammissibilità della questione relativa all'art.  24, primo  &#13;
 comma,  della  stessa  legge  provinciale,  in  quanto  le  conseguenze  &#13;
 ipotizzate  dal  giudice  a  quo  si  sarebbero verificate solo se e in  &#13;
 quanto il  legislatore  altoatesino,  nel  ridefinire,  in  conseguenza  &#13;
 dell'illegittimità   costituzionale   allora  pronunziata,  il  regime  &#13;
 indennitario, non avesse tenuto conto del  trattamento  di  favore  che  &#13;
 nelle  ordinanze  di  remissione  si assumeva riservato ai terreni e ai  &#13;
 soggetti espropriati nelle zone di espansione;                           &#13;
     che non vi sono motivi per discostarsi da tali decisioni, in quanto  &#13;
 il  giudice a quo prospetta le questioni con argomentazioni identiche a  &#13;
 quelle contenute nelle ordinanze di cui ai giudizi decisi con la citata  &#13;
 sentenza n. 231/84.                                                      &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e 9, secondo comma, delle Norme integrative per i giudizi dinanzi  alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a)   dichiara   la   manifesta   infondatezza  della  questione  di  &#13;
 legittimità costituzionale dell'art.  12,  primo  comma,  della  legge  &#13;
 della  Provincia  di  Bolzano 20 agosto 1972, n. 15  ("Legge di riforma  &#13;
 dell'edilizia abitativa") e  successive  modificazioni,  sollevata,  in  &#13;
 riferimento  agli  artt. 3 e 42 Cost., dalla Corte di appello di Trento  &#13;
 con tutte le ordinanze indicate in epigrafe;                             &#13;
     b)  dichiara  la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità  costituzionale  dell'art.  24,  primo  comma, della legge  &#13;
 della Provincia di Bolzano 20 agosto 1972, n. 15, sollevata dalla Corte  &#13;
 di appello di Trento, in riferimento agli artt. 3 e 42  Cost.,  con  le  &#13;
 ordinanze nn. 949 del 1982 e 170, 171, 209, 310 e 600 del 1983.          &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 dicembre 1984.       &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN   -   BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN   -   ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - FRANCESCO SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI - GIUSEPPE BORZELLINO.      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
