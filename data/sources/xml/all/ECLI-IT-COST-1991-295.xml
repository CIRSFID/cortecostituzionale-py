<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>295</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:295</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GALLO E.</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/06/1991</data_decisione>
    <data_deposito>26/06/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Ettore GALLO; &#13;
 Giudici: dott. Aldo CORASANITI, dott. Francesco GRECO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art.7, primo  comma,    &#13;
 del   decreto-legge   21   marzo   1988,  n.  86  (Norme  in  materia    &#13;
 previdenziale, di occupazione giovanile  e  di  mercato  del  lavoro,    &#13;
 nonché  per  il  potenziamento del sistema informatico del ministero    &#13;
 del   lavoro   e   della   previdenza   sociale),   convertito,   con    &#13;
 modificazioni,  in  legge  20  maggio  1988,  n.  160,  promosso  con    &#13;
 ordinanza emessa il 19  gennaio  1991  dal  Pretore  di  Messina  nel    &#13;
 procedimento  civile  vertente tra Costa Rosetta e I.N.P.S., iscritta    &#13;
 al n. 220 del registro ordinanze 1991  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale della Repubblica n. 15 prima Serie speciale dell'anno 1991;    &#13;
    Visto  l'atto  di  costituzione  dell'I.N.P.S.  nonché  l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito nella camera di consiglio del  22  maggio  1991  il  Giudice    &#13;
 relatore Aldo Corasaniti;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  corso  di  un  giudizio  promosso da Rosetta Costa nei    &#13;
 confronti dell'Istituto Nazionale della Previdenza Sociale (I.N.P.S.)    &#13;
 per la rivalutazione della indennità di disoccupazione, per gli anni    &#13;
 precedenti il 1988, a seguito della  sentenza  n.  497  del  1988  di    &#13;
 questa  Corte, l'adito Pretore di Messina, con ordinanza emessa il 19    &#13;
 gennaio 1991, ha sollevato questione di legittimità  costituzionale,    &#13;
 in  riferimento agli artt. 2, 3 e 38, secondo comma, Cost., dell'art.    &#13;
 7, primo comma, del d.l. 21 marzo 1988,  n.  86,  recante  "Norme  in    &#13;
 materia  previdenziale,  di  occupazione  giovanile  e di mercato del    &#13;
 lavoro, nonché per il  potenziamento  del  sistema  informatico  del    &#13;
 Ministero  del  lavoro  e  della previdenza sociale", convertito, con    &#13;
 modificazioni, in l. 20 maggio 1988, n. 160, nella parte in  cui  non    &#13;
 prevede alcuna indennità di disoccupazione per il periodo precedente    &#13;
 alla sua entrata in vigore e per il periodo successivo al 31 dicembre    &#13;
 1988, né prevede alcun criterio di calcolo di detta indennità.         &#13;
    Premette  il  giudice  a  quo  che, nelle more del giudizio al cui    &#13;
 esito questa Corte dichiarava costituzionalmente  illegittimo  l'art.    &#13;
 13 del d.l. 2 marzo 1974, n. 30 (Norme per il miglioramento di alcuni    &#13;
 trattamenti  previdenziali  ed  assistenziali),  convertito  in l. 16    &#13;
 aprile 1974, n. 114 - che elevava a lire  800  al  giorno  la  misura    &#13;
 dell'indennità di disoccupazione ordinaria -, nella parte in cui non    &#13;
 prevede  un  meccanismo  di  adeguamento  del  valore  monetario  ivi    &#13;
 indicato, con il denunciato art. 7, primo comma, del d.l. n.  86  del    &#13;
 1988,  l'importo  di  tale  indennità  veniva fissato, dalla data di    &#13;
 entrata in vigore del decreto e per il solo 1988, "nella  misura  del    &#13;
 7,50% della retribuzione".                                               &#13;
    Ciò  posto,  l'autorità  remittente  osserva  come,  non essendo    &#13;
 applicabile la nuova disciplina,  limitata  ad  una  parte  dell'anno    &#13;
 1988, alle situazioni giuridiche formatesi prima della sua entrata in    &#13;
 vigore  -  in  questo  senso già si era espressa, in motivazione, la    &#13;
 sentenza n. 497 del 1988 citata -, e non  essendo  più  applicabile,    &#13;
 "neppure  con eventuali correttivi", la norma (art. 13 del d.l. n. 30    &#13;
 del 1974) colpita dalla pronuncia di questa Corte, ci  si  trovi  "di    &#13;
 fronte  all'insolubile  problema  del  criterio  da  adottare  per la    &#13;
 definizione delle controversie pendenti".                                &#13;
   La formulazione dell'art. 7, primo comma, del d.l. n. 86  del  1988    &#13;
 si  appalesa  così,  secondo  il giudice a quo, in contrasto con gli    &#13;
 artt. 2, 3 e 38 Cost.                                                    &#13;
    È violato infatti il principio di eguaglianza, in quanto soggetti    &#13;
 titolari di un medesimo diritto, l'indennità di  disoccupazione,  lo    &#13;
 vedono   tutelato   in  modo  diverso,  o  addirittura  non  tutelato    &#13;
 affatto, in relazione  ad  un  evento,  l'epoca  di  maturazione  del    &#13;
 diritto, del tutto indipendente dalla loro volontà.                     &#13;
    Se   poi   si  considera  che  scopo  della  norma  denunciata  è    &#13;
 "assicurare ai lavoratori mezzi adeguati alle loro esigenze  di  vita    &#13;
 in  caso  di  disoccupazione involontaria, e di dare conseguentemente    &#13;
 attuazione tanto all'art. 2 quanto all'art. 38, secondo comma, Cost.,    &#13;
 la mancata regolamentazione, all'art. 7, primo comma, del d.l. n.  86    &#13;
 del 1988, sia dei periodi precedenti la sua entrata in vigore, che di    &#13;
 quelli successivi al 31 dicembre 1988, si pone in netto contrasto con    &#13;
 le  previsioni  costituzionali",  in  quanto,  neppure  in  linea  di    &#13;
 ipotesi, si può ritenere  che  la  disoccupazione  involontaria  sia    &#13;
 fenomeno limitato all'anno 1988.                                         &#13;
    2.  -  Nel  giudizio  si  è  costituito  l'INPS,  concludendo per    &#13;
 l'inammissibilità, e comunque per l'infondatezza, della questione.      &#13;
    Quanto alla mancata regolamentazione, lamentata dal giudice a quo,    &#13;
 dei periodi successivi al 31 dicembre 1988, ad  avviso  dell'INPS  la    &#13;
 questione  è  manifestamente  infondata,  in  quanto  a  ciò si era    &#13;
 provveduto, in epoca anteriore all'ordinanza di  rimessione,  con  il    &#13;
 decreto-legge 1 aprile 1989, n. 119 ("Norme in materia di trattamento    &#13;
 ordinario   di   disoccupazione   e   di   proroga   del  trattamento    &#13;
 straordinario di integrazione  salariale  in  favore  dei  dipendenti    &#13;
 delle  società  costituite  dalla GEPI S.p.a. e dei lavoratori edili    &#13;
 del Mezzogiorno, nonché di pensionamento anticipato") che,  all'art.    &#13;
 1,   dal  1°  gennaio  1989  elevava  la  misura  dell'indennità  di    &#13;
 disoccupazione al 15% della retribuzione.                                &#13;
    Tale disciplina era contenuta anche nei  successivi  decreti-legge    &#13;
 presentati   (a  seguito  della  mancata  conversione  in  legge  dei    &#13;
 precedenti), sino agli ultimi, il decreto-legge 22 novembre 1990,  n.    &#13;
 337,  ed  il  decreto-legge  28  gennaio  1991,  n. 29, che elevavano    &#13;
 altresì, per il 1990, l'importo dell'indennità di disoccupazione al    &#13;
 20% della retribuzione.                                                  &#13;
    In  ordine  al  secondo  aspetto  della  questione  -  la  mancata    &#13;
 previsione, per il periodo precedente l'entrata in vigore della norma    &#13;
 censurata,  di  alcuna  indennità  di  disoccupazione,  né di alcun    &#13;
 criterio di calcolo di detta indennità - osserva la difesa dell'INPS    &#13;
 che già  questa  Corte,  con  la  sentenza  n.  497  del  1988,  era    &#13;
 consapevole,  da una parte, dell'impossibilità per il Parlamento, in    &#13;
 sede di conversione del decreto-legge n. 86 del 1988,  di  provvedere    &#13;
 all'adeguamento degli importi per i periodi precedenti il 1988, "pena    &#13;
 la decadenza del provvedimento" e, dall'altra, della irretroattività    &#13;
 del provvedimento e, in ogni caso, della istituzionale competenza del    &#13;
 legislatore a provvedere per il passato.                                 &#13;
    Infatti,   quando   si  profilano  una  pluralità  di  soluzioni,    &#13;
 derivanti da varie possibili valutazioni,  l'intervento  della  Corte    &#13;
 non  è  ammissibile  -  così le sentenze nn. 125 del 1988 e 109 del    &#13;
 1986 -, spettando  la  relativa  scelta  unicamente  al  legislatore.    &#13;
 Qualora,  per  il  caso  di  specie,  i  vari  giudici ordinari, come    &#13;
 paventato dall'autorità  remittente,  provvedessero  per  i  periodi    &#13;
 precedenti  al  1988,  si realizzerebbe una inevitabile diversità di    &#13;
 tutela  del  diritto  alla  predetta  indennità  per  i  criteri  di    &#13;
 adeguamento ed i meccanismi di volta in volta prescelti.                 &#13;
    3.  -  È intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, che ha concluso per l'infondatezza della questione, osservando    &#13;
 che  la  cristalizzazione della misura dell'indennità prima del 1988    &#13;
 va  valutato  in  funzione  dell'esigenza  di  risanare  le  gestioni    &#13;
 previdenziali a rilevante connotazione di solidarietà sociale.          &#13;
    Sulla  base  di  scelte  economico-sociali  ed  in  funzione delle    &#13;
 disponibilità di bilancio, il legislatore, nel  fissare  le  diverse    &#13;
 decorrenze  per  differenti regimi, si è lasciato guidare da criteri    &#13;
 di gradualità come, segnatamente, per la delimitazione  della  sfera    &#13;
 temporale della nuova disciplina.                                        &#13;
    Quanto  alla proporzionalità di trattamento alle esigenze di base    &#13;
 del  lavoratore  disoccupato,   conclude   l'Avvocatura,   appartiene    &#13;
 esclusivamente  alla valutazione del legislatore ordinario disporre i    &#13;
 mezzi per l'attuazione di tale principio.<diritto>Considerato in diritto</diritto>1. - È sollevata in via  incidentale  questione  di  legittimità    &#13;
 costituzionale,  in  riferimento agli artt. 2, 3 e 38, secondo comma,    &#13;
 della Costituzione, dell'art. 7, primo comma,  del  decreto-legge  21    &#13;
 marzo  1988,  n.  86  (Norme in materia previdenziale, di occupazione    &#13;
 giovanile e di mercato del lavoro, nonché per il  potenziamento  del    &#13;
 sistema  informatico  del  Ministero  del  lavoro  e della previdenza    &#13;
 sociale), come convertito con  la  legge  20  maggio  1988,  n.  160.    &#13;
 L'impugnazione investe la norma denunciata nella parte in cui, mentre    &#13;
 fissa  per  il  solo  periodo a decorrere dalla sua entrata in vigore    &#13;
 alla   fine   dell'anno   1988   l'ammontare    dell'indennità    di    &#13;
 disoccupazione  di cui all'art. 13 del decreto-legge 2 marzo 1974, n.    &#13;
 30, come convertito con legge 16 aprile 1974, n. 114,  nella  maggior    &#13;
 misura  del  7,5  per  cento  della  retribuzione,  non  dispone tale    &#13;
 aumento, né un criterio di rivalutazione, per il  tempo  precedente,    &#13;
 né per il tempo successivo.                                             &#13;
   2.  -  Il  giudice  a  quo,  premesso  che l'art. 13 del suindicato    &#13;
 decreto-legge n. 30 del 1974, come convertito,  è  stato  dichiarato    &#13;
 costituzionalmente illegittimo con la sentenza di questa Corte n. 497    &#13;
 del   1988  per  la  parte  in  cui  non  prevede  un  meccanismo  di    &#13;
 rivalutazione dell'indennità di disoccupazione,  osserva  come,  non    &#13;
 "potendo  essere  applicata,  neppure  con  eventuali correttivi", la    &#13;
 norma ora indicata (applicabile in base ai principi sulla successione    &#13;
 delle leggi), in quanto dichiarata illegittima, né quella successiva    &#13;
 perché  non  retroattiva,   egli   si   trovi   messo   "di   fronte    &#13;
 all'insolubile  problema  del criterio da adottare per la definizione    &#13;
 delle controversie pendenti" e pertanto  costretto  ad  impugnare  la    &#13;
 norma   successiva   per   non  avere  retroattivamente  disposto  la    &#13;
 rivalutazione o un meccanismo o un criterio per effettuarla.             &#13;
    3. - La norma impugnata è censurata sotto due profili:               &#13;
       a)  perché  non  prevede  un   meccanismo   di   rivalutazione    &#13;
 dell'indennità di disoccupazione per il periodo successivo al 1988;     &#13;
       b)  perché  non  prevede  un meccanismo di rivalutazione della    &#13;
 detta indennità per il periodo anteriore alla sua entrata in vigore.    &#13;
    Sotto il profilo sub a) la questione è inammissibile, perché  si    &#13;
 desume  dalla  stessa  ordinanza  di  rimessione  che l'oggetto della    &#13;
 controversia davanti al giudice a quo era limitato alla rivalutazione    &#13;
 della indennità "per gli anni precedenti al 1988".                      &#13;
    Sotto il profilo sub b) la questione è egualmente inammissibile.     &#13;
    Questa Corte, con la sentenza n. 497 del 1988, ha già pronunciato    &#13;
 sul medesimo oggetto, vale a dire sulla  legittimità  costituzionale    &#13;
 della norma - identificata nell'art. 13 della legge n. 114 del 1974 -    &#13;
 regolatrice  dell'indennità  di  disoccupazione  fino all'entrata in    &#13;
 vigore della nuova disciplina recata dal decreto-legge n. 86 del 1988    &#13;
 e ne ha dichiarato  l'illegittimità  costituzionale  per  lo  stesso    &#13;
 vizio  ora  dedotto  (mancata  rivalutazione dell'indennità per quel    &#13;
 periodo) e in riferimento a parametri anche ora invocati (artt.  2  e    &#13;
 38,  secondo comma, della Costituzione). Né la Corte ha mancato, con    &#13;
 la citata sentenza, di prendere in esame il decreto-legge n.  86  del    &#13;
 1988   considerandolo,   come   soltanto   poteva   (e  può)  essere    &#13;
 considerato, e cioè come il modo in cui avrebbe  potuto  essere,  ma    &#13;
 non  era  stata,  realizzata  la condizione che avrebbe impedito alla    &#13;
 Corte  stessa  di  pronunciarsi  e  di  ravvisare  la  illegittimità    &#13;
 costituzionale dedotta.                                                  &#13;
    Questa  Corte non può dunque pronunciarsi una seconda volta, come    &#13;
 in realtà postula il giudice a quo.                                     &#13;
    Questi  vi  è  indotto  dalla  considerazione  che  per   effetto    &#13;
 dell'intervenuta   dichiarazione   di   illegittimità   della  norma    &#13;
 applicabile e dell'irretroattività (e quindi della inapplicabilità)    &#13;
 della norma ora impugnata esso giudice verserebbe nell'impossibilità    &#13;
 di rinvenire criteri di giudizio per la decisione delle controversie.    &#13;
    Ma  tale considerazione, a parte ogni riserva sulla sua rilevanza,    &#13;
 non è comunque esatta.                                                  &#13;
    La dichiarazione di illegittimità costituzionale di una omissione    &#13;
 legislativa  -  com'è  quella  ravvisata  nell'ipotesi  di   mancata    &#13;
 previsione,  da  parte della norma di legge regolatrice di un diritto    &#13;
 costituzionalmente garantito, di un meccanismo idoneo  ad  assicurare    &#13;
 l'effettività   di   questo   -   mentre   lascia   al  legislatore,    &#13;
 riconoscendone  l'innegabile   competenza,   di   introdurre   e   di    &#13;
 disciplinare   anche  retroattivamente  tale  meccanismo  in  via  di    &#13;
 normazione astratta, somministra essa  stessa  un  principio  cui  il    &#13;
 giudice  comune  è  abilitato a fare riferimento per porre frattanto    &#13;
 rimedio all'omissione in via di individuazione della regola del  caso    &#13;
 concreto.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara    inammissibile    la    questione    di    legittimità    &#13;
 costituzionale, in riferimento agli artt. 2, 3 e 38,  secondo  comma,    &#13;
 della  Costituzione,  dell'art.7,  primo  comma, del decreto-legge 21    &#13;
 marzo 1988, n. 86 (Norme in  materia  previdenziale,  di  occupazione    &#13;
 giovanile  e  di mercato del lavoro, nonché per il potenziamento del    &#13;
 sistema informatico del  Ministero  del  lavoro  e  della  previdenza    &#13;
 sociale),  convertito, con modificazioni, in legge 20 maggio 1988, n.    &#13;
 160, sollevata dal Pretore di Messina  con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 17 giugno 1991.                                  &#13;
                         Il Presidente: GALLO                             &#13;
                       Il redattore: CORASANITI                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 giugno 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
