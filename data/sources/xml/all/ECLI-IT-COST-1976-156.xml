<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>156</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:156</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1976</data_decisione>
    <data_deposito>07/07/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art.  50,  secondo  &#13;
 comma, del r.d. 30 dicembre 1923, n. 3269 (legge di registro), promosso  &#13;
 con  ordinanza emessa il 25 maggio 1973 dalla Commissione tributaria di  &#13;
 secondo grado di Milano sul ricorso di Colombo Natale contro  l'Ufficio  &#13;
 del  registro di Milano, iscritta al n. 184 del registro ordinanze 1974  &#13;
 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  159  del  19  &#13;
 giugno 1974.                                                             &#13;
     Udito  nella  camera  di  consiglio  del  22 aprile 1976 il Giudice  &#13;
 relatore Ercole Rocchetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza emessa il 25 maggio 1973 nel procedimento relativo al  &#13;
 ricorso proposto da Colombo Natale contro  l'Ufficio  del  registro  di  &#13;
 Milano,  la  Commissione  tributaria  di  secondo  grado  di  Milano ha  &#13;
 ritenuto rilevante e  non  manifestamente  infondata  la  questione  di  &#13;
 legittimità  costituzionale  dell'art.  50  della  legge  del registro  &#13;
 approvata con r.d. 30 dicembre 1923,  n.  3269,  con  riferimento  agli  &#13;
 artt. 3 e 53 della Costituzione.                                         &#13;
     Ad  avviso  del  giudice a quo, la disciplina contenuta nel secondo  &#13;
 comma del citato art. 50, che esclude dal giudizio di  congruità  solo  &#13;
 le vendite effettuate ai pubblici incanti, mentre assoggetta le vendite  &#13;
 forzate  senza  incanto  al  principio  generale  dell'accertamento del  &#13;
 valore  venale  da parte degli uffici dell'Amministrazione finanziaria,  &#13;
 contrasta con gli artt. 3 e 53  della  Costituzione  in  quanto,  senza  &#13;
 alcun  ragionevole motivo, sottopone le espropriazioni immobiliari a un  &#13;
 diverso trattamento, discriminando fra le  aggiudicazioni  al  pubblico  &#13;
 incanto e quelle effettuate senza incanto.                               &#13;
     L'ordinanza   è   stata   ritualmente   notificata,  comunicata  e  &#13;
 pubblicata, ma nessuno si è costituito nel giudizio  avanti  la  Corte  &#13;
 costituzionale.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. -  L'art. 50, secondo comma, della legge del registro, approvata  &#13;
 con  r.d. 30 dicembre 1923, n. 3269 (decreto ora non più in vigore, ma  &#13;
 applicabile nel giudizio a quo), dispone che la tassa proporzionale per  &#13;
 la vendita di mobili ed immobili ai pubblici incanti è corrisposta sul  &#13;
 prezzo  risultante  dall'ultimo  incanto,  e   cioè   su   quello   di  &#13;
 aggiudicazione.                                                          &#13;
     Per  le  altre  vendite  forzate,  che  per legge debbono o possono  &#13;
 compiersi con modalità diverse da  quelle  del  pubblico  incanto,  la  &#13;
 tassa  viene  applicata  non  già  sul  prezzo  di aggiudicazione, ma,  &#13;
 secondo la regola generale espressa nell'art.  30 della stessa legge di  &#13;
 registro, sul valore venale in comune  commercio  dei  beni  che  sono,  &#13;
 perciò,  assoggettati al giudizio di valutazione da parte dell'ufficio  &#13;
 fiscale competente.                                                      &#13;
     Tale differenza di trattamento fra vendite  forzate,  con  o  senza  &#13;
 incanto,   sul   punto   della   determinazione  del  valore  e  quindi  &#13;
 dell'imponibile, è sembrata ingiustificata alla commissione tributaria  &#13;
 di secondo grado di Milano, la quale, dovendo decidere sulla tassazione  &#13;
 di un immobile, venduto in sede   fallimentare senza incanto  ai  sensi  &#13;
 dell'art.  108  del r.d. 16 marzo  1942, n. 267, ha impugnato la citata  &#13;
 norma  dell'art.  50  con  riferimento  agli  artt.  3   e   53   della  &#13;
 Costituzione,  sostenendo che le due situazioni, oggettivamente eguali,  &#13;
 non possono essere diversamente disciplinate.                            &#13;
     2. - La questione sottoposta  all'esame  della  Corte  investe  una  &#13;
 proposizione   normativa  che,  sotto  altri  aspetti,  è  stata  già  &#13;
 esaminata in sede di controllo di costituzionalità.                     &#13;
     Il dubbio  sulla  legittimità  della  norma  impugnata  era  stato  &#13;
 infatti  proposto  sotto il profilo della disparità di trattamento tra  &#13;
 le vendite effettuate ai pubblici incanti e quelle del libero  mercato;  &#13;
 esso,  per  altro, venne disatteso dalla Corte in considerazione che il  &#13;
 diverso criterio di valutazione, previsto dalla norma per le vendite ai  &#13;
 pubblici incanti, trova la sua giustificazione nel fatto che le vendite  &#13;
 effettuate con tale sistema danno assoluta  garanzia  sull'autenticità  &#13;
 del  prezzo  pagato  e  possono  far presumere, ragionevolmente, che il  &#13;
 prezzo di aggiudicazione sia corrispondente al valore venale  del  bene  &#13;
 acquistato  sul libero mercato (sent. n. 62 del 1965).  Richiamandosi a  &#13;
 questi criteri, in altra  occasione,  la  Corte,  investita  dell'esame  &#13;
 della  legittimità  costituzionale  dell'art.  4  del r.d.l. 19 agosto  &#13;
 1943, n. 737, che, in deroga all'art.  50  della  legge  del  registro,  &#13;
 assoggettava  al procedimento di valutazione le vendite coatte promosse  &#13;
 in dipendenza di mutui in danaro,  ha  ritenuto  che,  nell'ambito  dei  &#13;
 trasferimenti   mediante   aggiudicazione   ai   pubblici  incanti,  il  &#13;
 trattamento riservato a  quelle  vendite  non  trovasse  alcuna  logica  &#13;
 giustificazione e che, di conseguenza, il sistema dell'accertamento del  &#13;
 valore  venale, per i beni trasferiti ai pubblici incanti in dipendenza  &#13;
 di  mutui  in  danaro,  dovesse  ritenersi illegittimo (sent. n. 59 del  &#13;
 1970).                                                                   &#13;
     3. - L'orientamento della Corte  sul  fondamento  e    l'ambito  di  &#13;
 applicazione della deroga alla ammissibilità del  giudizio di stima da  &#13;
 parte  degli  uffici  finanziari, consente di individuare i presupposti  &#13;
 necessari alla soluzione della questione in esame.                       &#13;
     Al riguardo occorre osservare che anche per le vendite coatte senza  &#13;
 incanto,  disciplinate  dagli  artt.  570  e  seguenti  del  codice  di  &#13;
 procedura  civile,  non  sono  contestabili  l'autenticità  del prezzo  &#13;
 pagato e la sua presumibile corrispondenza al prezzo di  mercato:  ciò  &#13;
 avviene  grazie  a  un procedimento di determinazione del valore venale  &#13;
 che, per essere posto sotto il controllo del giudice dell'esecuzione, e  &#13;
 subordinato a rigorose forme di pubblicità, presenta ampie garanzie di  &#13;
 oggettività e di automatismo per la realizzazione del  massimo  ricavo  &#13;
 possibile.                                                               &#13;
     È  evidente  quindi  che  anche  per i beni soggetti ad esecuzione  &#13;
 forzata venduti senza incanto sussistono le stesse  ragioni perché  si  &#13;
 applichi  la  normativa  contenuta nel secondo comma dell'art. 50 della  &#13;
 legge del registro: ne deriva  che  la  discriminazione  attuata  dalla  &#13;
 norma  impugnata  nell'ambito  dell'espropriazione forzata, tra vendite  &#13;
 realizzate con il sistema dell'incanto e vendite senza incanto, essendo  &#13;
 priva  di  ogni   fondamento   razionale,   deve   essere   considerata  &#13;
 costituzionalmente illegittima.                                          &#13;
     La  preclusione,  che  all'amministrazione  finanziaria viene così  &#13;
 imposta, nel procedere all'accertamento del valore dei beni trasferiti,  &#13;
 riguarda esclusivamente il sistema delle vendite degli  immobili  senza  &#13;
 incanto disciplinato dagli artt. 570 e seguenti del codice di procedura  &#13;
 civile,   che   risulta  applicabile  anche  in  sede  di  liquidazione  &#13;
 dell'attivo fallimentare, in virtù del  richiamo  contenuto  nell'art.  &#13;
 108  del  r.d.  16  marzo 1942, n. 267. Restano invece escluse, come è  &#13;
 ovvio, le vendite e le aggiudicazioni effettuate a trattativa  privata,  &#13;
 in  ordine  alle quali non sussistono i presupposti e le garanzie circa  &#13;
 la corrispondenza tra il prezzo stabilito dalle parti e il  valore  del  &#13;
 bene  in comune commercio, che giustificano, come s'è detto, l'esonero  &#13;
 dal giudizio di congruità.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la illegittimità costituzionale dell'art.  50,    secondo  &#13;
 comma,  della  legge del registro, approvata con r.d. 30 dicembre 1923,  &#13;
 n. 3269, nella parte in cui  non  dispone  che  anche  per  le  vendite  &#13;
 forzate  senza  incanto, effettuate ai sensi degli artt. 570 e seguenti  &#13;
 del codice di procedura civile, la tassa proporzionale  è  dovuta  sul  &#13;
 prezzo di aggiudicazione.                                                &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 giugno 1976.         &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO.                            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
