<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-01-14/2/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-01-14/2"/>
          <FRBRalias value="ECLI:IT:COST:1982:2" name="ECLI"/>
          <FRBRdate date="1982-01-14" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="2"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-01-14/2/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-01-14/2/ita@"/>
          <FRBRdate date="1982-01-14" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-01-14/2/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-01-14/2/ita@.xml"/>
          <FRBRdate date="1982-01-14" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1982-01-07" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1982-01-14" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="ELIA" href="/akn/ontology/person/it/ELIA" showAs="ELIA"/>
        <TLCPerson eId="leopoldoElia" href="/akn/ontology/person/it/leopoldoElia" showAs="LEOPOLDO ELIA"/>
        <TLCPerson eId="edoardoVolterra" href="/akn/ontology/person/it/edoardoVolterra" showAs="EDOARDO VOLTERRA"/>
        <TLCPerson eId="micheleRossano" href="/akn/ontology/person/it/micheleRossano" showAs="MICHELE ROSSANO"/>
        <TLCPerson eId="guglielmoRoehrssen" href="/akn/ontology/person/it/guglielmoRoehrssen" showAs="GUGLIELMO ROEHRSSEN"/>
        <TLCPerson eId="oronzoReale" href="/akn/ontology/person/it/oronzoReale" showAs="ORONZO REALE"/>
        <TLCPerson eId="albertoMalagugini" href="/akn/ontology/person/it/albertoMalagugini" showAs="ALBERTO MALAGUGINI"/>
        <TLCPerson eId="livioPaladin" href="/akn/ontology/person/it/livioPaladin" showAs="LIVIO PALADIN"/>
        <TLCPerson eId="antonioLaPergola" href="/akn/ontology/person/it/antonioLaPergola" showAs="ANTONIO LA PERGOLA"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="VIRGILIO ANDRIOLI"/>
        <TLCPerson eId="giuseppeFerrari" href="/akn/ontology/person/it/giuseppeFerrari" showAs="GIUSEPPE FERRARI"/>
        <TLCPerson eId="giovanniVitale" href="/akn/ontology/person/it/giovanniVitale" showAs="GIOVANNI VITALE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1982</akn4cc:anno>
        <akn4cc:numero>2</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1982:2</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>ELIA</akn4cc:presidente>
        <akn4cc:relatore>Alberto Malagugini</akn4cc:relatore>
        <akn4cc:data_decisione>1982-01-07</akn4cc:data_decisione>
        <akn4cc:data_deposito>1982-01-14</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge>, Presidente - Prof. <judge refersTo="#edoardoVolterra">EDOARDO VOLTERRA</judge> - Dott. <judge refersTo="#micheleRossano">MICHELE ROSSANO</judge> - Prof. <judge refersTo="#guglielmoRoehrssen">GUGLIELMO ROEHRSSEN</judge> - 
 Avv. <judge refersTo="#oronzoReale">ORONZO REALE</judge> - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. <judge refersTo="#albertoMalagugini">ALBERTO 
 MALAGUGINI</judge> - Prof. <judge refersTo="#livioPaladin">LIVIO PALADIN</judge> - Prof. <judge refersTo="#antonioLaPergola">ANTONIO LA PERGOLA</judge> - Prof. <judge refersTo="#virgilioAndrioli">VIRGILIO ANDRIOLI</judge> - Prof. <judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge>, Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 26, lett.  a,  
 ultima parte, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974-05-31/417/!main">d.P.R. 31 maggio 1974, n. 417</ref> (concorsi  a  posti  di  
 preside  di  scuola  media),  promosso con ordinanza emessa il 16 marzo  
 1979 dal Tribunale amministrativo regionale della Calabria sul  ricorso  
 proposto  da  Greco Ettore contro il Provveditore agli studi di Cosenza  
 ed altro, iscritta al n. 568 del registro ordinanze 1979  e  pubblicata  
 nella Gazzetta Ufficiale della Repubblica n. 265 del 26 settembre 1979.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  
 ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udito nell'udienza pubblica del 7 ottobre 1981 il Giudice  relatore  
 Alberto Malagugini;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udito  l'avvocato dello Stato Sergio Laporta, per il Presidente del  
 Consiglio dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto:</heading>
          <paragraph>
            <content>
              <p>Con ricorso notificato il 21-22 giugno 1978 il prof. Greco  Ettore,  
 insegnante  titolare  di  educazione  tecnica maschile presso la scuola  
 media, impugnava presso il TAR della Calabria, sede  di  Catanzaro,  il  
 provvedimento  in  data  16  giugno 1978 del Provveditore agli studi di  
 Cosenza che  lo  aveva  escluso  dalla  graduatoria  provinciale  degli  
 aspiranti  all'incarico  di  preside  nella  scuola  media  per  l'anno  
 scolastico 1978/79, in quanto in possesso di laurea - in  sociologia  -  
 che  non  consentiva di partecipare al concorso a preside per la scuola  
 media, perché conseguita dopo l'entrata in vigore  del  D.M.  2  marzo  
 1972 sulle nuove classi di concorsi a cattedra.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Decidendo sul ricorso il TAR rilevava anzitutto che l'O.M. 28 marzo  
 1978, applicata col provvedimento impugnato, richiamava integralmente i  
 requisiti generali e particolari prescritti rispettivamente dagli artt.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>24</num>
            <content>
              <p>  e  26  del  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974-05-31/417/!main">d.P.R. 31 maggio 1974, n. 417</ref>, il quale ultimo dispone,  
 alla lettera a), che sono ammessi ai concorsi a  posti  di  preside  di  
 scuola media "gli insegnanti di ruolo della scuola media forniti di una  
 delle  lauree richieste per l'ammissione ai concorsi a cattedre in tale  
 tipo di scuola" - secondo la disciplina  contenuta  nel  D.M.  2  marzo  
 1972,  che  non  vi  include  la  laurea  in  sociologia - "nonché gli  
 insegnanti di ruolo di educazione fisica forniti di laurea".  Escludeva  
 inoltre,  -  in  punto  di  rilevanza - da un lato che a sostegno della  
 pretesa del Greco potesse farsi ricorso a norme previgenti  (<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1966-11-21/1298/!main">d.P.R.  21  
 novembre  1966,  n.  1298</ref>;  <ref href="/akn/it/act/regioDecreto/stato/1923-05-06/1054/!main#art_12">art.  12  R.D.    6  maggio  1923, n. 1054</ref>,  
 modificato dal D.L.<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">C.P.</ref>S. 21 aprile 1947,  n.  629  e  dalla  <ref href="/akn/it/act/legge/stato/1962-05-25/545/!main">legge  25  
 maggio  1962,  n.  545</ref>)  essendo  quelle  sopra richiamate disposizioni  
 vincolanti  e  non  esistendo  norme  transitorie  per   la   specifica  
 questione;   dall'altro  che  il  ricorrente  potesse  vantare  diritti  
 acquisiti in relazione al fatto che avrebbe avuto titolo, in base  alla  
 precedente  regolamentazione, a partecipare al concorso se questo fosse  
 stato bandito prima dell'entrata in  vigore  del  decreto  delegato  n.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>417</num>
            <content>
              <p>/74.  Il  TAR  reputava  peraltro  non  manifestamente  infondata la  
 questione di costituzionalità del citato art. 26 lett. a), prospettata  
 in  subordine  dal  ricorrente,  assumendo  non  potersi  escludere  la  
 violazione  dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.  3  Cost.</ref>  nel  trattamento differenziato da tale  
 norma riservato da un lato al titolare di educazione tecnica -  ammesso  
 al concorso a preside solo se in possesso di una delle lauree richieste  
 per i concorsi a cattedra nella scuola media - e dall'altro al titolare  
 di  educazione  fisica,  ammesso allo stesso concorso se in possesso di  
 una laurea qualsiasi. A tale differenziazione, ad avviso del  TAR,  non  
 corrisponde  una diversità di situazione di fatto e di diritto dei due  
 soggetti, essendovi in entrambi  i  casi  "identità  di  funzione",  e  
 quindi    "omogeneità    di    situazioni   che   postulerebbero   una  
 regolamentazione legislativa" unitaria e coerente.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'ordinanza  notificata  e  comunicata  come  per   legge,   veniva  
 pubblicata nella Gazzetta Ufficiale n. 265 del 26 settembre 1979.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nel  giudizio  interveniva,  in  rappresentanza  del Presidente del  
 Consiglio dei ministri, l'Avvocatura Generale  dello  Stato,  chiedendo  
 che  la  questione  fosse  dichiarata (anche manifestamente) infondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'Avvocatura osservava anzitutto che, per una corretta applicazione del  
 principio di uguaglianza "non è sufficiente rilevare una diversità di  
 trattamento  per  desumerne,  come  meccanicamente  conseguenziale,  la  
 necessità di operare una elevazione del trattamento meno favorevole al  
 livello  del  trattamento  più  favorevole"  ma  "è  necessario anche  
 individuare il cosiddetto  tertium  comparationis,  ossia  la  norma  o  
 principio  generale  da  assumere  -  o  perché  di  immediato rilievo  
 costituzionale  o  perché  più  consono  ai  "valori"affermati  anche  
 implicitamente   dalla  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>  -  come  termine  di  riferimento  
 indicativo del livello al quale l'eguaglianza deve essere  assicurata".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nella  specie,  sarebbe  idonea  ad  assumere  il  ruolo  di  misura di  
 comparazione la norma che richiede,  per  il  concorso  a  preside,  il  
 possesso  di  una delle lauree richieste per l'ammissione ai concorsi a  
 cattedra nel tipo di scuola  considerato  (art.  26,  lett.  a),  prima  
 parte);  non  invece la norma che ammette "gli insegnanti di educazione  
 fisica forniti di laurea". La  continuità  tra  laurea  richiesta  per  
 l'insegnamento  e  laurea  richiesta  per  la  presidenza è infatti un  
 valore che merita di essere  conservato,  essendo  stato  costantemente  
 ritenuto conforme all'interesse dell'istruzione pubblica che il preside  
 abbia  una  formazione e qualificazione professionale omogenea a quella  
 del personale docente (cfr. art. 1 D.L. <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">C.P.</ref>S. 21 aprile 1947, n.  629,  
 come modificato dall'<ref href="/akn/it/act/legge/stato/1962-05-25/545/!main#art_1">art. 1 della legge 25 maggio  1962,  n.  545</ref>);  ed  
 essendo  d'altra  parte  del  tutto  razionale  che per quest'ultimo si  
 distingua tra tipi di laurea, perché non tutte ugualmente valide  alla  
 formazione  di un buon docente. Non altrettanto potrebbe dirsi, invece,  
 della seconda disposizione, dettata per i soli insegnanti di educazione  
 fisica,  avendo  questa  natura eccezionale e quindi non estensibile ad  
 altre categorie di docenti.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'origine  di  tale  particolare  norma  si   colloca   -   secondo  
 l'Avvocatura - nell'ambito di un'evoluzione dell'assetto delle carriere  
 degli  insegnanti  di  educazione fisica, che in base all'<ref href="/akn/it/act/legge/stato/1958-02-07/88/!main#art_72">art. 72 della  
 legge 7 febbraio 1958, n. 88</ref> erano tutti inquadrati nel ruolo B,  senza  
 distinguere  -  come per gli altri insegnanti - tra ruolo A e ruolo B a  
 seconda  che  operassero  nelle  secondarie  superiori  o  nelle  medie  
 inferiori.  Tale  inquadramento fu modificato con l'<ref href="/akn/it/act/decretoLegge/stato/1971-01-30/13/!main#art_16">art. 16 del D.L. 30  
 gennaio 1971, n. 13</ref> (convertito, con modificazioni proprio all'art. 16,  
 con la <ref href="/akn/it/act/legge/stato/1976-03-30/88/!main">legge 30 marzo 1976, n. 88</ref>) mediante  il  passaggio  "riservato"  
 nella categoria dei docenti delle secondarie superiori (ex gruppo A) di  
 buona  parte  degli  insegnanti  di educazione fisica in servizio ed il  
 dirottamento nella scuola media dei giovani vincitori di concorso.  Nel  
 quadro  di  questo  indirizzo,  già  anticipato  dalle  forze  sociali  
 all'epoca della redazione del d.P.R. 417/74, si ritenne con la norma in  
 esame di ammettere al concorso a preside di scuola media gli insegnanti  
 di educazione fisica,  assumendo  come  fattore  determinante  il  loro  
 livello  di  inquadramento,  giacché all'epoca erano di gruppo B anche  
 gli altri insegnanti di tale tipo di scuola.  Il  legislatore  delegato  
 ritenne  peraltro  incongruo  -  secondo quanto riferito dal Ministro -  
 richiedere il possesso di una delle lauree per gli insegnamenti  propri  
 della scuola media, in quanto gli insegnanti di educazione fisica erano  
 inquadrati  in un ruolo non attinente unicamente a tale tipo di scuola.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>D'altra parte  per  costoro  -  non  esistendo  corsi  universitari  di  
 educazione  fisica  -  la  laurea  ha  solo  funzione  di  integrazione  
 culturale e non anche, come per  gli  altri  docenti,  di  preparazione  
 professionale  specifica:  sicché sotto questo profilo, vi sarebbe, ad  
 avviso dell'Avvocatura, una diversità di situazioni oggettive idonea a  
 giustificare la particolare disposizione in oggetto.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto:</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p> - Il Tribunale Regionale Amministrativo  della  Calabria  dubita  
 della  legittimità  costituzionale  dell'art.  26,  lettera a), ultima  
 parte, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974-05-31/417/!main">d.P.R. 31 maggio 1974, n. 417</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La disposizione di legge parzialmente denunziata così recita nella  
 sua interezza: "Ai concorsi a posti di preside della scuola media  sono  
 ammessi:  a)  gli insegnanti di ruolo della scuola media forniti di una  
 delle lauree richieste per l'ammissione ai concorsi a cattedra in  tale  
 tipo  di  scuola,  nonché gli insegnanti di ruolo di educazione fisica  
 forniti di laurea".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il giudice a quo chiamato a decidere sul ricorso di  un  insegnante  
 di  ruolo  di  educazione  tecnica  nella  scuola  media, escluso dalla  
 graduatoria provinciale degli aspiranti all'incarico di preside perché  
 fornito di laurea, in sociologia, non compresa tra quelle richieste per  
 l'ammissione ai  concorsi  a  cattedra  in  tale  tipo  di  scuola,  ha  
 ravvisato  un  possibile  vizio di costituzionalità, per contrasto con  
 l'art. 3, primo comma,  Cost.,  nel  diverso  e  deteriore  trattamento  
 riservato  dal legislatore all'insegnante di educazione tecnica fornito  
 di laurea non specifica rispetto all'insegnante di  educazione  fisica,  
 munito  di  uguale titolo di studio; entrambi insegnanti di ruolo nella  
 scuola media, entrambi forniti di laurea generica, ma escluso il  primo  
 ammesso,  invece,  il secondo al concorso (o alla graduatoria) ai posti  
 di preside nella scuola stessa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La questione non è fondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>  -  Dal  complesso  delle  disposizioni  disciplinanti  lo stato  
 giuridico del personale (direttivo, ispettivo, docente e  non  docente)  
 della  scuola (materna, elementare, secondaria e artistica) dello Stato  
 è agevole dedurre il criterio generale  fissato  dal  legislatore  sul  
 punto  specifico  riguardante  l'accesso alle funzioni di preside della  
 scuola  media.  La  regola  dettata  al  proposito  esige,  oltre   che  
 l'appartenenza  ai  ruoli  della scuola media, il possesso di una delle  
 lauree richieste per l'ammissione ai concorsi a cattedre  nella  scuola  
 della cui direzione si tratta.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La  prescrizione  di cui al denunziato art. 26, lett. a) del d.P.R.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>n. 417 del 1974 sembra intesa a garantire,  in  relazione  al  tipo  di  
 scuola  considerato,  il  migliore  svolgimento  delle  funzioni cui il  
 personale direttivo deve attendere, quali  fissate  in  linea  generale  
 dall'art. 6 del medesimo d.P.R.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>A  tal  fine,  è stato ritenuto rilevante il possesso di una delle  
 lauree richieste per l'insegnamento nella medesima  scuola,  in  quanto  
 tale  da  garantire  una  formazione e qualificazione professionale del  
 preside omogenea a quella del  personale  docente  e  quindi  una  più  
 sicura  capacità  del  preside  stesso,  che  deve sovraintendere alla  
 scuola soprattutto sotto il  profilo  didattico  (cfr.  <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1976/228/!main">sent.  228  del  
 1976</ref>),  di  promuovere e coordinare le varie attività di insegnamento,  
 valutandone anche le eventuali carenze. Più  in  generale,  la  regola  
 assunta  corrisponde  alla  convinzione  che  non  tutte le lauree sono  
 parimenti valide per la formazione di un buon docente di un  dato  tipo  
 di scuola; e, se così è, ad uguale se non a maggior ragione lo stesso  
 criterio  va  adottato  per  la  formazione  del  preside  della scuola  
 medesima.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p> - Vero è che il giudice a quo non sembra muovere alcuna censura  
 né  nel  dispositivo,  né   nella   motivazione   dell'ordinanza   di  
 remissione,  alla  regola  generale  posta dal legislatore delegato del  
 1974 nella soggetta materia.   Neppure  egli  denunzia  -  e  una  tale  
 questione  sarebbe  stata irrilevante l'art. 26 lettera a) ultima parte  
 del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974/417/!main">d.P.R. n. 417 del 1974</ref> per ciò che in esso è stabilita una deroga  
 in favore degli insegnanti di ruolo  di  educazione  fisica  muniti  di  
 laurea  quando  anche  diversa  da  quella richiesta per l'ammissione a  
 concorsi di cattedra nella medesima scuola.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Anzi, della norma derogatoria tanto è presupposta la  legittimità  
 costituzionale  che  essa  viene  assunta  a termine di riferimento nel  
 giudizio comparativo di uguaglianza, chiedendosene  l'estensione  anche  
 agli insegnanti di ruolo di altre discipline che versino nella medesima  
 condizione e siano cioè muniti di laurea, per così dire, generica.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ma  una questione di legittimità costituzionale per violazione del  
 principio di uguaglianza, in presenza di  norme  generali  e  di  norme  
 derogatorie,  come  nel caso in esame, in tanto può porsi in quanto si  
 assuma che queste ultime,  e  cioè  le  norme  derogatorie,  poste  in  
 relazione  alle  prime,  e  cioè  alle  norme generali, manifestino un  
 contrasto con l'art. 3, primo comma, Cost.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Quando, invece, si assume a termine di raffronto  del  giudizio  di  
 uguaglianza  la  norma  derogatrice,  la  questione  così  posta ha in  
 realtà  per  oggetto  la  norma  generale,  regolatrice  anche   della  
 fattispecie  iudicanda,  che  si  vorrebbe  sottratta  alla disciplina,  
 appunto, generale, con essa dettata.   Detto in altre parole  -  e  con  
 riguardo  al  profilo  della rilevanza nel caso di specie - la norma di  
 cui il giudice a quo è chiamato a fare applicazione è quella generale  
 di  cui all'art. 26, lett. a), prima parte, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974/417/!main">d.P.R. n. 417 del 1974</ref>,  
 che ammette ai concorsi a posti  di  preside  della  scuola  media  gli  
 insegnanti  di  ruolo  delle  scuole stesse forniti di una delle lauree  
 richieste per l'ammissione ai concorsi  a  cattedra  in  tale  tipo  di  
 scuola   media;   norma  generale  che,  per  il  suo  contenuto,  osta  
 all'accoglimento della pretesa del ricorrente. Ed è appena il caso  di  
 rilevare   che,  anche  da  un  punto  di  vista  logico,  il  chiedere  
 l'ammissione ai concorsi a posti di preside della  scuola  media  degli  
 insegnanti   di   ruolo   forniti  di  una  qualsiasi  laurea  equivale  
 esattamente a voler cancellare, per quanto concerne il titolo di studio  
 richiesto, la norma generale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ciò basta a  far  ritenere  infondata  la  predetta  questione  di  
 costituzionalità,  anche  a  prescindere  dalle  considerazioni  sopra  
 svolte sub 2), che comunque varrebbero ad  escludere  un  arbitrio  del  
 legislatore  - e, quindi una ingiustificata differenza di trattamento -  
 nel fissare per l'accesso ai posti di preside  nella  scuola  media  il  
 requisito del possesso di una delle lauree richieste per l'ammissione a  
 concorsi a cattedra in tale tipo di scuola.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara  non  fondata  la questione di legittimità costituzionale  
 dell'art. 26, lettera a), ultima parte del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974-05-01/3/!main">d.P.R.  31 maggio  1974</ref>,  n.</block>
        <paragraph>
          <num>417</num>
          <content>
            <p>,   sollevata,   in  riferimento  all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.  3  Cost.</ref>  dal  Tribunale  
 Amministrativo Regionale della Calabria  con  l'ordinanza  indicata  in  
 epigrafe.</p>
          </content>
        </paragraph>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1982-01-07">7 gennaio 1982</date>.</block>
      <p>F.to:   <signature><judge refersTo="#leopoldoElia">LEOPOLDO   ELIA</judge></signature>   -   <signature><judge refersTo="#edoardoVolterra">EDOARDO  
                                   VOLTERRA</judge></signature>    -   <signature><judge refersTo="#micheleRossano">MICHELE   ROSSANO</judge></signature>   -  
                                   <signature><judge refersTo="#guglielmoRoehrssen">GUGLIELMO ROEHRSSEN</judge></signature> - <signature><judge refersTo="#oronzoReale">ORONZO REALE</judge></signature>  -  
                                   BRUNETTO  BUCCIARELLI DUCCI - <signature><judge refersTo="#albertoMalagugini">ALBERTO  
                                   MALAGUGINI</judge></signature> - <signature><judge refersTo="#livioPaladin">LIVIO PALADIN</judge></signature> -  <signature><judge refersTo="#antonioLaPergola">ANTONIO  
                                   LA  PERGOLA</judge></signature>  -  <signature><judge refersTo="#virgilioAndrioli">VIRGILIO  ANDRIOLI</judge></signature>  -  
                                   <signature><judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge></signature>.</p>
      <p>
        <signature><person refersTo="#giovanniVitale" as="#cancelliere">GIOVANNI VITALE</person> - <role refersTo="#cancelliere">Cancelliere</role></signature>
      </p>
    </conclusions>
  </judgment>
</akomaNtoso>
