<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2019</anno_pronuncia>
    <numero_pronuncia>252</numero_pronuncia>
    <ecli>ECLI:IT:COST:2019:252</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CAROSI</presidente>
    <relatore_pronuncia>Daria de Pretis</relatore_pronuncia>
    <redattore_pronuncia>Daria de Pretis</redattore_pronuncia>
    <data_decisione>06/11/2019</data_decisione>
    <data_deposito>04/12/2019</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Aldo CAROSI; Giudici : Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 4 della legge della Regione Sardegna 5 novembre 2018, n. 41, recante «Modifiche alla legge regionale 13 marzo 2018, n. 8 (Nuove norme in materia di contratti pubblici di lavori, servizi e forniture)», promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 28 dicembre 2018-4 gennaio 2019, depositato in cancelleria il 7 gennaio 2019, iscritto al n. 1 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 5, prima serie speciale, dell'anno 2019.&#13;
 Visto l'atto di costituzione della Regione autonoma Sardegna;&#13;
 udito nella camera di consiglio del 6 novembre 2019 il Giudice relatore Daria de Pretis.&#13;
 Ritenuto che con ricorso notificato il 28 dicembre 2018-4 gennaio 2019, depositato in cancelleria il 7 gennaio 2019 e iscritto al n. 1 del registro ricorsi 2019, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato l'art. 4 della legge della Regione Sardegna 5 novembre 2018, n. 41, recante «Modifiche alla legge regionale 13 marzo 2018, n. 8 (Nuove norme in materia di contratti pubblici di lavori, servizi e forniture)»;&#13;
 che la disposizione impugnata, alla lettera a) del comma 1, ha sostituito il comma 2 dell'art. 23 della legge reg. Sardegna n. 8 del 2018 con il seguente: «Le stazioni appaltanti, per l'affidamento dei servizi [di ingegneria e architettura] di cui al comma 1, in conformità agli articoli 36 e 157 del decreto legislativo n. 50 del 2016, selezionano gli operatori economici iscritti nell'elenco di cui al presente articolo, nel rispetto, ove possibile, del principio della rotazione degli inviti e con l'applicazione di criteri oggettivi che tengono conto della loro pregressa capacità tecnico-professionale ed, eventualmente, economico-finanziaria»;&#13;
 che, per quanto qui rileva, il nuovo testo normativo prevede, a differenza di quello sostituito, che gli operatori economici siano selezionati dalle stazioni appaltanti nel rispetto del principio di rotazione degli inviti «ove possibile»;&#13;
 che tale previsione eccederebbe dalle competenze attribuite alla Regione autonoma Sardegna dalla legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna), invadendo la competenza legislativa esclusiva dello Stato in materia di «tutela della concorrenza» e di «ordinamento civile», di cui all'art. 117, secondo comma, lettere e) e l), della Costituzione;&#13;
 che lo statuto di autonomia, all'art. 3, lettera e), attribuirebbe sì alla Regione la potestà legislativa primaria in materia di «lavori pubblici di esclusivo interesse della Regione», ma non conterrebbe alcuno specifico riferimento alle forniture e ai servizi;&#13;
 che la disciplina delle procedure di gara, della qualificazione e selezione dei concorrenti e dei criteri di aggiudicazione negli appalti pubblici sarebbe riconducibile alla materia della «tutela della concorrenza», in quanto volta a garantire il rispetto delle regole concorrenziali e dei principi comunitari della libera circolazione delle merci, della libera prestazione dei servizi e della libertà di stabilimento, nonché dei principi costituzionali di trasparenza e di parità di trattamento, mentre la disciplina della fase di esecuzione del rapporto contrattuale con la pubblica amministrazione, connotato dalla normale mancanza di poteri autoritativi in capo al soggetto pubblico, dovrebbe ricondursi alla materia «ordinamento civile»;&#13;
 che lo Stato avrebbe esercitato tali competenze adottando prima il decreto legislativo 12 aprile 2006, n. 163 (Codice dei contratti pubblici relativi a lavori, servizi e forniture in attuazione delle direttive 2004/17/CE e 2004/18/CE) e poi il decreto legislativo 18 aprile 2016, n. 50 (Codice dei contratti pubblici), che regola ora la materia e alla cui disciplina, secondo quanto dispone l'art. 2, comma 3, dello stesso d.lgs. n. 50 del 2016, dovrebbe conformarsi anche la legislazione delle regioni ad autonomia speciale;&#13;
 che la nuova formulazione dell'art. 23, comma 2, della legge reg. Sardegna n. 8 del 2018 renderebbe solo eventuale («ove possibile») l'applicazione del principio della rotazione degli inviti e non indicherebbe le ragioni che ne giustificano la deroga, non consentendo di controllare la legittimità delle scelte effettuate dalle stazioni appaltanti;&#13;
 che ciò contrasterebbe con la disciplina dettata per i contratti «sotto soglia» dall'art. 36 del d.lgs. n. 50 del 2016, alla cui stregua il principio di rotazione opererebbe con riferimento «all'aggiudicazione» per i contratti di importo inferiore a 40.000 euro, in relazione ai quali è consentito l'affidamento diretto senza confronto concorrenziale, mentre per gli affidamenti «di valore superiore ai 40.000 (e sino a 150.000 euro)», per i quali è necessario promuovere «un confronto concorrenziale tra più ditte invitate dalla stazione appaltante (almeno cinque in caso di servizi e forniture, almeno dieci in caso di lavori)», il principio di rotazione «oper[erebbe] (esclusivamente) con riferimento alla fase degli inviti»;&#13;
 che una deroga motivata al principio di rotazione sarebbe consentita dalle «Linee guida n. 4», adottate dall'Autorità nazionale anticorruzione (ANAC) in attuazione del d.lgs. n. 50 del 2016 e recanti «Procedure per l'affidamento dei contratti pubblici di importo inferiore alle soglie di rilevanza comunitaria, indagini di mercato e formazione e gestione degli elenchi di operatori economici», solo in caso di riscontrata effettiva assenza di alternative, sicché l'invito all'affidatario uscente dovrebbe rivestire carattere eccezionale ed essere adeguatamente motivato avuto riguardo al numero ridotto di operatori presenti sul mercato, al grado di soddisfazione maturato a conclusione del precedente rapporto contrattuale ovvero all'oggetto ed alle caratteristiche del mercato di riferimento, come avrebbe chiarito la Commissione speciale del Consiglio di Stato con parere del 12 febbraio 2018, espresso nell'ambito dell'istruttoria relativa all'aggiornamento di dette linee guida;&#13;
 che, pertanto, la disposizione regionale impugnata, pur richiamando gli artt. 36 e 157 del d.lgs. n. 50 del 2016, contrasterebbe «di fatto» con essi;&#13;
 che la Regione autonoma Sardegna si è costituita in giudizio con atto depositato il 12 febbraio 2019, concludendo per l'inammissibilità e l'infondatezza delle questioni, con riserva di esporre le argomentazioni difensive in una successiva memoria;&#13;
 che nella memoria depositata il 20 marzo 2019 la Regione ha chiesto a questa Corte di dichiarare cessata la materia del contendere, essendo medio tempore entrata in vigore la legge della Regione Sardegna 11 febbraio 2019, n. 7 (Modifica alla legge regionale n. 48 del 2018 in materia di assunzione di personale da parte dei consorzi di bonifica e alla legge regionale n. 8 del 2018 in materia di contratti pubblici), che all'art. 2 ha modificato l'art. 23, comma 2, della legge reg. Sardegna n. 8 del 2018, come sostituito dalla norma impugnata, disponendo la soppressione delle parole «ove possibile»;&#13;
 che il Presidente del Consiglio dei ministri, previa conforme deliberazione del Consiglio dei ministri del 10 ottobre 2019, ha rinunciato al ricorso con atto depositato il 15 ottobre 2019, spedito per la notificazione il 14 ottobre 2019;&#13;
 che la Regione autonoma Sardegna ha depositato il 25 ottobre 2019 la deliberazione della Giunta regionale del 22 ottobre 2019, con la quale è stata accettata la rinuncia.&#13;
 Considerato che, con riguardo alle questioni proposte, vi è stata rinuncia da parte del Presidente del Consiglio dei ministri, previa conforme deliberazione del Consiglio dei ministri;&#13;
 che la rinuncia è stata accettata dalla Regione autonoma Sardegna;&#13;
 che, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, nei giudizi di legittimità costituzionale in via principale la rinuncia al ricorso, accettata dalla controparte costituita, determina l'estinzione del processo.&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e gli artt. 9, comma 2, e 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara estinto il processo.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 novembre 2019.&#13;
 F.to:&#13;
 Aldo CAROSI, Presidente&#13;
 Daria de PRETIS, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 4 dicembre 2019.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
