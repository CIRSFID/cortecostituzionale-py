<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>142</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:142</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Giuseppe Ferrari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/06/1981</data_decisione>
    <data_deposito>21/07/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. &#13;
 ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - &#13;
 Prof. ANTONIO LA PERGOLA - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 28, secondo  &#13;
 comma,  della  legge  approvata  il  1  agosto  1980,  recante   "Norme  &#13;
 integrative  in materia di agricoltura e foreste", promosso con ricorso  &#13;
 del Commissario dello Stato per la Regione  Sicilia,  notificato  il  9  &#13;
 agosto  1980, depositato in cancelleria il 14 successivo ed iscritto al  &#13;
 n. 17 del registro ricorsi 1980.                                         &#13;
     Visto l'atto di costituzione della Regione Sicilia;                  &#13;
     udito nell'udienza pubblica del 3 giugno 1981 il  Giudice  relatore  &#13;
 Giuseppe Ferrari;                                                        &#13;
     udito  l'avvocato  dello  Stato Franco Chiarotti, per il Presidente  &#13;
 del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     La vigente disciplina statale in tema di contratti  della  pubblica  &#13;
 amministrazione  (artt.  5  e  6  del  r.d.  18 novembre 1923, n. 2440,  &#13;
 recante  disposizioni  sull'amministrazione  del  patrimonio  e   sulla  &#13;
 contabilità  generale  dello  Stato,  come  modificato dall'art. 7 del  &#13;
 d.P.R. 30 giugno 1972, n. 748)  prescrive  che  su  tutti  i  contratti  &#13;
 eccedenti  determinati  importi  deve  essere  sentito  il Consiglio di  &#13;
 Stato.   Nella    Regione    siciliana,    invece,    ferma    restando  &#13;
 l'obbligatorietà  del  parere, al Consiglio di Stato si sostituisce il  &#13;
 Consiglio di giustizia  amministrativa,  che  in  quell'ordinamento  è  &#13;
 l'organo  di consulenza in virtù dell'art. 1 del decreto legislativo 6  &#13;
 maggio 1948, n. 654. Senonché, l'Assemblea regionale siciliana,  nella  &#13;
 seduta  del  1  agosto  1980,  approvava  una  legge, dal titolo "Norme  &#13;
 integrative in materia di agricoltura e foreste", il cui art.  28, dopo  &#13;
 avere enunciato di perseguire  lo  scopo  di  riordinare  i  molteplici  &#13;
 interventi  ed  adempimenti  della  legislazione  regionale in materia,  &#13;
 autorizzava (primo comma) l'Assessore regionale competente a "stipulare  &#13;
 all'uopo apposite  convenzioni,  anche  mediante  trattative  private",  &#13;
 statuendo  altresì  (secondo  comma) che alle "predette convenzioni si  &#13;
 applica il disposto dell'art. 4 della legge regionale 6 marzo 1976,  n.  &#13;
 20  e successive aggiunte e modificazioni". E poiché tale ultima norma  &#13;
 facoltizza  l'Assessorato  regionale  dell'agricoltura  e  foreste   "a  &#13;
 stipulare  apposite  convenzioni,  anche  tramite  trattative private e  &#13;
 prescindendo dal parere del Consiglio di giustizia amministrativa",  il  &#13;
 Commissario dello Stato, con ricorso 9 agosto 1980, sollevava questione  &#13;
 di legittimità costituzionale dell'art. 28, secondo comma, della legge  &#13;
 approvata  nella  suddetta seduta del 10 agosto 1980, in relazione agli  &#13;
 artt. 14 e 17 dello Statuto speciale cd ai decreti legislativi 6 maggio  &#13;
 1948, nn. 654 e 655.                                                     &#13;
     Si costituiva in giudizio  il  Presidente  della  Giunta  regionale  &#13;
 siciliana chiedendo, in via principale, che il ricorso fosse dichiarato  &#13;
 "irricevibile o inammissibile"; in via subordinata, che fosse respinto,  &#13;
 con  la  declaratoria  della  legittimità  costituzionale  della norma  &#13;
 impugnata;  in  via  ulteriormente  subordinata,  per  quanto   dovesse  &#13;
 occorrere ai fini della inammissibilità o del rigetto del ricorso, che  &#13;
 fosse  dichiarata manifestamente infondata la questione di legittimità  &#13;
 costituzionale  dell'art.  4  del  d.l.  6  maggio  1948,  n.  654,  in  &#13;
 riferimento  agli  artt. 14, 17 e 20 dello Statuto siciliano, e fissata  &#13;
 altra  udienza  per  la  relativa  discussione,  con  ogni  conseguente  &#13;
 statuizione.<diritto>Considerato in diritto</diritto>:                          &#13;
     A sostegno del ricorso promosso dal Commissario dello Stato avverso  &#13;
 la   norma   in  epigrafe,  l'Avvocatura  dello  Stato  rileva  che  la  &#13;
 sottrazione al parere del Consiglio di giustizia  amministrativa  delle  &#13;
 convenzioni  contemplate  nel  predetto  art.  28, secondo comma, della  &#13;
 legge approvata dall'Assemblea regionale siciliana il 1 agosto 1980  in  &#13;
 materia  di agricoltura e foreste, interferisce sulla normativa statale  &#13;
 disciplinante le attribuzioni di un organo statale, qual è appunto  il  &#13;
 Consiglio  di  giustizia  amministrativa,  così  violando  le norme di  &#13;
 attuazione  dello  Statuto  speciale  riguardanti  l'esercizio,   nella  &#13;
 Regione  siciliana,  delle  funzioni spettanti al Consiglio di Stato ai  &#13;
 sensi del decreto legislativo n. 654 del 1948.                           &#13;
     Da parte sua, la difesa della Regione ha eccepito  fondamentalmente  &#13;
 che  l'autorizzazione  a  stipulare apposite convenzioni "anche tramite  &#13;
 trattative private e prescindendo dal parere del Consiglio di giustizia  &#13;
 amministrativa"  era  stata  già  conferita  all'Assessore   regionale  &#13;
 all'agricoltura  e  foreste con la legge n. 20 del 1976 (art. 4), e che  &#13;
 pertanto  il  ricorso  in  esame  deve  considerarsi  "irricevibile  e,  &#13;
 comunque,  inammissibile",  in  quanto  l'impugnativa  è,  in realtà,  &#13;
 rivolta,  non già contro la legge approvata il 1 agosto  1980,  bensì  &#13;
 contro  l'anteriore legge n. 20 del 1976, non più impugnabile ormai in  &#13;
 via principale, perché non impugnata nei termini  previsti,  come  non  &#13;
 furono  impugnate  neppure  altre  leggi  regionali che hanno anch'esse  &#13;
 fatto applicazione, anteriormente al 1 agosto  1980,  della  disciplina  &#13;
 portata  dall'art.  4  della legge regionale n.  20 del 1976. La stessa  &#13;
 difesa ha eccepito altresì che i  decreti  legislativi  di  attuazione  &#13;
 dello  Statuto,  non  assurgendo  al rango di norme costituzionali, non  &#13;
 possono essere invocati come norme di riferimento, e che, in ogni caso,  &#13;
 la  dedotta  questione  sarebbe  infondata,  spettando   alla   Regione  &#13;
 siciliana,  in  virtù  della  propria  autonomia organizzativa ed allo  &#13;
 scopo di snellire le procedure burocratiche, la potestà  di  stabilire  &#13;
 che  alcuni  atti  regionali siano assoggettati, anziché al parere del  &#13;
 Consiglio di giustizia amministrativa, a quello di un organo regionale,  &#13;
 come appunto  nella  specie  sono  state  assoggettate  al  parere  del  &#13;
 "comitato    tecnico    amministrativo    dell'Assessorato    regionale  &#13;
 dell'agricoltura e foreste" le convenzioni  di  cui  all'art.    28  in  &#13;
 questione.                                                               &#13;
     Ma  poiché  la legge recante l'impugnata norma è stata promulgata  &#13;
 dal Presidente della Regione siciliana il 12 agosto 1980, con il n. 83,  &#13;
 e pubblicata nella "Gazzetta Ufficiale della Regione siciliana"  n.  38  &#13;
 del  23  agosto  1980,  omettendosi  peraltro la parte dell'art. 28 che  &#13;
 aveva formato oggetto  di  impugnativa,  deve  dichiararsi  cessata  la  &#13;
 materia  del  contendere,  come  ha  espressamente chiesto l'Avvocatura  &#13;
 dello Stato con le deduzioni depositate il 3  febbraio  1981.  E  ciò,  &#13;
 indipendentemente  dalla  considerazione  che  l'intero  art.  28 della  &#13;
 predetta legge regionale è stato esplicitamente abrogato dall'art.   2  &#13;
 della  successiva  legge regionale 29 dicembre 1980, n. 145, pubblicata  &#13;
 nel Giornale ufficiale il giorno successivo.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara cessata la materia del contendere in ordine al ricorso col  &#13;
 quale il Commissario dello Stato per la Regione siciliana  ha  promosso  &#13;
 questione  di  legittimità costituzionale dell'art. 28, secondo comma,  &#13;
 della legge approvata   dall'Assemblea regionale  nella  seduta  del  1  &#13;
 agosto  1980,    recante "Norme integrative in materia di agricoltura e  &#13;
 foreste", in riferimento agli artt. 14 e 17 dello Statuto  speciale  ed  &#13;
 ai decreti legislativi 6 maggio 1948, nn. 654 e 655.                     &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 25 giugno 1981.         &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA   -   EDOARDO   VOLTERRA  -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   - LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - GIUSEPPE FERRARI.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
