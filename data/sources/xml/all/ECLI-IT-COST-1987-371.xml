<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>371</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:371</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1987</data_decisione>
    <data_deposito>04/11/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale degli artt. 34 e 60 del    &#13;
 d.P.R.  26  ottobre  1972,  n.   633   ("Istituzione   e   disciplina    &#13;
 dell'imposta  sul valore aggiunto") e dell'articolo unico della legge    &#13;
 7 febbraio 1979 n. 56  ("Norme  per  il  pagamento  dell'imposta  sul    &#13;
 valore  aggiunto  per  la  vendita  della carne macellata proveniente    &#13;
 dagli allevamenti diretti ed effettuata direttamente  dai  produttori    &#13;
 agricoli-allevatori");  promossi  con  n.  4  ordinanze  emesse il 30    &#13;
 novembre 1984 dalla Commissione tributaria di primo grado di Potenza,    &#13;
 iscritte  rispettivamente  ai  nn.  222,  223, 224 e 225 del registro    &#13;
 ordinanze 1985 e pubblicate nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 113- bis dell'anno 1985;                                              &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 14 ottobre 1987 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che  nel  corso di quattro giudizi concernenti i ricorsi    &#13;
 proposti dalla Società Cooperativa agricola  "La  Nostrana"  a  r.l.    &#13;
 avverso  gli avvisi di rettifica dell'Ufficio IVA di Potenza relativi    &#13;
 alle dichiarazioni  annuali  degli  anni  1979/1982,  la  Commissione    &#13;
 tributaria  di  primo  grado  di  Potenza  ha  sollevato, con quattro    &#13;
 ordinanze  di   identico   contenuto,   questioni   di   legittimità    &#13;
 costituzionale;                                                          &#13;
       a)   dell'art.   34  del  d.P.R.  26  ottobre  1972  n.  633  e    &#13;
 dell'articolo  unico  della  legge  7  febbraio  1979,  n.   56,   in    &#13;
 riferimento all'art. 45 Cost.;                                           &#13;
       b)  dell'art.  60 dello stesso d.P.R. n. 633/72, in riferimento    &#13;
 agli artt. 3, 24 e 113 Cost.;                                            &#13;
      che,  ad avviso del giudice rimettente, le norme indicate sub a)    &#13;
 violerebbero  l'art.  45  Cost.,  in  quanto,   non   prevedendo   la    &#13;
 concessione alle cooperative di produttori agricoli dell'agevolazione    &#13;
 di usufruire del regime forfettario nella commercializzazione diretta    &#13;
 degli  animali  macellati,  costituirebbero un indubbio ostacolo alla    &#13;
 nascita e allo sviluppo di società cooperative;  la  norma  indicata    &#13;
 sub  b) - secondo la quale, se il contribuente propone ricorso contro    &#13;
 l'accertamento, il pagamento dell'imposta o  della  maggiore  imposta    &#13;
 deve   essere   eseguito   per   un  terzo  dell'ammontare  accertato    &#13;
 dall'ufficio entro sessanta giorni dalla notificazione dell'avviso di    &#13;
 accertamento  -  violerebbe  a sua volta gli artt. 24 e 113 Cost., in    &#13;
 quanto il diritto di difesa non potrebbe essere condizionato da alcun    &#13;
 peso,  nonché l'art. 3, in quanto, in presenza di condizioni fiscali    &#13;
 uguali, solo il contribuente  con  possibilità  economiche  adeguate    &#13;
 potrebbe  impugnare  l'accertamento  (è richiamata la sentenza n. 21    &#13;
 del 1961 sul solve et repete);                                           &#13;
      che  il  Presidente  del Consiglio dei ministri, rappresentato e    &#13;
 difeso   dall'Avvocatura   Generale   dello   Stato,   conclude   per    &#13;
 l'inammissibilità  o  l'infondatezza  della prima questione e per la    &#13;
 manifesta infondatezza della seconda;                                    &#13;
    Considerato che i giudizi, data l'identità delle questioni, vanno    &#13;
 riuniti e congiuntamente decisi;                                         &#13;
      che,  quanto  alla  prima questione, l'art. 34 censurato prevede    &#13;
 uno speciale regime  semplificato  per  le  cessioni  di  determinati    &#13;
 prodotti  agricoli e ittici (compresi nella prima parte della Tabella    &#13;
 A allegata  al  decreto)  effettuate  da  produttori  agricoli  anche    &#13;
 associati  in  cooperative  e consorzi; mentre l'articolo unico della    &#13;
 legge n. 56 del 1979 contempla una eccezionale disciplina transitoria    &#13;
 non applicabile al caso di specie;                                       &#13;
      che  il giudice a quo chiede alla Corte di estendere lo speciale    &#13;
 regime  suindicato   all'attività,   svolta   da   cooperativa,   di    &#13;
 commercializzazione  di  carni macellate, prodotti non compresi nella    &#13;
 prima parte della tabella A allegata al d.P.R. n. 633/72;                &#13;
      che  al  riguardo  va rilevato che non spetta alla Corte, bensì    &#13;
 rientra  nella  discrezionalità  del  legislatore   valutare   quali    &#13;
 attività  e  non  altre possano rivelarsi maggiormente utili ai fini    &#13;
 del promuovimento dell'incremento della cooperazione di cui  all'art.    &#13;
 45 Cost.;                                                                &#13;
      che, quindi, la questione appare manifestamente inammissibile;      &#13;
      che  la  questione concernente l'art. 60 del d.P.R. n. 633/72 è    &#13;
 stata,  sotto  gli  stessi  profili,  già  decisa  dalla  Corte  con    &#13;
 ordinanza  n.  176  del  1985 nel senso della manifesta infondatezza,    &#13;
 sulla base della considerazione  che  la  norma  censurata  non  pone    &#13;
 alcuna   condizione  di  procedibilità  all'azione  giudiziaria,  ma    &#13;
 costituisce espressione del principio della normale esecutorietà dei    &#13;
 provvedimenti amministrativi;                                            &#13;
    Visti  gli  artt.  26  legge  11 marzo 1953, n. 87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara:                                                              &#13;
       a)  manifestamente  inammissibile  la questione di legittimità    &#13;
 costituzionale dell'art. 34 del d.P.R. 26  ottobre  1972,  n.  633  e    &#13;
 dell'articolo   unico   della  legge  7  febbraio  1979,  n.  56,  in    &#13;
 riferimento all'art. 45 Cost.;                                           &#13;
       b)   manifestamente  infondata  la  questione  di  legittimità    &#13;
 costituzionale dell'art. 60 del d.P.R. 26 ottobre 1972,  n.  633,  in    &#13;
 riferimento  agli  artt.  3,  24  e  113  Cost.;  questioni  entrambe    &#13;
 sollevate dalla Commissione tributaria di primo grado di Potenza  con    &#13;
 le ordinanze indicate in epigrafe.                                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1987.                              &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: SAJA                                 &#13;
    Depositata in cancelleria il 4 novembre 1987.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
