<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>62</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:62</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/05/1963</data_decisione>
    <data_deposito>10/05/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. GIOVANNI &#13;
 CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO &#13;
 SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 77 del D.P.R.  &#13;
 7  gennaio  1956,  n.  164, promosso con ordinanza emessa il 15 gennaio  &#13;
 1962 dal Tribunale di Parma nel procedimento penale a carico di  Cesari  &#13;
 Aldo, iscritta al n.  50 del Registro ordinanze 1962 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 99 del 14 aprile 1962.            &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udita nell'udienza pubblica del  6  marzo  1963  la  relazione  del  &#13;
 Giudice Giuseppe Verzì;                                                 &#13;
     udito  il sostituto avvocato generale dello Stato Valente Simi, per  &#13;
 il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso del procedimento penale contro Cesari Aldo, imputato  del  &#13;
 delitto  di  omicidio  colposo  e  di due distinte contravvenzioni alla  &#13;
 legge per la prevenzione degli infortuni sul lavoro nelle  costruzioni,  &#13;
 su  istanza  della difesa, il Tribunale di Parma - con ordinanza del 15  &#13;
 gennaio 1962  -  sollevava  questione  di  legittimità  costituzionale  &#13;
 dell'art.  77 del D.P.R. 7 gennaio 1956, n.  164, in relazione all'art.  &#13;
 4 della legge 12 febbraio 1955, n.  51, ed in riferimento agli artt. 76  &#13;
 e 77 della Costituzione.                                                 &#13;
     Si  osserva  nell'ordinanza  che  l'art. 4 della legge delegante 12  &#13;
 febbraio 1955, n. 51,  mentre  dispone  che  per  le  violazioni  delle  &#13;
 norme...  "potrà essere stabilita la pena dell'arresto fino a tre mesi  &#13;
 e dell'ammenda non superiore a lire 300.000", non fissa i limiti minimi  &#13;
 di pena, intendendo con ciò rinviare a quanto dispone in via  generale  &#13;
 l'art.  26  del  Codice  penale.    Invece la legge delegata stabilisce  &#13;
 all'art. 77 la pena dell'ammenda  in  misura  minima  di  lire  50.000,  &#13;
 100.000  e 200.000, a seconda delle diverse contravvenzioni, violando i  &#13;
 limiti della delega legislativa.                                         &#13;
     Riconosciuta così la non manifesta  infondatezza  e  la  rilevanza  &#13;
 della  suindicata  questione  di  legittimità costituzionale, il detto  &#13;
 Tribunale ha ordinato la sospensione del  giudizio  e  la  trasmissione  &#13;
 degli atti a questa Corte.                                               &#13;
     L'ordinanza   è   stata   regolarmente  notificata,  comunicata  e  &#13;
 pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  99  del  14  &#13;
 aprile 1962.                                                             &#13;
     Nel  presente  giudizio  vi  è  stato  soltanto  l'intervento  del  &#13;
 Presidente del Consiglio dei Ministri,  rappresentato  e  difeso  dalla  &#13;
 Avvocatura generale dello Stato.                                         &#13;
     Nelle   deduzioni   depositate   in  cancelleria  e  nella  memoria  &#13;
 illustrativa del 5 febbraio 1963, l'Avvocatura dello  Stato  deduce  la  &#13;
 manifesta  infondatezza della questione proposta, osservando che l'art.  &#13;
 26 del Codice penale si limita a fissare il  limite  minimo  e  massimo  &#13;
 dell'ammenda  senza  vincolare  per  nulla il legislatore nella pena da  &#13;
 comminare per i singoli reati. Aggiunge che la legge delegante,  avendo  &#13;
 stabilito  la sola misura massima della pena, ha inteso imporre un solo  &#13;
 limite, rimettendo al potere discrezionale del legislatore delegato  di  &#13;
 fissare  i minimi della sanzione e che non sembra possibile desumere un  &#13;
 vincolo da una legge ordinaria, come il  Codice  penale,  che  per  sua  &#13;
 natura  è pienamente derogabile anche dagli atti legislativi delegati.  &#13;
 Cita, infine, una sentenza della Corte di cassazione, che ha dichiarato  &#13;
 manifestamente infondata la stessa questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     L'eccesso di delega, che l'ordinanza del Tribunale di Parma pone  a  &#13;
 fondamento  della questione di legittimità costituzionale dell'art. 77  &#13;
 del D.P.R. 7 gennaio 1956, n.  164, non sussiste.  La  questione  sorge  &#13;
 soltanto per la interpretazione data dal Tribunale alla norma dell'art.  &#13;
 4  della  legge  12  febbraio  1955, n. 51, la quale ha posto un limite  &#13;
 massimo (lire 300.000) alla pena dell'ammenda, ma  nulla  ha  detto  in  &#13;
 merito  al  minimo  della  stessa  pena.    Secondo  il  Tribunale, col  &#13;
 silenzio, il legislatore  avrebbe  inteso  rinviare  alla  disposizione  &#13;
 dell'art.  26  del  Codice  penale,  onde  l'art.  77  suindicato - nel  &#13;
 comminare per le varie contravvenzioni ammende che vanno da  un  minimo  &#13;
 di  lire  50.QOO, di lire 100.000 e di lire 200.000 fino a quel massimo  &#13;
 fissato dalla legge delegante - avrebbe violato i limiti  della  delega  &#13;
 legislativa.                                                             &#13;
     Rileva questa Corte che la norma dell'art. 4 della legge n.  51 del  &#13;
 1955  non  si  presta  alla  interpretazione data dal Tribunale, quando  &#13;
 venga inquadrata ed esaminata nei rapporti fra legge delegante e  legge  &#13;
 delegata.                                                                &#13;
     Ed  infatti  - anche a non volere tenere conto della ampiezza della  &#13;
 delega, quale si desume dall'art. 3 della legge n. 51 del 1955 -  basta  &#13;
 considerare che l'oggetto principale della delega contenuta nell'art. 4  &#13;
 è  precisamente  quello  di  fissare le sanzioni penali per le singole  &#13;
 violazioni alle norme di prevenzione contro gli infortuni sul lavoro-il  &#13;
 che vuoi dire, in altri termini, determinare un minimo ed un massimo di  &#13;
 ammenda  entro  il  quale  il giudice possa spaziare nella applicazione  &#13;
 concreta della pena-, per  dedurre  che  già  sussiste  la  delega  di  &#13;
 fissare  il  minimo  di  ammenda.  Quando  si  tenga  presente che, nel  &#13;
 processo formativo della legge delegata, c'è sempre  uno  sviluppo  di  &#13;
 ulteriore  attività legislativa, sia pure circoscritta entro limiti di  &#13;
 tempo, di oggetto,  di  principi  e  di  cri  teri  direttivi,  mal  si  &#13;
 giustifica,  nel  silenzio della legge delegante sui minimi di ammenda,  &#13;
 il rinvio ad una disposizione di altra legge  ordinaria,  quale  è  il  &#13;
 Codice penale.                                                           &#13;
     Inoltre,  nel  sistema  del  Codice  penale,  le  disposizioni  che  &#13;
 prevedono i singoli reati, determinano,  di  regola,  per  ciascuno  di  &#13;
 essi,  il minimo ed il massimo della sanzione, spaziando entro i limiti  &#13;
 della norma generale che descrive il tipo e  le  caratteristiche  della  &#13;
 pena.  Ne  consegue,  da  un  canto,  che l'impugnato art. 77, fissando  &#13;
 minimi di pena al di sopra di quelli stabiliti dall'art. 26 del  Codice  &#13;
 penale, si è in sostanza attenuto a siffatto principio; e, dall'altro,  &#13;
 che  l'argomento  addotto dal Tribunale di Parma avrebbe potuto trovare  &#13;
 giustificazione soltanto nel caso in cui il legislatore avesse  fissato  &#13;
 minimi di ammenda inferiori a quelli previsti dall'art. 26.              &#13;
     Infine,   lo   stesso   art.   77,   che   commina,   per  ciascuna  &#13;
 contravvenzione, minimi di  pena  decisamente  elevati,  risponde  alle  &#13;
 finalità  che  il  legislatore delegante intendeva conseguire e ad una  &#13;
 direttiva implicitamente contenuta nella legge delegante. Come  risulta  &#13;
 dalle  relazioni  parlamentari,  l'ammenda fu elevata nel massimo dalle  &#13;
 lire 80.000 previste a quel tempo dal Giudice penale  a  lire  300.000,  &#13;
 allo  scopo  dichiarato  di  evitare  che  la sanzione penale rimanesse  &#13;
 inoperante e potesse "indurre  eventualmente  il  datore  di  lavoro  a  &#13;
 correre  il  rischio  di  pagare  la  penale piuttosto che sostenere il  &#13;
 maggiore onere necessario per l'adozione delle indispensabili misure di  &#13;
 sicurezza".  Se  sussisteva,  pertanto,  la  particolare  esigenza   di  &#13;
 conferire alla pena una più valida forza ed efficienza, il legislatore  &#13;
 delegato  era  tenuto  a proporzionarla anche nei rapporti fra minimo e  &#13;
 massimo e non poteva, senza frustrare lo scopo perseguito  dalla  legge  &#13;
 delegante,  partire  dal  minimo di ammenda di sole lire 160, quale era  &#13;
 quello previsto a quel tempo dall'art. 26 del Codice penale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  77  del  D.P.R.  7  gennaio  1956,  n.    164, sollevata dal  &#13;
 Tribunale di Parma con l'ordinanza del 15 gennaio  1962,  in  relazione  &#13;
 all'art.  4 della legge 12 febbraio 1955, n. 51, ed in riferimento agli  &#13;
 artt. 76 e 77 della Costituzione.                                        &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 7 maggio 1963.                                &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  - ANTONINO PAPALDO - GIOVANNI  &#13;
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  &#13;
                                   ANTONIO   MANCA  -  ALDO  SANDULLI  -  &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI - GIUSEPPE VERZI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
