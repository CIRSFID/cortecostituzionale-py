<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>181</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:181</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/05/1987</data_decisione>
    <data_deposito>22/05/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, prof. Ettore GALLO, dott. &#13;
 Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, &#13;
 prof. Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 209 comma 2,    &#13;
 r.d.  16  marzo  1942,  n.  267  (legge  fallimentare)  promosso  con    &#13;
 ordinanza  emessa  l'11  febbraio  1986  dal  Tribunale  di  Roma nel    &#13;
 procedimento civile vertente tra Genghini Gaucci Mario e S.p.A. Banco    &#13;
 di  Roma  ed  altri  iscritta al n. 371 del registro ordinanze 1986 e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, 1ª  serie    &#13;
 speciale dell'anno 1986;                                                 &#13;
    Visti  gli  atti  di costituzione di Genghini Gaucci Mario nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
    Udito  nell'udienza pubblica del 5 maggio 1987 il giudice relatore    &#13;
 Virgilio Andrioli;                                                       &#13;
    Uditi  gli  avv.ti  Enrico  Esposito  e  Raffaele  Latagliata  per    &#13;
 Genghini Gaucci Mario e l'avvocato dello Stato Franco Favara  per  il    &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con ordinanza emessa l'11 febbraio 1986 (notificata il 17 e    &#13;
 comunicata il 24 del successivo  mese  di  aprile;  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  n. 38, 1ª serie speciale del primo agosto 1986 e    &#13;
 iscritta al n. 371 R.O. 1986) nel giudizio tra Genghini Gaucci  Mario    &#13;
 e  Banco  di  Roma  s.p.a., Genghini s.p.a., Ircesi s.p.a., Residence    &#13;
 Villa   Pamphili   s.p.a.,   Impresa   Mario   Genghini   s.p.a.   in    &#13;
 amministrazione  straordinaria  in  persona  del commissario Floriano    &#13;
 D'Alessandro, e l'avv. Filippo Pegorari quale curatore  speciale  del    &#13;
 Banco  Roma  s.p.a., il Tribunale di Roma, sez. fall., ha disposto la    &#13;
 trasmissione degli atti alla Corte  costituzionale  per  il  giudizio    &#13;
 sulla legittimità costituzionale dell'art. 209, co. 2, r.d. 16 marzo    &#13;
 1942, n.  267,  applicabile  alla  amministrazione  straordinaria  in    &#13;
 virtù  del richiamo contenuto nell'art. 1 co. 5 l. 3 aprile 1979, n.    &#13;
 95, a) nella parte in cui, nel far  richiamo  all'art.  100  r.d.  16    &#13;
 marzo  1942,  n.  267,  non  prevede  la  legittimazione del debitore    &#13;
 all'impugnazione dei crediti ammessi per contrasto con gli artt. 3  e    &#13;
 24  Cost.,  b) nella parte in cui prevede che le impugnazioni a norma    &#13;
 dell'art. 100 su menzionato debbono essere proposte  nel  termine  di    &#13;
 quindici  giorni  dal deposito in cancelleria dell'elenco dei crediti    &#13;
 ammessi o respinti per contrasto con l'art. 24 Cost.                     &#13;
    2.1.  -  Avanti  la  Corte  si  sono  costituiti nell'interesse di    &#13;
 Genghini Mario giusta procura con firma autenticata il 17 marzo  1986    &#13;
 per  notar  Angelo  Bianchi  (Cantone  Ticino  - Svizzera) gli avv.ti    &#13;
 Enrico Esposito, Angelo Raffaele Latagliata, Massimo Severo Giannini,    &#13;
 che con atto depositato il 3 giugno 1986 hanno argomentato e concluso    &#13;
 per la fondatezza delle proposte questioni.  Per  il  Presidente  del    &#13;
 Consiglio  dei  ministri ha spiegato intervento l'Avvocatura generale    &#13;
 dello Stato con atto depositato il 10 agosto 1986, con  il  quale  ha    &#13;
 argomentato  e concluso per l'inammissibilità della questione sub b)    &#13;
 e per l'inammissibilità o la infondatezza della questione sub a).       &#13;
    2.2.  -  Nella  pubblica udienza del 5 maggio 1986, nella quale il    &#13;
 giudice Andrioli ha svolto la  relazione,  hanno  parlato  la  difesa    &#13;
 della parte costituita e l'Avv. Stato Franco Favara.<diritto>Considerato in diritto</diritto>3.1.  - La questione di legittimità costituzionale, per contrasto    &#13;
 con gli artt. 3 e 24 Cost., dell'art. 209 co. 2 ("Le impugnazioni,  a    &#13;
 norma dell'articolo 98, e le impugnazioni, a norma dell'articolo 100,    &#13;
 sono proposte  nei  quindici  giorni  dal  deposito  con  ricorso  al    &#13;
 presidente  del  tribunale,  osservate  le  disposizioni  del comma 2    &#13;
 dell'articolo 93") r.d. 16 marzo 1942, n.  267  (legge  fallimentare)    &#13;
 applicabile all'amministrazione straordinaria delle grandi imprese in    &#13;
 crisi in virtù del richiamo contenuto nell'art. 1 comma  5  d.l.  30    &#13;
 gennaio  1979, n. 26 (sostituito in virtù della legge 3 aprile 1979,    &#13;
 n. 95  di  conversione),  per  il  quale  la  procedura  di  A.S.  è    &#13;
 disciplinata,  in  quanto  non diversamente stabilito dal d.l., dagli    &#13;
 artt. 195 e seguenti e dall'art. 237 della legge fallimentare,  nella    &#13;
 parte   in  cui  non  prevede  la  legittimazione  delle  imprese  in    &#13;
 amministrazione straordinaria all'impugnazione dei  crediti  ammessi,    &#13;
 è  ammissibile.  Invero  le  ragioni  prospettate  dalla  parte  nel    &#13;
 giudizio de quo  possono  pur  reputarsi  in  ipotesi  infondate  nel    &#13;
 merito,  ma  ciò  non  equivale  a  dirle non proposte per inferirne    &#13;
 l'irrilevanza  della  questione  d'incostituzionalità   coinvolgente    &#13;
 l'ammissibilità  della  opposizione, con la quale si è sostenuta la    &#13;
 fondatezza nel merito della prospettata questione.                       &#13;
    Disatteso  il  dubbio  sulla  rilevanza affacciato dall'Avvocatura    &#13;
 erariale, la questione è da giudicarsi infondata perché all'impresa    &#13;
 in  amministrazione  straordinaria  va  attribuito il mezzo di tutela    &#13;
 dell'intervento nella adunanza di verificazione dello  stato  passivo    &#13;
 assicurato  dall'art.  96  l. fall. al debitore fallito, che la Corte    &#13;
 con sent. n. 222 del 1984 ha  giudicato  conforme  ai  dettami  della    &#13;
 Carta  costituzionale.  Pertanto  è  d'uopo  che  il commissario, in    &#13;
 ossequio   all'art.   209   l.   fall.,   formi,   con   l'intervento    &#13;
 dell'imprenditore individuale o degli amministratori della società o    &#13;
 della persona giuridica in A.S., l'elenco  dei  creditori  ammessi  o    &#13;
 respinti  (e delle domande di cui all'art. 207 comma 2) e lo depositi    &#13;
 nella cancelleria del tribunale del luogo dove l'impresa ha  la  sede    &#13;
 principale dandone notizia con raccomandate con avviso di ricevimento    &#13;
 a coloro le cui pretese non sono stata in tutto o in parte ammesse.      &#13;
    3.2.   -  Giudicata  fondata  la  questione  di  costituzionalità    &#13;
 dell'art. 100 non già per aver negato la legittimazione dell'impresa    &#13;
 in  A.S.  ad  impugnare  lo  stato  passivo compilato dal Commissario    &#13;
 sibbene per non aver riconosciuto alla impresa debitrice l'intervento    &#13;
 nella  formazione  di  detto  stato  passivo diviene inammissibile la    &#13;
 questione di  costituzionalità  della  ripetuta  disposizione  nella    &#13;
 parte  in  cui  fissa  l'inizio  del  termine  di quindici giorni per    &#13;
 sperimentare l'impugnazione  nel  deposito  dello  stato  passivo  in    &#13;
 cancelleria.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale dell'art. 209 co. 2 r.d.    &#13;
 16 marzo 1942, n. 267  (Disciplina  del  fallimento,  del  concordato    &#13;
 preventivo,  dell'amministrazione  controllata  e  della liquidazione    &#13;
 coatta amministrativa), applicato  all'amministrazione  straordinaria    &#13;
 delle grandi imprese in crisi in virtù dell'art. 1 co. 5 l. 3 aprile    &#13;
 1979,  n.  95  di  conversione  del  d.l.  30  gennaio  1979,  n.  26    &#13;
 (Provvedimenti  urgenti  per  l'amministrazione  straordinaria  delle    &#13;
 grandi  imprese  in  crisi)  nella  parte  in  cui  non  prevede  che    &#13;
 l'imprenditore  individuale  o  gli  amministratori  della società o    &#13;
 della persona giuridica  soggetti  ad  amministrazione  straordinaria    &#13;
 siano   sentiti  dal  commissario  con  riferimento  alla  formazione    &#13;
 dell'elenco indicato nello stesso articolo 209 legge fallimentare;       &#13;
    Dichiara inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 100 dello stesso r.d. 16 marzo 1942, n. 267 nella parte  in    &#13;
 cui  prevede  che  il  termine per l'impugnazione dello stato passivo    &#13;
 prende a decorrere dal deposito dello stesso  nella  cancelleria  del    &#13;
 tribunale.                                                               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 maggio 1987.                               &#13;
                      Il Presidente: LA PERGOLA                           &#13;
                       Il Redattore: ANDRIOLI                             &#13;
    Depositata in cancelleria il 22 maggio 1987.                          &#13;
                 Il direttore della cancelleria: VITALE</dispositivo>
  </pronuncia_testo>
</pronuncia>
