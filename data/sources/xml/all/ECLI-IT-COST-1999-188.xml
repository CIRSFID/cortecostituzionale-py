<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>188</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:188</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/05/1999</data_decisione>
    <data_deposito>25/05/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nei  giudizi  promossi  con  ricorsi  della  Regione  Liguria e della    &#13;
 Regione Lombardia notificati  il  14  febbraio  1997,  depositati  in    &#13;
 cancelleria  il  24  febbraio  ed  il  3 marzo 1997, per conflitti di    &#13;
 attribuzione sorti a seguito del decreto del Ministro  della  sanità    &#13;
 del  20  settembre  1996  recante:  "Individuazione  delle  strutture    &#13;
 sanitarie veterinarie private", ed iscritti ai nn. 6 e 8 del registro    &#13;
 conflitti 1997.                                                          &#13;
   Udito nell'udienza pubblica del 23 marzo 1999 il  giudice  relatore    &#13;
 Piero Albero Capotosti;                                                  &#13;
   Uditi  gli  avvocati  Piergiorgio  Alberti per la Regione Liguria e    &#13;
 Massimo Luciani per la Regione Lombardia.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. -   La Regione Liguria  e  la  Regione  Lombardia,  con  ricorsi    &#13;
 notificati  il  14  febbraio  1997,  depositati rispettivamente il 24    &#13;
 febbraio ed  il  3  marzo  seguenti,  hanno  sollevato  conflitto  di    &#13;
 attribuzione  nei  confronti  dello Stato in relazione al decreto del    &#13;
 Ministro della sanità 20 settembre 1996, pubblicato  nella  Gazzetta    &#13;
 Ufficiale  del  successivo 16 dicembre, recante "Individuazione delle    &#13;
 strutture sanitarie veterinarie private", in riferimento  agli  artt.    &#13;
 5,  97,  117  e  118  della  Costituzione,  ai decreti legislativi di    &#13;
 trasferimento  alle  regioni  delle  funzioni  amministrative   nella    &#13;
 materia  dell'assistenza  sanitaria ed ospedaliera, alle disposizioni    &#13;
 della legge 23  dicembre  1978,  n.  833  (Istituzione  del  Servizio    &#13;
 Sanitario  Nazionale),  nonché al principio della leale cooperazione    &#13;
 fra lo Stato e le Regioni.                                               &#13;
   Le ricorrenti premettono che i decreti legislativi 14 gennaio 1972,    &#13;
 n. 4 (Trasferimento alle Regioni a statuto ordinario  delle  funzioni    &#13;
 amministrative   statali   in  materia  di  assistenza  sanitaria  ed    &#13;
 ospedaliera e dei relativi personali ed uffici) e 24 luglio 1977,  n.    &#13;
 616  (Attuazione della delega di cui all'art. 1 della legge 22 luglio    &#13;
 1975, n. 382), hanno trasferito alle regioni  ordinarie  le  funzioni    &#13;
 concernenti  "i  gabinetti  di  analisi  per  il  pubblico a scopo di    &#13;
 accertamento diagnostico .... nonché le case di  cura  private",  ed    &#13;
 inoltre   "le   funzioni   amministrative  degli  organi  centrali  e    &#13;
 periferici  dello  Stato  concernenti  l'assistenza  zooiatrica,  ivi    &#13;
 compresa  la  istituzione,  modifica  e  soppressione  delle condotte    &#13;
 veterinarie, nonché la costituzione di consorzi per il  servizio  di    &#13;
 assistenza veterinaria". La legge n. 833 del 1978, infine, proseguono    &#13;
 le  due  Regioni, ha attribuito loro la disciplina sia delle funzioni    &#13;
 in materia di "polizia veterinaria" (art. 32,  comma  2),  sia  delle    &#13;
 funzioni   relative   all'autorizzazione   e   alla  vigilanza  sulle    &#13;
 istituzioni sanitarie di  carattere  privato,  anche  ai  fini  della    &#13;
 definizione delle loro caratteristiche funzionali (art. 43).             &#13;
   2.   -   Le   predette  attribuzioni  regionali,  ad  avviso  delle    &#13;
 ricorrenti, sono vulnerate  dall'atto  impugnato,  in  quanto  questo    &#13;
 disciplina il settore relativo alla "autorizzazione ed alla vigilanza    &#13;
 sulle  istituzioni  sanitarie  di  carattere  privato",  fra le quali    &#13;
 rientrano anche le istituzioni veterinarie in ragione  dell'identità    &#13;
 delle  prestazioni,  per  l'appunto  "sanitarie",  che  sono  erogate    &#13;
 all'interno di esse.  Il decreto, difatti, disciplina  analiticamente    &#13;
 i  diversi  tipi  di "struttura veterinaria privata", configurandosi,    &#13;
 secondo le parti ricorrenti, come "un  regolamento  adottato  con  la    &#13;
 forma  del  decreto  ministeriale",  in violazione anche dell'art. 17    &#13;
 della legge 23 agosto 1988,  n.  400  (Disciplina  dell'attività  di    &#13;
 governo e ordinamento della Presidenza del Consiglio dei Ministri).      &#13;
   3.  -  Lo  Stato,  in  persona  del  Presidente  del  Consiglio dei    &#13;
 Ministri, non si è costituito.                                          &#13;
   4. - I difensori delle due Regioni ricorrenti, in prossimità della    &#13;
 pubblica udienza, hanno depositato un'istanza  nella  quale  rilevano    &#13;
 che,  con  decreto  del  3  aprile  1998,  pubblicato  nella Gazzetta    &#13;
 Ufficiale del successivo 30 ottobre, il  Ministro  della  sanità  ha    &#13;
 annullato d'ufficio il decreto del 20 settembre 1996, oggetto dei due    &#13;
 conflitti di attribuzione, e chiedono, conseguentemente, che la Corte    &#13;
 costituzionale  dichiari  "l'improcedibilità del ricorso per cessata    &#13;
 materia del contendere".<diritto>Considerato in diritto</diritto>1. -  I due giudizi, poiché hanno ad  oggetto  l'impugnazione  del    &#13;
 medesimo atto sotto profili in larga parte coincidenti, devono essere    &#13;
 riuniti per essere decisi con un'unica sentenza.                         &#13;
   2. - Il conflitto di attribuzione sollevato dalle Regioni Liguria e    &#13;
 Lombardia  con i ricorsi indicati in epigrafe concerne il decreto del    &#13;
 Ministro della sanità  20  settembre  1996  recante  "Individuazione    &#13;
 delle strutture sanitarie veterinarie private".                          &#13;
   Successivamente al deposito dei ricorsi, il Ministro della sanità,    &#13;
 con  decreto  del  3 aprile 1998, pubblicato nella Gazzetta Ufficiale    &#13;
 del successivo 30 ottobre, ha annullato, in via di autotutela, l'atto    &#13;
 impugnato, con la motivazione che,  potendo  quest'ultimo  "risultare    &#13;
 viziato  di  illegittimità  originaria" in ragione del trasferimento    &#13;
 alle regioni della materia da  esso  disciplinata,  fosse  necessario    &#13;
 "far  cessare  la  materia  del  contendere nell'instaurato giudizio"    &#13;
 dinanzi alla Corte costituzionale.                                       &#13;
   L'autoannullamento  dell'atto  impugnato  in  base  alla   predetta    &#13;
 motivazione e la sua efficacia retroattiva determinano pertanto, come    &#13;
 hanno  sostenuto  anche  le  regioni  ricorrenti,  una  situazione di    &#13;
 cessazione della materia del contendere.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi, dichiara cessata la materia  del  contendere  in    &#13;
 ordine ai conflitti di attribuzione sollevati dalla Regione Liguria e    &#13;
 dalla Regione Lombardia con i ricorsi indicati in epigrafe.              &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 maggio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 25 maggio 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
