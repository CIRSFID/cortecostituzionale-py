<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>92</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:92</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>06/03/2006</data_decisione>
    <data_deposito>10/03/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Franco GALLO, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 507 del codice di procedura penale e dell'art. 151, comma 2, delle norme di attuazione, di coordinamento e transitorie del codice di procedura penale (d.lgs. 28 luglio 1989, n. 271), promosso con ordinanza del 20 novembre 2003 dal Tribunale di La Spezia, sezione distaccata di Sarzana, nel procedimento penale a carico di V. D., iscritta al n. 50 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 9, prima serie speciale, dell'anno 2004. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio dell'8 febbraio 2006 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che, con l'ordinanza indicata in epigrafe, il Tribunale di La Spezia, sezione distaccata di Sarzana, ha sollevato, in riferimento all'art. 111 della Costituzione, questione di legittimità costituzionale: a) dell'art. 507 del codice di procedura penale, «nella parte in cui autorizza il giudice a disporre nuove prove anche in deroga alle decadenze previste dall'art. 468 cod. proc. pen., nonché nella parte in cui non prevede che l'ordinanza ammissiva indichi nuovi temi di prova e che le parti possano dedurre nuove prove disponendo un congruo termine a difesa»; b) dell'art. 151, comma 2, delle norme di attuazione, di coordinamento e transitorie del medesimo codice, nella parte in cui prevede che «… quando è stato disposto d'ufficio l'esame di una persona, il presidente vi provvede direttamente stabilendo, all'esito, la parte che deve condurre l'esame diretto…»; &#13;
    che il giudice a quo – premesso di aver provveduto ad ammettere «su sollecitazione del p.m., ex art. 507 c.p.p., i testi indicati in verbale» – osserva che, in forza della interpretazione di tale norma quale si ricava anche dall'intervento delle Sezioni Unite della Corte di cassazione, «si è consentito di superare il regime delle decadenze processuali», conferendo al giudice il potere di assumere non soltanto le prove realmente “nuove” perché sopravvenute, ma anche quelle che le parti avrebbero potuto dedurre e dal cui diritto di richiesta sono decadute in base alle preclusioni dell'art. 468 cod. proc. pen.; &#13;
    che, sotto altro profilo, secondo il Tribunale rimettente, il medesimo art. 507 cod. proc. pen. non prevede il diritto delle parti di dedurre nuove prove o prove contrarie in esito all'ordinanza di integrazione istruttoria emessa dal giudice; &#13;
    che, infatti, la contraria affermazione – a parere del giudice a quo – risulterebbe contrastare con il dato testuale della norma censurata ed, oltretutto, non sarebbe avvalorata da un'interpretazione sistematica: infatti, l'art. 493, comma 2, cod. proc. pen., limita l'acquisizione di prove non comprese nella lista prevista dall'art. 468 cod. proc. pen. al solo caso di impossibilità di tempestiva indicazione; mentre l'art. 495 cod. proc. pen. - autorizzando l'indicazione delle sole prove a discarico sui fatti oggetto delle prove a carico - non risulterebbe applicabile nel caso di prove disposte ex art. 507 cod. proc. pen., non essendo, per esse, previamente indicati i temi di prova, né precisato a favore o contro chi le stesse debbano valere; &#13;
    che dunque – secondo il rimettente – l'art. 507 cod. proc. pen., in ordine ad entrambi i profili rilevati, risulterebbe in contrasto con l'art. 111 della Costituzione, nella parte in cui afferma il diritto dell'imputato di «ottenere la convocazione e l'interrogatorio di persone a sua difesa»: diritto ulteriormente compromesso dalla mancata previsione, nella norma censurata, di un termine a difesa; &#13;
    che peraltro – prosegue il rimettente – anche l'art. 151, comma 2, delle disposizioni di attuazione, di coordinamento e transitorie del codice di procedura penale, risulterebbe in contrasto con il principio di terzietà ed imparzialità del giudice sancito dal medesimo art. 111 Cost., in quanto tale norma – prevedendo, una volta disposto d'ufficio l'esame di una persona, che sia il presidente a provvedervi direttamente, prima di stabilire, all'esito, la parte che deve condurre l'esame diretto – implicherebbe una valutazione della prova medesima subito dopo la deposizione del teste, «proprio al fine di individuare chi dovrà interrogarlo per primo e con quali modalità»; &#13;
    che nel giudizio di costituzionalità è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha chiesto che le questioni siano dichiarate inammissibili o infondate. &#13;
    Considerato, quanto alla prima delle questioni di legittimità costituzionale, relativa all'art. 507 cod. proc. pen., che essa risulta prospettata in modo contraddittorio;  &#13;
    che, infatti, la norma citata è censurata, innanzitutto, nella parte in cui è consentito al giudice di disporre nuove prove anche nel caso in cui le parti siano decadute dal relativo diritto di richiesta, in forza dell'art. 468 cod. proc. pen., reclamando una pronuncia demolitoria in parte qua della disposizione, per escludere, in tale ipotesi, la possibilità di escussione dei testi: esito, questo, che tuttavia – anche a voler prescindere dai contrari dicta emanati da questa Corte nella sentenza n. 111 del 1993, totalmente trascurata dal giudice a quo – renderebbe del tutto superflua e, dunque, contraddittoria l'ulteriore richiesta, avanzata immediatamente dopo, con la quale il Tribunale rimettente sollecita una pronuncia additiva di incostituzionalità della medesima norma, per la mancata previsione, in essa, del potere per le parti del processo di articolare prove contrarie o nuove prove e di disporre di un congruo termine a difesa;  &#13;
    che, inoltre, relativamente a quest'ultimo profilo, la questione di costituzionalità è prospettata in maniera del tutto ipotetica, giacché, da parte del Tribunale rimettente, risultano solo presunti l'effettivo interesse o la possibilità concreta, in capo alle parti del giudizio a quo, di articolare nuovi mezzi di prova o prove contrarie o di richiedere un termine a difesa; &#13;
    che, accanto a ciò, nell'ordinanza di rimessione, il giudice rimettente precisa di avere già esercitato, su sollecitazione del pubblico ministero, il potere di cui all'art. 507 cod. proc. pen., «provvedendo ad ammettere i testi indicati a verbale»; &#13;
    che, pertanto, il giudice a quo ha già fatto applicazione della norma della cui illegittimità costituzionale si duole ed ha quindi esaurito il corrispondente potere di sindacato di costituzionalità; &#13;
    che, infine, quanto alla questione avente ad oggetto l'art. 151, comma 2, delle norme di attuazione, di coordinamento e transitorie del codice di procedura penale, essa difetta del requisito della rilevanza: infatti, mentre l'impugnativa è specificamente espressa nei confronti del comma 2 di tale norma, la fattispecie che viene in rilievo nel giudizio a quo risulta essere, piuttosto, disciplinata dal comma 1 dell'art. 151 citato, considerato che, come evidenziato dallo stesso rimettente, le nuove prove erano state nella specie richieste dal pubblico ministero; con conseguente applicazione del normale ordine di assunzione, previsto dall'art. 496 cod. proc. pen.;  &#13;
    che, alla luce di quanto precede, entrambe le questioni vanno dichiarate manifestamente inammissibili. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE  &#13;
    dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 507 del codice di procedura penale e dell'art. 151, comma 2, delle norme di attuazione, di coordinamento e transitorie del codice di procedura penale, sollevate, in riferimento all'art. 111 della Costituzione, dal Tribunale di La Spezia, sezione distaccata di Sarzana, con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta il 6 marzo 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 10 marzo 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
