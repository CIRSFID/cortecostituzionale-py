<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>91</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:91</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1995</data_decisione>
    <data_deposito>17/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 72, secondo    &#13;
 comma, ultimo inciso, del decreto legislativo 31 dicembre 1992 n. 546    &#13;
 (Disposizioni sul  processo  tributario  in  attuazione  della  legge    &#13;
 delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991    &#13;
 n.  413),  promosso  con  ordinanza  emessa  il  7  giugno 1993 dalla    &#13;
 Commissione  tributaria  centrale  sul  ricorso  proposto  da Ufficio    &#13;
 Imposte Dirette di Roma contro Ranchi Germana, iscritta al n. 558 del    &#13;
 registro delle ordinanze 1994 e pubblicata nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 40, prima serie speciale, dell'anno 1994;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 22 febbraio  1995  il  Giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
    Ritenuto  che  la commissione tributaria centrale ha sollevato, in    &#13;
 riferimento all'art. 76 della Costituzione, questione di legittimità    &#13;
 costituzionale  dell'art.  72,  secondo  comma,  ultimo  inciso,  del    &#13;
 decreto  legislativo  31  dicembre  1992  n.  546  (Disposizioni  sul    &#13;
 processo tributario in  attuazione  della  legge  delega  al  Governo    &#13;
 contenuta  nell'art.  30  della  legge 30 dicembre 1991 n. 413) nella    &#13;
 parte in cui, con norma  di  interpretazione  autentica,  prevede  la    &#13;
 inapplicabilità   nel   procedimento   tributario,  del  termine  di    &#13;
 decadenza dalle impugnazioni stabilito dall'art. 327  del  codice  di    &#13;
 procedura civile;                                                        &#13;
      che  a  parere  del  giudice  a quo, l'introduzione della regola    &#13;
 della inapplicabilità nel processo tributario del predetto  termine,    &#13;
 in  contrasto con il consolidato orientamento della Cassazione, dallo    &#13;
 stesso  condiviso,  comporta  la  violazione   dell'art.   76   della    &#13;
 Costituzione  dal momento che la delega della funzione legislativa fu    &#13;
 concessa al Governo al solo fine  di  disporre  (per  il  futuro)  la    &#13;
 revisione  e  la  organizzazione  del  contenzioso  tributario e non,    &#13;
 quindi, per emanare norme retroattive di interpretazione autentica;      &#13;
      che è intervenuto in giudizio il Presidente del  Consiglio  dei    &#13;
 ministri,   rappresentato   e  difeso  dall'Avvocatura  dello  Stato,    &#13;
 concludendo per l'inammissibilità, o  comunque,  per  l'infondatezza    &#13;
 della questione;                                                         &#13;
    Considerato  che  l'art.  15,  1- bis, del decreto-legge 29 aprile    &#13;
 1994  n.  260  convertito  in  legge  27  giugno  1994,  n.  413   ha    &#13;
 ulteriormente   differito  al  1°  ottobre  1995  la  data  unica  di    &#13;
 insediamento  delle  nuove  commissioni  tributarie   provinciali   e    &#13;
 regionali prevista dall'art. 42, primo comma, del decreto legislativo    &#13;
 31 dicembre 1992, n. 545;                                                &#13;
      che,  in  conseguenza,  tutte le disposizioni sul nuovo processo    &#13;
 tributario di cui al decreto legislativo  31  dicembre  1992  n.  546    &#13;
 hanno  efficacia  dalla suddetta data, così come, analogamente, già    &#13;
 disposto dall'art. 69, primo comma, del decreto-legge 30 agosto 1993,    &#13;
 n. 331, convertito nella legge 29  ottobre  1993  n.  427  che  aveva    &#13;
 differito  al  1°  ottobre  1994  la data unica di insediamento delle    &#13;
 predette commissioni;                                                    &#13;
      che pertanto si profila una inammissibilità della questione per    &#13;
 difetto di rilevanza, non dovendo il giudice a quo  al  momento  fare    &#13;
 applicazione  della  norma  della  cui legittimità costituzionale si    &#13;
 dubita (ordinanze nn. 230 del 1994 e 502 del 1993);                      &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità  costituzionale  dell'art.  72,  secondo  comma,  ultimo    &#13;
 inciso,   del   decreto   legislativo   31   dicembre  1992,  n.  546    &#13;
 (Disposizioni sul processo tributario in attuazione della  delega  al    &#13;
 Governo  contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413)    &#13;
 sollevata  in  riferimento  all'art.  76  della  Costituzione,  dalla    &#13;
 Commissione tributaria centrale con l'ordinanza indicata in epigrafe.    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1995.                                  &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                       Il redattore: SANTOSUOSSO                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 17 marzo 1995.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
