<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/32/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/32"/>
          <FRBRalias value="ECLI:IT:COST:1963:32" name="ECLI"/>
          <FRBRdate date="1963-03-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="32"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/32/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/32/ita@"/>
          <FRBRdate date="1963-03-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/32/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/32/ita@.xml"/>
          <FRBRdate date="1963-03-16" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1963-03-05" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1963-03-16" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="AMBROSINI" href="/akn/ontology/person/it/AMBROSINI" showAs="AMBROSINI"/>
        <TLCPerson eId="gaspareAmbrosini" href="/akn/ontology/person/it/gaspareAmbrosini" showAs="GASPARE AMBROSINI"/>
        <TLCPerson eId="giuseppeCastelliAvolio" href="/akn/ontology/person/it/giuseppeCastelliAvolio" showAs="GIUSEPPE CASTELLI AVOLIO"/>
        <TLCPerson eId="antoninoPapaldo" href="/akn/ontology/person/it/antoninoPapaldo" showAs="ANTONINO PAPALDO"/>
        <TLCPerson eId="giovanniCassandro" href="/akn/ontology/person/it/giovanniCassandro" showAs="GIOVANNI CASSANDRO"/>
        <TLCPerson eId="biagioPetrocelli" href="/akn/ontology/person/it/biagioPetrocelli" showAs="BIAGIO PETROCELLI"/>
        <TLCPerson eId="antonioManca" href="/akn/ontology/person/it/antonioManca" showAs="ANTONIO MANCA"/>
        <TLCPerson eId="aldoSandulli" href="/akn/ontology/person/it/aldoSandulli" showAs="ALDO SANDULLI"/>
        <TLCPerson eId="giuseppeBranca" href="/akn/ontology/person/it/giuseppeBranca" showAs="GIUSEPPE BRANCA"/>
        <TLCPerson eId="micheleFragali" href="/akn/ontology/person/it/micheleFragali" showAs="MICHELE FRAGALI"/>
        <TLCPerson eId="costantinoMortati" href="/akn/ontology/person/it/costantinoMortati" showAs="COSTANTINO MORTATI"/>
        <TLCPerson eId="giuseppeChiarelli" href="/akn/ontology/person/it/giuseppeChiarelli" showAs="GIUSEPPE CHIARELLI"/>
        <TLCPerson eId="giuseppeVerzì" href="/akn/ontology/person/it/giuseppeVerzì" showAs="GIUSEPPE VERZÌ"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1963</akn4cc:anno>
        <akn4cc:numero>32</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1963:32</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>AMBROSINI</akn4cc:presidente>
        <akn4cc:relatore>Giovanni Cassandro</akn4cc:relatore>
        <akn4cc:data_decisione>1963-03-05</akn4cc:data_decisione>
        <akn4cc:data_deposito>1963-03-16</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#gaspareAmbrosini">GASPARE AMBROSINI</judge>, Presidente - Prof. <judge refersTo="#giuseppeCastelliAvolio">GIUSEPPE CASTELLI AVOLIO</judge> - Prof. <judge refersTo="#antoninoPapaldo">ANTONINO PAPALDO</judge> - Prof. <judge refersTo="#giovanniCassandro">GIOVANNI 
 CASSANDRO</judge> - Prof. <judge refersTo="#biagioPetrocelli">BIAGIO PETROCELLI</judge> - Dott. <judge refersTo="#antonioManca">ANTONIO MANCA</judge> - Prof. <judge refersTo="#aldoSandulli">ALDO 
 SANDULLI</judge> - Prof. <judge refersTo="#giuseppeBranca">GIUSEPPE BRANCA</judge> - Prof. <judge refersTo="#micheleFragali">MICHELE FRAGALI</judge> - Prof. <judge refersTo="#costantinoMortati">COSTANTINO MORTATI</judge> - Prof. <judge refersTo="#giuseppeChiarelli">GIUSEPPE CHIARELLI</judge> - Dott. <judge refersTo="#giuseppeVerzì">GIUSEPPE VERZÌ</judge>, 
 Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha deliberato in camera di consiglio la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel  giudizio di legittimità costituzionale dell'art. 285, secondo  
 comma, del T.U. per la finanza locale, approvato con <ref href="/akn/it/act/regioDecreto/stato/1931-09-14/1175/!main">R.D. 14  settembre  
 1931,  n.  1175</ref>, e dell'art.   209, secondo comma, del T.U. delle leggi  
 sulle imposte dirette, approvato con <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1958-01-29/645/!main">D.P.R. 29 gennaio  1958,  n.  645</ref>,  
 promosso con ordinanza emessa l'il maggio 1962 dal Tribunale di Palermo  
 nel  procedimento civile vertente tra Innaini Francesco Paolo contro il  
 Comune di Palermo e l'Esattoria comunale di Palermo, iscritta al n. 172  
 del Registro ordinanze 1962 e pubblicata nella Gazzetta Ufficiale della  
 Repubblica n. 300 del 24 novembre 1962.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udita nella camera di consiglio del 5 marzo 1963 la  relazione  del  
 Giudice Giovanni Cassandro;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Ritenuto  che  nel  corso  di  un  procedimento  civile  davanti al  
 Tribunale di Palermo è  stata  sollevata  d'ufficio  la  questione  di  
 legittimità  costituzionale  dell'art. 285, secondo comma, del T.U. 14  
 settembre 1931, n. 1175, e dell'art. 209, secondo comma,  del  T.U.  29  
 gennaio  1958,  n. 645, in riferimento alle norme contenute negli artt.</p>
          </content>
        </paragraph>
        <paragraph>
          <num>24</num>
          <content>
            <p> e 113 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che il Tribunale ha in conseguenza sospeso il  giudizio  e  inviato  
 gli atti a questa Corte con l'ordinanza citata in epigrafe;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che  nel  giudizio  nessuna  delle  parti  si è costituita, né è  
 intervenuto il Presidente del Consiglio dei Ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Considerato che con <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1962-07-03/86/!main">sentenza n. 86 del 3 luglio 1962</ref>  la  Corte  ha  
 dichiarato   l'illegittimità  costituzionale  dell'art.  285,  secondo  
 comma, del T.U. per la finanza locale, approvato con <ref href="/akn/it/act/regioDecreto/stato/1931-09-14/1175/!main">R.D. 14  settembre  
 1931, n. 1175</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che con <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1962-07-03/87/!main">sentenza n. 87 del 3 luglio 1962</ref> la Corte ha dichiarato non  
 fondata  la  questione  di  legittimità  costituzionale  sollevata nei  
 confronti  del  secondo  e  terzo  comma  del  citato  art.    209,  in  
 riferimento   alle   norme   contenute   negli  <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt.  3</ref>  e  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_113">113  della  
 Costituzione</ref></mref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che successivamente, con <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1962-11-15/101/!main">ordinanza n. 101 del 15 novembre 1962</ref>,  la  
 Corte  ha dichiarato la manifesta infondatezza della questione anche in  
 riferimento all'art. 24,  primo  comma,  della  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>,  "per  la  
 ragione  che il procedimento esecutivo esattoriale, pur nelle forme che  
 sono ad esso peculiari, non vieta ai cittadini di agire in giudizio per  
 la tutela dei propri diritti o interessi legittimi";</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che l'ordinanza non prospetta la questione sotto nuovi profili o in  
 termini diversi, anzi, per motivarne  la  non  manifesta  infondatezza,  
 espone  argomenti  che  sono  stati  già  considerati e respinti dalla  
 Corte;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, pertanto, le decisioni devono essere confermate;</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     visto l'art. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>;</block>
        <paragraph>
          <list>
            <intro>
              <p>dichiara la manifesta infondatezza:</p>
            </intro>
            <point>
              <num>a)</num>
              <content>
                <p> della questione di legittimità  costituzionale  dell'art.  285,  
 secondo  comma,  del  T.U. per la finanza locale, approvato con <ref href="/akn/it/act/regioDecreto/stato/1931-09-14/1175/!main">R.D. 14  
 settembre 1931, n. 1175</ref>, in  riferimento  alle  norme  contenute  negli  
 <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">artt. 24</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_113">113 della Costituzione</ref></mref>;</p>
              </content>
            </point>
            <point>
              <num>b)</num>
              <content>
                <p>  della  questione  di legittimità costituzionale dell'art. 209,  
 secondo comma, del T.U. delle leggi sulle  imposte  dirette,  approvato  
 con <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1958-01-29/645/!main">D.P.R. 29 gennaio 1958, n. 645</ref>, in riferimento alle norme contenute  
 nei medesimi <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">artt. 24</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_113">113 della Costituzione</ref></mref>.</p>
              </content>
            </point>
          </list>
        </paragraph>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  
 Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="1963-03-05">5 marzo 1963</date>.</block>
      <p><signature><judge refersTo="#gaspareAmbrosini">GASPARE AMBROSINI</judge></signature> - <signature><judge refersTo="#giuseppeCastelliAvolio">GIUSEPPE CASTELLI  
                                   AVOLIO</judge></signature> - <signature><judge refersTo="#antoninoPapaldo">ANTONINO PAPALDO</judge></signature> -  <signature><judge refersTo="#giovanniCassandro">GIOVANNI  
                                   CASSANDRO</judge></signature>   -   <signature><judge refersTo="#biagioPetrocelli">BIAGIO  PETROCELLI</judge></signature>  -  
                                   <signature><judge refersTo="#antonioManca">ANTONIO  MANCA</judge></signature>  -  <signature><judge refersTo="#aldoSandulli">ALDO  SANDULLI</judge></signature>   -  
                                   <signature><judge refersTo="#giuseppeBranca">GIUSEPPE  BRANCA</judge></signature>  - <signature><judge refersTo="#micheleFragali">MICHELE FRAGALI</judge></signature> -  
                                   <signature><judge refersTo="#costantinoMortati">COSTANTINO   MORTATI</judge></signature>    -    <signature><judge refersTo="#giuseppeChiarelli">GIUSEPPE  
                                   CHIARELLI</judge></signature> - <signature><judge refersTo="#giuseppeVerzì">GIUSEPPE  VERZÌ</judge></signature>.</p>
    </conclusions>
  </judgment>
</akomaNtoso>
