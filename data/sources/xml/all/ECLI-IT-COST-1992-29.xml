<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>29</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:29</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1992</data_decisione>
    <data_deposito>03/02/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 482, primo    &#13;
 comma, e 382, primo comma, del codice di procedura  penale  del  1930    &#13;
 promosso  con  ordinanza  emessa  il  30 aprile 1991 dal Tribunale di    &#13;
 Trapani nel procedimento penale a carico di Cizio Giuseppe ed  altro,    &#13;
 iscritta  al  n.  512  del registro ordinanze 1991 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  33,  prima  serie  speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 18 dicembre  1991  il  Giudice    &#13;
 relatore Mauro Ferri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ordinanza  emessa  il  30  aprile 1991 il Tribunale di    &#13;
 Trapani ha sollevato questione  di  legittimità  costituzionale,  in    &#13;
 riferimento  agli  artt.  3  e  24 della Costituzione, dell'art. 482,    &#13;
 primo comma - in relazione all'art. 382, primo comma - del codice  di    &#13;
 procedura   penale   abrogato,  nella  parte  in  cui,  nel  caso  di    &#13;
 proscioglimento dell'imputato  da  reato  punibile  a  querela  della    &#13;
 persona offesa, impone di condannare il querelante al pagamento delle    &#13;
 spese  processuali  anticipate  dallo Stato anche in assenza di colpa    &#13;
 del querelante stesso.                                                   &#13;
    2. - Il giudice a quo - premesso che nel caso  sottoposto  al  suo    &#13;
 esame  il  proscioglimento degli imputati dal reato di diffamazione a    &#13;
 mezzo stampa consegue al riconoscimento del legittimo  esercizio  del    &#13;
 diritto di critica garantito dall'art. 21 Cost., e quindi con la formula "perché il fatto non costituisce reato" - rileva che a siffatta    &#13;
 pronuncia   dovrebbe   inevitabilmente  conseguire  la  condanna  del    &#13;
 querelante al  pagamento  delle  spese  processuali  in  quanto,  dal    &#13;
 combinato  disposto  degli  artt.  482  e 382 dall'abrogato codice di    &#13;
 rito, ed eccettuati i casi di proscioglimento per perdono  giudiziale    &#13;
 o  per  altra  causa  estintiva  sopravvenuta  alla  querela,  non è    &#13;
 consentita alcuna valutazione del suo comportamento.                     &#13;
    Al querelante, pertanto, andrebbe addossata la responsabilità per    &#13;
 le spese quand'anche non sia ravvisabile  nei  suoi  confronti  alcun    &#13;
 profilo di colpa.                                                        &#13;
    Il  Tribunale  di Trapani rammenta che già con le sentenze n. 284    &#13;
 (rectius: n. 165)  del  1974  e  n.  52  del  1975  questa  Corte  ha    &#13;
 dichiarato  la  parziale illegittimità costituzionale delle norme in    &#13;
 questione, individuando la ratio unitaria delle ipotesi di  esenzione    &#13;
 del  querelante  dalla  responsabilità  per  le  spese nel fatto che    &#13;
 "l'assoluzione dell'imputato derivi da circostanze non  riconducibili    &#13;
 al   querelante,   cui  nessuna  colpa  può  essere  addebitata",  e    &#13;
 riconoscendo che contrastava col principio di eguaglianza la  mancata    &#13;
 considerazione  di  quegli  altri casi allora sottoposti al suo esame    &#13;
 (proscioglimento del non imputabile per incapacità di intendere e di    &#13;
 volere, querela  contro  ignoti  per  reato  realmente  verificatosi)    &#13;
 rispetto ai quali era ravvisabile la medesima ratio.                     &#13;
    Ad  avviso  del  remittente,  però,  detto  intervento  non  può    &#13;
 ritenersi risolutivo degli accennati profili di  incostituzionalità;    &#13;
 in  particolare,  l'equiparazione  fra  l'obbligo  al pagamento delle    &#13;
 spese incombente sull'imputato riconosciuto  colpevole  e  l'identico    &#13;
 obbligo   del   querelante,   nel  caso  di  assoluzione  del  primo,    &#13;
 apparirebbe  irragionevole  e  contrastante  con  il   principio   di    &#13;
 eguaglianza   poiché   assoggetta   alla  medesima  disciplina,  per    &#13;
 l'aspetto in esame, due situazioni radicalmente differenti: quella di    &#13;
 chi viene condannato a seguito  di  giudizio  necessariamente  esteso    &#13;
 alla  colpevolezza  e  quella  di  chi, invece, si vede addossata una    &#13;
 responsabilità di ordine patrimoniale prescindendo del tutto da ogni    &#13;
 considerazione sulla colpa.                                              &#13;
    Inoltre,  il  criterio  dell'automaticità  della   condanna   del    &#13;
 querelante alle spese (salve le tassative eccezioni prima rammentate)    &#13;
 evidenzierebbe  un  ulteriore profilo di irragionevolezza nella parte    &#13;
 in cui, escludendo ogni  valutazione  del  comportamento  di  chi  ha    &#13;
 esercitato  il  diritto  di  querela,  impone di addossare egualmente    &#13;
 l'onere delle spese  processuali  tanto  al  querelante  avventato  o    &#13;
 temerario  quanto  a  quello  cui  nessun  addebito  del genere possa    &#13;
 muoversi.                                                                &#13;
    A ciò deve aggiungersi, ad avviso del  remittente,  un  ulteriore    &#13;
 profilo  di illegittimità costituzionale, in riferimento all'art. 24    &#13;
 della Costituzione, emergente dal fatto che la persona offesa  da  un    &#13;
 reato  perseguibile  a querela viene a trovarsi esposta al rischio di    &#13;
 responsabilità patrimoniale per circostanze a lui estranee;  il  che    &#13;
 importerebbe  un'indebita  ed ingiustificata compressione del diritto    &#13;
 di agire in giudizio per la difesa dei propri diritti.                   &#13;
    Infine, i  dubbi  di  illegittimità  delle  norme  denunciate  si    &#13;
 prospetterebbero  ancor più fondati a seguito dell'entrata in vigore    &#13;
 del nuovo codice di rito penale, nel quale gli artt. 542 e  427,  pur    &#13;
 mantenendo  fermo  il  criterio dell'automatismo assoluzione-condanna    &#13;
 del querelante alle spese, hanno ristretto le ipotesi di condanna  ai    &#13;
 soli  casi  di  assoluzione  perché  il fatto non sussiste o perché    &#13;
 l'imputato  non  l'ha  commesso;   con   esclusione,   quindi   della    &#13;
 responsabilità  per  le  spese  nel  caso di assoluzione con formula    &#13;
 "perché il fatto non costituisce reato".                                &#13;
    Questa nuova e diversa normativa introdurrebbe quindi un ulteriore    &#13;
 profilo  di  irragionevole  disparità  di   trattamento   tra   vari    &#13;
 querelanti  a seconda che i relativi processi vengano celebrati - per    &#13;
 ragioni casuali, anche indipendenti dal tempo di presentazione  della    &#13;
 querela - applicando l'una o l'altra disciplina processuale.             &#13;
    3.  -  È intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  dall'Avvocatura   generale   dello   Stato,    &#13;
 concludendo per l'infondatezza della sollevata questione.                &#13;
    Ritiene l'Avvocatura che i richiami alle due sentenze con le quali    &#13;
 questa  Corte  ha  già  dichiarato  la  parziale incostituzionalità    &#13;
 dell'art. 382 del codice  di  procedura  penale  abrogato  non  siano    &#13;
 conferenti.                                                              &#13;
    In quelle ipotesi la condotta del querelante non era meritevole di    &#13;
 essere sanzionata, mentre nel caso in esame non sarebbe comprensibile    &#13;
 per  quali  motivi  il  querelante  risulterebbe  esente da colpa. Ad    &#13;
 avviso della difesa del  governo,  infatti,  la  responsabilità  del    &#13;
 querelante  discenderebbe  proprio  dall'aver  omesso  di prendere in    &#13;
 considerazione, prima di formulare l'istanza punitiva, la sussistenza    &#13;
 a favore degli imputati di quel diritto di critica costituzionalmente    &#13;
 garantito ed i cui parametri sono sufficientemente ben definiti.<diritto>Considerato in diritto</diritto>1.   -  Il  Tribunale  di  Trapani  ritiene  che  il  principio  di    &#13;
 eguaglianza ed il diritto di difesa, garantiti dagli  artt.  3  e  24    &#13;
 della  Costituzione,  siano violati dalle disposizioni previste dagli    &#13;
 artt. 482, primo comma, e 382, primo comma, del codice  di  procedura    &#13;
 penale  del  1930,  nelle  parti  in  cui  prevedono  la condanna del    &#13;
 querelante alle spese del procedimento anticipate dallo  Stato  anche    &#13;
 in  assenza  di  colpa del querelante stesso; vale a dire, per quanto    &#13;
 riguarda  il  giudizio  a  quo,   nell'ipotesi   di   proscioglimento    &#13;
 dell'imputato perché il fatto non costituisce reato.                    &#13;
    2.  -  In primo luogo il giudice remittente, premesso che nel caso    &#13;
 sottoposto al suo esame si impone il proscioglimento  degli  imputati    &#13;
 dal reato di diffamazione a mezzo stampa per l'esistenza di una causa    &#13;
 di giustificazione, sostiene che l'applicazione della norma impugnata    &#13;
 comporterebbe una irragionevole equiparazione, sotto il profilo della    &#13;
 responsabilità patrimoniale, del querelante avventato o temerario al    &#13;
 querelante cui nessun addebito possa muoversi.                           &#13;
    Sotto questo profilo la questione è fondata.                         &#13;
    Questa Corte ha già avuto occasione di rilevare (v. sentt. n. 165    &#13;
 del  1974  e  52 del 1975) che le indicate disposizioni, nel sancire,    &#13;
 anche al fine di  evitare  liti  temerarie,  la  responsabilità  del    &#13;
 querelante  per  il  pagamento  delle  spese  processuali nel caso di    &#13;
 proscioglimento dell'imputato, stabiliscono alcune eccezioni rette da    &#13;
 una  ratio  unitaria,  che  è  quella  di   esentare   dalla   detta    &#13;
 responsabilità  chi  ha esercitato il diritto di querela allorquando    &#13;
 l'assoluzione dell'imputato derivi da circostanze  non  riconducibili    &#13;
 al  querelante  stesso  al  quale,  quindi, nessuna colpa può essere    &#13;
 addebitata: "Ove ricorrano tali estremi - ha dichiarato  la  sentenza    &#13;
 n.  52  del 1975 - contrasta con il principio di eguaglianza la norma    &#13;
 giuridica, come quella denunciata, che egualmente imponga la condanna    &#13;
 alle spese processuali".                                                 &#13;
    Anche nell'ipotesi in esame si realizza una simile situazione.        &#13;
    Invero  la  formula  di  proscioglimento  "perché  il  fatto  non    &#13;
 costituisce  reato"  deve  essere  adottata  quando, pur affermandosi    &#13;
 l'esistenza del  fatto  nella  sua  materialità,  manchi  l'elemento    &#13;
 soggettivo  del  dolo o della colpa, ovvero quando sussista una causa    &#13;
 di giustificazione: circostanze tutte il cui accertamento non  è  in    &#13;
 alcun  modo  riconducibile  al  querelante;  né la sussistenza delle    &#13;
 medesime  può  essere  ritenuta  sintomo  di  una   avventatezza   o    &#13;
 temerarietà  della  querela,  tant'è  che  detta  formula, in linea    &#13;
 generale, non è preclusiva dell'azione civile, ben potendo il  fatto    &#13;
 lamentato   non  costituire  illecito  penale  ma  costituire  invece    &#13;
 illecito civile.                                                         &#13;
    Val la pena di sottolineare che  nel  nuovo  codice  di  procedura    &#13;
 penale del 1988 il legislatore (seguendo alcune indicazioni contenute    &#13;
 nelle  citate  sentt. nn. 165 del 1974 e 52 del 1975 di questa Corte)    &#13;
 ha già adottato la  medesima  soluzione  eliminando  la  formula  di    &#13;
 proscioglimento  in questione dal novero delle ipotesi che comportano    &#13;
 la condanna del querelante alle spese del procedimento.                  &#13;
    Sussiste quindi il lamentato contrasto in ordine all'art. 3  della    &#13;
 Costituzione   mentre  rimane  assorbita  la  questione  proposta  in    &#13;
 riferimento all'art. 24 della Costituzione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  degli  artt. 382, primo    &#13;
 comma, e 482, primo comma, del codice di procedura penale  del  1930,    &#13;
 nella  parte  in  cui prevedono la condanna del querelante alle spese    &#13;
 del  procedimento  anticipate  dallo  Stato,  anche  nell'ipotesi  di    &#13;
 proscioglimento dell'imputato perché il fatto non costituisce reato.    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 gennaio 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 3 febbraio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
