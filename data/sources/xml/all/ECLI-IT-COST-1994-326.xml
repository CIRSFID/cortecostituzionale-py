<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>326</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:326</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1994</data_decisione>
    <data_deposito>20/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 585, secondo    &#13;
 comma, lett.  c),  del  codice  di  procedura  penale,  promosso  con    &#13;
 ordinanza  emessa  l'8  novembre 1993 dalla Corte d'Appello di Reggio    &#13;
 Calabria nel procedimento penale a carico di Cudia Mariano,  iscritta    &#13;
 al  n.  71  del  registro  ordinanze 1994 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n.  11,  prima  serie  speciale  dell'anno    &#13;
 1994;                                                                    &#13;
    Udito  nella  Camera  di  consiglio  del 22 giugno 1994 il Giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
    Ritenuto che nel corso del procedimento penale  nei  confronti  di    &#13;
 Cudia  Mariano,  la Corte d'Appello di Reggio Calabria, con ordinanza    &#13;
 dell'8 novembre 1993 (R.O. n. 71/1994), ha sollevato, in  riferimento    &#13;
 agli  artt.  3  e 24 della Costituzione, la questione di legittimità    &#13;
 costituzionale dell'art. 585, secondo comma, lett. c) del  codice  di    &#13;
 procedura penale;                                                        &#13;
      che nell'ordinanza di rimessione si premette che con dispositivo    &#13;
 letto  all'udienza del 18 maggio 1992 il Tribunale di Reggio Calabria    &#13;
 aveva dichiarato non doversi procedere  nei  confronti  dell'imputato    &#13;
 per  intervenuta  amnistia;  che  la sentenza era stata depositata il    &#13;
 successivo 11 giugno 1992, senza  che  in  seguito  fosse  notificato    &#13;
 avviso  all'imputato ed ai suoi difensori; che nel corso del giudizio    &#13;
 di appello - proposto dal difensore dell'imputato in data 16  ottobre    &#13;
 1992   -   i   difensori   delle   parti   civili   avevano  eccepito    &#13;
 l'inammissibilità dell'appello dell'imputato sotto il profilo  della    &#13;
 sua  tardività,  e  ciò  alla stregua dell'art. 585, secondo comma,    &#13;
 lett. c), del vigente codice di procedura penale, secondo il quale il    &#13;
 termine per proporre  appello  decorre  dalla  scadenza  del  termine    &#13;
 stabilito dalla legge o determinato dal giudice per il deposito della    &#13;
 sentenza, ovvero, nel caso previsto dall'art. 548, secondo comma, dal    &#13;
 giorno  in  cui è stata eseguita la notificazione e la comunicazione    &#13;
 dell'avviso di deposito;                                                 &#13;
      che, secondo  il  giudice  a  quo,  con  la  locuzione  "termine    &#13;
 stabilito dalla legge o determinato dal giudice per il deposito della    &#13;
 sentenza",   contenuta  nella  norma  impugnata,  il  legislatore  fa    &#13;
 riferimento, rispettivamente, ai termini di cui al  secondo  e  terzo    &#13;
 comma   dell'art.   544   del  codice  di  procedura  penale,  mentre    &#13;
 l'originario  termine  di  trenta  giorni,  previsto  dall'art.  544,    &#13;
 secondo  comma,  per il deposito della sentenza, ove la stesura della    &#13;
 motivazione non sia stata coeva  alla  lettura  del  dispositivo,  è    &#13;
 stato  successivamente ridotto dall'art. 6 del decreto-legge 1° marzo    &#13;
 1991, n. 60, convertito  dalla  legge  22  aprile  1991,  n.  133,  a    &#13;
 quindici giorni;                                                         &#13;
      che,  sempre  ad  avviso  del  giudice  remittente,  a causa del    &#13;
 mancato adeguamento del termine di trenta giorni  indicato  dall'art.    &#13;
 548,  secondo  comma,  del  codice  di procedura penale al termine di    &#13;
 quindici giorni previsto per il  deposito  della  sentenza  dall'art.    &#13;
 544,  secondo  comma, del medesimo codice, l'art. 585, secondo comma,    &#13;
 lett. c), impugnato darebbe  vita  ad  una  irragionevole  disciplina    &#13;
 uniforme  di  situazioni  tra  loro  differenti, facendo decorrere il    &#13;
 termine di impugnazione della sentenza dal 15° giorno  dalla  lettura    &#13;
 del  dispositivo  anche  nell'ipotesi  di  deposito della motivazione    &#13;
 avvenuto  in  epoca  compresa  fra  il  15°  e  il 30° giorno da tale    &#13;
 lettura, con una conseguente lesione del diritto di  difesa  connesso    &#13;
 al  fatto che - sempre nell'ipotesi di deposito della sentenza tra il    &#13;
 15° e  il  30°  giorno  -  non  sarebbe  richiesta  la  notificazione    &#13;
 dell'avviso di deposito alle parti private, in violazione degli artt.    &#13;
 3 e 24 della Costituzione;                                               &#13;
    Considerato  che la Corte, con la sentenza n. 364/1993, esaminando    &#13;
 una questione analoga a quella sollevata nel  presente  giudizio,  ha    &#13;
 già  affermato - con riferimento alla incongruenza venutasi a creare    &#13;
 a causa della mancanza di coordinamento normativo tra il nuovo  testo    &#13;
 dell'art.  544,  secondo  comma, del codice di procedura penale (come    &#13;
 modificato dall'art. 6 del decreto-legge n. 60 del  1991,  convertito    &#13;
 dalla  legge  n.  133  del  1991),  e  l'art. 548, secondo comma, del    &#13;
 medesimo codice - che le incertezze  che  derivano  da  tale  mancato    &#13;
 coordinamento    sono    state    superate   dall'univoco   indirizzo    &#13;
 interpretativo adottato in merito dalla Corte di cassazione,  che  ha    &#13;
 operato  una  ricostruzione  sistematica della normativa in questione    &#13;
 giungendo ad affermare che la revisione dell'art. 544, secondo comma,    &#13;
 introdotta dall'art. 6 del decreto-legge n.  60  del  1991  ha  anche    &#13;
 modificato,  in  senso  conforme,  l'art.  548, secondo comma, con la    &#13;
 conseguenza che "l'avviso di deposito deve essere  effettuato  quando    &#13;
 la  sentenza  non  è depositata entro il quindicesimo giorno, invece    &#13;
 dell'originario trentesimo giorno" (Cass. Sez. V,  8  febbraio  1993;    &#13;
 Cass. Sez. I, 4 dicembre 1992);                                          &#13;
      che  questa  Corte  nella  sentenza  citata  ha conseguentemente    &#13;
 affermato che secondo il diritto vivente la normativa richiamata "non    &#13;
 ha l'effetto di ridurre il termine di  trenta  giorni  per  impugnare    &#13;
 assegnato  alle parti dall'art. 585, primo comma, lett. b) del codice    &#13;
 di  procedura  penale  poiché   -   nel   caso   di   sentenza   non    &#13;
 contestualmente  motivata  e  depositata oltre il quindicesimo giorno    &#13;
 dalla pronuncia  -  va  comunque  notificato  alle  parti  stesse  (e    &#13;
 comunicato  al  pubblico  ministero)  l'avviso di deposito, mentre il    &#13;
 termine per  l'impugnazione  decorre  dal  giorno  in  cui  è  stata    &#13;
 eseguita la notificazione (o la comunicazione) dell'avviso stesso";      &#13;
      che,  pertanto,  l'art, 585, secondo comma, lett. c), del codice    &#13;
 di procedura penale non lede il diritto di difesa, sancito  dall'art.    &#13;
 24  della  Costituzione,  dal  momento  che,  diversamente  da quanto    &#13;
 sostenuto nell'ordinanza di rimessione, l'obbligo  di  notifica  alle    &#13;
 parti   dell'avviso   di   deposito  della  sentenza  sussiste  anche    &#13;
 nell'ipotesi in cui questa sia depositata in epoca  compresa  tra  il    &#13;
 quindicesimo  e  il  trentesimo giorno dalla lettura del dispositivo;    &#13;
 che la stessa norma non  risulta  neppure  lesiva  del  principio  di    &#13;
 uguaglianza,  dal  momento che, per quanto affermato nella richiamata    &#13;
 sentenza n. 364 del 1993, il  termine  per  impugnare,  nel  caso  di    &#13;
 sentenza  depositata  oltre  il  quindicesimo giorno dalla pronuncia,    &#13;
 comincia comunque a decorrere dal giorno in cui è  stato  notificato    &#13;
 alla  parte  l'avviso  di  deposito,  con  la  conseguenza che non si    &#13;
 verifica la disparità lamentata dal  giudice  remittente  in  ordine    &#13;
 alla decorrenza del termine di impugnazione;                             &#13;
      che,  pertanto,  la  questione  sollevata deve essere dichiarata    &#13;
 manifestamente infondata.                                                &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 585, secondo comma, lett. c), del codice  di    &#13;
 procedura  penale,  sollevata, in riferimento agli artt. 3 e 24 della    &#13;
 Costituzione,  dalla  Corte  d'Appello   di   Reggio   Calabria   con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 7 luglio 1994.                                   &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: CHELI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 20 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
