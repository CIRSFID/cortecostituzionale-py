<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>421</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:421</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/09/1990</data_decisione>
    <data_deposito>27/09/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi di legittimità costituzionale dell'art. 247 delle norme    &#13;
 d'attuazione, di coordinamento e transitorie del  vigente  codice  di    &#13;
 procedura  penale  (testo approvato con decreto legislativo 28 luglio    &#13;
 1989, n. 271) e artt. 438 e 448 dello stesso codice, promossi con  le    &#13;
 seguenti ordinanze:                                                      &#13;
      1) ordinanza emessa il 13 dicembre 1989 dalla Corte d'Appello di    &#13;
 Bari nel procedimento penale a carico di Luvera  Consolato  ed  altro    &#13;
 iscritta  al  n.  315  del registro ordinanze 1990 e pubblicata nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  23,  1ª  serie  speciale,    &#13;
 dell'anno 1990;                                                          &#13;
      2) ordinanza emessa il 14 dicembre 1989 dalla Corte d'appello di    &#13;
 Bari nel procedimento penale a carico di Del Frate Maurizio ed altri,    &#13;
 iscritta  al  n.  316  del registro ordinanze 1990 e pubblicata nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  23,  1ª  serie  speciale,    &#13;
 dell'anno 1990;                                                          &#13;
    Udito  nella  camera  di  consiglio dell'11 luglio 1990 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che la Corte d'appello di Bari, con due ordinanze del 13    &#13;
 e del 14 dicembre 1989, ha sollevato, in riferimento all'art. 3 della    &#13;
 Costituzione, questione di legittimità costituzionale:                  &#13;
       a)  dell'art.  247 delle norme d'attuazione, di coordinamento e    &#13;
 transitorie del vigente codice di procedura penale  (testo  approvato    &#13;
 con  decreto  legislativo  28 luglio 1989, n. 271) nella parte in cui    &#13;
 non estende l'esperibilità del  giudizio  abbreviato,  di  cui  agli    &#13;
 artt.  438  e  seguenti del nuovo codice di procedura penale anche ai    &#13;
 procedimenti in corso, in  grado  d'appello,  al  momento  della  sua    &#13;
 entrata in vigore;                                                       &#13;
       b)  dell'art. 438 del vigente codice di procedura penale, nella    &#13;
 parte in cui non prevede la rimovibilità del dissenso  del  pubblico    &#13;
 ministero,  da  parte  del  giudice, per l'esperibilità del giudizio    &#13;
 abbreviato,  così  come  previsto  dall'art.  448   per   l'istituto    &#13;
 dell'applicazione della pena su richiesta delle parti;                   &#13;
    Considerato  che,  per  l'identità  delle  questioni sollevate, i    &#13;
 giudizi possono essere riuniti;                                          &#13;
      che  -  in  ordine  alla  questione  sub  a) - questa Corte, con    &#13;
 sentenza n. 277 del 1990, ha dichiarato non fondata,  in  riferimento    &#13;
 all'art.   3   della   Costituzione,  la  questione  di  legittimità    &#13;
 costituzionale   dell'art.   247   delle   norme   d'attuazione,   di    &#13;
 coordinamento  e  transitorie  del vigente codice di procedura penale    &#13;
 (testo approvato con decreto legislativo  28  luglio  1989,  n.  271)    &#13;
 nella parte in cui limita l'ammissibilità del giudizio abbreviato ai    &#13;
 procedimenti in corso alla data d'entrata in vigore del nuovo  codice    &#13;
 di  procedura penale nei quali non siano state compiute le formalità    &#13;
 d'apertura del dibattimento di primo grado;                              &#13;
      che,  in  particolare,  nella  citata  sentenza  la Corte ha fra    &#13;
 l'altro  sottolineato  l'"inscindibile  unità   finalistica"   della    &#13;
 disposizione  di  cui al citato art. 247, osservando che la riduzione    &#13;
 della pena in tanto è consentita in quanto è diretta a  sollecitare    &#13;
 la  richiesta, da parte dell'imputato, dell'attivazione d'un istituto    &#13;
 inteso ad assicurare la rapida  definizione  del  maggior  numero  di    &#13;
 processi;   divenuto,   invece,   impossibile,   con  l'apertura  del    &#13;
 dibattimento  di  primo  grado,  raggiungere  le  finalità  che   il    &#13;
 legislatore  si  prefigge,  diventa  conseguentemente e razionalmente    &#13;
 impossibile all'imputato realizzare il c.d. "diritto" alla  riduzione    &#13;
 della pena;                                                              &#13;
      che,  essendo,  appunto,  lo  scopo  dell'istituto  del giudizio    &#13;
 abbreviato l'esclusione  della  fase  dibattimentale,  è  del  tutto    &#13;
 razionale  che, per i procedimenti in corso all'entrata in vigore del    &#13;
 nuovo codice di procedura penale,  tali  istituti  siano  stati  resi    &#13;
 applicabili  soltanto  quando  il loro scopo possa essere interamente    &#13;
 perseguito;                                                              &#13;
      che  la  precitata sentenza ha altresì aggiunto che irrazionale    &#13;
 sarebbe  semmai  l'applicabilità  del   giudizio   abbreviato   dopo    &#13;
 l'apertura  del  dibattimento  di primo grado; giacché in tal caso i    &#13;
 benefici concessi all'imputato non sarebbero  più  giustificati  né    &#13;
 dallo  scopo  (ormai  impossibile) d'eliminare la fase dibattimentale    &#13;
 né dal rischio assunto dall'imputato (il quale si  troverebbe  nella    &#13;
 comoda  situazione di decidere dopo che il pubblico ministero ha già    &#13;
 offerto le sue prove e comunque dopo aver potuto valutare l'andamento    &#13;
 del dibattimento stesso);                                                &#13;
      che  identica questione di legittimità costituzionale dell'art.    &#13;
 247 del decreto legislativo n. 271 del 1989, proprio nella  parte  in    &#13;
 cui  non  consente  all'imputato di richiedere il giudizio abbreviato    &#13;
 per i procedimenti in corso in grado d'appello all'entrata in  vigore    &#13;
 del  nuovo codice di procedura penale, sollevata, in riferimento agli    &#13;
 artt. 3 e 25 della Costituzione, dalla Corte d'appello di Ancona  con    &#13;
 ordinanza  del  3  novembre  1989  (Reg.  ord.  n. 271/1990) è stata    &#13;
 dichiarata manifestamente infondata con ordinanza n. 361 del 1990;       &#13;
      che, pertanto, non è producente il confronto fra imputati per i    &#13;
 quali il dibattimento di primo grado sia stato o non sia stato ancora    &#13;
 aperto  o  fra imputati che hanno pendenze penali in primo grado o in    &#13;
 grado  d'appello,   appunto   perché   si   tratta   di   situazioni    &#13;
 oggettivamente diverse;                                                  &#13;
      che, di conseguenza, la questione di legittimità costituzionale    &#13;
 sub a) va dichiarata manifestamente infondata;                           &#13;
      che  -  in  ordine  alla  questione  sub b) - la stessa è stata    &#13;
 sollevata in procedimenti già in corso alla data d'entrata in vigore    &#13;
 del nuovo codice di procedura penale;                                    &#13;
      che  analoghe  questioni di legittimità costituzionale - aventi    &#13;
 ad oggetto l'art. 438 del vigente codice di procedura  penale,  nella    &#13;
 parte  in  cui  non  prevede  che  il pubblico ministero sia tenuto a    &#13;
 motivare il proprio dissenso sulla richiesta  dell'imputato  d'essere    &#13;
 giudicato  con  rito  abbreviato e nella parte in cui non permette al    &#13;
 giudice, una volta ritenuto il dissenso  ingiustificato,  d'applicare    &#13;
 la  riduzione  di pena prevista dall'art. 442 del codice di procedura    &#13;
 penale - sono state dichiarate  da  questa  Corte  inammissibili  per    &#13;
 irrilevanza   con  la  sentenza  n.  66  del  1990  e  manifestamente    &#13;
 inammissibili con le ordinanze nn. 173, 174, 208, 210 e 253 del 1990,    &#13;
 per  il  motivo che, per quanto riguarda i procedimenti in corso alla    &#13;
 data d'entrata in vigore del nuovo codice  di  procedura  penale,  la    &#13;
 possibilità  di  far  luogo  al giudizio abbreviato è appositamente    &#13;
 disciplinata dall'art. 247 delle norme d'attuazione, di coordinamento    &#13;
 e transitorie del vigente codice di procedura penale (testo approvato    &#13;
 con decreto legislativo 28 luglio 1989, n.  271)  e  che  quindi  nei    &#13;
 detti  procedimenti l'art. 438 del vigente codice di procedura penale    &#13;
 non  può  ricevere  diretta  applicazione,  data  l'autonomia  della    &#13;
 disciplina   transitoria  in  materia  rispetto  alla  corrispondente    &#13;
 disciplina codicistica;                                                  &#13;
     che,  di conseguenza, la questione di legittimità costituzionale    &#13;
 sub b) va dichiarata manifestamente inammissibile;                       &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale   dell'art.   247   delle   norme   d'attuazione,   di    &#13;
 coordinamento  e  transitorie  del vigente codice di procedura penale    &#13;
 (testo approvato con decreto legislativo  28  luglio  1989,  n.  271)    &#13;
 sollevata,  in riferimento all'art. 3 della Costituzione, dalla Corte    &#13;
 d'appello di Bari con le ordinanze indicate in epigrafe;                 &#13;
    Dichiara   la   manifesta   inammissibilità  della  questione  di    &#13;
 legittimità costituzionale  dell'art.  438  del  vigente  codice  di    &#13;
 procedura   penale,   sollevata,  in  riferimento  all'art.  3  della    &#13;
 Costituzione,  dalla  Corte  d'appello  di  Bari  con   le   medesime    &#13;
 ordinanze.                                                               &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 settembre 1990.      &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: DELL'ANDRO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 settembre 1990.                       &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
