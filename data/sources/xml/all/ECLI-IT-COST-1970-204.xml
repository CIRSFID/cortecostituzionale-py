<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>204</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:204</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Angelo de Marco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/12/1970</data_decisione>
    <data_deposito>28/12/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 2772, primo  &#13;
 comma, del codice civile, e dell'art. 97 del R.D. 30 dicembre 1923,  n.  &#13;
 3269  (legge  di registro), promosso con ordinanza emessa il 10 gennaio  &#13;
 1969 dalla Corte d'appello di  Genova nel procedimento civile  vertente  &#13;
 tra  la  società  Imperiale  e  l'Amministrazione  delle finanze dello  &#13;
 Stato, iscritta al n. 140 del  registro  ordinanze  1969  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 128 del 21 maggio 1969.     &#13;
     Visti   gli   atti  di  costituzione  della  società  Imperiale  e  &#13;
 dell'Amministrazione delle finanze dello Stato  e  l'atto  d'intervento  &#13;
 del Presidente del Consiglio dei ministri;                               &#13;
     udito  nell'udienza  pubblica  del  25  novembre  1970  il  Giudice  &#13;
 relatore Angelo De Marco;                                                &#13;
     uditi l'avv. Arturo Carlo Jemolo, per la società Imperiale,  ed il  &#13;
 sostituto  avvocato  generale  dello  Stato  Luciano    Tracanna,   per  &#13;
 l'Amministrazione  finanziaria  e per il   Presidente del Consiglio dei  &#13;
 ministri.                                                                &#13;
     Ritenuto che la Corte di appello di Genova con ordinanza  emessa il  &#13;
 10 gennaio 1969 nel procedimento civile vertente  tra la  società  per  &#13;
 azioni  Imperiale  e  l'Amministrazione  delle   finanze dello Stato ha  &#13;
 ritenuto rilevante e  non    manifestamente  infondata  in  riferimento  &#13;
 all'art. 42, secondo e  terzo comma, della Costituzione la questione di  &#13;
 legittimità    costituzionale,  sollevata  dalla  società appellante,  &#13;
 degli artt.  2772, primo comma, del codice civile  e  97  del  R.D.  30  &#13;
 dicembre  1923,  n.  3269 (legge di registro), nella parte in cui  tali  &#13;
 norme riconoscono  allo  Stato,  in  relazione,    rispettivamente,  ai  &#13;
 crediti  per tributi indiretti in generale e  per l'imposta di registro  &#13;
 in particolare, un privilegio speciale   sugli  immobili  ai  quali  il  &#13;
 tributo   si  riferisce,  esercitabile  -  ove  si  tratti  di  imposta  &#13;
 principale o complementare - anche nei confronti dei terzi che si siano  &#13;
 resi acquirenti di tali immobili in epoca successiva  al  trasferimento  &#13;
 che ha dato luogo all'imposizione;                                       &#13;
     che  nel  giudizio  davanti  a  questa  Corte si sono costituiti la  &#13;
 società Imperiale e l'Amministrazione delle finanze dello Stato ed  è  &#13;
 intervenuto il Presidente del Consiglio dei ministri;                    &#13;
     Considerato  che  nell'ordinanza  di  rinvio  la rilevanza è stata  &#13;
 soltanto così motivata: "giacché una volta che  si  pervenga  -  come  &#13;
 questa   Corte   reputa  doversi  pervenire,  conformemente  all'avviso  &#13;
 espresso dai primi giudici - a considerare quella  di  cui  si  discute  &#13;
 un'imposta di registro principale e non suppletiva";                     &#13;
     che  questa sopra riportata appare piuttosto una mera  affermazione  &#13;
 che una vera e propria motivazione, tanto più necessaria in quanto:     &#13;
     a)  è  pacifico  che  la  cosiddetta  dichiarazione  di  "comando"  &#13;
 oggetto  della  imposta in questione datata 27 marzo 1945 fu presentata  &#13;
 all'Ufficio del registro di Genova (atti privati) il giorno  13  aprile  &#13;
 1945  allegata  alla  scrittura  privata  pure  in  data  27 marzo 1945  &#13;
 (regolarmente registrata) e non fu  assoggettata ad autonoma tassazione  &#13;
 per erronea omissione  dello Ufficio;                                    &#13;
     b) non soltanto  nell'accertamento  (vedasi,  in  atti,  l'articolo  &#13;
 2072,  campione atti civili dell'Ufficio del registro di Genova) ma nel  &#13;
 corso sia dei giudizi davanti  alle    Commissioni  tributarie  sia  di  &#13;
 quelli  davanti  all'autorità   giudiziaria ordinaria la imposta sulla  &#13;
 dichiarazione di "comando" fu sempre definita "suppletiva" e, quel  che  &#13;
 più conta, così risulta definita nella sentenza della Corte d'appello  &#13;
 di  Genova  17-28  luglio 1959, passata in cosa giudicata, anche se fra  &#13;
 parti diverse da quelle oggi in causa;                                   &#13;
     c) soltanto in data 30 maggio 1962 (vedasi  il  sopra  citato  art.  &#13;
 2072  campione  atti  civili),  cioè quando, esauriti i giudizi di cui  &#13;
 sopra, in tanto si poteva far valere il privilegio di  cui  alle  norme  &#13;
 denunciate,  in  quanto  si  fosse trattato di imposta principale, tale  &#13;
 venne  tardivamente  definita    dall'Ufficio  quella  che  anche   per  &#13;
 giudicato era ormai accertata come imposta suppletiva;                   &#13;
     d)   il   terzo   comma   dell'art.   7  della  legge  di  registro  &#13;
 (integralmente riprodotto dall'art. 1 del  R.D.  13  gennaio  1936,  n.  &#13;
 2313)  testualmente dispone: "Sono suppletive le tasse che si applicano  &#13;
 sopra un atto o una denunzia quando l'Ufficio del registro sia incorso,  &#13;
 al momento della registrazione dell'atto  o  nella  liquidazione  della  &#13;
 tassa  in base a denunzia, in errore od omissione, tanto sulla qualità  &#13;
 della tassa dovuta, quanto sui titoli  tassabili  risultanti  dall'atto  &#13;
 stesso o dalla stessa denunzia";                                         &#13;
     che,  pertanto,  è  necessario  che  il giudice a quo riesamini la  &#13;
 sussistenza della rilevanza tenuto conto degli elementi sopra esposti;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     Ordina la restituzione degli atti alla Corte di appello di Genova.   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 18 dicembre 1970.                             &#13;
     GIUSEPPE  BRANCA  - MICHELE FRAGALI - COSTANTINO MORTATI - GIUSEPPE  &#13;
 CHIARELLI - GIUSEPPE  VERZÌ - GIOVANNI  BATTISTA BENEDETTI -  FRANCESCO  &#13;
 PAOLO  BONIFACIO - LUIGI OGGIONI - ANGELO DE MARCO - ERCOLE ROCCHETTI -  &#13;
 ENZO CAPALOZZA - VINCENZO MICHELE TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
 REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
