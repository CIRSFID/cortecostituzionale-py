<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>151</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:151</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/03/1992</data_decisione>
    <data_deposito>01/04/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Cesare MIRABELLI, prof. Francesco GUIZZI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  2,  secondo    &#13;
 comma,   n.   3,  della  legge  7  maggio  1981,  n.  180  (Modifiche    &#13;
 all'ordinamento giudiziario militare di pace), promosso con ordinanza    &#13;
 emessa l'8 maggio 1991  dal  Tribunale  militare  di  La  Spezia  nel    &#13;
 procedimento penale a carico di Di Giovanni Angelo ed altro, iscritta    &#13;
 al  n.  510  del  registro ordinanza 1991 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica, n. 33, prima  serie  speciale,  dell'anno    &#13;
 1991;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito, nella camera di consiglio del 18 dicembre 1991, il  Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso  di  un  giudizio  penale a carico di un    &#13;
 sottufficiale (maresciallo maggiore) e di  un  ufficiale  (generale),    &#13;
 imputati di vari reati in concorso tra loro, il Tribunale militare di    &#13;
 La  Spezia,  con  ordinanza  emessa l'8 maggio 1991, ha sollevato, in    &#13;
 riferimento all'art. 3 della Costituzione, questione di  legittimità    &#13;
 costituzionale  dell'art. 2, secondo comma, n. 3 della legge 7 maggio    &#13;
 1981, n.  180  (Modifiche  all'ordinamento  giudiziario  militare  di    &#13;
 pace),  nella  parte  in  cui  prevede  che  il  giudice  non  togato    &#13;
 componente il collegio giudicante debba essere di grado pari a quello    &#13;
 dell'imputato (e comunque non inferiore al grado di ufficiale);          &#13;
      che  l'ordinanza  di  rinvio,  ritenuta   la   rilevanza   della    &#13;
 questione,  in  quanto  attinente  "alla  costituzione del giudice in    &#13;
 relazione  ad  una  normativa,  ritenuta  viziata,  che  consente  di    &#13;
 sollevare  -  seppure  in  astratto  - dubbi sull'imparzialità nella    &#13;
 formazione del convincimento di  uno  dei  componenti  il  collegio",    &#13;
 censura   il   principio   della   parità   di   grado,   introdotto    &#13;
 nell'ordinamento penale militare, per la scelta  del  componente  non    &#13;
 togato   del   tribunale,   ritenendo   irragionevole  che  il  grado    &#13;
 dell'imputato  "influisca  sulla  composizione  del  collegio,  nella    &#13;
 presupposizione   erronea   che  nell'ambito  della  categoria  degli    &#13;
 ufficiali  non  si  possa   soddisfare   l'esigenza   di   competenza    &#13;
 indipendentemente  dal  grado  del componente del collegio medesimo",    &#13;
 onde "tale partecipazione o almeno il  grado  rivestito"  sarebbe  di    &#13;
 scarsa  importanza,  non  rilevando che "solo in parte, ai fini della    &#13;
 stretta valutazione giuridica del fatto-reato";                          &#13;
      che   lo   stesso   giudice   rimettente   ritiene   che    tale    &#13;
 irragionevolezza  "appare  massima"  nel  caso oggetto del giudizio a    &#13;
 quo, in cui il sottufficiale, imputato in concorso con  un  ufficiale    &#13;
 generale,  verrebbe  discriminato  perché  del collegio che dovrebbe    &#13;
 giudicarlo  sarebbe  componente,  come  giudice   non   togato,   non    &#13;
 l'ufficiale  estratto  a  sorte  (nel  caso  che  ha  dato luogo alla    &#13;
 questione, per la specifica udienza era stato sorteggiato un  tenente    &#13;
 colonnello dell'esercito) relativamente alla categoria degli imputati    &#13;
 sottufficiali, bensì un ufficiale-generale pari grado dell'ufficiale    &#13;
 imputato di concorso nel medesimo reato;                                 &#13;
      che,  quindi,  la  norma  impugnata darebbe "rilevanza di fronte    &#13;
 alla legge ad una condizione  personale,  qual'è  il  grado,  di  un    &#13;
 imputato e del suo giudicante";                                          &#13;
      che  è  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 ministri, per il  tramite  della  Avvocatura  generale  dello  Stato,    &#13;
 concludendo per l'infondatezza della questione;                          &#13;
    Considerato  che  il primo profilo della questione di legittimità    &#13;
 costituzionale -  consistente  nella  denuncia  del  principio  della    &#13;
 parità  di  grado  nella  scelta  del componente laico del tribunale    &#13;
 militare, cioè del principio in base al quale, per essere la  scelta    &#13;
 limitata   agli  ufficiali,  verrebbe  a  riservarsi  un  trattamento    &#13;
 ingiustificatamente differenziato ai coimputati di grado inferiore  -    &#13;
 è   manifestamente  infondato,  avendo  già  questa  Corte  escluso    &#13;
 l'illegittimità  costituzionale  (sent.  n.  49  del   1989)   della    &#13;
 previsione  che  riserva la funzione di componente del collegio quale    &#13;
 giudice non togato ai soli  ufficiali,  al  fine  "di  assicurare  al    &#13;
 collegio l'apporto di una persona dotata di un buon livello culturale    &#13;
 e  di  quelle  cognizioni  più  ampie  e  più  complete che vengono    &#13;
 dall'inserimento in  compiti  di  maggiore  responsabilità",  attesa    &#13;
 l'esigenza  della  presenza  nel  collegio  giudicante  di  un membro    &#13;
 "chiamato a dare un qualificato contributo inerente alla peculiarità    &#13;
 della vita e dell'organizzazione militare";                              &#13;
      che, qualora nel reato abbiano concorso (come nel caso  di  specie)  un  ufficiale ed un sottufficiale, diviene inevitabile e quindi    &#13;
 non  irragionevole  né  discriminatorio  che  solo  l'ufficiale  sia    &#13;
 giudicato  da  un  pari grado e che gli altri coimputati siano invece    &#13;
 giudicati da un superiore, perché  in  ogni  caso  il  militare  non    &#13;
 ufficiale, sia o meno imputato in concorso con un ufficiale, dovrebbe    &#13;
 essere  giudicato  (secondo una norma che, come si è già detto, non    &#13;
 è stata ritenuta in contrasto con la Costituzione) da un ufficiale;     &#13;
      che, quanto poi alla eventualità prospettata nell'ordinanza  di    &#13;
 rinvio,  secondo  cui  "qualora fossero coimputati ufficiali di grado    &#13;
 diverso,  quello   di   grado   inferiore   sarebbe   necessariamente    &#13;
 discriminato", a parte che tale evenienza esula dal giudizio a quo in    &#13;
 cui  sono  coimputati  un ufficiale ed un sottufficiale, la questione    &#13;
 anche sotto tale profilo è manifestamente infondata;                    &#13;
      che, difatti, la norma, la quale prescrive che  il  giudice  non    &#13;
 togato  sia  un ufficiale pari grado, riposa da un lato sull'esigenza    &#13;
 di assicurare, come si è già rilevato,  l'apporto  di  una  persona    &#13;
 dotata  di  un  più  qualificato  livello  culturale  e, dall'altro,    &#13;
 sull'esigenza di evitare che il militare possa essere giudicato da un    &#13;
 inferiore,  che  verrebbe  a  trovarsi  in  posizione  di  soggezione    &#13;
 rispetto all'imputato di grado superiore nella gerarchia militare;       &#13;
      che perciò non appare irragionevole che,  nel  caso  in  cui  i    &#13;
 coimputati  siano  ufficiali di grado diverso, prevalga, come giudice    &#13;
 non togato, quello sorteggiato fra i  pari  grado  dell'ufficiale  di    &#13;
 grado   superiore,   perché   ciò   che   la   norma   intende  non    &#13;
 irragionevolmente evitare è che il giudice possa venire  a  trovarsi    &#13;
 in posizione di soggezione rispetto a qualcuno degli imputati;           &#13;
      che,  manifestamente,  neppure discriminatoria può ritenersi la    &#13;
 circostanza che (come nel caso del giudizio a quo), per effetto della    &#13;
 norma impugnata, il sottufficiale possa non avere  "quale  componente    &#13;
 del collegio quel militare giudice che a suo tempo era stato estratto    &#13;
 a  sorte per gli imputati sottufficiali (un tenente colonnello) e che    &#13;
 gli sarebbe naturalmente spettato solo che non  avesse  concorso  nel    &#13;
 reato con un ufficiale generale";                                        &#13;
      che, in proposito, tenuto conto delle considerazioni sino ad ora    &#13;
 svolte,   è  sufficiente  osservare  che,  una  volta  non  ritenuto    &#13;
 illegittimo che  il  militare  non  ufficiale  sia  giudicato  da  un    &#13;
 ufficiale,  non ha alcuna importanza il grado che questi rivesta, non    &#13;
 essendo di conseguenza irragionevole che - qualora quello sorteggiato    &#13;
 per l'udienza  per  la  categoria  dei  sottufficiali  sia  di  grado    &#13;
 inferiore  all'ufficiale concorrente nel reato con il sottufficiale -    &#13;
 prevalga come giudice non  togato  l'ufficiale  di  grado  superiore,    &#13;
 invece  sorteggiato  fra  i  pari  grado  dell'ufficiale  imputato in    &#13;
 concorso con il sottufficiale, perché è  questo  l'unico  modo  per    &#13;
 realizzare la finalità della norma che, come si è già detto, tende    &#13;
 ad  evitare  che il giudice non togato possa trovarsi in posizione di    &#13;
 soggezione gerarchica anche con  uno  solo  degli  imputati,  il  che    &#13;
 invece   accadrebbe   ove   si  condividesse  il  dubbio  prospettato    &#13;
 nell'ordinanza di rinvio;                                                &#13;
      che la questione è pertanto manifestamente infondata;              &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale dell'art. 2, secondo comma, n. 3, della legge 7 maggio    &#13;
 1981,  n.  180  (Modifiche  all'ordinamento  giudiziario  militare di    &#13;
 pace), sollevata, in riferimento all'art. 3 della  Costituzione,  dal    &#13;
 Tribunale militare di La Spezia con l'ordinanza indicata in epigrafe.    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 marzo 1992.                                &#13;
                       Il presidente: BORZELLINO                          &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 1° aprile 1992.                          &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
