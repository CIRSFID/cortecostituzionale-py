<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2022</anno_pronuncia>
    <numero_pronuncia>47</numero_pronuncia>
    <ecli>ECLI:IT:COST:2022:47</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMATO</presidente>
    <relatore_pronuncia>Augusto Antonio Barbera</relatore_pronuncia>
    <redattore_pronuncia>Augusto Antonio Barbera</redattore_pronuncia>
    <data_decisione>09/02/2022</data_decisione>
    <data_deposito>01/03/2022</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Giuliano AMATO; Giudici : Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1 della legge della Regione Sardegna 23 ottobre 2019, n. 18 (Disposizioni in materia di enti locali), promosso dal Presidente del Consiglio dei ministri con ricorso spedito per la notificazione il 23 dicembre 2019, depositato in cancelleria il 31 dicembre 2019, iscritto al n. 116 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 5, prima serie speciale, dell'anno 2020.&#13;
 Udito nella camera di consiglio del 9 febbraio 2022 il Giudice relatore Augusto Antonio Barbera; &#13;
 deliberato nella camera di consiglio del 9 febbraio 2022.&#13;
 Ritenuto che, con ricorso depositato in cancelleria il 31 dicembre 2019, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha proposto questione di legittimità costituzionale dell'art. 1 della legge della Regione Sardegna 23 ottobre 2019, n. 18 (Disposizioni in materia di enti locali), in riferimento agli artt. 1, primo comma, 3, 5, 114, 117, secondo comma, lettera p) della Costituzione e all'art. 3, primo comma, lettera b), della legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna);&#13;
 che, ai sensi della norma censurata, nelle more della riforma del sistema delle autonomie locali della Sardegna, i nuovi amministratori delle Province di Sassari, Nuoro, Oristano e Sud Sardegna - nominati dalla Giunta regionale, su proposta dell'Assessore regionale degli enti locali - possono restare in carica fino all'insediamento degli organi provinciali da eleggersi entro il 1° luglio 2020;&#13;
 che, a parere del ricorrente, la nomina degli amministratori straordinari, evitando l'insediamento degli organi provinciali da eleggersi secondo le previsioni della legge 7 aprile 2014, n. 56 (Disposizioni sulle città metropolitane, sulle province, sulle unioni e fusioni di comuni), violerebbe il principio di democraticità degli enti locali e ne lederebbe l'autonomia e la rappresentatività;&#13;
 che la Regione autonoma Sardegna non si è costituita in giudizio nel termine previsto dall'art. 19, comma 3, delle Norme integrative per i giudizi dinanzi alla Corte costituzionale, vigente ratione temporis (trenta giorni dalla scadenza del termine stabilito per il deposito del ricorso);&#13;
 che allo scadere di detto termine è stata fissata l'udienza pubblica del 26 gennaio 2021;&#13;
 che la trattazione della causa è stata rinviata a nuovo ruolo in accoglimento di due istanze depositate dall'Avvocatura generale dello Stato, rispettivamente, il 12 gennaio 2021 e il 10 giugno 2021, che prospettavano la possibilità di una rinuncia al gravame all'esito della celebrazione delle elezioni di secondo grado dei Presidenti delle Province della Sardegna, posticipate dapprincipio al 30 gennaio 2021 e in seguito in una finestra temporale compresa tra il 15 settembre e il 15 ottobre 2021;&#13;
 che, con decreto del 13 ottobre 2021 il Presidente della Corte costituzionale ha fissato la discussione del giudizio all'udienza pubblica dell'8 febbraio 2022;&#13;
 che il 19 novembre 2021 l'Avvocatura generale dello Stato ha depositato in cancelleria la relata di notifica del ricorso, effettuata a mezzo posta, che attesta una mancata consegna del plico per irreperibilità del destinatario presso l'indirizzo indicato nell'atto di gravame: "Via dei Giornalisti, 6 - Cagliari";&#13;
 che da fonti aperte la sede della Regione autonoma Sardegna è allocata non in Via dei Giornalisti n. 6, bensì in Viale Trento n. 69;&#13;
 che il 25 gennaio 2022 l'Avvocatura generale dello Stato ha depositato atto di rinuncia, rappresentando che «è venuto meno l'interesse a coltivare il ricorso», in quanto «è intervenuta una modifica sostanziale del quadro normativo, riconducibile ad eventi estrinseci, tali da indurre a ritenere che oggi non vi sia più alcuna connessione causale tra la lesione dei principi costituzionali che si assumono violati e la norma scrutinata»;&#13;
 che, in considerazione della sopravvenuta rinuncia, è stata fissata la camera di consiglio del 9 febbraio 2022.&#13;
 Considerato che l'esito negativo della notifica del ricorso è imputabile al ricorrente, il quale ha errato nell'individuare l'indirizzo dell'ente convenuto, agevolmente rinvenibile su fonti aperte;&#13;
 che per costante giurisprudenza di legittimità (Corte di cassazione, sezione quarta civile, sentenza 24 giugno 2020, n. 12410; Corte di cassazione, sezioni unite civili, sentenze 20 luglio 2016, n. 14916 e n. 14917) e del Consiglio di Stato (sezione quinta, sentenza 7 febbraio 2018, n. 817), recepita anche da questa Corte (ordinanza n. 101 del 2017), la notificazione del ricorso in un luogo errato, diverso dalla sede dell'ente, non determina l'inesistenza della notificazione, bensì la sua nullità;&#13;
 che il ricorrente, a fronte di tale nullità, non ha proceduto a rinnovare la notifica del ricorso;&#13;
 che, dunque, atteso il decorso del termine per impugnare senza che all'ente convenuto sia stato notificato l'atto di gravame e non essendosi altrimenti instaurato il contraddittorio, il ricorso è manifestamente inammissibile per decadenza;&#13;
 che, assurgendo l'integrità del contraddittorio a questione logicamente preliminare rispetto all'estinzione del giudizio, la nullità della notifica del ricorso preclude ogni valutazione concernente gli effetti dell'atto di rinuncia depositato dal ricorrente.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara manifestamente inammissibile la questione di legittimità costituzionale dell'art. l della legge della Regione Sardegna 23 ottobre 2019, n. 18, recante «Disposizioni in materia di enti locali», promossa dal Presidente del Consiglio dei ministri, in riferimento agli. artt. 1, primo comma, 3, 5, 114, 117, secondo comma, lettera p), della Costituzione e all'art. 3, primo comma, lettera b), della legge costituzionale 26 febbraio 1948, n. 3 (Statuto speciale per la Sardegna), con il ricorso indicato in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 febbraio 2022.&#13;
 F.to:&#13;
 Giuliano AMATO, Presidente&#13;
 Augusto Antonio BARBERA, Redattore&#13;
 Roberto MILANA, Direttore della Cancelleria&#13;
 Depositata in Cancelleria l'1 marzo 2022.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
