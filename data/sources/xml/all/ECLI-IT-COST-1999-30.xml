<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>30</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:30</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/02/1999</data_decisione>
    <data_deposito>11/02/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 513,  comma  1,    &#13;
 del  codice di procedura penale, come modificato dalla legge 7 agosto    &#13;
 1997, n. 267 (Modifica delle disposizioni  del  codice  di  procedura    &#13;
 penale  in  tema  di valutazione delle prove), promosso con ordinanza    &#13;
 emessa il 6 luglio 1998 dal Tribunale di Nola nel procedimento penale    &#13;
 a carico di D. F. ed altri, iscritta al n. 808 del registro ordinanze    &#13;
 1998 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  44,    &#13;
 prima serie speciale, dell'anno 1998.                                    &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del  13  gennaio  1999  il  giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto  che  il  Tribunale  di  Nola  (r.o.  n.  808 del 1998) ha    &#13;
 sollevato, in riferimento agli  artt.  3,  25,  secondo  comma,  101,    &#13;
 secondo  comma, 111, primo comma, e 112 della Costituzione, questione    &#13;
 di legittimità costituzionale dell'art. 513, comma 1, del codice  di    &#13;
 procedura  penale,  come modificato dalla legge 7 agosto 1997, n. 267    &#13;
 (Modifica delle disposizioni del codice di procedura penale  in  tema    &#13;
 di  valutazione delle prove), nella parte in cui tale norma subordina    &#13;
 al consenso degli altri  imputati  l'utilizzabilità  ai  fini  della    &#13;
 decisione delle dichiarazioni rese dal coimputato che non si presenti    &#13;
 al  dibattimento  o  si  avvalga  in  tale sede della facoltà di non    &#13;
 rispondere;                                                              &#13;
     che a giudizio del rimettente la norma impugnata viola l'art.   3    &#13;
 Cost.,  per  l'irragionevole sacrificio imposto, in nome della tutela    &#13;
 del contraddittorio, all'utilizzazione di  fonti  di  prova  divenute    &#13;
 irripetibili,  nonché  per  la  disparità  di  trattamento che tale    &#13;
 disciplina  comporta  nei  confronti  delle  dichiarazioni  rese  dai    &#13;
 prossimi   congiunti   che   si   avvalgono  della  facoltà  di  non    &#13;
 testimoniare e delle dichiarazioni  rese  da  un  imputato  di  reato    &#13;
 connesso,  di  cui non sia possibile ottenere la presenza per fatti o    &#13;
 circostanze imprevedibili (art. 513, comma 2, prima parte, cod. proc.    &#13;
 pen.);                                                                   &#13;
     che ancora, a giudizio del rimettente, sono violati gli artt.   3    &#13;
 e  112  Cost.,  perché  l'impossibilità  di utilizzare atti, la cui    &#13;
 irripetibilità è oggettivamente sopravvenuta e  non  poteva  essere    &#13;
 prevista,   determina   un   irragionevole   ostacolo   all'esercizio    &#13;
 dell'azione penale;                                                      &#13;
     che vi sarebbe infine lesione degli artt. 25, 101, comma secondo,    &#13;
 e 111 Cost., perché la disciplina impugnata attribuisce  alle  parti    &#13;
 il   potere   di   disporre  della  prova,  sottraendola  così  alla    &#13;
 valutazione del giudice e  sacrificando  la  finalità  primaria  del    &#13;
 processo  penale,  che  è  la  ricerca  della verità per una giusta    &#13;
 decisione;                                                               &#13;
     che la questione è stata sollevata nel corso di un  dibattimento    &#13;
 nel  quale  un  imputato,  citato per la prima volta a comparire dopo    &#13;
 l'entrata in vigore della legge per essere sottoposto  ad  esame,  ha    &#13;
 dichiarato  di  rinunciare  a  presenziare  al  dibattimento, e che i    &#13;
 difensori degli altri imputati non hanno prestato  il  consenso  alla    &#13;
 utilizzazione delle dichiarazioni rese in precedenza;                    &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo  che la questione sia dichiarata inammissibile per l'omessa    &#13;
 indicazione dei parametri costituzionali di  riferimento  e  comunque    &#13;
 perché  analoga ad altre sulle quali la Corte si è già pronunciata    &#13;
 con la sentenza n. 361 del 1998.                                         &#13;
   Considerato  che  il  rimettente,  muovendo  dal  quadro  normativo    &#13;
 risultante  dalle  modifiche introdotte dalla legge 7 agosto 1997, n.    &#13;
 267, sottopone a censura il regime di inutilizzabilità ai fini della    &#13;
 decisione, in mancanza  del  consenso  degli  altri  imputati,  delle    &#13;
 dichiarazioni   rese   sul   fatto   altrui  dal  coimputato  che  in    &#13;
 dibattimento rifiuti di  sottoporsi  all'esame  o  si  avvalga  della    &#13;
 facoltà di non rispondere;                                              &#13;
     che, successivamente alla emissione dell'ordinanza di rimessione,    &#13;
 questa  Corte,  con  sentenza n. 361 del 1998, ha inciso sul predetto    &#13;
 quadro normativo, dichiarando  la  illegittimità  costituzionale  in    &#13;
 parte qua tra l'altro, degli artt. 513, comma 2, ultimo periodo e 210    &#13;
 del codice di procedura penale;                                          &#13;
     che,  per  effetto di detta pronuncia, qualora il coimputato, che    &#13;
 abbia in  precedenza  reso  dichiarazioni  su  fatti  concernenti  la    &#13;
 responsabilità  di  altri, in dibattimento rifiuti o comunque ometta    &#13;
 in tutto o in parte di  rispondere  su  tali  fatti,  si  applica  la    &#13;
 disciplina  degli artt. 210 e 513, comma 2, cod. proc. pen., nonché,    &#13;
 in  mancanza  dell'accordo   delle   parti,   il   meccanismo   delle    &#13;
 contestazioni  previsto  dall'art.   500, commi 2-bis e 4, cod. proc.    &#13;
 pen;                                                                     &#13;
     che  pertanto  occorre  restituire gli atti al giudice rimettente    &#13;
 affinché verifichi se, alla luce della nuova disciplina  applicabile    &#13;
 a  seguito della sentenza n. 361 del 1998, la questione sollevata sia    &#13;
 tuttora rilevante.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Ordina la restituzione degli atti al Tribunale di Nola.                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 febbraio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria l'11 febbraio 1999.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
