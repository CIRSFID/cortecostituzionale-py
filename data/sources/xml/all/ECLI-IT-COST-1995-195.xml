<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>195</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:195</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/05/1995</data_decisione>
    <data_deposito>26/05/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 8, comma 5, del    &#13;
 decreto-legge  21  marzo 1988, n. 86 (Norme in materia previdenziale,    &#13;
 di occupazione giovanile e di mercato  del  lavoro,  nonché  per  il    &#13;
 potenziamento  del  sistema  informatico  del  Ministero del lavoro e    &#13;
 della previdenza sociale), convertito nella legge 20 maggio 1988,  n.    &#13;
 160,  promosso  con  ordinanza emessa il 1 agosto 1994 dal Pretore di    &#13;
 Viterbo nel procedimento  civile  vertente  tra  Adriani  Giuseppe  e    &#13;
 l'INPS  iscritta  al  n. 599 del registro ordinanze 1994 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  42,  prima   serie    &#13;
 speciale, dell'anno 1994;                                                &#13;
    Visto   l'atto   di   costituzione  dell'INPS  nonché  l'atto  di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito nell'udienza pubblica del 2 maggio 1995 il Giudice  relatore    &#13;
 Luigi Mengoni;                                                           &#13;
    Uditi  gli avv.ti Giacomo Giordano e Giuseppe Fabiani per l'INPS e    &#13;
 l'Avvocato dello Stato Giuseppe Stipo per il Presidente del Consiglio    &#13;
 dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso del  giudizio  promosso  da  Giuseppe  Adriani  per    &#13;
 ottenere  l'accertamento  negativo dell'obbligazione di restituire la    &#13;
 somma di 1.663.666 lire pretesa  dall'INPS  in  quanto  indebitamente    &#13;
 percepita  a  titolo  di  integrazione salariale straordinaria per il    &#13;
 periodo 8 febbraio 1989 - 31 marzo 1989, e la condanna  dell'Istituto    &#13;
 a  corrispondere  le  somme  dovute al medesimo titolo per il periodo    &#13;
 successivo  fino  al  dicembre  1989,  il  Pretore  di  Viterbo,  con    &#13;
 ordinanza  in  data  1 agosto 1994, ha sollevato, in riferimento agli    &#13;
 artt.  3  e  38  della  Costituzione,   questione   di   legittimità    &#13;
 costituzionale  dell'art. 8, comma 5, del d.-l. 21 marzo 1988, n. 86,    &#13;
 convertito nella legge  20  maggio  1988,  n.  160,  che  prevede  la    &#13;
 decadenza  del  lavoratore dal diritto al trattamento di integrazione    &#13;
 salariale,  qualora  non   abbia   provveduto   a   dare   preventiva    &#13;
 comunicazione  all'INPS  dello  svolgimento  di attività lavorativa,    &#13;
 anche nel caso di occupazione temporanea o saltuaria.                    &#13;
    2.  -  Ad  avviso  del  giudice  rimettente,  la  norma  impugnata    &#13;
 contrasta:  a)  con  l'art. 3 della Costituzione sotto il profilo del    &#13;
 giudizio di eguaglianza, perché tratta  alla  medesima  stregua  due    &#13;
 casi   diversi,   quali  il  caso  del  lavoratore  che  si  rioccupa    &#13;
 stabilmente e il caso del lavoratore che, come  nella  specie,  trova    &#13;
 un'occupazione  temporanea  di  breve  durata; b) ancora con l'art. 3    &#13;
 della Costituzione sotto  il  principio  di  ragionevolezza,  perché    &#13;
 applica  la  stessa  grave  sanzione della revoca del trattamento sia    &#13;
 all'omissione che al semplice  ritardo  della  comunicazione,  e  nel    &#13;
 secondo   caso  indipendentemente  dalla  possibilità  dell'INPS  di    &#13;
 detrarre tempestivamente dall'indennità le giornate lavorate e senza    &#13;
 tenere conto dello stato soggettivo del lavoratore  inadempiente;  c)    &#13;
 con  l'art.  38,  secondo  comma,  della Costituzione, perché per un    &#13;
 periodo più o meno  lungo  il  lavoratore  viene  privato  di  mezzi    &#13;
 adeguati   alle   esigenze  di  vita.  A  sostegno  di  tali  ragioni    &#13;
 l'ordinanza  ritiene  infine  richiamabili  le  argomentazioni  della    &#13;
 sentenza di questa Corte n. 78 del 1988.                                 &#13;
    3.  -  Nel  giudizio  davanti  alla  Corte  costituzionale  si  è    &#13;
 costituito l'INPS chiedendo una dichiarazione di  infondatezza  della    &#13;
 questione.                                                               &#13;
    Secondo l'Istituto la ratio della disposizione impugnata è quella    &#13;
 di  sanzionare la violazione dell'obbligo di comunicazione preventiva    &#13;
 dello svolgimento  di  attività  lavorativa,  per  la  potenzialità    &#13;
 dannosa  che  essa  ha  rispetto all'interesse pubblico alla corretta    &#13;
 gestione dell'intervento  di  integrazione  salariale.  Sotto  questo    &#13;
 profilo  le due situazioni distinte dal giudice a quo si identificano    &#13;
 invece  in  un medesimo tipo di trasgressione, e quindi richiamano la    &#13;
 stessa   misura   sanzionatoria   indipendentemente   dalla    durata    &#13;
 dell'attività lavorativa non tempestivamente comunicata.                &#13;
    D'altra  parte,  la prospettazione dell'ordinanza, che nel caso di    &#13;
 occupazione temporanea vorrebbe sostituire alla sanzione della revoca    &#13;
 del trattamento la semplice sospensione per il periodo di ripresa del    &#13;
 lavoro, equivale in realtà  ad  abolizione  di  ogni  sanzione,  con    &#13;
 conseguente  ingiustificata  equiparazione  dei  lavoratori che hanno    &#13;
 trascurato l'obbligo di comunicazione  all'INPS  a  quelli  che  tale    &#13;
 obbligo hanno rispettato.                                                &#13;
    Non  pertinente  infine, ad avviso dell'INPS, è il richiamo della    &#13;
 sentenza  n.  78  del  1988,  relativa  a  tutt'altra   materia,   le    &#13;
 prestazioni  di  malattia, per le quali i controlli sulla persistenza    &#13;
 del presupposto del diritto, cioè lo stato di morbilità, sono molto    &#13;
 più agevoli.                                                            &#13;
    4. - È intervenuto il  Presidente  del  Consiglio  dei  ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, chiedendo che la questione    &#13;
 sia dichiarata manifestamente infondata con argomenti sostanzialmente    &#13;
 analoghi a quelli svolti dall'INPS.<diritto>Considerato in diritto</diritto>1.  -  Il  Pretore di Viterbo mette in dubbio, in riferimento agli    &#13;
 artt. 3 e  38  della  Costituzione,  la  legittimità  costituzionale    &#13;
 dell'art.  8,  comma  5,  del  d.-l. 21 marzo 1988, n. 86, convertito    &#13;
 nella legge 20 maggio 1988, n. 160,  che  prevede  la  decadenza  del    &#13;
 lavoratore  dal  diritto  al  trattamento  di  integrazione salariale    &#13;
 straordinaria,  qualora  non  abbia  provveduto  a  dare   preventiva    &#13;
 comunicazione  all'INPS  dello  svolgimento  di attività lavorativa,    &#13;
 anche nel caso di occupazione temporanea o saltuaria.                    &#13;
    2. - La questione non è fondata.                                     &#13;
    La  norma  impugnata  non  accomuna  nella   medesima   previsione    &#13;
 sanzionatoria due ipotesi diverse, quella del lavoratore collocato in    &#13;
 Cassa integrazione guadagni, il quale trova un nuovo impiego a durata    &#13;
 indeterminata  e  a  tempo  pieno  e  quella del lavoratore che trova    &#13;
 soltanto offerte di lavori temporanei o saltuari. L'art. 8, comma  5,    &#13;
 del  d.-l.  n. 86 del 1988 si riferisce alla seconda ipotesi, la sola    &#13;
 compatibile  con  la  continuazione  dello   stato   di   sospensione    &#13;
 dell'originario  rapporto  di  lavoro,  che  è  il  presupposto  del    &#13;
 trattamento di integrazione salariale.                                   &#13;
    Nella prima ipotesi  il  nuovo  impiego  a  tempo  pieno  e  senza    &#13;
 prefissione  di  termine,  alle  dipendenze  di  un diverso datore di    &#13;
 lavoro, comporta la risoluzione del rapporto  precedente  e,  quindi,    &#13;
 non già la sanzione della decadenza comminata dal comma 5, bensì la    &#13;
 perdita  del  diritto  al  trattamento  di integrazione salariale per    &#13;
 cessazione del rapporto di lavoro che ne costituiva il fondamento. Al    &#13;
 nuovo datore di lavoro incombe l'obbligo di  comunicare  l'assunzione    &#13;
 del lavoratore all'INPS.                                                 &#13;
    Non  appropriato  è il richiamo dell'ordinanza di rimessione alla    &#13;
 sentenza n. 78 del  1988  di  questa  Corte,  concernente  tutt'altra    &#13;
 materia,  la  quale  sanziona  con  la  decadenza  dal  diritto  alla    &#13;
 prestazione previdenziale un comportamento impeditivo  del  controllo    &#13;
 dell'INPS successivo, non anteriore (nella forma di una comunicazione    &#13;
 preventiva)  al  fatto  in  ordine  al quale il controllo deve essere    &#13;
 eseguito.                                                                &#13;
    3.  -  Inconsistente  è  pure l'altra censura, rivolta alla norma    &#13;
 impugnata ancora in riferimento all'art. 3 della Costituzione, di non    &#13;
 distinguere tra omissione e ritardo della comunicazione.  Poiché  la    &#13;
 norma esige una comunicazione "preventiva", i due casi rifluiscono in    &#13;
 un medesimo tipo di comportamento omissivo.                              &#13;
    4.  -  Non  è  violato  nemmeno  l'art.  38, secondo comma, della    &#13;
 Costituzione. Il diritto dei lavoratori sancito  da  questo  precetto    &#13;
 costituzionale  spetta  in  concreto  in  quanto  sussistano tutte le    &#13;
 condizioni stabilite dalla legge. Comunque il lavoratore decaduto dal    &#13;
 diritto all'integrazione  salariale  straordinaria  per  inosservanza    &#13;
 dell'obbligo  di  comunicazione  previsto  dall'art.  8, comma 5, del    &#13;
 d.-l. n. 86 del 1988, può ottenere, concorrendone  i  requisiti,  il    &#13;
 trattamento ordinario di disoccupazione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 8, comma 5, del d.-l.  21  marzo  1988,  n.  86  (Norme  in    &#13;
 materia  previdenziale,  di  occupazione  giovanile  e di mercato del    &#13;
 lavoro, nonché per il  potenziamento  del  sistema  informatico  del    &#13;
 Ministero  del  lavoro  e della previdenza sociale), convertito nella    &#13;
 legge 20 maggio 1988, n. 160, sollevata, in riferimento agli artt.  3    &#13;
 e  38  della  Costituzione, dal Pretore di Viterbo con l'ordinanza in    &#13;
 epigrafe.                                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 maggio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 maggio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
