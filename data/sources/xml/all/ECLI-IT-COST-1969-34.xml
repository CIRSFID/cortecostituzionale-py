<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>34</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:34</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/02/1969</data_decisione>
    <data_deposito>17/03/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI &#13;
 BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Dott. NICOLA REALE, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel   giudizio  di  legittimità  costituzionale  del  decreto  del  &#13;
 Presidente della Repubblica 9 maggio 1961, n. 740, nella parte  in  cui  &#13;
 rende  efficace  erga  omnes  l'art.  7,  comma  terzo,  del  contratto  &#13;
 collettivo di lavoro 22 settembre 1959 per gli operai dipendenti  dalle  &#13;
 imprese  delle  industrie edilizia ed affini della provincia di Milano,  &#13;
 promosso con ordinanza emessa il 21 settembre  1967  dal  tribunale  di  &#13;
 Milano   nel  procedimento  civile  vertente  tra  la  Cassa  edile  di  &#13;
 mutualità e assistenza di  Milano  ed  il  fallimento  di  Re  Cecconi  &#13;
 Giovanni,  iscritta  al n. 112 del Registro ordinanze 1968 e pubblicata  &#13;
 nella Gazetta Ufficiale della Repubblica n. 203 del 10 agosto 1968.      &#13;
     Udita nella camera di consiglio del 27 febbraio 1969  la  relazione  &#13;
 del Giudice Costantino Mortati.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso del giudizio civile di opposizione allo stato passivo del  &#13;
 fallimento  di  Re  Cecconi  Giovanni  promosso  dalla  Cassa  edile di  &#13;
 mutualità e assistenza di Milano per ottenere il riconoscimento di  un  &#13;
 credito  rappresentato dalle somme dovute dal fallito ai dipendenti per  &#13;
 ferie e gratifica natalizia, il tribunale di Milano ha  sollevato,  con  &#13;
 ordinanza   in  data  21  settembre  1967,  questione  di  legittimità  &#13;
 costituzionale dell'articolo unico del  decreto  del  Presidente  della  &#13;
 Repubblica  9  maggio  1961,  n. 740, nella parte in cui rende efficace  &#13;
 erga omnes l'art. 7, comma terzo, dell'accordo collettivo 22  settembre  &#13;
 1959,  integrativo  del  contratto  collettivo  nazionale  di lavoro 24  &#13;
 luglio 1959, da valere per gli operai dipendenti  dalle  imprese  delle  &#13;
 industrie  edilizia  e  affini  della  provincia  di  Milano,  il quale  &#13;
 stabilisce che l'accantonamento dell'importo delle  percentuali  dovute  &#13;
 per  gratifica  natalizia,  ferie  e  festività deve essere effettuato  &#13;
 presso la suddetta Cassa edile.                                          &#13;
     Nell'ordinanza si fa presente che l'opponente non ha fornito  prove  &#13;
 dell'appartenenza  dell'impresa  fallita  ad una delle associazioni che  &#13;
 stipularono il contratto collettivo e che  la  norma  invocata  risulta  &#13;
 pertanto  applicabile  soltanto  in  virtù della estensione erga omnes  &#13;
 della sua efficacia disposta con il decreto suindicato.                  &#13;
     Si  osserva  altresì  che  con  la sentenza n. 129 del 1963 questa  &#13;
 Corte ha dichiarato l'illegittimità costituzionale dell'articolo unico  &#13;
 del decreto del Presidente della Repubblica 14 luglio  1960,  n.  1032,  &#13;
 nella  parte in cui rendeva efficace erga omnes l'art. 62 del contratto  &#13;
 collettivo 24 luglio 1959 che disciplinava  l'istituzione  delle  Casse  &#13;
 edili  ed anche l'art. 34, relativo al trattamento economico per ferie,  &#13;
 gratifica natalizia e festività, "pel riferimento alle Casse edili  di  &#13;
 cui  alla  fine del terzultimo comma", e che poiché tale pronuncia non  &#13;
 si estende automaticamente alle corrispondenti statuizioni dell'accordo  &#13;
 provinciale, è necessario  che  la  consequenziale  illegittimità  di  &#13;
 queste sia dichiarata mediante una nuova pronuncia della Corte.          &#13;
     In  considerazione di ciò, il tribunale ha disposto la sospensione  &#13;
 del giudizio e la trasmissione degli atti a questa Corte.  L'ordinanza,  &#13;
 notificata  e  comunicata a termini di legge, è stata pubblicata nella  &#13;
 Gazzetta Ufficiale n. 203 del 10 agosto 1968. Nessuna delle parti si è  &#13;
 costituita nel giudizio così promosso.<diritto>Considerato in diritto</diritto>:                          &#13;
     La questione proposta con l'ordinanza del tribunale  di  Milano  si  &#13;
 presenta  corrispondente  a  quella  risolta con la sentenza n. 129 del  &#13;
 1963 e confermata in altre successive (n. 31, 59, 78, 79 e 97 del 1964,  &#13;
 n. 100 del 1965, n. 48 del 1966, n. 41 e 73 del  1967)  e,  riguardando  &#13;
 una   norma  che  ha  una  portata  distinta  da  quella  espressamente  &#13;
 dichiarata  illegittima  con  tale  pronuncia,  anche  se  per  il  suo  &#13;
 contenuto analoga, deve essere nuovamente decisa con sentenza.           &#13;
     Nel  merito  tuttavia  non  vi  è che da ripetere quanto allora fu  &#13;
 detto e cioè che le disposizioni degli accordi e contratti  collettivi  &#13;
 relative  agli  obblighi  derivanti  per  gli  addetti  alle  industrie  &#13;
 edilizie  ed  affini  dalla  costituzione   delle   casse   edili   non  &#13;
 corrispondono  alle  finalità  per  l'adempimento delle quali è stato  &#13;
 attribuito il potere legislativo  delegato  ai  sensi  della  legge  14  &#13;
 luglio 1959, n.  741, e pertanto tali obblighi non possono essere fatti  &#13;
 valere  obbligatoriamente  anche  nei  confronti  dei non iscritti alle  &#13;
 associazioni che li hanno stipulati.                                     &#13;
     Donde l'illegittimità  della  norma  qui  impugnata  che  -  senza  &#13;
 neppure  consentire  alternative,  come  faceva  invece  l'art.  34 del  &#13;
 contratto nazionale - prescrive l'accantonamento presso la cassa  edile  &#13;
 delle somme dovute per gratifica natalizia, ferie e festività.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale  dell'articolo unico del  &#13;
 decreto del Presidente della Repubblica 9 maggio 1961,  n.  740,  nella  &#13;
 parte  in  cui rende obbligatorio erga omnes il terzo comma dell'art. 7  &#13;
 dell'accordo collettivo 22 settembre 1959,  integrativo  del  contratto  &#13;
 collettivo nazionale di lavoro 24 luglio 1959, da valere per gli operai  &#13;
 dipendenti  dalle  imprese  delle  industrie  edilizia  e  affini della  &#13;
 provincia di Milano.                                                     &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 febbraio 1969.       &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - NICOLA REALE.</dispositivo>
  </pronuncia_testo>
</pronuncia>
