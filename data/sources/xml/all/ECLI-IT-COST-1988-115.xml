<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>115</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:115</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1988</data_decisione>
    <data_deposito>26/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco P. CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionale dell'art. 10- bis della    &#13;
 legge  8  agosto  1980,  n.   441   ("Conversione   in   legge,   con    &#13;
 modificazioni,  del decreto-legge 1° luglio 1980, n. 285, concernente    &#13;
 la disciplina transitoria delle  funzioni  dell'assistenza  sanitaria    &#13;
 delle  unità sanitarie locali"), promossi con ordinanze emesse il 28    &#13;
 gennaio 1981 (n. 2 ordinanze) dalla Corte  dei  Conti  -  Sezione  II    &#13;
 giurisdizionale,  e il 14 gennaio 1983 dal T.A.R. del Lazio - Sezione    &#13;
 staccata di Latina, iscritte rispettivamente ai nn.  844  e  845  del    &#13;
 registro  ordinanze  1982  e  al  n. 94 del registro ordinanze 1984 e    &#13;
 pubblicate nella Gazzetta Ufficiale della Repubblica nn.  121  e  128    &#13;
 dell'anno 1983 e n. 169 dell'anno 1984;                                  &#13;
    Visti  gli atti di costituzione di Pizzorni G. Luigi ed altri e di    &#13;
 Retti Roberto ed altri nonché l'atto di  intervento  del  Presidente    &#13;
 del Consiglio dei ministri;                                              &#13;
    Udito  nella  Camera  di consiglio del 10 dicembre 1987 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso  di  alcuni giudizi aventi ad oggetto la    &#13;
 responsabilità di amministratori di enti  pubblici  ospedalieri,  la    &#13;
 Corte  dei  conti con due identiche ordinanze in data 28 gennaio 1981    &#13;
 (reg. ord. nn. 844 e 845 del 1982),  ha  sollevato  la  questione  di    &#13;
 legittimità  costituzionale  dell'art.  10- bis della legge 8 agosto    &#13;
 1980,  n.  441  ("Conversione  in  legge,  con   modificazioni,   del    &#13;
 decreto-legge  1°  luglio  1980,  n.  285,  concernente la disciplina    &#13;
 transitoria delle funzioni  dell'assistenza  sanitaria  delle  unità    &#13;
 sanitarie locali");                                                      &#13;
      che  la  norma  impugnata  dispone  la  revoca  dei  trattamenti    &#13;
 economici del personale  ospedaliero  deliberati  in  difformità  da    &#13;
 quanto  stabilito dall'art. 7, l. 17 agosto 1974, n. 386, che vietava    &#13;
 la corresponsione di proventi o indennità eccedenti quelli  previsti    &#13;
 da disposizioni di legge o dagli accordi nazionali di cui all'art. 40    &#13;
 della legge 12 febbraio 1968,  n.  132,  prevedendo  altresì  che  i    &#13;
 predetti   trattamenti   non   fossero   soggetti   a   recupero  né    &#13;
 comportassero la responsabilità di coloro che li  avevano  disposti,    &#13;
 sempreché  le  amministrazioni interessate entro un certo termine ne    &#13;
 avessero  rideterminato   l'ammontare   alla   stregua   dell'accordo    &#13;
 nazionale 30 giugno 1979-31 dicembre 1982;                               &#13;
      che   la   norma   relativamente   alla  parte  che  esclude  la    &#13;
 responsabilità  degli  amministratori  degli  enti  ospedalieri,  è    &#13;
 ritenuta  in contrasto con gli artt. 3, 24, 25, 28, 42, 54, 97, 101 e    &#13;
 103 Cost.;                                                               &#13;
      che  la  stessa  disposizione  di  legge, pur se in relazione ad    &#13;
 altro aspetto, viene impugnata anche dal T.A.R. Lazio  con  ordinanza    &#13;
 in  data  14 gennaio 1983 (reg. ord. n. 94 del 1984), in quanto nella    &#13;
 parte  in  cui  dispone  la  revoca  dei  provvedimenti  adottati  in    &#13;
 difformità   dell'art.  7  della  legge  17  agosto  1974,  n.  386,    &#13;
 contrasterebbe con l'art. 97 Cost., per  ragioni  analoghe  a  quelle    &#13;
 già affermate da questa Corte con sentenza n. 161 del 1982;             &#13;
      che nel giudizio introdotto dagli atti di rimessione della Corte    &#13;
 dei  conti  si  sono  costituiti  gli   amministratori   degli   enti    &#13;
 ospedalieri  chiedendo che la questione venisse dichiarata infondata,    &#13;
 mentre, nel giudizio conseguente all'ordinanza di rinvio  del  T.A.R.    &#13;
 Lazio,  ha  spiegato  intervento  l'Avvocatura  generale  dello Stato    &#13;
 chiedendo l'emanazione di una pronuncia di inammissibilità;             &#13;
    Considerato  che  le  cause  vanno  riunite  in  quanto attenendo,    &#13;
 seppure sotto profili diversi, alla medesima disciplina prevista  per    &#13;
 i  trattamenti  economici  disposti  in difformità dell'art. 7 della    &#13;
 legge 17 agosto 1974, n. 386, implicano la definizione  di  questioni    &#13;
 comuni;                                                                  &#13;
      che  il  divieto, contenuto nel citato art. 7 della legge n. 386    &#13;
 del 1974,  è  stato  dichiarato  illegittimo  da  questa  Corte  con    &#13;
 sentenza n. 161 del 1982;                                                &#13;
      che,  a  seguito di tale declaratoria, la questione, concernente    &#13;
 la esclusione della responsabilità degli  amministratori  che  hanno    &#13;
 disposto  i  trattamenti  economici  vietati  dal predetto art. 7, è    &#13;
 stata ritenuta irrilevante in quanto "è venuto  meno  il  fondamento    &#13;
 della contestata responsabilità" (ord. n. 235 del 1985);                &#13;
      che  ad identiche conclusioni deve pervenirsi anche in relazione    &#13;
 all'altra questione che attiene alla norma impugnata nella  parte  in    &#13;
 cui   sancisce  la  revoca  dei  trattamenti  economici  disposti  in    &#13;
 difformità del citato art. 7, poiché anche in questo caso è venuto    &#13;
 meno  il  fondamento  della  revoca,  e  cioè l'illegittimità degli    &#13;
 stessi trattamenti;                                                      &#13;
      che,   pertanto,   le   questioni   sollevate  vanno  dichiarate    &#13;
 manifestamente inammissibili;                                            &#13;
    Visti gli artt. 26, comma secondo, l. 11 marzo 1953, n. 87 e n. 9,    &#13;
 comma secondo, delle Norme integrative per  i  giudizi  davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara   la   manifesta   inammissibilità  delle  questioni  di    &#13;
 legittimità costituzionale dell'art. 10- bis della  legge  8  agosto    &#13;
 1980,   n.  441,  ("Conversione  in  legge,  con  modificazioni,  del    &#13;
 decreto-legge 1° luglio  1980,  n.  285,  concernente  la  disciplina    &#13;
 transitoria  delle  funzioni  dell'assistenza  sanitaria delle unità    &#13;
 sanitarie locali"), sollevate in riferimento agli artt.  3,  24,  25,    &#13;
 28,  42,  54, 97, 101 e 103 Cost., dalla Corte dei Conti e dal T.A.R.    &#13;
 Lazio con le ordinanze indicate in epigrafe.                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 gennaio 1988.                              &#13;
                          Il presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
