<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>198</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:198</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1988</data_decisione>
    <data_deposito>18/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  degli artt. 171 e 180    &#13;
 della legge 22 aprile 1941, n. 633 ("Protezione del diritto  d'autore    &#13;
 e  di  altri  diritti  connessi  al suo esercizio"), promossi con due    &#13;
 ordinanze emesse il 30 giugno 1984 dal Pretore di Ovada, iscritte  ai    &#13;
 nn.  1177  e  1178  del  registro  ordinanze  1984 e pubblicate nella    &#13;
 Gazzetta Ufficiale della Repubblica nn. 50- bis e 47-  bis  dell'anno    &#13;
 1985;                                                                    &#13;
    Visti gli atti di costituzione della S.I.A.E., nonché gli atti di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio del 13 gennaio 1988 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto  che  nel  corso  di  due procedimenti penali a carico di    &#13;
 titolari di emittenti televisive  i  quali  avevano  trasmesso  opere    &#13;
 facenti   parte   dei   repertori   S.I.A.E.   senza   la  prescritta    &#13;
 autorizzazione, il Pretore di Ovada, con  due  ordinanze  di  analogo    &#13;
 contenuto  emesse  il  30 giugno 1984 ha sollevato, in relazione agli    &#13;
 artt. 3, 23, 41,  43,  53  e  97  della  Costituzione,  questioni  di    &#13;
 legittimità  costituzionale,  degli  artt.  180 e 171 della legge 22    &#13;
 aprile 1941, n. 633 ("Protezione del  diritto  d'autore  e  di  altri    &#13;
 diritti connessi al suo esercizio"), nella parte in cui: 1) impongono    &#13;
 al  titolare  di  emittenza  televisiva  o  radiofonica  di   munirsi    &#13;
 dell'autorizzazione  alle  trasmissioni  della  S.I.A.E., concernente    &#13;
 tutti gli autori del repertorio di queste e non  soltanto  coloro  le    &#13;
 cui   opere   vengono   diffuse;   2)   assoggettano  la  concessione    &#13;
 dell'autorizzazione al pagamento di una somma pari al 2,75 per  cento    &#13;
 dei  proventi  lordi dichiarati dall'impresa; 3) non predeterminano i    &#13;
 criteri  sulla  base  dei  quali  la  S.I.A.E.  concede  o  nega   le    &#13;
 autorizzazioni;                                                          &#13;
      che,  a  parere  del giudice a quo, attraverso l'imposizione del    &#13;
 pagamento della suddetta  somma,  si  realizzerebbe  un  prelievo  di    &#13;
 natura tributaria che verrebbe a giovare agli autori le cui opere non    &#13;
 sono  trasmesse,  pur  prescindendo,  nell'ammontare,  dal  numero  e    &#13;
 dall'importanza delle opere utilizzate;                                  &#13;
      che nel giudizio dinanzi alla Corte si è costituita depositando    &#13;
 anche memoria nell'imminenza  dell'udienza  -  la  Società  Italiana    &#13;
 degli  Autori  ed  Editori la quale ha chiesto che la questione venga    &#13;
 dichiarata inammissibile od infondata;                                   &#13;
      che le medesime conclusioni sono state precisate dall'Avvocatura    &#13;
 dello Stato, intervenuta nei giudizi in rappresentanza del Presidente    &#13;
 del Consiglio dei ministri;                                              &#13;
    Considerato  che  entrambe  le  ordinanze  trattano con i medesimi    &#13;
 argomenti le stesse questioni, sì che i due giudizi  possono  essere    &#13;
 riuniti;                                                                 &#13;
      che  l'ipotesi incriminatrice di cui all'art. 171 della legge 22    &#13;
 aprile 1941, n. 633, concerne la diffusione di opere altrui senza che    &#13;
 se  ne  abbia  il  diritto  e  non è diretta a sanzionare il mancato    &#13;
 pagamento   alla   S.I.A.E.   del   compenso    per    il    rilascio    &#13;
 dell'autorizzazione,  bensì  il  mancato  consenso  dell'autore alla    &#13;
 diffusione dell'opera;                                                   &#13;
      che,  inoltre,  se  l'attività  di  intermediario (svolta dalla    &#13;
 S.I.A.E. con quel  carattere  di  preminenza  reso  necessario  dalle    &#13;
 difficoltà  presentate  dal  controllo  dell'utilizzazione economica    &#13;
 delle opere protette: Corte  cost.  sent.  13  aprile  1972,  n.  65)    &#13;
 rappresenta  la  forma  più  diffusa di tutela del diritto d'autore,    &#13;
 essa d'altronde non esclude tuttavia la possibilità  di  un  diretto    &#13;
 rapporto tra l'utilizzazione dell'opera e l'autore;                      &#13;
      che,  quindi, non ricorre affatto tra la fattispecie di reato de    &#13;
 qua e l'art. 180 della legge  citata  la  relazione  individuata  dal    &#13;
 giudice   rimettente   -   risultando   ininfluente   la  prospettata    &#13;
 illegittimità della  seconda  norma  rispetto  alla  prima  -  e  la    &#13;
 questione  relativa  è  del tutto priva di rilevanza nel giudizio in    &#13;
 corso  dinanzi  a  questi,  sì  che  il  giudizio  di   legittimità    &#13;
 costituzionale non può essere ammesso;                                  &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la manifesta inammissibilità della    &#13;
 questione di legittimità costituzionale degli artt. 171 e 180  della    &#13;
 legge  22  aprile 1941, n. 633 ("Protezione del diritto d'autore e di    &#13;
 altri diritti connessi al suo esercizio"), sollevata dal  Pretore  di    &#13;
 Ovada  in  relazione  agli  artt.  3,  23,  41,  43,  53  e  97 della    &#13;
 Costituzione con le ordinanze di cui in epigrafe.                        &#13;
    Così  deciso  in  Roma,  in Camera di Consiglio, nella sede della    &#13;
 Corte Costituzionale, palazzo della Consulta il 10 febbraio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 febbraio 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
