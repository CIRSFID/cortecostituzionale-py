<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>221</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:221</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/2000</data_decisione>
    <data_deposito>19/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Cesare RUPERTO, Riccardo &#13;
 CHIEPPA, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido &#13;
 NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
 Giovanni Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di legittimità costituzionale dell'art. 19 della legge    &#13;
 24  novembre 1981, n. 689 (Modifiche al sistema penale), promossi con    &#13;
 ordinanze  emesse  il  27 e il 16 luglio 1998 dal pretore di Cagliari    &#13;
 sui  ricorsi  proposti  da  Ndao  Modou  e  da Ndiaye Massylla contro    &#13;
 l'Ufficio     provinciale    dell'industria,    del    commercio    e    &#13;
 dell'artigianato  di Cagliari, iscritte ai nn. 369 e 370 del registro    &#13;
 ordinanze 1999 e pubblicate nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 26, prima serie speciale, dell'anno 1999;                             &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio del 25 maggio 2000 il giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
     Ritenuto che, con ordinanza emessa il 16 luglio 1998 (R.O. n. 370    &#13;
 del  1999),  il pretore di Cagliari ha sollevato, in riferimento agli    &#13;
 artt. 24  e  113  Cost.,  questione  di  legittimità  costituzionale    &#13;
 dell'art.  19  della  legge  24 novembre  1981,  n. 689 (Modifiche al    &#13;
 sistema  penale), nella parte in cui "prevede solo la possibilità di    &#13;
 opporre ricorso all'autorità giudiziaria avverso il provvedimento di    &#13;
 sequestro [...] e non avverso il processo verbale di sequestro";         &#13;
         che  il  rimettente  rileva come la norma denunciata crei una    &#13;
 disparità  di  trattamento,  escludendo "la possibilità di adire, a    &#13;
 scelta   del   ricorrente,   l'autorità  giudiziaria  o  l'autorità    &#13;
 amministrativa,  prevista invece per i processi verbali di violazione    &#13;
 a  norme  del  codice  della  strada  [...]  in  danno  di coloro che    &#13;
 subiscono  i  processi verbali di sequestro, alla luce delle sentenze    &#13;
 della  Corte  costituzionale  n. 255  e  n. 311  del 1994 e n. 31 del    &#13;
 1996";                                                                   &#13;
         che,  con  altra ordinanza emessa il 27 luglio del 1998 (R.O.    &#13;
 n. 369 del 1999), lo stesso pretore di Cagliari solleva altra analoga    &#13;
 questione  di  legittimità  costituzionale  della medesima norma, in    &#13;
 riferimento agli stessi parametri;                                       &#13;
         che  il  rimettente  ribadisce  come  la  norma determini una    &#13;
 "disparità  di  trattamento  a vantaggio dei destinatari di processo    &#13;
 verbale  di  violazione  a  norme  del  codice  della strada, i quali    &#13;
 possono   invece   impugnare  il  processo  verbale  divenuto  titolo    &#13;
 esecutivo  e,  alla  luce  dell'interpretazione  delle sentenze della    &#13;
 Corte  costituzionale  n. 311  e  n. 255  del  1994 e n. 31 del 1996,    &#13;
 direttamente il processo verbale prima che il medesimo diventi titolo    &#13;
 esecutivo   per   scadenza   dei   termini   per   adire  l'autorità    &#13;
 amministrativa";                                                         &#13;
         che,  nel  giudizio  promosso  con  R.O.  n. 370 del 1999, è    &#13;
 intervenuto il Presidente del Consiglio dei Ministri, rappresentato e    &#13;
 difeso  dall'Avvocatura  generale  dello  Stato,  concludendo  per la    &#13;
 declaratoria  di  inammissibilità  ovvero  di manifesta infondatezza    &#13;
 della sollevata questione.                                               &#13;
     Considerato  che l'identità della norma sottoposta a scrutinio e    &#13;
 la  connessione  dei  profili  censurati  in  riferimento ai medesimi    &#13;
 parametri  impongono  la  riunione  dei  giudizi,  che vanno pertanto    &#13;
 congiuntamente decisi;                                                   &#13;
         che, in entrambe le ordinanze di rimessione, il giudice a quo    &#13;
 tralascia  completamente  di  enunciare  quali  siano i termini della    &#13;
 fattispecie   concreta   oggetto   del  giudizio  principale,  ed  in    &#13;
 particolare  non specifica in alcun modo le essenziali circostanze di    &#13;
 fatto e l'esatta natura dei provvedimenti sanzionatori opposti;          &#13;
         che  la  mancata  indicazione  di  detti elementi e, insieme,    &#13;
 l'omessa  esplicazione  delle ragioni della rilevanza delle sollevate    &#13;
 questioni  di  legittimità  costituzionale precludono a questa Corte    &#13;
 ogni   possibilità  di  valutazione  inerente  al  predetto  profilo    &#13;
 preliminare;                                                             &#13;
         che,   pertanto,   le   sollevate   questioni  devono  essere    &#13;
 dichiarate manifestamente inammissibili.                                 &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti i giudizi;                                                   &#13;
     Dichiara   la   manifesta  inammissibilità  delle  questioni  di    &#13;
 legittimità  costituzionale  dell'art.  19  della  legge 24 novembre    &#13;
 1981,   n. 689   (Modifiche   al  sistema  penale),  sollevate  -  in    &#13;
 riferimento  agli  artt. 24 e 113 della Costituzione - dal pretore di    &#13;
 Cagliari, con le ordinanze indicate in epigrafe.                         &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 giugno 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 19 giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
