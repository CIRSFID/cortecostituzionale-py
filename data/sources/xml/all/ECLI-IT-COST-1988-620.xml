<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>620</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:620</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/06/1988</data_decisione>
    <data_deposito>10/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio promosso con ricorso della Regione Toscana notificato il    &#13;
 15 aprile  1981,  depositato  in  Cancelleria  il  30  successivo  ed    &#13;
 iscritto  al  n.  16  del  registro  ricorsi  1981,  per conflitto di    &#13;
 attribuzione sorto a seguito della lettera 214903/FCH  della  Procura    &#13;
 generale  della  Corte  dei  Conti  -  Contenzioso  nelle  materie di    &#13;
 contabilità pubblica - relativa ai progettati invasi artificiali sul    &#13;
 fiume Merse in provincia di Siena;                                       &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nell'udienza pubblica dell'8 marzo 1988 il Giudice relatore    &#13;
 Antonio Baldassarre;                                                     &#13;
    Uditi  l'Avvocato  Alberto  Predieri  per  la  Regione  Toscana  e    &#13;
 l'Avvocato  dello  Stato  Giorgio  Azzariti  per  il  Presidente  del    &#13;
 Consiglio dei Ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  ricorso  notificato  il  15  aprile  1981 il Presidente della    &#13;
 Regione Toscana ha  proposto  conflitto  di  attribuzione  contro  la    &#13;
 lettera  prot.  n.  214903/FCH della Procura Generale della Corte dei    &#13;
 Conti  -  Contenzioso  nelle  materie   di   contabilità   pubblica,    &#13;
 indirizzata  al  Presidente  del  Consiglio regionale della Toscana e    &#13;
 firmata dal Vice Procuratore Generale.                                   &#13;
    Nell'atto  impugnato si chiede: a) di comunicare le determinazioni    &#13;
 adottate in merito a certi progettati invasi artificiali sul torrente    &#13;
 Farma   e   sul   fiume   Merse,   i   quali  potrebbero  danneggiare    &#13;
 irrimediabilmente ambienti degni della massima protezione per le loro    &#13;
 caratteristiche bellezze; b) di conoscere se tali territori godono di    &#13;
 speciale   tutela   legislativa   o    rientrino    in    fattispecie    &#13;
 particolarmente tutelate da apposita normativa.                          &#13;
    Negli  atti  introduttivi del giudizio, le parti sostengono, da un    &#13;
 lato,  che  l'atto  impugnato  invade  le  competenze  regionali,  in    &#13;
 violazione  dell'art. 117 Cost., in relazione agli artt. 66, 82, 87 e    &#13;
 seguenti del d.P.R. n. 616 del 1977, e, dall'altro, che il ricorso va    &#13;
 dichiarato inammissibile o comunque infondato.                           &#13;
    Nella  memoria  depositata in prossimità dell'udienza, la Regione    &#13;
 Toscana riconosce, invece, di non avere più interesse, in quanto  la    &#13;
 materia  reltiva al risarcimento del danno ambientale è ora regolata    &#13;
 dall'art. 18 della legge 8 luglio 1986 n. 749,  che  ha  previsto  la    &#13;
 giurisdizione   del   giudice   ordinario,   escludendo   quindi   la    &#13;
 giurisdizione della Corte dei Conti e della Procura Generale (su tale    &#13;
 articolo  è intervenuta, peraltro, una sentenza di rigetto di questa    &#13;
 Corte, la n. 641 del 1987).<diritto>Considerato in diritto</diritto>Con  l'atto impugnato la Procura Generale della Corte dei Conti ha    &#13;
 chiesto informazioni al  Presidente  del  Consiglio  regionale  della    &#13;
 Toscana  al  fine  di  accertare  l'eventuale  esistenza  di un danno    &#13;
 ambientale nei  territori  interessati  dalla  costruzione  di  certi    &#13;
 invasi artificiali sul torrente Farma e sul fiume Merse, in provincia    &#13;
 di Siena.                                                                &#13;
    Successivamente  all'emanazione dell'atto impugnato, è entrata in    &#13;
 vigore la legge 8 luglio  1986  n.  349  (Istituzione  del  Ministero    &#13;
 dell'ambiente  e norme in materia di danno ambientale), che, all'art.    &#13;
 18,  ha  previsto  la  giurisdizione  del   giudice   ordinario   per    &#13;
 l'accertamento del danno ambientale.                                     &#13;
    Poiché  le  informazioni  richieste  con  l'atto  impugnato erano    &#13;
 preordinate  all'accertamento  di  un'eventuale  responsabilità  per    &#13;
 danno  ambientale  da  parte  della  Corte  dei  Conti e poiché tale    &#13;
 accertamento è ora precluso dall'art. 18 della citata legge  n.  349    &#13;
 del  1986,  va  dichiarata  cessata  la  materia  del  contendere, in    &#13;
 conformità alla richiesta della regione ricorrente.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  cessata  la  materia  del contendere in ordine al ricorso    &#13;
 indicato in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 giugno 1988.                                 &#13;
                          Il Presidente: SAJA                             &#13;
                       Il redattore: BALDASSARRE                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 10 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
