<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>167</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:167</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/05/1995</data_decisione>
    <data_deposito>16/05/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  degli artt. 2, primo    &#13;
 comma, e 3 della legge della Regione Lombardia 7 giugno 1980,  n.  93    &#13;
 (Norme  in materia di edificazione nelle zone agricole), promosso con    &#13;
 ordinanza emessa il  27  maggio  1993  dal  Tribunale  amministrativo    &#13;
 regionale  per  la  Lombardia  su ricorso proposto da Cantù Ercole e    &#13;
 altri contro il Comune di Vimercate e altri, iscritta al n.  428  del    &#13;
 registro  ordinanze  1994 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 30, prima serie speciale, dell'anno 1994;                  &#13;
    Visto l'atto di intervento della Regione Lombardia;                   &#13;
    Udito  nella  camera  di  consiglio  del 20 aprile 1995 il Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto che, nel corso di un giudizio promosso da Ercole Cantù e    &#13;
 altri  per  ottenere  l'annullamento  di  una  concessione   edilizia    &#13;
 rilasciata dal Comune di Vimercate all'associazione "Centro ippico La    &#13;
 Corte"   per   la  costruzione  di  box  per  cavalli,  il  Tribunale    &#13;
 amministrativo regionale per la Lombardia ha sollevato  questione  di    &#13;
 legittimità  costituzionale,  in  riferimento agli artt. 3, 5, 117 e    &#13;
 128 della Costituzione, nei confronti degli artt. 2, primo comma, e 3    &#13;
 della legge della Regione Lombardia 7 giugno 1980, n.  93  (Norme  in    &#13;
 materia  di  edificazione nelle zone agricole), nella parte in cui la    &#13;
 realizzazione di opere non destinate alla residenza è subordinata al    &#13;
 possesso   di   particolari   requisiti   soggettivi   (qualità   di    &#13;
 imprenditore  agricolo  o di figure assimilate) e all'accertamento di    &#13;
 un collegamento funzionale delle  opere  stesse  con  l'attività  di    &#13;
 agricoltura;                                                             &#13;
      che,  secondo  il giudice a quo, la rilevanza della questione è    &#13;
 dimostrata dal rilievo che egli dovrebbe annullare il rilascio  della    &#13;
 concessione   in   esame,  a  meno  che  questa  Corte  non  dichiari    &#13;
 l'illegittimità costituzionale delle disposizioni contestate;           &#13;
      che, quanto alla non manifesta infondatezza, il  giudice  a  quo    &#13;
 osserva   che   le   disposizioni   impugnate,   essendo   dirette  a    &#13;
 regolamentare la concreta utilizzazione  delle  opere  (jus  utendi),    &#13;
 quale emerge, non già dalle caratteristiche strutturali delle opere,    &#13;
 ma  da  fattori  irrilevanti  sotto  il  profilo urbanistico, oltre a    &#13;
 esorbitare dalla materia "urbanistica",  attribuita  alla  competenza    &#13;
 legislativa  regionale,  violano,  per  un  verso, l'art. 3 e, per un    &#13;
 altro, l'art. 117  della  Costituzione,  per  il  fatto  che,  mentre    &#13;
 discriminano irragionevolmente la posizione di chi svolge l'attività    &#13;
 agricola  in modo professionale o principale rispetto a quella di chi    &#13;
 non l'esercita, ledono i principi desumibili dalle norme statali  (in    &#13;
 particolare:  l'art. 1 della legge 28 gennaio 1977, n. 10 e gli artt.    &#13;
 7, 8 e 25 della legge 28 febbraio  1985,  n.  47),  alla  luce  della    &#13;
 giurisprudenza  costituzionale  che  afferma  l'irrilevanza,  a  fini    &#13;
 urbanistici, delle concrete modalità di utilizzazione dell'immobile;    &#13;
     che, sempre secondo il giudice a quo, le disposizioni  impugnate,    &#13;
 predeterminando  rigidamente  gli strumenti urbanistici e vietando ai    &#13;
 comuni  di  consentire  interventi  non   contemplati   dalla   legge    &#13;
 regionale, violano altresì gli artt. 5 e 128 della Costituzione;        &#13;
      che è intervenuta in giudizio la Regione Lombardia per chiedere    &#13;
 che le questioni siano dichiarate inammissibili o infondate;             &#13;
      che, secondo la Regione, le disposizioni impugnate, introducendo    &#13;
 un'eccezione  alla  regola  generale  di  divieto  dell'utilizzazione    &#13;
 edilizia  dei  territori  agricoli  ad  esclusivo  beneficio  di  una    &#13;
 limitata  tipologia  di  opere,  previa  richiesta di una particolare    &#13;
 categoria  di  soggetti   operanti   nell'ambito   di   una   realtà    &#13;
 imprenditoriale  agricola,  contengono  una  disciplina  basata sulla    &#13;
 distinzione  oggettiva  tra  le   opere,   considerate   nelle   loro    &#13;
 caratteristiche  tipologiche  e  nel loro rapporto con il territorio,    &#13;
 come  si  conviene  a  una  disciplina  legislativa  in  materia   di    &#13;
 urbanistica;                                                             &#13;
      che,  sempre  secondo  la  Regione,  gli  artt.  3  e  117 della    &#13;
 Costituzione  non  sembrano  affatto   violati   dalle   disposizioni    &#13;
 impugnate,  sia  perché  il  riferimento,  in  queste,  ai requisiti    &#13;
 soggettivi rappresenta un necessario  strumento  di  selezione  degli    &#13;
 interventi  in  considerazione  della  ratio  della  legge, diretta a    &#13;
 salvaguardare lo spazio da destinarsi all'agricoltura di fronte  alla    &#13;
 crescente presenza di strutture edilizie residenziali, sia perché il    &#13;
 diverso  trattamento,  nell'ambito dello stesso tipo di zona, tra chi    &#13;
 è imprenditore agricolo e chi non lo è trova riscontro in  numerose    &#13;
 leggi  statali  e,  in particolare, nell'art. 9 della legge n. 10 del    &#13;
 1977;                                                                    &#13;
      che, infine, ad avviso  della  Regione,  le  pretese  violazioni    &#13;
 degli  artt.  5  e  128  della Costituzione, oltreché manifestamente    &#13;
 infondate, poiché  è  evidente  che  le  leggi  urbanistiche  hanno    &#13;
 proprio  l'effetto  di  non  consentire  ai comuni gli interventi non    &#13;
 contemplati dalle  leggi  stesse,  sono  inammissibili  per  assoluto    &#13;
 difetto   di   motivazione,  come  pure  inammissibili  appaiono,  in    &#13;
 subordine, tutte le censure, essendo dirette a un indebito  sindacato    &#13;
 sulle scelte discrezionali del legislatore regionale;                    &#13;
      che,  in  prossimità  della  camera  di  consiglio,  la Regione    &#13;
 Lombardia  ha  depositato  un'ulteriore  memoria,  nella   quale   si    &#13;
 ribadiscono    gli    argomenti   a   favore   della   richiesta   di    &#13;
 inammissibilità o di infondatezza delle questioni sollevate;            &#13;
    Considerato che gli artt. 2, primo comma, e 3  della  legge  della    &#13;
 Regione  Lombardia  n. 93 del 1980, oggetto di contestazione da parte    &#13;
 del  giudice  a  quo,  sono  frutto  di  un'insidacabile  scelta  del    &#13;
 legislatore  regionale,  diretta  a limitare l'utilizzazione edilizia    &#13;
 dei territori agricoli e a frenare  il  processo  di  erosione  dello    &#13;
 spazio  destinato  alle  colture, scelta che ha il proprio fondamento    &#13;
 nell'art. 44 della Costituzione, il quale, "al fine di conseguire  il    &#13;
 razionale  sfruttamento  del  suolo  e  di  stabilire  equi  rapporti    &#13;
 sociali", facoltizza il legislatore, anche regionale,  a  predisporre    &#13;
 aiuti e sostegni all'impresa agricola e alla proprietà coltivatrice;    &#13;
      che,  rispetto a tale ratio legislativa, non può essere affatto    &#13;
 considerata  un'irragionevole  discriminazione,  lesiva  dell'art.  3    &#13;
 della  Costituzione, la subordinazione del rilascio della concessione    &#13;
 edilizia sia al possesso della qualità di imprenditore agricolo o di    &#13;
 altra figura assimilata,  sia  all'accertamento  di  un  collegamento    &#13;
 funzionale  dell'opera  con  l'attività  agricola,  essendo elementi    &#13;
 volti  a  denotare  la  destinazione  effettiva  delle   opere   alla    &#13;
 conduzione del fondo o, in genere, alla attività di agricoltura;        &#13;
      che,  inoltre,  del  pari  manifestamente  infondata  risulta la    &#13;
 pretesa violazione dell'art. 117 della Costituzione sotto il  profilo    &#13;
 della  lesione  dei  principi  fondamentali  desumibili  dalle  leggi    &#13;
 statali in materia urbanistica, poiché, come questa  Corte  ha  già    &#13;
 avuto modo di affermare (v. ordinanze nn. 714 e 709 del 1988), mentre    &#13;
 rientra  nei poteri del legislatore in tema di disciplina urbanistica    &#13;
 sottoporre a un trattamento  differenziato  tanto  le  zone  agricole    &#13;
 rispetto  ad  altre  zone,  quanto, all'interno della stessa zona, la    &#13;
 posizione degli imprenditori agricoli o di  altre  figure  assimilate    &#13;
 rispetto  a quella di soggetti diversi, nello stesso tempo l'indicata    &#13;
 differenziazione è saldamente stabilita  in  disposizioni  di  legge    &#13;
 statale, segnatamente nell'art. 9 della legge 28 gennaio 1977, n. 10,    &#13;
 recante  "Norme  per la edificabilità dei suoli" (a nulla rilevando,    &#13;
 invece, gli altri articoli di legge citati dal giudice a quo);           &#13;
      che, infine, la previsione in una legge regionale dei requisiti,    &#13;
 soggettivi  e  oggettivi, per il rilascio di una concessione edilizia    &#13;
 in zona agricola non può essere minimamente considerata  lesiva  dei    &#13;
 principi volti a garantire l'autonomia comunale, ai sensi degli artt.    &#13;
 5 e 128 della Costituzione;                                              &#13;
      che,  pertanto,  le  questioni  di  legittimità  costituzionale    &#13;
 sollevate dal giudice a quo devono  esser  dichiarate  manifestamente    &#13;
 infondate;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza delle questioni di  legittimità    &#13;
 costituzionale  degli  artt.  2,  primo  comma, e 3 della legge della    &#13;
 Regione  Lombardia  7  giugno  1980,  n.  93  (Norme  in  materia  di    &#13;
 edificabilità  nelle  zone agricole), sollevate, in riferimento agli    &#13;
 artt.  3,  5,  117  e   128   della   Costituzione,   dal   Tribunale    &#13;
 amministrativo regionale per la Lombardia con l'ordinanza indicata in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 10 maggio 1995.                               &#13;
                Il Presidente e redattore: BALDASSARRE                    &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 16 maggio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
