<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>541</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:541</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/11/1989</data_decisione>
    <data_deposito>11/12/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 9 della legge    &#13;
 23 dicembre 1975, n. 698 (Scioglimento e trasferimento delle funzioni    &#13;
 dell'Opera   nazionale   per   la   protezione   della  maternità  e    &#13;
 dell'infanzia), come modificato dalla legge 1° agosto  1977,  n.  563    &#13;
 (Modifiche  ed  integrazioni  alla  legge  23  dicembre 1975, n. 698:    &#13;
 "Scioglimento e trasferimento delle funzioni dell'Opera nazionale per    &#13;
 la  protezione  della  maternità  e  dell'infanzia"),  promosso  con    &#13;
 ordinanza emessa il 18 maggio  1988  dal  T.A.R.  per  il  Lazio  sul    &#13;
 ricorso  proposto  da  Benedetti  Gino  ed altri contro l'E.N.P.A.S.,    &#13;
 iscritta al n. 290 del registro ordinanze  1989  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  24, prima serie speciale,    &#13;
 dell'anno 1989;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 25 ottobre 1989 il Giudice    &#13;
 relatore Ugo Spagnoli;                                                   &#13;
    Ritenuto  che  con  l'ordinanza indicata in epigrafe il T.A.R. del    &#13;
 Lazio  dubita,  in  riferimento  agli  artt.  3  e  36  Cost.,  della    &#13;
 legittimità  costituzionale  dell'art. 9, secondo comma, della legge    &#13;
 23 dicembre 1975, n. 698 (Scioglimento e trasferimento delle funzioni    &#13;
 dell'Opera   nazionale   per   la   protezione   della  maternità  e    &#13;
 dell'infanzia), nel testo modificato con  l'art.  5  della  legge  1°    &#13;
 agosto 1977, n. 563;                                                     &#13;
      che  l'impugnativa  muove  dal presupposto che tale disposizione    &#13;
 preveda che le indennità di anzianità maturate dagli ex  dipendenti    &#13;
 dell'O.N.M.I., trasferiti alle regioni od allo Stato, per il servizio    &#13;
 prestato alle dipendenze di tale ente vadano liquidate solo  all'atto    &#13;
 della   definitiva   cessazione  dal  servizio  presso  gli  enti  di    &#13;
 destinazione, ma sulla base non  dell'ultima  retribuzione  percepita    &#13;
 presso  di  questi,  bensì  di quella corrisposta dall'O.N.M.I. alla    &#13;
 data del suo scioglimento (31 dicembre 1975);                            &#13;
    Considerato  che,  tale  questione,  già  sollevata  nei medesimi    &#13;
 termini dallo stesso T.A.R. del Lazio (r.o. nn.  461  e  462/88),  è    &#13;
 stata  dichiarata non fondata, "nei sensi di cui in motivazione", con    &#13;
 la sentenza n. 164 del 1989, "dovendosi intendere la norma  impugnata    &#13;
 nel  senso  che  l'indennità di anzianità vada calcolata sulla base    &#13;
 dell'ultima retribuzione percepita presso l'ente di destinazione";       &#13;
      che   pertanto   la   questione   va  dichiarata  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 9, secondo comma, della  legge  23  dicembre    &#13;
 1975,  n. 698 (Scioglimento e trasferimento delle funzioni dell'Opera    &#13;
 nazionale per la protezione della maternità  e  dell'infanzia),  nel    &#13;
 testo  modificato  con  l'art.  5  della legge 1° agosto 1977, n. 563    &#13;
 (Modifiche ed integrazioni alla  legge  23  dicembre  1975,  n.  698:    &#13;
 "Scioglimento e trasferimento delle funzioni dell'Opera nazionale per    &#13;
 la protezione  della  maternità  e  dell'infanzia"),  sollevata,  in    &#13;
 riferimento  agli  artt.  3  e  36  della Costituzione, dal Tribunale    &#13;
 amministrativo regionale del Lazio con ordinanza del 18 maggio  1988,    &#13;
 pervenuta  alla  Corte  costituzionale  il  25  maggio  1989 (r.o. n.    &#13;
 290/89).                                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 30 novembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: SPAGNOLI                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria l'11 dicembre 1989.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
