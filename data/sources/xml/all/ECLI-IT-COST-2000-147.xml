<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>147</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:147</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GUIZZI</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/05/2000</data_decisione>
    <data_deposito>19/05/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Francesco GUIZZI; &#13;
 Giudici: Cesare MIRABELLI, Massimo VARI, Cesare RUPERTO, Riccardo &#13;
 CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, &#13;
 Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, &#13;
 Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità  costituzionale del combinato disposto    &#13;
 degli  artt. 32  della  legge  6 agosto  1990, n. 223 (Disciplina del    &#13;
 sistema  radiotelevisivo  pubblico  e privato); 1, commi 3 e 3-quater    &#13;
 del  d.-l. 19 ottobre 1992, n. 407 (Proroga dei termini in materia di    &#13;
 impianti  di  radiodiffusione),  convertito, con modificazioni, nella    &#13;
 legge  17 dicembre  1992,  n. 482;  1,  commi  13  e  14,  del  d.-l.    &#13;
 23 ottobre   1996,   n. 545  (Disposizioni  urgenti  per  l'esercizio    &#13;
 dell'attività    radiotelevisiva    e    delle   telecomunicazioni),    &#13;
 convertito,  con modificazioni, nella legge 23 dicembre 1996, n. 650;    &#13;
 3,  commi  1  e  2,  della  legge 31 luglio 1997, n. 249 (Istituzione    &#13;
 dell'Autorità  per  le  garanzie  nelle  comunicazioni  e  norme sui    &#13;
 sistemi  delle telecomunicazioni e radiotelevisivo), promossi con tre    &#13;
 ordinanze  emesse  il  5 novembre  1997  dal Tribunale amministrativo    &#13;
 regionale  della  Calabria,  sezione  staccata  di  Reggio  Calabria,    &#13;
 rispettivamente iscritte ai nn. 147, 148 e 149 del registro ordinanze    &#13;
 1998  e  pubblicate  nella Gazzetta Ufficiale della Repubblica n. 11,    &#13;
 prima serie speciale, dell'anno 1998.                                    &#13;
     Visti  gli  atti di costituzione di Radio Skylab S.a.s., di Radio    &#13;
 M.  Calabria,  nonché  gli  atti  di  intervento  del Presidente del    &#13;
 Consiglio dei Ministri;                                                  &#13;
     Udito  nell'udienza  pubblica  del  22 febbraio  2000  il giudice    &#13;
 relatore Massimo Vari;                                                   &#13;
     Uditi  l'avvocato  Eugenio  Porta  per  Radio Skylab S.a.s. e per    &#13;
 Radio  M. Calabria e l'Avvocato dello Stato Gian Paolo Polizzi per il    &#13;
 Presidente del Consiglio dei Ministri.                                   &#13;
     Ritenuto  che,  con  tre  distinte ordinanze emesse il 5 novembre    &#13;
 1997  (r.o. nn. 147, 148 e 149 del 1998), il Tribunale amministrativo    &#13;
 regionale  della  Calabria,  sezione  staccata di Reggio Calabria, ha    &#13;
 sollevato,  in  riferimento agli artt. 3, 21 e 41 della Costituzione,    &#13;
 questione  di  legittimità  costituzionale  "della norma sancita dal    &#13;
 combinato  disposto" degli artt. 32 della legge 6 agosto 1990, n. 223    &#13;
 (Disciplina del sistema radiotelevisivo pubblico e privato); 1, commi    &#13;
 3  e  3-quater del d.-l. 19 ottobre 1992, n. 407 (Proroga dei termini    &#13;
 in   materia   di   impianti  di  radiodiffusione),  convertito,  con    &#13;
 modificazioni,  nella  legge  17 dicembre 1992, n. 482; 1, commi 13 e    &#13;
 14,  del  d.-l.  23 ottobre  1996,  n. 545  (Disposizioni urgenti per    &#13;
 l'esercizio      dell'attività      radiotelevisiva      e     delle    &#13;
 telecomunicazioni),   convertito,   con  modificazioni,  nella  legge    &#13;
 23 dicembre 1996, n. 650; 3, commi 1 e 2, della legge 31 luglio 1997,    &#13;
 n. 249    (Istituzione   dell'Autorità   per   le   garanzie   nelle    &#13;
 comunicazioni   e   norme   sui  sistemi  delle  telecomunicazioni  e    &#13;
 radiotelevisivo);                                                        &#13;
         che  i  giudizi  a  quibus  sono  stati  promossi  da  talune    &#13;
 emittenti  radiofoniche  al  fine  di  ottenere  l'annullamento degli    &#13;
 impugnati   provvedimenti   di   diniego   di   concessione   per  la    &#13;
 radiodiffusione  sonora  in  ambito  locale,  adottati nell'anno 1994    &#13;
 dall'Amministrazione  delle  poste  e  telecomunicazioni,  in base al    &#13;
 combinato  disposto degli artt. 1, comma 3, del d.-l. n. 407 del 1992    &#13;
 (convertito,  con  modificazioni,  nella  legge n. 482 del 1992) e 32    &#13;
 della  legge  n. 223  del  1990, e motivati in ragione "dell'avvenuto    &#13;
 mutamento  esclusivamente  formale della veste giuridica del titolare    &#13;
 della emittente";                                                        &#13;
         che,  ad  avviso  del  giudice  rimettente, l'interpretazione    &#13;
 delle  suddette  norme  - tale da escludere che la concessione per la    &#13;
 radiodiffusione televisiva in ambito locale possa essere rilasciata a    &#13;
 soggetti  diversi  da quelli originariamente autorizzati a proseguire    &#13;
 nell'esercizio   degli   impianti   -   sarebbe   espressione  di  un    &#13;
 orientamento  puramente formalistico, nonostante i tratti di "diritto    &#13;
 vivente"   da   esso   assunto,   per   opera   della  giurisprudenza    &#13;
 amministrativa;                                                          &#13;
         che,  peraltro,  secondo le ordinanze, v'è da dubitare della    &#13;
 legittimità   costituzionale  della  normativa  transitoria  di  cui    &#13;
 all'art. 32   della   legge   n. 223   del  1990  e  delle  ulteriori    &#13;
 disposizioni  denunciate  che,  avendo  fatto slittare "più volte in    &#13;
 avanti"  il  termine  per  il  rilascio delle concessioni e quello di    &#13;
 scadenza  delle autorizzazioni provvisorie, avrebbero penalizzato "le    &#13;
 imprese  di dimensioni più ridotte, condannate a svolgere la propria    &#13;
 attività  senza  alcuna  certezza  e  con  poche  prospettive per il    &#13;
 prosieguo";                                                              &#13;
         che,  in  particolare,  il combinato disposto delle censurate    &#13;
 disposizioni,    operando    secondo    "la    logica    della   c.d.    &#13;
 "cristallizzazione"",  che "continua a disciplinare precariamente sin    &#13;
 dal  lontano  1990  la  vita delle piccole emittenti", si porrebbe in    &#13;
 contrasto con gli artt. 3, 21 e 41 della Costituzione;                   &#13;
         che,  nei  giudizi  di  cui  alle  ordinanze iscritte al r.o.    &#13;
 nn. 148  e  149 del 1998, si sono costituite, rispettivamente, "Radio    &#13;
 Skylab  S.a.s."  di  Reggio Calabria e "Radio M. Calabria", emittenti    &#13;
 ricorrenti  nei  giudizi principali, le quali hanno entrambe concluso    &#13;
 per sentir "dichiarare l'incostituzionalità delle norme denunciate",    &#13;
 sviluppando  le  argomentazioni  a sostegno delle proprie ragioni con    &#13;
 memorie  illustrative,  di identico tenore, depositate in prossimità    &#13;
 dell'udienza;                                                            &#13;
         che  in  tutti  i  giudizi  è  intervenuto il Presidente del    &#13;
 Consiglio   dei  Ministri,  rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale  dello Stato, chiedendo che le prospettate questioni vengano    &#13;
 dichiarate inammissibili o comunque, in subordine, infondate.            &#13;
     Considerato   che  le  ordinanze  di  rimessione  sollevano,  con    &#13;
 identiche  argomentazioni, le medesime questioni di costituzionalità    &#13;
 e che, pertanto, i relativi giudizi possono essere riuniti per essere    &#13;
 decisi con un'unica pronuncia;                                           &#13;
         che  le  controversie pendenti dinanzi al rimettente hanno ad    &#13;
 oggetto    provvedimenti   di   diniego   di   concessione   per   la    &#13;
 radiodiffusione  sonora in ambito locale, adottati, come emerge dalle    &#13;
 medesime  ordinanze,  dall'amministrazione nell'anno 1994, in base al    &#13;
 combinato  disposto degli artt. 1, comma 3, del d.-l. n. 407 del 1992    &#13;
 (convertito,  con  modificazioni,  nella  legge n. 482 del 1992) e 32    &#13;
 della legge n. 223 del 1990;                                             &#13;
         che il giudice a quo sospetta di incostituzionalità non solo    &#13;
 le   predette  disposizioni  (ricomprendendovi,  altresì,  il  comma    &#13;
 3-quater  dello  stesso art. 1 del menzionato d.-l. n. 407 del 1992),    &#13;
 ma,  in  combinato  disposto tra loro, anche l'art. 1, commi 13 e 14,    &#13;
 del  d.-l.  23 ottobre  1996,  n. 545, convertito, con modificazioni,    &#13;
 nella  legge 23 dicembre 1996, n. 650, e l'art. 3, commi 1 e 2, della    &#13;
 legge 31 luglio 1997, n. 249;                                            &#13;
         che,  per  quanto  attiene  agli  artt. 1, comma 3, del d.-l.    &#13;
 n. 407  del  1992,  convertito, con modificazioni, nella legge n. 482    &#13;
 del  1992,  e  32  della  legge  n. 223  del  1990, le ordinanze, nel    &#13;
 qualificare  come  "puramente formalistica" l'esegesi della censurata    &#13;
 normativa  quale  risulta dalla giurisprudenza amministrativa e quale    &#13;
 è  stata  fatta  propria  anche  dall'amministrazione,  mostrano  di    &#13;
 reputare  la  stessa  come  effetto  di  un non corretto procedimento    &#13;
 ermeneutico,  ma  omettono  di  esprimere  una propria diversa scelta    &#13;
 interpretativa,  non  chiarendo,  perciò, quale sia - a giudizio del    &#13;
 rimettente  -  la  portata  della  norma  della  quale egli deve fare    &#13;
 applicazione  e  non  consentendo,  così,  la  verifica, da parte di    &#13;
 questa   Corte,   della   rilevanza   della   proposta  questione  di    &#13;
 legittimità costituzionale;                                             &#13;
         che,  quanto alle altre disposizioni denunciate, le ordinanze    &#13;
 di  rimessione non esplicitano, invero, alcun elemento di valutazione    &#13;
 circa  l'incidenza  in  concreto  delle stesse sulla decisione che il    &#13;
 giudice  a  quo  è tenuto ad assumere nei procedimenti innanzi a sé    &#13;
 pendenti;                                                                &#13;
         che,  segnatamente,  una  puntuale e plausibile motivazione -    &#13;
 tale da assolvere all'obbligo previsto dall'art. 23 della legge n. 87    &#13;
 del  1953,  ai  fini  dell'ammissibilità della proposta questione di    &#13;
 legittimità  costituzionale  (vedi,  tra le altre, ordinanza. n. 236    &#13;
 del  1999)  - si rendeva tanto più necessaria nel caso di specie, in    &#13;
 ragione  del  fatto  che  il combinato disposto delle norme censurate    &#13;
 comprende,  appunto,  norme successive all'adozione dei provvedimenti    &#13;
 di  diniego di concessione, che costituiscono oggetto di controversia    &#13;
 nei giudizi a quibus;                                                    &#13;
         che,  pertanto,  l'evidenziato  difetto  di motivazione degli    &#13;
 atti   di  promovimento  degli  incidenti  di  costituzionalità  non    &#13;
 consente  di  valutare  l'applicabilità  nei  giudizi a quibus delle    &#13;
 norme di cui trattasi (vedi ordinanza n. 194 del 1999);                  &#13;
         che,   quindi,   le   sollevate  questioni  vanno  dichiarate    &#13;
 manifestamente inammissibili.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti  i  giudizi, dichiara la manifesta inammissibilità delle    &#13;
 questioni di legittimità costituzionale del combinato disposto degli    &#13;
 artt. 32  della  legge  6 agosto 1990, n. 223 (Disciplina del sistema    &#13;
 radiotelevisivo  pubblico e privato); 1, commi 3 e 3-quater del d.-l.    &#13;
 19 ottobre  1992,  n. 407 (Proroga dei termini in materia di impianti    &#13;
 di  radiodiffusione),  convertito,  con  modificazioni,  nella  legge    &#13;
 17 dicembre  1992,  n. 482;  1,  commi  13 e 14, del d.-l. 23 ottobre    &#13;
 1996,  n. 545  (Disposizioni  urgenti  per l'esercizio dell'attività    &#13;
 radiotelevisiva   e   delle   telecomunicazioni),   convertito,   con    &#13;
 modificazioni,  nella legge 23 dicembre 1996, n. 650; 3, commi 1 e 2,    &#13;
 della legge 31 luglio 1997, n. 249 (Istituzione dell'Autorità per le    &#13;
 garanzie    nelle   comunicazioni   e   norme   sui   sistemi   delle    &#13;
 telecomunicazioni  e radiotelevisivo), sollevate, in riferimento agli    &#13;
 artt. 3,  21  e  41  della Costituzione, dal Tribunale amministrativo    &#13;
 regionale della Calabria, sezione staccata di Reggio Calabria, con le    &#13;
 ordinanze indicate in epigrafe.                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 maggio 2000.                                &#13;
                         Il Presidente: Guizzi                            &#13;
                          Il redattore: Vari                              &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 19 maggio 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
