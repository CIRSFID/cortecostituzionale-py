<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>1</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:1</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/01/1979</data_decisione>
    <data_deposito>09/01/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - &#13;
 Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>sul  ricorso  proposto  da  Maria Luisa Galli, Maria Luisa Zardini,  &#13;
 Esperia Volpe, in  nome  e  per    conto  del  comitato  promotore  del  &#13;
 "referendum  abrogativo degli artt. 546, 547, 548, 549,  secondo comma,  &#13;
 550, 551, 552, 554, 555 del codice penale, emanato con R.D. 19  ottobre  &#13;
 1930,  n.    1398",  quale  rappresentante dei firmatari della relativa  &#13;
 richiesta, pervenuto in  cancelleria il 23 ottobre 1978 ed iscritto  al  &#13;
 n.  14  del  registro  a.r. 1978, per conflitto di attribuzioni sorto a  &#13;
 seguito dell'ordinanza dell'Ufficio centrale per il  referendum  presso  &#13;
 la    Corte  di cassazione, depositata nella cancelleria della Corte di  &#13;
 cassazione il 26 maggio 1978,    con  la  quale  si  dichiara  che  "le  &#13;
 operazioni  di cui alla richiesta di referendum popolare, presentata il  &#13;
 12 luglio 1975 per l'abrogazione degli  artt.    546,  547,  548,  549,  &#13;
 secondo  comma, 553, 554, 555 codice penale non hanno più corso".       &#13;
     Udito  nella  camera  di  consiglio del 20 novembre 1978 il Giudice  &#13;
 relatore Antonio La  Pergola.                                            &#13;
     Ritenuto che il Comitato promotore del referendum abrogativo ha, in  &#13;
 rappresentanza dei    firmatari  della  relativa  richiesta,  sollevato  &#13;
 conflitto  di  attribuzione nei confronti dell'Ufficio  centrale per il  &#13;
 referendum presso la Corte di cassazione,  deducendo,  con  ricorso  in  &#13;
 data  23  ottobre 1978: che, nel corso della procedura conseguente alla  &#13;
 presentazione della anzidetta  richiesta di referendum,  il  Parlamento  &#13;
 ha,  con  l'articolo 22 della legge 22 maggio 1978, n. 194 - "Norme per  &#13;
 la tutela sociale della maternità e sull'interruzione volontaria della  &#13;
 gravidanza" - abrogato l'intero titolo X del libro II c.p., in  cui  si  &#13;
 trovano  tutte  le norme, delle   quali era stata chiesta l'abrogazione  &#13;
 popolare;  che  nella  specie  vengono  in  considerazione  i   criteri  &#13;
 enunciati da questa Corte nelle sentenze  nn.  68  e  69  del  1978:  e  &#13;
 precisamente,   vertendo la richiesta di referendum sull'abrogazione di  &#13;
 un organico complesso di norme, che   l'Ufficio  centrale  era  tenuto,  &#13;
 prima  di  dichiarare cessate le relative operazioni ai sensi dell'art.  &#13;
 39 della legge n. 352 del 1970,  a  valutare  se  la  nuova  disciplina  &#13;
 lasciasse  sostanzialmente    inalterati  i  principi informatori della  &#13;
 preesistente legislazione oggetto del  quesito    referendario,  ed  in  &#13;
 questa  evenienza  a  disporre  che il referendum fosse trasferito alla  &#13;
 normazione sopravvenuta; che l'Ufficio  centrale  avrebbe  erroneamente  &#13;
 applicato  l'art.  39    della  legge n. 352 del 1970 al caso in esame,  &#13;
 ritenendo che la legge 22 maggio 1978  abbia    modificato  i  principi  &#13;
 informatori  delle  norme  contemplate  dalla  richiesta di abrogazione  &#13;
 popolare, laddove, alla stregua dei canoni ermeneutici  indicati  nelle  &#13;
 citate  sentenze  di  questa    Corte,  esso avrebbe dovuto ritenere il  &#13;
 contrario; che pertanto l'ordinanza dell'Ufficio  centrale,  depositata  &#13;
 il  26  maggio  1978,  avrebbe,  col  dichiarare  cessate le operazioni  &#13;
 referendarie,  violato  l'interesse  costituzionalmente  protetto   dei  &#13;
 promotori del referendum, ed  invaso la sfera a questi riservata.        &#13;
     Ritenuto  che  dai  ricorrenti  viene  chiesto  a  questa  Corte di  &#13;
 dichiarare che all'Ufficio centrale   non è attribuito  il  potere  di  &#13;
 disporre  la  cessazione  delle  operazioni  di  cui  alla richiesta di  &#13;
 referendum riguardante gli artt. 546, 547,  548,  549,  secondo  comma,  &#13;
 550,   551,   552,  554  e  555  del  codice  penale,  e  di  annullare  &#13;
 conseguentemente l'ordinanza  dell'Ufficio  centrale  del    referendum  &#13;
 depositata in data 26 maggio 1978.                                       &#13;
     Considerato  che  a norma dell'art. 37, terzo e quarto comma, della  &#13;
 legge n. 87 del 1953, la  Corte è in questa fase chiamata a deliberare  &#13;
 senza contraddittorio se il ricorso sia  ammissibile, in quanto  esista  &#13;
 "la  materia  di  un  conflitto  la  cui  risoluzione  spetti  alla sua  &#13;
 competenza",  rimanendo  impregiudicata,  ove  la  pronuncia   sia   di  &#13;
 ammissibilità,  la  facoltà    delle  parti  di  proporre,  nel corso  &#13;
 ulteriore del giudizio, anche su questo punto, istanze ed  eccezioni.    &#13;
     Che, secondo la giurisprudenza di questa Corte, per determinare  se  &#13;
 vi  sia  materia  di  conflitto   deve accertarsi unicamente, in via di  &#13;
 prima deliberazione, la concorrenza dei requisiti di  ordine soggettivo  &#13;
 ed oggettivo prescritti dal primo comma dell'art. 37 della legge n.  87  &#13;
 del    1953,  e  cioè  se  il  conflitto sorga fra organi competenti a  &#13;
 dichiarare definitivamente la  volontà del potere cui appartengono,  e  &#13;
 per  la  delimitazione della sfera di attribuzioni,  determinata, per i  &#13;
 vari poteri, da norme costituzionali.                                    &#13;
     Che, dal punto di vista soggettivo - come questa Corte ha in  altre  &#13;
 pronunzie  affermato   (ordinanza n. 17 e sentenza n. 69 del 1978) - la  &#13;
 frazione  del  corpo  elettorale,  identificata    dall'art.  75  della  &#13;
 Costituzione  in  almeno  cinquecentomila  elettori  firmatari  di  una  &#13;
 richiesta di referendum abrogativo, è, in  virtù  delle  funzioni  ad  &#13;
 essa  attribuite e garantite, assimilabile ad  un potere dello Stato, e  &#13;
 così legittimata a sollevare conflitto di attribuzione ai sensi  degli  &#13;
 artt.  134  Cost.  e  37  della  legge  87  del  1953; che competenti a  &#13;
 dichiarare in questa sede le   volontà dei firmatari  della  richiesta  &#13;
 devono  considerarsi i promotori ed, in quanto sono  anche i promotori,  &#13;
 i presentatori della richiesta stessa; che d'altra  parte  sussiste  la  &#13;
 legittimazione   passiva  dell'Ufficio  centrale  presso  la  Corte  di  &#13;
 cassazione; in quanto organo investito, in via esclusiva e  definitiva,  &#13;
 del  potere  sia  di  decidere  sulla legittimità delle   richieste di  &#13;
 referendum abrogativo, sia di disporre  la  cessazione  delle  relative  &#13;
 operazioni,  nei   limiti previsti, secondo la sentenza n. 68 del 1978,  &#13;
 nell'art. 39 della legge 352 del 1970.                                   &#13;
     Che, sotto il profilo oggettivo,  il  conflitto  sollevato  attiene  &#13;
 alla  sfera  di  applicazione   dell'istituto del referendum abrogativo  &#13;
 configurato dal  testo  costituzionale,  essendo  stato    dedotto  dai  &#13;
 ricorrenti  che  l'Ufficio  centrale non aveva il potere di disporre la  &#13;
 cessazione delle  operazioni relative al  referendum  abrogativo  degli  &#13;
 artt.  546,  547,548,549,  secondo comma, 550, 551, 552,554, 555 codice  &#13;
 penale: e  cioè  sull'assunto  che  ai  promotori  del  referendum  è  &#13;
 garantita  la  funzione di provocare lo svolgimento della consultazione  &#13;
 popolare anche con  riguardo alle norme sopravvenute nelle  more  della  &#13;
 procedura,   le   quali   risultino  ispirate  agli    stessi  principi  &#13;
 informatori  delle  norme  inizialmente  indicate  nella  richiesta  di  &#13;
 referendum.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riservato  ogni  definitivo giudizio circa l'ammissibilità e circa  &#13;
 il merito del ricorso;                                                   &#13;
     dichiara ammissibile, ai sensi dell'art. 37 della legge n.  87  del  &#13;
 1953, il ricorso per conflitto di  attribuzione di cui in epigrafe.      &#13;
     Dispone:                                                             &#13;
     a)  che  la  cancelleria della Corte dia immediata comunicazione al  &#13;
 comitato ricorrente, nelle   persone di tutti i  suoi  componenti  come  &#13;
 indicato in ricorso, della presente ordinanza;                           &#13;
     b)  che,  a  cura  del comitato ricorrente il ricorso e la presente  &#13;
 ordinanza siano notificati   all'Ufficio  centrale  per  il  referendum  &#13;
 presso  la  Corte  di  cassazione,  entro  20  giorni  dalla  data   di  &#13;
 ricevimento della comunicazione di cui sopra.                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo  della Consulta, l'8 gennaio 1979.         &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE  ROSSANO  -  LEOPOLDO  ELIA -  &#13;
                                   GUGLIELMO ROHERSSEN - ORONZO  REALE -  &#13;
                                   BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI.                    &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
