<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>400</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:400</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/1990</data_decisione>
    <data_deposito>31/07/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 61, quarto    &#13;
 comma, della legge della Regione Veneto 16 aprile 1985, n. 33  (Norme    &#13;
 per  la  tutela  dell'ambiente),  promosso  con ordinanza emessa il 9    &#13;
 gennaio 1990 dal Pretore di Treviso - Sezione distaccata di  Vittorio    &#13;
 Veneto,  nel  procedimento penale a carico di Poli Dante, iscritta al    &#13;
 n. 310 del  registro  ordinanze  1990  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  22, prima serie speciale, dell'anno    &#13;
 1990;                                                                    &#13;
    Udito  nella  camera  di  consiglio dell'11 luglio 1990 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto che, nel corso di un procedimento penale a carico di Poli    &#13;
 Dante, imputato della contravvenzione di cui agli artt. 16, lett. b),    &#13;
 e  26  del  d.P.R. 10 settembre 1982, n. 915, per avere effettuato lo    &#13;
 stoccaggio  provvisorio  di  rifiuti  tossici  e  nocivi   senza   la    &#13;
 prescritta autorizzazione, il Pretore di Treviso - Sezione distaccata    &#13;
 di Vittorio Veneto, con ordinanza del 9 gennaio 1990 (R.O. n. 310 del    &#13;
 1990),   ha   sollevato   questione  di  legittimità  costituzionale    &#13;
 dell'art. 61, quarto comma,  della  legge  della  Regione  Veneto  16    &#13;
 aprile  1985,  n.  33,  nella  parte  in  cui  esclude  la necessità    &#13;
 dell'autorizzazione regionale per gli accumuli temporanei di  rifiuti    &#13;
 tossici e nocivi presso il produttore;                                   &#13;
      che,  ad avviso del giudice remittente, non essendovi diversità    &#13;
 sostanziale tra la nozione di  stoccaggio  provvisorio  e  quella  di    &#13;
 accumulo  temporaneo,  la disposizione regionale impugnata violerebbe    &#13;
 il precetto di cui all'art. 117 della Costituzione,  riguardando  una    &#13;
 materia  - lo smaltimento dei rifiuti - in cui non viene riconosciuta    &#13;
 alle Regioni una potestà legislativa  esclusiva  o  concorrente  con    &#13;
 quella  statale,  ed,  inoltre, contrasterebbe con il principio della    &#13;
 riserva di legge in favore dello Stato  in  materia  penale,  di  cui    &#13;
 all'art.  25  della Costituzione, disciplinando e considerando lecita    &#13;
 un'attività penalmente sanzionata dalla legge statale;                  &#13;
    Considerato  che questa Corte ha già dichiarato la illegittimità    &#13;
 costituzionale della norma impugnata in parte qua (sentenza n. 43 del    &#13;
 1990);                                                                   &#13;
      che,  pertanto,  la  questione  è manifestamente inammissibile,    &#13;
 essendo stata la norma censurata già espunta dall'ordinamento;          &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 61, quarto comma,  della  legge    &#13;
 regionale  del  Veneto  16  aprile  1985,  n. 33 (Norme per la tutela    &#13;
 dell'ambiente), già dichiarato costituzionalmente illegittimo con la    &#13;
 sentenza  n.  43  del  1990,  nella  parte  in  cui esclude l'obbligo    &#13;
 dell'autorizzazione regionale di cui agli artt. 6, lett.  d),  e  16,    &#13;
 primo  comma,  del d.P.R. 10 settembre 1982, n. 915, per gli accumuli    &#13;
 temporanei di rifiuti tossici e nocivi presso il produttore o  presso    &#13;
 l'impianto di depurazione o trattamento, in riferimento agli artt. 25    &#13;
 e 117 della Costituzione, sollevata dal Pretore di Treviso -  Sezione    &#13;
 distaccata di Vittorio Veneto, con l'ordinanza in epigrafe.              &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 luglio 1990.                               &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 31 luglio 1990.                          &#13;
                        Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
