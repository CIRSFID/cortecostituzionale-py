<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-12-10/264/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-12-10/264"/>
          <FRBRalias value="ECLI:IT:COST:2019:264" name="ECLI"/>
          <FRBRdate date="2019-12-10" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="264"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-12-10/264/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-12-10/264/ita@"/>
          <FRBRdate date="2019-12-10" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2019-12-10/264/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2019-12-10/264/ita@.xml"/>
          <FRBRdate date="2019-12-10" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2019-11-05" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2019-12-10" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale parziale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CAROSI" href="/akn/ontology/person/it/CAROSI" showAs="CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2019</akn4cc:anno>
        <akn4cc:numero>264</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2019:264</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>CAROSI</akn4cc:presidente>
        <akn4cc:relatore>Augusto Antonio Barbera</akn4cc:relatore>
        <akn4cc:data_decisione>2019-11-05</akn4cc:data_decisione>
        <akn4cc:data_deposito>2019-12-10</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale parziale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>; Giudici : <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei giudizi di legittimità costituzionale dell'art. 7, comma 1, lettera b), della legge della Regione Calabria 2 ottobre 2018, n. 37 (Modifiche alla legge regionale 31 dicembre 2015, n. 37), e dell'art. 2, comma 1, lettera c), della legge della Regione Calabria 28 dicembre 2018, n. 53 (Interventi sulle leggi regionali 24/2013, 37/2015, 21/2016, 11/2017, 1/2018, 3/2018, 5/2018, 12/2018, 15/2018, 28/2018 e 31/2018), promossi dal Presidente del Consiglio dei ministri con ricorsi notificati il 3-7 dicembre 2018 e il 27 febbraio-5 marzo 2019, rispettivamente depositati in cancelleria il 5 dicembre 2018 ed il 5 marzo 2019, iscritti al n. 83 del registro ricorsi 2018 e al n. 34 del registro ricorsi 2019 e pubblicati nella Gazzetta Ufficiale della Repubblica n. 3 e n. 19, prima serie speciale, dell'anno 2019.
          Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Calabria;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica del 5 novembre 2019 il Giudice relatore Augusto Antonio Barbera;
          uditi l'avvocato dello Stato Gianna Maria De Socio per il Presidente del Consiglio dei ministri e gli avvocati Domenico Gullo, Gianclaudio Festa, Franceschina Talarico e Massimiliano Manna per la Regione Calabria.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso depositato il 5 dicembre 2018 (r.r. n. 83 del 2018), il Presidente del Consiglio dei ministri ha impugnato l'art. 7, comma 1, lettera b), della legge della Regione Calabria 2 ottobre 2018, n. 37 (Modifiche alla legge regionale 31 dicembre 2015, n. 37), per contrasto con l'art. 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 Con tale disposizione, la Regione Calabria - nel modificare il sistema di vigilanza sulle costruzioni realizzate in zone sismiche, mediante la riscrittura del previgente art. 6 della legge regionale 31 dicembre 2015, n. 37 (Procedure per l'esecuzione di interventi di carattere strutturale e per la pianificazione territoriale in prospettiva sismica) - ha stabilito - aggiungendo, in particolare, il comma 3-ter, lettera h), all'art. 6 indicato - che le verifiche spettanti al competente settore tecnico regionale ai fini del rilascio dell'atto autorizzativo o di diniego ai sensi della normativa sismica, aventi ad oggetto la regolarità formale del progetto esecutivo e la conformità dello stesso alle vigenti norme tecniche per le costruzioni, non devono riguardare «la progettazione degli elementi non strutturali e degli impianti, salvo le eventuali interazioni con le strutture, ove la progettazione debba tenerne conto».
 1.1.- Il Presidente del Consiglio dei ministri osserva anzitutto che la disciplina delle costruzioni in zona sismica, contenendo previsioni volte a tutelare la pubblica incolumità, è riconducibile alle materie del «governo del territorio» e della «protezione civile», entrambe soggette a competenza legislativa concorrente.
 Ciò posto, rileva che la riduzione dell'ambito delle verifiche preventive spettanti alla Regione in fase di valutazione dei progetti determina, di fatto, la sottrazione di alcuni interventi edilizi in zona sismica dal controllo preventivo previsto dal <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-06-06/380/!main">decreto del Presidente della Repubblica 6 giugno 2001, n. 380</ref> (Testo unico delle disposizioni legislative e regolamentari in materia edilizia; d'ora in avanti: t.u. edilizia), le cui previsioni relative alle zone sismiche costituiscono espressione di principi fondamentali nelle ricordate materie.
 1.2.- Più specificamente, il ricorrente richiama: l'art. 65, comma 1, t.u. edilizia, ove è stabilito che «le opere di conglomerato cementizio armato, normale e precompresso ed a struttura metallica, prima del loro inizio», nonché le eventuali «varianti che nel corso dei lavori si intendano introdurre alle opere», devono essere «denunciate dal costruttore allo sportello unico, che provvede a trasmettere tale denuncia al competente ufficio tecnico regionale»; l'art. 93, comma 1, t.u. edilizia, secondo cui «nelle zone sismiche chiunque intenda procedere a costruzioni, riparazioni e sopraelevazioni, è tenuto a darne preavviso scritto allo sportello unico, che provvede a trasmetterne copia al competente ufficio tecnico della regione, indicando il proprio domicilio, il nome e la residenza del progettista, del direttore dei lavori e dell'appaltatore»; ed infine l'art. 94, comma 1, t.u. edilizia, a mente del quale nelle località sismiche «non si possono iniziare lavori senza preventiva autorizzazione scritta del competente ufficio tecnico della regione».
 1.3.- Nel sottrarre alcuni interventi all'applicazione di tali previsioni, pertanto, la disposizione impugnata si pone in contrasto con i principi fondamentali della materia; di qui il denunziato vizio di illegittimità.
 2.- In data 8 gennaio 2019 si è costituita in giudizio la Regione Calabria, eccependo l'inammissibilità del ricorso, in ragione della non corrispondenza dei motivi formulati con il contenuto della relazione dipartimentale sottesa alla deliberazione ad impugnare da parte del Consiglio dei ministri.
 Nel merito la Regione ha poi dedotto l'infondatezza della questione, osservando che le modifiche apportate dalla disposizione impugnata alla disciplina previgente erano esclusivamente volte all'armonizzazione di quest'ultima con le previsioni del decreto del Ministro delle infrastrutture e dei trasporti 17 gennaio 2018 (Aggiornamento delle «Norme tecniche per le costruzioni»; d'ora in poi: NTC 2018), in particolare laddove escludevano la necessità di svolgere un'istruttoria per gli elementi dell'intervento edilizio che non devono essere contemplati nei modelli di calcoli sismici.
 	3.- Con successivo ricorso depositato il 5 marzo 2019 (r.r. n. 34 del 2019), il Presidente del Consiglio dei ministri ha impugnato l'art. 2, comma 1, lettera c), della legge della Regione Calabria 28 dicembre 2018, n. 53 (Interventi sulle leggi regionali 24/2013, 37/2015, 21/2016, 11/2017, 1/2018, 3/2018, 5/2018, 12/2018, 15/2018, 28/2018 e 31/2018), sempre per contrasto con l'art. 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 	Con tale disposizione, la Regione è intervenuta sull'art. 6 della legge reg. Calabria n. 37 del 2015, già modificato dall'art. 7 della legge reg. Calabria n. 37 del 2018, oggetto del precedente ricorso, aggiungendovi, nella lettera b) del comma 3-ter, il periodo «in conformità a quanto previsto dalle norme tecniche per le costruzioni di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001/380/!main#art_52">art. 52 del d.P.R. 380/2001</ref>».
 3.1.- Tale aggiunta, ad avviso del ricorrente, non consente di ritenere superate le questioni di legittimità costituzionale già sollevate, configurandosi come un semplice richiamo alla normativa statale, privo di effetti sul contenuto sostanziale della disposizione originaria.
 Le richiamate NTC 2018, infatti, non avallerebbero alcuna distinzione tra opere strutturali o non strutturali ai fini delle verifiche di conformità alla normativa antisismica, ed anzi continuerebbero a prevedere - in via generale - un controllo preventivo su tutte le progettazioni, comprese quelle degli impianti. In particolare, il paragrafo 7.2.3 di dette norme, contenente i criteri di progettazione di elementi strutturali secondari ed elementi costruttivi non strutturali, definisce questi ultimi come quelli che «pur non influenzando la risposta strutturale, sono ugualmente significativi ai fini della sicurezza e/o dell'incolumità delle persone», con conseguente necessità che anch'essi siano assoggettati alle prescritte verifiche in sede di rilascio dell'autorizzazione sismica, in senso opposto a quanto previsto dalla disposizione impugnata.
 4.- Il 12 aprile 2019 si è costituita la Regione Calabria, eccependo anzitutto l'inammissibilità della questione, poiché nelle conclusioni del ricorso viene indicata una disposizione - l'art. 7, comma 1, lettera b), della legge reg. Calabria n. 37 del 2018 -diversa da quella oggetto di impugnazione.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>1.- Quanto al merito della questione, la Regione ha poi evidenziato che la modifica effettuata dalla norma impugnata aveva unicamente la finalità di armonizzare aspetti di dettaglio della complessiva disciplina urbanistica vigente nel territorio, senza introdurre alcuna deroga alle disposizioni del t.u. edilizia invocate dal ricorrente.
 In particolare, ha sostenuto che, in relazione alle opere strutturali in zone sismiche, le NTC 2018 si limitano a dettare prescrizioni di tipo autorizzatorio in capo alla pubblica amministrazione per quanto riguarda il rispetto dei criteri antisismici nella progettazione e realizzazione di opere strutturali; nessun riferimento, invece, le stesse conterrebbero in relazione agli elementi non strutturali ed agli impianti, rispetto ai quali l'intervento normativo mirava ad un obiettivo di semplificazione.
 Tale obiettivo, del resto, caratterizzava anche il processo di modifica della disciplina statale attualmente in corso, evincibile dal contenuto dello schema di decreto-legge all'esame del Governo, nel quale era prevista una modifica del t.u. edilizia con sottrazione di alcuni interventi edilizi "minori" da ogni forma di verifica preventiva.
 5.- In prossimità dell'udienza le parti hanno depositato memorie illustrative.
 5.1.- Il Presidente del Consiglio dei ministri ha depositato memoria unica in relazione ad entrambi i ricorsi, ribadendo le proprie argomentazioni in ordine alla prospettata illegittimità delle disposizioni impugnate.
 5.2.- La Regione Calabria ha argomentato in relazione al solo ricorso r.r. n. 83 del 2018, ribadendo la propria eccezione di inammissibilità e rilevando altresì che, a seguito dell'entrata in vigore del <ref href="/akn/it/act/decretoLegge/stato/2019-04-18/32/!main">decreto-legge 18 aprile 2019, n. 32</ref> (Disposizioni urgenti per il rilancio del settore dei contratti pubblici, per l'accelerazione degli interventi infrastrutturali, di rigenerazione urbana e di ricostruzione a seguito di eventi sismici), convertito con modificazioni nella <ref href="/akn/it/act/legge/stato/2019-06-14/55/!main">legge 14 giugno 2019, n. 55</ref>, è stato aggiunto al t.u. edilizia l'art. 94-bis, che operava una distinzione fra interventi edilizi in ragione dell'idoneità degli stessi ad arrecare nocumento alla pubblica incolumità, con effetto anche sul regime delle verifiche preventive in ambito sismico.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso depositato il 5 dicembre 2018 (r.r. n. 83 del 2018), il Presidente del Consiglio dei ministri ha promosso questione di legittimità costituzionale dell'art. 7, comma 1, lettera b), della legge della Regione Calabria 2 ottobre 2018, n. 37 (Modifiche alla legge regionale 31 dicembre 2015, n. 37).
 La norma oggetto di censura, modificando il comma 3 dell'art. 6 della legge Regione Calabria 31 dicembre 2015, n. 37 (Procedure per l'esecuzione di interventi di carattere strutturale e per la pianificazione territoriale in prospettiva sismica), incide sulla disciplina della vigilanza sulle costruzioni in zone sismiche, che prescrive l'effettuazione, da parte del competente servizio tecnico regionale, di verifiche preventive sulla regolarità formale degli interventi edilizi e sulla conformità degli stessi alle vigenti norme tecniche per le costruzioni, ai fini del rilascio dell'atto autorizzativo o di diniego ai sensi della normativa sismica; intervenendo su detta previsione, la norma impugnata prescrive che tali verifiche non debbano riguardare «la progettazione degli elementi non strutturali e degli impianti, salvo le eventuali interazioni con le strutture, ove la progettazione debba tenerne conto».
 Il Presidente del Consiglio dei ministri sostiene che tale esenzione si porrebbe in contrasto con i principi fondamentali contenuti nella legislazione statale, ed in particolare con gli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-06-06/380/!main#art_65">artt. 65</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-06-06/380/!main#art_93">93</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-06-06/380/!main#art_94">94 del decreto del Presidente della Repubblica 6 giugno 2001, n. 380</ref></mref> (Testo unico delle disposizioni legislative e regolamentari in materia edilizia, d'ora in avanti: t.u. edilizia), che impongono verifiche preventive sull'intero territorio nazionale in relazione agli interventi edilizi da realizzare in località sismiche; denunzia, pertanto, un contrasto della norma impugnata con l'art. 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, nelle materie di legislazione concorrente della «protezione civile» e del «governo del territorio».
 2.- Con successivo ricorso depositato il 5 marzo 2019 (r.r. n. 34 del 2019), il Presidente del Consiglio dei ministri ha poi impugnato l'art. 2, comma 1, lettera c), della legge della Regione Calabria 28 dicembre 2018, n. 53 (Interventi sulle leggi regionali 24/2013, 37/2015, 21/2016, 11/2017, 1/2018, 3/2018, 5/2018, 12/2018, 15/2018, 28/2018 e 31/2018).
 Con tale disposizione, il legislatore calabrese, modificando l'art. 6, comma 3-ter, lettera b) della legge reg. Calabria n. 37 del 2015, ha inciso la disposizione oggetto del precedente ricorso, aggiungendovi il periodo «in conformità a quanto previsto dalle norme tecniche per le costruzioni di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001/380/!main#art_52">art. 52 del d.P.R. 380/2001</ref>».
 Ad avviso del ricorrente, detta modifica non consente di superare il dubbio di costituzionalità già formulato, configurandosi come un semplice richiamo alla normativa statale, privo di effetti sul contenuto sostanziale della disposizione originaria.
 3.- In entrambi i ricorsi si è costituita la Regione Calabria, concludendo per l'inammissibilità delle questioni e comunque per la loro infondatezza nel merito.
 4.- I due ricorsi sollevano un'identica questione, sicché i relativi giudizi vanno riuniti per essere decisi con un'unica sentenza.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>5.</num>
            <content>
              <p>- In via preliminare, va anzitutto rilevata l'infondatezza delle eccezioni di inammissibilità sollevate dalla Regione Calabria in relazione ad entrambi i ricorsi.
 Nel primo giudizio, la Regione ha infatti eccepito l'inammissibilità della questione per «non corrispondenza dei motivi, come formulati, con la relazione dipartimentale sottesa alla delibera del Consiglio dei ministri»; nel secondo, invece, per «erronea indicazione della disposizione normativa censurata» nelle conclusioni del ricorso.
 5.1.- Quanto alla prima eccezione, va osservato che la relazione del Dipartimento per gli affari regionali e le autonomie, cui la delibera di impugnazione del Consiglio dei ministri fa rinvio, contiene un'analitica indicazione dei profili di contrasto fra la norma censurata e le leggi statali invocati dal Governo quali parametri interposti, in termini del tutto corrispondenti al contenuto del ricorso.
 5.2.- Quanto alla seconda eccezione, poi, l'indicazione, nelle conclusioni del ricorso, della disposizione impugnata nel primo ricorso - l'art. 7, comma 1, lettera b) della legge reg. Calabria n. 37 del 2018 - anziché di quella oggetto del nuovo dubbio di costituzionalità - l'art. 2, comma 1, lettera c) della legge reg. Calabria n. 53 del 2018 - è riconducibile ad un errore materiale e non incide sul thema decidendum, per la cui individuazione occorre far riferimento alla motivazione dell'atto di promovimento (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/97/!main">is, sentenza n. 97 del 2019</ref>), in effetti interamente calibrata sul secondo intervento normativo della Regione Calabria.
 6.- Ciò posto, le questioni sono fondate nei termini che verranno precisati.
 6.1.- Secondo la consolidata giurisprudenza di questa Corte (da ultimo, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/246/!main">sentenza n. 246 del 2019</ref>; si vedano anche le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/60/!main">sentenze n. 60 del 2017</ref>, n. 167 del 2014 e n. 300 del 2013), la disciplina degli interventi edilizi in zona sismica è riconducibile all'ambito materiale del «governo del territorio», nonché a quello relativo alla «protezione civile» per i profili concernenti la tutela dell'incolumità pubblica.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>In entrambe le materie, di potestà legislativa concorrente, spetta allo Stato fissare i principi fondamentali.
 6.2.- A tale riguardo, la Corte ha ritenuto che assumano la valenza di principio fondamentale nelle cennate materie le disposizioni contenute nel t.u. edilizia che prevedono determinati adempimenti procedurali, ove questi ultimi rispondano ad esigenze unitarie, particolarmente pregnanti di fronte al rischio sismico (ex mult<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/232/!main">is, sentenze n. 232</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/60/!main">60 del 2017</ref></mref>, n. 282 del 2016 e n. 300 del 2013).
 Fra tali disposizioni, assume primario rilievo il disposto dei parametri evidenziati dal Presidente del Consiglio dei ministri, ed in particolare dell'art. 93, comma 1, del t.u. edilizia - in forza del quale «nelle zone sismiche chiunque intenda procedere a costruzioni, riparazioni e sopraelevazioni, è tenuto a darne preavviso scritto allo sportello unico, che provvede a trasmetterne copia al competente ufficio tecnico della regione, indicando il proprio domicilio, il nome e la residenza del progettista, del direttore dei lavori e dell'appaltatore» - e dell'art. 94, comma 1, t.u. edilizia, a mente del quale nelle località sismiche, ad eccezione di quelle a bassa sismicità, «non si possono iniziare lavori senza preventiva autorizzazione scritta del competente ufficio tecnico della regione».
 Tali previsioni costituiscono espressione evidente, alla pari degli altri parametri interposti indicati dal ricorrente, dell'intento unificatore che informa la legislazione statale, in tal senso orientata alla tutela dell'incolumità pubblica, che «non tollera alcuna differenziazione collegata ad ambiti territoriali» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/232/!main">sentenze n. 232 del 2017</ref> e n. 272 del 2016).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Pertanto le norme impugnate si pongono in contrasto con i richiamati principi fondamentali laddove sottraggono all'autorizzazione preventiva alcune parti progettuali degli interventi edilizi.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>6.</num>
            <content>
              <p>3.- Del resto, questa Corte ha già avuto modo di dichiarare costituzionalmente illegittime analoghe disposizioni emanate da altre Regioni, caratterizzate dal sottrarre ad ogni forma di vigilanza preventiva alcuni interventi edilizi realizzati in zone sismiche, non tipizzati dalla legislazione statale di riferimento (così le già citate <mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/232/!main">sentenze n. 232</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/60/!main">60 del 2017</ref></mref>, nonché la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/272/!main">sentenza n. 272 del 2016</ref>).
 In tali occasioni, la Corte ha ritenuto irrilevante il fatto che, come nella presente fattispecie, la norma regionale avesse esentato dalla previa autorizzazione sismica le sole opere "minori"; e tanto sia perché tutti gli interventi sul patrimonio edilizio esistente «sono ricompresi nell'ampio e trasversale concetto di opera edilizia rilevante per la pubblica incolumità utilizzato dalla normativa statale (artt. 83 e 94 del t.u. edilizia) con riguardo alle zone dichiarate sismiche, e ricadono quindi nell'ambito di applicazione dello stesso art. 94», sia perché l'autorizzazione preventiva costituisce «uno strumento tecnico idoneo ad assicurare un livello di protezione dell'incolumità pubblica indubbiamente più forte e capillare» e perciò «riveste una posizione "fondante" del settore dell'ordinamento al quale pertiene, attesa la rilevanza del bene protetto» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/232/!main">sentenza n. 232 del 2017</ref>).</p>
            </content>
          </paragraph>
          <paragraph>
            <num>6.</num>
            <content>
              <p>4.- Né, infine, assume alcun rilievo ai fini del ravvisato contrasto il contenuto del decreto del Ministro delle infrastrutture e dei trasporti del 17 gennaio 2018 (Aggiornamento delle «Norme tecniche per le costruzioni»; d'ora in avanti: NTC 2018), cui il legislatore calabrese ha ricondotto la propria scelta di sottrarre alcuni interventi dalle verifiche preventive di conformità sismica.
 In tal senso - e premesso che, per vero, la Regione Calabria non ha neppure indicato le previsioni che supporterebbero tale assunto - va osservato che le NTC 2018, al paragrafo 7.2, contengono una distinta previsione della «progettazione dei sistemi strutturali» (paragrafo 7.2.2) e della «progettazione di elementi costruttivi non strutturali» (paragrafo 7.2.3), definendo questi ultimi come elementi «con rigidezza, resistenza e massa tali da influenzare in maniera significativa la risposta strutturale» ovvero «che, pur non influenzando la risposta strutturale, sono ugualmente significativi ai fini della sicurezza e/o dell'incolumità delle persone».
 Le NTC 2018, pertanto, mantengono quale elemento distintivo fra le categorie di interventi edilizi quello dell'idoneità degli stessi ad arrecare pericolo per l'incolumità delle persone, ovvero un criterio affatto diverso da quello preso in considerazione dal legislatore regionale.
 Infatti le stesse norme, al paragrafo 7.3.6, specificano che «la capacità degli elementi non strutturali, compresi gli eventuali elementi strutturali che li sostengono e collegano, tra loro e alla struttura principale, deve essere maggiore della domanda sismica corrispondente a ciascuno degli stati limite da considerare» e che «le verifiche degli elementi non strutturali si effettuano in termini di funzionamento e stabilità», in termini che non delineano alcuna attenuazione delle necessarie cautele in ambito sismico.
 7.- Le superiori considerazioni non mutano anche alla luce delle modifiche intervenute, successivamente alla proposizione dei ricorsi, sulla disciplina statale degli interventi edilizi in zone sismiche, per effetto dell'entrata in vigore del <ref href="/akn/it/act/decretoLegge/stato/2019-04-18/32/!main">decreto-legge 18 aprile 2019, n. 32</ref> (Disposizioni urgenti per il rilancio del settore dei contratti pubblici, per l'accelerazione degli interventi infrastrutturali, di rigenerazione urbana e di ricostruzione a seguito di eventi sismici), convertito con modificazioni nella <ref href="/akn/it/act/legge/stato/2019-06-14/55/!main">legge 14 giugno 2019, n. 55</ref>.
 7.1.- Tale decreto, coerentemente con le finalità di semplificazione dei procedimenti amministrativi in ambito edilizio, che ne connotano il complessivo contenuto, ha, fra l'altro, introdotto l'art. 94-bis del t.u. edilizia, rubricato «Disciplina degli interventi strutturali in zone sismiche».
 Detta norma distingue fra diverse categorie di interventi edilizi, sulla base del fatto che gli stessi siano «rilevanti», «di minore rilevanza» o «privi di rilevanza» nei riguardi della pubblica incolumità; a tale distinzione la norma fa poi corrispondere una diversa disciplina del relativo procedimento autorizzatorio, che, in particolare, esenta dall'autorizzazione preventiva gli interventi «di minore rilevanza» o «privi di rilevanza».</p>
            </content>
          </paragraph>
          <paragraph>
            <num>7.</num>
            <content>
              <p>2.- Per l'individuazione specifica delle diverse tipologie di intervento, la norma statale rimanda alla definizione di linee-guida con apposito decreto del Ministero delle infrastrutture e dei trasporti, specificando che, nelle more, le Regioni potranno «confermare le disposizioni vigenti», salva la necessità di adottare, una volta emanate le linee-guida, «specifiche elencazioni di adeguamento».
 Allo stato, le linee-guida non sono ancora state definite; peraltro, coerentemente con la previsione della legge statale, l'art. 2 della legge della Regione Calabria 16 ottobre 2019, n. 37, recante «Modifiche e integrazioni alla legge regionale 31 dicembre 2015, n. 37 (Procedure per l'esecuzione di interventi di carattere strutturale e per la pianificazione territoriale in prospettiva sismica)», ha «demandato al dipartimento regionale competente il compito di predisporre le specifiche elencazioni» degli interventi edilizi riconducibili alle diverse categorie di interventi, disponendo altresì per il successivo adeguamento delle stesse a quelle definite con decreto ministeriale.
 7.3.- Ad avviso della Regione, siffatto mutamento del quadro normativo statale di riferimento determinerebbe il sostanziale venir meno del contrasto denunciato dal ricorrente; i principi fondamentali, riservati alla legislazione dello Stato, sarebbero infatti oggi allineati alle disposizioni impugnate, risultando anch'essi orientati alla semplificazione dell'azione amministrativa nell'ambito dell'edilizia in zone sismiche per gli interventi di minore impatto.
 Osserva invece la Corte - così come ribadito dal Presidente del Consiglio dei ministri - che, a prescindere da ogni considerazione sul possibile rilievo di mutamenti sopravvenuti del parametro interposto, la modifica della legge statale mantiene invariati i profili di disallineamento già rappresentati rispetto alle leggi regionali impugnate.
 7.4.- Va infatti rilevato che non vi è alcuna coincidenza fra il criterio di differenziazione degli interventi adottato dal legislatore statale, che si fonda sull'idoneità degli stessi ad arrecare nocumento all'incolumità pubblica, e quello adottato dal legislatore regionale, basato sull'incidenza del progetto sugli elementi strutturali della costruzione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Del resto, che si tratti di discipline non sovrapponibili è reso evidente dalla stessa rubrica dell'art. 94-bis del t.u. edilizia, ove si evidenzia che esso è riferito ai soli interventi strutturali; le disposizioni impugnate, invece, intendono sottrarre alle verifiche preventive in ambito sismico gli interventi non strutturali (ovvero, fra gli altri, e come prima rilevato, quelli relativi agli impianti che non presentano interazioni con la struttura degli edifici) e perciò una categoria di interventi estranea in radice al campo di applicazione della novella legislativa.
 7.5.- Dev'essere, pertanto, dichiarata l'illegittimità costituzionale dell'art. 7, comma 1, lettera b), della legge reg. Calabria n. 37 del 2018 - limitatamente alla parte in cui introduce il comma 3-ter, lettera b), dell'art. 6 della legge reg. Calabria n. 37 del 2015, - e dell'art. 2, comma 1, lettera c), della legge reg. Calabria n. 53 del 2018.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 riuniti i giudizi;
 1) dichiara l'illegittimità costituzionale dell'art. 7, comma 1, lettera b), della legge della Regione Calabria 2 ottobre 2018, n. 37 (Modifiche alla legge regionale 31 dicembre 2015, n. 37), limitatamente alla parte in cui introduce il comma 3-ter, lettera b), dell'art. 6 della legge della Regione Calabria 31 dicembre 2015, n. 37 (Procedure per l'esecuzione di interventi di carattere strutturale e per la pianificazione territoriale in prospettiva sismica);
 2) dichiara l'illegittimità costituzionale dell'art. 2, comma 1, lettera c), della legge della Regione Calabria 28 dicembre 2018, n. 53 (Interventi sulle leggi regionali 24/2013, 37/2015, 21/2016, 11/2017, 1/2018, 3/2018, 5/2018, 12/2018, 15/2018, 28/2018 e 31/2018).
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2019-11-05">5 novembre 2019</date>.
 F.to:
 <signature><judge refersTo="#aldoCarosi" as="#presidente">Aldo CAROSI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#augustoAntonioBarbera" as="#relatore">Augusto Antonio BARBERA</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2019-12-10">10 dicembre 2019</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
