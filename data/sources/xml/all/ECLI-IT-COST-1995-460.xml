<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>460</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:460</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CAIANIELLO</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/10/1995</data_decisione>
    <data_deposito>26/10/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Vincenzo CAIANIELLO; &#13;
 Giudici: avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 46, comma  3,    &#13;
 e  49,  ultimo  comma,  del  codice di procedura penale, promosso con    &#13;
 ordinanza emessa il 12 luglio 1994 dalla Corte d'appello  di  Trieste    &#13;
 nel  procedimento  penale  a carico di Pahor Samo, iscritta al n. 545    &#13;
 del registro ordinanze 1994 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 39, prima serie speciale, dell'anno 1994;            &#13;
    Visto   l'atto  d'intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 28 settembre 1995  il  Giudice    &#13;
 relatore Francesco Guizzi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  procedimento  penale  a  carico di Pahor Samo la Corte    &#13;
 d'appello  di  Trieste  ha  sollevato   questione   di   legittimità    &#13;
 costituzionale  degli  artt.  46,  comma  3,  e 49, ultimo comma, del    &#13;
 codice di procedura penale. Dopo aver presentato con atti del 9 marzo    &#13;
 e del 5 ottobre 1993 due  istanze  di  rimessione  del  procedimento,    &#13;
 rigettate  dalla Corte di cassazione con sentenze del 5 maggio 1993 e    &#13;
 19  gennaio  1994,  l'imputato  -  avvalendosi   della   possibilità    &#13;
 offertagli  dalla  normativa citata - ne aveva riproposto altra sulla    &#13;
 base di fatti nuovi rispetto a quelli in precedenza addotti.             &#13;
    Osserva il Collegio che la Corte di cassazione ha  sempre  escluso    &#13;
 ogni  possibilità  di  sindacato  da  parte  del  giudice  di merito    &#13;
 sull'ammissibilità di siffatta istanza, e ciò anche nel caso  (come    &#13;
 quello  in  esame)  in  cui vi sia stata reiterazione della richiesta    &#13;
 sulla base di motivi "solo  apparentemente  nuovi".  Tale  situazione    &#13;
 costituirebbe,   di   fatto,   fonte   di  pericolo  per  l'imminente    &#13;
 prescrizione dei reati. E palesandosi una violazione del principio di    &#13;
 obbligatorietà dell'azione penale,  contenuto  nell'art.  112  della    &#13;
 Costituzione,  la  Corte  d'appello  di  Trieste ha sollevato la già    &#13;
 menzionata questione di costituzionalità, anche per  l'implicazione,    &#13;
 vantaggiosa,  di  "sterilizzare" il termine prescrizionale del reato.    &#13;
 Perché attraverso la reiterazione delle istanze di  rimessione  (pur    &#13;
 se  infondate)  l'imputato porrebbe in essere le condizioni atte alla    &#13;
 maturazione  della  prescrizione,  così  sottraendosi  al   giudizio    &#13;
 instauratosi in seguito all'attivazione dell'azione penale.              &#13;
    2.  -  È  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 concludendo    per    l'inammissibilità   e,   in   subordine,   per    &#13;
 l'infondatezza della questione.                                          &#13;
    Premette l'Avvocatura che l'istituto della  rimessione,  se  e  in    &#13;
 quanto   ripetutamente   utilizzato,   può   certo   determinare  le    &#13;
 conseguenze negative segnalate. Che tuttavia non discendono - come si    &#13;
 sostiene nell'ordinanza  della  Corte  d'  appello  -  dalla  mancata    &#13;
 attribuzione  al  giudice  di  merito del sindacato di ammissibilità    &#13;
 dell'istanza di rimessione, poiché nulla  impedirebbe  all'imputato,    &#13;
 dopo  la  declaratoria  di  inammissibilità  della prima istanza, di    &#13;
 riproporla sulla base di motivi,  anche  solo  apparentemente  nuovi,    &#13;
 ottenendo   comunque  l'effetto  di  impedire  la  decisione  finale.    &#13;
 L'inconveniente potrebbe essere risolto o attraverso  un  sistema  di    &#13;
 preclusioni  tale  da  impedire  la riproposizione della richiesta, o    &#13;
 consentendo al giudice di merito di pronunciare la sentenza  (che  è    &#13;
 la  sola attività processuale ad essergli preclusa, in attesa che la    &#13;
 Cassazione si esprima sulla rimessione), o,  infine,  non  computando    &#13;
 nei   termini   di  prescrizione,  nelle  ipotesi  di  rigetto  o  di    &#13;
 inammissibilità dell'istanza, il periodo in cui il  processo  rimane    &#13;
 sospeso.                                                                 &#13;
    Sebbene  sottenda un problema applicativo, la questione - conclude    &#13;
 l'Avvocatura  -  sarebbe  mal  formulata:  sia  perché  non  risulta    &#13;
 impugnata  la  norma  che  disciplina  il termine di prescrizione dei    &#13;
 reati, sia perché si richiederebbe alla Corte una pronuncia  su  una    &#13;
 materia rimessa alle scelte discrezionali del legislatore.<diritto>Considerato in diritto</diritto>1.  -  È all'esame della Corte, per violazione dell'art. 112 della    &#13;
 Costituzione, la questione di legittimità costituzionale degli artt.    &#13;
 46, comma 3, e 49, ultimo comma, del codice di procedura  penale,  in    &#13;
 quanto  permettono all'imputato di riproporre ad libitum la richiesta    &#13;
 di rimessione del  procedimento,  basata  su  motivi  anche  solo  in    &#13;
 apparenza   nuovi,   così   favorendo  il  decorso  del  termine  di    &#13;
 prescrizione.                                                            &#13;
    2. - La questione è inammissibile.                                   &#13;
    Non  si  ignorano  in  questa  sede,  né  si  sottovalutano,  gli    &#13;
 inconvenienti  lamentati  dal  giudice  a  quo,  che  consistono  non    &#13;
 soltanto nella possibilità di un uso distorto della  riproposizione,    &#13;
 a  fini  dilatori,  della  richiesta di rimessione - stante il potere    &#13;
 esclusivo  conferito   alla   Corte   di   cassazione   di   decidere    &#13;
 sull'ammissibilita'e la fondatezza di essa - ma consistono, altresì,    &#13;
 nell'obbligo per il giudice di merito di fermarsi, ai sensi dell'art.    &#13;
 47,  comma  1,  del  codice  di  rito,  alle  soglie  della sentenza.    &#13;
 Tuttavia, questa Corte non può  non  rilevare  l'imprecisione  nella    &#13;
 denuncia  della  normativa  processuale,  non  essendo  coinvolto, in    &#13;
 particolare, il citato art. 47, e soprattutto non può non  ricordare    &#13;
 che  l'invocato  principio  costituzionale  è  inidoneo a garantire,    &#13;
 oltre il momento iniziale dell'impulso dato  dal  pubblico  ministero    &#13;
 (da ultimo, cfr. sentenza n. 280 del 1995), l'efficienza del processo    &#13;
 penale, che pure è bene costituzionalmente protetto.                    &#13;
    Tanto,  ovviamente,  non  preclude  la  possibilità di un riesame    &#13;
 della questione ove adeguatamente definita  ai  sensi  dell'art.  23,    &#13;
 primo comma, della legge 11 marzo 1953, n. 87.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 degli artt. 46, comma 3, e 49, ultimo comma, del codice di  procedura    &#13;
 penale,  sollevata,  in  riferimento all'art. 112 della Costituzione,    &#13;
 dalla Corte d'appello di Trieste, con l'ordinanza in epigrafe.           &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 19 ottobre 1995.                              &#13;
                       Il Presidente: CAIANIELLO                          &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 ottobre 1995.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
