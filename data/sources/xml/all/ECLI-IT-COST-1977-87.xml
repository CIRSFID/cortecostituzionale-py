<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1977</anno_pronuncia>
    <numero_pronuncia>87</numero_pronuncia>
    <ecli>ECLI:IT:COST:1977:87</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Guglielmo Roehrssen</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/05/1977</data_decisione>
    <data_deposito>30/05/1977</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. &#13;
 LEOPOLDO ELIA - Prof. GUGLIELMO ROEHERSSEN - Avv. ORONZO REALE - Dott. &#13;
 BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio promosso con ricorso del giudice istruttore presso il  &#13;
 tribunale di Torino,  iscritto  al  n.    30  del  registro  1976,  per  &#13;
 conflitto  di  attribuzione tra poteri dello Stato, sorto a seguito del  &#13;
 rifiuto  da  parte  del  Presidente  del  Consiglio  dei  ministri   di  &#13;
 trasmettere   all'Autorità   giudiziaria,   nella  loro  integralità,  &#13;
 documenti ritenuti coperti da segreto politico- militare.                &#13;
     Visto l'atto di  costituzione  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza pubblica del 13 aprile 1977 il Giudice relatore  &#13;
 Guglielmo Roehrssen;                                                     &#13;
     udito il sostituto avvocato generale dello Stato Renato Carafa, per  &#13;
 il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ricorso 5 maggio 1976 il giudice istruttore presso il tribunale  &#13;
 di Torino, nel corso di un procedimento penale  promosso  a  carico  di  &#13;
 Sogno Rata del Vallino Edgardo, Cavallo Luigi ed altri, ha sollevato un  &#13;
 conflitto  di  attribuzione  nei confronti del Presidente del Consiglio  &#13;
 dei ministri.                                                            &#13;
     Esponeva che, in seguito a richiesta fatta al Servizio Informazioni  &#13;
 Difesa, perché fosse  trasmesso  il  carteggio  relativo  all'imputato  &#13;
 Sogno, il S.I.D.  trasmetteva parte del carteggio esistente, precisando  &#13;
 che   i   restanti   documenti  non  potevano  essere  esibiti  perché  &#13;
 riferentisi   a   materia   connessa   a   "specifica   attività    di  &#13;
 controspionaggio". Il giudice istruttore esponeva di essersi rivolto al  &#13;
 Presidente   del  Consiglio  dei  ministri,  chiedendo  se  confermasse  &#13;
 l'esistenza del segreto politico- militare. Il Presidente del Consiglio  &#13;
 rispondeva che  il  carteggio  non  esibito  "rientrava  nella  materia  &#13;
 connessa  a  specifica  attività  di controspionaggio", in relazione a  &#13;
 dati formali soggettivi (nomi  di  personaggi  stranieri  e  di  agenti  &#13;
 informatori, sigle di operazioni di CS, denominazione di uffici addetti  &#13;
 alle  operazioni  ed  altri  elementi analoghi) da mantenersi segreti a  &#13;
 tutela d'interessi politici e militari".  Peraltro,  poiché  sotto  il  &#13;
 profilo del contenuto tale carteggio non conteneva notizie di carattere  &#13;
 segreto,  veniva disposta la sua trasmissione previa obliterazione "dei  &#13;
 dati formali soggettivi suindicati".                                     &#13;
     Il giudice  istruttore  di  Torino  lamentava  che  l'obliterazione  &#13;
 avrebbe  investito  anche  dati  sostanziali  e comunque che il segreto  &#13;
 opposto dal Presidente del Consiglio potrebbe investire  legittimamente  &#13;
 i  nomi  degli agenti informatori e gli altri dati relativi agli uffici  &#13;
 ed  alle  operazioni  dei  Servizi  di  sicurezza,  ma   non   potrebbe  &#13;
 altrettanto  legittimamente investire i nomi dei cittadini stranieri ai  &#13;
 quali ha accennato il  Presidente  del  Consiglio,  in  quanto  costoro  &#13;
 potrebbero  assumere  la  qualità  di  correi  - per avere contribuito  &#13;
 finanziariamente  a  quell'attività  dell'imputato  Sogno  che,  dalla  &#13;
 restante   documentazione   processuale,   apparirebbe   avere  assunto  &#13;
 rilevanza penale -  e  non  vi  sarebbe  ragione  per  assicurare  loro  &#13;
 l'impunità.                                                             &#13;
     Secondo  il  giudice di Torino l'Esecutivo, ponendo "un illegittimo  &#13;
 sbarramento al potere dovere del giudice di acquisire gli  elementi  di  &#13;
 prova   necessari  per  la  prosecuzione  dell'azione  penale"  avrebbe  &#13;
 interferito nelle funzioni giurisdizionali: di qui la  necessità  -  a  &#13;
 suo  parere  -  che la Corte costituzionale valuti se nella fattispecie  &#13;
 possa essere lesiva per la  sicurezza  delle  istituzioni  dello  Stato  &#13;
 l'acquisizione da parte dell'A.G. dei suddetti nominativi.               &#13;
     Questa  Corte  con  ordinanza  n.  49  del 1977 riteneva il ricorso  &#13;
 ammissibile ai sensi dell'art. 37 della legge n. 87 del 1953 disponendo  &#13;
 che esso fosse comunicato, unitamente all'ordinanza stessa, a cura  del  &#13;
 ricorrente, al Presidente del Consiglio dei ministri.                    &#13;
     Poiché  il giudice istruttore di Torino, con sentenza del 5 maggio  &#13;
 1976, aveva trasmesso il procedimento penale nel corso  del  quale  era  &#13;
 insorto  il  conflitto  al  giudice  istruttore  del tribunale di Roma,  &#13;
 ritenendolo competente per territorio a proseguire il processo,  questa  &#13;
 Corte  disponeva  che  la cancelleria desse comunicazione della propria  &#13;
 ordinanza anche al giudice istruttore del tribunale di Roma.             &#13;
     L'autorità  giudiziaria,  pur   provvedendo   alla   notifica   al  &#13;
 Presidente  del  Consiglio  dei ministri, non compiva il nuovo deposito  &#13;
 del ricorso,  prescritto  dall'art.    26,  terzo  comma,  delle  Norme  &#13;
 integrative  per  i  giudizi  davanti  alla  Corte  costituzionale, né  &#13;
 provvedeva a costituirsi ai sensi del comma successivo.                  &#13;
     Si  è  costituita,  invece,  l'Avvocatura  dello  Stato   per   il  &#13;
 Presidente  del  Consiglio  dei  ministri, chiedendo che il ricorso sia  &#13;
 dichiarato inammissibile o comunque respinto, sostanziandosi non in una  &#13;
 contestazione  del  potere  dell'Esecutivo  in   materia   di   segreto  &#13;
 politico-militare,  bensì  nel  modo  in  cui  tale  potere  è  stato  &#13;
 esercitato.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il ricorso proposto dall'Autorità giudiziaria contro il Presidente  &#13;
 del Consiglio dei  ministri  non  è  stato  ritualmente  proseguito  e  &#13;
 pertanto deve essere dichiarato inammissibile.                           &#13;
     La  legge  11 marzo 1953, n. 87 (art. 37), sulla costituzione ed il  &#13;
 funzionamento della Corte costituzionale, prevede, in caso di conflitti  &#13;
 di attribuzione fra poteri dello Stato, una particolare procedura,  che  &#13;
 si  articola  in  due  fasi:  la  prima  è  diretta  alla  delibazione  &#13;
 dell'ammissibilità in astratto del  ricorso  col  quale  il  conflitto  &#13;
 viene  sollevato,  sotto  il profilo dell'esistenza della materia di un  &#13;
 conflitto  la  cui  risoluzione  spetti  alla  competenza  della  Corte  &#13;
 costituzionale,  in  quanto  insorto fra organi competenti a dichiarare  &#13;
 definitivamente  la  volontà  del  potere  cui  appartengono,  per  la  &#13;
 definizione  della  sfera  di   attribuzioni   determinata   da   norme  &#13;
 costituzionali.  La  seconda  fase - eventuale - è destinata all'esame  &#13;
 del merito.                                                              &#13;
     La prima fase si svolge  senza  contraddittorio  fra  le  parti,  a  &#13;
 seguito  del  deposito  da  parte  dell'autorità ricorrente, presso la  &#13;
 cancelleria della Corte, del ricorso sul quale s'intende  provocare  la  &#13;
 pronuncia  di  ammissibilità  (art.  26,  primo e secondo comma, delle  &#13;
 Norme integrative per i  giudizi  davanti  alla  Corte  costituzionale,  &#13;
 pubblicate nella G.U. n.  71 del 1956).                                  &#13;
     Essa si chiude o con una pronuncia d'inammissibilità, che preclude  &#13;
 in  modo  definitivo  il  passaggio alla seconda fase del procedimento,  &#13;
 ovvero con un'ordinanza di ammissibilità, la quale invece  costituisce  &#13;
 un  provvedimento, che lascia impregiudicata, una volta costituitosi il  &#13;
 contraddittorio, ogni diversa e definitiva decisione  anche  in  ordine  &#13;
 alla concreta ammissibilità del conflitto, ed ha come unico effetto di  &#13;
 autorizzare il ricorrente a provocare l'apertura della seconda fase.     &#13;
     Data  l'autonomia  delle  due fasi, affinché si apra ritulmente la  &#13;
 seconda  fase  è  necessario  (art.  26,  terzo  comma,  delle   Norme  &#13;
 integrative  sopra  citate)  che  il  ricorrente notifichi il ricorso e  &#13;
 l'ordinanza di ammissibilità agli  organi  interessati,  ed  entro  20  &#13;
 giorni  dall'ultima  notificazione depositi presso la cancelleria della  &#13;
 Corte il ricorso stesso con la prova delle notificazioni eseguite: tale  &#13;
 deposito è l'atto che apre la seconda fase del procedimento.            &#13;
     Nel caso in esame la prima fase del procedimento si era chiusa  con  &#13;
 l'ordinanza  n. 49 del 1977, con la quale questa Corte disponeva che la  &#13;
 propria cancelleria  desse  comunicazione  dell'ordinanza  medesima  ai  &#13;
 giudici  istruttori  dei tribunali di Torino e di Roma e che il giudice  &#13;
 istruttore di Torino notificasse il ricorso  e  l'ordinanza  stessa  al  &#13;
 Presidente del Consiglio dei ministri.                                   &#13;
     Avvenute  le  comunicazioni  suddette da parte della cancelleria di  &#13;
 questa Corte ai giudici istruttori di Roma e di Torino, quest'ultimo ha  &#13;
 provveduto a far notificare il ricorso e l'ordinanza al Presidente  del  &#13;
 Consiglio dei ministri, ma ha omesso di depositare il ricorso presso la  &#13;
 cancelleria di questa Corte entro 20 giorni dall'avvenuta notificazione  &#13;
 al Presidente del Consiglio dei ministri.                                &#13;
     Ne   deriva  l'inammissibilità  del  ricorso,  dovendosi  ritenere  &#13;
 applicabili in materia - per il richiamo  fattone  dall'art.  22  della  &#13;
 legge  n.  87  del  1953  -  i  principi  della  normativa  dettata dal  &#13;
 Regolamento  di  procedura  dinanzi  al  Consiglio  di  Stato  in  sede  &#13;
 giurisdizionale,  il quale, in connessione con l'art. 36 del t.u. delle  &#13;
 leggi sul Consiglio stesso (r.d. 26 giugno 1924, n. 1054),  prevede  la  &#13;
 decadenza del ricorso per l'omesso deposto.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile il ricorso per conflitto di attribuzione di  &#13;
 cui in epigrafe, sollevato dal giudice istruttore presso  il  tribunale  &#13;
 di Torino.                                                               &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 24 maggio 1977.                               &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   VEZIO  CRISAFULLI  -  NICOLA  REALE -  &#13;
                                   LEONETTO AMADEI  -  GIULIO  GIONFRIDA  &#13;
                                   EDOARDO  VOLTERRA  -  GUIDO  ASTUTI -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   - LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO MALAGUGINI             &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
