<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>266</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:266</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/06/1992</data_decisione>
    <data_deposito>10/06/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Giuseppe BORZELLINO; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco &#13;
 Paolo CASAVOLA, prof. Antonio BALDASSARE, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI; prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 444 e 448 del    &#13;
 codice di procedura penale, promosso con ordinanza emessa l'8 ottobre    &#13;
 1991 dal Pretore di Velletri - sezione distaccata di  Genzano  -  nel    &#13;
 procedimento  penale  a  carico  di  Cantatore  Alessandro  ed altra,    &#13;
 iscritta al n. 12 del registro  ordinanze  1992  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  5,  prima serie speciale,    &#13;
 dell'anno 1992;                                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  6 maggio 1992 il Giudice    &#13;
 relatore Ugo Spagnoli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Decidendo  sulla  richiesta  di  applicazione  della   pena    &#13;
 concordata  avanzata  al  dibattimento  da  uno dei due coimputati di    &#13;
 concorso nel medesimo reato, il Pretore di Velletri - sez. distaccata    &#13;
 di  Genzano  -  nel  presupposto  che  essa   fosse   meritevole   di    &#13;
 accoglimento e che occorresse procedere alla separazione dei processi    &#13;
 prima  di  emanare  la  relativa sentenza, ha sollevato d'ufficio una    &#13;
 questione di legittimità costituzionale degli artt. 444 e  448  cod.    &#13;
 proc. pen., assumendone il contrasto con l'art. 3 Cost.                  &#13;
    Ad  avviso del rimettente, non sarebbe in tal caso applicabile, in    &#13;
 astratto, la disciplina della separazione contenuta nell'art. 18 cod.    &#13;
 proc. pen., dato che l'ipotesi di cui alla lettera a),  (possibilità    &#13;
 di  pervenire  prontamente alla decisione nei confronti di uno o più    &#13;
 imputati) è circoscritta all'udienza preliminare  e  non  è  quindi    &#13;
 applicabile  al  dibattimento. La separazione sarebbe poi preclusa in    &#13;
 concreto, apparendo nella specie la riunione assolutamente necessaria    &#13;
 all'accertamento dei fatti (art. 18, prima parte). Onde la violazione    &#13;
 dell'art. 3 Cost., dato che all'impossibilità di separare i processi    &#13;
 conseguirebbe per l'imputato la perdita  del  trattamento  di  favore    &#13;
 previsto   dalle   norme   impugnate:  le  quali,  quindi,  sarebbero    &#13;
 costituzionalmente illegittime "nella parte in cui non prevedono  che    &#13;
 il  giudice,  analogamente a quanto stabilito per il caso di dissenso    &#13;
 ingiustificato, possa, anche quando il P.M. abbia dato  il  consenso,    &#13;
 procedere  a  dibattimento  e  pronunciare  la  sentenza  ex art. 444    &#13;
 all'esito dello stesso, in tutti i casi in cui una  pronuncia,  resa,    &#13;
 sulla  base  degli  atti,  nei confronti di una parte dei coimputati,    &#13;
 previa  separazione  dei  processi,  nuocerebbe  in   modo   assoluto    &#13;
 all'accertamento dei fatti".                                             &#13;
    2.  -  Il  Presidente  del Consiglio dei ministri, rappresentato e    &#13;
 difeso dall'Avvocatura  Generale  dello  Stato,  ha  chiesto  che  la    &#13;
 questione  sia  dichiarata  infondata,  in quanto basata sull'erronea    &#13;
 premessa dell'impossibilità di disporre la separazione dei processi.    &#13;
 L'ipotesi in cui all'apertura del dibattimento  la  posizione  di  un    &#13;
 imputato  sia  matura  per  la  decisione ai sensi dell'art. 444 cod.    &#13;
 proc. pen. dovrebbe infatti farsi  rientrare,  secondo  l'Avvocatura,    &#13;
 nella  previsione  di  cui  alla  lettera  a) del citato art. 18, che    &#13;
 consente la separazione degli atti anche  nella  fase  dibattimentale    &#13;
 allorché   "nei  confronti  di  uno  o  più  imputati  l'istruzione    &#13;
 dibattimentale risulti conclusa".                                        &#13;
    D'altra parte, il potere del giudice di mantenere la  riunione  in    &#13;
 quanto  necessaria  all'accertamento  dei  fatti  dovrebbe  cedere di    &#13;
 fronte al diritto dell'imputato  di  poter  godere  di  un  beneficio    &#13;
 riconosciutogli  dalla  legge,  dato  che una diversa interpretazione    &#13;
 finirebbe  per  far  dipendere  il  godimento  del  trattamento  più    &#13;
 favorevole  da una valutazione di carattere economico-processuale del    &#13;
 giudice, che fuoriesce dalla logica dell'istituto,  fondato,  invece,    &#13;
 sul consenso delle parti.<diritto>Considerato in diritto</diritto>1.  -  In  un processo penale, pervenuto alla fase dibattimentale,    &#13;
 nel quale uno dei due coimputati di concorso nel medesimo reato aveva    &#13;
 chiesto  l'applicazione  di  una  pena  concordata  con  il  pubblico    &#13;
 ministero,  il  Pretore di Velletri - sezione distaccata di Genzano -    &#13;
 nel presupposto che  l'accoglimento  della  richiesta  comportava  la    &#13;
 separazione  dei  due  giudizi  e  che  questa,  peraltro,  non fosse    &#13;
 astrattamente  consentita  alla  stregua  della  disciplina  di   cui    &#13;
 all'art.   18   cod.   proc.   pen.  e  fosse  in  concreto  impedita    &#13;
 dall'assoluta necessità della riunione ai fini dell'accertamento dei    &#13;
 fatti, ha sollevato  una  questione  di  legittimità  costituzionale    &#13;
 degli  artt.  444 e 448 dello stesso codice, assumendone il contrasto    &#13;
 con l'art. 3 Cost. nella parte in cui non consentono, in tal caso, di    &#13;
 procedere al dibattimento  e  di  pronunciare  solo  all'esito  dello    &#13;
 stesso  la sentenza applicativa della pena: ciò perché, altrimenti,    &#13;
 il richiedente si vedrebbe privato di tale beneficio.                    &#13;
    2. - La questione non è fondata.                                     &#13;
    Il giudice rimettente muove dal presupposto interpretativo che  la    &#13;
 scissione  dei  procedimenti  che,  in  caso  di processo cumulativo,    &#13;
 consegue  all'introduzione  del  rito  speciale  in   questione   nei    &#13;
 confronti  di  alcuni  imputati  e  non  di  altri,  sia  fenomeno da    &#13;
 ricondurre nell'alveo della  disciplina  generale  sulla  separazione    &#13;
 contenuta  nell'art.  18  cod.  proc. pen., e che pertanto soggiaccia    &#13;
 alla regola della preclusione per l'ipotesi  di  ritenuta  necessità    &#13;
 della  riunione  ai  fini  dell'accertamento dei fatti, contenuta nel    &#13;
 primo comma di tale disposizione.                                        &#13;
    Tale presupposizione non considera, però, che il vigente  sistema    &#13;
 processuale   è   caratterizzato   non  solo  da  un  marcato  favor    &#13;
 separationis (resa  quest'ultima  tendenzialmente  obbligatoria),  ma    &#13;
 anche  da una spiccata tendenza a privilegiare i riti speciali, e tra    &#13;
 di essi l'applicazione di pena concordata.                               &#13;
    Tra i requisiti di ammissibilità  di  tale  rito,  la  disciplina    &#13;
 positiva  non  annovera  l'esigenza  di  accertamento  del  fatto nei    &#13;
 confronti dell'imputato che non lo  richieda;  e  la  prospettiva  di    &#13;
 differimento all'esito del dibattimento nei confronti di costui nella    &#13;
 sentenza  da  emettere  ex art. 444 - additata dal giudice a quo - è    &#13;
 contraddetta dalla disposizione (art. 448) che impone di pronunciarla    &#13;
 "immediatamente", non appena "ne ricorrono le condizioni".               &#13;
    Per altro verso, la circostanza che al "patteggiamento"  richiesto    &#13;
 in  fase  dibattimentale  non  si  attagli  alcuna delle regole sulla    &#13;
 separazione dettate nell'art. 18, primo comma, e che la disciplina di    &#13;
 esso non faccia cenno neanche a regole  particolari  -  del  tipo  di    &#13;
 quelle   enunciate  per  il  giudizio  direttissimo  ed  il  giudizio    &#13;
 immediato negli artt. 449, sesto comma e 453, secondo comma -  induce    &#13;
 a ritenere - come sostiene l'Avvocatura dello Stato - che nell'ottica    &#13;
 del  legislatore  il  diritto  dell'imputato  a  godere del beneficio    &#13;
 riconosciutogli dalla legge non  sia  condizionabile  da  valutazioni    &#13;
 giudiziali  di  carattere  economico  -  processuale estranee ai suoi    &#13;
 specifici  presupposti:  ciò  che  è  in  linea   con   l'opinione,    &#13;
 prospettata  in dottrina, secondo cui il silenzio normativo sul punto    &#13;
 è da intendere  come  frutto  dell'incompatibilità  logica  tra  la    &#13;
 disciplina dell'art. 18 e l'istituto in esame.                           &#13;
    Questo,  infatti,  è  congegnato  come  pattuizione  tra imputato    &#13;
 richiedente e parte pubblica, in ordine alla quale è bensì previsto    &#13;
 un controllo giurisdizionale (cfr. le sentenze nn. 313 del 1990 e 251    &#13;
 del 1991), che non include però la valutazione  delle  posizioni  di    &#13;
 eventuali  coimputati:  sicché  è  da ritenere che la scissione dei    &#13;
 procedimenti  che  concernono  costoro  sia  conseguenza   automatica    &#13;
 dell'ammissione del rito.                                                &#13;
    Così  intesa la disciplina impugnata, la questione, limitatamente    &#13;
 al profilo denunciato, resta  priva  del  proprio  presupposto  e  va    &#13;
 quindi dichiarata non fondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata, nei sensi di cui in motivazione, la questione    &#13;
 di  legittimità  costituzionale  degli artt. 444 e 448 del codice di    &#13;
 procedura  penale,  sollevata,  in  riferimento  all'art.   3   della    &#13;
 Costituzione, dal Pretore di Velletri - sezione distaccata di Genzano    &#13;
 - con ordinanza dell'8 ottobre 1991.                                     &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 1° giugno 1992.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: SPAGNOLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 giugno 1992.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
