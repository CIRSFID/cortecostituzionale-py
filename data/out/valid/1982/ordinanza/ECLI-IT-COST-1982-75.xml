<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1982-04-16/75/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1982-04-16/75"/>
          <FRBRalias value="ECLI:IT:COST:1982:75" name="ECLI"/>
          <FRBRdate date="1982-04-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="75"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1982-04-16/75/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1982-04-16/75/ita@"/>
          <FRBRdate date="1982-04-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1982-04-16/75/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1982-04-16/75/ita@.xml"/>
          <FRBRdate date="1982-04-16" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1982-04-02" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1982-04-16" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="ELIA" href="/akn/ontology/person/it/ELIA" showAs="ELIA"/>
        <TLCPerson eId="leopoldoElia" href="/akn/ontology/person/it/leopoldoElia" showAs="LEOPOLDO ELIA"/>
        <TLCPerson eId="micheleRossano" href="/akn/ontology/person/it/micheleRossano" showAs="MICHELE ROSSANO"/>
        <TLCPerson eId="guglielmoRoehrssen" href="/akn/ontology/person/it/guglielmoRoehrssen" showAs="GUGLIELMO ROEHRSSEN"/>
        <TLCPerson eId="oronzoReale" href="/akn/ontology/person/it/oronzoReale" showAs="ORONZO REALE"/>
        <TLCPerson eId="livioPaladin" href="/akn/ontology/person/it/livioPaladin" showAs="LIVIO PALADIN"/>
        <TLCPerson eId="arnaldoMaccarone" href="/akn/ontology/person/it/arnaldoMaccarone" showAs="ARNALDO MACCARONE"/>
        <TLCPerson eId="antonioLaPergola" href="/akn/ontology/person/it/antonioLaPergola" showAs="ANTONIO LA PERGOLA"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="VIRGILIO ANDRIOLI"/>
        <TLCPerson eId="giuseppeFerrari" href="/akn/ontology/person/it/giuseppeFerrari" showAs="GIUSEPPE FERRARI"/>
        <TLCPerson eId="francescoSaja" href="/akn/ontology/person/it/francescoSaja" showAs="FRANCESCO SAJA"/>
        <TLCPerson eId="giovanniVitale" href="/akn/ontology/person/it/giovanniVitale" showAs="GIOVANNI VITALE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1982</akn4cc:anno>
        <akn4cc:numero>75</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1982:75</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>ELIA</akn4cc:presidente>
        <akn4cc:relatore>Guglielmo Roehrssen</akn4cc:relatore>
        <akn4cc:data_decisione>1982-04-02</akn4cc:data_decisione>
        <akn4cc:data_deposito>1982-04-16</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge>, Presidente - Dott. <judge refersTo="#micheleRossano">MICHELE ROSSANO</judge> - Prof. <judge refersTo="#guglielmoRoehrssen">GUGLIELMO ROEHRSSEN</judge> - Avv. <judge refersTo="#oronzoReale">ORONZO REALE</judge> - 
 Dott. BRUNETTO BUCCIARELLI DUCCI - Prof. <judge refersTo="#livioPaladin">LIVIO PALADIN</judge> - Dott. <judge refersTo="#arnaldoMaccarone">ARNALDO 
 MACCARONE</judge> - Prof. <judge refersTo="#antonioLaPergola">ANTONIO LA PERGOLA</judge> - Prof. <judge refersTo="#virgilioAndrioli">VIRGILIO ANDRIOLI</judge> - Prof. <judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge> - Dott. <judge refersTo="#francescoSaja">FRANCESCO SAJA</judge>, Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <list>
            <intro>
              <p>nei  giudizi riuniti di legittimità costituzionale degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_6">artt. 6</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_14">14</ref> e  
 <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_15">15 del  d.P.R.  26  ottobre  1972,  n.  643</ref></mref>  (Istituzione  dell'imposta  
 comunale  sull'incremento  di  valore degli immobili) e degli artt. 35,  
 comma secondo, e 39, comma primo, del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/636/!main">d.P.R. 26 ottobre  1972,  n.  636</ref>  
 (Revisione della disciplina del contenzioso tributario) promossi con le  
 seguenti ordinanze:</p>
            </intro>
            <point>
              <num>1)</num>
              <content>
                <p>  ordinanza emessa il 16 giugno 1976 dalla Commissione tributaria  
 di primo grado di Perugia sul  ricorso  proposto  da  Martini  Bernardi  
 Bufalini  Carlo ed altri, iscritta al n. 61 del registro ordinanze 1977  
 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  94  del  6  
 aprile 1977;</p>
              </content>
            </point>
            <point>
              <num>2)</num>
              <content>
                <p>  ordinanza  emessa il 10 marzo 1977 dalla Commissione tributaria  
 di secondo grado di Latina sul ricorso  proposto  da  Capponi  Placido,  
 iscritta  al  n.  375  del  registro  ordinanze 1977 e pubblicata nella  
 Gazzetta Ufficiale della Repubblica n. 279 del 12 ottobre 1977;</p>
              </content>
            </point>
          </list>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  
 ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica del 12 gennaio 1982 il Giudice relatore  
 Guglielmo Roehrssen;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito  l'avvocato  dello Stato Giovanni Albisinni per il Presidente  
 del Consiglio dei ministri.</p>
          </content>
        </paragraph>
        <paragraph>
          <list>
            <intro>
              <p>Ritenuto  che  con  le  ordinanze  in  epigrafe,  aventi  contenuto  
 sostanzialmente identico, sono state sollevate:</p>
            </intro>
            <point>
              <num>a)</num>
              <content>
                <p>   dalla  Commissione  tributaria  di  primo  grado  di  Perugia,  
 questione di legittimità costituzionale, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt.  3</ref>,  
 <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_76">76</ref>  e  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_77">77  Cost.</ref></mref>, degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-06/2/!main#art_6">artt. 6</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-06/2/!main#art_14">14</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-06/2/!main#art_15">15 del d.P.R. 26 ottobre 1972</ref></mref>, n.</p>
              </content>
            </point>
          </list>
        </paragraph>
        <paragraph>
          <num>643</num>
          <list>
            <alinea>
              <content>
                <p> ("Istituzione dell'imposta comunale sull'incremento di valore degli  
 immobili"), nella parte in cui  nello  stabilire  che  l'incremento  di  
 valore  imponibile è costituito dalla differenza tra valore iniziale e  
 valore finale del bene calcolati  in  termini  monetari  nominali,  non  
 tiene conto della svalutazione monetaria;</p>
              </content>
            </alinea>
            <point>
              <num>b)</num>
              <content>
                <p>  dalla  stessa Commissione tributaria di primo grado di Perugia,  
 altra questione di legittimità  costituzionale,  in  riferimento  agli  
 <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt.  3</ref>,  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">24</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_76">76</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_77">77 Cost.</ref></mref>, degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/636/!main#art_35">artt. 35</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/636/!main#art_39">39 del d.P.R. 26 ottobre  
 1972, n. 636</ref></mref> ("Revisione della disciplina del contenzioso tributario"),  
 in quanto attribuiscono alle Commissioni tributarie il potere-dovere di  
 acquisire elementi conoscitivi tecnici soltanto attraverso relazioni di  
 organi  tecnici  dell'Amministrazione  dello   Stato,   escludendo   la  
 consulenza  tecnica  a  mezzo  di  periti nominati ad hoc ed escludendo  
 altresì comunque la partecipazione dei difensori del contribuente  nel  
 corso delle indagini svolte dagli organi tecnici dello Stato;</p>
              </content>
            </point>
            <point>
              <num>c)</num>
              <content>
                <p>  dalla  Commissione  tributaria  di  secondo  grado  di  Latina,  
 questione di legittimità costituzionale, in  riferimento  all'art.  53  
 Cost.,  degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_6">artt. 6</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_14">14 del d.P.R. n. 643/1972</ref></mref> suddetto, laddove per  
 la determinazione della base imponibile ai fini  dell'INVIM  non  tiene  
 conto della svalutazione monetaria;</p>
              </content>
            </point>
            <point>
              <num>d)</num>
              <content>
                <p>  dalla  stessa Commissione tributaria di secondo grado di Latina  
 altra questione di legittimità  costituzionale,  in  riferimento  agli  
 <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">24 Cost.</ref></mref>, dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/636/!main#art_35">art. 35 del d.P.R. n.  636/1972</ref>, citato, nella  
 parte  in  cui,  sancendo  l'obbligo  per  le Commissioni tributarie di  
 avvalersi per le indagini tecniche di organi dello Stato, violerebbe il  
 diritto di difesa ed il principio di eguaglianza.</p>
              </content>
            </point>
          </list>
        </paragraph>
        <paragraph>
          <content>
            <p>Considerato che questioni identiche a quelle sub a) e c) sono state  
 già  decise  dalla  Corte  con  sentenza  8  novembre  1979,  n.  126,  
 dichiarando la illegittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_14">art. 14 del d.P.R. 26  
 ottobre  1972,  n.  643</ref>, e dell'<ref href="/akn/it/act/legge/stato/1977-12-06/1/!main#art_8">art. 8 della legge 16 dicembre 1977</ref>, n.</p>
          </content>
        </paragraph>
        <paragraph>
          <num>904</num>
          <content>
            <p> ("Modificazioni alla  disciplina  dell'imposta  sul  reddito  delle  
 persone giuridiche e al regime tributario dei dividendi e degli aumenti  
 di  capitale,  adeguamento  del capitale minimo delle società ed altre  
 norme in materia  fiscale  e  societaria"),  "nella  parte  in  cui  le  
 disposizioni   concernenti   il   calcolo   dell'incremento  di  valore  
 imponibile netto determinano - in relazione al  periodo  di  formazione  
 dell'incremento stesso - ingiustificata disparità di trattamento tra i  
 soggetti  passivi  del tributo", e dichiarando non fondate le questioni  
 di costituzionalità degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_2">artt. 2</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_4">4</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_6">6</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_7">7</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_15">15</ref>  e  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_16">16  del  d.P.R.  26  
 ottobre  1972, n.  643</ref></mref>, sollevate in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_42">42</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_47">47</ref> e  
 <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53 della Costituzione</ref></mref>; e che  nelle  ordinanze  in  epigrafe  non  sono  
 prospettati  profili  nuovi,  né sono stati addotti motivi che possano  
 indurre la Corte a modificare la propria giurisprudenza;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, peraltro, successivamente alla decisione n. 126 del  1979,  la  
 disciplina  normativa  dell'INVIM è stata modificata con <ref href="/akn/it/act/decretoLegge/stato/1979-11-12/571/!main">decreto-legge  
 12 novembre 1979, n. 571</ref>, convertito con modificazioni nella  <ref href="/akn/it/act/legge/stato/1980-01-12/2/!main">legge  12  
 gennaio  1980,  n. 2</ref>, la quale ha soppresso l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_14">art. 14 del d.P.R. n. 643  
 del 1972</ref>, sostituito l'art.  15, e regolato le  misure  delle  aliquote  
 stabilite per gli anni 1979 e 1980 ai sensi dell'art. 16, statuendo che  
 le  nuove disposizioni si applicano anche ai rapporti sorti prima della  
 loro entrata in vigore ed a tale data non ancora definiti, "per i quali  
 tuttavia l'ammontare dell'imposta dovuta non può in ogni caso superare  
 quello    determinabile   con   i   criteri   contenuti   nelle   norme  
 precedentemente in vigore" (art. 3);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>considerato inoltre, in ordine alle questioni  sub  b)  e  d),  che  
 successivamente  alle  ordinanze  di  rimessione l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1981-11-03/739/!main#art_23">art. 23 del d.P.R. 3  
 novembre 1981, n. 739</ref> ("Norme integrative e correttive del decreto  del  
 Presidente  della  Repubbilca  26  ottobre 1972, n. 636, concernente la  
 revisione del  contenzioso  tributario")  ha  integralmente  sostituito  
 l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/636/!main#art_35">art.  35 del d.P.R. n. 636 del 1972</ref> ed ha dato una diversa disciplina  
 tanto  alla  acquisizione  di  ufficio  da  parte   delle   Commissioni  
 tributarie dei necessari elementi conoscitivi tecnici (di cui tratta in  
 particolare  l'art.  35)  quanto  alla  possibilità  di  nomina  di un  
 consulente tecnico (di cui si occupava l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/636/!main#art_39">art. 39 del d.P.R. n. 636  del  
 1972</ref>);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che,  conseguentemente,  si  ravvisa  la  necessità di disporre la  
 restituzione degli  atti  alle  Commissioni  tributarie  sopraindicate,  
 perché  accertino  se, ed in qual misura, le questioni sollevate siano  
 tuttora rilevanti.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     ordina la  restituzione  degli  atti  alle  Commissioni  tributarie  
 indicate in epigrafe.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1982-04-02">2 aprile 1982</date>.</block>
      <p>F.to: <signature><judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge></signature> - <signature><judge refersTo="#micheleRossano">MICHELE ROSSANO</judge></signature>  
                                   - <signature><judge refersTo="#guglielmoRoehrssen">GUGLIELMO ROEHRSSEN</judge></signature> - <signature><judge refersTo="#oronzoReale">ORONZO  REALE</judge></signature>  
                                   -  BRUNETTO BUCCIARELLI DUCCI - <signature><judge refersTo="#livioPaladin">LIVIO  
                                   PALADIN</judge></signature> - <signature><judge refersTo="#arnaldoMaccarone">ARNALDO MACCARONE</judge></signature> - <signature><judge refersTo="#antonioLaPergola">ANTONIO  
                                   LA  PERGOLA</judge></signature>  -  <signature><judge refersTo="#virgilioAndrioli">VIRGILIO  ANDRIOLI</judge></signature>  -  
                                   <signature><judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge></signature> - <signature><judge refersTo="#francescoSaja">FRANCESCO SAJA</judge></signature>.</p>
      <p>
        <signature><person refersTo="#giovanniVitale" as="#cancelliere">GIOVANNI VITALE</person> - <role refersTo="#cancelliere">Cancelliere</role></signature>
      </p>
    </conclusions>
  </judgment>
</akomaNtoso>
