<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>322</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:322</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>15/10/2003</data_decisione>
    <data_deposito>28/10/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 23, comma 2, del decreto legislativo 24 febbraio 1997, n.46 (Attuazione della direttiva 93/42/CEE, concernente i dispositivi medici) promosso con ordinanza del 27 settembre 2002 dal Tribunale di Milano nel procedimento penale a carico di S.A., iscritta al n. 17 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 5, prima serie speciale, dell'anno 2003. &#13;
Udito nella camera di consiglio del 24 settembre 2003 il Giudice relatore Giovanni Maria FLICK. &#13;
      Ritenuto che con l'ordinanza in epigrafe, emessa nel corso di un processo penale nei confronti di persona imputata del reato di pubblicità non autorizzata di dispositivi medici, il Tribunale di Milano ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 23, comma 2, del decreto legislativo 24 febbraio 1997, n. 46 (Attuazione della direttiva 93/42/CEE, concernente i dispositivi medici), nella parte in cui punisce con sanzione penale (arresto fino a tre mesi e ammenda da lire duecentomila a lire un milione) chi effettua la pubblicità di dispositivi medici in violazione dell'art. 21 del medesimo decreto legislativo; &#13;
      che il giudice a quo esclude, in via preliminare, che la norma impugnata — in quanto lex specialis, attuativa di una direttiva comunitaria — possa ritenersi tacitamente abrogata in seguito alla depenalizzazione, disposta dal decreto legislativo 30 dicembre 1999, n. 507 (Depenalizzazione dei reati minori e riforma del sistema sanzionatorio, ai sensi dell'art. 1 della legge 25 giugno 1999, n. 205), del reato di cui all'art. 201 del r.d. 27 luglio 1934, n. 1265 (Approvazione del testo unico delle leggi sanitarie), concernente, tra l'altro, la pubblicità non autorizzata di presidî medico-chirurgici: nozione, questa, che, in base alla definizione già offerta dall'art. 1 del d.P.R. 13 marzo 1986, n. 128 (Regolamento di esecuzione delle norme di cui all'art. 189 del testo unico delle leggi sanitarie, approvato con regio decreto 27 luglio 1934, n. 1265, e successive modificazioni, in materia di produzione e commercio di presidî medico-chirurgici), comprende anche la categoria dei dispositivi medici; &#13;
      che la disposizione denunciata continuerebbe, pertanto, a sanzionare penalmente chi effettua la pubblicità di dispositivi medici senza l'autorizzazione prevista dall'art. 21, comma 2, dello stesso d.lgs. n. 46 del 1997, quando invece, a seguito del citato d.lgs. n. 507 del 1999, la pubblicità non autorizzata di medicinali per uso umano — anch'essa precedentemente prevista come reato — è stata trasformata in semplice violazione amministrativa; &#13;
      che il diverso trattamento riservato alle due violazioni risulterebbe irragionevole, posto che la pubblicità non autorizzata di dispositivi medici e la pubblicità non autorizzata di medicinali per uso umano integrerebbero condotte lesive del medesimo bene giuridico e di eguale disvalore, tanto da essere state, in precedenza, disciplinate sempre in modo identico dal legislatore; &#13;
che entrambe le fattispecie risultavano difatti sanzionate penalmente, in origine, dall'art. 201, quinto comma, del r.d. n. 1265 del 1934 con l'arresto fino a tre mesi e con l'ammenda da lire duecentomila a un milione; &#13;
che tale uguaglianza di regime era stata indi ribadita da due decreti legislativi attuativi di direttive comunitarie: il d.lgs. 30 dicembre 1992, n. 541 (Attuazione della direttiva 92/28/CEE concernente la pubblicità dei medicinali per uso umano), quanto ai medicinali per uso umano; ed il d.lgs. n. 46 del 1997, attuativo della direttiva 93/42/CEE, quanto ai dispositivi medici; &#13;
che anche la procedura per il rilascio dell'autorizzazione risulta regolata allo stesso modo nei due casi; &#13;
che, di contro, per effetto dell'art. 7, comma 1, lettera f), della legge 25 giugno 1999, n. 205 (Delega al Governo per la depenalizzazione dei reati minori e modifiche al sistema penale e tributario), e del susseguente d.lgs. n. 507 del 1999 — in particolare, dei relativi artt. 68 e 90 (recte: 70 e 92) — la disciplina delle due fattispecie è venuta a divergere in modo netto: infatti, mentre la pubblicità non autorizzata di medicinali è stata depenalizzata, la pubblicità non autorizzata di dispositivi medici ha conservato natura penale; e ciò senza che nei lavori preparatori della citata legge delega emerga in alcun modo la ratio della soluzione adottata. &#13;
Considerato che, successivamente all'ordinanza di rimessione, è stato emanato il decreto legislativo 31 ottobre 2002, n. 271 (Modifiche ed integrazioni al decreto legislativo 24 febbraio 1997, n. 46, concernente i dispositivi medici, in attuazione delle direttive 2000/70/CE e 2001/104/CE), pubblicato nella Gazzetta Ufficiale del 12 dicembre 2002, serie generale, n. 291, il cui art. 4, comma 1, lettera b), ha sostituito la norma impugnata; &#13;
che per effetto della citata novella legislativa, le violazioni in materia di pubblicità di dispositivi medici — oltre a risultare diversamente identificate, in correlazione all'integrazione apportata dall'art. 3 del medesimo d.lgs. n. 271 del 2002 alla norma precettiva di cui all'art. 21 del d.lgs. n. 46 del 1997 — sono state trasformate in illeciti amministrativi, puniti con le sanzioni pecuniarie previste dall'art. 201 del testo unico delle leggi sanitarie: vale a dire, con le stesse sanzioni applicabili, in forza degli artt. 6, comma 10, e 15, comma 1, del d.lgs. 30 dicembre 1992, n. 541, come modificati dall'art. 90 del d.lgs. 30 dicembre 1999, n. 507, per le violazioni in materia di pubblicità di medicinali per uso umano (fattispecie invocata dal giudice rimettente come tertium comparationis); &#13;
che è quindi sopravvenuta la legge 3 febbraio 2003, n. 14 (Disposizioni per l'adempimento di obblighi derivanti dall'appartenenza dell'Italia alle Comunità europee. Legge comunitaria 2002), pubblicata nella Gazzetta Ufficiale del 7 febbraio 2003, serie generale, n. 31, il cui art. 15, comma 4, ha abrogato la norma impugnata, essendo stati i relativi contenuti trasfusi nel comma 3 dello stesso articolo, con ulteriori lievi modifiche, che lasciano comunque ferma la natura amministrativa della violazione ed il relativo assetto sanzionatorio; &#13;
che va disposta, pertanto, la restituzione degli atti al giudice a quo per un nuovo esame della rilevanza (cfr. ordinanza n. 65 del 2003, relativa ad identica questione).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
      ordina la restituzione degli atti al Tribunale di Milano. &#13;
Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 15 ottobre 2003. &#13;
F.to: &#13;
Riccardo CHIEPPA, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 28 ottobre 2003. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
