<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>18</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:18</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/01/2001</data_decisione>
    <data_deposito>23/01/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Cesare RUPERTO, Riccardo CHIEPPA, Gustavo &#13;
ZAGREBELSKY, Valerio ONIDA, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 1, comma 2, &#13;
lettera  b)  del  decreto-legge  19  settembre  1992,  n. 384 (Misure &#13;
urgenti  in  materia di previdenza, di sanità e di pubblico impiego, &#13;
nonché  disposizioni fiscali), convertito in legge 14 novembre 1992, &#13;
n. 438,  promosso  con ordinanza emessa il 10 aprile 1999 dal pretore &#13;
di Torino nel procedimento civile vertente tra Binello Angelo Luigi e &#13;
l'Istituto  nazionale  per  la previdenza sociale, iscritta al n. 331 &#13;
del  registro  ordinanze  1999  e pubblicata nella Gazzetta Ufficiale &#13;
della Repubblica n. 24 - prima serie speciale - dell'anno 1999. &#13;
    Visti   l'atto   di  costituzione  dell'INPS  nonché  l'atto  di &#13;
intervento del Presidente del Consiglio dei ministri; &#13;
    Udito  nell'udienza  pubblica  del  12  dicembre  2000 il giudice &#13;
relatore Cesare Ruperto; &#13;
    Uditi  l'avvocato  Carlo De Angelis per l'INPS e l'avvocato dello &#13;
Stato Giuseppe Stipo per il Presidente del Consiglio dei ministri. &#13;
    Ritenuto  che  -  nel  corso  di un giudizio civile, promosso dal &#13;
titolare di una pensione di anzianità per ottenere la retrodatazione &#13;
della decorrenza della pensione medesima al 1 gennaio 1993, negatagli &#13;
dall'Istituto  nazionale  per  la previdenza sociale poiché egli, al &#13;
momento  della  domanda di pensionamento, beneficiava del trattamento &#13;
di  cassa  integrazione  guadagni  straordinaria  quale dipendente di &#13;
un'impresa sottoposta a procedura concorsuale - il pretore di Torino, &#13;
con  ordinanza  emessa  il  10 aprile 1999, ha sollevato questione di &#13;
legittimità  costituzionale  dell'art.  1,  comma 2, lettera b), del &#13;
decreto-legge 19 settembre 1992, n. 384 (Misure urgenti in materia di &#13;
previdenza,  di  sanità  e di pubblico impiego, nonché disposizioni &#13;
fiscali),  convertito,  con modificazioni, in legge 14 novembre 1992, &#13;
n. 438; &#13;
        che,  secondo  il  rimettente, la denunciata norma si pone in &#13;
contrasto  con  l'art.  3  Cost.,  "nella  parte in cui limita la non &#13;
applicabilità  delle  disposizioni di cui al comma 1 [riguardanti il &#13;
blocco  dei  pensionamenti  di  anzianità] della medesima norma alle &#13;
[sole]  ipotesi relative: "ai lavoratori dipendenti da imprese per le &#13;
quali  siano  stati approvati i programmi di cui all'art. 1, comma 2, &#13;
della legge 23 luglio 1991, n. 223, nonché ai lavoratori ai quali si &#13;
applicano  le disposizioni di cui all'art. 7, comma 7, della medesima &#13;
legge n. 223 del 1991", senza menzionare i lavoratori di cui all'art. &#13;
3,  comma  1"  (fra  cui rientra appunto il ricorrente nel giudizio a &#13;
quo); &#13;
        che infatti, sempre secondo il rimettente, è irragionevole e &#13;
lesivo  del  principio  di  uguaglianza  un  sistema  che  -  volendo &#13;
assicurare  un  contemperamento tra diritto di accedere alla pensione &#13;
di  anzianità  e  interesse  dello  Stato  a perseguire politiche di &#13;
riforma  del  sistema  di  previdenza  e  di contenimento della spesa &#13;
pubblica - introduce una disparità di trattamento fra lavoratori che &#13;
si  trovano  nella  medesima  situazione giuridica di sospensione del &#13;
rapporto di lavoro, solo perché il trattamento di cassa integrazione &#13;
guadagni  straordinaria  viene  concesso,  ai  lavoratori  di imprese &#13;
cosiddette  "in  crisi",  a   séguito  della  verifica  da  parte del &#13;
competente  organo ministeriale dell'esistenza di una effettiva crisi &#13;
aziendale,   attraverso  un  meccanismo  di  controllo  non  previsto &#13;
nell'ipotesi di impresa sottoposta a procedura concorsuale, in cui il &#13;
trattamento  medesimo è concesso ai dipendenti su semplice richiesta &#13;
del curatore; &#13;
        che osserva in proposito il rimettente come non sia dissimile &#13;
la  posizione  dei  lavoratori  di  tali  imprese,  i quali, anzi, si &#13;
trovano  in  situazione più difficile, sotto il profilo sostanziale, &#13;
essendo  irreversibile  la crisi della loro azienda e assai scarse le &#13;
possibilità  che,  al termine del periodo di integrazione salariale, &#13;
essi possano riprendere l'attività lavorativa; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato, &#13;
concludendo  per la declaratoria di inammissibilità o, in subordine, &#13;
di manifesta infondatezza della sollevata questione; &#13;
        che  si  è  costituito  in  giudizio l'INPS, concludendo nel &#13;
senso della infondatezza della questione medesima. &#13;
    Considerato  che,  secondo  quanto  più  volte  questa  Corte ha &#13;
affermato,  le  previsioni  di  "blocco" dei pensionamenti anticipati &#13;
(contenute  nei  c.d. "decreti catenaccio", di cui quello in esame è &#13;
il  primo  in  ordine  cronologico)  si  inseriscono  nel processo di &#13;
radicale  riconsiderazione del trattamento di anzianità, finalizzato &#13;
ad una complessa opera di riforma, attraverso la quale il legislatore &#13;
è  passato  da  un  iniziale  e contingente intervento di ripristino &#13;
degli equilibri finanziari delle diverse gestioni ad una soluzione di &#13;
natura  strutturale,  raggiunta  con  la legge 8 agosto 1995, n. 335, &#13;
diretta  ad  incidere  sui  requisiti  stessi  del  pensionamento (v. &#13;
sentenze  n. 245  del  1997,  n. 417  del  1996,  n. 439  del 1994 ed &#13;
ordinanze n. 318 e 92 del 1997); &#13;
        che  la Corte ha rilevato come tale articolato processo muova &#13;
(e    tragga    giustificazione)   dalla   necessità   di   influire &#13;
sull'andamento  tendenziale  della  spesa  previdenziale  mediante la &#13;
stabilizzazione  entro  determinati livelli del rapporto tra la spesa &#13;
medesima  ed  il  prodotto  interno  lordo (sentenza n. 417 del 1996, &#13;
cit.); &#13;
        che  rispetto  all'obbiettivo  perseguito  -  atteso  che  le &#13;
limitazioni   della  (originariamente  generale)  operatività  della &#13;
temporanea  sospensione  della possibilità di accesso ai trattamenti &#13;
pensionistici di anzianità di cui al comma 2 dell'art. 1 (introdotte &#13;
con  emendamento  presentato  dal  Governo davanti alla V Commissione &#13;
della  Camera,  il 14 ottobre 1992, in sede di conversione del citato &#13;
decreto-legge n. 384 del 1992) assumono, come tali, natura di deroghe &#13;
eccezionali,  non  suscettibili  di  interpretazione  analogica, alla &#13;
regola  sancita  dal  precedente  comma  1  -  la scelta di escludere &#13;
determinate  categorie di lavoratori dalle misure di blocco rientra a &#13;
pieno  titolo  nella  sfera di discrezionalità politica riservata al &#13;
legislatore; &#13;
        che  tale  scelta  neppure  appare  di  per  sé irrazionale, &#13;
essendo   dettata  dalla  ritenuta  necessità  di  porre  rimedio  a &#13;
particolari  situazioni  occupazionali  e  di  evitare  il  possibile &#13;
"ingorgo    previdenziale",    mediante    uno   scaglionamento   dei &#13;
pensionamenti (v. lavori preparatori sopra citati); &#13;
        che,   d'altronde,   mentre   l'intervento  straordinario  di &#13;
integrazione  salariale di cui all'art. 3 della legge 23 luglio 1991, &#13;
n. 223,  è  di  automatica applicazione ed è diretto a sostenere il &#13;
reddito  del  lavoratore, l'intervento di cui all'art. 1 della stessa &#13;
legge,  previsto  per  il caso di approvazione ministeriale del piano &#13;
per  le  imprese in crisi indicato nel comma 2, è invece predisposto &#13;
soprattutto (anche se non esclusivamente) a favore dell'impresa ed è &#13;
discrezionalmente  concedibile  a  séguito   d'un esame, da parte del &#13;
deputato organo amministrativo, del relativo piano aziendale; &#13;
        che  la  diversità  di  presupposti,  struttura, procedura e &#13;
finalità  dei due istituti, benché entrambi diretti ad assolvere la &#13;
funzione  di  ammortizzatori  sociali, rende palese la disomogeneità &#13;
delle  normative  poste  a  raffronto,  e  dunque l'inidoneità della &#13;
disciplina   evocata   a   fungere   da  tertium  comparationis  onde &#13;
configurare  l'asserito  vulnus  al principio di uguaglianza da parte &#13;
della norma denunciata; &#13;
        che,  pertanto, la sollevata questione deve essere dichiarata &#13;
manifestamente infondata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art.  1,  comma 2, lettera b), del &#13;
decreto-legge 19 settembre 1992, n. 384 (Misure urgenti in materia di &#13;
previdenza,  di  sanità  e di pubblico impiego, nonché disposizioni &#13;
fiscali),  convertito,  con modificazioni, in legge 14 novembre 1992, &#13;
n. 438,  sollevata  -  in riferimento all'art. 3 della Costituzione - &#13;
dal pretore di Torino, con l'ordinanza indicata in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 gennaio 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                        Il redattore: Ruperto &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 23 gennaio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
