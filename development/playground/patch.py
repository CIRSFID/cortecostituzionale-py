import os
import re

from dotenv import load_dotenv
from lxml import etree
from tqdm import tqdm
from replus import Engine
from akomantoso.serializer.inline_marker import markup
from akomantoso.serializer.common import etree_to_string
from akomantoso.utils import to_camelCase

tqdm.monitor_interval = 0

load_dotenv()

SOURCE_DIR = os.environ["AKN_CC_SOURCE_PATH"]
OUT_DIR = SOURCE_DIR.replace("CorteCostituzionale_1956_2018", "out")

here = os.path.dirname(os.path.abspath(__file__))
models = os.path.join(here, "models")
parser = Engine(models, *"i")


def _ns(tag):
    return "{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}" + tag


def _nsc(tag):
    return "{http://cortecostituzionale.it/metadata}" + tag


def names_markup(string):
    inlines = []
    for m in parser.parse(string):
        name = m.group("nome")
        eid = "#" + to_camelCase(name.value)
        inlines.append(("person", (name.start, name.end), {"refersTo": eid, "as": "#cancelliere"}))
    return markup(string, inlines, root=None)


def fix_akn(filepath):
    out_filepath = filepath.replace("CorteCostituzionale_1956_2018", "fixed")
    out_dir, _ = os.path.split(out_filepath)
    os.makedirs(out_dir, exist_ok=True)
    with open(filepath) as f:
        akn_string = f.read()
        akn_string = re.sub(r"opendocument:\n *", "opendocument:", akn_string)
    tree = etree.fromstring(akn_string)
    frbr_dates = []
    conclusion_dates = []
    data_deposito = None
    flag_conclusions = False
    for e in tree.iter():
        if e.tag == _ns("FRBRdate"):
            frbr_dates.append(e)
        if e.tag == _ns("conclusions"):
            flag_conclusions = True
        if e.tag == _ns("date") and flag_conclusions:
            conclusion_dates.append(e)
        if e.tag == _ns("step") and e.attrib["refersTo"] == "#dataDeposito":
            data_deposito = e.attrib["date"]

    if data_deposito is not None:
        for d in frbr_dates:
            d.attrib["date"] = data_deposito

    if len(conclusion_dates) > 0:
        conclusion_dates[0].attrib["refersTo"] = "#dataDecisione"
        if len(conclusion_dates) > 1:
            conclusion_dates[1].attrib["refersTo"] = "#dataDeposito"
    string = etree_to_string(tree)
    string = names_markup(string)
    with open(out_filepath, "w") as f:
        f.write(etree_to_string(tree))
    return string


def fixall():
    errors = []
    try:
        with tqdm(sorted(os.listdir(SOURCE_DIR))) as pbaranno:
            for anno_dir in pbaranno:
                anno_path = os.path.join(SOURCE_DIR, anno_dir)
                if not os.path.isdir(anno_path):
                    continue
                pbaranno.set_description(anno_dir)
                for doctype_dir in os.listdir(anno_path):
                    doctype_path = os.path.join(anno_path, doctype_dir)
                    if not os.path.isdir(doctype_path):
                        continue
                    for doc in tqdm(os.listdir(doctype_path), desc=doctype_dir):
                        docdir_filepath = os.path.join(doctype_path, doc)
                        if not os.path.isdir(docdir_filepath):
                            continue
                        for akn in os.listdir(docdir_filepath):
                            if akn.endswith(".xml"):
                                akn_filepath = os.path.join(docdir_filepath, akn)
                                try:
                                    fix_akn(akn_filepath)
                                except KeyboardInterrupt:
                                    raise
                                except Exception as e:
                                    tqdm.write(f"FATAL: {akn_filepath} | {e}")
                                    errors.append(akn_filepath)
    except KeyboardInterrupt:
        raise
    finally:
        with open("errors.txt", "w") as f:
            f.write("\n".join(errors))


if __name__ == '__main__':
    # fp = "/Users/biagio/repos/CIRSFID/cortecostituzionale-py/data/CorteCostituzionale_1956_2018/2016/sentenza/akn_it_judgment_sentenza_cost_2016_13/akn_it_judgment_sentenza_cost_2016_13_ita@_!main.xml"
    # fix_akn(fp)
    # fixall()
    # fp ="/Users/biagio/repos/CIRSFID/cortecostituzionale-py/data/CorteCostituzionale_1956_2018/2018/sentenza/akn_it_judgment_sentenza_cost_2018_141/akn_it_judgment_sentenza_cost_2018_141_ita@_!main.xml"
    # fp = "/Users/biagio/repos/CIRSFID/cortecostituzionale-py/data/CorteCostituzionale_1956_2018/1990/sentenza/akn_it_judgment_sentenza_cost_1990_29/akn_it_judgment_sentenza_cost_1990_29_ita@_!main.xml"
    # print(fix_akn(fp))
    fixall()
