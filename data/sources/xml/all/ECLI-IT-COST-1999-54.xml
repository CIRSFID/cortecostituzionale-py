<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>54</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:54</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/02/1999</data_decisione>
    <data_deposito>04/03/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, &#13;
 prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 4  della  legge    &#13;
 20  marzo 1865, n. 2248 all. E (Legge sul contenzioso amministrativo)    &#13;
 e dell'art. 31 r.d. 14 aprile 1910, n. 639  (Approvazione  del  testo    &#13;
 unico  delle  disposizioni  di  legge relative alla riscossione delle    &#13;
 entrate patrimoniali dello Stato), promosso con ordinanza  emessa  il    &#13;
 14  aprile  1997  dal giudice istruttore del Tribunale di Venezia nei    &#13;
 procedimenti civili riuniti vertenti tra una s.r.l. ed  il  Ministero    &#13;
 delle  finanze  ed  altra,  iscritta al n. 408 del registro ordinanze    &#13;
 1997 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  28    &#13;
 prima serie speciale, dell'anno 1997.                                    &#13;
   Visto  l'atto  di  costituzione  di  detta s.r.l. nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
   Udito nell'udienza pubblica del 12 gennaio 1999 il giudice relatore    &#13;
 Piero Alberto Capotosti;                                                 &#13;
   Udito l'Avvocato dello Stato Carlo Salimei per  il  Presidente  del    &#13;
 Consiglio dei Ministri.                                                  &#13;
   Ritenuto  che  il  giudice istruttore del Tribunale di Venezia, con    &#13;
 ordinanza del 14 aprile  1997,  ha  sollevato,  in  riferimento  agli    &#13;
 articoli  3,  24  e 113 della Costituzione, questione di legittimità    &#13;
 costituzionale dell'articolo 4 (rectius: articolo 4,  secondo  comma,    &#13;
 primo  periodo) della legge 20 marzo 1865, n. 2248, all. E (Legge sul    &#13;
 contenzioso amministrativo), nonché, per il caso si dovesse ritenere    &#13;
 non abrogato, dell'art. 31 del r.d. 10 (recte: 14)  aprile  1910,  n.    &#13;
 639  (Approvazione  del  testo  unico  delle  disposizioni  di  legge    &#13;
 relative alla riscossione delle entrate  patrimoniali  dello  Stato),    &#13;
 nelle  parti  in  cui sottraggono all'autorità giudiziaria ordinaria    &#13;
 (a.g.o.) il potere di sospendere, in via  cautelare,  la  riscossione    &#13;
 dei tributi;                                                             &#13;
     che il giudice a quo è stato adito, con distinti ricorsi ex art.    &#13;
 700 cod. proc. civ., perché concedesse l'immediata sospensione della    &#13;
 procedura  di  riscossione relativa ad un avviso di pagamento nonché    &#13;
 ad un'ingiunzione fiscale per accise su alcole;                          &#13;
     che,  a  suo  avviso,  sia il divieto di concedere la misura, sia    &#13;
 l'inapplicabilità della riscossione frazionata del tributo nel corso    &#13;
 del giudizio davanti al giudice  ordinario,  sia  l'attribuzione  del    &#13;
 potere cautelare di sospensione alla sola amministrazione finanziaria    &#13;
 vulnerano diversi principi costituzionali;                               &#13;
     che,  in  particolare,  entrambe le norme denunciate violerebbero    &#13;
 l'art.  3  della  Costituzione,   in   quanto   realizzerebbero   una    &#13;
 irragionevole disparità di trattamento tra i contribuenti, a seconda    &#13;
 che  essi  possano  adire per la tutela dei propri diritti il giudice    &#13;
 tributario, ovvero il giudice ordinario; lederebbero, inoltre, l'art.    &#13;
 24 della Costituzione, poiché comprimono  la  tutela  cautelare  dei    &#13;
 contribuenti   che  agiscono  in  giudizio  contro  l'amministrazione    &#13;
 finanziaria presso il giudice ordinario; si  porrebbero,  infine,  in    &#13;
 contrasto  con  l'art.    113  della  Costituzione,  in  quanto,  per    &#13;
 determinati atti impositivi, limitano la tutela  giurisdizionale  nei    &#13;
 confronti  della  Amministrazione  finanziaria, data l'esclusione dei    &#13;
 rimedi cautelari;                                                        &#13;
     che si è costituita nel giudizio la ricorrente  nei  processi  a    &#13;
 quibus,  la  quale  ha  svolto  argomenti  largamente coincidenti con    &#13;
 quelli contenuti nel provvedimento di rimessione, i quali ha ribadito    &#13;
 nella memoria depositata in prossimità dell'udienza pubblica;           &#13;
     che il Presidente del Consiglio  dei  Ministri,  rappresentato  e    &#13;
 difeso  dall'Avvocatura  generale  dello  Stato,  è  intervenuto nel    &#13;
 giudizio ed ha chiesto che la questione sia dichiarata  inammissibile    &#13;
 ovvero  non  fondata  in  quanto,  da un lato, l'inammissibilità del    &#13;
 provvedimento d'urgenza non deriva dall'art.  4  bensì  dalle  norme    &#13;
 che,  nella materia, specificamente attribuiscono all'amministrazione    &#13;
 o alle commissioni tributarie il potere cautelare, dall'altro, l'art.    &#13;
 31 del r.d. n.  639 del 1910 non sarebbe più vigente;                   &#13;
     che, all'udienza pubblica, la difesa erariale  ha  insistito  per    &#13;
 l'accoglimento delle conclusioni svolte nell'atto di intervento.         &#13;
   Considerato  che  il  giudice  a  quo  ha  affermato di avere "già    &#13;
 superato l'argomento in altra occasione adottando una interpretazione    &#13;
 adeguatrice degli articoli 4 e 5 della legge n. 2248/1965  (...)  che    &#13;
 li ponesse al riparo dal sospetto di illegittimità costituzionale";     &#13;
     che    il    giudice   rimettente   solleva   la   questione   di    &#13;
 costituzionalità dopo aver constatato il "difforme orientamento  del    &#13;
 collegio",  che,  in precedenza, "in sede di reclamo ha riformato" il    &#13;
 provvedimento d'urgenza già accordato, così da fargli  ritenere  in    &#13;
 questa  occasione  "opportuno non concedere la misura ed investire la    &#13;
 Corte della questione";                                                  &#13;
     che pertanto risulta dalla stessa prospettazione  del  giudice  a    &#13;
 quo  che  "la  questione  sottoposta all'esame di questa Corte non è    &#13;
 volta a rimuovere un dubbio di legittimità  costituzionale,  che  il    &#13;
 remittente  ha  concretamente  mostrato  di  non nutrire affatto e di    &#13;
 poter risolvere in via interpretativa, ma è finalizzata a proteggere    &#13;
 l'emananda pronuncia (...) dall'alea di  una  impugnazione  e  di  un    &#13;
 eventuale annullamento" (ordinanza n. 70 del 1998);                      &#13;
     che, data l'estraneità di una finalità siffatta alla logica del    &#13;
 giudizio  incidentale  di  legittimità  costituzionale, la questione    &#13;
 deve essere dichiarata manifestamente inammissibile  (cfr.  ordinanze    &#13;
 n. 70 del 1998 e n. 410 del 1994).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'articolo  4,  secondo  comma,  primo    &#13;
 periodo  della  legge  20  marzo  1865,  n.  2248,  all. E (Legge sul    &#13;
 contenzioso  amministrativo),  nonché  dell'articolo  31  del  regio    &#13;
 decreto  14  aprile  1910, n. 639 (Approvazione del testo unico delle    &#13;
 disposizioni  di  legge  relative  alla  riscossione  delle   entrate    &#13;
 patrimoniali  dello  Stato), sollevata, in riferimento agli articoli,    &#13;
 3, 24 e 113 della Costituzione, dal giudice istruttore del  Tribunale    &#13;
 di Venezia, con l'ordinanza in epigrafe.                                 &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 febbraio 1999.                             &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 4 marzo 1999.                             &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
