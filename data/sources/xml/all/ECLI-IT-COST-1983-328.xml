<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>328</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:328</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Leopoldo Elia</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/11/1983</data_decisione>
    <data_deposito>28/11/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - &#13;
 Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Prof. &#13;
 GIOVANNI CONSO - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 42 del d.P.R.  &#13;
 26 ottobre 1972, n. 634 (Disciplina dell'imposta di registro)  promosso  &#13;
 con  ordinanza  emessa il 1 giugno 1981 dalla Commissione tributaria di  &#13;
 primo grado di Rovereto sul ricorso presentato  da  Rigatti  Enrico  ed  &#13;
 altro,  iscritta  al  n.  635  del registro ordinanze 1981 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 12 del 1982.                &#13;
   Udito nella camera di  consiglio  dell'11  ottobre  1983  il  Giudice  &#13;
 relatore Leopoldo Elia.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
   1.  -  La  Commissione  tributaria  di  primo  grado  di  Rovereto ha  &#13;
 sollevato questione di legittimità costituzionale dell'art.  42 d.P.R.  &#13;
 26 ottobre 1972, n. 634, nella parte in cui non dispone che  anche  per  &#13;
 le  vendite forzate senza incanto la tassa proporzionale sia dovuta sul  &#13;
 prezzo di aggiudicazione.                                                &#13;
   A sostegno dell'eccezione il Tribunale richiama la  sentenza  n.  156  &#13;
 del  1976  di  questa  Corte,  con  la  quale  è stata accolta analoga  &#13;
 questione in relazione all'art. 50 della legge di registro, ritenuto in  &#13;
 contrasto con gli artt. 3 e 53 della Costituzione.                       &#13;
   2. - L'ordinanza  è  stata  regolarmente  notificata,  comunicata  e  &#13;
 pubblicata nella Gazzetta Ufficiale.                                     &#13;
   Nessuno si è costituito dinanzi alla Corte costituzionale.<diritto>Considerato in diritto</diritto>:                          &#13;
   La questione è fondata.                                               &#13;
   La  norma  denunziata  (art.  42,  d.P.R.  n.  634/1972) riproduce in  &#13;
 sostanza  l'art.  50,  secondo  comma,  della  legge  di  registro  che  &#13;
 analogamente  non  disponeva  che per le vendite forzate senza incanto,  &#13;
 effettuate ai sensi dell'art. 570 e seguenti del  codice  di  procedura  &#13;
 civile,   la   tassa   proporzionale   fosse   dovuta   sul  prezzo  di  &#13;
 aggiudicazione.                                                          &#13;
   Anche in questo caso quindi va rilevato, come già fatto con sentenza  &#13;
 n. 156/1976, che per le  vendite  coatte  senza  incanto,  disciplinate  &#13;
 dagli  artt.  570  e  seguenti del codice di procedura civile, non sono  &#13;
 contestabili l'autenticità del prezzo  pagato  e  la  sua  presumibile  &#13;
 corrispondenza   al  prezzo  di  mercato:  ciò  avviene  grazie  a  un  &#13;
 procedimento di determinazione del valore venale che, per essere  posto  &#13;
 sotto  il  controllo  del  giudice  dell'esecuzione,  e  subordinato  a  &#13;
 rigorose forme di pubblicità, presenta ampie garanzie di  oggettività  &#13;
 e di automatismo per la realizzazione del massimo ricavo possibile.      &#13;
   È  evidente  quindi  che  per  i beni soggetti ad esecuzione forzata  &#13;
 venduti senza incanto sussistono le stesse ragioni perché si  applichi  &#13;
 la  normativa contenuta nell'art. 42 del d.P.R.  n. 634/1972: ne deriva  &#13;
 che  la  discriminazione  attuata  dalla  norma  impugnata  nell'ambito  &#13;
 dell'espropriazione  forzata,  tra  vendite  realizzate  con il sistema  &#13;
 all'incanto e vendite  senza  incanto,  è  priva  di  ogni  fondamento  &#13;
 razionale e deve essere considerata costituzionalmente illegittima.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
   dichiara  la  illegittimità  costituzionale dell'art. 42 d.P.R.   26  &#13;
 ottobre 1972, n. 634 (Disciplina dell'imposta di registro), nella parte  &#13;
 in cui non dispone che anche per  le  vendite  forzate  senza  incanto,  &#13;
 effettuate  ai sensi degli artt. 570 e seguenti del codice di procedura  &#13;
 civile, la base imponibile è costituita dal prezzo di aggiudicazione.   &#13;
   Così  deciso in Roma, in camera di consiglio, nella sede della Corte  &#13;
 costituzionale, Palazzo della Consulta, il 17 novembre 1983.             &#13;
                                   F.to: LEOPOLDO ELIA - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   - GIOVANNI CONSO - ETTORE GALLO.       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
