<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:19</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>16/01/2003</data_decisione>
    <data_deposito>30/01/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 199 del codice di procedura penale, promosso, nell'ambito di un procedimento penale, dalla Corte di assise di Messina, con ordinanza del 12 febbraio 2002, iscritta al n. 262 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 22, prima serie speciale, dell'anno 2002. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri;  &#13;
    udito nella camera di consiglio del 4 dicembre 2002 il Giudice relatore Guido Neppi Modona. &#13;
    Ritenuto che la Corte di assise di Messina ha sollevato, in riferimento agli artt. 3 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 199 del codice di procedura penale, &amp;laquo;nella parte in cui non estende la facoltà di astensione dal testimoniare dei prossimi congiunti dell'imputato ai prossimi congiunti dell'imputato di procedimento connesso o collegato, il quale possa a sua volta avvalersi, in base all'art. 210 cod. proc. pen., della facoltà di non rispondere nell'ambito del processo in cui dovrebbe essere raccolta la testimonianza del prossimo congiunto&amp;raquo;; &#13;
    che la Corte rimettente premette:  &#13;
- di procedere nei confronti di alcuni soggetti per il reato di omicidio aggravato e nei confronti di altri per il reato di favoreggiamento, aggravato ai sensi dell'art. 7 del decreto-legge 13 maggio 1991, n. 152, convertito nella legge 12 luglio 1991, n. 203;  &#13;
- che nel corso del dibattimento le parti civili avevano chiesto l'esame testimoniale di un soggetto che, ammesso a tale atto, aveva manifestato la volontà di non deporre su circostanze concernenti il padre, imputato nell'ambito di procedimento &amp;laquo;connesso o collegato&amp;raquo;, il quale, citato ex art. 210 cod. proc. pen., si era avvalso della facoltà di non rispondere; &#13;
- che la testimonianza doveva vertere anche su circostanze che, se non si fosse avvalso della facoltà di non rispondere, avrebbero formato specifico oggetto dell'esame del padre; &#13;
- che i difensori degli imputati avevano prospettato &amp;laquo;una interpretazione estensiva o analogica della facoltà di astensione&amp;raquo;, prevista formalmente dall'art. 199 cod. proc. pen. solo per i prossimi congiunti dell'imputato;  &#13;
    che, ad avviso della Corte rimettente, la disposizione censurata, &amp;laquo;per interpretazione assolutamente dominante, non è suscettibile di interpretazione analogica o estensiva, in quanto il rapporto di parentela deve sussistere tra il teste e il soggetto contro cui si sta procedendo e la norma esaurisce i suoi effetti nell'ambito del processo in questione&amp;raquo;; &#13;
    che tale disciplina si porrebbe però in contrasto con l'art. 3 Cost., in quanto &amp;laquo;situazioni assimilabili sono irragionevolmente disciplinate in maniera diversa&amp;raquo;;  &#13;
    che, infatti, se la ratio della facoltà di astensione è di &amp;laquo;impedire il conflitto che si determina tra l'obbligo di testimoniare e la volontà di non compromettere, con le proprie dichiarazioni, persone legate al dichiarante da vincoli particolarmente significativi&amp;raquo;, identica ratio si rinviene nel &amp;laquo;caso in cui il rapporto che potrebbe indurre a rendere dichiarazioni non veritiere o a essere reticenti intercorra tra il testimone e altra persona imputata di reato connesso o collegato&amp;raquo;, qualora la posizione processuale di quest'ultima sia intimamente collegata all'oggetto del processo nel quale le dichiarazioni dovrebbero essere raccolte; &#13;
    che in quest'ultima ipotesi troverebbe comunque applicazione la specifica causa di non punibilità prevista dall'art. 384, primo comma, del codice penale, sì che &amp;laquo;non si intende per quale plausibile motivo non possa anticiparsi la soluzione del problema al momento stesso dell'assunzione della testimonianza&amp;raquo;, evitando così di porre il dichiarante nell'alternativa di danneggiare il congiunto o di deporre il falso; &#13;
    che la disposizione censurata si porrebbe inoltre in contrasto con l'art. 24 Cost. in quanto, a prescindere dagli eventuali limiti di utilizzabilità delle dichiarazioni rese nel diverso processo, costringere il teste, prossimo congiunto dell'imputato di reato connesso, a deporre, potrebbe frustrare, in ipotesi come quelle in esame, le ragioni che hanno indotto quest'ultimo, esaminato a norma dell'art. 210 cod. proc. pen., ad avvalersi della facoltà di non rispondere;  &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha chiesto, con riserva di dedurre, che la questione sia dichiarata inammissibile o comunque non fondata. &#13;
    Considerato che il rimettente lamenta che l'art. 199 del codice di procedura penale, limitando la facoltà di astenersi dal deporre ai prossimi congiunti dell'imputato, e non prevedendo quindi la medesima facoltà in capo ai prossimi congiunti dell'imputato in procedimento connesso o collegato, si ponga in contrasto con gli artt. 3 e 24 della Costituzione; &#13;
    che il giudice a quo rileva che, se la ratio della disciplina censurata è quella di impedire il conflitto tra l'obbligo di testimoniare e la volontà di non esporre a conseguenze pregiudizievoli persone legate al testimone da vincoli di parentela, adozione o convivenza, è irragionevole non estendere la facoltà di astenersi dal deporre alla situazione, sorretta dalla medesima ratio, in cui sia chiamato a testimoniare il prossimo congiunto di un imputato in procedimento connesso o collegato; &#13;
    che, ad avviso del rimettente, la disparità della disciplina processuale appare tanto più ingiustificata ove si consideri che la specifica causa di giustificazione prevista dall'art. 384, primo comma, cod. pen. trova applicazione anche in favore del prossimo congiunto dell'imputato in procedimento connesso o collegato;  &#13;
    che il giudice a quo - malgrado ritenga che, ove l'esame del teste riguardi, come nel caso di specie, &amp;laquo;fatti coinvolgenti la posizione e la responsabilità del congiunto&amp;raquo;, la ratio della facoltà di astensione dovrebbe comportare l'applicazione della disciplina dettata dall'art. 199 cod. proc. pen. anche al prossimo congiunto dell'imputato in procedimento connesso o collegato - rileva che la disposizione censurata non è suscettibile di integrazione analogica o interpretazione estensiva, in quanto per la giurisprudenza &amp;laquo;assolutamente dominante&amp;raquo; il rapporto di parentela deve sussistere tra il testimone e l'imputato contro cui si procede; &#13;
    che, peraltro, l'unica decisione di legittimità (Cassazione, Sezione quarta penale, 12 giugno 1996, n. 8007) menzionata dal rimettente a conferma dell'interpretazione che precluderebbe una diversa lettura della disciplina censurata non appare pertinente;  &#13;
    che, infatti, la sentenza richiamata si riferisce alla situazione del tutto diversa in cui l'avviso circa la facoltà di astenersi ex art. 199 cod. proc. pen. era stato omesso in quanto la persona nei cui confronti sussisteva il vincolo di parentela non aveva mai assunto la qualità di imputato né nel procedimento in corso, né in altri eventualmente connessi; &#13;
    che inoltre il giudice a quo, nell'adeguarsi a tale supposto, e da lui non condiviso, "diritto vivente", non prende in considerazione altri orientamenti della giurisprudenza di legittimità che gli avrebbero consentito di interpretare la disciplina censurata alla luce della ratio che sorregge la facoltà di astensione prevista dall'art. 199 cod. proc. pen., così omettendo di esplorare la possibilità di pervenire, in via interpretativa, alla soluzione che egli ritiene conforme a Costituzione; &#13;
    che questa Corte ha avuto ripetutamente occasione di affermare che il giudice è abilitato a sollevare la questione di legittimità costituzionale solo dopo avere accertato che è impossibile seguire una interpretazione costituzionalmente corretta (cfr., da ultimo, sentenza n. 202 del 1999, nonché ordinanze n. 116 del 2002, n. 233, n. 27 e n. 13 del 2000); &#13;
    che la questione va pertanto dichiarata manifestamente inammissibile.  &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 199 del codice di procedura penale, sollevata, in riferimento agli artt. 3 e 24 della Costituzione, dalla Corte di assise di Messina, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 16 gennaio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 30 gennaio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
