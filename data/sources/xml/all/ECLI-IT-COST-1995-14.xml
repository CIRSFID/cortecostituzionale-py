<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>14</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:14</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/01/1995</data_decisione>
    <data_deposito>19/01/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 11, comma 9,    &#13;
 della legge 30 dicembre 1991, n. 413 (Disposizioni  per  ampliare  le    &#13;
 basi   imponibili,   per   razionalizzare,  facilitare  e  potenziare    &#13;
 l'attività  di  accertamento;  disposizioni  per  la   rivalutazione    &#13;
 obbligatoria  dei  beni immobili delle imprese, nonché per riformare    &#13;
 il contenzioso e per la definizione agevolata dei rapporti  tributari    &#13;
 pendenti; delega al Presidente della Repubblica per la concessione di    &#13;
 amnistia  per  reati  tributari; istituzione dei centri di assistenza    &#13;
 fiscale e del conto fiscale)  promosso  con  ordinanza  emessa  il  3    &#13;
 aprile  1993  dalla Commissione tributaria di primo grado di Cagliari    &#13;
 sul ricorso proposto da Cossu Maria Rosaria  contro  l'Intendenza  di    &#13;
 finanza di Cagliari, iscritta al n. 296 del registro ordinanze 1994 e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 22, prima    &#13;
 serie speciale, dell'anno 1994;                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 26 ottobre 1994 il Giudice    &#13;
 relatore Massimo Vari;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ordinanza emessa il 3 aprile 1993 (R.O. n. 296 del 1994),    &#13;
 nel giudizio promosso da Cossu Maria Rosaria avverso il provvedimento    &#13;
 con cui l'Intendente di finanza di Cagliari ha respinto l'istanza  di    &#13;
 rimborso della imposta sull'indennità erogata il 2 febbraio 1989 per    &#13;
 la  cessione  volontaria  sostitutiva dell'esproprio di un'area della    &#13;
 ricorrente, la Commissione tributaria di primo grado di  Cagliari  ha    &#13;
 sollevato  -  in  riferimento  agli artt. 3 e 53 della Costituzione -    &#13;
 questione di legittimità costituzionale dell'art. 11, comma 9, della    &#13;
 legge 30 dicembre 1991, n. 413.                                          &#13;
    Il  giudice  a  quo  ritiene  che  "la  norma  denunciata  incida,    &#13;
 alterandolo, sul rapporto tra imposizione e capacità  contributiva",    &#13;
 tanto   più   in   quanto   l'istituzione   dell'imposta   non   era    &#13;
 ragionevolmente prevedibile. Inoltre, gli atti di cessione volontaria    &#13;
 sostitutiva dell'esproprio sono di solito  frutto  di  accordi  sulla    &#13;
 misura  dell'indennizzo,  accordi  che,  "nel periodo in cui opera la    &#13;
 retroattività   dell'imposta   sostitutiva,   erano   verosimilmente    &#13;
 influenzati dalla prospettiva della totale esenzione tributaria delle    &#13;
 somme concordate".                                                       &#13;
    In   relazione   all'art.  3  della  Costituzione,  il  remittente    &#13;
 sostiene, poi, che  alla  retroattività  della  norma  in  esame  si    &#13;
 contrappone  il  diverso  trattamento  stabilito  per  le plusvalenze    &#13;
 realizzate a seguito di cessione delle aree fabbricabili, "alle quali    &#13;
 accede la libera scelta del tempo e del modo dell'operazione".           &#13;
    2.  -  Nel  giudizio  di  fronte  alla  Corte  costituzionale   è    &#13;
 intervenuto il Presidente del consiglio dei ministri, rappresentato e    &#13;
 difeso   dall'Avvocatura  generale  dello  Stato,  chiedendo  che  la    &#13;
 questione sia dichiarata inammissibile o infondata.<diritto>Considerato in diritto</diritto>1. - La Commissione tributaria di primo  grado  di  Cagliari,  con    &#13;
 l'ordinanza   in   epigrafe,   solleva   questione   di  legittimità    &#13;
 costituzionale dell'art. 11, comma 9, della legge 30  dicembre  1991,    &#13;
 n.   413,   che   estende  le  disposizioni  sulla  tassazione  delle    &#13;
 plusvalenze conseguite in occasione di procedimenti ablatori,  ovvero    &#13;
 a  seguito  di cessioni volontarie di aree nel corso dei procedimenti    &#13;
 stessi, alle "somme percepite in occasione di atti anche volontari  o    &#13;
 di  provvedimenti  emessi  successivamente al 31 dicembre 1988 e fino    &#13;
 alla data di entrata in vigore della legge".                             &#13;
    Ad avviso del giudice remittente, la norma  sarebbe  in  contrasto    &#13;
 con  l'art.  53 della Costituzione, perché inciderebbe, alterandolo,    &#13;
 sul rapporto tra imposizione e capacità contributiva, in un caso nel    &#13;
 quale  non   era   ragionevolmente   prevedibile   l'istituzione   di    &#13;
 un'imposta. Questa, infatti, "avendo ad oggetto un prelievo di natura    &#13;
 reddituaria realizzato a distanza di oltre due anni dalla data in cui    &#13;
 il  cespite  sottoposto  a  tassazione è affluito nel patrimonio del    &#13;
 contribuente, presuppone apoditticamente la permanenza di  una  certa    &#13;
 capacità  contributiva". Osserva, altresì, l'ordinanza che gli atti    &#13;
 di cessione volontaria sostitutiva dell'esproprio nella maggior parte    &#13;
 dei  casi  sono  frutto  di  accordi  sulla  misura  dell'indennizzo,    &#13;
 verosimilmente  influenzati  dalla prospettiva della totale esenzione    &#13;
 tributaria delle somme percepite.                                        &#13;
    Con riferimento, poi, all'art. 3 della  Costituzione,  l'ordinanza    &#13;
 rileva che alla retroattività stabilita per i casi di esproprio o di    &#13;
 cessione    volontaria    sostitutiva   dell'esproprio   stesso,   si    &#13;
 contrappone, in termini  che  non  appaiono  giustificati  da  scelte    &#13;
 razionali,  il  trattamento stabilito per le plusvalenze realizzate a    &#13;
 seguito di cessione di aree fabbricabili, alle quali accede la libera    &#13;
 scelta del tempo e del modo dell'operazione.                             &#13;
    2. - La questione non è fondata.                                     &#13;
    Sotto il profilo del  lamentato  contrasto  con  l'art.  53  della    &#13;
 Costituzione,  va rammentato che la questione ha già formato oggetto    &#13;
 di esame da parte di questa Corte, conclusosi con  una  pronunzia  di    &#13;
 infondatezza  (sentenza  n.  315  del  1994)  che  si  è  rifatta al    &#13;
 principio  secondo  il  quale,  per accertare se una legge tributaria    &#13;
 retroattiva  comporti  violazione  del  principio   della   capacità    &#13;
 contributiva,  occorre  verificare,  di  volta  in volta, se la legge    &#13;
 stessa, nell'assumere a presupposto della prestazione un fatto o  una    &#13;
 situazione  passati,  abbia  spezzato il rapporto che deve sussistere    &#13;
 tra imposizione e capacità contributiva, violando così il  precetto    &#13;
 costituzionale sopra richiamato.                                         &#13;
    Sulla base di tale principio, questa Corte ha ritenuto sussistente    &#13;
 -  nel  caso  della  retroattività  conferita dall'art. 11, comma 9,    &#13;
 della legge n. 413  del  1991,  alla  norma  sulla  tassazione  delle    &#13;
 plusvalenze derivanti dalla cessione volontaria di terreni sottoposti    &#13;
 ad  espropriazione  -  un elemento di prevedibilità dell'imposta non    &#13;
 privo di significato, quanto alla  verifica  della  permanenza  della    &#13;
 capacità contributiva, specie a tener conto del breve lasso di tempo    &#13;
 entro il quale il legislatore ha stabilito che tale retroattività è    &#13;
 destinata ad operare.                                                    &#13;
    La  questione  viene ora riproposta nella ordinanza in esame senza    &#13;
 che siano introdotti nuovi profili ed argomentazioni, tali da indurre    &#13;
 a diverso avviso, sicché  la  stessa  va  dichiarata  manifestamente    &#13;
 infondata.                                                               &#13;
    3.  -  Quanto  poi  alla  denunciata  violazione dell'art. 3 della    &#13;
 Costituzione, giova ricordare l'altro principio che, del pari, emerge    &#13;
 dalla giurisprudenza costituzionale, secondo il quale è rimessa alla    &#13;
 discrezionalità  del  legislatore  l'individuazione   degli   indici    &#13;
 concretamente  rivelatori  di  ricchezza  da  assumere  a presupposto    &#13;
 dell'imposizione,    salvo    il    limite    dell'arbitrarietà    e    &#13;
 dell'irragionevolezza,  e comunque dell'esigenza del pari trattamento    &#13;
 quando  sussista  identità  nelle  situazioni  di  fatto  prese   in    &#13;
 considerazione dalla legge.                                              &#13;
    In  effetti, la norma denunciata dispone retroattivamente soltanto    &#13;
 per le imposte sulle plusvalenze conseguite a seguito di procedimenti    &#13;
 espropriativi o di acquisizioni coattive conseguenti  ad  occupazioni    &#13;
 di urgenza divenute illegittime, e non su quelle realizzate a seguito    &#13;
 di  cessioni  a  titolo  oneroso  estranee  a  detti  procedimenti  e    &#13;
 situazioni, alle quali si riferisce il primo comma, lettera  f),  del    &#13;
 medesimo art. 11.                                                        &#13;
    Tuttavia,    tale   diverso   trattamento   non   può   reputarsi    &#13;
 irragionevole, se si considera che, alla data della entrata in vigore    &#13;
 della legge n. 413 del 1991, le  plusvalenze  derivanti  da  cessione    &#13;
 negoziale  privatistica  erano  e  sono,  sia  pure nei limiti di cui    &#13;
 all'art. 17  del  decreto  legislativo  30  dicembre  1992,  n.  504,    &#13;
 comunque  soggette  alla  imposta  sull'incremento  di  valore  degli    &#13;
 immobili,  mentre  quelle  realizzate  a  seguito   di   procedimenti    &#13;
 espropriativi  erano  e  sono  escluse  da  tale imposizione (art. 2,    &#13;
 ultimo comma, del d.P.R. 26 ottobre 1972, n. 643). La  retroattività    &#13;
 della  norma  trova,  quindi,  la  sua  giustificazione nella assenza    &#13;
 dell'altra imposta sul plusvalore immobiliare in capo al dante causa,    &#13;
 come è testimoniato  dallo  stesso  tenore  della  norma  impugnata:    &#13;
 infatti  il  comma  9,  dell'art. 11 dispone sì retroattivamente, in    &#13;
 ordine alle somme percepite in conseguenza di atti anche volontari  o    &#13;
 provvedimenti emessi successivamente al 31 dicembre 1988, ma soltanto    &#13;
 "se  l'incremento  di  valore  non  è stato assoggettato all'imposta    &#13;
 comunale sull'incremento di valore degli immobili".                      &#13;
    La questione va, pertanto, dichiarata infondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 11, comma 9, della legge 30  dicembre  1991,    &#13;
 n.  413,  sollevata,  in  riferimento all'art. 53 della Costituzione,    &#13;
 dalla Commissione tributaria  di  primo  grado  di  Cagliari  con  la    &#13;
 ordinanza in epigrafe;                                                   &#13;
    Dichiara  non  fondata la questione di legittimità costituzionale    &#13;
 dello stesso art. 11, comma 9, sollevata, in riferimento  all'art.  3    &#13;
 della Costituzione, dalla medesima ordinanza.                            &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 gennaio 1995.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: VARI                              &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 19 gennaio 1995.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
