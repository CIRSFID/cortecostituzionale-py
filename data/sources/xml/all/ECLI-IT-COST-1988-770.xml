<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>770</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:770</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/06/1988</data_decisione>
    <data_deposito>07/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel   giudizio  promosso  con  ricorso  della  Provincia  di  Bolzano    &#13;
 notificato il 14 giugno 1978, depositato in Cancelleria il 23  giugno    &#13;
 successivo  ed  iscritto  al  n.  16  del  registro ricorsi 1978, per    &#13;
 conflitto di attribuzione sorto a seguito del  d.P.R.   14  settembre    &#13;
 1976,  n.  1144,  avente  per  oggetto:  "Istituzione  di un istituto    &#13;
 professionale  di  Stato  per  il  commercio  in  lingua  tedesca  in    &#13;
 Vipiteno";                                                               &#13;
    Udito  nell'udienza  pubblica  del  9  febbraio  1988  il  Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Udito l'Avvocato Sergio Panunzio per la Provincia di Bolzano;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  ricorso  notificato  il  14  giugno  1978  e depositato il 23    &#13;
 successivo, la Provincia autonoma di Bolzano ha proposto conflitto di    &#13;
 attribuzione  in  riferimento  al  d.P.R. 14 settembre 1976, n. 1144,    &#13;
 pubblicato per estratto sulla G.U. del 15 aprile 1978, con il  quale,    &#13;
 su proposta del Ministro della Pubblica Istruzione, di concerto con i    &#13;
 Ministri  dell'Interno,  del  Tesoro  e  dell'Industria,   è   stato    &#13;
 istituito, a decorrere dal 1° ottobre 1973, un Istituto professionale    &#13;
 di Stato per il commercio in lingua tedesca nel comune di Vipiteno.      &#13;
    La  Provincia  ricorrente  deduce che, con il decreto suddetto, è    &#13;
 stata  posta  in  essere  una  violazione  della  propria  competenza    &#13;
 concorrente  in  materia  di istruzione elementare e secondaria, come    &#13;
 determinata dalle norme  di  attuazione  dello  Statuto  speciale  in    &#13;
 materia  di  ordinamento  scolastico in provincia di Bolzano, dettate    &#13;
 dal d.P.R. 20 gennaio 1973, n. 116.                                      &#13;
    L'art.   4   di   tale  decreto,  infatti,  stabilisce  che  "alla    &#13;
 istituzione di scuole elementari e di istituti e scuole di istruzione    &#13;
 secondaria...   provvede  la  Provincia  in  base  a  piani  da  essa    &#13;
 predisposti e d'intesa con il Ministero della Pubblica Istruzione  in    &#13;
 ordine agli oneri per il personale a carico dello Stato...".             &#13;
    La Provincia ricorrente, ritenendo palese la lesione della propria    &#13;
 competenza in materia di ordinamento  scolastico,  chiede  che  venga    &#13;
 dichiarata   la   propria   competenza  a  istituire  nel  territorio    &#13;
 provinciale istituti professionali  e,  conseguentemente,  che  venga    &#13;
 annullato il decreto impugnato.                                          &#13;
    Il  Presidente del Consiglio dei Ministri non si è costituito nel    &#13;
 presente giudizio.<diritto>Considerato in diritto</diritto>La   Provincia  autonoma  di  Bolzano  ha  proposto  conflitto  di    &#13;
 attribuzione avverso il d.P.R. 14 settembre 1976,  n.  1144,  con  il    &#13;
 quale   è  stato  istituito  nel  Comune  di  Vipiteno  un  Istituto    &#13;
 professionale di Stato in lingua tedesca,  lamentando  la  violazione    &#13;
 della  propria  competenza  amministrativa  in materia di ordinamento    &#13;
 scolastico, come determinata dall'art. 9, n. 2  St.  T.A.A.  e  dalle    &#13;
 relative norme di attuazione contenute nel d.P.R. 20 gennaio 1973, n.    &#13;
 116, ed ora nel d.P.R. 10 febbraio 1983, n.  89  (Testo  unico  delle    &#13;
 norme  di  attuazione  in  materia  di  ordinamento  scolastico nella    &#13;
 Provincia di Bolzano).                                                   &#13;
    Il ricorso è fondato.                                                &#13;
    Ai  sensi  dell'art.  4  del citato d.P.R. n. 116 del 1973, spetta    &#13;
 alla Provincia Autonoma di Bolzano provvedere alla istituzione  delle    &#13;
 scuole   elementari   e   degli   istituti  e  scuole  di  istruzione    &#13;
 professionale, sulla  base  di  appositi  piani  e  d'intesa  con  il    &#13;
 Ministro della Pubblica Istruzione.                                      &#13;
    Non  vi  può  essere,  quindi, alcun dubbio, come questa Corte ha    &#13;
 già affermato con la sentenza n.  279  del  1984,  resa  in  analogo    &#13;
 giudizio,  circa  l'avvenuta  invasione  da  parte  dello Stato della    &#13;
 competenza riconosciuta  alla  provincia  ricorrente  in  materia  di    &#13;
 ordinamento scolastico.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  che  spetta  alla Provincia autonoma di Bolzano istituire    &#13;
 nel  Comune  di  Vipiteno  un  istituto  professionale  di  Stato  e,    &#13;
 conseguentemente, annulla il d.P.R. 14 settembre 1976, n. 1144.          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 giugno 1988.                               &#13;
                          Il Presidente: SAJA                             &#13;
                       Il redattore: BALDASSARRE                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 7 luglio 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
