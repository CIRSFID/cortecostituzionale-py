<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>47</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:47</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1992</data_decisione>
    <data_deposito>05/02/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 429 del codice    &#13;
 di procedura penale, promosso con ordinanza emessa il 21 maggio  1991    &#13;
 dal  Giudice  per  le  indagini  preliminari  presso  il Tribunale di    &#13;
 Lamezia Terme nel procedimento penale a carico di  Porchia  Francesco    &#13;
 Maria,  iscritta  al  n. 489 del registro ordinanze 1991 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  33,  prima   serie    &#13;
 speciale, dell'anno 1991;                                                &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 18 dicembre  1991  il  Giudice    &#13;
 relatore Ugo Spagnoli;                                                   &#13;
    Ritenuto  che  con l'ordinanza indicata in epigrafe il Giudice per    &#13;
 le  indagini  preliminari  presso  il  Tribunale  di  Lamezia  Terme,    &#13;
 muovendo  dal presupposto interpretativo che l'art. 429 del codice di    &#13;
 procedura penale non consenta al giudice della udienza preliminare di    &#13;
 dare al fatto, nel decreto che dispone il giudizio,  una  definizione    &#13;
 giuridica diversa da quella enunciata nella imputazione formulata con    &#13;
 la  richiesta di rinvio a giudizio, ovvero modificata nel corso della    &#13;
 udienza preliminare, dubita della legittimità costituzionale di tale    &#13;
 disposizione, assumendo  che  essa  contrasterebbe  con  l'art.  101,    &#13;
 secondo comma, della Costituzione, perché l'esercizio della funzione    &#13;
 giurisdizionale  ne  risulterebbe  limitato  oltre  i  termini  della    &#13;
 stretta soggezione della legge;                                          &#13;
      che il Presidente del Consiglio dei  ministri,  rappresentato  e    &#13;
 difeso  dall'Avvocatura  Generale  dello  Stato,  ha  chiesto  che la    &#13;
 questione sia dichiarata non fondata;                                    &#13;
    Considerato  che  il  giudice  rimettente  solleva  la   questione    &#13;
 muovendo,  in punto di rilevanza, dalla premessa secondo cui il fatto    &#13;
 oggetto del giudizio principale, contestato  come  concussione  (art.    &#13;
 317 cod. pen.) nell'imputazione formulata dal pubblico ministero, non    &#13;
 sarebbe  a  suo  avviso inquadrabile in tale figura di reato perché,    &#13;
 alla stregua della modifica dell'art. 357 cod. pen. introdotta con la    &#13;
 legge  26  aprile  1990,  n.  86,   l'imputato   (assistente   medico    &#13;
 ospedaliero  esercente  le  specifiche  mansioni  di  sanitario)  non    &#13;
 potrebbe essere qualificato come pubblico ufficiale;                     &#13;
      che,  peraltro,  la medesima legge n. 86 del 1990 ha modificato,    &#13;
 oltre alle nozioni di pubblico ufficiale e di incaricato di  pubblico    &#13;
 servizio  (artt.  17 e 18), anche il delitto di concussione (art. 4),    &#13;
 rendendolo addebitabile ai soggetti che rivestano tanto la prima  che    &#13;
 la seconda di tali qualifiche;                                           &#13;
      che  il  giudice  rimettente  non  ha  chiarito né quale sia la    &#13;
 diversa  qualificazione  giuridica  del  fatto  da  lui   considerata    &#13;
 appropriata,  né se nella specie ritenga di dover escludere anche la    &#13;
 qualifica di incaricato di pubblico servizio;                            &#13;
      che, non essendovi mutamento del titolo del reato  qualora  tale    &#13;
 qualificazione   venga   invece   riconosciuta,  risulta  carente  la    &#13;
 motivazione della rilevanza della questione sollevata;                   &#13;
      che, conseguentemente, la questione va dichiarata manifestamente    &#13;
 inammissibile;                                                           &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 429  del  codice  di  procedura    &#13;
 penale,  nella  parte  in  cui  non  consente al giudice dell'udienza    &#13;
 preliminare di dare al fatto una  definizione  giuridica  diversa  da    &#13;
 quella  enunciata  nella  imputazione  formulata  con la richiesta di    &#13;
 rinvio a giudizio, sollevata in  riferimento  all'art.  101,  secondo    &#13;
 comma,  della  Costituzione  dal  Giudice per le indagini preliminari    &#13;
 presso il Tribunale di Lamezia Terme  con  ordinanza  del  21  maggio    &#13;
 1991.                                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 gennaio 1992.                              &#13;
                       Il Presidente: BORZELLINO                          &#13;
                        Il redattore: SPAGNOLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 5 febbraio 1992.                         &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
