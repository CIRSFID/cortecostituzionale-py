<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>216</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:216</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia>Piero Alberto Capotosti</redattore_pronuncia>
    <data_decisione>20/05/2002</data_decisione>
    <data_deposito>23/05/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
FLICK, Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di legittimità costituzionale degli artt. 3 e 5, commi &#13;
da   1  a  11,  del  decreto  legislativo  21 dicembre  1999,  n. 517 &#13;
(Disciplina   dei   rapporti  fra  Servizio  sanitario  nazionale  ed &#13;
università,  a  norma  dell'articolo  6  della  l. 30 novembre 1998, &#13;
n. 419),  promossi  con  n. 22  ordinanze emesse il 5 luglio 2000 dal &#13;
Tribunale   amministrativo   regionale   del   Lazio,   sezione  III, &#13;
rispettivamente  iscritte ai nn. 592, da 717 a 725 e da 741 a 752 del &#13;
registro  ordinanze  2001 e pubblicate nella Gazzetta Ufficiale della &#13;
Repubblica nn. 33, 38 e 39, prima serie speciale, dell'anno 2001. &#13;
    Visti  gli  atti  di  costituzione  di S.A. ed altri, di A. A. ed &#13;
altro,  G.  L.  ed  altri,  di M. R. ed altro, F. F. ed altro e della &#13;
Regione  Toscana  nonché  gli  atti di intervento del Presidente del &#13;
Consiglio dei ministri; &#13;
    Udito  nella  camera  di  consiglio del 10 aprile 2002 il giudice &#13;
relatore Piero Alberto Capotosti. &#13;
    Ritenuto  che  il  Tribunale  amministrativo regionale del Lazio, &#13;
sezione III, con ventidue ordinanze del 5 luglio 2000 (pervenute alla &#13;
Corte  il  21 giugno,  il  20 ed il 24 agosto del 2001), nel corso di &#13;
giudizi promossi da docenti universitari delle facoltà di medicina e &#13;
chirurgia   (infra:   medici   universitari),  solleva  questione  di &#13;
legittimità  costituzionale  delle seguenti disposizioni del decreto &#13;
legislativo  21 dicembre  1999,  n. 517  (Disciplina dei rapporti fra &#13;
Servizio  sanitario nazionale ed università, a norma dell'articolo 6 &#13;
della  l.  30 novembre 1998, n. 419): art. 5, comma 8, in riferimento &#13;
agli artt. 3 e 97 della Costituzione; art. 5, comma 7, in riferimento &#13;
agli  artt. 33 e 76 della Costituzione; art. 5, commi da 1 a 6 e da 8 &#13;
a  11, nonché art. 3 quest'ultimo nella parte in cui non prevede una &#13;
partecipazione  diretta  degli organi universitari nelle scelte delle &#13;
aziende  ospedaliero-universitarie  in materia di collegamento tra le &#13;
attività  di  assistenza,  didattica  e  ricerca in riferimento agli &#13;
artt. 33 e 76 della Costituzione; &#13;
        che   le   ordinanze,   con   argomentazioni  sostanzialmente &#13;
identiche,  censurano  l'art. 5, comma 8, del d.lgs. n. 517 del 1999, &#13;
il  quale  stabilisce  un  termine perentorio entro il quale i medici &#13;
universitari  esercitano  o  rinnovano l'opzione prevista dal comma 7 &#13;
per   l'esercizio   di  attività  assistenziale  intramuraria  (c.d. &#13;
attività    assistenziale    esclusiva),    ovvero    di   attività &#13;
libero-professionale  extramuraria,  disponendo  che,  in mancanza di &#13;
comunicazione,   si  intende  effettuata  l'opzione  per  l'attività &#13;
assistenziale esclusiva; &#13;
        che,   ad  avviso  dei  rimettenti,  la  norma,  fissando  il &#13;
succitato   termine   indipendentemente  dalla  individuazione  delle &#13;
strutture  destinate  allo  svolgimento  dell'attività assistenziale &#13;
intramuraria,  si  porrebbe  in  contrasto con gli artt. 3 e 97 della &#13;
Costituzione,   in   quanto   la   loro   preventiva  identificazione &#13;
configurerebbe  un presupposto dell'opzione e, proprio per questo, la &#13;
disposizione  inciderebbe  negativamente  sulla  compenetrazione  tra &#13;
attività   assistenziale   ed  attività  didattico-scientifica,  in &#13;
violazione    dei    principi    di    coerenza    e   ragionevolezza &#13;
dell'ordinamento, nonché di buon andamento dell'amministrazione; &#13;
        che, secondo il Tribunale amministrativo regionale, l'art. 5, &#13;
comma 7, del d.lgs. n. 517 del 1999 e le disposizioni ad esso sottese &#13;
e  connesse - ossia i commi da 1 a 6 e da 8 ad 11 - nonché l'art. 3, &#13;
nella   parte  riguardante  l'organizzazione  interna  delle  aziende &#13;
ospedaliero-universitarie,   violerebbe   gli  artt. 33  e  76  della &#13;
Costituzione; &#13;
        che,  in  particolare,  la  configurazione  dell'opzione  per &#13;
l'attività    assistenziale    esclusiva    quale    requisito   per &#13;
l'attribuzione  degli  incarichi di direzione dei programmi di cui al &#13;
comma 4 della norma impugnata pregiudicherebbe la compenetrazione tra &#13;
attività sanitaria assistenziale ed attività didattica e di ricerca &#13;
scientifica,   assoggettando  l'attività  assistenziale  svolta  dal &#13;
medico  universitario alle determinazioni organizzative del direttore &#13;
generale  dell'azienda  ospedaliero-universitaria,  in violazione del &#13;
principio dell'autonomia universitaria; &#13;
        che,  ad  avviso del Tribunale amministrativo regionale, agli &#13;
organi  dell'università sarebbero stati attribuiti compiti marginali &#13;
nel  coordinamento  degli  interessi  concernenti l'insegnamento e la &#13;
ricerca  scientifica,  tenuto  conto  sia  dei  poteri  attribuiti al &#13;
direttore del dipartimento, sia della circostanza che questi risponde &#13;
della  programmazione  e  della  gestione  delle risorse al direttore &#13;
generale  e  sarebbe tenuto a privilegiare le esigenze dell'attività &#13;
assistenziale   rispetto   a   quelle   dell'attività   didattica  e &#13;
scientifica,  così  da  non garantire lo svolgimento delle attività &#13;
assistenziali  "funzionali  alle  esigenze  della  didattica  e della &#13;
ricerca"  in  violazione dell'art. 6, comma 1, lettera b) della legge &#13;
30 novembre 1998, n. 419; &#13;
        che,  secondo  i  giudici  a quibus "la normativa delegata in &#13;
materia  di  opzione"  (ossia  l'art. 5,  commi da 1 a 6 e da 8 a 11, &#13;
nonché   l'art. 3  del  d.lgs.  n. 517  del  1999  "in  parte  qua") &#13;
violerebbe  gli  artt. 33 e 76 della Costituzione, dal momento che il &#13;
divieto  di  attribuire  al  medico universitario, il quale non abbia &#13;
scelto   l'attività  assistenziale  esclusiva,  la  direzione  delle &#13;
strutture  e  dei  programmi  finalizzati alla integrazione di queste &#13;
attività non garantirebbe "la coerenza fra l'attività assistenziale &#13;
e  le  esigenze  della  formazione e della ricerca" (art. 6, comma 1, &#13;
lettere  b  e  c  della  legge n. 419 del 1998), modificando lo stato &#13;
giuridico  del  personale universitario, in violazione dei principi e &#13;
dei criteri direttivi della legge-delega; &#13;
        che il Presidente del Consiglio dei ministri, rappresentato e &#13;
difeso  dall'Avvocatura generale dello Stato, è intervenuto in tutti &#13;
i  giudizi  fatta  eccezione  per  quello  instaurato con l'ordinanza &#13;
iscritta  al n. 750 del registro ordinanze del 2001 con separati atti &#13;
di  contenuto sostanzialmente coincidente, chiedendo che le questioni &#13;
siano dichiarate inammissibili e comunque infondate; &#13;
        che,  ad  avviso  della  difesa erariale, il d.lgs. 28 luglio &#13;
2000,  n. 254,  attribuendo  ai  medici  universitari  la facoltà di &#13;
esercitare  l'attività  libero-professionale  intramuraria in regime &#13;
ambulatoriale  presso i propri studi, neicasi di carenza di strutture &#13;
e      di      spazi     idonei     all'interno     delle     aziende &#13;
ospedaliero-universitarie, inciderebbe sulla fondatezza delle censure &#13;
riferite all'art. 5, comma 8, del d.lgs. n. 517 del 1999; &#13;
        che,   secondo  l'interveniente,  detta  norma,  fissando  un &#13;
termine perentorio per l'esercizio dell'opzione in esame, non sarebbe &#13;
legata  da  alcun  nesso  con  il  comma  7,  e,  comunque,  i medici &#13;
universitari,  allorquando  effettuano  la  scelta,  sono consapevoli &#13;
degli effetti che ne derivano; &#13;
        che,   ad   avviso   dell'Avvocatura,   le  censure  riferite &#13;
all'art. 5,  comma  7,  cit.,  ed  alle disposizioni ad esso sottese, &#13;
sarebbero  infondate,  in  quanto  gli  incarichi  di  direzione  dei &#13;
programmi  del comma 4 sono stati ragionevolmente riservati ai medici &#13;
universitari  i  quali,  scegliendo il rapporto esclusivo, assicurano &#13;
piena  disponibilità  per la loro realizzazione, ed inoltre le norme &#13;
censurate  non  violerebbero  il  principio  di  compenetrazione  tra &#13;
attività  assistenziale ed attività didattica e di ricerca, poiché &#13;
i   medici  universitari  che  scelgono  il  rapporto  non  esclusivo &#13;
continuano  a svolgere l'attività di ricerca e didattica strumentale &#13;
rispetto a quella assistenziale; &#13;
        che,   secondo   la  difesa  erariale,  le  censure  riferite &#13;
all'art. 76  della  Costituzione  sarebbero  infondate,  dato  che la &#13;
legge-delega ha inteso rafforzare la collaborazione tra università e &#13;
Servizio sanitario nazionale; &#13;
        che  nei  giudizi  instaurati  con le ordinanze di rimessione &#13;
iscritte  ai  numeri  592, 722, 724, 749 e 752 del registro ordinanze &#13;
dell'anno   2001,  si  sono  costituiti  i  ricorrenti  nei  processi &#13;
principali,   facendo  sostanzialmente  proprie  le  conclusioni  del &#13;
Tribunale  amministrativo  regionale,  nonché  deducendo, con alcuni &#13;
atti, il contrasto delle norme impugnate con parametri costituzionali &#13;
ulteriori rispetto a quelli indicati dalle ordinanze di rimessione; &#13;
        che  nel  giudizio promosso dall'ordinanza iscritta al numero &#13;
718  del  registro ordinanze dell'anno 2001 si è altresì costituita &#13;
la  Regione  Toscana  parte  nel  processo  a  quo  chiedendo  che le &#13;
questioni siano dichiarate inammissibili e comunque infondate. &#13;
    Considerato  che l'identità delle norme impugnate, delle censure &#13;
proposte   e   dei  parametri  costituzionali  invocati,  nonché  la &#13;
coincidenza delle argomentazioni svolte nelle ordinanze di rimessione &#13;
rendono opportuna la riunione dei giudizi; &#13;
        che,  nel decidere identiche questioni sollevate dallo stesso &#13;
Tribunale  amministrativo  regionale  del  Lazio,  questa  Corte, con &#13;
ordinanza  n. 394  del  2001, ha affermato che gli atti legislativi e &#13;
regolamentari,  nonché la sentenza n. 71 del 2001, sopravvenuti alle &#13;
ordinanze  di  rimessione,  hanno  influito  sul  complessivo  quadro &#13;
normativo di riferimento nel quale si inscrivono i molteplici profili &#13;
delle   questioni   di   legittimità   costituzionale,  richiedendo, &#13;
conseguentemente,  un  nuovo  esame da parte dei giudici a quibus dei &#13;
termini delle questioni e della loro perdurante rilevanza; &#13;
        che  le  argomentazioni  svolte in detta ordinanza conservano &#13;
validità  anche  in relazione ai provvedimenti di rimessione oggetto &#13;
del presente giudizio; &#13;
        che,  alla  luce delle modificazioni sopra indicate, gli atti &#13;
devono  essere  restituiti  ai  rimettenti, affinché procedano ad un &#13;
nuovo esame della perdurante rilevanza delle questioni.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    ordina  la  restituzione  degli  atti al Tribunale amministrativo &#13;
regionale del Lazio, sezione III. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 20 maggio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                       Il redattore: Capotosti &#13;
                       Il cancelliere: Di Paola &#13;
    Depositata in Cancelleria il 23 maggio 2002. &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
