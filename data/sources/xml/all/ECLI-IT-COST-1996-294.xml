<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>294</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:294</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia>Riccardo Chieppa</redattore_pronuncia>
    <data_decisione>18/07/1996</data_decisione>
    <data_deposito>22/07/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 22 della legge    &#13;
 28 febbraio 1985, n. 47 (Norme in materia di controllo dell'attività    &#13;
 urbanistico-edilizia, sanzioni,  recupero  e  sanatoria  delle  opere    &#13;
 edilizie),  promosso  con  ordinanza  emessa  il  25  maggio 1995 dal    &#13;
 pretore di Foggia nel procedimento penale a carico di Moffa  Michele,    &#13;
 iscritta  al  n.  526  del registro ordinanze 1995 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  39,  prima  serie  speciale,    &#13;
 dell'anno 1995;                                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio  del  17  aprile  1996  il  giudice    &#13;
 relatore Riccardo Chieppa;                                               &#13;
   Ritenuto che Michele Moffa, condannato, con sentenza emessa in data    &#13;
 2  marzo  1992,  alla  pena  di  dieci  giorni  di  arresto e di lire    &#13;
 ottomilioni  di  ammenda  per  una  serie  di  violazioni   edilizie,    &#13;
 unificate  insieme ad altri reati di altra natura, ai sensi dell'art.    &#13;
 81 cpv., cod. pen., otteneva, in  data  14  febbraio  1994,  dopo  il    &#13;
 passaggio  in  giudicato  della  sentenza, la concessione edilizia in    &#13;
 sanatoria e richiedeva al pretore di Foggia, in qualità  di  giudice    &#13;
 dell'esecuzione,  la  pronuncia  di  estinzione  dei  reati  a  norma    &#13;
 dell'art. 22 della legge n. 47 del 1985;                                 &#13;
     che il pretore di Foggia, con ordinanza emessa il 25 maggio 1995,    &#13;
 rilevato che l'art. 22 citato individua  nel  rilascio  in  sanatoria    &#13;
 della   concessione   edilizia   una   causa   estintiva   dei  reati    &#13;
 contravvenzionali previsti  dalle  norme  urbanistiche  vigenti,  cui    &#13;
 viene  attribuita  rilevanza  giuridica prima che intervenga sentenza    &#13;
 irrevocabile di condanna,  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale dello stesso art. 22 nella parte in cui non prevede la    &#13;
 estinzione  dei  reati anche in caso di condanna con sentenza passata    &#13;
 in giudicato, facendone cessare l'esecuzione e gli effetti penali;       &#13;
     che la disposizione de qua si porrebbe in contrasto con gli artt.    &#13;
 3 e 24 della  Costituzione  sotto  il  profilo  della  disparità  di    &#13;
 trattamento  e  della restrizione del diritto di difesa rispetto alle    &#13;
 ipotesi in cui le cause di estinzione del  reato  spiegano  efficacia    &#13;
 anche  nei  confronti della cosa giudicata, nonostante la eadem ratio    &#13;
 della causa estintiva, collegata in entrambi i casi ad un evento  che    &#13;
 fa venir meno il disvalore giuridico della fattispecie criminosa;        &#13;
     che  nell'ordinanza  di  rimessione  si afferma, sotto il profilo    &#13;
 della rilevanza, che, se è vero che  l'estinzione  investe  le  sole    &#13;
 contravvenzioni urbanistiche e non anche i concorrenti reati di altra    &#13;
 natura  per  i  quali  il Moffa aveva riportato la condanna (quali le    &#13;
 violazioni di cui all'art. 650 cod. pen. e della legge  antisismica),    &#13;
 e  che  il giudice, con la sentenza in esame, aveva unificato tutti i    &#13;
 reati contestati sotto il vincolo della continuazione,  è  pur  vero    &#13;
 che,  ai fini dell'applicazione della causa estintiva, non sussistono    &#13;
 impedimenti normativi al frazionamento del reato continuato nei  vari    &#13;
 episodi criminosi;                                                       &#13;
     che  se  poi  ciò fosse ritenuto inammissibile in presenza della    &#13;
 cosa giudicata,  si  porrebbe,  secondo  il  giudice  rimettente,  il    &#13;
 problema   della   legittimità   costituzionale   della   disciplina    &#13;
 impeditiva;                                                              &#13;
     che  nel  giudizio  ha  spiegato  intervento  il  Presidente  del    &#13;
 Consiglio   dei  Ministri,  rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale dello Stato, che ha concluso per la  inammissibilità  della    &#13;
 questione,  versandosi  in  materia  riservata  alla discrezionalità    &#13;
 legislativa e, comunque, per la infondatezza;                            &#13;
   Considerato che il legislatore, con l'art. 22, terzo  comma,  della    &#13;
 legge  28  febbraio  1985, n. 47, ha adottato  una formulazione degli    &#13;
 effetti estintivi, sui reati contravvenzionali previsti  dalle  norme    &#13;
 urbanistiche  vigenti,    derivanti dal rilascio delle concessioni in    &#13;
 sanatoria così detta ordinaria (accertamento  di  conformità:  art.    &#13;
 13 della legge 28 febbraio 1985, n. 47), che non prevede, in mancanza    &#13;
 di  espressa dizione (che coinvolga l'esecuzione e gli effetti penali    &#13;
 delle  sentenze   irrevocabili   di   condanna   già   pronunciate),    &#13;
 l'estensione degli stessi effetti sul giudicato già formatosi (cfr.,    &#13;
 per  fattispecie analoga riferita agli artt. 38, primo e terzo comma,    &#13;
 e 44 della legge n. 47 del 1985, la sentenza n. 369 del 1988);           &#13;
     che la anzidetta norma, con questa interpretazione, adottata  dal    &#13;
 giudice  a  quo  conformemente  al diritto vivente, certamente non si    &#13;
 pone  in  contrasto  con   il   principio   di   eguaglianza   e   di    &#13;
 ragionevolezza,  poiché  la  sanatoria  è intervenuta in un momento    &#13;
 successivo al passaggio in giudicato della sentenza di  condanna,  la    &#13;
 cui  definitività  si  è  realizzata  quando  l'imputato si trovava    &#13;
 ancora in situazione di  illegalità  (formale)  per  avere  compiuto    &#13;
 opere edilizie abusive senza avere il titolo abilitativo;                &#13;
     che  rientra  nella  discrezionalità  del legislatore, una volta    &#13;
 individuata una causa estintiva del reato, fissare, in relazione allo    &#13;
 status  dell'azione  penale,  i  limiti  temporali  di  questa  causa    &#13;
 estintiva,  che  deriva,  si  noti,  da  una  iniziativa dello stesso    &#13;
 responsabile dell'abuso (richiesta di concessione in sanatoria il cui    &#13;
 rilascio è subordinato al pagamento a titolo  di  oblazione  di  una    &#13;
 misura  maggiorata  del contributo di concessione, perfino in caso di    &#13;
 concessione gratuita:  art. 13, primo e terzo comma, della  legge  n.    &#13;
 47 del 1985);                                                            &#13;
     che,  d'altro  canto,  l'art.  22  della legge n. 47 del 1985 nel    &#13;
 testo originario, dandosi carico della natura formale dell'infrazione    &#13;
 (mancanza del titolo abilitativo in  presenza  di  piena  conformità    &#13;
 delle  opere  alla programmazione edilizia), prevedeva la sospensione    &#13;
 dell'azione penale, interpretata temporalmente  fino  all'esaurimento    &#13;
 del  procedimento  di  sanatoria nella fase amministrativa (fino alla    &#13;
 pronuncia del comune), mentre con la modifica  da  ultima  introdotta    &#13;
 con  il  d.-l.  25  maggio  1996,  n.  285  (art. 8, ottavo comma) la    &#13;
 sospensione è stata estesa fino alla  definizione  del  giudizio  di    &#13;
 impugnazione   dell'eventuale   rifiuto  della  sanatoria  avanti  al    &#13;
 tribunale amministrativo regionale, così consentendosi  un  maggiore    &#13;
 spazio  all'interessato  per  ottenere  la  sanatoria e far valere le    &#13;
 proprie  ragioni  di  tutela  avanti  al   tribunale   amministrativo    &#13;
 regionale  e  realizzare,  in  caso  di accoglimento e rilascio della    &#13;
 sanatoria, anche l'effetto estintivo;                                    &#13;
     che con tale meccanismo si realizza anche un  incentivo  per  una    &#13;
 sollecita definizione delle iniziative di sanatoria;                     &#13;
     che  la fattispecie è analoga a quella presa in considerazione a    &#13;
 proposito del condono-sanatoria previsto dai Capi IV e V della  legge    &#13;
 n.  47  del  1985  e con riferimento agli artt. 38 e 44, della stessa    &#13;
 legge, in relazione alla quale  è  stato  affermato  "che  non  può    &#13;
 ritenersi   "irrazionale"   il  non  avere  previsto,  a  favore  dei    &#13;
 richiedenti la concessione in sanatoria già condannati con  sentenza    &#13;
 definitiva,  l'estinzione della esecuzione della pena. D'altro canto,    &#13;
 situazioni diverse sono, certamente, quelle nelle quali si trovano da    &#13;
 una parte i soggetti  imputati,  durante  il  procedimento  penale  e    &#13;
 dall'altra   i   soggetti   condannati,   a  seguito  della  sentenza    &#13;
 definitiva:   le predette situazioni ben  possono,  pertanto,  essere    &#13;
 diversamente disciplinate dalla legge" (sentenza n. 369 del 1988);       &#13;
     che,  infine,  nessun contrasto, come delineato dal giudice a quo    &#13;
 può configurarsi con l'art. 24  della  Costituzione,  in  quanto  la    &#13;
 tutela  giurisdizionale non viene affatto lesa, mentre vi è solo una    &#13;
 non previsione di effetti  rispetto  a  situazioni  definite  con  il    &#13;
 giudicato penale;                                                        &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  22  della  legge  28  febbraio 1985, n. 47    &#13;
 (Norme in materia di controllo  dell'attività  urbanistico-edilizia,    &#13;
 sanzioni,  recupero  e sanatoria delle opere edilizie), sollevata, in    &#13;
 riferimento agli artt. 3 e 24  della  Costituzione,  dal  pretore  di    &#13;
 Foggia con l'ordinanza indicata in epigrafe.                             &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1996.                               &#13;
                          Il Presidente: Ferri                            &#13;
                         Il redattore: Chieppa                            &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 22 luglio 1996.                           &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
