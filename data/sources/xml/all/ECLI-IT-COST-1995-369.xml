<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>369</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:369</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/07/1995</data_decisione>
    <data_deposito>24/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 15 della legge    &#13;
 11 ottobre 1990, n.  290  (Modifiche  e  integrazioni  alla  legge  3    &#13;
 gennaio  1981,  n.  6, concernente norme in materia di previdenza per    &#13;
 gli ingegneri e  architetti),  recte:  dell'art.  20  della  legge  3    &#13;
 gennaio  1981, n. 6 (Norme in materia di previdenza per gli ingegneri    &#13;
 e gli architetti), promosso con ordinanza emessa  l'8  novembre  1994    &#13;
 dal  Pretore di Livorno nel procedimento civile vertente tra Bernacca    &#13;
 Ferdinando e la Cassa nazionale di previdenza ed assistenza  per  gli    &#13;
 ingegneri e gli architetti, iscritta al n. 124 del registro ordinanze    &#13;
 1995  e  pubblicata  nella Gazzetta Ufficiale della Repubblica n. 11,    &#13;
 prima serie speciale, dell'anno 1995;                                    &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 28 giugno 1995 il Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto  che,  nel  corso  di   un   giudizio   civile   promosso    &#13;
 dall'architetto  Ferdinando  Bernacca  contro  la  Cassa nazionale di    &#13;
 previdenza e assistenza per gli ingegneri e gli architetti,  il  Pretore di Livorno, con ordinanza dell'8 novembre 1994, ha sollevato, in    &#13;
 riferimento  agli  artt.  3  e  38  della  Costituzione, questione di    &#13;
 legittimità costituzionale dell'art. 20 della legge 3 gennaio  1981,    &#13;
 n.  6,  come  modificato dall'art. 15 della legge 11 ottobre 1990, n.    &#13;
 290, della parte (primo comma) in cui prevede che coloro che  cessano    &#13;
 dall'iscrizione   alla   Cassa   senza  avere  maturato  i  requisiti    &#13;
 assicurativi per il diritto alla pensione  non  possono  ottenere  il    &#13;
 rimborso   dei   contributi   versati   prima   del   compimento  del    &#13;
 sessantacinquesimo anno di età;                                         &#13;
      che, ad avviso del giudice rimettente, il limite cui dalla norma    &#13;
 impugnata  è  assoggettato  il  diritto  al  rimborso  comporta   un    &#13;
 sacrificio    eccessivo    dell'interesse    del   singolo   rispetto    &#13;
 all'interesse collettivo  della  categoria,  specialmente  nel  caso,    &#13;
 ricorrente  nella  specie,  in  cui l'ex iscritto alla Cassa, essendo    &#13;
 titolare di pensione  a  carico  dello  Stato,  non  sia  ammesso  al    &#13;
 beneficio previsto dall'art. 6 della legge n. 290 del 1990, avendo il    &#13;
 d.lgs.  30 giugno 1994, n. 479, istitutivo dell'Istituto nazionale di    &#13;
 previdenza per i dipendenti dell'amministrazione  pubblica  (INPDAP),    &#13;
 rinviato  a successivi provvedimenti di legge l'individuazione di una    &#13;
 gestione autonoma per  i  trattamenti  pensionistici  dei  dipendenti    &#13;
 statali,  che  dovrebbe consentire anche ai pensionati versanti nella    &#13;
 situazione del ricorrente di ottenere la ricongiunzione  contributiva    &#13;
 presso  l'ente erogatore della pensione ai fini della liquidazione di    &#13;
 un supplemento;                                                          &#13;
      che la  violazione  dei  parametri  costituzionali  invocati  è    &#13;
 argomentata  anche dal confronto con gli ordinamenti previdenziali di    &#13;
 altre  categorie  professionali,  nei  quali  la   restituzione   dei    &#13;
 contributi  è contestuale alla cancellazione dall'ente di previdenza    &#13;
 (art. 21 della legge 20 settembre 1980, n. 576, per gli avvocati e  i    &#13;
 procuratori legali; art. 21 della legge 29 gennaio 1986, n. 21, per i    &#13;
 dottori  commercialisti;  art.  23  della  legge 23 novembre 1971, n.    &#13;
 1100, per i consulenti del lavoro), nonché dal secondo  comma  dello    &#13;
 stesso  art.  20 della legge n. 6 del 1981 (modificato dalla legge n.    &#13;
 290 del 1990), che in caso  di  cessazione  dell'iscritto  per  morte    &#13;
 prevede  l'immediata  restituzione  dei  contributi ai superstiti non    &#13;
 aventi titolo a pensione indiretta;                                      &#13;
      che  nel  giudizio  davanti   alla   Corte   costituzionale   è    &#13;
 intervenuto  il  Presidente del Consiglio dei ministri, rappresentato    &#13;
 dall'Avvocatura  dello  Stato,  chiedendo  che   la   questione   sia    &#13;
 dichiarata inammissibile o infondata;                                    &#13;
    Considerato   che   questa   Corte  ha  già  avuto  occasione  di    &#13;
 pronunciarsi sulla questione nella sentenza n. 450 del 1993,  che  ha    &#13;
 giudicato  non  irrazionali i criteri più restrittivi con cui l'art.    &#13;
 15 della legge n. 290 del 1990 ha ridefinito il  contemperamento  tra    &#13;
 il  principio di solidarietà di gruppo e l'interesse individuale dei    &#13;
 singoli  al  rimborso  dei   contributi   in   caso   di   cessazione    &#13;
 dall'iscrizione  alla  Cassa  senza  avere  maturato  i requisiti del    &#13;
 diritto alla pensione;                                                   &#13;
      che la stessa  sentenza  ha  precisato  che  la  ratio  di  tale    &#13;
 giudizio,  per  quanto  riguarda  i liberi professionisti titolari di    &#13;
 pensione a carico di altro ente previdenziale, è indipendente  dalla    &#13;
 possibilità  di  conseguire  un  supplemento  di  pensione  mediante    &#13;
 ricongiunzione  contributiva  presso  l'ente  erogatore,   ai   sensi    &#13;
 dell'art. 6 della legge n. 290 del 1990;                                 &#13;
      che   comunque,  dopo  il  trasferimento  della  gestione  delle    &#13;
 pensioni statali dal Ministero  del  tesoro  a  un  ente  parastatale    &#13;
 (INPDAP),  non  dovrebbe  tardare  un  provvedimento  che  rimuova la    &#13;
 discriminazione dei pensionati statali contenuta nel citato art. 6;      &#13;
      che, per giurisprudenza costante di questa Corte,  ai  fini  del    &#13;
 principio  di  eguaglianza  non  sono  producenti confronti con altri    &#13;
 sistemi previdenziali, dovendosi  peraltro  rilevare  che,  a  fronte    &#13;
 degli  esempi  citati  dal  giudice  rimettente  (l'ultimo  dei quali    &#13;
 superato), sono più  numerosi  gli  ordinamenti  di  previdenza  per    &#13;
 professionisti  portanti una norma identica a quella denunciata (cfr.    &#13;
 art. 6 della legge 4 agosto 1990, n. 236, per  i  geometri;  art.  23    &#13;
 della  legge  12 aprile 1991, n. 136, per i veterinari; art. 21 della    &#13;
 legge  5  agosto  1991,  n. 249, per i consulenti del lavoro; art. 23    &#13;
 della legge 30 dicembre 1991, n.  414,  per  i  ragionieri  e  periti    &#13;
 commerciali);                                                            &#13;
      che nessun argomento può trarsi dal secondo comma dell'art. 20,    &#13;
 nel  testo  sostituito dall'art. 15 della legge del 1990, il criterio    &#13;
 dell'età non essendo evidentemente applicabile in caso di cessazione    &#13;
 dell'iscrizione alla Cassa per morte: in questo caso  la  restrizione    &#13;
 del  diritto  al rimborso dei contributi - precedentemente attribuito    &#13;
 agli eredi - è attuata dal nuovo  testo  limitandolo  ai  superstiti    &#13;
 indicati dall'art. 7;                                                    &#13;
      che il riferimento all'art. 38 Cost. non è pertinente, la norma    &#13;
 impugnata  avendo  precisamente una funzione di tutela dei livelli di    &#13;
 finanziamento   del    sistema    previdenziale    della    categoria    &#13;
 professionale.                                                           &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art. 20, comma 1, della legge 3 gennaio 1981, n.    &#13;
 6  (Norme  in  materia  di  previdenza  per  gli  ingegneri   e   gli    &#13;
 architetti),  come  sostituito  dall'art.  15  della legge 11 ottobre    &#13;
 1990, n. 290 (Modifiche e integrazioni alla legge 3 gennaio 1981,  n.    &#13;
 6,  concernente  norme  in  materia di previdenza per gli ingegneri e    &#13;
 architetti), sollevata, in  riferimento  agli  artt.  3  e  38  della    &#13;
 Costituzione, dal Pretore di Livorno con l'ordinanza in epigrafe.        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 luglio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 24 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
