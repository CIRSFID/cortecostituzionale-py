<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>37</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:37</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/02/1975</data_decisione>
    <data_deposito>25/02/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 63, primo  &#13;
 comma, della legge 10 agosto 1950, n.648, e dell'art.51,  primo  comma,  &#13;
 della  legge  18  marzo  1968,  n.313 (Riordinamento della legislazione  &#13;
 pensionistica di guerra), promosso con ordinanza emessa il 28  febbraio  &#13;
 1973  dalla  Corte  dei  conti  -  sezione III pensioni di guerra - sul  &#13;
 ricorso di Rizzi Giuseppa contro il Ministero del tesoro,  iscritta  al  &#13;
 n.    250  del  registro  ordinanze  1973  e  pubblicata nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 205 dell'8 agosto 1973.                    &#13;
     Udito nella camera di consiglio del 19  dicembre  1974  il  Giudice  &#13;
 relatore Giovanni Battista Benedetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     In data 13 aprile 1962 Rizzi Giuseppa avanzava domanda al Ministero  &#13;
 del  tesoro  intesa  ad  ottenere la pensione indiretta di guerra quale  &#13;
 orfana maggiorenne inabile  a  proficuo  lavoro  della  madre  Borgazzi  &#13;
 Celeste,  deceduta  il  13  maggio 1944 in un bombardamento aereo. Tale  &#13;
 domanda  veniva,  però,  respinta  sul  rilievo che l'inabilità della  &#13;
 richiedente era insorta in data successiva  a  quella  di  morte  della  &#13;
 madre.  Ricorreva  allora  la  Rizzi alla Corte dei conti la quale, con  &#13;
 ordinanza 28 febbraio 1973 emessa in sede  di  esame  del  ricorso,  ha  &#13;
 sollevato eccezione di incostituzionalità degli artt. 63, comma primo,  &#13;
 della  legge  10 agosto 1950, n. 648, e 51, comma primo, della legge 18  &#13;
 marzo 1968, n. 313, nella parte in cui - prescrivendo che  l'inabilità  &#13;
 deve sussistere "prima di aver raggiunto la maggiore età o prima della  &#13;
 data di cessazione del diritto a pensione da parte del genitore" - essi  &#13;
 escludono  dalla  pensione  indiretta  di guerra gli orfani maggiorenni  &#13;
 divenuti inabili in epoca successiva alla morte del genitore.            &#13;
     Le disposizioni denunciate contrasterebbero con l'art. 3 Cost.  per  &#13;
 la  disparità  di  trattamento  cui  darebbero  luogo  sia rispetto ai  &#13;
 genitori o allevatori del dante causa (per i quali la legge non pone la  &#13;
 medesima limitazione  relativamente  all'insorgenza  dell'inabilità  -  &#13;
 artt. 71 della legge n. 648 del 1950 e 64 della legge n. 313 del 1968),  &#13;
 sia  nell'ambito  della  stessa categoria degli orfani inabili giacché  &#13;
 non  sussiste  diversità  di  situazione  che  possa  giustificare  un  &#13;
 differente  trattamento  tra orfani che divengono inabili dopo la morte  &#13;
 del dante causa e  orfani  la  cui  inabilità  già  esista  all'epoca  &#13;
 predetta.                                                                &#13;
     Nel giudizio dinanzi a questa Corte nessuno si è costituito.<diritto>Considerato in diritto</diritto>:                          &#13;
     La   questione   di   legittimità   costituzionale   proposta  con  &#13;
 l'ordinanza indicata in epigrafe è fondata.                             &#13;
     Le norme denunciate (art. 63, comma primo, della  legge  10  agosto  &#13;
 1950,  n.  648, e corrispondente art. 51, comma primo, della successiva  &#13;
 legge 18 marzo 1968, n. 313) disciplinano il trattamento  pensionistico  &#13;
 degli  orfani di guerra maggiorenni disponendo che essi hanno diritto a  &#13;
 pensione solo se siano divenuti "inabili a  qualsiasi  proficuo  lavoro  &#13;
 prima  di  aver  raggiunto  la maggiore età oppure prima della data di  &#13;
 cessazione del diritto da parte del genitore". La  pensione,  pertanto,  &#13;
 non  spetta  agli  orfani maggiorenni che siano divenuti inabili dopo i  &#13;
 citati eventi.                                                           &#13;
     La limitazione stabilita dalle citate disposizioni non è certo  in  &#13;
 armonia  con  le altre norme della legislazione pensionistica di guerra  &#13;
 che, per evidenti ragioni di ordine naturale ed etico, assicurano  agli  &#13;
 orfani  di  guerra  -  quali  discendenti  immediati  del de cuius - un  &#13;
 trattamento di particolare favore,  anteponendoli  a  tutti  gli  altri  &#13;
 parenti  (genitori,  assimilati e collaterali) nell'ordine di vocazione  &#13;
 al diritto di pensione, non richiedendo per essi la condizione  di  uno  &#13;
 stato di bisogno e assicurando loro una pensione di maggior consistenza  &#13;
 economica.                                                               &#13;
     Ma  a  parte  ciò  è fuor di dubbio che le impugnate disposizioni  &#13;
 pongono in essere una diversità di trattamento rispetto ai genitori  e  &#13;
 allevatori ed attuano altresì una ingiusta discriminazione nell'ambito  &#13;
 della stessa categoria degli orfani inabili.                             &#13;
     Sotto  il  primo aspetto è sufficiente rilevare che limitazioni di  &#13;
 ordine temporale in ordine alla insorgenza dell'inabilità  a  proficuo  &#13;
 lavoro  non  sono  previste  dalla  legislazione  pensionistica  per  i  &#13;
 genitori ed  assimilati  (allevatore,  patrigno  e  matrigna).  Per  la  &#13;
 concessione  della  pensione  indiretta  al  padre  o all'assimilato è  &#13;
 difatti richiesto che essi abbiano compiuto il 58 anno di età o  siano  &#13;
 comunque inabili, mentre per la madre, allevatrice o matrigna, non sono  &#13;
 prescritte  condizioni  di età o inabilità ma è sufficiente lo stato  &#13;
 di vedovanza o quello di separazione legale dal marito senza diritto ad  &#13;
 alimenti (artt. 71 legge 648 e 64 legge 313).                            &#13;
     Nell'ambito  della  stessa  categoria  degli  orfani   inabili   la  &#13;
 distinzione  operata dalle norme in esame tra orfani che siano divenuti  &#13;
 inabili ad una certa data (prima della maggiore età oppure prima della  &#13;
 morte del genitore) ed orfani la  cui  inabilità  sia  successivamente  &#13;
 insorta,  è  fonte  di  discriminazione  di situazioni che sono invece  &#13;
 oggettivamente identiche. Giova ricordare che  il  diritto  a  pensione  &#13;
 dell'orfano  maggiorenne  è direttamente collegato all'evento, incerto  &#13;
 nel tempo, della morte del genitore titolare del diritto pensionistico;  &#13;
 si può quindi verificare il caso che il  genitore  pensionato  viva  a  &#13;
 lungo  e  che  alla  sua morte la pensione spetti ad un orfano divenuto  &#13;
 inabile anche in età avanzata; per contro la pensione non può  essere  &#13;
 concessa   a   quell'orfano  divenuto  inabile  anche  subito  dopo  il  &#13;
 raggiungimento della maggiore età  che  abbia  prematuramente  perduto  &#13;
 entrambi i genitori.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 63, comma primo,  &#13;
 della   legge   10  agosto  1950,  n.  648,  sul  "Riordinamento  delle  &#13;
 disposizioni sulle pensioni di guerra", e del corrispondente  art.  51,  &#13;
 comma   primo,   della   successiva   legge  18  marzo  1968,  n.  313,  &#13;
 limitatamente alla parte in cui subordinano il  diritto  alla  pensione  &#13;
 indiretta  di  guerra  dei  figli  e  delle figlie maggiorenni comunque  &#13;
 inabili a qualsiasi proficuo lavoro alla condizione che siano  divenuti  &#13;
 tali  prima  di aver raggiunto la maggiore età oppure prima della data  &#13;
 di cessazione del diritto del genitore.                                  &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 20 febbraio 1975.       &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI  - VEZIO CRISAFULLI - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI-   EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO.                               &#13;
                                   ARDUINO SALUSTRI Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
