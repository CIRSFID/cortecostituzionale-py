<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>40</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:40</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Romano Vaccarella</relatore_pronuncia>
    <redattore_pronuncia>Romano Vaccarella</redattore_pronuncia>
    <data_decisione>20/01/2004</data_decisione>
    <data_deposito>26/01/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito del corretto uso del potere di decidere sulla sussistenza dei presupposti di applicabilità dell'art. 68, primo comma, della Costituzione, come esercitato dalla Camera dei deputati con la delibera adottata nella seduta del 9 novembre 1999 relativamente al giudizio penale pendente davanti a questo Tribunale nei confronti dell'on. Vittorio Sgarbi, promosso dal Tribunale di Cosenza - seconda sezione penale, con ricorso depositato il 5 dicembre 2002 ed iscritto al n. 230 del registro ammissibilità conflitti. &#13;
      Udito nella camera di consiglio del 12 marzo 2003 il Giudice relatore Romano Vaccarella. &#13;
      Ritenuto che con "ordinanza" del 1° luglio 2002, pervenuta il 5 dicembre 2002 nella cancelleria di questa Corte a mezzo del servizio postale, il Tribunale di Cosenza - seconda sezione penale - in composizione collegiale, ha sollevato conflitto di attribuzione fra poteri dello Stato nei confronti della Camera dei deputati, in relazione alla deliberazione, adottata nella seduta del 9 novembre 1999 (doc. IV-ter, n. 36/R), con la quale è stato dichiarato che i fatti per i quali è in corso procedimento penale a carico di Vittorio Sgarbi concernono opinioni da ritenersi insindacabili ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
      che nei confronti di Vittorio Sgarbi, all'epoca dei fatti deputato al Parlamento, è in corso un procedimento penale per il reato previsto e punito dall'art. 30, comma 4, della legge n. 223 del 1990 in relazione all'art. 595 cod. pen. ed all'art. 13 della legge n. 47 del 1985, in quanto questi, facendo uso del mezzo televisivo, avrebbe «offeso la reputazione del consulente tecnico Sandro Lopez, qualificandolo persona incapace, professionalmente inidonea, con l'attribuzione del fatto determinato di non essere in grado di effettuare una perizia balistica per l'assoluta ignoranza della materia e per la conclamata incapacità di utilizzare il microscopio comparatore (strumento particolarmente importante nelle indagini balistiche), non essendo neanche in grado di riconoscere se stesso in uno specchio»; &#13;
      che, ad opinione del Tribunale, il conflitto di attribuzione in riferimento alla delibera in esame, poiché «è mutata nel corso degli anni la composizione del collegio e legittimamente ogni nuova composizione ha rivalutato le richieste delle parti alla luce del diniego espresso dalla Camera dei deputati», sarebbe ritualmente riproposto, benché già dichiarato una prima volta improcedibile ed una seconda volta inammissibile; &#13;
      che, ad avviso del ricorrente, la Camera dei deputati non avrebbe esercitato correttamente il proprio potere, tenuto conto che il particolare contesto televisivo in cui sono state espresse le opinioni per cui pende processo penale - e cioè la trasmissione "Sgarbi Quotidiani" - varrebbe a collocarle al di fuori dell'esercizio delle funzioni tipiche del mandato parlamentare col quale sussisterebbe nient'altro che una relazione occasionale ed eventuale; &#13;
      che, sulla scorta di tali deduzioni, il Tribunale conclude chiedendo l'annullamento della delibera della Camera dei deputati del 9 novembre 1999. &#13;
      Considerato che in questa fase la Corte è chiamata, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a delibare l'ammissibilità del ricorso; &#13;
      che, in via pregiudiziale, occorre osservare come il Tribunale di Cosenza, in riferimento alla medesima delibera della Camera dei deputati del 9 novembre 1999, riproponga il conflitto di attribuzione già dichiarato da questa Corte, una prima volta, improcedibile per l'avvenuto deposito del ricorso e dell'ordinanza che lo dichiarava ammissibile (n. 389 del 2000) oltre il termine stabilito dall'art. 26, terzo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale (sentenza n. 293 del 2001), ed una seconda volta inammissibile, «a prescindere da ogni ulteriore questione sulla ritualità di riproposizione», per il difetto nel ricorso «di una domanda chiaramente individuabile, consistente nella sostanziale richiesta di una pronuncia della Corte che dichiari non spettare alla Camera di appartenenza la valutazione contenuta nella delibera impugnata e che annulli la stessa delibera» (ordinanza n. 159 del 2002); &#13;
      che, col riproporre il medesimo conflitto per la terza volta, il Tribunale di Cosenza - essendo, evidentemente, del tutto irrilevante la circostanza della sua mutata composizione personale - pone in essere una situazione processuale in palese contrasto con quanto stabilito da questa Corte nella sentenza n. 116 del 2003 (e ribadito nelle ordinanze nn. 153, 188, 189, 214, 247, 254, 277, 280 e, da ultimo, 358 del 2003), secondo cui le finalità e particolarità dell'oggetto del conflitto di attribuzione tra poteri fanno emergere, nel quadro della disciplina della legge 11 marzo 1953, n. 87, «l'esigenza costituzionale che il giudizio, una volta instaurato, sia concluso in termini certi non rimessi alle parti confliggenti», ragion per cui non è ammissibile mantenere indefinitamente in sede processuale una situazione di conflittualità tra poteri, protraendo così ad libitum il ristabilimento della «certezza e definitività di rapporti»; &#13;
      che, pertanto, deve essere esclusa la riproponibilità del conflitto in esame e conseguentemente lo stesso deve essere dichiarato inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
      dichiara inammissibile il ricorso per conflitto di attribuzione tra poteri dello Stato proposto dal Tribunale di Cosenza - seconda sezione penale - nei confronti della Camera dei deputati, con l'atto indicato in epigrafe. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 gennaio 2004. &#13;
F.to: &#13;
Riccardo CHIEPPA, Presidente &#13;
Romano VACCARELLA, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 26 gennaio 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
