<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>323</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:323</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/10/1997</data_decisione>
    <data_deposito>30/10/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale degli artt. 1 e seguenti    &#13;
 del d.-l. 7 gennaio 1995, n. 3 (Disposizioni in materia di riutilizzo    &#13;
 dei residui derivanti da cicli di  produzione  o  di  consumo  in  un    &#13;
 processo  produttivo  o  in  un  processo  di combustione, nonché in    &#13;
 materia di smaltimento dei rifiuti), promosso con ordinanza emessa il    &#13;
 14 febbraio 1995 dal pretore di Ferrara  nel  procedimento  penale  a    &#13;
 carico di Urbano Andreotti ed altri, iscritta al n. 1357 del registro    &#13;
 ordinanze 1996 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 3, prima serie speciale, dell'anno 1997;                              &#13;
   Udito  nella  camera  di  consiglio  del  1 ottobre 1997 il giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
   Ritenuto che, nel corso di un procedimento  penale  per  violazione    &#13;
 delle  norme sullo smaltimento dei rifiuti (d.P.R. 10 settembre 1982,    &#13;
 n. 915), il pretore di Ferrara, con ordinanza emessa il  14  febbraio    &#13;
 1995,  ma  pervenuta  alla  Corte  il  17 dicembre 1996, ha sollevato    &#13;
 questione di legittimità costituzionale degli artt. 1 e seguenti del    &#13;
 d.-l.  7 gennaio 1995, n. 3 (Disposizioni in  materia  di  riutilizzo    &#13;
 dei  residui  derivanti  da  cicli  di  produzione o di consumo in un    &#13;
 processo produttivo o in  un  processo  di  combustione,  nonché  in    &#13;
 materia di smaltimento dei rifiuti);                                     &#13;
     che  il  decreto-legge  denunciato  -  che  segue  a una serie di    &#13;
 decreti-legge reiterati  con  contenuto  parzialmente  diverso  -  ad    &#13;
 avviso  del  giudice  rimettente contrasterebbe con gli artt. 25 e 77    &#13;
 della Costituzione, perché la reiterazione  per  circa  un  anno  di    &#13;
 diversi  decreti-legge  nella  stessa materia denoterebbe la mancanza    &#13;
 dei requisiti costituzionali di necessità ed urgenza e  sottrarrebbe    &#13;
 al  Parlamento  la  esclusiva  competenza  a  disciplinare la materia    &#13;
 penale, con carattere di stabilità e durevolezza, ed a regolare  con    &#13;
 legge i rapporti sorti in base a decreti-legge non convertiti, la cui    &#13;
 sostanziale  protrazione attraverso la reiterazione potrebbe, invece,    &#13;
 determinare effetti definitivi, quale il giudicato;                      &#13;
   Considerato che il decreto-legge denunciato non è stato convertito    &#13;
 in legge nei termini previsti dall'art. 77 della  Costituzione  ed  i    &#13;
 suoi  effetti sono stati poi sanati con la legge 11 novembre 1996, n.    &#13;
 575;                                                                     &#13;
     che,  successivamente  alla  proposizione  della   questione   di    &#13;
 legittimità costituzionale, la materia della gestione dei rifiuti è    &#13;
 stata  disciplinata  dal  decreto legislativo 5 febbraio 1997, n. 22,    &#13;
 emanato per  dare  attuazione,  tra  l'altro,  alle  direttive  della    &#13;
 Comunità economica europea 91/156 e 91/689;                             &#13;
     che  la  disciplina della materia risulta per più aspetti mutata    &#13;
 rispetto a quella considerata nell'ordinanza di rinvio, essendo stato    &#13;
 in particolare previsto un nuovo sistema di illeciti  e  di  sanzioni    &#13;
 (artt. 50 ss.) ed essendo stato abrogato il d.P.R. 10 settembre 1982,    &#13;
 n. 915 (art. 56);                                                        &#13;
     che,  indipendentemente  da ogni valutazione in ordine ai profili    &#13;
 attinenti alla decretazione d'urgenza,  è  opportuno  che  gli  atti    &#13;
 siano  restituiti  al  giudice  rimettente perché possa valutare se,    &#13;
 essendo mutato, a seguito del decreto legislativo n. 22 del 1997,  il    &#13;
 quadro  normativo  complessivo,  la  questione  sollevata sia tuttora    &#13;
 rilevante nel giudizio principale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Ordina la restituzione degli atti al pretore di Ferrara.               &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 16 ottobre 1997.                              &#13;
                         Il Presidente: Granata                           &#13;
                        Il redattore: Mirabelli                           &#13;
                        Il cancelliere: Di Paola                          &#13;
   Depositata in cancelleria il 30 ottobre 1997.                          &#13;
                Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
