<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>110</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:110</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>26/03/2003</data_decisione>
    <data_deposito>01/04/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>Nel giudizio di legittimità costituzionale dell'art. 582, comma 2, codice di procedura penale, promosso con ordinanza del 4 giugno 2002 dalla Corte di assise di appello di Palermo nel procedimento penale a carico di V.V., iscritta al n. 340 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 33, prima serie speciale, dell'anno 2002. &#13;
Udito nella camera di consiglio del 12 febbraio 2003 il Giudice relatore Giovanni Maria Flick. &#13;
Ritenuto che la Corte di assise di appello di Palermo ha sollevato, in riferimento agli artt. 3, 97, primo comma, 111, secondo comma, e 112 della Costituzione, questione di legittimità costituzionale dell'art. 582, comma 2, del codice di procedura penale, nella parte in cui non prevede che il pubblico ministero possa presentare l'atto di impugnazione «anche nella cancelleria ove si trova il suo ufficio»; &#13;
che il rimettente evidenzia come la norma impugnata consenta alle parti private ed ai difensori di presentare l'atto di impugnazione  non soltanto presso la cancelleria del giudice che ha emesso il provvedimento impugnato, ma anche presso la cancelleria del tribunale o del giudice di pace del luogo in cui si trovano, se tale luogo è diverso da quello in cui fu emesso il provvedimento oggetto di gravame: facoltà, quest'ultima, invece non prevista per il pubblico ministero, malgrado si realizzino anche per tale organo esigenze non dissimili a seguito della istituzione delle direzioni distrettuali antimafia, posto che per i magistrati ad esse addetti si pone «la necessità...di presentare atti di impugnazione avverso provvedimenti emessi in luoghi diversi da quelli in cui» il loro ufficio si trova; &#13;
che, a parere della Corte rimettente, la norma impugnata si porrebbe dunque in contrasto con l'art. 3 della Costituzione, in quanto non sussisterebbero fondate ragioni per le quali «la parte pubblica debba essere trattata in modo differente dalla parte privata, a fronte della medesima situazione di fatto»; &#13;
che risulterebbe violato anche l'art. 112 Cost., giacché la limitazione di cui innanzi si è detto inciderebbe sul potere di impugnazione del pubblico ministero, costituente , per tale organo, «necessario atto di prosecuzione» della azione penale; &#13;
che un ulteriore contrasto viene ravvisato anche con l'art. 97 Cost., in quanto, impedendo al pubblico ministero di presentare l'impugnazione presso la cancelleria del tribunale ove ha sede il suo ufficio e consentendo, invece, tale facoltà solo alle parti private ed ai difensori, «si limita irragionevolmente l'efficace ed efficiente andamento della attività giudiziaria, imponendo all'ufficio del p.m. modalità e forme procedimentali senza alcun apprezzabile vantaggio in termini di economia processuale»; &#13;
che sarebbe violato, infine, anche il principio di parità di trattamento fra accusa e difesa nel processo penale, sancito dall'art. 111, secondo comma, Cost., trattandosi di principio non circoscritto al solo contraddittorio ed alla acquisizione e formazione della prova, ma da «intendersi in senso più ampio, come parità nell'esercizio delle facoltà e dei diritti inerenti l'espletamento di tutte le attività riguardanti lo svolgimento del processo»; &#13;
che la rilevanza del quesito - sottolinea ancora il rimettente - sarebbe nella specie di tutta evidenza, in quanto, ove la norma impugnata fosse dichiarata costituzionalmente illegittima in parte qua, l'appello proposto dal pubblico ministero sarebbe tempestivo e, dunque, ammissibile. &#13;
Considerato che la Corte rimettente censura la previsione dettata dall'art. 582, comma 2, del codice di procedura penale, nella parte in cui consente soltanto alle parti private ed ai difensori - e non anche al pubblico ministero - di presentare l'atto di impugnazione nella cancelleria del tribunale o del giudice di pace del luogo in cui si trovano, se tale luogo è diverso da quello in cui fu emesso il provvedimento impugnato; &#13;
che tale previsione - di tradizione assai risalente, in quanto già presente nell'art. 198, terzo comma, del codice abrogato, come sostituito ad opera dell'art. 1 della legge 21 marzo 1958, n. 229 - si giustifica agevolmente in considerazione delle evidenti diversità di condizioni e status che caratterizzano i soggetti privati, da un lato, ed i magistrati del pubblico ministero, dall'altro, potendosi questi ultimi avvalere delle strutture del proprio ufficio e risultando, dunque, in concreto agevolati nella presentazione, eventualmente anche a mezzo di incaricato, dell'atto di impugnazione; &#13;
che tali rilievi non possono certo dirsi venuti meno a seguito della istituzione delle direzioni distrettuali antimafia, e della conseguente possibilità che i magistrati addetti si trovino nella necessità di proporre impugnazioni avverso provvedimenti adottati da autorità giudiziarie aventi sede anche in luogo diverso dal capoluogo del distretto, trattandosi, all'evidenza, di profili di mero fatto che in nessun modo incidono sulla intrinseca ragionevolezza della disposizione oggetto di impugnativa; &#13;
che appare del tutto improprio il dedotto contrasto con l'art. 112 Cost., non soltanto perché, una volta esclusa nella specie qualsiasi irragionevole limitazione nei poteri processuali del pubblico ministero, deve altresì escludersi la prospettata compromissione delle attribuzioni di quell'organo; ma anche perché il potere di impugnazione del pubblico ministero non costituisce, in sé, estrinsecazione necessaria dei poteri inerenti all'esercizio della azione penale (v., fra le altre, l'ordinanza n. 421 del 2001); &#13;
che è palesemente infondato anche il dubbio di legittimità sollevato in riferimento all'art. 111, secondo comma, Cost., in quanto il principio di parità tra accusa e difesa - pacificamente già presente fra i valori costituzionali anche prima delle modifiche apportate dalla legge costituzionale n. 2 del 1999 - non comporta necessariamente l'identità tra i poteri processuali del pubblico ministero e quelli delle altre parti, giacché una diversità di trattamento può essere, come nella specie, stabilita ragionevolmente, nell'ambito delle scelte discrezionali del legislatore, proprio in ragione della peculiare posizione istituzionale del pubblico ministero e degli ausili strutturali di cui, ratione officii, può avvalersi (v., ex plurimis, ordinanza n. 83 del 2002); &#13;
che, infine, deve ritenersi inconferente altresì la dedotta violazione dell'art. 97 Cost., posto che, secondo la costante giurisprudenza di questa Corte, il principio di buon andamento della pubblica amministrazione - pur concernendo anche gli organi dell'amministrazione della giustizia - si riferisce esclusivamente alle leggi relative all'ordinamento degli uffici giudiziari ed al funzionamento di questi ultimi sotto l'aspetto amministrativo, risultando di per sé estraneo all'esercizio della funzione giurisdizionale (v. ordinanza n. 370 del 2002); &#13;
che, pertanto, la questione proposta deve essere dichiarata manifestamente infondata. &#13;
Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 582, comma 2, del codice di procedura penale, sollevata, in riferimento agli artt. 3, 97, primo comma, 111, secondo comma, e 112 della Costituzione, dalla Corte di assise di appello di Palermo con l'ordinanza in epigrafe. &#13;
Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 26 marzo 2003. &#13;
F.to: &#13;
Riccardo CHIEPPA, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria l'1  aprile 2003. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
