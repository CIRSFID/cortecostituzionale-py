<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>122</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:122</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/04/1986</data_decisione>
    <data_deposito>30/04/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI &#13;
 - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - &#13;
 Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO &#13;
 GRECO - Prof. RENATO DELL'ANDRO - Prof. GABRIELE PESCATORE - Avv. UGO &#13;
 SPAGNOLI - Prof. FRANCESCO PAOLO CASAVOLA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 5, secondo  &#13;
 comma, seconda parte, legge  26  gennaio  1980,  n.  16  ("Disposizioni  &#13;
 concernenti  la corresponsione di indennizzi, incentivi ed agevolazioni  &#13;
 a cittadini ed imprese italiane che abbiano perduto  beni,  diritti  ed  &#13;
 interessi  in  territori  già  soggetti  alla  sovranità  italiana  e  &#13;
 all'estero"), promosso con l'ordinanza emessa il 14 febbraio 1984 dalla  &#13;
 Corte di Appello di Roma nel procedimento civile vertente tra Ministero  &#13;
 del Tesoro e Schonburg-Waldengurg Wolf, iscritta al n. 950 del registro  &#13;
 ordinanze 1984 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 341 dell'anno 1984.                                                   &#13;
     Visto  l'atto  di  costituzione degli eredi di Schonburg-Waldengurg  &#13;
 Wolf nonché l'atto di intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del  5  marzo  1986 il Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
     Ritenuto che la  Corte  di  Appello  di  Roma  con  l'ordinanza  in  &#13;
 epigrafe  ha  sollevato,  in riferimento all'art. 3 Cost., questione di  &#13;
 legittimità costituzionale dell'art. 5, secondo comma, seconda  parte,  &#13;
 della  legge  26  gennaio  1980,  n.  16  ("Disposizioni concernenti la  &#13;
 corresponsione di indennizzi, incentivi ed agevolazioni a cittadini  ed  &#13;
 imprese  italiane  che  abbiano  perduto  beni, diritti ed interessi in  &#13;
 territori già soggetti alla sovranità  italiana  e  all'estero"),  in  &#13;
 quanto,  con  il  prevedere che i titolari dei beni già posti sotto la  &#13;
 sovranità italiana (ed ora posti sotto la sovranità di altri Stati, e  &#13;
 nella specie  dello  Stato  jugoslavo)  i  quali,  "in  conseguenza  di  &#13;
 risarcimenti ottenuti con appositi accordi da Stati esteri, abbiano, in  &#13;
 sede di ripartizione dei valori, beneficiato di un indennizzo calcolato  &#13;
 in  base  a  coefficienti di rivalutazione fino a 25 volte il valore al  &#13;
 1938,  godranno  per  detti  beni  di  un  ulteriore  coefficiente   di  &#13;
 rivalutazione  pari  a  15  volte  il  valore al 1938", derogherebbe al  &#13;
 criterio di  rivalutazione  fissato  nella  prima  parte  dello  stesso  &#13;
 secondo  comma  dell'art.  5  legge  n.  16/80;  in  base a tale ultimo  &#13;
 criterio le valutazioni per l'indennizzo relativo alle perdite dei beni  &#13;
 avvenute  a partire dal gennaio 1950 vanno compiute secondo i prezzi di  &#13;
 mercato dei luoghi ove i beni siano situati, riferiti però al  1938  e  &#13;
 moltiplicati per 40 volte;                                               &#13;
     ritenuto  che  secondo  la  Corte  d'Appello  rimettente si sarebbe  &#13;
 venuto a determinare un trattamento discriminatorio  nei  confronti  di  &#13;
 chi  sia  stato privato dei beni ora posti sotto la sovranità di Stati  &#13;
 con i quali sia  intervenuto  un  accordo  internazionale,  rispetto  a  &#13;
 coloro  i  cui beni si trovino nei territori degli altri Stati, i quali  &#13;
 non siano  al  riguardo  addivenuti  ad  alcun  accordo  con  lo  Stato  &#13;
 italiano;                                                                &#13;
     che,  ancora secondo il giudice a quo, sussiste altresì differenza  &#13;
 di trattamento in favore di  chi,  non  avendo  presentato  domanda  di  &#13;
 indennizzo  alla  data dell'entrata in vigore della legge n. 16/80, ha,  &#13;
 comunque, diritto, a prescindere dallo Stato in cui il bene  si  trova,  &#13;
 all'applicazione  del  criterio  rivalutativo fissato nella prima parte  &#13;
 del secondo comma dell'art. 5.                                           &#13;
     Considerato, tuttavia, che nelle  more  del  presente  giudizio  di  &#13;
 costituzionalità  è  sopraggiunta  la legge 5 aprile 1985, n. 135, il  &#13;
 cui art. 4 sostituisce, prevedendo nuovi e più  elevati  parametri  di  &#13;
 rivalutazione,  l'art.  5 della legge n.  16/80, nel quale è contenuta  &#13;
 la norma censurata;                                                      &#13;
     che, pertanto, alla luce di tale ius  superveniens  si  impone  una  &#13;
 nuova  valutazione,  da  parte  del  giudice rimettente, in ordine alla  &#13;
 perdurante sussistenza dei presupposti che lo hanno indotto a sollevare  &#13;
 la presente questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina la restituzione degli atti del presente giudizio alla  Corte  &#13;
 d'Appello di Roma.                                                       &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 aprile 1986.         &#13;
                                   F.to:  LIVIO  PALADIN  -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO  GRECO  - RENATO DELL'ANDRO  &#13;
                                   GABRIELE PESCATORE - UGO  SPAGNOLI  -  &#13;
                                   FRANCESCO PAOLO CASAVOLA.              &#13;
                                   ROLANDO GALLI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
