<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>132</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:132</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Giuseppe Ferrari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/04/1987</data_decisione>
    <data_deposito>15/04/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio LA PERGOLA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giuseppe FERRARI, dott. &#13;
 Francesco SAJA, prof. Giovanni CONSO, dottor Aldo CORASANITI, &#13;
 prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. Renato &#13;
 DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  24, primo    &#13;
 comma, legge regionale siciliana 14 settembre 1979,  n.  212  ("Norme    &#13;
 riguardanti  l'Ente  di sviluppo agricolo (ESA), l'Istituto regionale    &#13;
 della vite e del vino (IRVV), l'Azienda  siciliana  trasporti  (AST),    &#13;
 l'Istituto  regionale  per  il  credito alla cooperazione (IRCAC), la    &#13;
 Cassa regionale per il  credito  alle  imprese  artigiane  (CRIAS)  e    &#13;
 l'Ente  acquedotti siciliani (EAS)"), promosso con l'ordinanza emessa    &#13;
 il 10 ottobre 1980 dal Tribunale di Catania nel  procedimento  civile    &#13;
 vertente  tra Di Graziano Cono e Pezzino Giovanni, iscritta al n. 832    &#13;
 del registro ordinanze 1980 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 56 dell'anno 1981;                                   &#13;
    Visto l'atto di costituzione di Di Graziano Cono;                     &#13;
    Udito  nell'udienza  pubblica  del  10  dicembre  1986  il Giudice    &#13;
 relatore Giuseppe Ferrari.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel  giudizio  promosso  da  Cono Di Graziano avverso la convalida    &#13;
 dell'elezione di Giovanni Pezzino a consigliere comunale  di  Catania    &#13;
 in  seguito alle votazioni svoltesi nei giorni 8 e 9 giugno 1980, per    &#13;
 non essere il medesimo  cessato  dalle  funzioni  di  componente  del    &#13;
 consiglio  di  amministrazione  dello ESA (ente di sviluppo agricolo)    &#13;
 almeno novanta giorni prima  dal  compimento  del  quinquennio  dalle    &#13;
 precedenti elezioni comunali del 15 e 16 giugno 1975, il Tribunale di    &#13;
 Catania, con ordinanza emessa il 10 ottobre  1980  ha  sollevato,  in    &#13;
 riferimento  agli  artt.  3  e  51  Cost.,  questione di legittimità    &#13;
 costituzionale dell'art.  24,  primo  comma,  della  legge  regionale    &#13;
 siciliana  14  settembre 1979, n. 212, "per la parte in cui limita ai    &#13;
 comuni   con   popolazione   superiore   ai   25.000   abitanti    la    &#13;
 ineleggibilità alla carica di consigliere comunale e provinciale dei    &#13;
 soggetti nella stessa norma indicati". Osserva il giudice a  quo  che    &#13;
 la  norma  denunciata,  laddove  prevede  che  gli  amministratori, i    &#13;
 componenti dei collegi sindacali ed i revisori dei conti  degli  enti    &#13;
 pubblici,  dipendenti  dalla regione, dall'articolo stesso menzionati    &#13;
 (ESPI, EMS, AZASI, IRCAC, CRIAS, AST, ESA,  IRVV  ed  EAS)  non  sono    &#13;
 eleggibili  a  consiglieri  provinciali  e  comunali  di  comuni  con    &#13;
 popolazione  superiore  a  25.000  abitanti  ove  non  cessino  dalle    &#13;
 rispettive  funzioni  almeno  novanta giorni prima del compimento del    &#13;
 quinquennio  dalla  data  delle  precedenti   elezioni   comunali   e    &#13;
 provinciali,  pone  una  deroga alla legislazione statale in materia,    &#13;
 non consentita al legislatore regionale.                                 &#13;
    La  Corte costituzionale - si afferma sostanzialmente in ordinanza    &#13;
 - con sentenze n. 108 del 1969 e n. 189                                  &#13;
 del   1971   ha  infatti  chiarito  che  l'esercizio  della  potestà    &#13;
 legislativa primaria attribuita alla regione Sicilia dall'art. 14,       &#13;
 lettera  o),  dello  Statuto speciale d'autonomia (r.d.lgs. 15 maggio    &#13;
 1946, n. 455), in materia di ordinamento degli enti locali, non  può    &#13;
 dar vita a norme limitative del diritto d'elettorato passivo, qual è    &#13;
 disciplinato  dalla  legislazione  elettorale  statale,  se  non   in    &#13;
 presenza di situazioni le quali siano esclusive per la Sicilia, ed in    &#13;
 ogni caso per motivi adeguati e ragionevoli, finalizzati alla  tutela    &#13;
 di   un  interesse  generale.  Nella  specie  difetterebbe  la  prima    &#13;
 condizione, sicché la norma violerebbe il principio  di  eguaglianza    &#13;
 in   materia   elettorale.   In  riferimento  all'art.  3  Cost.,  la    &#13;
 disposizione sarebbe comunque censurabile sotto  il  diverso  profilo    &#13;
 della   irragionevole   limitazione   dell'ineleggibilità   ai  soli    &#13;
 consiglieri  comunali  e  provinciali  di  comuni   con   popolazione    &#13;
 superiore  ai 25.000 abitanti, attesa l'uguale valenza che, anche nei    &#13;
 comuni con  popolazione  inferiore,  assume  la  ratio  della  norma,    &#13;
 mirante  ad  evitare la possibile captatio benevolentiae - nei comuni    &#13;
 con popolazione inferiore suscettibile di manifestarsi  con  maggiore    &#13;
 intensità  -  degli  elettori  da  parte  di  soggetti  che occupino    &#13;
 determinate cariche.<diritto>Considerato in diritto</diritto>1. - Il Consiglio comunale di Catania, con deliberazione in data 4    &#13;
 luglio 1980, convalidava la elezione di  tal  Pezzino  Giovanni,  che    &#13;
 nella  consultazione  elettorale  dell'8  giugno era risultato eletto    &#13;
 Consigliere di  quel  Comune.  Ma  poiché  il  Pezzino  non  si  era    &#13;
 tempestivamente  dimesso  dalla  carica  di  componente  il consiglio    &#13;
 d'amministrazione dell'ESA (ente di sviluppo agricolo),  la  suddetta    &#13;
 deliberazione  di  convalida veniva impugnata in sede giurisdizionale    &#13;
 da  un  elettore  sulla  base  dell'art.  24  della  legge  regionale    &#13;
 siciliana  14  settembre  1979,  n.  212,  a  sensi  del  quale  "gli    &#13;
 amministratori... dell'ESA... non sono  eleggibili  a  consiglieri...    &#13;
 comunali  di  Comuni con popolazione superiore a 25 mila abitanti ove    &#13;
 non  cessino  dalle...  funzioni  almeno  novanta  giorni  prima  del    &#13;
 compimento  del  quinquennio  dalla  data  delle  precedenti elezioni    &#13;
 comunali...".                                                            &#13;
    L'adito Tribunale di Catania, dopo avere osservato che "in materia    &#13;
 di elettorato passivo per gli enti locali" "la regione siciliana può    &#13;
 prevedere...  cause  di  ineleggibilità" "nuove e diverse" da quelle    &#13;
 stabilite dal legislatore statale, solo se giustificate e  razionali,    &#13;
 ha  lamentato  in  particolare  che  il  legislatore  regionale abbia    &#13;
 disposto l'ineleggibilità in oggetto  limitatamente  ai  Comuni  con    &#13;
 popolazione  superiore  a 25 mila abitanti, creando così "tra le due    &#13;
 categorie di Comuni" una discriminazione, che "non appare  suffragata    &#13;
 da  razionale giustificazione". E poiché - prosegue l'ordinanza "nei    &#13;
 Comuni  con  minore  popolazione  la  captatio   benevolentiae   può    &#13;
 manifestarsi  con maggiore intensità, attesa la possibilità di più    &#13;
 diretti e frequenti contatti con la base elettorale",  il  giudice  a    &#13;
 quo ha denunciato a questa Corte, per sospetta violazione degli artt.    &#13;
 51 e 3 Cost., l'art. 24 della menzionata legge regionale n.  212  del    &#13;
 1979   nella   parte   in   cui   distingue   i   Comuni,   ai   fini    &#13;
 dell'eleggibilità degli  amministratori  in  discorso,  secondo  che    &#13;
 abbiano una popolazione superiore o inferiore ai 25 mila abitanti.       &#13;
    2.  -  La questione, posta nei termini surriportati, va dichiarata    &#13;
 inammissibile.                                                           &#13;
    Oggetto della doglianza del ricorrente era il fatto che il Pezzino    &#13;
 non  si  fosse  dimesso  dalla  carica  di  amministratore  dell'ESA,    &#13;
 prevista  dall'art. 24 come causa di ineleggibilità, nel termine ivi    &#13;
 stabilito.  Il  Tribunale  di  Catania,  viceversa,  lamentando   che    &#13;
 l'ineleggibilità  è  stata  stabilita solo nei confronti dei Comuni    &#13;
 con popolazione superiore ai 25 mila abitanti, in sostanza  chiede  a    &#13;
 questa Corte di volere estendere la ineleggibilità anche rispetto ai    &#13;
 Comuni con popolazione inferiore. Ma poiché  il  resistente  risulta    &#13;
 essere stato eletto in un Comune con popolazione superiore ai 25 mila    &#13;
 abitanti, quale è quello di Catania, è di  tutta  evidenza  che  il    &#13;
 giudice  a  quo  ha impugnato una disposizione che non rileva ai fini    &#13;
 del decidere la controversia della quale era stato investito.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 dichiara   l'inammissibilità   della   questione   di   legittimità    &#13;
 costituzionale dell'art.  24,  primo  comma,  della  legge  regionale    &#13;
 siciliana  14  settembre  1979, n. 212, sollevata in riferimento agli    &#13;
 artt. 51 e 3 Cost.  dal  Tribunale  di  Catania  con  l'ordinanza  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta,                            &#13;
 l'8 aprile 1987.                                                         &#13;
                       Il Presidente: LA PERGOLA                          &#13;
                       Il Redattore: FERRARI                              &#13;
    Depositata in cancelleria il 15 aprile 1987.                          &#13;
                        Il cancelliere: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
