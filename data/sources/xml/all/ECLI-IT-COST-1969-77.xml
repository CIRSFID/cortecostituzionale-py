<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>77</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:77</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Giuseppe Chiarelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/03/1969</data_decisione>
    <data_deposito>11/04/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI &#13;
 BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 8 del regio  &#13;
 decreto legge 29 luglio 1927, n. 1509 (provvedimenti per  l'ordinamento  &#13;
 del  credito  agrario),  convertito  in legge 5 luglio 1928, n. 1760, e  &#13;
 successive modificazioni, e degli artt. 15, primo e  secondo  comma,  e  &#13;
 16,  secondo,  terzo e quarto comma, della legge 21 luglio 1960, n. 739  &#13;
 (provvidenze per le zone agrarie danneggiate da  calamità  naturali  e  &#13;
 provvidenze  per le imprese industriali), promosse con ordinanza emessa  &#13;
 il 27 giugno 1967 dal  tribunale  di  Matera  nel  procedimento  civile  &#13;
 vertente  tra  Torre  Angela  e  altri  ed  il Banco di Napoli e altri,  &#13;
 iscritta al n. 193 del  Registro  ordinanze  1967  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 258 del 14 ottobre 1967.          &#13;
     Visti gli atti di costituzione di Torre Angela ed altri e del Banco  &#13;
 di Napoli, e d'intervento del Presidente del Consiglio dei Ministri;     &#13;
     udita  nell'udienza  pubblica  del  12  marzo 1969 la relazione del  &#13;
 Giudice Giuseppe Chiarelli;                                              &#13;
     uditi l'avv. Giuseppe Tosatti, per Torre ed altri, l'avv.  Leopoldo  &#13;
 Piccardi,  per  il  Banco  di Napoli, ed il sostituto avvocato generale  &#13;
 dello Stato Giovanni Albissini, per il  Presidente  del  Consiglio  dei  &#13;
 Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Angela  Torre  e  altri  avevano concesso in locazione, nel 1961, a  &#13;
 Giuseppe Dilengite  un  fondo  rustico  denominato  Matina  Soprana  in  &#13;
 tenimento di Grottale (Matera) per la durata di due anni; in seguito al  &#13;
 decesso   del  Dilengite,  la  locazione  era  stata  convenzionalmente  &#13;
 prorogata con gli eredi fino al 15 agosto 1964. Cessato il rapporto coi  &#13;
 Dilengite, una parte del fondo veniva  data  in  locazione  ad  Antonio  &#13;
 Leone  e altri, e la restante parte a Nicola Ludovico, per la durata di  &#13;
 sei anni.                                                                &#13;
     Con  citazione  notificata  il  12-14  giugno   1965   i   predetti  &#13;
 proprietari  Torre  ed  i locatari Leone e Ludovico, premesso di essere  &#13;
 venuti a conoscenza che i Dilengite avevano ottenuto da  vari  istituti  &#13;
 esercenti  il  credito  agrario,  in relazione al fondo Matina Soprana,  &#13;
 prestiti di conduzione  per  ingenti  importi  ed  anche  con  scadenza  &#13;
 successiva  alla  durata  del  rapporto locativo di cui erano titolari,  &#13;
 rilasciando cambiali  agrarie  rimaste  in  gran  parte  insoddisfatte,  &#13;
 convenivano  in  giudizio  i  detti  istituti  di  credito,  per sentir  &#13;
 dichiarare la inesistenza di qualsivoglia  privilegio  sui  frutti  del  &#13;
 fondo,  ad  essi  opponibile  ai sensi dell'art. 8 del decreto legge 29  &#13;
 luglio 1927, n. 1509, convertito in  legge  5  luglio  1928,  n.  1760,  &#13;
 modificato  col  decreto  legge 29 luglio 1928, n. 2085, e disposizioni  &#13;
 connesse.   In   linea   subordinata   si   eccepiva   l'illegittimità  &#13;
 costituzionale  delle  dette  disposizioni  legislative, in riferimento  &#13;
 agli artt. 3, 23 e 42 della Costituzione.                                &#13;
     Il tribunale di Matera, con ordinanza 27 giugno 1967, riteneva  non  &#13;
 manifestamente  infondata  la questione solo rispetto all'art. 42 della  &#13;
 Costituzione.  Si  osserva  nell'ordinanza  che,  per   effetto   delle  &#13;
 impugnate  disposizioni,  il  proprietario,  per il comportamento di un  &#13;
 altro soggetto  e  senza  il  concorso  della  propria  volontà,  vede  &#13;
 assoggettati  i  propri  beni  a  un  vincolo reale, e ristretta la sua  &#13;
 facoltà di godimento, che, per  l'art.  42  della  Costituzione,  può  &#13;
 essere  soppressa  quando  ricorrano "motivi di interesse generale", ma  &#13;
 non può essere compressa o annullata per effetto del comportamento  di  &#13;
 un soggetto diverso dal proprietario. Se si ammette, anche alla stregua  &#13;
 degli  artt.  15  e  16  della  legge  21 luglio 1960, n.   739, che ha  &#13;
 consentito la  ratizzazione  dei  prestiti,  che  il  privilegio  possa  &#13;
 gravare   sui  frutti  che,  per  l'avvenuta  cessazione  del  rapporto  &#13;
 locativo, non appartengono più al conduttore, non sembra infondata  la  &#13;
 tesi  della  violazione  dell'art.  42  della Costituzione. L'ordinanza  &#13;
 rimetteva  pertanto  a  questa  Corte  la  questione  di   legittimità  &#13;
 costituzionale  del  citato  art.  8  del regio decreto legge 29 luglio  &#13;
 1927, n. 1509, e degli artt. 15, primo e secondo comma, e 16,  secondo,  &#13;
 terzo  e  quarto  comma,  della  legge  21  luglio  1960,  n.  739,  in  &#13;
 riferimento all'art. 42 della Costituzione.                              &#13;
     L'ordinanza  è  stata  regolarmente   notificata,   comunicata   e  &#13;
 pubblicata.                                                              &#13;
     Si  sono  costituiti nel presente giudizio i signori Torre, Leone e  &#13;
 Ludovico, rappresentati e  difesi  dall'avv.    Giuseppe  Tosatti,  con  &#13;
 deduzioni  depositate  il  28  agosto  1967. In esse si premette che la  &#13;
 giurisprudenza ritiene che il privilegio in questione  può  avere  una  &#13;
 durata  superiore  a  quella  del  rapporto  locativo  che  fa  capo al  &#13;
 debitore,  con  la  conseguenza  che  può  gravare  su  frutti che non  &#13;
 appartengono più a  quest'ultimo,  senza  che  il  proprietario  possa  &#13;
 impedire  tali  effetti.  In  ciò  si  ravvisa  la violazione non solo  &#13;
 dell'art. 42, ma anche degli artt. 3 e 23 della Costituzione.            &#13;
     Si è anche costituito il Banco di Napoli, rappresentato  e  difeso  &#13;
 dall'avv.  Leopoldo  Piccardi,  con  deduzioni  depositate il 28 agosto  &#13;
 1967, nelle quali si rileva preliminarmente che gli artt. 15 e 16 della  &#13;
 legge 739 del 1960 non possono formare oggetto di  sindacato  sotto  il  &#13;
 profilo precisato dall'ordinanza, perché il loro contenuto è estraneo  &#13;
 alla  censura da questa presa in considerazione.  Quanto all'art. 8 del  &#13;
 regio decreto legge n. 1509 del 1927 si  osserva  che  esso  non  viola  &#13;
 l'art.  42 della Costituzione, il quale prevede limiti alla proprietà,  &#13;
 allo  scopo  di  assicurarne  la  funzione  sociale,  e  va  inteso  in  &#13;
 correlazione con le altre norme della Costituzione relative ai rapporti  &#13;
 economici, nel cui quadro rientra la legislazione sul credito agrario.   &#13;
     Tali argomenti sono stati svolti in successiva memoria, nella quale  &#13;
 si  osserva  che  il  proprietario  si  arricchirebbe  ingiustamente se  &#13;
 potesse  far  propri  i  frutti  ottenuti  con  l'impiego  di  capitali  &#13;
 anticipati  da  altri,  senza sopportare alcun onere per il rimborso di  &#13;
 essi. A sostegno della tesi della legittimità delle  norme  impugnate,  &#13;
 in   quanto  incidono  su  un  rapporto  contrattuale,  si  ricorda  la  &#13;
 giurisprudenza della Corte relativa  all'avviamento  commerciale,  alla  &#13;
 proroga  dei  contratti  di locazione, alla risoluzione di canoni e via  &#13;
 dicendo. Per quanto riguarda la legge n. 739 del 1960  si  insiste  nel  &#13;
 rilevare  che  non  ha  formato  oggetto  di  autonoma  impugnativa,  e  &#13;
 comunque,  l'eccezione   di   illegittimità   costituzionale   sarebbe  &#13;
 infondata,  perché la ragione della legge, emanata per far fronte alle  &#13;
 conseguenze di calamità naturali, giustifica i maggiori  oneri  che  i  &#13;
 proprietari  sono  chiamati a sostenere.  È intervenuto in giudizio il  &#13;
 Presidente  del  Consiglio  dei  Ministri,   rappresentato   e   difeso  &#13;
 dall'avvocato  generale  dello Stato, con atto depositato il 3 novembre  &#13;
 1967, in cui si rileva che la legislazione sul credito agrario  investe  &#13;
 un   rilevante   interesse   pubblico,  di  stimolo  e  incentivo  alla  &#13;
 coltivazione dei terreni.  L'art.  8  impugnato  si  armonizza  con  la  &#13;
 funzione  sociale  della  proprietà,  essendo  giustificabile  che  il  &#13;
 privilegio, nel caso di mancata realizzazione del  credito  sui  frutti  &#13;
 dell'anno,  si  estenda  sui  frutti  dell'anno  successivo,  alla  cui  &#13;
 realizzazione le somme erogate hanno contribuito. Ancora più  evidente  &#13;
 è  questa  giustificazione  per  la  legge n. 739 del 1960, emanata in  &#13;
 presenza di eccezionali momenti, e con  esclusivo  riferimento  a  zone  &#13;
 agricole danneggiate da calamità naturali.                              &#13;
     Nella  discussione orale i difensori delle parti hanno sviluppato i  &#13;
 rispettivi argomenti.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Per  precisare  i  termini  del  giudizio,  va  osservato  che  &#13;
 l'ordinanza  del  tribunale  di  Matera  ha  rimesso  a questa Corte la  &#13;
 questione di legittimità  costituzionale  delle  norme  impugnate  con  &#13;
 riferimento   all'art.   42   della  Costituzione,  ritenendola  invece  &#13;
 manifestamente infondata  in  riferimento  agli  artt.  3  e  23  della  &#13;
 Costituzione.  Vanno  perciò  disattese le considerazioni svolte dalla  &#13;
 difesa Torre, nell'atto di costituzione, a proposito di  questi  ultimi  &#13;
 articoli.                                                                &#13;
     Così  determinato  il  thema  decidendum,  è da premettere che la  &#13;
 legge 5 luglio 1928, n. 1760 (di conversione del D.L. 29  luglio  1927,  &#13;
 n.  1509),  regola  le  operazioni  di  credito  agrario  di esercizio,  &#13;
 riguardanti: "1) i prestiti per la conduzione delle aziende  agrarie  e  &#13;
 per  la  utilizzazione,  manipolazione  e  trasformazione dei prodotti"  &#13;
 (art. 2; le altre operazioni ivi considerate non concernono il presente  &#13;
 giudizio).  Per  tali  prestiti  è  stabilito  che  "avranno  scadenza  &#13;
 rispettivamente  alla epoca del raccolto o della completa utilizzazione  &#13;
 o trasformazione del prodotto" (art. 5). A garanzia dei detti prestiti,  &#13;
 l'art. 8 dispone che essi "sono privilegiati sopra i frutti pendenti  e  &#13;
 quelli  raccolti  nell'anno  della  scadenza  del  prestito  e sopra le  &#13;
 derrate che si trovano nelle abitazioni e fabbriche  annesse  ai  fondi  &#13;
 rustici   e   provenienti   dai   medesimi".  Tale  privilegio  compete  &#13;
 all'Istituto mutuante "in confronto di  chiunque  possegga,  coltivi  e  &#13;
 conduca  il  fondo  entro  l'anno in cui scade il prestito o la singola  &#13;
 rata di esso. In caso di mancato o insufficiente raccolto il privilegio  &#13;
 si trasferisce sui frutti dell'annata successiva; purché  il  debitore  &#13;
 continui  nella  conduzione  del  fondo"  (secondo comma dell'articolo,  &#13;
 modificato dal decreto legge 29 luglio 1928, n. 2085).                   &#13;
     Dalle riportate norme si evince che beneficiario del prestito è il  &#13;
 conduttore dell'azienda, sia  o  non  proprietario  del  fondo,  e  che  &#13;
 formano  oggetto  del  privilegio  i  frutti  dell'anno di scadenza del  &#13;
 prestito: scadenza  che,  per  l'art.    5  innanzi  riportato,  si  ha  &#13;
 "all'epoca  del  raccolto o della compiuta utilizzazione del prestito".  &#13;
 La garanzia del prestito è pertanto costituita,  come  esattamente  si  &#13;
 afferma  nelle  deduzioni  del  Banco di Napoli, da beni che sono stati  &#13;
 prodotti  con  l'attività  del  debitore  e  con  l'utilizzazione  del  &#13;
 prestito.  La  norma  impugnata  trova rispondenza nella norma generale  &#13;
 dell'art. 2757 del Codice  civile,  per  la  quale  i  crediti  per  le  &#13;
 somministrazioni   e  per  i  lavori  di  coltivazione  e  di  raccolta  &#13;
 dell'annata agricola hanno privilegio sui frutti, alla  cui  produzione  &#13;
 abbiano concorso.                                                        &#13;
     Trattasi di un privilegio di carattere reale, costituito per legge,  &#13;
 rispetto  al  quale,  per  questa  sua  natura,  non  può invocarsi il  &#13;
 principio della irrilevanza della res inter  alios  acta,  e  che  può  &#13;
 essere  esercitato  contro chiunque, per il diritto di seguito, proprio  &#13;
 dei privilegi reali.                                                     &#13;
     Non sussiste pertanto  l'asserita  violazione  dell'art.  42  della  &#13;
 Costituzione.                                                            &#13;
     Né,  a  giudizio  della  Corte, può ravvisarsi violazione di tale  &#13;
 articolo ove si ritenga,  con  la  prevalente  giurisprudenza,  che  la  &#13;
 durata  del  privilegio  corrisponda  all'anno di calendario successivo  &#13;
 alla scadenza del prestito, e che quindi possa  essere  esercitato  sul  &#13;
 raccolto   di   tale  anno,  anche  nel  caso  che  il  debitore  abbia  &#13;
 precedentemente cessato la conduzione del fondo.                         &#13;
     Va in proposito osservato che la disciplina del credito agrario  è  &#13;
 intesa  al  conseguimento dei fini di utilità sociale della produzione  &#13;
 agricola e del suo incremento,  rispetto  ai  quali  si  legittimano  i  &#13;
 limiti  ed  i vincoli della proprietà privata, inerenti alla "funzione  &#13;
 sociale" di essa, e preordinati,  per  quanto  riguarda  la  proprietà  &#13;
 terriera,  al fine di "conseguire il razionale sfruttamento del suolo e  &#13;
 di stabilire equi rapporti sociali" (art. 44 della Costituzione).        &#13;
     La possibilità di far valere il privilegio nei confronti del terzo  &#13;
 (proprietario o nuovo conduttore del fondo) corrisponde  agli  indicati  &#13;
 scopi  di interesse generale, di favorire il credito agrario, giacché,  &#13;
 se  la  si  escludesse,  la  garanzia  del  prestito  potrebbe   essere  &#13;
 inoperante,  e  d'altronde  l'utilità del suo impiego non si esaurisce  &#13;
 con la produzione dei frutti dell'annata, potendo derivare da  esso  un  &#13;
 vantaggio per la valorizzazione del fondo e la realizzazione dei frutti  &#13;
 successivi.                                                              &#13;
     È  anche  da tener presente che la legge contiene norme dirette ad  &#13;
 assicurare che il prestito sia utilizzato per  gli  scopi  per  cui  è  &#13;
 concesso  (artt.  7  e 10), e il regolamento di esecuzione stabilisce i  &#13;
 mezzi idonei perché l'istituto di credito, nel concedere il  prestito,  &#13;
 accerti  il  titolo  al  quale  il  richiedente  coltiva  il fondo, con  &#13;
 riferimento ai contratti che lo comprovino (d.m. 23 gennaio 1928,  art.  &#13;
 1, lett. b).                                                             &#13;
     2.  - Gli artt. 15 e 16 della legge n. 739 del 1960 sono impugnati,  &#13;
 sia nell'ordinanza che nell'atto di costituzione dei signori Torre,  in  &#13;
 quanto fanno riferimento all'art. 8 del decreto legge n. 1509 del 1927,  &#13;
 e pertanto valgono per essi le ragioni innanzi esposte.                  &#13;
     Va   comunque  considerato  che  la  legge  del  1960,  riguardante  &#13;
 determinate zone agricole, fu emanata per far fronte  alle  conseguenze  &#13;
 di  calamità  naturali  verificatesi  in quell'anno, e ciò legittima,  &#13;
 come questa Corte ha ritenuto in casi analoghi (sentenze n. 7 del  1956  &#13;
 e n. 8 del 1962), l'eventuale limitazione che da esse possa derivare ai  &#13;
 diritti   dei   proprietari,   anche   in   conformità   al  principio  &#13;
 costituzionale della solidarietà sociale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.   8   del  regio  decreto  legge  29  luglio  1927,  n.  1509  &#13;
 (Provvedimenti per l'ordinamento del credito  agrario),  convertito  in  &#13;
 legge 5 luglio 1928, n. 1760, e successive modificazioni, nonché degli  &#13;
 artt.  15,  primo e secondo comma, e 16, secondo, terzo e quarto comma,  &#13;
 della legge 21 luglio 1960, n. 739 (Provvidenze  per  le  zone  agrarie  &#13;
 danneggiate   da  calamità  naturali  e  provvidenze  per  le  imprese  &#13;
 industriali), proposta con  l'ordinanza  del  tribunale  di  Matera  in  &#13;
 epigrafe indicata, in riferimento all'art. 42 della Costituzione.        &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 27 marzo 1969.                                &#13;
                                   ALDO SANDULLI  -  GIUSEPPE  BRANCA  -  &#13;
                                   MICHELE  FRAGALI - COSTANTINO MORTATI  &#13;
                                   -  GIUSEPPE  CHIARELLI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ERCOLE  ROCCHETTI  - ENZO  &#13;
                                   CAPALOZZA    -    VINCENZO    MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE.</dispositivo>
  </pronuncia_testo>
</pronuncia>
