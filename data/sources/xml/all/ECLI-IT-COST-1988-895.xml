<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>895</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:895</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1988</data_decisione>
    <data_deposito>26/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, &#13;
 dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, &#13;
 prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 205 del d.P.R.    &#13;
 29 dicembre 1973, n. 1092 (Approvazione del testo unico  delle  norme    &#13;
 sul  trattamento di quiescenza dei dipendenti civili e militari dello    &#13;
 Stato), in relazione agli artt. 81 e 114 del d.P.R. 23 dicembre 1978,    &#13;
 n.  915  (Testo  unico delle norme in materia di pensione di guerra),    &#13;
 promosso con ordinanza emessa il 14 maggio 1988 dalla Corte dei Conti    &#13;
 -   Sez.  IV Giurisdizionale -  sui ricorsi riuniti proposti da Pocai    &#13;
 Giovanni,  iscritta  al  n.  1284  del  registro  ordinanze  1984   e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 91-  bis    &#13;
 dell'anno 1985;                                                          &#13;
    Visto l'atto di costituzione di Pocai Giovanni;                       &#13;
    Udito  nella  camera  di  consiglio  del 20 aprile 1988 il Giudice    &#13;
 relatore Francesco Greco.                                                &#13;
    Ritenuto  che  la  Corte  dei  Conti  ha  sollevato  questione  di    &#13;
 legittimità costituzionale dell'art.  205  del  d.P.R.  29  dicembre    &#13;
 1978,   n.  1092  (Approvazione  del  testo  unico  delle  norme  sul    &#13;
 trattamento di quiescenza dei  dipendenti  civili  e  militari  dello    &#13;
 Stato), in relazione agli artt. 81 e 114 del d.P.R. 23 dicembre 1978,    &#13;
 n. 915 (Testo unico delle norme in materia di  pensioni  di  guerra),    &#13;
 nella  parte  in  cui  non  prevede il potere- dovere della Corte dei    &#13;
 Conti di segnalare all'amministrazione i  possibili  casi  di  revoca    &#13;
 della  pensione  privilegiata  e  fissa  il  termine  di tre anni per    &#13;
 l'eliminazione, da  parte  dell'amministrazione  stessa,  degli  atti    &#13;
 viziati  da errore di fatto o assimilabile, in riferimento all'art. 3    &#13;
 Cost.,  verificandosi  disparità  di  trattamento  tra  la  pensione    &#13;
 ordinaria, soggetta alla disciplina di cui alla norma censurata, e la    &#13;
 pensione di guerra, per la quale l'art. 81 del cit. d.P.R. n. 915 del    &#13;
 1978   prevede   l'autotutela   dell'amministrazione   senza   limiti    &#13;
 temporali;                                                               &#13;
      che  si  è  costituita  nel  giudizio dinanzi a questa Corte la    &#13;
 parte privata, la quale  ha  concluso  per  l'inammissibilità  della    &#13;
 questione,   in  quanto  irrilevante  o,  quanto  meno,  per  la  sua    &#13;
 infondatezza;                                                            &#13;
    Considerato   che,   nella   specie,   il   tema   controverso  è    &#13;
 l'attribuzione di una migliore categoria della pensione  già  goduta    &#13;
 dal  ricorrente  (seconda  invece  di  quarta)  e  non  è affatto in    &#13;
 discussione il diritto alla pensione stessa;                             &#13;
      che   l'eventuale   errore   di   fatto   in   cui   è  incorsa    &#13;
 l'amministrazione è stato rilevato solo dalla  Corte  remittente  di    &#13;
 ufficio;                                                                 &#13;
      che, conseguentemente, la questione sollevata non è finalizzata    &#13;
 alla decisione del merito;                                               &#13;
      che, comunque, come più volte questa Corte ha deciso (v. sentt.    &#13;
 nn. 46 del  1979,  97  e  55  del  1980),  sussiste  una  sostanziale    &#13;
 differenza  fra  la  pensione  ordinaria  e la pensione di guerra, la    &#13;
 quale ha finalità risarcitorie, sicché sono giustificate  eventuali    &#13;
 diversità delle rispettive discipline sostanziali;                      &#13;
      che,  nella  specie,  non  si  tratta  di  censure  attinenti al    &#13;
 procedimento di concessione della pensione, ma  all'attribuzione  del    &#13;
 diritto ed al consolidamento della relativa situazione per decorrenza    &#13;
 dei termini di decadenza;                                                &#13;
      che,   pertanto   la   questione   sollevata  è  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 205 del d.P.R. 29  dicembre  1973,  n.  1092    &#13;
 (Approvazione   del  testo  unico  delle  norme  sul  trattamento  di    &#13;
 quiescenza  dei  dipendenti  civili  e  militari  dello  Stato),   in    &#13;
 relazione  agli  artt.  81  e 114 del d.P.R. 23 dicembre 1978, n. 915    &#13;
 (Testo  unico  delle  norme  in  materia  di  pensione  di   guerra),    &#13;
 sollevata, in riferimento all'art. 3 Cost., dalla Corte dei Conti con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 7 luglio 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
