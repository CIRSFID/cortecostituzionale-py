<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>240</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:240</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/04/1989</data_decisione>
    <data_deposito>21/04/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  46, comma    &#13;
 primo, del d.P.R. 29 settembre 1973, n. 600 (Disposizioni  comuni  in    &#13;
 materia  di  accertamento  delle  imposte  sui redditi), promosso con    &#13;
 ordinanza emessa il 1° dicembre 1987 dalla Commissione Tributaria  di    &#13;
 primo grado di Lecce sul ricorso proposto da Schito Benito - curatore    &#13;
 del fallimento Mercurio Pasquale contro l'Ufficio II.DD. di Casarano,    &#13;
 iscritta  al  n.  620  del registro ordinanze 1988 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  46,  prima  serie  speciale,    &#13;
 dell'anno 1988;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  dell'8  marzo 1989 il Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto  che,  durante  il  processo  tributario  instaurato  dal    &#13;
 ricorso del curatore  del  fallimento  Mercurio  Pasquale  contro  il    &#13;
 provvedimento  dell'Ufficio delle imposte dirette di Casarano che gli    &#13;
 ha irrogato la pena pecuniaria prevista dall'art.  46,  primo  comma,    &#13;
 del  d.P.R.  29  settembre  1973,  n.  600,  per omessa dichiarazione    &#13;
 iniziale dei redditi del fallito ai sensi dell'art. 10, quarto comma,    &#13;
 del  medesimo  decreto,  la  Commissione tributaria di primo grado di    &#13;
 Lecce, con ordinanza del 1° dicembre 1987, pervenuta alla Corte il 18    &#13;
 ottobre  1988,  ha sollevato questione di legittimità costituzionale    &#13;
 del citato art. 46, primo comma, "nella parte  in  cui  sanziona  con    &#13;
 identica  pena  pecuniaria,  pari  nel  minimo  a due volte l'imposta    &#13;
 evasa, sia l'omessa dichiarazione di cui all'art. 10  del  d.P.R.  n.    &#13;
 600  del  1973  a  carico  del  curatore  fallimentare,  sia l'omessa    &#13;
 dichiarazione da parte del contribuente ai sensi degli artt. da  1  a    &#13;
 6";                                                                      &#13;
      che, ad avviso del giudice remittente, la norma denunziata viola    &#13;
 il principio di cui all'art. 3 della Costituzione, il quale vieta  di    &#13;
 applicare  un  trattamento uguale a situazioni disuguali, considerato    &#13;
 che il curatore non è soggetto passivo d'imposta e l'omissione della    &#13;
 dichiarazione,  da  parte  sua,  "certamente non è finalizzata a una    &#13;
 evasione d'imposta";                                                     &#13;
      che  nel  giudizio  davanti  alla  Corte non si è costituito il    &#13;
 ricorrente, mentre è intervenuto il  Presidente  del  Consiglio  dei    &#13;
 Ministri, rappresentato dall'Avvocatura dello Stato, chiedendo che la    &#13;
 questione sia dichiarata inammissibile o, in subordine, infondata;       &#13;
      che  in  una  memoria successiva l'Avvocatura ha insistito nella    &#13;
 domanda di infondatezza, sul riflesso che "non esiste un principio di    &#13;
 livello   costituzionale   per  cui  responsabile  per  gli  illeciti    &#13;
 tributari  dovrebbe  essere  solamente  il  soggetto   assunto   come    &#13;
 'contribuente',  e  non anche la persona fisica che nell'interesse di    &#13;
 detto soggetto concretamente opera e trasgredisce";                      &#13;
    Considerato che l'obbligo, autonomo ed esclusivo, di presentare la    &#13;
 dichiarazione dei redditi relativa al periodo  di  imposta  anteriore    &#13;
 alla  data  del  fallimento  e la dichiarazione relativa al risultato    &#13;
 finale della liquidazione è una  funzione  sostitutiva  del  fallito    &#13;
 attribuita  dalla  legge tributaria al curatore nella sua qualità di    &#13;
 organo di giustizia nell'interesse della massa dei creditori;            &#13;
      che,  pertanto,  dell'inadempimento  di  tale obbligo, in quanto    &#13;
 produce    all'Amministrazione     finanziaria     le     conseguenze    &#13;
 pregiudizievoli  in  generale derivanti dall'omessa dichiarazione, il    &#13;
 curatore fallimentare risponde personalmente  alla  medesima  stregua    &#13;
 del  debitore  d'imposta,  onde  non  può certo dirsi irrazionale la    &#13;
 previsione normativa, nei due casi, della medesima sanzione;             &#13;
    Visti  gli  artt.  26  della legge 11 marzo 1953, n. 87, e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 46, primo comma,  del  d.P.R.  29  settembre    &#13;
 1973,  n.  600  (Disposizioni comuni in materia di accertamento delle    &#13;
 imposte sui redditi), sollevata,  in  riferimento  all'art.  3  della    &#13;
 Costituzione,  dalla  Commissione  tributaria di primo grado di Lecce    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 aprile 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 aprile 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
