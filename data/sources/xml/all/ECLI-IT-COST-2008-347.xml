<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>347</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:347</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FLICK</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>20/10/2008</data_decisione>
    <data_deposito>23/10/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Giovanni Maria FLICK; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 33 della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), promossi con ordinanze del 5 ottobre 2007 e dell'8 febbraio 2008 dal Tribunale di Napoli, sezione per il riesame, nei procedimenti penali a carico di P.P. e di L.T.T.F., iscritte ai nn. 3 e 156 del registro ordinanze 2008 e pubblicate nella Gazzetta Ufficiale della Repubblica nn. 7 e 22, prima serie speciale, dell'anno 2008. &#13;
      Visto l'atto di costituzione di L.T.T.F.; &#13;
      udito nella camera di consiglio del 24 settembre 2008 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che, con ordinanza del 5 ottobre 2007, il Tribunale di Napoli ha sollevato, in riferimento agli artt. 3, 13 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 33 della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), nella parte in cui non prevede che il periodo di custodia cautelare scontato all'estero in esecuzione del mandato d'arresto europeo sia computato anche agli effetti della durata dei termini di fase previsti dall'art. 303, commi 1 e 3, del codice di procedura penale; &#13;
    che il rimettente riferisce che, a seguito di mandato di arresto europeo emesso il 28 febbraio 2007 dal giudice per le indagini preliminari del Tribunale di Napoli, P.P. è stato arrestato in Olanda; &#13;
    che, il 23 giugno 2007, il difensore dell'imputato ha chiesto che lo stesso fosse rimesso in libertà per decorrenza dei termini di fase della custodia cautelare, dovendosi a tal fine computare anche il periodo di detenzione sofferta all'estero prima della consegna all'autorità giudiziaria italiana; &#13;
    che detta istanza è stata rigettata dal GIP, in quanto l'art. 33 della legge n. 69 del 2005 prevede che il periodo di custodia cautelare sofferto all'estero in esecuzione del mandato d'arresto europeo sia computato solo ai fini dei termini massimi di fase e non anche dei termini ordinari di fase; &#13;
    che il rimettente, investito del gravame avverso il cennato provvedimento, nel condividere l'interpretazione fornita dal GIP della disposizione censurata, ritiene che essa si pone in contrasto con i principi di eguaglianza e ragionevolezza, avendo la Corte costituzionale, con la sentenza n. 253 del 2004, dichiarato l'illegittimità costituzionale dell'art. 722 del codice di procedura penale, nella parte in cui non prevedeva che il periodo di custodia cautelare sofferto all'estero in conseguenza di una domanda di estradizione presentata dallo Stato, fosse computato anche agli effetti della durata dei termini di fase; &#13;
    che, in particolare, secondo il Tribunale, comune agli istituti dell'estradizione e del mandato di arresto europeo è l'esigenza che i tempi di custodia cautelare non superino i termini stabiliti dal codice di rito, ciò tenuto conto del principio della equivalenza tra la detenzione cautelare all'estero in attesa di estradizione e la custodia cautelare in Italia affermato dalla Corte nella indicata sentenza; &#13;
    che, infine, secondo il giudice a quo la disposizione censurata violerebbe anche il diritto alla libertà personale ed il diritto di difesa; &#13;
    che, in punto di rilevanza, il rimettente osserva che, nel caso in cui la disposizione censurata fosse dichiarata incostituzionale, potrebbe accogliere la richiesta di revoca della misura in atto applicata all'indagato; &#13;
    che questione analoga è sollevata dal Tribunale di Napoli, ottava sezione penale, con ordinanza dell'8 febbraio 2008, con la quale è censurato, in riferimento all'art. 3 della Costituzione, l'art. 33 della legge n. 69 del 2005; &#13;
    che il rimettente è investito, in sede di riesame, del gravame avverso il rigetto dell'istanza, proposta da L.T.T.F., volta ad ottenere la declaratoria di inefficacia della misura cautelare della custodia in carcere disposta nei suoi confronti; &#13;
    che il Tribunale riferisce che il provvedimento impugnato si fonda sulla circostanza che l'indagato è stato consegnato allo Stato italiano in esecuzione di mandato di arresto europeo, di talché il periodo di custodia cautelare da quest'ultimo scontato all'estero deve essere calcolato, a norma dell'art. 33 della legge n. 69 del 2005, solo ai fini del termine di cui all'art. 303, comma 4, del codice di procedura penale; &#13;
    che, secondo il giudice a quo, tale interpretazione risulta l'unica possibile, stante il tenore letterale della disposizione censurata, la quale prevede che il periodo di custodia cautelare sofferto all'estero in esecuzione del mandato d'arresto europeo è computato solo ai sensi e per gli effetti degli artt. 303, comma 4, 304 e 657 del codice di procedura penale; &#13;
    che, pertanto, a parere del rimettente, l'art. 33 della legge n. 69 del 2005, nella parte in cui non prevede che la custodia cautelare all'estero, in esecuzione del mandato d'arresto europeo, sia computata anche agli effetti della durata dei termini di fase previsti dall'art. 303, commi 1, 2 e 3 del codice di procedura penale, viola i principi di razionalità e eguaglianza sanciti dall'art. 3 della Costituzione; &#13;
    che, secondo il Tribunale, la denunciata violazione risulta ancora più palese tenuto conto della sentenza n. 253 del 2004, con la quale la Corte costituzionale ha dichiarato l'incostituzionalità dell'art. 722 del codice di procedura penale, non potendosi non applicare anche alla fattispecie in esame i principi in essa affermati con riferimento al procedimento di estradizione; &#13;
    che il giudice a quo ritiene la questione rilevante, poiché la declaratoria di incostituzionalità della disposizione censurata comporterebbe l'accoglimento del gravame. &#13;
    Considerato che il dubbio di costituzionalità sottoposto a questa Corte ha ad oggetto l'art. 33 della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), nella parte in cui non prevede che il periodo di custodia cautelare all'estero, in esecuzione del mandato d'arresto europeo, sia computato anche agli effetti della durata dei termini di fase previsti dall'art. 303, commi 1, 2 e 3 del codice di procedura penale;  &#13;
    che, stante l'identità delle questioni sollevate, i relativi giudizi vanno riuniti per essere decisi con unica pronuncia; &#13;
    che, successivamente alle ordinanze di rimessione, questa Corte, con sentenza n. 143 del 2008, ha dichiarato l'illegittimità costituzionale dell'art. 33 della legge 22 aprile 2005, n. 69, nella parte in cui non prevede che la custodia cautelare all'estero, in esecuzione del mandato d'arresto europeo, sia computata anche agli effetti della durata dei termini di fase previsti dall'art. 303, commi 1, 2 e 3 del codice di procedura penale;  &#13;
    che, pertanto, alla stregua della richiamata pronuncia di questa Corte, gli atti devono essere restituiti ai giudici rimettenti per un nuovo esame della rilevanza delle questioni.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    ordina la restituzione degli atti al Tribunale di Napoli. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 ottobre 2008.  &#13;
    F.to:  &#13;
    Giovanni Maria FLICK, Presidente  &#13;
    Maria Rita SAULLE, Redattore  &#13;
    Giuseppe DI PAOLA, Cancelliere  &#13;
    Depositata in Cancelleria il 23 ottobre 2008.  &#13;
    Il Direttore della Cancelleria  &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
