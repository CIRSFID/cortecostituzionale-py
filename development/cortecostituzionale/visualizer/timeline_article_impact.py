import json
import os
import re

from cortecostituzionale.visualizer.pronunciation_reader import get_pronunciation_meta_dict

CONSTITUTION_DOC_PATH = "/Users/biagio/desktop/corte_cost_paper/visualizer/cost.json"
PRONUNCIATION_DOC_PATH = "/Users/biagio/repos/cirsfid/cortecostituzionale-py/data/out"  # sys.argv[2]
PLOT_DIR = "plot"

os.makedirs(PLOT_DIR, exist_ok=True)
PANELBAR_PLOT_DIR = os.path.join(PLOT_DIR, 'panel_bar')
os.makedirs(PANELBAR_PLOT_DIR, exist_ok=True)
STACKEDBAR_PLOT_DIR = os.path.join(PLOT_DIR, 'stacked_bar')
os.makedirs(STACKEDBAR_PLOT_DIR, exist_ok=True)

if PRONUNCIATION_DOC_PATH.endswith('.json'):
    with open(PRONUNCIATION_DOC_PATH, 'r') as f:
        pronunciation_meta_dict = json.load(f)
else:
    pronunciation_meta_dict = get_pronunciation_meta_dict(PRONUNCIATION_DOC_PATH)

method_set = set()
article_reference_per_year = {}
for k, v in pronunciation_meta_dict.items():
    year = v["decision_date"]['year']
    if year not in article_reference_per_year:
        article_reference_per_year[year] = {}
    method = v['pronunciation_method']
    method_set.add(method)
    article_reference_dict = article_reference_per_year[year]
    for reference, count in v['constitution_article_reference_counter'].items():
        article_id = reference.split('_')[1]
        if re.search(r'[a-zA-Z]', article_id) is not None:
            continue
        paragraph_id = reference.split('para_')[-1] if 'para' in reference else None
        if article_id not in article_reference_dict:
            article_reference_dict[article_id] = {}
        reference_dict = article_reference_dict[article_id]
        if method not in reference_dict:
            reference_dict[method] = 0
        reference_dict[method] += count

#################################################################################
######################## Stacked barplot with matplotlib ########################
#################################################################################
# https://python-graph-gallery.com/12-stacked-barplot-with-matplotlib/

# libraries
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec

barWidth = 1
for year, article_reference_dict in article_reference_per_year.items():
    article_groups_dict = {
        article: [
            method_dict.get(method, 0)
            for method in method_set
        ]
        for article, method_dict in article_reference_dict.items()
    }

    try:
        article_list, groups_list = zip(*sorted(article_groups_dict.items(), key=lambda x: int(x[0])))
    except:
        continue
    # Values of each group
    bars = zip(*groups_list)
    bars_method = sorted(zip(bars, method_set), key=lambda x: sum(x[0]))
    bars, methods = zip(*filter(lambda x: sum(x[0]) > 0, bars_method))
    # max_height = max(np.sum(bars,axis=0))
    # bars = list(map(lambda x:x/max_height, bars))

    # The position of the bars on the x-axis
    r = list(range(len(article_list)))
    ncols = 1
    nrows = len(bars)

    # Panel Bar Chart
    plt.title(year)
    cmap = sns.color_palette("cubehelix", nrows)
    figure = Figure(figsize=(20 * ncols, 7 * nrows))
    canvas = FigureCanvas(figure)
    grid = GridSpec(ncols=ncols, nrows=nrows)
    axes = []
    for i, b in enumerate(bars):
        ax = figure.add_subplot(grid[i // ncols, i % ncols])
        axes.append(ax)
        ax.bar(r, b, width=barWidth, color=cmap[i], edgecolor='white')
        ax.set_title(methods[i])
        ax.set_ylabel('References')
        ax.set_xlabel('Article')
        # ax.set_yticks(b)
        ax.set_xticks(r)
        ax.set_xticklabels(article_list, fontweight='bold', rotation=90)
        ax.grid(True)
    figure.savefig(os.path.join(PANELBAR_PLOT_DIR, f"year_{year}.png"), bbox_inches='tight')

    # Stacked Bar Chart
    plt.figure(figsize=(20, 8))
    plt.xlabel('Article')
    plt.ylabel('References')
    plt.title(year)
    for i, b in enumerate(bars):
        if i == 0:
            plt.bar(r, b, width=barWidth, color=cmap[i], edgecolor='white')
        else:  # Create green bars (middle), on top of the firs ones
            bottom = np.sum(bars[:i], axis=0).tolist()
            plt.bar(r, b, bottom=bottom, width=barWidth, color=cmap[i], edgecolor='white')
    plt.xticks(r, article_list, fontweight='bold', rotation=90)
    plt.legend(
        (Line2D([0], [0], color=c, lw=4) for c in cmap),
        methods
    )
    plt.savefig(os.path.join(STACKEDBAR_PLOT_DIR, f"year_{year}.png"), bbox_inches='tight')
