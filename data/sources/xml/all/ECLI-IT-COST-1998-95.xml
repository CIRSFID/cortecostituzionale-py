<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>95</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:95</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/03/1998</data_decisione>
    <data_deposito>01/04/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 156 del d.lgs.    &#13;
 28 luglio 1989, n. 271  (Norme  di  attuazione,  di  coordinamento  e    &#13;
 transitorie  del  codice di procedura penale), promosso con ordinanza    &#13;
 emessa il 7 aprile 1997  dal  giudice  per  le  indagini  preliminari    &#13;
 presso  la  pretura  di  Modena,  iscritta  al  n.  644  del registro    &#13;
 ordinanze 1997 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 41, prima serie speciale, dell'anno 1997;                             &#13;
   Udito nella camera di  consiglio  dell'11  marzo  1998  il  giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
   Ritenuto  che  il  giudice  per  le  indagini preliminari presso la    &#13;
 pretura di Modena, dopo aver premesso di essere  stato  investito  di    &#13;
 una  richiesta  di  archiviazione  in relazione alla quale la persona    &#13;
 offesa ha proposto opposizione, ha  sollevato,  in  riferimento  agli    &#13;
 artt.  3,  primo  comma,  e  24,  secondo  comma, della Costituzione,    &#13;
 questione di legittimità costituzionale dell'art. 156 del d.lgs.  28    &#13;
 luglio  1989,  n.  271  (Norme  di  attuazione,  di  coordinamento  e    &#13;
 transitorie del codice di procedura penale), nella parte in  cui  non    &#13;
 prevede  che,  nel  procedimento  davanti  al  pretore,  il  pubblico    &#13;
 ministero  debba   notificare   l'opposizione   alla   richiesta   di    &#13;
 archiviazione  formulata  dalla  persona  offesa - ovvero almeno dare    &#13;
 notizia dell'avvenuta proposizione dell'opposizione  -  alla  persona    &#13;
 sottoposta  alle  indagini  preliminari, con contestuale avvertimento    &#13;
 che,  nel  termine  di  dieci  giorni   dalla   notificazione   della    &#13;
 opposizione  -  o  dalla  comunicazione  della sua proposizione -, la    &#13;
 persona sottoposta alle indagini può  prendere  visione  degli  atti    &#13;
 depositati  presso  la  procura della Repubblica e presentare proprie    &#13;
 deduzioni  difensive,  anche  con  richiesta  motivata  di   indagini    &#13;
 preliminari  su fatti e circostanze ad essa favorevoli, nonché nella    &#13;
 parte in cui non dispone che, qualora sia stata proposta  opposizione    &#13;
 alla  richiesta di archiviazione, il pubblico ministero trasmetta gli    &#13;
 atti al giudice per le indagini  preliminari  dopo  la  presentazione    &#13;
 delle  deduzioni  difensive  della  persona  sottoposta  ad indagini,    &#13;
 ovvero dopo la scadenza del termine  concesso  per  la  presentazione    &#13;
 delle   sue  deduzioni,  analogamente  a  quanto  stabilito,  per  il    &#13;
 procedimento davanti al tribunale, dall'art. 126 del medesimo  d.lgs.    &#13;
 n. 271 del 1989;                                                         &#13;
     che  a parere del giudice a quo la disciplina censurata determina    &#13;
 una  palese  disparità  di  trattamento  tra  la  persona  offesa  e    &#13;
 l'indagato,  in  quanto,  mentre  alla  persona  offesa è consentito    &#13;
 replicare alla richiesta di archiviazione del  pubblico  ministero  a    &#13;
 tutela  del proprio interesse al perseguimento del reato del quale è    &#13;
 stata vittima, la  persona  nei  cui  confronti  si  è  proceduto  a    &#13;
 indagini  non  è  posta  in condizione di contrastare quanto dedotto    &#13;
 dall'opponente;                                                          &#13;
     che l'impossibilità di conoscere e di contrastare  l'opposizione    &#13;
 si  risolve  anche,  ad  avviso  del  rimettente,  in una lesione del    &#13;
 diritto di difesa per l'indagato "nella fase cruciale della  chiusura    &#13;
 delle   indagini   preliminari",  giacché  a  questi  devono  essere    &#13;
 assicurate garanzie non inferiori a quelle  riconosciute  alla  parte    &#13;
 lesa.                                                                    &#13;
   Considerato  che  questa  Corte  ha  avuto modo di affermare che il    &#13;
 problema dell'archiviazione sta nell'evitare  il  processo  superfluo    &#13;
 senza  eludere  il principio di obbligatorietà dell'azione penale ed    &#13;
 anzi controllando caso per caso la legalità dell'inazione, onde  far    &#13;
 sì  che  i  processi  concretamente non instaurati siano solo quelli    &#13;
 risultanti  effettivamente  superflui,  sicché  gli   strumenti   di    &#13;
 verifica  del  giudice  in ordine all'attività omissiva del pubblico    &#13;
 ministero  devono  essere  articolati  in  modo tale "da fornirgli la    &#13;
 possibilità di contrastare le inerzie e le lacune  investigative  di    &#13;
 quest'ultimo  ed  evitare che le sue scelte si traducano in esercizio    &#13;
 discriminatorio dell'azione (o inazione) penale" (v. sentenza  n.  88    &#13;
 del 1991);                                                               &#13;
     che   essendo,   dunque,   il   controllo  del  giudice  volto  a    &#13;
 salvaguardare nel procedimento di archiviazione il pieno rispetto del    &#13;
 principio di obbligatorietà  dell'azione  penale  e  dei  valori  di    &#13;
 legalità  ed  uguaglianza  che  esso  coinvolge, ne deriva che anche    &#13;
 l'opposizione della persona offesa non può  non  risultare  iscritta    &#13;
 nel  medesimo  alveo  finalistico,  sicché  le  relative deduzioni e    &#13;
 prospettive  di  ulteriori   indagini   -   ora   profilabili   anche    &#13;
 nell'archiviazione pretorile a seguito della sentenza n. 445 del 1990    &#13;
 -  si  configurano come strumenti di tutela dell'offeso negli stretti    &#13;
 limiti in cui ciò risponda alla funzione di verifica che il  giudice    &#13;
 è  chiamato  ad  esercitare  a  salvaguardia  del  precetto  sancito    &#13;
 dall'art. 112 della Carta fondamentale;                                  &#13;
     che  pertanto,  avendo  l'ordinamento  configurato  lo   ius   ad    &#13;
 loquendum dell'opponente come istituto strumentalmente correlato alla    &#13;
 ratio  del  controllo  giurisdizionale  costituzionalmente imposto in    &#13;
 sede di  archiviazione,  nessuna  assimilabilità  può  prospettarsi    &#13;
 rispetto  alla posizione della persona già sottoposta alle indagini,    &#13;
 essendo  quest'ultima  portatrice  di  diritti  non   coinvolti   dal    &#13;
 parametro che quel controllo mira a presidiare;                          &#13;
     che,  quindi,  stante  l'evidente  eterogeneità delle situazioni    &#13;
 poste a raffronto, la disciplina  oggetto  di  impugnativa  non  può    &#13;
 ritenersi  in  contrasto con il principio di uguaglianza, così come,    &#13;
 di riflesso, nessuna violazione appare subire il diritto  di  difesa,    &#13;
 considerato   che,   attesa   la  peculiare  natura  e  funzione  del    &#13;
 procedimento  di  archiviazione,  l'eventuale  "contraddittorio"  con    &#13;
 l'opponente  non  può  certo  configurarsi alla stregua di soluzione    &#13;
 costituzionalmente obbligata;                                            &#13;
     che,  infine,   neppure   evocabile   è   il   diverso   modello    &#13;
 procedimentale  stabilito  nel  caso di opposizione alla richiesta di    &#13;
 archiviazione per i reati di competenza del tribunale o  della  corte    &#13;
 di  assise,  in quanto, come questa Corte ha più volte rilevato, nel    &#13;
 procedimento pretorile il principio di "massima  semplificazione"  di    &#13;
 cui  alla  direttiva  n. 103 della legge-delega n. 81 del 16 febbraio    &#13;
 1987 consente di giustificare, insieme con l'esclusione  dell'udienza    &#13;
 preliminare,  anche  l'assenza  del  rito  camerale  nell'ipotesi  di    &#13;
 opposizione della persona offesa  alla  richiesta  di  archiviazione,    &#13;
 prevedendo "per questa fase del procedimento la sola contrapposizione    &#13;
 tra  due  atti  formali quali la richiesta di archiviazione formulata    &#13;
 dal pubblico ministero e  l'opposizione  a  tale  richiesta  avanzata    &#13;
 dalla   parte  interessata  alla  prosecuzione  delle  indagini"  (v.    &#13;
 ordinanza n. 291 del 1993 e sentenza n. 123 del 1993);                   &#13;
     che di conseguenza la questione proposta deve  essere  dichiarata    &#13;
 manifestamente infondata.                                                &#13;
   Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 156 del d.lgs. 28 luglio 1989, n. 271 (Norme    &#13;
 di attuazione, di coordinamento e transitorie del codice di procedura    &#13;
 penale), sollevata, in riferimento agli artt. 3, primo comma,  e  24,    &#13;
 secondo  comma,  della  Costituzione,  dal  giudice  per  le indagini    &#13;
 preliminari presso la pretura di Modena con l'ordinanza in epigrafe.     &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 25 marzo 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Vassalli                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 1 aprile 1998.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
