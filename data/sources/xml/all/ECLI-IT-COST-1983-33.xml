<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>33</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:33</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>DE STEFANO</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/01/1983</data_decisione>
    <data_deposito>22/02/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ANTONINO DE STEFANO, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. &#13;
 BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO &#13;
 PALADIN - Dott. ARNALDO MACCARONE - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. &#13;
 ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  7,  comma  &#13;
 secondo,  n. 2, della legge 14 agosto 1971, n. 817 (Disposizioni per il  &#13;
 rifinanziamento delle provvidenze  per  lo  sviluppo  della  proprietà  &#13;
 coltivatrice)  promosso  con  ordinanza,  emessa  il  5  marzo 1976 dal  &#13;
 Tribunale di Pinerolo nel procedimento civile  vertente  tra  Vaglienti  &#13;
 Francesco  Giuseppe  e Rollé Giuseppe ed altri, iscritta al n. 388 del  &#13;
 registro ordinanze 1976 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 164 del 23 giugno 1976.                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  dell'11  gennaio  1983  il   Giudice  &#13;
 relatore Virgilio Andrioli;                                              &#13;
     udito  l'avvocato  dello  Stato Benedetto Baccari per il Presidente  &#13;
 del Consiglio dei ministri.                                              &#13;
      Ritenuto che: 1.  -  Con  atto  di  citazione,  notificato  il  23  &#13;
 settembre   1975,  Vaglienti  Francesco  Giuseppe  convenne  avanti  il  &#13;
 Tribunale di Pinerolo Rollé Giuseppe, Rostagno Francesca in  Rollé  e  &#13;
 Rollé  Andrea esponendo che con atto 9 ottobre 1974 per notar Galleano  &#13;
 Rollé Giuseppe, Rostagno Francesca in Rollé e Rollé  Andrea  avevano  &#13;
 acquistato  da  Barberis  Rosa  Lucia  in  Renaldo  due appezzamenti di  &#13;
 terreno siti in Vigone, e  che  i  terreni  confinavano  con  altri  di  &#13;
 proprietà dell'attore, che allo stesso attore non era stata notificata  &#13;
 la  proposta  di  alienazione e, pertanto, non gli si era consentito di  &#13;
 esercitare il diritto di prelazione, che pur gli competeva  per  essere  &#13;
 coltivatore  diretto,  e, dato atto che offriva a borsa aperta la somma  &#13;
 di lire 9.000.000 e si  riservava  di  effettuarne  il  versamento  nei  &#13;
 termini  di  legge,  chiese  dichiararsi  il riscatto, a favore di esso  &#13;
 attore, dei due terreni ordinando ai convenuti di comparire  avanti  il  &#13;
 notaio  eligendo  entro  breve  termine  per  sottoscrivere  l'atto  di  &#13;
 trasferimento - in difetto di che l'emananda  sentenza  terrebbe  luogo  &#13;
 dell'atto stesso - .                                                     &#13;
     che: 2. - Nel contraddittorio dei convenuti i quali, nella comparsa  &#13;
 7  novembre  1975,  opposero che i terreni in oggetto, con scrittura 23  &#13;
 giugno 1974, erano stati concessi in affitto a  Mellica  Sergio  e  che  &#13;
 difettava  al Vaglienti e alla moglie la capacità di lavoro, richiesta  &#13;
 in chi eserciti  il  diritto  di  prelazione,  l'adito  Tribunale,  con  &#13;
 ordinanza  emessa  il 5 marzo 1976, notificata il 12 e comunicata il 16  &#13;
 dello stesso mese, pubblicata nella G. U. n. 164 del 23 giugno  1976  e  &#13;
 iscritta al n. 388 R.O. 1976 - premesso che l'art. 7 comma secondo n. 2  &#13;
 l.  14  agosto  1971  n. 817 (Disposizioni per il rifinanziamento delle  &#13;
 provvidenze per lo sviluppo della proprietà coltivatrice), così  come  &#13;
 costantemente   interpretato,   attribuisce   al   coltivatore  diretto  &#13;
 proprietario di terreni confinanti il diritto di  prelazione  su  fondi  &#13;
 offerti in vendita nella sola ipotesi che gli stessi siano direttamente  &#13;
 condotti  dal  venditore  e non nella ipotesi in cui vi siano insediati  &#13;
 mezzadri, coloni, affittuari, compartecipanti o  enfiteuti  coltivatori  &#13;
 diretti  -  considerava  che l'art. 7 comma secondo, n. 2, in tal guisa  &#13;
 interpretato, fosse costituzionalmente illegittimo nella parte  in  cui  &#13;
 non   concede   il   diritto   di  prelazione  al  coltivatore  diretto  &#13;
 proprietario di terreni confinanti con i fondi in vendita  anche  se  i  &#13;
 mezzadri,  coloni,  affittuari,  compartecipanti  o coltivatori diretti  &#13;
 insediati sui detti fondi offerti in vendita non  intendano  esercitare  &#13;
 il  diritto  di  prelazione,  individuava  i  parametri del giudizio di  &#13;
 costituzionalità a) nell'art. 3 Cost.  che  sarebbe  offeso  a  motivo  &#13;
 della  diseguaglianza,  che  dalla  normativa impugnata riverrebbe, tra  &#13;
 proprietario che conduce direttamente il fondo e  proprietario  che  vi  &#13;
 abbia    insediato   mezzadri,   affittuari   ecc.,   né   riceverebbe  &#13;
 giustificazione in una scala di  priorità  nelle  scelte  di  politica  &#13;
 agraria  tendenti  a favorire l'accesso alla proprietà della terra per  &#13;
 coloro che già si trovano sui fondi, in quanto  il  presupposto  della  &#13;
 sospettata  incostituzionalità  consiste  proprio  nel  fatto che essi  &#13;
 abbiano rinunciato alla prelazione ai medesimi giustamente spettante in  &#13;
 via prioritaria, e b) nell'art. 44 Cost. che sarebbe offeso dalla norma  &#13;
 impugnata in quanto questa in caso di presenza sul fondo  di  mezzadri,  &#13;
 affittuari,  ecc., e di rinuncia, da parte loro, alla prelazione, priva  &#13;
 di  analogo  diritto  il  confinante  coltivatore  diretto   abilitando  &#13;
 l'alienante  a  vendere  liberamente ad un terzo che non sia confinante  &#13;
 né  coltivatore  diretto;  ravvisava  la  rilevanza   della   proposta  &#13;
 questione in ciò che la normativa "de qua" importerebbe, nella specie,  &#13;
 la reiezione della domanda, dall'attore coltivatore diretto intesa alla  &#13;
 declaratoria  di  validità  ed  efficacia  dell'esercitato  diritto di  &#13;
 riscatto, laddove la dichiarazione  di  incostituzionalità  nel  senso  &#13;
 prospettato dal Tribunale ne comporterebbe l'accoglimento nei confronti  &#13;
 dei terzi acquirenti.                                                    &#13;
     che:  3. - Avanti la Corte nessuna delle parti si è costituita; ha  &#13;
 invece spiegato intervento il Presidente del Consiglio dei ministri con  &#13;
 atto depositato il 13 luglio 1976 nel quale l'Avvocatura generale dello  &#13;
 Stato ha argomentato per l'infondatezza della questione nel  senso  che  &#13;
 I)  la ipotizzata diseguaglianza di trattamento, ai fini dell'esercizio  &#13;
 del diritto di prelazione, tra il  proprietario  di  un  fondo  che  lo  &#13;
 conduca  direttamente,  e  il  proprietario  del  fondo  su  cui  siano  &#13;
 insediati mezzadri, coloni,  affittuari,  compartecipanti  o  enfiteuti  &#13;
 coltivatori   diretti,   si   ricollega   a  situazioni  obiettivamente  &#13;
 differenziate, razionalmente prese in  considerazione  dal  legislatore  &#13;
 nell'ambito  di  una  scelta di politica agraria finalizzata a tutelare  &#13;
 l'interesse del coltivatore - quale affttuario, mezzadro,  ecc.  -  sul  &#13;
 fondo  oggetto  di vendita a terzi, anche nei confronti del coltivatore  &#13;
 confinante, che può essere disposto a corrispondere il prezzo più che  &#13;
 congruo sulla base del quale  il  coltivatore  insediato  potrebbe  non  &#13;
 avere  inteso esercitare il diritto di prelazione riconosciutogli dalla  &#13;
 legge,   II)   l'interesse   del   coltivatore   insediato   è   stato  &#13;
 ragionevolmente considerato meritevole di speciale tutela tenendo conto  &#13;
 che,  ove  al  lavoratore  confinante  fosse  consentito  esercitare il  &#13;
 diritto di prelazione rinunciato dal  coltivatore  insediato  medesimo,  &#13;
 questi  si  vedrebbe  costretto  a  lasciare  il  fondo  (e,  quindi, a  &#13;
 rinunciare alla propria attività di lavoro)  e  ciò  in  applicazione  &#13;
 dell'art. 3 l. 28 marzo 1957 n. 244.                                     &#13;
     che:  4.  - Alla pubblica udienza dell'11 gennaio 1983, nella quale  &#13;
 il giudice Andrioli ha svolto la relazione, l'avv. dello Stato  Baccari  &#13;
 ha illustrato la conclusione d'infondatezza della proposta questione.    &#13;
     che:  5.  -  Il  Tribunale  di  Pinerolo  ha  ritenuto  rilevante e  &#13;
 giudicato non manifestamente infondato "il  dubbio  sulla  legittimità  &#13;
 costituzionale  dell'art.  7  comma  secondo  l. 14 agosto 1971 n. 817,  &#13;
 nella parte in cui esclude che  spetti  il  diritto  di  prelazione  in  &#13;
 favore  del  coltivatore diretto proprietario di terreni confinanti con  &#13;
 fondi offerti in vendita ove sugli  stessi  siano  insediati  mezzadri,  &#13;
 coloni,  affittuari,  compartecipanti,  o enfiteuti coltivatori diretti  &#13;
 anche nell'ipotesi che questi non  abbiano  esercitato  il  diritto  di  &#13;
 prelazione,  in  riferimento  all'art. 3 Cost., per la diseguaglianza -  &#13;
 non giustificata da plausibile motivo, ma fondata  su  una  irrilevante  &#13;
 differenza  di  situazioni  -  di trattamento fra il proprietario di un  &#13;
 fondo che lo conduce direttamente (tenuto a rispettare nella vendita il  &#13;
 diritto di prelazione verso i  coltivatori  diretti  confinanti)  e  il  &#13;
 proprietario  del fondo su cui siano insediati coltivatori diretti (non  &#13;
 tenuto a rispettare il diritto di prelazione)  nonché  in  riferimento  &#13;
 all'art.    44  Cost.  perché  viene  limitata la ricostituzione delle  &#13;
 unità produttive nelle ipotesi in cui sul fondo in  vendita  vi  siano  &#13;
 mezzadri,  coloni,  affittuari, compartecipanti o enfiteuti coltivatori  &#13;
 diretti i quali non siano interessati all'acquisto".                     &#13;
     che: 6. - Nelle more dell'incidente è entrata in vigore  la  l.  3  &#13;
 maggio  1982 n. 203 (Norme sui contratti agrari), la quale per un verso  &#13;
 non prevede il diritto di prelazione in  riferimento  al  fondo  ma  lo  &#13;
 limita  alle  scorte  nelle  ipotesi descritte nell'art. 35 e per altro  &#13;
 verso dispone all'art. 53 che la legge si applica a tutti  i  rapporti,  &#13;
 comunque in corso, anche se oggetto di controversie che non siano state  &#13;
 definite  con  sentenza passata in giudicato, salvo che la sentenza sia  &#13;
 già  esecutiva,  oppure  con  transazione  stipulata  in   conformità  &#13;
 dell'art.  23 l. 11/ 1971, ad eccezione di quanto disposto nell'art. 42  &#13;
 comma primo (diritto di ripresa) della  stessa  legge,  e  all'art.  58  &#13;
 comma  secondo  che  le disposizioni incompatibili con quelle contenute  &#13;
 nella nuova legge sono abrogate.                                         &#13;
     che: 7. - Poiché la Corte non ha  notizia  (né  è  in  grado  di  &#13;
 acquisire conoscenza) di sentenze, comunque esecutive, o di transazioni  &#13;
 rese  le  une  e  consentite  le  altre  nelle  more  dell'incidente e,  &#13;
 comunque, non consta di interpretazioni giudiziali e dottrinali sul  se  &#13;
 la mancata considerazione, nella nuova legge, del diritto di prelazione  &#13;
 relativo  al  fondo  sia  da  intendere  quale esclusione del medesimo,  &#13;
 s'impone la restituzione degli atti al giudice a quo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione degli atti al Tribunale di Pinerolo che ha  &#13;
 sollevato la questione di legittimità costituzionale dell'art. 7 comma  &#13;
 secondo n. 2 l. 14 agosto 1971 n. 817, in riferimento agli artt. 3 e 44  &#13;
 Cost., con ordinanza 5 marzo 1976 (n. 388 R.O.  1976).                   &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 28 gennaio 1983.        &#13;
                                   F.to:  ANTONINO  DE STEFANO - MICHELE  &#13;
                                   ROSSANO  -  GUGLIELMO   ROEHRSSEN   -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN   -   ARNALDO   MACCARONE   -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ETTORE GALLO.                          &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
