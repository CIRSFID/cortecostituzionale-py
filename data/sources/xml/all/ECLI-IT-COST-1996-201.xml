<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>201</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:201</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MENGONI</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/06/1996</data_decisione>
    <data_deposito>17/06/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Luigi MENGONI; &#13;
 Giudici: prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 309, commi 8, 9    &#13;
 e 10, del codice di procedura penale e dell'art. 13 del decreto-legge    &#13;
 20 novembre 1991, n. 367, convertito con modificazioni nella legge 20    &#13;
 gennaio 1992, n. 8 (Coordinamento delle indagini nei procedimenti per    &#13;
 reati  di criminalità organizzata), promosso con ordinanza emessa il    &#13;
 25 agosto 1995 dal Tribunale di Catanzaro sulle richieste riunite  di    &#13;
 riesame  proposte  da  Farao  Silvio  ed altri iscritta al n. 937 del    &#13;
 registro ordinanze 1995 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 3, prima serie speciale, dell'anno 1996;                   &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio  del  15  maggio  1996  il  giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
   Ritenuto  che nel corso dei procedimenti penali riuniti a carico di    &#13;
 Farao Silvio e altri 149  indagati,  scaturiti  da  diversi  atti  di    &#13;
 indagine  che avevano condotto all'emissione di ordinanze di custodia    &#13;
 cautelare nei loro confronti, il Tribunale di Catanzaro, in  sede  di    &#13;
 riesame  di  tali provvedimenti, con ordinanza del 25 agosto 1995, ha    &#13;
 sollevato, in riferimento agli artt. 3,  24,  secondo  comma,  e  97,    &#13;
 primo   comma,   della   Costituzione,   questioni   di  legittimità    &#13;
 costituzionale dell'art.  309,  commi  8,  9  e  10,  del  codice  di    &#13;
 procedura  penale, e dell'art. 13 del decreto-legge 20 novembre 1991,    &#13;
 n. 367, convertito con modificazioni nella legge 20 gennaio 1992,  n.    &#13;
 8  (Coordinamento  delle  indagini  nei  procedimenti  per  reati  di    &#13;
 criminalità organizzata);                                               &#13;
     che  il  giudice   rimettente   richiama   il   contenuto   delle    &#13;
 disposizioni  impugnate,  che  disciplinano le modalità del deposito    &#13;
 degli atti relativi al riesame, fissano il termine  di  dieci  giorni    &#13;
 per  la  decisione - la cui violazione è sanzionata dalla perdita di    &#13;
 efficacia dei  provvedimenti  impugnati  -  e  prevedono  aumenti  di    &#13;
 organico del personale giudiziario;                                      &#13;
     che  lo  stesso  giudice  -  pur dando atto che questa Corte, con    &#13;
 l'ordinanza n. 126 del 1993, ha dichiarato  manifestamente  infondata    &#13;
 una   analoga  questione  di  costituzionalità  -  osserva  che  gli    &#13;
 adempimenti che il giudice del  riesame  è  tenuto  a  compiere  nel    &#13;
 termine   indicato  evidenziano  l'irragionevolezza  della  normativa    &#13;
 impugnata nonché la  violazione  del  principio  di  buon  andamento    &#13;
 dell'amministrazione  della  giustizia  e  del diritto di difesa, dal    &#13;
 momento che, specialmente nell'ipotesi di procedimenti  con  numerosi    &#13;
 indagati,  risulta  "impossibile  assicurare il serio esercizio della    &#13;
 funzione giurisdizionale ...    sia  in  relazione  al  numero  delle    &#13;
 posizioni da riesaminare che alla complessità delle indagini", anche    &#13;
 a causa delle carenze di mezzi e di personale che risultano aggravate    &#13;
 dal  mancato  aumento degli organici dei giudici addetti ai tribunali    &#13;
 che hanno sede nei capoluoghi dei distretti,  esclusi  dagli  aumenti    &#13;
 previsti dall'art. 13 del decreto-legge n. 367 del 1991;                 &#13;
     che,  pertanto,  il  giudice  rimettente  richiede alla Corte una    &#13;
 pronuncia di illegittimità delle disposizioni impugnate nelle  parti    &#13;
 in  cui  non  prevedono  la possibilità di concessione di un congruo    &#13;
 termine  a  difesa,  nonché  "un  adeguato  rinforzo  di   personale    &#13;
 giudiziario";                                                            &#13;
     che  nel giudizio davanti alla Corte è intervenuto il Presidente    &#13;
 del Consiglio dei ministri, rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale  dello  Stato,  per chiedere che la questione sia dichiarata    &#13;
 infondata;                                                               &#13;
   Considerato che questa Corte, con l'ordinanza n. 126 del  1993,  ha    &#13;
 già  dichiarato manifestamente infondata, in riferimento ai medesimi    &#13;
 parametri costituzionali invocati nel presente giudizio, la questione    &#13;
 di legittimità costituzionale dell'art.  309,  commi  9  e  10,  del    &#13;
 codice  di  procedura  penale,  affermando  che  la  previsione di un    &#13;
 termine  perentorio  breve  per  l'adozione  della  decisione   sulla    &#13;
 richiesta  di  riesame:    a)  "non  può dirsi lesiva del diritto di    &#13;
 difesa sancito dall'art.   24 della  Costituzione,  ma  realizza,  al    &#13;
 contrario,  una  forma  di  tutela  del  soggetto che ha impugnato il    &#13;
 provvedimento  evitando  che  questi  possa  essere  in  alcun   modo    &#13;
 danneggiato da inadempienze o ritardi dell'autorità giudiziaria"; b)    &#13;
 "non  si  presenta  irragionevole  in  relazione  alle operazioni che    &#13;
 devono essere svolte dal giudice  del  riesame,  tanto  più  ove  si    &#13;
 consideri  che  le norme impugnate hanno considerevolmente elevato il    &#13;
 termine di tre giorni (prorogabile per altri tre) già  previsto  per    &#13;
 la procedura di riesame dall'art. 263-ter c.p.p. abrogato";              &#13;
     che,  in  ordine  a  tali  censure, l'ordinanza di rimessione non    &#13;
 prospetta  argomentazioni  sostanzialmente  diverse  da  quelle  già    &#13;
 esaminate dalla Corte nell'ordinanza n. 126 del 1993, e, pertanto, la    &#13;
 questione relativa deve essere dichiarata manifestamente infondata;      &#13;
     che,  con  riferimento  all'art.  13 del decreto-legge n. 367 del    &#13;
 1991, convertito nella legge n. 8 del 1992, il giudice  rimettente  -    &#13;
 censurando "la mancata previsione (se non di un ufficio distrettuale)    &#13;
 di  un  adeguato  rinforzo  di  personale giudiziario" - richiede una    &#13;
 pronuncia addittiva in materia di  organizzazione  giudiziaria  e  di    &#13;
 attribuzione dei posti di organico agli uffici giudiziari;               &#13;
     che  l'adozione di tale decisione non rientra nella competenza di    &#13;
 questa Corte, dal momento che la scelta tra  le  possibili  soluzioni    &#13;
 organizzative  relative  all'allocazione  dei  mezzi  e del personale    &#13;
 presso  gli  uffici  giudiziari  risulta  riservata  alla  sfera   di    &#13;
 discrezionalità del legislatore;                                        &#13;
     che,  pertanto,  la  questione di costituzionalità sollevata nei    &#13;
 confronti  dell'art.  13  del  decreto-legge  n.  367  del  1991   va    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 309, commi 8, 9 e 10 del codice di procedura    &#13;
 penale, sollevata, in riferimento agli artt. 3, 24, secondo comma,  e    &#13;
 97,  primo  comma, della Costituzione, dal Tribunale di Catanzaro con    &#13;
 l'ordinanza indicata in epigrafe;                                        &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità   costituzionale   dell'art.  13  del  decreto-legge  20    &#13;
 novembre 1991, n.  367, convertito nella legge 20 gennaio 1992, n.  8    &#13;
 (Coordinamento   delle   indagini   nei  procedimenti  per  reati  di    &#13;
 criminalità organizzata), sollevata, in riferimento  agli  artt.  3,    &#13;
 24,  secondo  comma,  e  97,  primo  comma,  della  Costituzione  dal    &#13;
 Tribunale  di  Catanzaro  con  la  medesima  ordinanza  indicata   in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 10 giugno 1966.                               &#13;
                        Il Presidente: Mengoni                            &#13;
                          Il redattore: Cheli                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 17 giugno 1996.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
