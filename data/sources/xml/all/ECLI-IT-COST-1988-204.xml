<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>204</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:204</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1988</data_decisione>
    <data_deposito>18/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 92 della legge    &#13;
 24 novembre 1981 n. 689 (modifiche al sistema penale),  promossi  con    &#13;
 ordinanze  emesse  il 20 maggio 1983 e il 3 febbraio 1984 dal Giudice    &#13;
 Istruttore presso il Tribunale di Novara, iscritta ai nn.91 e 466 del    &#13;
 registro  ordinanze  1984 e pubblicate nella Gazzetta Ufficiale della    &#13;
 Repubblica nn. 169 e 259 dell'anno 1984;                                 &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 27 gennaio 1988 il Giudice    &#13;
 relatore Ettore Gallo;                                                   &#13;
    1.  -  Ritenuto,  in  fatto,  che  il Giudice Istruttore presso il    &#13;
 Tribunale di Novara e lo stesso Tribunale di Novara,  rispettivamente    &#13;
 con    ordinanze   20   maggio   e   3   febbraio   1984,   investiti    &#13;
 dall'impugnazione  del  Pubblico  Ministero  rispettivamente   contro    &#13;
 sentenza  istruttoria del Pretore di Novara e sentenza dibattimentale    &#13;
 del Pretore di Borgomanero, che  avevano  prosciolto  per  remissione    &#13;
 della  querela  i  rispettivi  imputati  di  lesioni  colpose  gravi,    &#13;
 sollevavano questione di  legittimità  costituzionale  dell'art.  92    &#13;
 della  l.  24 novembre 1981 n. 689, con riferimento agli artt. 2, 3 e    &#13;
 32 Cost.:                                                                &#13;
      che  nelle rispettive ordinanze, identiche nel testo per forma e    &#13;
 sostanza,  sostengono   i   giudici   rimettenti   che   il   diritto    &#13;
 all'integrità  fisica,  almeno  nei limiti previsti dall'art. 5 cod.    &#13;
 civ., è diritto indisponibile, come tale tutelato dalla Costituzione    &#13;
 negli  artt.  2  e  32 quale interesse della collettività, e perciò    &#13;
 insuscettibile di essere abbandonato alla discrezione del privato  in    &#13;
 ordine alla sua liceità ed alla procedibilità;                         &#13;
      che  peraltro  anche  il principio d'uguaglianza viene in causa,    &#13;
 giacché  l'esercizio  o  non  del  diritto  di   querela   determina    &#13;
 conseguenze diverse per fatti che ledono lo stesso interesse;            &#13;
      che,  pur  essendo  vero  che  anche  in ordine ad altri delitti    &#13;
 lesivi di interessi della generalità il legislatore ha  previsto  la    &#13;
 procedibilità a querela, come per i reati contemplati negli artt. da    &#13;
 519 a 526 e dall'art. 530 cod.  pen.,  la  Relazione  al  codice  ha,    &#13;
 però,  chiarito  che  si tratta di eccezioni inserite per evitare lo    &#13;
 strepitus fori che potrebbe riuscire alla vittima più dannoso  dello    &#13;
 stesso  reato, e che comunque per quelle ipotesi la querela sporta è    &#13;
 poi irrevocabile;                                                        &#13;
      che,  tale  giustificazione  non potendo valere per i delitti in    &#13;
 esame, l'illegittimità denunziata sembrerebbe fondata;                  &#13;
      che  nel  giudizio  relativo al procedimento in corso innanzi al    &#13;
 Giudice  Istruttore  di  Novara  è  intervenuto  il  Presidente  del    &#13;
 Consiglio  dei Ministri, rappresentato dall'Avvocatura Generale dello    &#13;
 Stato,  la  quale  ha  chiesto  che  la  questione   sia   dichiarata    &#13;
 manifestamente infondata;                                                &#13;
    2. - Considerato, in diritto, che le questioni sollevate dalle due    &#13;
 ordinanze riguardano la stessa norma  e  sono  riferite  agli  stessi    &#13;
 parametri  costituzionali,  per  cui appare opportuna la riunione dei    &#13;
 giudizi per deciderli con unica ordinanza;                               &#13;
      che  i  giudici  rimettenti hanno focalizzato la loro attenzione    &#13;
 sulla disponibilità o indisponibilità del  diritto  tutelato  dalla    &#13;
 norma   penale   e  perciò  sul  suo  riferimento  ad  un  interesse    &#13;
 individuale o a quello generale della società, mentre in realtà - a    &#13;
 differenza  di  quanto  si verifica a proposito della scriminante del    &#13;
 consenso dell'avente diritto - duplice è il criterio che ha  indotto    &#13;
 il   legislatore   a   rendere   perseguibili   taluni  reati  previa    &#13;
 proposizione della querela da parte dell'offeso;                         &#13;
      che, infatti, come è ormai riconosciuto dalla prevalente e più    &#13;
 autorevole dottrina, se effettivamente il legislatore ha  inteso,  da    &#13;
 una  parte, tener conto di ragioni di riguardo all'interesse privato,    &#13;
 dall'altra, però, si è ispirato  altresì  ad  una  certa  relativa    &#13;
 tenuità  dell'interesse pubblico compromesso, talché ambo i criteri    &#13;
 e, perciò, ambo gl'interessi concorrono a spiegare l'istituto  della    &#13;
 querela;                                                                 &#13;
      che,  pertanto,  alla  luce  di  tali  premesse,  non è affatto    &#13;
 decisiva l'indagine dei giudici rimettenti circa il  carattere  anche    &#13;
 pubblicistico  dell'interesse offeso dalla fattispecie in esame, come    &#13;
 dimostrano numerosi esempi del  codice,  ben  al  di  là  di  quelli    &#13;
 esemplificati  dalle  ordinanze  (si  considerino gli artt. 513, 554,    &#13;
 614, 622, 623, 633, 635 primo co., 637, 638, 639, 641, 642 u.c., 646,    &#13;
 647  e la disposizione di cui al secondo co. dell'art. 649 cod.pen.),    &#13;
 dove pure  è  innegabile  la  presenza  anche  dell'interesse  della    &#13;
 generalità,   e  tuttavia  la  procedibilità  è  subordinata  alla    &#13;
 proposizione della querela perché il legislatore ha tenuto conto  di    &#13;
 ambo   i  criteri  e,  nella  tenuità  dell'interesse  pubblico,  ha    &#13;
 preferito, per ragioni  di  politica  criminale  e  di  opportunità,    &#13;
 rendere  rilevante  come presupposto della procedibilità la volontà    &#13;
 del privato;                                                             &#13;
      che,  pertanto,  l'intento  di  evitare  lo  strepitus  fori  si    &#13;
 riferisce soltanto a poche ipotesi, oltre quelle che nel citato passo    &#13;
 considerava   la   relazione   del  Guardasigilli,  mentre  le  altre    &#13;
 fattispecie dimostrano che svariate sono le ragioni che  inducono  il    &#13;
 legislatore  a  subordinare alla querela la procedibilità per taluni    &#13;
 reati;                                                                   &#13;
      che,  in  analoga  ipotesi,  d'altra parte, questa Corte ha già    &#13;
 sottolineato che  "la  scelta  del  modo  di  procedibilità...  deve    &#13;
 rimanere  affidata  a  valutazioni  discrezionali  (del legislatore),    &#13;
 insindacabili in questa sede...": e ciò in quanto  trattasi  appunto    &#13;
 di  materia  di  politica  legislativa  che sfugge ad ogni censura di    &#13;
 legittimità costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i   giudizi,  dichiara  manifestamente  inammissibili  le    &#13;
 questioni di legittimità costituzionale dell'art.  92  della  l.  24    &#13;
 novembre 1981 n. 689 (Modifiche al sistema penale), che ha sostituito    &#13;
 l'ultimo  comma  dell'art.  590  cod.  pen.  sollevate  dal   Giudice    &#13;
 istruttore presso il Tribunale di Novara con ordinanza 20 maggio 1983    &#13;
 (n. 91 del 1984) e dal Tribunale di Novara con ordinanza  3  febbraio    &#13;
 1984 (n. 466 del 1984), in riferimento agli artt. 2, 3 e 32 Cost.        &#13;
    Così  deciso  in  Roma,  in Camera di Consiglio, nella sede della    &#13;
 Corte Costituzionale, palazzo della Consulta il 10 febbraio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 febbraio 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
