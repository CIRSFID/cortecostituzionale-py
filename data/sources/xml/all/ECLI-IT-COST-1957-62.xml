<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1957</anno_pronuncia>
    <numero_pronuncia>62</numero_pronuncia>
    <ecli>ECLI:IT:COST:1957:62</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Nicola Jaeger</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/05/1957</data_decisione>
    <data_deposito>25/05/1957</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Prof. &#13;
 TOMASO PERASSI - Prof. GASPARE AMBROSINI - Prof. ERNESTO BATTAGLINI - &#13;
 Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. MARIO BRACCI &#13;
 - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO &#13;
 PETROCELLI - Dott. ANTONIO MANCA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  del  decreto   del  &#13;
 Presidente  della  Repubblica  19 novembre 1952, n.  2337, promosso con  &#13;
 l'ordinanza 26 giugno 1956 del  Tribunale  di  Lecce,  pronunciata  nel  &#13;
 procedimento  civile  vertente  fra  Persone' Egidio ed Alessandro e la  &#13;
 Sezione speciale per la riforma fondiaria dell'Ente Puglia  e  Lucania,  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 213 del 25  &#13;
 agosto 1956 ed iscritta al n.  248 del Registro ordinanze 1956.          &#13;
     Vista la dichiarazione d'intervento del  Presidente  del  Consiglio  &#13;
 dei Ministri;                                                            &#13;
     udita  nell'udienza  pubblica  del  27  marzo 1957 la relazione del  &#13;
 Giudice Nicola Jaeger;                                                   &#13;
     uditi l'avv. Enrico Bassanelli ed il  sostituto  avvocato  generale  &#13;
 dello Stato Francesco Agrò.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con atto di citazione notificato il 10 agosto 1955 i signori Egidio  &#13;
 ed  Alessandro  Persone'  convennero  davanti  al Tribunale di Lecce la  &#13;
 Sezione speciale per la riforma fondiaria  dell'Ente  per  lo  sviluppo  &#13;
 della  irrigazione  e  la trasformazione fondiaria in Puglia e Lucania,  &#13;
 nonché il Ministero dell'agricoltura e delle  foreste,  esponendo  che  &#13;
 mediante  decreto  del  Presidente della Repubblica in data 19 novembre  &#13;
 1952, n. 2337 (pubblicato nel supplemento della Gazzetta  Ufficiale  n.  &#13;
 295   del   20   dicembre   1952),   era   stato   approvato  il  piano  &#13;
 particolareggiato di espropriazione compilato dall'Ente  nei  confronti  &#13;
 degli  attori  e  relativo  ad  un  comprensorio  di terreni in agro di  &#13;
 Nardò, per complessivi ettari  169.22.11  ed  era  stato  disposto  il  &#13;
 trasferimento in proprietà dell'Ente dei terreni espropriati.           &#13;
     Gli  attori  chiedevano  che il Tribunale dichiarasse che essi sono  &#13;
 attualmente proprietari e legittimi possessori dei  beni  compresi  nel  &#13;
 piano  di  espropriazione,  avendo acquistato i beni stessi con atti di  &#13;
 donazione e di compravendita del 1906 e del  1950,  mentre  il  decreto  &#13;
 presidenziale  di  espropriazione  avrebbe  dovuto  essere  considerato  &#13;
 costituzionalmente illegittimo per violazione degli artt. 76 e 77 della  &#13;
 Costituzione, per avere con esso il Governo ecceduto dai  limiti  della  &#13;
 delegazione  conferitagli  con  l'art. 5 della legge 12 maggio 1950, n.  &#13;
 230, e con la legge 21 ottobre 1950, n.  841.                            &#13;
     I convenuti si costituivano e contestavano le domande.  Su  istanza  &#13;
 degli  attori,  il  Tribunale  di  Lecce  disponeva  la sospensione del  &#13;
 giudizio e la trasmissione degli atti alla  Corte  costituzionale,  con  &#13;
 ordinanza  in data 26 giugno 1956, regolarmente notificata e comunicata  &#13;
 ai soggetti e agli organi  indicati  dalla  legge  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica del 25 agosto 1956.                  &#13;
     Le  questioni sottoposte al giudizio della Corte dall'ordinanza del  &#13;
 Tribunale sotto il profilo dell'eccesso di delega sono le seguenti:      &#13;
     a) se, nel calcolo della superficie totale, ai  sensi  dell'art.  4  &#13;
 della  legge  21  ottobre 1950, n. 841, si dovessero includere anche le  &#13;
 aree classificate come fabbricati rurali, che non sarebbero  espressive  &#13;
 di  reddito dominicale, giusta il disposto dell'art. 16 del testo unico  &#13;
 sul nuovo catasto;                                                       &#13;
     b) se, determinata la percentuale di scorporo, con riferimento alla  &#13;
 proprietà di tutti i beni di un soggetto, in tutto il territorio dello  &#13;
 Stato, ai sensi dell'art. 4 della legge 21  ottobre  1950,  n.  841,  e  &#13;
 dell'articolo unico della legge 16 agosto 1952, n. 1206, si dovesse poi  &#13;
 concretamente   espropriare  una  superficie  corrispondente  a  quella  &#13;
 percentuale del reddito totale ovvero  una  parte  corrispondente  alla  &#13;
 stessa percentuale dei soli beni compresi nel perimetro di riforma.      &#13;
     Davanti  alla  Corte  costituzionale si costituivano in termini gli  &#13;
 attori del giudizio principale Egidio ed Alessandro Persone'  e  l'Ente  &#13;
 Puglia  e  Lucania,  ed  interveniva  nel  giudizio  il  Presidente del  &#13;
 Consiglio dei Ministri, rappresentato, come  l'Ente,  dalla  Avvocatura  &#13;
 generale dello Stato.                                                    &#13;
     I  primi  concludevano  perché  fosse dichiarata la illegittimità  &#13;
 costituzionale  del  decreto  presidenziale  di  esproprio,  la  difesa  &#13;
 dell'Ente   perché   fosse   dichiarata   la   improponibilità  o  la  &#13;
 inammissibilità e,  in  subordine,  la  infondatezza  delle  questioni  &#13;
 sollevate.                                                               &#13;
     Le  argomentazione  esposte  nelle  deduzioni e nelle memorie delle  &#13;
 parti e successivamente illustrate nella discussione orale ripropongono  &#13;
 temi discussi anche nelle altre cause analoghe dibattute  davanti  alla  &#13;
 Corte,  con  particolare ricchezza di citazioni e di analisi dei lavori  &#13;
 preparatori della riforma agraria.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte ha già avuto occasione di  pronunciarsi  sulla  eccezione  &#13;
 pregiudiziale  sollevata  dall'Avvocatura  generale dello Stato, con la  &#13;
 sentenza n. 59 del 13 maggio 1957 e non rileva ragioni sufficienti  per  &#13;
 indurla a modificare il proprio giudizio.                                &#13;
     In  quanto  alle  questioni di legittimità costituzionale proposte  &#13;
 dal  Tribunale  di  Lecce,  essa  osserva  che  esattamente  la  difesa  &#13;
 dell'Ente Puglia e Lucania ha sostenuto che i fabbricati rurali mancano  &#13;
 di  propria autonomia funzionale e costituiscono un elemento di ausilio  &#13;
 per la conduzione di terreni agrari di cui concorrono a  migliorare  il  &#13;
 reddito,  onde  la  mancanza  di  estimazione  delle  particelle che li  &#13;
 sopportano  è soltanto apparente e l'incremento di reddito determinato  &#13;
 dalla loro presenza è incorporato nell'estimo dei terreni cui  servono  &#13;
 e dei quali seguono le sorti.                                            &#13;
     Si può aggiungere che molti dei dubbi insorti su questo punto sono  &#13;
 dovuti  al  linguaggio  usato  dalle  leggi di riforma, nel testo delle  &#13;
 quali si leggono spesso espressioni che sembrano riferirsi  soltanto  a  &#13;
 terreni   (terreni  classificati  in  un  modo  o  nell'altro,  terreni  &#13;
 espropriabili, terreni da conservare, da  trasformare,  ecc.);  il  che  &#13;
 deriva  evidentemente dal fatto che il legislatore aveva presenti, come  &#13;
 uno dei fini della riforma, la trasformazione e  il  miglioramento  dei  &#13;
 terreni.  Sennonché,  nella  maggior  parte  dei  casi,  oggetto della  &#13;
 espropriazione non è e non può essere soltanto il terreno,  ma  anche  &#13;
 altri  beni,  che  al  terreno ineriscono e che servono, direttamente o  &#13;
 indirettamente, alla coltivazione, quali  i  magazzini,  le  stalle,  i  &#13;
 granai,  le  abitazioni dei coltivatori e simili. Ed è naturale che il  &#13;
 reddito dominicale complessivamente accertato  per  i  terreni  includa  &#13;
 anche  quella  parte che può derivare dall'uso di tali beni ausiliari,  &#13;
 ma che non si presterebbero neppure  ad  una  valutazione  distinta  ed  &#13;
 autonoma.                                                                &#13;
     Più   delicata  e  più  discussa  è  l'altra  questione,  se  la  &#13;
 percentuale di scorporo, calcolata con riferimento alla  proprietà  di  &#13;
 tutti   i   beni  situati  in  qualunque  parte  del  territorio  della  &#13;
 Repubblica, ai sensi dell'art. 4 della legge stralcio  e  dell'articolo  &#13;
 unico  della  legge  16 agosto 1952, n. 1206, dovesse poi concretamente  &#13;
 applicarsi all'intera proprietà  o  soltanto  alla  porzione  di  beni  &#13;
 compresa nel perimetro di riforma.                                       &#13;
     Su   questo   punto,  si  deve  riconoscere  che  la  difesa  degli  &#13;
 espropriati ha posto efficacemente in  evidenza  diversi  inconvenienti  &#13;
 che  conseguono  alla  applicazione  del sistema adottato e che possono  &#13;
 eventualmente anche diminuirne la efficacia; ma la indicazione di  tali  &#13;
 inconvenienti,  dovuti  in definitiva al fatto che non si è provveduto  &#13;
 finora ad una riforma agraria nazionale e  si  è  preferito  procedere  &#13;
 parzialmente  e  gradualmente,  si  risolve  in una critica dei criteri  &#13;
 adottati dalla legge di delegazione, piuttosto che in una dimostrazione  &#13;
 di illegittimità costituzionale dei decreti legislativi conseguenti.    &#13;
     Proprio nella legge di delegazione e in altre leggi  successive  si  &#13;
 trovano disposizioni, le quali stabiliscono che "Nel caso di proprietà  &#13;
 di  terreni  situati  in parte nei territori indicati nell'art. 1 della  &#13;
 presente legge, e  in  parte  fuori  di  tali  territori,  lo  scorporo  &#13;
 derivante  dall'art.   4 si applica ai terreni situati nei territori di  &#13;
 cui all'art. 1 fino alla totale applicazione della quota di  esproprio"  &#13;
 (art.   13   della  legge  stralcio);  che  "L'Ente  espropriante  può  &#13;
 provvedere alla espropriazione dei  terreni  oggetto  della  comunione,  &#13;
 fino  ad  esaurire  il  valore  della  quota  ideale  spettante a detto  &#13;
 condominio. La porzione  espropriata  sarà  imputata  alla  quota  del  &#13;
 condominio  colpito da espropriazione" (art. 8, secondo comma, legge 18  &#13;
 maggio 1951, n. 333); che "Gli Enti di riforma possono pubblicare piani  &#13;
 particolareggiati di espropriazione... fino  al  30  settembre  1952...  &#13;
 quando  in  conseguenza  dell'applicazione  dell'art. 10 della legge 21  &#13;
 ottobre 1950,  n.  841,  siano  stati  esonerati  dalla  espropriazione  &#13;
 terreni  compresi  in  piani espropriativi pubblicati nei termini e sia  &#13;
 così divenuta necessaria,  per  integrare  la  quota  di  scorporo  in  &#13;
 osservanza  della  legge  stessa,  la  pubblicazione di nuovi piani che  &#13;
 comprendono altri terreni in luogo di quelli esonerati" (art. 2,  primo  &#13;
 comma, legge 2 aprile 1952, n. 339).                                     &#13;
     Anche  se  nessuna  delle  disposizioni  riferite  sarebbe  da sola  &#13;
 sufficiente a risolvere il problema, dal loro  congiunto  esame  e  dal  &#13;
 loro  coordinamento,  oltre  che dalla considerazione della ratio della  &#13;
 legge 16 agosto  1952,  n.  1206,  si  ricava  la  convinzione  che  il  &#13;
 legislatore  ha  voluto  che  la percentuale di scorporo, calcolata con  &#13;
 riferimento alla proprietà di tutti i beni situati in qualunque  parte  &#13;
 del  territorio  nazionale, possa concretarsi sui beni esistenti in una  &#13;
 sola zona, compresa nel perimetro della riforma, anche  se  l'esproprio  &#13;
 giunga  ad  assorbire  integralmente tutti i terreni che l'espropriando  &#13;
 possiede nella zona e perfino se la  quota  da  espropriare  superi  la  &#13;
 estensione di quei terreni.                                              &#13;
     Né  si  può  sostenere che questa soluzione si trovi in contrasto  &#13;
 con le norme della Costituzione, e in particolare con quelle  contenute  &#13;
 nell'art.  3,  senza contestare la legittimità costituzionale di tutta  &#13;
 la riforma fondiaria, così come è stata finora compiuta,  perché  è  &#13;
 chiaro  che una riforma parziale non può non determinare disparità di  &#13;
 trattamenti da zona a zona e da categorie a categorie  territoriali  di  &#13;
 proprietari  e  di  lavoratori  agricoli;  ma  ciò  appare previsto ed  &#13;
 ammesso dalle norme contenute nell'art. 44 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     respinta  la  eccezione  pregiudiziale   proposta   dall'Avvocatura  &#13;
 generale dello Stato,                                                    &#13;
     dichiara  non  fondate  le questioni di legittimità costituzionale  &#13;
 del decreto del Presidente della Repubblica 19 novembre 1952, n.  2337,  &#13;
 in  riferimento  alle norme contenute negli artt. 76 e 77, primo comma,  &#13;
 della Costituzione, nell'art. 4 della legge 21 ottobre 1950, n. 841,  e  &#13;
 nell'articolo  unico  della legge 16 agosto 1952, n. 1206, proposte con  &#13;
 l'ordinanza 26 giugno 1956 del Tribunale di Lecce.                       &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 13 maggio 1957.                               &#13;
                                   GAETANO  AZZARITI  - TOMASO PERASSI -  &#13;
                                   GASPARE    AMBROSINI    -     ERNESTO  &#13;
                                   BATTAGLINI    -   MARIO   COSATTI   -  &#13;
                                   FRANCESCO   PANTALEO    GABRIELI    -  &#13;
                                   GIUSEPPE  CASTELLI  AVOLIO - ANTONINO  &#13;
                                   PAPALDO  -  MARIO  BRACCI  -   NICOLA  &#13;
                                   JAEGER  - GIOVANNI CASSANDRO - BIAGIO  &#13;
                                   PETROCELLI - ANTONIO MANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
