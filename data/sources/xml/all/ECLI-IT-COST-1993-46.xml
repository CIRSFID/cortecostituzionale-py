<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>46</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:46</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/01/1993</data_decisione>
    <data_deposito>10/02/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  1,  comma    &#13;
 quinto,  della  legge  9 gennaio 1963, n. 7 (Divieto di licenziamento    &#13;
 delle lavoratrici per causa di matrimonio e modifiche alla  legge  26    &#13;
 agosto  1950,  n.  860: "Tutela fisica ed economica della lavoratrici    &#13;
 madri"), promosso con ordinanza emessa l'11 maggio 1992  dal  Pretore    &#13;
 di Torino nel procedimento civile vertente tra Bellagarda Paola e AGM    &#13;
 Italiana, iscritta al n. 377 del registro ordinanze 1992 e pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  29,  prima  serie    &#13;
 speciale, dell'anno 1992;                                                &#13;
    Udito nella camera di consiglio del 2  dicembre  1992  il  Giudice    &#13;
 relatore Luigi Mengoni;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel  corso  di  un  giudizio  di  impugnazione  proposto  da Paola    &#13;
 Bellagarda contro il licenziamento  intimatole,  entro  l'anno  dalla    &#13;
 celebrazione   del   suo  matrimonio,  dalla  società  AGM  italiana    &#13;
 nell'ambito di una procedura di mobilità  regolata  dalla  legge  23    &#13;
 luglio  1991,  n.  223,  il  Pretore di Torino, con ordinanza dell'11    &#13;
 maggio 1992, ha sollevato, in riferimento all'art. 3 Cost., questione    &#13;
 di legittimità costituzionale dell'art. 1, quinto comma, della legge    &#13;
 9 gennaio 1963, n. 7, "nella parte in cui non prevede la facoltà del    &#13;
 datore di lavoro di provare che il licenziamento della lavoratrice è    &#13;
 stato effettuato, oltre che per una delle ipotesi di cui alle lettere    &#13;
 a), b) e c) del secondo comma dell'art. 3 della legge 26 agosto 1950,    &#13;
 n. 860, anche nell'ambito delle procedure di mobilità previste dalla    &#13;
 legge n. 223 del 1991 e secondo i criteri ivi determinati".              &#13;
    Ad avviso del giudice remittente, l'estensione della tutela  delle    &#13;
 lavoratrici  madri,  disposta  dalla  legge  n.  860  del  1950  alle    &#13;
 lavoratrici sposate da non più di un anno  per  il  solo  fatto  del    &#13;
 matrimonio,  indipendentemente dalla condizione di gravidanza, mentre    &#13;
 si giustifica con  riguardo  ai  licenziamenti  individuali,  non  è    &#13;
 invece  plausibile  in relazione ai licenziamenti collettivi adottati    &#13;
 nell'ambito di procedure improntate a criteri oggettivi  come  quelle    &#13;
 regolate  dalla  legge  n.  223  del 1991, mancando qui il preminente    &#13;
 interesse pubblico alla tutela della vita sotteso  all'art.  2  della    &#13;
 legge  n.  1204  del  1971. Data la facoltà del datore di lavoro, in    &#13;
 caso di reintegrazione di lavoratori licenziati nel corso delle  procedure  di mobilità, di procedere al licenziamento in numero pari di    &#13;
 altri lavoratori senza dover esperire una nuova  procedura  (art.  17    &#13;
 della legge n. 223 del 1991), si evidenzia una violazione dell'art. 3    &#13;
 Cost.  nella forma di un privilegio ingiustificatamente concesso alle    &#13;
 lavoratrici di cui si discute, in  danno  dei  lavoratori  aventi  un    &#13;
 interesse  più  meritevole di tutela alla conservazione del posto di    &#13;
 lavoro secondo i criteri elencati nell'art. 5 della stessa legge.<diritto>Considerato in diritto</diritto>1. - Dal Pretore di Torino  è  messa  in  dubbio,  in  riferimento    &#13;
 all'art.  3 Cost., la legittimità costituzionale dell'art. 1, quinto    &#13;
 comma, della legge 9 gennaio 1963, n. 7,  "nella  parte  in  cui  non    &#13;
 prevede   la  facoltà  del  datore  di  lavoro  di  provare  che  il    &#13;
 licenziamento della lavoratrice è stato effettuato,  oltre  che  per    &#13;
 una  delle  ipotesi di cui alle lettere a), b) e c) del secondo comma    &#13;
 dell'art. 3 della legge 26 agosto 1950,  n.  860,  anche  nell'ambito    &#13;
 delle  procedure di mobilità previste dalla legge 23 luglio 1991, n.    &#13;
 223, e secondo i criteri ivi determinati".                               &#13;
    2. - La questione non è fondata.                                     &#13;
    La ratio dell'art. 1 della legge n. 7 del 1963  non  si  esaurisce    &#13;
 nell'occasio  legis,  cioè nella finalità di reagire con una tutela    &#13;
 speciale contro la  prassi  (allora  diffusa)  dei  licenziamenti  di    &#13;
 lavoratrici  per  causa di matrimonio. A questa finalità si limitava    &#13;
 l'originario disegno di legge, il quale innovava soltanto  sul  piano    &#13;
 processuale dell'onere della prova, ammettendo una presunzione (iuris    &#13;
 tantum)  della  "causa  di  matrimonio" come determinante del recesso    &#13;
 intimato nel periodo compreso tra il  giorno  della  richiesta  delle    &#13;
 pubblicazioni  e l'anno successivo alla celebrazione, con conseguente    &#13;
 nullità del licenziamento  per  illiceità  del  motivo  (secondo  i    &#13;
 principi  generali: artt. 1324 e 1345 cod. civ.) qualora il datore di    &#13;
 lavoro non fornisse la prova di un diverso "giusto motivo". Il  testo    &#13;
 approvato, invece, avendo ridotto tassativamente la facoltà di prova    &#13;
 contraria  ai  tre  casi  indicati  nell'art. 3, secondo comma, della    &#13;
 legge n. 860 del 1950 sulla tutela delle lavoratrici madri (ora  art.    &#13;
 2,   terzo  comma,  della  legge  30  dicembre  1971,  n.  1204),  ha    &#13;
 sopravanzato la detta finalità assumendo non semplicemente il valore    &#13;
 di  provvedimento   repressivo   di   un'ipotesi   di   licenziamento    &#13;
 (individuale)  illecito particolarmente grave per l'offesa arrecata a    &#13;
 un  diritto  fondamentale  della  lavoratrice,  ma  anche  il  valore    &#13;
 positivo   di  provvedimento  promozionale  del  matrimonio  e  della    &#13;
 famiglia legittima, fondato quindi non solo sugli artt. 2, 4, 35,  37    &#13;
 e  41,  secondo  comma,  Cost.,  ma  anche  sugli artt. 29 e 31 della    &#13;
 Costituzione.                                                            &#13;
    Sotto la veste impropria di  una  presunzione  di  illiceità  del    &#13;
 motivo  non  superabile  se  non  nei tre casi richiamati nell'ultimo    &#13;
 comma dell'art. 1, la legge innova  sostanzialmente  garantendo  alla    &#13;
 lavoratrice,  nel periodo indicato, la stabilità del posto di lavoro    &#13;
 correlata alla sospensione del potere di recesso in capo  al  datore.    &#13;
 Sotto  questo profilo, la condizione della lavoratrice sposata da non    &#13;
 più di un anno è analoga a quella della lavoratrice nel periodo  di    &#13;
 comporto per maternità (cfr. sent. n. 61 del 1991). Ne consegue che,    &#13;
 nel tratto di tempo definito dall'art. 1, terzo comma, della legge n.    &#13;
 7  del  1963,  la  lavoratrice non solo non può essere colpita da un    &#13;
 licenziamento individuale motivato da una  causa  diversa  da  quelle    &#13;
 elencate  nell'ultimo  comma  dell'art. 1, ma non può nemmeno essere    &#13;
 assoggettata  alle  procedure  di   "messa   in   mobilità"   o   di    &#13;
 licenziamento  collettivo  per riduzione del personale regolate dalla    &#13;
 legge 23 luglio 1991, n. 223.                                            &#13;
    3. - In quest'ultima ipotesi la scelta del legislatore del 1963 di    &#13;
 coordinare la legge n. 7, oltre che con la  politica  di  tutela  dei    &#13;
 lavoratori  contro  i  licenziamenti, anche con la politica di favore    &#13;
 per il matrimonio e di agevolazione della formazione  della  famiglia    &#13;
 legittima,  non  può  essere commisurata ai criteri di selezione dei    &#13;
 lavoratori da licenziare, specificati nell'art. 5 della legge n.  223    &#13;
 del  1991, e censurata, in riferimento al principio costituzionale di    &#13;
 eguaglianza, perché al posto della lavoratrice protetta dall'art.  1    &#13;
 della  legge potrà essere licenziata una lavoratrice sposata da più    &#13;
 di un anno o un lavoratore con maggiore anzianità aziendale e/o  con    &#13;
 carichi  di  famiglia  più pesanti. Confronti di questo tipo possono    &#13;
 essere fatti soltanto tra due lavoratori entrambi soggetti al  potere    &#13;
 di   licenziamento   dell'imprenditore,   mentre  la  lavoratrice  in    &#13;
 questione a tale potere è sottratta fino a quando  non  compirà  un    &#13;
 anno di matrimonio. Nel dettare i criteri di scelta dei lavoratori da    &#13;
 collocare in mobilità o da licenziare per riduzione del personale ai    &#13;
 sensi  dell'art.  24,  l'art.  5  della  legge n. 223 si riferisce ai    &#13;
 lavoratori per i quali non sussista, in  forza  di  altre  leggi,  un    &#13;
 divieto  di  licenziamento.  Per quelli tutelati da cause attributive    &#13;
 del  diritto  alla  conservazione  del  posto  non   si   può   dire    &#13;
 propriamente - come si legge nell'ordinanza di rimessione - che altri    &#13;
 lavoratori,  eventualmente  dotati,  ai  sensi  del citato art. 5, di    &#13;
 maggiori titoli a rimanere in azienda, verranno a trovarsi licenziati    &#13;
 in loro vece.                                                            &#13;
    4. - L'incidenza sulla sfera  soggettiva  dei  lavoratori  esposti    &#13;
 alle  procedure  di  collocamento in mobilità o di licenziamento per    &#13;
 riduzione del personale non mette in contrasto l'art. 1  della  legge    &#13;
 n.  7  del  1963  con  l'art.  3  Cost.  neppure sotto il profilo del    &#13;
 principio di razionalità. Il criterio di ragionevolezza,  quando  è    &#13;
 disgiunto  dal  riferimento  a un tertium comparationis, può trovare    &#13;
 ingresso solo se l'irrazionalità o iniquità delle conseguenze della    &#13;
 norma  sia manifesta e irrefutabile (cfr., da ultimo, sent. n. 81 del    &#13;
 1992). Da un giudizio di manifesta eccessività di tutela del diritto    &#13;
 al matrimonio e alla creazione di una famiglia la norma denunciata è    &#13;
 tenuta al riparo - come già osservava conclusivamente la sentenza n.    &#13;
 27 del 1969 - per un verso,  dalla  considerazione  che  ad  essa  è    &#13;
 sotteso  non  soltanto  un  interesse  individuale (nella forma di un    &#13;
 diritto fondamentale), ma  altresì  l'interesse  pubblico,  tutelato    &#13;
 dall'art.  31  Cost.,  che  sia favorita la formazione della famiglia    &#13;
 legittima  fondata   sul   matrimonio,   per   altro   verso,   dalla    &#13;
 considerazione  del  limite  ben  definito  di  durata  entro  cui è    &#13;
 contenuto il divieto di licenziamento.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  1, quinto comma, della legge 9 gennaio 1963, n. 7 (Divieto    &#13;
 di  licenziamento  delle  lavoratrici  per  causa  di  matrimonio   e    &#13;
 modifiche  alla  legge  26  agosto  1950,  n.  860: "Tutela fisica ed    &#13;
 economica  delle  lavoratrici  madri"),  sollevata,  in   riferimento    &#13;
 all'art.  3 della Costituzione, dal Pretore di Torino con l'ordinanza    &#13;
 in epigrafe.                                                             &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 28 gennaio 1993.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 febbraio 1993.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
