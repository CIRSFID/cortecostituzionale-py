<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2009</anno_pronuncia>
    <numero_pronuncia>44</numero_pronuncia>
    <ecli>ECLI:IT:COST:2009:44</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMIRANTE</presidente>
    <relatore_pronuncia>Alfonso Quaranta</relatore_pronuncia>
    <redattore_pronuncia>Alfonso Quaranta</redattore_pronuncia>
    <data_decisione>09/02/2009</data_decisione>
    <data_deposito>13/02/2009</data_deposito>
    <tipo_procedimento>GIUDIZIO PER CONFLITTO DI ATTRIBUZIONE TRA ENTI</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Francesco AMIRANTE; Giudici: Ugo DE SIERVO, Paolo MADDALENA, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra enti sorto a seguito della nota del Ministero dei trasporti – Direzione generale dei porti – prot. numero M. TRA/DINFR/4520, DIV. IV, in data 17 aprile 2008, promosso con ricorso della Regione Toscana, notificato il 20 giugno 2008, depositato in cancelleria il successivo 27 giugno ed iscritto al n. 10 del registro conflitti tra enti 2008. &#13;
    Udito nell'udienza pubblica del 27 gennaio 2009 il Giudice relatore Alfonso Quaranta. &#13;
    Ritenuto che la Regione Toscana, con ricorso notificato il 20 giugno 2008 e depositato presso la cancelleria della Corte il successivo 27 giugno, ha proposto conflitto di attribuzione nei confronti dello Stato impugnando la nota del Ministero dei trasporti – Direzione generale dei porti – prot. numero M. TRA/DINFR/4520, DIV. IV, in data 17 aprile 2008; &#13;
    che con detto atto, il Ministero in questione, in attesa della revisione del d.P.C.m. 21 dicembre 1995 (Identificazione delle aree demaniali marittime escluse dalla delega alle Regioni ai sensi dell'art. 59 del d.P.R. 24 luglio 1977, n. 616), «per la fase transitoria richiama ancora l'applicabilità del suddetto d.P.C.m. (..) identificativo delle aree marittime di preminente interesse nazionale»;  &#13;
    che ad avviso della Regione, il sistema così delineato, nel riservare allo Stato la competenza su tutti i porti indicati nel suddetto atto governativo, si porrebbe in contrasto con il riparto delle attribuzioni tra lo Stato e le Regioni in ordine alle concessioni sui beni del demanio marittimo portuale, come risultante dal nuovo assetto costituzionale; &#13;
    che la nota impugnata, infatti, «non considera che, a seguito della riforma degli artt. 117 e 118 della Costituzione, il settore dei porti civili è stato demandato alla potestà legislativa concorrente delle Regioni, senza distinguere tra aree portuali aventi rilevanza economica regionale, ovvero nazionale o internazionale»;  &#13;
    che, pertanto, la Regione ritiene che l'atto oggetto del conflitto sia lesivo delle proprie attribuzioni per contrasto con gli artt. 117 e 118 Cost., anche in relazione all'art. 5 Cost.; &#13;
    che la ricorrente ricorda che la Corte, con le sentenze n. 89 del 2006 e n. 344 del 2007, ha già esaminato analoghi conflitti di attribuzione ed ha affermato, con riguardo, rispettivamente, al porto di Viareggio e agli ulteriori porti turistici e commerciali di rilevanza economica regionale ed interregionale siti nel territorio della Regione Toscana, che il nuovo sistema delle competenze, delineato dalla legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della Costituzione), impedisce che possa attribuirsi attuale valenza, ai fini del riparto delle richiamate funzioni amministrative tra lo Stato e le Regioni, all'inserimento dei suddetti porti nel citato d.P.C.m. del 1995; &#13;
    che ad avviso della Regione, mentre per i porti di Livorno, Marina di Carrara, Piombino, Portoferraio, Rio Marina, in quanto sedi di autorità portuale, permane la competenza statale, tutti i siti portuali della Regione Toscana diversi da quest'ultimi, «ferma restando la titolarità della Regione sui porti ad esclusiva funzione turistica, sono da considerare “porti di rilevanza economica regionale e interregionale” secondo la vigente classificazione di cui alla legge n. 84 del 1994», con la conseguente competenza della Regione; &#13;
    che il Presidente del Consiglio dei ministri non si è costituito; &#13;
    che in prossimità dell'udienza pubblica, l'8 gennaio 2009, la Regione ricorrente ha depositato in cancelleria atto di rinuncia al ricorso, in forza della deliberazione assunta, il 15 dicembre 2008, dalla Giunta regionale; &#13;
    che, infatti, «con nota del 25 novembre 2008, prot. 0022176-25/11/2008 – Uscita 27.511/768 il Ministero delle infrastrutture e dei trasporti ha chiarito che la parte contestata della» nota impugnata, «relativa alla fase transitoria riguarda esclusivamente i porti e le aree demaniali marittime, anche esterne agli stessi, che rivestono interesse militare e di sicurezza dello Stato e che, peraltro anche in tali porti ed aree l'esercizio delle funzioni amministrative può competere alla Regione, ove il Ministero della difesa evidenzi la carenza di interesse militare e di sicurezza dello Stato». &#13;
    Considerato che in mancanza di costituzione in giudizio della parte resistente, la rinuncia al ricorso comporta, ai sensi dell'articolo 27, ultimo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo (ordinanza n. 230 del 2007).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara estinto il processo. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 febbraio 2009.  &#13;
F.to:  &#13;
Francesco AMIRANTE, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 13 febbraio 2009.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
