<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>312</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:312</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia>Fernanda Contri</redattore_pronuncia>
    <data_decisione>20/06/2002</data_decisione>
    <data_deposito>04/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
FLICK, Francesco AMIRANTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di legittimità costituzionale dell'art. 351 del codice &#13;
di  procedura  civile, promossi con sei ordinanze emesse il 22 giugno &#13;
2001  dalla  Corte  di appello di Torino, rispettivamente iscritte ai &#13;
nn. 807,  808,  809,  810,  811  e  812 del registro ordinanze 2001 e &#13;
pubblicate  nella Gazzetta Ufficiale della Repubblica n. 41, 1ª serie &#13;
speciale, dell'anno 2001. &#13;
    Visti  gli  atti  di  costituzione della Sitaf S.p.a. nonché gli &#13;
atti di intervento del Presidente del Consiglio dei ministri; &#13;
    Udito  nell'udienza  pubblica  del  23  aprile  2002  il  giudice &#13;
relatore Fernanda Contri; &#13;
    Uditi l'avvocato Fabrizio Magrì per la Sitaf S.p.a. e l'avvocato &#13;
dello  Stato  Paolo  Gentili  per  il  Presidente  del  Consiglio dei &#13;
ministri. &#13;
    Ritenuto  che  la Corte d'appello di Torino, con sei ordinanze di &#13;
identico  contenuto,  emesse  il  22  giugno  2001,  ha sollevato, in &#13;
riferimento  agli  artt. 3,  primo  comma,  24, secondo comma, e 111, &#13;
secondo   comma,   della   Costituzione,  questione  di  legittimità &#13;
costituzionale  dell'art. 351 del codice di procedura civile, laddove &#13;
condiziona  l'esercizio  del diritto di difesa della parte resistente &#13;
all'onere  della  preventiva  costituzione  rituale  nel  giudizio di &#13;
appello; &#13;
        che   la   Corte   rimettente,   chiamata  a  decidere  sulla &#13;
sospensione    dell'efficacia    della   sentenza   impugnata   prima &#13;
dell'udienza   di  comparizione,  ritiene  di  dover  preliminarmente &#13;
verificare se la disciplina processuale consenta la partecipazione al &#13;
giudizio  incidentale  della  parte  resistente  che  non  sia ancora &#13;
costituita nella causa di merito; &#13;
        che  il  giudice  a  quo osserva come nella disciplina tipica &#13;
dell'appello,  inteso  quale revisio prioris instantiae dopo una fase &#13;
preparatoria,  in  cui  si  verifica  la  regolare  costituzione  del &#13;
contraddittorio,  sussista  una  fase,  di carattere endoprocessuale, &#13;
degli  incidenti  relativi alla sospensione dell'esecuzione, la quale &#13;
postula  che il contraddittorio sia completo, in presenza di tutte le &#13;
parti  costituite  e  la già pronunciata dichiarazione di contumacia &#13;
delle parti non costituite, non essendo altrimenti possibile delibare &#13;
la  non  manifesta  infondatezza dell'appello e la sussistenza o meno &#13;
dei gravi motivi per la sospensione; &#13;
        che   tale   situazione  non  muta  qualora  sull'istanza  di &#13;
sospensione  si  provveda  anticipatamente  in  camera  di  consiglio &#13;
anziché alla prima udienza, poiché anche in questa ipotesi la parte &#13;
resistente,  per  poter  contrastare il provvedimento di sospensione, &#13;
deve essere costituita nel giudizio di merito; &#13;
        che   dalla   impossibilità  per  la  parte  resistente  non &#13;
costituita   di  partecipare  all'udienza  camerale  e  dall'asserita &#13;
impossibilità    di    ipotizzare    una   diversa   interpretazione &#13;
costituzionalmente  compatibile, il giudice a quo trae la conseguenza &#13;
del  contrasto  di  tale  disciplina  con  i  principi  sanciti dagli &#13;
artt. 3,  24  e 111 della Costituzione, in quanto la parte resistente &#13;
sarebbe privata del potere di contraddire sull'istanza di sospensione &#13;
avanzata  dalla  controparte,  ovvero,  per  poter interloquire su di &#13;
essa,  sarebbe  costretta  a costituirsi anticipatamente, rinunciando &#13;
così all'integrale godimento dei termini a comparire; &#13;
        che,  inoltre,  la  norma  impugnata  contrasterebbe  con  il &#13;
novellato  art. 111 della Costituzione, essendo incompatibile sia con &#13;
il  principio  del  contraddittorio  che  con una reale situazione di &#13;
parità delle parti; &#13;
        che, infine, vi sarebbe una irragionevole discriminazione tra &#13;
le  parti  resistenti  nell'inibitoria  e  non  ancora  costituite, a &#13;
seconda  che  sia  o  meno  proposta  dalla  controparte l'istanza di &#13;
pronuncia anticipata sulla sospensione; &#13;
        che  in  tutti  i  procedimenti  innanzi a questa Corte si è &#13;
costituita  la  parte  appellante  e ricorrente dei giudizi a quibus, &#13;
chiedendo che la questione sia dichiarata infondata; &#13;
        che,  ad  avviso  della  difesa  dell'appellante,  il giudice &#13;
rimettente   muove   da   un   presupposto   interpretativo  erroneo, &#13;
attribuendo al termine parti, contenuto nel terzo comma dell'art. 351 &#13;
cod.  proc. civ., il significato di parti costituite, anziché quello &#13;
di   parti   del  procedimento  di  anticipata  sospensione,  con  la &#13;
conseguenza  che,  in  base  a  tale  interpretazione,  l'istanza  di &#13;
anticipazione  non  potrebbe  proporsi  se  non  dopo la scadenza del &#13;
termine di costituzione dell'appellato; &#13;
        che  è  intervenuto  in  tutti  i  giudizi il Presidente del &#13;
Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura &#13;
generale  dello  Stato, concludendo per l'inammissibilità o comunque &#13;
per  l'infondatezza  della questione, in quanto il rimettente avrebbe &#13;
eseguito   una   ricostruzione   basata  su  un  erroneo  presupposto &#13;
interpretativo,  in  assenza  di  un  testuale riscontro normativo, e &#13;
senza dare della norma una lettura conforme alla Costituzione. &#13;
    Considerato  che  le censure di incostituzionalità dell'art. 351 &#13;
cod.  proc.  civ.,  mosse  dalla Corte d'appello di Torino, si basano &#13;
tutte  sull'erroneo  presupposto  che  la pronuncia sulla sospensione &#13;
postuli   l'onere  della  preventiva  costituzione  delle  parti  nel &#13;
giudizio di merito; &#13;
        che  tale  affermazione  non  trova  tuttavia riscontro nella &#13;
formulazione  letterale  della  norma,  la quale prevede modalità di &#13;
trattazione  diverse  in  relazione  alla  urgenza della richiesta di &#13;
decisione  sulla  sospensione  e  con  riferimento  al momento in cui &#13;
interviene  la  pronuncia,  ma non detta alcuna disposizione circa la &#13;
costituzione delle parti in tale fase; &#13;
        che  deve  sottolinearsi  come il procedimento incidentale di &#13;
inibitoria conservi tuttora, anche in seguito alla novella introdotta &#13;
con  la  legge  di riforma n. 353 del 1990, uno spiccato carattere di &#13;
autonomia  rispetto  al  giudizio  sul  merito dell'appello, come del &#13;
resto è stato riconosciuto dalla prevalente dottrina; &#13;
        che  proprio  in ragione di tale autonomia è consentito alla &#13;
parte   resistente   di  costituirsi  ai  soli  fini  di  contraddire &#13;
all'istanza  di  sospensione,  analogamente  a quanto si verifica nei &#13;
procedimenti   incidentali   di  natura  cautelare  che  si  svolgono &#13;
anticipatamente rispetto alla trattazione del merito; &#13;
        che,  pertanto,  qualora la decisione sulla sospensione debba &#13;
essere  pronunciata  prima  dell'udienza  di comparizione e non siano &#13;
ancora  decorsi i termini per la costituzione nel giudizio di merito, &#13;
la  parte  nei cui confronti è proposta l'istanza di sospensione ben &#13;
può scegliere di costituirsi nel solo procedimento di inibitoria; &#13;
        che  la  norma,  ove correttamente interpretata, è del tutto &#13;
esente  dai  lamentati vizi di incostituzionalità e che la sollevata &#13;
questione risulta perciò manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi; &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art.  351  del codice di procedura &#13;
civile,  sollevata,  in  riferimento  agli  artt. 3, primo comma, 24, &#13;
secondo  comma, e 111, secondo comma, della Costituzione, dalla Corte &#13;
d'appello di Torino con le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 20 giugno 2002. &#13;
                       Il Presidente: Ruperto &#13;
                        Il redattore: Contri &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 4 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
