<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>17</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:17</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/01/1999</data_decisione>
    <data_deposito>05/02/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. &#13;
 Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 3  della  legge    &#13;
 della  Regione  Abruzzo  22  febbraio  1997, n. 16 (Istituzione della    &#13;
 riserva naturale guidata "Gole del Sagittario") e degli artt. 4,  11,    &#13;
 comma  18,  e 21 della legge della Regione Abruzzo 21 giugno 1996, n.    &#13;
 38 (Legge quadro  sulle  aree  protette  della  Regione  Abruzzo  per    &#13;
 l'Appennino  Parco  d'Europa),  promosso  con  ordinanza emessa il 15    &#13;
 dicembre 1997 dal Commissario regionale per  il  riordinamento  degli    &#13;
 usi  civici in Abruzzo nel procedimento civile vertente tra il comune    &#13;
 di Anversa degli Abruzzi e l'Amministrazione separata dei beni civici    &#13;
 della frazione di Castrovalva  ed  altra,  iscritta  al  n.  217  del    &#13;
 registro  ordinanze  1998 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 14, prima serie speciale, dell'anno 1998.                  &#13;
    Visto l'atto di costituzione del comune di Anversa degli Abruzzi;     &#13;
   Udito  nella  camera  di  consiglio  del 28 ottobre 1998 il giudice    &#13;
 relatore Gustavo Zagrebelsky.                                            &#13;
   Ritenuto che, nel corso di un giudizio promosso  su  ricorso  della    &#13;
 Amministrazione  separata  dei  beni  di uso civico della frazione di    &#13;
 Castrovalva del comune di Anversa degli Abruzzi, con il quale si  era    &#13;
 chiesto  di  accertare  la  "legittimità"  di  un  disegno  di legge    &#13;
 regionale (poi divenuto legge della Regione Abruzzo n. 16  del  1997)    &#13;
 istitutivo  della riserva naturale guidata "Gole del Sagittario", nel    &#13;
 cui perimetro sarebbero ricompresi beni di uso civico, il Commissario    &#13;
 regionale per  il  riordinamento  degli  usi  civici  in  Abruzzo  ha    &#13;
 sollevato  questione di legittimità costituzionale dell'art. 3 della    &#13;
 legge della Regione Abruzzo 22  febbraio  1997,  n.  16  (Istituzione    &#13;
 della  riserva  naturale guidata "Gole del Sagittario") e degli artt.    &#13;
 4, 11, comma 18, e 21 della legge della  Regione  Abruzzo  21  giugno    &#13;
 1996,  n.  38 (Legge quadro sulle aree protette della Regione Abruzzo    &#13;
 per l'Appennino Parco d'Europa), per contrasto con gli artt. 2, 3, 4,    &#13;
 5, 35, 44, 46 e 117 della Costituzione e in relazione agli artt.   1,    &#13;
 lettera  a),  3  e  9  della  legge  31  gennaio  1994,  n. 97 (Nuove    &#13;
 disposizioni per le zone montane) e 22, lettere c) ed e), della legge    &#13;
 6 dicembre 1991, n. 394 (Legge quadro sulle aree protette);              &#13;
     che il  rimettente,  nell'affermare  preliminarmente  la  propria    &#13;
 giurisdizione,  precisa  che  la  ricorrente Amministrazione separata    &#13;
 "non si duole dell'avvenuta istituzione della  riserva  naturale  ...    &#13;
 né  delle  gravi limitazioni ... che essa comporta all'esercizio dei    &#13;
 diritti  di  usi  civici  ...  bensì  del  fatto  che,  pur  essendo    &#13;
 proprietaria  a  titolo collettivo della maggior parte del territorio    &#13;
 di natura demaniale civica incluso nel suo perimetro (70%), sia stata    &#13;
 esclusa da ogni potere gestionale e, quindi,  decisionale  attribuito    &#13;
 ...  esclusivamente  al  comune ... che è invece proprietario ... di    &#13;
 appena il 30% del residuo territorio demaniale civico";                  &#13;
     che nel ricorso introduttivo del giudizio  a  quo  la  ricorrente    &#13;
 Amministrazione  separata  riferiva:  a)  che  si erano interrotte le    &#13;
 trattative in corso con il comune di Anversa degli  Abruzzi  tendenti    &#13;
 alla stipula di una convenzione con il comune medesimo e con il World    &#13;
 Wildlife   Fund  (WWF)  per  la  gestione  della  istituenda  riserva    &#13;
 naturale, nel territorio della quale insistevano anche terreni di uso    &#13;
 civico; b) che  il  comune  era  stato  diffidato  dall'intraprendere    &#13;
 qualsiasi  attività  sui  terreni  in  questione, se non previamente    &#13;
 concordata; c) che, avendo la Giunta della Regione  Abruzzo  proposto    &#13;
 al Consiglio regionale l'approvazione del disegno di legge istitutivo    &#13;
 della detta riserva, l'Amministrazione separata aveva conclusivamente    &#13;
 chiesto  al  Commissario  di  stabilire "se fosse legittimo o meno il    &#13;
 suindicato disegno di legge";                                            &#13;
     che,  ad  avviso  del  rimettente,  i  diritti  di   uso   civico    &#13;
 riconosciuti  in  capo  ai  "naturali"  di  Castrovalva fino dal 1936    &#13;
 sarebbero vanificati dall'art. 3  della  legge  regionale  per  prima    &#13;
 denunciata  (n.  16 del 1997) che affida la gestione della riserva al    &#13;
 comune di Anversa degli Abruzzi, il quale può avvalersi di  soggetti    &#13;
 (associazioni  ambientalistiche,  consulenti,  società cooperative o    &#13;
 istituti particolarmente qualificati, quali il Corpo forestale  dello    &#13;
 Stato,  l'Università,  l'Istituto zooprofilattico per l'Abruzzo e il    &#13;
 Molise "G. Caporale"), tra cui  non  risulta  inclusa  la  ricorrente    &#13;
 Amministrazione  separata,  privata  così  del potere di "tutelare i    &#13;
 suoi  naturali  che  resterebbero sottoposti unicamente ai voleri del    &#13;
 comune capoluogo" e "espropriata del  potere  di  disposizione  degli    &#13;
 anzidetti fondi demaniali civici";                                       &#13;
     che,  inoltre,  la  legge  regionale  quadro sulle aree protette,    &#13;
 anch'essa denunciata, pur riconoscendo (art. 8 della legge  regionale    &#13;
 n.  38  del 1996) i valori e il rispetto delle forme di utilizzazione    &#13;
 collettiva  delle   montagne,   avrebbe   del   tutto   ignorato   le    &#13;
 amministrazioni  separate  frazionali  di  cui  alla legge n. 278 del    &#13;
 1957, molto numerose nel territorio  montano  abruzzese,  quali  enti    &#13;
 legittimati  a richiedere l'istituzione di parchi o riserve regionali    &#13;
 (art. 4), a costituire la comunità del parco (art.  11),  a  gestire    &#13;
 riserve naturali e regionali o a convenzionarsi con esse (art. 21);      &#13;
     che  tutto  ciò  sarebbe  in primo luogo contrario, oltreché al    &#13;
 criterio  della  ragionevolezza  (art.  3  della  Costituzione),   ai    &#13;
 principi  generali  desumibili dalla legislazione nazionale (art. 117    &#13;
 della Costituzione), quali, ai sensi della legge n. 97 del  1994,  la    &#13;
 salvaguardia  e  la  valorizzazione  delle  zone  montane  da attuare    &#13;
 mediante forme di tutela e di promozione di  risorse  ambientali  che    &#13;
 tengano  conto  delle  insopprimibili  esigenze  di vita civile delle    &#13;
 popolazioni residenti (art. 1, lettera a) al  fine  di  garantire  la    &#13;
 partecipazione  alla  gestione  comune dei rappresentanti liberamente    &#13;
 scelti  dalle   famiglie   originarie   stabilmente   stanziate   sul    &#13;
 territorio,  ovvero,  ai sensi dell'art.   22, lettere c) ed e) della    &#13;
 legge n. 394 del 1991,  l'indispensabile  partecipazione  degli  enti    &#13;
 interessati  alla  gestione  delle aree protette e la possibilità di    &#13;
 affidare la  stessa  alle  "comunioni  familiari  associate"  qualora    &#13;
 l'area  protetta  sia  in  tutto  o  in  parte  compresa  tra  i beni    &#13;
 costituenti patrimonio delle comunità medesime;                         &#13;
     che tali principi non sarebbero rispettati poiché sarebbe  stato    &#13;
 ignorato  il ruolo essenziale demandato alle amministrazioni separate    &#13;
 di uso civico, "la cui presenza materiale sul posto  è  la  migliore    &#13;
 garanzia  della  protezione  dell'ambiente  e della tutela dei demani    &#13;
 civici  che   ...   hanno   ormai   assunto   valore   ambientale   e    &#13;
 paesaggistico";                                                          &#13;
     che  sarebbe  stata inoltre consentita la creazione della riserva    &#13;
 de  qua,  inaudita  altera  parte,  in  spregio  del  principio   del    &#13;
 contraddittorio   con   l'Amministrazione  separata,  riconosciuto  a    &#13;
 salvaguardia del diritto di difesa di ogni soggetto  (art.  24  della    &#13;
 Costituzione)  ma,  nella  specie,  disatteso nonostante la contraria    &#13;
 volontà  dichiarata  dalla  Amministrazione  medesima,   omettendosi    &#13;
 persino  la consultazione con le popolazioni interessate, che sarebbe    &#13;
 stata  tanto  più  necessaria  considerato   il   palese   conflitto    &#13;
 d'interessi  con  il  comune  (art.  75 del regolamento di esecuzione    &#13;
 della legge n. 1766 del 1927, approvato con regio decreto 26 febbraio    &#13;
 1928, n. 332; art. 78 cod. proc. civ.);                                  &#13;
     che si è costituito in giudizio, fuori  termine,  il  comune  di    &#13;
 Anversa degli Abruzzi.                                                   &#13;
   Considerato    che   la   sollevata   questione   di   legittimità    &#13;
 costituzionale si presenta impropriamente come azione diretta  contro    &#13;
 una  legge,  dal momento che l'eventuale pronunzia di accoglimento di    &#13;
 questa Corte verrebbe a concretare di per sé la tutela richiesta  al    &#13;
 rimettente  e  ad  esaurirla,  mentre  il carattere di incidentalità    &#13;
 presuppone necessariamente che il petitum del giudizio nel corso  del    &#13;
 quale  viene  sollevata la questione non coincida con la proposizione    &#13;
 della questione stessa;                                                  &#13;
     che,  difatti, nel caso in esame, non è dato scorgere, una volta    &#13;
 venute  meno  le  norme  censurate,  quale  provvedimento   ulteriore    &#13;
 dovrebbe  essere emesso dal Commissario agli usi civici per rimuovere    &#13;
 la denunciata turbativa a  danno  della  Amministrazione  ricorrente,    &#13;
 essendosi nel giudizio principale richiesto soltanto di stabilire "se    &#13;
 fosse legittimo ... il disegno di legge" regionale;                      &#13;
     che  pertanto  la  questione  di  legittimità  costituzionale è    &#13;
 manifestamente inammissibile.                                            &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 3  della  legge  della  Regione    &#13;
 Abruzzo  22  febbraio 1997, n. 16 (Istituzione della riserva naturale    &#13;
 guidata "Gole del Sagittario") e degli artt. 4, 11, comma  18,  e  21    &#13;
 della legge della Regione Abruzzo 21 giugno 1996, n. 38 (Legge quadro    &#13;
 sulle  aree  protette  della  Regione  Abruzzo  per l'Appennino Parco    &#13;
 d'Europa), sollevata, in riferimento agli artt. 2, 3, 4, 5,  35,  44,    &#13;
 46  e 117 della Costituzione e in relazione agli artt. 1, lettera a),    &#13;
 3 e 9 della legge 31 gennaio 1994 n. 97 e 22, lettere c) ed e), della    &#13;
 legge 6 dicembre 1991, n.   394, dal  Commissario  regionale  per  il    &#13;
 riordinamento  degli  usi civici in Abruzzo, con l'ordinanza indicata    &#13;
 in epigrafe.                                                             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 27 gennaio 1999.                              &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 5 febbraio 1999.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
