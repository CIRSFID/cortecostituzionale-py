<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>375</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:375</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/10/1993</data_decisione>
    <data_deposito>14/10/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 4 della legge    &#13;
 della Regione Liguria 14 aprile 1983, n. 11 (Norme per l'applicazione    &#13;
 delle sanzioni amministrative  pecuniarie  in  materia  di  igiene  e    &#13;
 sanità  pubblica,  vigilanza  sulle farmacie e polizia veterinaria),    &#13;
 promosso con ordinanza emessa  il  16  giugno  1992  dalla  Corte  di    &#13;
 cassazione  sul  ricorso  proposto  dal Comune di La Spezia contro la    &#13;
 s.n.c. Pastificio  Frediani  Montecatini,  iscritta  al  n.  124  del    &#13;
 registro  ordinanze  1993 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 13, prima serie speciale, dell'anno 1993;                  &#13;
    Udito nella camera di consiglio del  23  giugno  1993  il  giudice    &#13;
 relatore Francesco Guizzi;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>La Corte Suprema di Cassazione, prima sezione civile, nel giudizio    &#13;
 sul  ricorso  promosso dal Comune di La Spezia avverso la sentenza n.    &#13;
 248 del 1988 pronunciata dal Pretore di La Spezia  in  una  causa  di    &#13;
 opposizione   ad   ordinanza-ingiunzione  di  pagamento  di  sanzione    &#13;
 amministrativa, ha sollevato, in riferimento agli artt. 3, 97  e  117    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art. 4 della legge della Regione Liguria 14 aprile 1983,  n.  11    &#13;
 (Norme per l'applicazione delle sanzioni amministrative pecuniarie in    &#13;
 materia  di  igiene  e  sanità  pubblica, vigilanza sulle farmacie e    &#13;
 polizia veterinaria).                                                    &#13;
    La Corte rimettente  ricorda,  in  via  preliminare,  che  fra  le    &#13;
 funzioni  amministrative  in  materia di igiene e sanità, trasferite    &#13;
 alle regioni dal d.P.R. 24  luglio  1977,  n.  616,  vi  sono  quelle    &#13;
 concernenti  la tutela igienico-sanitaria della produzione, commercio    &#13;
 e lavorazione delle sostanze alimentari (art. 27, lett. e del decreto    &#13;
 citato) e riconosce la competenza regionale per l'applicazione  delle    &#13;
 sanzioni   amministrative   nei   casi   di  violazione  delle  leggi    &#13;
 disciplinatrici della materia.                                           &#13;
    La Regione Liguria - ricorda la Corte di Cassazione - ha  adottato    &#13;
 una  legge  generale per l'applicazione delle sanzioni amministrative    &#13;
 pecuniarie (legge 2 dicembre 1982, n. 45)  che,  all'art.  7,  ultimo    &#13;
 comma,  individua  quale  organo  competente  a comminare la sanzione    &#13;
 quello del luogo in cui è stata commessa la violazione. Con la legge    &#13;
 14 aprile 1983, n. 11, la Regione ha poi introdotto  ulteriori  norme    &#13;
 per  l'applicazione delle sanzioni pecuniarie nella specifica materia    &#13;
 dell'igiene e della sanità pubblica. L'art. 4 di tale  legge  introduce  un  criterio diverso rispetto a quello del luogo della commessa    &#13;
 violazione, attribuendo le funzioni di cui alla  legge  regionale  al    &#13;
 sindaco del comune nel cui territorio la violazione viene accertata.     &#13;
    Tale   disposizione   appare   al   giudice   a  quo  sfornita  di    &#13;
 giustificazione razionale, anche alla luce di quanto  disposto  dalla    &#13;
 legge  regionale  generale  (la  n.  45  del  1982),  e  viene dunque    &#13;
 censurata in relazione all'art. 3 della Costituzione.                    &#13;
    Vi sarebbe altresì violazione dell'art. 117  della  Costituzione.    &#13;
 La  normativa  regionale,  nella  materia  in esame, non può infatti    &#13;
 ledere i principi generali fissati dalla legislazione statale: e ora,    &#13;
 in tema di sanzioni amministrative l'art.  17,  quinto  comma,  della    &#13;
 legge 24 novembre 1981, n. 689, prevede come ufficio territorialmente    &#13;
 competente quello del luogo dove è stata commessa la violazione.        &#13;
    La  norma impugnata, prosegue la Corte rimettente, contrasta anche    &#13;
 con  il  principio  di  buon  andamento  dell'amministrazione   posto    &#13;
 dall'art.  97  della Costituzione, considerata la mera casualità con    &#13;
 cui  si  accerta  la  presenza  della  sostanza  alimentare   in   un    &#13;
 determinato  luogo - che può non coincidere con quello di produzione    &#13;
 ed essere dislocato nel territorio di altra regione - con il rischio,    &#13;
 in tali ipotesi, che il produttore sia assoggettato, per il  medesimo    &#13;
 fatto, alla potestà sanzionatoria di due diverse regioni.<diritto>Considerato in diritto</diritto>Dubita   la   Corte   di  Cassazione,  prima  sezione  civile,  in    &#13;
 riferimento agli articoli 3,  97  e  117  della  Costituzione,  della    &#13;
 legittimità  costituzionale  dell'art.  4  della legge della Regione    &#13;
 Liguria 14 aprile 1983, n. 11, nella parte  in  cui  individua  quale    &#13;
 organo  competente ad applicare la sanzione amministrativa quello del    &#13;
 luogo di accertamento della violazione, anziché  quello  in  cui  è    &#13;
 stata  commessa  la  violazione della vigente normativa in materia di    &#13;
 igiene e sanità pubblica.                                               &#13;
    La questione è fondata.                                              &#13;
    Fra  le  funzioni  amministrative  in materia di igiene e sanità,    &#13;
 trasferite alle regioni dal d.P.R. 24 luglio 1977, n. 616,  vi  sono,    &#13;
 come  ricorda  anche  il  giudice a quo, quelle concernenti la tutela    &#13;
 igienico-sanitaria della produzione, commercio  e  lavorazione  delle    &#13;
 sostanze  alimentari:  è  dunque  fuori  discussione  la  competenza    &#13;
 regionale per l'applicazione delle relative  sanzioni  amministrative    &#13;
 (si veda, nella giurisprudenza più recente di questa Corte, la sent.    &#13;
 n. 1034 del 1988).                                                       &#13;
    Non   per   questo,   il   legislatore  regionale  può  sottrarsi    &#13;
 all'osservanza  dei  principi  fondamentali  posti  dal   legislatore    &#13;
 statale,  ai  sensi  dell'art. 117 della Costituzione, secondo quanto    &#13;
 già precisato da questa Corte (v., ad  es.,  la  sent.  n.  350  del    &#13;
 1991).  Nella  materia  in  esame,  l'art. 17 della legge 24 novembre    &#13;
 1981, n. 689, introduce un principio  sugli  uffici  territorialmente    &#13;
 competenti che vincola il legislatore regionale, anche a salvaguardia    &#13;
 del buon andamento dell'attività amministrativa.                        &#13;
    Va  infine considerato che la disposizione di cui all'art. 4 della    &#13;
 legge regionale n. 11 del 1983, incongruamente derogando  alla  legge    &#13;
 generale  sull'applicazione  delle  sanzioni  amministrative (art. 7,    &#13;
 ultimo comma, legge regionale 2 dicembre 1982, n. 45),  introduce  un    &#13;
 elemento   di  contraddittorietà  nella  stessa  legislazione  della    &#13;
 Regione, e  va  dunque  censurato  anche  alla  luce  del  canone  di    &#13;
 razionalità.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  dell'art. 4 della legge    &#13;
 della Regione Liguria 14 aprile 1983, n. 11 (Norme per l'applicazione    &#13;
 delle sanzioni amministrative  pecuniarie  in  materia  di  igiene  e    &#13;
 sanità  pubblica,  vigilanza  sulle farmacie e polizia veterinaria),    &#13;
 nella parte in cui individua quale  organo  competente  all'esercizio    &#13;
 delle  funzioni  di  cui  alla legge regionale 2 dicembre 1982, n. 45    &#13;
 (Norme per l'applicazione delle sanzioni amministrative pecuniarie di    &#13;
 competenza della Regione o di enti da essa  individuati,  delegati  o    &#13;
 sub delegati), il Sindaco del comune nel cui territorio la violazione    &#13;
 è  stata  accertata,  anziché  il  Sindaco  del  comune  in  cui la    &#13;
 violazione è stata commessa.                                            &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 6 ottobre 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 14 ottobre 1993.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
