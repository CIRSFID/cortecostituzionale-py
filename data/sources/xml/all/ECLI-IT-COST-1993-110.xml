<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>110</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:110</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/03/1993</data_decisione>
    <data_deposito>26/03/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, &#13;
 prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, &#13;
 prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, &#13;
 prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 5, secondo    &#13;
 comma, del d.-l. 29 marzo  1991,  n.  108  (Disposizioni  urgenti  in    &#13;
 materia  di  sostegno  dell'occupazione),  convertito  nella legge 1°    &#13;
 giugno 1991, n. 169, promosso con ordinanza emessa il  5  marzo  1992    &#13;
 dal  Pretore di Milano nel procedimento civile vertente tra la S.p.a.    &#13;
 3M Italia e l'I.N.P.S. ed altri, iscritta  al  n.  319  del  registro    &#13;
 ordinanze 1992 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 26, prima serie speciale, dell'anno 1992;                             &#13;
    Visti   gli   atti  di  costituzione  della  s.p.a.  3M  Italia  e    &#13;
 dell'I.N.P.S.;                                                           &#13;
    Udito  nell'udienza  pubblica  del  3  novembre  1992  il  Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Udito l'avv. Federico Sorrentino per la s.p.a. 3M Italia;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Nel corso di un procedimento civile promosso dalla S.p.a. 3M    &#13;
 Italia contro  l'INPS  per  far  accertare  di  non  essere  soggetta    &#13;
 all'onere  contributivo  previsto  dall'art. 5, comma 5, del d.-l. 29    &#13;
 marzo 1991, n. 108, convertito nella legge 1° giugno 1991, n. 169, in    &#13;
 conseguenza del prepensionamento di undici suoi dipendenti,  il  Pretore  di  Milano,  con  ordinanza  del 5 marzo 1992, ha sollevato, in    &#13;
 riferimento agli artt.  3  e  97  della  Costituzione,  questione  di    &#13;
 legittimità costituzionale dell'art. 5, comma 2, del decreto citato,    &#13;
 nella  parte  i cui subordina il beneficio dell'esonero delle imprese    &#13;
 dagli oneri contributivi connessi al prepensionamento  di  dipendenti    &#13;
 (con  accollo  integrale  a carico della Cassa integrazione guadagni)    &#13;
 non  solo  alla presentazione della domanda dell'azienda al Ministero    &#13;
 del lavoro entro il 28 febbraio 1989, ma altresì alla condizione  di    &#13;
 "giacenza"  della  domanda presso il CIPI entro la stessa data. Nella    &#13;
 specie la domanda, presentata al Ministero del lavoro il  7  dicembre    &#13;
 1988,  alla  data del 28 febbraio 1989 non risultava ancora trasmessa    &#13;
 al CIPI.                                                                 &#13;
    Ad avviso del giudice  remittente  la  norma  impugnata  viola  il    &#13;
 principio  di  eguaglianza  e  il  principio  di  imparzialità della    &#13;
 pubblica amministrazione in quanto fa dipendere il  ripristino  dello    &#13;
 sgravio  contributivo  previsto  dalla legislazione precedente, oltre    &#13;
 che da una tempestiva domanda dell'impresa al Ministero  del  lavoro,    &#13;
 da   una   condizione   ulteriore,   il   cui  adempimento  è  nella    &#13;
 disponibilità esclusiva del competente ufficio di  detto  Ministero,    &#13;
 il  quale  deve  curare la trasmissione della domanda e dell'allegata    &#13;
 documentazione alla segreteria  del  CIPI  presso  il  Ministero  del    &#13;
 bilancio.                                                                &#13;
 S                                                                        &#13;
   2.  -  Nel  giudizio davanti alla Corte si è costituito l'INPS, il    &#13;
 quale, premesso di non conoscere la ratio della condizione in  esame,    &#13;
 conclude allo stato per l'infondatezza della questione.                  &#13;
    Si  è pure costituita la parte privata chiedendo che la questione    &#13;
 sia dichiarata fondata. Nell'atto di costituzione e  in  una  memoria    &#13;
 aggiunta  è  ampiamente sviluppato l'argomento dell'irragionevolezza    &#13;
 della condizione contestata. Secondo la società  attrice,  la  norma    &#13;
 impugnata  non  può spiegarsi se non con l'intento di alleggerire il    &#13;
 più possibile il peso dei prepensionamenti sul pubblico  erario;  ma    &#13;
 tale  obiettivo  deve  essere  perseguito  con  mezzi  ragionevoli  e    &#13;
 conformi ai precetti costituzionali.<diritto>Considerato in diritto</diritto>1. - Dal Pretore di Milano è sollevata, in riferimento agli artt.    &#13;
 3 e 97 della Costituzione, questione di  legittimità  costituzionale    &#13;
 dell'art.  5,  comma  2,  del d.-l. 29 marzo 1991, n. 108, convertito    &#13;
 nella legge 1° giugno 1991, n. 169, nella parte in cui  subordina  il    &#13;
 beneficio   dell'esonero   delle  imprese  dagli  oneri  contributivi    &#13;
 previsti dal successivo comma 5, in connessione  al  prepensionamento    &#13;
 di dipendenti, non solo alla presentazione della domanda al Ministero    &#13;
 del  lavoro entro il 28 febbraio 1989, ma altresì alla condizione di    &#13;
 "giacenza" della domanda presso il CIPI entro la medesima data.          &#13;
    2. - La questione è fondata.                                         &#13;
    Il giudice remittente interpreta  la  disposizione  impugnata  nel    &#13;
 senso  che  la  domanda  di  prepensionamento  non  solo  deve essere    &#13;
 presentata dall'azienda al Ministero del lavoro entro il 28  febbraio    &#13;
 1989,  ma  deve anche, entro la stessa data, essere trasmessa al CIPI    &#13;
 (Comitato  interministeriale  per  il  coordinamento  della  politica    &#13;
 industriale)  presso  il  Ministero  del  bilancio,  corredata  della    &#13;
 proposta di accertamento prevista dall'art. 2,  quinto  comma,  della    &#13;
 legge  12  agosto 1977, n. 675. Così interpretata, la norma viola il    &#13;
 principio di eguaglianza  (art.  3  della  Costituzione)  perché  fa    &#13;
 dipendere  il  diritto  delle aziende allo sgravio contributivo dalla    &#13;
 maggiore o minore sollecitudine  con  cui  l'ufficio  competente  del    &#13;
 Ministero   del   lavoro  prende  in  esame  le  singole  domande  di    &#13;
 prepensionamento e dalla maggiore o minore durata dei  tempi  tecnici    &#13;
 occorrenti  per  formulare  la  proposta  che  deve  accompagnare  la    &#13;
 trasmissione della domanda  al  CIPI.  Più  volte  questa  Corte  ha    &#13;
 ravvisato  una  irragionevole  disparità di trattamento in norme che    &#13;
 subordinavano  l'applicazione  di  un   beneficio   alla   condizione    &#13;
 dell'espletamento  di  una  procedura  amministrativa  entro un certo    &#13;
 termine (sentenze nn. 85 del 1965 e 121 del 1967).                       &#13;
    3. - La norma impugnata  potrebbe  essere  messa  in  armonia  con    &#13;
 l'art.  3  della  Costituzione  in  via  di  interpretazione se fosse    &#13;
 consentito intendere la frase "giacenti presso il CIPI" non nel senso    &#13;
 (ritenuto dal giudice a quo) di "(già) trasmesse  al  CIPI",  bensì    &#13;
 nel   senso   di   "non   (ancora)   definite  dal  CIPI",  concetto,    &#13;
 quest'ultimo, includente  sia  le  domande  di  prepensionamento  non    &#13;
 ancora  trasmesse  al CIPI entro il 28 febbraio 1989, sia quelle già    &#13;
 trasmesse,   ma   non   ancora   decise   a   tale   data.   Siffatta    &#13;
 interpretazione,  sebbene  di  per  sé  conforme  all'accezione  del    &#13;
 termine "giacenza"  (di  una  pratica)  nel  linguaggio  burocratico,    &#13;
 toglierebbe  però  al requisito in esame ogni significato normativo,    &#13;
 finendo col leggere la  disposizione  impugnata  -  contenuta  in  un    &#13;
 decreto  convertito in legge dopo essere stato reiterato undici volte    &#13;
 - nei termini in cui era formulata nei primi sei decreti della  serie    &#13;
 (iniziata  dal d.-l. 1° aprile 1989, n. 119), i quali si limitavano a    &#13;
 richiedere la presentazione delle domande di  prepensionamento  entro    &#13;
 il  28  febbraio  1989,  ai  sensi dell'art. 5, comma 1, del d.-l. 11    &#13;
 gennaio 1989, n. 5 (non convertito).                                     &#13;
    La nuova formulazione adottata dall'art. 4, comma 2, del d.-l.  24    &#13;
 aprile  1990, n. 82 (non convertito) e ripetuta nell'art. 5, comma 2,    &#13;
 del d.-l. n. 108 del 1991 (convertito), ha tradito  l'intenzione  del    &#13;
 legislatore,  che  non  era  quella  di  restringere la cerchia delle    &#13;
 aziende beneficiarie, ma soltanto di precisare  (superfluamente)  che    &#13;
 le  domande  presentate  in  data anteriore al 1° marzo 1989, ai fini    &#13;
 dell'art. 5, comma 1, del decaduto d.-l. n. 5  del  1989,  rimanevano    &#13;
 valide  anche  se  alla  data  del  28 febbraio 1989 non fosse ancora    &#13;
 intervenuta una deliberazione positiva del CIPI ai sensi dell'art. 2,    &#13;
 quinto comma, della legge n. 675 del  1977,  indipendentemente  dalla    &#13;
 circostanza  che  entro  tale  data fossero già state o no trasmesse    &#13;
 alla segreteria del CIPI con la proposta del Ministro del lavoro.        &#13;
    Tale intenzione non è ricostruibile  dall'interprete  in  termini    &#13;
 compatibili  col  tenore  letterale della disposizione, e pertanto si    &#13;
 deve far luogo - alla stregua dell'interpretazione  rigorosa  accolta    &#13;
 dal   giudice   a   quo  -  a  una  dichiarazione  di  illegittimità    &#13;
 costituzionale che espunga dalla norma l'elemento  di  irrazionalità    &#13;
 da cui è oggettivamente inficiata.                                      &#13;
    4.  -  Resta  assorbito  l'altro  motivo  di impugnazione riferito    &#13;
 all'art. 97 della Costituzione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara l'illegittimità costituzionale dell'art. 5, comma 2,  del    &#13;
 d.-l.  29  marzo  1991,  n.  108  (Disposizioni urgenti in materia di    &#13;
 sostegno dell'occupazione), convertito nella legge 1° giugno 1991, n.    &#13;
 169, nella  parte  in  cui  subordina  lo  sgravio  dell'impresa  dal    &#13;
 contributo  all'INPS, previsto dal successivo comma 5, oltre che alla    &#13;
 presentazione della domanda di pensionamento anticipato entro  il  28    &#13;
 febbraio  1989,  anche  alla  condizione della giacenza della domanda    &#13;
 presso il CIPI alla medesima data.                                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 marzo 1993.                                &#13;
                       Il Presidente: BORZELLINO                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 marzo 1993.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
