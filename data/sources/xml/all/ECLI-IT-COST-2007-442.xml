<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>442</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:442</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Alfonso Quaranta</relatore_pronuncia>
    <redattore_pronuncia>Alfonso Quaranta</redattore_pronuncia>
    <data_decisione>12/12/2007</data_decisione>
    <data_deposito>20/12/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, 2, 3, lettera a), e 4 della legge della Regione Friuli-Venezia Giulia 25 agosto 2006, n. 18 (Istituzione della fondazione per la valorizzazione archeologica, monumentale e urbana di Aquileia e finanziamenti per lo sviluppo turistico dell'area), promosso dal Presidente del Consiglio dei ministri notificato il 30 ottobre 2006, depositato in cancelleria il 7 novembre 2006 ed iscritto al n. 108 del registro ricorsi 2006. &#13;
    Visto l'atto di costituzione della Regione Friuli-Venezia Giulia; &#13;
    udito nella camera di consiglio del 21 novembre 2007 il Giudice relatore Alfonso Quaranta. &#13;
    Ritenuto che, con ricorso notificato il 30 ottobre 2006 e depositato il successivo 7 novembre, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato gli articoli «3, lettera a)» (recte: 3, comma 1, lettera a), 1, 2 e 4 e gli articoli ad essi collegati della legge della Regione Friuli-Venezia Giulia 25 agosto 2006, n. 18 (Istituzione della fondazione per la valorizzazione archeologica, monumentale e urbana di Aquileia e finanziamenti per lo sviluppo turistico dell'area), per asserita violazione dell'art. 117, secondo comma, lettera s), e terzo comma, nonché degli artt. 117 e 118 della Costituzione, nonché dell'art. 6 della legge costituzionale 31 gennaio 1963, n. 1 (Statuto speciale della Regione Friuli-Venezia Giulia) e dell'art. 10 della legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della Costituzione);  &#13;
    che il ricorrente premette che la Regione Friuli-Venezia Giulia, con la predetta legge n. 18 del 2006, ha istituito una Fondazione regionale, denominata «Fondazione Aquileia», alla quale ha affidato la gestione degli strumenti di valorizzazione del patrimonio archeologico e monumentale del Comune di Aquileia; &#13;
    che tale legge, che è composta di nove disposizioni, ha abrogato la legge regionale 13 giugno 1988, n. 47 (Interventi per la valorizzazione del patrimonio culturale, storico ed ambientale di Aquileia), la legge regionale 12 maggio 1977, n. 25 (Modifiche ed integrazioni della legge regionale 10 agosto 1970, n. 33, concernente interventi straordinari per il Comune di Aquileia) e la legge regionale 10 agosto 1970, n. 33 (Interventi straordinari per lo sviluppo sociale, economico e turistico di Aquileia e provvedimenti d'integrazione della legge 9 marzo 1967, n. 121, per la salvaguardia e la valorizzazione delle sue zone archeologiche); &#13;
    che la difesa erariale, dopo avere riportato il contenuto dell'intera legge n. 18 del 2006, assume che gli artt. 3, comma 1, lettera a), 1, 2 e 4 e gli articoli ad essi collegati eccederebbero la competenza legislativa cosiddetta “integrativa” riconosciuta alla Regione dall'art. 6, numero 3, dello statuto speciale in materia di «antichità e belli arti, tutela del paesaggio, della flora e della fauna», ed eccederebbe, altresì, applicando la clausola di maggior favore di cui all'art. 10 della legge costituzionale n. 3 del 2001, la competenza concorrente attribuita alle Regioni in materia di valorizzazione dei beni culturali dall'art. 117, terzo comma, della Costituzione;  &#13;
    che, in particolare, l'art. 3, comma 1, lettera a), prevedendo che la Fondazione di Aquileia, istituita dalla Regione per la valorizzazione di tale sito, predisponga i piani delle attività di ricerca archeologica nella zona stessa, inciderebbe sulla competenza legislativa esclusiva statale in materia di tutela dei beni culturali di cui all'art. 117, secondo comma, lettera s); &#13;
    che si osserva, inoltre, che l'art. 88 del decreto legislativo 22 gennaio 2004, n. 42 (Codice dei beni culturali e del paesaggio, ai sensi dell'articolo 10 della legge 6 luglio 2002, n. 137) – che riserva al Ministero le ricerche archeologiche e, in genere, le opere per il ritrovamento dei beni culturali (art. 10) in qualunque parte del territorio nazionale – sarebbe norma interposta relativamente alla ricerca archeologica; &#13;
    che gli artt. 1, 2, 4 e le disposizioni connesse attinenti alla tutela e valorizzazione dei beni culturali, promuovendo la valorizzazione del patrimonio monumentale e archeologico di Aquileia (artt. 1 e 2), e conferendo alla Fondazione di Aquileia «diritti d'uso su beni immobili di proprietà» (art. 4, comma 1, lettera b), eccederebbero la competenza regionale, violando il principio fondamentale di cui all'art. 112 del d.lgs. n. 42 del 2004;  &#13;
    che quest'ultima disposizione prevede che la valorizzazione dei beni culturali è collegata alla proprietà del bene ed i siti archeologici non sono di proprietà della Regione in base al principio «dominicale della valorizzazione»; &#13;
    che, alla luce del predetto principio, la Regione non sarebbe competente a dettare disposizioni in materia di valorizzazione delle aree di interesse archeologico presenti in Aquileia, trattandosi di aree appartenenti al demanio dello Stato;  &#13;
    che il ricorrente sottolinea, inoltre, che l'art. 1, comma 1, prevede la promozione della valorizzazione archeologica di Aquileia sulla base di «un'intesa programmatica con i competenti organi dello Stato» per la realizzazione di un parco archeologico, e attribuisce, al secondo comma, alla Regione l'iniziativa della costituzione di una Fondazione per la valorizzazione di Aquileia, aperta alla partecipazione delle istituzione pubbliche competenti; &#13;
    che tale norma violerebbe il principio di leale collaborazione di cui agli artt. 117 e 118 della Costituzione, in quanto «provvede in via immediata ed unilaterale con legge, attraverso la Fondazione, alla valorizzazione di beni culturali appartenenti allo Stato senza che sia stata perfezionata l'intesa di cui al comma 1, la quale verrebbe in tal modo fortemente condizionata»; &#13;
    che tali disposizioni violerebbero, altresì, sempre nella prospettiva della difesa erariale, il principio fondamentale di cui all'art. 112, comma 4, del d.lgs. n. 42 del 2004, il quale, nel testo originario, prevedeva che «Al fine di coordinare, armonizzare ed integrare le attività di valorizzazione dei beni del patrimonio culturale di appartenenza pubblica, lo Stato, per il tramite del Ministero, le regioni e gli altri enti pubblici territoriali stipulano accordi su base regionale, al fine di definire gli obbiettivi e fissarne i tempi e le modalità di attuazione»; &#13;
    che si è costituita in giudizio la Regione Friuli-Venezia Giulia chiedendo, con riserva di indicare le ragioni, che il ricorso venga respinto perché inammissibile e infondato; &#13;
    che con atto depositato presso la cancelleria della Corte in data 5 giugno 2007, il Presidente del Consiglio dei ministri ha dichiarato di rinunciare al ricorso proposto;  &#13;
    che la Regione resistente ha accettato la rinuncia con atto depositato presso la cancelleria della Corte in data 15 ottobre 2007.  &#13;
    Considerato che, ai sensi dell'art. 25 delle norme integrative per i giudizi dinanzi a questa Corte, la rinuncia al ricorso, seguita dall'accettazione della controparte, comporta l'estinzione del processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara estinto il processo. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 12 dicembre 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfonso QUARANTA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 20 dicembre 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
