<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1980</anno_pronuncia>
    <numero_pronuncia>85</numero_pronuncia>
    <ecli>ECLI:IT:COST:1980:85</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Guido Astuti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/06/1980</data_decisione>
    <data_deposito>11/06/1980</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - &#13;
 Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA &#13;
 - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale dell'art. 44 del  &#13;
 d.P.R.  26  ottobre  1972,  n.  636  (Revisione  della  disciplina  del  &#13;
 contenzioso  tributario),  e degli artt. 10, secondo comma, n. 14, e 15  &#13;
 della legge 9 ottobre 1971, n. 825 (legge di delegazione), promossi con  &#13;
 ordinanza emessa il 25 novembre 1977 dalla Commissione tributaria di  1  &#13;
 grado  di  Milano sul ricorso proposto da Ronchi Elena ed altri e con 4  &#13;
 ordinanze emesse il 15 maggio 1979 dalla Commissione  tributaria  di  2  &#13;
 grado di Oristano sui ricorsi proposti da Bozano Luigi, rispettivamente  &#13;
 iscritte  ai nn. 539, 848, 849, 850 e 851 del registro ordinanze 1979 e  &#13;
 pubblicate nella Gazzetta Ufficiale  della  Repubblica  n.  258  del  9  &#13;
 settembre 1979 e n. 36 del 6 febbraio 1980.                              &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio  del  22  aprile  1980  il  Giudice  &#13;
 relatore Guido Astuti.                                                   &#13;
     Ritenuto  che  con  le  ordinanze  di  cui  in  epigrafe  è  stata  &#13;
 sollevata, in riferimento agli artt. 3  e  24  della  Costituzione,  la  &#13;
 questione  di  legittimità  costituzionale  dell'art. 44 del d.P.R. 26  &#13;
 ottobre 1972,  n.  636  (revisione  della  disciplina  del  contenzioso  &#13;
 tributario);                                                             &#13;
     che   con  le  ordinanze  nn.  848  -  851/1979  della  Commissione  &#13;
 tributaria di secondo grado di Oristano è stata altresì sollevata, in  &#13;
 riferimento  all'art.  76   della   Costituzione,   la   questione   di  &#13;
 legittimità costituzionale degli artt.  10, secondo comma, n. 14, e 15  &#13;
 della  legge  9  ottobre 1971, n. 825, e 44, terzo comma, del d.P.R. 26  &#13;
 ottobre 1972, n. 636, in quanto il legislatore delegante avrebbe omesso  &#13;
 di dettare, in ordine alla istanza  per  fissazione  d'udienza  di  cui  &#13;
 all'art. 44 del decreto delegato, "i principi e i criteri direttivi con  &#13;
 la  doverosa,  massima  precisione  che l'art. 76 Cost. gli imponeva di  &#13;
 indicare".                                                               &#13;
     Considerato  che  la  questione  di   legittimità   costituzionale  &#13;
 dell'art.  44  del d.P.R. n. 636 del 1972, in riferimento agli articoli  &#13;
 3, 24 e 76 Cost., è già stata decisa e ritenuta non fondata da questa  &#13;
 Corte con sentenza 20 aprile 1977, n. 63;                                &#13;
     che  in  particolare,  anche per quanto concerne la irreparabilità  &#13;
 degli effetti della mancata proposizione dell'istanza per fissazione di  &#13;
 udienza, - dato che i termini di decadenza e  di  prescrizione  che  ai  &#13;
 sensi  dell'art.  44,  terzo  comma, decorrono o riprendono a decorrere  &#13;
 dalla data di notificazione dell'ordinanza presidenziale di  estinzione  &#13;
 del   processo,   sono,   secondo  la  costante  interpretazione  della  &#13;
 Commissione  tributaria  centrale,  unicamente  quelli  concernenti  la  &#13;
 realizzazione      dell'obbligazione      tributaria      da      parte  &#13;
 dell'amministrazione finanziaria, e non i termini  processuali  interni  &#13;
 del  processo, concernenti impugnazioni ormai esaurite - , la questione  &#13;
 deve dichiararsi infondata in  base  alle  considerazioni  già  svolte  &#13;
 nell'ordinanza  6  dicembre 1977, n.   144, in quanto la sanzione della  &#13;
 estinzione del procedimento  per  inattività  del  contribuente  trova  &#13;
 giustificazione  nelle speciali esigenze connesse alla prima attuazione  &#13;
 della riforma, già illustrate  nella  motivazione  della  sentenza  di  &#13;
 questa  Corte, "mentre - come precisato nella ricordata ordinanza - non  &#13;
 integra ulteriore  profilo  di  contrasto  con  l'art.    24  Cost.  la  &#13;
 circostanza  che  nel  processo tributario, diretto all'annullamento di  &#13;
 atti di imposizione o  di  comportamenti  equipollenti  della  pubblica  &#13;
 amministrazione,  il  termine  per  ricorrere  sia  sempre  termine  di  &#13;
 decadenza, e non possa trovare applicazione il principio  sancito,  per  &#13;
 le azioni civili, dall'art. 310 del codice di procedura civile";         &#13;
     che infine, per quanto concerne la prospettata violazione dell'art.  &#13;
 76  Cost.,  questa Corte ha già dichiarato che le norme della legge di  &#13;
 delegazione "non contengono alcuna  determinazione  specifica  rispetto  &#13;
 alla  quale possa ravvisarsi contrasto o esorbitanza nelle disposizioni  &#13;
 dei primi tre commi dell'art. 44 del decreto legislativo" (sentenza  n.  &#13;
 63  del  1977); mentre per converso la ampiezza delle enunciative degli  &#13;
 artt.  10, secondo comma, n. 14, e 15 della legge  di  delegazione,  in  &#13;
 ordine  alla  riforma  del  procedimento contenzioso davanti alle nuove  &#13;
 commissioni tributarie, e alla emanazione delle opportune  disposizioni  &#13;
 transitorie  e  di attuazione, non appare tale da integrare inesistenza  &#13;
 di principi e criteri direttivi, non potendosi pretendere che la  legge  &#13;
 di  delega,  preordinata  ad  una generale riforma del sistema fiscale,  &#13;
 provvedesse alla puntuale specificazione della  disciplina  processuale  &#13;
 idonea  a  soddisfare  l'esigenza  di  una ricognizione dello stato dei  &#13;
 numerosissimi giudizi pendenti;                                          &#13;
     che  d'altra  parte,  per  le  considerazioni  già  svolte   nella  &#13;
 ricordata  sentenza,  l'introduzione  di  un  adempimento  a carico dei  &#13;
 contribuenti  per  ottenere  dalle  nuove  commissioni  tributarie   la  &#13;
 trattazione  di  giudizi  in corso, evitandone la comminata estinzione,  &#13;
 non confligge con la garanzia costituzionale  del  diritto  di  difesa,  &#13;
 sancita dall'art. 24 della Costituzione;                                 &#13;
     che  nelle ordinanze non sono prospettati altri profili né addotti  &#13;
 motivi  che  possano  indurre  la  Corte  a   modificare   la   propria  &#13;
 giurisprudenza.                                                          &#13;
     Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.  &#13;
 87, e 9, secondo comma, delle Norme integrative per i  giudizi  davanti  &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza delle questioni di legittimità  &#13;
 costituzionale dell'art. 44 del d.P.R. 26 ottobre 1972, n. 636, e degli  &#13;
 artt. 10, secondo comma, n. 14, e 15 della legge  9  ottobre  1971,  n.  &#13;
 825,  sollevate con le ordinanze di cui in epigrafe in riferimento agli  &#13;
 artt. 3, 24 e 76 della Costituzione, e già decise con la  sentenza  n.  &#13;
 63 e con l'ordinanza n. 144 del 1977.                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 giugno 1980.          &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE   STEFANO   -   LEOPOLDO   ELIA  -  &#13;
                                   GUGLIELMO ROEHRSSEN - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
