<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>31</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:31</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1992</data_decisione>
    <data_deposito>03/02/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  degli artt. 5, terzo    &#13;
 comma, n. 3, della legge 21 febbraio 1980, n. 28 (Delega  al  Governo    &#13;
 per il riordinamento della docenza universitaria e relativa fascia di    &#13;
 formazione  e per la sperimentazione organizzativa e didattica) e 50,    &#13;
 n. 3, del d.P.R. 11 luglio 1980, n. 382 (Riordinamento della  docenza    &#13;
 universitaria, relativa fascia di formazione, nonché sperimentazione    &#13;
 organizzativa  e  didattica),  promosso  con  ordinanza  emessa il 26    &#13;
 ottobre 1990 dal Consiglio di Stato - Sezione  VI  giurisdizionale  -    &#13;
 sul  ricorso  proposto  da  Antonino  Vitarelli  ed  altri  contro il    &#13;
 Ministero della pubblica istruzione ed altro, iscritta al n. 314  del    &#13;
 registro  ordinanze  1991 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 19, prima sperie speciale, dell'anno 1991;                 &#13;
    Visti gli atti di costituzione  di  Maria  Ambrosini  ed  altri  e    &#13;
 Antonino  Vitarelli,  nonché l'atto di intervento del Presidente del    &#13;
 Consiglio dei ministri;                                                  &#13;
    Udito  nell'udienza  pubblica  del  3  dicembre  1991  il  Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Uditi   l'avvocato  Massimo  Colarizi  per  Antonino  Vitarelli  e    &#13;
 l'avvocato dello Stato Carlo Tonello per il Presidente del  Consiglio    &#13;
 dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ordinanza emessa il 26 ottobre 1990 (pervenuta alla Corte    &#13;
 costituzionale  il  16  aprile  1991) il Consiglio di Stato - Sez. VI    &#13;
 Giurisdizionale - sul ricorso proposto da Vitarelli Antonino ed altri    &#13;
 contro il Ministero della pubblica istruzione ed altro (Reg. ord.  n.    &#13;
 314/1991)  ha  sollevato  questione  di  legittimità  costituzionale    &#13;
 "degli artt. 5, comma terzo, n. 3, della legge n. 21  febbraio  1980,    &#13;
 n.  28  e  50, n. 3, del d.P.R. 11 luglio 1980, n. 382 nella parte in    &#13;
 cui non contemplano, tra i soggetti che  possono  essere  ammessi  ai    &#13;
 giudizi  di idoneità per professore associato, i titolari di assegni    &#13;
 di formazione didattica e scientifica, di cui all'art. 6 del decreto-legge 1 ottobre 1973, n. 580,  convertito  nella  legge  30  novembre    &#13;
 1973,  n.  766,  che entro l'anno accademico 1979/1980 abbiano svolto    &#13;
 per un triennio attività  didattica  e  scientifica,  comprovata  da    &#13;
 pubblicazioni  edite  documentate  dal preside di facoltà in base ad    &#13;
 atti risalenti al periodo di svolgimento delle  attività  medesime",    &#13;
 in  riferimento  all'art.  3  della  Costituzione,  per disparità di    &#13;
 trattamento, ad asserita parità di condizioni, rispetto  ai  tecnici    &#13;
 laureati,  ai  medici  interni  di  cui  alla  sentenza  della  Corte    &#13;
 costituzionale  14  aprile  1986,  n.   89,   nonché   rispetto   ai    &#13;
 contrattisti  universitari  di  cui all'altra sentenza della Corte 13    &#13;
 luglio 1989, n. 397.                                                     &#13;
    Il giudizio a quo verte sul ricorso in appello avverso la sentenza    &#13;
 del Tribunale amministrativo regionale del Lazio, con la  quale,  nel    &#13;
 respingere il ricorso del dott. Vitarelli, si affermò che non poteva    &#13;
 estendersi  il  dettato  della  citata sentenza n. 89 del 1986 fino a    &#13;
 comprendervi  i  medici  assegnisti  (quale  si  era  presentato   il    &#13;
 ricorrente), ai fini dell'ammissione alla seconda tornata dei giudizi    &#13;
 di idoneità a professore associato.                                     &#13;
    Osservato  che  il  dottor  Vitarelli  non rientra in alcuna delle    &#13;
 puntuali categorie, individuate dalla legge, ovvero introdotte  dalla    &#13;
 giurisprudenza  costituzionale,  il  Collegio remittente richiama, in    &#13;
 particolare, la sentenza della Corte 13  luglio  1989,  n.  397,  per    &#13;
 rilevare  come  la  questione ivi trattata sia stata ritenuta fondata    &#13;
 sulla considerazione che, giusta l'art. 5 del  decreto-legge  n.  580    &#13;
 del  1973,  convertito  nella  legge  n.  766 del 1973, i titolari di    &#13;
 contratto sono equiparati agli assistenti qualora,  a  parità  delle    &#13;
 altre  condizioni,  oltre  i  limiti  di  impegno attinenti alla loro    &#13;
 qualità  scientifica  previsti  dallo  stesso   articolo,   svolgano    &#13;
 attività  di assistenza e cura. Precisazione questa che rende palese    &#13;
 come i contrattisti venivano a trovarsi in posizione  sostanzialmente    &#13;
 analoga  a  quella  dei  medici  interni,  oggetto  della  precedente    &#13;
 sentenza n. 89 del  1986,  recante  disparità  rispetto  ai  tecnici    &#13;
 laureati.                                                                &#13;
    E  dunque,  per  quanto  concerne  gli  assegnisti  ex  art. 6 del    &#13;
 decreto-legge n. 580 del 1973, la  medesima  disparità  rispetto  ai    &#13;
 tecnici  laureati  è  per il Collegio a quo irrazionale e lesiva del    &#13;
 principio  di   eguaglianza   come   recepito   dall'art.   3   della    &#13;
 Costituzione, versandosi - si sostiene - in situazione simile qualora    &#13;
 gli  assegnisti  abbiano  svolto,  a  parità di condizioni oggettive    &#13;
 (concorso,   triennio   di   riferimento,    attività    scientifica    &#13;
 documentata),  compiti  di  assistenza  e  cura.  Anche  per  costoro    &#13;
 ricorrerebbe la medesima  ratio  che,  muovendo  dalla  constatazione    &#13;
 della   situazione   di   fatto   venutasi  all'epoca  a  determinare    &#13;
 nell'ambito universitario a seguito  del  decreto-legge  n.  580  del    &#13;
 1973,  consentiva  - attraverso il transitorio sistema dei giudizi di    &#13;
 idoneità - il passaggio  alla  figura,  di  nuova  istituzione,  del    &#13;
 professore associato.                                                    &#13;
    2. - Si sono costituiti in giudizio i ricorrenti unendosi ai dubbi    &#13;
 di   costituzionalità   espressi   nell'ordinanza  di  rimessione  e    &#13;
 assumendo l'assoluta identità delle situazioni poste a confronto.       &#13;
    È  intervenuto  il  Presidente  del   Consiglio   dei   ministri,    &#13;
 rappresentato  e  difeso dall'Avvocatura generale dello Stato, che ha    &#13;
 concluso   per   l'inammissibilità   ovvero   l'infondatezza   della    &#13;
 questione.  L'inammissibilità deriverebbe dal fatto che si richiede,    &#13;
 in sostanza, un provvedimento fondato su precedenti  sentenze  "additive":  con  ciò  la  Corte  "finirebbe per allontanarsi troppo" dal    &#13;
 modello delineato dalle disposizioni costituzionali  regolatrici  del    &#13;
 giudizio di legittimità costituzionale.                                 &#13;
    Nel  merito  non vi sarebbe "alcun dato normativo di equiparazione    &#13;
 agli assistenti ospedalieri come invece per i contrattisti medici con    &#13;
 l'art. 5 della legge n. 580 del 1973".<diritto>Considerato in diritto</diritto>1. -  Quanto  prospettato  nell'ordinanza  di  rimessione  importa    &#13;
 stabilire  se  l'art.  5,  terzo comma, n. 3, della legge 21 febbraio    &#13;
 1980, n. 28 (Delega al Governo per  il  riordinamento  della  docenza    &#13;
 universitaria   e   relativa   fascia   di   formazione   e   per  la    &#13;
 sperimentazione organizzativa e didattica) e l'art.  50,  n.  3,  del    &#13;
 d.P.R.   11   luglio   1980.  n.  382  (Riordinamento  della  docenza    &#13;
 universitaria, relativa fascia di formazione nonché  sperimentazione    &#13;
 organizzativa   e   didattica)   contrastino   con   l'art.  3  della    &#13;
 Costituzione non essendo  state  contemplate  tra  le  qualifiche  da    &#13;
 ammettere, in via transitoria, ai giudizi di idoneità per professore    &#13;
 associato,   i  titolari  di  assegni  di  formazione  scientifica  e    &#13;
 didattica di cui all'art. 6 del decreto-legge  1'  ottobre  1973,  n.    &#13;
 580,   convertito   nella   legge   30  novembre  1973,  n.  766  con    &#13;
 modificazioni, che entro l'anno accademico  1979/80  abbiano  svolto,    &#13;
 per un triennio, comprovata attività didattica e scientifica.           &#13;
    Secondo  l'assunto  del  Collegio  remittente  sarebbe identica la    &#13;
 posizione dei titolari di  assegno,  nell'ambito  della  facoltà  di    &#13;
 medicina con il connesso esercizio di attività medica, con i tecnici    &#13;
 laureati  specificamente  previsti  dalla legge, nonché con i medici    &#13;
 interni (aiuti e assistenti) ed i contrattisti universitari,  oggetto    &#13;
 delle  sentenze di questa Corte n. 89 del 1986 e, rispettivamente, n.    &#13;
 397 del 1989.                                                            &#13;
    2.   -   L'Avvocatura   dello   Stato    oppone    preliminarmente    &#13;
 l'inammissibilità  della  questione:  essa  -  nell'unicum che si è    &#13;
 venuto a formare, per  gli  specifici  fini  in  esame,  tra  tecnici    &#13;
 laureati,  medici interni e contrattisti verrebbe a fondarsi, in gran    &#13;
 parte, sui principi contenuti nelle sentenze della Corte indicate  in    &#13;
 narrativa.                                                               &#13;
    Ma  l'eccezione  va  disattesa  in radice: quel che è in giuoco -    &#13;
 l'asserita disparità di trattamento - concerne,  in  ogni  caso,  un    &#13;
 presunto  contrasto  con  l'ordinamento  ed  il tertium comparationis    &#13;
 offerto  riguarda,  nella  susseguente  verifica,  soggetti  comunque    &#13;
 ammessi a sostenere il giudizio di idoneità.                            &#13;
    3. - Nel merito, la questione non è fondata.                         &#13;
    La  Corte  ha  già  considerato  e  posto  in  luce  la  funzione    &#13;
 strumentale e spiccatamente coadiuvante  dell'attività  dei  tecnici    &#13;
 laureati  in  armonia  con  quella didattica e scientifica svolta dal    &#13;
 personale docente, così come precisato dall'art. 35  del  d.P.R.  n.    &#13;
 382 citato (sentenza n. 89). Di conseguenza, con la medesima sentenza    &#13;
 è  stata  affermato,  nell'unità  di  posizioni  che si è venuta a    &#13;
 costituire, l'ammissione al giudizio de quo dei medici interni (aiuti    &#13;
 e assistenti), potendosi ingenerare altrimenti evidente sperequazione    &#13;
 rispetto a un trattamento avente conferente identità di requisiti.      &#13;
    Quanto poi ai titolari di contratto viene rilevato che per costoro    &#13;
 vennero normativamente fissati  (d.-l.  n.  580,  art.  5,)  rigorosi    &#13;
 impegni,  anche  in  termini di orario, di assistenza agli studenti e    &#13;
 soprattutto  di  controllo  del  loro  profitto  e  di   obbligo   di    &#13;
 esercitazioni;  elementi tutti che risultano rispondenti ad integrare    &#13;
 l'espletamento di prestazioni istituzionali d'ordine didattico (sent.    &#13;
 n. 397). Sì che per i contrattisti presso la facoltà di medicina  e    &#13;
 chirurgia  che ebbero a svolgere attività di assistenza e cura oltre    &#13;
 i  limiti  di  impegno  sopra  ricordati,  si   rese   necessaria   e    &#13;
 giustificata  l'equiparazione in toto, da parte del legislatore, agli    &#13;
 assistenti ospedalieri fin dal  1973  (predetto  art.  5,  undicesimo    &#13;
 comma - cfr.sent. n. 549 del 1990).                                      &#13;
    Per  converso,  per i titolari degli assegni, di cui alla presente    &#13;
 controversia, è meramente prevista una  generica  partecipazione  ai    &#13;
 seminari  ed alle esercitazioni per gli studenti (art. 6 del d.-l. n.    &#13;
 580), con il che non viene rivelata, di certo, alcuna  identità  nei    &#13;
 sensi  di  cui  innanzi. D'altronde, il servizio di assistenza e cura    &#13;
 prestato è sì equiparabile al servizio di assistente ospedaliero di    &#13;
 ruolo,  ma  -  esplicitamente  -  soltanto  ai  fini   dei   concorsi    &#13;
 ospedalieri  (d.-l.  23 dicembre 1978, n. 817, convertito nella legge    &#13;
 19 febbraio 1979 n. 54):  beneficio,  adunque,  operante  all'interno    &#13;
 dell'ordinamento  ospedaliero e non suscettibile di alcun riferimento    &#13;
 estensivo.                                                               &#13;
    Non rinvenendosi, pertanto, indizio alcuno  di  quella  identità,    &#13;
 che  si  assume  vulnerata  per disparità ex art. 3 Costituzione, la    &#13;
 questione è infondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  5,  terzo comma, n. 3, della legge 21 febbraio 1980, n. 28    &#13;
 (Delega al Governo per il riordinamento della docenza universitaria e    &#13;
 relativa fascia di formazione e per la sperimentazione  organizzativa    &#13;
 e  didattica) e dell'art. 50, n. 3, del d.P.R. 11 luglio 1980, n. 382    &#13;
 (Riordinamento  della  docenza  universitaria,  relativa  fascia   di    &#13;
 formazione,  nonché  sperimentazione  organizzativa e didattica), in    &#13;
 riferimento all'art. 3 della Costituzione, sollevata dal Consiglio di    &#13;
 Stato - Sezione VI giurisdizionale, con l'ordinanza in epigrafe.         &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 22 gennaio 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: BORZELLINO                           &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 3 febbraio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
