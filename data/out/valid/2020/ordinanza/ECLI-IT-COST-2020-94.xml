<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/94/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/94"/>
          <FRBRalias value="ECLI:IT:COST:2020:94" name="ECLI"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="94"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/94/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/94/ita@"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/94/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/94/ita@.xml"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-04-22" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-05-15" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>94</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:94</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Giulio Prosperetti</akn4cc:relatore>
        <akn4cc:data_decisione>2020-04-22</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-05-15</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<mref><ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_54">art. 59, commi 54</ref> e <ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">55, della legge 27 dicembre 1997, n. 449</ref></mref> (Misure per la stabilizzazione della finanza pubblica), e dell'art. 1 del decreto del Ministro del lavoro e della previdenza sociale 30 marzo 1998, emanato di concerto con il Ministro del tesoro, del bilancio e della programmazione economica e il Ministro per la funzione pubblica e gli affari regionali (Programmazione dell'accesso al pensionamento di anzianità dei militari, ai sensi dell'<ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">art. 59, comma 55, della legge 27 dicembre 1997, n. 449</ref>), promosso dalla Corte dei conti, sezione giurisdizionale per la Regione Puglia, nel procedimento vertente tra A. S. e il Ministero dell'interno - Direzione provinciale del tesoro, e altro, con ordinanza del 6 dicembre 2018, iscritta al n. 162 del registro ordinanze 2019 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 42, prima serie speciale, dell'anno 2019.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> dell'Istituto nazionale della previdenza sociale (INPS), nonché l'atto di intervento del Presidente del Consiglio dei ministri;
 udito il Giudice relatore Giulio Prosperetti nell'udienza del 22 aprile 2020, svolta, ai sensi del decreto della Presidente della Corte del 24 marzo 2020, punto 1), lettera c), senza discussione orale, su conformi istanze delle parti, pervenute in data 6 e 15 aprile 2020;
 deliberato nella camera di consiglio del 22 aprile 2020.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Ritenuto che la Corte dei conti, sezione giurisdizionale per la Regione Puglia, con ordinanza depositata il 6 dicembre 2018, ha sollevato questione di legittimità costituzionale dell'<mref><ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_54">art. 59, commi 54</ref> e <ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">55, della legge 27 dicembre 1997, n. 449</ref></mref> (Misure per la stabilizzazione della finanza pubblica) e dell'art. 1 del decreto del Ministro del lavoro e della previdenza sociale 30 marzo 1998, emanato di concerto con il Ministro del tesoro, del bilancio e della programmazione economica e il Ministro per la funzione pubblica e gli affari regionali (Programmazione dell'accesso al pensionamento di anzianità dei militari, ai sensi dell'<ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">art. 59, comma 55, della legge 27 dicembre 1997, n. 449</ref>), in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>;
 che il giudice a quo espone che il ricorrente, già dipendente del Ministero dell'interno, aveva presentato il 19 maggio 1997 domanda di collocamento in pensione e che l'amministrazione, con decreto in data 12 giugno 1997, ne aveva disposto la cessazione dal servizio a decorrere dal 30 novembre 1997;
 che tuttavia, a seguito dell'entrata in vigore dell'<ref href="/akn/it/act/decretoLegge/stato/1997-11-03/375/!main#art_1">art. 1 del decreto-legge 3 novembre 1997, n. 375</ref> (Disposizioni urgenti in tema di trattamenti pensionistici anticipati) - poi decaduto per decorrenza dei termini e abrogato dall'<ref href="/akn/it/act/legge/stato/1997/449/!main#art_63">art. 63 della legge n. 449 del 1997</ref> - veniva sospesa immediatamente l'applicazione di ogni disposizione di legge, di regolamento e di accordi collettivi che prevedevano il diritto a trattamenti pensionistici di anzianità anticipati rispetto all'età pensionabile o alla età prevista per la cessazione dal servizio in base ai singoli ordinamenti;
 che tale sospensione veniva definitivamente confermata dall'<ref href="/akn/it/act/legge/stato/1997/449/!main#art_59__para_54">art. 59, comma 54, della legge n. 449 del 1997</ref>, relativamente al periodo dal 3 novembre 1997 sino alla data della sua entrata in vigore (1° gennaio 1998);
 che per effetto della suddetta normativa, come integrata dal citato d.m. 30 marzo 1998, il trattamento di pensione veniva attribuito al ricorrente a decorrere dal 1° aprile 1998;
 che pertanto, il ricorrente, essendo cessato dal servizio il 30 novembre 1997, chiedeva l'accertamento del suo diritto a conseguire la pensione da tale data con il conseguente pagamento dei ratei previdenziali arretrati e non riscossi, relativi ai mesi di dicembre 1997 e gennaio, febbraio e marzo 1998, oltre interessi legali e rivalutazione monetaria, previa dichiarazione di illegittimità costituzionale delle disposizioni recate dall'<mref><ref href="/akn/it/act/legge/stato/1997/449/!main#art_59__para_54">art. 59, commi 54</ref> e <ref href="/akn/it/act/legge/stato/1997/449/!main#art_59__para_55">55, della legge n. 449 del 1997</ref></mref> e dell'art. 1 del d.m. del 30 marzo 1998, per violazione degli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38 Cost.</ref></mref>;
 che, in diritto, il giudice a quo, ritenute le norme impugnate rilevanti ai fini del decidere, deduce, con riferimento alla non manifesta infondatezza, che esse si porrebbero in «irrimediabile contrasto con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3__para_1">art. 3, comma 1, Cost.</ref> inteso quale canone di "ragionevolezza"», ledendo l'affidamento riposto dall'interessato sulla «perdurante validità delle vecchie regole del pensionamento di anzianità»;
 che le disposizioni censurate violerebbero, altresì, il principio di ragionevolezza in quanto prive di una stima dei risparmi di spesa derivanti, che devono «essere allegati e giustificati in funzione e in proporzione al sacrificio imposto agli interessi economici lesi» (in proposito il rimettente richiama le sentenze di questa Corte n. 70 del 2015 e n. 108 del 2016);
 che si è costituito l'Istituto nazionale della previdenza sociale (INPS), chiedendo di dichiarare inammissibile e, comunque, infondata la questione;
 che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, il quale ha concluso per la inammissibilità e/o manifesta infondatezza della questione, richiamando le <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/10/!main">ordinanze n. 10</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/145/!main">145 del 2011</ref></mref>, con cui questa Corte ha dichiarato manifestamente infondate le questioni promosse dallo stesso giudice rimettente in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">artt. 36</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38 Cost.</ref></mref>, nei confronti delle medesime disposizioni.
 Considerato che il giudice rimettente censura l'<mref><ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_54">art. 59, commi 54</ref> e <ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">55, della legge 27 dicembre 1997, n. 449</ref></mref> (Misure per la stabilizzazione della finanza pubblica) e l'art. 1 del decreto del Ministro del lavoro e della previdenza sociale 30 marzo 1998, emanato di concerto con il Ministro del tesoro, del bilancio e della programmazione economica e con il Ministro per la funzione pubblica e gli affari regionali (Programmazione dell'accesso al pensionamento di anzianità dei militari, ai sensi dell'<ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">art. 59, comma 55, della legge 27 dicembre 1997, n. 449</ref>), in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>;
 che, in particolare, il comma 54 dell'<ref href="/akn/it/act/legge/stato/1997/449/!main#art_59">art. 59 della legge n. 449 del 1997</ref> confermava, relativamente al periodo dal 3 novembre 1997 sino alla data di entrata in vigore della medesima legge (1° gennaio 1998), la sospensione delle previgenti norme di legge, di regolamento o di accordo collettivo attributive del diritto, con decorrenza nel periodo suindicato, a trattamenti pensionistici di anzianità anticipati rispetto all'età pensionabile o all'età prevista per la cessazione dal servizio dai singoli ordinamenti;
 che la disposizione in esame rendeva così definitiva la predetta sospensione già stabilita dall'<ref href="/akn/it/act/decretoLegge/stato/1997-11-03/375/!main#art_1">art. 1 del decreto-legge 3 novembre 1997, n. 375</ref> (Disposizioni urgenti in tema di trattamenti pensionistici anticipati), decaduto per mancata conversione ed espressamente abrogato, conservando validità agli atti e ai provvedimenti adottati e facendo salvi gli effetti prodottisi, dall'<ref href="/akn/it/act/legge/stato/1997/449/!main#art_63">art. 63 della legge n. 449 del 1997</ref>;
 che il comma 55 dell'<ref href="/akn/it/act/legge/stato/1997/449/!main#art_59">art. 59 della legge n. 449 del 1997</ref> demandava a un decreto del Ministro del lavoro e della previdenza sociale di concerto con il Ministro del tesoro, del bilancio e della programmazione economica e il Ministro per la funzione pubblica e gli affari regionali, da emanarsi entro il 31 marzo 1998, i termini di accesso al trattamento pensionistico per i lavoratori che avessero presentato, antecedentemente al 3 novembre 1997 domanda, accettata dall'amministrazione di appartenenza, per accedere al pensionamento entro il 1998;
 che, in via preliminare, deve essere disattesa la eccezione di inammissibilità avanzata dalla difesa statale in riferimento all'assenza di motivazione sulla violazione dei parametri costituiti dagli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">artt. 36</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38 Cost.</ref></mref>, posto che l'unico parametro evocato dal giudice rimettente è costituito dall'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, come si evince dal complessivo tenore dell'ordinanza e dalle argomentazioni ivi svolte;
 che, parimenti, va disattesa l'eccezione della stessa difesa statale concernente l'inammissibilità delle censure relativamente al d.m. 30 marzo 1998, non essendo esso atto di rango primario, perché, come già osservato da questa Corte nell'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/10/!main">ordinanza n. 10 del 2011</ref>, la previsione del differimento del trattamento pensionistico contenuta nell'impugnato art. 1 del predetto decreto è strettamente collegata alla disciplina dettata dalla norma primaria congiuntamente censurata;
 che questa Corte, con le <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/10/!main">ordinanze n. 10</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/145/!main">145 del 2011</ref></mref>, ha dichiarato la manifesta infondatezza delle questioni promosse dalla stessa Corte dei conti, sezione giurisdizionale per la Puglia, nei confronti delle disposizioni oggi impugnate, in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">artt. 36</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_38">38 Cost.</ref></mref>;
 che il giudice a quo ripropone la questione assumendo la violazione da parte delle disposizioni censurate del principio di ragionevolezza posto dall'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che anche in riferimento a tale censura la questione risulta manifestamente infondata;
 che, a tal fine, possono richiamarsi le argomentazioni svolte da questa Corte nelle precedenti ordinanze, in quanto, seppur svolte in riferimento al parametro dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">art. 36 Cost.</ref> evocato all'epoca dal rimettente, assumono con ogni evidenza rilievo anche al fine della valutazione dello scrutinato intervento normativo quanto alla sua complessiva ragionevolezza;
 che, difatti, questa Corte, come ricordato in particolare nell'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/10/!main">ordinanza n. 10 del 2011</ref>, «ha già più volte escluso l'illegittimità costituzionale di interventi di "blocco" dell'accesso ai trattamenti pensionistici di anzianità, come quello censurato in questa sede», in quanto sono «tutti ragionevolmente inseriti nel processo di radicale riconsiderazione di tali trattamenti al fine di stabilizzare la spesa previdenziale entro determinati livelli del rapporto con il prodotto interno lordo (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/1997/245/!main">sentenze n. 245 del 1997</ref>, n. 417 del 1996 e n. 439 del 1994; <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2001/319/!main">ordinanze n. 319</ref> e n. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2001/18/!main">18 del 2001</ref></mref>, nonché n. 318 del 1997)»;
 che nelle citate ordinanze è stato, altresì, evidenziato che le disposizioni scrutinate, nell'offrire agli interessati dalla sospensione temporanea dell'accesso al pensionamento anticipato (come il ricorrente nel giudizio principale) la possibilità di chiedere la prosecuzione e il ripristino del rapporto d'impiego, consentivano loro di non rimanere privi di reddito da lavoro, in attesa di conseguire quello pensionistico;
 che, conseguentemente, questa Corte ha rilevato che l'effetto economico negativo lamentato dall'interessato finisce per dipendere dalla sua eventuale scelta di non utilizzare gli strumenti così posti a sua disposizione e, dunque, da un suo atto volontario;
 che, inoltre, sotto diverso profilo, l'intervento normativo in oggetto, contrariamente a quanto asserito dal rimettente, risulta, dall'esame dei lavori parlamentari, corredato da analitica relazione tecnica che indica i rilevanti risparmi di spesa prodotti, conseguenti allo slittamento della decorrenza dei trattamenti pensionistici per l'effetto del loro temporaneo blocco e alla successiva ridefinizione dell'accesso attraverso il sistema delle "finestre fisse" operata dallo stesso <ref href="/akn/it/act/legge/stato/1997/449/!main#art_59">art. 59 della legge n. 449 del 1997</ref>;
 che, pertanto, le disposizioni impugnate realizzano un ragionevole contemperamento tra le finalità di riequilibrio del sistema pensionistico, con rilevanti e evidenti benefici sulla finanza pubblica, e la compressione delle aspettative dei soggetti incisi dall'intervento, quale è il ricorrente, in quanto consistente in una limitata posticipazione della decorrenza del trattamento pensionistico;
 che, per tutte tali ragioni la questione deve essere dichiarata manifestamente infondata.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'<mref><ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_54">art. 59, commi 54</ref> e <ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">55, della legge 27 dicembre 1997, n. 449</ref></mref> (Misure per la stabilizzazione della finanza pubblica), e dell'art. 1 del decreto del Ministro del lavoro e della previdenza sociale 30 marzo 1998, emanato di concerto con il Ministro del tesoro, del bilancio e della programmazione economica e il Ministro per la funzione pubblica e gli affari regionali (Programmazione dell'accesso al pensionamento di anzianità dei militari, ai sensi dell'<ref href="/akn/it/act/legge/stato/1997-12-27/449/!main#art_59__para_55">art. 59, comma 55, della legge 27 dicembre 1997, n. 449</ref>), sollevata, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 della Costituzione</ref>, dalla Corte dei conti, sezione giurisdizionale per la Regione Puglia, con l'ordinanza indicata in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-04-22">22 aprile 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giulioProsperetti" as="#relatore">Giulio PROSPERETTI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-05-15">15 maggio 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
