<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>367</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:367</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2000</data_decisione>
    <data_deposito>26/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Franco BILE, &#13;
 Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 34 del codice    &#13;
 di  procedura penale promosso, in un procedimento di ricusazione, con    &#13;
 ordinanza  emessa il 7 luglio 1999 dalla Corte di appello di Palermo,    &#13;
 iscritta  al  n. 617  del  registro ordinanze 1999 e pubblicata nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica n. 45 - prima serie speciale -    &#13;
 dell'anno 1999.                                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio del 7 giugno 2000, il giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
     Ritenuto  che  con ordinanza emessa il 7 luglio 1999, la Corte di    &#13;
 appello  di  Palermo,  chiamata  a  decidere  sulla  dichiarazione di    &#13;
 ricusazione   presentata   dalla   parte  civile  nei  confronti  del    &#13;
 Presidente  del  Tribunale di Marsala, nell'ambito di un procedimento    &#13;
 penale  per  i reati di cui agli artt. 110, 113 e 449, secondo comma,    &#13;
 cod.  pen.,  ha  sollevato,  in riferimento agli artt. 3, 24, secondo    &#13;
 comma,   e   101   della   Costituzione,  questione  di  legittimità    &#13;
 costituzionale  dell'art. 34  del  codice  di procedura penale, nella    &#13;
 parte  in cui non prevede quale causa di incompatibilità del giudice    &#13;
 l'ipotesi in cui "l'illecito da valutare penalmente sia stato oggetto    &#13;
 di  un  precedente  giudizio  in  sede  civile  da parte del medesimo    &#13;
 magistrato";                                                             &#13;
         che  la  Corte  rimettente  premette  che il giudice ricusato    &#13;
 aveva fatto parte di altro collegio dello stesso Tribunale di Marsala    &#13;
 che  aveva  definito  con sentenza il giudizio civile promosso da una    &#13;
 società  di  armamento,  a  responsabilità  limitata,  di  cui  era    &#13;
 amministratore unico uno degli attuali imputati, esprimendo in quella    &#13;
 sede  in  modo inequivoco il suo convincimento sui fatti divenuti poi    &#13;
 oggetto del procedimento penale e affermando, in particolare, che "le    &#13;
 violazioni   commesse   dall'armatore   al   massimo  comportano  una    &#13;
 contravvenzione depenalizzata";                                          &#13;
         che  nel merito la rimettente osserva che la dichiarazione di    &#13;
 ricusazione  non può essere accolta in quanto "la dedotta situazione    &#13;
 di incompatibilità, che ove sussistente si risolverebbe in una causa    &#13;
 di  ricusazione",  non è disciplinata da alcuna previsione di legge:    &#13;
 "non  dall'art. 34  c.p.p.  né  dall'art. 36  lett.  c) dello stesso    &#13;
 codice di rito che si riferisce a manifestazioni di giudizio espresse    &#13;
 fuori dall'esercizio delle funzioni giudiziarie";                        &#13;
         che il giudice a quo dubita della legittimità costituzionale    &#13;
 di tale mancata previsione in quanto, potendo uno stesso fatto essere    &#13;
 oggetto  di  separati  giudizi ai fini della responsabilità civile e    &#13;
 penale,  il  giudice  che  si  sia pronunciato in sede civile su tale    &#13;
 fatto,  e  sia poi chiamato a valutarlo nuovamente in sede penale, si    &#13;
 trova in una posizione che menoma la sua terzietà;                      &#13;
         che nella specie il pregiudizio sarebbe consistito nell'avere    &#13;
 il giudice ricusato sostanzialmente affermato nel procedimento civile    &#13;
 la  "innocenza  anche"  ai fini penali del soggetto che poi era stato    &#13;
 chiamato a giudicare in sede penale;                                     &#13;
         che  secondo la Corte rimettente la mancata previsione di una    &#13;
 causa  di incompatibilità riconducibile alla ipotesi in questione è    &#13;
 in  contrasto  con  l'art. 3,  della  Costituzione,  data  la "palese    &#13;
 violazione  del  principio  di  parità  di trattamento di situazione    &#13;
 analoghe";  con  l'art. 24, secondo comma, della Costituzione, stante    &#13;
 la  violazione  del  diritto  di  difesa "pregiudicato da convinzioni    &#13;
 precostituite  in  ordine  alla  stessa  materia del decidere"; e con    &#13;
 l'art. 101  della  Costituzione, "posto a presidio dell'imparzialità    &#13;
 della funzione giudicante";                                              &#13;
         che  nel  giudizio è intervenuto il Presidente del Consiglio    &#13;
 dei  Ministri,  rappresentato e difeso dall'avvocatura generale dello    &#13;
 Stato,   riportandosi   integralmente   al   contenuto  dell'atto  di    &#13;
 intervento  spiegato  nel  giudizio di costituzionalità promosso con    &#13;
 l'ordinanza  iscritta  al  n. 396  del  r.o. del 1999 (poi deciso con    &#13;
 sentenza n. 283 del 2000).                                               &#13;
     Considerato  che  la  Corte  di  appello  di Palermo dubita della    &#13;
 legittimità  costituzionale  dell'art. 34  del  codice  di procedura    &#13;
 penale,   nella   parte   in   cui   non   prevede   quale  causa  di    &#13;
 incompatibilità del giudice l'ipotesi in cui "l'illecito da valutare    &#13;
 penalmente sia stato oggetto di un precedente giudizio in sede civile    &#13;
 da parte del medesimo magistrato";                                       &#13;
         che con l'ordinanza n. 431 del 1999, successiva all'ordinanza    &#13;
 di    rimessione,   questa   Corte   ha   dichiarato   la   manifesta    &#13;
 inammissibilità di analoga questione di costituzionalità, rilevando    &#13;
 che  nell'ambito  dei rapporti tra gli istituti apprestati dal codice    &#13;
 di  rito a garanzia dell'imparzialità del giudice l'art. 34 del cod.    &#13;
 proc.  pen. fa  riferimento  alle  situazioni  in cui i termini della    &#13;
 relazione  di  incompatibilità intercorrono all'interno del medesimo    &#13;
 procedimento,  o  comunque  nell'ambito  di  una  vicenda processuale    &#13;
 sostanzialmente unitaria riguardante la medesima regiudicanda, mentre    &#13;
 i  casi  in  cui  la funzione pregiudicante è espressa in un diverso    &#13;
 procedimento rientrano nella sfera di applicazione della astensione e    &#13;
 della ricusazione;                                                       &#13;
         che  già  in  precedenza, con le sentenze nn. 306, 307 e 308    &#13;
 del 1997, questa Corte aveva rilevato che le situazioni pregiudicanti    &#13;
 descritte  dall'art. 34  c.p.p. sono  tipicamente  e  preventivamente    &#13;
 individuate  dal  legislatore  in  base  alla  presunzione  che  esse    &#13;
 determinino   l'incompatibilità  ad  esercitare  ulteriori  funzioni    &#13;
 giurisdizionali  nel  medesimo  procedimento,  mentre  il pregiudizio    &#13;
 determinato  da  valutazioni  sul merito della responsabilità penale    &#13;
 espresse   in  un  diverso  procedimento  deve  essere  accertato  in    &#13;
 concreto,  caso  per  caso,  facendo  ricorso  alla  disciplina degli    &#13;
 istituti dell'astensione e della ricusazione, eventualmente integrata    &#13;
 da un intervento della Corte stessa;                                     &#13;
         che  con  la  sentenza  n. 283  del  2000 è stata dichiarata    &#13;
 l'illegittimità  costituzionale  dell'art. 37 cod. proc. pen. "nella    &#13;
 parte  in  cui  non  prevede che possa essere ricusato dalle parti il    &#13;
 giudice   che,  chiamato  a  decidere  sulla  responsabilità  di  un    &#13;
 imputato, abbia espresso in altro procedimento, anche non penale, una    &#13;
 valutazione  di  merito sullo stesso fatto nei confronti del medesimo    &#13;
 soggetto";                                                               &#13;
         che,   a   seguito   della   mutata   sfera  di  applicazione    &#13;
 dell'art. 37,  comma  1, cod. proc. pen., occorre restituire gli atti    &#13;
 all'autorità  giudiziaria  rimettente perché valuti se la questione    &#13;
 sia tuttora rilevante.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Ordina  la  restituzione  degli  atti  alla  Corte  di appello di    &#13;
 Palermo.                                                                 &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 luglio 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 26 luglio 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
