<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>349</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:349</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/11/2001</data_decisione>
    <data_deposito>06/11/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Gustavo ZAGREBELSKY; Valerio ONIDA; Carlo &#13;
MEZZANOTTE; Fernanda CONTRI; Guido NEPPI MODONA; Piero Alberto &#13;
CAPOTOSTI; Annibale MARINI; Franco BILE; Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di legittimità costituzionale dell'art. 33 del decreto &#13;
del  Presidente  della  Repubblica 10 gennaio 1957, n. 3 (Testo unico &#13;
delle  disposizioni  concernenti  lo  statuto  degli impiegati civili &#13;
dello  Stato),  promossi con 3 ordinanze emesse il 28 giugno 2000 dal &#13;
Tribunale  amministrativo regionale della Puglia, sezione staccata di &#13;
Lecce,  rispettivamente  iscritte  ai nn. 133, 134 e 135 del registro &#13;
ordinanze 2001 e pubblicate nella Gazzetta Ufficiale della Repubblica &#13;
n. 9, 1ª serie speciale, dell'anno 2001. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella camera di consiglio del 26 settembre 2001 il giudice &#13;
relatore Massimo Vari. &#13;
    Ritenuto che, con tre distinte ordinanze (r.o. nn. 133, 134 e 135 &#13;
del  2001), tutte emesse il 28 giugno 2000 e analogamente motivate in &#13;
diritto,  il Tribunale amministrativo regionale della Puglia, sezione &#13;
staccata  di  Lecce,  ha  sollevato, in riferimento all'art. 36 della &#13;
Costituzione,  questione  di legittimità costituzionale dell'art. 33 &#13;
del  decreto  del  Presidente  della Repubblica 10 gennaio 1957, n. 3 &#13;
(Testo   unico   delle  disposizioni  concernenti  lo  statuto  degli &#13;
impiegati civili dello Stato); &#13;
        che  i  giudizi a quibus sono stati promossi da dipendenti di &#13;
ruolo  del comune di Galatina per ottenere, previo annullamento della &#13;
negativa  deliberazione  della  Giunta comunale, l'accertamento delle &#13;
mansioni   superiori   svolte   ininterrottamente   in   un   periodo &#13;
intercorrente  tra  il 1981 ed il 1998, nonché il riconoscimento dei &#13;
conseguenti benefici economici; &#13;
        che  il  rimettente,  premesso  che  i ricorrenti nei giudizi &#13;
principali hanno effettivamente espletato mansioni superiori a quelle &#13;
proprie  della  qualifica  di  inquadramento,  "sulla base di precise &#13;
disposizioni  del  responsabile  del  servizio"  e  "per  coprire una &#13;
carenza  di  posti  previsti  in  pianta  organica",  osserva che, di &#13;
recente, la giurisprudenza amministrativa, nel rimeditare l'indirizzo &#13;
più  favorevole  al  dipendente  pubblico, ha optato per la tesi che &#13;
esclude  la  retribuibilità  dello svolgimento di mansioni superiori &#13;
nel pubblico impiego; &#13;
        che,   le   ordinanze,  sul  presupposto  che  debba  trovare &#13;
applicazione  nelle  fattispecie  oggetto di cognizione l'art. 33 del &#13;
d.P.R.  n. 3  del  1957,  ritengono che i proposti ricorsi dovrebbero &#13;
essere  rigettati proprio alla luce del menzionato orientamento della &#13;
giurisprudenza amministrativa; &#13;
        che,  tuttavia,  il  giudice  a  quo  ritiene che "sussistano &#13;
fortissimi dubbi in ordine alla compatibilità della disposizione con &#13;
la  previsione  dell'art. 36 della Costituzione" e ciò in virtù dei &#13;
principi  affermati, più volte, dalla giurisprudenza costituzionale, &#13;
"nel senso opposto" rispetto al predetto indirizzo giurisprudenziale; &#13;
        che, dunque, secondo il rimettente, "il divieto di retribuire &#13;
le  mansioni  superiori  desumibile" dall'art. 33 del d.P.R. n. 3 del &#13;
1957  appare  "in  contrasto  con l'esigenza, desumibile dall'art. 36 &#13;
della  Costituzione, ad una retribuzione proporzionata alla quantità &#13;
e qualità del lavoro prestato", giacché la disposizione denunciata, &#13;
"a  differenza  di  altre  previsioni  normative  (ad  es. l'art. 29, &#13;
secondo  comma,  del  d.P.R.  20  dicembre  1979,  n. 761),  viene ad &#13;
integrare  una  deroga  assoluta  e  temporalmente  indeterminata" al &#13;
suddetto  principio costituzionale e costituisce, quindi, espressione &#13;
"non  di  un  ragionevole  bilanciamento  di  interessi, quanto della &#13;
volontà  assoluta  (... contraria alla previsione dell'art. 36 della &#13;
Costituzione)   di   escludere   la  retribuibilità  delle  mansioni &#13;
superiori"; &#13;
        che  in  tutti  i  giudizi  è  intervenuto il Presidente del &#13;
Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura &#13;
generale  dello  Stato,  il  quale,  anche con successive memorie, ha &#13;
chiesto che la questione sia dichiarata inammissibile o infondata. &#13;
    Considerato preliminarmente, che le ordinanze denunciano tutte la &#13;
stessa  disposizione, prospettandone il contrasto con l'art. 36 della &#13;
Costituzione in base ad identiche censure, sicché i relativi giudizi &#13;
vanno riuniti per essere decisi con un'unica pronuncia; &#13;
        che,  quanto al merito, va osservato che questa Corte è già &#13;
stata investita dello scrutinio di costituzionalità dell'art. 33 del &#13;
d.P.R.   10  gennaio  1957,  n. 3,  sotto  il  profilo  dell'asserita &#13;
violazione   dell'art. 36   della  Costituzione  e  sulla  scorta  di &#13;
doglianze aventi tenore analogo alle censure attualmente dedotte; &#13;
        che,   in  quella  occasione,  nel  dichiarare  la  manifesta &#13;
infondatezza della sollevata questione, si è avuto modo di affermare &#13;
che  il  ricordato  art. 33 si riferisce "alla situazione fisiologica &#13;
degli  uffici",  cioè  alla  normale situazione nella quale sussiste &#13;
coincidenza  tra  mansioni  svolte  dall'impiegato e la sua qualifica &#13;
funzionale,  "sicché  nel  caso eccezionale di adibizione temporanea &#13;
del  dipendente  a  mansioni  superiori,  corrispondenti  a  un posto &#13;
vacante,   non  si  può  argomentare  a  contrario  una  preclusione &#13;
all'adeguamento   del   trattamento   economico  secondo  i  principi &#13;
ripetutamente  enunciati da questa Corte in conformità agli artt. 36 &#13;
della  Costituzione  e  2126  cod.  civ." (così ordinanza n. 347 del &#13;
1996;   ma  anche,  sia  pure  in  occasione  dell'esame  di  diversa &#13;
fattispecie  concernente il personale sanitario, ordinanza n. 289 del &#13;
1996); &#13;
        che,  dovendosi  ribadire  il  già espresso orientamento, le &#13;
questioni vanno, pertanto, dichiarate manifestamente infondate. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    Dichiara   la   manifesta   infondatezza   delle   questioni   di &#13;
legittimità  costituzionale  dell'art. 33 del decreto del Presidente &#13;
della   Repubblica   10   gennaio   1957,  n. 3  (Testo  unico  delle &#13;
disposizioni  concernenti  lo  statuto  degli  impiegati civili dello &#13;
Stato), sollevate, in riferimento all'art. 36 della Costituzione, dal &#13;
Tribunale  amministrativo regionale della Puglia, sezione staccata di &#13;
Lecce, con le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 novembre 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                         Il redattore: Vari &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 6 novembre 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
