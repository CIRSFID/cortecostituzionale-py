<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>337</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:337</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>02/07/1992</data_decisione>
    <data_deposito>15/07/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, prof. &#13;
 Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale  del  combinato  disposto    &#13;
 degli  artt.  4,  primo  comma,  della  legge  8  agosto 1972, n. 464    &#13;
 (Modifiche ed integrazioni alla legge 5 novembre 1968,  n.  1115,  in    &#13;
 materia  di  integrazione  salariale  e  di  trattamento  speciale di    &#13;
 disoccupazione) e 8, secondo comma, della legge 5 novembre  1968,  n.    &#13;
 1115  (Estensione,  in  favore dei lavoratori, degli interventi della    &#13;
 Cassa integrazione guadagni, della gestione dell'assicurazione contro    &#13;
 la disoccupazione e della Cassa assegni familiari  e  provvidenze  in    &#13;
 favore  dei  lavoratori  anziani  licenziati), promosso con ordinanza    &#13;
 emessa il 21 marzo 1991 dal Pretore di Busto Arsizio nel procedimento    &#13;
 civile vertente tra Zoni Ester ed altri e l'I.N.P.S., iscritta al  n.    &#13;
 80  del registro ordinanze 1992 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 10, prima serie speciale, dell'anno 1992;            &#13;
    Visto l'atto  di  costituzione  dell'I.N.P.S.  nonché  l'atto  di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito nell'udienza pubblica del 16 giugno 1992 il Giudice relatore    &#13;
 Luigi Mengoni;                                                           &#13;
    Uditi  gli  avvocati Giacomo Giordano, Gian Carlo Perone e Tiziano    &#13;
 Treu per l'I.N.P.S. e l'Avvocato dello Stato Giorgio D'Amato  per  il    &#13;
 Presidente del Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un giudizio civile promosso contro l'I.N.P.S. da    &#13;
 Ester  Zoni  e  altri  lavoratori  beneficiari,  fino  al  1987,  del    &#13;
 trattamento speciale di disoccupazione a seguito di licenziamento per    &#13;
 cessazione dell'impresa datrice di  lavoro,  dichiarata  fallita  nel    &#13;
 1977,  il  Pretore di Busto Arsizio, con ordinanza del 21 marzo 1991,    &#13;
 pervenuta alla Corte costituzionale il 12 febbraio 1992, ha sollevato    &#13;
 questione di legittimità costituzionale del combinato disposto degli    &#13;
 artt. 8, secondo comma, della legge 5 novembre 1968, n.  1115,  e  4,    &#13;
 primo  comma,  della  legge 8 agosto 1972, n. 464, nella parte in cui    &#13;
 non prevede un sistema di adeguamento  del  trattamento  speciale  di    &#13;
 disoccupazione  ai  miglioramenti  contrattuali  della  categoria  di    &#13;
 appartenenza  o,  quanto  meno,  al  mutato  potere di acquisto della    &#13;
 moneta.                                                                  &#13;
    Secondo il giudice  remittente,  le  norme  impugnate,  in  quanto    &#13;
 assumono  come  base  fissa  di  calcolo  del trattamento speciale di    &#13;
 disoccupazione la retribuzione percepita dal  lavoratore  al  momento    &#13;
 della  cessazione  del  rapporto  di lavoro, violano gli artt. 3 e 36    &#13;
 Cost. per  la  disparità  di  trattamento  che  così  si  determina    &#13;
 rispetto   ai   lavoratori   ammessi   al   trattamento  della  Cassa    &#13;
 integrazione  guadagni,  commisurato  alla  retribuzione   che   loro    &#13;
 spetterebbe  se  fossero  in  servizio,  nonostante  che i due regimi    &#13;
 abbiano in comune, almeno per certi aspetti, presupposti e  funzione.    &#13;
 Invero   la   Cassa  integrazione  guadagni  si  è  progressivamente    &#13;
 allontanata  dall'originario  fine  istituzionale  per  divenire,  in    &#13;
 sostanza, una forma di tutela della disoccupazione.                      &#13;
    In ogni caso le norme denunciate sarebbero in contrasto con l'art.    &#13;
 38, secondo comma, Cost., perché non prevedono nemmeno un meccanismo    &#13;
 di   adeguamento   monetario   della   prestazione   calcolata  sulla    &#13;
 retribuzione spettante al momento della cessazione  del  rapporto  di    &#13;
 lavoro, risalente nella specie al 1977.                                  &#13;
    2.  - Nel giudizio davanti alla Corte si è costituito l'I.N.P.S.,    &#13;
 chiedendo che la questione sia dichiarata  inammissibile  e  comunque    &#13;
 non fondata.                                                             &#13;
    Ad  avviso dell'Istituto, l'ordinanza di rimessione sottovaluta le    &#13;
 essenziali   differenze   intercorrenti   tra   il   trattamento   di    &#13;
 integrazione  salariale  e il trattamento speciale di disoccupazione.    &#13;
 L'uno presuppone la continuazione del rapporto di lavoro, che  rimane    &#13;
 soltanto  sospeso, l'altro presuppone la cessazione del rapporto, con    &#13;
 conseguente stato di disoccupazione del lavoratore. Né si può  dire    &#13;
 che  il  legislatore  non  sia intervenuto per adeguare l'importo del    &#13;
 trattamento speciale di disoccupazione. Con legge 29  febbraio  1980,    &#13;
 n.  33,  l'importo  è  stato  elevato  da due terzi a quattro quinti    &#13;
 dell'ultima retribuzione, e inoltre si  è  stabilito  che  l'importo    &#13;
 massimo di lire 600.000 mensili è annualmente incrementato in misura    &#13;
 pari   all'ottanta   per   cento   dell'aumento   dell'indennità  di    &#13;
 contingenza maturato nell'anno precedente.                               &#13;
    Osserva infine l'I.N.P.S. che, se è vero che  il  trattamento  in    &#13;
 esame,  non  essendo indicizzato alla dinamica della retribuzione, è    &#13;
 soggetto  a  una  progressiva  diminuzione,  ciò  si  giustifica  in    &#13;
 rapporto al prolungarsi della durata del trattamento medesimo.           &#13;
    3.  -  È  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato   dall'Avvocatura   dello   Stato,   concludendo    per    &#13;
 l'inammissibilità o comunque per l'infondatezza della questione.        &#13;
    Secondo  l'interveniente,  la  disparità di disciplina denunziata    &#13;
 nell'ordinanza  di  rimessione  è  giustificata  dai  caratteri   di    &#13;
 specialità  e di transitorietà del trattamento di disoccupazione di    &#13;
 cui è causa.<diritto>Considerato in diritto</diritto>1. - Dal Pretore  di  Busto  Arsizio  è  sollevata  questione  di    &#13;
 legittimità  costituzionale  del  combinato  disposto degli artt. 8,    &#13;
 secondo comma, della legge 5 novembre  1968,  n.  1115,  e  4,  primo    &#13;
 comma,  della  legge  8  agosto 1972, n. 464 (entrambi abrogati dalla    &#13;
 legge 23 luglio 1991, n. 223, rispettivamente artt. 16,  comma  4,  e    &#13;
 22,  comma  9),  "nella  parte  in  cui  non  prevede  un  sistema di    &#13;
 adeguamento del trattamento  speciale  di  disoccupazione  al  mutato    &#13;
 potere di acquisto della moneta e ai miglioramenti contrattuali della    &#13;
 categoria di appartenenza".                                              &#13;
    Più  precisamente  le  norme  citate  sono  censurate  sotto  due    &#13;
 profili: a) in riferimento agli artt.  3  e  36  Cost.,  perché  non    &#13;
 prevedono  l'agganciamento dell'indennità speciale di disoccupazione    &#13;
 alla dinamica salariale dei contratti collettivi; b)  in  riferimento    &#13;
 all'art.  38  Cost.,  perché  non  prevedono almeno un meccanismo di    &#13;
 adeguamento dell'indennità ai mutamenti del potere di acquisto della    &#13;
 moneta.                                                                  &#13;
    2. - La questione non è fondata.                                     &#13;
    Sotto il  primo  profilo,  il  giudice  remittente  muove  da  una    &#13;
 premessa   erronea,   secondo   cui   il   trattamento   speciale  di    &#13;
 disoccupazione regolato dalle  norme  in  esame  avrebbe  gli  stessi    &#13;
 presupposti  e la medesima funzione del trattamento corrisposto dalla    &#13;
 Cassa integrazione guadagni.  Ma  che  entrambi  gli  istituti  siano    &#13;
 riconducibili   nel   quadro  degli  strumenti  di  lotta  contro  la    &#13;
 disoccupazione non è un argomento che possa superare  la  differenza    &#13;
 essenziale  di  presupposti  e  di  funzione  che  li  distingue.  Il    &#13;
 trattamento speciale di disoccupazione presuppone la  cessazione  del    &#13;
 rapporto  di  lavoro  a  seguito  di licenziamento del prestatore per    &#13;
 cessazione dell'attività produttiva in cui era  occupato,  e  ha  la    &#13;
 funzione  di conservare transitoriamente - nella misura di due terzi,    &#13;
 poi elevati a quattro quinti - la retribuzione percepita  nell'ultimo    &#13;
 mese  di  occupazione.  Il  trattamento  di  integrazione  salariale,    &#13;
 invece, presuppone la continuazione del rapporto di lavoro e  perciò    &#13;
 è  calcolato  sulla  retribuzione  in  corso,  avendo la funzione di    &#13;
 sostituire temporaneamente all'obbligazione retributiva del datore di    &#13;
 lavoro, che rimane sospesa, l'obbligazione previdenziale, commisurata    &#13;
 alla prima, dell'ente gestore della Cassa.                               &#13;
    Pertanto, l'istituto della Cassa integrazione  guadagni  non  può    &#13;
 fornire  un  termine utile di confronto ai fini dell'art. 3 Cost. Né    &#13;
 il trattamento speciale di disoccupazione può essere  valutato  alla    &#13;
 stregua  dell'art.  36  Cost., applicabile solo sul presupposto di un    &#13;
 rapporto di lavoro in atto.                                              &#13;
    3. - Sotto il secondo profilo va osservato anzitutto che,  secondo    &#13;
 la  giurisprudenza  di  questa  Corte,  l'art. 38 Cost. non esige che    &#13;
 l'adeguamento delle prestazioni previdenziali ai mutamenti del potere    &#13;
 di acquisto della moneta,  proceda  mediante  meccanismi  automatici.    &#13;
 Esso  può  avvenire  anche  con interventi legislativi periodici, la    &#13;
 scelta  dell'uno   o   dell'altro   metodo   essendo   rimessa   alla    &#13;
 discrezionalità   del   legislatore.   Nel   1980,   tre  anni  dopo    &#13;
 l'ammissione   dei   ricorrenti   al    trattamento    speciale    di    &#13;
 disoccupazione, è intervenuta la legge 29 febbraio, n. 33, che ne ha    &#13;
 elevato   l'importo   da  due  terzi  a  quattro  quinti  dell'ultima    &#13;
 retribuzione, e inoltre  ha  agganciato  l'importo  massimo  di  lire    &#13;
 600.000 agli incrementi annuali dell'indennità di contingenza.          &#13;
    È vero che successivamente non ci sono stati altri interventi, ma    &#13;
 occorre considerare, da un lato, che il legislatore deve tenere conto    &#13;
 anche  delle  disponibilità finanziarie della gestione assicurativa,    &#13;
 dall'altro,  che  i  ricorrenti,  grazie  a  una  serie  di  proroghe    &#13;
 consentite  dalla legge n. 464 del 1972, hanno fruito del trattamento    &#13;
 speciale di disoccupazione, ben superiore  all'indennità  ordinaria,    &#13;
 per  dieci anni (dal 1977 al 1987), anziché per la durata normale di    &#13;
 centottanta  giorni.  Il  prolungarsi  del  beneficio  nel  tempo  ne    &#13;
 determina e insieme giustifica la diminuzione di valore.                 &#13;
   Inoltre,  il  trattamento  speciale  di disoccupazione, nella parte    &#13;
 eccedente l'indennità ordinaria, si configurava come  un  intervento    &#13;
 assistenziale  a  carico dello Stato (arg. ex art. 37, comma 3, lett.    &#13;
 d, della legge 9 marzo  1989,  n.  88),  di  guisa  che  non  è  qui    &#13;
 invocabile il criterio dell'adeguatezza alle esigenze di vita proprio    &#13;
 delle prestazioni previdenziali.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 del combinato disposto degli artt. 8, secondo comma,  della  legge  5    &#13;
 novembre  1968,  n. 1115 (Estensione, in favore dei lavoratori, degli    &#13;
 interventi  della  Cassa  integrazione   guadagni,   della   gestione    &#13;
 dell'assicurazione  contro  la  disoccupazione  e della Cassa assegni    &#13;
 familiari e provvidenze in favore dei lavoratori anziani licenziati),    &#13;
 e 4, primo comma, della legge 8 agosto 1972,  n.  464  (Modifiche  ed    &#13;
 integrazioni  alla  legge  5  novembre  1968,  n. 1115, in materia di    &#13;
 integrazione salariale e di trattamento speciale di  disoccupazione),    &#13;
 sollevata,  in  riferimento agli artt. 3, 36 e 38 della Costituzione,    &#13;
 dal Pretore di Busto Arsizio con l'ordinanza indicata in epigrafe.       &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 2 luglio 1992.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 15 luglio 1992.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
