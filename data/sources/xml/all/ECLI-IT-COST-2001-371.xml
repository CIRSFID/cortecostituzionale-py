<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>371</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:371</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia>Fernanda Contri</redattore_pronuncia>
    <data_decisione>06/11/2001</data_decisione>
    <data_deposito>22/11/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, &#13;
Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel   giudizio   per   conflitto  di  attribuzione  sorto  a  seguito &#13;
dell'art. 1, comma 3, del d.P.R. 27 ottobre 1999, n. 458 (Regolamento &#13;
recante  norme di attuazione del regolamento [CE] n. 2815/98 relativo &#13;
alle  norme  commerciali  dell'olio  di  oliva), promosso con ricorso &#13;
della  Provincia  autonoma  di Trento, notificato il 4 febbraio 2000, &#13;
depositato  in  cancelleria  il 11 successivo ed iscritto al n. 6 del &#13;
registro conflitti 2000. &#13;
    Visto  l'atto  di  costituzione  del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito   nell'udienza  pubblica  del  10 luglio  2001  il  Giudice &#13;
relatore Fernanda Contri; &#13;
    Uditi l'avvocato Giandomenico Falcon per la Provincia autonoma di &#13;
Trento  e  l'avvocato dello Stato Oscar Fiumara per il Presidente del &#13;
Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con   ricorso   regolarmente  notificato  e  depositato,  la &#13;
Provincia  autonoma  di Trento ha sollevato conflitto di attribuzione &#13;
nei  confronti  dello  Stato, in relazione al d.P.R. 27 ottobre 1999, &#13;
n. 458  (Regolamento recante norme di attuazione del regolamento [CE] &#13;
n. 2815/98  relativo  alle  norme  commerciali  dell'olio  di oliva), &#13;
chiedendo a questa Corte di dichiarare che "non spetta allo Stato - e &#13;
per  esso  al  Ministero  delle  politiche  agricole e forestali - il &#13;
compito  di  provvedere ai controlli previsti dal regolamento CE 2815 &#13;
del  1998  e di attribuire allo stesso Ministero il potere di fissare &#13;
con  decreto  le  relative  modalità di attuazione". La Provincia ha &#13;
chiesto    inoltre    l'annullamento    dell'impugnato    regolamento &#13;
governativo, limitatamente all'art. 1, comma 3. &#13;
    La   ricorrente   lamenta  la  lesione  della  propria  sfera  di &#13;
attribuzioni costituzionali, come definite dagli artt. 8, numero 21), &#13;
9,  numero  3),  e 16 del d.P.R. 31 agosto 1972, n. 670 (Approvazione &#13;
del  testo  unico  delle  leggi costituzionali concernenti lo statuto &#13;
speciale  per  il  Trentino-Alto  Adige),  e dalle "relative norme di &#13;
attuazione";  dal decreto legislativo 16 marzo 1992, n. 266 (Norme di &#13;
attuazione   dello   statuto  speciale  per  il  Trentino-Alto  Adige &#13;
concernenti   il  rapporto  tra  atti  legislativi  statali  e  leggi &#13;
regionali  e  provinciali, nonché la potestà statale di indirizzo e &#13;
coordinamento);  "dai  principi e regole costituzionali in materia di &#13;
rapporti  tra regolamenti statali e potestà legislativa provinciale, &#13;
nonché di atti di indirizzo e coordinamento". &#13;
    La  Provincia  autonoma  di Trento rivendica anzitutto la propria &#13;
competenza   in   ordine   all'esecuzione   del   citato  regolamento &#13;
comunitario nell'ambito del proprio territorio, analogamente a quanto &#13;
previsto  per  le direttive comunitarie dalla legge n. 86 del 1989 ed &#13;
in base agli stessi principi affermati da questa Corte nella sentenza &#13;
n. 425  del  1999,  ritenendo  la  disciplina  della denominazione di &#13;
origine  dell'olio  di oliva riconducibile, per un verso, all'art. 8, &#13;
numero   21,  dello  statuto,  che  assegna  alle  Province  autonome &#13;
competenza  legislativa  primaria  in  materia di agricoltura; per un &#13;
altro  verso,  all'art. 9, numero 3, del medesimo, che attribuisce al &#13;
legislatore provinciale potestà concorrente in materia di commercio. &#13;
La   ricorrente   invoca   altresì   l'art. 16  dello  statuto,  che &#13;
attribuisce  alle  Province  autonome  potestà  amministrativa nelle &#13;
materie nelle quali queste possono emanare norme legislative. &#13;
    Nel ricorso viene poi specificamente censurato l'art. 1, comma 3, &#13;
dell'impugnato  regolamento  governativo,  il  quale  dispone che "ai &#13;
controlli  previsti dal regolamento (CE) n. 2815/98 della Commissione &#13;
del 2 dicembre 1998, provvede il Ministero delle politiche agricole e &#13;
forestali,  avvalendosi  anche dell'Agecontrol", aggiungendo che "con &#13;
decreto del Ministro delle politiche agricole e forestali, sentita la &#13;
Conferenza  permanente  per  i rapporti tra lo Stato, le Regioni e le &#13;
Province autonome di Trento e Bolzano, sono stabilite le modalità di &#13;
attuazione del presente comma". &#13;
    Si  tratta,  ad  avviso  della  Provincia,  di  funzioni che "per &#13;
definizione"  devono  svolgersi localmente: "l'art. 5 del regolamento &#13;
comunitario  n. 2815  ... prevede che il controllo sulle designazioni &#13;
di  origine  ... venga effettuato dagli Stati membri nelle imprese di &#13;
condizionamento  interessate ... dato che solo in questo modo si può &#13;
garantire  la corrispondenza ... tra le designazioni di origine degli &#13;
oli acquistati e quelle degli oli che escono dall'impresa". &#13;
    La  ricorrente  lamenta  inoltre  che l'attribuzione al Ministero &#13;
delle  politiche  agricole  e forestali delle funzioni di verifica di &#13;
cui  si  tratta  -  operata,  tra  l'altro, con fonte secondaria - si &#13;
porrebbe  in  contrasto  anche  con  l'art. 4,  comma  1, del decreto &#13;
legislativo  n. 266  del  1992,  a  norma del quale, nelle materie di &#13;
competenza  propria  delle  Province  autonome,  "la  legge  non può &#13;
attribuire  agli  organi  statali  funzioni  amministrative, comprese &#13;
quelle  di  vigilanza, di polizia amministrativa e di accertamento di &#13;
violazioni  amministrative,  diverse  da  quelle spettanti allo Stato &#13;
secondo lo statuto speciale e le relative norme di attuazione". &#13;
    Le   censure  sopra  riportate  si  estendono,  ad  avviso  della &#13;
Provincia  ricorrente,  anche  alla  previsione - ad opera del citato &#13;
comma  3  dell'art. 1  -  di  un  decreto  ministeriale  destinato  a &#13;
stabilire   le  modalità  di  attuazione  dell'impugnata  disciplina &#13;
regolamentare. &#13;
    2. - Nel    presente    giudizio,    rappresentato    e    difeso &#13;
dall'Avvocatura  generale dello Stato, si è costituito il Presidente &#13;
del Consiglio dei ministri, per chiedere il rigetto del ricorso. &#13;
    Deduce  innanzitutto  quest'ultimo che - affidando il regolamento &#13;
comunitario  agli Stati membri il controllo di cui si tratta "al fine &#13;
di accertare la corrispondenza tra le designazioni dell'origine degli &#13;
oli   di   oliva   vergini  usciti  dall'impresa  e  le  designazioni &#13;
dell'origine  dei  quantitativi  di  olio d'oliva vergini utilizzati" &#13;
(art. 5,  par.  1)  e  prevedendo che questi adottino tutte le misure &#13;
necessarie  e  in  particolare  istituiscano  un  regime  di sanzioni &#13;
pecuniarie  (art. 5,  par.  2)  -  "la  materia  in questione ... pur &#13;
attenendo  senza  dubbio  all'agricoltura, riguarda principalmente il &#13;
commercio  e in particolare la tutela e l'affidamento del consumatore &#13;
contro    possibili   frodi   ed   inganni   nella   preparazione   e &#13;
commercializzazione del prodotto". &#13;
    Per  questa  ragione,  aggiunge  il  resistente,  "il regolamento &#13;
comunitario  ammette  esplicitamente  una  designazione  dell'origine &#13;
nazionale,  coincidente con lo Stato membro (v. art. 2, par. 1, lett. &#13;
b, primo trattino)". &#13;
    La   difesa   erariale   deduce  inoltre  che  la  materia  della &#13;
prevenzione  e repressione delle frodi nel commercio è di competenza &#13;
statale,  come risulterebbe dagli artt. 2 e 3 del decreto legislativo &#13;
4 giugno  1997,  n. 143,  e  dall'art. 33,  comma  3, lettera b), del &#13;
decreto  legislativo 30 luglio 1999, n. 300, nonché dall'art. 10 del &#13;
decreto-legge  n. 282 del 1986, che ha istituito, per quanto riguarda &#13;
i  prodotti  alimentari,  l'Ispettorato  centrale  repressione frodi, &#13;
dipendente dal Ministero. &#13;
    La  depenalizzazione  di  cui  al decreto legislativo 30 dicembre &#13;
1999, n. 507, sopravvenuta al regolamento comunitario, ha mantenuto - &#13;
sottolinea  poi  il Presidente del Consiglio - la sanzione penale per &#13;
le  più  gravi  frodi alimentari, ed ha previsto un'aggravante per i &#13;
fatti  che "hanno per oggetto alimenti o bevande la cui denominazione &#13;
di  origine  o  geografica  o le cui specificità sono protette dalle &#13;
norme  vigenti" (art. 5). Ancora, si legge nell'atto di costituzione, &#13;
con  il  decreto legislativo 19 ottobre 1999, n. 426, si è stabilito &#13;
che,   salvo  che  il  fatto  costituisca  reato,  "chi  utilizza  la &#13;
designazione  di  origine prevista dal regolamento (CE) n. 2815/98 in &#13;
violazione  delle disposizioni da questo poste è punito con sanzioni &#13;
amministrative   pecuniarie   e  con  la  sospensione  o  revoca  del &#13;
riconoscimento". &#13;
    La  difesa  erariale  conclude  escludendo  la  violazione  delle &#13;
disposizioni statutarie invocate dalla ricorrente: essendo preminente &#13;
la  materia  commerciale,  assegnata  dallo  statuto  alla competenza &#13;
legislativa  concorrente  delle Province autonome, non può ritenersi &#13;
preclusa,  in  assenza  di  normativa  regionale,  l'adozione  di una &#13;
disciplina  statale  di attuazione comunitaria eventualmente anche di &#13;
dettaglio. &#13;
    Quanto  alla  possibilità  di utilizzare l'Agecontrol S.p.a., di &#13;
cui  alla legge 23 dicembre 1986, n. 898, il Presidente del Consiglio &#13;
sottolinea  che  "si tratta di una agenzia pubblica specializzata nei &#13;
controlli in materia di olio, istituita in base alla regolamentazione &#13;
comunitaria".  L'attività  di  tale  organismo,  prosegue  la difesa &#13;
erariale,  "a  seguito  della  entrata in vigore del regolamento (CE) &#13;
n. 150/99  del Consiglio del 19 gennaio 1999, è stata prorogata, con &#13;
l'art. 6, comma 1, del decreto legislativo 30 ottobre 1999 n. 419". &#13;
    3. - In prossimità dell'udienza, la Provincia autonoma di Trento &#13;
ha  depositato  una  memoria  illustrativa  per  ribadire  e svolgere &#13;
ulteriormente  le  deduzioni  già proposte in sede del ricorso e per &#13;
replicare   alle  difese  contenute  nell'atto  di  costituzione  del &#13;
Presidente del Consiglio dei Ministri. &#13;
    La  ricorrente afferma innanzi tutto essere "priva di conseguenze &#13;
giuridiche  sul  presente giudizio" la controversia circa l'afferenza &#13;
del regolamento contestato al commercio o all'agricoltura, investendo &#13;
la  normativa  in  esame  profili  propri delle due materie, entrambe &#13;
assegnate alla competenza legislativa e amministrativa delle province &#13;
autonome. &#13;
    Si legge a questo riguardo nella memoria: "se anche ci si volesse &#13;
riferire alla potestà concorrente in materia di commercio anziché a &#13;
quella  esclusiva  in  materia  di  agricoltura,  nulla  cambierebbe, &#13;
perché  anche  nelle materie di potestà concorrente la Provincia di &#13;
Trento  ha  un  obbligo  di adeguamento della propria legislazione ai &#13;
soli  principi  della  legislazione  statale, e ... comunque opera il &#13;
divieto  per lo Stato di svolgimento di poteri amministrativi locali, &#13;
posto dall'art. 4 del decreto legislativo n. 266 del 1992". &#13;
    La   Provincia   nega   poi  che  possa  applicarsi  ad  essa  la &#13;
disposizione  del decreto legislativo n. 143 del 1997, invocato dalla &#13;
difesa   erariale,  che  mantiene  alla  competenza  dello  Stato  la &#13;
"prevenzione  e  repressione  delle  frodi  nella  preparazione e nel &#13;
commercio dei prodotti agroalimentari", escludendo l'art. 1, comma 3, &#13;
del medesimo decreto legislativo dal proprio campo di applicazione le &#13;
regioni  a  statuto speciale e le province autonome, e stabilendo che &#13;
per  esse  "il trasferimento delle funzioni e dei compiti ... avviene &#13;
nel   rispetto   degli   statuti   e  attraverso  apposite  norme  di &#13;
attuazione".  Si  tratta  di una precisazione applicabile, secondo la &#13;
ricorrente,  anche  all'art. 33,  comma  3,  lettera  b), del decreto &#13;
legislativo  n. 300 del 1999, anch'esso richiamato dal Presidente del &#13;
Consiglio. &#13;
    Il riferimento obbligato è piuttosto, ad avviso della Provincia, &#13;
all'art. 8,  lettera  g),  delle norme di attuazione di cui al d.P.R. &#13;
n. 279  del  1974,  che  mantiene alla competenza statale soltanto le &#13;
funzioni  amministrative  in  ordine  alla  " repressione delle frodi &#13;
nella  preparazione  e  nel commercio di sostanze di uso agrario e di &#13;
prodotti agrari".<diritto>Considerato in diritto</diritto>1. -   La  Provincia autonoma di Trento ha sollevato conflitto di &#13;
attribuzione  nei  confronti  dello  Stato,  in  relazione  al d.P.R. &#13;
27 ottobre  1999, n. 458 (Regolamento recante norme di attuazione del &#13;
regolamento [CE] n. 2815/98 relativo alle norme commerciali dell'olio &#13;
di  oliva),  chiedendo  a  questa Corte di dichiarare che "non spetta &#13;
allo  Stato  -  e  per  esso  al Ministero delle politiche agricole e &#13;
forestali  -  il  compito  di  provvedere  ai  controlli previsti dal &#13;
regolamento CE 2815 del 1998 e di attribuire allo stesso Ministero il &#13;
potere  di  fissare con decreto le relative modalità di attuazione". &#13;
La   Provincia   ha  chiesto  inoltre  l'annullamento  dell'impugnato &#13;
regolamento governativo, limitatamente all'art. 1, comma 3. &#13;
    La  ricorrente  ritiene  lesa  la  propria  sfera di attribuzioni &#13;
costituzionalmente  garantite dal regolamento governativo emanato con &#13;
d.P.R.  27 ottobre 1999, n. 458, che all'art. 1, comma 3, attribuisce &#13;
al  Ministero  delle  politiche  agricole  e  forestali il compito di &#13;
provvedere  ai  controlli previsti dal regolamento CE 2815 del 1998 e &#13;
conferisce al Ministro delle politiche agricole e forestali il potere &#13;
di fissare con un suo decreto le relative modalità di attuazione. &#13;
    La  ricorrente impugna il d.P.R. n. 458 del 1999 in quanto lesivo &#13;
delle  proprie  attribuzioni,  come definite dall'art. 8, numero 21), &#13;
del d.P.R. 31 agosto 1972, n. 670 (Approvazione del testo unico delle &#13;
leggi   costituzionali   concernenti   lo  statuto  speciale  per  il &#13;
Trentino-Alto  Adige),  che assegna alle Province autonome competenza &#13;
legislativa  primaria  in materia di agricoltura; dall'art. 9, numero &#13;
3), del d.P.R. 31 agosto 1972, n. 670, che attribuisce al legislatore &#13;
provinciale   potestà   concorrente   in   materia   di   commercio; &#13;
dall'art. 16  del d.P.R. 31 agosto 1972, n. 670, che attribuisce alle &#13;
Province  autonome  potestà amministrativa nelle materie nelle quali &#13;
queste  possono  emanare  norme legislative; dalle "relative norme di &#13;
attuazione" dello statuto per il Trentino-Alto Adige; "dai principi e &#13;
regole  costituzionali in materia di rapporti tra regolamenti statali &#13;
e  potestà  legislativa  provinciale, nonché di atti di indirizzo e &#13;
coordinamento";  dal decreto legislativo 16 marzo 1992, n. 266 (Norme &#13;
di  attuazione  dello  statuto  speciale  per  il Trentino-Alto Adige &#13;
concernenti   il  rapporto  tra  atti  legislativi  statali  e  leggi &#13;
regionali  e  provinciali, nonché la potestà statale di indirizzo e &#13;
coordinamento). &#13;
    In  particolare,  l'attribuzione  al  Ministero  delle  politiche &#13;
agricole  e  forestali  delle funzioni di verifica di cui si tratta - &#13;
operata, lamenta la Provincia ricorrente, con fonte secondaria - e la &#13;
previsione,  con tale fonte, di un ulteriore regolamento ministeriale &#13;
per  fissare le modalità di attuazione dei controlli in questione si &#13;
porrebbero  in contrasto con l'art. 4, comma 1, dell'invocato decreto &#13;
legislativo  n. 266  del  1992,  a  norma del quale, nelle materie di &#13;
competenza  propria  delle Province autonome, la legge - e, a maggior &#13;
ragione,  una  fonte  secondaria  -  "non può attribuire agli organi &#13;
statali  funzioni  amministrative,  comprese  quelle di vigilanza, di &#13;
polizia    amministrativa    e    di   accertamento   di   violazioni &#13;
amministrative,  diverse  da  quelle  spettanti allo Stato secondo lo &#13;
statuto speciale e le relative norme di attuazione". &#13;
    2. - Il  ricorso  della  Provincia autonoma di Trento deve essere &#13;
accolto. &#13;
    2.1. - Nel presente caso, occorre premettere, viene in rilievo il &#13;
momento precedente l'esercizio di poteri sanzionatori, vale a dire la &#13;
fase   dei   controlli  e  della  prevenzione,  di  competenza  della &#13;
ricorrente   a   norma  del  citato  art. 4,  comma  1,  del  decreto &#13;
legislativo  n. 266  del  1992, oltre che a norma del d.P.R. 22 marzo &#13;
1974,  n. 279  (Norme  di  attuazione  dello  statuto speciale per la &#13;
Regione   Trentino-Alto   Adige   in  materia  di  minime  proprietà &#13;
colturali,  caccia e pesca, agricoltura e foreste), che mantiene allo &#13;
Stato  la  competenza  in  materia  di  repressione delle frodi nella &#13;
preparazione  e  nel  commercio  di prodotti agrari, ma non quella in &#13;
materia di vigilanza e prevenzione. &#13;
    La  menzionata  normativa  di  attuazione statutaria non è stata &#13;
superata  dalla  sopravvenuta  legislazione  statale richiamata dalla &#13;
difesa  erariale.  Non  dal decreto legislativo 4 giugno 1997, n. 143 &#13;
(Conferimento  alle  regioni delle funzioni amministrative in materia &#13;
di   agricoltura  e  pesca  e  riorganizzazione  dell'Amministrazione &#13;
centrale),  che  all'art. 2,  comma  3,  riserva  al Ministero per le &#13;
politiche  agricole  la  prevenzione  oltre  che la repressione delle &#13;
frodi nella preparazione e nel commercio dei prodotti agroalimentari, &#13;
non  applicabile  alle  regioni  a  statuto  speciale e alle province &#13;
autonome,  per  le  quali,  anzi,  l'art. 1,  comma 3 prevede che "il &#13;
trasferimento  delle  funzioni  e  dei  compiti e dei connessi beni e &#13;
risorse  avviene  nel  rispetto  degli  statuti e attraverso apposite &#13;
norme  di  attuazione".  Né  dal decreto legislativo 30 luglio 1999, &#13;
n. 300    (Riforma   dell'organizzazione   del   Governo,   a   norma &#13;
dell'articolo  11 della legge 15 marzo 1997, n. 59), che all'art. 33, &#13;
comma  3,  lettera  b),  include tra le attribuzioni del Ministero la &#13;
prevenzione  e  la  repressione delle frodi, giacché tale disciplina &#13;
non può spiegare effetti nei confronti della ricorrente. &#13;
    2.2.   -   Circa   l'attuazione  ed  esecuzione  dei  regolamenti &#13;
comunitari,   con   specifico   riguardo   alle   attribuzioni  della &#13;
ricorrente,  il  d.P.R.  19 novembre  1987,  n. 526  (Estensione alla &#13;
Regione  Trentino-Alto  Adige  ed  alle province autonome di Trento e &#13;
Bolzano   delle   disposizioni   del  decreto  del  Presidente  della &#13;
Repubblica  24 luglio 1977, n. 616), all'art. 6 stabilisce che spetta &#13;
alla  Regione  Trentino-Alto  Adige  ed  alle  Province  di  Trento e &#13;
Bolzano,   nelle   materie   di   rispettiva  competenza,  provvedere &#13;
all'attuazione  dei regolamenti comunitari "ove questi richiedano una &#13;
normazione integrativa o un'attività amministrativa di esecuzione". &#13;
    Sotto  quest'ultimo  profilo,  non  rileva  il  tenore  letterale &#13;
dell'art. 17,  comma 1, lettera a), della legge n. 400 del 1988, come &#13;
modificato   dall'art. 11   della   legge   5 febbraio   1999,  n. 25 &#13;
(Disposizioni   per   l'adempimento   di   obblighi  derivanti  dalla &#13;
appartenenza  dell'Italia  alla Comunità europea - legge comunitaria &#13;
1998),  al  quale non può ragionevolmente attribuirsi il significato &#13;
di  un  superamento della generale competenza regionale e provinciale &#13;
quanto  all'esecuzione  dei  regolamenti  comunitari nelle materie di &#13;
rispettiva competenza. &#13;
    La   competenza   regionale   e   delle   province  autonome  per &#13;
l'attuazione   e   l'esecuzione   dei   regolamenti  comunitari  "non &#13;
autosufficienti",  nelle  materie  assegnate alla loro competenza, è &#13;
stata  riconosciuta in più di una occasione da questa Corte, che non &#13;
ha  d'altro canto escluso il possibile ricorso, da parte dello Stato, &#13;
a  tutti  gli  strumenti  consentitigli, a seconda della natura della &#13;
competenza  regionale  o  provinciale,  per  far valere gli interessi &#13;
unitari  di  cui esso è portatore (sentenze n. 398 del 1998 e n. 126 &#13;
del 1996; n. 284 del 1989; n. 433 del 1987; n. 304 del 1987). &#13;
    L'art. 5  del  regolamento  comunitario n. 2815/98, peraltro, non &#13;
richiede,  per  la  sua esecuzione, l'adozione di norme legislative o &#13;
regolamentari,  ben  potendo  l'ente  territoriale  interessato  dare &#13;
attuazione   alla   normativa  comunitaria  in  questione  attraverso &#13;
"un'attività amministrativa di esecuzione". &#13;
    Dal  canto  suo,  la  Provincia  ha  nel  frattempo  adottato, in &#13;
esecuzione   del   regolamento  comunitario  di  cui  si  tratta,  la &#13;
deliberazione   della   Giunta   provinciale   26 maggio  2000  sulle &#13;
"Modalità  di  applicazione  del  Regolamento  CE  n. 2815/98  della &#13;
Commissione  del  22 dicembre  1998  relativo  alle norme commerciali &#13;
dell'olio di oliva". &#13;
    2.3.  -  Il  conflitto  che  la Corte è chiamata a risolvere non &#13;
concerne  una  ipotesi  di  preventiva  sostituzione dello Stato alla &#13;
Provincia autonoma inadempiente rispetto agli obblighi comunitari. &#13;
    Il  regolamento  impugnato  non  si  presenta  infatti come norma &#13;
cedevole  o  suppletiva,  adottata  in  via  preventiva,  destinata a &#13;
lasciare  il  campo alla successiva, eventuale, normativa provinciale &#13;
di esecuzione del regolamento comunitario. &#13;
    L'art. 1,  comma 3, del regolamento impugnato assegna stabilmente &#13;
la  competenza  relativa  "ai controlli previsti dal regolamento (CE) &#13;
n. 2815/98 della Commissione del 22 dicembre 1998" al Ministero delle &#13;
politiche  agricole  e  forestali,  ed  aggiunge che "con decreto del &#13;
Ministro  delle politiche agricole e forestali, sentita la Conferenza &#13;
permanente  per  i  rapporti  tra  lo Stato, le regioni e le province &#13;
autonome  di  Trento  e  di  Bolzano,  sono stabilite le modalità di &#13;
attuazione del presente comma". &#13;
    Si   tratta,   in  altri  termini,  di  una  stabile  alterazione &#13;
dell'assetto  delle  competenze  delineato  dallo  statuto speciale e &#13;
dalle norme di attuazione statutaria, inconciliabile, in particolare, &#13;
con  il  citato  art. 4,  comma 1, del decreto legislativo n. 266 del &#13;
1992,  a  norma  del quale, nelle materie di competenza propria delle &#13;
Province  autonome,  la  legge - e, a fortiori una fonte secondaria - &#13;
"non  può  attribuire  agli  organi statali funzioni amministrative, &#13;
comprese   quelle  di  vigilanza,  di  polizia  amministrativa  e  di &#13;
accertamento   di   violazioni   amministrative,  diverse  da  quelle &#13;
spettanti  allo Stato secondo lo statuto speciale e le relative norme &#13;
di attuazione". &#13;
    Tanto  più  che in questo caso lo Stato è intervenuto con norma &#13;
secondaria  attributiva,  a  sua  volta,  di  poteri  ministeriali in &#13;
materia  assegnata  alla  competenza provinciale, in contrasto con la &#13;
costante  giurisprudenza  di  questa  Corte,  la quale esclude che un &#13;
regolamento  governativo o ministeriale possa legittimamente limitare &#13;
o interferire con l'esercizio di competenze attribuite alle regioni o &#13;
alle  province  autonome  (ex plurimis v. le sentenze n. 84 del 2001; &#13;
n. 209 del 2000; n. 420 del 1999; n. 352 del 1998; n. 250 del 1996). &#13;
    Per  le ragioni su esposte il provvedimento impugnato si appalesa &#13;
lesivo delle attribuzioni costituzionali della ricorrente. &#13;
    Il  ricorso  della  Provincia  autonoma  di  Trento deve pertanto &#13;
essere  accolto  e  l'impugnato regolamento governativo, adottato con &#13;
d.P.R. 27 ottobre 1999, n. 458, deve essere annullato, nella parte in &#13;
cui si applica alla ricorrente. &#13;
    Rimane assorbita ogni ulteriore censura. &#13;
    2.4. - In  considerazione  della  piena  equiparazione statutaria &#13;
delle due province autonome relativamente alle attribuzioni di cui si &#13;
tratta,  la  presente sentenza deve produrre i suoi effetti anche nei &#13;
confronti della Provincia autonoma di Bolzano.</testo>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  che non spetta allo Stato adottare, nei confronti delle &#13;
Province  autonome  di  Trento  e  di Bolzano, l'art. 1, comma 3, del &#13;
d.P.R.   27 ottobre   1999,  n. 458  (Regolamento  recante  norme  di &#13;
attuazione  del  regolamento  [CE]  n. 2815/98  relativo  alle  norme &#13;
commerciali  dell'olio di oliva) e conseguentemente annulla l'art. 1, &#13;
comma  3,  del medesimo d.P.R. n. 458 del 1999, nella parte in cui si &#13;
applica alle Province autonome di Trento e di Bolzano. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 6 novembre 2001. &#13;
                       Il Presidente: Ruperto &#13;
                        Il redattore: Contri &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 22 novembre 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
