<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>254</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:254</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/05/1991</data_decisione>
    <data_deposito>06/06/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale degli artt. 160, n. 1    &#13;
 (recte primo comma), 28, n. 1 (recte secondo comma) e 431 del  codice    &#13;
 di procedura penale, promosso con ordinanza emessa il 5 dicembre 1990    &#13;
 dal Giudice per le indagini preliminari presso il Tribunale di Ancona    &#13;
 nel  procedimento penale a carico di Carmelo Reale iscritta al n. 160    &#13;
 del registro ordinanze 1991 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 11, prima serie speciale,                            &#13;
 dell'anno 1991;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  dell'8  maggio  1991  il  Giudice    &#13;
 relatore Enzo Cheli;                                                     &#13;
    Ritenuto che, nel corso del procedimento penale contro Carmelo Reale,  imputato  di  peculato,  il  Tribunale di Ancona, con ordinanza    &#13;
 emanata nel corso dell'udienza dibattimentale del 28  novembre  1990,    &#13;
 pur  rilevando l'irreperibilità dell'imputato dichiarata dal giudice    &#13;
 per le  indagini  preliminari  al  fine  della  notifica  degli  atti    &#13;
 introduttivi  dell'udienza  preliminare,  dichiarava  la nullità del    &#13;
 decreto di  citazione  per  omessa  notifica  all'imputato  ai  sensi    &#13;
 dell'art. 179, primo comma, del codice di procedura penale;              &#13;
      che il giudice per le indagini preliminari, al quale erano stati    &#13;
 restituiti  gli atti, con ordinanza del 5 dicembre 1990, ha sollevato    &#13;
 questione di legittimità costituzionale:                                &#13;
        a) dell'art. 160, primo comma, del codice di procedura penale,    &#13;
 che, prevedendo che il decreto di irreperibilità emesso dal  giudice    &#13;
 per  le  indagini  preliminari  sia  efficace  fino  al provvedimento    &#13;
 conclusivo dell'udienza preliminare, contrasterebbe con gli artt.  2,    &#13;
 3  e  97  della  Costituzione,  poiché, non individuando chiaramente    &#13;
 l'organo giudiziario che deve rinnovare il decreto di irreperibilità    &#13;
 e non disponendo che il decreto di irreperibilità  emanato  al  fine    &#13;
 della  notifica  degli  atti  introduttivi  dell'udienza  preliminare    &#13;
 conservi efficacia  fino  al  provvedimento  di  rinvio  a  giudizio,    &#13;
 sarebbe    causa    di    disfunzioni    e   incertezze   applicative    &#13;
 nell'amministrazione della giustizia;                                    &#13;
        b) dell'art.  28,  secondo  comma,  del  codice  di  procedura    &#13;
 penale,  dove  risulta stabilito che, nei casi di conflitto, "qualora    &#13;
 il contrasto sia tra giudice dell'udienza preliminare e  giudice  del    &#13;
 dibattimento  prevale  la  decisione di quest'ultimo", per violazione    &#13;
 degli artt. 101, secondo comma, 2, 3 e 97 della Costituzione, poiché    &#13;
 la  sua  applicazione  costringerebbe  il  giudice  per  le  indagini    &#13;
 preliminari  a  porre in essere un'attività processuale non prevista    &#13;
 da alcuna disposizione  di  legge  in  virtù  di  un  provvedimento,    &#13;
 ritenuto  erroneo, di altra autorità giudiziaria, in ordine al quale    &#13;
 il giudice per le indagini preliminari  non  ha  alcuna  facoltà  di    &#13;
 controdeduzione;                                                         &#13;
        c)  dell'art.  431  del codice di procedura penale, perché in    &#13;
 contrasto con gli artt. 2, 3 e 97 della Costituzione, in quanto,  non    &#13;
 prevedendo  che  nel fascicolo del dibattimento sia compreso anche il    &#13;
 verbale  dell'udienza  preliminare  che  attesti  la   presenza   del    &#13;
 difensore,  destinatario delle notifiche all'imputato irreperibile ex    &#13;
 art. 159 del codice  di  procedura  penale,  nonché  il  decreto  di    &#13;
 fissazione  dell'udienza preliminare e il decreto di irreperibilità,    &#13;
 obbligherebbe il giudice per le indagini preliminari ad  un'attività    &#13;
 ultronea   consistente   nell'inserimento  degli  atti  suddetti  nel    &#13;
 fascicolo del giudice del dibattimento;                                  &#13;
      che  l'Avvocatura   Generale   dello   Stato,   intervenuta   in    &#13;
 rappresentanza del Presidente del Consiglio dei ministri, ha concluso    &#13;
 chiedendo  che  la questione concernente l'art. 160, primo comma, del    &#13;
 codice di procedura penale, sia dichiarata inammissibile, ovvero  che    &#13;
 gli  atti  siano restituiti al giudice a quo per nuovo esame in punto    &#13;
 di rilevanza, e la dichiarazione di infondatezza  e  inammissibilità    &#13;
 delle altre questioni sollevate.                                         &#13;
    Considerato  che l'art. 4 del decreto legislativo 14 gennaio 1991,    &#13;
 n. 12, entrato in vigore successivamente all'ordinanza di  rimessione    &#13;
 del  presente  giudizio,  ha  modificato  l'art.  160  del  codice di    &#13;
 procedura penale, prevedendo che il decreto di irreperibilità emesso    &#13;
 dal giudice ai  fini  della  notificazione  degli  atti  introduttivi    &#13;
 dell'udienza preliminare conservi efficacia fino alla pronuncia della    &#13;
 sentenza di primo grado;                                                 &#13;
      che,  pertanto,  gli  atti  relativi  alla  questione  sollevata    &#13;
 concernente l'art. 160, primo comma, del codice di  procedura  penale    &#13;
 devono  essere  restituiti  al  giudice  remittente perché valuti il    &#13;
 permanere della rilevanza della stessa questione alla  stregua  della    &#13;
 legge sopravvenuta;                                                      &#13;
      che  questa  Corte,  con  ordinanza  n.  241  del  1991, ha già    &#13;
 dichiarato manifestamente infondata,  in  riferimento  all'art.  101,    &#13;
 secondo  comma,  della  Costituzione,  la  questione  di legittimità    &#13;
 costituzionale dell'art. 28, secondo comma, del codice  di  procedura    &#13;
 penale,  nella  parte  in  cui  prevede  che in caso di contrasto tra    &#13;
 giudice dell'udienza preliminare e giudice del dibattimento  prevalga    &#13;
 la decisione di quest'ultimo;                                            &#13;
      che,   in   riferimento   all'art.  101,  secondo  comma,  della    &#13;
 Costituzione, non adducendo l'ordinanza di rimessione argomenti nuovi    &#13;
 o diversi da quelli allora esaminati, la questione qui proposta  deve    &#13;
 essere dichiarata manifestamente infondata;                              &#13;
      che   nell'ordinanza   di  rimessione  non  si  rinviene  alcuna    &#13;
 motivazione in ordine alla pretesa violazione da parte dell'art.  28,    &#13;
 secondo  comma,  del codice di procedura penale degli artt. 2, 3 e 97    &#13;
 della Costituzione e che, pertanto, la questione relativa deve essere    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
      che il disposto dell'art. 431 del codice di procedura penale non    &#13;
 trova applicazione nel giudizio a quo, poiché al giudice  remittente    &#13;
 sono  stati  restituiti  gli  atti,  a seguito della dichiarazione di    &#13;
 nullità del decreto  di  citazione,  esclusivamente  ai  fini  della    &#13;
 rinnovazione  del  decreto  medesimo  e  che,  pertanto, la questione    &#13;
 relativa a tale disposizione per violazione degli artt.  2,  3  e  97    &#13;
 della    Costituzione    deve    essere   dichiarata   manifestamente    &#13;
 inammissibile.                                                           &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  28, secondo comma, del codice di procedura    &#13;
 penale, sollevata, in riferimento all'art.  101  della  Costituzione,    &#13;
 dal giudice per le indagini preliminari presso il Tribunale di Ancona    &#13;
 con l'ordinanza in epigrafe;                                             &#13;
    Dichiara   la   manifesta   inammissibilità  della  questione  di    &#13;
 legittimità costituzionale dell'art. 28, secondo comma,  del  codice    &#13;
 di  procedura  penale, sollevata, in riferimento agli artt. 2, 3 e 97    &#13;
 della Costituzione, dallo stesso giudice con l'ordinanza in epigrafe;    &#13;
    Dichiara  la  manifesta  inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  dell'art.  431  del codice di procedura    &#13;
 penale, sollevata,  in  riferimento  agli  artt.  2,  3  e  97  della    &#13;
 Costituzione, dallo stesso giudice con l'ordinanza in epigrafe;          &#13;
    Dispone  la  restituzione  al  giudice per le indagini preliminari    &#13;
 presso il Tribunale di Ancona degli atti relativi alla  questione  di    &#13;
 costituzionalità dell'art. 160, primo comma, del codice di procedura    &#13;
 penale,  sollevata  in  riferimento  agli  artt.  2,  3  e  97  della    &#13;
 Costituzione.                                                            &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  Costituzionale,    &#13;
 Palazzo della Consulta il 22 maggio 1991.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: CHELI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 6 giugno 1991.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
