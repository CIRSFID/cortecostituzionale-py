<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>192</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:192</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>03/05/2006</data_decisione>
    <data_deposito>11/05/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 19, comma 2, lettera d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promosso con ordinanza dell'8 giugno 2005 dal Giudice di pace di Genova, sul ricorso proposto da B. L. contro il Prefetto di Genova, iscritta al n. 527 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 44, prima serie speciale, dell'anno 2005. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 22 febbraio 2006 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che il Giudice di pace di Genova, con ordinanza depositata l'8 giugno 2005, ha sollevato, in riferimento agli artt. 2, 30 e 32 della Costituzione, questione di legittimità costituzionale dell'art. 19, comma 2, lettera d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui prevede che il decreto di espulsione debba essere eseguito anche nei confronti dello straniero extracomunitario legato da una relazione affettiva con una cittadina italiana, in stato di gravidanza, impedendo così a costui di assicurare alla donna stessa e al nascituro assistenza materiale e morale; &#13;
    che il rimettente solleva la questione di legittimità, relativamente alla tutela della cittadina italiana, in riferimento agli artt. 2 e 32 Cost. e, relativamente alla tutela del nascituro, in riferimento agli artt. 2 e 30 Cost.; &#13;
    che il giudizio a quo ha ad oggetto l'opposizione al decreto di espulsione emesso dal Prefetto di Genova nei confronti di B. L., in quanto questi, entrato nel territorio dello Stato sottraendosi ai controlli di frontiera, non aveva richiesto il permesso di soggiorno entro i termini di legge; &#13;
    che il rimettente, illustrati brevemente i suoi poteri in materia di opposizione al decreto di espulsione, in punto di fatto rileva che il B. L. risulta legato affettivamente ad M. V., cittadina italiana, come da dichiarazione dalla stessa sottoscritta, con la quale ha concepito un nascituro; &#13;
    che, nel corso del giudizio a quo è emerso che costei era stata ricoverata, in data 14 maggio 2005, presso un Pronto soccorso ospedaliero ove le era stato rilasciato un certificato medico con una prognosi di tre giorni; &#13;
    che, a parere del giudice a quo, l'art. 16 della Dichiarazione universale dei diritti dell'uomo e l'art. 8 della Convenzione europea per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali, resa esecutiva con la legge 4 agosto 1955, n. 848 (Ratifica ed esecuzione della Convenzione per la salvaguardia dei diritti dell'uomo e delle libertà fondamentali firmata a Roma il 4 novembre 1950 e del Protocollo addizionale alla Convenzione stessa, firmato a Parigi il 20 marzo 1952), nel prestare tutela alla famiglia, prescindono dal fatto che la stessa sia fondata o meno sul vincolo del matrimonio; &#13;
    che, ad avviso del rimettente, il decreto di espulsione impugnato costituisce un grave nocumento per la madre del nascituro, in violazione dell'art. 2 della Costituzione, nonché dell'art. 32 della Costituzione, in quanto limita o esclude il dovere inderogabile di solidarietà connesso alla tutela del diritto alla salute; &#13;
    che il decreto di espulsione oggetto di giudizio, a parere del giudice a quo, vanifica, altresì, i diritti del «nascituro-figlio naturale» ed «impedisce l'adempimento dei doveri del genitore-naturale» destinatario «del provvedimento amministrativo opposto», nonostante la tutela che il legislatore e l'art. 30 della Costituzione accordano al figlio naturale; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la declaratoria di inammissibilità o comunque di manifesta infondatezza della questione; &#13;
    che, ad avviso della parte pubblica, la questione di legittimità risulterebbe, innanzitutto, irrilevante ponendosi al di fuori della fattispecie procedimentale che il rimettente è chiamato a decidere; &#13;
    che l'esecuzione del provvedimento di espulsione, a parere della difesa erariale, non pregiudica il diritto-dovere del ricorrente di adempiere ai suoi doveri di padre del nascituro una volta che quest'ultimo, venuto al mondo, venga dallo stesso riconosciuto; &#13;
    che, secondo l'Avvocatura, l'ordinamento non prevede situazioni soggettive tutelabili in capo al presunto padre, prima della nascita del concepito, «fino al punto di sottrarlo al trattamento sanzionatorio derivante dalla violazione di leggi»; &#13;
    che, a parere della difesa erariale, in favore del ricorrente, in quanto convivente e non unito in matrimonio con la madre del nascituro, non si può invocare l'illegittimità costituzionale dell'art. 19, comma 2, lettera d), del d.lgs. n. 286 del 1998 sulla base della sentenza n. 376 del 2000, con la quale questa Corte, al fine di assicurare un'adeguata tutela alla famiglia, ha dichiarato illegittima la norma citata nella parte in cui non prevedeva il divieto di espulsione nei confronti del marito convivente con donna in stato di gravidanza o nei sei mesi successivi alla nascita del figlio; &#13;
    che, comunque, i valori dell'unità familiare e dell'interesse dei figli minori devono essere bilanciati con altri valori aventi uguale dignità costituzionale, quali quelli garantiti e protetti dalla normativa in materia di ingresso e soggiorno degli stranieri nel territorio nazionale; &#13;
    che, infine, la parte pubblica ritiene non applicabile al caso sottoposto al rimettente la legge 8 marzo 2000, n. 52 (recte: n. 53) (Disposizioni per il sostegno della maternità e della paternità, per il diritto alla cura e alla formazione e per il coordinamento dei tempi delle città), che ha introdotto una disciplina a tutela della funzione genitoriale a prescindere dall'esistenza del rapporto di coniugio, attesa la presenza irregolare nel territorio dello Stato del ricorrente; &#13;
    Considerato che il Giudice di pace di Genova, ha sollevato, in riferimento agli artt. 2, 30 e 32 della Costituzione, questione di legittimità costituzionale dell'art. 19, comma 2, lettera d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui prevede che il decreto di espulsione debba essere eseguito anche nei confronti dello straniero extracomunitario legato da una relazione affettiva con una cittadina italiana, in stato di gravidanza, impedendo così a costui di assicurare alla donna stessa e al nascituro assistenza materiale e morale; &#13;
    che, premesso che l'art. 19, comma 2, lettera d), del decreto legislativo 25 luglio 1998, n. 286, prevede non già un divieto assoluto, ma una temporanea sospensione del potere di espulsione (o di respingimento) «delle donne in stato di gravidanza o nei sei mesi successivi alla nascita del figlio cui provvedono», l'estensione di tale disciplina (operata dalla sentenza n. 376 del 2000) al rispettivo marito convivente presuppone una certezza dei rapporti familiari che non è dato riscontrare - e tanto meno è dato verificare nel giudizio a quo - nel caso di una relazione di fatto che, come tale, non può che essere affermata dagli interessati; &#13;
    che, conseguentemente, la questione di legittimità costituzionale, sebbene prospettata in termini di tutela della famiglia di fatto e dei conseguenti diritti-doveri, pone in realtà in comparazione trattamenti riservati a situazioni profondamente diverse – e cioè quella del marito di cittadina extracomunitaria incinta e quella dell'extracomunitario che afferma di essere padre naturale di un nascituro – e, quindi, non irragionevolmente disciplinate in modo diverso dal legislatore; &#13;
    che,  quanto  alla   censura  relativa  alla    asserita    violazione  del  dovere inderogabile  di  solidarietà,    collegata  al  diritto  alla   salute   tutelato  &#13;
dall'art. 32 della Costituzione, le ragioni della solidarietà umana non sono di per sé in contrasto con le regole in materia di immigrazione previste in funzione di un ordinato flusso migratorio e di un'adeguata accoglienza ed integrazione degli stranieri; &#13;
    che, pertanto, la questione è manifestamente infondata. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 19, comma 2, lettera d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), sollevata, in riferimento agli artt. 2, 30 e 32 della Costituzione, dal Giudice di pace di Genova con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta,  il 3 maggio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria l'11 maggio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
