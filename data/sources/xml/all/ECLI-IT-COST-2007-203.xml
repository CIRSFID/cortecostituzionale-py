<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>203</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:203</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>06/06/2007</data_decisione>
    <data_deposito>18/06/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale in materia di inappellabilità delle sentenze di proscioglimento), che ha sostituito l'art. 593 del codice di procedura penale, promosso con ordinanza del 16 marzo 2006 dalla Corte d'appello di Roma nel procedimento penale a carico di C. V. D., iscritta al n. 161 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica, n. 22, prima serie speciale, dell'anno 2006. &#13;
    Udito nella camera di consiglio del 23 maggio 2007 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che la Corte d'appello di Roma ha sollevato, in riferimento agli artt. 3, 24, 111 e 112 della Costituzione, questione di legittimità costituzionale dell'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), che ha modificato, sostituendolo, l'art. 593 del codice di procedura penale, nella parte in cui non consente al pubblico ministero di proporre appello avverso le sentenze di proscioglimento, se non nel caso previsto dall'art. 603, comma 2, del codice di procedura penale – ossia quando sopravvengano o si scoprano nuove prove dopo il giudizio di primo grado – e sempre che tali prove risultino decisive; &#13;
    che la Corte rimettente è chiamata a celebrare il giudizio d'appello a seguito dell'impugnazione proposta dal pubblico ministero avverso una sentenza di assoluzione pronunciata in primo grado, come risulta dalla memoria del pubblico ministero allegata all'ordinanza di rimessione; &#13;
    che, ad avviso del giudice a quo, la disposizione censurata viola diversi precetti costituzionali; &#13;
    che essa risulterebbe lesiva, anzitutto, del principio di eguaglianza, sancito dall'art. 3 Cost.: consentire, infatti, all'imputato di proporre appello nei confronti delle sentenze di condanna senza concedere al pubblico ministero lo speculare potere di appellare contro «le sentenze di assoluzione», se non in un caso estremamente circoscritto, significherebbe porre l'imputato in «una posizione di evidente favore nei confronti degli altri componenti la collettività», i quali vedrebbero fortemente limitato, in tal modo, il diritto-dovere del pubblico ministero di esercitare l'azione penale, che tutela i loro interessi;  &#13;
    che la norma censurata si porrebbe, altresì, in contrasto con l'art. 24 Cost., non consentendo alla «collettività», i cui interessi sono rappresentati e difesi dal pubblico ministero, «di tutelare adeguatamente i suoi diritti»; &#13;
    che risulterebbe violato, ancora, l'art. 111 Cost., nella parte in cui impone che ogni processo si svolga «nel contraddittorio tra le parti, in condizioni di parità davanti ad un giudice terzo e imparziale», posto che la disposizione denunciata non permetterebbe all'accusa di far valere le sue ragioni con modalità e poteri simmetrici a quelli di cui dispone la difesa; &#13;
    che, da ultimo, detta disposizione lederebbe l'art. 112 Cost.: la previsione di un secondo grado di giudizio di merito – fruibile tanto dal pubblico ministero che dall'imputato – sarebbe infatti «consustanziale» al sistema processuale vigente, con la conseguenza che la sottrazione all'organo dell'accusa del potere di proporre appello avverso le sentenze assolutorie eluderebbe i vincoli posti dal principio dell'obbligatorietà dell'azione penale, «considerata nella sua interezza». &#13;
    Considerato che il dubbio di costituzionalità sottoposto a questa Corte ha per oggetto la preclusione, conseguente alla modifica dell'art. 593 del codice di procedura penale ad opera dell'art. 1 della legge 20 febbraio 2006, n. 46, dell'appello delle sentenze dibattimentali di proscioglimento da parte del pubblico ministero; &#13;
    che, successivamente all'ordinanza di rimessione, questa Corte, con sentenza n. 26 del 2007, ha dichiarato l'illegittimità costituzionale dell'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui, sostituendo l'art. 593 del codice di procedura penale, esclude che il pubblico ministero possa appellare contro le sentenze di proscioglimento, fatta eccezione per le ipotesi previste dall'art. 603, comma 2, del medesimo codice, se la nuova prova è decisiva», e dell'art. 10, comma 2, della citata legge n. 46 del 2006, «nella parte in cui prevede che l'appello proposto contro una sentenza di proscioglimento dal pubblico ministero prima della data di entrata in vigore della medesima legge è dichiarato inammissibile»; &#13;
    che, alla stregua della richiamata pronuncia di questa Corte, gli atti devono essere pertanto restituiti al giudice rimettente per un nuovo esame della rilevanza della questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi  &#13;
LA CORTE COSTITUZIONALE  &#13;
    ordina la restituzione degli atti alla Corte d'appello di Roma. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 giugno 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 giugno 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
