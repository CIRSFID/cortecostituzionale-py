<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>169</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:169</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/03/1990</data_decisione>
    <data_deposito>04/04/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, comma 4, del    &#13;
 decreto legge 2 marzo 1989, n. 66 (Disposizioni urgenti in materia di    &#13;
 autonomia   impositiva  degli  enti  locali  e  di  finanza  locale),    &#13;
 convertito in legge 24 aprile 1989, n. 144,  promosso  con  ordinanza    &#13;
 emessa  il  20  luglio  1989  dal  Giudice  conciliatore  di Pisa nel    &#13;
 procedimento civile vertente tra  Mangano  Enrico  e  Viviani  Alfio,    &#13;
 iscritta  al  n.  528  del registro ordinanze 1989 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  46,  prima  serie  speciale,    &#13;
 dell'anno 1989.                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 31 gennaio 1990 il Giudice    &#13;
 relatore Vincenzo Caianiello.                                            &#13;
    Ritenuto  che  nel  corso  di  un  giudizio  civile promosso da un    &#13;
 commerciante nei confronti del rappresentante di  una  organizzazione    &#13;
 sindacale   -   che   aveva  avviato  una  campagna  di  tesseramento    &#13;
 promettendo di accollarsi le  imposte  comunali  dell'anno  in  corso    &#13;
 dovute  dai  soci  e che, nella specie, aveva omesso di provvedere al    &#13;
 pagamento dell'imposta comunale sulle  imprese,  arti  e  professioni    &#13;
 (ICIAP)  relativa  all'esercizio  commerciale  dell'attore, asserendo    &#13;
 trattarsi di  tassa  ingiusta  e  quindi  non  dovuta  -  il  giudice    &#13;
 conciliatore   di   Pisa   ha  sollevato  questione  di  legittimità    &#13;
 costituzionale dell'art. 1, comma 4, del decreto legge 2 marzo  1989,    &#13;
 n.  66,  come convertito nella legge 24 aprile 1989, n. 144, a tenore    &#13;
 del quale il nuovo tributo  è  determinato  "in  base  all'attività    &#13;
 esercitata e per classi di superficie utilizzata", secondo la tabella    &#13;
 allegata al provvedimento legislativo;                                   &#13;
      che  il giudice a quo, ritenuta la rilevanza della questione, in    &#13;
 quanto coinvolgente una norma la cui applicazione  si  porrebbe  come    &#13;
 "risolutiva"  della  controversia  insorta  tra le parti, denuncia la    &#13;
 norma impugnata per violazione dell'art. 3 della Costituzione, per la    &#13;
 discriminazione  che  si  determinerebbe  tra le attività lavorative    &#13;
 colpite ai fini del concorso nelle spese pubbliche,  e  dell'art.  53    &#13;
 della  Costituzione  in  quanto  il  sistema  "rigido" ipotizzato dal    &#13;
 legislatore non terrebbe conto della capacità contributiva effettiva    &#13;
 che,  a  parità di attività e di superficie utilizzata, può essere    &#13;
 diversa in ragione della  localizzazione  dell'esercizio  commerciale    &#13;
 (sito  in  una  metropoli  o in un piccolo comune) e che comunque non    &#13;
 può essere determinata con il criterio della "classe di superficie",    &#13;
 in  quanto criterio espressivo di una mera presunzione di reddito non    &#13;
 suffragata da riscontri obiettivi;                                       &#13;
      che,  ad  avviso  dello  stesso  giudice rimettente, la norma si    &#13;
 porrebbe altresì in contrasto  con  l'art.  35  della  Costituzione,    &#13;
 perché   verrebbero   penalizzate   le   attività   lavorative  che    &#13;
 necessitano di un certo spazio per il loro svolgimento, anche se  non    &#13;
 producono redditi vistosi;                                               &#13;
      che non si sono costituite le parti private;                        &#13;
      che  è  invece  intervenuto  il  Presidente  del  Consiglio dei    &#13;
 ministri, eccependo la inammissibilità della  questione  sotto  più    &#13;
 profili:  del  difetto  temporaneo di giurisdizione del giudice a quo    &#13;
 (art. 20 del d.P.R. 26 ottobre 1972, n. 638 richiamato  dall'art.  4,    &#13;
 comma  8,  del  D.L.  2 marzo 1989, n. 66, come convertito in legge);    &#13;
 dell'incompetenza per materia del giudice conciliatore adito (art. 9,    &#13;
 comma  secondo, c.p.c.); del carattere "fittizio" del giudizio a quo,    &#13;
 che, tenuto conto dei fatti  narrati  nell'ordinanza  di  rimessione,    &#13;
 denota  che  in  realtà  si  è in presenza di una controversia solo    &#13;
 apparente sulla sussistenza o meno della obbligazione di "accollo" di    &#13;
 imposte,   figura   contrattuale  questa  non  meritevole  di  tutela    &#13;
 giuridica ai sensi dell'art. 1322, secondo comma, del codice  civile,    &#13;
 e  comunque nulla, onde l'irrilevanza della questione che concerne la    &#13;
 legittimità costituzionale della imposta che avrebbe formato oggetto    &#13;
 dell'accollo;                                                            &#13;
      che,  nel  merito,  l'interveniente  ha contestato il denunciato    &#13;
 contrasto con i parametri costituzionali invocati, concludendo per la    &#13;
 manifesta infondatezza della questione.                                  &#13;
    Considerato  che  ictu oculi risulta che il giudice a quo non deve    &#13;
 fare applicazione della  norma  impugnata,  dovendo  la  controversia    &#13;
 essere  definita  in  base  alle  norme  sull'accollo  ed ai principi    &#13;
 affermati dalla giurisprudenza circa la nullità delle clausole di un    &#13;
 negozio  con  cui  una  parte  si  accolla  il  debito  tributario di    &#13;
 un'altra;                                                                &#13;
      che, pertanto, la questione di legittimità costituzionale, come    &#13;
 già ritenuto da questa Corte in un giudizio analogo  (sent.  n.  579    &#13;
 del  1989),  appare  artificiosamente  formulata,  essendo  del tutto    &#13;
 irrilevanti  nel  giudizio  a  quo  i  profili  che  attengono   alla    &#13;
 legittimità della pretesa tributaria, la quale concerne rapporti che    &#13;
 intercorrono fra soggetti diversi.                                       &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 1, comma 4, del decreto legge 2    &#13;
 marzo  1989  n.  66  (Disposizioni  urgenti  in  materia di autonomia    &#13;
 impositiva degli enti locali e di finanza  locale),  convertito,  con    &#13;
 modificazioni,  nella  legge  24  aprile  1989, n. 144, sollevata, in    &#13;
 riferimento agli artt. 3, 35 e 53  della  Costituzione,  dal  giudice    &#13;
 conciliatore di Pisa con l'ordinanza indicata in epigrafe.               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 marzo 1990;                                &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 4 aprile 1990.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
