<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>467</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:467</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CAIANIELLO</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/10/1995</data_decisione>
    <data_deposito>26/10/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Vincenzo CAIANIELLO; &#13;
 Giudici: avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  147  codice    &#13;
 penale  militare  di  pace,  promosso con ordinanza emessa il 7 marzo    &#13;
 1995 dal Tribunale militare  di  Padova  nel  procedimento  penale  a    &#13;
 carico  di  Piovan  Alessandro,  iscritta  al  n.  302  del  registro    &#13;
 ordinanze 1995 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 22, prima serie speciale, dell'anno 1995.                             &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 4 ottobre 1995 il Giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
    Ritenuto che il Tribunale militare di  Padova  ha  sollevato,  con    &#13;
 ordinanza   emessa   il  7  marzo  1995,  questione  di  legittimità    &#13;
 costituzionale - in riferimento agli artt. 2  e  25,  secondo  comma,    &#13;
 della  Costituzione  -  dell'art.  147  del codice penale militare di    &#13;
 pace, nella parte in cui rimette alla scelta del comandante di  corpo    &#13;
 se  esercitare  l'azione  disciplinare  ovvero  proporre richiesta di    &#13;
 procedimento penale;                                                     &#13;
      che a causa di quest'ultimo  istituto,  previsto  dall'art.  260    &#13;
 dello  stesso  codice,  a parere del remittente si configurerebbe nel    &#13;
 reato di allontanamento illecito una  norma  incriminatrice  dove  la    &#13;
 sanzione  conseguirebbe da un'opzione non già del legislatore bensì    &#13;
 di una diversa autorità, con violazione del principio di legalità;     &#13;
      che la soggezione al potere del comandante di  corpo  appare  al    &#13;
 Tribunale  remittente  anche  lesiva della garanzia dei diritti della    &#13;
 persona, assicurata dall'art. 2 della Costituzione;                      &#13;
      che è intervenuto il Presidente  del  Consiglio  dei  ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura  dello  Stato,  la quale ha    &#13;
 concluso  nel  senso  della  manifesta  infondatezza  sulla  base  di    &#13;
 precedenti decisioni di questa Corte;                                    &#13;
    Considerato che il medesimo Tribunale remittente ha già sollevato    &#13;
 più   volte   identica  questione  di  legittimità  costituzionale,    &#13;
 peraltro rendendo esplicito quel collegamento tra la norma  impugnata    &#13;
 e l'art. 260 cod. pen. mil. pace oggi non più formalmente censurato;    &#13;
      che  con  ordinanza  n. 496 del 1991 questa Corte ha ribadito il    &#13;
 carattere meramente processuale della  richiesta  del  comandante  di    &#13;
 corpo,  già rilevato in altre occasioni (sentenza n. 114 del 1982 ed    &#13;
 ordinanza n. 397 del 1987), e la sua compatibilità  con  i  principi    &#13;
 costituzionali (sentenza n. 42 del 1975);                                &#13;
      che  successivamente  la Corte non ha affatto "mutato indirizzo"    &#13;
 circa   la   qualificazione   dell'istituto   quale   condizione   di    &#13;
 procedibilità   (come   invece   afferma   il   remittente),  ma  ha    &#13;
 semplicemente  posto  in  evidenza  che  tale   meccanismo   assicura    &#13;
 un'ulteriore  garanzia  di  congruità  del  sistema  delle  sanzioni    &#13;
 rispetto alla specifica gravità del fatto (v. ordinanza n.  448  del    &#13;
 1992);   ed   ha  poi  sottolineato  che,  attraverso  l'uso  accorto    &#13;
 dell'istituto stesso, si può raggiungere  il  risultato  pratico  di    &#13;
 evitare   che  vengano  in  concreto  punite  condotte  astrattamente    &#13;
 previste dalla legge come reato;                                         &#13;
      che, inoltre, è stato posto più volte in risalto il dovere  di    &#13;
 accreditare  a  chi  esercita  il  comando  doti  di  imparzialità e    &#13;
 distacco, per cui l'uso del  potere  di  richiesta  "se  oculatamente    &#13;
 esercitato"  si  palesa  come  strumento  idoneo  ad adeguare al caso    &#13;
 concreto la risposta dell'ordinamento  militare,  il  quale  conserva    &#13;
 anche  in  ciò una sua peculiarità (cfr. sentenza n. 436 del 1995 e    &#13;
 n. 449 del 1991, nonché ordinanza n. 82 del 1994);                      &#13;
      che pertanto la questione è manifestamente infondata;              &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 147 del  codice  penale  militare  di  pace,    &#13;
 sollevata,  in  riferimento  agli  artt. 2 e 25, secondo comma, della    &#13;
 Costituzione, dal Tribunale militare di Padova,  con  l'ordinanza  di    &#13;
 cui in epigrafe.                                                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 ottobre 1995.                              &#13;
                       Il Presidente: CAIANIELLO                          &#13;
                         Il redattore: RUPERTO                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 24 ottobre 1995.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
