<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>459</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:459</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1999</data_decisione>
    <data_deposito>23/12/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di ammissibilità del conflitto tra poteri dello Stato    &#13;
 sorto a seguito della delibera dell'11 febbraio 1999 della Camera dei    &#13;
 Deputati  relativa  alla  insindacabilità  delle  opinioni  espresse    &#13;
 dall'on.  Tiziana  Parenti nei confronti del dott. Antonio Di Pietro,    &#13;
 promosso dal Tribunale di Roma, sezione  sesta  penale,  con  ricorso    &#13;
 depositato  il  16  giugno  1999  ed  iscritto al n. 121 del registro    &#13;
 ammissibilità conflitti.                                                &#13;
   Udito nella camera di consiglio del  27  ottobre  1999  il  giudice    &#13;
 relatore Fernanda Contri.                                                &#13;
   Ritenuto  che il Tribunale di Roma, nel corso di un giudizio penale    &#13;
 a carico del deputato Tiziana Parenti per il reato di diffamazione  a    &#13;
 mezzo  stampa,  con  ordinanza  emessa il 27 aprile 1999, ha promosso    &#13;
 conflitto di attribuzione fra poteri dello Stato nei confronti  della    &#13;
 Camera dei deputati, in relazione alla deliberazione dell'11 febbraio    &#13;
 1999  con  la  quale,  su  conforme  proposta  della  Giunta  per  le    &#13;
 autorizzazioni  a  procedere,  ha  dichiarato  che  i  fatti  oggetto    &#13;
 dell'imputazione   concernono   opinioni  espresse  dal  parlamentare    &#13;
 nell'esercizio delle sue funzioni e  sono  perciò  insindacabili  ai    &#13;
 sensi dell'art. 68, primo comma, della Costituzione;                     &#13;
     che,  ad  avviso  del  ricorrente, dalla formulazione della norma    &#13;
 costituzionale emergerebbe chiaramente  che  l'immunità  penale  dei    &#13;
 membri  del  Parlamento  è strettamente limitata alla loro attività    &#13;
 istituzionale, e che esulerebbe da tale previsione  ogni  altra  loro    &#13;
 attività,  sia  pure  in  senso lato politica, svolta al di fuori di    &#13;
 tali funzioni;                                                           &#13;
     che, secondo il Tribunale di Roma, nel caso in  esame  la  Camera    &#13;
 dei  deputati  non  avrebbe  legittimamente  esercitato  il potere di    &#13;
 dichiarare l'insindacabilità delle opinioni espresse  dal  deputato,    &#13;
 in  quanto  la  condotta addebitata all'on. Parenti - che consiste in    &#13;
 dichiarazioni,  ritenute  diffamatorie,  rese  nel   corso   di   una    &#13;
 intervista  ad  un organo di stampa - esulerebbe dall'esercizio delle    &#13;
 funzioni parlamentari intese in senso stretto;                           &#13;
     che il ricorrente ritiene perciò che vi siano i presupposti  per    &#13;
 sollevare  conflitto  di  attribuzione  tra  poteri  dello  Stato dal    &#13;
 momento che, sotto il profilo soggettivo, il tribunale è  competente    &#13;
 a  decidere  in  ordine  alla  illiceità  della  condotta addebitata    &#13;
 all'on.  Parenti, e che, sotto il  profilo  oggettivo,  si  verte  in    &#13;
 ordine  alla sussistenza dei presupposti per l'applicazione dell'art.    &#13;
 68, primo comma, della Costituzione ed alla lesione, da  parte  della    &#13;
 Camera     dei     deputati,    di    attribuzioni    giurisdizionali    &#13;
 costituzionalmente garantite;                                            &#13;
     che il Tribunale di Roma chiede perciò alla Corte di  dichiarare    &#13;
 che non spetta alla Camera dei deputati di deliberare che i fatti per    &#13;
 i   quali  pende  il  procedimento  nei  confronti  dell'on.  Parenti    &#13;
 concernono opinioni dalla stessa espresse  nell'esercizio  delle  sue    &#13;
 funzioni   di  parlamentare  e  conseguentemente  di  annullare,  per    &#13;
 violazione degli artt. 68, primo comma, 3,  primo  comma,  24,  primo    &#13;
 comma, 101, secondo comma e 104, primo comma, Cost., la deliberazione    &#13;
 assunta in data 11 febbraio 1999.                                        &#13;
   Considerato che nella presente fase del giudizio, a norma dell'art.    &#13;
 37,  terzo e quarto comma, della legge 11 marzo 1953, n. 87, la Corte    &#13;
 è chiamata a delibare, senza contraddittorio, se, nel  concorso  dei    &#13;
 requisiti  soggettivi prescritti, e in quanto esista la materia di un    &#13;
 conflitto la cui decisione appartenga alla sua competenza, il ricorso    &#13;
 sia ammissibile, restando impregiudicata ogni altra decisione;           &#13;
     che, sotto  il  profilo  soggettivo,  il  Tribunale  di  Roma  è    &#13;
 legittimato a sollevare il conflitto, in quanto, come questa Corte ha    &#13;
 più  volte affermato, i singoli organi giurisdizionali, in relazione    &#13;
 al carattere diffuso che  connota  il  potere  di  cui  fanno  parte,    &#13;
 svolgono  le  loro  funzioni  in  piena  autonomia  ed hanno perciò,    &#13;
 nell'esercizio  della  loro  attività,  assistita   dalla   garanzia    &#13;
 costituzionale,  competenza  a dichiarare definitivamente la volontà    &#13;
 del potere cui appartengono (cfr. da ultimo le ordinanze  nn.  363  e    &#13;
 319 del 1999);                                                           &#13;
     che,  sempre  secondo la costante giurisprudenza di questa Corte,    &#13;
 anche la Camera dei deputati è parimenti legittimata ad essere parte    &#13;
 del conflitto, posto che essa è  competente  a  dichiarare  in  modo    &#13;
 definitivo   la   propria   volontà   in   relazione  all'ambito  di    &#13;
 applicazione ai suoi componenti  dell'art.  68,  primo  comma,  della    &#13;
 Costituzione;                                                            &#13;
     che,   sotto  il  profilo  oggettivo,  sussiste  la  materia  del    &#13;
 conflitto, dal momento che il ricorrente assume che la propria  sfera    &#13;
 di    attribuzioni,    costituzionalmente    garantita,    è   stata    &#13;
 illegittimamente lesa dalla citata  deliberazione  della  Camera  dei    &#13;
 deputati;                                                                &#13;
     che  dal  ricorso si ricavano le ragioni del conflitto e le norme    &#13;
 costituzionali che regolano la materia.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara ammissibile, ai sensi dell'art. 37 della  legge  11  marzo    &#13;
 1953,  n.  87,  il conflitto di attribuzione promosso, con il ricorso    &#13;
 indicato in epigrafe, dal  Tribunale  di  Roma  nei  confronti  della    &#13;
 Camera dei deputati;                                                     &#13;
   Dispone:                                                               &#13;
     a)  che  la  Cancelleria  della  Corte  dia  comunicazione  della    &#13;
 presente ordinanza al Tribunale di Roma ricorrente;                      &#13;
     b) che, a cura del ricorrente, il ricorso e la presente ordinanza    &#13;
 siano notificati  alla  Camera  dei  deputati,  in  persona  del  suo    &#13;
 Presidente  entro  il termine di 60 giorni dalla comunicazione di cui    &#13;
 al punto a) per essere depositati nella cancelleria della Corte entro    &#13;
 il termine di venti giorni dalla notificazione,  secondo  l'art.  26,    &#13;
 terzo comma, delle norme integrative per i giudizi davanti alla Corte    &#13;
 costituzionale.                                                          &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 dicembre 1999.                             &#13;
                        Il Presidente: Vassalli                           &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 23 dicembre 1999.                         &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
