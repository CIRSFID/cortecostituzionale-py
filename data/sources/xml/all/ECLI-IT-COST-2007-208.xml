<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>208</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:208</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>06/06/2007</data_decisione>
    <data_deposito>18/06/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale in materia di inappellabilità delle sentenze di proscioglimento), promossi nel corso di diversi procedimenti penali con ordinanze del 14 e 17 marzo, del 3 aprile e del 17 marzo 2006 dalla Corte d'appello di Brescia, rispettivamente iscritte ai nn. 270, 494, 520 e 603 del registro ordinanze 2006 e pubblicate nella Gazzetta Ufficiale della Repubblica, nn. 35, 46 e 47, prima serie speciale, dell'anno 2006 ed al n. 2, prima serie speciale, dell'anno 2007. &#13;
    Udito nella camera di consiglio del 23 maggio 2007 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che con quattro ordinanze, di contenuto identico nella parte motiva, la Corte d'appello di Brescia ha sollevato, in riferimento agli artt. 3, 24, 111, secondo comma, e 112 della Costituzione, questione di legittimità costituzionale dell'art. 593 del codice di procedura penale, come sostituito dall'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale in materia di inappellabilità delle sentenze di proscioglimento), nella parte in cui non prevede per il pubblico ministero la possibilità di appellare le sentenze di proscioglimento al di fuori dei casi di cui al comma 2 del medesimo articolo; &#13;
    che il giudice a quo – premesso di essere chiamato a celebrare il giudizio d'appello a seguito di impugnazione proposta dal pubblico ministero avverso una sentenza di assoluzione pronunciata in primo grado – precisa che, entrata in vigore nelle more del gravame la legge n. 46 del 2006, l'appello dovrebbe essere dichiarato inammissibile ai sensi dell'art. 10, comma 2, della medesima legge; &#13;
    che, nel merito, la disciplina censurata violerebbe plurimi parametri costituzionali; &#13;
    che la Corte rimettente lamenta, in primo luogo, la lesione del principio dell'obbligatorietà dell'azione penale sancito nell'art. 112 Cost., sul rilievo che la limitazione del potere di appello in capo al pubblico ministero, essendo «tale da ridurre lo stesso a casi marginali, per non dire estremi», pregiudica la funzionalità propria dell'esercizio dell'azione, tesa alla concreta attuazione di valori costituzionali; &#13;
    che nella medesima prospettiva la norma censurata vanificherebbe anche il diritto di difesa garantito «anche alle parti offese dei reati», atteso che l'esercizio dell'azione penale da parte del pubblico ministero vale «ad offrire alle vittime dei reati l'essenziale tutela del loro legittimo interesse»; &#13;
     che il giudice a quo evidenzia, inoltre, il contrasto della norma censurata con l'art. 111, comma secondo, Cost., e precisamente con la regola secondo cui il processo si svolge nel contraddittorio fra le parti ed in condizioni di parità delle stesse: ancorché infatti il principio della parità tra le parti non comporti necessariamente identità dei poteri processuali una distribuzione del potere di impugnazione completamente squilibrata «pregiudica significativamente il principio del contraddittorio» proprio in quanto altera in misura intollerabile l'equilibrio imposto dalla norma costituzionale; &#13;
    che il rimettente ritiene ancora che la norma censurata risulti incompatibile con il principio di ragionevolezza, difettando nella limitazione pressoché assoluta del potere di impugnazione del pubblico ministero ogni ragionevole giustificazione, mentre proprio l'attuazione di valori di legalità e difesa sociale esigono che «il processo mantenga un equilibrato contraddittorio fra tali ragioni e la difesa dell'imputato, perché nessuna opportunità  di ricerca della verità venga ad essere sottratta al giudizio»;  &#13;
    che, infine, l'art. 593 cod. proc. pen., come novellato, creerebbe un'irragionevole disparità di trattamento là dove, per un verso, impedisce all'organo dell'accusa l'appello contro le sentenze di proscioglimento, e, per l'altro, mantiene in capo al pubblico ministero il potere di impugnazione avverso le sentenze di condanna, così privilegiando «la cura di un interesse processuale indubbiamente di minore consistenza». &#13;
    Considerato che il dubbio di costituzionalità sottoposto a questa Corte ha per oggetto la preclusione, conseguente alla modifica dell'art. 593 del codice di procedura penale ad opera dell'art. 1 della legge 20 febbraio 2006, n. 46, dell'appello delle sentenze dibattimentali di proscioglimento da parte del pubblico ministero; &#13;
    che, stante l'identità delle questioni proposte, i relativi giudizi vanno riuniti per essere decisi con unica pronuncia;  &#13;
    che, successivamente alle ordinanze di rimessione, questa Corte, con sentenza n. 26 del 2007, ha dichiarato l'illegittimità costituzionale dell'art. 1 della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui, sostituendo l'art. 593 del codice di procedura penale, esclude che il pubblico ministero possa appellare contro le sentenze di proscioglimento, fatta eccezione per le ipotesi previste dall'art. 603, comma 2, del medesimo codice, se la nuova prova è decisiva», e dell'art. 10, comma 2, della citata legge n. 46 del 2006, «nella parte in cui prevede che l'appello proposto contro una sentenza di proscioglimento dal pubblico ministero prima della data di entrata in vigore della medesima legge è dichiarato inammissibile»; &#13;
    che, alla stregua della richiamata pronuncia di questa Corte, gli atti devono essere pertanto restituiti ai giudici rimettenti per un nuovo esame della rilevanza delle questioni.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi  &#13;
LA CORTE COSTITUZIONALE  &#13;
    riuniti i giudizi,  &#13;
    ordina la restituzione degli atti alla Corte d'appello di Brescia. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 giugno 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 18 giugno 2007.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
