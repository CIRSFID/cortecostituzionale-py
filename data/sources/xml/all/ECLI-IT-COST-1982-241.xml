<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>241</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:241</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Brunetto Bucciarelli Ducci</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/12/1982</data_decisione>
    <data_deposito>29/12/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI &#13;
 - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. &#13;
 ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'articolo unico  &#13;
 della  legge  17  luglio  1975,  n.  355  (Esclusione  dei  rivenditori  &#13;
 professionali della stampa periodica e dei librai dalla responsabilità  &#13;
 derivante dagli artt. 528 e 725 del codice penale e dagli artt. 14 e 15  &#13;
 della  legge 8 febbraio 1948, n. 47) promosso con ordinanza emessa il 6  &#13;
 marzo 1976 dal Pretore di Palermo, nel procedimento penale a carico  di  &#13;
 Ajello  Simone,  iscritta  al  n.  360  del  registro  ordinanze 1976 e  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  158  del  16  &#13;
 giugno 1976.                                                             &#13;
     Visti   l'atto  di  costituzione  di  Ajello  Simone  e  l'atto  di  &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
     udito  nell'udienza  pubblica  del  10  novembre  1982  il  Giudice  &#13;
 relatore Brunetto Bucciarelli Ducci;                                     &#13;
     udito  l'avvocato  dello  Stato Giorgio Azzariti, per il Presidente  &#13;
 del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ordinanza del 6 marzo 1976 (r.o. n.  360)  il  Pretore  di  &#13;
 Palermo  ha  sollevato, nel corso di un procedimento penale a carico di  &#13;
 Simone Ajello, questione di legittimità  costituzionale  in  relazione  &#13;
 agli  artt.  3,  21  e 31 della Costituzione, dell'articolo unico della  &#13;
 legge 17 luglio 1975, n. 355, nella parte in cui esclude i  rivenditori  &#13;
 professionali  della stampa periodica ed i librai dalla responsabilità  &#13;
 derivante dagli artt. 528 e 725 c.p. e dagli artt. 14 e 15 della  legge  &#13;
 8 febbraio 1948, n. 47.                                                  &#13;
     Dubita  il  giudice  a quo che la norma impugnata, con l'esclusione  &#13;
 automatica e generalizzata del dolo, e persino  della  colpa  nel  caso  &#13;
 della    contravvenzione   di   cui   all'art.   725   c.p.,   riferita  &#13;
 all'appartenenza a  categorie  professionali  ed  alla  presunzione  di  &#13;
 inadeguatezza   culturale,  introduca  un  nuovo  tipo  di  incapacità  &#13;
 incompatibile coi principi fondamentali relativi alla persona  umana  e  &#13;
 violi  così  l'art.  3  della  Costituzione, escludendo la punibilità  &#13;
 dell'autore di un fatto che oggettivamente integra gli  estremi  di  un  &#13;
 reato  sulla base di inesistenti condizioni e circostanze soggettive, e  &#13;
 ciò anche in contrasto con le pronuncie di questa Corte, che non hanno  &#13;
 ravvisato   la   difficoltà   di  ordine  intellettivo  riguardo  alla  &#13;
 possibilità e al dovere degli edicolanti di esaminare  e  valutare  la  &#13;
 merce  ed  hanno  demandato  al  giudice,  come è costituzionalmente e  &#13;
 giuridicamente naturale, di stabilire caso per caso la  responsabilità  &#13;
 penale dell'imputato (sentt.  nn. 159/1970 e 93/1972).                   &#13;
     La  norma  sarebbe  inoltre  in  contrasto - secondo l'ordinanza di  &#13;
 rimessione -  con  l'art.  21  ultimo  comma  della  Costituzione,  che  &#13;
 espressamente  e  categoricamente  vieta le pubblicazioni a stampa, gli  &#13;
 spettacoli e tutte le altre manifestazioni contrarie  al  buon  costume  &#13;
 senza  eccezione alcuna in ordine a fatti e a persone, sia direttamente  &#13;
 attraverso l'anomala deroga alla punibilità della violazione  di  tale  &#13;
 diritto,  sia  indirettamente attraverso la difficoltà che si è posta  &#13;
 alla generale applicazione di alcune norme relative  al  buon  costume,  &#13;
 per  la  mancanza  di  coordinamento  della  norma  impugnata con altre  &#13;
 disposizioni di legge in materia. Quando infatti si  esclude  l'esonero  &#13;
 di   responsabilità   solo   nel  caso  in  cui  siano  esposte  parti  &#13;
 "palesemente"  oscene  delle  pubblicazioni,  si   rende   praticamente  &#13;
 inapplicabile  l'art.  1  della  legge  2  dicembre 1960, n. 1591, che,  &#13;
 riguardo all'ipotesi più lata di  esposizione  di  soggetti  figurati,  &#13;
 rapporta la nozione di osceno e di contrario alla pubblica decenza alla  &#13;
 particolare  sensibilità  dei  minori  degli anni 18, con un parametro  &#13;
 quindi più severo rispetto a quello della "palese oscenità",  di  cui  &#13;
 alla norma impugnata.                                                    &#13;
     Quanto al contrasto con l'art. 31 Cost. il giudice a quo lo ravvisa  &#13;
 nel  fatto  che  la  norma  impugnata  anziché  agevolare,  ostacola e  &#13;
 contrasta l'adempimento del compito educativo e formativo dei  genitori  &#13;
 riguardo  ai  figli  e  viene  meno al dichiarato impegno di proteggere  &#13;
 l'infanzia e la gioventù di cui al capoverso dell'art. 31.              &#13;
     La  norma,  infine,  violerebbe  i  principi   fondamentali   della  &#13;
 divisione  dei  poteri, essendosi arrogato il legislatore il compito di  &#13;
 giudicare riservato al giudice, e  della  effettiva  espressione  della  &#13;
 democrazia  parlamentare, essendosi legiferato sotto la pressione della  &#13;
 piazza.                                                                  &#13;
     2. - Si è costituita in giudizio la parte privata  Simone  Ajello,  &#13;
 rappresentato e difeso dall'avv. Dario Di Gravio, con atto del 3 luglio  &#13;
 1976, sostenendo l'infondatezza della questione sollevata, in quanto il  &#13;
 contrasto con i parametri costituzionali invocati sarebbe escluso dalla  &#13;
 particolare  ratio  della  norma  impugnata.  Questa, infatti, in tanto  &#13;
 esclude la responsabilità di edicolanti e librai in quanto parte dalla  &#13;
 constatazione della impossibilità che in astratto hanno i  rivenditori  &#13;
 di   giornali  di  ingerirsi  nelle  situazioni  che  attribuiscono  il  &#13;
 carattere penale ad una determinata attività umana.                     &#13;
     3. - È intervenuto nel giudizio anche il Presidente del Consiglio,  &#13;
 rappresentato e difeso dall'Avvocatura Generale dello Stato,  con  atto  &#13;
 del  23  giugno  1976,  concludendo  per l'infondatezza della questione  &#13;
 proposta.                                                                &#13;
     In particolare,  quanto  alla  pretesa  violazione  dell'art.    3,  &#13;
 l'Avvocatura  osserva  che  la  causa di non punibilità prevista dalla  &#13;
 norma impugnata si inserisce tra i molti esempi che l'ordinamento offre  &#13;
 di  cause  speciali  di   non   punibilità   per   inesigibilità   di  &#13;
 comportamento   diverso.    Piuttosto  che  esprimere  disistima  nella  &#13;
 capacità intellettiva dei beneficiari della norma, il  legislatore  ha  &#13;
 ritenuto  che  non si possa da essi pretendere un comportamento tale da  &#13;
 consentire loro di ben distinguere pubblicazione da  pubblicazione,  in  &#13;
 quanto   li   assorbirebbe   per  intero  ed  anche  oltre  le  proprie  &#13;
 possibilità di lavoro. Si tratta - per la difesa dello Stato - di  una  &#13;
 valutazione  di  politica  criminale che rientra nella discrezionalità  &#13;
 del legislatore; valutazione che può anche mutare rispetto al passato,  &#13;
 cosicché  tale  discrezionalità  non  è  infirmata   dall'ortodossia  &#13;
 costituzionale  di  precedenti  norme che regolavano la materia in modo  &#13;
 diverso  (ved.  citate  sentenze  della  Corte  cost.  nn.  159/1970  e  &#13;
 93/1972).<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - La questione sottoposta all'esame della Corte è se contrasti  &#13;
 o meno con gli artt. 3, 21 e 31  della  Costituzione  l'articolo  unico  &#13;
 della  legge  17  luglio  1975,  n.  355,  nella parte in cui esclude i  &#13;
 rivenditori professionali della stampa  periodica  ed  i  librai  dalla  &#13;
 responsabilità  derivante  dagli  artt.  528 e 725 c.p. e 14, 15 della  &#13;
 legge 8 febbraio 1948, n. 47.                                            &#13;
     Secondo il giudice a quo  tale  norma  infatti:  a)  violerebbe  il  &#13;
 principio   di   uguaglianza,   sulla  base  di  condizioni  soggettive  &#13;
 giuridicamente irrilevanti, escludendo  la  punibilità  di  fatti  che  &#13;
 obiettivamente  integrano  un  reato;  b)  renderebbe  vano  il divieto  &#13;
 costituzionale delle pubblicazioni a stampa contrarie al buon  costume;  &#13;
 c)  disattenderebbe  il  dovere  dello  Stato  di proteggere moralmente  &#13;
 l'infanzia e la gioventù; d) violerebbe, infine,  il  principio  della  &#13;
 divisione dei poteri sottraendo al giudice l'indagine sulla sussistenza  &#13;
 dell'elemento soggettivo del reato.                                      &#13;
     2. - La questione è inammissibile per difetto di motivazione circa  &#13;
 la sua rilevanza nel procedimento a quo.                                 &#13;
     In  effetti, non solo l'ordinanza di rimessione non contiene alcuna  &#13;
 motivazione circa l'influenza che la decisione della questione da parte  &#13;
 della  Corte  costituzionale  avrebbe  nella  fattispecie  oggetto  del  &#13;
 giudizio  penale  pendente davanti al Pretore di Palermo, nel corso del  &#13;
 quale l'eccezione di legittimità costituzionale è stata sollevata; ma  &#13;
 non fa alcun riferimento al caso  concreto  sottoposto  al  giudice  di  &#13;
 merito  e  al  contesto  di  fatto  che  è  all'origine  del  rapporto  &#13;
 processuale.                                                             &#13;
     È rimasta, quindi, insoddisfatta la fondamentale esigenza, cui  è  &#13;
 preordinato  l'art.  23 della legge 11 marzo 1953, n. 87 (commi 2 e 3),  &#13;
 di far conoscere con le dovute garanzie di pubblicità i  termini  e  i  &#13;
 motivi con i quali la questione è stata sollevata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   l'inammissibilità   della   questione  di  legittimità  &#13;
 costituzionale dell'articolo unico della legge 17 luglio 1975, n.  355,  &#13;
 sollevata  in  riferimento  agli artt. 3, 21 e 31 Cost. con l'ordinanza  &#13;
 indicata in epigrafe.                                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 20 dicembre 1982.                             &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
