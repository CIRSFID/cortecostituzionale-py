<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-07-30/180/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-07-30/180"/>
          <FRBRalias value="ECLI:IT:COST:2020:180" name="ECLI"/>
          <FRBRdate date="2020-07-30" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="180"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-07-30/180/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-07-30/180/ita@"/>
          <FRBRdate date="2020-07-30" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-07-30/180/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-07-30/180/ita@.xml"/>
          <FRBRdate date="2020-07-30" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-07-09" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-07-30" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>180</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:180</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Luca Antonini</akn4cc:relatore>
        <akn4cc:data_decisione>2020-07-09</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-07-30</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge della Regione Lombardia 4 marzo 2019, n. 4, recante «Modifiche e integrazioni alla legge regionale 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità): abrogazione del Capo III "Norme in materia di attività e servizi necroscopici, funebri e cimiteriali" del Titolo VI e introduzione del Titolo VI-bis "Norme in materia di medicina legale, polizia mortuaria, attività funebre"», promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 30 aprile-3 maggio 2019, depositato in cancelleria il 7 maggio 2019, iscritto al n. 56 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 25, prima serie speciale, dell'anno 2019.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Lombardia;
 udito nella udienza pubblica del 7 luglio 2020 il Giudice relatore Luca Antonini;
 uditi l'avvocato dello Stato Enrico De Giovanni per il Presidente del Consiglio dei ministri e l'avvocato Emanuela Quici per la Regione Lombardia;
 deliberato nella camera di consiglio del 9 luglio 2020.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso notificato il 30 aprile-3 maggio 2019 e depositato il 7 maggio 2019, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso - in riferimento, nel complesso, agli art. 117, commi secondo, lettere i) ed l), e terzo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> - questioni di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge della Regione Lombardia 4 marzo 2019, n. 4, recante «Modifiche e integrazioni alla legge regionale 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità): abrogazione del Capo III "Norme in materia di attività e servizi necroscopici, funebri e cimiteriali" del Titolo VI e introduzione del Titolo VI-bis "Norme in materia di medicina legale, polizia mortuaria, attività funebre"».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Tale disposizione è censurata nelle parti in cui introduce nella legge della Regione Lombardia 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità), gli artt. 69, comma 3; 70-bis; 71, comma 3; 72, comma 1; 73, commi 2, primo periodo, e 4; 74, comma 1, lettera e); 74-bis; 75, commi 4, primo periodo, 8, lettere a) e c) (recte: lettera b), 11, ultimo periodo, e 13; 76, comma 1, lettere e) e g).
 2.- Il citato art. 69, comma 3, dispone che «[l]'accertamento di morte è effettuato, su richiesta dell'ufficiale di stato civile, da un medico incaricato delle funzioni di necroscopo dall'ASST [azienda socio sanitaria territoriale]».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il successivo art. 73 prevede, nei suoi commi 2, primo periodo, e 4, che le autorizzazioni, rispettivamente, alla dispersione delle ceneri e, in caso di comprovata insufficienza delle sepolture, alla cremazione dei cadaveri inumati da almeno dieci anni o tumulati da almeno venti anni siano rilasciate dall'ufficiale di stato civile.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ad avviso del ricorrente, queste norme violerebbero l'art. 117, secondo comma, lettera i), Cost., in relazione alla materia «stato civile», attribuendo agli ufficiali dello stato civile compiti ulteriori rispetto a quelli previsti dagli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2000-11-03/396/!main#art_71">artt. 71</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2000-11-03/396/!main#art_72">72</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2000-11-03/396/!main#art_74">74 del decreto del Presidente della Repubblica 3 novembre 2000, n. 396</ref></mref> (Regolamento per la revisione e la semplificazione dell'ordinamento dello stato civile, a norma dell'<ref href="/akn/it/act/legge/stato/1997-05-15/127/!main#art_2__para_12">articolo 2, comma 12, della legge 15 maggio 1997, n. 127</ref>).</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>1.- L'art. 71, comma 3 - disponendo che, «[a] seguito di interventi chirurgici in strutture ospedaliere del territorio comunale», il cittadino può «decide[re] se donare eventuali parti anatomiche riconoscibili per finalità di studio, ricerca o insegnamento o se richiederne la sepoltura» -, recherebbe un vulnus all'art. 117, secondo comma, lettera l), Cost., in relazione alla materia «ordinamento civile».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Lo Stato, infatti, avrebbe esercitato la propria competenza legislativa esclusiva in tale ambito materiale disciplinando sia l'utilizzo di cadaveri ai fini dell'insegnamento e delle indagini scientifiche (<mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990-09-10/285/!main#art_40">artt. 40</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990-09-10/285/!main#art_41">41</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990-09-10/285/!main#art_42">42 del decreto del Presidente della Repubblica 10 settembre 1990, n. 285</ref></mref>, recante «Approvazione del regolamento di polizia mortuaria»), sia il prelievo di organi e di tessuti a scopo di trapianto terapeutico (artt. 3 e 6 della legge 1° aprile 1999, n. 91, recante «Disposizioni in materia di prelievi e di trapianti di organi e di tessuti», e, in precedenza, <ref href="/akn/it/act/legge/stato/1968-04-02/519/!main#art_1">art. 1 della legge 2 aprile 1968, n. 519</ref>, recante «Modifiche alla <ref href="/akn/it/act/legge/stato/1957-04-03/235/!main">legge 3 aprile 1957, n. 235</ref>, relativa ai prelievi di parti di cadavere a scopo di trapianto terapeutico»). Al riguardo, l'Avvocatura generale dello Stato richiama anche il disegno di legge AS 733 del 2018 che, nelle more dell'odierno giudizio, è stato approvato con la <ref href="/akn/it/act/legge/stato/2020-02-10/10/!main">legge 10 febbraio 2020, n. 10</ref> (Norme in materia di disposizione del proprio corpo e dei tessuti post mortem a fini di studio, di formazione e di ricerca scientifica).
 2.2.- Secondo il ricorrente, le altre disposizioni impugnate introdotte nella legge reg. Lombardia n. 33 del 2009 sarebbero ascrivibili a una materia che, «per gli aspetti tecnici [...], ricade in ambito sanitario» e violerebbero l'art. 117, terzo comma, Cost., in relazione alla «tutela della salute».
 Il Presidente del Consiglio dei ministri premette che tali disposizioni non sarebbero «in linea con i principi fondamentali in materia di "tutela della salute" contenuti nella normativa statale di riferimento, e segnatamente nel <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref>», recante il regolamento di polizia mortuaria.
 Quindi, precisa che l'invasione della sfera di competenza statale deriverebbe dal rilievo che, da un lato, un primo gruppo di queste avrebbe ad oggetto «fattispecie non previste» dal menzionato <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref>; dall'altro, un secondo gruppo delle medesime detterebbe una disciplina in contrasto con specifiche previsioni di tale regolamento.
 2.2.1.- Sarebbero, in particolare, riconducibili al primo profilo di censura: a) l'art. 70-bis, che regola le «case funerarie»; b) l'art. 74, comma l, lettera e), che annovera i trattamenti di tanatocosmesi tra le prestazioni che le imprese esercenti attività funebre possono svolgere; c) l'art. 74-bis, che disciplina il «centro servizi» quale impresa che svolge attività funebre; d) l'art. 75, comma 8, lettera a), il quale dispone che il Comune può autorizzare «la costruzione e l'uso di aree e spazi per la sepoltura di animali d'affezione»; e) infine, l'art. 76, comma l, lettera e), che demanda a un regolamento di attuazione la definizione delle caratteristiche, tra l'altro, dei «loculi areati».</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>2.2.- Sarebbe, invece, riconducibile al secondo profilo di censura, innanzitutto, l'art. 72, il quale disciplina il trasporto funebre disponendo, all'ultimo periodo del comma l, che, «[a]l fine di consentire lo svolgimento dei riti funebri, il trasferimento deve comunque essere effettuato entro ventiquattro ore dal rilascio della certificazione attestante il termine delle operazioni di prelievo di organi o di riscontro diagnostico, ovvero dal rilascio del nulla osta al seppellimento o alla cremazione da parte dell'autorità giudiziaria»: in particolare, questa disposizione confliggerebbe con le norme di cui agli artt. 8 e 10 e di cui al Capo IV del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref>, secondo cui il trasporto delle salme può avvenire solo dopo il decorso di ventiquattro ore dal decesso; essa si presterebbe inoltre a interpretazioni elusive dell'art. 23 del medesimo d.P.R., a mente del quale l'incaricato al trasporto delle salme deve essere munito di apposita autorizzazione comunale.
 L'art. 75, comma 4, primo periodo, consentendo di devolvere la gestione e la manutenzione dei cimiteri a soggetti privati, contrasterebbe con l'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main#art_51__para_1">art. 51, comma 1, del d.P.R. n. 285 del 1990</ref>: norma, questa, che al contrario attribuirebbe «i compiti di manutenzione, ordine e vigilanza dei cimiteri al comune, in ragione dei rilevanti interessi igienico­sanitari sottesi a tali attività».
 Sono altresì censurati gli artt. 75, comma 8, lettera b), e 76, comma l, lettera g). La prima disposizione prevede la possibilità che il Comune autorizzi «la costruzione di cappelle private fuori dal cimitero, purché contornate da un'area di rispetto», e la seconda rimette a un regolamento di attuazione la determinazione dell'ampiezza di tali aree: il «combinato disposto» di siffatte norme divergerebbe dall'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main#art_104__para_2">art. 104, comma 2, del d.P.R. n. 285 del 1990</ref>, a mente del quale la costruzione e l'uso di dette cappelle sono consentite soltanto quando queste «siano attorniate per un raggio di metri 200 da fondi di proprietà delle famiglie che ne chiedano la concessione e sui quali gli stessi assumano il vincolo di inalienabilità e di inedificabilità».
 L'art. 75, comma 11, stabilisce, al suo ultimo periodo, che la soppressione dei cimiteri è autorizzata dalle agenzie di tutela della salute (ATS), così ponendosi in contrasto, a parere della difesa statale, con il disposto dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main#art_96">art. 96 del d.P.R. n. 285 del 1990</ref>, secondo cui nessun cimitero può essere soppresso se non per ragioni di dimostrata necessità (comma 1) e la soppressione deve essere deliberata dal consiglio comunale (comma 2).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>È, infine, impugnato l'art. 75, comma 13, che consente di deporre nel loculo del defunto o nella tomba di famiglia, in teca separata e previa cremazione, i resti degli animali di affezione: tale facoltà contraddirebbe, in particolare, la norma di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main#art_50">art. 50 del d.P.R. n. 285 del 1990</ref>, secondo cui nei cimiteri potrebbero essere ricevuti soltanto i cadaveri delle persone.
 3.- Si è costituita in giudizio la Regione Lombardia, nella persona del Presidente della Giunta regionale, chiedendo la declaratoria d'inammissibilità delle questioni promosse in riferimento all'art. 117, terzo comma, Cost. e, comunque, il rigetto integrale del ricorso.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>1.- Prendendo le mosse dagli artt. 69, 71 e 73 della legge reg. Lombardia n. 33 del 2009, la difesa regionale rappresenta che queste norme sarebbero state oggetto, in considerazione dei motivi d'impugnazione, di «attento esame» da parte dell'ufficio legislativo e della avvocatura della Regione, preannunciandone un possibile esito abrogativo.
 3.2.- Le censure afferenti alla violazione, da parte delle altre disposizioni impugnate, dell'art. 117, terzo comma, Cost. sarebbero invece inammissibili perché formulate in modo generico e indeterminato: il ricorrente non avrebbe difatti individuato le specifiche norme statali disattese e i principi fondamentali da queste posti e in ipotesi compromessi.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>2.1.- Lesione che, venendo al merito, in ogni caso non sussisterebbe, dal momento che le disposizioni censurate recherebbero norme di dettaglio non confliggenti con la normativa statale evocata dal ricorrente.
 Nello specifico, muovendo dall'art. 70-bis della legge reg. Lombardia n. 33 del 2009, la difesa regionale rileva che le case funerarie da esso regolate rappresenterebbero una «realtà diffusa sul territorio [...], per la quale è stata avvertita l'esigenza di adottare specifiche disposizioni, comunque non in contrasto» con il <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref>.
 Quanto alla censura afferente all'art. 72, comma 1, la Regione sottolinea che questa norma riguarda il trasporto della salma, ovvero, secondo quanto previsto dal precedente art. 67-bis, il «corpo umano rimasto privo delle funzioni vitali fino all'accertamento della morte», e non del cadavere, ovvero, secondo la qualificazione datane dal medesimo art. 67-bis, il «corpo umano privo delle funzioni vitali, di cui sia stata accertata la morte»: da ciò deriverebbe che sarebbe stato rispettato il disposto dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main#art_8">art. 8 del d.P.R. n. 285 del 1990</ref>, il quale prescrive un periodo di osservazione dei cadaveri di ventiquattro ore dal decesso.
 In merito all'art. 74, comma 1, lettera e), la difesa regionale si limita a evidenziare che la tanatocosmesi - consistente in un «insieme di trattamenti igienici ed estetici praticati sul cadavere allo scopo di migliorane la presentabilità» - costituirebbe una pratica distinta dalla tanatoprassi.
 Con riguardo all'art. 74-bis, che disciplina il «centro servizi», la resistente rimarca che si tratta di un'impresa funebre che fornisce ad altre imprese dello stesso settore «supporti per sgravarle [...] di oneri relativi al personale e [ai] mezzi richiesti per lo svolgimento dell'attività».
 Per quanto concerne l'art. 75, comma 4, la Regione precisa che l'eventuale devoluzione della gestione e della manutenzione dei cimiteri anche a soggetti privati deve comunque avvenire nel rispetto delle modalità previste dall'ordinamento e, quindi, attraverso «pubbliche gare per [l']affidamento [dei] servizi in concessione».
 Nemmeno i commi 8, lettera a), e 13 dell'art. 75 contrasterebbero con il <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref>, giacché questo non vieta l'utilizzo di spazi cimiteriali per la sepoltura degli animali di affezione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Analogamente, l'art. 75, comma 8, lettera b), nel consentire ai Comuni di autorizzare la costruzione di cappelle private fuori dal cimitero, avrebbe natura sostanzialmente ricognitiva, essendo tale possibilità già prevista dalla normativa statale.
 In ordine all'art. 75, comma 11, la Regione osserva che l'autorizzazione della ATS alla soppressione dei cimiteri presuppone in ogni caso la richiesta del Comune: così intesa, la norma non divergerebbe dall'evocato <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main#art_96">art. 96 del d.P.R. n. 285 del 1990</ref>.
 Infine, con riferimento all'art. 76, comma 1, lettera e), la resistente sottolinea, innanzitutto, che il <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref> non distinguerebbe tra loculi areati o meno; in secondo luogo, che lo stesso ricorrente ha affermato, nell'atto introduttivo del giudizio, che i loculi areati offrono «indubbi vantaggi in termini igienico-sanitari».
 4.- Successivamente alla proposizione del ricorso, l'art. 17 della legge della Regione Lombardia 6 agosto 2019, n. 15 (Assestamento al bilancio 2019-2021 con modifiche di leggi regionali), per quanto qui interessa: a) ha soppresso il primo periodo dell'art. 73, comma 2, della legge reg. Lombardia n. 33 del 2009 e, al comma 3 del precedente art. 69, le parole «su richiesta dell'ufficiale di stato civile»; b) ha abrogato il comma 3 dell'art. 71 e il comma 4 dell'art. 73 della legge appena citata.
 Sulla scorta di siffatto ius superveniens, il Presidente del Consiglio dei ministri, con atto depositato il 12 marzo 2020, ha rinunciato al ricorso in parte qua, ritenendo che siano venute meno le ragioni poste a fondamento dell'impugnazione.
 La Regione Lombardia ha accettato tale rinuncia parziale.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso - in riferimento, nel complesso, agli art. 117, commi secondo, lettere i) ed l), e terzo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> - questioni di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge della Regione Lombardia 4 marzo 2019, n. 4, recante «Modifiche e integrazioni alla legge regionale 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità): abrogazione del Capo III "Norme in materia di attività e servizi necroscopici, funebri e cimiteriali" del Titolo VI e introduzione del Titolo VI-bis "Norme in materia di medicina legale, polizia mortuaria, attività funebre"».</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>- Tale disposizione inserisce nella legge della Regione Lombardia 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità), il Titolo VI-bis, composto dagli articoli da 67 a 77.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il ricorrente dubita, in particolare, della legittimità costituzionale delle norme di cui agli artt. 69, comma 3; 70-bis; 71, comma 3; 72, comma 1; 73, commi 2, primo periodo, e 4; 74, comma 1, lettera e); 74-bis; 75, commi 4, primo periodo, 8, lettere a) e c) (recte: lettera b), 11, ultimo periodo, e 13; 76, comma 1, lettere e) e g).
 3.- L'art. 69, comma 3, dispone, per quanto interessa in considerazione del tenore della censura formulata, che l'accertamento della morte è effettuato su richiesta dell'ufficiale di stato civile.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>I commi 2, primo periodo, e 4 dell'art. 73 statuiscono che le autorizzazioni, rispettivamente, alla dispersione delle ceneri e, in caso di comprovata insufficienza delle sepolture, alla cremazione dei cadaveri inumati da almeno dieci anni o tumulati da almeno venti anni siano rilasciate dall'ufficiale di stato civile.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Queste norme violerebbero, ad avviso del Presidente del Consiglio dei ministri, l'art. 117, secondo comma, lettera i), Cost., in relazione alla materia «stato civile», attribuendo agli ufficiali dello stato civile compiti ulteriori rispetto a quelli previsti dalla normativa statale.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>1.- L'art. 71, comma 3 - stabilendo che, a seguito di interventi chirurgici, il cittadino può decidere di donare parti anatomiche per finalità di studio, ricerca o insegnamento -, lederebbe, secondo il ricorrente, l'art. 117, secondo comma, lettera l), Cost., in relazione alla materia «ordinamento civile».
 3.2.- Nel corso del giudizio, l'art. 17 della legge della Regione Lombardia 6 agosto 2019, n. 15 (Assestamento al bilancio 2019-2021 con modifiche di leggi regionali), per quanto qui interessa, da un lato, ha soppresso il primo periodo dell'art. 73, comma 2, della legge reg. Lombardia n. 33 del 2009 e, al comma 3 del precedente art. 69, le parole «su richiesta dell'ufficiale di stato civile»; dall'altro, ha abrogato il comma 3 dell'art. 71 e il comma 4 dell'art. 73 della legge appena menzionata.
 Conseguentemente il Presidente del Consiglio dei ministri ha rinunciato alla impugnazione di queste norme.
 Poiché la resistente ha accettato tale rinuncia parziale, va dichiarata, limitatamente alle questioni di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge reg. Lombardia n. 4 del 2019, nelle parti in cui introduce nella legge reg. Lombardia n. 33 del 2009 gli artt. 69, comma 3, 71, comma 3, e 73, commi 2, primo periodo, e 4, l'estinzione del processo, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/192/!main">sentenze n. 192 del 2019</ref> e n. 127 del 2018).</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>- Le restanti questioni hanno ad oggetto gli artt. 70-bis, 72, comma 1, 74, comma 1, lettera e), 74-bis, 75, commi 4, primo periodo, 8, lettere a) e c) (recte: lettera b), 11, ultimo periodo, e 13, 76, comma 1, lettere e) e g), della legge reg. Lombardia n. 33 del 2009, come introdotti dall'art. 1, comma 1, lettera b), della legge reg. Lombardia n. 4 del 2019.
 Queste disposizioni disciplinano: le «case funerarie» (art. 70-bis); il trasporto delle salme (art. 72, comma 1); i trattamenti di tanatocosmesi (art. 74, comma l, lettera e); una particolare tipologia di impresa che svolge attività funebre, ovvero il «centro servizi» (art. 74-bis); la devoluzione della gestione e della manutenzione dei cimiteri anche a soggetti privati (art. 75, comma 4, primo periodo); la costruzione e l'uso di aree e spazi per la sepoltura di animali d'affezione (art. 75, comma 8, lettera a); la costruzione di cappelle private al di fuori dei cimiteri (artt. 75, comma 8, lettera b, e 76, comma l, lettera g); l'autorizzazione alla soppressione dei cimiteri (art. 75, comma 11, ultimo periodo); la possibilità di  deporre nel loculo del defunto o nella tomba di famiglia, in teca separata e previa cremazione, i resti degli animali di affezione (art. 75, comma 13); i «loculi areati» (art. 76, comma l, lettera e).
 Secondo il Presidente del Consiglio dei ministri, tali norme violerebbero l'art. 117, terzo comma, Cost., in relazione alla materia «tutela della salute».</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>1.- È pregiudiziale l'esame dell'eccezione d'inammissibilità sollevata in limine dalla Regione, la quale si duole della omessa individuazione delle disposizioni statali violate e dei principi fondamentali da queste dettati e in ipotesi pregiudicati dalle norme impugnate.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'eccezione è fondata, per le ragioni di seguito precisate.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'Avvocatura generale dello Stato - che in sostanza si limita a una mera riproduzione del contenuto della delibera autorizzativa alla proposizione del ricorso - sostiene, in particolare, che le norme in parola contrastino con i principi fondamentali asseritamente posti dal <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990-09-10/285/!main">decreto del Presidente della Repubblica 10 settembre 1990, n. 285</ref> (Approvazione del regolamento di polizia mortuaria), in quanto alcune di esse introdurrebbero fattispecie non contemplate da tale regolamento, mentre altre detterebbero una disciplina da questo difforme.
 Così prospettate, le censure risultano all'evidenza apodittiche, essendo prive di ogni supporto argomentativo in ordine alla premessa su cui riposano, ovvero alla asserita idoneità del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1990/285/!main">d.P.R. n. 285 del 1990</ref> ad assurgere al rango di normativa interposta, in grado, quindi, di dettare principi fondamentali vincolanti la potestà legislativa concorrente regionale.
 Va al riguardo rilevato che il suddetto regolamento di polizia mortuaria, emanato ai sensi dell'<ref href="/akn/it/act/regioDecreto/stato/1934-07-27/1265/!main#art_358">art. 358 del regio decreto 27 luglio 1934, n. 1265</ref> (Approvazione del testo unico delle leggi sanitarie), rientra, di per sé, tra le fonti normative secondarie cui, in quanto tali, «è inibita in radice la possibilità di vincolare l'esercizio della potestà legislativa regionale o di incidere su disposizioni regionali preesistenti (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2003/22/!main">sentenza n. 22 del 2003</ref>); e neppure i principî di sussidiarietà e adeguatezza possono conferire ai regolamenti statali una capacità che è estranea al loro valore, quella cioè di modificare gli ordinamenti regionali a livello primario» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2003/303/!main">sentenza n. 303 del 2003</ref>). Le norme regolamentari, infatti, non possono essere ascritte «all'area dei principi fondamentali» delle materie concorrenti, «in quanto la fonte regolamentare, anche in forza di quanto previsto dall'art. 117, sesto comma, Cost., sarebbe comunque inidonea a porre detti principi» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2011/92/!main">sentenza n. 92 del 2011</ref>) e, quindi, a vincolare il legislatore regionale (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2004/162/!main">sentenza n. 162 del 2004</ref>).
 D'altro canto è anche vero che questa Corte ha ritenuto che gli atti di normazione secondaria possano vincolare la potestà legislativa regionale, ma solo in ben circoscritte ipotesi, ovvero quando, «in settori squisitamente tecnici», intervengono a completare la normativa statale primaria (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/286/!main">sentenza n. 286 del 2019</ref>) e costituiscono «un corpo unico con la disposizione legislativa che li prevede e che ad essi affida il compito di individuare le specifiche tecniche che mal si conciliano con il contenuto di un atto legislativo e che necessitano di applicazione uniforme in tutto il territorio nazionale» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/69/!main">sentenza n. 69 del 2018</ref>). Unicamente in queste limitate ipotesi il mancato rispetto di atti di normazione secondaria, «nel caso si verta nelle materie di cui al terzo comma dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_117">art. 117 Cost.</ref> e qualora la norma interposta esprima principi fondamentali», può comportare «l'illegittimità costituzionale della norma censurata» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/11/!main">sentenza n. 11 del 2014</ref>).
 Il ricorso, invece, sul punto tace del tutto.
 L'Avvocatura generale dello Stato si limita difatti a evocare esclusivamente il regolamento di polizia mortuaria, senza dedurre alcunché in ordine alla possibilità di qualificarlo come norma interposta; addirittura nemmeno mai menziona, nell'atto introduttivo, la normativa primaria di cui esso è attuazione; non indica, infine, quali sarebbero i principi fondamentali deducibili dall'ipotetica normativa interposta.
  In tal modo non risulta assolto l'onere, che secondo il costante orientamento di questa Corte grava sul ricorrente laddove sia denunciata la violazione dell'art. 117, terzo comma, Cost., di indicare specificamente il principio o i principi fondamentali della materia asseritamente lesi (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/143/!main">is, sentenza n. 143 del 2020</ref>).</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>2.- Alla stregua delle considerazioni svolte, deve, in conclusione, essere dichiarata l'inammissibilità delle questioni di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge reg. Lombardia n. 4 del 2019, nelle parti in cui introduce nella legge reg. Lombardia n. 33 del 2009 gli artt. 70-bis; 72, comma 1; 74, comma 1, lettera e); 74-bis; 75, commi 4, primo periodo, 8, lettere a) e b), 11, ultimo periodo, e 13; 76, comma 1, lettere e) e g).</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 1) dichiara inammissibili le questioni di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge della Regione Lombardia 4 marzo 2019, n. 4, recante «Modifiche e integrazioni alla legge regionale 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità): abrogazione del Capo III "Norme in materia di attività e servizi necroscopici, funebri e cimiteriali" del Titolo VI e introduzione del Titolo VI-bis "Norme in materia di medicina legale, polizia mortuaria, attività funebre"» - nelle parti in cui introduce nella legge della Regione Lombardia 30 dicembre 2009, n. 33 (Testo unico delle leggi regionali in materia di sanità), gli artt. 70-bis; 72, comma 1; 74, comma 1, lettera e); 74-bis; 75, commi 4, primo periodo, 8, lettere a) e b), 11, ultimo periodo, e 13; 76, comma 1, lettere e) e g) -, promosse dal Presidente del Consiglio dei ministri, in riferimento all'art. 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, con il ricorso indicato in epigrafe;
 2) dichiara estinto il processo relativamente alle questioni di legittimità costituzionale dell'art. 1, comma 1, lettera b), della legge reg. Lombardia n. 4 del 2019 - nelle parti in cui introduce nella legge reg. Lombardia n. 33 del 2009 gli artt. 69, comma 3; 71, comma 3; 73, commi 2, primo periodo, e 4 -, promosse dal Presidente del Consiglio dei ministri, in riferimento all'art. 117, secondo comma, lettere i) ed l), Cost., con il ricorso indicato in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-07-09">9 luglio 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#lucaAntonini" as="#relatore">Luca ANTONINI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-07-30">30 luglio 2020</date>.
 Il Cancelliere
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
