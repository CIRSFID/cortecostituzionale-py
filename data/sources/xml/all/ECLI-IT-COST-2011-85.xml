<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2011</anno_pronuncia>
    <numero_pronuncia>85</numero_pronuncia>
    <ecli>ECLI:IT:COST:2011:85</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>DE SIERVO</presidente>
    <relatore_pronuncia>Giuseppe Tesauro</relatore_pronuncia>
    <redattore_pronuncia>Giuseppe Tesauro</redattore_pronuncia>
    <data_decisione>07/03/2011</data_decisione>
    <data_deposito>11/03/2011</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 18, comma 1, lettera r), della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), promosso dalla Corte di appello di Perugia nel procedimento penale a carico di V.F.C.J., con ordinanza del 23 febbraio 2010, iscritta al n. 259 del registro ordinanze 2010 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 38, prima serie speciale, dell'anno 2010.&#13;
 	Udito nella camera di consiglio del 9 febbraio 2011 il Giudice relatore Giuseppe Tesauro.</epigrafe>
    <testo>Ritenuto che la Corte di appello di Perugia, con ordinanza del 23 febbraio 2010, iscritta al r.o. n. 259 del 2010, ha sollevato, in riferimento agli artt. 3, 27, terzo comma, e 117, primo comma, della Costituzione, questione di legittimità costituzionale dell'art. 18, comma 1, lettera r), della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), nella parte in cui stabilisce che, «se il mandato d'arresto europeo è stato emesso ai fini della esecuzione di una pena o di una misura di sicurezza privative della libertà personale», la Corte di appello può disporre che tale pena o misura di sicurezza sia eseguita in Italia conformemente al diritto interno, soltanto «qualora la persona ricercata sia cittadino italiano»; &#13;
 	che il rimettente è investito di un procedimento a carico dell'imputata V.F.C.J., cittadina francese residente in Italia, attinta, unitamente al coniuge italiano, da mandato di arresto per l'esecuzione della sentenza, emessa dalla Corte di appello di Besançon (Francia), di condanna alla pena di anni due di reclusione per il delitto di cui gli artt. 313-1AL 2, 313-7, 313-8 del codice penale francese (corrispondente al reato di cui all'art. 640 del codice penale italiano), commesso nel novembre-dicembre 2004 in Francia (Jura);&#13;
 	che risulta agli atti come l'imputata sia stabilmente dimorante nel comune di Tuoro sul Trasimeno, unitamente alla propria famiglia: situazione questa comprovata dalla documentazione acquisita e, ciononostante, alla luce della citata legge n. 69 del 2005, occorrerebbe dare esecuzione alla consegna allo Stato richiedente;&#13;
 	che il giudice a quo deduce la violazione dell'art. 3 Cost. in quanto, sebbene la decisione quadro 2002/584/GAI dia una mera facoltà agli Stati membri della Unione europea di estendere le guarentigie eventualmente riconosciute ai propri cittadini anche agli stranieri residenti sul territorio; tuttavia, una volta introdotta tale parificazione per quanto riguarda il «MAE processuale» (art. 19, comma 1, lettera c), sarebbe del tutto illogico che tale parificazione non sia effettuata dall'art. 18, comma 1, lettera r) concernente il «MAE esecutivo» di una sentenza di condanna di uno Stato estero, che riserva al solo cittadino italiano il rifiuto della consegna; &#13;
 	che la norma impugnata violerebbe anche l'art. 27, terzo comma Cost., poiché un soggetto stabilmente residente sul territorio dello Stato, ove ha stabilito il centro dei propri interessi affettivi e lavorativi, sarebbe costretto ad espiare la pena inflittagli in un contesto territoriale a lui ormai estraneo, con pregiudizio di un futuro reinserimento sociale del condannato; &#13;
 	che la disposizione in esame si porrebbe altresì in contrasto con l'art. 117, primo comma, Cost., in quanto, nel prevedere il rifiuto di consegna per il solo cittadino italiano, non rispetterebbe i vincoli derivanti dall'ordinamento comunitario, in particolare dall'art. 4 punto 6 della decisione quadro 2002/584/GAI laddove non consente di differenziare, in tema di rifiuto della consegna, la posizione del cittadino da quella di residente non cittadino.&#13;
 	Considerato che la Corte di appello di Perugia, dubita, in riferimento agli artt. 3, 27, terzo comma, e 117, primo comma, della Costituzione, della legittimità costituzionale dell'art. 18, comma 1, lettera r), della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), nella parte in cui stabilisce che, «se il mandato d'arresto europeo è stato emesso ai fini della esecuzione di una pena o di una misura di sicurezza privative della libertà personale», la Corte di appello può disporre che tale pena o misura di sicurezza sia eseguita in Italia conformemente al diritto interno, soltanto «qualora la persona ricercata sia cittadino italiano»; &#13;
 	che questa Corte, con la sentenza n. 227 del 2010, successiva alla pubblicazione dell'ordinanza di rimessione, ha già dichiarato l'illegittimità costituzionale dell'art. 18, comma 1, lettera r), della legge n. 69 del 2005, nella parte in cui non prevede il rifiuto di consegna anche del cittadino di un altro Paese membro dell'Unione europea, che legittimamente ed effettivamente abbia residenza o dimora nel territorio italiano, ai fini dell'esecuzione della pena detentiva in Italia; &#13;
 	che, dunque, la questione va dichiarata manifestamente inammissibile, essendo venuto meno il limite alla possibilità del rifiuto di consegna, cui si riferisce la censura del rimettente (ordinanza n. 306 del 2010, nonché n. 415 e n. 269 del 2008, n. 290 e n. 34 del 2002, n. 575 del 2000). &#13;
 	Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 	dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 18, comma 1, lettera r), della legge 22 aprile 2005, n. 69 (Disposizioni per conformare il diritto interno alla decisione quadro 2002/584/GAI del Consiglio, del 13 giugno 2002, relativa al mandato d'arresto europeo e alle procedure di consegna tra Stati membri), sollevata, in riferimento agli artt. 3, 27, terzo comma, e 117, primo comma della Costituzione, dalla Corte di appello di Perugia, con l'ordinanza indicata in epigrafe.&#13;
 	Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 7 marzo 2011.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Giuseppe TESAURO, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria l'11 marzo 2011.&#13;
 Il Cancelliere&#13;
 F.to: MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
