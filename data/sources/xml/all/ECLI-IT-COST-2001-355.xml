<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>355</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:355</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/11/2001</data_decisione>
    <data_deposito>07/11/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI; Riccardo CHIEPPA; Gustavo ZAGREBELSKY; &#13;
Valerio ONIDA; Carlo MEZZANOTTE; Fernanda CONTRI; Guido NEPPI MODONA; &#13;
Piero Alberto CAPOTOSTI; Annibale MARINI; Franco BILE; Giovanni Maria &#13;
FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 37, comma 3, &#13;
della  legge della regione Marche 17 luglio 1996, n. 26 (Riordino del &#13;
servizio  sanitario  regionale),  promosso con ordinanza emessa il 26 &#13;
maggio  1999  dal Tribunale amministrativo regionale delle Marche sul &#13;
ricorso  proposto  da  Vera Serroni Laboratorio Analisi S.r.l. contro &#13;
l'ASL  n. 11  regione  Marche  ed altra, iscritta al 204 del registro &#13;
ordinanze 2000 e pubblicata nella Gazzetta Ufficiale della Repubblica &#13;
n. 20, 1ª serie speciale, dell'anno 2000. &#13;
    Visti  l'atto di costituzione di Serroni Vera Laboratorio Analisi &#13;
S.r.l. nonché l'atto di intervento della regione Marche; &#13;
    Udito  nell'udienza  pubblica  del  25  settembre 2001 il giudice &#13;
relatore Piero Alberto Capotosti; &#13;
    Udito l'avvocato Stefano Grassi per la regione Marche; &#13;
    Ritenuto  che il Tribunale amministrativo regionale delle Marche, &#13;
adito  da  un  laboratorio  di  analisi  cliniche,  nella qualità di &#13;
struttura sanitaria accreditata dal servizio sanitario nazionale, per &#13;
ottenere  l'annullamento dell'atto con il quale la competente azienda &#13;
sanitaria  locale  aveva  stabilito  di  ammettere  al rimborso "solo &#13;
quelle  impegnative debitamente autorizzate dagli uffici competenti", &#13;
con  ordinanza  emessa  il  26  maggio 1999, depositata il 27 gennaio &#13;
2000, ha sollevato questione di costituzionalità dell'art. 37, comma &#13;
3,  della  legge della regione Marche 17 luglio 1996, n. 26 (Riordino &#13;
del  servizio  sanitario  regionale), in relazione agli articoli 97 e &#13;
117 della Costituzione; &#13;
        che  l'azienda  sanitaria  locale,  secondo  quanto espone il &#13;
giudice  a  quo a seguito del mancato raggiungimento dell'accordo con &#13;
il  laboratorio  di analisi ricorrente sull'importo delle prestazioni &#13;
da   rendere   al  servizio  sanitario  regionale,  ha  applicato  la &#13;
disposizione  regionale  impugnata  sul  punto  in cui questa rinvia, &#13;
"fino  alla  definizione degli accordi previsti dall'art. 5, comma 4" &#13;
della  stessa legge regionale n. 26 del 1996, all'art. 19 della legge &#13;
statale   11   marzo   1988,   n. 67,  ed  al  connesso  sistema  del &#13;
convenzionamento  del  servizio  sanitario nazionale con le strutture &#13;
sanitarie  private,  che  prevede  la necessità per gli assistiti di &#13;
munirsi  di  provvedimento  autorizzatorio  per  la  fruizione  delle &#13;
prestazioni dei presidi sanitari privati; &#13;
        che,  ad  avviso del Tribunale amministrativo regionale delle &#13;
Marche,  la disposizione regionale, "reintroducendo, ancorché in via &#13;
provvisoria  (...)  l'obbligo  del  rilascio di un'autorizzazione per &#13;
accedere alla struttura privata", si pone in contrasto con l'art. 117 &#13;
della   Costituzione,  in  quanto  viola  i  principi  dettati  dalla &#13;
legislazione  statale nella materia dei rapporti fra presidi sanitari &#13;
pubblici  e  privati, desumibili in particolare dall'art. 8, comma 5, &#13;
del  decreto  legislativo  30 dicembre  1992, n. 502, (Riordino della &#13;
disciplina  in  materia sanitaria, a norma dell'art. 1 della legge 23 &#13;
ottobre  1992,  n. 421) che ha sostituito alle precedenti convenzioni &#13;
con  le  strutture private il sistema dei rapporti di accreditamento, &#13;
caratterizzato,  a  suo  avviso,  dalla  posizione  di  parità delle &#13;
strutture  sanitarie  private  e  pubbliche e dal principio di libera &#13;
scelta fra di esse dell'assistito; &#13;
        che  l'art. 6 della legge 23 dicembre 1994, n. 724, abrogando &#13;
espressamente   il   principio  del  carattere  soltanto  integrativo &#13;
dell'intervento   delle  strutture  sanitarie  private  nel  sistema, &#13;
avrebbe,  secondo  il  rimettente,  eliminato  ogni residuo limite al &#13;
principio  di  parità fra strutture pubbliche e private, e che anche &#13;
l'art. 2, comma 8, della legge 28 dicembre 1995, n. 549, introducendo &#13;
la  necessità di una contrattazione con le strutture private al fine &#13;
di  stabilire  la quantità presunta e la tipologia delle prestazioni &#13;
sanitarie erogabili, avrebbe comunque mantenuto ferma la "facoltà di &#13;
libera    scelta",    indipendente    e   preliminare   rispetto   al &#13;
perfezionamento degli accordi stessi; &#13;
        che   la  stessa  disciplina  impugnata  violerebbe  altresì &#13;
l'art. 97  della  Costituzione,  disponendo un "filtro all'accesso di &#13;
strutture    già    accreditate",    che    "costituisce   in   mano &#13;
all'amministrazione  sanitaria  un'arma  per  imporre  alle strutture &#13;
private  le  condizioni  contrattuali  che  vorrà  determinare a suo &#13;
piacimento",  con  violazione dei principi di imparzialità e di buon &#13;
andamento dell'amministrazione; &#13;
        che   si   è   costituita   la   regione  Marche,  deducendo &#13;
l'infondatezza  della  questione,  poiché  il rimettente non avrebbe &#13;
considerato  la  nuova  disciplina dell'accreditamento introdotta dal &#13;
decreto   legislativo   19   giugno   1999,   n. 229  (Norme  per  la &#13;
razionalizzazione   del   servizio   sanitario   nazionale,  a  norma &#13;
dell'art. 1  della  legge 30 novembre 1998, n. 419) nell'ambito di un &#13;
sistema  articolato di autorizzazione e di accreditamento dei presidi &#13;
sanitari  e  di  negoziazione  fra  di  essi ed il servizio sanitario &#13;
nazionale,  da  cui  deriverebbe  il  principio  che "il diritto alla &#13;
libera  scelta  da  parte  dell'utente  non  è  (...)  automatico ed &#13;
incondizionato,  ma  è  subordinato  al  duplice  requisito  che  le &#13;
strutture  che erogano le prestazioni siano da un lato "accreditate,, &#13;
e dall'altro abbiano stipulato gli "accordi contrattuali,, previsti"; &#13;
        che,  ad  avviso  della  regione,  il  principio della libera &#13;
scelta   dell'utente   va   comunque   commisurato  all'interesse  al &#13;
contenimento   della  spesa  pubblica,  poiché  non  esisterebbe  un &#13;
"principio  fondamentale"  che  assicuri  l'assoluta  prevalenza  del &#13;
diritto   di   libertà   di   scelta   del   privato  sull'interesse &#13;
dell'amministrazione  a ridurre, in maniera logica e responsabile, le &#13;
spese  sanitarie,  e  che  inoltre  la  norma costituirebbe esercizio &#13;
dell'autonomia      legislativa     della     regione     nell'ambito &#13;
dell'organizzazione  del  servizio  sanitario  e  di  definizione dei &#13;
criteri di finanziamento delle strutture; &#13;
        che  si  è  costituita  la  società ricorrente nel giudizio &#13;
principale,   considerando   che   il   sistema  dell'accreditamento, &#13;
introdotto dal decreto legislativo n. 502 del 1992, sarebbe "divenuto &#13;
senza limitazioni" a seguito della soppressione, ad opera dell'art. 6 &#13;
della  legge  n. 724  del  1994,  del carattere meramente integrativo &#13;
delle  strutture  private  rispetto  a  quelle  pubbliche,  e  che di &#13;
conseguenza    la   "reviviscenza"   del   sistema   anteriore,   con &#13;
l'imposizione   di   un   "filtro"   all'accesso  di  strutture  già &#13;
accreditate, si porrebbe in contrasto con gli articoli 97 e 117 della &#13;
Costituzione. &#13;
    Considerato  che, successivamente all'ordinanza di rimessione, è &#13;
entrata  in vigore la legge della regione Marche 16 marzo 2000, n. 20 &#13;
(Disciplina   in  materia  di  autorizzazione  alla  realizzazione  e &#13;
all'esercizio,  accreditamento  istituzionale  e accordi contrattuali &#13;
delle  strutture sanitarie e socio-sanitarie pubbliche e private), la &#13;
quale   ha   dettato   una   nuova   disciplina   sia   del   sistema &#13;
dell'autorizzazione  e  dell'accreditamento istituzionale dei presidi &#13;
sanitari regionali, sia degli accordi contrattuali che questi debbono &#13;
stipulare per definire "la tipologia e la quantità delle prestazioni &#13;
erogabili  agli  utenti  del servizio sanitario regionale, nonché la &#13;
relativa  remunerazione  a  carico  del  servizio  sanitario medesimo &#13;
nell'ambito  di  livelli di spesa determinati in corrispondenza delle &#13;
scelte della programmazione regionale" (art. 2, comma 3); &#13;
        che  detta  disciplina  regionale sopravviene a quella recata &#13;
dall'art. 5,  comma 4, della legge regionale n. 26 del 1996, alla cui &#13;
mancata  applicazione  la norma impugnata condiziona espressamente il &#13;
rinvio,  sia pure in via transitoria, all'art. 19 della legge statale &#13;
n. 67  del  1988  ed  al  connesso  sistema  del convenzionamento del &#13;
servizio sanitario nazionale con le strutture sanitarie private; &#13;
        che  il  giudice  rimettente  non  ha potuto considerare tali &#13;
modificazioni della disciplina regionale in materia di accreditamento &#13;
istituzionale  e di accordi contrattuali con le istituzioni sanitarie &#13;
accreditate,  cosicché si rende necessario restituirgli gli atti del &#13;
processo  perché  possa valutare la permanenza della rilevanza della &#13;
questione nel giudizio principale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Ordina  la  restituzione  degli  atti al Tribunale amministrativo &#13;
regionale delle Marche. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 6 novembre 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                       Il redattore: Capotosti &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 7 novembre 2001. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
