<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>357</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:357</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ONIDA</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia>Fernanda Contri</redattore_pronuncia>
    <data_decisione>15/11/2004</data_decisione>
    <data_deposito>25/11/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 3, comma 1, lettera a), legge della Regione Friuli-Venezia Giulia 22 febbraio 2000, n. 2 (Disposizioni per la formazione del bilancio pluriennale ed annuale della Regione. Legge finanziaria 2000), promosso con ordinanza del 26 marzo 2003 dal Consiglio di Stato sul ricorso proposto da Kavo Franca contro il Comune di Trieste ed altra, iscritta al n. 1096 del registro ordinanze 2003 e pubblicata nella  Gazzetta Ufficiale della Repubblica n. 51, prima serie speciale, dell'anno 2003. &#13;
      Udito nella camera di consiglio del 13 ottobre 2004 il Giudice relatore Fernanda Contri. &#13;
    Ritenuto che il Consiglio di Stato, richiesto del parere sul ricorso straordinario al Presidente della Repubblica proposto da una cittadina italiana avverso il provvedimento del Comune di Trieste che le aveva negato il diritto all'assegno di maternità per il secondo figlio, per difetto dei presupposti fissati dalla legge della Regione Friuli-Venezia Giulia 22 febbraio 2000, n. 2 (Disposizioni per la formazione del bilancio pluriennale ed annuale della Regione. Legge finanziaria 2000), ha sollevato questione di legittimità costituzionale dell'art. 3, comma 1, lettera a), della indicata legge regionale, per contrasto con gli artt. 3, 30 e 31 della Costituzione; &#13;
    che il rimettente Consiglio, in via preliminare, afferma la sussistenza della propria legittimazione ad instaurare incidente di costituzionalità, riportando le argomentazioni contenute nell'ordinanza n. 534 del 27 marzo 2002, con cui la medesima sezione dello stesso Consiglio aveva sollevato altra questione di legittimità costituzionale; &#13;
    che nella richiamata ordinanza la tesi della legittimazione era sostenuta con riferimento alla decisione della Corte di giustizia delle Comunità europee del 16 ottobre 1997, emessa nelle cause riunite da C-69/96 a C-79/96, nella quale si affermava la natura giurisdizionale del Consiglio di Stato anche in sede consultiva, e con riferimento alla sentenza della Corte costituzionale n. 226 del 1976, che aveva ritenuto ammissibili questioni di legittimità costituzionale sollevate dalla Corte dei conti in sede di controllo; &#13;
    che, nella illustrazione del merito della questione, il rimettente riferisce che l'istanza di erogazione di un assegno “una tantum” per la nascita del secondo figlio, previsto dall'art. 3, comma 1, lettera a) della citata legge regionale a favore dei nuclei familiari “ove almeno uno dei coniugi sia cittadino italiano residente da almeno dodici mesi”, era stata respinta dal Comune di Trieste, in quanto il nucleo familiare della ricorrente non risultava fondato sul matrimonio ed era composto soltanto dalla medesima e da un altro figlio;  &#13;
    che il Consiglio di Stato, escludendo la possibilità di interpretazioni diverse della predetta disposizione, anche in considerazione della circostanza che il regolamento emanato dalla Regione Friuli-Venezia Giulia in attuazione della legge regionale n. 2 del 2000 definisce espressamente come nucleo familiare quello composto dai coniugi e dai figli conviventi alla data del parto, ritiene di dover sollevare questione di legittimità costituzionale dell'art. 3 della citata legge regionale, nella parte in cui dispone che l'erogazione delle prestazioni pecuniarie per il figlio successivo al primo postula il matrimonio dei genitori; &#13;
    che, ad avviso del rimettente, la norma impugnata sembrerebbe porsi in contrasto con il principio di eguaglianza, per la disparità di trattamento in danno dei figli di persone singole, risultando altresì violato il principio sancito dall'art. 30 della Costituzione, in quanto la disposizione in esame non consente l'erogazione di aiuti per soddisfare i bisogni dei figli nati fuori del matrimonio;  &#13;
    che sarebbe leso anche il principio di protezione della maternità e dell'infanzia, sancito dall'art. 31 della Costituzione, il quale non opera alcuna distinzione tra figli naturali e legittimi. &#13;
    Considerato che il profilo preliminare relativo alla legittimazione del Consiglio di Stato deve essere risolto in conformità alla recente sentenza n. 254 del 2004, con la quale questa Corte, reiterando, nell'esame di analogo aspetto, il proprio giudizio in ordine alla natura amministrativa del ricorso straordinario al Presidente della Repubblica, già più volte espresso in precedenti pronunce, ha conseguentemente affermato che il Consiglio di Stato opera in tale sede come organo non giurisdizionale, privo di legittimazione a sollevare questioni di legittimità costituzionale; &#13;
    che, pertanto, anche nel caso di specie deve essere dichiarata la inammissibilità della questione. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 3, comma 1, lettera a), della legge della Regione Friuli-Venezia Giulia 22 febbraio 2000, n. 2 (Disposizioni per la formazione del bilancio pluriennale ed annuale della Regione. Legge finanziaria 2000), sollevata, in riferimento agli artt. 3, 30 e 31 della Costituzione, dal Consiglio di Stato, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 15 novembre 2004. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Fernanda CONTRI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 25 novembre 2004. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
