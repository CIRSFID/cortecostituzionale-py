<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>330</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:330</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/2000</data_decisione>
    <data_deposito>21/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Massimo VARI, Riccardo CHIEPPA, Gustavo &#13;
 ZAGREBELSKY, Valerio ONIDA, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
 Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
 FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'articolo 39 del    &#13;
 decreto  legislativo  31  dicembre  1992,  n. 546  (Disposizioni  sul    &#13;
 processo  tributario  in attuazione della delega al Governo contenuta    &#13;
 nell'articolo  30 della legge 30 dicembre 1991, n. 413), promosso con    &#13;
 ordinanza  emessa  il  19  novembre 1999 dalla Commissione tributaria    &#13;
 provinciale  di  Verbania  sui  ricorsi  riuniti  proposti  da Vidoli    &#13;
 Claudio ed altro contro l'Ufficio delle entrate di Verbania, iscritta    &#13;
 al  n. 71  del  registro  ordinanze  2000 e pubblicata nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  n. 10,  prima serie speciale, dell'anno    &#13;
 2000.                                                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 6 luglio 2000 il giudice    &#13;
 relatore Annibale Marini.                                                &#13;
     Ritenuto  che  la Commissione tributaria provinciale di Verbania,    &#13;
 con   ordinanza   emessa  il  19  novembre  1999,  ha  sollevato,  in    &#13;
 riferimento  agli  artt. 3,  primo  comma,  e  76 della Costituzione,    &#13;
 questione  di  legittimità  costituzionale  dell'art. 39 del decreto    &#13;
 legislativo  31  dicembre  1992,  n. 546  (Disposizioni  sul processo    &#13;
 tributario   in   attuazione   della   delega  al  Governo  contenuta    &#13;
 nell'articolo  30  della legge 30 dicembre 1991, n. 413), nella parte    &#13;
 in   cui   non   prevede   né   "la   sospensione   necessaria   per    &#13;
 pregiudizialità", né la "sospensione su istanza delle parti";          &#13;
         che,  ad avviso della Commissione rimettente, la disposizione    &#13;
 denunciata,  limitando  la  sospensione  del processo tributario alle    &#13;
 sole  ipotesi  in  cui  sia stata presentata querela di falso o debba    &#13;
 essere  decisa  in  via  pregiudiziale una questione sullo stato o la    &#13;
 capacità delle persone, salvo che si tratti della capacità di stare    &#13;
 in  giudizio,  si  porrebbe  in  contrasto con l'art. 3, primo comma,    &#13;
 della Costituzione, per la irragionevole diversità di disciplina che    &#13;
 si  verrebbe in tal modo a determinare rispetto a quella prevista nel    &#13;
 processo   civile,   nel   quale   la   sospensione   necessaria  per    &#13;
 pregiudizialità  e  su  istanza  delle  parti sarebbe invece ammessa    &#13;
 dagli artt. 295 e 296 del codice di procedura civile;                    &#13;
         che,   a   parere  dello  stesso  giudice,  la  citata  norma    &#13;
 violerebbe  altresì  il criterio direttivo di cui all'art. 30, comma    &#13;
 1,   lettera   g),  della  legge  delega  30  dicembre  1991,  n. 413    &#13;
 (Disposizioni  per  ampliare  le basi imponibili, per razionalizzare,    &#13;
 facilitare e potenziare l'attività di accertamento; disposizioni per    &#13;
 la  rivalutazione  obbligatoria  dei  beni  immobili  delle  imprese,    &#13;
 nonché  per  riformare il contenzioso e per la definizione agevolata    &#13;
 dei   rapporti   tributari   pendenti;  delega  al  Presidente  della    &#13;
 Repubblica  per  la  concessione  di  amnistia  per  reati tributari;    &#13;
 istituzione  dei  centri  di assistenza fiscale e del conto fiscale),    &#13;
 che  sancisce  l'adeguamento  delle  norme  del processo tributario a    &#13;
 quelle del processo civile;                                              &#13;
         che  la  riduzione  della  mole  del  contenzioso  tributario    &#13;
 renderebbe  non  più  attuale  l'argomento con cui questa Corte - in    &#13;
 relazione all'esigenza di rapida definizione dei processi tributari -    &#13;
 ha  in  altra  occasione  ritenuto  non  fondata analoga questione di    &#13;
 legittimità costituzionale;                                             &#13;
         che  nel  giudizio  dinanzi  a questa Corte è intervenuto il    &#13;
 Presidente   del  Consiglio  dei  Ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura  generale dello Stato, chiedendo che venga dichiarata    &#13;
 l'infondatezza della questione in quanto già decisa, quanto al primo    &#13;
 profilo  con  la  sentenza  n. 31  del  1998,  quanto al secondo, con    &#13;
 l'ordinanza n. 8 del 1999.                                               &#13;
     Considerato  che  questa  Corte,  rispettivamente con la sentenza    &#13;
 n. 31  del  1998  e  con l'ordinanza n. 8 del 1999, ha dichiarato non    &#13;
 fondate  due  questioni  sostanzialmente identiche a quella sollevata    &#13;
 nel presente giudizio;                                                   &#13;
         che,  in  particolare,  quanto  alla  violazione dell'art. 3,    &#13;
 primo  comma,  della  Costituzione, si è affermato che la scelta del    &#13;
 legislatore   di   limitare   i  casi  di  sospensione  del  processo    &#13;
 tributario,  in  quanto  diretta  a  rendere più rapida e agevole la    &#13;
 definizione del processo medesimo, non risulta lesiva del criterio di    &#13;
 ragionevolezza (sentenza n. 31 del 1998);                                &#13;
         che,  con  riferimento alla violazione dell'art. 30, comma 1,    &#13;
 lettera  g),  della  legge n. 413 del 1991, questa Corte ha osservato    &#13;
 come il criterio direttivo di carattere generale in esso contenuto è    &#13;
 quello  dell'adeguamento,  e  non  dell'uniformità  delle  norme del    &#13;
 processo  tributario a quelle del processo civile (ordinanza n. 8 del    &#13;
 1999);                                                                   &#13;
         che  è  stato  altresì  evidenziato come ulteriore criterio    &#13;
 direttivo  di  carattere specifico previsto dal citato art. 30, comma    &#13;
 1,  lettera g), n. 3, quanto alla disciplina dei casi di sospensione,    &#13;
 interruzione  ed estinzione del processo tributario, sia quello della    &#13;
 sollecita  definizione  del  processo  medesimo  (ordinanza  n. 8 del    &#13;
 1999);                                                                   &#13;
         che,  dunque,  alla  luce  delle suesposte considerazioni, la    &#13;
 mancata  previsione  -  nella  norma  denunciata -  delle  ipotesi di    &#13;
 sospensione necessaria per pregiudizialità e su istanza delle parti,    &#13;
 di  cui  agli  artt. 295  e  296  cod. proc. civ., non viola la legge    &#13;
 delega   n. 413   del   1991  e,  conseguentemente,  l'art. 76  della    &#13;
 Costituzione;                                                            &#13;
         che, quanto all'asserita riduzione della mole del contenzioso    &#13;
 tributario,  si  tratta  di  una  circostanza di fatto che, in quanto    &#13;
 tale,  non  può  incidere  sull'esigenza,  evidenziata  nelle citate    &#13;
 pronunce, di assicurare una definizione quanto più rapida ed agevole    &#13;
 del processo tributario;                                                 &#13;
         che,  quindi, contrariamente a quanto affermato dal giudice a    &#13;
 quo  l'ordinanza  di  rimessione  non prospetta nuovi ed apprezzabili    &#13;
 profili rispetto a quelli già esaminati da questa Corte;                &#13;
         che,   pertanto,   la   questione   deve   essere  dichiarata    &#13;
 manifestamente infondata.                                                &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87  e  9,  secondo  comma,  delle  norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale dell'articolo 39 del decreto legislativo    &#13;
 31  dicembre  1992,  n. 546  (Disposizioni sul processo tributario in    &#13;
 attuazione  della  delega al Governo contenuta nell'articolo 30 della    &#13;
 legge  30  dicembre  1991,  n. 413),  sollevata,  in riferimento agli    &#13;
 artt. 3,  primo  comma,  e  76  della Costituzione, dalla Commissione    &#13;
 tributaria provinciale di Verbania con l'ordinanza in epigrafe.          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta l'11 luglio 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                          Il relatore: Marini                             &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 21 luglio 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
