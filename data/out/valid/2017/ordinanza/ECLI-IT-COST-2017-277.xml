<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-12-20/277/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-12-20/277"/>
          <FRBRalias value="ECLI:IT:COST:2017:277" name="ECLI"/>
          <FRBRdate date="2017-12-20" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="277"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-12-20/277/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-12-20/277/ita@"/>
          <FRBRdate date="2017-12-20" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-12-20/277/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-12-20/277/ita@.xml"/>
          <FRBRdate date="2017-12-20" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2017-12-12" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2017-12-20" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>inammissibile</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="GROSSI" href="/akn/ontology/person/it/GROSSI" showAs="GROSSI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="paoloGrossi" href="/akn/ontology/person/it/paoloGrossi" showAs="Paolo GROSSI"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2017</akn4cc:anno>
        <akn4cc:numero>277</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2017:277</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>GROSSI</akn4cc:presidente>
        <akn4cc:relatore>Nicolò Zanon</akn4cc:relatore>
        <akn4cc:data_decisione>2017-12-12</akn4cc:data_decisione>
        <akn4cc:data_deposito>2017-12-20</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>inammissibile</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#paoloGrossi">Paolo GROSSI</judge>; Giudici : <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>, <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della delibera del Consiglio dei ministri del 10 ottobre 2017 e dell'atto di Governo del 10 ottobre 2017, promosso dal CODACONS (Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori), da Bartolomeo Pepe, nella qualità di senatore, e da Giovanni Pignoloni, nella qualità di cittadino elettore, depositato in cancelleria il 17 ottobre 2017 ed iscritto al n. 4 del registro conflitto tra poteri 2017, fase di ammissibilità.
 Udito nella camera di consiglio del 12 dicembre 2017 il Giudice relatore Nicolò Zanon.
 Ritenuto che, con ricorso depositato il 17 ottobre 2017 ed iscritto al n. 4 del registro conflitti tra poteri 2017, il CODACONS (Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori), il senatore Bartolomeo Pepe e Giovanni Pignoloni nella qualità di cittadino elettore, hanno congiuntamente sollevato conflitto di attribuzione tra poteri dello Stato contro il Governo, in relazione alla delibera del Consiglio dei ministri del 10 ottobre 2017, con la quale è stato dato l'assenso a porre la questione di fiducia sull'approvazione, senza emendamenti né articoli aggiuntivi, degli artt. 1, 2 e 3 del testo unificato delle proposte di legge n. 2352 e abbinate A/R, recante «Modifiche al sistema di elezione della Camera dei deputati e del Senato della Repubblica. Delega al governo per la determinazione dei collegi elettorali uninominali e plurinominali», e all'atto con il quale, nella medesima giornata, il Governo, tramite il Ministro per i rapporti con il Parlamento, ha posto, alla Camera dei deputati, la questione di fiducia sui menzionati articoli;
 che, ad avviso dei ricorrenti, il Governo avrebbe in tal modo leso «le prerogative del Corpo elettorale connesse al diritto alla sovranità popolare, di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_1">art. 1 Cost.</ref>, al diritto di libertà ed eguaglianza, diritto alla libera espressione del proprio voto "personale ed eguale, libero e segreto" ex <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_48__para_2">art. 48, comma 2, Cost.</ref>; diritto alla vita politica della Nazione, nel legittimo esercizio della propria quota di sovranità popolare, così come previsto e garantito dagli artt. 1, 2, 3, 24, 48, 49, 51, 56, 71, 92, 111, 113, 117, 138 Cost. e dagli artt. 13 CEDU (Convenzione europea per la salvaguardia dei diritti dell'uomo), 3 Protocollo CEDU, entrambi ratificati in Italia con <ref href="/akn/it/act/legge/stato/1955-08-04/848/!main">legge 4 agosto 1955, n. 848</ref> [...]; nonché in violazione del combinato disposto dell'art. 49 e 116, comma 4 del Regolamento della Camera e dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_72">art. 72 Cost.</ref>»;
 che il CODACONS afferma di essere legittimato a sollevare conflitto di attribuzione tra poteri, poiché - rientrando tra le sue finalità statutarie la tutela dei diritti dei cittadini - esso agirebbe a tutela dei diritti dei medesimi, nella loro qualità di elettori e, dunque, quale rappresentante del corpo elettorale;
 che sarebbe legittimato a ricorrere anche Giovanni Pignoloni, nella qualità di elettore e rappresentante del corpo elettorale;
 che, infine, legittimato a proporre il ricorso sarebbe il senatore Bartolomeo Pepe, «in rappresentanza» del Senato della Repubblica, in quanto - a causa degli atti impugnati - egli vedrebbe «vanificato il proprio mandato elettivo» e le prerogative costituzionali garantitegli, quale parlamentare, dagli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_58">artt. 58</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_67">67</ref>, <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_70">70</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_72">72 della Costituzione</ref></mref>;
 che, quanto al profilo oggettivo, i ricorrenti asseriscono che il corpo elettorale - che esercita la propria funzione costituzionale eleggendo gli organi rappresentativi - sarebbe leso «nella propria quota di sovranità popolare», laddove il Parlamento, chiamato ad approvare una legge elettorale, non possa «liberamente e autonomamente discutere» a causa della decisione del Governo di ricorrere al voto di fiducia;
 che, in particolare, quest'ultimo, costringendo la maggioranza parlamentare ad approvare o a respingere l'articolo su cui è posta la fiducia, precluderebbe alle Camere la possibilità di esaminare e discutere i singoli aspetti del progetto di legge, con conseguente lesione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_72">art. 72 Cost.</ref>, in base al quale la legge deve essere votata articolo per articolo e con votazione finale e, dunque, ad avviso dei ricorrenti, con piena consapevolezza da parte di ciascun parlamentare in ordine ad ogni contenuto del progetto di legge;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che il ricorso alla questione di fiducia pregiudicherebbe «la libertà del voto e la sovranità dei cittadini tutti», poiché - assumendo i ricorrenti che i partiti non possano sostituirsi al corpo elettorale e che l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_67">art. 67 Cost.</ref> presupponga l'esistenza di un mandato conferito direttamente dagli elettori agli eletti - la posizione della questione di fiducia da parte del Governo renderebbe il voto dei cittadini «né libero, né personale»;
 che, peraltro, il Governo non potrebbe porre la questione di fiducia sulla legge elettorale, poiché si tratterebbe di una legge che «neppure in astratto» sarebbe suscettibile di incidere sul suo indirizzo politico;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, infine, il Governo, ponendo la questione di fiducia su un progetto di legge in materia elettorale, avrebbe violato il «combinato disposto» degli artt. 49 e 116 del regolamento della Camera, «alla luce del dettato dell'ultimo comma dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_72">articolo 72 della Costituzione</ref>», in quanto - sempre ad avviso dei ricorrenti - dalle citate disposizioni regolamentari dovrebbe dedursi il divieto di porre la questione di fiducia anche sulle leggi, come quelle elettorali, sulle quali il voto segreto è disposto su richiesta e non è invece obbligatoriamente prescritto;
 che l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_72">art. 72 Cost.</ref> imporrebbe il ricorso alla normale procedura legislativa per l'approvazione delle leggi elettorali, le quali, proprio alla luce di tale disposizione costituzionale, godrebbero «di una copertura procedurale di rango costituzionale» che non potrebbe dirsi rispettata in presenza di una «coartazione» del dibattito parlamentare imposto dalla maggioranza;
 che, nel procedimento di approvazione della legge elettorale da cui ha avuto origine il presente conflitto, la violazione sarebbe stata, peraltro, duplice, poiché uno degli articoli su cui è stata posta la questione di fiducia contiene una delega al Governo per la definizione dei collegi elettorali, e le leggi di delega sono - al pari di quelle elettorali - soggette alla cosiddetta "riserva di assemblea" ai sensi dell'art. 72, comma quarto, Cost.;
 che, in data 27 novembre 2017, i ricorrenti hanno depositato memoria in cui ribadiscono il loro interesse a ricorrere, anche all'esito della promulgazione della nuova legge elettorale, aggiungendo che l'approvazione di tale legge in prossimità delle elezioni politiche sarebbe illegittima «anche perché espone l'Italia al rischio di una censura da parte della Corte Europea dei Diritti dell'Uomo» (è richiamata la sentenza della Corte europea dei diritti dell'uomo, 6 novembre 2012, Ekoglasnost contro Bulgaria).
 Considerato che, in questa fase del giudizio, la Corte è chiamata a deliberare, in camera di consiglio e senza contraddittorio, sulla sussistenza dei requisiti soggettivo e oggettivo prescritti dall'art. 37, primo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), ossia a decidere se il conflitto insorga tra organi competenti a dichiarare definitivamente la volontà del potere cui appartengono e per la delimitazione della sfera di attribuzioni delineata per i vari poteri da norme costituzionali;
 che il ricorso per conflitto è stato congiuntamente presentato da tre diversi soggetti, i quali lamentano la decisione del Governo di porre la questione di fiducia, presso la Camera dei deputati, sull'approvazione della legge elettorale;
 che ciascuno dei tre soggetti è dotato di una distinta qualificazione soggettiva e può, in astratto, lamentare la lesione di distinte attribuzioni;
 che la prospettazione dei ricorrenti è resa incerta dal carattere cumulativo e congiunto del ricorso e dalla circostanza che le censure in esso contenute sono presentate senza considerazione della diversità delle rispettive qualificazioni;
 che, in ogni caso, l'inammissibilità del ricorso emerge anche a prendere partitamente in considerazione la distinta posizione dei tre ricorrenti rispetto alla lesione lamentata;
 che, infatti, con riferimento all'associazione CODACONS (Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori) e al cittadino elettore, questa Corte ha già affermato che soggetti ed organi diversi dallo Stato-apparato possono essere parti di un conflitto tra poteri, ai sensi dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_134">art. 134 della Costituzione</ref> e del citato <ref href="/akn/it/act/legge/stato/1953/87/!main#art_37">art. 37 della l. n. 87 del 1953</ref>, solo se titolari di una «pubblica funzione costituzionalmente rilevante e garantita» (<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1978/17/!main">ordinanza n. 17 del 1978</ref>);
 che il CODACONS non è titolare di funzioni costituzionalmente rilevanti, bensì delle sole situazioni soggettive spettanti alle «organizzazioni proprie della società civile» (<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2016/256/!main">ordinanza n. 256 del 2016</ref>);
 che, inoltre, secondo costante giurisprudenza di questa Corte, nemmeno il singolo cittadino, seppure vanti la qualità di elettore, è investito di funzioni tali da legittimarlo a sollevare conflitto di attribuzione, non essendogli conferita, in quanto singolo, alcuna attribuzione costituzionalmente rilevante (<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2016/256/!main">ordinanze n. 256 del 2016</ref>, n. 121 del 2011, n. 85 del 2009, n. 434, n. 284 e n. 189 del 2008, n. 296 del 2006);
 che, pertanto, il ricorso promosso dal CODACONS e da Giovanni Pignoloni è inammissibile per carenza dei requisiti soggettivi, nessuno dei due ricorrenti potendo qualificarsi potere dello Stato ai sensi dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_134">art. 134 Cost.</ref>;
 che, peraltro, è insussistente anche il requisito oggettivo del conflitto, giacché gli atti che si innestano nel procedimento legislativo sono inidonei a ledere la sfera di soggetti estranei alle Camere (<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2011/121/!main">ordinanze n. 121 del 2011</ref>, n. 120 del 2009, n. 172 del 1997, n. 45 del 1983 e, con particolare riferimento alla posizione della questione di fiducia da parte del Governo, n. 44 del 1983);
 che, sotto questo punto di vista, è erronea la prospettazione adombrata dai ricorrenti, secondo i quali la posizione della questione di fiducia da parte del Governo lederebbe direttamente «la libertà di voto e la sovranità dei cittadini tutti», essendo semmai essa idonea, in astratto, a incidere sulle attribuzioni costituzionali dei membri del Parlamento, che rappresentano la Nazione senza vincolo di mandato (<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_67">art. 67 Cost.</ref>);
 che, per ragioni diverse da quelle fin qui esposte, nemmeno il senatore Bartolomeo Pepe è legittimato, nel caso di specie, a ricorrere per conflitto;
 che, infatti, impregiudicata restando la configurabilità di attribuzioni individuali di potere costituzionale per la cui tutela il singolo parlamentare sia legittimato a promuovere un conflitto fra poteri (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2001/225/!main">sentenza n. 225 del 2001</ref>; <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2016/149/!main">ordinanze n. 149 del 2016</ref>, n. 222 del 2009, n. 177 del 1998), nel caso di specie il senatore ricorrente pretende inammissibilmente di rappresentare - in un conflitto promosso contro il Governo - l'intero organo cui appartiene;
 che, in ogni caso, un membro del Senato della Repubblica non può lamentare la violazione del procedimento parlamentare svoltosi presso la Camera dei deputati, non riguardando tale procedimento alcuna competenza o prerogativa di un senatore, configurandosi perciò come meramente ipotetica la lamentata lesione e come preventivo il relativo conflitto;
 che, per le complessive ragioni illustrate, è inammissibile il ricorso promosso dal CODACONS, da Giovanni Pignoloni e dal senatore Bartolomeo Pepe.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara inammissibile il ricorso per conflitto di attribuzione tra poteri dello Stato, promosso dal CODACONS (Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori), dal senatore Bartolomeo Pepe e da Giovanni Pignoloni, nella qualità di cittadino elettore, nei confronti del Governo.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2017-12-12">12 dicembre 2017</date>.
 F.to:
 <signature><judge refersTo="#paoloGrossi" as="#presidente">Paolo GROSSI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#nicolòZanon" as="#relatore">Nicolò ZANON</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2017-12-20">20 dicembre 2017</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
