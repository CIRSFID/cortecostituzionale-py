<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>355</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:355</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/1995</data_decisione>
    <data_deposito>21/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  8,  comma  1,    &#13;
 lettera  a-bis),  e comma 2-bis, del decreto-legge 19 settembre 1992,    &#13;
 n. 384 (Misure urgenti in materia di  previdenza,  di  sanità  e  di    &#13;
 pubblico  impiego,  nonché  disposizioni  fiscali),  convertito, con    &#13;
 modificazioni, nella legge 14 novembre 1992,  n.  438,  promosso  con    &#13;
 ordinanza  emessa  l'11 febbraio 1994 dalla Commissione tributaria di    &#13;
 primo grado di Torino sul ricorso proposto da Roberto Castello contro    &#13;
 l'Intendenza di finanza di Torino, iscritta al  n.  58  del  registro    &#13;
 ordinanze 1995 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 7, prima serie speciale, dell'anno 1995;                              &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  12  luglio  1995  il  Giudice    &#13;
 relatore Massimo Vari;                                                   &#13;
    Ritenuto  che  la Commissione tributaria di primo grado di Torino,    &#13;
 con ordinanza dell'11 febbraio 1994 (r.o. n. 58 del 1995), emessa nel    &#13;
 corso di un giudizio  promosso  da  Roberto  Castello  nei  confronti    &#13;
 dell'Amministrazione   finanziaria,   ha   sollevato   questione   di    &#13;
 legittimità costituzionale dell'art. 8, comma 1, lettera  a-bis),  e    &#13;
 comma  2-bis,  del  decreto-legge  19  settembre 1992, n. 384 (Misure    &#13;
 urgenti in materia di previdenza, di sanità e di  pubblico  impiego,    &#13;
 nonché  disposizioni  fiscali), convertito, con modificazioni, nella    &#13;
 legge 14 novembre 1992, n. 438, in riferimento agli  artt.  3  e  53,    &#13;
 primo comma, della Costituzione;                                         &#13;
      che,  come  risulta dall'ordinanza stessa, il caso all'esame del    &#13;
 giudice a quo concerne  la  richiesta  di  restituzione  della  somma    &#13;
 versata per effetto delle disposizioni denunciate, che prevedono, per    &#13;
 l'anno  1992,  un'imposizione  straordinaria  a  carico delle persone    &#13;
 fisiche che possiedono motocicli di potenza superiore a 6  CV,  nella    &#13;
 misura del quintuplo delle tasse automobilistiche erariali, regionali    &#13;
 e relativa addizionale;                                                  &#13;
      che,  secondo  il  giudice remittente, le disposizioni stesse si    &#13;
 porrebbero in contrasto con gli artt. 3  e  53,  primo  comma,  della    &#13;
 Costituzione,  per  la loro irragionevolezza ed arbitrarietà, attesa    &#13;
 la  manifesta  disparità  di  trattamento  operata,  a   danno   dei    &#13;
 possessori  di  motocicli  di  potenza fiscale superiore a 6 CV (beni    &#13;
 sicuramente non di lusso), rispetto ad "altre categorie di beni quali    &#13;
 le imbarcazioni da diporto con motore di lunghezza oltre 12 ed  entro    &#13;
 15  metri,  nonché per la disparità di trattamento che deriva dalle    &#13;
 modalità di quantificazione dell'imposta stabilite dal  comma  2-bis    &#13;
 del   citato   art.   8,   modalità   che   colpiscono   in   misura    &#13;
 proporzionalmente maggiore i possessori di motocicli";                   &#13;
      che è intervenuto il Presidente  del  Consiglio  dei  ministri,    &#13;
 rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato,    &#13;
 chiedendo che la questione venga dichiarata infondata;                   &#13;
    Considerato che la questione è stata già esaminata  dalla  Corte    &#13;
 che,  con  ordinanza  n.  475  del 1994, l'ha ritenuta manifestamente    &#13;
 infondata,  in  quanto  è  riservata   alla   discrezionalità   del    &#13;
 legislatore  la determinazione degli indici di capacità contributiva    &#13;
 e della conseguente entità dell'onere tributario, salvo i  controlli    &#13;
 di  legittimità  sotto  il  profilo  della  palese  arbitrarietà  e    &#13;
 irrazionalità, che, nella specie, non possono reputarsi sussistenti;    &#13;
      che, pertanto, la questione, in mancanza di profili e  argomenti    &#13;
 nuovi,  atti  ad  indurre  in diverso avviso, deve anche questa volta    &#13;
 dichiararsi manifestamente infondata;                                    &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 8, comma 1, lettera a-bis), e  comma  2-bis,    &#13;
 del  decreto-legge  19  settembre  1992,  n.  384  (Misure urgenti in    &#13;
 materia di previdenza, di sanità  e  di  pubblico  impiego,  nonché    &#13;
 disposizioni  fiscali), convertito, con modificazioni, nella legge 14    &#13;
 novembre 1992, n. 438, sollevata, in riferimento agli artt.  3  e  53    &#13;
 della  Costituzione,  dalla  Commissione tributaria di primo grado di    &#13;
 Torino con l'ordinanza in epigrafe.                                      &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 12 luglio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                          Il redattore: VARI                              &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 21 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
