<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/93/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/93"/>
          <FRBRalias value="ECLI:IT:COST:2020:93" name="ECLI"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="93"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/93/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/93/ita@"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/93/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/93/ita@.xml"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-04-22" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-05-15" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>93</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:93</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Nicolò Zanon</akn4cc:relatore>
        <akn4cc:data_decisione>2020-04-22</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-05-15</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta infondatezza</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei giudizi di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2010-07-06/103/!main#art_1bis__para_2__point_e">art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103</ref> (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2010-08-04/127/!main">legge 4 agosto 2010, n. 127</ref>, nella parte in cui inserisce l'<ref href="/akn/it/act/decretoLegislativo/stato/2005-11-21/286/!main#art_7ter">art. 7-ter del decreto legislativo 21 novembre 2005, n. 286</ref> (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore), promossi dal Tribunale ordinario di Prato, con ordinanza del 5 marzo 2019, dal Giudice onorario di pace di Cagliari, con ordinanza del 19 dicembre 2018, e dalla Corte d'appello di Cagliari, con ordinanza del 27 febbraio 2019, rispettivamente iscritte ai numeri 112, 124 e 126 del registro ordinanze 2019 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 34 e n. 37, prima serie speciale, dell'anno 2019.
 Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Guna spa e della Prealpi spa, nonché gli atti d'intervento del Presidente del Consiglio dei ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito il Giudice relatore Nicolò Zanon nella camera di consiglio del 22 aprile 2020, svolta, ai sensi del decreto della Presidente della Corte del 24 marzo 2020, punto 1), lettera a);
 deliberato nella camera di consiglio del 22 aprile 2020.
 Ritenuto che il Tribunale ordinario di Prato, il Giudice onorario di pace di Cagliari e la Corte d'appello di Cagliari, con ordinanze di analogo tenore (rispettivamente iscritte ai numeri 112, 124 e 126 del registro ordinanze 2019), hanno sollevato, in riferimento all'art. 77, secondo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2010-07-06/103/!main#art_1bis__para_2__point_e">art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103</ref> (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2010-08-04/127/!main">legge 4 agosto 2010, n. 127</ref>, nella parte in cui inserisce l'<ref href="/akn/it/act/decretoLegislativo/stato/2005-11-21/286/!main#art_7ter">art. 7-ter del decreto legislativo 21 novembre 2005, n. 286</ref> (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore), che assegna al vettore, il quale ha svolto un servizio di trasporto su incarico di altro vettore, un'azione diretta per il pagamento del corrispettivo nei confronti di tutti coloro che hanno ordinato il trasporto;
 che, con riferimento al giudizio iscritto al r.o. n. 112 del 2019, la controversia innanzi al Tribunale di Prato ha ad oggetto l'opposizione promossa dalla società Guna spa contro il decreto ingiuntivo emesso su istanza della società Vetos srl, al fine di ottenere il pagamento del corrispettivo di prestazioni di autotrasporto di merci su strada per conto terzi;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che la pretesa di pagamento soddisfatta dal decreto ingiuntivo è avanzata dalla società Vetos srl allegando di avere eseguito prestazioni di trasporto come vettore su incarico della società Trans Vector 2 srl, a sua volta incaricata dalla mittente Guna spa, nei confronti della quale la ricorrente intende esercitare l'azione diretta di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2005/286/!main#art_7ter">art. 7-ter del d.lgs. n. 286 del 2005</ref>;
 che la società Guna spa ha proposto opposizione avverso il predetto decreto ingiuntivo allegando, in primo luogo, di avere stipulato un contratto di trasporto solo con altra società (Pharmavector srl), poi dichiarata fallita, e, in secondo luogo, eccependo l'illegittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/2005/286/!main#art_7ter">art. 7-ter del d.lgs. n. 286 del 2005</ref>, per violazione dell'art. 77, secondo comma, Cost.;
 che il Tribunale di Prato ritiene rilevante la questione, in quanto la società creditrice, nel giudizio a quo, avrebbe esercitato proprio l'azione diretta prevista dalla disposizione censurata, procedendo nei confronti del committente originario, il quale avrebbe incaricato del trasporto, come vettore, la società Pharmavector srl, che a sua volta avrebbe dato incarico alla società Trans Vector 2 srl, la quale infine avrebbe dato mandato di eseguire il trasporto alla società Vetos srl;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, quanto alla non manifesta infondatezza, il rimettente ricorda che l'<ref href="/akn/it/act/decretoLegislativo/stato/2005/286/!main#art_7ter">art. 7-ter del d.lgs. n. 286 del 2005</ref> è stato introdotto dalla <ref href="/akn/it/act/legge/stato/2010/127/!main">legge n. 127 del 2010</ref>, di conversione del <ref href="/akn/it/act/decretoLegge/stato/2010/103/!main">d.l. n. 103 del 2010</ref>, che in origine non conteneva una simile previsione normativa;
 che, infatti, il <ref href="/akn/it/act/decretoLegge/stato/2010/103/!main">d.l. n. 103 del 2010</ref>, titolato «Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo», sarebbe stato emanato al solo scopo - esplicitato nel relativo preambolo - di completare la procedura di dismissione dell'intero capitale sociale della società Tirrenia di Navigazione spa e, nel contempo, di assicurare la regolarità del servizio pubblico di trasporto marittimo, con particolare riguardo al periodo di picco del traffico estivo;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che la legge di conversione del <ref href="/akn/it/act/decretoLegge/stato/2010/103/!main">d.l. n. 103 del 2010</ref>, oltre a modificare il titolo del testo normativo - da «Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo» a «Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti» - avrebbe invece introdotto una serie di disposizioni attinenti anche all'attività di autotrasporto di merci per conto di terzi, tra cui quella censurata, giudicata «completamente scollegata dai contenuti già disciplinati dal decreto-legge, riguardanti esclusivamente la necessità di assicurare la regolarità del servizio pubblico di trasporto marittimo in un arco temporale limitato»;
 che il rimettente richiama la giurisprudenza costituzionale secondo cui la legge di conversione deve avere un contenuto omogeneo a quello del decreto-legge (sono citate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/32/!main">sentenze n. 32 del 2014</ref> e n. 22 del 2012, nonché l'<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2013/34/!main">ordinanza n. 34 del 2013</ref>), sicché l'inclusione di emendamenti e articoli aggiuntivi che non siano attinenti alla materia oggetto del decreto-legge, o alle finalità di quest'ultimo, determinerebbe un vizio della legge di conversione in parte qua;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, in definitiva, per il giudice a quo, dalla giurisprudenza costituzionale citata si trarrebbe la conclusione che la violazione dell'art. 77, secondo comma, Cost. per difetto di omogeneità si determina quando le disposizioni aggiunte siano totalmente «estranee» o addirittura «intruse», cioè tali da interrompere ogni correlazione tra il decreto-legge e la legge di conversione (sono citate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2017/169/!main">sentenze n. 169 del 2017</ref>, n. 145 del 2015 e n. 251 del 2014), ciò che sarebbe avvenuto, appunto, nel caso in esame;
 che si è costituita la società Guna spa, parte del giudizio principale, aderendo alle argomentazioni esposte nell'ordinanza di rimessione a sostegno della dichiarazione di illegittimità costituzionale della disposizione censurata;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, quanto al giudizio iscritto al r.o. n. 124 del 2019, il Giudice onorario di pace di Cagliari espone che la controversia nasce dall'opposizione proposta dalla società KWS Italia spa avverso il decreto ingiuntivo emesso in favore della società Logistica Mediterranea spa, per il pagamento di somme a titolo di corrispettivo per i servizi di trasporto eseguiti su incarico della società Topco spa;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che l'opposta società Logistica Mediterranea spa avrebbe agito direttamente nei confronti dell'opponente società KWS Italia spa, ossia della committente dei trasporti dalla prima eseguiti su incarico del vettore principale Topco spa, ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2005/286/!main#art_7ter">art. 7-ter del d.lgs. n. 286 del 2005</ref>, con conseguente rilevanza della questione di legittimità costituzionale sollevata, dalla cui definizione dipenderebbe l'accertamento della legittimazione passiva della società KWS Italia spa, non risultando che questa sia legata alla società Logistica Mediterranea spa «da un rapporto contrattuale che avrebbe, comunque, legittimato l'azione diretta nei suoi confronti»;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, in punto di non manifesta infondatezza, il Giudice onorario di pace di Cagliari espone argomentazioni del tutto coincidenti con quelle dell'ordinanza del Tribunale di Prato illustrate in precedenza;
 che, per quanto concerne il giudizio iscritto al r.o. n. 126 del 2019, la Corte d'appello di Cagliari riferisce di essere chiamata a decidere sull'impugnazione proposta avverso una sentenza pronunciata dal Tribunale ordinario di Oristano;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che quest'ultimo ha rigettato l'opposizione avanzata dalla società Prealpi spa avverso il decreto ingiuntivo emesso in favore della società AF Sardegna srl, per il pagamento di somme pretese - in forza dell'azione diretta prevista dall'<ref href="/akn/it/act/decretoLegislativo/stato/2005/286/!main#art_7ter">art. 7-ter del d. lgs. 286 del 2005</ref> - a titolo di corrispettivo per i trasporti eseguiti per conto della società Trasporti Brianese C. &amp; G. srl, a tanto, a sua volta, incaricata dalla società Prealpi spa;
 che avverso la sentenza di primo grado ha proposto appello la società Prealpi spa, adducendo motivi attinenti al merito della pretesa avanzata con il decreto ingiuntivo opposto e riproponendo una eccezione d'illegittimità costituzionale della disposizione censurata, disattesa dal giudice di prime cure e attinente, tra l'altro, alla violazione dell'art. 77, secondo comma, Cost.
 che la Corte d'appello rimettente evidenzia come la pretesa creditoria della società AF Sardegna srl nei confronti della società appellante Prealpi spa si fonda proprio sulla disposizione censurata, da ciò derivando la rilevanza della questione sollevata;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, sotto il profilo della non manifesta infondatezza, la Corte d'appello di Cagliari menziona la medesima giurisprudenza costituzionale richiamata nelle ordinanze iscritte ai numeri r.o. n. 112 e n. 124 del 2019, in precedenza illustrate, e afferma che l'applicazione dei principi in essa enunciati «dovrebbe portare a far ritenere insussistente il requisito dell'omogeneità e del nesso finalistico del censurato articolo rispetto alle norme originarie contenute nel decreto-legge», per le medesime ragioni evidenziate nelle ordinanze di rimessione appena citate;
 che nel giudizio si è costituita la società Prealpi spa, parte appellante nel giudizio principale, aderendo alle argomentazioni proposte nell'ordinanza di rimessione a sostegno della dichiarazione di illegittimità costituzionale della disposizione censurata;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che in tutti i giudizi è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, depositando atti di analogo tenore e concludendo per la non fondatezza della questione, non potendosi definire la disposizione censurata "totalmente estranea" o addirittura "intrusa";
 che, in prossimità dell'udienza pubblica, la società Guna spa ha depositato ulteriore memoria, con la quale - pur prendendo atto della sentenza della <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/226/!main">Corte costituzionale n. 226 del 2019</ref>, medio tempore intervenuta - ha riproposto gli argomenti articolati nell'atto d'intervento.
 Considerato che il Tribunale ordinario di Prato, il Giudice onorario di pace di Cagliari e la Corte d'appello di Cagliari, con ordinanze di analogo tenore, hanno sollevato, in riferimento all'art. 77, secondo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2010-07-06/103/!main#art_1bis__para_2__point_e">art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103</ref> (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2010-08-04/127/!main">legge 4 agosto 2010, n. 127</ref>, nella parte in cui inserisce l'art. 7-ter nel <ref href="/akn/it/act/decretoLegislativo/stato/2005-11-21/286/!main">decreto legislativo 21 novembre 2005, n. 286</ref> (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, in considerazione dell'identità della disposizione censurata e del parametro evocato, i giudizi vanno riuniti per essere definiti con unica decisione;
 che, a parere dei rimettenti, la disposizione denunciata, aggiunta in sede di conversione, sarebbe contrastante con l'art. 77, secondo comma, Cost., presentando un contenuto disomogeneo rispetto a quello dell'originario <ref href="/akn/it/act/decretoLegge/stato/2010/103/!main">d.l. n. 103 del 2010</ref>;
 che, infatti, l'<ref href="/akn/it/act/decretoLegislativo/stato/2005/286/!main#art_7ter">art. 7-ter del d.lgs. n. 286 del 2005</ref> - introducendo l'azione diretta del vettore che ha svolto un servizio di trasporto su incarico di altro vettore nei confronti di tutti coloro che hanno ordinato il trasporto, con riferimento all'attività di autotrasporto di merci per conto di terzi - risulterebbe disposizione «completamente scollegata dai contenuti già disciplinati dal decreto-legge, riguardanti esclusivamente la necessità di assicurare la regolarità del servizio pubblico di trasporto marittimo» (così l'ordinanza iscritta al r.o. n. 112 del 2019), venendo dunque a mancare «omogeneità di contenuti e di finalità tra la disposizione introdotta in sede di conversione e le disposizioni originariamente contenute nel decreto legge» (ordinanza iscritta al r.o. n. 124 del 2019), con conseguente insussistenza del «requisito dell'omogeneità e del nesso finalistico del censurato articolo rispetto alle norme originarie contenute nel decreto-legge» (ordinanza iscritta al r.o. n. 126 del 2019);
 che la questione in esame, sollevata in forza di censure del tutto corrispondenti a quelle ora dedotte, è già stata dichiarata non fondata con la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/226/!main">sentenza n. 226 del 2019</ref>, successiva alle ordinanze di rimessione;
 che, in tale pronuncia, questa Corte - nel riaffermare il principio secondo cui la legge di conversione rappresenta una legge funzionalizzata e specializzata, che non può aprirsi a oggetti eterogenei rispetto a quelli originariamente contenuti nell'atto con forza di legge (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/181/!main">sentenza n. 181 del 2019</ref>) - ha anche ribadito che un difetto di omogeneità, in violazione dell'art. 77, secondo comma, Cost., si determina solo quando le disposizioni aggiunte in sede di conversione sono totalmente «estranee» o addirittura «intruse», cioè tali da interrompere ogni correlazione tra il decreto-legge e la legge di conversione (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/251/!main">sentenza n. 251 del 2014</ref>);
 che, pertanto, solo la palese «estraneità delle norme impugnate rispetto all'oggetto e alle finalità del decreto-legge» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2012/22/!main">sentenza n. 22 del 2012</ref>) oppure la «evidente o manifesta mancanza di ogni nesso di interrelazione tra le disposizioni incorporate nella legge di conversione e quelle dell'originario decreto-legge» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/154/!main">sentenza n. 154 del 2015</ref>) possono inficiare di per sé la legittimità costituzionale della norma introdotta con la legge di conversione (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/181/!main">sentenza n. 181 del 2019</ref>);
 che, sempre nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/226/!main">sentenza n. 226 del 2019</ref>, si è chiarito che la coerenza delle disposizioni aggiunte in sede di conversione con la disciplina originaria può essere valutata sia dal punto di vista oggettivo o materiale, sia dal punto di vista funzionale e finalistico (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/32/!main">sentenza n. 32 del 2014</ref>), come del resto confermato anche dalla giurisprudenza successiva (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/247/!main">sentenza n. 247 del 2019</ref> e <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2019/274/!main">ordinanza n. 274 del 2019</ref>);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che la disposizione censurata, relativa alla stessa «materia» sulla quale incide l'atto con forza di legge da convertire, cioè il trasporto, prevede un intervento a favore delle imprese di autotrasporto (in particolare dei vettori finali, nell'ambito del trasporto di merci su strada), e perciò condivide con il decreto-legge originario la "comune natura" (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2014/251/!main">sentenza n. 251 del 2014</ref>) di misura finalizzata alla risoluzione di una situazione di crisi, sicché, sia dal punto di vista oggettivo o materiale, sia dal punto di vista funzionale e finalistico, deve essere esclusa l'evidente o manifesta mancanza di un nesso di interrelazione tra le disposizioni incorporate nella legge di conversione e quelle dell'originario decreto-legge;
 che, in base a questi criteri di valutazione, la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/226/!main">sentenza n. 226 del 2019</ref> ha affermato l'insussistenza di elementi sufficienti a sostenere la palese estraneità, o addirittura il carattere intruso, della disposizione censurata;
 che la questione oggi proposta, non aggiungendo né argomenti, né profili nuovi rispetto a quelli già esaminati, deve essere dichiarata manifestamente infondata.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 2, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegge/stato/2010-07-06/103/!main#art_1bis__para_2__point_e">art. 1-bis, comma 2, lettera e), del decreto-legge 6 luglio 2010, n. 103</ref> (Disposizioni urgenti per assicurare la regolarità del servizio pubblico di trasporto marittimo ed il sostegno della produttività nel settore dei trasporti), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2010-08-04/127/!main">legge 4 agosto 2010, n. 127</ref>, nella parte in cui inserisce l'<ref href="/akn/it/act/decretoLegislativo/stato/2005-11-21/286/!main#art_7ter">art. 7-ter del decreto legislativo 21 novembre 2005, n. 286</ref> (Disposizioni per il riassetto normativo in materia di liberalizzazione regolata dell'esercizio dell'attività di autotrasportatore), sollevata, in riferimento all'art. 77, secondo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, dal Tribunale ordinario di Prato, dal Giudice onorario di pace di Cagliari e dalla Corte d'appello di Cagliari, con le ordinanze indicate in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-04-22">22 aprile 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#nicolòZanon" as="#relatore">Nicolò ZANON</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-05-15">15 maggio 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
