<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/24/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/24"/>
          <FRBRalias value="ECLI:IT:COST:1963:24" name="ECLI"/>
          <FRBRdate date="1963-03-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="24"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/24/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/24/ita@"/>
          <FRBRdate date="1963-03-16" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/24/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1963-03-16/24/ita@.xml"/>
          <FRBRdate date="1963-03-16" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1963-03-05" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1963-03-16" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="AMBROSINI" href="/akn/ontology/person/it/AMBROSINI" showAs="AMBROSINI"/>
        <TLCPerson eId="gaspareAmbrosini" href="/akn/ontology/person/it/gaspareAmbrosini" showAs="GASPARE AMBROSINI"/>
        <TLCPerson eId="giuseppeCastelliAvolio" href="/akn/ontology/person/it/giuseppeCastelliAvolio" showAs="GIUSEPPE CASTELLI AVOLIO"/>
        <TLCPerson eId="antoninoPapaldo" href="/akn/ontology/person/it/antoninoPapaldo" showAs="ANTONINO PAPALDO"/>
        <TLCPerson eId="giovanniCassandro" href="/akn/ontology/person/it/giovanniCassandro" showAs="GIOVANNI CASSANDRO"/>
        <TLCPerson eId="biagioPetrocelli" href="/akn/ontology/person/it/biagioPetrocelli" showAs="BIAGIO PETROCELLI"/>
        <TLCPerson eId="antonioManca" href="/akn/ontology/person/it/antonioManca" showAs="ANTONIO MANCA"/>
        <TLCPerson eId="aldoSandulli" href="/akn/ontology/person/it/aldoSandulli" showAs="ALDO SANDULLI"/>
        <TLCPerson eId="giuseppeBranca" href="/akn/ontology/person/it/giuseppeBranca" showAs="GIUSEPPE BRANCA"/>
        <TLCPerson eId="micheleFragali" href="/akn/ontology/person/it/micheleFragali" showAs="MICHELE FRAGALI"/>
        <TLCPerson eId="costantinoMortati" href="/akn/ontology/person/it/costantinoMortati" showAs="COSTANTINO MORTATI"/>
        <TLCPerson eId="giuseppeVerzì" href="/akn/ontology/person/it/giuseppeVerzì" showAs="GIUSEPPE VERZÌ"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1963</akn4cc:anno>
        <akn4cc:numero>24</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1963:24</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>AMBROSINI</akn4cc:presidente>
        <akn4cc:relatore>Giuseppe Branca</akn4cc:relatore>
        <akn4cc:data_decisione>1963-03-05</akn4cc:data_decisione>
        <akn4cc:data_deposito>1963-03-16</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#gaspareAmbrosini">GASPARE AMBROSINI</judge>, Presidente - Prof. <judge refersTo="#giuseppeCastelliAvolio">GIUSEPPE CASTELLI AVOLIO</judge> - Prof. <judge refersTo="#antoninoPapaldo">ANTONINO PAPALDO</judge> - Prof. <judge refersTo="#giovanniCassandro">GIOVANNI 
 CASSANDRO</judge> - Prof. <judge refersTo="#biagioPetrocelli">BIAGIO PETROCELLI</judge> - Dott. <judge refersTo="#antonioManca">ANTONIO MANCA</judge> - Prof. <judge refersTo="#aldoSandulli">ALDO 
 SANDULLI</judge> - Prof. <judge refersTo="#giuseppeBranca">GIUSEPPE BRANCA</judge> - Prof. <judge refersTo="#micheleFragali">MICHELE FRAGALI</judge> - Prof. <judge refersTo="#costantinoMortati">COSTANTINO MORTATI</judge> - Prof. GIUSEPPE CHIARETTI - Dott. <judge refersTo="#giuseppeVerzì">GIUSEPPE VERZÌ</judge>, 
 Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha deliberato in camera di consiglio la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel  giudizio  di  legittimità  costituzionale dell'art. 10, n. 3,  
 della <ref href="/akn/it/act/legge/stato/1950-05-23/253/!main">legge 23 maggio 1950, n. 253</ref>, promosso con ordinanza emessa il 10  
 ottobre 1962 dal  Pretore  di  Montepulciano  nel  procedimento  civile  
 vertente  fra Giovannangela Secchi Tarugi e Gustavo Romani, iscritta al  
 n.  184  del  Registro  ordinanze  1962  e  pubblicata  nella  Gazzetta  
 Ufficiale della Repubblica n. 300 del 24 novembre 1962.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udita  nella  camera di consiglio del 5 marzo 1963 la relazione del  
 Giudice Giuseppe Branca;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Ritenuto  che  il  Pretore  di  Montepulciano,  con   la   suddetta  
 ordinanza,   ha  sollevato  questione  di  legittimità  costituzionale  
 dell'<ref href="/akn/it/act/legge/stato/1950-05-23/253/!main#art_10__point_3">art. 10, n. 3, della legge 23 maggio 1950, n. 253</ref>, in  riferimento  
 agli artt. 24, primo e secondo comma, e 102 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che in questa sede non vi è stata <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> di parti;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Considerato  che  la Corte costituzionale con la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1962/94/!main">sentenza n. 94 del  
 1962</ref>,  ha  dichiarato  non  fondata  la   questione   di   legittimità  
 costituzionale  in  riferimento  all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_24">art.  24  della  Costituzione</ref>  con  
 motivazione  che  vale  anche  in  riferimento   all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_102">art.   102   della  
 Costituzione</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, non essendo stati addotti nuovi e diversi motivi, la decisione  
 va confermata;</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara  la manifesta infondatezza della questione di legittimità  
 costituzionale dell'<ref href="/akn/it/act/legge/stato/1950-05-23/253/!main#art_10__point_3">art. 10, n. 3, della legge 23 maggio 1950, n.  253</ref>,  
 sollevata con l'ordinanza di cui in epigrafe.</block>
        <p>
          <docAuthority>Cosi deciso in Roma, in camera di consiglio, nella sede della Corte  
 costituzionale, Palazzo della Consulta, il 5 marzo 1963.</docAuthority>
        </p>
        <paragraph>
          <content>
            <p>GASPARE AMBROSINI - GIUSEPPE CASTELLI  
                                   AVOLIO  - ANTONINO PAPALDO - GIOVANNI  
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  
                                   ANTONIO   MANCA  -  ALDO  SANDULLI  -  
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  
                                   COSTANTINO    MORTATI    -   GIUSEPPE  
                                   CHIARELLI - GIUSEPPE  VERZÌ.</p>
          </content>
        </paragraph>
      </decision>
    </judgmentBody>
  </judgment>
</akomaNtoso>
