<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>310</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:310</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2001</data_decisione>
    <data_deposito>25/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di ammissibilità del conflitto tra poteri dello Stato &#13;
sorto a seguito della delibera della Camera dei deputati del 5 luglio &#13;
2000,   relativa   alla   insindacabilità  delle  opinioni  espresse &#13;
dall'on. Vittorio  Sgarbi  nei  confronti del dott. Agostino Cordova, &#13;
promosso  dal tribunale di Salerno, prima sezione penale, con ricorso &#13;
depositato  il 29 gennaio  2001  ed  iscritto  al n. 180 del registro &#13;
ammissibilità conflitti. &#13;
    Udito  nella  camera  di  consiglio del 23 maggio 2001 il giudice &#13;
relatore Massimo Vari. &#13;
    Ritenuto  che  -  con  ordinanza  del 15 gennaio 2001, emessa nel &#13;
corso  dell'udienza  dibattimentale  del giudizio penale a carico del &#13;
deputato  on. Vittorio Sgarbi, per il reato di cui agli artt. 13 e 21 &#13;
della  legge  8 febbraio  1948, n. 47, in relazione all'art. 30 della &#13;
legge  6 agosto 1990, n. 223 - il tribunale di Salerno, prima sezione &#13;
penale,  in  composizione  monocratica,  ha  sollevato  conflitto  di &#13;
attribuzione  fra  poteri  dello Stato nei confronti della Camera dei &#13;
deputati,  chiedendo che sia dichiarato che non spetta a quest'ultima &#13;
"affermare,  secondo  quanto dalla stessa deliberato nella seduta del &#13;
5 luglio  2000,  la  insindacabilità  delle  opinioni  espresse  dal &#13;
deputato  on. Vittorio  Sgarbi,  in  quanto  estranee alla previsione &#13;
dell'art. 68,  primo  comma, della Costituzione" e, conseguentemente, &#13;
che  venga  annullata  la  predetta  deliberazione (atti Camera, doc. &#13;
IV-quater n. 141); &#13;
        che   il   ricorrente   sostiene  che  le  opinioni  espresse &#13;
dall'on. Sgarbi  non  possono  essere ricondotte alle funzioni svolte &#13;
dal  medesimo  come deputato; sicché, la valutazione della Camera è &#13;
da  reputarsi  "arbitraria", costituendo "un'illegittima interferenza &#13;
nella  sfera di attribuzione" del giudice, "in palese contrasto con i &#13;
principi di cui all'art. 101 e ss. della Costituzione". &#13;
    Considerato  che,  in questa fase, la Corte è chiamata, ai sensi &#13;
dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, &#13;
a  delibare  esclusivamente se il ricorso sia ammissibile, valutando, &#13;
senza  contraddittorio  tra  le  parti,  se  sussistono  i  requisiti &#13;
soggettivo  e  oggettivo  di  un conflitto di attribuzione tra poteri &#13;
dello Stato; &#13;
        che,  quanto al requisito soggettivo, il tribunale di Salerno &#13;
è  legittimato  a  sollevare  il  conflitto  in  quanto competente a &#13;
dichiarare definitivamente, in relazione al procedimento del quale è &#13;
investito,  la  volontà  del potere cui appartiene, in ragione della &#13;
posizione  di indipendenza, costituzionalmente garantita, nella quale &#13;
i singoli organi giurisdizionali svolgono le proprie funzioni; &#13;
        che,  parimenti, la Camera dei deputati, che ha deliberato la &#13;
insindacabilità  delle  opinioni  espresse  da un proprio membro, è &#13;
legittimata   ad   essere  parte  del  conflitto,  in  quanto  organo &#13;
competente  a dichiarare in via definitiva la volontà del potere che &#13;
rappresenta; &#13;
        che,  sotto  il  profilo  oggettivo,  il  giudice  ricorrente &#13;
lamenta   la   lesione   della   propria   sfera   di   attribuzioni, &#13;
costituzionalmente  garantita,  da  parte  della citata deliberazione &#13;
della Camera dei deputati; &#13;
        che,  pertanto,  esiste  la  materia  di un conflitto, la cui &#13;
risoluzione    spetta   alla   competenza   della   Corte,   restando &#13;
impregiudicata ogni ulteriore decisione definitiva (a contraddittorio &#13;
integro), anche in ordine all'ammissibilità del ricorso.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riservato ogni definitivo giudizio; &#13;
    Dichiara  ammissibile, ai sensi dell'art. 37 della legge 11 marzo &#13;
1953,  n. 87,  il  ricorso  per  conflitto  di attribuzione di cui in &#13;
epigrafe,  proposto  dal  tribunale di Salerno, prima sezione penale, &#13;
nei confronti della Camera dei deputati; &#13;
    Dispone: &#13;
        a) che la cancelleria della Corte dia immediata comunicazione &#13;
della presente ordinanza al tribunale di Salerno, ricorrente; &#13;
        b)  che,  a  cura  del  ricorrente,  il ricorso e la presente &#13;
ordinanza  siano  notificati alla Camera dei deputati, in persona del &#13;
suo   presidente,   entro   il   termine  di  sessanta  giorni  dalla &#13;
comunicazione,  per essere poi depositati presso la cancelleria della &#13;
Corte  entro  venti  giorni  dalla notifica, secondo l'art. 26, terzo &#13;
comma,  delle  norme  integrative  per  i  giudizi davanti alla Corte &#13;
costituzionale. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 12 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Vari &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 25 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
