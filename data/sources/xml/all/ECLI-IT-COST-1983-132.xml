<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>132</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:132</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/04/1983</data_decisione>
    <data_deposito>05/05/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN &#13;
 - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Prof. LIVIO &#13;
 PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 22 della  &#13;
 legge 24  dicembre  1969,  n.  990  (Assicurazione  obbligatoria  della  &#13;
 responsabilità  civile  derivante  dalla  circolazione  dei  veicoli a  &#13;
 motore e dei natanti) promosso con ordinanza emessa il 23  aprile  1977  &#13;
 dal Pretore di Bergamo, nel procedimento civile vertente tra Cortinovis  &#13;
 Filippo e Bergamelli Luigina, iscritta al n. 290 del registro ordinanze  &#13;
 1977  e pubblicata nella Gazzetta Ufficiale della Repubblica n. 205 del  &#13;
 27 luglio 1977.                                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del  23  marzo 1983 il Giudice  &#13;
 relatore Ettore Gallo.                                                   &#13;
     Ritenuto che, coll'ordinanza  e  nel  procedimento  civile  di  cui  &#13;
 all'epigrafe, il pretore di Bergamo sollevava questione di legittimità  &#13;
 costituzionale  dell'art.  22  l. 24 dicembre 1969 n.  990 in relazione  &#13;
 all'art. 24, comma primo e  secondo,  Cost.,  in  quanto  riteneva  che  &#13;
 l'onere,  previsto  dalla  norma, a carico dell'attore danneggiato (che  &#13;
 intenti l'azione risarcitoria nei confronti del responsabile civile) di  &#13;
 dimostrare di avere richiesto il risarcimento, sessanta  giorni  prima,  &#13;
 all'assicuratore,  comporti  altresì  la  dimostrazione  che si tratti  &#13;
 effettivamente dell'assicuratore del responsabile civile convenuto,      &#13;
     che siffatta dimostrazione incomberebbe quand'anche il responsabile  &#13;
 civile si fosse costituito e avesse, implicitamente  o  esplicitamente,  &#13;
 riconosciuto   che   l'assicuratore  destinatario  della  richiesta  è  &#13;
 effettivamente il suo assicuratore:  e  ciò  in  quanto,  quest'ultimo  &#13;
 essendo estraneo al processo, il detto riconoscimento non avrebbe alcun  &#13;
 effetto nei suoi confronti,                                              &#13;
     che, perciò, un siffatto onere costituirebbe per il danneggiato un  &#13;
 tale  aggravio  "da vanificare o compromettere di fatto la possibilità  &#13;
 di agire in giudizio",                                                   &#13;
     che ha svolto intervento il Presidente del Consiglio  dei  ministri  &#13;
 chiedendo,  tramite l'Avvocatura Generale dello Stato, che la questione  &#13;
 sia dichiarata irrilevante o infondata.                                  &#13;
     Udito in camera di consiglio il Relatore, prof. Ettore Gallo.        &#13;
     Considerato che, nonostante il responsabile civile costituito nulla  &#13;
 abbia  eccepito  sulla  proponibilità  della  domanda, non per questo,  &#13;
 tuttavia, la questione resta priva di rilevanza, proprio per le ragioni  &#13;
 indotte dal Pretore e più sopra riportate,                              &#13;
     che, però, la questione appare infondata, sia perché questa Corte  &#13;
 ha già avvertito che "sono costituzionalmente legittime  le  questioni  &#13;
 di  legge  che  impongono  oneri diretti ad evitare l'abuso o l'eccesso  &#13;
 nell'esercizio del diritto" o "a salvaguardare interessi  generali  che  &#13;
 con  tale  diritto  sostanzialmente  non contrastano" (vedi sentenze 14  &#13;
 giugno 1973 n. 97; 14 febbraio 1973 n. 24; n. 57 del 1972; n.  130  del  &#13;
 1970,  nonché  le  ord.    59/1974 e 19/1975), sia perché l'apparente  &#13;
 diverso profilo prospettato dal pretore  si  sostanzia  in  realtà  in  &#13;
 qualche  ulteriore  difficoltà  di  fatto  che  può essere facilmente  &#13;
 superata provocando dal magistrato l'ordine di esibizione della polizza  &#13;
 a carico del responsabile  civile,  nella  specie  costituito  e  nulla  &#13;
 opponente,                                                               &#13;
     che, perciò e in ogni caso, non si tratta affatto di onere tale da  &#13;
 vanificare  o pregiudicare la possibilità di agire in giudizio, né da  &#13;
 indebolire la validità dei principi sul punto  reiteramente  affermati  &#13;
 dalla citata giurisprudenza di questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  manifestamente  infondata  la  questione  di legittimità  &#13;
 costituzionale sollevata dal  Pretore  di  Bergamo,  con  ordinanza  25  &#13;
 aprile  1977,  in  ordine  all'art.  22 l. 24 dicembre 1969, n. 990 per  &#13;
 contrasto coll'art. 24, primo e secondo comma, della Costituzione.       &#13;
     Così deciso in Roma, in camera di consiglio nella sede della Corte  &#13;
 costituzionale, Palazzo della Consulta, il 21 aprile 1983.               &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   - ANTONINO  DE  STEFANO  -  GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI DUCCI - LIVIO  PALADIN  -  &#13;
                                   ARNALDO   MACCARONE   -   ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
