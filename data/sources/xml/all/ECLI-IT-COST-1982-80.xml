<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1982</anno_pronuncia>
    <numero_pronuncia>80</numero_pronuncia>
    <ecli>ECLI:IT:COST:1982:80</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/04/1982</data_decisione>
    <data_deposito>29/04/1982</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - &#13;
 Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  70,  comma  &#13;
 primo,  della  legge 26 luglio 1975, n. 354, e successive modificazioni  &#13;
 (Norme sull'ordinamento penitenziario e sulla esecuzione  delle  misure  &#13;
 privative e limitative della libertà) promosso con ordinanza emessa il  &#13;
 18  maggio  1977  dal  Presidente  della  Sezione  di  Sorveglianza del  &#13;
 Tribunale di Roma nel procedimento per revoca di  misura  di  sicurezza  &#13;
 relativo a Cogo Roberto, iscritta al n. 431 del registro ordinanze 1977  &#13;
 e  pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 313 del 16  &#13;
 novembre 1977.                                                           &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  10  febbraio  1982  il  Giudice  &#13;
 relatore Virgilio Andrioli;                                              &#13;
     udito l'avvocato dello Stato Franco Chiarotti per il Presidente del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Provvedendo su istanza diretta alla  Sezione  di  Sorveglianza  &#13;
 presso  la  Corte  d'appello di Roma, con cui Cogo Roberto residente in  &#13;
 Grottaferrata  (Roma)  aveva  chiesto  il  beneficio  della  revoca   o  &#13;
 sospensione  della  libertà  vigilata,  il Presidente della Sezione di  &#13;
 Sorveglianza presso  il  Tribunale  di  Roma,  che  premise  di  essere  &#13;
 competente  a  dare impulso al procedimento, reputò non manifestamente  &#13;
 infondata la questione di legittimità  costituzionale  dell'art.    70  &#13;
 della  legge  26 luglio 1975 n. 354 sull'ordinamento penitenziario, nel  &#13;
 testo sostituito dall'art. 9 della legge  12  gennaio  1977  n.  1,  in  &#13;
 riferimento  agli  artt.  3  comma 1, 24 comma 1 e 25 comma 1 Cost. con  &#13;
 ordinanza 18 maggio 1977, comunicata il 7  giugno  e  notificata  il  2  &#13;
 agosto  successivo, pubblicata nella G.U. n. 313 del 16 novembre 1977 e  &#13;
 iscritta al n. 431 R.O. 1977,  in  cui  considerò  che  la  denunciata  &#13;
 normativa,  per  non  disciplinare  il  procedimento,  impedirebbe agli  &#13;
 interessati di agire  in  giudizio  a  tutela  dei  loro  diritti,  che  &#13;
 costituirebbe   irragionevole   disuguaglianza   l'attribuzione   della  &#13;
 competenza ad adottare il richiesto provvedimento alla sezione e non al  &#13;
 magistrato di  sorveglianza,  cui  spetta  in  generale  provvedere  in  &#13;
 materia  di  revoca  anticipata  della  misura  di  sicurezza,  che non  &#13;
 sussisterebbero norme processuali al caso applicabili per non  essergli  &#13;
 estensibili  il  procedimento  di sorveglianza avanti la Sezione né il  &#13;
 procedimento di sicurezza ex artt. 635  ss.  cod.    proc.  pen.,  che,  &#13;
 infine,  il  magistrato  di  sorveglianza, pur competente a disporre la  &#13;
 revoca della misura di sicurezza alla scadenza del termine originario o  &#13;
 nel corso dei termini successivi,  sarebbe  irrazionalmente  privo  del  &#13;
 potere di disporre la revoca anticipata.                                 &#13;
     2.  -  Avanti  la  Corte  non  si  è costituito il Cogo; ha invece  &#13;
 spiegato intervento il Presidente del Consiglio dei ministri  con  atto  &#13;
 28  luglio  1977 depositato il successivo 5 agosto, in cui l'Avvocatura  &#13;
 generale dello Stato ha chiesto dichiararsi inammissibile  per  difetto  &#13;
 di  legittimazione  del  Presidente  della  Sezione di Sorveglianza del  &#13;
 Tribunale di Roma a sollevarla  e  in  ipotesi  infondata  la  proposta  &#13;
 questione  sul  riflesso  che  il  diritto alla difesa è garantito nel  &#13;
 procedimento di cui agli artt. 635  ss.  cod.  proc.  pen.,  e  giudice  &#13;
 naturale   è  la  Sezione  di  Sorveglianza,  cui  il  legislatore  ha  &#13;
 attribuito la relativa competenza ispirandosi ad  evidenti  criteri  di  &#13;
 ragionevolezza. Alla pubblica udienza del 10 febbraio 1982, nella quale  &#13;
 il  giudice  Andrioli  ha  svolto  la relazione, l'avvocato dello Stato  &#13;
 Chiarotti si è rimesso allo scritto.<diritto>Considerato in diritto</diritto>:                          &#13;
     3.  -  La  proposta  questione  è  inammissibile  per  non  essere  &#13;
 legittimato  a  sollevarla  il presidente della sezione di sorveglianza  &#13;
 che non è autonomo organo giurisdizionale né è abilitato ad adottare  &#13;
 provvedimenti sulla competenza e sul rito afferenti la sezione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 dell'art.  70  l.  26  luglio  1975  n. 354 e successive modificazioni,  &#13;
 sollevata, in riferimento agli artt.  24 comma 1 e 25  comma  1  Cost.,  &#13;
 con  ordinanza emessa il 18 maggio 1977 dal Presidente della Sezione di  &#13;
 Sorveglianza del Tribunale di Roma  nel  procedimento  di  sorveglianza  &#13;
 relativo a Cogo Roberto.                                                 &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 16 aprile 1982.                               &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   - GUGLIELMO ROEHRSSEN - ORONZO  REALE  &#13;
                                   -   BRUNETTO   BUCCIARELLI   DUCCI  -  &#13;
                                   ALBERTO MALAGUGINI - LIVIO PALADIN  -  &#13;
                                   ANTONIO   LA   PERGOLA   -   VIRGILIO  &#13;
                                   ANDRIOLI   -   GIUSEPPE   FERRARI   -  &#13;
                                   FRANCESCO SAJA - GIOVANNI CONSO.       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
