<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>304</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:304</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/06/1995</data_decisione>
    <data_deposito>06/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 19 della legge    &#13;
 della Regione Siciliana 29 aprile 1985, n. 21 (Norme per l'esecuzione    &#13;
 dei lavori pubblici in Sicilia), promosso con ordinanza emessa  il  1    &#13;
 febbraio  1994  dal  Giudice  per  le  indagini preliminari presso la    &#13;
 Pretura di Enna nel procedimento penale a carico di Antonio  Gagliano    &#13;
 e  Salvatore  Termine,  iscritta al n. 591 del registro ordinanze del    &#13;
 1994 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  41,    &#13;
 prima serie speciale, dell'anno 1994;                                    &#13;
    Visto l'atto di intervento della Regione Siciliana;                   &#13;
    Udito  nella  camera  di  consiglio  del 17 maggio 1995 il Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Ritenuto che con ordinanza emessa il 1 febbraio 1994 -  nel  corso    &#13;
 di   un   giudizio  con  rito  abbreviato  nei  confronti  di  alcuni    &#13;
 amministratori  pubblici,  imputati  della  contravvenzione  prevista    &#13;
 dall'art.   1-sexies  del  decreto-legge  27  giugno  1985,  n.  312,    &#13;
 introdotto dalla legge di conversione 8  agosto  1985,  n.  431,  per    &#13;
 avere  modificato,  attraverso l'esecuzione di opere edilizie a pochi    &#13;
 metri dalla linea di battigia del  lago  di  Pergusa,  lo  stato  dei    &#13;
 luoghi della zona sottoposta a vincolo paesaggistico - il Giudice per    &#13;
 le  indagini  preliminari  presso la Pretura di Enna ha sollevato, in    &#13;
 riferimento  agli  artt. 9, 32 e 116 della Costituzione, questione di    &#13;
 legittimità costituzionale dell'art. 19 della  legge  della  Regione    &#13;
 Siciliana  29  aprile  1985, n. 21 (Norme per l'esecuzione dei lavori    &#13;
 pubblici in Sicilia), nella parte in cui prevede che quando si tratta    &#13;
 di opere  pubbliche  in  zone  soggette  a  tutela  il  parere  della    &#13;
 Sovrintendenza   ai   beni   culturali   e   ambientali   si  intende    &#13;
 favorevolmente reso in mancanza di pronunzia entro  90  giorni  dalla    &#13;
 richiesta,   mentre   la   necessità   di   autorizzazione  espressa    &#13;
 risponderebbe ad un principio vincolante anche  per  la  legislazione    &#13;
 regionale;                                                               &#13;
      che  è  intervenuto  il  Presidente della Regione Siciliana, il    &#13;
 quale ha eccepito l'inammissibilità della  questione  per  manifesta    &#13;
 irrilevanza.  Difatti  nella  zona  considerata  sarebbe radicalmente    &#13;
 vietata ogni attività costruttiva, sia  perché  essa  ricade  nella    &#13;
 fascia  di  rispetto di 100 metri dalla linea di costa del lago (art.    &#13;
 2, comma 3, della legge della Regione Siciliana 30  aprile  1991,  n.    &#13;
 15), sia perché è inclusa in una riserva naturale integrale (art. 6    &#13;
 della legge della Regione Siciliana 9 agosto 1988, n. 14);               &#13;
    Considerato   che,   come   risulta   dalla  stessa  ordinanza  di    &#13;
 rimessione, le opere in questione sono state realizzate a meno di 100    &#13;
 metri dalla linea di battigia del lago di Pergusa, e  che  l'area  è    &#13;
 soggetta  a  vincolo  di inedificabilità assoluta ai sensi dell'art.    &#13;
 15, lettera d), della legge della Regione Siciliana 12  giugno  1976,    &#13;
 n.  78 e dell'art. 2, comma 3, della legge della Regione Siciliana 30    &#13;
 aprile  1991,  n.  15,  come  del  resto   ha   anche   rilevato   la    &#13;
 Sovrintendenza  per  i  beni  culturali  e ambientali dichiarando non    &#13;
 luogo a pronunciarsi nel merito del progetto sottoposto al suo esame;    &#13;
      che non sussiste il necessario nesso di pregiudizialità tra  la    &#13;
 questione  di legittimità costituzionale e la decisione del giudizio    &#13;
 principale, perché manca del tutto il presupposto per  applicare  la    &#13;
 norma  denunciata,  non  dovendosi  fare  ricorso alla disciplina del    &#13;
 silenzio  assenso  prevista  nella  procedura  per  il  parere  della    &#13;
 Sovrintendenza per i beni culturali e ambientali;                        &#13;
      che   di   conseguenza   deve  essere  dichiarata  la  manifesta    &#13;
 inammissibilità della questione;                                        &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  dell'art.  19 della legge della Regione    &#13;
 Siciliana 29 aprile 1985, n. 21 (Norme per  l'esecuzione  dei  lavori    &#13;
 pubblici  in  Sicilia),  sollevata, in riferimento agli artt. 9, 32 e    &#13;
 116 della Costituzione,  dal  Giudice  per  le  indagini  preliminari    &#13;
 presso la Pretura di Enna con l'ordinanza indicata in epigrafe.          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 6 luglio 1995.                           &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
