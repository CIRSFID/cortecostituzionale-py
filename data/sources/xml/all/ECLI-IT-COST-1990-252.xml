<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>252</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:252</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/05/1990</data_decisione>
    <data_deposito>15/05/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, &#13;
 prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 438 del codice    &#13;
 di procedura penale del 1988, promossi con le seguenti ordinanze:        &#13;
      1)  ordinanza  emessa  il 27 novembre 1989 dal Tribunale di Asti    &#13;
 nel procedimento penale a carico di Lauricella Giovanni, iscritta  al    &#13;
 n.  45  del  registro  ordinanze  1990  e  pubblicata  nella Gazzetta    &#13;
 Ufficiale della Repubblica n.  7,  prima  serie  speciale,  dell'anno    &#13;
 1990;                                                                    &#13;
      2)  ordinanza  emessa  il 9 dicembre 1989 dal Pretore di Ravenna    &#13;
 nel procedimento penale a carico di Lazzari Angelo, iscritta al n. 52    &#13;
 del  registro  ordinanze  1990  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 7, prima serie speciale, dell'anno 1990;             &#13;
     3)  ordinanza emessa l'8 gennaio 1990 dal Tribunale di Padova nel    &#13;
 procedimento penale a carico di Besson Ettore, iscritta al n. 77  del    &#13;
 registro  ordinanze  1990 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 9, prima serie speciale, dell'anno 1990;                   &#13;
    Udito  nella  camera  di  consiglio  del  4 aprile 1990 il Giudice    &#13;
 relatore Giovanni Conso;                                                 &#13;
    Ritenuto che il Tribunale di Asti, prima di aprire un dibattimento    &#13;
 promosso con rito direttissimo, ha, con  ordinanza  del  27  novembre    &#13;
 1989,   sollevato,   in   riferimento   agli  artt.  3  e  102  della    &#13;
 Costituzione, questione di legittimità dell'art. 438 del  codice  di    &#13;
 procedura  penale  del  1988,  "nella parte in cui non prevede per il    &#13;
 giudice la possibilità di sindacare il  mancato  consenso  del  P.M.    &#13;
 alla  richiesta  di  giudizio  abbreviato, formulata dall'imputato al    &#13;
 fine di applicare la riduzione  della  pena  prevista  dall'art.  442    &#13;
 C.P.P.";                                                                 &#13;
      e   che  un'analoga  questione  hanno  sollevato,  sempre  prima    &#13;
 dell'apertura di dibattimenti  promossi  con  rito  direttissimo,  il    &#13;
 Pretore  di  Ravenna, con ordinanza del 9 dicembre 1989, denunciando,    &#13;
 in riferimento agli artt. 3 e 24 della Costituzione, l'art.  438  del    &#13;
 codice  di procedura penale del 1988, "laddove attribuisce al P.M. la    &#13;
 facoltà di esprimere  un  dissenso  immotivato  sulla  richiesta  di    &#13;
 giudizio  abbreviato  avanzata dall'imputato e laddove non prevede un    &#13;
 controllo dell'organo giudicante sulla fondatezza delle  ragioni  del    &#13;
 dissenso,   così   impedendo   l'instaurazione   di   un   effettivo    &#13;
 contraddittorio tra le parti e una verifica  del  giudice  in  ordine    &#13;
 alla  causa  impeditiva dell'applicazione di una riduzione automatica    &#13;
 della pena", e il Tribunale di Padova, con ordinanza  dell'8  gennaio    &#13;
 1990,  denunciando,  in  riferimento agli artt. 3, 24, 101, 102 e 111    &#13;
 della Costituzione, l'illegittimità  dell'art.  438  del  codice  di    &#13;
 procedura  penale  del  1988  - "il quale nel caso di specie dovrebbe    &#13;
 trovare applicazione in relazione agli artt. 451 n.  5  e  452  n.  2    &#13;
 dello  stesso codice" - in quanto "la decisione del P.M. di negare il    &#13;
 consenso alla applicazione del giudizio abbreviato  è  assolutamente    &#13;
 discrezionale   ed   insindacabile  perché  non  è  previsto  alcun    &#13;
 controllo dell'organo giudicante sulla fondatezza delle  ragioni  del    &#13;
 dissenso,  sicché  viene a dipendere esclusivamente dalla scelta del    &#13;
 P.M. la possibilità per l'imputato di fruire dei benefici in termini    &#13;
 di pena ai sensi dell'art. 442 c.p.p.";                                  &#13;
    Considerato  che  i giudizi, concernendo questioni analoghe, vanno    &#13;
 riuniti;                                                                 &#13;
      che  le  ordinanze  di rimessione - nonostante siano state tutte    &#13;
 pronunciate anteriormente all'apertura di dibattimenti  promossi  dal    &#13;
 pubblico  ministero  con  rito  direttissimo  ai sensi dell'art. 449,    &#13;
 terzo comma, del codice di procedura penale del 1988, rito in  ordine    &#13;
 al  quale  "il  ruolo  esplicato  dal consenso del pubblico ministero    &#13;
 forma oggetto di autonoma previsione da parte dell'art. 452,  secondo    &#13;
 comma",  del  codice  di procedura penale del 1988 - hanno denunciato    &#13;
 l'art. 438 dello stesso codice, norma non applicabile nei  giudizi  a    &#13;
 quibus (v. sentenza n. 183 del 1990);                                    &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 438  del  codice  di  procedura    &#13;
 penale del 1988, sollevata, in riferimento agli artt. 3, 24, 101, 102    &#13;
 e 111 della Costituzione, dal  Tribunale  di  Asti,  dal  Pretore  di    &#13;
 Ravenna e dal Tribunale di Padova con le ordinanze in epigrafe.          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 3 maggio 1990.                                &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: CONSO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 15 maggio 1990.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
