<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>5</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:5</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/12/2000</data_decisione>
    <data_deposito>04/01/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Cesare RUPERTO, Riccardo CHIEPPA, Gustavo &#13;
ZAGREBELSKY, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 5, comma 2, del &#13;
decreto  legislativo 30 dicembre 1992, n. 504 (Riordino della finanza &#13;
degli  enti  territoriali  a norma dell'art. 4 della legge 23 ottobre &#13;
1992,  n. 421),  promosso,  con  ordinanza  emessa il 10 aprile 2000, &#13;
dalla  Commissione  tributaria  provinciale  di  Biella  sul  ricorso &#13;
proposto  dalla Max Eric di Donati e Lavagno s.s. contro il comune di &#13;
Biella,  iscritta  al n. 415 del registro ordinanze 2000 e pubblicata &#13;
nella  Gazzetta  Ufficiale della Repubblica n. 29, 1ª serie speciale, &#13;
dell'anno 2000. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera di consiglio del 13 dicembre 2000 il giudice &#13;
relatore Massimo Vari. &#13;
    Ritenuto  che, con ordinanza del 10 aprile 2000, emessa nel corso &#13;
di  un  giudizio promosso da un contribuente per l'annullamento di un &#13;
avviso di accertamento, con il quale si era provveduto alla rettifica &#13;
in  aumento  del  "valore di un immobile, dichiarato ai fini ICI", la &#13;
Commissione   tributaria  provinciale  di  Biella  ha  sollevato,  in &#13;
riferimento  agli  artt.  24  e  53  della Costituzione, questione di &#13;
legittimità   costituzionale  dell'art.  5,  comma  2,  del  decreto &#13;
legislativo  30  dicembre  1992, n. 504 (Riordino della finanza degli &#13;
enti  territoriali  a  norma dell'art. 4 della legge 23 ottobre 1992, &#13;
n. 421),  "laddove  non  consente  al  contribuente,  a differenza di &#13;
quanto  stabilito  dal  testo  unico  n. 131 del 1986 (Registro), dal &#13;
decreto  legislativo  n. 346  del 1990 (Successioni e donazioni), dal &#13;
d.P.R.  n. 643 del 1972 e successive modifiche (INVIM), di dichiarare &#13;
un valore inferiore a quello risultante dal calcolo aritmetico"; &#13;
        che,  ad  avviso  del rimettente, la disposizione denunciata, &#13;
nel  prevedere  che  "per i fabbricati iscritti in catasto, il valore &#13;
costituente  base imponibile dell'ICI si determina in modo automatico &#13;
applicando all'ammontare delle rendite risultanti in catasto, vigenti &#13;
al   primo   gennaio   dell'anno  di  imposizione,  i  moltiplicatori &#13;
determinati  con  i criteri e le modalità previste dal primo periodo &#13;
dell'ultimo  comma  dell'art.  52" del d.P.R. 26 aprile 1986, n. 131, &#13;
non  contempla  "deroghe  a  differenza  della  legge di Registro che &#13;
consente invece di dichiarare un valore inferiore a quello risultante &#13;
dal calcolo aritmetico"; &#13;
        che,  in  tal  modo,  la disposizione stessa non terrebbe "in &#13;
alcun  conto  le  situazioni che, con riferimento alle singole unità &#13;
immobiliari,  si possono verificare", sì da impedire, inoltre, "allo &#13;
stesso  comune,  destinatario dell'imposta, di discostarsi dal rigido &#13;
criterio di valutazione" anzidetto; &#13;
        che, pertanto, ne deriverebbe, secondo l'ordinanza, un vulnus &#13;
agli  artt. 24 e 53 della Costituzione, giacché il contribuente "non &#13;
è  in condizione di potersi difendere dimostrando l'effettivo valore &#13;
dell'immobile" e, per altro verso, "l'applicazione dell'imposta su un &#13;
valore  determinato  in  base  a  criteri  astratti viola il precetto &#13;
costituzionale della imposizione secondo la capacità contributiva"; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
rappresentato e difeso dall'Avvocatura generale dello Stato, il quale &#13;
ha  concluso  per  l'inammissibilità o, comunque, per l'infondatezza &#13;
della sollevata questione. &#13;
    Considerato  che, successivamente all'ordinanza di rimessione, il &#13;
legislatore,  con  l'art.  74  della  legge  21 novembre 2000, n. 342 &#13;
(Misure  in  materia  fiscale), nel dettare una disciplina che incide &#13;
sugli  atti  attributivi  o  modificativi delle rendite catastali, ha &#13;
previsto,  tra l'altro, che, avverso i predetti atti, resi definitivi &#13;
per   mancata   impugnazione   (comma  2  del  citato  art.  74),  il &#13;
contribuente   può   proporre,   entro   il  termine  di  60  giorni &#13;
dall'entrata in vigore della stessa legge, ricorso innanzi al giudice &#13;
tributario; &#13;
        che, pertanto, occorre ordinare, alla luce del menzionato jus &#13;
superveniens,  la  restituzione  degli atti al giudice rimettente, al &#13;
quale  spetta  di  valutare  la persistente rilevanza della sollevata &#13;
questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Ordina  la  restituzione  degli  atti alla Commissione tributaria &#13;
provinciale di Biella. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 15 dicembre 2000. &#13;
                     Il Presidente: Santosuosso &#13;
                         Il redattore: Vari &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 4 gennaio 2001. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
