<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>22</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:22</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/01/2001</data_decisione>
    <data_deposito>23/01/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Cesare RUPERTO, Riccardo CHIEPPA, Gustavo &#13;
ZAGREBELSKY, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 378 del codice &#13;
penale   in   relazione  all'art.  371-bis  stesso  codice,  promosso &#13;
nell'ambito  di  un  procedimento  penale  con ordinanza emessa il 22 &#13;
febbraio 2000 dal tribunale di Nuoro, iscritta al n. 174 del registro &#13;
ordinanze 2000 e pubblicata nella Gazzetta Ufficiale della Repubblica &#13;
n. 17 - 1ª serie speciale - dell'anno 2000; &#13;
    Udito  nella  camera di consiglio del 13 dicembre 2000 il giudice &#13;
relatore Guido Neppi Modona; &#13;
    Ritenuto  che  il tribunale di Nuoro ha sollevato, in riferimento &#13;
agli  artt.  3  e  24  della  Costituzione, questione di legittimità &#13;
costituzionale  dell'art.  378  del  codice  penale  (favoreggiamento &#13;
personale),  nella  parte  in  cui,  a differenza di quanto stabilito &#13;
dall'art.   371-bis   cod.   pen.  (false  informazioni  al  pubblico &#13;
ministero),  non  prevede  la sospensione del procedimento iniziato a &#13;
carico  di  chi,  richiesto  dalla  polizia giudiziaria su delega del &#13;
pubblico  ministero  di  fornire informazioni ai fini delle indagini, &#13;
abbia reso dichiarazioni false o, in tutto o in parte, reticenti; &#13;
        che  il  rimettente ritiene la questione rilevante, in quanto &#13;
dal  suo accoglimento deriverebbe la sospensione del procedimento per &#13;
il  reato  di  cui  all'art.  378 cod. pen. fino alla pronuncia della &#13;
sentenza  di  primo  grado  nel procedimento nel corso del quale sono &#13;
state assunte le informazioni; &#13;
        che,   ad   avviso   del  giudice  a  quo  la  non  manifesta &#13;
infondatezza  della  questione  discende  dalle argomentazioni svolte &#13;
nella  sentenza  della  Corte  costituzionale n. 101 del 1999, che ha &#13;
dichiarato,    per   violazione   del   principio   di   uguaglianza, &#13;
l'illegittimità  costituzionale  dell'art.  376,  primo  comma, cod. &#13;
pen.,  nella  parte  in cui non prevede l'applicazione della causa di &#13;
non punibilità della ritrattazione in favore di chi, richiesto dalla &#13;
polizia  giudiziaria  delegata  dal  pubblico  ministero  di  fornire &#13;
informazioni  ai  fini delle indagini, abbia reso dichiarazioni false &#13;
ovvero in tutto o in parte reticenti; &#13;
        che  nella  menzionata sentenza - prosegue il rimettente - la &#13;
Corte  ha  infatti  affermato  che  l'assunzione diretta da parte del &#13;
pubblico ministero di informazioni dalle persone che possono riferire &#13;
circostanze  utili ai fini delle indagini e l'assunzione delle stesse &#13;
informazioni da parte della polizia giudiziaria delegata dal pubblico &#13;
ministero   rappresentano   soltanto  forme  diverse  della  medesima &#13;
attività, e ha messo in rilievo che per i predetti atti, assunti sia &#13;
dal  pubblico  ministero che dalla polizia giudiziaria, sono previste &#13;
le  medesime  modalità  di  documentazione  e  le medesime regole di &#13;
utilizzazione probatoria; &#13;
        che,   dopo   l'intervento  della  Corte,  tra  i  due  reati &#13;
rispettivamente   previsti  dagli  artt.  371-bis  e  378  cod.  pen. &#13;
permarrebbe  una  irragionevole disparità di trattamento sul terreno &#13;
processuale,  in  quanto  la sospensione del procedimento a carico di &#13;
chi abbia fornito dichiarazioni false o reticenti è contemplata solo &#13;
in   relazione   al  primo  reato,  e  non  anche  quando  le  stesse &#13;
dichiarazioni  siano rese da chi, richiesto dalla polizia giudiziaria &#13;
delegata dal pubblico ministero di fornire informazioni ai fini delle &#13;
indagini,  venga chiamato a rispondere del delitto di favoreggiamento &#13;
personale; &#13;
        che,  ad avviso del rimettente, tale diversità di disciplina &#13;
non  trova  alcuna  ragionevole  giustificazione  nella  circostanza, &#13;
peraltro  rimessa  alla  discrezionalità del pubblico ministero, che &#13;
sia  quest'ultimo,  ovvero la polizia giudiziaria da lui delegata, ad &#13;
assumere le informazioni; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo  che  la  questione sia dichiarata inammissibile o, &#13;
comunque, infondata, riservandosi di presentare memoria; &#13;
        che  successivamente l'Avvocatura ha revocato il proprio atto &#13;
di intervento. &#13;
    Considerato che il tribunale di Nuoro lamenta che l'art. 378 cod. &#13;
pen.  non  preveda,  analogamente a quanto disposto dall'art. 371-bis &#13;
secondo  comma,  cod.  pen.  per le informazioni assunte dal pubblico &#13;
ministero, la sospensione del procedimento instaurato per il reato di &#13;
favoreggiamento  personale  nei  confronti  di  chi,  richiesto dalla &#13;
polizia  giudiziaria  delegata  dal  pubblico  ministero  di  fornire &#13;
informazioni  ai  fini delle indagini, abbia reso dichiarazioni false &#13;
o, in tutto o in parte, reticenti; &#13;
        che,  a  sostegno  della  non  manifesta  infondatezza  della &#13;
questione,  il rimettente fa leva sulla sentenza n. 101 del 1999, con &#13;
cui  questa  Corte  ha  dichiarato,  per  violazione del principio di &#13;
uguaglianza, l'illegittimità costituzionale dell'art. 376 cod. pen., &#13;
nella  parte in cui non prevede che la causa di non punibilità della &#13;
ritrattazione (espressamente contemplata per il reato di cui all'art. &#13;
371-bis  cod.  pen.) si applichi anche a chi, richiesto dalla polizia &#13;
giudiziaria  delegata dal pubblico ministero di fornire informazioni, &#13;
abbia reso dichiarazioni false o in tutto o in parte reticenti (e sia &#13;
chiamato a rispondere del reato di favoreggiamento personale); &#13;
        che  nella sentenza ora menzionata la Corte ha affermato che, &#13;
per   cogliere  l'irragionevolezza  della  disciplina  censurata,  è &#13;
sufficiente  rilevare  che  le  informazioni  assunte  direttamente e &#13;
personalmente  dal  pubblico ministero e quelle assunte dalla polizia &#13;
giudiziaria  a  ciò  delegata  dal pubblico ministero "costituiscono &#13;
esclusivamente   forme  diverse  della  medesima  attività"  e  sono &#13;
sottoposte  alle  medesime  modalità di documentazione e alle stesse &#13;
regole di utilizzazione probatoria; &#13;
        che,  in particolare, la Corte ha avuto cura di precisare che &#13;
ai  fini  della soluzione della questione non è necessario procedere &#13;
ad  alcun  confronto,  per  ravvisarvi  eventuali  elementi  comuni o &#13;
differenziali,  tra  i  reati previsti dagli artt. 371-bis e 378 cod. &#13;
pen; &#13;
        che  il  richiamo  del  rimettente alle argomentazioni svolte &#13;
nella sentenza n. 101 del 1999 non è quindi conferente ai fini della &#13;
soluzione  della  presente questione di costituzionalità, che appare &#13;
res  integra  rispetto  a questa e alle altre pronunce della Corte su &#13;
aspetti  affini  della  materia  di  cui  si discute (v., di recente, &#13;
sentenza n. 424 del 2000); &#13;
        che, al fine di sostenere l'irragionevolezza della denunciata &#13;
disparità  di  trattamento  processuale  riservata  al  reato di cui &#13;
all'art.   378   cod.  pen.,  il  rimettente  utilizza  come  tertium &#13;
comparationis  la  sospensione  del  procedimento  prevista dall'art. &#13;
371-bis  secondo comma, cod. pen., cioè una disciplina eccezionale e &#13;
derogatoria  rispetto  al  principio  generale, enunciato nell'art. 2 &#13;
cod.   proc.   pen.,  che  attribuisce  al  giudice,  salvo  che  sia &#13;
diversamente  stabilito, il potere-dovere di risolvere ogni questione &#13;
da  cui  dipende  la  decisione  (cfr. sentenza n. 208 del 1994 circa &#13;
l'insussistenza  del rapporto di pregiudizialità tra il procedimento &#13;
per il reato di falsa testimonianza e quello nel corso del quale sono &#13;
state rese le false dichiarazioni); &#13;
        che,  stante  il  suo  carattere  derogatorio,  la disciplina &#13;
dettata  dall'art.  371-bis  secondo comma, cod. pen. potrebbe essere &#13;
assunta  come  termine di raffronto al fine di verificare il rispetto &#13;
del  principio  di  eguaglianza  solo  se fosse sorretta da una ratio &#13;
integralmente  estensibile  alla fattispecie di cui all'art. 378 cod. &#13;
pen.,  sì da rendere la diversità di trattamento del tutto priva di &#13;
ragionevole  giustificazione (v., sia pure con diverse accentuazioni, &#13;
sentenze  nn.  298  e  272  del 1994, nn. 383 e 283 del 1992, nonché &#13;
ordinanze nn. 484 e 140 del 1994); &#13;
        che,  invece,  la  diversità degli elementi che integrano il &#13;
modello legale delle due fattispecie poste a raffronto - nel reato di &#13;
false  informazioni  al  pubblico  ministero,  rendere  dichiarazioni &#13;
false,   ovvero   tacere   in   tutto   o  in  parte;  nel  reato  di &#13;
favoreggiamento personale, aiutare taluno a eludere le investigazioni &#13;
dell'Autorità  mediante  una  condotta che, come nel caso di specie, &#13;
può   sostanziarsi   in   false   dichiarazioni  rese  alla  polizia &#13;
giudiziaria  delegata  dal pubblico ministero - dimostra all'evidenza &#13;
che  diversa è l'oggettività giuridica dei due reati presi in esame &#13;
sicché essi non sono comparabili ai fini della denunciata disparità &#13;
di trattamento processuale; &#13;
        che,  infine,  la  sentenza  n. 101 del 1999 - dalla quale il &#13;
rimettente  vorrebbe  fare  automaticamente  discendere  l'estensione &#13;
della  disciplina della sospensione del procedimento, prevista per il &#13;
reato  di cui all'art. 371-bis cod. pen., al reato di favoreggiamento &#13;
personale commesso mediante dichiarazioni false o reticenti rese alla &#13;
polizia  giudiziaria  delegata  dal pubblico ministero - si riferisce &#13;
alla   sfera  di  operatività  di  un  istituto  di  diritto  penale &#13;
sostanziale,   quale   è   la   causa   di   non  punibilità  della &#13;
ritrattazione,   mentre   ora  alla  Corte  si  chiede  di  estendere &#13;
l'applicazione  di  un  istituto processuale, quale è la sospensione &#13;
del   procedimento,   che,   oltre  ad  avere  natura  eccezionale  e &#13;
derogatoria,   potrebbe   essere  disciplinato  dal  legislatore  con &#13;
modalità diverse da quelle previste dall'art. 371-bis secondo comma, &#13;
cod. pen; &#13;
        che  per  le  concorrenti  ragioni sopra esposte la questione &#13;
deve pertanto essere dichiarata manifestamente infondata. &#13;
    Visti  gli  artt.  26,  secondo comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art.  378  del  codice  penale, in &#13;
relazione   all'art.  371-bis  dello  stesso  codice,  sollevata,  in &#13;
riferimento  agli  artt.  3 e 24 della Costituzione, dal tribunale di &#13;
Nuoro, con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 gennaio 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                     Il redattore: Neppi Modona &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 23 gennaio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
