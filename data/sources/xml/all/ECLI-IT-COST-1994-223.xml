<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>223</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:223</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/05/1994</data_decisione>
    <data_deposito>08/06/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 689 del codice    &#13;
 di procedura penale, promosso con ordinanza emessa il 22 luglio  1993    &#13;
 dal  Tribunale  di  Varese  sull'istanza  proposta  da Ermolli Mauro,    &#13;
 iscritta al n. 695 del registro ordinanze  1993  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  48, prima serie speciale,    &#13;
 dell'anno 1993;                                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1994 il Giudice    &#13;
 relatore Vincenzo Caianiello;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nell'ambito di un procedimento instaurato, ai sensi dell'art.    &#13;
 690  del  codice  di  procedura  penale,  per  la  cancellazione  dal    &#13;
 casellario giudiziale di una iscrizione di condanna a pena pecuniaria    &#13;
 (ammenda,  comprensiva di pena sostitutiva dell'arresto) irrogata con    &#13;
 decreto penale, nonché per la non menzione  di  detta  condanna  nel    &#13;
 certificato  correlativo,  il  Tribunale  di Varese ha sollevato, con    &#13;
 ordinanza   del   22   luglio   1993,   questione   di   legittimità    &#13;
 costituzionale  dell'art.  689  del  medesimo  codice, in riferimento    &#13;
 all'art. 3 della Costituzione, nella parte in cui include nell'elenco    &#13;
 delle pronunce che non sono menzionate nel certificato del casellario    &#13;
 giudiziale rilasciato a richiesta  dell'interessato  la  sentenza  di    &#13;
 "patteggiamento" di cui all'art. 445 del codice di procedura penale e    &#13;
 non anche il decreto penale di condanna.                                 &#13;
    2.  -  Nel  sollevare  la  questione,  il  rimettente  si sofferma    &#13;
 preliminarmente sull'istanza, proposta dalla difesa dell'interessato,    &#13;
 diretta  a  ottenere  la  cancellazione  dell'iscrizione   attraverso    &#13;
 l'applicazione  analogica  della disciplina dettata con riguardo alle    &#13;
 sentenze di "patteggiamento" ex artt. 77 e seguenti  della  legge  n.    &#13;
 689 del 1981 (normativa abrogata a regime col nuovo codice, e vigente    &#13;
 solo  in  via  transitoria: artt. 196, 234, 248, comma 4, disp. att.,    &#13;
 trans. e coord. c.p.p.); la  conclusione  è,  sul  punto,  negativa,    &#13;
 avuto  in  particolare  riguardo  alla  differenza tra l'applicazione    &#13;
 della sanzione sostitutiva a norma degli artt. 53  e  seguenti  della    &#13;
 legge  n.  689  del  1981  -  normativa  tuttora  in  vigore, ed anzi    &#13;
 presupposta  dal  codice,  della  quale  del  resto  è  stata  fatta    &#13;
 applicazione  in  concreto  con  il  decreto  penale  iscritto - e il    &#13;
 previgente "patteggiamento"  ex  artt.  77  e  seguenti  della  legge    &#13;
 citata, istituto eccezionale e con connotati peculiari, ostativi, sia    &#13;
 per  la  lettera  che  per  la ratio delle norme, all'accoglimento di    &#13;
 questa istanza.                                                          &#13;
    3. -  Diffusamente  analizzata,  poi,  la  normativa  in  tema  di    &#13;
 casellario  giudiziale nonché, parallelamente, quella dettata per il    &#13;
 "nuovo" patteggiamento disciplinato dal codice  di  procedura  penale    &#13;
 agli artt. 444 e seguenti, il Tribunale rileva che:                      &#13;
       a)  il  decreto  penale  di  condanna  è,  nel caso di specie,    &#13;
 assoggettato ad iscrizione nel certificato del casellario giudiziale,    &#13;
 e non rientra in alcuna delle ipotesi di esclusione  dell'iscrizione,    &#13;
 in  particolare  per  quanto  riguarda  i  certificati  rilasciati su    &#13;
 richiesta dell'interessato (art. 689 c.p.p.);                            &#13;
       b) la sentenza di  applicazione  della  pena  su  richiesta  (o    &#13;
 "patteggiamento")   è   anch'essa  assoggettata  ad  iscrizione  nel    &#13;
 certificato del casellario giudiziale, giacché  è  "equiparata"  ad    &#13;
 una  sentenza  di  condanna  (art.  445, comma 1); essa, tuttavia, è    &#13;
 espressamente esclusa dall'ambito delle iscrizioni che risultano  nel    &#13;
 certificato  del  casellario  rilasciato a richiesta dell'interessato    &#13;
 (art. 689, comma 2, lett.  a),  punto  5,  del  codice  di  procedura    &#13;
 penale,  dove  questo tipo di sentenza è accomunato alla sentenza di    &#13;
 patteggiamento ex art. 77 della legge n. 689 del 1981).                  &#13;
    4. - La diversa disciplina sopra accennata, e più esattamente  la    &#13;
 mancata  inclusione anche del decreto penale di condanna nel novero -    &#13;
 tassativo  -  delle  iscrizioni  che  non   debbono   risultare   nel    &#13;
 certificato  rilasciato su richiesta dell'interessato ex art. 689 del    &#13;
 codice  di  procedura  penale,  integra,  ad avviso del Tribunale, un    &#13;
 vizio di irragionevolezza della citata norma impugnata, nel raffronto    &#13;
 con quanto all'opposto stabilito  per  la  sentenza  di  applicazione    &#13;
 della   pena   su   richiesta,   in   riferimento  all'art.  3  della    &#13;
 Costituzione.                                                            &#13;
    5.    -    Le    coordinate    argomentative    della    lamentata    &#13;
 incostituzionalità  concernono  da  un lato la finalità della norma    &#13;
 impugnata, dall'altro la comparazione tra  gli  istituti  processuali    &#13;
 (decreto  penale  e  patteggiamento) i cui epiloghi soggiacciono alla    &#13;
 detta differenziata disciplina.                                          &#13;
    6. - Sotto il primo profilo, il  Tribunale  reputa  che  la  ratio    &#13;
 della  norma sull'esclusione di talune iscrizioni dal certificato del    &#13;
 casellario giudiziale  (sia  generale,  che  penale)  sia  quella  di    &#13;
 incentivare  il  recupero del condannato attraverso l'eliminazione di    &#13;
 una delle conseguenze  negative  del  reato,  quella  attinente  alla    &#13;
 pubblicità  del  suo autore; con detta norma, il legislatore agevola    &#13;
 il  reinserimento  sociale  eliminando  l'annotazione   di   pronunce    &#13;
 relative  a  fatti di modesta dimensione o comunque indicativi di una    &#13;
 ridotta antisocialità (contravvenzioni punite con la  sola  ammenda;    &#13;
 "patteggiamenti";  condanne  con  il  beneficio  della  non menzione)    &#13;
 ovvero rispetto ai  quali  siano  intervenute  vicende  di  carattere    &#13;
 estintivo   (riabilitazione;   abolitio   criminis;  amnistia;  cause    &#13;
 estintive speciali); particolare risalto è dato,  nell'ordinanza  di    &#13;
 rimessione,  al  rilievo  per  cui  la  sentenza di patteggiamento è    &#13;
 esclusa dall'iscrizione nel certificato ex art. 689 del codice, così    &#13;
 anticipandosi per questa parte l'effetto favorevole scaturente  dalla    &#13;
 estinzione  del  reato che consegue alla citata pronuncia, decorso un    &#13;
 certo termine (art. 445, comma 2).                                       &#13;
    7. - Sotto il secondo profilo, il Tribunale individua una identica    &#13;
 finalità negli istituti posti a raffronto: tanto il  decreto  penale    &#13;
 di  condanna  quanto  la  sentenza di patteggiamento ( ex artt. 444 e    &#13;
 segg. c.p.p.) costituiscono riti  differenziati  che  realizzano  una    &#13;
 anticipata definizione del procedimento.                                 &#13;
    Attesa  la  comune  finalità,  è  ravvisabile  una irragionevole    &#13;
 disparità di trattamento nella già riferita disciplina;  disparità    &#13;
 accentuata  dal  rilievo per cui la pronunzia di cui all'art. 444 del    &#13;
 codice di procedura penale ha un ambito di operatività assai ampio e    &#13;
 può concernere condanne a pene detentive sino a due anni, mentre  il    &#13;
 decreto  penale ha riguardo a reati puniti con pene pecuniarie ovvero    &#13;
 con pene detentive sostituite dalle prime, nel limite di un mese.        &#13;
    In conclusione, la scelta legislativa, che  "premia"  con  la  non    &#13;
 menzione  nel  certificato a richiesta dell'interessato il ricorso al    &#13;
 patteggiamento, è reputata arbitraria, in quanto è il  procedimento    &#13;
 per  decreto  quello  che  più  degli altri soddisfa le esigenze, di    &#13;
 economia  processuale  e  di  deflazione,  che  hanno   ispirato   il    &#13;
 legislatore  per  questa  parte  della  disciplina  del codice; ed è    &#13;
 infine  da  rilevare  -  conclude  il  rimettente  -  che  attraverso    &#13;
 l'opposizione   al   decreto   penale   è   possibile   accedere  al    &#13;
 patteggiamento, e dunque fruire della non menzione nel certificato in    &#13;
 discorso, ciò che  è  precluso  invece  prestando  acquiescenza  al    &#13;
 decreto penale.                                                          &#13;
    8.  -  È  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  Generale  dello    &#13;
 Stato.  In  una  memoria prodotta a sostegno dell'atto di intervento,    &#13;
 l'Avvocatura nega la validità dell'asserzione circa la identità  di    &#13;
 ratio  e struttura che starebbe al fondo dei due procedimenti, quello    &#13;
 per decreto e quello del patteggiamento, i quali  non  presentano  in    &#13;
 effetti  altra consonanza se non nel fatto di essere entrambi diversi    &#13;
 ("speciali") dal processo ordinario.  Esclusa  la  assimilazione  dei    &#13;
 riti,  ne  segue  -  conclude l'interveniente - la razionalità della    &#13;
 diversificazione,  e  dunque  la  non  fondatezza   della   questione    &#13;
 sollevata.<diritto>Considerato in diritto</diritto>1. - Il Tribunale di Varese dubita, in riferimento all'art. 3 della    &#13;
 Costituzione,  della  legittimità  costituzionale  dell'art. 689 del    &#13;
 codice di procedura penale nella parte in cui non include il  decreto    &#13;
 penale  di  condanna  nell'elenco  delle pronunce non menzionabili ex    &#13;
 lege nel certificato del casellario giudiziale rilasciato a richiesta    &#13;
 dall'interessato, in ciò ravvisando un contrasto con l'art. 3  della    &#13;
 Costituzione  per  irragionevolezza, una volta che lo stesso articolo    &#13;
 esclude da detta menzione la sentenza di applicazione della  pena  su    &#13;
 richiesta (c.d. "patteggiamento": comma 2, lett. a), n. 5 della norma    &#13;
 impugnata).                                                              &#13;
    2. - La questione non è fondata.                                     &#13;
    Quanto  al profilo, prospettato nell'ordinanza di rinvio, circa la    &#13;
 funzione di incentivazione al recupero  sociale  del  condannato  che    &#13;
 sarebbe  alla  base  delle  ipotesi  di esclusione della menzione nel    &#13;
 certificato del casellario di pronunce relative a  fatti  di  modesta    &#13;
 dimensione o di ridotta antisocialità, osserva la Corte che, anche a    &#13;
 volere seguire l'orientamento giurisprudenziale, pur non univoco, che    &#13;
 assegna  al  beneficio della non menzione tale finalità rieducativa,    &#13;
 ciò non elide la discrezionalità del legislatore nella scelta delle    &#13;
 decisioni giurisdizionali da ammettere automaticamente  al  beneficio    &#13;
 in parola, mentre le esclusioni da questo potrebbero essere censurate    &#13;
 solo per irragionevolezza.                                               &#13;
    3.  -  Ma  il giudice rimettente reputa appunto irragionevole aver    &#13;
 previsto il beneficio per le sentenze di applicazione della  pena  su    &#13;
 richiesta  (c.d.  "patteggiamento") ai sensi dell'art. 445 del codice    &#13;
 di procedura penale,  che  possono  comportare  la  condanna  a  pene    &#13;
 detentive  fino a due anni di reclusione, e non per il decreto penale    &#13;
 che ha riguardo a condanne solo  a  pene  pecuniarie  ovvero  a  pene    &#13;
 detentive  sostituite  dalle prime nel limite di "un mese" (oggi, tre    &#13;
 mesi, dopo la modifica dell'art. 53 della legge n. 689  del  1981  ad    &#13;
 opera   dell'art.  5  del  decreto-legge  14  giugno  1993,  n.  187,    &#13;
 convertito in legge 12 agosto 1993, n. 296).                             &#13;
    Osserva la Corte che, anche a volersi ammettere, per la  presa  in    &#13;
 considerazione   della  denuncia  di  irragionevolezza,  la  astratta    &#13;
 possibilità del raffronto con una norma di  deroga  alla  disciplina    &#13;
 generale dell'iscrizione nel casellario giudiziale e delle risultanze    &#13;
 da  annotare  nel  relativo  certificato,  in concreto l'ordinanza di    &#13;
 rinvio, per conseguire l'estensione al decreto penale  del  beneficio    &#13;
 automatico  della non menzione, assume come tertium comparationis una    &#13;
 ipotesi, quale quella del "patteggiamento", che non è  confrontabile    &#13;
 con quella che riguarda il decreto penale.                               &#13;
    Se  è  vero, come ricorda il giudice a quo, che in entrambe dette    &#13;
 ipotesi - del "patteggiamento" e  del  decreto  penale  -  si  è  in    &#13;
 presenza  di  riti  alternativi  rispetto al giudizio ordinario, riti    &#13;
 diretti  cioè  a   realizzare   una   anticipata   definizione   del    &#13;
 procedimento,  diversi  sono tuttavia nei due casi i presupposti e le    &#13;
 modalità attraverso i quali vi si perviene.                             &#13;
    Mentre con  il  decreto  penale,  omesso  il  contraddittorio,  si    &#13;
 perviene  alla  condanna  mediante l'attività esclusiva del pubblico    &#13;
 ministero e del giudice, senza nessun apporto  dell'imputato  -  alla    &#13;
 cui  iniziativa,  come  è  noto,  è rimessa solo successivamente la    &#13;
 possibilità di ripercorrere le fasi  processuali  omesse  per  poter    &#13;
 esercitare  il  diritto  di  difesa - con il c.d. "patteggiamento" la    &#13;
 definizione anticipata  del  processo,  in  funzione  deflattiva  del    &#13;
 dibattimento,   consegue   alla   iniziativa   -   o  al  consenso  -    &#13;
 dell'imputato,  il  quale  viene  a  ciò  incentivato  anche   dalla    &#13;
 previsione del beneficio ex lege della non menzione.                     &#13;
   Non senza ancora considerare, alla luce di quanto appena precisato,    &#13;
 e   tralasciando   il  connotato  estintivo  sostanziale  che  accede    &#13;
 all'istituto del "patteggiamento", che, anche a volersi ammettere una    &#13;
 certa assimilazione, quoad effectum e nel rispettivo contenuto, della    &#13;
 sentenza pronunciata a seguito di "patteggiamento" al decreto  penale    &#13;
 di  condanna,  il  secondo si differenzia dalla prima sotto i profili    &#13;
 ontologico  e  strutturale,  perché  costituisce  (alla  pari  della    &#13;
 sentenza   di   condanna   in  senso  proprio)  pur  sempre  un  atto    &#13;
 riconducibile  soltanto  alla  volontà  del  giudice.  E   ciò   è    &#13;
 sufficiente  ad escludere l'irragionevolezza della mancata estensione    &#13;
 al decreto penale della deroga alla  disciplina  generale  della  non    &#13;
 menzione,  pur prevista ex lege per le sentenze di applicazione della    &#13;
 pena su richiesta,  perché  questa  deroga  appare  dettata  da  una    &#13;
 esigenza  specifica  -  e  cioè  quella di ulteriormente incentivare    &#13;
 l'imputato a pervenire sollecitamente alla definizione del processo -    &#13;
 che non rileva nel decreto penale, il  quale  ultimo,  coerentemente,    &#13;
 può  contenere  o  meno,  in  base ai comuni criteri prognostici, la    &#13;
 statuizione relativa alla concessione del beneficio (art. 460,  comma    &#13;
 2, c.p.p.), al pari della sentenza di condanna in senso proprio.         &#13;
    4.   -   Per   quel   che   riguarda  poi  il  rilievo,  formulato    &#13;
 nell'ordinanza di rinvio,  secondo  cui  la  mancata  estensione  del    &#13;
 beneficio  di  cui  trattasi  al  decreto  penale sarebbe arbitraria,    &#13;
 potendosi - mediante l'opposizione al decreto penale  -  accedere  al    &#13;
 "patteggiamento"  e  fruire  così ex lege del beneficio in questione    &#13;
 per altra via, in contrasto con gli scopi propri dei riti alternativi    &#13;
 che sono quelli di  pervenire  sollecitamente  alla  definizione  dei    &#13;
 processi, osserva la Corte che questo inconveniente non può assumere    &#13;
 rilevanza in termini di costituzionalità.                               &#13;
    Un  inconveniente che spetterebbe in ogni caso solo al legislatore    &#13;
 di correggere, valutata l'opportunità di farlo, dovendo  pur  sempre    &#13;
 farsi  carico  contemporaneamente di altri effetti, operanti su piani    &#13;
 diversi, che l'estensione  del  beneficio  potrebbe  comportare.  Non    &#13;
 senza  considerare che l'inconveniente è solo eventuale, sia perché    &#13;
 rilevabile solo nell'ipotesi in cui il  decreto  penale  opposto  non    &#13;
 contenga  la  concessione  del  beneficio  in  argomento; sia perché    &#13;
 l'incentivo a proporre opposizione al decreto per  fruire  della  non    &#13;
 menzione  è compensato dal rischio di una decisione complessivamente    &#13;
 meno vantaggiosa per l'imputato.                                         &#13;
    Né  rispetto  alla  scelta attualmente operata dal legislatore la    &#13;
 Corte può compiere quel sindacato invocato nell'ordinanza di  rinvio    &#13;
 sul  piano  della  minore  gravità dei reati punibili con il decreto    &#13;
 penale rispetto a quelli punibili con il patteggiamento,  poiché  si    &#13;
 è  in  presenza di apprezzamenti che esulano dai poteri della Corte,    &#13;
 spettando  alla  discrezionalità  del  legislatore   valutare,   nel    &#13;
 concedere  certi  benefici,  se  possa ritenersi determinante il solo    &#13;
 profilo della gravità dei reati,  oppure  se  possano  valere  altre    &#13;
 considerazioni, come nella specie quelle indicate in precedenza.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 689 codice di procedura penale - nella  parte  in  cui  non    &#13;
 include   il   decreto   penale  di  condanna  fra  le  pronunce  non    &#13;
 menzionabili  ex  lege  nel  certificato  del  casellario  giudiziale    &#13;
 rilasciato  a  richiesta dell'interessato - sollevata, in riferimento    &#13;
 all'art.  3  della  Costituzione,  dal  Tribunale   di   Varese   con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 maggio 1994.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria l'8 giugno 1994.                            &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
