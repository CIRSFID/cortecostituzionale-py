<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>57</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:57</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia>Franco Bile</redattore_pronuncia>
    <data_decisione>06/02/2006</data_decisione>
    <data_deposito>10/02/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 166 codice di procedura civile, promosso con ordinanza del 1° ottobre 2004 dal Tribunale di Milano, in composizione monocratica, nel procedimento civile vertente tra M.P. Informatica S.r.l. e T.C. Sistema S.p.A iscritta al n. 258 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 20, prima serie speciale, dell'anno 2005. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 25 gennaio 2006 il Giudice relatore Franco Bile. &#13;
    Ritenuto che il Tribunale di Milano, in composizione monocratica - chiamato a decidere, nel corso di un processo civile, sull'eccezione di tardività della domanda riconvenzionale formulata dalla parte convenuta - ha sollevato, con ordinanza emessa il 1° ottobre 2004 e in riferimento agli artt. 3 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 166 del codice di procedura civile, nella parte in cui non prevede che il convenuto, in ipotesi di abbreviazione dei termini ex art. 163-bis, secondo comma, cod. proc. civ., possa costituirsi almeno dieci giorni prima dell'udienza di comparizione quando la stessa sia stata differita ai sensi dell'art. 168-bis, quinto comma, dello stesso codice; &#13;
    che - dopo aver precisato che la convenuta si è costituita in giudizio (il 5 luglio 2004 ossia) dieci giorni prima dell'udienza di comparizione, differita ai sensi dell'art. 168-bis, quinto comma (e fissata per il 15 luglio 2004), mentre avrebbe dovuto costituirsi (entro il 25 giugno 2004 ossia) venti giorni prima dell'udienza differita, come testualmente prescrive la norma censurata – il rimettente osserva che, per verificare la tempestività della costituzione, a nulla rileverebbe il provvedimento presidenziale di abbreviazione dei termini concesso ex art. 163-bis; &#13;
    che infatti la norma impugnata, mentre distingue i termini per la costituzione rispetto all'udienza indicata nell'atto di citazione a seconda che vi sia stato o meno un provvedimento presidenziale di abbreviazione, non «sembra» operare alcuna differenziazione rispetto all'udienza differita ex art. 168-bis, quinto comma, prescrivendo in ogni caso la costituzione almeno venti giorni prima di tale udienza; &#13;
    che secondo il giudice a quo - sebbene l'abbreviazione del termine a difesa provochi «la variazione, automatica ed a catena, di tutti i successivi termini che il codice di procedura impone alle parti per lo svolgimento delle ulteriori operazioni caratterizzanti la fase introduttiva del giudizio di cognizione» - «atteso che la formulazione letterale della norma non pare agevolmente superabile», «analoghe differenziazioni non sembrano essere state previste dal legislatore allorché l'udienza di prima comparizione venga differita ai sensi del quinto comma dell'art. 168-bis»; &#13;
    che, dunque, la norma impugnata si porrebbe in contrasto: a) con l'art. 3 Cost., per l'irragionevole disparità di trattamento tra la situazione del convenuto che deve parametrare la sua costituzione sull'udienza indicata nell'atto di citazione e quella di chi ha, invece, come termine di riferimento l'udienza differita ai sensi dell'art. 168-bis, quinto comma, potendo solo il primo fruire di termini differenziati nel caso in cui ricorra un provvedimento presidenziale ex art. 163-bis, secondo comma; b) con l'art. 24 Cost., giacché la conseguente limitazione del diritto di difesa non appare giustificata dalla configurabilità di un interesse superiore che motivi il differente e deteriore trattamento riservato dal legislatore all'ipotesi di costituzione del convenuto a seguito del differimento dell'udienza ex art. 168-bis, quinto comma; &#13;
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'inammissibilità o, comunque, per l'infondatezza della sollevata questione. &#13;
    Considerato che il rimettente fonda i suoi dubbi di costituzionalità sulla considerazione che la norma impugnata - mentre distingue i termini per la costituzione del convenuto rispetto all'udienza indicata nell'atto di citazione, secondo che vi sia stato o meno il provvedimento presidenziale di abbreviazione - non «sembra» operare alcuna differenziazione rispetto all'udienza differita ex art. 168-bis, quinto comma, del codice di rito, prescrivendo in ogni caso la costituzione almeno venti giorni prima di tale udienza; &#13;
    che, in particolare, il rimettente - rilevato che l'abbreviazione del termine a difesa «provoca la variazione, automatica ed a catena, di tutti i successivi termini che il codice di procedura impone alle parti per lo svolgimento delle ulteriori operazioni caratterizzanti la fase introduttiva del giudizio di cognizione» - sottolinea tuttavia che, «atteso che la formulazione letterale della norma non pare agevolmente superabile», «analoghe differenziazioni non sembrano essere state previste dal legislatore allorché l'udienza di prima comparizione venga differita ai sensi del quinto comma dell'art. 168-bis»; &#13;
    che, al di là del carattere perplesso di tale argomentazione, il rimettente si arresta di fronte al dato meramente letterale della norma, senza neppure tentare una ricostruzione sistematica dell'istituto che ne consenta una diversa interpretazione; &#13;
    che, così facendo, il giudice a quo - nel rilevato silenzio della legge ed in assenza di un “diritto vivente” - non assolve all'onere di sperimentare letture alternative della disposizione impugnata, ricavandole dai principi, e quindi non rispetta la necessità di motivare sull'impossibilità di interpretare la norma in senso conforme alla Costituzione (da ultimo, ordinanze n. 427 e n. 420 del 2005); &#13;
    che, pertanto, la questione deve essere dichiarata manifestamente inammissibile. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 166 del codice di procedura civile, sollevata - in riferimento agli artt. 3 e 24 della Costituzione - dal Tribunale di Milano, in composizione monocratica, con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 febbraio 2006.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Franco BILE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 10 febbraio 2006.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
