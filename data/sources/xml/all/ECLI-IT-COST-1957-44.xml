<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1957</anno_pronuncia>
    <numero_pronuncia>44</numero_pronuncia>
    <ecli>ECLI:IT:COST:1957:44</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>DE NICOLA</presidente>
    <relatore_pronuncia>Giuseppe Cappi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1957</data_decisione>
    <data_deposito>18/03/1957</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. ENRICO DE NICOLA, Presidente - Dott. &#13;
 GAETANO AZZARITI - Avv. GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. &#13;
 GASPARE AMBROSINI - Prof. ERNESTO BATTAGLINI - Dott. MARIO COSATTI - &#13;
 Prof. FRANCESCO PANTALEO GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - &#13;
 Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI &#13;
 CASSANDRO - Prof. BIAGIO PETROCELLI, Giudici,</collegio>
    <epigrafe>ha pronunziato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale della legge approvata  &#13;
 dall'Assemblea  regionale  siciliana  il   3   ottobre   1956   recante  &#13;
 "interpretazione  autentica  dell'art. 2 della legge regionale 1 agosto  &#13;
 1953, n. 44", promosso con ricorso del  Presidente  del  Consiglio  dei  &#13;
 Ministri  e  del  Commissario  dello Stato presso la Regione siciliana,  &#13;
 depositato nella cancelleria della Corte costituzionale il  19  ottobre  &#13;
 1956 ed iscritto al n. 61 del Reg.  ricorsi 1956.                        &#13;
     Visto   l'atto  di  costituzione  in  giudizio, con deposito  nella  &#13;
 cancelleria delle proprie  deduzioni  in  data  17  ottobre  1956,  del  &#13;
 Presidente  della  Regione  siciliana  rappresentato e difeso dall'avv.  &#13;
 Pietro Virga;                                                            &#13;
     udita nell'udienza pubblica del 13 febbraio 1957 la  relazione  del  &#13;
 Giudice Giuseppe Cappi;                                                  &#13;
     uditi il sost. avv. gen. dello Stato Cesare Arias per il ricorrente  &#13;
 e l'avv. Pietro Virga per la Regione siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il  Presidente  del Consiglio dei Ministri, previa deliberazione di  &#13;
 detto Consiglio adottata nella riunione del  10  ottobre  1956,  ed  il  &#13;
 Commissario  dello  Stato presso la Regione siciliana hanno impugnato -  &#13;
 il Commissario dello Stato "per quanto possa  occorrere"  -  davanti  a  &#13;
 questa  Corte la legge della Regione siciliana recante "interpretazione  &#13;
 autentica dell'art. 2 della legge regionale 1   agosto  1953,  n.  44",  &#13;
 approvata dalla Assemblea regionale in data 3 ottobre 1956 e comunicata  &#13;
 al Commissario dello Stato il 5 dello stesso mese.                       &#13;
     Il  ricorso, firmato, per il Presidente del Consiglio dei Ministri,  &#13;
 dal Commissario dello Stato e firmato altresì da un sostituto avvocato  &#13;
 generale  dello  Stato,  fu  notificato  al  Presidente  della   Giunta  &#13;
 regionale  in  data 10 ottobre e depositato nella cancelleria di questa  &#13;
 Corte il successivo giorno 19. Ne è stata  fatta  pubblicazione  nella  &#13;
 Gazzetta Ufficiale della Repubblica il 3 novembre 1956 e nella Gazzetta  &#13;
 Ufficiale della Regione siciliana il 10 dello stesso mese.               &#13;
     Nel  ricorso  si  premette che le disposizioni di temporaneo favore  &#13;
 per la registrazione di atti di anticipazione e finanziamenti in genere  &#13;
 in correlazione con operazioni di cessioni o di costituzioni  in  pegno  &#13;
 di  crediti  furono emanate dallo Stato con R.D.L. 19 dicembre 1936, n.  &#13;
 2170, e successivamente prorogate con  altre  numerose  leggi,  la  cui  &#13;
 efficacia  venne  a scadere il 31 dicembre 1951. A tale data riprendeva  &#13;
 vigore la disciplina ordinaria fino all'emanazione della legge 4 aprile  &#13;
 1953, n. 261, con  la  quale  veniva  regolata  in  via  permanente  la  &#13;
 materia, con effetto per l'avvenire. Sulla trama di questa legislazione  &#13;
 nazionale, nella quale si ebbe l'interruzione di ogni beneficio fiscale  &#13;
 dal  31  dicembre  1951  alla  data  di entrata in vigore della legge 4  &#13;
 aprile 1953, si inserisce un primo intervento della  Regione  siciliana  &#13;
 con  la  legge 29 dicembre 1947, con la quale si dispose la proroga del  &#13;
 beneficio previsto da una legge nazionale del 31 ottobre 1946. La legge  &#13;
 regionale fu impugnata dal Commissario dello Stato, ma l'Alta Corte per  &#13;
 la  Regione  siciliana  la   dichiarò   legittima,   pur   rilevandone  &#13;
 l'inutilità   per   essere   sopravvenuta  la  proroga  del  beneficio  &#13;
 tributario ad opera del decreto legislativo del Capo Provvisorio  dello  &#13;
 Stato 18 dicembre 1947, n. 1385.                                         &#13;
     Cessata,  con  il  31 dicembre 1951, ogni agevolazione, la Regione,  &#13;
 con legge 22 agosto 1952, n. 49, concedeva ex novo fino  al  30  giugno  &#13;
 1954  le  agevolazioni già vigenti in precedenza nella stessa materia.  &#13;
 Finalmente, con legge 1 agosto 1953, n. 44,  la  Regione,  nel  dettare  &#13;
 norme  per  l'applicazione  della  richiamata  legge  n.  49  del 1952,  &#13;
 stabiliva che per beneficiare   delle agevolazioni ivi  previste  fosse  &#13;
 necessaria  l'indicazione  negli  atti  sottoposti  a registrazione che  &#13;
 l'efficacia delle cessioni e costituzioni in pegno non si estendeva  ad  &#13;
 altre  operazioni,  stabilendo    altresì nell'art. 2 che per gli atti  &#13;
 stipulati prima dell'entrata in vigore della legge 1    agosto  1953  e  &#13;
 registrati  in termine con l'applicazione delle aliquote di favore, che  &#13;
 non risultassero  redatti  in  conformità  alle  prescrizioni  di  cui  &#13;
 all'articolo    precedente   o   contenessero   indicazioni   vaghe   o  &#13;
 indeterminate, le operazioni di apertura di  credito,  anticipazione  o  &#13;
 finanziamento, in relazione alle quali la cessione era stata stipulata,  &#13;
 potevano  essere  precisate  con apposito atto aggiuntivo da stipularsi  &#13;
 tra le stesse parti contraenti, con espresso riferimento  al  contratto  &#13;
 già  registrato,  entro il termine di sessanta giorni dalla entrata in  &#13;
 vigore della legge.                                                      &#13;
     Nei riguardi della disposizione  dell'art.  2  è  stata  sollevata  &#13;
 questione   di   legittimità   costituzionale   con   ordinanza  della  &#13;
 Commissione provinciale delle imposte di Agrigento.                      &#13;
     La successiva legge regionale del 1956, ora impugnata, contiene due  &#13;
 articoli. Il primo dichiara, nel primo comma, che la stipula degli atti  &#13;
 aggiuntivi previsti dall'art. 2 della legge regionale 1  agosto 1953 ha  &#13;
 effetto quando dagli atti di cessione o di  costituzione  in  pegno  di  &#13;
 crediti  non  risulti  che  la  efficacia della garanzia è limitata al  &#13;
 finanziamento ovvero gli atti contengano clausole che prevedano in modo  &#13;
 generico e indeterminato la  estensione  di  tale  efficacia  ad  altre  &#13;
 operazioni senza precisarle. Nel secondo comma l'articolo primo dispone  &#13;
 che  gli  atti  aggiuntivi  hanno valore quando contengano la specifica  &#13;
 indicazione delle altre operazioni,  diverse  dal  finanziamento,  alle  &#13;
 quali   la  garanzia  sia  eventualmente  in  parte  destinata,  ovvero  &#13;
 l'esplicita dichiarazione che la cessione o la  costituzione  in  pegno  &#13;
 dei  crediti era destinata o ebbe efficacia esclusivamente e per il suo  &#13;
 importo a garanzia e soddisfacimento del finanziamento stesso, e non di  &#13;
 altre e diverse operazioni di credito.                                   &#13;
     L'art. 2 dichiara che le disposizioni contenute nell'art.  2  della  &#13;
 legge  regionale 1   agosto 1953, n. 44, si applicano a tutti gli atti,  &#13;
 nello stesso  articolo  indicati,  stipulati  nella  Regione  siciliana  &#13;
 posteriotmente  al  1    aprile 1947, i quali siano stati registrati in  &#13;
 termine con la  applicazione  delle  aliquote  speciali  stabilite  dal  &#13;
 R.D.L. 19 dicembre 1936, n. 2170, e successivi provvedimenti.            &#13;
     I  motivi  addotti  per l'impugnazione della legge del 1956 possono  &#13;
 così riassumersi:                                                       &#13;
     1. - La Regione siciliana non ha alcuna potestà  legislativa,  né  &#13;
 esclusiva né secondaria, in materia tributaria. La legge impugnata è,  &#13;
 pertanto,  illegittima al pari delle precedenti leggi regionali emanate  &#13;
 in materia.                                                              &#13;
     Ma  la  legge  stessa  consuma  altresì, in via autonoma e diversa  &#13;
 dalla sua dipendenza da precedenti leggi incostituzionali, la  medesima  &#13;
 violazione dei limiti della competenza legislativa regionale, in quanto  &#13;
 estende  a  tutti  gli  atti  stipulati dopo il 1 luglio 1947, e quindi  &#13;
 sottratti al regime delle leggi n. 49 del 1952 e n.  44  del  1953,  il  &#13;
 beneficio  fiscale  da queste illegittimamente istituito e quindi porta  &#13;
 un contenuto normativo nuovo e diverso rispetto  alle  accennate  leggi  &#13;
 precedenti.                                                              &#13;
     Per tale ragione non può avere rilievo la circostanza che le leggi  &#13;
 precedenti  non  siano  state impugnate tempestivamente dal Commissario  &#13;
 dello Stato, perché per un verso la mancata  impugnazione  non  poteva  &#13;
 costituire in capo alla Regione un potere normativo che statutariamente  &#13;
 non  le  compete e per altro verso la legge regionale 1  agosto 1953 è  &#13;
 stata impugnata, in via incidentale, davanti a questa Corte. Ciò rende  &#13;
 evidente la necessità che sia dato ingresso alla presente impugnazione  &#13;
 perché altrimenti la eventuale declaratoria di  inefficacia  di  detta  &#13;
 legge  precedente  sarebbe  resa  vana dalla recezione di quella stessa  &#13;
 norma nell'art. 2 della legge ora  impugnata,  che  pretende  di  darne  &#13;
 l'interpretazione autentica.                                             &#13;
     2.  - Quand'anche, in via di mera ipotesi, si  volesse ritenere che  &#13;
 la  Regione  abbia  in  materia   tributaria   competenza   legislativa  &#13;
 secondaria,  sussisterebbe  la violazione di tale competenza, in quanto  &#13;
 la   disposizione   richiamata   nell'articolo   che   pretende   darle  &#13;
 interpretazione  autentica,  viola il principio generale cui si informa  &#13;
 la legislazione dello Stato in materia  di  registro,  che  è  sancito  &#13;
 nell'art.  8  della legge del registro, a norma del quale gli atti sono  &#13;
 soggetti a tassazione secondo la loro  forma  e    il  loro  contenuto,  &#13;
 esclusa  quindi la possibilità di trasformare la condizione obbiettiva  &#13;
 di  tassabilità  dell'atto  mediante  la  stipulazione  di   un   atto  &#13;
 aggiuntivo,  stipulato  dopo  la  registrazione  dell'atto principale e  &#13;
 diretto   ad  integrare  l'atto  già  registrato,  introducendovi  una  &#13;
 clausola della quale era sprovvisto, ed  in  assenza  della  quale  era  &#13;
 tenuto a scontare l'imposta normale anziché quella di favore. Anche ai  &#13;
 sensi  della  legge  nazionale  4  aprile 1953 il beneficio della minor  &#13;
 tariffa per gli atti  in  questione  è  subordinato  alla  contestuale  &#13;
 specificazione  della destinazione dell'operazione con esclusione della  &#13;
 sua estensibilità ad operazioni diverse  e  con  espressa  statuizione  &#13;
 dell'applicabilità,  in  mancanza  di  tali  specifiche determinazioni  &#13;
 dell'atto, della normale imposta di registro.                            &#13;
     3. - L'art. 2 della  legge  impugnata  è  inoltre  affetto  da  un  &#13;
 ulteriore  vizio  di  incostituzionalità,  in  quanto  con l'estendere  &#13;
 l'agevolazione  tributaria  della  legge  22  agosto  1952  agli   atti  &#13;
 stipulati  anteriormente  all'entrata in vigore di questa, per un verso  &#13;
 contraddice alla legge stessa, la quale espressamente prevedeva che  il  &#13;
 beneficio  sarebbe  divenuto  applicabile  solo  dopo la sua entrata in  &#13;
 vigore, e per altro verso estende il valore retroattivo  del  beneficio  &#13;
 ad  un periodo, andante dal 1  gennaio 1952 al 6 settembre dello stesso  &#13;
 anno, durante il quale  nessuna  agevolazione  assisteva  gli  atti  in  &#13;
 questione. In tal modo si intenderebbe attribuire efficacia retroattiva  &#13;
 alla   legge   regionale  n.  49  del  1952  sotto  il  pretesto  della  &#13;
 interpretazione autentica delle disposizioni per  la  sua  applicazione  &#13;
 contenute  nella  legge regionale 1  agosto 1953, n. 44, violando così  &#13;
 il disposto dell'ultimo  comma  dell'art.  73  della  Costituzione  che  &#13;
 statuisce che le leggi entrano in vigore dopo la loro pubblicazione.     &#13;
     D'altra  parte,  applicando  retroattivamente un beneficio di legge  &#13;
 che modifica il regime tributario di atti  sottoposti  a  registrazione  &#13;
 secondo  la  legge  statale vigente, la Regione verrebbe ad abrogare la  &#13;
 legislazione statale con effetto retroattivo, il che esula  chiaramente  &#13;
 dalla potestà legislativa della Regione, la quale, anche in materia di  &#13;
 sua  competenza,  non  può  mai  legiferare  ex tunc su rapporti prima  &#13;
 disciplinati soltanto dalla legge statale.                               &#13;
     Né può  tale  illegittima  interferenza  nel  potere  legislativo  &#13;
 statale  trovare  riparo dietro la forma dell'interpretazione autentica  &#13;
 di una legge regionale, sia perché non è interpretazione autentica di  &#13;
 una norma l'estensione della sua validità protratta  all'indietro  nel  &#13;
 tempo  in  cui  essa  non aveva vigore, sia perché comunque la vigente  &#13;
 Costituzione non contempla, come già lo Statuto Albertino,  l'istituto  &#13;
 specifico dell'interpretazione autentica; dal che deriva la conseguenza  &#13;
 che la legge rivolta all'interpretazione di altra precedente può avere  &#13;
 tale  effetto  solo  nei limiti in cui l'interpretazione che ex novo si  &#13;
 propone dal legislatore possa essere sopportata  dalla  legge  vigente,  &#13;
 nel  senso  cioè  che  non  potrà aversi modifica ex tunc della legge  &#13;
 anteriore ad opera  della  nuova  ma  solo  sostituzione,  con  effetto  &#13;
 abrogativo, dell'una all'altra.                                          &#13;
     Per  le  esposte  considerazioni,  si  conclude che la Corte voglia  &#13;
 dichiarare  l'illegittimità  costituzionale  della   legge   regionale  &#13;
 impugnata,  e subordinatamente dell'art. 2 di essa per violazione degli  &#13;
 artt. 14 e 17 dello Statuto della Regione siciliana, in relazione  alla  &#13;
 legge  4  aprile  1953,  n.  261,  e all'art. 8 della legge organica di  &#13;
 registro,  nonché  dell'art.  73  in  relazione  all'art.   25   della  &#13;
 Costituzione e degli articoli 36 e 37 dello Statuto regionale.           &#13;
     Si  è  costituito  in giudizio il Presidente della Regione, la cui  &#13;
 difesa ha depositato in data 17 ottobre 1956 deduzioni con le quali  ha  &#13;
 concluso  chiedendo  che  il  ricorso  sia  dichiarato  inammissibile o  &#13;
 comunque respinto.                                                       &#13;
     1.   -   Sostiene,   anzitutto,  la  Regione,  che  il  ricorso  è  &#13;
 inammissibile davanti a questa Corte, giacché le norme  dello  Statuto  &#13;
 siciliano  relative all'Alta Corte per la Regione siciliana ed alla sua  &#13;
 competenza a giudicare sui ricorsi proposti in via principale contro le  &#13;
 leggi  siciliane  non  sono  state  abrogate  dall'art.  134  e   dalle  &#13;
 disposizioni  transitorie  VII  e  XVI della Costituzione e traggono il  &#13;
 fondamento  della  loro  attuale  efficacia  dall'art.  116   e   dalla  &#13;
 disposizione  transitoria  XVII della Costituzione, nonché dall'art. 1  &#13;
 della legge costituzionale 26 febbraio 1948, n. 2.                       &#13;
     2.  -  Il  ricorso  è,   poi,   inammissibile   per   difetto   di  &#13;
 legittimazione  attiva,  essendo  stato  proposto  dal  Presidente  del  &#13;
 Consiglio dei Ministri e, per quanto possa occorrere,  dal  Commissario  &#13;
 dello  Stato per la Sicilia, ma unicamente sottoscritto dal Commissario  &#13;
 stesso "per il Presidente del Consiglio dei Ministri". La difesa  della  &#13;
 Regione  deduce  che  o la impugnativa contro la legge siciliana veniva  &#13;
 proposta dal Commissario dello Stato ed  allora  doveva  adirsi  l'Alta  &#13;
 Corte  ovvero  veniva  proposta  dal Presidente del Consiglio ed allora  &#13;
 doveva rimanere del tutto estraneo il Commissario dello Stato, il quale  &#13;
 invece  figura  come  sottoscrittore  principale  e   munito   di   una  &#13;
 rappresentanza    del  Presidente  del  Consiglio,  che  nessuna  norma  &#13;
 costituzionale o ordinaria gli attribuisce.                              &#13;
     3. - Il ricorso è, inoltre, inammissibile per  acquiescenza  dello  &#13;
 Stato  nei  confronti  della  legislazione  in materia. Lo Stato non ha  &#13;
 impugnato tre precedenti leggi aventi analogo  oggetto.  Da  ciò  deve  &#13;
 desumersi l'accettazione da parte dello Stato dell'atto o del principio  &#13;
 contro cui si intende ricorrere.                                         &#13;
     4.  - La legge regionale approvata dall'Assemblea il 3 ottobre 1956  &#13;
 è confermativa di quella precedente, non impugnata, del 1 agosto 1953,  &#13;
 n. 44, e quindi non poteva proporsi l'impugnativa principale contro  la  &#13;
 seconda  senza  che fosse contemporaneamente proposta impugnativa della  &#13;
 legge confermata. Posto infatti che si tratta di legge  interpretativa,  &#13;
 la  quale si   limita a chiarire il significato delle norme della legge  &#13;
 interpretata, è evidente come la invasione di competenza  statale,  se  &#13;
 mai  vi  fosse,  sarebbe  derivata  dalla legge interpretata e non già  &#13;
 dalla legge interpretativa.                                              &#13;
     5. - È infondato il primo motivo del ricorso in quanto si nega  la  &#13;
 potestà  legislativa  della  Regione in materia tributaria.  L'assunto  &#13;
 contrasta con uno dei caratteri essenziali dell'autonomia della Regione  &#13;
 siciliana ed è chiaramente  contraddetto  dal  disposto  dell'art.  36  &#13;
 dello  Statuto  siciliano,  che  attribuisce  alla Regione il potere di  &#13;
 deliberare  i  tributi  necessari  al  fabbisogno   finanziario   della  &#13;
 medesima,  attuando  un  regime  di  separazione  con l'attribuzione di  &#13;
 alcune imposte alla competenza esclusiva dello Stato  e  l'attribuzione  &#13;
 di altre alla competenza esclusiva della Regione.                        &#13;
     6.  - Il secondo motivo del ricorso è inammissibile per due ordini  &#13;
 di considerazioni. In primo luogo, perché esso si riferisce  non  già  &#13;
 alla legge impugnata sì bene alla precedente legge regionale 1  agosto  &#13;
 1953,  con  la  quale  venne  conferita  la  facoltà  di precisare con  &#13;
 apposito atto aggiuntivo le  operazioni  in  relazione  alle  quali  la  &#13;
 cessione  di  credito  era  stata  stipulata.  Non può farsi valere in  &#13;
 questa sede un vizio che inficiava eventualmente la legge interpretata,  &#13;
 non impugnata entro i termini.                                           &#13;
     In   secondo   luogo,  il  motivo  è  inammissibile  perché  esso  &#13;
 presuppone  che  la  competenza  legislativa   regionale   in   materia  &#13;
 tributaria   trovi   il  limite  dei  principi  a  cui  si  informa  la  &#13;
 legislazione statale mentre, essendo stato adottato  dalla  Regione  un  &#13;
 sistema  di  separazione  e  non  di  quotità  per la ripartizione dei  &#13;
 tributi, la competenza tributaria della Regione stessa è  esclusiva  e  &#13;
 non complementare.                                                       &#13;
     Il  motivo  è infondato nel merito, perché il principio enunciato  &#13;
 dal  ricorso  non   costituisce   un   principio   fondamentale   della  &#13;
 legislazione  dello  Stato.  Difatti  la  legislazione  dello  Stato in  &#13;
 materia tributaria ammette in casi determinati la  regolarizzazione  ai  &#13;
 fini fiscali di atti stipulati. Basti ricordare l'esempio offerto dalla  &#13;
 legge  6  agosto  1954,  n. 604, sulla piccola proprietà contadina, la  &#13;
 quale ha ammesso la regolarizzazione degli atti stipulati anteriormente  &#13;
 alla entrata in vigore della legge stessa.                               &#13;
     7. - Il terzo motivo del ricorso è anch'esso inammissibile per  le  &#13;
 due ragioni dedotte in ordine al motivo precedente.                      &#13;
     Nel merito, è infondato.                                            &#13;
      a)  In  primo  luogo  non  è  esatto che la legge impugnata abbia  &#13;
 violato la precedente legge regionale. La interpretazione data  con  la  &#13;
 legge   impugnata   è  la  più  aderente  allo  spirito  della  legge  &#13;
 precedente, la quale intendeva riferirsi a  tutti  gli  atti  stipulati  &#13;
 anteriormente alla entrata in vigore della legge stessa e contenenti le  &#13;
 clausole generiche.                                                      &#13;
     La legge in esame è interpretativa e non innovativa; ma del resto,  &#13;
 anche  qualora  volesse  ritenersi che sia innovativa, è evidente che,  &#13;
 una volta riconosciuto un potere legislativo  in  materia,  la  Regione  &#13;
 possa modificare ed abrogare le norme da essa stessa poste.              &#13;
     b)  La Costituzione vieta la retroattività solo limitatamente alle  &#13;
 leggi penali e non la vieta affatto per le leggi tributarie, specie per  &#13;
 quelle fra di esse che sono dirette a consentire agevolazioni a  favore  &#13;
 dei  contribuenti.  D'altro lato, il carattere distintivo più saliente  &#13;
 della legge interpretativa è la sua retroattività, e  quindi  non  si  &#13;
 vede   come   la   Regione   avrebbe   potuto   emanare  una  legge  di  &#13;
 interpretazione autentica non retroattiva.                               &#13;
     c) Quanto all'argomento  relativo  alla  violazione  del  principio  &#13;
 secondo   cui   la  legge  regionale  non  può  abrogare  con  effetto  &#13;
 retroattivo la legge statale, esso si riferirebbe, se mai,  alla  legge  &#13;
 interpretata  e  non  alla  legge  di    interpretazione ed è comunque  &#13;
 infondato, giacché  una  volta  riconosciuto  che  la  disciplina  dei  &#13;
 rapporti  in  questione  è  soggetta alla legge regionale, non si vede  &#13;
 perché, limitatamente ad un periodo determinato, i rapporti dovrebbero  &#13;
 considerarsi soggetti al legislatore statale.                            &#13;
     d) È infine inaccettabile  l'affermazione  contenuta  nel  ricorso  &#13;
 secondo  cui  l'interpretazione autentica è vietata dalla Costituzione  &#13;
 solo perché non espressamente prevista dal Costituente.  Non  solo  il  &#13;
 legislatore  statale ha fatto più volte uso del suo potere legislativo  &#13;
 di interpretare autenticamente le leggi precedenti, ma non esiste alcun  &#13;
 ostacolo costituzionale alla  interpretazione  autentica  o  ad  alcune  &#13;
 specie della medesima.                                                   &#13;
     Nella  discussione orale gli avvocati hanno illustrato le deduzioni  &#13;
 e le conclusioni   svolte negli scritti difensivi.    L'Avvocato  dello  &#13;
 Stato  ha  precisato  che  nel  ricorso non si è voluta specificamente  &#13;
 porre la questione generale circa la legittimità costituzionale  delle  &#13;
 leggi  di  interpretazione  autentica, ma si è inteso piuttosto negare  &#13;
 tale potestà, nel caso concreto, alla Regione siciliana.<diritto>Considerato in diritto</diritto>:                          &#13;
     Con  sentenza n. 38 del 27 febbraio 1957 è stato deciso che spetta  &#13;
 a questa Corte  la  competenza  a  giudicare,  fra  l'altro,  sopra  le  &#13;
 impugnazioni  proposte in via principale dal Governo dello Stato contro  &#13;
 le leggi regionali siciliane. A tale pronuncia ed alle ragioni  che  la  &#13;
 sorreggono  basta qui far riferimento per respingere la prima eccezione  &#13;
 di inammissibilità proposta dalla Regione.                              &#13;
     Anche la seconda  eccezione  pregiudiziale  è  infondata.  Con  la  &#13;
 stessa  sentenza ora richiamata, la Corte ha enunciato il principio che  &#13;
 tutte le norme relative alla proposizione del ricorso  contenute  nello  &#13;
 Statuto siciliano restano applicabili.                                   &#13;
     Nella  specie,  il  ricorso  è  stato  proposto dal Presidente del  &#13;
 Consiglio e dal Commissario dello  Stato  entro  il  termine  stabilito  &#13;
 dall'art.  28 dello Statuto predetto. È chiaro che, proponendo ricorso  &#13;
 per quanto possa occorrere, è stata prevista l'ipotesi che si  è  ora  &#13;
 verificata,   che,  cioè,  fosse  riconosciuto  il  principio  che  la  &#13;
 legittimazione attiva spetti al Commissario dello Stato. È  ovvio  che  &#13;
 il  superfluo  ricorso  del  Presidente  del Consiglio dei Ministri non  &#13;
 vizia il parallelo ricorso proposto dall'organo competente.              &#13;
     Vero è che il ricorso è stato sottoscritto dal Commissario  dello  &#13;
 Stato  per  il  Presidente  del  Consiglio  ed  è  anche  vero  che il  &#13;
 Commissario non ha alcuna rappresentanza del Presidente del  Consiglio,  &#13;
 ma  agisce  in  virtù  di  un  potere inerente al proprio ufficio; è,  &#13;
 tuttavia, da  rilevare  che  il  Commissario  ha  sicuramente  proposto  &#13;
 ricorso  in  nome  proprio  e  che  la  incertezza della sottoscrizione  &#13;
 dell'atto non è tale da far ritenere che l'atto manchi di un requisito  &#13;
 formale indispensabile per il raggiungimento del suo scopo; tanto  più  &#13;
 che  l'atto è stato formato nel periodo in cui questa Corte non si era  &#13;
 ancora pronunciata sulla questione.                                      &#13;
     Si possono esaminare insieme le eccezioni di  inammissibilità  per  &#13;
 acquiescenza e per il carattere confermativo del provvedimento.          &#13;
     Non   si   esclude   a  priori  che  nei  giudizi  di  legittimità  &#13;
 costituzionale, proposti in via  principale,  possano  avere  rilevanza  &#13;
 preclusioni che spiegano efficacia nei giudizi inter partes, sempre che  &#13;
 ciò   sia   compatibile  con  la  natura  peculiare  del  giudizio  di  &#13;
 legittimità  costituzionale  delle  norme  giuridiche.  Non  si   può  &#13;
 disconoscere   che  in  determinati  casi  un  giudizio  si  istituisca  &#13;
 inutiliter, in quanto la mancata impugnazione di norme precedenti renda  &#13;
 vana l'impugnazione di norme successive: un caso del  genere  si  trova  &#13;
 nella  sentenza  di  questa Corte n. 18 del 6 luglio 1956, con la quale  &#13;
 furono dichiarati inammissibili due ricorsi della Regione sarda per  il  &#13;
 fatto  che  l'impugnativa investiva norme statali che avevano carattere  &#13;
 strettamente conseguenziale di fronte a precedenti norme non impugnate.  &#13;
 Ma ammettere  tali  possibilità  non  significa  riconoscere  che  nei  &#13;
 giudizi  di  legittimità  costituzionale,  anche  se  proposti  in via  &#13;
 principale,   possano    avere    rilievo    istituti    come    quelli  &#13;
 dell'inammissibilità  del  ricorso per acquiescenza e per il carattere  &#13;
 confermativo del provvedimento impugnato, quali sono stati specialmente  &#13;
 elaborati nella giurisprudenza amministrativa.                           &#13;
     Nel caso di specie, poi, non  può  ravvisarsi  alcuna  ragione  di  &#13;
 inammissibilità  del  ricorso.  Anche  se  resta ferma la legittimità  &#13;
 costituzionale della norma precedente, della quale la  legge  impugnata  &#13;
 costituirebbe  interpretazione  autentica,  il giudizio di legittimità  &#13;
 della  seconda  legge  è  sempre  possibile  in quanto il fatto che la  &#13;
 Regione abbia, in virtù dei propri poteri legislativi e nei limiti  di  &#13;
 tali  poteri, emanato una certa norma, non può importare che la stessa  &#13;
 Regione possa  emanare  una  norma  successiva  nella  stessa  materia,  &#13;
 travalicando i limiti che è sempre tenuta a rispettare. Ora, l'oggetto  &#13;
 del  presente giudizio è questo: accertare se la legge impugnata resti  &#13;
 nella sfera dei poteri legislativi della Regione; e questa indagine non  &#13;
 può trovare ostacoli in precedenti disposizioni regionali nella stessa  &#13;
 materia.                                                                 &#13;
     Il primo motivo del ricorso è  infondato.  Questa  Corte,  con  la  &#13;
 sentenza  n.  9  del  17  gennaio  1957,  ha  riconosciuto  la potestà  &#13;
 legislativa della Regione siciliana in materia  tributaria  nei  limiti  &#13;
 derivanti  dal  carattere  concorrente  che  a  tale  potestà  risulta  &#13;
 attribuito. Del principio posto nella sentenza ora ricordata  la  Corte  &#13;
 ha  avuto  occasione  di  fare più di una applicazione, l'ultima delle  &#13;
 quali è rappresentata dalla sentenza n. 42 del giorno 1   marzo  1957,  &#13;
 con la quale è stata dichiarata non fondata la questione sollevata con  &#13;
 ordinanza  della  Commissione provinciale delle imposte di Agrigento in  &#13;
 ordine  alla  legittimità  costituzionale  dell'art.  2  della   legge  &#13;
 regionale 1  agosto 1953, n. 44.                                         &#13;
     Passando  all'esame  del secondo e del terzo motivo del ricorso, la  &#13;
 Corte osserva preliminarmente che non  sono  fondate  le  eccezioni  di  &#13;
 inammissibilità  proposte  dalla  difesa della Regione rispetto a tali  &#13;
 motivi. Come si è  riferito  nella  narrativa  di  fatto,  la  Regione  &#13;
 contesta l'ammissibilità dei due motivi, sia perché si sarebbe dovuto  &#13;
 impugnare  la  precedente  legge  regionale  del  1953, sia perché non  &#13;
 sussisterebbe un limite della potestà legislativa regionale in materia  &#13;
 tributaria derivante  dal  rispetto  dei  principi  della  legislazione  &#13;
 statale.  Queste due ragioni di inammissibilità (senza rilevare che la  &#13;
 seconda attiene al merito) sono contraddette  da  quanto  si  è  sopra  &#13;
 esposto circa l'eccezione di inammissibilità per acquiescenza e per il  &#13;
 carattere  confermativo  del provvedimento e da quanto la Corte ha già  &#13;
 deciso in ordine ai limiti della  potestà  legislativa  della  Regione  &#13;
 siciliana in materia tributaria.                                         &#13;
     Nel  merito  i  due  motivi  in  esame  sono infondati per ciò che  &#13;
 riguarda l'articolo primo della legge; sono fondati  per  quei  che  si  &#13;
 riferisce all'art. 2.                                                    &#13;
     È  da  premettere  che  per  definire la presente controversia non  &#13;
 occorre risolvere, nella loro  portata  di  massima,  alcune  questioni  &#13;
 prospettate  dalle  parti  circa  la  legittimità costituzionale delle  &#13;
 leggi statali e regionali di interpretazione autentica;  del  resto  lo  &#13;
 stesso  Avvocato  dello Stato, nella discussione orale, ha riconosciuto  &#13;
 che tale questione, se posta in termini generali, non ha rilevanza  nel  &#13;
 presente giudizio.                                                       &#13;
     Per   dimostrare   la  legittimità  dell'articolo  1  della  legge  &#13;
 regionale in esame,  basta  richiamare  le  considerazioni  esposte  da  &#13;
 questa  Corte  nella  sopra  citata  sentenza  l   marzo 1957 in ordine  &#13;
 all'art. 2 della legge  1    agosto  1953,  n.  44.  La  considerazione  &#13;
 decisiva   della   Corte  fu  tratta  dalla  constatazione  che  l'atto  &#13;
 aggiuntivo non poteva contenere una nuova manifestazione  di  volontà,  &#13;
 la  quale modificasse comunque il contenuto dell'atto originario, quale  &#13;
 fu voluto dalle parti e riconosciuto, al momento  della  registrazione,  &#13;
 dagli  uffici  finanziari;  ma  era  ammesso  soltanto per confermare e  &#13;
 precisare  la  stipulazione  precedente,  eliminando unicamente i dubbi  &#13;
 che,  dopo  avvenuta  la  registrazione  dell'atto,  qualche   clausola  &#13;
 generica potesse far sorgere sul reale contenuto di essa in difformità  &#13;
 di  quanto  fu  effettivamente  voluto  dalle parti.   In vista di tale  &#13;
 contenuto degli atti aggiuntivi, la Corte, anche in considerazione  del  &#13;
 carattere contingente e, sotto alcuni aspetti, eccezionale della norma,  &#13;
 e  dei fini specifici che essa si proponeva di raggiungere, ritenne che  &#13;
 la norma stessa non violasse principi generali  dell'ordinamento  dello  &#13;
 Stato né principi emergenti dalla legge di registro.                    &#13;
     L'art.  1  della  legge  del  1956  ora  in  esame non fa altro che  &#13;
 consentire una ulteriore precisazione, senza  spostare  per  niente  la  &#13;
 situazione.  Non sono permessi nuovi atti aggiuntivi; non sono concesse  &#13;
 nuove facilitazioni; non sono riaperti o prorogati termini al di là di  &#13;
 quelli fissati dalla legge regionale del 1953.  La norma  presuppone  e  &#13;
 conferma  rigorosamente la situazione quale era al momento in cui venne  &#13;
 emanata  la  legge  del  1953.  L'incertezza,  che  aveva   spinto   il  &#13;
 legislatore  regionale ad intervenire, derivava da due cause: o perché  &#13;
 nella  stipulazione  originaria  le  parti  non  si  erano  chiaramente  &#13;
 espresse  o  perché  nella  stessa stipulazione era stata aggiunta una  &#13;
 clausola   generica    che    avrebbe    travolto    la    possibilità  &#13;
 dell'applicazione  del beneficio fiscale. L'art. 1 della legge del 1956  &#13;
 nei suoi due commi contiene due disposizioni, entrambe poste in termini  &#13;
 disgiuntivi, al fine di chiarire che,  nell'una  e  nell'altra  ipotesi  &#13;
 considerata, deve valere ciò che l'atto aggiuntivo precisa e chiarisce  &#13;
 in rapporto alla originaria stipulazione.                                &#13;
     Ma,  si  ripete,  l'unico intento della norme del 1956 è quello di  &#13;
 ulteriormente  chiarire   la   situazione,   nel   suo   carattere   di  &#13;
 contingibilità  e  di  eccezionalità,  senza  consentire che assumano  &#13;
 rilevanza eventi nuovi. Anche l'accenno  a  successivi  fatti  concreti  &#13;
 ("la cessione. . . ebbe efficacia") non è destinato a dare efficacia a  &#13;
 tali  fatti,  ma  a  trarre  dai  fatti  stessi  elementi  di ulteriore  &#13;
 precisazione e chiarificazione della originaria stipulazione.            &#13;
     Quanto si è detto per dimostrare la non fondatezza delle questioni  &#13;
 di legittimità costituzionale dell'art. 1 della legge in esame vale  a  &#13;
 chiarire le ragioni della illegittimità dell'art. 2.                    &#13;
     Questa  disposizione  sarebbe  superflua  se non fosse illegittima.  &#13;
 Difatti, interpretandolo correttamente  in  relazione  alle  precedenti  &#13;
 leggi  regionali  del  1952 e del 1953 ed all'art. 1 della stessa legge  &#13;
 del 1956, l'art. 2 appare inutile, giacché l'art. 1 basta  a  chiarire  &#13;
 la  portata  e  gli  effetti  degli  atti  aggiuntivi  formati ai sensi  &#13;
 dell'art.  2 della legge regionale del 1953. Ma l'art.  2  della  legge  &#13;
 del  1956,  riferendosi  a  tutti  gli  atti  stipulati  nella  Regione  &#13;
 siciliana posteriormente al 1 luglio 1947, viene, in  primo  luogo,  ad  &#13;
 allargare  la piattaforma su cui poggiavano le disposizioni della legge  &#13;
 del 1953. Questo allargamento avrebbe potuto dar  luogo  ad  una  nuova  &#13;
 indagine  di  legittimità  costituzionale,  tendente a stabilire se le  &#13;
 stesse ragioni per le quali sono stati  ritenuti  non  illegittimi  gli  &#13;
 articoli  2 della legge del 1953 e 1 della legge del 1956 sorreggessero  &#13;
 la legittimità dell'art. 2 di questa ultima legge.  Ma  l'indagine  è  &#13;
 preclusa  dal  fatto  che  l'art. 2, secondo la sua chiara dizione, non  &#13;
 vuole regolare soltanto situazioni  disciplinate  da  leggi  regionali,  &#13;
 bensì anche situazioni previste da leggi statali.                       &#13;
     Ora,  non  è  ammissibile che la Regione regoli con una sua norma,  &#13;
 avente efficacia retroattiva, situazioni già disciplinate da una legge  &#13;
 statale. Il potere che, entro limiti più o meno ampi, ha la Regione di  &#13;
 dettare nuove e diverse norme nella stessa  materia  già  regolata  da  &#13;
 leggi  statali  non può riflettersi sul passato, vessendo ovvio che la  &#13;
 Regione non può annullare o togliere efficacia ad  atti  che  si  sono  &#13;
 compiuti  nell'ambito  del  suo territorio in base a leggi statali. Una  &#13;
 diversa  opinione  contrasterebbe  con  il  principio  ormai  pacifico,  &#13;
 secondo  cui  la  legge  statale entra in vigore e produce tutti i suoi  &#13;
 effetti nell'intero territorio dello Stato. Tali  effetti  non  possono  &#13;
 essere  paralizzati  da una legge regionale, senza violare il principio  &#13;
 fondamentale dell'unità dell'ordinamento giuridico dello Stato: unità  &#13;
 la quale, se consente che una nuova legge regionale deroghi, sempre nei  &#13;
 limiti consentiti, per l'avvenire ad una precedente legge statale,  non  &#13;
 tollera  che  la  legge regionale si sovrapponga con effetti ex tunc ad  &#13;
 una legge statale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1) respinge l'eccezione d'incompetenza della Corte a decidere sulla  &#13;
 controversia;                                                            &#13;
     2) respinge le eccezioni di inammissibilità proposte dalla Regione  &#13;
 siciliana;                                                               &#13;
     3) dichiara l'illegittimità costituzionale della  norma  contenuta  &#13;
 nell'art.  2  della  legge regionale approvata dall'Assemblea regionale  &#13;
 siciliana in data 3  ottobre  1956  recante  interpretazione  autentica  &#13;
 dell'art.  2 della legge regionale 1 agosto 1953, n. 44, in riferimento  &#13;
 agli artt. 17 e 36 dello Statuto speciale per la Sicilia;                &#13;
     4) respinge il ricorso predetto per quanto si riferisce all'art.  1  &#13;
 della stessa legge.                                                      &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 7 marzo 1957.                                 &#13;
                                   ENRICO DE NICOLA - GAETANO AZZARITI -  &#13;
                                   GIUSEPPE CAPPI  -  TOMASO  PERASSI  -  &#13;
                                   GASPARE     AMBROSINI    -    ERNESTO  &#13;
                                   BATTAGLINI   -   MARIO   COSATTI    -  &#13;
                                   FRANCESCO    PANTALEO    GABRIELI   -  &#13;
                                   GIUSEPPE CASTELLI AVOLIO  -  ANTONINO  &#13;
                                   PAPALDO  -  NICOLA  JAEGER - GIOVANNI  &#13;
                                   CASSANDRO - BIAGIO PETROCELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
