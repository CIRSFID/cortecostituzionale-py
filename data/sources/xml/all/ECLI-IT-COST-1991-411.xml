<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>411</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:411</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/11/1991</data_decisione>
    <data_deposito>12/11/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 555, terzo    &#13;
 comma, del codice di procedura penale, promosso con ordinanza  emessa    &#13;
 il  6  dicembre  1990  dal Pretore di Brescia - sezione distaccata di    &#13;
 Montichiari -  nel  procedimento  penale  a  carico  di  Guida  Rosa,    &#13;
 iscritta  al  n.  328  del registro ordinanze 1991 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  21,  prima  serie  speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di consiglio del  9  ottobre  1991  il  Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto  che  il  Pretore  di  Brescia  -  sezione  distaccata di    &#13;
 Montichiari - ha  sollevato,  con  ordinanza  del  6  dicembre  1990,    &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3    &#13;
 e  76  della  Costituzione, dell'art. 555, terzo comma, del codice di    &#13;
 procedura penale, secondo cui il decreto di citazione a giudizio  "è    &#13;
 notificato  all'imputato  e  al  suo  difensore almeno quarantacinque    &#13;
 giorni prima della data fissata per il giudizio";                        &#13;
      che, ad avviso del remittente, la norma, nel fissare un  termine    &#13;
 per  comparire  di  gran  lunga  maggiore  di quello - venti giorni -    &#13;
 previsto dagli artt. 429, terzo e quarto comma, e 456,  terzo  comma,    &#13;
 del  codice  per  il  giudizio di tribunale, violerebbe sia l'art. 76    &#13;
 della Costituzione, per contrasto  con  il  principio  della  massima    &#13;
 semplificazione  di cui alla direttiva n. 103 della legge-delega, sia    &#13;
 l'art. 3 della Costituzione stessa,  per  irrazionale  disparità  di    &#13;
 trattamento tra giudizio di tribunale e giudizio pretorile;              &#13;
     che,   in   particolare,  il  giudice  a  quo  osserva  che  tale    &#13;
 differenza, contrariamente a  quanto  affermato  nella  relazione  al    &#13;
 progetto  preliminare, non è giustificabile né dalla presenza di un    &#13;
 termine intermedio (15 giorni dalla notifica) previsto dall'art. 555,    &#13;
 primo comma, lett. e) per la richiesta  da  parte  dell'imputato  del    &#13;
 giudizio  abbreviato o dell'applicazione della pena o dell'oblazione,    &#13;
 giacché il  termine  ordinario  di  venti  giorni  sarebbe  comunque    &#13;
 superiore   al  termine  per  chiedere  il  rito  speciale  e  quindi    &#13;
 compatibile con esso, né dall'intento di lasciare  ai  difensori  un    &#13;
 ampio  margine  per  il  primo  esame  del  caso,  data  la  mancanza    &#13;
 dell'udienza preliminare, risultando la norma anche in  tale  ipotesi    &#13;
 sproporzionata  allo  scopo, poiché in tribunale il termine di dieci    &#13;
 giorni per comparire all'udienza preliminare (art. 419),  sommato  ai    &#13;
 venti per comparire al dibattimento, non raggiunge che i due terzi di    &#13;
 quello stabilito per comparire in pretura;                               &#13;
      che  è  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 ministri, concludendo per l'infondatezza della questione;                &#13;
    Considerato che,  quanto  alla  dedotta  violazione  della  legge-delega,  la  direttiva n. 103 lascia al legislatore delegato un ampio    &#13;
 spazio di discrezionalità in ordine alla disciplina  delle  concrete    &#13;
 modalità  di funzionamento del processo pretorile (cfr. ord.  n. 208    &#13;
 del 1991);                                                               &#13;
      che, nel caso in  esame,  tale  discrezionalità  non  è  stata    &#13;
 certamente  adoperata  in  modo  irragionevole, in quanto la maggiore    &#13;
 lunghezza del termine dilatorio in esame rispetto  a  quello  fissato    &#13;
 per il giudizio dinanzi al tribunale è, in primo luogo, giustificata    &#13;
 dal  fatto  che,  in  ragione  delle  particolari caratteristiche del    &#13;
 decreto di citazione a  giudizio,  quale  atto  complesso  capace  di    &#13;
 produrre effetti diversi ed alternativi (caratteristiche a loro volta    &#13;
 derivanti  dalle peculiarità del processo pretorile, nel quale manca    &#13;
 l'udienza preliminare),  è  solo  successivamente  alla  infruttuosa    &#13;
 scadenza  del termine intermedio di 15 giorni dalla notificazione del    &#13;
 decreto all'imputato che il pubblico  ministero  -  a  differenza  di    &#13;
 quanto previsto nel procedimento dinanzi al tribunale - deve compiere    &#13;
 una  serie  di  adempimenti,  fra  i quali la citazione della persona    &#13;
 offesa (v. art. 558 c.p.p.);                                             &#13;
      che, inoltre, e soprattutto,  la  norma  impugnata  è  ispirata    &#13;
 dall'evidente  fine  di favorire (mediante la messa a disposizione di    &#13;
 un congruo spatium deliberandi successivo al primo esame degli  atti)    &#13;
 il ricorso da parte dell'imputato all'istituto dell'applicazionedella    &#13;
 pena  su  richiesta,  sempre  possibile  fino  alla  dichiarazione di    &#13;
 apertura del dibattimento: la  norma  stessa  costituisce,  pertanto,    &#13;
 espressione  di  quel  favor per i riti differenziati, alternativi al    &#13;
 dibattimento, la cui incentivazione mira in definitiva  a  perseguire    &#13;
 proprio  quella  finalità  di  massima  semplificazione invocata dal    &#13;
 remittente (cfr. citata ord. n. 208 del 1991);                           &#13;
      che le anzidette considerazioni valgono chiaramente ad escludere    &#13;
 anche la dedotta violazione del principio di eguaglianza;                &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 555, terzo comma, del  codice  di  procedura    &#13;
 penale,   sollevata,   in   riferimento  agli  artt.  3  e  76  della    &#13;
 Costituzione,  dal  Pretore  di  Brescia  -  sezione  distaccata   di    &#13;
 Montichiari, con l'ordinanza in epigrafe.                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 novembre 1991.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 12 novembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
