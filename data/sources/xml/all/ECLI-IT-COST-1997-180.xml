<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>180</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:180</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/06/1997</data_decisione>
    <data_deposito>13/06/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 11,  comma  26,    &#13;
 della  legge  24  dicembre  1993,  n.  537  (Interventi correttivi di    &#13;
 finanza pubblica), promosso con ordinanza emessa il 9 aprile 1996 dal    &#13;
 pretore di Rieti sui ricorsi riuniti proposti da  Marchetti  Enzo  ed    &#13;
 altri contro l'ENPAV, iscritta al n. 1119 del registro ordinanze 1996    &#13;
 e  pubblicata  nella Gazzetta Ufficiale della Repubblica n. 42, prima    &#13;
 serie speciale, dell'anno 1996;                                          &#13;
   Visto  l'atto  di  costituzione  dell'ENPAV   nonché   l'atto   di    &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
   Udito  nella  camera  di  consiglio  del  21 maggio 1997 il giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
   Ritenuto che  nel  corso  di  un  giudizio  in  cui  i  ricorrenti,    &#13;
 veterinari iscritti all'albo prima dell'entrata in vigore della legge    &#13;
 12    aprile   1991,   n.   136,   avevano   chiesto   l'accertamento    &#13;
 dell'inesistenza  del  loro  obbligo   contributivo   nei   confronti    &#13;
 dell'ENPAV,  sancito dall'art.  11, comma 26, della legge 24 dicembre    &#13;
 1993, n. 537 (Interventi correttivi di finanza pubblica), il  pretore    &#13;
 di  Rieti,  con  ordinanza emessa il 9 aprile 1996, ha sollevato - in    &#13;
 riferimento agli artt. 3, 38 e 97 della Costituzione -  questione  di    &#13;
 legittimità costituzionale di tale norma nella parte in cui prevede,    &#13;
 interpretando l'art. 32 della legge n. 136 del 1991, che l'iscrizione    &#13;
 all'ENPAV e la relativa contribuzione siano obbligatorie soltanto per    &#13;
 i veterinari iscritti all'albo anteriormente all'entrata in vigore di    &#13;
 detta legge;                                                             &#13;
     che,  a  parere del rimettente, la norma violerebbe "il principio    &#13;
 ontologico del diritto" di irretroattività della legge prevedendo il    &#13;
 ripristino dell'obbligo di iscrizione all'ENPAV e così  contrastando    &#13;
 per  l'irragionevolezza  con  l'art. 3 della Costituzione, richiamato    &#13;
 anche quanto a disparità tra veterinari liberi professionisti da  un    &#13;
 lato e lavoratori dipendenti dall'altro, penalizzati questi ultimi da    &#13;
 una  doppia  contribuzione,  lesiva  dell'art.  38 della Costituzione    &#13;
 (poiché  creativa  di  oneri  superiori  a  quelli   necessari   per    &#13;
 assicurare "mezzi adeguati");                                            &#13;
     che, secondo il giudice a quo sarebbe infine vulnerato l'art.  97    &#13;
 della  Costituzione  in  quanto, assoggettandosi a contribuzione solo    &#13;
 alcuni  lavoratori  iscritti   all'albo,   risulterebbe   compromessa    &#13;
 l'imparzialità della pubblica amministrazione;                          &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato e difeso  dall'Avvocatura  dello  Stato,  il  quale  ha    &#13;
 chiesto   dichiararsi  la  manifesta  infondatezza  della  questione,    &#13;
 richiamandosi alla sentenza di questa Corte n. 88 del 1995;              &#13;
     che  nel  medesimo  senso  ha  concluso  l'ENPAV, intervenuto nel    &#13;
 giudizio dinanzi a questa Corte.                                         &#13;
   Considerato che questa Corte ha già ritenuto non fondata,  con  la    &#13;
 sentenza  n. 88 del 1995 (anche in riferimento ad ulteriori parametri    &#13;
 rispetto a quelli evocati dal giudice  rimettente)  e  manifestamente    &#13;
 infondata  con  ordinanze  nn.  252  e  455  del  1995,  la questione    &#13;
 sollevata dal pretore di Rieti;                                          &#13;
     che quest'ultimo non offre  argomenti  ulteriori  o  diversi  né    &#13;
 profili  di  censure  nuovi  rispetto a quelli esaminati nelle citate    &#13;
 decisioni, tutte peraltro anteriori all'emissione  dell'ordinanza  di    &#13;
 rimessione ma non considerate dal giudice a quo;                         &#13;
     che pertanto la questione va dichiarata manifestamente infondata.    &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 11, comma 26, della legge 24 dicembre  1993,    &#13;
 n.  537  (Interventi  correttivi  di finanza pubblica), sollevata, in    &#13;
 riferimento agli artt. 3, 38 e 97 della Costituzione, dal pretore  di    &#13;
 Rieti.                                                                   &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 5 giugno 1997.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 13 giugno 1997.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
