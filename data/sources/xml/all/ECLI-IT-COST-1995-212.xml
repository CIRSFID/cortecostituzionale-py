<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>212</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:212</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/05/1995</data_decisione>
    <data_deposito>31/05/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  58, ultimo    &#13;
 comma, seconda parte, della legge 8 giugno 1990, n. 142  (Ordinamento    &#13;
 delle  autonomie  locali); dell'art. 1, comma 6, del decreto-legge 27    &#13;
 agosto 1993, n. 324 (Proroga dei termini di durata  in  carica  degli    &#13;
 amministratori  straordinari  delle UU.SS.LL., ecc.), convertito, con    &#13;
 modificazioni, nella legge 27 ottobre  1993,  n.  423;  dell'art.  3,    &#13;
 comma  1, del decreto-legge 15 novembre 1993, n. 453 (Disposizioni in    &#13;
 materia di  giurisdizione  e  controllo  della  Corte  dei  conti)  e    &#13;
 dell'art.  1  della  legge  14  gennaio  1994, n. 20 (Disposizioni in    &#13;
 materia di giurisdizione e controllo della Corte dei conti), promossi    &#13;
 con due ordinanze emesse il 12 novembre 1993 e  il  14  gennaio  1994    &#13;
 dalla  Corte  dei  conti,  I  Sezione giurisdizionale, nei giudizi di    &#13;
 responsabilità promossi dal procuratore generale  nei  confronti  di    &#13;
 Zanoni Emilio ed altri e di Contestabile Fausto ed altri, iscritte ai    &#13;
 nn. 183 e 772 del registro ordinanze 1994 e pubblicate nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  nn. 15, prima serie speciale, dell'anno    &#13;
 1994 e 3, prima serie speciale, dell'anno 1995;                          &#13;
    Visto l'atto di costituzione di Contestabile Fausto ed altri;         &#13;
    Udito nell'udienza pubblica del 4 aprile 1995 il Giudice  relatore    &#13;
 Massimo Vari;                                                            &#13;
    Ritenuto  che,  con  ordinanza  del  12  novembre  1993  (registro    &#13;
 ordinanze n. 183 del 1994),  emessa  nel  corso  di  un  giudizio  di    &#13;
 responsabilità  contro  Emilio  Zanoni  ed altri, amministratori del    &#13;
 Comune di Cremona, la Corte dei conti ha  sollevato,  in  riferimento    &#13;
 agli  artt.  3  e  97  della  Costituzione, questione di legittimità    &#13;
 costituzionale dell'art. 58, ultimo comma, seconda parte, della legge    &#13;
 8  giugno  1990,  n.  142,  nonché  dell'art.  1,   comma   6,   del    &#13;
 decreto-legge  27 agosto 1993, n. 324, convertito, con modificazioni,    &#13;
 nella legge 27 ottobre 1993, n. 423  e  dell'art.  3,  comma  1,  del    &#13;
 decreto-legge 15 novembre 1993, n. 453;                                  &#13;
      che,   a   giudizio   del   remittente,   le   norme   impugnate    &#13;
 nell'escludere o, quantomeno, nel limitare la  trasmissibilità  agli    &#13;
 eredi  della  responsabilità  del  loro dante causa si porrebbero in    &#13;
 contrasto, oltre che con  l'art.  3  della  Costituzione,  anche  con    &#13;
 l'art.  97 della Costituzione, non apparendo conformi al principio di    &#13;
 buon  andamento  dell'amministrazione   disposizioni,   come   quelle    &#13;
 impugnate,    che    consentono    un   ingiustificato   esonero   da    &#13;
 responsabilità  con  danno  per   l'erario,   oppure,   come   nella    &#13;
 fattispecie,  fanno  discendere  (non per mera accidentalità, ma per    &#13;
 una precisa scelta) dalla morte di uno dei corresponsabili in  solido    &#13;
 un    aggravamento    della    posizione    debitoria   degli   altri    &#13;
 corresponsabili;                                                         &#13;
      che con altra ordinanza emessa  il  14  gennaio  1994  (registro    &#13;
 ordinanze   n.   772   del   1994)   nel  corso  di  un  giudizio  di    &#13;
 responsabilità contro Contestabile Fausto ed  altri,  amministratori    &#13;
 del  Comune  di Pornassio (Imperia), la Corte dei conti ha nuovamente    &#13;
 sollevato, in riferimento agli artt. 3, 24 e 97  della  Costituzione,    &#13;
 questione di legittimità                                                &#13;
 costituzionale dell'art. 58, ultimo comma, seconda parte, della legge    &#13;
 8  giugno  1990,  n.  142; dell'art. 1, comma 6, del decreto-legge 27    &#13;
 agosto 1993, n. 324 (convertito, con modificazioni,  nella  legge  27    &#13;
 ottobre  1993,  n.  423);  dell'art. 3, comma 1, del decreto-legge 15    &#13;
 novembre 1993, n. 453; nonché dell'art. 1  della  legge  14  gennaio    &#13;
 1994, n. 20;                                                             &#13;
      che il remittente ha lamentato violazione:                          &#13;
       dell'art.  3 della Costituzione, "per disparità di trattamento    &#13;
 fra eredi di amministratori e dipendenti pubblici e quelli  privati",    &#13;
 nonché  fra  "creditori degli amministratori e dipendenti pubblici e    &#13;
 altri creditori";                                                        &#13;
       dell'art.   97   della   Costituzione,   sotto    il    profilo    &#13;
 dell'"imparzialità"   e  del  "buon  andamento",  per  l'  "indubbia    &#13;
 incoerenza nell'assetto organizzativo dello Stato-persona"  provocato    &#13;
 dalla    previsione    di    questo    "ingiustificato   esonero   da    &#13;
 responsabilità";                                                        &#13;
       dell'art. 24 della Costituzione, per il "sostanziale sacrificio    &#13;
 al diritto di difesa giudiziale delle ragioni dello Stato-persona";      &#13;
      che nel primo giudizio nessuna parte si  è  costituita,  mentre    &#13;
 nel  secondo  si  sono  costituite  le parti private, rappresentate e    &#13;
 difese dagli avvocati Guido Romanelli e Giuliano Gallanti,  chiedendo    &#13;
 una declaratoria di infondatezza della questione;                        &#13;
    Considerato  che  i giudizi, concernenti le medesime disposizioni,    &#13;
 possono essere riuniti e congiuntamente decisi;                          &#13;
      che la questione di legittimità costituzionale,  per  risultare    &#13;
 ammissibile, esige la puntuale individuazione, da parte del giudice a    &#13;
 quo,  della  disposizione da applicare alla fattispecie sottoposta al    &#13;
 suo  esame,  onde  consentire  alla  Corte  le  valutazioni  di   sua    &#13;
 competenza anche in ordine alla rilevanza della questione, così come    &#13;
 proposta dal remittente;                                                 &#13;
      che  entrambe  le  ordinanze  in  epigrafe, denunciando le varie    &#13;
 disposizioni che, in tempi successivi, hanno disciplinato la materia,    &#13;
 non hanno tuttavia individuato quella, fra  di  esse,  specificamente    &#13;
 applicabile,  in  base  ai  principi  di  diritto intertemporale, nei    &#13;
 giudizi a quibus;                                                        &#13;
      che,  pertanto,  le  questioni,  nei  termini  in  cui   vengono    &#13;
 prospettate,   vanno   dichiarate  manifestamente  inammissibili  per    &#13;
 incerta individuazione della norma applicabile alla fattispecie.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  manifestamente   inammissibile   le    &#13;
 questioni di legittimità costituzionale:                                &#13;
      dell'art.  58, ultimo comma, seconda parte, della legge 8 giugno    &#13;
 1990, n. 142 (Ordinamento delle autonomie locali); dell'art. 1, comma    &#13;
 6, del decreto-legge 27 agosto 1993, n. 324 (Proroga dei  termini  di    &#13;
 durata  in  carica degli amministratori straordinari delle UU.SS.LL.,    &#13;
 ecc.), convertito, con modificazioni, nella legge 27 ottobre 1993, n.    &#13;
 423 e dell'art. 3, comma 1, del decreto-legge 15  novembre  1993,  n.    &#13;
 453 (Disposizioni in materia di giurisdizione e controllo della Corte    &#13;
 dei   conti),   sollevata   dalla   Corte   dei  conti  -  I  Sezione    &#13;
 giurisdizionale, in riferimento agli artt. 3 e 97 della Costituzione,    &#13;
 con ordinanza del 12 novembre 1993;                                      &#13;
      dell'art. 58, ultimo comma, seconda parte, della legge 8  giugno    &#13;
 1990, n. 142 (Ordinamento delle autonomie locali); dell'art. 1, comma    &#13;
 6,  del  decreto-legge 27 agosto 1993, n. 324 (Proroga dei termini di    &#13;
 durata in carica degli amministratori straordinari  delle  UU.SS.LL.,    &#13;
 ecc.), convertito, con modificazioni, nella legge 27 ottobre 1993, n.    &#13;
 423; dell'art. 3, comma 1, del decreto-legge 15 novembre 1993, n. 453    &#13;
 (Disposizioni in materia di giurisdizione e controllo della Corte dei    &#13;
 conti);  nonché  dell'art.  1  della  legge  14  gennaio 1994, n. 20    &#13;
 (Disposizioni in materia di giurisdizione e controllo della Corte dei    &#13;
 conti),  sollevata dalla Corte dei conti - I Sezione giurisdizionale,    &#13;
 con ordinanza del 14 gennaio 1994, in riferimento agli artt. 3, 24  e    &#13;
 97 della Costituzione.                                                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 29 maggio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                          Il redattore: VARI                              &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 31 maggio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
