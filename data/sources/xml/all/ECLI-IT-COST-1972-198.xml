<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>198</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:198</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1972</data_decisione>
    <data_deposito>29/12/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Prof. &#13;
 COSTANTINO MORTATI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA &#13;
 BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - &#13;
 Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - &#13;
 Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. &#13;
 NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. GIULIO &#13;
 GIONFRIDA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale dell'art.  9,  &#13;
 secondo comma, del r.d.l.  20  luglio  1934,  n.  1404  (istituzione  e  &#13;
 funzionamento  del tribunale per i minorenni), promossi con le seguenti  &#13;
 ordinanze:                                                               &#13;
     1) ordinanza emessa il 14 aprile 1971 dal tribunale di Trieste  nel  &#13;
 procedimento penale a carico di Donaggio Bruno ed altri, iscritta al n.  &#13;
 209  del  registro ordinanze 1971 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 170 del 7 luglio 1971;                               &#13;
     2) ordinanza emessa il 5 maggio 1971 dal tribunale di  Venezia  nel  &#13;
 procedimento  penale a carico di Pierdicchi Maurizio ed altri, iscritta  &#13;
 al n. 224 del registro  ordinanze  1971  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 177 del 14 luglio 1971.                    &#13;
     Udito  nella  camera  di  consiglio  del 26 ottobre 1972 il Giudice  &#13;
 relatore Giovanni Battista Benedetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con  ordinanza  14  aprile  1971  emessa  nel  corso  di  un  &#13;
 procedimento  penale  a  carico del minore Donaggio Bruno, imputato del  &#13;
 reato di furto aggravato continuato, ed  a  carico  di  altri  soggetti  &#13;
 maggiori  degli  anni 18, imputati del reato di ricettazione delle cose  &#13;
 dal primo sottratte, il tribunale di Trieste ha sollevato la  questione  &#13;
 di  legittimità costituzionale, in riferimento agli artt. 3 e 25 della  &#13;
 Costituzione,  della disposizione contenuta nell'art. 9, secondo comma,  &#13;
 del r.d.l. 20 luglio 1934, n. 1404, la quale stabilisce lo  spostamento  &#13;
 della  competenza  del tribunale per i minorenni al tribunale ordinario  &#13;
 ogni qualvolta in un procedimento a carico di minori degli anni  18  vi  &#13;
 siano  coimputati  maggiori  di tale età.   Nella propria ordinanza il  &#13;
 giudice a quo ricorda che la Corte, con sentenza n.  10  del  1966,  ha  &#13;
 escluso  il  contrasto  tra l'art. 9 del citato r.d.l. e l'art. 3 della  &#13;
 Costituzione nel caso in cui il minore sia  imputato  in  concorso  con  &#13;
 persona  maggiore  degli  anni  18,  nel  rilievo  che  l'unicità  del  &#13;
 procedimento è giustificata dall'esigenza di uniformità del  giudizio  &#13;
 sull'accertamento e sulla valutazione del fatto.  Questa esigenza però  &#13;
 non  ricorre  nella  ipotesi  in cui il minore abbia commesso, come nel  &#13;
 caso di specie, un reato distinto e diverso da quello ascritto ad altri  &#13;
 imputati maggiorenni. In tale ultimo caso, in cui la contraddittorietà  &#13;
 dei giudicati potrebbe comunque  essere  evitata  facendo  applicazione  &#13;
 dell'istituto  delle  pregiudiziali  (art. 18 c.p.p.), appare opportuno  &#13;
 che l'autonoma azione del minore venga valutata ad opera del  tribunale  &#13;
 per i minorenni composto da elementi dotati dei requisiti necessari per  &#13;
 procedere a tale valutazione.                                            &#13;
     L'obbligatorio  ed  inevitabile  spostamento di competenza previsto  &#13;
 dalla norma impugnata si  pone  quindi  in  contrasto,  ad  avviso  del  &#13;
 giudice  a  quo,  sia con l'art. 3 della Costituzione poiché determina  &#13;
 una disparità di trattamento in situazioni di fatto che sono,  invece,  &#13;
 sostanzialmente  uguali,  sia  con l'art. 25 della Costituzione poiché  &#13;
 sottrae al giudizio del tribunale per i minorenni il minore  coimputato  &#13;
 con  maggiorenni  anche  in ipotesi in cui ciò non appare giustificato  &#13;
 dal rispetto di superiori esigenze dell'economia del giudizio.           &#13;
     2. - L'art. 9, comma secondo, del r.d.l. 20 luglio 1934, n.   1404,  &#13;
 -  è  stato  denunciato  come costituzionalmente illegittimo anche dal  &#13;
 tribunale  di  Venezia,  con  ordinanza  5  maggio  1971   emessa   nel  &#13;
 procedimento  penale  a  carico di tre minori un maggiore degli anni 18  &#13;
 tutti imputati di concorso  nello  stesso  reato  di  furto  aggravato.  &#13;
 L'eccezione  è  stata  proposta  soltanto  nei confronti dell'art. 24,  &#13;
 secondo comma, della Costituzione ed in proposito il  tribunale  rileva  &#13;
 che  nel  concetto di diritto di difesa e di processo giusto rientra la  &#13;
 struttura particolare dell'organo giudicante;  da  ciò  l'esigenza  di  &#13;
 garantire  ai minori - a prescindere da ogni relazione di connessione -  &#13;
 il diritto di essere giudicati da un tribunale che  possa  valutare  in  &#13;
 pieno  la personalità e la responsabilità con la sensibilità e con i  &#13;
 mezzi, anche procedurali, forniti  dalla  legge  e  non  consentiti  al  &#13;
 giudice ordinario.                                                       &#13;
     Nel giudizio dinanzi a questa Corte nessuno si è costituito.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.   -  Le  due  ordinanze  propongono  questioni  di  legittimità  &#13;
 costituzionale dell'art. 9, comma secondo, del r.d.l. 20  luglio  1934,  &#13;
 n.  1404,  che esclude dalla competenza del tribunale per i minorenni i  &#13;
 procedimenti penali per i reati  commessi  dai  minori  degli  anni  18  &#13;
 quando  in  essi  vi siano coimputati maggiori di tale età. I relativi  &#13;
 giudizi, pertanto, possono essere riuniti e decisi con unica sentenza.   &#13;
     2. - Il tribunale di Trieste ha emesso  la  propria  ordinanza  nel  &#13;
 corso di un procedimento penale in cui sono stati portati a giudizio un  &#13;
 minore,  sotto  l'imputazione  di  furto  aggravato, ed alcuni maggiori  &#13;
 degli anni 18 chiamati a rispondere del  delitto  di  ricettazione  per  &#13;
 aver  ricevuto  dal  primo  parte  delle  cose  che  si assumono da lui  &#13;
 sottratte.                                                               &#13;
     Rileva il giudice a quo che la precedente sentenza (n. 10 del 1966)  &#13;
 con la quale la Corte costituzionale ebbe ad escludere il contrasto tra  &#13;
 l'art.  9 cpv. del r.d.l. n. 1404 del 1934 e l'art. 3 Cost. si riferiva  &#13;
 ad ipotesi in cui imputati minori avessero commesso lo stesso reato  in  &#13;
 concorso  con maggiorenni e non anche al caso, come quello in esame, in  &#13;
 cui l'imputato minore ha commesso un reato distinto  e  non  legato  da  &#13;
 vincoli di compartecipazione con quello ascritto ai maggiori degli anni  &#13;
 18.                                                                      &#13;
     L'esigenza  di  evitare  la  contraddittorietà  dei giudicati e di  &#13;
 valutare unitariamente il fatto, che la Corte ha giustamente addotto  a  &#13;
 motivo  della  sua  precedente  decisione,  non  ricorrerebbe  nel caso  &#13;
 sottoposto al giudizio del tribunale in cui l'azione del  minore  degli  &#13;
 anni  18, proprio in ragione della sua autonomia e diversità da quella  &#13;
 dei maggiori di tale età, è giusto che sia valutata nell'adatta  sede  &#13;
 ad  opera  di  quell'organo  composto  da elementi dotati dei requisiti  &#13;
 necessari per procedere a tale valutazione.                              &#13;
     3. - La questione è fondata.                                        &#13;
     Nel primo comma dell'art. 9 della legge  istitutiva  del  tribunale  &#13;
 per  i minorenni viene enunciata la regola generale della competenza di  &#13;
 detto tribunale per tutti i procedimenti penali "per reati commessi dai  &#13;
 minori degli anni 18"; nel capoverso dello stesso articolo è  prevista  &#13;
 l'eccezione  a tale regola per il caso in cui "nel procedimento vi sono  &#13;
 coimputati maggiori degli anni 18".                                      &#13;
     Evidente è la stretta relazione che intercorre  tra  le  locuzioni  &#13;
 "reati commessi dai minori" e "coimputati maggiori".                     &#13;
     L'ipotesi  prevista  è soltanto quella della compartecipazione del  &#13;
 maggiore allo stesso reato compiuto dal  minore:  il  caso  tipico  del  &#13;
 concorso di più persone nel medesimo reato. Nella previsione normativa  &#13;
 non rientrano altre forme di connessione.                                &#13;
     Questa  interpretazione  è  d'altronde  giustificata  dallo stesso  &#13;
 contenuto della disposizione in esame. Essa prevede una eccezione  alla  &#13;
 generale competenza del tribunale per i minorenni ed in tema di deroghe  &#13;
 non sono consentite interpretazioni estensive.                           &#13;
     Diversa è, però, l'interpretazione data dalla giurisprudenza alla  &#13;
 norma impugnata.                                                         &#13;
     Al  termine coimputato è stato infatti costantemente attribuito un  &#13;
 significato ampio e generico in modo da ricomprendervi non  solo  colui  &#13;
 che  è  imputato di concorso nel reato commesso da altri contro cui si  &#13;
 procede, ma anche colui che è imputato di un reato connesso  a  quello  &#13;
 per il quale si procede a carico di altri. Ed è stato conseguentemente  &#13;
 affermato che la competenza del giudice ordinario sussiste non solo nel  &#13;
 caso  in  cui debba procedersi per un reato commesso da un minore degli  &#13;
 anni 18 e un maggiore in concorso tra loro, ma anche in ogni altro caso  &#13;
 di connessione di procedimenti.                                          &#13;
     Così interpretata ed applicata  la  disposizione  denunciata  vive  &#13;
 nella   realtà   concreta  in  modo  incompatibile  col  principio  di  &#13;
 uguaglianza enunciato dall'art. 3 della Costituzione                     &#13;
     La necessità del simultanens processus, che  la  Corte  nella  sua  &#13;
 precedente  decisione  ha  posto  a  giustificazione  della deroga alla  &#13;
 competenza del tribunale per i minorenni per l'ipotesi di  procedimenti  &#13;
 contro  minori  e  maggiori  coimputati dello stesso reato, non ricorre  &#13;
 quando il reato commesso dal minore  -  come  nel  caso  sottoposto  al  &#13;
 giudizio  del tribunale - sia distinto e diverso da quello compiuto dal  &#13;
 maggiore  degli  anni 18, anche se fra tali reati sussista connessione.  &#13;
 Non v'è sostanziale differenza tra questa  seconda  ipotesi  e  quella  &#13;
 relativa ad un minore che commetta da solo un reato; in entrambi i casi  &#13;
 l'azione del minore ha un'autonomia tutta propria sicché si giustifica  &#13;
 l'identità della loro disciplina.                                       &#13;
     La  sussistenza  del denunciato contrasto con l'art. 3 Cost.  della  &#13;
 norma impugnata - nella parte in  cui  non  limita  la  competenza  del  &#13;
 giudice  ordinario  al caso di procedimenti nei quali minori e maggiori  &#13;
 degli anni 18 siano coimputati dello stesso reato - dispensa  la  Corte  &#13;
 dall'esame  dell'altro  motivo  di  incostituzionalità  prospettato in  &#13;
 riferimento all'art. 25 della Costituzione.                              &#13;
     4. - Il tribunale di Venezia ha invece emesso la propria  ordinanza  &#13;
 in  un procedimento penale instaurato a carico di tre minori degli anni  &#13;
 18 ed un maggiore di tale età, tutti imputati di concorso nello stesso  &#13;
 reato di furto aggravato  continuato,  ed  ha  ritenuto  che  la  norma  &#13;
 impugnata  sia  in  contrasto  con  l'art.  24,  comma  secondo,  della  &#13;
 Costituzione in quanto al concetto di diritto  di  difesa  non  sarebbe  &#13;
 estranea la struttura particolare dell'organo giudicante.                &#13;
     La questione non è fondata.                                         &#13;
     È  invero  di  tutta  evidenza  che  non  può essere lamentata la  &#13;
 lesione del  diritto  di  difesa  quando  viene  garantita  l'effettiva  &#13;
 possibilità di tutela delle proprie ragioni. La deroga alla competenza  &#13;
 del  tribunale  per  i  minorenni, disposta con la norma impugnata, non  &#13;
 preclude, né limita in alcun modo il diritto di  farsi  assistere  dal  &#13;
 difensore nel procedimento dinanzi al giudice comune.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   l'illegittimità   costituzionale   dell'art.  9,  comma  &#13;
 secondo, del  r.d.l.  20  luglio  1934,  n.  1404,  sull'istituzione  e  &#13;
 funzionamento  del  tribunale  per  i minorenni, nella parte in cui non  &#13;
 limita la deroga alla competenza del tribunale  per  i  minorenni  alla  &#13;
 sola  ipotesi  nella  quale  minori  e  maggiori  degli  anni  18 siano  &#13;
 coimputati dello stesso reato;                                           &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art.  9,  comma  secondo,  del  r.d.l.  20  luglio  1934, n. 1404,  &#13;
 sollevata,  in  riferimento   all'art.   24,   comma   secondo,   della  &#13;
 Costituzione, con ordinanza 5 maggio 1971 del tribunale di Venezia.      &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 dicembre 1972.       &#13;
                                   GIUSEPPE   CHIARELLI   -   COSTANTINO  &#13;
                                   MORTATI  - GIUSEPPE  VERZÌ - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - FRANCESCO  PAOLO  &#13;
                                   BONIFACIO - LUIGI OGGIONI - ANGELO DE  &#13;
                                   MARCO   -  ERCOLE  ROCCHETTI  -  ENZO  &#13;
                                   CAPALOZZA    -    VINCENZO    MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA.                    &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
