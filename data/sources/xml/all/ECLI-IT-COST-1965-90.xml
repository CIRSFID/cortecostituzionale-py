<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1965</anno_pronuncia>
    <numero_pronuncia>90</numero_pronuncia>
    <ecli>ECLI:IT:COST:1965:90</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Nicola Jaeger</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1965</data_decisione>
    <data_deposito>22/12/1965</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la Seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della  legge  approvata  &#13;
 dalla  Assemblea  regionale  siciliana  nella seduta del 24 marzo 1965,  &#13;
 recante: "Sgravi fiscali per le nuove costruzioni in Sicilia", promosso  &#13;
 con ricorso del Commissario  dello  Stato  per  la  Regione  siciliana,  &#13;
 notificato  il  1 aprile 1965, depositato nella cancelleria della Corte  &#13;
 costituzionale il 10 successivo  ed  iscritto  al  n.  4  del  Registro  &#13;
 ricorsi 1965.                                                            &#13;
     Visto   l'atto   di   costituzione  del  Presidente  della  Regione  &#13;
 siciliana;                                                               &#13;
     udita nell'udienza pubblica del 17 novembre 1965 la  relazione  del  &#13;
 Giudice Nicola Jaeger;                                                   &#13;
     uditi  il  sostituto avvocato generale dello Stato Pietro Peronaci,  &#13;
 per il Commissario dello Stato, e l'avv. Pietro Virga, per  la  Regione  &#13;
 siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ricorso notificato il 1 aprile 1965 al Presidente della Regione  &#13;
 siciliana  il  Commissario  dello  Stato  presso  la  Regione stessa ha  &#13;
 impugnato la legge approvata dalla Assemblea regionale siciliana  nella  &#13;
 seduta  del  24  marzo  1965,  recante:  "Sgravi  fiscali  per le nuove  &#13;
 costruzioni in Sicilia",  ed  ha  chiesto  che  ne  sia  dichiarata  la  &#13;
 illegittimità costituzionale.                                           &#13;
     Il  ricorrente  espone  che  tale  provvedimento  statuisce  che le  &#13;
 agevolazioni tributarie sui materiali impiegati nelle nuove costruzioni  &#13;
 edilizie, concesse dalla legge 18 ottobre 1954,  n.  37,  si  applicano  &#13;
 alle  costruzioni ultimate entro il 31 dicembre 1966, con una riduzione  &#13;
 del 95 per cento dell'aliquota dell'imposta per l'anno 1965  e  del  90  &#13;
 per cento per l'anno 1966.                                               &#13;
     La  citata  legge  regionale del 1954 concedeva l'esenzione fiscale  &#13;
 fino a tutto  il  31  dicembre  1957  per  la  costruzione  di  edifici  &#13;
 destinati    ad  abitazioni civili non aventi carattere di lusso oppure  &#13;
 destinati ad albergo, anche se comprendenti ambienti a piano terreno da  &#13;
 adibire  a  negozio  o  ad  altro  uso,  e  per  l'ampliamento   e   la  &#13;
 sopraelevazione  di  edifici  destinati  agli  stessi scopi. Il termine  &#13;
 suddetto era poi stato ulteriormente prorogato con leggi successive: n.  &#13;
 46 del 29 luglio 1957, n. 29 del 12  novembre  1959  e  n.  22  del  27  &#13;
 novembre  1961,  giungendo  fino  alla  data  del  31 dicembre 1965; ma  &#13;
 quest'ultima legge, impugnata in via  incidentale  davanti  alla  Corte  &#13;
 costituzionale,  era  stata  da  questa  dichiarata illegittima, con la  &#13;
 sentenza n. 2 del 22-26 gennaio 1965, in relazione agli artt. 36, 17  e  &#13;
 15 dello Statuto della Regione.                                          &#13;
     A  parere del ricorrente, anche la legge ora denunciata si discosta  &#13;
 dai criteri  seguiti  dal  legislatore  nazionale,  che,  nell'evidente  &#13;
 intento   di   salvaguardare  le  entrate  dei  Comuni,  ha  provveduto  &#13;
 gradualmente al ripristino dell'imposta, limitandone la riduzione  alla  &#13;
 misura del 20 per cento.                                                 &#13;
     Pertanto   la   legge   regionale   è   da   ritenere  viziata  di  &#13;
 incostituzionalità per violazione degli artt. 15 e  36  dello  Statuto  &#13;
 siciliano.                                                               &#13;
     Il  ricorso era depositato nella cancelleria della Corte in data 10  &#13;
 aprile, e poi pubblicato nel n. 98 del 17 aprile  1965  della  Gazzetta  &#13;
 Ufficiale  della  Repubblica  e  nel  n.   18 del 30 aprile detto nella  &#13;
 Gazzetta della Regione.                                                  &#13;
     Il  Presidente  della  Regione  si  è   costituito   in   giudizio  &#13;
 presentando  un controricorso, pervenuto alla cancelleria il 29 aprile,  &#13;
 nel quale si conclude perché la Corte costituzionale voglia dichiarare  &#13;
 "cessata la materia del contendere" e subordinatamente "respingere  (il  &#13;
 ricorso) perché inammissibile per acquiescenza" o quanto meno "perché  &#13;
 infondato",   dichiarando   costituzionalmente   legittima   la   legge  &#13;
 denunciata.                                                              &#13;
     A sostegno di tali conclusioni si osserva anzitutto  che  la  norma  &#13;
 della  legge  regionale aveva trovato applicazione per ben quattro anni  &#13;
 consecutivi, senza che il  Commissario  dello  Stato  avesse  sollevato  &#13;
 alcuna censura di incostituzionalità, tanto che la ricordata decisione  &#13;
 della  Corte  era  stata  emessa  in  sede  di giudizio incidentale, in  &#13;
 seguito ad una ordinanza del Tribunale di Patti.                         &#13;
     Si aggiunge che con la legge ora impugnata la Regione ha provveduto  &#13;
 - in  accoglimento  del  principio  fissato  in  tale  decisione  -  ad  &#13;
 accordare non già una esenzione totale, bensì solo una riduzione: del  &#13;
 95  per  cento per il 1965 e del 90 per cento per il 1966. Inoltre, con  &#13;
 un'altra legge, approvata dall'Assemblea regionale il 9 aprile 1965, si  &#13;
 è inteso provvedere "allo scopo di  far  fronte  alle  minori  entrate  &#13;
 derivanti  ai  Comuni  siciliani  dagli  sgravi  fiscali  per  le nuove  &#13;
 costruzioni edilizie", autorizzando  il  Presidente  della  Regione  "a  &#13;
 decurtare   i   crediti   verso   i   Comuni  medesimi,  relativi  alle  &#13;
 anticipazioni concesse ai sensi delle  leggi  vigenti  in  rapporto  al  &#13;
 minore   ammontare  delle  entrate  anzidette,  fino  alla  complessiva  &#13;
 concorrenza di un miliardo di lire".                                     &#13;
     Dalla circostanza che la nuova legge non  è  stata  impugnata  dal  &#13;
 Commissario  dello  Stato  la difesa della Regione arguisce che nessuna  &#13;
 censura si intende sollevare da parte dello Stato circa il modo con cui  &#13;
 la Regione ha garantito l'autonomia finanziaria dei  Comuni;  donde  si  &#13;
 traggono  le  conclusioni sopra riferite relative alla cessazione della  &#13;
 materia del contendere oppure alla acquiescenza.                         &#13;
     A tali argomentazioni ha risposto l'Avvocatura generale dello Stato  &#13;
 con una memoria depositata in cancelleria il  3  novembre  1965,  nella  &#13;
 quale  afferma  che  invece  la  legge regionale impugnata nel presente  &#13;
 giudizio si è  discostata  dal  principio  direttivo  enunciato  nella  &#13;
 sentenza   della   Corte,   corrispondente  al  metodo  adottato  dalla  &#13;
 legislazione dello Stato (art. 45 del  D.  L.15  marzo  1965,  n.  124,  &#13;
 ratificato  dalla  legge 13 maggio 1965, n. 431), inteso a ripristinare  &#13;
 l'imposizione. Si aggiunge che la legge regionale impugnata aggrava  la  &#13;
 situazione,  estendendo  gli  sgravi  fiscali anche alle costruzioni di  &#13;
 alberghi e accordando così una  agevolazione,  che  non  trova  alcuna  &#13;
 corrispondenza nella legislazione statale.                               &#13;
     Secondo  l'Avvocatura  generale  dello Stato neppure la nuova legge  &#13;
 regionale, approvata il 24 marzo 1965, può eliminare i vizi  contenuti  &#13;
 in  quella  precedente  rispetto all'autonomia finanziaria spettante ai  &#13;
 Comuni, cui  si  è  imposto  il  rimborso  delle  imposte  di  consumo  &#13;
 incassate medio tempore, mentre i presunti vantaggi, contenuti comunque  &#13;
 entro  un  limite  massimo, non possono giovare ai Comuni che non siano  &#13;
 debitori verso la  Regione.  Essa  insiste  perciò  nella  domanda  di  &#13;
 accoglimento del ricorso.                                                &#13;
     In  una  "breve memoria", pervenuta alla cancelleria il 3 novembre,  &#13;
 la difesa della Regione ribadisce le tesi già  sostenute,  richiamando  &#13;
 altre  disposizioni  legislative  posteriori, emanate dalla Regione (15  &#13;
 giugno 1965) e dallo Stato (13 maggio 1965, n. 431), per concludere che  &#13;
 non esistono differenze sostanziali in materia fra i due ordinamenti  e  &#13;
 che,  se  mai,  i Comuni hanno ottenuto maggiori vantaggi proprio dalle  &#13;
 norme emanate dalla Regione siciliana.                                   &#13;
     I difensori delle parti hanno confermato le proprie  conclusioni  e  &#13;
 gli  argomenti  dedotti a sostegno di esse alla pubblica udienza del 17  &#13;
 novembre 1965.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte ritiene fondato il ricorso proposto dal Commissario  dello  &#13;
 Stato  presso  la  Regione  siciliana  contro la legge, approvata nella  &#13;
 seduta del 24 marzo 1965 dell'Assemblea  regionale  siciliana,  recante  &#13;
 "Sgravi fiscali per le nuove costruzioni in Sicilia".                    &#13;
     Dalle  circostanze, esposte dalla Avvocatura generale dello Stato e  &#13;
 non contestate in linea di fatto dalla  difesa  della  Regione,  e  dal  &#13;
 confronto fra le norme statali e quelle contenute nella legge regionale  &#13;
 impugnata risulta in modo evidente che la Regione non si è adeguata in  &#13;
 alcun modo alla tipologia della legislazione statale in materia, né ha  &#13;
 tenuto  conto  dei  principi  richiamati  nella sentenza n. 2 del 22-28  &#13;
 gennaio 1965 della Corte costituzionale, con la quale venne  dichiarata  &#13;
 la  illegittimità  costituzionale  della  legge  regionale 27 novembre  &#13;
 1961, n. 22.                                                             &#13;
     Questa Corte non trova  nelle  deduzioni  difensive  esposte  dalla  &#13;
 Regione nel presente giudizio alcun motivo, che possa valere ad indurla  &#13;
 a  modificare  la  propria giurisprudenza in materia neppure per quanto  &#13;
 concerne il rispetto dell'autonomia  finanziaria  garantita  ai  Comuni  &#13;
 dallo  Statuto  regionale  e  l'adozione  di  un  serio  ristoro per la  &#13;
 cessazione di cespiti tributari ad essi spettanti.                       &#13;
     Né, d'altra parte, sembra possibile negare che il metodo  adottato  &#13;
 dalla  legislazione statale nel disposto dell'art. 45 del D. L.15 marzo  &#13;
 1965, n. 124, convertito  nella  legge  13  maggio  1965,  n.  431,  è  &#13;
 fondamentalmente  diverso  da  quello  seguito  dalla  legge  regionale  &#13;
 impugnata; e che, pertanto, anche nel caso in  esame  è  mancato  quel  &#13;
 coordinamento  tra  finanza  statale,  regionale  e comunale, sulla cui  &#13;
 necessità la Corte si è pronunciata ripetute volte, e da ultimo nella  &#13;
 ricordata sentenza n. 2 del 1965.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  illegittimità  costituzionale  della legge approvata  &#13;
 dalla Assemblea regionale siciliana nella seduta  del  24  marzo  1965,  &#13;
 recante:  "Sgravi  fiscali  per  le  nuove  costruzioni in Sicilia", in  &#13;
 riferimento agli artt. 15 e 36 dello Statuto della Regione siciliana.    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 14 dicembre 1965.                             &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
