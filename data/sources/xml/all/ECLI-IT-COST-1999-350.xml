<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>350</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:350</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/07/1999</data_decisione>
    <data_deposito>22/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale degli artt. 7,  comma  5,    &#13;
 8,  11,  12  e  21,  comma  1,  della  legge della Regione Siciliana,    &#13;
 approvata il 7 novembre 1997, recante "Norme per il  recupero  ed  il    &#13;
 completamento  delle  aree  artigianali  attrezzate realizzate a cura    &#13;
 della SIRAP S.p.A. e interpretazione  autentica  dell'art.  30  della    &#13;
 legge  regionale  4  aprile  1995,  n. 29. Disposizioni in materia di    &#13;
 agricoltura  e  foreste,  trasporti,  lavori  pubblici,   turismo   e    &#13;
 personale   dell'Amministrazione   regionale   e   delle   Camere  di    &#13;
 commercio", promosso con ricorso del Commissario dello Stato  per  la    &#13;
 Regione  Siciliana,  notificato  il  15  novembre 1997, depositato in    &#13;
 Cancelleria  il  25  successivo  ed  iscritto  al  n. 76 del registro    &#13;
 ricorsi 1997.                                                            &#13;
   Visto l'atto di costituzione della Regione Siciliana;                  &#13;
   Udito nell'udienza pubblica dell'8 giugno 1999 il giudice  relatore    &#13;
 Massimo Vari;                                                            &#13;
   Uditi l'Avvocato dello Stato Ignazio F. Caramazza per il Presidente    &#13;
 del  Consiglio  dei  Ministri  e  l'avvocato  Laura Ingargiola per la    &#13;
 Regione Siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ricorso  regolarmente  notificato  e   depositato,   il    &#13;
 Commissario   dello  Stato  per  la  Regione  Siciliana  ha  proposto    &#13;
 questione di legittimità costituzionale di varie disposizioni  della    &#13;
 legge approvata dall'Assemblea regionale siciliana nella seduta del 6    &#13;
 (recte:  7)  novembre 1997 (Norme per il recupero ed il completamento    &#13;
 delle aree artigianali  attrezzate  realizzate  a  cura  della  SIRAP    &#13;
 S.p.A. e interpretazione autentica dell'art. 30 della legge regionale    &#13;
 4  aprile  1995,  n.  29.  Disposizioni  in  materia di agricoltura e    &#13;
 foreste,   trasporti,   lavori   pubblici,   turismo   e    personale    &#13;
 dell'Amministrazione regionale e delle Camere di commercio).             &#13;
   2.  -  Oggetto  di  denuncia,  da  parte  del  ricorrente,  sono in    &#13;
 particolare:                                                             &#13;
     l'art. 8, il quale, assumendo le  connotazioni  della  "norma  di    &#13;
 mera  sanatoria",  verrebbe  ad  ampliare  "oltremodo" la sfera degli    &#13;
 originari destinatari dell'art. 30 della legge regionale  n.  29  del    &#13;
 1995,  con  riguardo all'assegnazione di lotti nelle aree di sviluppo    &#13;
 industriale, così violando gli artt. 3 e 97 della Costituzione;         &#13;
     l'art. 11, che, nel riprodurre le previsioni  dell'art.  1  della    &#13;
 delibera  legislativa  approvata  dall'Assemblea  regionale siciliana    &#13;
 nella seduta del 14 agosto 1997 e già  oggetto  di  impugnativa  con    &#13;
 distinto  ricorso iscritto al  r. ric. n. 55 del 1997, ha riproposto,    &#13;
 per il 1998, "il finanziamento di 1.500 milioni di lire da  destinare    &#13;
 al pagamento di personale assunto a tempo determinato dai consorzi di    &#13;
 bonifica", vulnerando gli artt. 3, 51, 97 e 136 della Costituzione;      &#13;
     l'art.   12,   che,   nell'intento  di  sopperire  alle  esigenze    &#13;
 finanziarie  dei  consorzi  di   bonifica   causate   dalla   mancata    &#13;
 riscossione  di quanto dovuto dai privati, ha posto il relativo onere    &#13;
 a carico della Pubblica Amministrazione,  ledendo,  da  un  lato,  il    &#13;
 "principio  di  buon  andamento  dell'amministrazione" (art. 97 della    &#13;
 Costituzione) e, dall'altro, l'art. 3 della  Costituzione,  sotto  il    &#13;
 profilo della disparità di trattamento fra i soci dei consorzi;         &#13;
     gli  artt.  7,  comma  5,  e  21,  comma  1,  che  "attribuiscono    &#13;
 all'Assessore  regionale   per   la   cooperazione,   il   commercio,    &#13;
 l'artigianato  e  la  pesca  il  compito  di  emanare  regolamenti di    &#13;
 attuazione relativi, rispettivamente, alla concessione dei contributi    &#13;
 previsti dall'art. 7 della legge in esame ed  alla  disciplina  della    &#13;
 gestione   patrimoniale  e  finanziaria  delle  Camere  di  commercio    &#13;
 dell'Isola", ledendo, così, l'art. 12 dello statuto  speciale,  che,    &#13;
 invece,  conferisce  tale  potestà regolamentare al Presidente della    &#13;
 Regione.                                                                 &#13;
   3.  -  Il  Presidente  della  Regione  Siciliana,  costituitosi  in    &#13;
 giudizio, ha concluso per l'infondatezza del ricorso.                    &#13;
   4. - Con memoria depositata il 1 aprile 1999, l'Avvocatura generale    &#13;
 dello  Stato  ha  chiesto che sul giudizio in epigrafe sia dichiarata    &#13;
 cessata la materia del  contendere,  giacché,  dopo  l'instaurazione    &#13;
 dello stesso, il Presidente della Regione Siciliana ha proceduto alla    &#13;
 promulgazione parziale della delibera legislativa impugnata, divenuta    &#13;
 così  legge  regionale  24 dicembre 1997, n. 46, con omissione delle    &#13;
 disposizioni censurate.<diritto>Considerato in diritto</diritto>1. - Con il ricorso in  epigrafe  il  Commissario  dello  Stato  ha    &#13;
 proposto  questione  di  legittimità  costituzionale  degli artt. 7,    &#13;
 comma  5,  8,  11,  12  e  21,  comma  1,   della   legge   approvata    &#13;
 dall'Assemblea  regionale siciliana il 7 novembre del 1997 (Norme per    &#13;
 il recupero ed il completamento  delle  aree  artigianali  attrezzate    &#13;
 realizzate  a  cura  della SIRAP S.p.A.   e interpretazione autentica    &#13;
 dell'articolo  30  della  legge  regionale  4  aprile  1995,  n.  29.    &#13;
 Disposizioni  in  materia di agricoltura e foreste, trasporti, lavori    &#13;
 pubblici, turismo e personale dell'Amministrazione regionale e  delle    &#13;
 Camere  di  commercio),  prospettando,  quanto  agli artt. 8 e 12, la    &#13;
 violazione degli artt. 3 e 97 della Costituzione; quanto all'art. 11,    &#13;
 il contrasto con gli artt.  3,  51,  97  e  136  della  Costituzione;    &#13;
 quanto,  infine,  agli artt. 7, comma 5, e 21, comma 1, la violazione    &#13;
 dell'art. 12 dello statuto speciale.                                     &#13;
   2. - Come già accennato nelle premesse in fatto, la  deliberazione    &#13;
 legislativa sulla quale si appuntano i dubbi di costituzionalità del    &#13;
 Commissario  dello Stato ha formato oggetto di promulgazione da parte    &#13;
 del Presidente della Regione Siciliana, con  omissione  di  tutte  le    &#13;
 disposizioni  impugnate,  divenendo la legge 24 dicembre 1997, n.  46    &#13;
 (in Gazzetta Ufficiale della Regione siciliana 30 dicembre  1997,  n.    &#13;
 73).                                                                     &#13;
   Pertanto,  conformemente  alla  costante  giurisprudenza  di questa    &#13;
 Corte (da ultimo, sentenza n. 139 del 1999), va dichiarata cessata la    &#13;
 materia del contendere.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara cessata la materia del contendere in ordine al ricorso  di    &#13;
 cui in epigrafe.                                                         &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 luglio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                          Il redattore: Vari                              &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 luglio 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
