<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>357</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:357</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>22/10/2007</data_decisione>
    <data_deposito>26/10/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 14, comma 5-bis, del decreto legislativo 25 luglio 1998 n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), aggiunto dall'art. 13, comma 1, della legge 30 luglio 2002 n. 189 (Modifica alla normativa in materia di immigrazione e di asilo), promosso con ordinanza del 6 maggio 2006 dal Tribunale di Gorizia nel procedimento penale a carico di D.S., iscritta al n. 220 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 16, prima serie speciale, dell'anno 2007. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei Ministri; &#13;
    udito nella camera di consiglio del 10 ottobre 2007 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che, con ordinanza emessa il 6 maggio 2006, il Tribunale di Gorizia, in composizione monocratica, nel corso di un giudizio direttissimo nei confronti di un cittadino extracomunitario, imputato del reato di cui all'art. 14, comma 5-quater, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), ha sollevato, in riferimento agli artt. 2, 3, 10, 13 e 24 della Costituzione, questione di legittimità costituzionale dell'art. 14, comma 5-bis, del citato decreto legislativo nel testo attualmente vigente, nella parte in cui prevede che il questore può dare immediata esecuzione al decreto di espulsione, intimando allo straniero espulso di lasciare il territorio dello Stato entro il termine di cinque giorni; e ciò senza che sia preventivamente richiesta al giudice di pace la convalida del decreto di accompagnamento alla frontiera o, in alternativa, del provvedimento di trattenimento presso un centro di permanenza temporanea ed assistenza, ovvero senza che sia prevista analoga tutela giurisdizionale per l'intimazione del questore; &#13;
    che, in ordine alla rilevanza, il giudice a quo osserva che il reato per il quale egli procede ha quale elemento costitutivo la trasgressione all'intimazione emessa dal questore ai sensi dell'art 14, comma 5-bis, del d.lgs. n. 286 del 1998;  &#13;
        che, quanto alla non manifesta infondatezza, il giudice a quo afferma che, nel caso sottoposto al suo giudizio, il questore, secondo una prassi consolidata, ha intimato l'allontanamento dal territorio dello Stato senza preventivamente adottare il provvedimento di accompagnamento alla frontiera o di trattenimento presso un centro di permanenza; provvedimenti, questi, che, a differenza di quello emesso, sono soggetti, nel rispetto di quanto affermato dalla Corte costituzionale nella sentenza n. 222 del 2004, a convalida da parte dell'autorità giudiziaria nel contraddittorio delle parti; &#13;
      che, a parere del rimettente, la mancata previsione di un procedimento di convalida lede i parametri costituzionali evocati, non essendo possibile superare i dubbi di costituzionalità della norma con un'interpretazione della stessa conforme a Costituzione, secondo la quale il questore potrebbe ordinare l'allontanamento dal territorio dello Stato solo quando l'espulsione sia divenuta eseguibile, a seguito di convalida, ma non sia attuabile per mezzo dell'accompagnamento alla frontiera; &#13;
              che il rimettente ritiene la norma impugnata incostituzionale, in quanto, seppure essa non determina una diretta restrizione della libertà personale del destinatario, pone tuttavia a carico di quest'ultimo un obbligo di condotta che, se non ottemperato, ne comporta l'arresto obbligatorio e la responsabilità penale per il reato previsto dall'art. 14, comma 5-quater, del d.lgs. n. 286 del 1998, nonché ulteriori conseguenze penali; &#13;
       che, dunque, a parere del Tribunale di Gorizia, la norma impugnata realizza una disparità di trattamento tra i destinatari dei provvedimenti di accompagnamento alla frontiera o di trattenimento in un centro di assistenza e i destinatari dell'ordine di allontanamento dal territorio dello Stato, essendo prevista solo per i primi la tutela giurisdizionale della convalida del provvedimento; &#13;
      che, infine, a parere del rimettente, la norma censurata sarebbe irragionevole in quanto il ricorso alla procedura in essa prevista si basa sulla sussistenza di due presupposti tra loro alternativi, e cioè: o l'impossibilità di trattenere lo straniero presso un centro di permanenza temporanea, o l'intervenuta scadenza dei termini di tale permanenza senza che l'espulsione o il respingimento sia stato eseguito, laddove solo in tale ultimo caso è previsto un controllo giurisdizionale sulla legittimità della procedura di espulsione; &#13;
        che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la manifesta infondatezza della questione, essendo essa identica ad altre già esaminate e decise in tal senso da questa Corte. &#13;
        Considerato che il Tribunale di Gorizia, con l'ordinanza indicata in epigrafe, dubita della legittimità costituzionale dell'art. 14, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui consente al questore di dare immediata esecuzione al decreto di espulsione, mediante intimazione allo straniero di lasciare il territorio dello Stato entro il termine di cinque giorni, senza che sia prevista una tutela giurisdizionale incidente, in modo diretto o indiretto, su tale intimazione; tutela, al contrario, prevista per le altre forme di esecuzione del decreto di espulsione; &#13;
     che il rimettente ripropone negli stessi termini la questione di legittimità costituzionale già oggetto di scrutinio da parte di questa Corte con le ordinanze n. 228 del 2007 e n. 280 del 2006 e dichiarata manifestamente infondata; &#13;
     che, stante l'immutato quadro normativo, le argomentazioni poste a base delle indicate pronunce devono essere confermate, giustificandosi la convalida da parte del giudice di pace del provvedimento di trattenimento presso un centro di permanenza temporanea (art. 14, commi 3 e 4), nonché del provvedimento di accompagnamento alla frontiera a mezzo della forza pubblica (art. 13, comma 5-bis) con il fatto che tali provvedimenti, a differenza dell'ordine di allontanamento dal territorio dello Stato (art. 14, comma 5-bis), incidono sulla libertà personale dei destinatari e, quindi, devono essere assistiti dalla garanzia di cui all'art. 13 della Costituzione; &#13;
    che la mancata previsione della convalida dell'ordine di allontanamento emesso dal questore è, dunque, giustificata dalla diversa natura di tale provvedimento, il quale non incide direttamente sulla libertà personale del destinatario, atteso che l'autorità di polizia non può esercitare alcuna forma di coazione fisica al fine di ottenerne l'adempimento (sentenza n. 194 del 1996); &#13;
    che, quindi, la questione di legittimità costituzionale sollevata dal Tribunale di Gorizia va dichiarata manifestamente infondata, non contrastando la norma impugnata con alcuno dei parametri costituzionali evocati; &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
     dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 14, comma 5-bis, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), sollevata, in riferimento agli artt. 2, 3, 10, 13 e 24 della Costituzione, dal Tribunale di Gorizia con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 ottobre 2007. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Maria Rita SAULLE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 26 ottobre 2007. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
