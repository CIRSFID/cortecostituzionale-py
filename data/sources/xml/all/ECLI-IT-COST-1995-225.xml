<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>225</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:225</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/06/1995</data_decisione>
    <data_deposito>01/06/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  9-bis del    &#13;
 decreto-legge 29 marzo 1991, n. 103 (Disposizioni urgenti in  materia    &#13;
 previdenziale), aggiunto dalla legge di conversione 1 giugno 1991, n.    &#13;
 166,  promosso  con ordinanza emessa l'11 marzo 1994 dal Tribunale di    &#13;
 Lecco nel procedimento civile vertente tra l'INPS e la Banca Briantea    &#13;
 s.p.a., iscritta al n. 485 del registro ordinanze 1994  e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  37,  prima  serie    &#13;
 speciale, dell'anno 1994;                                                &#13;
    Visti gli atti di  costituzione  della  Banca  Briantea  s.p.a.  e    &#13;
 dell'INPS  nonché  l'atto di intervento del Presidente del Consiglio    &#13;
 dei ministri;                                                            &#13;
    Udito nell'udienza pubblica del 30 maggio 1995 il Giudice relatore    &#13;
 Luigi Mengoni;                                                           &#13;
    Uditi l'avv. Fabio Fonzo  per  l'INPS  e  l'Avvocato  dello  Stato    &#13;
 Giuseppe Fiengo per il Presidente del Consiglio dei ministri.            &#13;
    Ritenuto  che nel corso del giudizio di appello proposto dall'INPS    &#13;
 avverso la sentenza n. 303 del 1993 del Pretore di Lecco, che l'aveva    &#13;
 condannata  a  restituire  alla  Banca  Briantea  la  somma  di  lire    &#13;
 306.472.000   indebitamente   riscossa   a   titolo   di   contributi    &#13;
 previdenziali, il Tribunale di Lecco,  con  ordinanza  dell'11  marzo    &#13;
 1994,  ha  dichiarato  rilevante  e  non manifestamente infondata, in    &#13;
 riferimento all'art. 3,  secondo  (recte:  primo)  comma,  Cost.,  la    &#13;
 questione   di   legittimità   costituzionale  dell'art.  9-bis  del    &#13;
 decreto-legge  29  marzo  1991,  n.  103,  aggiunto  dalla  legge  di    &#13;
 conversione  1  giugno  1991,  n.  166, nella parte in cui esclude il    &#13;
 diritto alla ripetizione dei versamenti contributivi,  effettuati  in    &#13;
 epoca anteriore alla data dell'entrata in vigore della legge predetta    &#13;
 (16  giugno  1991),  sulle  somme versate o accantonate dai datori di    &#13;
 lavoro a favore di gestioni  eroganti  prestazioni  previdenziali  ed    &#13;
 assistenziali   integrative,   in   adempimento   di   contrattazione    &#13;
 collettiva;                                                              &#13;
      che, ad avviso  del  giudice  rimettente,  la  norma  impugnata,    &#13;
 letteralmente   interpretata,   crea   una   evidente  disparità  di    &#13;
 trattamento fra situazioni sostanzialmente identiche sulla base di un    &#13;
 dato  puramente  temporale,  cioè  a  seconda   che   i   contributi    &#13;
 previdenziali  ivi  previsti siano stati versati prima o dopo la data    &#13;
 di entrata in vigore della legge  di  conversione  oppure  non  siano    &#13;
 stati pagati affatto;                                                    &#13;
      che,   sempre  ad  avviso  del  giudice  a  quo,  non  riesce  a    &#13;
 razionalizzare tale discriminazione la giurisprudenza della Corte  di    &#13;
 cassazione,  che  interpreta  restrittivamente  l'irripetibilità dei    &#13;
 versamenti contributivi effettuati anteriormente alla  data  suddetta    &#13;
 limitandola   alle   somme  pagate  "volontariamente";  anzi,  questo    &#13;
 requisito implicito - tratto dalla sentenza n. 885 del 1988 di questa    &#13;
 Corte, relativa all'art. 1, comma 13, della legge 3 marzo 1987, n. 61    &#13;
 -  introduce  una  discriminazione  ulteriore  fondata  su  un   dato    &#13;
 soggettivo   estraneo  alla  disciplina  generale  della  ripetizione    &#13;
 dell'indebito oggettivo, e comunque non è sufficiente per  esonerare    &#13;
 nella   specie   la   Banca   Briantea   dalla  soluti  retentio,  la    &#13;
 volontarietà  non  essendo  esclusa  dalla   semplice   riserva   di    &#13;
 ripetizione formulata all'atto del pagamento.                            &#13;
    Considerato  che  l'art.  9-bis, comma 1, del decreto-legge n. 103    &#13;
 del 1991, in quanto  configura  un'eccezione  rispetto  alla  nozione    &#13;
 generale di retribuzione imponibile di cui all'art. 12 della legge n.    &#13;
 153  del  1969,  come  precedentemente  interpretato  da questa Corte    &#13;
 (sentenza n. 427 del 1990) in relazione ai contributi  versati  dalle    &#13;
 imprese  a  fondi  di  previdenza  integrativa  previsti da contratti    &#13;
 collettivi o da accordi o da  regolamenti  aziendali,  è  una  norma    &#13;
 innovativa munita di efficacia retroattiva mediante l'autodefinizione    &#13;
 come legge di interpretazione autentica;                                 &#13;
      che,  peraltro,  la  retroattività  viene  limitata dall'ultima    &#13;
 parte della disposizione, che attribuisce all'INPS la soluti retentio    &#13;
 dei versamenti contributivi già effettuati anteriormente  alla  data    &#13;
 di   entrata   in   vigore  della  legge  di  conversione,  dovendosi    &#13;
 sottintendere,  secondo   l'interpretazione   giurisprudenziale,   il    &#13;
 requisito  della  "volontarietà",  la  cui  portata  è  di  incerta    &#13;
 definizione;                                                             &#13;
      che,   ai   fini   del   giudizio   sulla   questione   proposta    &#13;
 dall'ordinanza  in  esame - la quale mira a rimuovere il detto limite    &#13;
 alla retroattività  della  norma  -  è  pregiudiziale  valutare  la    &#13;
 legittimità  costituzionale della norma che, prevedendo la ricordata    &#13;
 retroattività, si pone come la previsione  rispetto  alla  quale  la    &#13;
 norma impugnata costituisce l'eccezione;                                 &#13;
      che,  pertanto,  risulta  necessario  sollevare,  in riferimento    &#13;
 all'art. 3, 38 e 81 della  Costituzione,  questione  di  legittimità    &#13;
 costituzionale dell'art. 9-bis, comma 1, del decreto-legge n. 166 del    &#13;
 1991, nella parte in cui conferisce efficacia retroattiva all'esonero    &#13;
 dall'obbligo dei contributi previdenziali sulle contribuzioni e somme    &#13;
 ivi considerate.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dispone  la  trattazione  davanti a sé della    &#13;
 questione di legittimità costituzionale dell'art. 9-bis del d.-l. 29    &#13;
 marzo 1991, n. 103 (Disposizioni urgenti in  materia  previdenziale),    &#13;
 aggiunto  dalla  legge  di  conversione  1 giugno 1991, n. 166, nella    &#13;
 parte in cui esonera dal pagamento dei contributi di previdenza e  di    &#13;
 assistenza sociale -  dovuti  fino  alla  data  di entrata in vigore     &#13;
 della citata legge di conversione sulle contribuzioni e somme versate    &#13;
 o accantonate  a  finanziamento  di  casse, fondi, gestioni  o  forme    &#13;
 assicurative previsti da contratti collettivi o  da accordi o regolamenti aziendali -  i  datori  di  lavoro  che  alla data suddetta non    &#13;
 abbiano ancora provveduto al  pagamento o abbiano adempiuto posteriormente,  in riferimento agli artt. 3, 38 e 81 della Costituzione;        &#13;
      Ordina il rinvio del presente giudizio, per  poter  trattare  la    &#13;
 questione   relativa   congiuntamente  a  quella  di  cui  al  numero    &#13;
 precedente;                                                              &#13;
      Ordina che la Cancelleria provveda agli adempimenti di legge;       &#13;
      Ordina che la presente ordinanza sia pubblicata  nella  Gazzetta    &#13;
 Ufficiale della Repubblica.                                              &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 1 giugno 1995.                                &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 1 giugno 1995.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
