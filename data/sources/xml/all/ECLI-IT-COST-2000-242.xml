<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>242</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:242</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/06/2000</data_decisione>
    <data_deposito>23/06/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, Cesare &#13;
 RUPERTO, Riccardo CHIEPPA, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda &#13;
 CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, &#13;
 Franco BILE, Giovanni Maria FLICK.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 1, comma 5, del    &#13;
 decreto-legge  30  dicembre  1988,  n. 549  (non  convertito)  e  del    &#13;
 decreto-legge 2 marzo 1989, n. 66 (Disposizioni urgenti in materia di    &#13;
 autonomia   impositiva  degli  enti  locali  e  di  finanza  locale),    &#13;
 convertito nella legge 24 aprile 1989, n. 144, promosso con ordinanza    &#13;
 emessa il 9 novembre 1998 dalla Commissione tributaria provinciale di    &#13;
 Biella  sul  ricorso proposto dalla "T.M.T. Manenti s.r.l." contro il    &#13;
 comune  di  Bioglio, iscritta al n. 192 del registro ordinanze 1999 e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 14, prima    &#13;
 serie speciale, dell'anno 1999.                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri.                                                                &#13;
     Udito  nella  camera  di  consiglio del 24 maggio 2000 il giudice    &#13;
 relatore Riccardo Chieppa.                                               &#13;
     Ritenuto  che  la Commissione tributaria provinciale di Biella ha    &#13;
 sollevato,  con  ordinanza  emessa  il  9  novembre  1998 sul ricorso    &#13;
 proposto dalla "T.M.T. Manenti s.r.l." per l'annullamento dell'avviso    &#13;
 di   liquidazione   emesso   dal  comune  di  Bioglio,  questione  di    &#13;
 legittimità   costituzionale   indicando   l'art. 1,  comma  5,  del    &#13;
 decreto-legge  30 dicembre  1988,  n. 549 (non convertito), reiterato    &#13;
 con  decreto-legge  2  marzo  1989,  n. 66  (Disposizioni  urgenti in    &#13;
 materia  di  autonomia  impositiva  degli  enti  locali  e di finanza    &#13;
 locale),  convertito,  con modificazioni, nella legge 24 aprile 1989,    &#13;
 n. 144,  nella  parte  in cui dispone: "se lo stesso soggetto passivo    &#13;
 esercita  attività  in  locali  diversi siti in unico edificio od in    &#13;
 edifici  contigui  od in complessi produttivi unitari, ovvero su aree    &#13;
 attrezzate  contigue,  l'imposta  è dovuta in misura unica a ciascun    &#13;
 comune sul cui territorio sono ubicati detti insediamenti, sulla base    &#13;
 della  superficie  complessiva compresa nel territorio di ogni comune    &#13;
 ..." (R.O. n. 192 del 1999);                                             &#13;
         che  - secondo la predetta ordinanza - con il suddetto avviso    &#13;
 di   liquidazione   veniva   assoggettata  ad  imposta  comunale  per    &#13;
 l'esercizio di imprese e di arti e professioni (Iciap) la porzione di    &#13;
 insediamento  ricadente  nel  comune  di  Bioglio,  considerata  come    &#13;
 attività  a  sé  stante  ed  autonoma,  senza che venisse tenuto in    &#13;
 debito  conto  il  fatto  che  essa  fosse  strettamente correlata ed    &#13;
 interdipendente alla porzione di insediamento contiguo insistente nel    &#13;
 comune di Vallemosso;                                                    &#13;
         che il giudice a quo sottolinea che il reddito prodotto dalla    &#13;
 Società  ricorrente  è unico ed è dato dall'attività dell'azienda    &#13;
 nel  suo  insieme  e non può, quindi, essere discriminato; tuttavia,    &#13;
 per  i meccanismi di calcolo dell'imposta in questione si verifica di    &#13;
 fatto   una   duplicazione   dell'imposta  rispetto  al  caso  di  un    &#13;
 insediamento insistente in un unico comune;                              &#13;
         che  la  norma  impugnata  -  secondo  il  giudice  a  quo  -    &#13;
 recherebbe  vulnus  all'art. 3  della Costituzione, per disparità di    &#13;
 trattamento  tra insediamenti della stessa tipologia (ed estensione),    &#13;
 ricadenti  in un unico comune rispetto a quelli che interessano due o    &#13;
 più  comuni,  verificandosi,  in  quest'ultimo  caso,  un  raddoppio    &#13;
 dell'imposta,  per cui vi sarebbe violazione anche dell'art. 53 della    &#13;
 Costituzione;                                                            &#13;
         che  nel  giudizio,  introdotto  avanti a questa Corte con la    &#13;
 citata  ordinanza,  è  intervenuto  il  Presidente del Consiglio dei    &#13;
 Ministri  con il patrocinio dell'Avvocatura generale dello Stato, che    &#13;
 ha chiesto che la questione sia dichiarata inammissibile o, comunque,    &#13;
 sia  rigettata. Con successiva memoria, dopo aver sottolineato che la    &#13;
 questione  di  legittimità  costituzionale  è  stata  indebitamente    &#13;
 riferita  al d.l. n. 549 del 1988, in quanto il decreto stesso non è    &#13;
 stato,  poi, convertito in legge, giacché la richiamata legge n. 144    &#13;
 del 1989 ha invece convertito il successivo d.l. 2 marzo 1989, n. 66,    &#13;
 ha  prospettato  la  manifesta  inammissibilità  della questione, in    &#13;
 quanto  riferita  ad  una  norma  non  più in vigore, e, quindi, non    &#13;
 applicabile  al  caso  di specie: infatti l'art. 1, comma 5, del d.l.    &#13;
 n. 66  del  1989,  convertito  dalla  legge  n. 144  del 1989, cui si    &#13;
 incentrano le censure sollevate, è stato sostituito, a decorrere dal    &#13;
 1990,  dall'art. 1 del d.l. 30 settembre 1989, n. 332 (Misure fiscali    &#13;
 urgenti), convertito dalla legge 27 novembre 1989, n. 384.               &#13;
     Considerato  che  l'ordinanza  di  rimessione  della questione di    &#13;
 legittimità   costituzionale   è   manifestamente   carente   nella    &#13;
 motivazione  sulla  rilevanza per un duplice ordine di deficienze: a)    &#13;
 per  quanto  riguarda  il d.l. indicato come oggetto della questione,    &#13;
 poiché  il  d.l.  30 dicembre 1988, n. 549 (denunciato per l'art. 1,    &#13;
 comma  5) non è stato convertito in legge, mentre la legge 24 aprile    &#13;
 1989  n. 144  (indicata  come  legge  di  conversione) ha convertito,    &#13;
 invece,  con  modifiche  il decreto-legge 2 marzo 1989, n. 66 (che è    &#13;
 riproduttivo  per  la  parte  che  interessa  della  precedente norma    &#13;
 rimasta  priva di efficacia fin dall'inizio per effetto della mancata    &#13;
 conversione  in  legge); b) in quanto la norma denunciata è stata, a    &#13;
 sua  volta,  interamente  sostituita nell'art. 1 con una disposizione    &#13;
 (parzialmente  diversa  nel  contenuto  da  quella denunciata), cioè    &#13;
 dall'art. 1  del  d.l.  30  settembre  1989,  n. 332,  convertito con    &#13;
 modificazioni in legge 27 novembre 1989, n. 384;                         &#13;
         che   il   giudice   rimettente   non   ha  preso  in  alcuna    &#13;
 considerazione,   nell'esame  della  rilevanza  della  questione,  la    &#13;
 successiva  norma  (d.l.  n. 332 del 1989) in vigore negli anni 1994,    &#13;
 1995   e  1996,  cui  si  riferiscono  gli  accertamenti  di  imposta    &#13;
 contestati;                                                              &#13;
         che,  di  conseguenza,  deve  essere  dichiarata la manifesta    &#13;
 inammissibilità della questione.                                        &#13;
     Visti  gli articoli 26, secondo comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale  dell'art. 1, comma 5, del decreto-legge    &#13;
 30 dicembre 1988, n. 549 (non convertito) e del decreto-legge 2 marzo    &#13;
 1989  n. 66  (Disposizioni urgenti in materia di autonomia impositiva    &#13;
 degli  enti  locali  e  di finanza locale), convertito nella legge 24    &#13;
 aprile  1989,  n. 144, sollevata, in riferimento agli articoli 3 e 53    &#13;
 della  Costituzione,  dalla  Commissione  tributaria  provinciale  di    &#13;
 Biella, con l'ordinanza indicata in epigrafe.                            &#13;
                                                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 giugno 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Chieppa                            &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 23 giugno 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
