<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>242</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:242</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/07/2001</data_decisione>
    <data_deposito>06/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 57 del d.P.R. &#13;
29 settembre  1973,  n. 602  (Disposizioni  sulla  riscossione  delle &#13;
imposte  sul  reddito),  come  modificato  dall'art. 16  del  decreto &#13;
legislativo  26 febbraio 1999, n. 46 (Riordino della disciplina della &#13;
riscossione   mediante   ruolo,   a  norma  dell'art. 1  della  legge &#13;
28 settembre   1998,   n. 337),  promosso  con  ordinanza  emessa  il &#13;
20 luglio  2000  dal  tribunale  di Torre Annunziata nel procedimento &#13;
civile  vertente  tra  Antonio Cafiero ed altra ed il Banco di Napoli &#13;
S.p.a.,  iscritta  al n. 705 del registro ordinanze 2000 e pubblicata &#13;
nella  Gazzetta  Ufficiale della Repubblica n. 48, 1ª serie speciale, &#13;
dell'anno 2000; &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 23 maggio 2001 il giudice &#13;
relatore Franco Bile. &#13;
    Ritenuto  che  il  tribunale di Torre Annunziata ha proposto - in &#13;
riferimento  agli  artt. 3 e 111 della Costituzione - la questione di &#13;
legittimità  costituzionale  dell'art. 57  del  d.P.R.  29 settembre &#13;
1973,  n. 602  (Disposizioni  sulla  riscossione  delle  imposte  sul &#13;
reddito), nel testo risultante dalle modifiche apportate dall'art. 16 &#13;
del  decreto  legislativo  26 febbraio  1999,  n. 46  (Riordino della &#13;
disciplina  della  riscossione  mediante  ruolo,  a norma dell'art. 1 &#13;
della legge 28 settembre 1998, n. 337); &#13;
        che l'ordinanza è stata pronunziata in un giudizio nel quale &#13;
due  debitori  -  proprietari  di  due  beni  immobili  pignorati dal &#13;
concessionario  del servizio di riscossione dei tributi, che ne aveva &#13;
determinato  il  valore  ai  sensi dell'art. 79 del d.P.R. n. 602 del &#13;
1973   -   avevano  chiesto  (sulla  premessa  che  tale  valore  non &#13;
corrispondesse  a  quello di mercato) un provvedimento di urgenza, di &#13;
cui  il  rimettente  non  specifica il contenuto, assumendo di essere &#13;
"privati  del  diritto  di  azionare  i  rimedi di cui agli artt. 496 &#13;
(riduzione del pignoramento) e 601 (divisione degli immobili)"; &#13;
        che  il rimettente - ritenuta l'ammissibilità del ricorso ex &#13;
art. 700  del  codice  di  procedura  civile,  in quanto "nel sistema &#13;
delineato dal d.P.R. n. 602 del 1973 non è previsto il rimedio della &#13;
riduzione  del  pignoramento  né  quello della divisione dei beni da &#13;
espropriare",  e  premesso  che  il giudice ordinario può sospendere &#13;
l'esecuzione  esattoriale  relativa  a  tributi  riservati  alla  sua &#13;
giurisdizione - ha, con la stessa ordinanza, sospeso la vendita degli &#13;
immobili   pignorati   e   sollevato  la  questione  di  legittimità &#13;
costituzionale della norma citata "nella parte in cui non prevede per &#13;
il   debitore  la  possibilità  di  agire  con  ricorso  al  giudice &#13;
dell'esecuzione   per   ottenere   la   riduzione  del  pignoramento, &#13;
analogamente a quanto disposto dall'art. 496 cod. proc. civ."; &#13;
        che  nessuna  delle  parti  si  è costituita avanti a questa &#13;
Corte. &#13;
    Considerato  che  la norma impugnata dispone, al primo comma, che &#13;
non   sono   ammesse   le   opposizioni   (all'esecuzione)   regolate &#13;
dall'art. 615 cod. proc. civ., fatta eccezione per quelle concernenti &#13;
la  pignorabilità  dei  beni, e le opposizioni (agli atti esecutivi) &#13;
regolate  dall'art. 617  cod.  proc.  civ.  relative alla regolarità &#13;
formale  ed  alla  notificazione del titolo esecutivo, disciplinando, &#13;
quindi,  al secondo comma, le modalità di fissazione dell'udienza di &#13;
comparizione delle parti "se è proposta opposizione all'esecuzione o &#13;
agli atti esecutivi"; &#13;
        che - secondo il rimettente - tale norma, viceversa, "esclude &#13;
la  possibilità di opposizione all'esecuzione o agli atti esecutivi" &#13;
ed  inoltre "non prevede per il debitore la possibilità di agire con &#13;
ricorso  al  giudice  dell'esecuzione  per  ottenere la riduzione del &#13;
pignoramento"; &#13;
        che il giudice rimettente - adito ai sensi dell'art. 700 cod. &#13;
proc.  civ.  -  non  spiega minimamente le ragioni per le quali, dopo &#13;
aver emanato un provvedimento urgente di sospensione della vendita di &#13;
beni  pignorati,  ha mantenuto il giudizio avanti a sé, ancora nella &#13;
veste  di  giudice  della  cautela,  ed  ha  considerato rilevante la &#13;
questione  di  legittimità costituzionale di una norma concernente i &#13;
poteri del giudice dell'esecuzione; &#13;
        che  è  del pari assolutamente priva di motivazione la tesi, &#13;
apoditticamente  enunciata  in  termini  generali,  secondo  cui,  in &#13;
materia  di  esecuzione  esattoriale,  non  sarebbero  ammesse né le &#13;
opposizioni  all'esecuzione  e  agli atti esecutivi, né la riduzione &#13;
del pignoramento ex art. 496 cod. proc. civ; &#13;
        che,  del resto, il rimettente ha già ritenuto di soddisfare &#13;
-  con  il  provvedimento  di sospensione dell'esecuzione esattoriale &#13;
emanato  ex  art. 700  cod.  proc. civ. - le esigenze di tutela a suo &#13;
dire non garantite dalla norma impugnata, nell'interpretazione da lui &#13;
accolta; &#13;
        che  ciascuno  di  siffatti  rilievi  determina  da  solo  la &#13;
manifesta    inammissibilità   della   questione   di   legittimità &#13;
costituzionale. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  dell'art. 57  del  d.P.R.  29 settembre &#13;
1973,  n. 602  (Disposizioni  sulla  riscossione  delle  imposte  sul &#13;
reddito),   come  modificato  dall'art. 16  del  decreto  legislativo &#13;
26 febbraio  1999, n. 46 (Riordino della disciplina della riscossione &#13;
mediante  ruolo,  a  norma dell'art. 1 della legge 28 settembre 1998, &#13;
n. 337),   sollevata,   in  riferimento  agli  artt. 3  e  111  della &#13;
Costituzione,  dal  tribunale  di  Torre  Annunziata  con l'ordinanza &#13;
indicata in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 4 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Bile &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 6 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
