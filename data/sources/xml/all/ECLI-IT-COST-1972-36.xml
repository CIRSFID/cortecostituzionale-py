<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>36</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:36</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/02/1972</data_decisione>
    <data_deposito>01/03/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della legge regionale 6  &#13;
 luglio  1971,  riapprovata  dal  Consiglio regionale del Piemonte il 21  &#13;
 settembre 1971, recante "Approvazione del rendiconto finanziario 1970",  &#13;
 promosso  con  ricorso  del  Presidente  del  Consiglio  dei  ministri,  &#13;
 notificato   il  9  ottobre  1971,  depositato  in  cancelleria  il  12  &#13;
 successivo ed iscritto al n. 22 del registro ricorsi 1971.               &#13;
     Udito nell'udienza pubblica del 12 gennaio 1972 il Giudice relatore  &#13;
 Giuseppe Verzì;                                                         &#13;
     udito il sostituto avvocato generale dello Stato Michele  Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nella  seduta  del  6  luglio  1971,  il  Consiglio regionale della  &#13;
 Regione piemontese approvava la legge di  approvazione  del  rendiconto  &#13;
 finanziario per l'anno 1970.                                             &#13;
     Tale  legge  consta  di  un  solo  articolo  così  formulato:  "Il  &#13;
 rendiconto  finanziario  1970  presentato  dalla  Giunta  regionale  è  &#13;
 approvato secondo le risultanze del documento allegato".                 &#13;
     Questo   si  compone  di  un  "conto  di  cassa"  e  di  un  "conto  &#13;
 amministrativo".  Il  primo  reca  due  voci:    riscossioni  per  lire  &#13;
 462.133.335  e  pagamenti per lire 184.934.020. Il secondo distingue le  &#13;
 somme rimaste da riscuotere (L. 1.251.630) da quelle rimaste da  pagare  &#13;
 (L.   202.444.930)   e  precisa  il  fondo  disponibile  alla  chiusura  &#13;
 dell'esercizio in lire 76.006.015.                                       &#13;
     Ai sensi dell'art. 45 dello  Statuto  regionale  (legge  22  maggio  &#13;
 1971,  n.  338),  il  Governo  rinviava  la legge per un nuovo esame al  &#13;
 Consiglio regionale e questo, nella seduta del 21  settembre  1971,  la  &#13;
 riapprovava  nello  stesso  testo,  a  maggioranza  assoluta  dei  suoi  &#13;
 componenti, e ne dava comunicazione al Commissario del  Governo  il  24  &#13;
 settembre successivo.                                                    &#13;
     Il Governo, giusta deliberazione del Consiglio dei ministri in data  &#13;
 30  settembre  1971,  con  ricorso  del  suo  Presidente, notificato al  &#13;
 Presidente della Giunta regionale piemontese  il  9  ottobre  1971,  ha  &#13;
 proposto   ricorso   avverso   la   ripetuta   legge,   eccependone  la  &#13;
 illegittimità costituzionale, in riferimento agli artt.   125  e  119,  &#13;
 primo  comma,  della  Costituzione,  in  relazione  agli artt. 78 dello  &#13;
 Statuto piemontese (legge 22 maggio 1971, n. 338), 41 e seguenti, della  &#13;
 legge 10 febbraio 1953, n. 62, e 20 della legge 16 maggio 1970, n. 281,  &#13;
 e al d.P.R. 3 dicembre 1970, n. 1171.                                    &#13;
     Nel presente giudizio la Regione piemontese non si è costituita.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La legge della Regione  del  Piemonte,  che  ha  approvato  il  &#13;
 rendiconto dell'anno 1970, senza avere prima sottoposto al controllo di  &#13;
 legittimità della speciale Commissione gli atti amministrativi, che di  &#13;
 esso rendiconto rappresentano gli elementi materiali, ha violato l'art.  &#13;
 125  della Costituzione per cui il controllo di legittimità sugli atti  &#13;
 amministrativi della Regione è esercitato in forma  decentrata  da  un  &#13;
 organo  dello  Stato  nei  modi  e  nei limiti stabiliti da leggi della  &#13;
 Repubblica.                                                              &#13;
     E gli artt. 41 e seguenti della legge  10  febbraio  1953,  n.  62,  &#13;
 disciplinano  compiutamente  l'istituzione  ed  il  funzionamento delle  &#13;
 Commissioni di controllo.                                                &#13;
     La circostanza che tale Commissione sia stata costituita in data 30  &#13;
 giugno 1971, se poteva essere motivo di ritardo nella presentazione del  &#13;
 rendiconto (30 aprile, per il disposto dell'art. 77 dello Statuto), non  &#13;
 vale a giustificare la completa omissione  del  controllo.  Il  che  è  &#13;
 confermato  dai  decreti ministeriali del 5 giugno e 1  ottobre 1970, 4  &#13;
 gennaio e 1  luglio 1971, i quali, pur autorizzando la giunta regionale  &#13;
 a deliberare,  nelle  more  della  costituzione  della  Commissione  di  &#13;
 controllo,  l'effettuazione delle spese urgenti ed indifferibili, hanno  &#13;
 fatta sempre salva la successiva  approvazione  della  spesa  da  parte  &#13;
 della Commissione stessa.                                                &#13;
     Appare  evidente,  dunque,  che  soltanto dopo l'approvazione degli  &#13;
 atti amministrativi da parte della ripetuta Commissione, il legislatore  &#13;
 regionale sarebbe stato posto in condizioni di controllare la  gestione  &#13;
 finanziaria e di approvare il rendiconto.                                &#13;
     2. - Risulta altresì violato il principio stabilito dall'art. 119,  &#13;
 primo  comma,  della  Costituzione,  per cui le Regioni hanno autonomia  &#13;
 finanziaria  nelle  forme  e  nei  limiti  stabiliti  da  leggi   della  &#13;
 Repubblica,  che  la  coordinano  con  la  finanza  dello  Stato, delle  &#13;
 Provincie e dei Comuni. Allo scopo di attuare tale principio  la  legge  &#13;
 16  maggio 1970, n. 281, ha delegato il Governo ad emanare disposizioni  &#13;
 per la redazione dei bilanci regionali,  in  modo  che  il  sistema  di  &#13;
 classificazione delle entrate e delle spese sia coordinato con le norme  &#13;
 della legge 1  marzo 1964, n. 62 (concernente il bilancio dello Stato e  &#13;
 quelli  degli enti pubblici); ed ha altresì disposto che nel frattempo  &#13;
 i bilanci  regionali  osservino  le  norme  sulla  amministrazione  del  &#13;
 patrimonio  e  della  contabilità  generale  dello  Stato,  in  quanto  &#13;
 applicabili. Ed a tale legge ha fatto  seguito  il  d.P.R.  3  dicembre  &#13;
 1970,  n. 1171: il bilancio di previsione regionale è costituito dallo  &#13;
 stato di previsione dell'entrata, dallo stato di previsione della spesa  &#13;
 e da un quadro generale riassuntivo da approvarsi con distinti articoli  &#13;
 della legge di bilancio (art. 1) e le entrate e le spese delle  Regioni  &#13;
 sono ripartite in titoli, capittoli, categorie, ecc.                     &#13;
     La   funzione  del  rendiconto  si  identifica  nella  esigenza  di  &#13;
 garantire  la  destinazione  ai  fini  pubblici  dei  mezzi  finanziari  &#13;
 indicati  nei bilanci di previsione; ed è per tale motivo che anche il  &#13;
 rendiconto deve seguire la stessa classificazione delle entrate e delle  &#13;
 spese disposta per quelli.                                               &#13;
     Il documento approvato dalla legge impugnata, non articolato su una  &#13;
 siffatta classificazione, ma esaurentesi in un sommario conto di  cassa  &#13;
 ed un conto amministrativo, non risponde alle esigenze di cui innanzi.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale  della  legge  regionale  &#13;
 piemontese  (Approvazione  del  rendiconto   finanziario   anno   1970)  &#13;
 approvata  dal Consiglio regionale il 6 luglio 1971 e riapprovata il 21  &#13;
 settembre 1971.                                                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 23 febbraio 1972.                             &#13;
                                   GIUSEPPE  CHIARELLI - MICHELE FRAGALI  &#13;
                                   -  COSTANTINO  MORTATI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO CRISAFULLI-  &#13;
                                   NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
