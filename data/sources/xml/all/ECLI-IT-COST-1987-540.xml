<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>540</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:540</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/11/1987</data_decisione>
    <data_deposito>17/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi di legittimità costituzionale degli artt. 35 del d.P.R.    &#13;
 29  settembre  1973,  n.  600  (Disposizioni  comuni  in  materia  di    &#13;
 accertamento  delle  imposte  sui redditi) e 51 del d.P.R. 26 ottobre    &#13;
 1972, n.  633  (Istituzione  e  disciplina  dell'imposta  sul  valore    &#13;
 aggiunto),  promossi  con  ordinanze  emesse il 28 gennaio 1982 dalla    &#13;
 Commissione tributaria di primo grado di Trieste e il 25  marzo  1982    &#13;
 dalla  Commissione  tributaria  di secondo grado di Venezia, iscritte    &#13;
 rispettivamente al n. 527 del registro ordinanze 1982 e al n. 126 del    &#13;
 registro  ordinanze  1983 e pubblicate nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 4 dell'anno 1982 e n. 191 dell'anno 1983;                  &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera di consiglio dell'11 novembre 1987 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che  l'Ufficio  IVA  di  Trieste  notificava  a  Lunardi    &#13;
 Giordano un avviso di accertamento  in  rettifica  per  l'anno  1975,    &#13;
 fondato  su  elementi acquisiti dalla Polizia tributaria nel corso di    &#13;
 indagini  disposte  dalla  locale  Procura   della   Repubblica   per    &#13;
 imputazioni in materia valutaria;                                        &#13;
      che  la  stessa  Procura  aveva autorizzato l'uso a fini fiscali    &#13;
 della documentazione acquisita in dette indagini;                        &#13;
      che,  avendo adito il Lunardi la Commissione tributaria di primo    &#13;
 grado di Trieste, quest'ultima con  ordinanza  del  28  gennaio  1982    &#13;
 (reg.  ord.  n.  527  del  1982)  sollevava questione di legittimità    &#13;
 costituzionale, in riferimento agli artt. 3 e 53 Cost., dell'art. 51,    &#13;
 n.   5   d.P.R.   26   ottobre   1972,  n.  633,  che  non  prevedeva    &#13;
 l'utilizzazione ai fini IVA di dati bancari acquisiti  dalla  Polizia    &#13;
 tributaria per procedimenti penali;                                      &#13;
      che,  secondo  il  Collegio  rimettente,  la  detta omissione di    &#13;
 previsione causava una disparità di trattamento ingiustificata tra i    &#13;
 soggetti  tenuti  al  pagamento  dell'imposta  sul  valore aggiunto e    &#13;
 quelli tenuti al pagamento dell'Irpef e dell'Ilor: infatti per queste    &#13;
 ultime  imposte  l'art.  35,  d.P.R. 29 settembre 1973 n. 600 rendeva    &#13;
 utilizzabili gli elementi acquisiti in sede  di  indagini  effettuate    &#13;
 dalla Guardia di finanza presso banche;                                  &#13;
      che    la    detta   ingiustificata   limitazione   dei   poteri    &#13;
 d'accertamento della Polizia  tributaria  sembrava  ledere  anche  il    &#13;
 principio di capacità contributiva;                                     &#13;
      che  analoghe  questioni  venivano  sollevate  dalla Commissione    &#13;
 tributaria di secondo grado di Venezia                                   &#13;
 con  ordinanza  del  25  maggio  1982 (reg. ord. n. 126 del 1983), la    &#13;
 quale impugnava anche il citato art. 35,                                 &#13;
 d.P.R. n. 600 del 1973;                                                  &#13;
      che   secondo   il   collegio   rimettente   quest'ultima  norma    &#13;
 confliggeva anzitutto con l'art. 10,  n.  12  della  legge  delega  9    &#13;
 ottobre 1971, n. 825, e quindi con l'art. 76 Cost., in quanto ai fini    &#13;
 Irpef ed Ilor consentiva bensì agli uffici delle imposte dirette, in    &#13;
 deroga al segreto bancario, di chiedere notizie ad aziende e istituti    &#13;
 di   credito,   ma   subordinava   l'esercizio   di   questo   potere    &#13;
 all'autorizzazione  del  presidente  della  commissione tributaria di    &#13;
 primo grado territorialmente competente, con ciò rendendo  possibile    &#13;
 "un'ingerenza"    di   un   organo   giurisdizionale   nell'attività    &#13;
 dell'amministrazione finanziaria;                                        &#13;
      che il cit. art. 35 sembrava contrastare anche con gli artt. 3 e    &#13;
 53 Cost., in quanto  consentiva  di  chiedere  dati  e  notizie  alle    &#13;
 banche,  ma  non anche di effettuarvi accessi, ispezioni e verifiche,    &#13;
 ciò che impediva di  adeguare  il  carico  tributario  all'effettiva    &#13;
 capacità patrimoniale dei contribuenti;                                 &#13;
      che,  sempre  ad avviso della Commissione tributaria di Venezia,    &#13;
 anche l'art. 51, n. 5, d.P.R. n. 633 del 1972 violava l'art. 76 Cost.    &#13;
 per  avere  il  legislatore delegato rinunziato, in materia di IVA, a    &#13;
 derogare al segreto bancario, contrariamente a  quanto  previsto  dal    &#13;
 legislatore delegante nel cit. art. 10, n. 12, l. n. 825 del 1971;       &#13;
      che  la  Presidenza  del  Consiglio dei Ministri, intervenuta in    &#13;
 entrambi i giudizi, notava la sopravvenienza del d.P.R.  n.  463  del    &#13;
 1982  e  comunque  chiedeva  che  le questioni fossero dichiarate non    &#13;
 fondate;                                                                 &#13;
    Considerato  che  i  giudizi,  per  l'analogia  del  loro oggetto,    &#13;
 debbono essere riuniti;                                                  &#13;
      che  dopo  l'emanazione delle ordinanze di rimessione è entrato    &#13;
 in vigore il d.P.R. 15 luglio 1982, n. 463,  contenente  disposizioni    &#13;
 integrative e correttive dei citati dd.P.R. n. 633 del 1972 e 600 del    &#13;
 1973;                                                                    &#13;
      che,  per  quanto in particolare attiene ai giudizi qui riuniti,    &#13;
 il nuovo d.P.R. (art. 3) ha sostituito l'art. 53,  d.P.R.  n.600/1973    &#13;
 attribuendo agli uffici delle imposte dirette, tra l'altro, il potere    &#13;
 di accesso presso le aziende ed istituti di credito, ed  ha  altresì    &#13;
 (art.  4)  modificato  l'art.  51,  d.P.R.  n.  633/1972, consentendo    &#13;
 deroghe al segreto bancario in favore degli uffici IVA;                  &#13;
      che  pertanto  è  necessario  restituire  gli  atti  ai giudici    &#13;
 rimettenti onde accertino la persistente  rilevanza  delle  questioni    &#13;
 nei  giudizi  pendenti  davanti  a loro, alla stregua della normativa    &#13;
 sopravvenuta;                                                            &#13;
      che  l'esercizio  dei  poteri di accertamento degli uffici delle    &#13;
 imposte dirette nei confronti delle aziende ed istituti di credito è    &#13;
 tuttora    sottoposto   all'autorizzazione   del   presidente   della    &#13;
 commissione tributaria di primo grado territorialmente competente, ma    &#13;
 ciò, lungi dal contrastare con l'art. 10 n. 12 della legge-delega n.    &#13;
 825 del 1971, dà luogo ad  un'ulteriore  garanzia  di  imparzialità    &#13;
 dell'accertamento,  trattandosi  di  potere  attribuito  ad un organo    &#13;
 giurisdizionale  e  rivelandosi  così  manifestamente  infondata  la    &#13;
 censura sollevata dalla Commissione tributaria di Venezia;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
 riuniti i giudizi,                                                       &#13;
    Ordina  la  restituzione degli atti alla Commissione tributaria di    &#13;
 primo grado di Trieste ed  alla  Commissione  tributaria  di  secondo    &#13;
 grado  di  Venezia  onde  verifichino  la persistente rilevanza delle    &#13;
 questioni nei giudizi pendenti davanti  ad  esse,  alla  stregua  del    &#13;
 sopravvenuto d.P.R. 15 luglio 1982, n. 463;                              &#13;
    Dichiara  manifestamente  infondata  la  questione di legittimità    &#13;
 costituzionale dell'art. 35, d.P.R. 29 settembre 1973, n. 600 - nella    &#13;
 parte  attinente  al potere di autorizzazione spettante ai presidenti    &#13;
 delle  commissioni  tributarie  di  primo  grado   -   sollevata   in    &#13;
 riferimento all'art. 76 Cost. dalla Commissione tributaria di secondo    &#13;
 grado di Venezia con l'ordinanza indicata in epigrafe.                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 27 novembre 1987.                             &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: SAJA                                 &#13;
    Depositata in cancelleria il 17 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
