<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>58</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:58</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SPAGNOLI</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/02/1995</data_decisione>
    <data_deposito>24/02/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Ugo SPAGNOLI; &#13;
 Giudici: prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  86,  primo    &#13;
 comma,  del d.P.R. 9 ottobre 1990, n. 309 (Testo unico delle leggi in    &#13;
 materia di  disciplina  degli  stupefacenti  e  sostanze  psicotrope,    &#13;
 prevenzione,   cura   e   riabilitazione   dei   relativi   stati  di    &#13;
 tossicodipendenza), promosso con l'ordinanza emessa il 17 maggio 1994    &#13;
 dal  Tribunale di Roma nel procedimento penale a carico di Noureddine    &#13;
 Bachri, iscritta al n. 584 del registro ordinanze 1994  e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  41,  prima  serie    &#13;
 speciale, dell'anno 1994.                                                &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio dell'8 febbraio 1995 il Giudice    &#13;
 relatore Antonio Baldassarre.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Il Tribunale di Roma,  giudice  di  rinvio  nel  procedimento    &#13;
 penale  a carico di Noureddine Bachri, imputato del reato di cessione    &#13;
 di sostanza stupefacente,  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale  - in riferimento agli artt. 3, 25, terzo comma, e 27,    &#13;
 terzo comma, della Costituzione - nei confronti dell'art.  86,  primo    &#13;
 comma, del decreto del Presidente della Repubblica 9 ottobre 1990, n.    &#13;
 309,   nella   parte   in   cui   obbliga   il  giudice  a  emettere,    &#13;
 contestualmente alla condanna, l'ordine di  espulsione  dallo  Stato,    &#13;
 eseguibile  a  pena espiata, nei confronti dello straniero condannato    &#13;
 per uno dei reati previsti dagli articoli 73, 74, 79 e 82, commi 2  e    &#13;
 3, precludendogli, in forza dell'art. 164, secondo comma, n. 2, c.p.,    &#13;
 la concessione della sospensione condizionale della pena inflitta.       &#13;
    Il  giudice  rimettente  premette che il giudizio a quo segue alla    &#13;
 pronunzia della Corte di cassazione che, annullando la sentenza dello    &#13;
 stesso  Tribunale  limitatamente  alla  parte  in  cui  concedeva  il    &#13;
 beneficio  della  sospensione  condizionale  della  pena all'imputato    &#13;
 condannato a sei mesi di reclusione, per il reato previsto  dall'art.    &#13;
 73  del d.P.R. n. 309 del 1990, ha fissato il "principio di diritto",    &#13;
 secondo il quale l'impugnato art. 86, primo  comma,  va  interpretato    &#13;
 nel senso che impone al giudice, senza alcuna valutazione in concreto    &#13;
 della  sussistenza  della  pericolosità  sociale,  di  espellere dal    &#13;
 territorio  nazionale,  una  volta  espiata  la  pena,  lo  straniero    &#13;
 condannato  per  alcuni dei reati previsti dal testo unico in materia    &#13;
 di stupefacenti, precludendogli, in  ragione  dell'irrogazione  della    &#13;
 misura  di sicurezza dell'espulsione e in forza del divieto dell'art.    &#13;
 164, secondo comma, n. 2,  c.p.,  la  concessione  della  sospensione    &#13;
 condizionale della pena inflitta.                                        &#13;
   Lo  stesso  giudice  a  quo ricorda, sempre in via di premessa, che    &#13;
 l'interpretazione appena menzionata appare del  tutto  isolata  nella    &#13;
 giurisprudenza formatasi sull'art. 86 (nonché sul previgente art. 81    &#13;
 della  legge 22 dicembre 1975, n. 685, dall'analogo contenuto), anche    &#13;
 con riferimento ad altre sentenze della Corte di cassazione,  con  le    &#13;
 quali  l'istituto dell'espulsione in oggetto, essendo ricondotto alla    &#13;
 disciplina  generale  delle  misure  di  sicurezza  e  presupponendo,    &#13;
 quindi,  dopo  l'abrogazione dell'art. 204 c.p. da parte dell'art. 31    &#13;
 della legge n.  663  del  1986,  la  valutazione  in  concreto  della    &#13;
 pericolosità  sociale  del  condannato,  viene  considerato come non    &#13;
 preclusivo della concessione  della  sospensione  condizionale  della    &#13;
 pena.                                                                    &#13;
    Ciò  posto,  il  giudice  rimettente, ritenendosi vincolato, come    &#13;
 giudice  di  rinvio,  dal  "principio  di  diritto"  enunciato  dalla    &#13;
 pronunzia  di  annullamento della Corte di cassazione, afferma che la    &#13;
 dovuta applicazione di tale principio gli impedirebbe di concedere la    &#13;
 sospensione condizionale della pena,  nonostante  che  ricorrano  nel    &#13;
 caso  le  condizioni oggettive e soggettive richieste dalla legge per    &#13;
 tale beneficio, a meno che l'interpretazione sostenuta  dalla  stessa    &#13;
 Corte  di  cassazione  non  si riveli contraria a Costituzione. E, in    &#13;
 effetti, continua il giudice a quo, considerare che  l'espulsione  in    &#13;
 oggetto  sia  una misura di sicurezza comportante eccezionalmente una    &#13;
 presunzione legale di pericolosità sociale e, quindi, un automatismo    &#13;
 nell'applicazione che prescinde da ogni accertamento in  concreto  da    &#13;
 parte del giudice della medesima pericolosità, è un'interpretazione    &#13;
 che  sembra contrastare con gli artt. 3, 25, terzo comma, e 27, terzo    &#13;
 comma, della Costituzione.                                               &#13;
    Sotto il primo profilo, l'interpretazione contenuta nel "principio    &#13;
 di diritto" affermato dalla Corte di cassazione appare, innanzitutto,    &#13;
 viziata da irragionevolezza, comportando una ingiustificata deroga al    &#13;
 principio generale secondo il quale l'applicazione  delle  misure  di    &#13;
 sicurezza  debba  avvenire  previa  la  valutazione in concreto della    &#13;
 pericolosità sociale del condannato; in  secondo  luogo,  la  stessa    &#13;
 interpretazione  sembra  comportare  una  disparità  di trattamento,    &#13;
 poiché non consentirebbe che, a situazioni del tutto simili sotto il    &#13;
 profilo soggettivo  e  oggettivo  ai  fini  della  concessione  della    &#13;
 sospensione condizionale della pena, possa corrispondere un'identità    &#13;
 di  valutazione  a  causa  della  preclusione  connessa  a un preteso    &#13;
 ingiustificato  automatismo  nell'applicazione  di  una   misura   di    &#13;
 sicurezza.                                                               &#13;
    La  stessa  interpretazione  sarebbe, poi, in contrasto con l'art.    &#13;
 25,  terzo  comma,  della  Costituzione,  poiché   postulerebbe   la    &#13;
 sottoposizione a una misura di sicurezza in difetto di una previsione    &#13;
 legislativa:  infatti,  a seguito dell'entrata in vigore dell'art. 31    &#13;
 della legge 10 ottobre 1986, n. 663, l'accertamento in concreto della    &#13;
 pericolosità sociale del condannato appare necessario tanto all'atto    &#13;
 dell'applicazione della misura  di  sicurezza  personale,  quanto  al    &#13;
 momento dell'esecuzione della stessa in caso di differimento.            &#13;
    Infine,  l'impugnato art. 86, primo comma, come interpretato dalla    &#13;
 Corte di cassazione  nella  pronunzia  di  annullamento  con  rinvio,    &#13;
 violerebbe  l'art.  27, terzo comma, della Costituzione, per il fatto    &#13;
 che la  misura  di  sicurezza  dell'espulsione,  ove  automaticamente    &#13;
 applicata,   inibirebbe   allo   straniero   ogni   possibilità   di    &#13;
 reinserimento sociale, postulando che sempre e comunque lo  straniero    &#13;
 continuerà  a  delinquere  a prescindere da qualsiasi valutazione da    &#13;
 parte  del  giudice  della  effettiva   pericolosità   sociale   del    &#13;
 condannato.                                                              &#13;
    Né,   conclude   il   giudice   a  quo,  varrebbe  obiettare  che    &#13;
 l'espulsione rappresenti, nei riguardi dello straniero criminale, una    &#13;
 ormai  prevalente  linea  politica  rimessa  alla   valutazione   del    &#13;
 legislatore e avallata da recenti sentenze della Corte costituzionale    &#13;
 sull'art.  7,  commi 12- bis e 12- ter, della legge 28 febbraio 1990,    &#13;
 n. 39. In realtà, l'espulsione dello straniero  regolata  da  queste    &#13;
 ultime  disposizioni, prevista in alternativa alla custodia cautelare    &#13;
 e all'esecuzione della pena, è  del  tutto  diversa  da  quella  ora    &#13;
 esaminata,  trattandosi  di  un  provvedimento  adottato  soltanto su    &#13;
 richiesta dell'interessato o del suo difensore, e non già imposta.      &#13;
    2. - Si è costituito in giudizio il Presidente del Consiglio  dei    &#13;
 ministri chiedendo che la questione sia dichiarata non fondata.          &#13;
    Riguardo  alla pretesa violazione dell'art. 25, terzo comma, della    &#13;
 Costituzione,  l'Avvocatura  dello  Stato  rileva  che   quest'ultimo    &#13;
 articolo si limita a ribadire il principio di legalità in materia di    &#13;
 misure  di  sicurezza,  che  tuttavia  non esclude la possibilità di    &#13;
 tipizzare specifiche ipotesi di pericolosità alle  quali  collegare,    &#13;
 in  via  obbligatoria  e  automatica,  l'applicazione  di determinate    &#13;
 misure indipendentemente da ogni altro accertamento o considerazione.    &#13;
    In ordine alla asserita violazione dell'art. 3 della Costituzione,    &#13;
 l'Avvocatura ricorda che  la  Corte  costituzionale  ha  recentemente    &#13;
 posto  in  rilievo,  in  materia di espulsione, la peculiarità della    &#13;
 posizione dello straniero  in  relazione  alla  tutela  di  interessi    &#13;
 pubblici   inerenti   alla   sanità   pubblica,   alla  politica  di    &#13;
 immigrazione e, ciò che rileva nel caso di specie, alla sicurezza  e    &#13;
 all'ordine  pubblico (nell'ipotesi: nel settore del traffico illecito    &#13;
 di sostanze stupefacenti).                                               &#13;
    Infine, relativamente al  prospettato  contrasto  con  l'art.  27,    &#13;
 terzo  comma,  della  Costituzione,  la  difesa  erariale osserva che    &#13;
 quest'ultima norma, riguardando le modalità  esecutive  della  pena,    &#13;
 non  esclude  che  al  fatto oggettivo dell'accertata responsabilità    &#13;
 penale possano collegarsi ulteriori conseguenze in ordine  alla  vita    &#13;
 di relazione e alla sfera di capacità dell'interessato.                 &#13;
    3.  -  In  prossimità  dell'udienza  l'Avvocatura  dello Stato ha    &#13;
 depositato un'ulteriore memoria  eccependo  l'inammissibilità  della    &#13;
 questione.  Quest'ultima, infatti, risolvendosi in una prospettazione    &#13;
 di dubbi interpretativi o, ad essere più precisi, nella formulazione    &#13;
 dell'auspicio che la Corte operi una scelta fra due diversi  modi  di    &#13;
 interpretare  la  norma  impugnata,  sembra  prospettare un conflitto    &#13;
 giurisprudenziale, non certo  un  vizio  di  costituzionalità  della    &#13;
 norma  impugnata, e perciò, secondo la giurisprudenza costituzionale    &#13;
 (v. ordd. nn. 848 del 1988, 77 del 1990 e 269 del 1991, nonché sent.    &#13;
 n.  26  del   1990),   dovrebbe   esser   dichiarata,   innanzitutto,    &#13;
 inammissibile.<diritto>Considerato in diritto</diritto>1.  -  Il  Tribunale di Roma, giudice di rinvio in un procedimento    &#13;
 penale a carico di uno straniero extracomunitario imputato del  reato    &#13;
 di  cessione  di  sostanza  stupefacente,  ha  sollevato questione di    &#13;
 legittimità costituzionale - per violazione degli artt. 3, 25, terzo    &#13;
 comma,  e  27,  terzo  comma,  della  Costituzione  -  nei  confronti    &#13;
 dell'art.   86,   primo  comma,  del  decreto  del  Presidente  della    &#13;
 Repubblica 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia    &#13;
 di disciplina degli stupefacenti e sostanze psicotrope,  prevenzione,    &#13;
 cura e riabilitazione dei relativi stati di tossicodipendenza), nella    &#13;
 parte  in  cui  obbliga  il  giudice a emettere, contestualmente alla    &#13;
 condanna, l'ordine di  espulsione  dallo  Stato,  eseguibile  a  pena    &#13;
 espiata,  nei  confronti dello straniero condannato per uno dei reati    &#13;
 previsti dagli artt. 73, 74, 79 e 82, commi 2 e 3, precludendogli, in    &#13;
 forza dell'art. 164, secondo comma, n. 2, c.p., la concessione  della    &#13;
 sospensione condizionale della pena inflitta.                            &#13;
    L'Avvocatura     dello     Stato     eccepisce     preliminarmente    &#13;
 l'inammissibilità della questione,  adducendo  che  quest'ultima  si    &#13;
 risolva  nella prospettazione di un dubbio interpretativo e comporti,    &#13;
 quindi,  la  richiesta  a  questa  Corte  di  dirimere  un  conflitto    &#13;
 giurisprudenziale  scegliendo  tra due diversi modi d'interpretare la    &#13;
 norma impugnata.                                                         &#13;
    2. - L'eccezione  d'inammissibilità  va  respinta,  poiché,  sin    &#13;
 dalla  sentenza  n.  30  del  1990,  è  stato  ritenuto "consolidato    &#13;
 indirizzo di questa Corte" quello secondo  il  quale  il  giudice  di    &#13;
 rinvio  può  sollevare,  come  avviene  nel caso di specie, dubbi di    &#13;
 costituzionalità concernenti l'interpretazione normativa  risultante    &#13;
 dal  "principio  di  diritto"  enunciato  dalla  Corte di cassazione:    &#13;
 dovendo, infatti, la norma, nel significato attribuitole dalla  Corte    &#13;
 di cassazione, ricevere ancora applicazione nella fase di rinvio, "il    &#13;
 precludere   che   su   di  essa  vengano  prospettate  questioni  di    &#13;
 legittimità  costituzionale  comporterebbe  un'indubbia   violazione    &#13;
 delle disposizioni regolanti la materia (art. 1, legge costituzionale    &#13;
 9  febbraio  1948, n. 1, e art. 23 della legge 11 marzo 1953, n. 87)"    &#13;
 (v. sent. n. 30 del 1990, nonché sentt. nn. 257 del  1994,  130  del    &#13;
 1993, 2 e 345 del 1987, 21 del 1982, 11 del 1981, 138 del 1977).         &#13;
    Ed invero, nel respingere analoghe eccezioni dell'Avvocatura dello    &#13;
 Stato,  questa  Corte  ha  chiarito,  anche di recente, che, ai sensi    &#13;
 delle ora citate disposizioni regolanti l'accesso delle questioni nel    &#13;
 processo costituzionale, per aversi  una  questione  di  legittimità    &#13;
 costituzionale validamente posta, è sufficiente che il giudice a quo    &#13;
 riconduca   alla  disposizione  contestata  una  interpretazione  non    &#13;
 implausibile della quale egli, a una valutazione compiuta in una fase    &#13;
 meramente iniziale del processo, ritenga di dover  fare  applicazione    &#13;
 nel giudizio principale e sulla quale egli nutra dubbi non arbitrari,    &#13;
 o  non pretestuosi, di conformità a determinate norme costituzionali    &#13;
 (v., ad esempio, sentt. nn. 31 del 1995, 463 del 1994, 51  del  1992,    &#13;
 64   del   1991,   41   del   1990).   La   stessa  Corte  ha,  anzi,    &#13;
 significativamente precisato che la questione di costituzionalità è    &#13;
 validamente  posta  anche  quando  il  giudice  a   quo,   affermando    &#13;
 motivatamente   di   dubitare   dell'orientamento   giurisprudenziale    &#13;
 prevalente o dominante, ritiene di dover  applicare  la  disposizione    &#13;
 contestata  in un diverso o opposto significato normativo, sempreché    &#13;
 l'interpretazione offerta non risulti del tutto implausibile, e cioè    &#13;
 palesemente arbitraria (v. sentt. nn. 463 del 1994,  103,  112,  163,    &#13;
 344 del 1993, 436 del 1992).                                             &#13;
    Sicché  può  aversi questione d'interpretazione, anziché una di    &#13;
 costituzionalità, soltanto nei casi in cui il giudice rimettente non    &#13;
 individua   profili   di   contrasto   con   determinati    parametri    &#13;
 costituzionali  o,  anche se formalmente li indica, in realtà chiede    &#13;
 alla Corte  di  avallare  determinate  ipotesi  interpretative  senza    &#13;
 sostanzialmente  prospettare,  riguardo alle interpretazioni assunte,    &#13;
 dubbi di legittimità costituzionale (v., per  quest'ultima  ipotesi,    &#13;
 ord.  n.  274  del  1991).  Ma  questo  non è il caso dell'ordinanza    &#13;
 introduttiva del presente giudizio, nella quale  il  giudice  a  quo,    &#13;
 dopo  aver illustrato il "principio di diritto" enunciato dalla Corte    &#13;
 di  cassazione,   concernente   l'automatica   sottoposizione   dello    &#13;
 straniero  alla  misura  di  sicurezza  dell'espulsione,  e dopo aver    &#13;
 affermato di  doverne  fare  applicazione  nel  giudizio  principale,    &#13;
 manifesta  il  dubbio  che quel principio sia contrario a determinate    &#13;
 norme  della  Costituzione,   che   comporterebbero   la   necessaria    &#13;
 valutazione  da parte del giudice della sussistenza in concreto della    &#13;
 pericolosità sociale del condannato.                                    &#13;
    Né contro tale  conclusione  possono  desumersi  argomenti  dalle    &#13;
 decisioni  di  questa  Corte citate dall'Avvocatura dello Stato nella    &#13;
 propria memoria  di  udienza.  Le  sentenze  ricordate  dalla  difesa    &#13;
 erariale,  infatti,  riguardano il diverso profilo d'inammissibilità    &#13;
 relativo a questioni proposte in modo "perplesso" o  "alternativo"  o    &#13;
 "ancipite"  o  "ipotetico",  nel  senso  che in quei casi i giudici a    &#13;
 quibus   prospettavano   più   possibilità   interpretative   della    &#13;
 disposizione  contestata  senza  scegliere e, quindi, indicare quella    &#13;
 che essi ritenevano  di  dover  applicare  nel  giudizio  principale.    &#13;
 Questo  orientamento,  indubbiamente consolidato nella giurisprudenza    &#13;
 di questa Corte (v., ad esempio, sentt. nn.  117  del  1994,  51  del    &#13;
 1992,  473, 472 e 456 del 1989, ordd. nn. 325 e 227 del 1994, 207 del    &#13;
 1993, 548 del 1988), è diverso da quello prospettato dall'Avvocatura    &#13;
 dello  Stato,  poiché incide, innanzitutto, sulla problematica della    &#13;
 rilevanza della  questione,  cioè  dell'applicabilità  delle  norme    &#13;
 impugnate  nel  giudizio  principale,  piuttosto  che su quella della    &#13;
 prospettazione, o meno, della violazione di parametri costituzionali,    &#13;
 cioè della valida proposizione  della  questione  nei  suoi  termini    &#13;
 essenziali, prescritti dall'art. 23 della legge n. 87 del 1953.          &#13;
    3. - La questione è fondata.                                         &#13;
    Il  giudice  rimettente contesta la legittimità costituzionale di    &#13;
 un "principio di diritto" enunciato dalla Corte di cassazione in  una    &#13;
 sentenza  di  annullamento,  con  rinvio, di una precedente decisione    &#13;
 dello  stesso  Tribunale  di  Roma,  con  la  quale  a  un  cittadino    &#13;
 marocchino,  condannato  per  cessione  di sostanza stupefacente, era    &#13;
 stata concessa la sospensione condizionale della pena.  Ponendosi  in    &#13;
 una  posizione  contraria  rispetto  ad  altre decisioni della stessa    &#13;
 Corte di cassazione, per le  quali  l'applicazione  della  misura  di    &#13;
 sicurezza  dell'espulsione  prevista  dall'art.  86, primo comma, del    &#13;
 d.P.R. n. 309  del  1990  comporta  l'accertamento  giudiziale  della    &#13;
 pericolosità sociale del condannato, la sentenza di annullamento con    &#13;
 rinvio enuncia il "principio di diritto" secondo il quale la predetta    &#13;
 misura  di  sicurezza  dell'espulsione,  eseguibile  a  pena espiata,    &#13;
 dev'essere ordinata con la  sentenza  di  condanna  come  conseguenza    &#13;
 automatica  della  commissione  del  reato  di  cui all'art. 73 dello    &#13;
 stesso testo unico, con l'effetto di costituire  giuridico  ostacolo,    &#13;
 per  l'art.  164,  secondo  comma, n. 2, c.p., alla concessione della    &#13;
 sospensione condizionale della pena. Così  interpretato,  il  citato    &#13;
 art.  86,  primo  comma,  si  pone  in  contrasto  con l'art. 3 della    &#13;
 Costituzione.                                                            &#13;
    Configurata  quale  misura  di  sicurezza  -  come  ritengono   la    &#13;
 giurisprudenza  di merito e la stessa Corte di cassazione anche nella    &#13;
 sopra menzionata sentenza di annullamento con rinvio  -  l'espulsione    &#13;
 del  condannato  straniero  prevista  dall'art.  86, primo comma, del    &#13;
 d.P.R. n. 309 del 1990  va  inquadrata  nell'ambito  dell'ordinamento    &#13;
 penale,  nel  quale, in seguito all'adozione dell'art. 31 della legge    &#13;
 10 ottobre 1986, n. 663 (che ha abrogato l'art. 204  c.p.),  vige  il    &#13;
 principio  che "tutte le misure di sicurezza personali sono ordinate,    &#13;
 previo accertamento che colui il  quale  ha  commesso  il  fatto,  è    &#13;
 persona  socialmente  pericolosa". Rispetto a tale principio generale    &#13;
 dell'ordinamento penale, un'ipotesi  di  presunzione  ex  lege  della    &#13;
 qualità  di  persona  socialmente  pericolosa,  come  è configurata    &#13;
 dall'interpretazione  contestata  dal  giudice  a   quo,   dev'essere    &#13;
 sottoposta,  sotto  il  profilo  dell'accertamento della legittimità    &#13;
 costituzionale,  al  vaglio  di  un  rigoroso   scrutinio.   Infatti,    &#13;
 qualunque sia la natura che ontologicamente si intende assegnare alle    &#13;
 misure  di  sicurezza,  è  indubitabile  che  le misure di sicurezza    &#13;
 personali comportano comunque la privazione o  la  limitazione  della    &#13;
 libertà  personale e, quindi, incidono in ogni caso su un valore che    &#13;
 l'art. 13  della  Costituzione  riconosce  come  diritto  inviolabile    &#13;
 dell'uomo, sia esso cittadino o straniero (v., da ultimo, sent. n. 62    &#13;
 del  1994).  Ed  è  giurisprudenza  di  questa  Corte che, di fronte    &#13;
 all'incisione   di   beni   di   tal   pregio,   il   controllo    di    &#13;
 costituzionalità  delle  norme  di legge contestate deve avvenire in    &#13;
 modo da garantire che il sacrificio della libertà  sia  giustificato    &#13;
 dall'effettiva  realizzazione  di  altri  valori costituzionali o non    &#13;
 vada incontro a ostacoli insormontabili costituiti  dalla  protezione    &#13;
 di  altri  valori  costituzionali  (v., ad esempio, sentt. nn. 63 del    &#13;
 1994, 81 del 1993, 368 del 1992, 366 del 1991).                          &#13;
    Dall'interpretazione data dalla Corte di cassazione  all'impugnato    &#13;
 art. 86, primo comma, con il "principio di diritto" contestato deriva    &#13;
 che   quest'ultimo   impone   al  giudice  l'applicazione  automatica    &#13;
 dell'ordine di espulsione nei confronti  dello  straniero  condannato    &#13;
 per  alcuno  dei  reati indicati dalla stessa disposizione censurata,    &#13;
 senza consentire ad esso l'accertamento della sussistenza in concreto    &#13;
 delle  condizioni  per  un  giudizio  di  pericolosità  sociale  del    &#13;
 condannato. In conseguenza di ciò, al giudice di merito è preclusa,    &#13;
 ai  sensi  dell'art.  164,  secondo comma, n. 2, c.p., la concessione    &#13;
 della sospensione condizionale della pena inflitta, pur nelle ipotesi    &#13;
 nelle  quali  sussistano,  come  nel  caso   dedotto   nel   giudizio    &#13;
 principale,  i requisiti soggettivi e oggettivi richiesti dalla legge    &#13;
 per l'erogazione di tal beneficio.                                       &#13;
    Di fronte all'esistenza in concreto di tali requisiti, la predetta    &#13;
 preclusione    evidenzia    che    l'art.    86,     primo     comma,    &#13;
 nell'interpretazione  contestata dal giudice a quo, non contiene, per    &#13;
 chi abbia commesso i reati ivi indicati,  altro  presupposto  legale,    &#13;
 per  la  determinazione  presuntiva  della  pericolosità sociale del    &#13;
 soggetto, che il fatto della condizione di straniero del  condannato.    &#13;
 Messa a confronto con le altre ipotesi di applicabilità della misura    &#13;
 di sicurezza dell'espulsione, previste dagli artt. 235 e 312 c.p., le    &#13;
 quali,  pur  essendo subordinate al presupposto di condotte obiettive    &#13;
 altrettanto gravi rispetto a quelle considerate  nell'impugnato  art.    &#13;
 86,  primo  comma,  comportano  pur  sempre,  in ossequio alla regola    &#13;
 generale stabilita dal ricordato art. 31 della legge n. 663 del 1986,    &#13;
 la valutazione da parte del giudice  della  sussistenza  in  concreto    &#13;
 della  pericolosità  sociale  dello  straniero condannato, l'ipotesi    &#13;
 contestata configura un'irragionevole disparità  di  trattamento.  E    &#13;
 l'irragionevolezza   risulta  evidente  se  si  considera  anche  che    &#13;
 l'applicazione della misura di sicurezza della  espulsione  senza  la    &#13;
 valutazione   del   giudice  alla  stregua  degli  indici  menzionati    &#13;
 dall'art. 133 c.p. (cui fa  rinvio  l'art.  203,  cpv.,  c.p.)  e  la    &#13;
 conseguente   preclusione   della   concessione   della   sospensione    &#13;
 condizionale della  pena  frappongono  ingiustificati  ostacoli,  non    &#13;
 soltanto  alla  libertà  personale,  ma  anche  alle possibilità di    &#13;
 sviluppo della personalità del condannato  in  vista  dell'eventuale    &#13;
 superamento   della   sua   condizione   come   soggetto  socialmente    &#13;
 pericoloso.                                                              &#13;
    Né tale conclusione può  essere  efficacemente  contrastata  dal    &#13;
 richiamo,  operato  dall'Avvocatura  dello  Stato,  alle  sentenze di    &#13;
 questa Corte nn. 62 e 283 del  1994,  relative  all'espulsione  dello    &#13;
 straniero prevista dall'art. 7, commi 12- bis e 12- ter, del decreto-legge  30  dicembre  1989,  n. 416, nel testo introdotto dall'art. 8,    &#13;
 primo comma, del decreto-legge 14 giugno 1993, n.  187.    In  quelle    &#13;
 decisioni,  infatti, è stato precisato che l'espulsione ivi prevista    &#13;
 è un istituto diverso da  quello  ora  considerato,  trattandosi  di    &#13;
 un'ipotesi  di  sospensione della esecuzione della custodia cautelare    &#13;
 in carcere, ovvero dell'espiazione  della  pena,  condizionata  dalla    &#13;
 richiesta  dell'interessato (o del suo difensore), richiesta che - si    &#13;
 precisa nella sentenza n. 62 del 1994 - "rappresenta, come si  deduce    &#13;
 anche   dai   lavori   preparatori,   un   requisito  diretto,  nella    &#13;
 fattispecie, ad armonizzare la condizione dello straniero  ai  valori    &#13;
 costituzionali  cui  il  legislatore deve riferirsi nel prevedere una    &#13;
 misura pur sempre incidente sulla libertà  personale,  cioè  su  un    &#13;
 diritto inviolabile dell'uomo".</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara l'illegittimità costituzionale dell'art. 86, primo comma,    &#13;
 del  decreto  del  Presidente della Repubblica 9 ottobre 1990, n. 309    &#13;
 (Testo unico delle leggi in materia di disciplina degli  stupefacenti    &#13;
 e   sostanze  psicotrope,  prevenzione,  cura  e  riabilitazione  dei    &#13;
 relativi stati di tossicodipendenza), nella parte in cui  obbliga  il    &#13;
 giudice   a  emettere,  senza  l'accertamento  della  sussistenza  in    &#13;
 concreto della pericolosità sociale, contestualmente alla  condanna,    &#13;
 l'ordine  di  espulsione,  eseguibile  a  pena espiata, nei confronti    &#13;
 dello straniero condannato per uno dei reati previsti dagli artt. 73,    &#13;
 74, 79 e 82, commi 2 e 3, del medesimo testo unico.                      &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 febbraio 1995.                             &#13;
                        Il Presidente: SPAGNOLI                           &#13;
                       Il redattore: BALDASSARRE                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 24 febbraio 1995.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
