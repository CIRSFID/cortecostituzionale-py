<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>226</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:226</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia>Annibale Marini</redattore_pronuncia>
    <data_decisione>22/05/2002</data_decisione>
    <data_deposito>29/05/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 25, primo comma, della legge 27 dicembre 1953, n. 968 (Concessione di indennizzi e contributi per danni di guerra), promosso con ordinanza emessa il 14 marzo 2000 dal Consiglio di Stato sul ricorso proposto dal Ministero del tesoro contro Rossi Franca ed altri, iscritta al n. 713 del registro ordinanze 2000 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2000. &#13;
    Visti l'atto di costituzione di Rossi Franca ed altri nonché l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nell'udienza pubblica del 9 aprile 2002 il Giudice relatore Annibale Marini; &#13;
    uditi l'avvocato Stanislao Aureli per Rossi Franca ed altri e l'avvocato dello Stato Franco Favara per il Presidente del Consiglio dei ministri. &#13;
    Ritenuto che il Consiglio di Stato, nel corso di un giudizio per l'ottemperanza di una precedente pronuncia del giudice amministrativo resa nei confronti del Ministro del tesoro per la liquidazione di un indennizzo per danni di guerra, con ordinanza emessa il 14 marzo 2000 e depositata il 2 giugno 2000, ha sollevato, in riferimento agli artt. 2 e 3 della Costituzione, questione di legittimità costituzionale dell'art. 25, primo comma, della legge 27 dicembre 1953, n. 968 (Concessione di indennizzi e contributi per danni di guerra), limitatamente alla parola «cinque»; &#13;
    che, ad avviso del rimettente, la norma impugnata, prevedendo che l'indennizzo sia concesso in misura pari all'entità del danno valutato ai prezzi vigenti al 30 giugno 1943, moltiplicato per il coefficiente 5, precluderebbe qualsiasi ulteriore rivalutazione dell'indennizzo stesso ad opera del giudice; &#13;
    che in tal modo, secondo lo stesso giudice, la norma denunciata si porrebbe in contrasto sia con il principio di ragionevolezza sia con quello di solidarietà sociale ed etica, in quanto il suddetto coefficiente sarebbe ormai del tutto inidoneo a mantenere un minimo di proporzionalità tra la misura dell'indennizzo - quando esso sia liquidato, come nella specie, a notevole distanza di tempo dai fatti - ed i danni effettivamente patiti; &#13;
    che si sono costituite in giudizio Franca e Paola Rossi, ricorrenti nel giudizio a quo, concludendo per l'accoglimento della questione ovvero per l'adozione di una pronuncia interpretativa di rigetto sulla effettiva portata della norma denunciata; &#13;
    che, ad avviso delle parti private, il lungo tempo trascorso nella specie tra il fatto dannoso e l'erogazione dell'indennizzo sarebbe di per sé «elemento indiscutibile di prova» della irragionevolezza della disposizione censurata, se interpretata nel senso di ritenerla preclusiva di ulteriore rivalutazione monetaria; &#13;
    che peraltro, secondo le stesse parti, il coefficiente di moltiplicazione previsto dalla norma andrebbe più esattamente qualificato come coefficiente «di riparazione» e non di rivalutazione, tale dunque da non precludere l'ulteriore rivalutazione monetaria dell'indennizzo; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la declaratoria di inammissibilità della questione o, in alternativa, per la restituzione degli atti al giudice rimettente per una nuova valutazione sulla rilevanza della questione e comunque, in subordine, nel merito, per la declaratoria di manifesta infondatezza della questione stessa; &#13;
    che, secondo la parte pubblica, le domande di rivalutazione ed interessi, sulle quali il rimettente ritiene di doversi pronunciare, sarebbero state già respinte, nel giudizio di merito, con sentenza passata in giudicato, cosicché la questione risulterebbe priva di rilevanza; &#13;
    che potrebbe, semmai, ritenersi ritualmente introdotta nel giudizio a quo la sola domanda di rivalutazione riferita al periodo di pendenza della lite o a quello della inottemperanza; &#13;
    che sulla domanda come sopra individuata il rimettente avrebbe tuttavia potuto pronunciarsi direttamente ovvero avrebbe potuto sollevare la questione di legittimità costituzionale con riguardo esclusivo alla mancata esplicita previsione di una rivalutazione dei crediti pecuniari durante il protrarsi della asserita inottemperanza; &#13;
    che peraltro, qualora i ricorrenti intendessero far valere una pretesa risarcitoria fondata sull'asserito ritardo colposo dell'amministrazione nell'erogazione dell'indennizzo, tale pretesa dovrebbero azionare - ad avviso sempre della parte pubblica - in un distinto giudizio e non già introdurre surrettiziamente nel giudizio di ottemperanza, attraverso la domanda di rivalutazione dell'indennizzo; &#13;
    che in ogni caso, nel merito, l'infondatezza della questione discenderebbe dalla natura stessa degli indennizzi per danni di guerra, in quanto contributi di solidarietà a carico della generalità dei cittadini, la cui funzione non sarebbe quella di offrire un'integrale riparazione dei pregiudizi subiti ed ai quali non potrebbero perciò estendersi i principi propri delle obbligazioni risarcitorie; &#13;
    che qualsiasi rivalutazione degli indennizzi in questione risulterebbe d'altro canto preclusa dalla rigidità dei relativi stanziamenti di bilancio. &#13;
    Considerato che il rimettente - il quale assume di doversi ormai pronunciare sulle sole domande di rivalutazione ed interessi -  muovendo dal presupposto che sia precluso al giudice di procedere alla rivalutazione monetaria degli indennizzi previsti dalla legge 27 dicembre 1953, n. 968 (Concessione di indennizzi e contributi per danni di guerra), e successive integrazioni e modifiche, e ritenendo siffatta preclusione in contrasto con gli artt. 2 e 3 della Costituzione, invoca una pronuncia mediante la quale sia espunto dall'art. 25, primo comma, della suddetta legge n. 968 del 1953 il riferimento al «coefficiente 5»; &#13;
    che siffatto petitum risulta tuttavia palesemente incongruo e comunque irrilevante rispetto al risultato che il rimettente vorrebbe perseguire; &#13;
    che la norma impugnata concerne infatti esclusivamente i criteri di determinazione degli indennizzi per la perdita, la distruzione o il danneggiamento di cose mobili o immobili in dipendenza di un fatto di guerra, mentre i profili concernenti la possibilità di rivalutazione di detti indennizzi - cui lo stesso rimettente ritiene non possa attribuirsi la natura di debiti di valore - sono regolati da diverse disposizioni e principi dell'ordinamento; &#13;
      che la caducazione del previsto coefficiente di moltiplicazione comporterebbe perciò, quale unica conseguenza, la liquidazione dell'indennizzo con riferimento ai prezzi vigenti al 30 giugno 1943, senza spiegare alcun effetto ai fini della decisione sulla domanda di rivalutazione dell'indennizzo stesso; &#13;
    che l'evidente difetto di rilevanza comporta la declaratoria di manifesta inammissibilità della questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 25, primo comma, della legge 27 dicembre 1953, n. 968 (Concessione di indennizzi e contributi per danni di guerra), sollevata, in riferimento agli artt. 2 e 3 della Costituzione, dal Consiglio di Stato con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 maggio 2002. &#13;
F.to: &#13;
Cesare RUPERTO, Presidente &#13;
Annibale MARINI, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 29 maggio 2002. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
