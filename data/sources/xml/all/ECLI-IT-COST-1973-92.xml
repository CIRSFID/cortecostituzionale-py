<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>92</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:92</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Leonetto Amadei</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/06/1973</data_decisione>
    <data_deposito>27/06/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Dott. &#13;
 LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. &#13;
 ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Prof. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  54,  terzo  &#13;
 comma, del codice di procedura civile, promosso con ordinanza emessa il  &#13;
 22  febbraio  1971  dal  pretore  di  Bari su istanza di Elia Giuseppe,  &#13;
 iscritta al n. 165 del  registro  ordinanze  1971  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 151 del 16 giugno 1971.           &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 3 maggio 1973 il  Giudice  relatore  &#13;
 Leonetto Amadei;                                                         &#13;
     udito  il sostituto avvocato generale dello Stato Michele Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ricorso 20 giugno 1970, diretto al Presidente del tribunale  di  &#13;
 Bari,  Giuseppe Elia proponeva istanza di ricusazione nei confronti del  &#13;
 pretore dott.  Michele  Paone,  giudice  della  causa  di  sfratto  per  &#13;
 morosità intentata da esso Elia contro tal Vincenzo Francone.           &#13;
     Nelle  more  del  procedimento,  l'Elia  Giuseppe,  nelle deduzioni  &#13;
 presentate, dichiarava di essersi  sbagliato  nella  valutazione  degli  &#13;
 elementi   posti   a   fondamento  dell'istanza  di  ricusazione  e  di  &#13;
 rinunciare, pertanto, ad essa.                                           &#13;
     Con ordinanza 28 agosto 1970, ampiamente  motivata,  il  Presidente  &#13;
 del  tribunale  di Bari, dopo aver rilevato che la rinuncia all'istanza  &#13;
 di ricusazione non poteva produrre gli effetti previsti  dall'art.  306  &#13;
 del  codice  di  procedura  civile  (rinuncia  agli atti del giudizio),  &#13;
 rigettava l'istanza stessa e condannava, ex  art.    54  c.p.c.,  terzo  &#13;
 comma,  il  proponente  alle  spese  del  procedimento  e  ad  una pena  &#13;
 pecuniaria di lire 2.000.                                                &#13;
     La cancelleria della pretura  di  Bari  (ufficio  campione  civile)  &#13;
 richiedeva  all'Elia,  con  atto del 18 novembre 1970, notificato il 27  &#13;
 gennaio 1971,  il  pagamento  della  pena  pecuniaria  irrogatagli  dal  &#13;
 Presidente del tribunale.                                                &#13;
     In  data 11 febbraio 1971, l'Elia inoltrava un esposto al dirigente  &#13;
 la pretura di Bari chiedendo la  trasmissione  degli  atti  alla  Corte  &#13;
 costituzionale   perché   fosse   dichiarata   la  incostituzionalità  &#13;
 dell'art. 54, terzo comma, del codice di procedura civile,  per  essere  &#13;
 tale norma in contrasto con gli artt. 3, 24 e 111 della Costituzione.    &#13;
     Il  pretore dichiarava non manifestamente infondata la questione di  &#13;
 legittimità  costituzionale  e,  sulla  base  del  semplice   esposto,  &#13;
 ordinava  la  trasmissione  degli  atti alla Corte con ordinanza del 22  &#13;
 febbraio 1971.                                                           &#13;
     Nella schematica motivazione si afferma che la disposizione di  cui  &#13;
 all'art.  54,  terzo  comma,  del  codice  di  procedura  civile, è in  &#13;
 contrasto con i richiamati precetti costituzionali, in quanto:           &#13;
     a) la sanzione della pena pecuniaria,  che  accompagua  il  rigetto  &#13;
 dell'istanza,  costituirebbe  una  limitazione del libero esercizio del  &#13;
 diritto di difesa del cittadino, per quel  tanto  di  timore  che  può  &#13;
 determinare  nella  parte interessata ad agire perché le sia garantita  &#13;
 la imparzialità del giudice;                                            &#13;
     b) per il procedimento di rigetto non  è  prevista  motivazione  e  &#13;
 possibilità  di  impugnazione,  quantunque  incida  anche  su  diritti  &#13;
 patrimoniali del ricorrente.                                             &#13;
     La parte non si è costituita nel giudizio davanti alla Corte.       &#13;
     È  intervenuto  il  Presidente   del   Consiglio   dei   ministri,  &#13;
 rappresentato  e  difeso dall'Avvocatura dello Stato, che ha depositato  &#13;
 in cancelleria il 5 luglio 1971 le sue deduzioni.                        &#13;
     L'Avvocatura  dello  Stato  contesta,  in   via   preliminare,   la  &#13;
 legittimazione    del    giudice    a   sollevare   la   questione   di  &#13;
 incostituzionalità  per  la  mancanza  di  un   giudizio   di   merito  &#13;
 (impugnazione  del  titolo  esecutivo), in quanto l'unica materia nella  &#13;
 quale esso è stato chiamato a decidere è stata quella di richiesta di  &#13;
 rimessione degli atti alla Corte costituzionale.                         &#13;
     Nel merito rileva che:                                               &#13;
     a) il pretore non ha precisato per quali motivi ha ritenuto vi  sia  &#13;
 stata,  nel caso, violazione del principio di eguaglianza dei cittadini  &#13;
 davanti alla legge (art. 3 Cost.);                                       &#13;
     b)  non   sussisterebbe   la   violazione   dell'art.   111   della  &#13;
 Costituzione,  nella  parte  in  cui dispone che tutti i provvedi menti  &#13;
 giurisdizionali debbono essere motivati, sia  perché  la  denuncia  di  &#13;
 incostituzionalità  avrebbe  dovuto  colpire l'art. 53, comma secondo,  &#13;
 del codice di procedura  civile,  come  quello  che  prevede  la  forma  &#13;
 dell'ordinanza  non  impugnabile  per la decisione sulla ricusazione, e  &#13;
 non  già  il  successivo  art.  54,  comma  terzo,  sia   perché   il  &#13;
 provvedimento  di ricusazione non si sottrarrebbe al principio generale  &#13;
 stabilito dall'art. 134 c.p.c., in forza del quale anche  le  ordinanze  &#13;
 debbono essere succintamente motivate;                                   &#13;
     c)   non   sussisterebbe,  neppure,  lesione  dell'art.  111  della  &#13;
 Costituzione,  nella  parte  in  cui  stabilisce  che   per   tutti   i  &#13;
 provvedimenti  giurisdizionali  è  ammesso  ricorso  in cassazione per  &#13;
 violazione di legge, in quanto, per la  costante  giurisprudenza  della  &#13;
 Corte  di  cassazione,  il  provvedimento  di ricusazione non ha natura  &#13;
 giurisdizionale, ma ha solo carattere  meramente  strumentale  rispetto  &#13;
 alla  lite  nella quale si inserisce, senza pregiudizio per i diritti e  &#13;
 per gli interessi della parte in causa.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con  l'ordinanza  di rinvio il pretore di Bari, a seguito di  &#13;
 istanza a lui diretta nella fase di intimazione all'istante,  da  parte  &#13;
 della cancelleria civile, di pagamento di pena pecuniaria conseguente a  &#13;
 condanna  per  rigetto di proposta ricusazione del giudice in una causa  &#13;
 di sfratto, ha posto la questione di legittimità dell'art.  54,  terzo  &#13;
 comma,  del codice di procedura civile, in riferimento agli artt. 3, 24  &#13;
 e 111 della Costituzione.                                                &#13;
     2. - La  questione  deve  essere  preliminarmente  esaminata  sotto  &#13;
 l'aspetto  della sua proponibilità, ossia sulla base dell'accertamento  &#13;
 della sussistenza o meno delle condizioni che possono  legittimarne  la  &#13;
 proposizione.                                                            &#13;
     3.  -  Condizione  essenziale perché una questione di legittimità  &#13;
 costituzionale possa essere sollevata, è che essa lo sia nel corso  di  &#13;
 un  giudizio  in relazione alle limitazioni stabilite dall'art. 1 della  &#13;
 legge costituzionale n. 1 del 9 febbraio 1948 e ribadite  dall'art.  23  &#13;
 della legge 11 marzo 1953, n. 87.                                        &#13;
     Nella sentenza 30 giugno 1964, n. 65, questa Corte ha precisato non  &#13;
 essere  sufficiente che una domanda sia presentata, né che con essa si  &#13;
 chieda comunque la istituzione di un giudizio, ma occorre,  quantomeno,  &#13;
 che effettivamente ricorrano i presupposti in base ai quali un siffatto  &#13;
 giudizio  possa dirsi concretamente ed effettivamente instaurato con un  &#13;
 suo proprio autonomo svolgimento, in modo da poter essere  indirizzato,  &#13;
 per  suo  conto, ad una propria conclusione al di fuori della questione  &#13;
 di legittimità, il cui insorgere è soltanto eventuale.                 &#13;
     In sostanza, il processo di merito  si  presenta  come  presupposto  &#13;
 rispetto al processo instaurato davanti alla Corte costituzionale.       &#13;
     4  -  Nel caso all'esame della Corte difetta, in senso assoluto, la  &#13;
 precostituzione di un qualsiasi giudizio; la  questione  che  è  stata  &#13;
 sollevata  si incardina unicamente in una istanza diretta al pretore al  &#13;
 fine  di   sollecitarne   la   rimessione   degli   atti   alla   Corte  &#13;
 costituzionale,  senza  che dall'istanza stessa si profili la richiesta  &#13;
 di una qualsiasi pronuncia giurisdizionale.                              &#13;
     Infatti,  come  risulta  in  narrativa,  l'interessato,  dopo  aver  &#13;
 ricevuto  dalla cancelleria civile della pretura l'intimazione a pagare  &#13;
 l'importo  della  pena  pecuniaria  applicatagli  dal  Presidente   del  &#13;
 tribunale,   si  è  limitato  a  inoltrare  al  pretore,  con  lettera  &#13;
 raccomandata, un'istanza  chiaramente  delimitata  nel  suo  contenuto;  &#13;
 diretta, cioè , ad ottenere che fosse sollevata, dallo stesso pretore,  &#13;
 questione  di legittimità della norma in base alla quale gli era stata  &#13;
 inflitta la pena pecuniaria.                                             &#13;
     L'istanza, pertanto, non prospetta una domanda diretta a  impostare  &#13;
 una  controversia  di  merito,  sulla  quale il pretore deve decidere e  &#13;
 nella decisione della quale  si  è  inserita  in  via  incidentale  la  &#13;
 questione  di  legittimità. Ciò è tanto vero che, nell'ordinanza che  &#13;
 accoglie  la  richiesta,  non  vi  è,  e  non  poteva  esservi,  alcun  &#13;
 riferimento  all'esistenza  di  un autonomo giudizio; ma ci si limita a  &#13;
 prendere atto della  semplice  volontà  dell'istante  di  proporre  la  &#13;
 questione  di  legittimità costituzionale di una disposizione di legge  &#13;
 che aveva trovato applicazione in un  altro  giudizio,  davanti  ad  un  &#13;
 altro  giudice,  al  quale,  e  a  lui solo, la questione poteva essere  &#13;
 proposta.                                                                &#13;
     La questione, pertanto, deve essere dichiarata inammissibile.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art.  54,  terzo  comma,  del  codice  di  procedura  civile,   in  &#13;
 riferimento  agli  artt.  3, 24 e 111 della Costituzione, sollevata dal  &#13;
 pretore di Bari con ordinanza 22 febbraio 1971.                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 14 giugno 1973.                               &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
