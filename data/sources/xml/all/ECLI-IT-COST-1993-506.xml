<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>506</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:506</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/12/1993</data_decisione>
    <data_deposito>31/12/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale degli artt. 1, 2, 7, 9 e    &#13;
 10 del decreto-legge 17 luglio 1993, n. 232 (Disposizioni in  materia    &#13;
 di  legittimità  dell'azione  amministrativa),  promosso con ricorso    &#13;
 della Regione autonoma della Valle d'Aosta, notificato  il  7  agosto    &#13;
 1993, depositato in cancelleria il 10 successivo ed iscritto al n. 34    &#13;
 del registro ricorsi 1993;                                               &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di consiglio del 1  dicembre  1993  il  Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto  che la Regione autonoma della Valle d'Aosta, con ricorso    &#13;
 notificato il 7 agosto 1993 e depositato il successivo 10 agosto,  ha    &#13;
 sollevato  questione di legittimità costituzionale degli artt. 1, 2,    &#13;
 7, 9 e 10 del decreto-legge 17 luglio 1993, n. 232  (Disposizioni  in    &#13;
 materia  di  legittimità  dell'azione  amministrativa), deducendo la    &#13;
 violazione  degli  artt.  77,  100,  103,  108,  116  e   125   della    &#13;
 Costituzione,  nonché  degli  artt.  2, 3, 4, 29, 38, 43 e 46, primo    &#13;
 comma, del proprio Statuto (legge costituzionale 26 febbraio 1948, n.    &#13;
 4);                                                                      &#13;
      che, in  particolare,  le  disposizioni  impugnate,  applicabili    &#13;
 anche  alle  Regioni  a  statuto  speciale  per l'espressa previsione    &#13;
 contenuta  nell'art.  10,  concernono  l'istituzione  delle   sezioni    &#13;
 regionali   della   Corte   dei   conti  (art.  1),  l'individuazione    &#13;
 dell'organo incaricato dello svolgimento delle funzioni  di  pubblico    &#13;
 ministero presso le sezioni regionali della Corte dei conti (art. 2),    &#13;
 la  individuazione  degli atti, anche delle regioni, da sottoporre al    &#13;
 controllo della Corte dei conti, nonché dei modi e dei contenuti del    &#13;
 controllo successivo (art. 7), e, infine, la istituzione dei  servizi    &#13;
 di  controllo  interno  in  tutte  le amministrazioni pubbliche, e le    &#13;
 relative modalità di funzionamento (art. 9);                            &#13;
      che, ad  avviso  della  ricorrente,  le  disposizioni  impugnate    &#13;
 sarebbero  variamente  lesive  delle proprie competenze, dei principi    &#13;
 costituzionali in materia di controllo sugli atti delle regioni e  di    &#13;
 tutela   delle   minoranze  linguistiche  riconosciute,  nonché  dei    &#13;
 principi che regolano la decretazione d'urgenza e la riserva di legge    &#13;
 formale, prevista dagli  artt.  100,  secondo  e  terzo  comma,  103,    &#13;
 secondo comma, e 108 della Costituzione;                                 &#13;
      che  si  è costituito il Presidente del Consiglio dei ministri,    &#13;
 chiedendo  che  il  ricorso  sia  dichiarato   inammissibile   ovvero    &#13;
 infondato;                                                               &#13;
    Considerato  che  il  decreto-legge  17 luglio 1993, n. 232 non è    &#13;
 stato convertito in legge nel termine di sessanta  giorni  dalla  sua    &#13;
 pubblicazione,  come risulta dal comunicato pubblicato nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 217 del 15 settembre 1993;                 &#13;
      che, pertanto, in  conformità  alla  giurisprudenza  di  questa    &#13;
 Corte  (v.,  da ultimo, l'ordinanza n. 470 del 1993), le questioni di    &#13;
 legittimità costituzionale sollevate  dalla  Regione  Valle  d'Aosta    &#13;
 devono essere dichiarate manifestamente inammissibili;                   &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   delle  questioni  di    &#13;
 legittimità  costituzionale  degli  artt.  1,  2,  7,  9  e  10  del    &#13;
 decreto-legge  17  luglio  1993,  n.  232 (Disposizioni in materia di    &#13;
 legittimità dell'azione  amministrativa),  sollevate  dalla  Regione    &#13;
 autonoma  della  Valle  d'Aosta, con il ricorso indicato in epigrafe,    &#13;
 per violazione degli artt.  77,  100,  103,  108,  116  e  125  della    &#13;
 Costituzione,  nonché  degli  artt.  2, 3, 4, 29, 38, 43 e 46, primo    &#13;
 comma, dello Statuto speciale approvato con legge  costituzionale  26    &#13;
 febbraio 1948, n. 4.                                                     &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 29 dicembre 1993.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: BALDASSARRE                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 31 dicembre 1993.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
