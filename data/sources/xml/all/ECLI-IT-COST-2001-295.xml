<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>295</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:295</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2001</data_decisione>
    <data_deposito>25/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, CarloMEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 41 della legge &#13;
18 ottobre  1961,  n. 1168  (Norme  sullo  stato  giuridico  dei vice &#13;
brigadieri  e  dei militari di truppa dell'Arma dei carabinieri), "in &#13;
combinato  disposto" con l'art. 73 della legge 31 luglio 1954, n. 599 &#13;
(Stato    dei    sottufficiali    dell'Esercito,   della   Marina   e &#13;
dell'Aeronautica),  promosso  con  ordinanza emessa il 10 luglio 1998 &#13;
dal  tribunale  amministrativo  regionale della Campania, iscritta al &#13;
n. 835  del  registro  ordinanze  2000  e  pubblicata  nella Gazzetta &#13;
Ufficiale della Repubblica n. 3, 1ª serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 23 maggio 2001 il giudice &#13;
relatore Valerio Onida. &#13;
    Ritenuto che, con ordinanza emessa il 10 giugno 1998, pervenuta a &#13;
questa   Corte  il  18 dicembre  2000,  il  tribunale  amministrativo &#13;
regionale  della  Campania  ha  sollevato  questione  di legittimità &#13;
costituzionale,   in   riferimento  all'art. 97  della  Costituzione, &#13;
dell'art. 41  della legge 18 ottobre 1961, n. 1168 (Norme sullo stato &#13;
giuridico  dei vice brigadieri e dei militari di truppa dell'Arma dei &#13;
carabinieri),  "in  combinato  disposto"  con  l'art. 73  della legge &#13;
31 luglio  1954, n. 599 (Stato dei sottufficiali dell'Esercito, della &#13;
Marina e dell'Aeronautica); &#13;
        che  il  remittente premette che il ricorrente nel giudizio a &#13;
quo,  impugnando  i  provvedimenti  con  cui  era  stato sottoposto a &#13;
procedimento  disciplinare  e  colpito  dalla sanzione di perdita del &#13;
grado  per rimozione, aveva lamentato fra l'altro che gli fosse stata &#13;
preclusa   la  possibilità  di  farsi  difendere,  nel  procedimento &#13;
disciplinare  medesimo,  da  un militare non appartenente al medesimo &#13;
corpo,  e  aveva eccepito l'illegittimità costituzionale della norma &#13;
che  prevede la obbligatorietà della difesa ad opera di un ufficiale &#13;
appartenente al medesimo corpo; &#13;
        che  il  giudice  a  quo  ricorda la sentenza di questa Corte &#13;
n. 37   del   1992,   la   quale   ha   dichiarato   l'illegittimità &#13;
costituzionale  dell'art. 15,  secondo  comma,  della legge 11 luglio &#13;
1978,  n. 382,  nella  parte  in  cui  non  prevedeva per il militare &#13;
sottoposto  a  procedimento disciplinare la facoltà di indicare come &#13;
difensore  nel procedimento stesso un altro militare non appartenente &#13;
all'ente nel quale egli presta servizio; &#13;
        che,  sempre  secondo  il  remittente, analoga limitazione al &#13;
diritto  di  difesa  sarebbe prevista in base all'art. 41 della legge &#13;
n. 1168  del  1961  e  all'art. 74 (recte: 73) della legge n. 599 del &#13;
1954,  e sarebbe tuttora in vigore, nonostante la citata pronuncia di &#13;
questa  Corte,  in ragione della specialità del rapporto di servizio &#13;
dei  sottufficiali  nonché  dei  vice  brigadieri  e dei militari di &#13;
truppa dell'Arma dei carabinieri, di cui alle leggi da ultimo citate; &#13;
        che,  tuttavia, tali norme sarebbero di dubbia compatibilità &#13;
con   l'art. 97   della  Costituzione,  il  quale  esigerebbe  regole &#13;
procedurali  sull'attività  amministrativa dirette ad assicurare una &#13;
esatta  valutazione  degli  interessi  coinvolti  nella decisione: la &#13;
limitata    possibilità   di   scelta   del   difensore   da   parte &#13;
dell'incolpato,  in  forza  della  quale il difensore sarebbe in ogni &#13;
caso in posizione di subordinazione gerarchica rispetto al comandante &#13;
di  corpo,  che  è  l'autorità chiamata a decidere sull'infrazione, &#13;
sarebbe   irragionevole   e  incongruente  rispetto  ai  principi  di &#13;
imparzialità e buon andamento dell'amministrazione, e alla finalità &#13;
di  assicurare una adeguata e indipendente difesa al militare, atteso &#13;
che  il  condizionamento  derivante  dal  vincolo  di  subordinazione &#13;
gerarchica  potrebbe  essere  tale,  in alcuni casi, da non garantire &#13;
l'espletamento del mandato difensivo in modo adeguatamente imparziale &#13;
e indipendente da pressioni esterne; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
chiedendo che la questione sia dichiaratainfondata; &#13;
        che,  secondo l'interveniente, le conclusioni enunciate nella &#13;
sentenza  n. 37  del  1992  di  questa  Corte  discenderebbero  dalla &#13;
premessa  di  una  circoscritta  definizione  dell'"ente" cui essa si &#13;
riferisce,  termine  con  il  quale  si  identificherebbe  una unità &#13;
organizzativa  sottoposta  alla  direzione  di  un comandante nei cui &#13;
rispetti  il  militare  prescelto  come  difensore è in ogni caso in &#13;
posizione  di  subordinazione gerarchica, mentre nella prospettazione &#13;
odierna  del giudice a quo ci si riferirebbe genericamente al "corpo" &#13;
militare,  senza specificare il significato del termine, che peraltro &#13;
non  compare  nelle  disposizioni  impugnate, sembrando così volersi &#13;
riferire all'intera struttura di appartenenza del militare incolpato, &#13;
ipotesi  questa  che sarebbe già stata valutata conforme all'art. 97 &#13;
della  Costituzione:  ma  in  tal  modo,  ipotizzando una facoltà di &#13;
scelta  del  difensore  al  di  fuori  di tale struttura, il giudizio &#13;
disciplinare   perderebbe  i  connotati  degli  interna  corporis,  e &#13;
vedrebbe  assicurate  per  il difensore maggiori cautele di quante ne &#13;
siano  previste  per  lo  stesso  giudice  disciplinare; se invece il &#13;
giudice  a quo intendesse lamentare che la scelta del difensore debba &#13;
avvenire all'interno della singola unità organizzativa, la questione &#13;
si  risolverebbe  attraverso  la corretta interpretazione della norma &#13;
impugnata. &#13;
    Considerato  che  le  due  disposizioni impugnate si riferiscono, &#13;
rispettivamente,  ai  sottufficiali  di  tutte le armi (art. 73 della &#13;
legge  n. 599  del  1954)  e  ai vice brigadieri e militari di truppa &#13;
dell'Arma dei carabinieri (art. 41 della legge n. 1168 del 1961); &#13;
        che, trattandosi nella specie di un giudizio disciplinare nei &#13;
confronti di un sottufficiale, e precisamente di un maresciallo capo, &#13;
dell'Arma  dei  carabinieri,  non  risulta  in alcun modo applicabile &#13;
l'art. 41   della  legge  n. 1168  del  1961,  né  alcun  "combinato &#13;
disposto" tra tale norma e l'altra norma impugnata: onde la questione &#13;
va  circoscritta  all'art. 73,  secondo comma, della legge n. 599 del &#13;
1954,  risultando  invece  manifestamente  inammissibile con riguardo &#13;
all'altra disposizione denunciata; &#13;
        che,  quanto  all'art. 73,  secondo comma, della legge n. 599 &#13;
del  1954 - premesso che deve consentirsi sulla sua attuale vigenza e &#13;
sulla   sua  estraneità  all'ambito  della  pronuncia  di  cui  alla &#13;
richiamata sentenza di questa Corte n. 37 del 1992, che si riferiva a &#13;
disposizione   concernente   il  procedimento  per  l'irrogazione  di &#13;
sanzioni   disciplinari   di   corpo,  mentre  la  disposizione  oggi &#13;
denunciata  riguarda  il  procedimento  per l'irrogazione di sanzioni &#13;
disciplinari  di stato - esso si limita a stabilire che "il difensore &#13;
deve  essere  ufficiale  in  servizio,  di  grado  inferiore a quello &#13;
rivestito  dal presidente della commissione di disciplina, e non deve &#13;
trovarsi  in  alcuna  delle  condizioni  di cui all'articolo 70" (che &#13;
stabilisce  a  sua  volta i casi di incompatibilità per i componenti &#13;
della   commissione   di  disciplina,  in  ragione  del  rapporto  di &#13;
dipendenza  gerarchica  dell'incolpato,  o  delle attività svolte in &#13;
rapporto  a  procedimenti attinenti allo stesso fatto, o dei rapporti &#13;
di parentela o affinità fra di loro, o della qualità di offeso o di &#13;
danneggiato  o  di parente o affine di essi o del giudicando, o degli &#13;
speciali   uffici   ricoperti,   o   infine  della  sottoposizione  a &#13;
procedimento penale o disciplinare); &#13;
        che dal tenore e dalla ratio della disposizione impugnata non &#13;
si  ricava  affatto  la limitazione lamentata dal remittente, secondo &#13;
cui il difensore dovrebbe essere scelto fra gli ufficiali in servizio &#13;
appartenenti al medesimo "corpo" cui appartiene l'incolpato, cioè ad &#13;
una  unità organizzativa "sottoposta alla direzione di un comandante &#13;
nei  cui  rispetti  il  militare prescelto come difensore è, in ogni &#13;
caso,  in posizione di subordinazione gerarchica" (sentenza n. 37 del &#13;
1992);  né  tale limitazione è ricavabile da altre disposizioni, in &#13;
particolare  dall'art. 41,  secondo  comma,  della  legge n. 1168 del &#13;
1961, pure denunciato, la cui analoga formulazione ("Il militare può &#13;
farsi  assistere da un ufficiale difensore, da lui scelto o designato &#13;
dal  presidente  della  commissione  di  disciplina, che sia di grado &#13;
inferiore  a quello rivestito dal presidente e non si trovi in alcuna &#13;
delle  condizioni  di  cui  all'art. 40") non dà, a sua volta, alcun &#13;
fondamento alla limitazione in questione; &#13;
        che  pertanto  la  questione,  a  questo  riguardo, si palesa &#13;
manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    a)  Dichiara  la  manifesta  inammissibilità  della questione di &#13;
legittimità costituzionale dell'art. 41 della legge 18 ottobre 1961, &#13;
n. 1168  (Norme  sullo  stato  giuridico  dei  vice  brigadieri e dei &#13;
militari   di   truppa  dell'Arma  dei  carabinieri),  sollevata,  in &#13;
riferimento    all'art. 97    della   Costituzione,   dal   tribunale &#13;
amministrativo regionale della Campania con l'ordinanza in epigrafe; &#13;
    b)   Dichiara   la  manifesta  infondatezza  della  questione  di &#13;
legittimità  costituzionale dell'art. 73 della legge 31 luglio 1954, &#13;
n. 599   (Stato  dei  sottufficiali  dell'esercito,  della  marina  e &#13;
dell'aeronautica),   sollevata,   in  riferimento  all'art. 97  della &#13;
Costituzione,  dallo stesso tribunale amministrativo regionale con la &#13;
medesima ordinanza. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 12 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Onida &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 25 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
