<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>107</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:107</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/04/2000</data_decisione>
    <data_deposito>18/04/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale del combinato disposto    &#13;
 degli  artt. 186,  comma  4,  del decreto legislativo 30 aprile 1992,    &#13;
 n. 285  (Nuovo  codice  della  strada)  e  379,  comma  1, del d.P.R.    &#13;
 16 dicembre  1992,  n. 495 (Regolamento di esecuzione e di attuazione    &#13;
 del  nuovo  codice  della  strada),  promosso con ordinanza emessa il    &#13;
 6 febbraio  1998  dal  pretore  di  Milano  nel procedimento penale a    &#13;
 carico  di  Centorbi  Francesco,  iscritta  al  n. 353  del  registro    &#13;
 ordinanze 1999 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 25, prima serie speciale, dell'anno 1999.                             &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 22 marzo 2000 il giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
     Ritenuto che - nel corso di un procedimento penale a carico di un    &#13;
 automobilista  per guida in stato di ebbrezza - il pretore di Milano,    &#13;
 con ordinanza emessa il 6 febbraio 1998, ha sollevato, in riferimento    &#13;
 all'art. 25,   secondo   comma,   della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale  dell'art. 186,  comma  4,  del  decreto    &#13;
 legislativo  30 aprile  1992,  n. 285  (Nuovo  codice  della strada),    &#13;
 "nella  parte  in  cui,  anziché prevedere l'obbligo di una verifica    &#13;
 tecnico-scientifica  (dello  stato di ebbrezza), ne prevede invece la    &#13;
 mera facoltà";                                                          &#13;
         che,  secondo  il  rimettente,  con  la  norma  impugnata  il    &#13;
 legislatore  avrebbe optato per una nozione "elastica" dello stato di    &#13;
 ebbrezza,  concepito  come  realtà  fisio-psichica  non  ancorata  a    &#13;
 precisi riferimenti quantitativi, ma desumibile da tutta una serie di    &#13;
 indici  di  fatto,  dai  quali  inferire  l'esistenza di uno stato di    &#13;
 consistente  alterazione  di  natura  psichica  caratterizzato  dalla    &#13;
 perdita  di  un'adeguata  capacità  valutativa  concernente il mondo    &#13;
 fenomenico circostante;                                                  &#13;
         che  in  ciò  sarebbe  ravvisabile  una distonia rispetto al    &#13;
 quadro   normativo   approntato   dall'art. 379  del  regolamento  di    &#13;
 esecuzione  del nuovo codice della strada, in cui sembra al contrario    &#13;
 che  il  legislatore  si  sia  determinato  ad accogliere una nozione    &#13;
 oggettiva  dello  stato  di  ebbrezza,  vincolata  ad un preciso dato    &#13;
 quantitativo,    consistente    nel   raggiungimento   di   parametri    &#13;
 predeterminati,  potendosi  considerare  l'interessato  in  stato  di    &#13;
 ebbrezza esclusivamente nel caso di raggiungimento della soglia data,    &#13;
 indipendentemente  dalla  presenza o meno di quei sintomi, di profilo    &#13;
 soggettivo,  ritenuti  idonei  ad  evidenziare  il  suddetto stato di    &#13;
 alterazione qualora non si sia fatto uso dell'etilometro;                &#13;
         che  - rilevato come l'adozione in concreto dell'uno anziché    &#13;
 dell'altro  criterio sia di fatto affidata ad un'insindacabile scelta    &#13;
 del  pubblico  ufficiale  -  il rimettente sospetta la violazione, da    &#13;
 parte   della   denunciata   norma,   del  principio  di  certezza  e    &#13;
 tassatività  della  fattispecie  penale,  attesa  la mancanza di una    &#13;
 sufficiente  nitidezza  dei  contorni  della  nozione  dello stato di    &#13;
 ebbrezza,  la  quale  non  può che essere unica e non deve risentire    &#13;
 delle opzioni dei singoli organi accertatori;                            &#13;
         che  è intervenuto il Presidente del Consiglio dei Ministri,    &#13;
 rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato,    &#13;
 concludendo  per  la  manifesta inammissibilità o infondatezza della    &#13;
 sollevata questione.                                                     &#13;
     Considerato   che  identica  questione,  sollevata  dallo  stesso    &#13;
 rimettente  sulla  base di eguali considerazioni, è stata dichiarata    &#13;
 manifestamente  inammissibile con ordinanza n. 149 del 1998, peraltro    &#13;
 pronunciata   in   data  successiva  alla  proposizione  dell'odierno    &#13;
 incidente di costituzionalità;                                          &#13;
         che,  in  tale sede, questa Corte ha rammentato di avere già    &#13;
 precisato,  nella sentenza n. 194 del 1996 (ignorata dal rimettente),    &#13;
 che  il  voler ancorare il dubbio di costituzionalità esclusivamente    &#13;
 al  modo  dell'accertamento  dello  stato  di ebbrezza costituisca il    &#13;
 frutto  di  una deviazione prospettica insita nel non considerare che    &#13;
 "le  indicazioni  circa  le  circostanze  che,  in  mancanza  di  uso    &#13;
 dell'etilometro, inducono a ritenere la presenza di tale stato, altro    &#13;
 non  sono  che  elementi  destinati  a concorrere alla formazione del    &#13;
 convincimento del giudice";                                              &#13;
         che  il  rimettente  cade nel medesimo errore, scambiando, in    &#13;
 ultima  analisi, l'a'mbito di discrezionalità relativa alle tecniche    &#13;
 di  accertamento  del fatto-reato (riguardante in quanto tale il mero    &#13;
 piano  probatorio)  con  l'asserita  mancanza di oggettiva certezza e    &#13;
 tassatività della condotta sanzionata dalla fattispecie penale;         &#13;
         che,  d'altronde, va ancora ribadito come solo al legislatore    &#13;
 sarebbe  dato  trasformare  in  obbligo  la  facoltà  prevista dalla    &#13;
 denunciata  norma,  al  fine  del preteso recupero di detta oggettiva    &#13;
 certezza,  trattandosi  infatti  di  previsione  attinente alla sfera    &#13;
 delle  prove,  certamente  non  ottenibile  attraverso  una pronuncia    &#13;
 manipolativa di questa Corte;                                            &#13;
         che,  pertanto,  la questione è da dichiarare manifestamente    &#13;
 inammissibile.                                                           &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale  dell'art. 186,  comma  4,  del  decreto    &#13;
 legislativo  30 aprile  1992,  n. 285  (Nuovo  codice  della strada),    &#13;
 sollevata,   in   riferimento   all'art. 25,   secondo  comma,  della    &#13;
 Costituzione, dal pretore di Milano, con l'ordinanza in epigrafe.        &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 aprile 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 18 aprile 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
