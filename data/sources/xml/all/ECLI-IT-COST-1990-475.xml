<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>475</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:475</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/10/1990</data_decisione>
    <data_deposito>22/10/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. &#13;
 Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 438, 439, 440    &#13;
 e 442 del codice di procedura penale, promosso con  ordinanza  emessa    &#13;
 il  29  marzo  1990 dal Pretore di Patti - Sezione distaccata di Naso    &#13;
 nel procedimento penale a carico di Foraci Antonino, iscritta  al  n.    &#13;
 353 del registro ordinanze 1990 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 24, prima serie speciale, dell'anno 1990;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera di consiglio del 26 settembre 1990 il Giudice    &#13;
 relatore Giovanni Conso;                                                 &#13;
    Ritenuto  che  il  Procuratore  della Repubblica presso la Pretura    &#13;
 circondariale di Patti faceva  notificare  nei  confronti  di  Foraci    &#13;
 Antonino   decreto  di  citazione  a  giudizio,  contenente  l'avviso    &#13;
 previsto dall'art.  555,  primo  comma,  lettera  e,  del  codice  di    &#13;
 procedura penale;                                                        &#13;
      che,   non   essendosi   avuta   nei  quindici  giorni  da  tale    &#13;
 notificazione né richiesta di giudizio abbreviato né  richiesta  di    &#13;
 "patteggiamento",  gli  atti venivano trasmessi al Pretore di Patti -    &#13;
 Sezione distaccata  di  Naso,  il  quale  all'udienza  dibattimentale    &#13;
 disponeva il rinvio del processo e la rinnovazione della citazione;      &#13;
      e  che,  alla  successiva  udienza,  l'imputato formulava, prima    &#13;
 dell'apertura del dibattimento, richiesta di giudizio abbreviato, cui    &#13;
 il pubblico ministero si opponeva;                                       &#13;
      che,  con  ordinanza  del  29  marzo 1990, il Pretore di Patti -    &#13;
 Sezione distaccata di Naso, ha sollevato, in riferimento  agli  artt.    &#13;
 3,  24, 102 e 111 della Costituzione, questione di legittimità degli    &#13;
 artt. 438, 439, 440 e 442 del codice di procedura penale, "in  quanto    &#13;
 le  norme  censurate  non  dettano i criteri in base ai quali il p.m.    &#13;
 debba  esprimere  consenso  o  dissenso   all'ammissione   del   rito    &#13;
 abbreviato  e precludono al giudice ogni valutazione sulla fondatezza    &#13;
 di tale rifiuto";                                                        &#13;
      e  che  nel  giudizio è intervenuto il Presidente del Consiglio    &#13;
 dei ministri, rappresentato e difeso dall'Avvocatura  Generale  dello    &#13;
 Stato, riportandosi "integralmente" all'atto di intervento depositato    &#13;
 in relazione ad "identica questione" sollevata dal Pretore  di  Rieti    &#13;
 con ordinanza del 4 gennaio 1990 (R.O.145 del 1990);                     &#13;
    Considerato  che,  non  avendo l'imputato formulato entro quindici    &#13;
 giorni dalla notificazione del  decreto  di  citazione  richiesta  di    &#13;
 giudizio  abbreviato, resterebbe comunque preclusa la possibilità di    &#13;
 presentare la richiesta durante la fase  degli  atti  preliminari  al    &#13;
 dibattimento  (v. artt. 555, primo comma, lettera e, 557 e 560, primo    &#13;
 comma);                                                                  &#13;
      che,  pertanto,  le  norme  denunciate  non  potrebbero ricevere    &#13;
 applicazione  nel  processo  a   quo,   con   conseguente   manifesta    &#13;
 inammissibilità della questione;                                        &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt.  438,  439,  440  e  442  del    &#13;
 codice  di  procedura penale, sollevata, in riferimento agli artt. 3,    &#13;
 24, 102 e 111 della Costituzione, dal  Pretore  di  Patti  -  Sezione    &#13;
 distaccata di Naso, con ordinanza del 29 marzo 1990.                     &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 ottobre 1990.                               &#13;
                    Il Presidente e redattore: CONSO                      &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 22 ottobre 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
