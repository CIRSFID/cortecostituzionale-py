<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>84</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:84</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/03/1996</data_decisione>
    <data_deposito>21/03/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nei  giudizi  di  legittimità  costituzionale  degli artt. 2 e 4 del    &#13;
 decreto-legge del 21 giugno  1995,  n.  238  promossi  con  ordinanze    &#13;
 emesse  il  28 giugno 1995 e il 27 giugno 1995 dal pretore di Verona,    &#13;
 ed il 28 luglio 1995 dal giudice istruttore del Tribunale di  Rovigo,    &#13;
 rispettivamente iscritte ai nn. 541, 592 e 664 del registro ordinanze    &#13;
 1995  e  pubblicate nella Gazzetta Ufficiale della Repubblica nn. 40,    &#13;
 41 e 43, prima serie speciale, dell'anno 1995;                           &#13;
   Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio del 21 febbraio 1996 il Giudice    &#13;
 relatore Renato Granata.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso del procedimento civile tra la Cassa di risparmio di    &#13;
 Verona, Vicenza, Belluno ed Ancona e la società Carlutti costruzioni    &#13;
 s.r.l. per il pagamento della somma di  L.  36.160.453  a  saldo  del    &#13;
 conto  corrente  di  quest'ultima,  l'adito  Pretore  di  Verona  con    &#13;
 ordinanza del 27 giugno 1995 ha sollevato  questione  incidentale  di    &#13;
 legittimità  costituzionale  dell'art. 2 del decreto-legge 21 giugno    &#13;
 1995,  n.  238  (Interventi  urgenti  sul  processo  civile  e  sulla    &#13;
 disciplina transitoria della legge 26 novembre 1990, n. 353, relativa    &#13;
 al  medesimo processo).  Tale disposizione, modificando l'art. 8 cod.    &#13;
 proc. civ., ha stabilito che il pretore è competente per  le  cause,    &#13;
 anche  se  relative  a  beni immobili, di valore non superiore a lire    &#13;
 cinquanta milioni, in quanto non siano di competenza del  giudice  di    &#13;
 pace.                                                                    &#13;
   Il  pretore rimettente dubita innanzi tutto che il decreto-legge n.    &#13;
 238  del  1995  sia  sorretto  dal  requisito   della   straordinaria    &#13;
 necessità  ed  urgenza  di  cui  all'art.  77,  secondo comma, della    &#13;
 Costituzione; requisito questo che richiede situazioni oggettivamente    &#13;
 eccezionali, tali da porsi al di fuori della concreta possibilità di    &#13;
 intervento del legislatore ordinario. Nella  specie  la  disposizione    &#13;
 censurata   non  persegue  affatto  la  finalità  di  colmare  vuoti    &#13;
 normativi di sorta o di  fronteggiare  una  situazione  di  eccessivo    &#13;
 carico  del  tribunale  il  quale  risultava anzi già opportunamente    &#13;
 sgravato delle  controversie  in  materia  di  circolazione  stradale    &#13;
 nonché  delle  controversie  locative,  di  comodato  e  di  affitto    &#13;
 d'azienda.  Invece  la  modifica  -  in  senso  ampliativo  -   della    &#13;
 competenza  per  valore  pretorile, quale prevista dalla disposizione    &#13;
 censurata, si muove nella  direzione  di  un  ulteriore  sgravio  del    &#13;
 carico giudiziale dei tribunali, al di fuori di ogni disegno organico    &#13;
 di  revisione  dei  criteri  di competenza verticali (nella auspicata    &#13;
 prospettiva  del  giudice  unico  di  primo  grado)  e  senza  alcuna    &#13;
 previsione di razionalizzazione delle circoscrizioni                     &#13;
  giudiziarie.  Inoltre  tale aumento di competenza si accompagna alla    &#13;
 restituzione al pretore della competenza  per materia in ordine  alle    &#13;
 cause  di  opposizione alle ingiunzioni di cui alla legge 24 novembre    &#13;
 1981,  n.  689    nonché  a  quelle  di  opposizione  alle  sanzioni    &#13;
 amministrative  irrogate  ex  art.  75  del testo unico approvato con    &#13;
 d.P.R. 9 ottobre 1990, n. 309; ciò infatti consegue dall'art. 1  del    &#13;
 cit.  decreto-legge  n.  238  del 1995 che ha sottratto al giudice di    &#13;
 pace tale competenza, abrogando i commi terzo  e  quarto,  numero  4,    &#13;
 dell'art. 7 cod. proc.  civ.                                             &#13;
   Tutto  ciò  -  secondo  il  giudice  rimettente  -  è destinato a    &#13;
 comportare, tenendo conto comparativamente delle sopravvenienze medie    &#13;
 annuali  presso  le  preture  e  presso  i  tribunali,  la   paralisi    &#13;
 dell'ufficio del pretore, essendo rimasta inalterata l'attuale pianta    &#13;
 organica  e  la  distribuzione  territoriale  dei  pretori,  entrambe    &#13;
 fissate  in  relazione  a  competenze  ampiamente  minori  di  quelle    &#13;
 odierne.  Sarebbe  quindi  violato anche l'art. 97 della Costituzione    &#13;
 sul  buon   andamento   dell'amministrazione   pubblica   che   trova    &#13;
 applicazione anche all'amministrazione della giustizia.                  &#13;
   2.  -  È  intervenuto  il  Presidente  del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura generale dello Stato eccependo    &#13;
 pregiudizialmente l'inammissibilità della  questione  sollevata  per    &#13;
 intervenuta decadenza della disposizione censurata.                      &#13;
   Nel  merito  l'Avvocatura  sostiene  l'infondatezza della questione    &#13;
 rilevando da una parte che non  esiste  una  riserva  in  favore  del    &#13;
 Parlamento  quanto  ai  provvedimenti  che  incidono sulla competenza    &#13;
 giurisdizionale;  d'altra  parte  che  non  sussiste   il   lamentato    &#13;
 sovraccarico   dell'ufficio   del   pretore  rispetto  a  quello  del    &#13;
 tribunale.                                                               &#13;
   3.  -  Analoga  questione  di  costituzionalità  ha  sollevato  il    &#13;
 medesimo  Pretore  di  Verona  con ordinanza del   28 giugno 1995 nel    &#13;
 procedimento civile tra la  Banca  nazionale  del  lavoro  e  Bonuzzi    &#13;
 Giuseppe.                                                                &#13;
   4.  - Anche in questo secondo giudizio è intervenuto il Presidente    &#13;
 del Consiglio dei ministri, rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale  dello Stato sostenendo pregiudizialmente l'inammissibilità    &#13;
 della questione sollevata e nel merito la sua infondatezza.              &#13;
   5. - Nel corso del procedimento civile  tra  Roccato  Giuseppina  e    &#13;
 Anastasia  Salvatore,  avente  ad  oggetto  il risarcimento del danno    &#13;
 subito dall'attrice a seguito di  incidente  stradale  provocato  dal    &#13;
 convenuto,  il  Giudice  istruttore  presso  il  Tribunale di Rovigo,    &#13;
 sciogliendo la riserva in ordine ad una richiesta di provvisionale ex    &#13;
 art. 24 della legge n. 990 del 1969, ha da  una  parte  accolto  tale    &#13;
 richiesta, e dall'altra - avendo le parti in sede di udienza di prima    &#13;
 comparizione domandato la concessione del termine di cui all'art. 180    &#13;
 cod.  proc.    civ.  per  il  deposito  di  memorie  e documenti - ha    &#13;
 sollevato (con ordinanza del 28 luglio 1995) questione incidentale di    &#13;
 legittimità costituzionale dell'art. 4 del decreto-legge  21  giugno    &#13;
 1995, n.238, modificativo dell'art. 180 cit.                             &#13;
   In  particolare  il  giudice  rimettente  ritiene  insussistenti  i    &#13;
 presupposti di urgenza e  necessità  richiesti  dall'art.  77  della    &#13;
 Costituzione per la decretazione d'urgenza. Il considerevole lasso di    &#13;
 tempo  tra l'approvazione della riforma del processo civile (legge 26    &#13;
 novembre 1990, n. 353) e la sua entrata in vigore esclude  -  secondo    &#13;
 il  giudice rimettente - l'impellenza di provvedere ulteriormente per    &#13;
 riformare norme (quale quella censurata) non ancora, quasi del tutto,    &#13;
 applicate e  non  giustifica  quindi  il  ricorso  alla  decretazione    &#13;
 d'urgenza.                                                               &#13;
   Inoltre  l'art.  180  cod.  proc.  civ.  novellato si pone anche in    &#13;
 contrasto con l'art.  97  della  Costituzione  apparendo  al  giudice    &#13;
 rimettente  del tutto contrario al principio di buona amministrazione    &#13;
 giudiziaria prevedere da una parte il frazionamento del processo,  in    &#13;
 luogo  della sua tendenziale concentrazione, e dall'altro rinviare la    &#13;
 prima  udienza  di  trattazione  senza  che  alla  prima  udienza  di    &#13;
 comparizione possa svolgersi alcuna attività istruttoria.               &#13;
   6.  -  È  intervenuto  il  Presidente  del Consiglio dei ministri,    &#13;
 rappresentato  e  difeso   dall'Avvocatura   generale   dello   Stato    &#13;
 concludendo  per  l'inammissibilità o l'infondatezza della questione    &#13;
 di costituzionalità.<diritto>Considerato in diritto</diritto>1. - È  stata  sollevata  questione  incidentale  di  legittimità    &#13;
 costituzionale  -  in  riferimento agli artt. 77, secondo comma, e 97    &#13;
 della Costituzione - dell'art. 2 del decreto-legge 21 giugno 1995, n.    &#13;
 238 (Interventi  urgenti  sul  processo  civile  e  sulla  disciplina    &#13;
 transitoria  della  legge  26  novembre  1990,  n.  353,  relativa al    &#13;
 medesimo processo), nella parte in cui,  modificando  l'art.  8  cod.    &#13;
 proc.  civ., prevede che il pretore è competente per le cause, anche    &#13;
 se  relative  a  beni  immobili,  di  valore  non  superiore  a  lire    &#13;
 cinquantamilioni,  in  quanto  non siano di competenza del giudice di    &#13;
 pace; si sospetta - da parte del giudice rimettente - la violazione:     &#13;
     a) dell'art. 77, secondo comma,  della  Costituzione  perché  il    &#13;
 cit.  decreto-legge  n.  238  del  1995 non è sorretto dal requisito    &#13;
 della straordinaria necessità ed urgenza;                               &#13;
     b) dell'art. 97 della Costituzione perché non è  assicurato  il    &#13;
 buon   andamento   dell'amministrazione  della  giustizia  in  quanto    &#13;
 l'aumento della  competenza  per  valore  rischia  di  comportare  la    &#13;
 paralisi   dell'ufficio   del  pretore,  essendo  rimasta  inalterata    &#13;
 l'attuale  pianta  organica  e  la  distribuzione  territoriale   dei    &#13;
 pretori,  entrambe  fissate in relazione a competenze molto minori di    &#13;
 quelle odierne.                                                          &#13;
   Altra censura di incostituzionalità - in riferimento  ancora  agli    &#13;
 artt.  77,  secondo  comma,  e  97  della Costituzione - ha investito    &#13;
 l'art.  4 del medesimo decreto-legge 21 giugno 1995,  n.  238,  nella    &#13;
 parte  in  cui  prevede  una  prima udienza di comparizione, distinta    &#13;
 dalla successiva prima udienza di trattazione, in cui il giudice  non    &#13;
 può  compiere  alcuna  attività  processuale  che non sia quella di    &#13;
 verificare la correttezza del contraddittorio; si sospetta - da parte    &#13;
 del giudice rimettente - la violazione:                                  &#13;
     a) dell'art. 77, secondo comma,  della  Costituzione  perché  il    &#13;
 cit.  decreto-legge  n.  238  del  1995 non è sorretto dal requisito    &#13;
 della straordinaria necessità ed urgenza;                               &#13;
     b) dell'art. 97 della Costituzione perché non è  assicurato  il    &#13;
 buon  andamento  dell'amministrazione  della  giustizia in quanto, in    &#13;
 contrasto con il principio di economia processuale, non è consentito    &#13;
 al giudice il compimento di ulteriori attività processuali.             &#13;
   2. - Preliminarmente i tre giudizi incidentali  vanno  riuniti  per    &#13;
 connessione oggettiva delle questioni sollevate.                         &#13;
   Inoltre  in  via  pregiudiziale  deve  affermarsi in particolare la    &#13;
 legittimazione del giudice istruttore a  sollevare  la  questione  di    &#13;
 costituzionalità dell'art. 4 cit. trattandosi di una disposizione di    &#13;
 cui egli deve fare diretta applicazione per adottare provvedimenti di    &#13;
 sua  competenza, quale è quello di fissazione della prima udienza di    &#13;
 trattazione  (cfr. ex plurimis ordinanza n. 436 del 1994).               &#13;
   3.   -   Va   poi  esamimata  in  via  ulteriormente  pregiudiziale    &#13;
 l'eccezione sollevata dall'Avvocatura dello Stato secondo  cui  tutte    &#13;
 le  questioni  di  costituzionalità, come sopra formulate, sarebbero    &#13;
 inammissibili  atteso  che  il  decreto-legge   contenente   le   due    &#13;
 disposizioni  censurate  (decreto-legge n. 238 del 1995) non è stato    &#13;
 convertito in legge, ancorché i suoi effetti siano stati fatti salvi    &#13;
 dall'art. 1, secondo comma, della legge 20 dicembre 1995, n.  534  di    &#13;
 conversione in legge, con modificazioni, del decreto-legge 18 ottobre    &#13;
 1995,   n.  432,  che,  reiterando  quello  decaduto,  reca  peraltro    &#13;
 disposizioni  di  contenuto  normativo  identico   a   quello   delle    &#13;
 disposizioni censurate.                                                  &#13;
   In  effetti  non può dubitarsi che tali due disposizioni censurate    &#13;
 abbiano sin dall'inizio perso efficacia  ex  art.  77,  terzo  comma,    &#13;
 della  Costituzione in ragione della mancata conversione in legge del    &#13;
 decreto-legge n. 238 del 1995, che le conteneva; tuttavia il  secondo    &#13;
 comma  dell'art.  1  della  medesima legge n.534 del 1995 ha previsto    &#13;
 (con una tipica clausola di salvezza) che restano validi gli atti  ed    &#13;
 i provvedimenti adottati e sono fatti salvi gli effetti prodottisi ed    &#13;
 i  rapporti  giuridici  sorti  sulla base dei decreti-legge 21 aprile    &#13;
 1995, n.121, 21 giugno 1995, n. 238 e 9  agosto  1995,  n.    347.  E    &#13;
 questa  Corte  ha  già  affermato,  in generale (sentenza n. 243 del    &#13;
 1985), che "traverso la tecnica  della  sanatoria"  "il  terzo  comma    &#13;
 dell'art.  77 della Costituzione abilita il legislatore a dettare una    &#13;
 regolamentazioneretroattiva dei rapporti," senza porre "altri  limiti    &#13;
 se non quelli rappresentati dal rispetto delle altre norme e principi    &#13;
 costituzionali".  Nel  caso  particolare,  poi,  in  cui il contenuto    &#13;
 precettivo (id est la norma) espresso dalla disposizione decaduta sia    &#13;
 riprodotto in uno o più decreti-legge successivi, l'ultimo dei quali    &#13;
 convertito (così come nella specie), tale clausola di salvezza ha la    &#13;
 funzione (ispirata alla esigenza di certezza dei rapporti  giuridici)    &#13;
 di  ripristinare - secondo un'opzione che è rimessa alla valutazione    &#13;
 discrezionale  del  Parlamento  -  un  continuum  normativo   facendo    &#13;
 risalire  nel  tempo la nuova disciplina alla originaria disposizione    &#13;
 decaduta e consolidandola  negli  effetti,  così  da  assicurare  la    &#13;
 permanenza dei medesimi senza soluzione di continuità.                  &#13;
   Da una parte, quindi, resta salvo, nel giudizio pendente innanzi al    &#13;
 Pretore di Verona, l'effetto di radicare la (più elevata) competenza    &#13;
 per   valore   del  pretore  adito  quale  fissata  dall'art.  2  del    &#13;
 decreto-legge n. 238 del 1995, modificativo dell'art.  8  cod.  proc.    &#13;
 civ.;  sicché,  pur essendo l'art. 2 decaduto, il giudice rimettente    &#13;
 deve  tener  conto  del  suo  contenuto  precettivo  in  forza  della    &#13;
 disposizione  di  salvezza,  della quale egli deve fare applicazione.    &#13;
 Analogamente,  d'altra  parte,  nel  giudizio  pendente  innanzi   al    &#13;
 Tribunale  di  Rovigo, la norma espressa dalla disposizione censurata    &#13;
 è stata fatta salva nei suoi  effetti  dalla  medesima  clausola  di    &#13;
 salvezza  dettata  con  il  secondo  comma dell'art. 1 della legge di    &#13;
 conversione.                                                             &#13;
   4. - Si pone quindi la questione di principio se la censura rivolta    &#13;
 nei confronti di una disposizione che esprima una  determinata  norma    &#13;
 possa  riferirsi  alla  medesima  norma  riprodotta  in una diversa e    &#13;
 successiva disposizione, identica nel nucleo precettivo essenziale  o    &#13;
 addirittura,   come  nella  specie,  nella  sua  stessa  formulazione    &#13;
 letterale.                                                               &#13;
   4.1. - La valorizzazione della perdurante identità della norma pur    &#13;
 nel  mutamento  della  disposizione  per    jus  superveniens già si    &#13;
 rinviene nella giurisprudenza della Corte.                               &#13;
   La quale ha, in primo luogo, implicitamente  mostrato  di  ritenere    &#13;
 ipotizzabile  il  trasferimento (nel senso che sarà precisato infra)    &#13;
 da uno ad altro atto  normativo  della  censura  proposta  contro  la    &#13;
 identica  norma  dettata nell'uno e nell'altro, quando nei tempi più    &#13;
 recenti  -  pronunciando  su  questioni  proposte  nei  confronti  di    &#13;
 decreti-legge   non   convertiti   -  ha  abbandonato  la  perentoria    &#13;
 enunciazione  di  principio,   propria   della   sua   giurisprudenza    &#13;
 precedente  (da  ultimo affermata nelle ordinanze nn. 171, 172, 173 e    &#13;
 174 del 1995) ed ha valorizzato significative precisazioni di specie.    &#13;
 Così, nell'adottare  decisioni  di  manifesta  inammissibilità,  ha    &#13;
 espressamente  messo  in  evidenza  la  circostanza che la disciplina    &#13;
 dettata dal decreto-legge censurato risultava modificata ad opera  di    &#13;
 un  successivo decreto-legge reiterato (ordinanze nn. 179, 176, 175 e    &#13;
 165 del 1995) ovvero, nell'adottare decisioni di  restituzione  atti,    &#13;
 ha sottolineato, a seconda dei casi, che era mutata la disciplina nel    &#13;
 successivo decreto-legge convertito (ordinanze nn. 535, 403 e 272 del    &#13;
 1995)  o nel decreto-legge reiterato al momento vigente ma non ancora    &#13;
 convertito (ordinanze  nn.  518,  279  e  243  del  1995),  con  ciò    &#13;
 lasciando  intendere - come la dottrina ha avvertito - che appunto la    &#13;
 diversità del contenuto precettivo concorreva a costituire la  ratio    &#13;
 decidendi delle pronunce così adottate.                                 &#13;
   Ma  soprattutto è da ricordare che in non poche occasioni la Corte    &#13;
 ha, nel concreto, "trasferito" di fatto la sua valutazione da uno  ad    &#13;
 altro  atto  normativo in ordine alla stessa norma già contenuta nel    &#13;
 primo e riprodotta nel secondo.                                          &#13;
   A parte  le  pronunce  nelle  quali  -  riguardo  ad  ordinanze  di    &#13;
 rimessione  che,  pur  lasciando chiaramente intendere quale norma si    &#13;
 volesse   censurare,   tuttavia   individuavano    erroneamente    la    &#13;
 disposizione   denunciata  -  si  è  ritenuto  di  dover  affrontare    &#13;
 egualmente  la  questione  con  riferimento  all'atto  effettivamente    &#13;
 contenente  quella norma (cfr. ex plurimis sentenza n. 188 del 1994),    &#13;
 casi  nei  quali  è  dato  ravvisare,   sottesa   all'aspetto   più    &#13;
 appariscente  di una mera operazione interpretativa dell'ordinanza di    &#13;
 rimessione, anche la implicita valorizzazione della norma  come  vero    &#13;
 oggetto  del  giudizio,  non di rado, invero, la Corte ha in concreto    &#13;
 tenuto  conto  della  disciplina  normativa,  già  espressa  in  una    &#13;
 disposizione  non  più  in vigore, sull'esplicito rilievo che quella    &#13;
 disciplina continuava ad  essere  operante  nell'ordinamento  perché    &#13;
 riprodotta in altro atto normativo.                                      &#13;
   Così  nel caso di abrogazione della disposizione impugnata in sede    &#13;
 di giudizio incidentale, il cui contenuto  precettivo era stato però    &#13;
 riformulato in altra disposizione sostitutiva della prima,  la  Corte    &#13;
 ha  ritenuto  di dover   procedere all'esame di merito della censura.    &#13;
 In particolare la Corte nella sentenza n. 482 del 1995, nel rilevare     &#13;
  che la disposizione censurata era stata sostituita da altra, osserva    &#13;
 che "permane, tuttavia, la stessa  disciplina  sostanziale  ...";  in    &#13;
 senso  conforme  è  anche  l'ulteriore  pronuncia di poco precedente    &#13;
 (sentenza n.  446  del  1995)  con  cui  la  Corte  ha  ritenuto  che    &#13;
 l'abrogazione  della  disposizione  censurata e la riformulazione del    &#13;
 contenuto  precettivo  in  altra   disposizione   non   comporta   la    &#13;
 restituzione  degli  atti  al  giudice  rimettente,  ma la censura va    &#13;
 esaminata  nel  merito.  Analogamente  in  altro  giudizio  la  Corte    &#13;
 (sentenza  n.  482  del  1991)  -  nel  rilevare  che la disposizione    &#13;
 impugnata  è  stata  abrogata,  ma  che  nel  contempo  il  medesimo    &#13;
 contenuto  precettivo  è stato riformulato in altra disposizione che    &#13;
 sostituisce la disposizione censurata - afferma che ciò consente  di    &#13;
 esaminare  le  censure  nel  merito,  pur se formalmente proposte nei    &#13;
 confronti della disposizione sostituita.                                 &#13;
   Così  di  fronte  alla  intervenuta  decadenza  del  decreto-legge    &#13;
 invocato  dal  giudice  a  quo come tertium comparationis, la Corte -    &#13;
 ancora - ha ritenuto di dover apprezzare il contenuto  precettivo  di    &#13;
 quella disposizione quando la norma interposta sia "indiscutibilmente    &#13;
 presente   nell'ordinamento"  in  quanto  riprodotta  nel  successivo    &#13;
 decreto-legge di reiterazione  del  precedente  (sentenza  n.429  del    &#13;
 1993).   Particolarmente  significativa,  rispetto  alla  specie,  si    &#13;
 appalesa poi  la  circostanza  che  proprio  nella  ipotesi  in  cui,    &#13;
 successivamente   alla   decadenza   del  decreto-legge  per  mancata    &#13;
 conversione, intervenga una disposizione  che  ne  faccia  salvi  gli    &#13;
 effetti,  egualmente la Corte ha ritenuto che il richiamo - contenuto    &#13;
 in un  ricorso  per  conflitto  di  attribuzione  -  alla  disciplina    &#13;
 prevista  dalla  prima disposizione vigente al momento della adozione    &#13;
 dell'atto oggetto del conflitto conserva efficacia proprio  in  forza    &#13;
 della  clausola  di  salvezza  stabilita  dalla  seconda disposizione    &#13;
 (sentenza n. 40 del 1994).                                               &#13;
   4.2.1. - La Corte ritiene che la linea di tendenza emergente  dalle    &#13;
 citate pronunce dimostri come non sia essenziale alla attività della    &#13;
 Corte, per rendere operante la sua funzione di garanzia, la identità    &#13;
 formale  tra  la  disposizione  denunciata  e  quella  successiva con    &#13;
 riferimento alla  quale  viene  resa  la  pronuncia  di  merito,  ove    &#13;
 invariata rimanga la norma dall'una e dall'altra espressa.               &#13;
   In  generale la disposizione - della cui esatta identificazione, al    &#13;
 momento  dell'ordinanza  di  rimessione,  è   onerato   il   giudice    &#13;
 rimettente  (sentenza  n.176  del 1972), non potendo egli limitarsi a    &#13;
 denunciare un principio (sentenza n.188 del 1995)  -  costituisce  il    &#13;
 necessario  veicolo  di  accesso della norma al giudizio della Corte,    &#13;
 che si  svolge  sulla  norma  quale  oggetto  del  raffronto  con  il    &#13;
 contenuto  precettivo del parametro costituzionale, e rappresenta poi    &#13;
 parimenti  il  tramite  di  ritrasferimento  nell'ordinamento   della    &#13;
 valutazione  così  operata, a seguito di tale raffronto, dalla Corte    &#13;
 medesima,  la  quale  quindi  giudica  su  norme,  ma  pronuncia   su    &#13;
 disposizioni.  Si  disvela così, in tal caso, la funzione servente e    &#13;
 strumentale della disposizione rispetto alla  norma,  sicché  è  la    &#13;
 immutata  persistenza  di quest'ultima nell'ordinamento ad assicurare    &#13;
 la perdurante ammissibilità del giudizio di costituzionalità  sotto    &#13;
 il   profilo   dell'inalterata   sussistenza  del  suo  oggetto  (che    &#13;
 costituisce  altresì,  sotto  questo  aspetto,  ragione  della   sua    &#13;
 persistente   rilevanza),   mentre  l'eventuale  successione  di  una    &#13;
 disposizione  ad  altra  rileva  soltanto  al   fine   di   riversare    &#13;
 correttamente    l'esito    del    sindacato   di   costituzionalità    &#13;
 nell'ordinamento.    Sicché  solo  in  senso  figurato  può   dirsi    &#13;
 "trasferita"  la questione di costituzionalità, che viceversa rimane    &#13;
 ancorata  all'oggetto   identificato   nell'atto   introduttivo   del    &#13;
 giudizio.                                                                &#13;
   4.2.2.   -   Il  giudizio  di  costituzionalità  -  così  essendo    &#13;
 strutturato il suo oggetto - risulta in  tal  modo  assistito  da  un    &#13;
 favor   per  la  sua  effettività  e  tempestività,  che  viceversa    &#13;
 sarebbero in una qualche misura frustrate quando invece si  ritenesse    &#13;
 necessaria  -  di  fronte  a  vicende  come  quella  in  esame  -  la    &#13;
 riproposizione delle medesime censure nei  confronti  della  medesima    &#13;
 norma  provvedendosi  a  denunziare  ex novo la sopravvenuta, diversa    &#13;
 disposizione  che  la  contiene,   inalterata   nella   sua   portata    &#13;
 precettiva;  favor  non  dissimile  da  quello  che  ha  ispirato  la    &#13;
 giurisprudenza che ritiene possibile  il  trasferimento  del  quesito    &#13;
 referendario dalla disposizione abrogata, oggetto del quesito stesso,    &#13;
 a  quella  successivamente  introdotta  di  contenuto sostanzialmente    &#13;
 analogo (sentenza n.68 del  1978).  Il  principio  dell'economia  dei    &#13;
 giudizi  nel  processo  costituzionale  - che tra l'altro costituisce    &#13;
 anche la ragione  giustificatrice  della  ammissibilità  di  censure    &#13;
 plurime,  una  subordinata  all'altra  (sentenza  n.  188 del 1995) -    &#13;
 sottende il valore fondamentale della effettività e  della  pienezza    &#13;
 del  controllo  demandato  alla  Corte, controllo tanto più efficace    &#13;
 quanto più tempestivo.                                                  &#13;
   4.2.3. - In conclusione deve affermarsi in linea di  principio  che    &#13;
 la  norma  contenuta  in  un  atto  avente  forza di legge vigente al    &#13;
 momento in cui l'esistenza nell'ordinamento  della  norma  stessa  è    &#13;
 rilevante  ai  fini di una utile investitura della Corte, ma non più    &#13;
 in vigore nel momento in cui essa rende la sua pronunzia, continua ad    &#13;
 essere oggetto dello scrutinio alla  Corte  stessa  demandato  quando    &#13;
 quella   medesima  norma  permanga  tuttora  nell'ordinamento  -  con    &#13;
 riferimento allo stesso spazio temporale rilevante per il giudizio  -    &#13;
 perché  riprodotta,  nella sua espressione testuale o comunque nella    &#13;
 sua  identità   precettiva   essenziale,   da   altra   disposizione    &#13;
 successiva, alla quale dunque dovrà riferirsi la pronunzia.             &#13;
   4.3.  -  Nella  specie  si  ha che la norma, introdotta dalla prima    &#13;
 disposizione  censurata  e  costituita  dalla  nuova   regola   della    &#13;
 competenza  per  valore  del  pretore,  individuata  al momento della    &#13;
 proposizione del giudizio,  permane  inalterata  nell'ordinamento  (e    &#13;
 deve  essere  applicata  dal giudice a quo), perché il secondo comma    &#13;
 dell'art.  1 della legge n. 534 del 1995 fa salvi gli  effetti  della    &#13;
 disposizione   decaduta,  quali  prodottisi  nel  periodo  della  sua    &#13;
 precaria vigenza.                                                        &#13;
   Analogamente e per la stessa ragione permane  la  norma  introdotta    &#13;
 dalla  seconda  disposizione  censurata e costituita dal precetto che    &#13;
 impone al giudice istruttore in sede e all'esito della prima  udienza    &#13;
 di   comparizione   di  fissare  la  prima  udienza  di  trattazione,    &#13;
 concedendo termine al convenuto per proporre le eccezioni processuali    &#13;
 e di merito che non siano rilevabili  d'ufficio  ed  astenendosi  dal    &#13;
 compiere atti istruttori.                                                &#13;
   Il  fatto  che entrambe le disposizioni siano state reiterate prima    &#13;
 nel decreto-legge n. 347 del 1995 e poi nel decreto-legge n. 432  del    &#13;
 1995,  convertito in legge n. 534 del 1995, non muta la direzione del    &#13;
 trasferimento della questione di costituzionalità, direzione che  è    &#13;
 univocamente  orientata  dalle disposizioni originariamente censurate    &#13;
 in quanto queste sono state utilizzate per  dare  corpo  e  contenuto    &#13;
 alle disposizioni di salvezza recate dalla legge di conversione; sono    &#13;
 quindi queste ultime che conservano nell'ordinamento (con riferimento    &#13;
 ratione  temporis  ai  rapporti sorti nella vigenza del decreto-legge    &#13;
 decaduto) quelle stesse norme espresse dalle  disposizioni  censurate    &#13;
 attraverso  una  tecnica  sotto  il  profilo effettuale non del tutto    &#13;
 dissimile da quella della conversione in legge, per la  quale  questa    &#13;
 Corte   ha   già   affermato   il   trasferimento   della  questione    &#13;
 originariamente  sollevata  in  riferimento  alle  disposizioni   del    &#13;
 decreto-legge (sentenze nn. 1033 e 742 del 1988; n. 70 del 1987).        &#13;
   Le  questioni  di  costituzionalità  proposte  nei confronti degli    &#13;
 artt. 2 e 4 del decreto-legge n. 238 del 1995 vanno quindi  esaminate    &#13;
 nel  merito  in  riferimento  alla  disposizione dell'art. 1, secondo    &#13;
 comma, della legge n. 534 del 1995 che ne fa salvi gli effetti.          &#13;
   5. - Comune alle  due  questioni  è  l'invocazione  del  parametro    &#13;
 costituito  dal  terzo comma dell'art. 77 della Costituzione sotto il    &#13;
 profilo che entrambe le norme (quella sulla  competenza  pretorile  e    &#13;
 quella  sulla prima udienza di comparizione) sarebbero state adottate    &#13;
 pur mancando il requisito della straordinaria urgenza e necessità.      &#13;
   Sotto tale profilo entrambe le questioni sono infondate, perché il    &#13;
 parametro non è pertinente. Il requisito della necessità ed urgenza    &#13;
 va apprezzato con riferimento al singolo decreto-legge, e la  censura    &#13;
 relativa - a differenza dal caso della conversione (sentenze nn. 29 e    &#13;
 165   del   1995),   di  cui  la  sanatoria  non  costituisce  idoneo    &#13;
 equipollente (ordinanza n. 1119 del 1988) - non  è  riferibile  alla    &#13;
 disposizione di sanatoria, che si limita a fare salvi gli effetti del    &#13;
 decreto-legge  stesso.  Ed  infatti il presupposto di quest'ultima è    &#13;
 costituito  unicamente  dalla  circostanza  di  fatto  della  mancata    &#13;
 conversione  di  un  decreto-legge,  formalmente  identificabile come    &#13;
 tale, e non anche dalla sussistenza del requisito della necessità  e    &#13;
 dell'urgenza,  il  quale,  pur  essendo presupposto del decreto-legge    &#13;
 stesso, non rileva più dopo la sua decadenza, atteso che in tal caso    &#13;
 la forza di legge che  assiste  la  norma  che  disciplina,  ora  per    &#13;
 allora,   i  rapporti  altrimenti  regolati  dalla  disposizione  del    &#13;
 decreto-legge, deriva unicamente dalla legge di sanatoria. Né rileva    &#13;
 il fatto che quest'ultima sia contenuta in una legge  di  conversione    &#13;
 di altro decreto-legge, anziché in un'altra legge, atteso che, da un    &#13;
 lato,  il terzo comma dell'art. 77 della Costituzione non pone alcuna    &#13;
 riserva in proposito, e che,  dall'altro,  l'eventuale  sindacato  di    &#13;
 costituzionalità  per  mancanza  del  requisito  della  necessità e    &#13;
 dell'urgenza, che questa Corte (sentenza n. 161  del  1995  cit.)  ha    &#13;
 limitato  alla  ipotesi  della "evidente mancanza" di tale requisito,    &#13;
 riguarda unicamente quella  parte  della  legge  di  conversione  che    &#13;
 ratifica l'esercizio del potere legislativo ad opera del Governo.        &#13;
   6  -    Parimenti  infondate sono le questioni di costituzionalità    &#13;
 delle norme censurate con riferimento all'art. 97 della Costituzione.    &#13;
   Da  una  parte,  anche  sotto  questo  profilo,  viene  evocato  un    &#13;
 parametro  non  pertinente  alla disciplina della competenza (propria    &#13;
 del pretore).  Infatti questa Corte (ex plurimis, cfr.  ordinanza  n.    &#13;
 257  del  1995)  ha  più  volte affermato che il "principio del buon    &#13;
 andamento della pubblica amministrazione, pur potendo riferirsi anche    &#13;
 agli   organi   dell'amministrazione   della    giustizia,    attiene    &#13;
 esclusivamente  alle  leggi  concernenti  l'ordinamento  degli uffici    &#13;
 giudiziari ed il loro funzionamento sotto  l'aspetto  amministrativo,    &#13;
 mentre  è  del  tutto  estraneo  alla  materia  dell'esercizio della    &#13;
 funzione giurisdizionale nel suo complesso (...) e quindi ai  criteri    &#13;
 di ripartizione delle competenze tra organi giudiziari".                 &#13;
   Né,  per  altro  verso,  il  medesimo parametro è pertinente alla    &#13;
 disciplina  della  prima  udienza   di   comparizione   che   attiene    &#13;
 interamente alla regolamentazione del processo; non senza considerare    &#13;
 che  la  distinzione  tra la prima udienza di comparizione e la prima    &#13;
 udienza  di trattazione è funzionale alla disciplina delle eccezioni    &#13;
 processuali e di merito che il convenuto può proporre (dopo l'una  e    &#13;
 prima  dell'altra)  nel  termine  perentorio  assegnato  dal giudice,    &#13;
 termine non inferiore  a  venti  giorni  prima  di  tale  udienza  di    &#13;
 trattazione.                                                             &#13;
   Comunque,  nell'uno  e  nell'altro caso si è nell'area di un'ampia    &#13;
 discrezionalità del legislatore non irragionevolmente esercitata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  non   fondate   le   questioni   di    &#13;
 legittimità  costituzionale  dell'art. 1, secondo comma, della legge    &#13;
 20 dicembre 1995, n. 534 (Conversione in  legge,  con  modificazioni,    &#13;
 del decreto-legge 18 ottobre 1995, n. 432, recante interventi urgenti    &#13;
 sul  processo  civile  e  sulla disciplina transitoria della legge 26    &#13;
 novembre 1990, n. 353, relativa al medesimo processo), nella parte in    &#13;
 cui prevede che restano validi gli atti ed i provvedimenti adottati e    &#13;
 sono fatti salvi gli effetti prodottisi ed i rapporti giuridici sorti    &#13;
 sulla base degli artt. 2 e 4 del decreto-legge  21  giugno  1995,  n.    &#13;
 238,  sollevate,  in  riferimento  agli artt. 77, secondo comma, e 97    &#13;
 della Costituzione, rispettivamente, dal  Pretore  di  Verona  e  dal    &#13;
 Giudice  istruttore  presso  il  Tribunale di Rovigo con le ordinanze    &#13;
 indicate in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 marzo 1996.                                &#13;
                         Il Presidente: Ferri                             &#13;
                         Il redattore: Granata                            &#13;
                       Il cancelliere:  Di Paola                          &#13;
   Depositata in cancelleria il 21 marzo 1996.                            &#13;
               Il direttore della cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
