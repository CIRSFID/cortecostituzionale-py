<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>426</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:426</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Giuliano Vassalli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1996</data_decisione>
    <data_deposito>27/12/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 25  del  codice    &#13;
 di  procedura  penale,  promosso  con ordinanza emessa il 19 febbraio    &#13;
 1996 dal pretore di Pistoia, sezione distaccata di  Monsummano  Terme    &#13;
 nel  procedimento  penale a carico di Baldini Roberto, iscritta al n.    &#13;
 388 del registro ordinanze 1996 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 19, prima serie speciale, dell'anno 1996;            &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di consiglio dell'11 dicembre 1996 il giudice    &#13;
 relatore Giuliano Vassalli;                                              &#13;
   Ritenuto  che  il  pretore  di  Pistoia,  Sezione   distaccata   di    &#13;
 Monsummano  Terme  premette, in fatto, che la difesa dell'imputato ha    &#13;
 tempestivamente eccepito l'incompetenza per territorio, rilevando che    &#13;
 nei confronti del  medesimo  imputato  è  stato  emesso  decreto  di    &#13;
 citazione  a  giudizio davanti al pretore di Pisa per il reato di cui    &#13;
 all'art. 367 cod.   pen. commesso in Pisa e  per  il  connesso  reato    &#13;
 previsto  dall'art.    1 della legge 15 dicembre 1990, n. 386, reato,    &#13;
 quest'ultimo, in relazione al quale la competenza spetta allo  stesso    &#13;
 giudice  rimettente,  in  conformità  a quanto deciso dalla Corte di    &#13;
 cassazione con la sentenza che ha risolto il conflitto tra il pretore    &#13;
 di Livorno ed il medesimo giudice a quo;                                 &#13;
     che la difesa dell'imputato - si precisa ancora  nella  ordinanza    &#13;
 di  rimessione  - ha sottolineato come nella specie lo stesso giudice    &#13;
 dovrebbe  dichiararsi  territorialmente  incompetente  a  norma   del    &#13;
 combinato  disposto  degli artt. 21, secondo comma, e 12, lettera b),    &#13;
 cod. proc.  pen., in quanto la competenza va radicata in capo  ad  un    &#13;
 terzo   giudice,   identificato   nel   pretore  di  Pisa,  giacché,    &#13;
 procedendosi separatamente nei confronti dello  stesso  imputato  per    &#13;
 reati  connessi,  competente  per  tutti  i  procedimenti è, a norma    &#13;
 dell'art. 16 cod. proc. pen., il giudice competente per il reato più    &#13;
 grave, vale a dire il pretore di Pisa  davanti  al  quale  l'imputato    &#13;
 deve  essere giudicato per il reato previsto dall'art. 367 cod. pen.,    &#13;
 relativo proprio ad uno  degli  assegni  per  i  quali  la  Corte  di    &#13;
 cassazione,  risolvendo  il  conflitto, ha statuito la competenza del    &#13;
 medesimo rimettente;                                                     &#13;
     che   il   giudice   a  quo,  pur  convenendo  sul  merito  delle    &#13;
 prospettazioni avanzate dalla difesa in  punto  di  incompetenza,  ma    &#13;
 reputando  al  tempo  stesso  di  non  poter  procedere alla relativa    &#13;
 declaratoria "attesa la natura vincolante della decisione della Corte    &#13;
 di cassazione sulla competenza  per  territorio  di  questo  Pretore,    &#13;
 giusta  la surrichiamata sentenza", ha sollevato, in riferimento agli    &#13;
 artt. 3, 24, 25, primo comma, e 101 della Costituzione, questione  di    &#13;
 legittimità costituzionale dell'art. 25 cod. proc. pen., nella parte    &#13;
 in  cui  non prevede che tra i nuovi fatti che comportino una diversa    &#13;
 definizione  giuridica  rilevino,  oltre  quelli  da  cui  derivi  la    &#13;
 modificazione  della  giurisdizione  o  la  competenza  di un giudice    &#13;
 superiore,  anche  "quelli  da  cui  derivi  la  modificazione  della    &#13;
 competenza  territoriale  per  ragioni  di  connessione  tra  giudici    &#13;
 egualmente competenti per materia";                                      &#13;
     che nel giudizio è intervenuto il Presidente del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata non fondata;            &#13;
   Considerato che la Corte di cassazione, allorché viene chiamata  a    &#13;
 risolvere  un  conflitto  di competenza, provvede a determinare quale    &#13;
 debba essere l'organo competente fra i giudici che, a norma dell'art.    &#13;
 28  cod.  proc.  pen.,  contemporaneamente  prendono  o  ricusano  di    &#13;
 prendere   cognizione  del  medesimo  fatto  attribuito  alla  stessa    &#13;
 persona, sicché, presupposto del conflitto e al tempo stesso  limite    &#13;
 della  decisione  è  l'esistenza  di  un  contrasto  fra determinati    &#13;
 giudici avente ad  oggetto  una  specifica  questione  di  competenza    &#13;
 riguardante  l'identico  fatto attribuito al medesimo imputato, senza    &#13;
 possibilità alcuna, quindi, di annettere  alla  pronuncia  solutoria    &#13;
 del   conflitto  una  portata  espansiva  tale  da  generare  effetti    &#13;
 preclusivi al di là del peculiare e circoscritto tema che, sul piano    &#13;
 soggettivo e oggettivo, è stato devoluto alla Corte regolatrice;        &#13;
     che nella specie, risolto dalla Corte di cassazione un  conflitto    &#13;
 negativo   di   competenza   territoriale,   si   profila   e   viene    &#13;
 tempestivamente dedotta la competenza per  connessione  di  un  terzo    &#13;
 giudice,  un  organo,  dunque,  diverso  da  quelli  fra  i quali era    &#13;
 intervenuto il contrasto e  competente  in  ragione  di  un  criterio    &#13;
 diverso  da quello esaminato dalla Corte medesima, cosicché non v'è    &#13;
 ragione alcuna che impedisca al giudice rimettente  di  declinare  la    &#13;
 propria   competenza  in  favore  dell'organo  che  esercita  la  vis    &#13;
 attractiva, proprio perché vincolato dalla pronuncia della Corte  di    &#13;
 cassazione  soltanto  nei  confronti del primo giudice confliggente e    &#13;
 nei limiti del fatto e del tema che ha formato oggetto del contrasto;    &#13;
     che, pertanto, risultando erronea la premessa  interpretativa  da    &#13;
 cui  ha tratto origine la questione, la stessa deve essere dichiarata    &#13;
 manifestamente infondata;                                                &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  25  del  codice  di    procedura   penale,    &#13;
 sollevata,  in  riferimento  agli artt. 3, 24, 25, primo comma, e 101    &#13;
 della Costituzione, dal pretore   di Pistoia, sezione  distaccata  di    &#13;
 Monsummano Terme con l'ordinanza in epigrafe.                            &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 dicembre 1996.                             &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Vassalli                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 27 dicembre 1996.                         &#13;
                       Il cancelliere: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
