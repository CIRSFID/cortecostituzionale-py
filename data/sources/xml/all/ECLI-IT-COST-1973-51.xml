<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>51</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:51</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/04/1973</data_decisione>
    <data_deposito>30/04/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 13, ultimo  &#13;
 comma, della legge 24 marzo 1958, n. 195, sul Consiglio superiore della  &#13;
 magistratura, e, dell'art. 154 del d.P.R. 16 settembre  1958,  n.  916,  &#13;
 promosso con ordinanza emessa il 22 ottobre 1971 dal Consiglio di Stato  &#13;
 in  sede  giurisdizionale  -  sezione  IV  -  sul ricorso di Augugliaro  &#13;
 Giuseppe contro il Ministero di grazia e giustizia, iscritta al n.  176  &#13;
 del registro ordinanze 1972 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n.  158 del 21 giugno 1972.                                   &#13;
     Visti  gli atti di costituzione del Ministero di grazia e giustizia  &#13;
 e di Augugliaro Giuseppe;                                                &#13;
     udito nell'udienza pubblica del 21 marzo 1973 il  Giudice  relatore  &#13;
 Paolo Rossi;                                                             &#13;
     uditi  l'avv.  Leopoldo  Piccardi,  per  l'Augugliaro,  ed  il vice  &#13;
 avvocato generale dello Stato Francesco  Agrò,  per  il  Ministero  di  &#13;
 grazia e giustizia.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di  un  giudizio  avente  ad oggetto la legittimità di  &#13;
 taluni decreti del Presidente della Repubblica datati 24  giugno  1967,  &#13;
 emessi  in  conformità  della  delibera  del Consiglio superiore della  &#13;
 magistratura del  9  marzo  1967,  con  la  quale  era  stata  respinta  &#13;
 l'istanza  di  revisione  dello  scrutinio  a  magistrato di Cassazione  &#13;
 avanzata dal  dr.  Augugliaro,  il  Consiglio  di  Stato  ha  sollevato  &#13;
 questione  incidentale  di  legittimità costituzionale degli artt. 13,  &#13;
 ultima parte, della legge 24 maggio 1958, n. 195, e 54  del  d.P.R.  16  &#13;
 settembre  1958,  n.  916,  in  riferimento  agli  artt.  3 e 97. della  &#13;
 Costituzione.                                                            &#13;
     Il  giudice  a  quo,  osserva,  in  ordine  alla  rilevanza   della  &#13;
 questione,   che   dalla  incostituzionalità  della  norma  denunciata  &#13;
 conseguirebbe, per. derivazione, il vizio dell'atto impugnato,  emanato  &#13;
 prima  che  la  legge 18 dicembre 1967, n. 1198, abrogasse la norma ora  &#13;
 sospettata  di  illegittimità costituzionale. Nel merito si rileva che  &#13;
 gli  scrutinati  potevano  ricorrere  al  Consiglio   superiore   della  &#13;
 magistratura avverso la delibera della Commissione di scrutinio al fine  &#13;
 di  ottenerne  la revisione, ma la necessaria partecipazione all'organo  &#13;
 di riesame del Primo Presidente della Corte di  cassazione,  presidente  &#13;
 di  diritto  della  Commissione  di  primo  grado,  secondo  il sistema  &#13;
 legislativo  allora  vigente,  poteva  implicare  una  violazione   del  &#13;
 principio costituzionale d'uguaglianza e del principio di imparzialità  &#13;
 della pubblica Amministrazione.                                          &#13;
     L'incompatibilità  con  tali  principi  dovrebbe  dedursi, secondo  &#13;
 l'ordinanza   di   remissione,   dalle   disposizioni   normative   che  &#13;
 espressamente  escludono  la  partecipazione  alle  delibere di riesame  &#13;
 degli scrutini,  degli  altri  membri  del  Consiglio  superiore  della  &#13;
 magistratura  che  abbiano  fatto parte della Commissione di scrutinio,  &#13;
 nonché della disparità di trattamento tra "lo scrutinando, che  abbia  &#13;
 riportato tra i voti favorevoli quello del Primo Presidente e che possa  &#13;
 quindi  contare  su  una  coerente  nuova favorevole valutazione, ed il  &#13;
 collega che detto voto non abbia riportato, e che debba  quindi  temere  &#13;
 una coerente nuova valutazione sfavorevole".                             &#13;
     Si è costituito in questa sede il Ministero di grazia e giustizia,  &#13;
 rappresentato  e  difeso dall'Avvocatura generale dello Stato, con atto  &#13;
 di  deduzioni  depositato  il  7  luglio  1972,  chiedendo  dichiararsi  &#13;
 l'infondatezza della questione sollevata.                                &#13;
     Osserva   l'Avvocatura  dello  Stato  che  nel  nostro  ordinamento  &#13;
 giuridico esistono vari casi di giudici  o  di  organi  amministrativi,  &#13;
 chiamati  a  riesaminare i propri pronunciati (revocazione, opposizione  &#13;
 ecc.); comunque, in via del tutto non concessa, a voler considerare, in  &#13;
 materia di ricorsi amministrativi, come  principio  generale,  che  non  &#13;
 possa  far  parte  del  collegio  di riesame chi abbia partecipato alla  &#13;
 deliberazione del provvedimento impugnato, non per  questo  ogni  norma  &#13;
 che  ad  esso derogasse, come nella specie, dovrebbe essere considerata  &#13;
 in contrasto con gli artt. 3 e 97 della Costituzione.                    &#13;
     Invero la violazione del principio  costituzionale  di  uguaglianza  &#13;
 non  può  ricorrere  quando  la diversità di disciplina, adottata dal  &#13;
 legislatore nell'esercizio della sua discrezionalità politica, non sia  &#13;
 contraria  ai  criteri  della  ragionevolezza,  ma  risponda   ad   una  &#13;
 diversità oggettiva di situazioni.                                      &#13;
     Nella   specie   la  ragionevole  giustificazione  del  trattamento  &#13;
 differenziato nei confronti del solo Primo Presidente  della  Corte  di  &#13;
 cassazione,  dovrebbe desumersi dalla posizione eminente del medesimo e  &#13;
 dalla circostanza che egli è  un  semplice  componente  del  Consiglio  &#13;
 superiore  della magistratura, che è formato da 24 membri. Tale ultimo  &#13;
 rilievo, insieme alla constatazione che egli non è parte né portatore  &#13;
 di interessi propri, dovrebbe far risultare  inconferente  il  richiamo  &#13;
 all'art. 97 della Costituzione.                                          &#13;
     Si   è   costituito  in  questa  sede  anche  il  dr.  Augugliaro,  &#13;
 rappresentato  e  difeso  dall'avv.  Leopoldo  Piccardi,  con  atto  di  &#13;
 deduzioni   depositato   il  7  novembre  1972,  il  quale  ha  chiesto  &#13;
 l'accoglimento della questione sollevata.                                &#13;
     La parte privata si  riporta  sostanzialmente  alle  argomentazioni  &#13;
 svolte  nella  ordinanza  di remissione, osservando che l'art. 97 della  &#13;
 Costituzione opera anche nei confronti del  Consiglio  superiore  della  &#13;
 magistratura,  in  considerazione della natura dell'attività da questo  &#13;
 svolta,   e  non  ostandovi  la  particolare  rilevanza  costituzionale  &#13;
 dell'organo stesso.                                                      &#13;
     Nelle memorie depositate e nella discussione orale le  parti  hanno  &#13;
 ulteriormente sviluppato le rispettive argomentazioni, insistendo nelle  &#13;
 conclusioni prese.<diritto>Considerato in diritto</diritto>:                          &#13;
     Secondo  il  sistema normativo vigente prima dell'entrata in vigore  &#13;
 dell'art. 6 della legge 18 dicembre 1967, n.  1198, il Primo Presidente  &#13;
 della Corte di cassazione, presidente della Commissione di scrutinio  a  &#13;
 magistrato   di  Cassazione,  poteva  partecipare  alle  decisioni  del  &#13;
 Consiglio superiore della magistratura da adottarsi in sede di  reclamo  &#13;
 avverso  le delibere della Commissione stessa, a differenza degli altri  &#13;
 membri elettivi del Consiglio superiore, nei cui confronti operava come  &#13;
 causa  d'incompatibilità  l'aver  già  fatto  parte  della   suddetta  &#13;
 Commissione  (artt. 13, u.c., legge 24 marzo 1958, n.  195, e 54 d.P.R.  &#13;
 16 settembre 1958, n. 916).                                              &#13;
     La Corte costituzionale deve quindi decidere se la doppia veste che  &#13;
 veniva in tal modo ad assumere il Primo  Presidente  della  Cassazione,  &#13;
 membro  di  diritto del Consiglio superiore della magistratura ai sensi  &#13;
 dell'art. 104 della Costituzione, contrastasse o meno con gli artt.  97  &#13;
 e 3 della Carta.                                                         &#13;
     Sotto  il  primo profilo è stata prospettata l'eventualità che la  &#13;
 composizione  dell'organo  procedente  al  riesame  potesse   risultare  &#13;
 contrastante   con   il   principio  di  imparzialità  della  pubblica  &#13;
 Amministrazione, per l'intervento necessario di colui che  avendo  già  &#13;
 presieduto la Commissione di primo grado, non avrebbe potuto non essere  &#13;
 influenzato dal voto già espresso.                                      &#13;
     Sotto  il secondo profilo è stata posta in luce una ingiustificata  &#13;
 disparità di trattamento tra i diversi membri del Consiglio  superiore  &#13;
 della  magistratura,  essendo inibito a quelli elettivi, e non al Primo  &#13;
 Presidente della Cassazione, come agli  altri  membri  di  diritto,  di  &#13;
 partecipare alla delibera di revisione degli scrutini, qualora avessero  &#13;
 fatto  parte  della  relativa  commissione.  Altra  disparità è stata  &#13;
 ravvisata tra i diversi scrutinati e  in  particolare  tra  colui  "che  &#13;
 abbia riportato tra i voti favorevoli quello del Primo Presidente e che  &#13;
 possa  quindi  contare su una coerente nuova favorevole valutazione, ed  &#13;
 il collega che detto voto non abbia riportato e che debba quindi temere  &#13;
 una coerente nuova valutazione sfavorevole".                             &#13;
     La  questione  è  fondata  con  riferimento   all'art.   3   della  &#13;
 Costituzione, da cui discende, per regola generale, la parità di tutti  &#13;
 i  componenti  gli  organi  collegiali,  titolari  di  uguali diritti e  &#13;
 doveri.                                                                  &#13;
     La  partecipazione  del  Primo  Presidente  della  cassazione  alla  &#13;
 delibera  consiliare  di revisione dello scrutinio non implicherebbe di  &#13;
 per se sola, automatica violazione del principio di imparzialità della  &#13;
 pubblica Amministrazione, attesa l'amplissima composizione  dell'organo  &#13;
 di secondo grado e l'assenza di un interesse di parte nella persona del  &#13;
 Primo Presidente.                                                        &#13;
     È  invece  da  rilevare che la qualità del Primo Presidente della  &#13;
 Corte di cassazione, considerato dalla Costituzione come titolo per  la  &#13;
 sua   partecipazione   "di   diritto"   al  Consiglio  superiore  della  &#13;
 magistratura, non  può  autorizzare  una  discriminazione  tra  membri  &#13;
 elettivi  del  Consiglio  superiore  della  magistratura,  e  membri di  &#13;
 diritto,  consentendo  a  questi ultimi, in deroga ai principi generali  &#13;
 vigenti in materia, l'ulteriore potere di far valere una seconda  volta  &#13;
 la  propria  opinione,  quando  il  Consiglio  riesamini  gli  scrutini  &#13;
 disposti dall'apposita commissione.                                      &#13;
     Rimane  così  assorbita   ogni   ulteriore   censura   prospettata  &#13;
 nell'ordinanza di remissione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   l'illegittimità  costituzionale  dell'art.  54,  ultimo  &#13;
 comma, del d.P.R. 16 settembre 1958, n. 916, nella parte in cui esclude  &#13;
 i membri di diritto del  Consiglio  superiore  della  magistratura  dal  &#13;
 divieto  di  partecipazione  alle deliberazioni del Consiglio, previste  &#13;
 nei commi primo e secondo dello stesso articolo, sui ricorsi e  reclami  &#13;
 avverso gli atti e le deliberazioni delle Commissioni.                   &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 16 aprile 1973.                               &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - LUIGI OGGIONI - ANGELO DE  MARCO  -  &#13;
                                   ERCOLE  ROCCHETTI  - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
