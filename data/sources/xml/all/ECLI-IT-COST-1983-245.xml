<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>245</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:245</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/07/1983</data_decisione>
    <data_deposito>25/07/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di  legittimità costituzionale dell'art. 13  &#13;
 u.c., della legge della Regione Lombardia  20  agosto  1976,  n.  28  e  &#13;
 dell'art. 42, n. 1, della legge della Regione Lombardia 31 luglio 1978,  &#13;
 n. 47 (Competenza in ordine alla opposizione all'ingiunzione che irroga  &#13;
 una  sanzione  amministrativa)  promossi  con le ordinanze emesse il 12  &#13;
 febbraio 1980 dal Tribunale di Sondrio e il 17 marzo, il 20 marzo e  12  &#13;
 maggio  (due  ordinanze)  1981  dal Pretore di Legnano, rispettivamente  &#13;
 iscritte al n. 331 del registro ordinanze 1980 ed ai nn. 350, 351,  507  &#13;
 e 508 del registro ordinanze 1981 e pubblicate nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 180 del 1980 e nn. 276 e 304 del 1981.               &#13;
     Visti  gli atti di intervento del Presidente della Giunta regionale  &#13;
 della Lombardia;                                                         &#13;
     udito nella camera di consiglio  del  22  giugno  1983  il  Giudice  &#13;
 relatore Alberto Malagugini.                                             &#13;
     Ritenuto  che con le ordinanze indicate in epigrafe il Tribunale di  &#13;
 Sondrio ed il Pretore di Legnano dubitano  -  entrambi  in  riferimento  &#13;
 all'art.  117 Cost. ed il secondo anche agli artt. 108, primo comma e 5  &#13;
 Cost. - della legittimità costituzionale dell'art.  13, ultimo  comma,  &#13;
 della  legge  della Regione Lombardia 20 agosto 1976, n. 28 (recante la  &#13;
 "disciplina delle  sanzioni  amministrative  pecuniarie  di  competenza  &#13;
 regionale"),  in  quanto  prevede che l'opposizione all'ingiunzione che  &#13;
 irroga una sanzione amministrativa vada proposta ai sensi  dell'art.  3  &#13;
 r.d.  14  aprile  1910,  n.  639,  e cioè "avanti il conciliatore o il  &#13;
 pretore o il tribunale del luogo in cui ha  sede  l'ufficio  emittente,  &#13;
 secondo  la  rispettiva  competenza,  a  norma  del codice di procedura  &#13;
 civile";                                                                 &#13;
     che il Tribunale di Sondrio dubita altresì - sempre a motivo della  &#13;
 carenza  di   competenza   legislativa   delle   regioni   in   materia  &#13;
 giurisdizionale  (art.  117  Cost.) - della legittimità costituzionale  &#13;
 dell'art. 42, n. 1 della legge della regione Lombardia 31 luglio  1978,  &#13;
 n.  47  (recante  norme  per  la protezione della fauna e la disciplina  &#13;
 dell'esercizio venatorio) in quanto richiama, in tema di procedura  per  &#13;
 l'irrogazione  delle  sanzioni  amministrative  previste  dalla  stessa  &#13;
 legge, le disposizioni di cui alla predetta  l.  reg.  n.    28/1976  e  &#13;
 quindi, tra esse, il citato art. 13, ultimo comma;                       &#13;
     che  infine  il  Pretore  di  Legnano  dubita  anche  con le stesse  &#13;
 ordinanze, in riferimento agli artt.  108,  primo  comma  e  117  Cost.  &#13;
 della  legittimità  costituzionale del medesimo art. 13, ultimo comma,  &#13;
 l. reg. n. 28/1976, in quanto richiama (tra l'altro) l'art. 2,  secondo  &#13;
 comma, del citato r.d. n. 639/1910 e con ciò prevede che l'ingiunzione  &#13;
 emessa dal sindaco debba essere vidimata e resa esecutoria dal pretore.  &#13;
     Considerato   che   peraltro,   dopo  l'emanazione  delle  predette  &#13;
 ordinanze, è entrata in vigore la  legge  24  novembre  1981,  n.  689  &#13;
 (recante  "Modifiche  al sistema penale") la quale ha dettato, nel capo  &#13;
 I,  una  nuova  disciplina   generale   dell'illecito   amministrativo,  &#13;
 stabilendo  in  particolare:  a)  all'art.  12, che "le disposizioni di  &#13;
 questo capo si osservano, in quanto applicabili e  salvo  che  non  sia  &#13;
 diversamente  stabilito,  per  tutte  le  violazioni  per  le  quali è  &#13;
 prevista la sanzione amministrativa  del  pagamento  di  una  somma  di  &#13;
 denaro";   b)   all'art.  31  cpv.,  sotto  la  rubrica  "provvedimenti  &#13;
 dell'autorità      regionale"      che      "l'opposizione      contro  &#13;
 l'ordinanza-ingiunzione  è  regolata dagli artt. 22 e 23" il primo dei  &#13;
 quali prevede in proposito che l'opposizione è  proposta  "davanti  al  &#13;
 pretore  del luogo in cui è stata commessa la violazione"; c) all'art.  &#13;
 42, che è abrogata - oltre a talune leggi  specificamente  indicate  -  &#13;
 "ogni  altra  disposizione  incompatibile  con  la  presente legge"; d)  &#13;
 all'art.  18,  ult.  comma,  che  "l'ordinanza-ingiunzione  costituisce  &#13;
 titolo  esecutivo"  senza  che  occorra  il  visto di esecutorietà del  &#13;
 pretore prescritto dall'art. 2 cpv. R.D. n. 639/1910;                    &#13;
     che pertanto, dovendosi valutare l'applicabilità  nei  giudizi  in  &#13;
 corso  delle  norme  processuali  sopravvenute,  si rende necessaria la  &#13;
 restituzione degli  atti  ai  giudici  a  quibus,  affinché  procedano  &#13;
 (anche)  al  conseguente  riesame  della  rilevanza  delle questioni di  &#13;
 legittimità costituzionale sollevate.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione  degli  atti al Tribunale di Sondrio ed al  &#13;
 Pretore di Legnano.                                                      &#13;
    Così deciso in Roma, in camera di consiglio, nella sede della Corte  &#13;
 costituzionale, Palazzo della Consulta, il 15 luglio 1983.               &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
