<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>176</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:176</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Angelo de Marco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/1976</data_decisione>
    <data_deposito>14/07/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale dell'art. 32 del  &#13;
 regio  decreto  5  giugno 1939, n. 1016 (testo unico delle norme per la  &#13;
 protezione della selvaggina e per l'esercizio della caccia), modificato  &#13;
 dall'art. 10 della legge 2 agosto 1967, n. 799, promossi con  ordinanze  &#13;
 emesse  il  18  aprile 1975 dal pretore di Orvieto e il 10 ottobre 1975  &#13;
 dal tribunale di Macerata nei  procedimenti  penali  rispettivamente  a  &#13;
 carico  di Rughetti Arnaldo ed altro e di Giorgi Nello, iscritte ai nn.  &#13;
 210 e 593 del registro  ordinanze  1975  e  pubblicate  nella  Gazzetta  &#13;
 Ufficiale  della  Repubblica  n.  181 del 9 luglio 1975 e n. 38 dell'11  &#13;
 febbraio 1976.                                                           &#13;
     Udito nella camera di consiglio  del  15  giugno  1976  il  Giudice  &#13;
 relatore Angelo De Marco.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  - Con ordinanza in data 18 aprile 1975, emessa nel procedimento  &#13;
 penale a carico di due imputati del reato di cui all'art. 32 del  regio  &#13;
 decreto 5 giugno 1939, n. 1016, nel testo modificato dall'art. 10 della  &#13;
 legge  2  agosto 1967, n. 799 (penultimo ed ultimo capoverso) per avere  &#13;
 trasportato,  senza  giustificato  motivo,  un  fucile  con   munizione  &#13;
 spezzata  in  zona di ripopolamento, il pretore di Orvieto ha sollevato  &#13;
 d'ufficio  questione  di  legittimità   costituzionale   delle   norme  &#13;
 suindicate, in riferimento all'art. 3 della Costituzione.                &#13;
     Al  riguardo si rileva che: la norma denunziata ha per obiettività  &#13;
 giuridica  evidente   la   disciplina   dell'esercizio   dell'attività  &#13;
 venatoria; che, ciò posto, appare assolutamente ingiustificata e priva  &#13;
 di  fondamento giuridico e razionale la pena della multa da lire 20.000  &#13;
 a lire 100.000, con la revoca della licenza di caccia da uno a tre anni  &#13;
 preveduta per questo reato formale, mentre lo stesso testo  legislativo  &#13;
 per   l'esercizio   concreto   dell'attività   venatoria  in  zona  di  &#13;
 ripopolamento e cattura prevede la meno grave sanzione dell'ammenda  da  &#13;
 lire 6.000 a lire 60.000.                                                &#13;
     2.  -  Identica questione ha sollevato il tribunale di Macerata con  &#13;
 ordinanza 10 ottobre 1975, che, rilevando come ai sensi  dell'art.  43,  &#13;
 comma settimo, della legge n. 799 del 1967 per l'esercizio della caccia  &#13;
 in  zona di ripopolamento prevede la pena dell'ammenda da lire 12.000 a  &#13;
 lire   120.000,   ravvisa   l'irrazionalità   del   trattamento  nella  &#13;
 considerazione che viene punita  come  delitto  una  ipotesi  di  reato  &#13;
 evidentemente  meno  grave  di  altra  che  viene  punita, invece, come  &#13;
 semplice contravvenzione.                                                &#13;
     3.  -  Dopo  gli  adempimenti  di  legge,   non   essendovi   state  &#13;
 costituzioni  di  parti, entrambi i giudizi come sopra promossi vengono  &#13;
 alla cognizione della Corte, convocata in camera di consiglio ai  sensi  &#13;
 dell'art. 26, secondo comma, della legge 11 marzo 1953, n. 87.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  I due giudizi, come sopra promossi, vanno riuniti per essere  &#13;
 decisi con unica sentenza, avendo per oggetto la stessa questione.       &#13;
     2. - L'impugnato art.  32  del  testo  unico  delle  norme  per  la  &#13;
 protezione  della  selvaggina  e per l'esercizio della caccia approvato  &#13;
 con regio decreto 5 giugno 1939,  n.  1016,  quale  risulta  modificato  &#13;
 dall'articolo  10  della  legge  2  agosto 1967, n.   799, contiene due  &#13;
 ordini di divieti penalmente sanzionati: a) quelli preveduti nei  primi  &#13;
 quattro  commi, che, come chiaramente risulta dal richiamo all'art. 703  &#13;
 c.p. ed all'art. 57 della legge di pubblica sicurezza, pur  riferendosi  &#13;
 all'esercizio   della  caccia,  hanno  per  obiettività  giuridica  la  &#13;
 prevenzione dei delitti contro la vita e l'incolumità individuale;  b)  &#13;
 quelli  preveduti  nel  penultimo  comma,  che  hanno  per obiettività  &#13;
 giuridica la protezione della selvaggina.                                &#13;
     L'inosservanza di tali divieti, oggettivamente  considerata,  anche  &#13;
 in  riferimento  alle  norme  del c.p. e della legge di p.s.  di cui al  &#13;
 citato richiamo, ha indubbio carattere contravvenzionale.                &#13;
     Senonché nell'ultimo comma - nonostante vi si adoperi  il  termine  &#13;
 che dovrebbe essere rivelatore di "contravventori" - viene preveduta la  &#13;
 sanzione  della  multa,  che  presupponendo  un delitto, tale carattere  &#13;
 conferisce all'inosservanza di quei divieti.                             &#13;
     L'art. 43 del t.u. n.  1016, quale risulta modificato dall'art.  10  &#13;
 della  legge  n.  799  del  1967,  poi,  punisce con la sola ammenda e,  &#13;
 quindi, considera semplice contravvenzione, l'esercizio  abusivo  della  &#13;
 caccia  nelle  riserve,  nelle  bandite  ed  in  genere  nelle  zone di  &#13;
 ripopolamento,  predisposte,  appunto,  a   quella   protezione   della  &#13;
 selvaggina,  che  come  sopra  si  è detto, costituisce l'obiettività  &#13;
 giuridica dei divieti di cui al penultimo comma dell'impugnato art. 32.  &#13;
     A ciò si aggiunga che l'art. 1 della legge 24  dicembre  1975,  n.  &#13;
 706,  sulla  disciplina  del  sistema  sanzionatorio  delle  norme  che  &#13;
 prevedono  contravvenzioni  punibili  con  l'ammenda  dispone:     "Non  &#13;
 costituiscono  reato  e  sono soggette alla sanzione amministrativa del  &#13;
 pagamento di una somma di denaro tutte le violazioni per  le  quali  è  &#13;
 prevista  la pena dell'ammenda, salvo quanto previsto negli articoli 10  &#13;
 e  14"  e  che  questi  due   articoli   non   prevedono   l'esclusione  &#13;
 dell'applicazione  di  quanto disposto dall'art. 1 alle contravvenzioni  &#13;
 punibili con la sola ammenda, prevedute dalle  leggi  sulla  protezione  &#13;
 della selvaggina e sull'esercizio della caccia.                          &#13;
     3.  -  Da  quanto  precede  risulta in modo evidente che manca ogni  &#13;
 elemento logico che possa spiegare il fondamento giuridico e  razionale  &#13;
 di  una normativa come quella sopra esaminata, che prevede la punizione  &#13;
 come delitto dell'inosservanza di un divieto diretto  alla  prevenzione  &#13;
 di  semplici    contravvenzioni, punibili solo con l'ammenda e che, per  &#13;
 giunta, in seguito alla nuova legge n. 706 del 1975, non  costituiscono  &#13;
 più reato e sono perseguibili soltanto con sanzione amministrativa.     &#13;
     Ne  consegue  che  anche  nel caso in esame, come in quello oggetto  &#13;
 della sentenza di questa Corte n. 218 del 1974, deve  riconoscersi  che  &#13;
 il  legislatore,  nell'esercizio della discrezionalità che gli compete  &#13;
 nello statuire  quali  comportamenti  debbano  essere  puniti  e  quali  &#13;
 debbano  essere  la  qualità  e la misura della pena da infliggere, ha  &#13;
 ecceduto  dai  limiti  della  razionalità,  ponendo  in   essere   una  &#13;
 disparità di trattamento che viola il principio di eguaglianza sancito  &#13;
 dall'art. 3 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   l'illegittimità  costituzionale  dell'art.  32,  ultimo  &#13;
 comma, del regio decreto 5 giugno 1939, n. 1016, che approva il  "Testo  &#13;
 unico  delle norme per la protezione della selvaggina e per l'esercizio  &#13;
 della caccia", come modificato dall'art. 10 della legge 2 agosto  1967,  &#13;
 n.  799,  nella parte in cui, limitatamente alle zone di ripopolamento,  &#13;
 punisce il porto " delle armi da caccia con  munizione  spezzata  e  di  &#13;
 arnesi  per  l'uccellaggione,    a  meno  che  il trasporto avvenga per  &#13;
 giustificato motivo e che il fucile sia smontato o chiuso  in  busta  o  &#13;
 altro involucro idoneo", con la multa da lire 20.000 a lire 100.000.     &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 luglio 1976.         &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO - LEOPOLDO ELIA.            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
