<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>108</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:108</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/03/2001</data_decisione>
    <data_deposito>10/04/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA,Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'art. 34, comma 2, del &#13;
codice   di   procedura  penale,  promossi,  nell'ambito  di  diversi &#13;
procedimenti  penali,  dal tribunale di Verona, sezione distaccata di &#13;
Soave, con ordinanze emesse il 21 e il 14 gennaio, il 18 febbraio, il &#13;
24  e  il  3 marzo  e  il 16 giugno 2000, iscritte rispettivamente ai &#13;
nn. 186,  187,  188,  376,  377  e  565 del registro ordinanze 2000 e &#13;
pubblicate nella Gazzetta Ufficiale della Repubblica nn. 18, 27 e 42, &#13;
1ª serie speciale, dell'anno 2000. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio  del  7 marzo 2001 il giudice &#13;
relatore Guido Neppi Modona. &#13;
    Ritenuto  che con sei ordinanze (r.o. nn. 186, 187, 188, 376, 377 &#13;
e  565 del 2000) di contenuto analogo il tribunale di Verona, sezione &#13;
distaccata  di  Soave,  ha sollevato, in riferimento agli artt. 3, 24 &#13;
(parametro  evocato nelle sole ordinanze iscritte ai numeri 376 e 377 &#13;
del  r.o.  del  2000),  25  e  97  della  Costituzione,  questione di &#13;
legittimità  costituzionale  dell'art. 34,  comma  2,  del codice di &#13;
procedura  penale,  come  integrato  dalla  sentenza n. 186 del 1992, &#13;
nella  parte  in  cui  prevede  l'incompatibilità  alla  funzione di &#13;
giudizio del giudice che negli atti preliminari al dibattimento abbia &#13;
respinto  la  richiesta  di  applicazione  della  pena  concordata ex &#13;
art. 444 cod. proc. pen; &#13;
        che  in  tutti  i  procedimenti  a  quibus  il  rimettente ha &#13;
rigettato  per diversi motivi la richiesta di applicazione della pena &#13;
formulata  dall'imputato,  con  il  consenso  del pubblico ministero, &#13;
prima del compimento delle formalità di apertura del dibattimento, e &#13;
ritiene  quindi  di  trovarsi,  per effetto della sentenza n. 186 del &#13;
1992,  in una situazione di incompatibilità a giudicare gli imputati &#13;
nel  merito dell'imputazione loro contestata, con conseguente obbligo &#13;
di  astensione  ai sensi dell'art. 36, comma 1, lettera g) cod. proc. &#13;
pen; &#13;
        che ad avviso del giudice a quo la previsione di tale ipotesi &#13;
di  incompatibilità  si  pone  in  contrasto  con  la  più  recente &#13;
giurisprudenza  costituzionale  e,  in  particolare,  con l'ordinanza &#13;
n. 232  del  1999,  che - nel dichiarare manifestamente infondata una &#13;
questione  di legittimità costituzionale dell'art. 34, comma 2, cod. &#13;
proc.  pen.,  nella  parte  in  cui non prevede l'incompatibilità ad &#13;
emettere  sentenza  del giudice che abbia rigettato, nella fase degli &#13;
atti  preliminari al dibattimento, istanza di oblazione - ha ribadito &#13;
il  principio  generale  secondo cui "l'imparzialità del giudice non &#13;
può  ritenersi intaccata da una valutazione anche di merito compiuta &#13;
all'interno della medesima fase del procedimento"; &#13;
        che,  a giudizio del rimettente, tali affermazioni sono state &#13;
ribadite  dalla  successiva  ordinanza  n. 443  del  1999,  avente ad &#13;
oggetto  una  questione di legittimità costituzionale concernente la &#13;
mancata  previsione  dell'incompatibilità  del  giudice  che  si sia &#13;
pronunciato,  negli  atti  preliminari  al  dibattimento,  su  misure &#13;
cautelari personali nei confronti dell'imputato; &#13;
        che  in  tale  occasione  la  Corte ebbe a riaffermare che la &#13;
incompatibilità  conseguente al compimento di atti tipici della fase &#13;
unitaria  di  cui il giudice è investito "finirebbe con l'attribuire &#13;
alle parti la potestà di determinare l'incompatibilità nel corso di &#13;
un  giudizio  nel  quale  il  giudice è investito, sicché lo stesso &#13;
giudice verrebbe spogliato di tale giudizio in ragione del compimento &#13;
di  un  atto  processuale cui è tenuto a seguito dell'istanza di una &#13;
parte;  esito, questo, non solo irragionevole, ma in contrasto con il &#13;
principio  del  giudice  naturale  precostituito per legge, dal quale &#13;
l'imputato verrebbe o potrebbe chiedere di essere distolto"; &#13;
        che,  sulla  base  delle medesime argomentazioni, e ritenendo &#13;
che  le  modifiche  recate  dal decreto legislativo 19 febbraio 1998, &#13;
n. 51  (Norme  in  materia  di istituzione del giudice unico di primo &#13;
grado),  non  abbiano  apportato  elementi  di  novità rispetto alle &#13;
valutazioni  già  espresse da questa Corte, il giudice a quo solleva &#13;
questione  di legittimità costituzionale dell'art. 34, comma 2, cod. &#13;
proc.   pen.,   come   integrato  dalla  sentenza  n. 186  del  1992, &#13;
assumendone il contrasto con gli artt. 3, 24, 25 e 97 Cost; &#13;
        che,   ad   avviso   del   rimettente,  la  previsione  della &#13;
incompatibilità  al  giudizio  del giudice che abbia rigettato negli &#13;
atti  preliminari  al  dibattimento  la richiesta di pena patteggiata &#13;
determina  una  irragionevole  disparità  di  trattamento rispetto a &#13;
situazioni  analoghe  in  cui  la causa di incompatibilità non opera &#13;
(come  quella  - oggetto dell'ordinanza n. 232 del 1999 - del giudice &#13;
che negli atti preliminari al dibattimento abbia rigettato istanza di &#13;
oblazione),  e  nello  stesso tempo irragionevolmente assoggetta alla &#13;
medesima    disciplina   situazioni   processuali   non   comparabili &#13;
processualmente,  "prevedendo  l'incompatibilità al giudizio sia del &#13;
giudice  che  abbia  legittimamente  espresso  valutazioni  di merito &#13;
nell'ambito  della  medesima fase processuale, sia del giudice che le &#13;
abbia espresse nell'ambito di fase processuale diversa"; &#13;
        che  la disciplina censurata violerebbe inoltre i principi di &#13;
buona   amministrazione   (art. 97  Cost.)  e  del  giudice  naturale &#13;
precostituito   per  legge,  realizzando  per  un  verso  "un'assurda &#13;
frammentazione  del  procedimento"  e  per  l'altro consentendo "alle &#13;
parti,  mediante  studiata proposizione di istanze ex art. 444 c.p.p. &#13;
inaccoglibili,   di   "sbarazzarsi"   del   loro   giudice  naturale, &#13;
costringendolo all'astensione"; &#13;
        che l'art. 24 della Costituzione sarebbe violato in quanto il &#13;
diritto di difesa della parte civile viene leso dall'allungamento dei &#13;
tempi  necessari  alla definizione del procedimento, "su cui vanno ad &#13;
incidere  quelli  della procedura di astensione ed individuazione del &#13;
nuovo giudice"; &#13;
        che  nei  giudizi  promossi  con  le  ordinanze  iscritte  ai &#13;
nn. 376, 377 e 565 del r.o. del 2000 è intervenuto il Presidente del &#13;
Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura &#13;
generale  dello  Stato,  chiedendo  che  la  questione sia dichiarata &#13;
inammissibile o comunque infondata; &#13;
        che,  a  giudizio dell'Avvocatura, con l'ordinanza n. 232 del &#13;
1999  la Corte si sarebbe limitata "a dare atto della "maturazione" e &#13;
del   consolidamento   di   un  indirizzo  che  vuole  "sterilizzato" &#13;
l'istituto  delle  incompatibilità all'interno della stessa fase del &#13;
giudizio,   salva,  eventualmente,  l'applicabilità  dei  meccanismi &#13;
dell'astensione e della ricusazione"; &#13;
        che  il  criterio affermato consente di valutare le questioni &#13;
di  legittimità costituzionale che venissero sollevate nei confronti &#13;
dell'art. 34,  comma  2, cod. proc. pen., ma non potrebbe "consentire &#13;
di  revocare in dubbio la permanente validità degli effetti prodotti &#13;
da  altre  e  precedenti  pronunce  della Corte" che debbono rimanere &#13;
ferme  a  prescindere  dai  "mutati itinerari argomentativi [...], in &#13;
certa  misura  "fisiologici  nel  cammino  di  consolidamento  di  un &#13;
indirizzo giurisprudenziale". &#13;
    Considerato  che, stante la sostanziale identità delle questioni &#13;
di  legittimità  costituzionale  sollevate  dalle  sei  ordinanze di &#13;
rimessione  emesse  in distinti procedimenti dal tribunale di Verona, &#13;
deve essere disposta la riunione dei relativi giudizi; &#13;
        che  il  giudice  rimettente,  pur  sollevando formalmente la &#13;
questione   nei  confronti  dell'art. 34,  comma  2,  del  codice  di &#13;
procedura  penale  nella  parte  in  cui, a seguito dell'integrazione &#13;
disposta  dalla  sentenza n. 186 del 1992, prevede l'incompatibilità &#13;
alla  funzione  di giudizio del giudice che negli atti preliminari al &#13;
dibattimento  abbia  respinto la richiesta di applicazione della pena &#13;
concordata  a norma dell'art. 444 cod. proc. pen., in realtà censura &#13;
una precedente decisione di accoglimento della Corte; &#13;
        che   tale  sindacato  non  è  ammissibile  ai  sensi  degli &#13;
artt. 136,   primo   comma,   e  137,  terzo  comma,  Cost.,  nonché &#13;
dell'art. 30, terzo comma, della legge 11 marzo 1953, n. 87, i quali, &#13;
nello  stabilire  che  contro le decisioni della Corte costituzionale &#13;
non  è  consentita  alcuna impugnazione, precludono in modo assoluto &#13;
qualsiasi   tipo  di  domanda  diretta  a  contrastare,  annullare  o &#13;
riformare  tali  decisioni  (v.  ordinanze  n. 461 del 1999, n. 7 del &#13;
1991,  nn. 203,  93  e 27 del 1990, nonché sentenza n. 29 del 1998 e &#13;
ordinanza n. 220 del 1998); &#13;
        che    pertanto   la   questione   deve   essere   dichiarata &#13;
manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi; &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  dell'art. 34,  comma  2,  del codice di &#13;
procedura penale, sollevata, in riferimento agli artt. 3, 24, 25 e 97 &#13;
della  Costituzione,  dal  tribunale di Verona, sezione distaccata di &#13;
Soave, con le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 22 marzo 2001. &#13;
                       Il Presidente: Ruperto &#13;
                     Il redattore: Neppi Modona &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 10 aprile 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
