<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2014</anno_pronuncia>
    <numero_pronuncia>206</numero_pronuncia>
    <ecli>ECLI:IT:COST:2014:206</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASSESE</presidente>
    <relatore_pronuncia>Marta Cartabia</relatore_pronuncia>
    <redattore_pronuncia>Marta Cartabia</redattore_pronuncia>
    <data_decisione>09/07/2014</data_decisione>
    <data_deposito>16/07/2014</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Sabino CASSESE; Giudici : Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Giancarlo CORAGGIO, Giuliano AMATO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 4-bis e 4-vicies ter del decreto-legge 30 dicembre 2005, n. 272 (Misure urgenti per garantire la sicurezza ed i finanziamenti per le prossime Olimpiadi invernali, nonché la funzionalità dell'Amministrazione dell'interno. Disposizioni per favorire il recupero di tossicodipendenti recidivi e modifiche al testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza, di cui al d.P.R. 9 ottobre 1990, n. 309), convertito, con modificazioni, dall'art. 1, comma 1, della legge 21 febbraio 2006, n. 49, promosso dal Tribunale ordinario di Milano, undicesima sezione penale, nel procedimento a carico di T.M. con ordinanza dell'11 dicembre 2013, iscritta al n. 43 del registro ordinanze 2014 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 15, prima serie speciale, dell'anno 2014.&#13;
 Udito nella camera di consiglio del 23 giugno 2014 il Giudice relatore Marta Cartabia.&#13;
 Ritenuto che, con ordinanza dell'11 dicembre 2013 (r.o. n. 43 del 2014), il Tribunale ordinario di Milano, undicesima sezione penale, ha dubitato della legittimità costituzionale degli artt. 4-bis e 4-vicies ter del decreto-legge 30 dicembre 2005, n. 272 (Misure urgenti per garantire la sicurezza ed i finanziamenti per le prossime Olimpiadi invernali, nonché la funzionalità dell'Amministrazione dell'interno. Disposizioni per favorire il recupero di tossicodipendenti recidivi e modifiche al testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza, di cui al d.P.R. 9 ottobre 1990, n. 309), convertito, con modificazioni, dall'art. 1, comma 1, della legge 21 febbraio 2006, n. 49, in riferimento all'art. 77, secondo comma, della Costituzione;&#13;
 che, più precisamente, il Tribunale, riportandosi alle motivazioni contenute in precedente ordinanza di rimessione della Corte di cassazione, sezione terza penale (ordinanza n. 1426 del 2013), ha ritenuto che il citato art. 4-bis - con il quale è stato riformato il trattamento sanzionatorio di cui all'art. 73 del decreto del Presidente della Repubblica 9 ottobre 1990, n. 309 (Testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza), senza attribuire rilievo alla tipologia delle sostanze stupefacenti - avrebbe un contenuto totalmente disomogeneo rispetto a quello dell'originario decreto-legge, così da violare l'art. 77, secondo comma, Cost.;&#13;
 che, ad avviso del rimettente, analoga violazione dovrebbe ravvisarsi nell'art. 4-vicies ter, con il quale sono state unificate le tabelle che identificano le sostanze stupefacenti;&#13;
 che, in via subordinata, il Tribunale ha altresì lamentato che le disposizioni censurate violerebbero il predetto art. 77, secondo comma, Cost., per difetto dei presupposti della necessità ed urgenza.&#13;
 Considerato che, successivamente all'ordinanza di rimessione, questa Corte, con la sentenza n. 32 del 2014, ha dichiarato l'illegittimità costituzionale degli artt. 4-bis e 4-vicies ter, del decreto-legge 30 dicembre 2005, n. 272 (Misure urgenti per garantire la sicurezza ed i finanziamenti per le prossime Olimpiadi invernali, nonché la funzionalità dell'Amministrazione dell'interno. Disposizioni per favorire il recupero di tossicodipendenti recidivi e modifiche al testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza, di cui al d.P.R. 9 ottobre 1990, n. 309), convertito, con modificazioni, dall'art. 1, comma 1, della legge 21 febbraio 2006, n. 49;&#13;
 che, dunque, le questioni di legittimità costituzionale sopra indicate debbono essere dichiarate manifestamente inammissibili per sopravvenuta carenza di oggetto, giacché, a seguito della sentenza citata, le disposizioni censurate dal giudice a quo sono già state rimosse dall'ordinamento con efficacia ex tunc (ex plurimis, ordinanze n. 321 e n. 177 del 2013, n. 315 e n. 182 del 2012).&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale degli artt. 4-bis e 4-vicies ter del decreto-legge 30 dicembre 2005, n. 272 (Misure urgenti per garantire la sicurezza ed i finanziamenti per le prossime Olimpiadi invernali, nonché la funzionalità dell'Amministrazione dell'interno. Disposizioni per favorire il recupero di tossicodipendenti recidivi e modifiche al testo unico delle leggi in materia di disciplina degli stupefacenti e sostanze psicotrope, prevenzione, cura e riabilitazione dei relativi stati di tossicodipendenza, di cui al d.P.R. 9 ottobre 1990, n. 309), convertito, con modificazioni, dall'art. 1, comma 1, della legge 21 febbraio 2006, n. 49, sollevate, in riferimento all'art. 77, secondo comma, della Costituzione, dal Tribunale ordinario di Milano, undicesima sezione penale, con l'ordinanza indicata in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 luglio 2014.&#13;
 F.to:&#13;
 Sabino CASSESE, Presidente&#13;
 Marta CARTABIA, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 16 luglio 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
