<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>14</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:14</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>14/01/2008</data_decisione>
    <data_deposito>25/01/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, 5, e 6 del codice di procedura civile e degli artt. 3 e 5 della legge 31 maggio 1995, n. 218 (Riforma del sistema italiano di diritto internazionale privato), promosso con ordinanza del 5 aprile 2007 dal Tribunale di Sanremo nel procedimento civile vertente tra I. I. e l'Associazione Governo del Principato di Seborga, iscritta al n. 576 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 34, prima seria speciale, dell'anno 2007. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 12 dicembre 2007 il Giudice relatore Maria Rita Saulle. &#13;
      Ritenuto che, con ordinanza del 5 aprile 2007, il Tribunale di Sanremo ha sollevato, in riferimento agli artt. 10 e 11 della Costituzione, questione di legittimità costituzionale degli artt. 1, 5 e 6 del codice di procedura civile e degli artt. 3 e 5 della legge 31 maggio 1995, n. 218 (Riforma del sistema italiano di diritto internazionale privato), nella parte in cui non prevedono «la giurisdizione esclusiva di uno Stato non riconosciuto sovrano dallo Stato italiano, ma considerato tale da altre comunità e/o Stati stranieri riconosciuti dall'Italia»; &#13;
      che il giudizio a quo ha ad oggetto la convalida dello sfratto per morosità intimato dal proprietario di un immobile sito in Seborga e concesso in locazione al Governo del Principato di Seborga; &#13;
      che il rimettente, dopo aver rilevato che il suddetto Principato «parrebbe possedere i requisiti essenziali» di uno Stato, così da poter ottenere «il riconoscimento della propria sovranità», peraltro già avvenuto da parte di alcuni Stati, lamenta il presunto contrasto tra le norme impugnate e i parametri costituzionali evocati; &#13;
      che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o, comunque, manifestamente infondata; &#13;
      che, in via preliminare, la difesa erariale osserva che l'ordinanza è priva di motivazione in ordine alla rilevanza e alla non manifesta infondatezza della questione sollevata, atteso che il rimettente si è limitato ad indicare l'oggetto del giudizio a quo, evocando in maniera meramente assertiva i parametri costituzionali ipoteticamente violati e senza specificare il tipo di pronuncia richiesta alla Corte costituzionale; &#13;
      che, a parere dell'Avvocatura, le censure formulate sono comunque infondate, in quanto non esiste alcuna norma di diritto internazionale che impone allo Stato italiano di riconoscere il Principato di Seborga, non avendo, peraltro, l'Italia rinunciato ad alcuno dei suoi poteri nel suddetto territorio ed essendo comunque le limitazioni alla sovranità previste dall'art. 11 della Costituzione, subordinate, nella loro concreta individuazione, alla discrezionalità del legislatore ordinario. &#13;
      Considerato che il Tribunale di Sanremo dubita della legittimità costituzionale degli artt. 1, 5 e 6 del codice di procedura civile e degli artt. 3 e 5 della legge 31 maggio 1995, n. 218 (Riforma del sistema italiano di diritto internazionale privato), in riferimento agli artt. 10 e 11 della Costituzione, nella parte in cui non prevedono la giurisdizione esclusiva di uno Stato non riconosciuto sovrano dallo Stato italiano, ma considerato tale da altre comunità e/o Stati stranieri riconosciuti dall'Italia; &#13;
      che il rimettente si è limitato ad indicare le norme censurate e i parametri costituzionali da esse asseritamente lesi senza fornire alcuna motivazione riguardo al preteso contrasto; &#13;
      che tali lacune dell'ordinanza di rimessione, impedendo alla Corte di svolgere la necessaria verifica circa l'applicabilità delle norme denunciate nel giudizio principale, si risolvono nella radicale carenza di motivazione sulla rilevanza e non manifesta infondatezza e comportano, secondo la costante giurisprudenza di questa Corte, la manifesta inammissibilità della questione (ex plurimis, ordinanze numeri 164, 161 e 123 del 2006, numero 123 del 2005). &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87 e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE  &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale degli artt. 1, 5 e 6 del codice di procedura civile e degli artt. 3 e 5 della legge 31 maggio 1995, n. 218 (Riforma del sistema italiano di diritto internazionale privato), sollevata dal Tribunale di Sanremo, in riferimento agli artt. 10 e 11 della Costituzione, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 gennaio 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 25 gennaio 2008.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
