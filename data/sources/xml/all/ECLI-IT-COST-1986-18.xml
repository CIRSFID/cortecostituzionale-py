<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>18</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:18</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1986</data_decisione>
    <data_deposito>30/01/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. &#13;
 GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO - Prof. RENATO DELL'ANDRO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale  dell'art.  696,  &#13;
 comma primo, del codice di procedura civile promossi con n. 2 ordinanze  &#13;
 emesse  il  1  ottobre 1977 dal Pretore di Bari nei procedimenti civili  &#13;
 vertenti tra la s.p.a. FINA ITALIANA e Bonanno GIUSEPPE e tra la s.p.a.  &#13;
 FINA ITALIANA e Li Muli Vittorio, iscritte ai nn. 7 e  8  del  registro  &#13;
 ordinanze  1978  e pubblicate nella Gazzetta Ufficiale della Repubblica  &#13;
 nn. 74 e 81 dell'anno 1978.                                              &#13;
     Visti gli atti di costituzione della s.p.a. FINA ITALIANA,  nonché  &#13;
 gli atti di intervento del Presidente del Consiglio dei ministri;        &#13;
     udito nell'udienza pubblica dell'8 gennaio 1986 il Giudice relatore  &#13;
 Virgilio Andrioli;                                                       &#13;
     uditi  l'avv.  Napoleone  Bartuli  per  la  s.p.a.  FINA ITALIANA e  &#13;
 l'avvocato dello Stato Giorgio D'Amato per il Presidente del  Consiglio  &#13;
 dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.1.  -  Con  ricorso  depositato  il  27  settembre  1977, la FINA  &#13;
 ITALIANA s.p.a., premesso che il  dott.  Bonanno  Giuseppe,  dipendente  &#13;
 presso  la  filiale  di Bari con mansioni di addetto commerciale, aveva  &#13;
 totalizzato, nel corrente anno 1977, 135 giorni di assenze di malattia,  &#13;
 in atto perdurante, giustificate da certificazione medica e  confermate  &#13;
 dai  controlli  fatti  eseguire  dall'INAM  ai  sensi dell'art. 5 1. 20  &#13;
 maggio 1970, n.300, che essa FINA ITALIANA s.p.a.  intendeva contestare  &#13;
 le risultanze della certificazione medica allegata dal  Bonanno  e  dei  &#13;
 referti  delle  visite  di  controllo  INAM, e che, pertanto, intendeva  &#13;
 agire in giudizio per la tutela del proprio diritto a  contestare  e  a  &#13;
 oppugnare   risultanze   sanitarie   -   che   tanta   incidenza  hanno  &#13;
 nell'organizzazione  di  un'azienda  -  formatesi  senza  garanzia   di  &#13;
 certezza   e,  quindi,  a  difendersi  dalla  ineluttabilità  di  tali  &#13;
 risultanze, aveva chiesto al  Pretore  di  Bari  disporsi  accertamento  &#13;
 tecnico preventivo al fine di verificare, prima del giudizio di merito,  &#13;
 a)  lo  stato  di  malattia  del  Bonanno, b) se tale stato di malattia  &#13;
 impedisse al soggetto ogni  prestazione  lavorativa,  c)  se  lo  stato  &#13;
 d'infermità   e  il  suo  perdurare  fossero  derivati  da  incuria  o  &#13;
 negligenza del soggetto.                                                 &#13;
     Con  ordinanza  depositata  il  1  ottobre 1977 (comunicata il 21 e  &#13;
 notificata il 27 dello stesso mese; pubblicata nella G.U. n. 74 del  15  &#13;
 marzo  1978 e iscritta al n. 7 R.O. 1978), l'adito Pretore rilevato che  &#13;
 l'art. 696 comma primo c.p.c.  non consente il  richiesto  accertamento  &#13;
 tecnico  preventivo  concernente  le condizioni fisiche del resistente,  &#13;
 sollevò  d'ufficio  questione  d'illegittimità  costituzionale  della  &#13;
 menzionata disposizione del rito civile in riferimento all'art. 3 Cost.  &#13;
 perché  l'urgenza  di  far  verificare lo stato o la condizione di una  &#13;
 persona viene posta in una  condizione  deteriore  rispetto  a  chi  ha  &#13;
 urgenza di far verificare lo stato o la condizione di cose o di luoghi,  &#13;
 e in riferimento all'art.  24 Cost. perché ne riuscirebbe vulnerata la  &#13;
 possibilità  per  la  parte  di  acquisire agli atti prove in una fase  &#13;
 precedente al giudizio di merito; in punto a rilevanza  della  proposta  &#13;
 questione  l'adito  giudice la ritenne sussistere perché la ricorrente  &#13;
 società intendeva conseguire e conservare la prova di fatti  idonei  a  &#13;
 legittimare   eventuali   provvedimenti   ritenuti   conseguenti   alla  &#13;
 prospettata inesattezza degli accertamenti sanitari.                     &#13;
     1.2. - Avanti la Corte si sono costituiti, giusta delega in margine  &#13;
 all'atto depositato il 3 aprile 1978, gli avv.ti  Napoleone  Bartuli  e  &#13;
 Lorenzo  Ferrigni  non  solo  argomentando e concludendo nell'interesse  &#13;
 della FINA ITALIANA s.p.a. per la fondatezza della proposta  questione,  &#13;
 ma  anche prospettando la incostituzionalità dell'art. 5 comma secondo  &#13;
 dello  Statuto  dei  lavoratori   -   pur   dichiarata   manifestamente  &#13;
 insussistente dal giudice a quo - sul riflesso che sarebbe pregiudicato  &#13;
 il  diritto  di difesa per non essere previsto il contraddittorio delle  &#13;
 parti.                                                                   &#13;
     Per il Presidente del Consiglio dei ministri  è  intervenuta,  con  &#13;
 atto  depositato  il  4  aprile 1978, l'Avvocatura generale dello Stato  &#13;
 contestando la rilevanza della questione e argomentando  e  concludendo  &#13;
 per la infondatezza della proposta questione.                            &#13;
     2.1.  -  Con  ordinanza  emessa il l'ottobre 1977 (comunicata il 21  &#13;
 ottobre e notificata il 30 novembre successivi; pubblicata  nella  G.U.  &#13;
 n.  81 del 22 marzo 1978 e iscritta al n. 8 R.O. 1978) nel procedimento  &#13;
 ex art. 696 c.p.c. intentato  dalla  FINA  ITALIANA  s.p.a.  contro  il  &#13;
 dipendente Li Muli Vittorio, la cui posizione sostanziale e procedurale  &#13;
 non  era diversa da quella del Bonanno, il Pretore di Bari ha giudicato  &#13;
 rilevante e non manifestamente infondata la questione  d'illegittimità  &#13;
 costituzionale  dell'art.  696  comma  primo c.p.c. nella parte in cui,  &#13;
 limitando l'ammissibilità dell'accertamento  tecnico  preventivo  alla  &#13;
 verifica  dello stato dei luoghi o della qualità o condizione di cose,  &#13;
 esclude l'ammissibilità dello stesso mezzo istruttorio per la verifica  &#13;
 dello stato o della condizione o della qualità della persona.           &#13;
     2.2. - Avanti la Corte si sono costituiti, giusta delega in margine  &#13;
 all'atto depositato il 3 aprile 1978, gli avv.ti  Napoleone  Bartuli  e  &#13;
 Lorenzo  Ferrigni, argomentando e concludendo nell'interesse della FINA  &#13;
 ITALIANA  s.p.a.    per  la  fondatezza   della   proposta   questione.  &#13;
 Nell'interesse   del   Presidente   del   Consiglio   dei  ministri  è  &#13;
 intervenuta,  con  atto  depositato  l'11  aprile  1978,   l'Avvocatura  &#13;
 generale  dello  Stato argomentando e concludendo per la irrilevanza e,  &#13;
 in ipotesi, per la infondatezza della proposta questione.                &#13;
     3.1. - Nell'imminenza della pubblica udienza la difesa  della  FINA  &#13;
 ITALIANA  s.p.a.  ha  versato nei due incidenti memoria illustrativa in  &#13;
 cui ha svolto altre argomentazioni  a  sostegno  delle  conclusioni  di  &#13;
 fondatezza della questione.                                              &#13;
     3.2.  -  Alla  pubblica udienza dell'8 gennaio 1986, nella quale il  &#13;
 giudice Andrioli ha svolto congiunta relazione sui due incidenti, hanno  &#13;
 parlato l'avv. Bartuli per la FINA ITALIANA s.p.a. e l'avv. dello Stato  &#13;
 D'Amato per l'intervenuto Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     4.1. - I due incidenti vanno riuniti sia perché oggetto ne  è  la  &#13;
 stessa   questione   d'illegittimità   costituzionale  sia  perché  i  &#13;
 procedimenti svoltisi  avanti  il  Pretore  di  Bari,  dai  quali  sono  &#13;
 originati,  differiscono  sol  per essere due i dipendenti peraltro non  &#13;
 evocati né presenti  nei  procedimenti  stessi,  né  costituitisi  in  &#13;
 questa sede.                                                             &#13;
     4.2.  -  La  questione d'incostituzionalità, nei termini in cui è  &#13;
 stata prospettata, è inammissibile per  i  motivi,  sui  quali  ha  in  &#13;
 particolar  guisa  insistito  l'Avvocatura  generale  dello Stato negli  &#13;
 scritti e nella trattazione orale senza incontrare persuasive obiezioni  &#13;
 nelle difese della FINA ITALIANA s.p.a..                                 &#13;
     Porre invero la persona umana sullo stesso piano dei luoghi e delle  &#13;
 cose, che l'art. 696 comma primo c.p.c.   identifica quali  obietti  di  &#13;
 accertamento  tecnico  preventivo,  è  lecito  sol  a  chi  ometta  di  &#13;
 considerare che la persona umana, cui ci si riferisce  nel  dispositivo  &#13;
 delle ordinanze di rimessione, non può formare oggetto di procedimenti  &#13;
 cautelari,  né  il  corpo umano, cui ci si riferisce nella motivazione  &#13;
 delle stesse, può essere considerato avulso dalla persona laddove tale  &#13;
 inseparabilità non sussiste  per  i  beni  economici:  diversità  che  &#13;
 emerge  anche  dagli  artt. 2 e 42 della Carta costituzionale, il primo  &#13;
 dei  quali  considera  la  personalità  dell'uomo  e  il  secondo   la  &#13;
 proprietà pubblica e privata.                                           &#13;
     Né ha questa Corte mancato, con sent. n. 326/1983, di ammonire che  &#13;
 la legislazione italiana, ove confondesse il credito del dipendente per  &#13;
 infortunio  sul  lavoro tra i debiti chirografari del datore di lavoro,  &#13;
 regredirebbe ai tempi in cui il lavoro dell'uomo a vantaggio  di  altri  &#13;
 simili si allineava sul piano delle locationes bovis et rei.             &#13;
     In secondo luogo - e data per concessa la equiparabilità tra corpo  &#13;
 umano,  preso  nel  suo  complesso,  e  beni  economici e ipotizzata la  &#13;
 idoneità del corpo umano a formare  oggetto  di  accertamenti  tecnici  &#13;
 preventivi   -   le  ordinanze  di  rimessione  non  sono  riuscite  ad  &#13;
 identificare il " bene  della  vita  "  che  l'accertamento  preventivo  &#13;
 mirerebbe  a preservare dalle ingiurie del tempo: il Pretore di Bari si  &#13;
 è limitato  a  registrare  l'intendimento,  dalla  datrice  di  lavoro  &#13;
 espresso  nel  ricorso  ex  art.  693 c.p.c., di precostituire prove da  &#13;
 utilizzare  in  eventuali  iniziative  future  non   identificate   né  &#13;
 identificabili  perché  non  consta  che  i  due dipendenti si fossero  &#13;
 giovati degli accertamenti effettuati ai sensi dell'art. 5 l. 20 maggio  &#13;
 1970, n. 300 (disposizioni del cui  sospetto  d'incostituzionalità  le  &#13;
 ordinanze  di  rimessione  hanno  reputato  in motivazione la manifesta  &#13;
 infondatezza).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riuniti gli incidenti iscritti ai nn. 7,  8  R.O.  1978,  dichiaria  &#13;
 inammissibile  la questione di illegittimità costituzionale del l'art.  &#13;
 696 comma primo c.p.c. sollevata dal Pretore di  Bari,  in  riferimento  &#13;
 agli  artt.  3  e  24  commi  primo e secondo Cost. nella parte in cui,  &#13;
 limitando  l'ammissibilità  dell'accertamento  tecnico preventivo alla  &#13;
 verifica dello stato dei luoghi e della qualità o condizione di  cose,  &#13;
 esclude,   l'ammissibilità  dello  stesso  mezzo  istruttorio  per  la  &#13;
 verifica dello stato o della condizione o della qualità della  persona  &#13;
 umana.                                                                   &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 22 gennaio 1986.                              &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO.   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
