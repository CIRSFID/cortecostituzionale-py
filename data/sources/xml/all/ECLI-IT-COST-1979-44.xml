<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>44</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:44</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/06/1979</data_decisione>
    <data_deposito>18/06/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 2948, n. 4,  &#13;
 cod. civ., promosso con   ordinanza  emessa  il  25  ottobre  1977  dal  &#13;
 pretore  di  Bolzano,  nel  procedimento  civile  vertente   tra Scalet  &#13;
 Erminia ed altri e la Cassa di Risparmio della  Provincia  di  Bolzano,  &#13;
 iscritta  al  n.  586  del  registro  ordinanze 1977 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 60  del 1 marzo 1978.             &#13;
     Visto l'atto  di  costituzione  di  Pettenella  Maria  e  Triggiani  &#13;
 Fernando, della Cassa di  Risparmio della Provincia di Bolzano, nonché  &#13;
 l'atto di intervento del Presidente del  Consiglio dei ministri;         &#13;
     udito  nell'udienza  pubblica del 21 marzo 1979 il Giudice relatore  &#13;
 Virgilio Andrioli;                                                       &#13;
     uditi gli avvocati Luciano  Ventura  per  Pettenella  e  Triggiani,  &#13;
 Giuseppe  Guarino per la Cassa  di Risparmio della Provincia di Bolzano  &#13;
 e il sostituto avvocato generale dello Stato  Renato    Carafa  per  il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ricorsi 9 febbraio 1977 e 9 marzo 1977 Erminia Scalet, Fernando  &#13;
 Triggiani  e  altri  126    dipendenti  della  Cassa di Risparmio della  &#13;
 Provincia di Bolzano, premesso che  la  Cassa  aveva    corrisposto  al  &#13;
 personale  femminile  e  a  dipendenti  in  minore  età un trattamento  &#13;
 economico inferiore a quello  applicato  rispettivamente  al  personale  &#13;
 maschile  e agli impiegati  maggiorenni, e, inoltre, che il trattamento  &#13;
 economico per coloro che avevano prestato servizio militare in costanza  &#13;
 di rapporto di lavoro non teneva conto di tale servizio ai fini   della  &#13;
 progressione  economica,  chiesero al pretore di Bolzano in funzione di  &#13;
 giudice del   lavoro condannarsi  la  Cassa  alla  ricostruzione  della  &#13;
 carriera   economica   a  seguito  della  parificazione  tabellare  dei  &#13;
 lavoratori minori di età con quelli di età maggiore e  del  personale  &#13;
 femminile con quello maschile, nonché  a seguito del riconoscimento del  &#13;
 servizio  militare    compiuto  in costanza del rapporto di lavoro, con  &#13;
 decorrenza dal 1  gennaio  1948.  La  Cassa  costituitasi  in  giudizio  &#13;
 eccepì,  in via preliminare, la prescrizione  dei diritti fatti valere  &#13;
 dagli attori ai sensi dell'art. 2948, n. 4, cod. civ. A seguito di che,  &#13;
 l'adito pretore, con ordinanza 25 ottobre 1977, notificata,  comunicata  &#13;
 e  pubblicata  sulla  Gazzetta   Ufficiale n. 60 del 1 marzo 1978 (ord.  &#13;
 586/1977),  ha dichiarato non manifestamente  infondata la questione di  &#13;
 legittimità costituzionale dell'articolo 2948, n. 4, cod.  civ.  nella  &#13;
 parte  in  cui  consente  il  decorso della prescrizione dei crediti di  &#13;
 lavoro in pendenza di  rapporto di pubblico impiego per  contrasto  con  &#13;
 l'art. 3 della Costituzione.                                             &#13;
     Avanti  la Corte si sono costituiti due dei ricorrenti - Pettenella  &#13;
 Maria Pia in Ku-Statscher e  Triggiani Fernando -  che, nelle deduzioni  &#13;
 31 gennaio 1978, hanno concluso per  l'inammissibilità della questione  &#13;
 vuoi perché gli  asseriti  limiti  degli  effetti  della  sentenza  n.  &#13;
 63/1966  della  Corte  non  possono  dar  luogo ad una dichiarazione di  &#13;
 illegittimità    costituzionale  in  quanto  non  risultano   inseriti  &#13;
 nell'ordinamento  da  legge  o da dispositivo di   sentenza della Corte  &#13;
 vuoi perché l'accertamento della garanzia della stabilità  del  posto  &#13;
 di  lavoro  e  del  conseguente  diverso regime giuridico che regola il  &#13;
 rapporto è compito esclusivo  del giudice ordinario e non può  essere  &#13;
 vincolato  al  rispetto  di  criteri  interpretativi  che  non    siano  &#13;
 espressamente previsti  da  norme  vigenti,  e  la  Cassa,  che,  nelle  &#13;
 deduzioni  14 marzo 1978, ha concluso per la assoluta irrilevanza della  &#13;
 questione sollevata dal pretore di Bolzano   perché la  disuguaglianza  &#13;
 di  trattamento,  che ha il parametro di costituzionalità nell'art. 3,  &#13;
 assume per  poli  di  contrasto  non  già,  come  reputa  il  pretore,  &#13;
 dipendenti  pubblici  e lavoratori  privati, che possono giovarsi delle  &#13;
 leggi 604/1966 e 300/1970, sibbene  rapporti  muniti  di  stabilità  e  &#13;
 rapporti  che  di  tale  stabilità  sono privi e ne ha la Cassa stessa  &#13;
 inferito la estraneità  della questione a rapporti, che,  come  quelli  &#13;
 intrattenuti  con  i  suoi  dipendenti, sono assistiti   da garanzie di  &#13;
 stabilità. La  Presidenza  del  Consiglio  dei  ministri  ha  spiegato  &#13;
 intervento    mediante  atto  20 marzo 1978, in cui, richiamati atti di  &#13;
 intervento spiegato in altri procedimenti, insta per  la  dichiarazione  &#13;
 di infondatezza della proposta questione. Alla  udienza pubblica del 21  &#13;
 marzo  1979 le difese delle parti private e l'Avvocatura generale dello  &#13;
 Stato hanno illustrato le riassunte conclusioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     Successivamente   alla   sentenza   63/1966,   su   cui   si   sono  &#13;
 particolarmente  diffusi  le  parti  private    e  il pretore, la Corte  &#13;
 costituzionale,   con   sentenza    n.    174/1972,    ha    dichiarato  &#13;
 l'illegittimità    dell'art. 49, comma terzo, del contratto collettivo  &#13;
 di lavoro 24 maggio 1956 per i dipendenti  delle case di cura  private,  &#13;
 recepito  dall'articolo  unico del d.P.R. 1040/1960, nella parte che fa  &#13;
 decorrere il termine di decadenza per i reclami dei dipendenti medesimi  &#13;
 dal giorno in cui il  pagamento venga effettuato, anche per i  rapporti  &#13;
 di   lavoro   non  considerati  dalla  legge    604/1966  e  successive  &#13;
 modificazioni (tra cui la  legge  300/1970),  ritenendo  incontestabile  &#13;
 l'analogia  tra  i rapporti di impiego pubblico, estranei alla sentenza  &#13;
 63/1966, e quei rapporti  di  diritto  privato,  per  i  quali  ricorra  &#13;
 l'applicabilità delle due serie di disposizioni menzionate,  di cui la  &#13;
 seconda  (e  cioè  la  legge  300/1970)  deve  considerarsi necessaria  &#13;
 integrazione della  prima, dato che una vera stabilità non si assicura  &#13;
 se all'annullamento dell'avvenuto licenziamento non si  faccia  seguire  &#13;
 la  completa  reintegrazione  nella  posizione giuridica   preesistente  &#13;
 fatta illegittimamente cessare.                                          &#13;
     Non spetta a questa Corte, che ha avuto  occasione  di  riaffermare  &#13;
 tale  orientamento  anche    nella  sent.    115/1975,  il  compito  di  &#13;
 verificare se  i  giudici  delle  controversie  intendano  nel    senso  &#13;
 conforme  alla  legge, alla quale soltanto sono, ai sensi dell'art. 101  &#13;
 Cost.,  soggetti,  la    duplice  condizione, chiaramente puntualizzata  &#13;
 nella sent.  174/  1972  (possibilità  di  annullamento  dell'atto  di  &#13;
 licenziamento;   completa   reintegrazione  della  posizione  giuridica  &#13;
 preesistente fatta illegittimamente cessare), così come ben potrà  il  &#13;
 Parlamento approvare  leggi, che pongano punti fermi nel tutt'altro che  &#13;
 univoco  contesto  normativo  in  atto  e  in  irrefrenabile  divenire;  &#13;
 contesto di cui fan parte - è appena il caso di rilevarlo  -  anche  i  &#13;
 dispositivi  di  pronunce  di  fondatezza  di questioni di legittimità  &#13;
 costituzionale rese dalla  Corte e pubblicati nei modi  di  legge,  nel  &#13;
 senso  fatto  palese  dal  significato  proprio delle parole secondo la  &#13;
 connessione di esse e dalla intenzione espressa da questa  Corte  nelle  &#13;
 motivazioni delle pronunce medesime.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 2948, n. 4, cod. civ.,    nella  parte  in  cui  consente  il  &#13;
 decorso  della  prescrizione  dei  crediti  di  lavoro  in pendenza del  &#13;
 rapporto di pubblico impiego  sollevata  dal  pretore  di  Bolzano  con  &#13;
 l'ordinanza   25  ottobre    1977,  in  riferimento  all'art.  3  della  &#13;
 Costituzione.                                                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 12  giugno 1979.                              &#13;
                                   F.to:   LEONETTO   AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA -  GUIDO  ASTUTI  -  MICHELE  &#13;
                                   ROSSANO   -  ANTONINO  DE  STEFANO  -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO REALE -  BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
