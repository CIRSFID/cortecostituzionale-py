<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>241</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:241</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/05/1992</data_decisione>
    <data_deposito>03/06/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi di legittimità  costituzionale  dell'art.  519,  secondo    &#13;
 comma,  del  codice  di  procedura  penale,  promossi con le seguenti    &#13;
 ordinanze:                                                               &#13;
      1) ordinanza emessa il 23 maggio 1991 dal Pretore di Potenza nel    &#13;
 procedimento penale a carico di Verrastro Leonardo,  iscritta  al  n.    &#13;
 576 del registro ordinanze 1991 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 38, prima serie speciale, dell'anno 1991;            &#13;
      2) ordinanza emessa il 1° luglio 1991 dal Tribunale di Lecce nel    &#13;
 procedimento  penale  a carico di Del Coco Antonio ed altri, iscritta    &#13;
 al n. 666 del registro ordinanze 1991  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  44, prima serie speciale, dell'anno    &#13;
 1991;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di  consiglio  del  4  marzo  1992  il  Giudice    &#13;
 relatore Ugo Spagnoli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel corso di un processo per il delitto di incendio colposo    &#13;
 di un fabbricato, addebitato  per  deficienze  di  costruzione  e  di    &#13;
 misure   preventive,  il  pubblico  ministero  contestava,  ai  sensi    &#13;
 dell'art. 516  cod.  proc.  pen.,  un  ulteriore  profilo  di  colpa,    &#13;
 consistente nella tardività della richiesta di intervento dei Vigili    &#13;
 del  Fuoco.  In  relazione  a  tale  nuova contestazione, il pubblico    &#13;
 ministero  e  l'imputato  chiedevano  l'ammissione  di  nuove  prove,    &#13;
 depositando  le  relative  liste  testimoniali ai sensi dell'art. 468    &#13;
 cod. proc. pen.                                                          &#13;
    Il Tribunale di Lecce, rilevato che  l'art.  519,  secondo  comma,    &#13;
 cod.  proc.  pen., per l'ipotesi (tra l'altro) di nuove contestazioni    &#13;
 prevede, oltre al termine a difesa, che "in ogni caso l'imputato può    &#13;
 chiedere l'ammissione di  nuove  prove  a  norma  dell'art.  507",  e    &#13;
 ritenendo  che - in base al tenore di tale disposizione - il pubblico    &#13;
 ministero (e la persona offesa, anche se costituita parte civile) non    &#13;
 possano chiedere nuove prove, e che anche per l'imputato esse possono    &#13;
 essere ammesse solo se ciò "risulta assolutamente necessario"  (art.    &#13;
 507),  ha sollevato d'ufficio, con ordinanza del 1° luglio 1991 (r.o.    &#13;
 n. 666/91), una questione di legittimità costituzionale del suddetto    &#13;
 art. 519, secondo comma - in  quanto  ostativo,  nei  detti  termini,    &#13;
 all'accoglimento  della  richiesta - assumendone il contrasto con gli    &#13;
 artt. 3, 24, 76 e 97 Cost.                                               &#13;
    Ad avviso del  giudice  a  quo,  la  giustificazione  della  norma    &#13;
 impugnata data nella Relazione al codice, secondo la quale l'espresso    &#13;
 richiamo  all'art. 507 sarebbe sostanzialmente superfluo, dato che il    &#13;
 diritto di chiedere prove in base a  tale  disposizione  può  sempre    &#13;
 esercitarsi  fino  all'inizio  della discussione finale, è frutto di    &#13;
 inadeguata comprensione dei meccanismi di articolazione  del  diritto    &#13;
 alla  prova risultanti dal testo del codice. Dagli artt. 468 e 493 si    &#13;
 ricava, infatti, un principio - connesso a quello di concentrazione -    &#13;
 di preclusione nella deduzione dei mezzi di prova. Di questo,  l'art.    &#13;
 507  è  un  indispensabile  correttivo,  limitato,  però  - secondo    &#13;
 l'interpretazione giurisprudenziale - ai soli casi  in  cui  le  tesi    &#13;
 dell'accusa  o  della  difesa  siano  supportate  da  alcuni elementi    &#13;
 probatori,   e   tuttavia   risulti   un'incompletezza    rimediabile    &#13;
 nell'istruzione   della   causa.   L'art.   507,  quindi,  presuppone    &#13;
 un'insufficiente attività probatoria delle parti e riguarda mezzi di    &#13;
 prova dai quali queste sono sostanzialmente decadute.                    &#13;
    Nei casi, invece, in cui l'esigenza di  nuove  prove  emerge  solo    &#13;
 all'esito  delle prove della controparte, ovvero da nuove circostanze    &#13;
 emerse dall'espletamento dei mezzi di prova originari (ad es.,  dalla    &#13;
 contraddittorietà   delle  dichiarazioni  rese  da  due  testimoni),    &#13;
 subordinare le nuove prove (nell'esempio,  il  confronto  tra  i  due    &#13;
 testi), al requisito dell'assoluta necessità ex art. 507 condurrebbe    &#13;
 a  risultati irrazionali. Né sarebbe possibile ritenere che le parti    &#13;
 siano tenute a premunirsi indicando preventivamente  mezzi  di  prova    &#13;
 per simili eventualità all'atto delle richieste di cui all'art. 493,    &#13;
 dato  che  dalle norme in materia non può desumersi che il principio    &#13;
 di preclusione sia stato portato fino all'estremo del c.d.  principio    &#13;
 di  eventualità.  Questo è, del resto, estraneo agli altri processi    &#13;
 ispirati  ai  principi  di  oralità,  concentrazione e immediatezza,    &#13;
 quali il rito del lavoro ed il nuovo modello  generale  del  processo    &#13;
 civile (legge n. 353 del 1990), nei quali, invece, i limiti ai "nova"    &#13;
 possono  essere  superati con "replicatio " e "duplicatio" in caso di    &#13;
 impossibilità di precedente deduzione del mezzo di prova (art.  420,    &#13;
 commi  quinto  e  settimo, cod. proc. civ.): e ciò dovrebbe valere a    &#13;
 maggior ragione nel nuovo processo penale, in cui le parti non  hanno    &#13;
 a priori una piena conoscenza dei fatti di causa.                        &#13;
    Secondo  il  Tribunale  rimettente,  quindi, il diritto alla prova    &#13;
 sulle circostanze e con i mezzi divenuti ammissibili e rilevanti solo    &#13;
 nel corso dell'istruzione dovrebbe  essere  riconosciuto  negli  ampi    &#13;
 limiti  di  cui  all'art.  190 cod. proc. pen. e non in quelli di cui    &#13;
 all'art. 507.                                                            &#13;
    Il diniego al pubblico ministero del diritto  a  nuove  prove  non    &#13;
 potrebbe  giustificarsi,  d'altra  parte,  presupponendo che le nuove    &#13;
 contestazioni debbano avvenire solo se ci siano elementi già di  per    &#13;
 sé  sufficienti per la condanna; l'art. 516 richiede, invece, che la    &#13;
 contestazione  avvenga  non  appena  vi  sono   seri   elementi   per    &#13;
 effettuarla,  onde  consentire  all'imputato  di difendersi subito ed    &#13;
 evitare che il pubblico ministero possa essere indotto a  cercare  di    &#13;
 estrarre dai mezzi già ammessi le prove per contestazioni future.       &#13;
    Per  altro  verso, non sono accoglibili, secondo il giudice a quo,    &#13;
 interpretazioni della norma impugnata tendenti a mitigarne il rigore,    &#13;
 quali quella secondo cui il limite di cui all'art. 507 varrebbe  solo    &#13;
 se  l'imputato  non  chiede termini a difesa o l'altra secondo cui è    &#13;
 assolutamente necessario tutto ciò che all'imputato sarebbe spettato    &#13;
 se l'imputazione avesse avuto sin dall'inizio i connotati assunti  in    &#13;
 seguito.                                                                 &#13;
    Alla   stregua   delle   suesposte  considerazioni,  il  Tribunale    &#13;
 rimettente  prospetta  la  violazione  degli  artt.  3  e  24   della    &#13;
 Costituzione,  in  quanto  la  norma  impugnata  introduce una deroga    &#13;
 irrazionale ed ingiustificata ai principi generali in tema di diritto    &#13;
 alla prova, compresso per l'imputato e negato  per  le  altre  parti.    &#13;
 Sarebbero  violate,  inoltre,  le direttive della legge delega di cui    &#13;
 all'art. 2, punti 3 (parità tra accusa e difesa), 69  (diritto  alla    &#13;
 prova  di  tutte le parti) e 78 (garanzie di difesa adeguate - ma non    &#13;
 meccanismi irrazionali - in caso di nuove contestazioni) e,  perciò,    &#13;
 l'art.  76  della  Costituzione. Dal fatto che al pubblico ministero,    &#13;
 tenuto ad indagare, sia inibito di fornire  le  prove  della  propria    &#13;
 attività   scaturirebbe,   infine,   un   difetto  di  funzionalità    &#13;
 dell'amministrazione della giustizia, con lesione dell'art. 97  della    &#13;
 Costituzione.                                                            &#13;
    2. - Un'analoga questione di legittimità costituzionale dell'art.    &#13;
 519, secondo comma, cod. proc. pen. è stata sollevata dal Pretore di    &#13;
 Potenza,  con  ordinanza del 23 maggio 1991 (r.o. n. 576/91), in sede    &#13;
 di decisione sull'ammissibilità  di  liste  testimoniali  presentate    &#13;
 dall'imputato  e  dalla  parte  civile  a  seguito  di  una  modifica    &#13;
 dell'imputazione effettuata dal pubblico  ministero,  concernente  la    &#13;
 data di commissione del fatto.                                           &#13;
    Muovendo dal presupposto che il riferimento all'art. 507 comporta,    &#13;
 ai  fini  dell'ammissione,  non  solo  il  requisito più restrittivo    &#13;
 dell'assoluta necessità, ma un potere discrezionale del  giudice  in    &#13;
 applicazione di tale regola di giudizio, e che alle parti private diverse  dall'imputato  -  ed in particolare alla parte civile - non è    &#13;
 riconosciuto  il diritto di richiedere nuove prove a seguito di nuove    &#13;
 contestazioni, il giudice a quo impugna la predetta disposizione  sia    &#13;
 nella  parte in cui ammette la richiesta di nuove prove, a seguito di    &#13;
 nuove contestazioni, esclusivamente "a norma dell'art.  507  c.p.p.",    &#13;
 sia,  in  particolare, nella parte in cui ammette tale richiesta solo    &#13;
 per l'imputato e non anche per la parte civile.                          &#13;
    Anche  secondo  l'ordinanza  in   esame,   di   fronte   a   nuove    &#13;
 contestazioni  il  diritto  alla prova dovrebbe esplicarsi pienamente    &#13;
 alla stregua della regola generale desumibile dagli artt. 190,  primo    &#13;
 comma  e  495,  primo  comma,  cod.  proc.  pen.:  onde  la lamentata    &#13;
 violazione dei principi di uguaglianza e ragionevolezza (art. 3 della    &#13;
 Costituzione), del diritto di difesa (art. 24) e dell'art.  76  della    &#13;
 Costituzione, in riferimento alle citate direttive di cui ai numeri 3    &#13;
 e 69 dell'art. 2 della legge delega.                                     &#13;
    In  particolare,  il Pretore di Potenza nega - in riferimento alla    &#13;
 parte civile - che rispetto alle nuove contestazioni sia  applicabile    &#13;
 l'art.  493,  terzo comma, del codice, dato che questo fa riferimento    &#13;
 ad un'impossibilità di tempestiva deduzione di carattere  materiale,    &#13;
 tant'è  che ne è richiesta la dimostrazione; ma rileva che, anche a    &#13;
 ritenere tale norma applicabile,  la  posizione  della  parte  civile    &#13;
 sarebbe  comunque  deteriore  rispetto  a  quella  dell'imputato, sia    &#13;
 perché  essa  dovrebbe  fornire  tale  dimostrazione,  sia   perché    &#13;
 l'imputato, dopo la nuova contestazione, può chiedere immediatamente    &#13;
 le  nuove  prove,  mentre  la  parte  civile, in forza dell'art. 507,    &#13;
 dovrebbe  attendere  l'espletamento  di  queste  e  potrebbe  vedersi    &#13;
 respinte  le  proprie  qualora  il  quadro  probatorio  in  tal  modo    &#13;
 acquisito venisse ritenuto esauriente.                                   &#13;
    3. - Il Presidente del Consiglio  dei  ministri,  rappresentato  e    &#13;
 difeso  dall'Avvocatura  Generale  dello  Stato,  è  intervenuto nei    &#13;
 predetti giudizi con memorie di tenore  identico,  riferite  entrambe    &#13;
 alla fattispecie oggetto della seconda ordinanza.                        &#13;
    Posto   che  in  questa  la  variazione  rispetto  all'imputazione    &#13;
 originaria concerne solo la data  del  commesso  reato,  il  richiamo    &#13;
 all'art. 507 è, secondo l'Avvocatura, pienamente legittimo e sarebbe    &#13;
 invece  irragionevole  il ricorso ad una disciplina lunga e complessa    &#13;
 quale quella di cui all'art. 493 cod. proc. pen.                         &#13;
    Ad avviso dell'interveniente, inoltre, la questione sarebbe frutto    &#13;
 di erronea interpretazione, dato che, come si evince dalla  Relazione    &#13;
 al  progetto  preliminare  del  codice  (p. 119), il richiamo al solo    &#13;
 imputato non comporta un'"esclusiva"  di  costui  nella  facoltà  di    &#13;
 attivare  il  meccanismo di cui all'art. 507, ma vale solo a chiarire    &#13;
 che egli può chiedere nuove  prove  anche  se  non  si  avvalga  del    &#13;
 termine a difesa, di cui allo stesso art. 519, secondo comma.<diritto>Considerato in diritto</diritto>1. - I due giudizi investono la medesima disposizione di legge, ed    &#13;
 è perciò opportuna la loro riunione.                                   &#13;
    2.  - Il Tribunale di Lecce ed il Pretore di Potenza dubitano, con    &#13;
 le ordinanze indicate in epigrafe, della legittimità  costituzionale    &#13;
 dell'art.  519,  secondo comma, del codice di procedura penale, nella    &#13;
 parte  in  cui  -  dopo  aver  previsto  che  nell'ipotesi  di  nuove    &#13;
 contestazioni  ex  art.  516 l'imputato ha diritto ad ottenere, se lo    &#13;
 chiede, un termine a difesa - stabilisce che "In ogni caso l'imputato    &#13;
 può chiedere l'ammissione di nuove prove a norma dell'articolo 507".    &#13;
 Secondo i giudici a quibus, tale  disposizione  comporta  un  duplice    &#13;
 limite  alle  richieste probatorie relative alle nuove contestazioni,    &#13;
 nel senso che esse, da un lato, risultano precluse  per  il  pubblico    &#13;
 ministero e per le parti private diverse dall'imputato e, dall'altro,    &#13;
 sono  limitate,  per  lo  stesso  imputato,  a  quelle "assolutamente    &#13;
 necessarie"  secondo  la  discrezionale  valutazione   del   giudice.    &#13;
 Sarebbero perciò violati, ad avviso di entrambi i giudici, gli artt.    &#13;
 3,   24   e  76  della  Costituzione,  dato  che  in  caso  di  nuove    &#13;
 contestazioni il diritto alla prova andrebbe riconosciuto secondo  le    &#13;
 regole  generali  di  cui  agli  artt. 190 e 495 cod. proc. pen. e le    &#13;
 predette preclusioni e limitazioni sarebbero irragionevoli, contrarie    &#13;
 al principio di uguaglianza e tali  da  produrre  una  ingiustificata    &#13;
 limitazione  del  diritto  di  difesa, nonché lesive dei principi di    &#13;
 parità tra accusa e difesa, di diritto alla prova di tutte le  parti    &#13;
 e   di   adeguatezza  delle  garanzie  difensive  in  caso  di  nuove    &#13;
 contestazioni di cui ai nn. 3, 69 e 78 dell'art. 2 della legge delega    &#13;
 n. 81 del 1987.                                                          &#13;
    Secondo il Tribunale di Lecce  sarebbe  violato  anche  l'art.  97    &#13;
 della  Costituzione,  perché  l'impedire  al  pubblico  ministero di    &#13;
 provare i fatti su cui  è  tenuto  ad  indagare  si  tradurrebbe  in    &#13;
 difetto di funzionalità dell'amministrazione della giustizia.           &#13;
    3.   -   Le  questioni  sono  fondate  sotto  entrambi  i  profili    &#13;
 prospettati.                                                             &#13;
    In un sistema processuale imperniato su  un  ampio  riconoscimento    &#13;
 del  diritto  alla  prova  e  nel  quale l'acquisizione del materiale    &#13;
 probatorio è rimessa in primo luogo all'iniziativa delle parti (art.    &#13;
 190),   è   indubbiamente   incongruo   che   la    regolamentazione    &#13;
 dell'attività  probatoria  che  si rende necessaria in caso di nuove    &#13;
 contestazioni sia effettuata mediante il richiamo  all'art.  507:  ad    &#13;
 una  norma,  cioè,  che  conferisce  al  giudice il potere-dovere di    &#13;
 integrazione, anche d'ufficio, delle prove per l'ipotesi  in  cui  la    &#13;
 carenza o insufficienza, per qualsiasi ragione, dell'iniziativa delle    &#13;
 parti   impedisca   al  dibattimento  di  assolvere  la  funzione  di    &#13;
 assicurare la piena conoscenza da parte del giudice dei fatti oggetto    &#13;
 del processo, onde consentirgli di pervenire ad una giusta decisione.    &#13;
 Tale potere-dovere di integrazione si colloca infatti in una fase  in    &#13;
 cui  è "terminata l'acquisizione delle prove" che siano state svolte    &#13;
 ad iniziativa delle parti (artt. 468, 493, 495) o su indicazione  del    &#13;
 giudice  (art.  506);  e  di  conseguenza,  esso  è  connotato da un    &#13;
 criterio di "assoluta necessità" delle nuove  prove,  cioè  da  una    &#13;
 più  penetrante  e  approfondita valutazione della loro pertinenza e    &#13;
 rilevanza che è correlativa alla più ampia conoscenza dei fatti  di    &#13;
 causa che il giudice ha ormai conseguito in tale momento.                &#13;
    In  ben  diversa  fase  si colloca l'ammissione dei mezzi di prova    &#13;
 conseguente a nuove contestazioni del pubblico ministero, che aprono,    &#13;
 nel corso del dibattimento, nuovi momenti di dialettica tra le parti,    &#13;
 sotto  il  profilo  probatorio,  per  effetto   della   modificazione    &#13;
 dell'accusa.  Non vi è ragione, in questa fase, di non riconoscere a    &#13;
 ciascuna  delle  parti  l'esercizio  pieno  del  diritto  alla  prova    &#13;
 rispetto  agli  elementi nuovi emersi nel processo, secondo i criteri    &#13;
 generali  previsti  dall'art.    190,  alla  stregua  dei  quali   il    &#13;
 legislatore  ha  garantito non solo la piena esplicazione del diritto    &#13;
 alla controprova sulle  circostanze  da  altri  dedotte  (artt.  468,    &#13;
 quarto comma e 495, secondo comma), ma anche il diritto ad introdurre    &#13;
 le  prove  nuove che non si sia potuto indicare tempestivamente (art.    &#13;
 493,  terzo  comma),  o  la  cui  rilevanza scaturisca da quelle già    &#13;
 assunte  o  acquisite  nel  dibattimento  (art.      506)   o   dalla    &#13;
 insufficienza  di  queste (artt. 507 e 603). Ovviamente, i limiti che    &#13;
 il giudice concretamente imporrà alla ammissione delle prove,  sotto    &#13;
 il  profilo  della rilevanza, discenderanno anche dal maggior livello    &#13;
 di conoscenza di dati e di elementi acquisiti nel corso del processo,    &#13;
 certamente  superiore  rispetto  a  quanto  conosciuto   nella   fase    &#13;
 predibattimentale.  Ma  si tratta pur sempre di modalità concrete di    &#13;
 applicazione  di  generali  criteri  che  debbono   presiedere   alla    &#13;
 assunzione  della  prova  durante il dibattimento ad iniziativa delle    &#13;
 parti, che non coincidono con quelli che caratterizzano  l'iniziativa    &#13;
 del giudice nella fase terminale del processo.                           &#13;
    È perciò evidentemente irragionevole, nonché lesivo del diritto    &#13;
 di  difesa, che di fronte a nuove contestazioni - rispetto alle quali    &#13;
 un onere di preventiva indicazione  di  prove  è  da  escludere  per    &#13;
 definizione  -  il  diritto  alla  prova delle parti possa incontrare    &#13;
 limiti diversi e più penetranti di quelli vigenti  in  via  generale    &#13;
 per i "nova".                                                            &#13;
    4. - Quanto detto finora vale anche a dimostrare la fondatezza del    &#13;
 secondo  profilo  di  incostituzionalità denunciato. La disposizione    &#13;
 impugnata, infatti, riferendosi testualmente al solo imputato, sembra    &#13;
 implicare che in presenza di  nuove  contestazioni  il  diritto  alla    &#13;
 prova  non spetti, neanche nei limiti di cui all'art. 507, alle altre    &#13;
 parti private e al pubblico ministero.                                   &#13;
    Quanto a quest'ultimo, deve condividersi il rilievo del  Tribunale    &#13;
 di  Lecce  secondo  cui  tale  esclusione  non potrebbe giustificarsi    &#13;
 assumendo che le nuove contestazioni vanno effettuate solo se vi sono    &#13;
 già elementi sufficienti a dare compiuta dimostrazione  delle  relative   circostanze.  La  logica  del  pieno  contraddittorio  cui  è    &#13;
 improntata  la  fase  dibattimentale  esige,  al  contrario,  che  la    &#13;
 contestazione   avvenga   non   appena  emergano  seri  elementi  per    &#13;
 effettuarla: sicché, se al pubblico ministero - alla  stregua  della    &#13;
 norma  impugnata  -  è  inibito  di  integrarli per pervenire ad una    &#13;
 compiuta dimostrazione del proprio assunto, il suo diritto alla prova    &#13;
 in condizioni di parità con l'imputato ne  risulta  compromesso.  Ed    &#13;
 analoga  violazione del principio di parità delle parti, nonché del    &#13;
 diritto di difesa, deve riconoscersi  per  l'esclusione  dal  diritto    &#13;
 alla prova delle parti private diverse dall'imputato, alle quali pure    &#13;
 sono  riferite tanto la disposizione generale di cui all'art. 190 che    &#13;
 quelle specifiche dianzi richiamate.                                     &#13;
    L'art. 519, secondo comma, cod. proc. pen. va  perciò  dichiarato    &#13;
 costituzionalmente  illegittimo,  per  violazione  degli artt. 3 e 24    &#13;
 della Costituzione (restando con ciò assorbiti gli  altri  parametri    &#13;
 invocati),  tanto nella parte in cui, in caso di nuove contestazioni,    &#13;
 consente all'imputato di chiedere l'ammissione di nuove prove solo "a    &#13;
 norma dell'art. 507", quanto nella parte in  cui  esclude  che  nuove    &#13;
 prove possano essere in tal caso chieste anche dalle altre parti private e dal pubblico ministero.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   illegittimità  costituzionale  del  secondo  comma    &#13;
 dell'art. 519 del codice di procedura penale:                            &#13;
       a)  nella  parte  in  cui,  nei casi previsti dall'art. 516 del    &#13;
 codice di procedura penale, non consente al pubblico ministero e alle    &#13;
 parti private diverse dall'imputato di chiedere l'ammissione di nuove    &#13;
 prove;                                                                   &#13;
       b) dell'inciso, "a norma dell'art. 507".                           &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 maggio 1992.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: SPAGNOLI                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 3 giugno 1992.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
