<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1978</anno_pronuncia>
    <numero_pronuncia>46</numero_pronuncia>
    <ecli>ECLI:IT:COST:1978:46</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/04/1978</data_decisione>
    <data_deposito>20/04/1978</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. LEONETTO AMADEI - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. &#13;
 LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1 della legge  &#13;
 22 maggio 1975, n. 152 (Disposizioni a  tutela  dell'ordine  pubblico),  &#13;
 promosso  con  ordinanza  emessa il 9 dicembre 1975 dal tribunale per i  &#13;
 minorenni di Torino, nel procedimento penale  a  carico  di  Ciavarelli  &#13;
 Massimo  ed  altro,  iscritta  al  n. 284 del registro ordinanze 1976 e  &#13;
 pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  145  del  3  &#13;
 giugno 1976.                                                             &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  20  dicembre  1977  il  Giudice  &#13;
 relatore Michele Rossano;                                                &#13;
     udito  il sostituto avvocato generale dello Stato Giorgio Azzariti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il Tribunale per i minorenni di Torino, nel corso del  procedimento  &#13;
 a  carico  di Scarano Nicola e Ciavarelli Massimo, imputati del delitto  &#13;
 di rapina aggravata ai sensi dell'art. 628 pp.  e cpv.  codice  penale,  &#13;
 per aver sottratto, con la minaccia di un coltello, lire 1.500 ciascuno  &#13;
 ed  un  paio di occhiali Ray-Ban a Bosso Fabio e Caselli Silvano ed una  &#13;
 moto vespa 125 ad Albertello Armando, sollevava con ordinanza emessa in  &#13;
 data  9  dicembre  1975,  questione  di   legittimità   costituzionale  &#13;
 dell'art.   1 della legge 22 maggio 1975, n. 152, recante "Disposizioni  &#13;
 a tutela dell'ordine pubblico" nella  parte  in  cui  non  esclude  dal  &#13;
 divieto  di  concessione della libertà provvisoria i minori degli anni  &#13;
 18. La norma,  così  disponendo,  parificherebbe  situazioni  diverse,  &#13;
 senza tener conto dei caratteri peculiari dei delitti minorili, sovente  &#13;
 rivolti  al  possesso  di  oggetti aventi valore irrisorio, motivati da  &#13;
 ragioni di mero teppismo, attuati con mezzi rudimentali e non idonei  a  &#13;
 suscitare  allarme sociale; senza tener conto inoltre della particolare  &#13;
 disciplina  che  regola  la  materia,  disciplina   che   richiede   la  &#13;
 dimostrazione  in concreto della capacità di intendere e di volere, è  &#13;
 volta innanzi tutto al recupero sociale di chi ha commesso il delitto e  &#13;
 considera in ogni caso la condanna come ipotesi estrema pur in presenza  &#13;
 di una situazione di colpevolezza (istituto del perdono giudiziale).     &#13;
     Nel  giudizio  innanzi a questa Corte interveniva il Presidente del  &#13;
 Consiglio dei ministri attraverso l'Avvocatura dello Stato chiedendo il  &#13;
 rigetto della questione. Si richiamava alle affermazioni  della  Corte,  &#13;
 contenute nella sentenza n. 88 del 1976, secondo cui la norma di cui si  &#13;
 tratta  è  stata  dettata  dal  legislatore  "al  fine di fronteggiare  &#13;
 l'allarmante recrudescenza del fenomeno della  criminalità  successiva  &#13;
 all'emanazione  della  legge  15  dicembre  1972,  n.  733,  e, quindi,  &#13;
 certamente o molto probabilmente favorita  dalla  libertà  provvisoria  &#13;
 accordata ad imputati pericolosi e proclivi alla recidiva", per dedurre  &#13;
 che  le  censure del giudice a quo tenderebbero ad una critica politica  &#13;
 di tale normativa, inammissibile nella  presente  sede.  La  Corte  del  &#13;
 resto avrebbe già chiaramente affermato la compatibilità con le norme  &#13;
 costituzionali  dell'istituto del mandato di cattura obbligatorio e del  &#13;
 divieto  di  concedere  la  libertà   provvisoria   quando   si   può  &#13;
 ragionevolmente   presumere   un   pericolo  derivante  dalla  libertà  &#13;
 dell'indiziato (sent. n. 64 del 1970).<diritto>Considerato in diritto</diritto>:                          &#13;
     La  questione  di  legittimità  costituzionale,   per   violazione  &#13;
 dell'art.  3,  primo  comma,  Cost.,  è  sollevata dal Tribunale per i  &#13;
 minorenni perché l'art. 1, primo comma, della legge 22 maggio 1975, n.  &#13;
 152  disciplinerebbe  in   modo   eguale   situazioni   sostanzialmente  &#13;
 diseguali,  e  cioè  quella  degli  adulti e quella dei minori di età  &#13;
 potenzialmente imputabili, in relazione al diverso significato ed  alla  &#13;
 differente   funzione   che   assumerebbe   per  essi  la  carcerazione  &#13;
 preventiva.                                                              &#13;
     Sembra peraltro che, al  di  la  di  questo  aspetto,  risulterebbe  &#13;
 profondamente  contraddittorio  con tutta la normativa sui minori degli  &#13;
 anni diciotto il rigido automatismo di un divieto  che  precludesse  al  &#13;
 giudice la possibilità di adottare differenziate valutazioni in ordine  &#13;
 all'adozione  o  meno di misure di carcerazione preventiva: in realta a  &#13;
 ferire la coerenza e la razionalità del sistema  sarebbe  l'arbitraria  &#13;
 equiparazione dei minori tra di loro.                                    &#13;
     In  effetti,  secondo  il  disposto  dell'art. 98 codice penale, la  &#13;
 capacità di intendere e di volere del minore tra  i  quattordici  e  i  &#13;
 diciotto  anni  (e  cioè  la sua imputabilità) deve essere verificata  &#13;
 caso per caso in relazione al momento  in  cui  è  stato  commesso  il  &#13;
 fatto.                                                                   &#13;
     In  secondo  luogo  il  largo ricorso alla sospensione condizionale  &#13;
 della  pena  ed  al  perdono  giudiziale  nell'ambito  della  giustizia  &#13;
 minorile  (cfr.  sentenze  Corte cost. nn. 108 del 1973 e 154 del 1976)  &#13;
 conferma non soltanto la tendenza generale a  considerare  come  ultima  &#13;
 ratio  il  ricorso  all'istituzione  carceraria  per  questa  fascia di  &#13;
 minorenni, ma sottolinea con forza  la  necessita  di  valutazioni  del  &#13;
 giudice  fondate su prognosi ovviamente individualizzate in ordine alle  &#13;
 prospettive di recupero del minore deviante.                             &#13;
     Non mancano infine altri dati normativi (ad esempio l'articolo  255  &#13;
 codice  procedura penale) per dimostrare che l'ordinamento italiano non  &#13;
 si è ispirato ad un generico favor per i minori,  ma,  sul  fondamento  &#13;
 dell'art.  31, ultimo comma, Cost. ha provveduto (in particolare con la  &#13;
 legge 25 luglio 1956, n. 888)  a  sviluppare  istituti  e  servizi  che  &#13;
 dovrebbero  rendere residuale l'internamento dei minori nei riformatori  &#13;
 giudiziari  e  nelle  prigioni-scuola.   Ciò   non   comporta   alcuna  &#13;
 sottovalutazione  della  pericolosità  e  gravita  del  fenomeno della  &#13;
 delinquenza  minorile:  ma  significa  solamente  che  non  si  intende  &#13;
 lasciare  intentata  alcuna  possibilità  di  recupero di soggetti non  &#13;
 ancora del tutto maturi dal punto di vista fisiopsichico.                &#13;
     In  ogni  caso,  come  l'art.  13  Cost.  va  letto in collegamento  &#13;
 sistematico oltreché con l'art. 27 Cost. anche  con  l'art.  32  Cost.  &#13;
 (cfr.  l'ultimo  comma  art.  1 legge n. 152 del 1975), così lo stesso  &#13;
 art. 13 non può essere scisso (ai fini della  carcerazione  preventiva  &#13;
 dei minori) da quanto dispone l'ultimo comma dell'art. 31 Cost. in tema  &#13;
 di protezione della gioventù.                                           &#13;
     Tuttavia  il testo dell'art. 1, comma primo, della legge n. 152 del  &#13;
 1975 appare suscettibile di una  interpretazione  che  non  escluda  la  &#13;
 concessione  della libertà provvisoria ai minori compresi nella fascia  &#13;
 tra i quattordici ed i diciotto anni: una interpretazione che è  anche  &#13;
 la  sola ad essere in armonia con l'art. 3, primo comma, Cost. e con le  &#13;
 norme della Costituzione in tema di gioventù e di minori.               &#13;
     Sulla base di queste considerazioni non può dunque accogliersi  la  &#13;
 questione sollevata dal Tribunale per i minorenni di Torino.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata, nei sensi di cui in motivazione, la questione  &#13;
 di  legittimità costituzionale dell'art. 1 della legge 22 maggio 1975,  &#13;
 n. 152, recante "Disposizioni a tutela dell'ordine pubblico" sollevata,  &#13;
 in riferimento all'art.  3  della  Costituzione,  con  l'ordinanza  del  &#13;
 Tribunale dei minorenni di Torino in epigrafe indicata.                  &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, l'11 aprile 1978.                                &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   LEONETTO  AMADEI  -  EDOARDO VOLTERRA  &#13;
                                   GUIDO  ASTUTI   -   MICHELE   ROSSANO  &#13;
                                   ANTONINO DE STEFANO - LEOPOLDO ELIA -  &#13;
                                   GUGLIELMO    ROEHRSSEN   -   BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE.                             &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
