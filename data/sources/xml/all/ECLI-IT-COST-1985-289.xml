<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>289</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:289</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ROEHRSSEN</presidente>
    <relatore_pronuncia>Giuseppe Ferrari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/11/1985</data_decisione>
    <data_deposito>13/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GUGLIELMO ROEHRSSEN, Presidente - Avv. &#13;
 ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. &#13;
 ETTORE GALLO - Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - &#13;
 Dott. FRANCESCO GRECO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 1, 3, 4, 6,  &#13;
 15,  17,  18  e  27  della  legge  28  gennaio 1977, n. 10 (norme sulla  &#13;
 edificabilità dei suoli) e artt. 31 e 41 della legge 17  agosto  1942,  &#13;
 n.  1150  (legge urbanistica) promosso con ordinanza emessa il 14 marzo  &#13;
 1977 dal Pretore di Nardò nel procedimento penale a carico di  Martina  &#13;
 Pasquale,  iscritta al n. 216 del registro ordinanze 1977, e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 162 dell'anno 1977;         &#13;
     visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del  16 aprile 1985 il Giudice  &#13;
 relatore Giuseppe Ferrari.                                               &#13;
     Ritenuto che nel corso del procedimento penale a carico di  persona  &#13;
 tratta  a  giudizio per rispondere del reato di cui agli artt. 31 e 41,  &#13;
 lett. b), della legge 17 agosto 1942, n.  1150 - per aver  iniziato  la  &#13;
 costruzione  di  un'abitazione  senza  aver preventivamente ottenuto la  &#13;
 licenza edilizia - il Pretore di Nardò, con  ordinanza  emessa  il  14  &#13;
 marzo 1977, ha sollevato questione di legittimità costituzionale:       &#13;
     a) degli artt. 18 e 27 (rectius 21) della legge 28 gennaio 1977, n.  &#13;
 10 (Norme sulla edificabilità dei suoli), "in quanto non prevedono una  &#13;
 adeguata  disciplina  transitoria"  (in ordine al reato, permanente, di  &#13;
 costruzione senza licenza o concessione) "per le  costruzioni  iniziate  &#13;
 prima  dell'entrata  in  vigore  della  stessa  legge  e  proseguite ed  &#13;
 ultimate successivamente, in relazione agli artt. 25, secondo comma,  e  &#13;
 3 della Costituzione italiana";                                          &#13;
     b)  degli  artt.  3,  4  e  6  della  stessa  legge,  "in quanto la  &#13;
 'concessione' comporta la corresponsione di un  contributo  commisurato  &#13;
 al  costo  di  costruzione, in relazione agli artt.  53 e 42, secondo e  &#13;
 terzo comma, della Costituzione italiana";                               &#13;
     c) degli artt. 1, 15 e 17 della medesima legge  e  31  e  41  della  &#13;
 legge  17  agosto 1942, n. 1150 (legge urbanistica), "in relazione agli  &#13;
 artt. 3 e 42,  secondo  e  terzo  comma,  e  9,  secondo  comma,  della  &#13;
 Costituzione italiana, per mancanza di precisazione e differenziazione,  &#13;
 penalmente  rilevante,  circa  l'attività  comportante  trasformazione  &#13;
 urbanistica ed edilizia";                                                &#13;
     che  dichiarato  presupposto  della prima delle questioni sollevate  &#13;
 sarebbe, stante l'asserita  natura  permanente  del  reato  contestato,  &#13;
 l'incertezza,  derivante  dalla mancanza di una disciplina transitoria,  &#13;
 sulle norme penali applicabili all'imputato di  una  costruzione  senza  &#13;
 licenza  iniziata  sotto  il vigore della legge urbanistica n. 1150 del  &#13;
 1942 ed ancora in corso di realizzazione (o non ultimata),  al  momento  &#13;
 dell'entrata in vigore della legge n. 10 del 1977;                       &#13;
     Considerato  che,  non emergendo dagli atti processuali elementi di  &#13;
 sorta dai quali possa evincersi che l'imputato continuò  la costruzione  &#13;
 sotto  la  vigenza  della  nuova  legge  e  risultando,   invece,   che  &#13;
 l'attività  costruttiva non fu proseguita dopo l'ordine di sospensione  &#13;
 emesso dal Sindaco di Copertino il 27 marzo 1973, appare  insussistente  &#13;
 lo stesso presupposto da cui muove il giudice a quo;                     &#13;
     che  la  questione  sub  a)  è  manifestamente  inammissibile  per  &#13;
 assoluto  difetto  di  rilevanza  in  quanto:  a)   essendo   principio  &#13;
 giurisprudenziale  consolidato che l'interruzione dell'opera difforme o  &#13;
 priva  di  licenza  (o  concessione)  fa  cessare  la  permanenza,   le  &#13;
 disposizioni  denunciate non sono suscettibili di applicazione nel caso  &#13;
 di specie; b) l'individuazione  della  legge  applicabile  in  caso  di  &#13;
 successione di leggi penali durante la permanenza del reato è comunque  &#13;
 problema ermeneutico, che deve essere risolto dal giudice ordinario pur  &#13;
 in ipotetico difetto di esplicite previsioni normative, la cui semplice  &#13;
 carenza nella legge sopravvenuta non è in sé sicuramente idonea a dar  &#13;
 luogo ad alcuna ipotesi di contrasto con precetti costituzionali;        &#13;
     che  la  questione  indicata sub b), concernendo disposizioni della  &#13;
 stessa  legge  n.  10  del  1977,  è  a   sua   volta   manifestamente  &#13;
 inammissibile per la medesima ragione;                                   &#13;
     che   va  dichiarata  la  manifesta  inammissibilità  anche  della  &#13;
 questione riportata sub c) per assoluto difetto  di  motivazione  sulla  &#13;
 rilevanza, non avendo il Pretore di Nardò in alcun passo della propria  &#13;
 ordinanza  affermato  di dubitare che la realizzazione senza licenza di  &#13;
 una casa d'abitazione priva solo, come nella  specie,  di  intonaco  ed  &#13;
 infissi,  integri  il  reato di cui agli artt. 31 e 41, lett. b), della  &#13;
 legge 17 agosto 1942,  n.  1150,  per  rispondere  del  quale  soltanto  &#13;
 l'imputato era stato tratto a giudizio;                                  &#13;
     visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87  &#13;
 e   9  delle  Norme  integrative  per  i  giudizi  davanti  alla  Corte  &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  manifesta   inammissibilità   delle   questioni   di  &#13;
 legittimità   costituzionale  sollevate  dal  Pretore  di  Nardò  con  &#13;
 l'ordinanza di cui in epigrafe.                                          &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 novembre 1985.       &#13;
                                   F.to:  GUGLIELMO  ROEHRSSEN  - ORONZO  &#13;
                                   REALE - BRUNETTO BUCCIARELLI DUCCI  -  &#13;
                                   ALBERTO  MALAGUGINI - LIVIO PALADIN -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE   BORZELLINO   -   FRANCESCO  &#13;
                                   GRECO.                                 &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
