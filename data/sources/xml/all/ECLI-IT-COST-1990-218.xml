<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>218</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:218</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/04/1990</data_decisione>
    <data_deposito>19/04/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 13 della legge    &#13;
 20 marzo 1975, n.  70  (Disposizioni  sul  riordinamento  degli  enti    &#13;
 pubblici   e  del  rapporto  di  lavoro  del  personale  dipendente);    &#13;
 dell'art. 2 del decreto-legge  7  maggio  1980,  n.  153  (Norme  per    &#13;
 l'attività  gestionale  e  finanziaria  degli enti locali per l'anno    &#13;
 1980), conv. nella legge 7 luglio 1980, n. 299, e dell'art.  4  della    &#13;
 legge  29  maggio  1982,  n.  297 (Disciplina del trattamento di fine    &#13;
 rapporto e norme in materia pensionistica),  promosso  con  ordinanza    &#13;
 emessa  il 20 ottobre 1988 dal Tribunale amministrativo regionale per    &#13;
 il Lazio sul ricorso proposto da Lupini Orazio contro  l'Associazione    &#13;
 della Croce Rossa Italiana, iscritta al n. 654 del registro ordinanze    &#13;
 1989 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  52,    &#13;
 prima serie speciale, dell'anno 1989;                                    &#13;
    Visto  l'atto  di  costituzione di Lupini Orazio nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di consiglio del 21 febbraio 1990 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto  che il Tribunale amministrativo regionale per il Lazio -    &#13;
 nel corso di un giudizio promosso da un ex dipendente  della  C.R.I.,    &#13;
 il  quale  chiedeva  la  riliquidazione dell'indennità di anzianità    &#13;
 tenendo conto, nella base  di  calcolo,  dell'indennità  integrativa    &#13;
 speciale  -  con ordinanza 20 ottobre 1988 (R.O. n. 654 del 1989), ha    &#13;
 sollevato questione di  legittimità  costituzionale  dell'  art.  13    &#13;
 della  legge  20  marzo 1975, n. 70, nella parte in cui non comprende    &#13;
 l'indennità integrativa speciale tra gli emolumenti  computabili  ai    &#13;
 fini  dell'indennità  di anzianità, nonché dell'art. 3 (rectius 2)    &#13;
 del d.-l. 7 maggio 1980, n. 153,  convertito  nella  legge  7  luglio    &#13;
 1980,  n. 299 e dell'art. 4 della legge 29 maggio 1982, n. 297, nella    &#13;
 parte in cui escludono,  nei  confronti  dei  dipendenti  degli  enti    &#13;
 pubblici  di  cui alla legge n. 70 del 1975, la computabilità, nella    &#13;
 misura e con le decorrenze ivi previste, dell'indennità  integrativa    &#13;
 speciale ai fini dell'indennità di anzianità;                          &#13;
    Considerato  che  la  questione  è stata sollevata in riferimento    &#13;
 agli artt. 3 e  36  della  Costituzione,  sotto  il  profilo  che  il    &#13;
 trattamento così fatto ai dipendenti degli enti di cui alla legge n.    &#13;
 70 del 1975 sarebbe discriminatorio e  ingiustificatamente  deteriore    &#13;
 rispetto a quello relativo ai lavoratori privati ed a quelli iscritti    &#13;
 all'I.N.A.D.E.L. e lederebbe il  principio  di  proporzionalità  tra    &#13;
 retribuzione e trattamento di quiescenza;                                &#13;
      che  questa  Corte  ha  già  dichiarato  inammissibili  (e  poi    &#13;
 manifestamente inammissibili), in relazione agli artt. 3 e  36  della    &#13;
 Costituzione,  censurandosi  scelte  riservate  alla discrezionalità    &#13;
 legislativa,   altre   questioni   di   legittimità   costituzionale    &#13;
 riguardanti  la  esclusione  dal  computo, ai fini dell'indennità di    &#13;
 buonuscita dei dipendenti statali e  dei  dipendenti  delle  Ferrovie    &#13;
 dello   Stato,  dell'indennità  integrativa  speciale  (sentenza  n.    &#13;
 220/1988; ordinanze n. 143/1990; n. 419/1989; nn. 641,  869,  1070  e    &#13;
 1072 del 1988);                                                          &#13;
      che,  sulla  base di tale indirizzo, anche la questione in esame    &#13;
 va dichiarata  manifestamente  inammissibile,  essendo  di  esclusiva    &#13;
 competenza  del  legislatore determinare, in relazione alla struttura    &#13;
 complessiva della retribuzione e dei trattamenti di fine  rapporto  e    &#13;
 pensionistici,   nell'ambito   di   ciascun   sistema  retributivo  e    &#13;
 previdenziale,  la  misura  dei  vari  tipi  d'indennità  dovute  al    &#13;
 dipendente   al   termine   del   rapporto  di  lavoro,  non  essendo    &#13;
 comparabile, a detto fine, la posizione  dei  dipendenti  degli  enti    &#13;
 pubblici  di  cui  alla  legge  n. 70/1975, con quella dei dipendenti    &#13;
 privati, né con quella  dei  dipendenti  iscritti  all'I.N.A.D.E.L.,    &#13;
 appartenendo essi a sistemi differenziati;                               &#13;
      che, pertanto, l'omogeneizzazione dei trattamenti è compito del    &#13;
 legislatore, al quale va reiterato il pressante invito  formulato  da    &#13;
 questa Corte nella coeva ordinanza n. 143 del 1990;                      &#13;
    Visti gli artt. 26 della legge 11 marzo 1953 n. 87 e 9 delle Norme    &#13;
 integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 13 della legge 20  marzo  1975,    &#13;
 n.  70  (Disposizioni  sul  riordinamento  degli  enti pubblici e del    &#13;
 rapporto  di  lavoro  del  personale  dipendente);  dell'art.  2  del    &#13;
 decreto-legge 7 maggio 1980, n. 153 (Norme per l'attività gestionale    &#13;
 e finanziaria degli enti locali per l'anno 1980), conv. nella legge 7    &#13;
 luglio  1980,  n. 299; dell'art. 4 della legge 29 maggio 1982, n. 297    &#13;
 (Disciplina del trattamento di  fine  rapporto  e  norme  in  materia    &#13;
 pensionistica),  sollevata  in  riferimento  agli  artt. 3 e 36 della    &#13;
 Costituzione, dal Tribunale amministrativo regionale per il Lazio con    &#13;
 l'ordinanza di cui in epigrafe.                                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 aprile 1990.                                &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 aprile 1990.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
