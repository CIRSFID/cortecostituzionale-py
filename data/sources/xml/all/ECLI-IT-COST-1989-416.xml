<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>416</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:416</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/07/1989</data_decisione>
    <data_deposito>18/07/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  10, comma    &#13;
 secondo,  n.  11,  della  legge  9  ottobre  1971,  n.  825   (Delega    &#13;
 legislativa  al  Governo della Repubblica per la riforma tributaria),    &#13;
 degli artt. 46, 47, 55 e 56 del d.P.R.  29  settembre  1973,  n.  600    &#13;
 (Disposizioni  comuni  in  materia  di accertamento delle imposte sui    &#13;
 redditi) nonché della restante normativa concernente le sanzioni  di    &#13;
 cui  al  titolo V del medesimo decreto, promosso con ordinanza emessa    &#13;
 il 30 luglio 1981 dalla Commissione tributaria di 1° grado di  Savona    &#13;
 sul  ricorso  proposto  da Sambin Stanislao ed altri contro l'Ufficio    &#13;
 Imposte Dirette di Savona iscritta al n. 45  del  registro  ordinanze    &#13;
 1989  e  pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 7,    &#13;
 prima serie speciale, dell'anno 1989;                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 17 maggio 1989 il giudice    &#13;
 relatore Vincenzo Caianiello.                                            &#13;
    Ritenuto  che nel corso di tre giudizi riuniti, proposti da alcuni    &#13;
 contribuenti avverso avvisi di accertamento notificati  dagli  uffici    &#13;
 finanziari per IRPEF e ILOR relative all'anno 1974 e, in uno di essi,    &#13;
 per  obblighi  nascenti  dalla  qualità  di  sostituto  di   imposta    &#13;
 rivestita dal ricorrente, la Commissione tributaria di primo grado di    &#13;
 Savona, con ordinanza emessa il 30 luglio 1981  (pervenuta  a  questa    &#13;
 Corte  il  21  gennaio  1989)  ha sollevato questioni di legittimità    &#13;
 costituzionale: dell'art. 10, comma secondo,  n.  11  della  legge  9    &#13;
 ottobre  1971,  n.  825, in relazione all'art. 76 della Costituzione;    &#13;
 del  "nuovo  ordinamento  sanzionatorio"  nel   suo   complesso   per    &#13;
 violazione  del  c.d. "principio di fissità" stabilito dal capoverso    &#13;
 dell'art. 1 della legge 7 gennaio 1929, n. 4; degli artt.  46,  comma    &#13;
 quarto,  e  56, comma primo, del d.P.R. 29 settembre 1973, n. 600, in    &#13;
 relazione agli artt. 3, 24 e 76 della Costituzione; degli artt. 47  e    &#13;
 55  del medesimo d.P.R. n. 600, in relazione agli artt. 76 e 77 della    &#13;
 Costituzione;  nonché  "della  restante  normativa  concernente   le    &#13;
 sanzioni di cui al titolo V" del medesimo d.P.R.;                        &#13;
      che,  in  particolare,  assunta genericamente la rilevanza delle    &#13;
 questioni in relazione alle controversie sottoposte al suo esame,  il    &#13;
 giudice   rimettente  dubita,  in  primo  luogo,  della  legittimità    &#13;
 costituzionale dell'art. 10, secondo comma, n. 11 della legge  delega    &#13;
 per  la  riforma tributaria n. 825 del 1971 perché, in contrasto con    &#13;
 l'art. 76 della Costituzione, non  avrebbe  indicato  al  legislatore    &#13;
 delegato  sufficienti  principi  e  criteri direttivi per la concreta    &#13;
 determinazione delle sanzioni amministrative e penali e per  la  loro    &#13;
 graduazione   in   relazione   alla  effettiva  entità  oggettiva  e    &#13;
 soggettiva delle violazioni alle leggi tributarie;                       &#13;
      che,  per  quel  che  concerne  la  questione relativa al "nuovo    &#13;
 ordinamento sanzionatorio" nel suo complesso, il giudice a quo assume    &#13;
 che  esso non rispetterebbe il c.d. "principio di fissità" stabilito    &#13;
 dal capoverso dell'art. 1 della legge 7 gennaio 1929, n.  4,  secondo    &#13;
 cui  le  disposizioni  di  tale  legge,  recante norme generali sulla    &#13;
 repressione delle violazioni delle  leggi  finanziarie,  non  possono    &#13;
 essere modificate o abrogate se non in modo espresso;                    &#13;
      che,  in  ordine agli artt. 46, comma quarto, e 56, comma primo,    &#13;
 del d.P.R. n. 600 citato,  che  rispettivamente  recano  la  sanzione    &#13;
 amministrativa   e   quella   penale  per  i  casi  ivi  considerati,    &#13;
 prescindendo  entrambi  dalla  dimostrazione  di   un   comportamento    &#13;
 fraudolento,  doloso  o  colposo,  del  contribuente, si sostiene dal    &#13;
 giudice a quo che essi si porrebbero in contrasto con l'art. 3  della    &#13;
 Costituzione  in quanto non distinguerebbero tra omessa dichiarazione    &#13;
 e dichiarazione infedele nella irrogazione delle rispettive sanzioni;    &#13;
      che,  in  proposito  si  soggiunge  che  la  prima  delle  norme    &#13;
 denunciate (art. 46  cit.),  prevedendo  la  irrogazione  della  pena    &#13;
 pecuniaria  anche nel caso in cui la differenza di reddito tra quello    &#13;
 dichiarato e quello accertato - differenza sulla quale si calcola  in    &#13;
 concreto  la misura della sanzione - dipenda dalla indeducibilità di    &#13;
 spese, passività  ed  oneri,  non  terrebbe  conto  della  difficile    &#13;
 determinazione  di  tali fattori anche a causa delle incertezze della    &#13;
 stessa amministrazione finanziaria nel procedere  agli  accertamenti,    &#13;
 cosicché  la  dichiarazione  diverrebbe  "infedele"  per  effetto di    &#13;
 elementi incerti, con conseguente applicazione anche  della  sanzione    &#13;
 penale di cui all'art. 56, primo comma, dello stesso d.P.R.;             &#13;
      che,  inoltre, si rileva che il mancato adeguamento della misura    &#13;
 massima della imposta evasa, da cui scaturisce la  irrogazione  della    &#13;
 sanzione  penale  per  omessa  o  incompleta o infedele dichiarazione    &#13;
 (art. 56, primo comma, del d.P.R. n. 600), unitamente alla incertezza    &#13;
 degli   uffici   finanziari  in  ordine  alle  detrazioni  possibili,    &#13;
 renderebbe  il  sistema  sanzionatorio  ancor  più  ingiusto,  così    &#13;
 violandosi gli artt. 76 e 24 della Costituzione;                         &#13;
      che,  nell'ordinanza  di  rinvio,  la  questione di legittimità    &#13;
 costituzionale degli artt. 47 e 55 del d.P.R. 29 settembre  1973,  n.    &#13;
 600,  in  riferimento  agli  artt.  76  e  77  della Costituzione, è    &#13;
 menzionata solo nel dispositivo, senza  riscontri  nella  motivazione    &#13;
 sui profili della rilevanza e della non manifesta infondatezza;          &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 concludendo per  la  inammissibilità  o  infondatezza  di  tutte  le    &#13;
 questioni.                                                               &#13;
    Considerato   che  la  questione  di  legittimità  costituzionale    &#13;
 dell'art. 10, comma secondo, n. 11, della legge 9  ottobre  1971,  n.    &#13;
 825,  già  sollevata  in  riferimento allo stesso parametro (art. 76    &#13;
 della Costituzione) e sotto gli  stessi  profili  ora  enunciati,  è    &#13;
 stata già dichiarata non fondata con sentenze nn. 83 del 1989, 111 e    &#13;
 128 del 1986 e manifestamente infondata con sentenza n. 83 del 1989 e    &#13;
 con ordinanza n. 45 del 1988;                                            &#13;
      che,  non essendo dedotti, nell'ordinanza di rimessione, profili    &#13;
 o  argomenti  diversi  da  quelli  già  disattesi  dalla  Corte,  la    &#13;
 questione proposta deve essere dichiarata manifestamente infondata;      &#13;
      che,  quanto alla questione, concernente l'art. 56, comma primo,    &#13;
 del d.P.R. 29 settembre 1973, n. 600), va rilevato che questo prevede    &#13;
 sanzioni   penali   la   cui  applicazione  è  preclusa  al  giudice    &#13;
 rimettente, onde deve essere dichiarata la manifesta inammissibilità    &#13;
 della questione per difetto di rilevanza;                                &#13;
      che  la  questione  di  legittimità costituzionale, relativa al    &#13;
 "nuovo ordinamento sanzionatorio" recato  dal  titolo  V  del  citato    &#13;
 d.P.R.  n.  600  del 1973, sotto il profilo della violazione del c.d.    &#13;
 "principio della fissità"  stabilito  dall'art.  1,  comma  secondo,    &#13;
 della  legge  7  gennaio 1929, n. 4 (Norme generali sulla repressione    &#13;
 della violazione delle leggi finanziarie), articolo peraltro abrogato    &#13;
 espressamente   dall'art.  13  del  D.L.  10  luglio  1982,  n.  429,    &#13;
 convertito con modificazioni nella legge 7 agosto 1982,  n.  516,  è    &#13;
 manifestamente inammissibile, essendo stato assunto come parametro di    &#13;
 riferimento un principio non avente rango costituzionale;                &#13;
      che  manifestamente  inammissibile  è  anche  la  questione  di    &#13;
 legittimità costituzionale degli artt. 47 e 55 del d.P.R. n. 600 del    &#13;
 1973,  in riferimento agli artt. 76 e 77 della Costituzione, perché,    &#13;
 come già precisato nella narrativa,  manca  nell'ordinanza  ad  essa    &#13;
 ogni  motivazione,  essendosi  limitato  il giudice a quo a censurare    &#13;
 tali norme nel dispositivo  con  un  mero  riferimento  ai  parametri    &#13;
 costituzionali (artt. 76 e 77) invocati;                                 &#13;
      che  anche la questione di legittimità costituzionale dell'art.    &#13;
 46, comma quarto, dello stesso d.P.R. n.  600,  in  riferimento  agli    &#13;
 artt.  3  e 76 della Costituzione - sollevata sotto il profilo che la    &#13;
 norma  denunciata   riserverebbe   identico   trattamento   sia   per    &#13;
 l'omissione  che  per  la  infedele  dichiarazione, quest'ultima resa    &#13;
 possibile da  fattori  di  difficile  determinazione  a  causa  delle    &#13;
 incertezze  della  Amministrazione  finanziaria  nelle  operazioni di    &#13;
 accertamento in ordine alla indeducibilità di spese,  passività  ed    &#13;
 oneri  - è manifestamente infondata poiché le due fattispecie poste    &#13;
 a raffronto risultano invece sanzionate in modo diverso (cfr. art. 46    &#13;
 commi primo e quarto);                                                   &#13;
      che, per quanto riguarda la questione, relativa allo stesso art.    &#13;
 46, comma quarto, cit., sollevata in riferimento  all'art.  24  della    &#13;
 Costituzione,  va  chiarito  che  la  normativa  vigente  consente al    &#13;
 contribuente la  scelta  tra  due  alternative,  e  cioè  quella  di    &#13;
 effettuare la dichiarazione e il conseguente versamento dell'imposta,    &#13;
 senza avvalersi della detrazione, chiedendo il rimborso  delle  somme    &#13;
 ritenute  detraibili  e  quindi  non  dovute  e  con  la  conseguente    &#13;
 possibilità in caso di  rifiuto  di  adire  il  giudice  tributario,    &#13;
 oppure  quella  di operare direttamente le detrazioni e di difendersi    &#13;
 in giudizio, qualora esse  vengano  contestate  dall'amministrazione,    &#13;
 sì  che  nel sistema non è ravvisabile la denunciata violazione del    &#13;
 diritto di difesa;.                                                      &#13;
    Visti  gli  artt. 26, comma secondo, della legge 11 marzo 1953, n.    &#13;
 87 e 9, comma secondo, delle Norme integrative per i giudizi  davanti    &#13;
 la Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   1)  dichiara  la  manifesta  inammissibilità  della  questione  di    &#13;
 legittimità costituzionale dell'art. 56, comma primo, del d.P.R.  29    &#13;
 settembre   1973,   n.   600   (Disposizioni  comuni  in  materia  di    &#13;
 accertamento delle imposte sui redditi) sollevata,  in    riferimento    &#13;
 agli artt. 3, 24 e 76 della Costituzione dalla Commissione tributaria    &#13;
 di primo grado di Savona con l'ordinanza indicata in epigrafe;           &#13;
    2)  dichiara  la  manifesta  inammissibilità  della  questione di    &#13;
 legittimità costituzionale del titolo  V  del  d.P.R.  29  settembre    &#13;
 1973,  n.  600,  sollevata  con  la  medesima ordinanza, in relazione    &#13;
 all'art. 1, comma secondo, della legge 7 gennaio 1929,  n.  4  (Norme    &#13;
 generali sulla repressione delle violazioni delle leggi finanziarie);    &#13;
    3)  dichiara  la  manifesta  inammissibilità  della  questione di    &#13;
 legittimità costituzionale  degli  artt.  47  e  55  del  d.P.R.  29    &#13;
 settembre  1973, n. 600, sollevata, in riferimento agli artt. 76 e 77    &#13;
 della Costituzione, con la medesima ordinanza;                           &#13;
    4)   dichiara   la   manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 10, comma secondo, n. 11, della    &#13;
 legge  9  ottobre  1971,  n. 825 (Delega legislativa al Governo della    &#13;
 Repubblica per  la  riforma  tributaria)  sollevata,  in  riferimento    &#13;
 all'art. 76 della Costituzione, con la medesima ordinanza;               &#13;
    5)   dichiara   la   manifesta  infondatezza  della  questione  di    &#13;
 legittimità costituzionale dell'art. 46, comma quarto, del d.P.R. 29    &#13;
 settembre  1973, n. 600, sollevata, in riferimento agli artt. 3, 24 e    &#13;
 76 della Costituzione, con la medesima ordinanza.                        &#13;
    Così  deciso  in  Roma,  nella  Sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 6 luglio 1989.                                &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: DI PAOLA                          &#13;
    Depositata in cancelleria il 18 luglio 1989.                          &#13;
                        Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
