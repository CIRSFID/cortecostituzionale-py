<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>146</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:146</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1989</data_decisione>
    <data_deposito>21/03/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale degli artt. 20, terzo    &#13;
 comma, e 28,  primo  comma,  del  d.P.R.  26  ottobre  1972,  n.  636    &#13;
 (Revisione  della  disciplina  del  sistema  tributario),  nel  testo    &#13;
 novellato dal d.P.R. 3 novembre 1981, n.  739  (Norme  integrative  e    &#13;
 correttive  del  decreto  del  Presidente della Repubblica 26 ottobre    &#13;
 1972,  n.  636,  concernente  la  revisione  della   disparità   del    &#13;
 contenzioso  tributario),  promosso  con ordinanza emessa il 21 marzo    &#13;
 1988 dalla Commissione Tributaria di  primo  grado  di  Verbania  sul    &#13;
 ricorso  proposto  da  Bellezza  Romano ed altri contro l'Ufficio del    &#13;
 Registro di Verbania, iscritta al n. 356 del registro ordinanze  1988    &#13;
 e  pubblicata  nella Gazzetta Ufficiale della Repubblica n. 31, prima    &#13;
 serie speciale, dell'anno 1988.                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nell'udienza  pubblica  del  29  novembre  1988  il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Udito  l'Avvocato  dello Stato Gaetano Zotta per il Presidente del    &#13;
 Consiglio dei Ministri.                                                  &#13;
    Ritenuto  che,  con ordinanza emessa il 21 marzo 1988 (R.O. n. 356    &#13;
 del 1988) nel procedimento  promosso  da  Bellezza  Romano  ed  altri    &#13;
 contro  l'Ufficio  del  Registro di Verbania in punto di accertamento    &#13;
 dell'imposta di registro e del valore finale di beni immobili oggetto    &#13;
 di  compravendita,  la  Commissione  Tributaria  di  primo  grado  di    &#13;
 Verbania ha sollevato questione di legittimità costituzionale  degli    &#13;
 artt. 20, terzo comma, e 28, primo comma, del d.P.R. 26 ottobre 1972,    &#13;
 n. 636, nel testo novellato dal d.P.R. 3 novembre 1981, n. 739, nella    &#13;
 parte  in  cui  non  prevedono,  per  i  componenti delle Commissioni    &#13;
 tributarie l'obbligo del segreto sulla camera  di  consiglio  ed,  in    &#13;
 particolare, sul processo di formazione della decisione del collegio,    &#13;
 in riferimento agli artt. 108, secondo comma, e 3, primo comma, della    &#13;
 Costituzione,   in   quanto  risulterebbe  leso  il  principio  della    &#13;
 indipendenza  dei  giudici  delle   giurisdizioni   speciali   e   si    &#13;
 verificherebbe  disparità  di trattamento nei confronti di tutti gli    &#13;
 altri giudici per i quali vige il rispetto del segreto di  camera  di    &#13;
 consiglio;                                                               &#13;
      che   l'Avvocatura   Generale   dello   Stato,   intervenuta  in    &#13;
 rappresentanza del Presidente del Consiglio dei ministri, ha concluso    &#13;
 per  la  restituzione  degli  atti  alla Commissione remittente o per    &#13;
 l'infondatezza della questione.                                          &#13;
    Considerato  che,  come  ha  rilevato  l'Avvocatura Generale dello    &#13;
 Stato, la segretezza della camera di consiglio è stata  disciplinata    &#13;
 ex  novo  dall'art. 16, secondo comma, della legge 13 aprile 1988, n.    &#13;
 117, (Risarcimento dei danni cagionati nell'esercizio delle  funzioni    &#13;
 giudiziarie e responsabilità civile dei magistrati).                    &#13;
      Che  dette norme si applicano anche ai provvedimenti dei giudici    &#13;
 collegiali aventi giurisdizione in ogni "altra" materia (terzo comma,    &#13;
 art. 16) e, quindi, anche alle Commissioni Tributarie;                   &#13;
      che,  pertanto,  la  rilevanza  della  questione va esaminata di    &#13;
 nuovo alla stregua delle nuove norme.                                    &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle norme integrative dei  giudizi  dinanzi  alla    &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Ordina  la  restituzione  degli atti alla Commissione Tributaria di    &#13;
 primo grado di Verbania.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'8 marzo 1989.                                  &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 marzo 1989.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
