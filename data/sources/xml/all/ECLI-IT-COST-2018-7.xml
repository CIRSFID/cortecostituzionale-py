<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2018</anno_pronuncia>
    <numero_pronuncia>7</numero_pronuncia>
    <ecli>ECLI:IT:COST:2018:7</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GROSSI</presidente>
    <relatore_pronuncia>Giorgio Lattanzi</relatore_pronuncia>
    <redattore_pronuncia>Giorgio Lattanzi</redattore_pronuncia>
    <data_decisione>22/11/2017</data_decisione>
    <data_deposito>18/01/2018</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Paolo GROSSI; Giudici : Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO, Silvana SCIARRA, Daria de PRETIS, Nicolò ZANON, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 552, comma 1, lettera f), del codice di procedura penale, promossi dal Tribunale ordinario di Spoleto, nel procedimento penale a carico di M. S. e altri, con ordinanza del 23 febbraio 2016, e dal Tribunale ordinario di Pistoia, nel procedimento penale a carico di K. Z. e altro, con ordinanza del 4 novembre 2016, rispettivamente iscritte al n. 204 del registro ordinanze 2016 e al n. 28 del registro ordinanze 2017 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 42, prima serie speciale, dell'anno 2016 e n. 10, prima serie speciale, dell'anno 2017. &#13;
 Visti gli atti di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 22 novembre 2017 il Giudice relatore Giorgio Lattanzi.&#13;
 Ritenuto che il Tribunale ordinario di Spoleto, in composizione monocratica, con ordinanza del 23 febbraio 2016 (r.o. n. 204 del 2016), ha sollevato, in riferimento agli artt. 3, 24 e 111 della Costituzione, questioni di legittimità costituzionale dell'art. 552, comma 1, lettera f), del codice di procedura penale, «nella parte in cui non prevede che all'[i]mputato venga dato [a]vviso anche della facoltà di richiedere tempestivamente la sospensione del procedimento con messa alla prova ex art. 464 bis C.p.p.»;&#13;
 che il giudice a quo premette di essere investito del procedimento penale nei confronti di tre persone imputate dei reati di cui agli artt. 44, comma 1, lettera b), 71 e 95 del d.P.R. 6 giugno 2001, n. 380 (Testo unico delle disposizioni legislative e regolamentari in materia edilizia - Testo A) «come meglio descritti e circostanziati nel Decreto di citazione a giudizio del 13.8.2015»;&#13;
 che nell'udienza del 2 febbraio 2016 il difensore di uno degli imputati ha chiesto che fossero sollevate questioni di legittimità costituzionale dell'art. 552, comma 1, lettera f), cod. proc. pen., in relazione agli artt. 3, 24 e 111 Cost., «nella parte in cui [...] non prevede che nel Decreto di citazione a giudizio venga dato [a]vviso all'imputato della facoltà di richiedere, fra l'altro, di avvalersi dell'istituto della [m]essa alla [p]rova ex art. 464 bis cpp prima della dichiarazione di apertura del dibattimento»;&#13;
 che secondo il giudice a quo le questioni sarebbero rilevanti, perché, se accolte, comporterebbero la declaratoria di nullità del decreto di citazione a giudizio, e non manifestamente infondate;&#13;
 che l'istituto della messa alla prova integrerebbe un nuovo procedimento speciale, alternativo al dibattimento, la cui richiesta deve essere formulata «entro limiti temporali e processuali ben definiti e tali da rendere immediatamente inammissibile la stessa ove tardivamente proposta»;&#13;
 che l'art. 552, comma 1, lettera f), cod. proc. pen. prescrive, a pena di nullità, che il decreto di citazione a giudizio contenga «l'avviso che, qualora ne ricorrano i presupposti, l'imputato, prima della dichiarazione di apertura del dibattimento di primo grado, può presentare le richieste previste dagli articoli 438 e 444 ovvero presentare domanda di oblazione»;&#13;
 che tale avviso dovrebbe «necessariamente far riferimento anche alla facoltà di formulare richiesta di sospensione del procedimento con messa alla prova sulla base del nuovo art. 464 bis Cpp»;&#13;
 che, in caso contrario, sarebbe «menomata [...] l'effettività del diritto alla [d]ifesa dell'[i]mputato costituzionalmente garantito»;&#13;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, e ha chiesto che le questioni siano dichiarate inammissibili e, comunque, non fondate;&#13;
 che le questioni sarebbero inammissibili, in quanto «non è dato sapere, dal tenore del provvedimento del Giudice, se l'udienza del 2.2.2016 fosse la prima udienza e se fosse stato già dichiarato aperto il dibattimento»;&#13;
 che inoltre secondo l'Avvocatura dello Stato «la fattispecie di cui è questione [...] è [...] sostanzialmente diversa» da quella oggetto della sentenza n. 201 del 2016 di questa Corte, perché il termine ultimo per presentare la richiesta di sospensione del procedimento con messa alla prova coincide con la dichiarazione di apertura del dibattimento di primo grado, «cadendo, pertanto, nei preliminari della udienza dibattimentale, a partecipazione necessaria, nel corso della quale l'imputato è obbligatoriamente assistito dal difensore»;&#13;
 che il Tribunale ordinario di Pistoia, in composizione monocratica, con ordinanza del 4 novembre 2016 (r.o. n. 28 del 2017), ha sollevato, in riferimento agli artt. 3 e 24 Cost., questioni di legittimità costituzionale dell'art. 552, comma 1, lettera f), cod. proc. pen.;&#13;
 che il giudice a quo premette che gli imputati sono stati citati a giudizio per rispondere del reato di cui all'art. 6, comma 3, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), «per non aver fornito documenti ad un controllo dei CC nell'ottobre 2013»;&#13;
 che il difensore degli imputati ha chiesto che fossero sollevate questioni di legittimità costituzionale dell'art. 552, comma 1, lettera f), cod. proc. pen., in relazione agli artt. 3 e 24 Cost., nella parte in cui non prevede che il decreto di citazione a giudizio «contenga anche il richiamo alla possibilità di aderire alla richiesta di messa alla prova»;&#13;
 che secondo il giudice a quo le questioni sarebbero rilevanti perché «dal decreto di citazione a giudizio contenuto nel fascicolo emerge l'assenza, nel caso di specie, dell'avviso di poter richiedere il nuovo istituto della messa alla prova, istituto ammissibile perché si verte in uno dei casi di cui all'art. 168 bis cp.»;&#13;
 che, insomma, «a fronte dell'astratta possibilità per l'imputato di accedere all'istituto della messa alla prova», non gli è stato dato alcun avviso in tal senso, in violazione del diritto di difesa costituzionalmente garantito;&#13;
 che le questioni sarebbero altresì non manifestamente infondate in riferimento al principio di uguaglianza sancito dall'art. 3 Cost.;&#13;
 che l'art. 168-bis del codice penale ha riconosciuto la possibilità di sospensione del procedimento con messa alla prova, «con possibile esito positivo di estinzione del reato, esattamente come nel caso di oblazione»;&#13;
 che nel rispetto del principio di uguaglianza dovrebbe pertanto essere previsto «che all'imputato sia dato avviso della possibilità di chiedere la messa alla prova esattamente allo stesso modo in cui gli è dato, dalla legge nell'attuale formulazione, avviso della possibilità di accedere all'oblazione», anche perché per entrambi gli istituti sono previsti «lo stesso termine di decadenza e [...] una serie di attività extra-processuali effettuate personalmente dall'imputato o a mezzo di Procuratore speciale»;&#13;
 che la norma censurata violerebbe, inoltre, l'art. 24 Cost., in quanto «il diritto di difesa impone la conoscenza, da parte dell'imputato, delle sanzioni in cui può incorrere e di tutti i mezzi processuali di cui può disporre, conoscenza [...] impossibile, mancando appunto l'avviso su uno degli istituti che permettono l'estinzione del reato»;&#13;
 che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, e ha chiesto che le questioni siano dichiarate inammissibili e, comunque, non fondate;&#13;
 che le questioni sarebbero inammissibili, in quanto «non è dato sapere, dal tenore del provvedimento del Giudice, se nel momento in cui ha adottato l'ordinanza di cui è questione si fosse nella prima udienza e se fosse stato già dichiarato aperto il dibattimento»;&#13;
 che il giudice rimettente, inoltre, avrebbe potuto interpretare «la normativa ritenendo eventualmente sussistente la necessità di una rimessione degli atti al P.M. [...] anche in considerazione di quanto previsto dall'art. 141 bis, disp. att. c.p.p.»;&#13;
 che «la fattispecie di cui è questione [...] è [...] sostanzialmente diversa» da quella oggetto della sentenza n. 201 del 2016 di questa Corte, perché il termine ultimo per presentare la richiesta di sospensione del procedimento con messa alla prova coincide con la dichiarazione di apertura del dibattimento di primo grado, «cadendo, pertanto, nei preliminari della udienza dibattimentale, a partecipazione necessaria, nel corso della quale l'imputato è obbligatoriamente assistito dal difensore».&#13;
 Considerato che il Tribunale ordinario di Spoleto e il Tribunale ordinario di Pistoia hanno sollevato, in riferimento complessivamente agli artt. 3, 24 e 111 della Costituzione, questioni di legittimità costituzionale dell'art. 552, comma 1, lettera f), del codice di procedura penale, nella parte in cui non prevede che all'imputato venga dato avviso anche della facoltà di richiedere tempestivamente la sospensione del procedimento con messa alla prova ex art. 464-bis cod. proc. pen.;&#13;
 che le questioni sollevate dai due indicati tribunali sono analoghe e devono perciò essere riunite;&#13;
 che l'ordinanza di rimessione del Tribunale ordinario di Spoleto non contiene alcuna descrizione dei fatti oggetto del giudizio a quo, limitandosi ad indicare, con il solo numero, le disposizioni che prevedono i reati contestati agli imputati, «come meglio descritti e circostanziati nel Decreto di citazione a giudizio del 13.8.2015», senza neppure riportare i relativi capi di imputazione;&#13;
 che sotto questo aspetto le questioni sollevate da tale giudice sono manifestamente inammissibili per omessa descrizione della fattispecie concreta e conseguente difetto di motivazione sulla rilevanza (ex multis, ordinanze n. 210 e n. 46 del 2017);&#13;
 che, inoltre, come è stato rilevato dall'Avvocatura dello Stato, entrambe le ordinanze di rimessione non hanno specificato se nell'udienza in cui sono state sollevate le questioni di legittimità costituzionale fosse già stata dichiarata l'apertura del dibattimento e se gli imputati avessero manifestato la volontà di richiedere la sospensione del procedimento con messa alla prova; &#13;
 che ai sensi dell'art. 182, comma 1, cod. proc. pen. la nullità del decreto di citazione a giudizio non può essere eccepita da chi non ha interesse all'osservanza della disposizione violata;&#13;
 che i rimettenti avrebbero dovuto precisare se era avvenuta l'apertura del dibattimento, che avrebbe precluso agli imputati la possibilità di chiedere la sospensione del procedimento con messa alla prova;&#13;
 che vi sono elementi per ritenere che in entrambi i casi tale apertura non fosse ancora avvenuta;&#13;
 che solo l'imputato nei cui confronti si sia verificata la preclusione conseguente all'apertura del dibattimento, e che abbia l'intenzione di chiedere la sospensione del procedimento con messa alla prova, può aver interesse alla declaratoria di nullità del decreto di citazione a giudizio che non contenga l'avvertimento relativo a tale facoltà;&#13;
 che l'insufficiente descrizione della fattispecie processuale, e in particolare dello stato in cui si trovava il giudizio, impedisce il necessario controllo in punto di rilevanza e rende le questioni manifestamente inammissibili (ex multis, ordinanze n. 210 del 2017 e n. 237 del 2016).&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 1, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 riuniti i giudizi,&#13;
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'art. 552, comma 1, lettera f), del codice di procedura penale, sollevate, in riferimento agli artt. 3, 24 e 111 della Costituzione, dal Tribunale ordinario di Spoleto e dal Tribunale ordinario di Pistoia, con le ordinanze indicate in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 22 novembre 2017.&#13;
 F.to:&#13;
 Paolo GROSSI, Presidente&#13;
 Giorgio LATTANZI, Redattore&#13;
 Roberto MILANA, Cancelliere&#13;
 Depositata in Cancelleria il 18 gennaio 2018.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
