<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>863</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:863</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/07/1988</data_decisione>
    <data_deposito>21/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 5 della legge 8    &#13;
 dicembre 1956, n. 1378 (Esami di Stato di abilitazione  all'esercizio    &#13;
 delle  professioni),  come  sostituito  dall'art.  1  della  legge 31    &#13;
 dicembre 1962, n. 1866 (Modifiche all'art. 5 della legge  8  dicembre    &#13;
 1956,  n.  1378), promosso con ordinanza emessa il 27 giugno 1987 dal    &#13;
 Pretore  di  Napoli  nel  procedimento  civile  vertente  tra   Amato    &#13;
 Salvatore  e  il  Ministero di Grazia e Giustizia, iscritta al n. 544    &#13;
 del registro ordinanze 1987 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 44, prima serie speciale, dell'anno 1987;            &#13;
    Visti  l'atto di costituzione di Amato Salvatore nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che, con ordinanza in data 27 giugno 1987, il Pretore di    &#13;
 Napoli   ha   sollevato   questione   incidentale   di   legittimità    &#13;
 costituzionale dell'art. 5 della legge 8 dicembre 1956, n. 1378, come    &#13;
 sostituito dall'art. 1 della legge 31 dicembre 1962, n.  1866,  nella    &#13;
 parte  in  cui,  per  i  componenti  delle  commissioni  di esami per    &#13;
 l'abilitazione all'esercizio della professione di procuratore legale,    &#13;
 non si prevede lo stesso compenso di quello previsto per i componenti    &#13;
 delle commissioni di esami per l'abilitazione all'esercizio di  altre    &#13;
 professioni;  e  ciò  per  preteso  contrasto con gli artt. 3, primo    &#13;
 comma, 33, quinto comma e 36, primo comma, della Costituzione;           &#13;
      che,  a parte la diversità delle prestazioni rese nelle diverse    &#13;
 commissioni  di  esame  di  abilitazione  all'esercizio  delle  varie    &#13;
 professioni,  la  valutazione degli apporti dei componenti è rimessa    &#13;
 alla discrezionalità del legislatore, che rimane  insindacabile  nel    &#13;
 giudizio di costituzionalità siccome non degrada in arbitrio;           &#13;
      che,  per  quanto la censura riferita all'art. 33, quinto comma,    &#13;
 Cost., anzitutto manca in modo assoluto una specifica motivazione sul    &#13;
 punto  ed,  inoltre,  non è dato vedere altrimenti il modo in cui la    &#13;
 norma  costituzionale  invocata   possa   risultare   violata   dalla    &#13;
 determinazione  dell'entità  del  compenso  spettante  ai componenti    &#13;
 delle commissioni per  gli  esami  per  l'abilitazione  all'esercizio    &#13;
 professionale;                                                           &#13;
      che  non  sussiste  la  lamentata violazione dell'art. 36, primo    &#13;
 comma,  Cost.,  in  quanto  il  compenso  spettante   ai   componenti    &#13;
 costituisce  solo  una  parte  accessoria della complessiva posizione    &#13;
 reddituale di tali soggetti, dato il carattere episodico, eventuale e    &#13;
 temporalmente limitato, della prestazione retribuita;                    &#13;
      che,   conseguentemente,   la  proposta  questione  deve  essere    &#13;
 dichiarata manifestamente infondata;                                     &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 5, legge 8 dicembre 1956, n. 1378 (Esami  di    &#13;
 Stato   di   abilitazione   all'esercizio  delle  professioni),  come    &#13;
 sostituito dall'art. 1, legge 31 dicembre 1962,  n.  1866  (Modifiche    &#13;
 all'art.  5  della  legge  8  dicembre 1956, n. 1378), sollevata, con    &#13;
 riferimento agli artt. 3, primo comma, 33, quinto comma, e 36,  primo    &#13;
 comma,  della  Costituzione, dal Pretore di Napoli con l'ordinanza in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 luglio 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 21 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
