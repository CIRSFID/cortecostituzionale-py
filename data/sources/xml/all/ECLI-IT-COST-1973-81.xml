<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>81</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:81</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/06/1973</data_decisione>
    <data_deposito>19/06/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio promosso con  ricorso  del  Presidente  della  Regione  &#13;
 siciliana, notificato il 9 giugno 1972, depositato in cancelleria il 22  &#13;
 successivo  ed  iscritto  al  n.  19  del  registro conflitti 1972, per  &#13;
 conflitto di attribuzione sorto a seguito della nota 30 dicembre  1971,  &#13;
 n. 12/3507/71, del Ministero delle finanze, con la quale si afferma che  &#13;
 alla  Regione  non spettano le ritenute erariali operate sui redditi di  &#13;
 categoria C/2 dei dipendenti dello Stato e degli enti  parastatali  con  &#13;
 sede centrale fuori del territorio della Regione, che prestino servizio  &#13;
 in Sicilia.                                                              &#13;
     Visto  l'atto  di  costituzione  del  Presidente  del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 21 marzo 1973 il  Giudice  relatore  &#13;
 Giuseppe Verzì;                                                         &#13;
     uditi  l'avv.  Antonio  Sorrentino, per la Regione siciliana, ed il  &#13;
 sostituto avvocato  generale  dello  Stato  Michele  Savarese,  per  il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  atto  notificato  il 9 giugno 1972 il Presidente della Regione  &#13;
 siciliana, regolarmente autorizzato dalla Giunta, ha  proposto  ricorso  &#13;
 per la risoluzione del conflitto di attribuzione sorto tra la Regione e  &#13;
 lo  Stato  in  seguito  alla  nota 30 dicembre 1971, n. 12/3507/71, del  &#13;
 Ministero delle finanze con la quale si comunica che alla  Regione  non  &#13;
 spettano  le  ritenute  erariali  operate  sui redditi di categoria C/2  &#13;
 (ricchezza mobile, complementare,  addizionali  varie)  dei  dipendenti  &#13;
 dello  Stato  e  degli  enti  parastatali  con  sede centrale fuori del  &#13;
 territorio della Regione, che prestino servizio in Sicilia.              &#13;
     Nel menzionato atto, dopo aver fatto  presente  che  la  Presidenza  &#13;
 regionale ha avuto conoscenza della decisione ministeriale il 14 aprile  &#13;
 1972,  la  Regione denunzia la sopra indicata nota 30 dicembre 1971, n.  &#13;
 12/3507/71, per violazione dell'art. 36 dello Statuto speciale  per  la  &#13;
 Sicilia e degli artt. 2 e 4 del d.P.R. 26 luglio 1965, n. 1074.          &#13;
     Nel  presente giudizio si è costituito il Presidente del Consiglio  &#13;
 dei ministri per resistere al ricorso.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il conflitto  di  attribuzione  promosso  dalla  Regione  siciliana  &#13;
 riguarda  la  spettanza  alla  Regione oppure allo Stato delle ritenute  &#13;
 erariali sui redditi di categoria C/2  dei  dipendenti  dello  Stato  e  &#13;
 degli   enti   parastatali  con  sede  centrale  fuori  del  territorio  &#13;
 regionale, i quali prestino servizio in Sicilia. La  Regione  fonda  la  &#13;
 sua  pretesa  sul disposto degli artt. 2 e 4 del d.P.R. 26 luglio 1965,  &#13;
 n. 1074, i quali le attribuirebbero sia le entrate tributarie  erariali  &#13;
 riscosse  nell'ambito  del  suo  territorio,  sia le entrate relative a  &#13;
 fattispecie  maturate  nell'ambito  regionale,  che   affluiscono   per  &#13;
 esigenze  amministrative  ad  uffici situati fuori del territorio della  &#13;
 Regione.                                                                 &#13;
     Con sentenza n. 71 del corrente anno  questa  Corte,  interpretando  &#13;
 gli  artt.  2  e  4  del  suindicato  decreto  contenente  le  norme di  &#13;
 attuazione  dello  Statuto   della   Regione   siciliana   in   materia  &#13;
 finanziaria,  è  giunta  alla  conclusione  che  alla Regione spettano  &#13;
 quelle entrate tributarie, e solo quelle, che essa avrebbe  potuto  far  &#13;
 proprie  ove, nel rispetto dei principi fondamentali della legislazione  &#13;
 statale, fosse stata esercitata la competenza  legislativa  concorrente  &#13;
 assegnata  alla  Regione  dal  primo  comma dell'art. 36 dello Statuto.  &#13;
 Sulla base di questa interpretazione, la Corte è giunta alla ulteriore  &#13;
 conclusione che la disposizione dell'art. 4 delle norme  di  attuazione  &#13;
 non  ha  altra  funzione che quella di salvaguardare la regola generale  &#13;
 enunciata nell'art. 2, secondo la quale alla  Regione  spettano  (salvo  &#13;
 alcune  eccezioni  qui  non  rilevanti)  tutte  le  entrate  tributarie  &#13;
 riscosse nel suo territorio;  e  ciò  nel  senso  che  l'affluenza  di  &#13;
 entrate  tributarie  ad  uffici situati fuori del territorio regionale,  &#13;
 disposta per esigenze amministrative, non incide sulla  loro  spettanza  &#13;
 determinata in base alle regole cui si è fatto cenno.                   &#13;
     Ciò  posto, è sufficiente, per la decisione del ricorso in esame,  &#13;
 osservare che le ritenute erariali sui redditi  di  categoria  C/2  dei  &#13;
 dipendenti  statali  e dei dipendenti di enti parastatali costituiscono  &#13;
 un sistema che la Regione siciliana, se avesse  legiferato  nei  limiti  &#13;
 consentiti  dall'art.  36 dello Statuto, non avrebbe potuto modificare.  &#13;
 Non si tratta infatti di entrate la cui riscossione  avrebbe  dovuto  o  &#13;
 potuto  aver  luogo  nel  territorio regionale siciliano e che solo per  &#13;
 "esigenze amministrative" affluiscono ad uffici situati fuori  di  quel  &#13;
 territorio; ci si trova al contrario di fronte ad un principio generale  &#13;
 della  legislazione  statale,  in  forza  del  quale le amministrazioni  &#13;
 interessate debbono operare determinate  ritenute  e  corrispondere  le  &#13;
 retribuzioni  al  netto di esse. E, come in sede legislativa la Regione  &#13;
 non avrebbe potuto non rispettare  siffatto  principio,  così  non  ha  &#13;
 diritto,  nel quadro del sistema delineato nelle norme di attuazione, a  &#13;
 pretendere  entrate  tributarie  che   una   corretta   interpretazione  &#13;
 dell'art.   36   dello   Statuto,  e  di  quelle  norme,  ad  essa  non  &#13;
 attribuiscono.                                                           &#13;
     Una ulteriore conferma della validità di siffatta  conclusione  si  &#13;
 ricava  per  altro dall'art. 7 del decreto n.  1074, e, in particolare,  &#13;
 dalla disposizione contenuta nel secondo comma di tale articolo.  Nella  &#13;
 già  menzionata  decisione  n. 71 è stato messo in luce che l'art. 37  &#13;
 dello Statuto, lungi dall'esprimere un principio generale, attribuisce,  &#13;
 in via eccezionale, alla Regione  una  quota  di  imposta  sui  redditi  &#13;
 prodotti  da  imprese  aventi sede fuori del territorio, commisurata al  &#13;
 reddito prodotto  da  stabilimenti  ed  impianti  situati  in  Sicilia,  &#13;
 estendendo  in questo modo le entrate tributarie spettanti alla Regione  &#13;
 al di là di quanto l'art. 36 Stat. consenta. Orbene, il citato art. 37  &#13;
 Stat. che predispone gli strumenti procedurali attraverso  i  quali  si  &#13;
 perviene   alla  individuazione  della  prescritta  quota,  concorre  a  &#13;
 dimostrare  che  le  stesse  norme  di  attuazione  hanno  inteso  come  &#13;
 eccezionale  il  disposto  dell'art. 37 Stat.; ed il secondo comma, che  &#13;
 coerentemente attribuisce alla Regione  i  corrispondenti  tributi  sui  &#13;
 redditi  di lavoro, prova che, al di fuori della materia attinente alle  &#13;
 imprese con stabilimenti ed impianti in Sicilia, e che abbiano la  loro  &#13;
 sede  in  altra  parte  del  territorio  nazionale,  non esistono altre  &#13;
 ipotesi nelle quali sia possibile identificare tributi che, riscossi  e  &#13;
 ritenuti  da  enti non aventi sede nella Regione, siano di spettanza di  &#13;
 questa ultima.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara che spettano allo Stato, e non alla Regione siciliana,  le  &#13;
 ritenute  erariali  sui  redditi  di categoria C/2 (ricchezza mobile ed  &#13;
 imposta  complementare)  dei  dipendenti  dello  Stato  e  degli   enti  &#13;
 parastatali  con  sede  fuori  del  territorio  della  Regione, i quali  &#13;
 prestino servizio in Sicilia.                                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 12 giugno 1973.                               &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
