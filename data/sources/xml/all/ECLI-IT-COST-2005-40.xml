<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>40</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:40</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONTRI</presidente>
    <relatore_pronuncia>Alfio Finocchiaro</relatore_pronuncia>
    <redattore_pronuncia>Alfio Finocchiaro</redattore_pronuncia>
    <data_decisione>12/01/2005</data_decisione>
    <data_deposito>27/01/2005</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Fernanda CONTRI; Giudici: Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli articoli 1, commi 1 e 2, e 66, commi 3 e 7, della legge della Regione Veneto 7 novembre 2003, n. 27 (Disposizioni generali in materia di lavori pubblici di interesse regionale e per le costruzioni in zone classificate sismiche), promosso con ricorso del Presidente del Consiglio dei ministri, notificato il 9 gennaio 2004, depositato in Cancelleria il 19 successivo ed iscritto al n. 5 del registro ricorsi 2004. &#13;
    Visto l'atto di costituzione della Regione Veneto; &#13;
    udito nell'udienza pubblica del 14 dicembre 2004 il Giudice relatore Alfio Finocchiaro; &#13;
    uditi l'avvocato dello Stato Giacomo Aiello per il Presidente del Consiglio dei ministri e l'avvocato Sergio Panunzio per la Regione Veneto. &#13;
    Ritenuto che, con ricorso depositato il 19 gennaio 2004, il Presidente del Consiglio dei ministri ha chiesto dichiararsi l'illegittimità costituzionale degli articoli 1, commi 1 e 2, e 66, commi 3 e 7, della legge della Regione Veneto 7 novembre 2003, n. 27 (Disposizioni generali in materia di lavori pubblici di interesse regionale e per le costruzioni in zone classificate sismiche) in relazione all'articolo 117 della Costituzione; &#13;
    che, secondo il ricorrente, la legge impugnata, dettando la disciplina generale in tema di lavori pubblici di interesse regionale, stabilisce che la vigente normativa statale in tema di lavori pubblici può trovare applicazione solo per quanto non diversamente disciplinato dalle successive disposizioni della legge stessa (art. 1, comma 2), e che l'autorizzazione prevista dalla legge statale per la costruzione in zona sismica (art. 18 della legge 2 febbraio 1974, n. 64, “Provvedimenti per le costruzioni con particolari prescrizioni per le zone sismiche”) può essere sostituita dal rilascio da parte del comune competente dell'attestazione dell'avvenuto deposito della certificazione di rispondenza della costruzione alle norme sismiche; &#13;
    che la materia dei lavori pubblici non appartiene alla competenza regionale esclusiva ma a quella ripartita e, in particolare, il regime degli interventi edilizi va ricondotto alla materia dell'urbanistica, da ritenersi compresa in quella più ampia del “governo del territorio”; &#13;
    che, secondo il ricorrente, l'art. 66, commi 3 e 7, della legge regionale impugnata, disponendo che l'attestato deposito presso il comune del progetto di lavori e delle inerenti relazioni costituisce autorizzazione implicita all'inizio dei lavori, si pone in contrasto con i limiti posti dall'articolo 117 della Costituzione, per violazione dei principî fondamentali della legislazione statale in tema di “governo del territorio” e di “protezione civile”, nonché della competenza esclusiva dello Stato con riferimento all'”ordinamento civile” e al livello di prestazioni concernenti i diritti civili e sociali da garantirsi pariteticamente su tutto il territorio nazionale (rispettivamente lettere l ed m del secondo comma dell'art. 117 della Costituzione); &#13;
    che, inoltre, viene sovvertito il principio posto dall'art. 18 della legge n. 64 del 1974 di analitico e specifico riscontro di ciascun progetto edilizio in zone sismiche alle prescrizioni tecniche stabilite dalla normativa statale; &#13;
    che la Regione Veneto si è costituita con memoria difensiva del 5 febbraio 2004, chiedendo che il ricorso sia dichiarato inammissibile – per difetto della determinazione governativa all'impugnazione dell'art. 1 della legge regionale impugnata e per difetto di interesse, perché tale norma non ha un valore prescrittivo-normativo – e comunque infondato perché, come affermato dalla sentenza n. 303 del 2003, i lavori pubblici di interesse regionale sono di competenza della regione; &#13;
    che, inoltre, le norme impugnate non riguardano la materia dei lavori pubblici ma le costruzioni in zone sismiche, come risulta dallo stesso titolo della legge che distingue chiaramente tra “disposizioni generali in materia di lavori pubblici”, che occupano gli artt. da 1 a 64, e le disposizioni riguardanti “le costruzioni in zone classificate sismiche”, disciplinate nella parte seguente della legge; &#13;
    che, infine, secondo la difesa regionale, la legge impugnata ha semplicemente dato attuazione all'art. 20 della legge 4 dicembre 1981, n. 741 (Ulteriori norme per l'accelerazione delle procedure per l'esecuzione di opere pubbliche), secondo cui “le Regioni possono definire con legge modalità di controllo successivo anche con metodo a campione; in tal caso possono prevedere che l'autorizzazione preventiva di cui all'art. 18 legge n. 64 del 1974 non sia necessaria per l'inizio dei lavori”. &#13;
    Considerato che in data 23 agosto 2004 l'Avvocatura generale dello Stato, ha depositato un atto, con il quale, premesso che con successiva legge regionale 21 maggio 2004, n. 13 (Modifica della legge regionale 7 novembre 2003, n. 27) la Regione Veneto ha abrogato la normativa impugnata, ha dichiarato di rinunciare al ricorso, a seguito di delibera del Consiglio dei ministri; &#13;
    che, con atto depositato il 26 ottobre 2004, la difesa della Regione Veneto ha dichiarato di accettare la rinuncia al ricorso, con allegata conforme delibera della Giunta regionale; &#13;
    che, a norma dell'articolo 25 delle norme integrative per i giudizi davanti alla Corte costituzionale, la rinuncia al ricorso seguita dalla relativa accettazione determina l'estinzione del processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara estinto il processo. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 12 gennaio 2005. &#13;
F.to: &#13;
Fernanda CONTRI, Presidente &#13;
Alfio FINOCCHIARO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 27 gennaio 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
