<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2021-07-06/139/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2021-07-06/139"/>
          <FRBRalias value="ECLI:IT:COST:2021:139" name="ECLI"/>
          <FRBRdate date="2021-07-06" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="139"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2021-07-06/139/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2021-07-06/139/ita@"/>
          <FRBRdate date="2021-07-06" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2021-07-06/139/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2021-07-06/139/ita@.xml"/>
          <FRBRdate date="2021-07-06" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2021-06-09" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2021-07-06" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CORAGGIO" href="/akn/ontology/person/it/CORAGGIO" showAs="CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2021</akn4cc:anno>
        <akn4cc:numero>139</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2021:139</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>CORAGGIO</akn4cc:presidente>
        <akn4cc:relatore>Francesco Viganò</akn4cc:relatore>
        <akn4cc:data_decisione>2021-06-09</akn4cc:data_decisione>
        <akn4cc:data_deposito>2021-07-06</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>; Giudici : <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 2 della legge della Regione Friuli-Venezia Giulia 18 maggio 2020, n. 8 (Misure urgenti per far fronte all'emergenza epidemiologica da COVID-19 in materia di demanio marittimo e idrico), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 16-23 luglio 2020, depositato in cancelleria il 23 luglio 2020, iscritto al n. 61 del registro ricorsi 2020 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 37, prima serie speciale, dell'anno 2020.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione autonoma Friuli-Venezia Giulia;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica dell'8 giugno 2021 il Giudice relatore Francesco Viganò;
 uditi l'avvocato dello Stato Ettore Figliolia per il Presidente del Consiglio dei ministri e l'avvocato Massimo Luciani per la Regione autonoma Friuli-Venezia Giulia;
 deliberato nella camera di consiglio del 9 giugno 2021.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso notificato il 16-23 luglio 2020 e depositato in cancelleria il 23 luglio 2020, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato l'art. 2 della legge della Regione Friuli-Venezia Giulia 18 maggio 2020, n. 8 (Misure urgenti per far fronte all'emergenza epidemiologica da COVID-19 in materia di demanio marittimo e idrico), per violazione dell'art. 117, secondo comma, lettera e), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>1.</num>
            <content>
              <p>1.- L'articolo impugnato prevede, al comma 1, che la validità delle concessioni con finalità turistico-ricreativa e sportiva, diportistica e attività cantieristiche connesse, nonché con finalità di acquacoltura sia in mare che in laguna, in essere alla data del 31 dicembre 2018, con scadenza antecedente al 2033, sia estesa, a domanda dei concessionari, fino al 31 dicembre 2033. Il comma 2 prevede poi che la «durata degli atti concessori» sia prorogata fino al termine del procedimento di cui al comma 1, comunque, per un periodo massimo di un anno decorrente dalla data di entrata in vigore della legge regionale impugnata.
 Rileva il ricorrente che la Regione autonoma Friuli-Venezia Giulia è certamente titolare di una competenza statutaria in materia di ittica, pesca e turismo, ed è altresì titolare delle funzioni amministrative in materia di «demanio marittimo, lacuale e fluviale», quando l'utilizzazione abbia finalità turistico-ricreativa. Cionondimeno, la disciplina degli affidamenti delle concessioni su tali aree afferirebbe alla materia della tutela della concorrenza, di esclusiva competenza statale ai sensi dell'art. 117, secondo comma, lettera e), Cost.
 La disciplina impugnata, estendendo a ulteriori concessioni demaniali marittime la proroga ex lege disposta dall'<mref><ref href="/akn/it/act/legge/stato/2018-12-30/145/!main#art_1__para_682">art. 1, commi 682</ref> e <ref href="/akn/it/act/legge/stato/2018-12-30/145/!main#art_1__para_683">683, della legge 30 dicembre 2018, n. 145</ref></mref> (Bilancio  di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021), inciderebbe sull'assetto concorrenziale dei mercati in termini tali da restringere il libero estrinsecarsi delle iniziative imprenditoriali, invadendo così la competenza esclusiva dello Stato in materia, appunto, di tutela della concorrenza.
 2.- Si è costituita in giudizio la Regione autonoma Friuli-Venezia Giulia, chiedendo che il ricorso sia dichiarato inammissibile, e comunque infondato nel merito.
 2.1.- Ad avviso della difesa regionale, il ricorso sarebbe anzitutto inammissibile, non avendo il ricorrente assolto l'onere di spiegare perché alla Regione ad autonomia speciale dovrebbe essere applicato il Titolo V della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> e non lo statuto speciale.
 2.2.- Nel merito, il ricorso sarebbe comunque infondato.
 In via generale, le concessioni demaniali marittime rientrerebbero infatti in diversi ambiti materiali, molti dei quali di competenza regionale. La disciplina impugnata, in particolare, sarebbe intervenuta nella materia delle concessioni «con finalità turistico ricreativa e sportiva, diportistica e attività cantieristiche connesse, nonché con finalità di acquacoltura sia in mare che in laguna», già disciplinate da tre leggi della Regione Friuli-Venezia Giulia cui lo stesso art. 2 impugnato rinvia. Tali leggi regionali, d'altra parte, già regolerebbero la durata delle relative concessioni, senza essere mai state oggetto - con l'eccezione della sola legge della Regione Friuli-Venezia Giulia 21 aprile 2017, n. 10 (Disposizioni in materia di demanio marittimo regionale, demanio ferroviario e demanio stradale regionale, nonché modifiche alle leggi regionali 17/2009, 28/2002 e 22/2006), scrutinata dalla <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/109/!main">sentenza n. 109 del 2018</ref> - di censure da parte del Presidente del Consiglio dei ministri.
 L'art. 2 ora impugnato, dal canto suo, si limiterebbe a meglio precisare e a dare attuazione alle previsioni della legge statale, dettate per far fronte all'emergenza determinata dalla pandemia in corso, peraltro nel pieno rispetto del diritto dell'Unione europea e dei principi di imparzialità, trasparenza e pubblicità, da essa espressamente richiamati.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>- In prossimità dell'udienza, la Regione autonoma Friuli-Venezia Giulia ha depositato memoria, con la quale ha anzitutto rilevato di avere impugnato innanzi a questa Corte, nelle more del presente giudizio, l'<ref href="/akn/it/act/decretoLegge/stato/2020-08-04/104/!main#art_100">art. 100 del decreto-legge 4 agosto 2020, n. 104</ref> (Misure urgenti per il sostegno e il rilancio dell'economia), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2020-10-13/126/!main">legge 13 ottobre 2020, n. 126</ref>, che estende ad ulteriori concessioni demaniali la proroga ex lege stabilita dall'<mref><ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_682">art. 1, commi 682</ref> e <ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_683">683, della legge n. 145 del 2018</ref></mref>.
 Ribadita poi l'eccezione di inammissibilità dell'impugnazione per non avere il ricorrente chiarito le ragioni dell'applicabilità del Titolo V della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> in luogo delle norme statutarie, la difesa regionale eccepisce altresì l'inammissibilità e, comunque, l'improcedibilità dell'impugnazione con specifico riferimento al comma 2 dell'art. 2 della legge reg. Friuli-Venezia Giulia n. 8 del 2020, dal momento che l'art. 11, comma 4, della legge della Regione Friuli-Venezia Giulia 30 dicembre 2020, n. 25 (Legge collegata alla manovra di bilancio 2021-2023), entrato in vigore nelle more del presente giudizio, estende sino al 31 dicembre 2021 la «proroga tecnica» prevista dall'art. 2, comma 2, della legge regionale ora impugnata, sicché l'eventuale accoglimento del presente ricorso lascerebbe comunque inalterato l'effetto della nuova disciplina, non impugnata dal Presidente del Consiglio dei ministri.
 Nel merito, ribaditi gli argomenti già svolti nell'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>, la difesa regionale osserva che la censura statale assumerebbe toni paradossali, dal momento che con la disciplina contestata la Regione avrebbe addirittura esteso, in materie di competenza regionale, la disciplina prevista in via generale dello Stato per far fronte all'emergenza sanitaria in corso.
 D'altra parte, la disciplina impugnata garantirebbe a ben guardare in misura più ampia la concorrenza rispetto alla stessa disciplina statale. A differenza di quest'ultima, che disporrebbe una mera proroga ex lege sino al 2033 delle concessioni in scadenza, la legge regionale qui all'esame subordinerebbe tale proroga alla domanda dei concessionari, in presenza della quale si avvierebbe un procedimento di affidamento imperniato sui principi di trasparenza, pubblicità e concorrenza, che prevedrebbe in particolare - ai sensi dell'art. 7 della legge reg. Friuli-Venezia Giulia n. 10 del 2017 - la pubblicazione delle domande e la possibilità per chiunque di presentare osservazioni, opposizioni o istanze concorrenti entro i venti giorni successivi. Tale procedimento sarebbe stato puntualmente rispettato dall'amministrazione regionale dopo l'entrata in vigore della disciplina impugnata, essendo state pubblicate ormai un centinaio di domande di rinnovo, come da prospetto riepilogativo prodotto in allegato alla memoria. In almeno un caso, poi, vi sarebbe stata altresì un'istanza di concessione concorrente. Ciò che dimostrerebbe che «la disciplina regionale non restringe (ma - anzi - amplia) il "libero esplicarsi delle iniziative imprenditoriali" e, in ultima analisi, tutela adeguatamente (addirittura più della disciplina statale) la concorrenza».</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con il ricorso indicato in epigrafe, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato l'art. 2 della legge della Regione Friuli-Venezia Giulia 18 maggio 2020, n. 8 (Misure urgenti per far fronte all'emergenza epidemiologica da COVID-19 in materia di demanio marittimo e idrico), per violazione dell'art. 117, secondo comma, lettera e), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 1.1.- L'articolo impugnato, rubricato «Modifica della durata delle concessioni del demanio marittimo», dispone al comma 1 che, «[a]ttesa anche l'emergenza epidemiologica da COVID-19 e al fine di garantire certezza alle situazioni giuridiche e assicurare l'interesse pubblico all'ordinata gestione del demanio senza soluzione di continuità, in conformità alle previsioni dei commi 682 e 683 dell'<ref href="/akn/it/act/legge/stato/2018-12-30/145/!main#art_1">articolo 1 della legge 30 dicembre 2018, n. 145</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2019 e bilancio pluriennale per il triennio 2019-2021), e nel rispetto dei principi di imparzialità, trasparenza e pubblicità, la validità delle concessioni con finalità turistico ricreativa e sportiva, diportistica e attività cantieristiche connesse, nonché con finalità di acquacoltura sia in mare che in laguna», disciplinate da tre leggi regionali richiamate e «in essere alla data del 31 dicembre 2018, con scadenza antecedente al 2033», sia «estesa fino alla data del 31 dicembre 2033 a domanda dei concessionari».
 Il comma 2 prevede poi che «[l]a durata degli atti concessori è prorogata fino al termine del procedimento di cui al comma 1 e, comunque, per un periodo massimo di un anno decorrente dalla data di entrata in vigore» della legge regionale medesima.
 1.2.- Secondo il Presidente del Consiglio dei ministri, l'art. 2 impugnato, prevedendo una proroga sino al 2033 delle concessioni menzionate, eccederebbe le competenze statutarie, incidendo nella materia - riservata alla legislazione statale - della tutela della concorrenza.
 2.- Il ricorso è ammissibile.
 2.1.- La difesa regionale ha eccepito invero l'inammissibilità del ricorso, dal momento che il Presidente del Consiglio dei ministri non avrebbe spiegato perché dovrebbero trovare applicazione, nella specie, le norme del Titolo V della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> e non, invece, le pertinenti norme dello statuto speciale.
 L'eccezione, tuttavia, non è fondata.
 Il ricorso riconosce, infatti, che la Regione autonoma Friuli-Venezia Giulia è titolare di competenze legislative primarie in materia di ittica, pesca e turismo, nonché delle competenze amministrative sul demanio marittimo, lacuale e fluviale; ma osserva che il censurato art. 2 - introducendo una proroga delle concessioni in essere sino al 2033 e, in tal modo, non consentendo di organizzare procedure di selezione per l'accesso di nuovi operatori - limita la concorrenza tra imprese, incidendo così in una materia riservata alla competenza esclusiva della legislazione statale.
 Nella prospettiva del ricorrente, dunque, l'afferenza della disciplina censurata alla materia della tutela concorrenza vale a escludere che la Regione possa rivendicare qualsiasi propria competenza statutaria, la quale pacificamente non comprende la materia in questione (ex mult<mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/153/!main">is, sentenze n. 153</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/119/!main">119 del 2019</ref></mref> e n. 109 del 2018, tutte con specifico riferimento alla Regione autonoma Friuli-Venezia Giulia).
 2.2.- Nella memoria depositata in prossimità dell'udienza, la difesa regionale ha altresì eccepito l'inammissibilità o, comunque, l'improcedibilità dell'impugnazione relativa al comma 2 impugnato. Osserva la difesa regionale che l'art. 11, comma 4, della legge della Regione Friuli-Venezia Giulia 30 dicembre 2020, n. 25 (Legge collegata alla manovra di bilancio 2021-2023), entrato in vigore nelle more del presente giudizio, estende sino al 31 dicembre 2021 la «proroga tecnica» prevista dall'art. 2, comma 2, della legge regionale ora impugnata; di talché l'eventuale accoglimento del ricorso con riferimento al comma 2 lascerebbe comunque inalterata la proroga disposta da tale ius superveniens, non impugnato dal Presidente del Consiglio dei ministri.
 Nemmeno tale eccezione è fondata.
 È assorbente in proposito il rilievo che il comma 2 dell'art. 2 della legge reg. Friuli-Venezia Giulia n. 8 del 2020 - che prevede una proroga "tecnica" delle concessioni in essere sino al termine del procedimento di rinnovo previsto dal comma 1 - è disposizione meramente ancillare rispetto al medesimo comma 1: di talché, nell'ipotesi in cui solo quest'ultimo venisse dichiarato costituzionalmente illegittimo, il comma 2 resterebbe comunque privo di autonoma portata normativa.
 3.- Nel merito, il ricorso è fondato.
 3.1.- Non v'è dubbio che, come giustamente osserva la difesa regionale, la disciplina delle concessioni demaniali interseca numerosi ambiti materiali di competenza legislativa primaria della Regione autonoma Friuli-Venezia Giulia; ma è altrettanto pacifico, nella giurisprudenza di questa Corte, che discipline regionali le quali dispongano proroghe o rinnovi automatici delle concessioni demaniali in essere incidono sulla materia, di competenza esclusiva statale, della tutela della concorrenza, ostacolando l'ingresso di altri potenziali operatori economici nel mercato di riferimento (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2021/10/!main">sentenze n. 10 del 2021</ref>, n. 1 del 2019, n. 171 del 2013 e n. 213 del 2011).
 3.2.- L'invasione della competenza statale non è esclusa nemmeno nell'ipotesi in cui la legislazione regionale si limiti - come accade nella specie - a riprodurre, nella sostanza, una disciplina già prevista dalla legislazione statale, e in particolare dall'<mref><ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_682">art. 1, commi 682</ref> e <ref href="/akn/it/act/legge/stato/2018/145/!main#art_1__para_683">683, della legge n. 145 del 2018</ref></mref> e successive modificazioni. Infatti, qualsiasi disciplina che comporti una restrizione al libero accesso nel mercato di altri operatori, come certamente accade quando si stabiliscano proroghe dei rapporti concessori in corso, è riservata dall'art. 117, secondo comma, lettera e), Cost., alla legislazione statale, restando invece precluso qualsiasi intervento della legislazione regionale in questa materia.
 3.3.- Né persuade l'argomento della difesa regionale secondo cui la disciplina in questa sede impugnata non introdurrebbe, in realtà, una proroga ex lege delle concessioni esistenti, bensì una procedura che consentirebbe la mera estensione della durata di tali concessioni su domanda degli interessati, in esito a un procedimento trasparente ed eventualmente comparativo, nel caso di presentazione di istanze concorrenti relative alla medesima concessione. Ciò che garantirebbe, ad avviso della Regione resistente, un livello di tutela della concorrenza più elevato di quello garantito oggi dalla legislazione statale, che prevede invece una generalizzata proroga - appunto - ex lege, di quindici anni e quindi sino al 2033, delle concessioni demaniali marittime.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il tenore dell'art. 2, comma 1, impugnato, tuttavia, non supporta una simile ricostruzione. Esso si limita infatti a stabilire che, in presenza di domanda del titolare, la durata delle concessioni ivi indicate, in essere alla data del 31 dicembre 2018 e con scadenza antecedente al 2033, «è estesa fino alla data del 31 dicembre 2033»: senza alcun richiamo a procedure comparative che potrebbero essere innescate da domande di concessione concorrenti, e senza alcun espresso rinvio - in particolare - al procedimento disciplinato dalla legge regionale n. 10 del 2017, che secondo la difesa regionale troverebbe applicazione in questa ipotesi.
 Tale conclusione non può essere revocata in dubbio dai documenti prodotti in giudizio dalla difesa regionale, che attestano l'avvenuta pubblicazione di un centinaio di domande di estensione della durata delle relative concessioni ai sensi della disciplina all'esame e la presentazione - in almeno un caso - di una domanda di concessione concorrente, senza che sia noto, peraltro, quale esito abbia avuto tale istanza. Ai fini della valutazione della legittimità costituzionale dell'art. 2 impugnato questa Corte non può, infatti, che muovere dal suo dato letterale, che subordina univocamente l'effetto di "estensione" sino al 2033 della durata delle concessioni esistenti alla data di entrata in vigore della legge regionale alla sola condizione della domanda dell'interessato, e appare pertanto atteggiarsi quale lex specialis rispetto ad ogni altra normativa - inclusa la legge regionale n. 10 del 2017 - che disciplina il procedimento di affidamento delle concessioni demaniali in parola.
 Tanto basta a configurare l'effetto di "estensione" al 2033 della durata delle concessioni come una sostanziale proroga delle concessioni esistenti, eccedente per le ragioni anzidette la competenza legislativa regionale.
 3.4.- Da ciò discende l'illegittimità costituzionale dell'art. 2 della legge reg. Friuli-Venezia Giulia n. 8 del 2020, per contrasto con l'art. 117, secondo comma, lettera e), Cost.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'art. 2 della legge della Regione Friuli-Venezia Giulia 18 maggio 2020, n. 8 (Misure urgenti per far fronte all'emergenza epidemiologica da COVID-19 in materia di demanio marittimo e idrico).
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2021-06-09">9 giugno 2021</date>.
 F.to:
 <signature><judge refersTo="#giancarloCoraggio" as="#presidente">Giancarlo CORAGGIO</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#francescoViganò" as="#relatore">Francesco VIGANÒ</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2021-07-06">6 luglio 2021</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
