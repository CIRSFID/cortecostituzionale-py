<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>164</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:164</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/05/1995</data_decisione>
    <data_deposito>16/05/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  del decreto-legge 16    &#13;
 novembre 1994, n. 629 (Modifiche alla disciplina degli scarichi delle    &#13;
 pubbliche fognature e degli insediamenti civili che non recapitano in    &#13;
 pubbliche fognature), promosso con ordinanza emessa  il  21  novembre    &#13;
 1994  dal  Pretore  di  Locri,  sezione  distaccata  di Caulonia, nel    &#13;
 procedimento penale a carico di Costantino Franco, iscritta al n. 735    &#13;
 del registro ordinanze 1994 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 51, prima serie speciale, dell'anno 1994;            &#13;
    Udito  nella  camera  di  consiglio  del  22 marzo 1995 il Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Ritenuto che con ordinanza emessa il 21 novembre 1994 nel corso di    &#13;
 un procedimento penale a carico di Costantino  Franco,  imputato  del    &#13;
 reato  previsto  e punito dall'art. 21 della legge 10 maggio 1976, n.    &#13;
 319 per avere effettuato lo scarico sul suolo di rifiuti liquidi  del    &#13;
 proprio  allevamento  di  bovini  senza  aver richiesto la prescritta    &#13;
 autorizzazione, il Pretore di Locri, sezione distaccata di  Caulonia,    &#13;
 ha   sollevato   questione   di   legittimità   costituzionale   del    &#13;
 decreto-legge 16 novembre 1994, n.  629  (Modifiche  alla  disciplina    &#13;
 degli  scarichi delle pubbliche fognature e degli insediamenti civili    &#13;
 che non recapitano in pubbliche fognature), nell'intero suo testo  ed    &#13;
 in particolare dell'art. 7;                                              &#13;
      che  il  giudice  rimettente ritiene che il decreto-legge n. 629    &#13;
 del 1994 sia, nel suo complesso, in contrasto  con  l'art.  77  della    &#13;
 Costituzione,  mancando  i  requisiti di necessità e di urgenza, che    &#13;
 costituiscono il presupposto essenziale di tale atto normativo;          &#13;
      che l'art. 7 del decreto-legge  stabilisce  che  i  titolari  di    &#13;
 scarichi  in  esercizio alla data di entrata in vigore della legge di    &#13;
 conversione  possono  presentare   domanda   di   autorizzazione   in    &#13;
 sanatoria,  al  cui rilascio consegue l'estinzione dei reati previsti    &#13;
 dall'art. 21, primo e secondo comma, della legge n. 319 del 1976;        &#13;
      che tale norma è denunciata, in riferimento agli artt. 3  e  97    &#13;
 della Costituzione, nella parte in cui non prevede la sospensione del    &#13;
 processo   penale   in  attesa  della  definizione  del  procedimento    &#13;
 amministrativo di rilascio  dell'autorizzazione  in  sanatoria,  come    &#13;
 avviene invece nella disciplina sul condono edilizio;                    &#13;
    Considerato  che  il decreto-legge 16 novembre 1994, n. 629 non è    &#13;
 stato convertito in legge entro il termine di sessanta  giorni  dalla    &#13;
 sua  pubblicazione  (si  veda  il  comunicato  relativo  alla mancata    &#13;
 conversione nella Gazzetta Ufficiale della Repubblica  n.  12,  serie    &#13;
 generale, del 16 gennaio 1995);                                          &#13;
      che  pertanto,  secondo  la  giurisprudenza  di questa Corte (da    &#13;
 ultimo ordinanze nn. 141, 123  e  122  del  1995),  la  questione  di    &#13;
 legittimità  costituzionale  deve  essere  dichiarata manifestamente    &#13;
 inammissibile, tenuto anche conto che  il  decreto-legge  attualmente    &#13;
 vigente  a  seguito di successive reiterazioni (17 marzo 1995, n. 79)    &#13;
 presenta un contenuto normativo in parte diverso da  quello  espresso    &#13;
 dal testo denunciato dal giudice rimettente;                             &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale del decreto-legge 16  novembre  1994,  n.    &#13;
 629   (Modifiche  alla  disciplina  degli  scarichi  delle  pubbliche    &#13;
 fognature e degli insediamenti civili che non recapitano in pubbliche    &#13;
 fognature), ed in particolare dell'art. 7, sollevata, in  riferimento    &#13;
 agli  artt.  77,  3  e  97  della Costituzione, dal Pretore di Locri,    &#13;
 sezione distaccata di Caulonia, con l'ordinanza indicata in epigrafe.    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 10 maggio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 16 maggio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
