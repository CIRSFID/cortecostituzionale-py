<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>117</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:117</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1988</data_decisione>
    <data_deposito>26/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco P. CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 1, 2 e 3 della    &#13;
 legge della Provincia Autonoma di Trento  9  dicembre  1978,  n.  56,    &#13;
 ("Disposizioni  transitorie  in  materia  di protezione della fauna e    &#13;
 disciplina  della  caccia")  e  2  e  3  della  legge  della  Regione    &#13;
 Trentino-Alto  Adige  7  settembre  1964,  n.  30,  ("Costituzione  e    &#13;
 gestione delle riserve di caccia nel territorio regionale"), promossi    &#13;
 con   n.   8  ordinanze  emesse  l'11  luglio  1983  dal  Pretore  di    &#13;
 Mezzolombardo, iscritte ai nn. 706, 707, 711, 712, 713,  714,  715  e    &#13;
 716 del registro ordinanze 1983 e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 355 dell'anno 1983 e n. 25 dell'anno 1984;           &#13;
    Visti  gli atti di costituzione della Provincia Autonoma di Trento    &#13;
 e della Federazione Italiana della caccia;                               &#13;
    Udito  nella  Camera  di consiglio del 10 dicembre 1987 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto che nel corso di alcuni giudizi di opposizione ex art. 23    &#13;
 l. 24 novembre 1981, n. 689, il Pretore  di  Mezzolombardo  con  otto    &#13;
 identiche ordinanze in data 11 luglio 1983 (r.o. 706 - 707, 711 - 716    &#13;
 del 1983), ha sollevato questione di legittimità costituzionale:        &#13;
      a) degli artt. 1, 2, 3 l. prov. di Trento 9 dicembre 1978, n. 56    &#13;
 ("Disposizioni transitorie in materia di  protezione  della  fauna  e    &#13;
 disciplina della caccia"), per contrasto con l'art. 105 dello Statuto    &#13;
 regionale;                                                               &#13;
      b)  degli  artt.  2  e 3 l. reg. T.A.A. 30 settembre 1964, n. 30    &#13;
 ("Costituzione e gestione delle  riserve  di  caccia  nel  territorio    &#13;
 regionale"),  per  contrasto  con l'art. 18, in relazione all'art. 16    &#13;
 dello Statuto regionale, nonché con l'art. 18 Cost. e  4  ed  8  del    &#13;
 medesimo Statuto;                                                        &#13;
      che    i    giudizi    a   quibus   hanno   tutti   ad   oggetto    &#13;
 ordinanze-ingiunzioni adottate dalla Provincia autonoma di Trento  in    &#13;
 seguito   all'accertamento  dell'infrazione  concernente  l'esercizio    &#13;
 della  caccia  in  riserva  senza  il  permesso  del  concessionario,    &#13;
 prevista e punita dall'art. 43 R.D. 5 giugno 1939, n. 1016;              &#13;
      che con la prima delle questioni sollevate il Pretore remittente    &#13;
 censura gli artt. 1, 2, 3 della legge prov. di Trento n. 56 del 1978,    &#13;
 nella parte in cui recepiscono con un rinvio i precetti e le sanzioni    &#13;
 contenente nel predetto art. 43;                                         &#13;
      che,  per  l'ordinanza  di rimessione tale rinvio contrasterebbe    &#13;
 con l'art. 34 comma terzo della legge quadro sulla caccia n. 968  del    &#13;
 1977,  che  prevede  la  vigenza  del titolo III del R.D. n. 1016 del    &#13;
 1939, soltanto fino ad un anno dopo la  sua  entrata  in  vigore  (19    &#13;
 gennaio  1979), termine entro il quale, peraltro, le regioni dovranno    &#13;
 emanare le proprie norme in materia (comma primo);                       &#13;
      che  la  questione  non  viene  però  sollevata in relazione al    &#13;
 profilo, ictu oculi  inconsistente,  della  violazione  della  citata    &#13;
 norma  statale  (art. 24 legge-quadro sulla caccia), nell'ipotesi che    &#13;
 essa possa costituire un principio dell'ordinamento  giuridico  dello    &#13;
 Stato  (art.  11 dello Statuto), ma con riferimento, invece, all'art.    &#13;
 105 dello Statuto regionale il quale testualmente dispone che: "Nelle    &#13;
 materie  attribuite  alla competenza della Regione o della provincia,    &#13;
 fino a quando non sia diversamente disposto  con  leggi  regionali  o    &#13;
 provinciali, si applicano le leggi dello Stato";                         &#13;
      che  il  parametro  invocato,  riferendosi  a  leggi regionali o    &#13;
 provinciali  che,  secondo  il  giudice  a  quo   dovrebbero   essere    &#13;
 necessariamente  "organiche", risulterebbe violato dall'emanazione di    &#13;
 norme che tale natura non hanno, quali quelle impugnate;                 &#13;
      che,   difatti,   le   stesse,  proprio  perché  formalmente  e    &#13;
 sostanzialmente inserite in una disciplina a  carattere  transitorio,    &#13;
 non  sarebbero  organiche  e  non riuscirebbero quindi a mantenere in    &#13;
 vita il titolo III del R.D. n. 1016 del 1939 (di cui fa parte  l'art.    &#13;
 43  posto  a  fondamento  dell'illecito  contestato) oltre il termine    &#13;
 annuale previsto dal citato art. 34 della legge quadro sulla caccia;     &#13;
      che,  pertanto,  non  potendosi  più ritenere vigente l'art. 43    &#13;
 R.D. n. 1016 del 1939, né essendo valido a tal fine, e relativamente    &#13;
 al  territorio  della Provincia, il richiamo recettizio operato dalle    &#13;
 norme  impugnate,  ad  avviso  dello   stesso   giudice   remittente,    &#13;
 l'esercizio   della   caccia   in   riserva  senza  il  permesso  del    &#13;
 concessionario, non costituirebbe più illecito, con  la  conseguenza    &#13;
 che:  "Sembra  legittimo  sostenere  la  nullità  della  ingiunzione    &#13;
 intimata in carenza di precetto sanzionatorio";                          &#13;
    Ritenuto,  inoltre,  che  la  seconda  delle  questioni  sollevate    &#13;
 attiene agli artt. 2 e 3 della legge regionale T.A.A. n. 30 del 1984,    &#13;
 che  affidano  la  gestione  delle  riserve  di  caccia "alle sezioni    &#13;
 provinciali cacciatori di Trento e Bolzano della Federazione Italiana    &#13;
 della  caccia  a  vantaggio  dei cacciatori iscritti e non iscritti",    &#13;
 demandando la disciplina della gestione -  fino  alla  emanazione  di    &#13;
 nuove  norme legislative - a disposizioni regolamentari appositamente    &#13;
 emanate dalla Giunta Regionale;                                          &#13;
      che,   questo  secondo  gruppo  di  norme,  viene  censurato  in    &#13;
 relazione ai seguenti profili:                                           &#13;
        a)  in quanto l'affidamento della gestione, comprendendo anche    &#13;
 una delega di  funzioni  amministrative  (come  dimostrano  le  norme    &#13;
 regolamentari al riguardo emanate dalla Giunta Regionale), violerebbe    &#13;
 il combinato disposto dagli artt. 16 e 18 dello Statuto che escludono    &#13;
 la  possibilità  di  delegare  tali  funzioni  a  soggetti giuridici    &#13;
 diversi dagli enti locali;                                               &#13;
        b)  in  quanto  l'esclusivo  affidamento  della gestione delle    &#13;
 riserve  alla  Federazione  italiana  della  caccia,   ponendosi   in    &#13;
 contrasto  con  il  criterio  normativo  della  "gestione sociale del    &#13;
 territorio" previsto dall'art. 15 della legge quadro sulla caccia (n.    &#13;
 968  del  1977) che costituisce principio generale dell'ordinamento o    &#13;
 comunque norma fondamentale di riforma economico-sociale,  violerebbe    &#13;
 i  limiti  imposti alla potestà legislativa primaria della Provincia    &#13;
 dagli artt. 4 e  8  dello  Statuto,  nonché  l'art.  18  Cost.,  che    &#13;
 garantisce  il  pluralismo delle strutture associative anche in campo    &#13;
 venatorio;                                                               &#13;
      che  nei  giudizi  introdotti con le ordinanze nn. 706 e 707 del    &#13;
 1983, si è costituita la Provincia autonoma di Trento chiedendo  che    &#13;
 le  sollevate  questioni  vengano dichiarate inammissibili o comunque    &#13;
 infondate;                                                               &#13;
      che   identiche  richieste  sono  state  formulate  anche  dalla    &#13;
 Federazione Italiana della caccia e dalla sua Sezione Provinciale  di    &#13;
 Trento, intervenute nei giudizi di merito;                               &#13;
    Considerato che in relazione alla prima delle questioni sollevate,    &#13;
 l'asserito contrasto con l'art. 105 dello Statuto non appare  neppure    &#13;
 astrattamente  ipotizzabile,  in  quanto  dal chiaro tenore letterale    &#13;
 della disposizione si evince che la sua ratio consiste  nell'impedire    &#13;
 un  vuoto  normativo  nella  materia,  fintantoché  la  regione o la    &#13;
 provincia non abbiano al riguardo legiferato;                            &#13;
      che  da  ciò  consegue  che  se, da un lato, potrebbe ritenersi    &#13;
 illegittima una legge statale che pretenda di disciplinare il settore    &#13;
 nonostante  che  la  regione  o  la  provincia  abbiano  a  ciò già    &#13;
 provveduto, ovvero una legge provinciale o  regionale  che,  pur  non    &#13;
 regolando  la  materia,  pretenda di impedire l'applicazione di norme    &#13;
 statali, dall'altro, non può certo esprimersi lo stesso giudizio nei    &#13;
 confronti  di  disposizioni provinciali, quali quelle impugnate, che,    &#13;
 nell'ambito  di  una  regolamentazione  transitoria,  si  limitano  a    &#13;
 ribadire l'applicabilità delle norme statali;                           &#13;
      che  inoltre, quest'ultime, secondo la stessa prospettazione del    &#13;
 giudice remittente, in quanto abrogate da successive  leggi  statali,    &#13;
 non   risulterebbero   più  applicabili,  così  restando  privo  di    &#13;
 efficacia anche il richiamo contenuto nelle disposizioni denunciate;     &#13;
      che ciò rende evidente l'irrilevanza, ai fini della definizione    &#13;
 del giudizio a  quo,  della  questione  sollevata,  che  va  pertanto    &#13;
 dichiarata manifestamente inammissibile, anche in ragione del modo in    &#13;
 cui si prospetta la violazione del parametro invocato;                   &#13;
      che  ad identiche conclusioni deve pervenirsi anche in relazione    &#13;
 all'asserita illegittimità costituzionale degli artt. 2  e  3  della    &#13;
 legge  regionale  T.A.A.  n.  30  del  1964,  sia  perché  l'atto di    &#13;
 rimessione  è  assolutamente  carente  di  qualsiasi  motivazione  o    &#13;
 indicazione  circa  la  rilevanza  della  questione,  sia  in  quanto    &#13;
 quest'ultima, in una fattispecie analoga e limitatamente  al  profilo    &#13;
 che  lamenta  l'affidamento esclusivo della gestione alla F.I.C., era    &#13;
 stata già ritenuta da questa Corte irrilevante  (sent.  n.  212  del    &#13;
 1970, punto 2);                                                          &#13;
      che un ulteriore motivo di inammissibilità va riscontrato anche    &#13;
 in relazione al profilo attinente all'asserita  illegittimità  della    &#13;
 delega  di  funzioni  amministrative  alla  F.I.C. dal momento che le    &#13;
 norme impugnate non  prevedono  in  alcun  modo  tale  delega  e  che    &#13;
 l'attribuzione  di  funzioni amministrative al predetto soggetto, ove    &#13;
 sussistente,  non  potrebbe   che   discendere   dalle   disposizioni    &#13;
 regolamentari  che  lo stesso giudice a quo individua ed erroneamente    &#13;
 ritiene, per  tale  aspetto,  attuative  della  fonte  primaria  (per    &#13;
 l'inammissibilità  di  analoga  questione  vedi ordinanza n. 501 del    &#13;
 1987);                                                                   &#13;
    Visti  gli  artt.  26, comma secondo, della legge 11 marzo 1953 n.    &#13;
 87, e n. 9, comma secondo, delle  Norme  integrative  per  i  giudizi    &#13;
 davanti la Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara   la   manifesta   inammissibilità  delle  questioni  di    &#13;
 legittimità costituzionale degli artt. 1, 2, 3,  della  legge  prov.    &#13;
 Trento  9  dicembre 1978, n. 56 ("Disposizioni transitorie in materia    &#13;
 di protezione della fauna e disciplina della caccia"), e degli  artt.    &#13;
 2  e 3 legge regionale T.A.A. 30 settembre 1964, n. 30 ("Costituzione    &#13;
 e gestione delle riserve di caccia  nel  territorio  regionale"),  in    &#13;
 riferimento  agli artt. 105, 16 e 18 dello Statuto regionale, nonché    &#13;
 agli artt. 18 Cost. e 4 e  8  del  medesimo  Statuto,  sollevate  dal    &#13;
 Pretore di Mezzolombardo con le ordinanze indicate in epigrafe.          &#13;
    Così  deciso  in  Roma,  nella  Sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 gennaio 1988.                              &#13;
                          Il presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
