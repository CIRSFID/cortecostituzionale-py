<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1964</anno_pronuncia>
    <numero_pronuncia>107</numero_pronuncia>
    <ecli>ECLI:IT:COST:1964:107</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/12/1964</data_decisione>
    <data_deposito>11/12/1964</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di  legittimità  costituzionale della norma  &#13;
 contenuta nel primo comma dell'art. 570 del Codice penale, promossi con  &#13;
 le seguenti ordinanze:                                                   &#13;
     1) ordinanza emessa il 16 aprile 1964 dal Pretore di  S.  Arcangelo  &#13;
 nel  procedimento penale a carico di Branco Caterina, iscritta al n. 80  &#13;
 del Registro ordinanze 1964 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica, n. 144 del 13 giugno 1964;                                   &#13;
     2) ordinanza emessa il 26 giugno 1964 dal Pretore di  Courgné  nel  &#13;
 procedimento penale a carico di Obertino Albina, iscritta al n. 133 del  &#13;
 Registro  ordinanze  1964  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica, n.  212 del 29 agosto 1964.                                  &#13;
     Udita nella camera di consiglio del 6 novembre  1964  la  relazione  &#13;
 del Giudice Giovanni Cassandro.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Nel  corso  di  un  giudizio penale davanti al Pretore di S.  &#13;
 Arcangelo a carico della signora Caterina Branco, il  Pretore  sollevò  &#13;
 d'ufficio  la  questione  di  legittimità  costituzionale  della norma  &#13;
 contenuta nel primo comma dell'art. 570 del Codice penale, che  punisce  &#13;
 "chiunque,  abbandonando  il  domicilio  domestico..  si  sottrae  agli  &#13;
 obblighi di assistenza inerenti alla patria potestà o alla qualità di  &#13;
 coniuge...", perché in contrasto con i precetti contenuti negli  artt.  &#13;
 13,   primo  comma,  16,  primo  comma,  e  29,  secondo  comma,  della  &#13;
 Costituzione.                                                            &#13;
     L'ordinanza,  ritualmente  notificata  e   comunicata,   è   stata  &#13;
 pubblicata nella Gazzetta Ufficiale, n. 144 del 13 giugno 1964.          &#13;
     2.  -  Secondo  l'ordinanza, il denunciato contrasto sorgerebbe dal  &#13;
 fatto  che,  contrariamente  a  una  interpretazione  della  Corte   di  &#13;
 cassazione,  il reato previsto dal primo comma dell'art. 570 del Codice  &#13;
 penale,  si  perfezionerebbe  col  semplice  abbandono  del   domicilio  &#13;
 domestico  da parte del coniuge non legalmente separato, non già, come  &#13;
 invece sostiene un'altra  corrente  giurisprudenziale,  quando  vi  sia  &#13;
 l'effettiva  inosservanza dell'obbligo di assistenza morale e materiale  &#13;
 verso  i  familiari  o quando l'abbandono sia mezzo per la consumazione  &#13;
 del reato. Per il Pretore di S.  Arcangelo,  infatti,  l'art.  570  del  &#13;
 Codice  penale  costituirebbe  la  sanzione  penale  dell'obbligo della  &#13;
 coabitazione posto dall'art. 143 del Codice civile e con  ciò  sarebbe  &#13;
 in  contrasto,  oltre  che  con  una concezione moderna dei rapporti di  &#13;
 diritto matrimoniale, che tenderebbe, "come  conquista  di  un  costume  &#13;
 più  evoluto",  a  porre  i  coniugi  sopra  un  piano  di parità e a  &#13;
 circoscrivere le norme che regolano l'istituto matrimoniale nell'ambito  &#13;
 del diritto privato, coi precetti costituzionali sopra  ricordati.  Con  &#13;
 quello  dell'art.  13,  che  proclama  l'inviolabilità  della libertà  &#13;
 personale, in quanto questa sarebbe  violata  da  una  sanzione  penale  &#13;
 posta  a  tutela di un obbligo, qual'è quello della coabitazione (come  &#13;
 l'altro  della  fedeltà),  di  natura  privatistica,  derivante  dalla  &#13;
 volontà  negoziale manifestata dal coniuge. Col precetto dell'art. 16,  &#13;
 che riconosce la libertà di circolazione e soggiorno e ne consente  la  &#13;
 limitazione  soltanto  per  motivi  di sanità e di sicurezza pubblica,  &#13;
 stabiliti dalla legge in  via  generale,  e  non,  come  nel  caso,  in  &#13;
 conseguenza  di uno status che trae origine, come quello di coniuge, da  &#13;
 un rapporto privatistico. Col precetto dell'art. 29, secondo comma,  in  &#13;
 quanto  la  norma impugnata violerebbe il principio paritario stabilito  &#13;
 per i rapporti tra i coniugi.  L'obbligo  della  coabitazione  infatti,  &#13;
 sostiene  l'ordinanza  pretorile, dovrebbe sussistere, se il matrimonio  &#13;
 è fondato su basi di eguaglianza, soltanto qualora la residenza  o  il  &#13;
 domicilio domestico fossero scelti di comune accordo tra i coniugi; non  &#13;
 potrebbe,  invece,  essere  imposto  a  un coniuge (la moglie) che, per  &#13;
 legge, deve seguire il marito.                                           &#13;
     3. - Non c'e  stata  costituzione  di  parti,  né  intervento  del  &#13;
 Presidente del Consiglio dei Ministri.                                   &#13;
     4.  -  La  stessa questione di legittimità costituzionale è stata  &#13;
 sollevata dalla difesa  dell'imputato  nel  corso  di  un  procedimento  &#13;
 penale  davanti al Pretore di Courgné che, avendola ritenuta rilevante  &#13;
 e non manifestamente infondata, ha sospeso il giudizio e trasmesso  gli  &#13;
 atti alla Corte,                                                         &#13;
     L'ordinanza,   regolarmente   notificata  e  comunicata,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale, n. 212 del 29 agosto 1964.          &#13;
     5. - Secondo quanto si  legge  nell'ordinanza,  la  violazione  dei  &#13;
 principi   della   libertà   naturale   della  donna  e  della  libera  &#13;
 circolazione della stessa deriverebbe  dal  fatto  che  la  scelta  del  &#13;
 domicilio domestico sia lasciata dalla legge all'arbitrio esclusivo del  &#13;
 marito e non all'accordo fra i coniugi. Né il contrasto con la parità  &#13;
 nel matrimonio, sancito dall'art. 29 della Costituzione, può ritenersi  &#13;
 sanato  dal fatto che il medesimo art. 29 prevede limiti legislativi al  &#13;
 principio  dell'eguaglianza  dei   coniugi   a   garanzia   dell'unità  &#13;
 familiare.  Non sarebbe, infatti, manifestamente infondato ritenere che  &#13;
 l'attribuzione  del  potere  di  scelta  della  residenza   al   marito  &#13;
 rappresenti,  per la troppa ampia configurazione legislativa del limite  &#13;
 al principio dell'eguaglianza fra i coniugi, una violazione della norma  &#13;
 costituzionale.                                                          &#13;
     6. - Non v'è stata  costituzione  di  parte,  né  intervento  del  &#13;
 Presidente del Consiglio dei Ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.   -   I   giudizi,  che  riguardano  le  medesime  questioni  di  &#13;
 costituzionalità, possono essere riuniti e decisi con unica sentenza.   &#13;
     2. - Le due ordinanze, malgrado talune differenze nello svolgimento  &#13;
 dei   motivi  che  sorreggerebbero  l'incostituzionalità  della  norma  &#13;
 impugnata,  sostanzialmente  concordano  nel  ritenere  che  la   norma  &#13;
 dell'art.  570  del  Codice  penale  altro  non  sia  se  non una norma  &#13;
 sanzionatrice dell'altra contenuta  nel  Codice  civile  all'art.  144,  &#13;
 giusta  la  quale la moglie è tenuta ad accompagnare il marito ovunque  &#13;
 egli creda opportuno di fissare la sua residenza, e pertanto violatrice  &#13;
 della eguale  condizione  dei  coniugi  nel  matrimonio.  Ora,  codesto  &#13;
 presupposto  della  sollevata  questione  di  legittimità  è  affatto  &#13;
 insussistente. La norma impugnata non punisce la moglie che si  rifiuti  &#13;
 di seguire il marito nella residenza da costui fissata, o che abbandoni  &#13;
 il  domicilio  domestico, ma il coniuge (sia il marito, sia la moglie),  &#13;
 il quale, mediante codesto  rifiuto  o  abbandono  (o  in  altra  guisa  &#13;
 prevista  dalla  medesima  norma),  viene meno all'obbligo fondamentale  &#13;
 della società coniugale, che consiste nella mutua assistenza materiale  &#13;
 e morale. Non è dunque l'abbandono del domicilio a integrare  da  solo  &#13;
 la  figura  del  reato, ma l'abbandono in quanto modo di sottrarsi agli  &#13;
 obblighi di assistenza ai quali i coniugi sono vicendevolmente  tenuti.  &#13;
 E  le  stesse  considerazioni,  com'è  evidente,  valgono  altresì ad  &#13;
 escludere che la norma abbia il fine di conferire  la  sanzione  penale  &#13;
 all'obbligo  della coabitazione, posto dall'art. 143 del Codice civile,  &#13;
 in sé e per sé considerato.                                            &#13;
     Ne consegue che l'art. 570 del Codice penale, prevedendo come reato  &#13;
 il comportamento di "chiunque, abbandonando il  domicilio  domestico...  &#13;
 si  sottrae  agli  obblighi  di assistenza inerenti... alla qualità di  &#13;
 coniuge", non viola il principio della eguaglianza morale  e  giuridica  &#13;
 dei  coniugi posta dall'art. 29, secondo comma, della Costituzione come  &#13;
 base sulla quale è "ordinato" il matrimonio.                            &#13;
     3. Stando così le  cose,  non  occorre  seguire  le  ordinanze,  e  &#13;
 segnatamente   quella   del   Pretore   di   S.  Arcangelo,  in  alcune  &#13;
 qualificazioni e affermazioni singolari circa la natura del  matrimonio  &#13;
 e  degli  obblighi  che  ne  derivano; e potrebbero ritenersi assorbite  &#13;
 anche le questioni relative agli artt. 13, primo  comma,  e  16,  primo  &#13;
 comma,   della   Costituzione,  le  quali  logicamente  derivano  dalla  &#13;
 violazione dell'altra, fondamentale e determinante nel caso  sottoposto  &#13;
 alla   Corte,   dell'art.   29.   Ma   si  può  anche  aggiungere  che  &#13;
 l'inviolabilità della libertà personale,  quale  deve  essere  intesa  &#13;
 nella  enunciazione  che  ne  fa  l'art. 13 citato, o la garanzia posta  &#13;
 dall'art. 16  (libertà  di  circolare  e  soggiornare  liberamente  in  &#13;
 qualsiasi  parte  del  territorio  nazionale)  o  non  vengono punto in  &#13;
 discussione nel caso in  esame,  o,  comunque,  non  possono  ritenersi  &#13;
 violate da limitazioni poste in relazione a un particolare status della  &#13;
 persona.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     pronunciando  con  unica sentenza sui giudizi indicati in epigrafe,  &#13;
 dichiara non fondata la questione di legittimità costituzionale  della  &#13;
 norma  contenuta  nel  primo  comma dell'art. 570 del Codice penale, in  &#13;
 relazione agli artt. 13, primo comma, 16, primo comma,  e  29,  secondo  &#13;
 comma, della Costituzione.                                               &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 4 dicembre 1964.        &#13;
                                   GASPARE  AMBROSINI - ANTONINO PAPALDO  &#13;
                                   - NICOLA JAEGER - GIOVANNI  CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
