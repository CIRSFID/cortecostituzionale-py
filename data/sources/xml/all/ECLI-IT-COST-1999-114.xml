<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>114</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:114</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/03/1999</data_decisione>
    <data_deposito>02/04/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 16  del  d.P.R.    &#13;
 26  ottobre  1972, n. 636 (Revisione della disciplina del contenzioso    &#13;
 tributario), promosso con ordinanza emessa il 9  gennaio  1997  dalla    &#13;
 commissione  tributaria  regionale  di Torino sul ricorso proposto da    &#13;
 Introvaia Gaetano contro l'ufficio I.V.A. di Torino, iscritta  al  n.    &#13;
 453 del registro ordinanze 1998 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 36, prima serie speciale, dell'anno 1998;            &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 24  febbraio  1999  il  giudice    &#13;
 relatore Annibale Marini;                                                &#13;
   Ritenuto  che  la  commissione  tributaria regionale di Torino, con    &#13;
 ordinanza del 9 gennaio 1997, ha sollevato, in  riferimento  all'art.    &#13;
 53,  primo  comma,  della  Costituzione,  questione  di  legittimità    &#13;
 costituzionale dell'art. 16, terzo comma, del decreto del  Presidente    &#13;
 della  Repubblica 26 ottobre 1972, n. 636 (Revisione della disciplina    &#13;
 del contenzioso tributario), "laddove non  consente  la  impugnazione    &#13;
 della  iscrizione  a  ruolo  preceduta  dalla notifica dell'avviso di    &#13;
 accertamento non autonomamente impugnato, anche nel caso in cui detto    &#13;
 avviso contenga errori di fatto";                                        &#13;
     che,  ad avviso della commissione rimettente, la norma denunciata    &#13;
 precluderebbe al contribuente, che  non  abbia  impugnato  l'atto  di    &#13;
 accertamento  fondato  su  un  errore  di  fatto,  la possibilità di    &#13;
 sottrarsi  ad  una  pretesa  fiscale  sicuramente  ingiusta   e   non    &#13;
 proporzionata  alla  sua  capacità  contributiva.  Mentre,  sotto un    &#13;
 diverso aspetto, l'esigenza di certezza e  definitività  degli  atti    &#13;
 amministrativi  non  potrebbe, comunque, comportare il sacrificio del    &#13;
 principio di capacità contributiva  garantito  dall'art.  53,  primo    &#13;
 comma, della Costituzione;                                               &#13;
     che  nel  giudizio  dinanzi  a  questa  Corte  è  intervenuto il    &#13;
 Presidente  del  Consiglio  dei  Ministri,  rappresentato  e   difeso    &#13;
 dall'Avvocatura  generale dello Stato, chiedendo che la questione sia    &#13;
 dichiarata infondata in   base alla  ritenuta  coerenza  della  norma    &#13;
 denunciata con tutto il sistema del diritto positivo, che, al fine di    &#13;
 assicurare  la  certezza  dei  rapporti  giuridici,  provvederebbe  a    &#13;
 fissare   termini   perentori   per   l'impugnazione    degli    atti    &#13;
 amministrativi;                                                          &#13;
     che, ad avviso della stessa difesa erariale, l'infondatezza della    &#13;
 questione,    sarebbe   ancora   più   evidente   considerando   che    &#13;
 l'Amministrazione  finanziaria   potrebbe   comunque   procedere   ad    &#13;
 annullare,  in  via  di autotutela, ai sensi dell'art. 68 del decreto    &#13;
 del Presidente della Repubblica 27 marzo 1992, n. 287 e  del  decreto    &#13;
 del  Ministro  delle  finanze  11  febbraio  1997,  n.  37, l'atto di    &#13;
 accertamento  fondato  su  un  errore  di  fatto  anche  se  divenuto    &#13;
 definitivo per la mancata impugnazione;                                  &#13;
   Considerato  che,  come  più  volte  chiarito  da questa Corte, il    &#13;
 principio  della  capacità  contributiva,  evocato  quale  parametro    &#13;
 esclusivo   dalla  commissione  rimettente,  riguarda  la  disciplina    &#13;
 sostanziale del sistema tributario ed esula perciò dalla  disciplina    &#13;
 del  processo  tributario  oggetto  della  censura  di illegittimità    &#13;
 costituzionale  (sentenza n. 120 del 1992; ordinanze n. 322 del  1992    &#13;
 e n. 108 del 1990);                                                      &#13;
     che, pertanto, la questione deve essere dichiarata manifestamente    &#13;
 infondata  per l'assorbente motivo della non pertinenza del parametro    &#13;
 evocato;                                                                 &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e  9, secondo comma, delle integrative per i giudizi davanti alla    &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art. 16, terzo comma, del decreto del Presidente    &#13;
 della Repubblica 26 ottobre 1972, n. 636 (Revisione della  disciplina    &#13;
 del  contenzioso  tributario), sollevata, in riferimento all'art. 53,    &#13;
 primo  comma,  della  Costituzione,  dalla   commissione   tributaria    &#13;
 regionale di Torino con l'ordinanza in epigrafe.                         &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 marzo 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Marini                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 2 aprile 1999.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
