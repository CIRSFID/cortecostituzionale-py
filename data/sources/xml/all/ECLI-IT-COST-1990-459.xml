<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>459</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:459</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ugo Spagnoli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/09/1990</data_decisione>
    <data_deposito>16/10/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 13, terzo comma,    &#13;
 del decreto-legge 12  settembre  1983,  n.  463  (Misure  urgenti  in    &#13;
 materia  previdenziale  e sanitaria e per il contenimento della spesa    &#13;
 pubblica,   disposizioni   per   vari    settori    della    pubblica    &#13;
 amministrazione   e  proroga  di  taluni  termini),  convertito,  con    &#13;
 modificazioni, nella legge 11 novembre 1983,  n.  638,  promossi  con    &#13;
 ordinanze  emesse  il  28  novembre  1989,  il 2 dicembre 1989, il 26    &#13;
 gennaio 1990, il 2 e 3 febbraio 1990 dal Pretore di Torino,  iscritte    &#13;
 rispettivamente  ai  nn.  259,  260,  261,  262  e  263  del registro    &#13;
 ordinanze 1990 e pubblicate nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 21, prima serie speciale, dell'anno 1990;                             &#13;
    Visti  gli atti di costituzione della S.p.a. Iveco FIAT, di Geraci    &#13;
 Umberto nonché l'atto di intervento del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio dell'11 luglio 1990 il Giudice    &#13;
 relatore Ugo Spagnoli;                                                   &#13;
    Ritenuto  che  con  le  cinque  ordinanze  indicate in epigrafe il    &#13;
 Pretore di Torino ha sollevato due diverse questioni di  legittimità    &#13;
 costituzionale  dell'art.  13,  terzo  comma,  del  decreto-legge  12    &#13;
 settembre 1983, n. 463 (Misure urgenti  in  materia  previdenziale  e    &#13;
 sanitaria  e  per  il contenimento della spesa pubblica, disposizioni    &#13;
 per vari settori della pubblica amministrazione e proroga  di  taluni    &#13;
 termini),  convertito,  con  modificazioni,  nella  legge 11 novembre    &#13;
 1983, n. 638, impugnandolo:                                              &#13;
       a)  nella  parte  in cui, ai fini della retribuibilità ex art.    &#13;
 2110 cod. civ. delle assenze per cure  idrotermali,  richiederebbe  -    &#13;
 secondo   l'interpretazione   delle  Sezioni  Unite  della  Corte  di    &#13;
 cassazione - l'indifferibilità di tali cure fino al periodo feriale,    &#13;
 ciò  che  ad  avviso  del  Pretore darebbe luogo a contrasto con gli    &#13;
 artt. 3, 32, 36, 38 e 102 della Costituzione (questione sollevata con    &#13;
 le  ordinanze, di identico tenore, iscritte ai nn. 259, 260, 261, 263    &#13;
 reg. ord. 1990);                                                         &#13;
       b)  nella  parte in cui escluderebbe il predetto trattamento di    &#13;
 cui all'art. 2110 cod. civ. in caso di assenza per  cure  idrotermali    &#13;
 aventi finalità preventive (ord. n. 262 del 1990);                      &#13;
      che  nel  giudizio  di  cui  all'ordinanza  n. 260/1990 la parte    &#13;
 privata Iveco FIAT S.p.a. e l'Avvocatura dello  Stato  hanno  chiesto    &#13;
 che  la  questione  sub  a) sia dichiarata inammissibile o infondata,    &#13;
 mentre nel giudizio di cui all'ordinanza n. 263/1990 la parte privata    &#13;
 Geraci Umberto ne ha chiesto l'accoglimento;                             &#13;
      Considerato che la questione sub a), già sollevata dal medesimo    &#13;
 Pretore con altra ordinanza di tenore identico a quelle qui in esame,    &#13;
 è  stata  dichiarata non fondata, "nei sensi di cui in motivazione",    &#13;
 con la sentenza n. 297 del 1990;                                         &#13;
      che pertanto essa va dichiarata manifestamente infondata;           &#13;
      che  la questione sub b) è prospettata nell'assunto che le cure    &#13;
 idrotermali  con  finalità  preventive  per  le  quali  non  possono    &#13;
 concedersi   permessi   extraferiali   siano   quelle  tendenti  alla    &#13;
 prevenzione del peggioramento della patologia  o  alla  riduzione  di    &#13;
 esso,  ciò  che presuppone l'esistenza di uno stato di malattia, pur    &#13;
 se  non  in  fase  acuta;  che  viceversa  l'esclusione  delle   cure    &#13;
 preventive che discende dalla limitazione di permessi extraferiali ai    &#13;
 casi  in   cui   ricorrano   "effettive   esigenze   terapeutiche   o    &#13;
 riabilitative"  concerne  -  secondo  quanto precisato dalla Corte di    &#13;
 cassazione, oltre che da questa stessa Corte  (sentenza  n.  559  del    &#13;
 1987)  -  le  sole  ipotesi  in  cui le prestazioni idrotermali siano    &#13;
 destinate  a  prevenire  una  malattia  non  ancora  insorta,  mentre    &#13;
 l'evitare  o  attenuare  il peggioramento delle affezioni a carattere    &#13;
 cronico o recidivante alla cui cura tali prestazioni sono tipicamente    &#13;
 preordinate  rientra  a  pieno titolo tra le finalità terapeutiche e    &#13;
 riabilitative cui la norma impugnata riconosce tutela;                   &#13;
      che  pertanto  anche  tale  questione,  in  quanto fondata in un    &#13;
 presupposto erroneo, va dichiarata manifestamente infondata;             &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza delle questioni di legittimità    &#13;
 costituzionale  dell'art.  13,  terzo  comma,  del  decreto-legge  12    &#13;
 settembre  1983,  n.  463  (Misure urgenti in materia previdenziale e    &#13;
 sanitaria e per il contenimento della  spesa  pubblica,  disposizioni    &#13;
 per  vari  settori della pubblica amministrazione e proroga di taluni    &#13;
 termini) convertito, con modificazioni, nella legge 11 novembre 1983,    &#13;
 n.  638, sollevata dal Pretore di Torino in riferimento agli artt. 3,    &#13;
 32, 36, 38 e 102 della Costituzione con ordinanza del 28 novembre,  2    &#13;
 dicembre  1989,  26  gennaio  e  3  febbraio  1990  ed in riferimento    &#13;
 all'art. 32 della Costituzione con ordinanza del 2 febbraio 1990.        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 settembre 1990.                            &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: SPAGNOLI                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 16 ottobre 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
