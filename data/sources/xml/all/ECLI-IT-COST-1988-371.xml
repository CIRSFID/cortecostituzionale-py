<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>371</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:371</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/03/1988</data_decisione>
    <data_deposito>31/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art.  9, secondo    &#13;
 comma, della legge 29 novembre 1962, n. 1655 (Norme per la disciplina    &#13;
 dei  contributi  e delle prestazioni concernenti l'"Ente nazionale di    &#13;
 previdenza e di  assistenza  per  gli  impiegati  dell'agricoltura"),    &#13;
 promossi  con  n.  2 ordinanze emesse il 4 giugno 1986 dal Pretore di    &#13;
 Perugia, iscritte ai nn. 693 e 694  del  registro  ordinanze  1986  e    &#13;
 pubblicate  nella Gazzetta Ufficiale della Repubblica n. 57, 1ª serie    &#13;
 speciale, dell'anno 1986;                                                &#13;
    Visti  gli  atti  di  costituzione  della  Cooperativa  Produttori    &#13;
 Tabacco  Alto  Tevere,  della  s.r.l.  Azienda  Agraria   Biagini   e    &#13;
 dell'I.N.A.I.L.;                                                         &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Francesco Greco;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>La Cooperativa Produttori Tabacchi Alto Tevere era dall'I.N.A.I.L.    &#13;
 convenuta in giudizio  dinanzi  al  Pretore  di  Perugia  per  sentir    &#13;
 dichiarare  il  suo  obbligo  di assicurare contro gli infortuni e le    &#13;
 malattie professionali i suoi impiegati dipendenti, adibiti  anche  a    &#13;
 mansioni manuali.                                                        &#13;
    La stessa sollevava eccezione di legittimità costituzionale delle    &#13;
 relative norme perché, dalla loro applicazione, derivava  un  doppio    &#13;
 onere  contributivo  essendo  i  detti  impiegati  contemporaneamente    &#13;
 iscritti anche all'E.N.P.A.I.A.                                          &#13;
    Il   Pretore,  in  accoglimento  dell'eccezione,  ritenendola  non    &#13;
 manifestamente infondata e rilevante ai fini del giudizio,  sollevava    &#13;
 questione di legittimità costituzionale:                                &#13;
       a)  dell'art.  9, secondo comma, legge 29 novembre 1962 n. 1655    &#13;
 nella parte in cui richiama l'art.  4  del  Regolamento  della  Cassa    &#13;
 assistenza degli impiegati agricoli e forestali del 1° febbraio 1947,    &#13;
 siccome prevede un doppio obbligo assicurativo;                          &#13;
       b)  dello  stesso  art.  9  nella  parte in cui non prevede una    &#13;
 riduzione dei contributi gravanti sui  datori  di  lavoro  in  favore    &#13;
 dell'E.N.P.A.I.A.,  proporzionale  all'area  di  rischio già coperta    &#13;
 dall'I.N.A.I.L.<diritto>Considerato in diritto</diritto>La  Corte  rileva  che,  in  riferimento all'art. 3 Cost., risulta    &#13;
 censurato l'art. 9, secondo comma, della  legge  n.  1655  del  1962,    &#13;
 interpretato  nel  senso  che  resterebbe  applicabile  l'art.  4 del    &#13;
 Regolamento  del  1°  febbraio  1947  della  Cassa  assistenza  degli    &#13;
 impiegati  agricoli e forestali, con la conseguente imposizione di un    &#13;
 doppio  obbligo  assicurativo  e  contributivo  dei  dipendenti   che    &#13;
 svolgono  anche  mansioni manuali, sia presso l'I.N.A.I.L. che presso    &#13;
 l'E.N.P.A.I.A. e della impossibilità, per i  datori  di  lavoro,  di    &#13;
 detrarre  dai  contributi  da versare all'E.N.P.A.I.A. quelli versati    &#13;
 all'I.N.A.I.L.                                                           &#13;
    La questione non è fondata.                                          &#13;
    Invero,  secondo  l'indirizzo  giurisprudenziale  della  Corte  di    &#13;
 cassazione  a  Sezioni  Unite,  le  due   forme   di   assicurazione,    &#13;
 E.N.P.A.I.A.  ed  I.N.A.I.L.,  operano  su un piano diverso e coprono    &#13;
 rischi completamente differenti in quanto, recependo  i  patti  della    &#13;
 contrattazione  collettiva  del  settore,  che  a  loro volta avevano    &#13;
 recepito   le   norme   dei   contratti    collettivi    corporativi,    &#13;
 l'assicurazione  presso  il  primo  ente  è diretta a coprire i soli    &#13;
 infortuni extraprofessionali e professionali di dirigenti, tecnici  e    &#13;
 impiegati,  sia  di  concetto  che di ordine, che non siano assistiti    &#13;
 dalla garanzia dell'I.N.A.I.L., tenuto conto che questa ultima, anche    &#13;
 nella  disciplina  introdotta  dal  d.P.R. 30 giugno 1965 n. 1124, è    &#13;
 limitata agli infortuni del personale con  mansioni  di  direzione  o    &#13;
 sorveglianza  sul  luogo  in  cui  si svolgono le operazioni agricole    &#13;
 esponenti a rischio, con la conseguente  esclusione  delle  attività    &#13;
 meramente burocratiche.                                                  &#13;
    Pertanto,  non  sussistendo  la lamentata doppia contribuzione, la    &#13;
 questione sollevata non è fondata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 9, secondo comma, della legge  29  novembre  1962  n.  1655    &#13;
 sollevata,  in  riferimento  all'art. 3 Cost., dal Pretore di Perugia    &#13;
 con le ordinanze indicate in epigrafe.                                   &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 marzo 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 31 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
