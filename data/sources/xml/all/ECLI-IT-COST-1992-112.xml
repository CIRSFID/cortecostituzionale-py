<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>112</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:112</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/03/1992</data_decisione>
    <data_deposito>18/03/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 5 e 10  della    &#13;
 legge  14  aprile  1985,  n.  140  (Miglioramento  e  perequazione di    &#13;
 trattamenti pensionistici e aumento della pensione sociale), e  degli    &#13;
 artt. 7 e seguenti del decreto-legge 31 luglio 1987, n. 317 (Norme in    &#13;
 materia   di  tutela  dei  lavoratori  italiani  operanti  nei  Paesi    &#13;
 extracomunitari e di rivalutazione delle pensioni erogate  dai  fondi    &#13;
 speciali gestiti dall'I.N.P.S.), convertito, con modificazioni, nella    &#13;
 legge  3  ottobre  1987,  n.  398, promosso con ordinanza emessa il 2    &#13;
 luglio 1991 dal Pretore di Venezia nei  procedimenti  civili  riuniti    &#13;
 vertenti  tra  Armando Forte ed altri ed E.N.P.A.L.S., iscritta al n.    &#13;
 634 del registro ordinanze 1991 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 41, prima serie speciale, dell'anno 1991;            &#13;
    Visto l'atto di intervento Presidente del Consiglio dei Ministri;     &#13;
    Udito nella camera di consiglio del 5  febbraio  1992  il  Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto  che  nel  corso  di  un  giudizio  in  cui i ricorrenti,    &#13;
 pensionati E.N.P.A.L.S., avevano richiesto  gli  aumenti  previsti  -    &#13;
 dall'art.  5  della  legge 15 aprile 1985, n. 140 - per le pensioni a    &#13;
 carico  dell'assicurazione  generale  obbligatoria  per  invalidità,    &#13;
 vecchiaia  e  superstiti  dei  lavoratori  dipendenti,  il Pretore di    &#13;
 Venezia, con ordinanza emessa il 2  luglio  1991,  ha  sollevato,  in    &#13;
 relazione  agli  artt.  3  e  38  della  Costituzione,  questione  di    &#13;
 legittimità costituzionale della norma citata, nonché dell'art.  10    &#13;
 della  stessa  legge  e degli artt. 7 e seguenti del decreto-legge 31    &#13;
 luglio 1987, n. 317, convertito, con  modificazioni,  nella  legge  3    &#13;
 ottobre  1987,  n.  398,  nella  parte  in  cui  non  prevedono che i    &#13;
 miglioramenti economici da esse stabiliti vengano estesi  anche  alle    &#13;
 pensioni E.N.P.A.L.S.;                                                   &#13;
      che  il  giudice  a quo osserva come la rivalutazione prevista a    &#13;
 favore delle pensioni dell'A.G.O. dalla legge 15 aprile 1985, n. 140,    &#13;
 fosse  stata  dalla   medesima   normativa   rinviata   a   "separati    &#13;
 provvedimenti" per tutte le altre gestioni sostitutive od esonerative    &#13;
 dell'assicurazione  generale  (provvedimenti  contenuti nella legge 3    &#13;
 ottobre 1987, n. 398, concernente tutte le altre forme di  previdenza    &#13;
 ma non l'E.N.P.A.L.S.);                                                  &#13;
      che  inoltre  il Pretore rimettente rileva come il decreto-legge    &#13;
 22  dicembre  1990,  n.  409  (Disposizioni  urgenti   in   tema   di    &#13;
 perequazione  dei  trattamenti  di  pensione  nei  settori  privato e    &#13;
 pubblico), convertito, con modificazioni,  nella  legge  27  febbraio    &#13;
 1991,  n.  59  (sopravvenuto  il quale, la Corte aveva restituito gli    &#13;
 atti ad altri giudici che avevano sollevato  la  medesima  questione)    &#13;
 non avrebbe in realtà risolto il problema;                              &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato e difeso  dall'Avvocatura  dello  Stato,  la  quale  ha    &#13;
 concluso  per  l'inammissibilità  ovvero  per  l'infondatezza  della    &#13;
 questione;                                                               &#13;
    Considerato  che  la  censura  è  sostanzialmente   rivolta   nei    &#13;
 confronti della recente normativa di cui al decreto-legge 22 dicembre    &#13;
 1990,  n.  409,  per  l'asserita  tardività  con  cui il legislatore    &#13;
 avrebbe ottemperato alle indicazioni  contenute  nell'art.  10  della    &#13;
 legge  15  aprile  1985,  n.  140,  realizzando,  per i pensionati in    &#13;
 argomento, una disciplina in  cui  i  coefficienti  di  rivalutazione    &#13;
 risulterebbero inferiori rispetto al congegno perequativo attuato nel    &#13;
 regime generale;                                                         &#13;
      che il giudice a quo si duole altresì della discrasia temporale    &#13;
 tra  il  richiamato  decreto-legge  n.  409 del 1990 ed il precedente    &#13;
 decreto-legge n. 317 del 1987 che ha previsto miglioramenti economici    &#13;
 per le pensioni a carico di altre forme di previdenza sostitutive  ed    &#13;
 esonerative;                                                             &#13;
      che, a prescindere dai limiti del sindacato di costituzionalità    &#13;
 connessi  al  confronto  tra  diverse  gestioni,  è evidente come il    &#13;
 prospettato  vizio  di  illegittimità  non  discenda   dalle   norme    &#13;
 impugnate,  bensì,  se  mai,  dal più volte citato e non denunziato    &#13;
 decreto-legge n. 409 del 1990;                                           &#13;
      che, pertanto, la questione è manifestamente inammissibile.        &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente inammissibile la questione di legittimità    &#13;
 costituzionale degli artt. 5 e 10 della legge 15 aprile 1985, n.  140    &#13;
 (Miglioramento  e perequazione di trattamenti pensionistici e aumento    &#13;
 della pensione sociale) e degli artt. 7 e seguenti del  decreto-legge    &#13;
 31  luglio  1987,  n.  317 (Norme in materia di tutela dei lavoratori    &#13;
 italiani operanti nei Paesi extracomunitari e di rivalutazione  delle    &#13;
 pensioni   erogate   dai   fondi   speciali  gestiti  dall'I.N.P.S.),    &#13;
 convertito, con modificazioni, nella legge 3 ottobre  1987,  n.  398,    &#13;
 sollevata,  in  riferimento agli artt. 3 e 38 della Costituzione, dal    &#13;
 Pretore di Venezia con l'ordinanza in epigrafe.                          &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 4 marzo 1992.                                 &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: CASAVOLA                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 18 marzo 1992.                           &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
