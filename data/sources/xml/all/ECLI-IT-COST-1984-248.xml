<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>248</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:248</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/10/1984</data_decisione>
    <data_deposito>05/11/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Dott. ALDO CORASANITI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  12  della  &#13;
 legge  3 agosto 1978, n. 405 (Delega al Presidente della Repubblica per  &#13;
 la concessione di amnistia e  di  indulto  e  disposizioni  sull'azione  &#13;
 civile  in  seguito  ad  amnistia), promosso con ordinanza emessa il 12  &#13;
 ottobre 1982 dal Tribunale di Lucca nel procedimento penale a carico di  &#13;
 Capovani Mirto, iscritta al  n.  875  del  registro  ordinanze  1982  e  &#13;
 pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 135 dell'anno  &#13;
 1983.                                                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di consiglio del 25 settembre 1984 il Giudice  &#13;
 relatore Giovanni Conso.                                                 &#13;
     Rilevato che il Tribunale di Lucca, con ordinanza  del  12  ottobre  &#13;
 1982,  ha  denunciato,  in  riferimento  agli  artt.    3  e  24  della  &#13;
 Costituzione, l'illegittimità dell'art. 12 della legge 3 agosto  1978,  &#13;
 n.  405,  "nella parte in cui non prevede altre cause di estinzione del  &#13;
 reato, all'infuori dell'amnistia, per poter decidere  sull'impugnazione  &#13;
 ai  soli  effetti  delle  disposizioni  e  dei  capi della sentenza che  &#13;
 concernono gli interessi civili";                                        &#13;
     ritenuto che le  censure  prospettate  investono  concretamente  la  &#13;
 mancata  estensione dell'art. 12 della legge n. 405 del 1978 alla morte  &#13;
 del reo, unica causa estintiva rilevante nel processo a quo;             &#13;
     considerato, quanto all'art. 3 Cost., che la Corte con la  sentenza  &#13;
 n.  68  del  1983  - nel dichiarare non fondata, in  riferimento a tale  &#13;
 parametro, la questione di legittimità  dell'art.  12  della  legge  3  &#13;
 agosto  1978,  n.    405,  nella  parte  in  cui  non prevede che anche  &#13;
 nell'ipotesi di  estinzione  del  reato  per  prescrizione  il  giudice  &#13;
 dell'impugnazione  possa  decidere  sull'impugnazione  stessa  ai  soli  &#13;
 effetti dei capi concernenti gli interessi  civili  -  ha  ribadito  il  &#13;
 principio,  già  fissato  con  la sentenza n. 202 del 1971, secondo il  &#13;
 quale  le  diversità  di  trattamento  della   prescrizione   rispetto  &#13;
 all'amnistia  trovano  giustificazione  nel  collegamento  dell'effetto  &#13;
 estintivo  della prescrizione ad un evento, quale il decorso del tempo,  &#13;
 sottratto, contrariamente a quanto  avviene  per  l'amnistia,  ad  ogni  &#13;
 discrezionalità;                                                        &#13;
     e  che  tale  principio è valido anche con riguardo alla morte del  &#13;
 reo, che dall'art. 152 c.p. viene  presa  in  considerazione,  ai  fini  &#13;
 dell'estinzione  del  reato, come mero fatto, comportante, oltre tutto,  &#13;
 il venir meno dello stesso rapporto processuale penale;                  &#13;
     che,  quanto  all'art.  24  Cost.,  il   giudice   a   quo   adduce  &#13;
 argomentazioni  che  insistono  sulla  disparità  di  trattamento  fra  &#13;
 l'amnistia e  la  morte  del  reo,  senza  concretamente  motivare  con  &#13;
 riguardo al diritto di difesa.                                           &#13;
     Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.  &#13;
 87, e 9, secondo comma, delle norme integrative per i  giudizi  davanti  &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1)   dichiara   la   manifesta   infondatezza  della  questione  di  &#13;
 legittimità costituzionale dell'art. 12 della legge 3 agosto 1978,  n.  &#13;
 405,  sollevata,  in  riferimento  all'art.  3  della Costituzione, dal  &#13;
 Tribunale di Lucca con l'ordinanza in epigrafe;                          &#13;
     2)  dichiara  la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità  costituzionale dell'art. 12 della legge 3 agosto 1978, n.  &#13;
 405, sollevata, in riferimento all'art. 24  della    Costituzione,  dal  &#13;
 Tribunale di Lucca con l'ordinanza in epigrafe.                          &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 30  ottobre 1984.       &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN   -   BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN   -   ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ALDO CORASANITI.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
