<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>319</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:319</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/06/1995</data_decisione>
    <data_deposito>13/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale del  d.P.R.  29  novembre    &#13;
 1952,  n.  2491 (Trasferimento in proprietà all'Ente per lo sviluppo    &#13;
 della irrigazione e la trasformazione fondiaria in Puglia e  Lucania,    &#13;
 sezione  speciale  per la riforma fondiaria, di terreni di proprietà    &#13;
 di Ostuni Adriano fu Saverio, in Comune  di  Palagianello),  promosso    &#13;
 con  ordinanza  emessa il 20 maggio 1994 dal Tribunale di Taranto nel    &#13;
 procedimento civile vertente tra Petruzzi Pietro e  l'Ente  regionale    &#13;
 di  sviluppo  agricolo  della Puglia (ERSAP) ed altro, iscritta al n.    &#13;
 451 del registro ordinanze 1994 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica al n. 35, prima serie speciale, dell'anno 1994;         &#13;
    Visti gli atti di costituzione di Petruzzi Pietro  e  di  D'Erchia    &#13;
 Cosimo, nonché l'atto di intervento del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nell'udienza pubblica del 30 maggio 1995 il Giudice relatore    &#13;
 Riccardo Chieppa;                                                        &#13;
    Udito l'avvocato Costantino Ventura per D'Erchia Cosimo;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel corso di un procedimento civile promosso da P. Petruzzi    &#13;
 contro l'Ente regionale di sviluppo agricolo della Puglia (ERSAP) per    &#13;
 sentir dichiarare di sua proprietà una parte dei  terreni  siti  nel    &#13;
 Comune di Palagianello, intestati alla ditta A. Ostuni ed espropriati    &#13;
 in  virtù  del  d.P.R.  29  novembre  1952, n. 2491, il Tribunale di    &#13;
 Taranto ha sollevato,  in  riferimento  agli  artt.  76  e  77  della    &#13;
 Costituzione,  questione  di legittimità costituzionale del suddetto    &#13;
 decreto presidenziale per eccesso di delega rispetto  alla  legge  21    &#13;
 ottobre 1950, n. 841.                                                    &#13;
    In particolare, il remittente premette che:                           &#13;
       a)  in  attuazione  delle  leggi di riforma fondiaria 12 maggio    &#13;
 1950, n. 230 e 21 ottobre 1950, n. 841, con d.P.R. 29 novembre  1952,    &#13;
 n.  2491, erano stati espropriati e trasferiti in proprietà all'Ente    &#13;
 per lo sviluppo della irrigazione e la  trasformazione  fondiaria  in    &#13;
 Puglia  e  Lucania,  sezione  speciale  per  la  riforma fondiaria, i    &#13;
 terreni  succitati  ed  in  particolare   quelli   ricompresi   nella    &#13;
 particella 22, foglio 17, della estensione di ettari 53, 16, 46;         &#13;
       b) che il Petruzzi aveva convenuto dinanzi al Tribunale l'ERSAP    &#13;
 assumendo  di  essere  esclusivo titolare - sia per acquisto a titolo    &#13;
 derivativo che per  usucapione  -  di  una  porzione  della  suddetta    &#13;
 particella  22  della  estensione  di  ettari 2, 22, 45, erroneamente    &#13;
 intestata in catasto alla ditta A. Ostuni  e  aveva  per  conseguenza    &#13;
 chiesto   che   fosse   dichiarato   di   sua   proprietà  il  suolo    &#13;
 illegittimamente espropriato;                                            &#13;
       c) che  il  Collegio  -  ritenendo  essenziale  ai  fini  della    &#13;
 decisione   accertare   l'effettivo  proprietario  del  suolo,  avuto    &#13;
 riguardo "alla reale situazione esistente all'epoca del provvedimento    &#13;
 ablatorio e non alla situazione apparente e  di  carattere  meramente    &#13;
 indicativo risultante dalle certificazioni catastali" - aveva ammesso    &#13;
 la prova per testi richiesta dall'attore;                                &#13;
       d)  che  all'esito  della  suddetta  prova  la  causa era stata    &#13;
 rimessa al Collegio per la decisione.                                    &#13;
    Ciò premesso, il remittente deduce la  rilevanza  della  proposta    &#13;
 questione  ai  fini della definizione del giudizio principale poiché    &#13;
 la prova, da parte dell'attore, del diritto di proprietà, aveva dato    &#13;
 esito  positivo  evidenziando   la   "effettiva   sussistenza   della    &#13;
 usucapione  dedotta come titolo di acquisto dell'immobile per effetto    &#13;
 di un possesso animo domini  esercitato  da  tempo  immemorabile  dal    &#13;
 medesimo   Petruzzi,   dal  suo  dante  causa  e  dagli  antenati  di    &#13;
 quest'ultimo".                                                           &#13;
    2. - Nel giudizio dinanzi a  questa  Corte  si  è  costituito  P.    &#13;
 Petruzzi,  chiedendo  che la questione di legittimità costituzionale    &#13;
 sia accolta sulla base di argomentazioni analoghe a quelle  formulate    &#13;
 nell'ordinanza di rimessione.                                            &#13;
    2.1.  - Si è, altresì, costituito C. D'Erchia, parte nel giudizio    &#13;
 a quo, il quale chiede  che  la  proposta  questione  sia  dichiarata    &#13;
 inammissibile e comunque non fondata.                                    &#13;
    In  particolare  si  sostiene  che  è "pregiudiziale acquisire un    &#13;
 esatto  accertamento  della  identità  catastale   tra   i   terreni    &#13;
 espropriati   e  quelli  rivendicati,  nonché  una  prova,  certa  e    &#13;
 inconfutabile, del possesso ininterrotto  ultratrentennale  da  parte    &#13;
 dei  danti  causa  del  Petruzzi alla data del decreto di esproprio",    &#13;
 indagini  che  rivestendo  carattere  pregiudiziale   rispetto   alla    &#13;
 questione  di  legittimità costituzionale sarebbero allo stato prive    &#13;
 di "convincente soluzione".                                              &#13;
    Nell'imminenza  dell'udienza  la  parte  privata  C.  D'Erchia  ha    &#13;
 depositato  memoria  illustrativa  con la quale riafferma e svolge le    &#13;
 tesi sostenute nell'atto introduttivo.                                   &#13;
    È intervenuto, fuori termine, il  Presidente  del  Consiglio  dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato.<diritto>Considerato in diritto</diritto>1. - Questa Corte è chiamata a decidere se il d.P.R. 29  novembre    &#13;
 1952,  n.  2491,  espropriativo  di terreni intestati in catasto a A.    &#13;
 Ostuni e  siti  nel  Comune  di  Palagianello  (Taranto),  violi,  in    &#13;
 riferimento  agli  artt.  76  e  77  della  Costituzione,  i  criteri    &#13;
 direttivi contenuti nella legge-delega 21 ottobre 1950, n. 841  (c.d.    &#13;
 Legge Stralcio: Norme per la espropriazione, bonifica, trasformazione    &#13;
 ed  assegnazione dei terreni ai contadini), per avere assoggettato ad    &#13;
 espropriazione   beni   non   appartenenti   al   destinatario    del    &#13;
 provvedimento  espropriativo,  in quanto precedentemente usucapiti da    &#13;
 altro soggetto.                                                          &#13;
    2. - Preliminarmente si rileva che l'indagine volta  ad  accertare    &#13;
 la  reale titolarità del diritto di proprietà ricadente sui terreni    &#13;
 espropriati, indispensabile ai fini della soluzione  della  questione    &#13;
 di  costituzionalità, è stata compiuta dal giudice a quo il quale -    &#13;
 nell'ordinanza che solleva la presente questione -  dichiara  che  la    &#13;
 prova  fornita  al  riguardo  dall'attore  "ha  dato  esito positivo"    &#13;
 evidenziando "la effettiva sussistenza dell'usucapione per effetto di    &#13;
 un possesso animo domini esercitato da tempo immemorabile dall'attore    &#13;
 stesso e dai suoi danti causa" (e relativo al terreno  corrispondente    &#13;
 alla  particella  22  del  foglio  17  del  catasto  di Palagianello,    &#13;
 ricompresa nell'espropriazione decretata nei confronti di A. Ostuni).    &#13;
    3. - Ciò premesso, la questione è fondata.                          &#13;
    La legge 21 ottobre 1950, n. 841 (art. 4, secondo e quarto comma),    &#13;
 richiede  quale  vero  e proprio presupposto legittimante l'esercizio    &#13;
 della  procedura  espropriativa  che  l'espropriazione  debba  essere    &#13;
 effettuata,  ricorrendo  le  condizioni  prescritte, nei confronti di    &#13;
 soggetti che siano proprietari dei terreni assoggettati ad esproprio:    &#13;
 ciò che si evince, come affermato da  questa  Corte,  dalla  lettera    &#13;
 stessa delle norme in armonia con il sistema della legge: in numerosi    &#13;
 articoli   di  essa  ricorrono,  infatti,  le  locuzioni  "proprietà    &#13;
 terriera privata" e "proprietario" usate in  senso  tecnico-giuridico    &#13;
 (sentenze  n.  8 e n. 57 del 1959, n. 21 del 1967, n. 3 del 1987). Ne    &#13;
 deriva  che  l'avere  la  legge  di   esproprio   identificato   come    &#13;
 proprietario  quello risultante dalle certificazioni catastali, altro    &#13;
 essendo il vero dominus da usucapione, configura  eccesso  di  delega    &#13;
 nei   riguardi  del  bene  oggetto  di  contestazione.  È,  infatti,    &#13;
 giurisprudenza costante di questa Corte il principio  per  cui  "alle    &#13;
 intestazioni  catastali  può attribuirsi valore soltanto indicativo"    &#13;
 circa  la  titolarità  di  diritti   reali.   Invero,   nel   nostro    &#13;
 ordinamento,  le  scritture catastali non rivestono valore probatorio    &#13;
 ai fini dell'accertamento della  proprietà  privata.  Pertanto,  nel    &#13;
 contrasto  tra intestazioni catastali e giuridica prova dell'acquisto    &#13;
 del diritto di proprietà, quest'ultima deve prevalere  agli  effetti    &#13;
 di   cui   trattasi.   L'espropriazione  in  esame  poteva,  perciò,    &#13;
 legittimamente effettuarsi solo riguardo alle porzioni di terreno che    &#13;
 appartenevano ai soggetti espropriati.                                   &#13;
    Il  decreto  presidenziale  impugnato,  in  quanto   ha   compreso    &#13;
 nell'esproprio  terreni intestati alla ditta A. Ostuni e che a quella    &#13;
 ditta non appartenevano (identificati  nell'ordinanza  di  rimessione    &#13;
 con  particella 22, foglio 17, Comune di Palagianello), ha, pertanto,    &#13;
 certamente esorbitato dai limiti della delega di cui all'art. 4 della    &#13;
 legge n. 841 del 1950 e va conseguentemente  dichiarato,  per  questa    &#13;
 parte,   illegittimo  per  violazione  degli  artt.  76  e  77  della    &#13;
 Costituzione.                                                            &#13;
    La dichiarazione di illegittimità costituzionale non travolge  le    &#13;
 restanti parti del decreto aventi autonoma efficacia.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  del  d.P.R. 29 novembre    &#13;
 1952, n. 2491 (Trasferimento in proprietà all'Ente per  lo  sviluppo    &#13;
 della  irrigazione e la trasformazione fondiaria in Puglia e Lucania,    &#13;
 sezione speciale per la riforma fondiaria, di terreni  di  proprietà    &#13;
 di Ostuni Adriano fu Saverio, in Comune di Palagianello), nella parte    &#13;
 in  cui  ha  incluso  nell'espropriazione terreni non appartenenti al    &#13;
 soggetto espropriato.                                                    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 28 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                         Il redattore: CHIEPPA                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 13 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
