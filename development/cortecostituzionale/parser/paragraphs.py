from akomantoso.dom.elements import Background, Motivation, Division, Heading
from akomantoso.dom.elements import Paragraph, Point

from cortecostituzionale.parser.parser import parser


def parse_paragraphs(paragraphs, target, default=Paragraph, structure=False):
    elements = []
    root = target
    fatto_flag = diritto_flag = False
    for t in paragraphs:
        if structure:
            m = parser.search(t.strip(), "structure")
            if m is not None:
                mkey = m.first().key
                if mkey == "fatto" and not fatto_flag:
                    fatto_flag = True
                    background = Background()
                    background_division = Division()
                    heading = Heading(text=t)
                    background.append(background_division)
                    background_division.append(heading)
                    root.append(background)
                    target = background_division
                elif mkey == "diritto" and not diritto_flag:
                    diritto_flag = True
                    motivation = Motivation()
                    motivation_division = Division()
                    heading = Heading(text=t)
                    motivation.append(motivation_division)
                    motivation_division.append(heading)
                    root.append(motivation)
                    target = motivation_division
                else:
                    raise Exception(f"Unkown structural element {mkey}")
                continue
            if not (fatto_flag or diritto_flag):
                motivation = Motivation()
                motivation_division = Division()
                motivation.append(motivation_division)
                root.append(motivation)
                target = motivation_division
                diritto_flag = True
        m = parser.search(t, "paragraphs")
        if m is not None:
            num_match = m.first()
            num = num_match.value
            text = t[num_match.end:]
            if num_match.key == "point_num" and len(elements):
                elements[-1].append(Point(text=text, num=num))
            else:
                par = default(text=text, num=num)
                target.append(par)
                elements.append(par)
        else:
            par = default(text=t)
            target.append(par)
            elements.append(par)
