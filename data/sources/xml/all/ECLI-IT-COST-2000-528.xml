<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>528</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:528</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/11/2000</data_decisione>
    <data_deposito>22/11/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 333 e 336 del    &#13;
 codice civile e degli artt. 738 e 739 del codice di procedura civile,    &#13;
 promosso  con  ordinanza  emessa  il  28 ottobre  1999 dalla Corte di    &#13;
 appello di Genova - sezione per i minorenni - sul reclamo proposto da    &#13;
 Ruani Sergio ed altra, iscritta al n. 281 del registro ordinanze 2000    &#13;
 e  pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 23, 1ª    &#13;
 serie speciale, dell'anno 2000.                                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 ministri;                                                                &#13;
     Udito  nella camera di consiglio del 27 settembre 2000 il giudice    &#13;
 relatore Fernando Santosuosso.                                           &#13;
     Ritenuto che la Corte d'appello di Genova - sezione specializzata    &#13;
 per  i  minorenni  -  con  ordinanza del 28 ottobre 1999 (pervenuta a    &#13;
 questa  Corte  in  data  28 aprile  2000),  ha sollevato questione di    &#13;
 legittimità  costituzionale  degli artt. 336 e 333 cod. civ. nonché    &#13;
 738  e  739  cod.  proc.  civ.  in riferimento agli artt. 24, secondo    &#13;
 comma,  2,  3, secondo comma, 30 e 31 della Costituzione, nella parte    &#13;
 in   cui   nel   procedimento   camerale  limitativo  della  potestà    &#13;
 genitoriale  non prevedono la nomina di un curatore in rappresentanza    &#13;
 del minore;                                                              &#13;
         che i parametri costituzionali sarebbero violati in quanto la    &#13;
 mancata   considerazione   del   minore,  come  parte  del  giudizio,    &#13;
 inciderebbe  negativamente  sulla  tutela  dei  suoi  diritti  ed  in    &#13;
 particolare  di  quello  ad  uno  sviluppo compiuto ed armonico della    &#13;
 personalità, implicitamente garantito dalle disposizioni stesse;        &#13;
         che  il procedimento di reclamo ex art 739 cod. proc. civ. è    &#13;
 stato   introdotto   dai   collocatari   della   minore   avverso  un    &#13;
 provvedimento  del  tribunale  che aveva affidato la stessa al comune    &#13;
 affinché questi ne curasse il rientro presso il nucleo familiare del    &#13;
 nonno materno;                                                           &#13;
         che  i  reclamanti  lamentavano  come il tribunale non avesse    &#13;
 tenuto conto delle risultanze della consulenza tecnica nella parte in    &#13;
 cui  si  dubitava  dell'idoneità  educativa del gruppo familiare del    &#13;
 nonno,  sottolineando  la  situazione  di  grave e protratta violenza    &#13;
 fisica e psicologica;                                                    &#13;
         che,  nel  costituirsi,  i  nonni della minore sostenevano il    &#13;
 difetto  di legittimazione dei reclamanti e l'adeguatezza del proprio    &#13;
 nucleo familiare;                                                        &#13;
         che  all'udienza,  davanti  al  giudice  a  quo, l'A.N.F.A.A.    &#13;
 (Associazione  Nazionale Famiglie Adottive e Affidatarie) chiedeva di    &#13;
 intervenire  in  adesione  alle conclusioni dei reclamanti, ritenendo    &#13;
 che  questi  avevano  agito  nell'esclusivo  interesse  della minore,    &#13;
 atteso che appariva preoccupante il ritorno di questa presso il nonno    &#13;
 e  auspicava  la  nomina di un curatore speciale sia sotto il profilo    &#13;
 sostanziale che sotto quello processuale;                                &#13;
         che  nell'ordinanza di rimessione, premesso che in dottrina e    &#13;
 giurisprudenza  si evidenzia una sempre maggiore considerazione della    &#13;
 posizione  del  minore  quale soggetto titolare di diritti soggettivi    &#13;
 perfetti,  autonomi  ed  azionabili, membro a tutti gli effetti della    &#13;
 comunità   sociale   ("non   più   oggetto   di   una  assoluta  ed    &#13;
 incondizionata  volontà  degli adulti, ma sicuramente "persona" alla    &#13;
 pari  di ogni altro individuo"), si richiama il c.d. diritto minorile    &#13;
 e  cioè un complesso di norme riguardanti la posizione del fanciullo    &#13;
 nella prospettiva di una tutela preminente del suo interesse;            &#13;
         che  il  procedimento  ex artt. 333, 336 cod. civ., in cui il    &#13;
 giudice  assume  ogni  opportuno  e  conveniente provvedimento per la    &#13;
 prole, ha effetti rilevanti sull'avvenire del fanciullo, e tuttavia a    &#13;
 questi non è dato stare in giudizio a mezzo di curatore speciale per    &#13;
 la  tutela  dei  suoi  interessi  e  neppure è previsto che egli sia    &#13;
 obbligatoriamente sentito;                                               &#13;
         che  gli interessi del minore non sono adeguatamente tutelati    &#13;
 dal  p.m.  in sede di intervento obbligatorio, anche perché i poteri    &#13;
 di  quest'organo  non si ricollegano a tali specifici interessi, ma a    &#13;
 quello   generale  dell'attuazione  della  legge;  analogamente,  non    &#13;
 potrebbe  costituire valida alternativa alla nomina di un curatore il    &#13;
 potere largamente officioso del giudice;                                 &#13;
         che la nomina di un curatore in rappresentanza del minore non    &#13;
 è  sconosciuta  al nostro ordinamento, come nei casi di conflitto di    &#13;
 interessi  tra genitori e figli (art. 370 recte: art. 320, u.c., cod.    &#13;
 civ.)  ovvero,  in  sede  processuale,  per  le azioni di status e le    &#13;
 procedure di opposizione al decreto di adottabilità;                    &#13;
         che  la  nomina  di  un curatore del minore nel caso in esame    &#13;
 trova  una  base  normativa  in  alcuni  documenti internazionali: la    &#13;
 Convenzione  sui  diritti  del  fanciullo di New York del 20 novembre    &#13;
 1989,  ratificata  e  resa  esecutiva  con  la  legge 27 maggio 1991,    &#13;
 n. 176,  la  quale afferma che al fanciullo deve essere assicurata la    &#13;
 possibilità  di  essere  ascoltato in ogni procedura che lo riguardi    &#13;
 sia  direttamente  che attraverso un rappresentante (artt. 3 e 12); e    &#13;
 la  Convenzione  europea  di Strasburgo del 25 gennaio 1996, la quale    &#13;
 prevede  il  riconoscimento  al  minore di diritti processuali che lo    &#13;
 riguardano  e, in particolare, il diritto di chiedere la nomina di un    &#13;
 terzo rappresentante;                                                    &#13;
         che  questa  Corte  ha  già  dichiarato (sentenza n. 185 del    &#13;
 1986)  non  fondata  questione  analoga, ma con specifico riferimento    &#13;
 alla  procedura  di  divorzio  e  separazione  per i provvedimenti di    &#13;
 affidamento della prole;                                                 &#13;
         che  comunque  oggi  vi  è una accresciuta sensibilità alla    &#13;
 problematica del minore;                                                 &#13;
         che  sulla  rilevanza  della  questione  la  Corte rimettente    &#13;
 osserva  che nella presente causa essa si dovrebbe "pronunciare sulla    &#13;
 collocazione   della   minore   senza   avere  chiara  consapevolezza    &#13;
 dell'interesse   di   questa   non   adeguatamente  rappresentata  in    &#13;
 giudizio".  Allo  stato  - soggiunge il giudice a quo - vi sarebbe la    &#13;
 "necessità  di  dichiarare  la carenza di legittimazione processuale    &#13;
 dei  ricorrenti che non hanno alcun potere di rappresentanza dei suoi    &#13;
 interessi, nonché l'inammissibilità dell'intervento dell'A.N.F.A.A.    &#13;
 con conseguente conferma del provvedimento impugnato"; che, tuttavia,    &#13;
 dall'accoglimento  della  questione  discenderebbe  la  nomina  di un    &#13;
 rappresentante  del  minore,  nei  confronti  del  quale  si dovrebbe    &#13;
 disporre  l'integrazione  del  contraddittorio  e  la  riapertura dei    &#13;
 termini per l'impugnazione;                                              &#13;
         che  nel  giudizio  dinanzi  a questa Corte è intervenuto il    &#13;
 Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura  generale dello Stato, chiedendo che la questione sia    &#13;
 dichiarata inammissibile o infondata.                                    &#13;
     Considerato  che,  secondo  la  costante giurisprudenza di questa    &#13;
 Corte,  la  questione  di  legittimità  costituzionale,  per  essere    &#13;
 ammissibile,   esige   anzitutto  una  plausibile  e  non  carente  o    &#13;
 contraddittoria  motivazione  sulla  rilevanza  della medesima; nella    &#13;
 specie,  l'ordinanza  ritiene  necessaria  ai  fini  del decidere una    &#13;
 pronuncia di incostituzionalità della omessa previsione della nomina    &#13;
 di  un  curatore  speciale  per  l'ipotesi  in  esame, senza tuttavia    &#13;
 motivare  in  modo  completo  sulla  effettiva  mancanza  nel vigente    &#13;
 ordinamento  di  norme,  speciali  o  generali,  che  consentano tale    &#13;
 nomina:  verifica  che  costituiva  presupposto indispensabile per la    &#13;
 predetta rilevanza;                                                      &#13;
         che inoltre il giudice a quo, mentre afferma che "il collegio    &#13;
 si   troverebbe   nella   necessità  di  dichiarare  la  carenza  di    &#13;
 legittimazione processuale dei (reclamanti) collocatari della minore,    &#13;
 i quali non hanno alcun potere di rappresentanza dei suoi interessi",    &#13;
 non  risolve  poi  il  problema pregiudiziale sull'ammissibilità del    &#13;
 procedimento  di  gravame;  e  nel  contempo  solleva la questione di    &#13;
 costituzionalità, senza argomentare sul perché, nonostante ciò, la    &#13;
 medesima possa ritenersi rilevante in quello stesso procedimento;        &#13;
         che,   pertanto,   deve   essere   accolta   l'eccezione   di    &#13;
 inammissibilità sollevata dall'Avvocatura dello Stato.                  &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità  costituzionale  degli artt. 333 e 336 cod. civ. nonché    &#13;
 738  e  739  cod.  proc.  civ., in riferimento agli artt. 24, secondo    &#13;
 comma,  2,  3,  secondo  comma, 30 e 31 della Costituzione, sollevata    &#13;
 dalla  Corte  d'appello  di  Genova  -  sezione  specializzata  per i    &#13;
 minorenni - con l'ordinanza di cui in epigrafe.                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 novembre 2000.                             &#13;
 .                                                                        &#13;
                       Il Presidente: Mirabelli                           &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 15 novembre 2000.                       &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
