<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:19</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>VASSALLI</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/01/2000</data_decisione>
    <data_deposito>21/01/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giuliano VASSALLI; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI, dott. &#13;
 Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 448 del  codice    &#13;
 di  procedura penale, promosso con ordinanza emessa il 26 maggio 1998    &#13;
 dalla Corte d'appello di Venezia nel procedimento penale a carico  di    &#13;
 S.I.,  iscritta  al  n.  849 del registro ordinanze 1998 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  47,  prima   serie    &#13;
 speciale, dell'anno 1998.                                                &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 24  novembre  1999  il  giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto  che  la  Corte  di  appello  di  Venezia ha sollevato, in    &#13;
 riferimento all'art. 3 della  Costituzione, questione di legittimità    &#13;
 costituzionale dell'art. 448 del codice di  procedura  penale,  nella    &#13;
 parte  in  cui  non  prevede  il  potere  del  giudice  di appello di    &#13;
 pronunciare sentenza di applicazione della  pena  concordata  tra  le    &#13;
 parti  nel  giudizio  di  primo  grado  anche quando in tale giudizio    &#13;
 l'imputato sia stato assolto a norma dell'art. 129 cod. proc. pen;       &#13;
     che il rimettente premette che il pretore di Bassano del  Grappa,    &#13;
 che  procedeva  nei  confronti  di un imputato di furto aggravato che    &#13;
 aveva presentato richiesta di applicazione della pena, per  la  quale    &#13;
 il   pubblico   ministero   aveva  prestato  il  consenso,  ritenendo    &#13;
 contraddittori  gli  elementi di prova, aveva pronunciato sentenza di    &#13;
 assoluzione ai sensi degli artt. 129 e 530, comma 2, cod. proc. pen;     &#13;
     che la Corte di appello rimettente,  investita  dell'impugnazione    &#13;
 del procuratore generale che aveva chiesto, in riforma della sentenza    &#13;
 appellata,  la  condanna  dell'imputato,  rileva  che, ove si dovesse    &#13;
 pervenire, in conformità alla richiesta del procuratore generale, ad    &#13;
 una  pronuncia  di  condanna,  l'imputato  verrebbe   privato   della    &#13;
 possibilità  di  ottenere  la  diminuzione  di pena ex art. 444 cod.    &#13;
 proc. pen; diminuzione di cui invece l'imputato potrebbe usufruire, a    &#13;
 norma dell'art. 448 cod. proc. pen., in caso di dissenso del pubblico    &#13;
 ministero all'applicazione della pena,  ritenuto  ingiustificato  dal    &#13;
 giudice di appello;                                                      &#13;
     che   a   parere   del  giudice  rimettente  l'impossibilità  di    &#13;
 disciplinare il caso in esame mediante un meccanismo analogo a quello    &#13;
 previsto dall'art. 448 cod. proc. pen.,  che  consente,  appunto,  di    &#13;
 applicare  la  pena  nella  misura  richiesta dall'imputato quando il    &#13;
 giudice ritenga ingiustificato il dissenso del pubblico  ministero  e    &#13;
 congrua  la  pena  stessa,  determina una irragionevole disparità di    &#13;
 trattamento, in quanto anche il caso di specie è  caratterizzato  da    &#13;
 una  "ingiustificata  mancata  applicazione  della ... diminuzione di    &#13;
 pena ricollegata alla richiesta di  patteggiamento";                     &#13;
     che nel giudizio è intervenuto il Presidente del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, che ha concluso chiedendo  che  la  questione  sia  dichiarata    &#13;
 inammissibile  e  comunque  infondata,  richiamando,  tra l'altro, il    &#13;
 principio, affermato dalla sentenza della Corte di  cassazione,  sez.    &#13;
 VI,  2 giugno 1992, Cannone, secondo cui in sede di impugnazione deve    &#13;
 essere  comunque  riconosciuto  all'imputato  che  ne   abbia   fatto    &#13;
 tempestivamente  richiesta  nel  giudizio  di primo grado ex art. 444    &#13;
 cod. proc. pen., anche fuori del caso di dissenso ingiustificato  del    &#13;
 pubblico  ministero,  il diritto alla riduzione della pena, quando il    &#13;
 giudice riconosce che la richiesta era fondata in relazione sia  alla    &#13;
 qualifica del reato che alla pena da applicare.                          &#13;
   Considerato  che  il giudice rimettente, nel sollevare la questione    &#13;
 di legittimità costituzionale relativa all'art. 448 cod. proc. pen.,    &#13;
 si  basa  sul  presupposto  che  la  norma  censurata   precluda   la    &#13;
 possibilità  di  procedere  all'applicazione della pena richiesta ex    &#13;
 art. 444 cod.  proc. pen. anche nel caso in cui  tale  pena  non  sia    &#13;
 stata  ingiustificatamente  applicata  dal giudice di primo grado per    &#13;
 effetto dell'erronea pronuncia di una sentenza di assoluzione  emessa    &#13;
 a norma degli artt. 129 e 530, comma 2, cod. proc. pen;                  &#13;
     che  tale  presupposto  interpretativo è peraltro smentito dallo    &#13;
 specifico precedente, citato dall'Avvocatura dello Stato, della Corte    &#13;
 di cassazione, la quale ha affermato il  principio  che  in  sede  di    &#13;
 impugnazione  deve  comunque  essere riconosciuto all'imputato che ne    &#13;
 abbia fatto richiesta ex art.  444,  comma  1,  cod.  proc.  pen.  il    &#13;
 diritto  alla  diminuzione della pena, anche al di fuori dell'ipotesi    &#13;
 di dissenso ingiustificato del pubblico ministero, quando il  giudice    &#13;
 riconosca  che  la  richiesta  era  fondata  in  relazione  sia  alla    &#13;
 qualificazione giuridica del fatto, sia alla pena da applicare;          &#13;
     che, comunque, la norma censurata è stata  modificata  dall'art.    &#13;
 34  della  legge  16 dicembre 1999, n. 479, pubblicata nella Gazzetta    &#13;
 Ufficiale n. 296 del 18 dicembre 1999;                                   &#13;
     che  la  nuova  formulazione  dell'art.  448, comma 1, cod. proc.    &#13;
 pen. prevede espressamente, per quanto interessa il  caso  in  esame,    &#13;
 che nel giudizio di impugnazione il giudice può pronunciare sentenza    &#13;
 di  applicazione della pena anche nel caso di rigetto della richiesta    &#13;
 formulata ex art. 444, comma 1, cod. proc. pen;                          &#13;
     che pertanto va disposta la restituzione degli  atti  al  giudice    &#13;
 rimettente  perché  valuti  se, a seguito della intervenuta modifica    &#13;
 legislativa  della   disposizione   denunciata,   la   questione   di    &#13;
 legittimità sia tuttora rilevante.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Ordina la restituzione degli atti alla Corte di appello di Venezia.    &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 gennaio 2000.                              &#13;
                        Il Presidente: Vassalli                           &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 21 gennaio 2000.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
