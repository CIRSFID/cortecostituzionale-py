<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>309</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:309</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2001</data_decisione>
    <data_deposito>25/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  ammissibilità  del  conflitto di attribuzione tra &#13;
poteri  dello  Stato  sorto  a  seguito  degli  atti  con  i quali il &#13;
Consiglio  superiore  della  magistratura,  a far data dal 3 dicembre &#13;
1998,  ha  adottato  provvedimenti  incidenti  sullo stato giuridico, &#13;
sulla  assegnazione  di sede e/o di funzioni al giudice del tribunale &#13;
di  Lecce dott. Francesco Manzo, promosso da quest'ultimo in qualità &#13;
di giudice per le indagini preliminari presso il tribunale di Lecce e &#13;
di  giudice  della  I  sezione  penale  dello  stesso  tribunale,  in &#13;
composizione  monocratica,  con ricorso depositato il 24 gennaio 2001 &#13;
ed iscritto al n. 177 del registro ammissibilità conflitti. &#13;
    Udito  nella  camera  di  consiglio  del 9 maggio 2001 il giudice &#13;
relatore Carlo Mezzanotte. &#13;
    Ritenuto  che  il dott. Francesco Manzo, che dichiara di agire in &#13;
qualità  di  giudice per le indagini preliminari presso il tribunale &#13;
di  Lecce e di giudice della I sezione penale del medesimo tribunale, &#13;
in  composizione  monocratica,  ha  proposto ricorso per conflitto di &#13;
attribuzione  tra  poteri  dello  Stato contro il Consiglio superiore &#13;
della  magistratura, per far dichiarare a questa Corte che, a partire &#13;
dal 3 dicembre 1998, non spettava e non spetta al Consiglio superiore &#13;
della  magistratura  nell'attuale composizione adottare provvedimenti &#13;
che  comunque  incidano  sullo  stato giuridico, sull'assegnazione di &#13;
sede  e/o  di  funzioni  al  giudice  dott. Francesco Manzo e per far &#13;
conseguentemente  annullare  tutti  gli atti emessi medio tempore nei &#13;
suoi  confronti,  in  quanto  tali  atti  risulterebbero lesivi delle &#13;
prerogative costituzionali di indipendenza, autonomia, inamovibilità &#13;
e difesa in giudizio degli organi giurisdizionali (artt. 24, 101, 104 &#13;
e 107 della Costituzione); &#13;
        che  il  ricorrente  riferisce  che in data 3 dicembre 1998 i &#13;
suoi  legali gli avevano comunicato che l'allora procuratore generale &#13;
aggiunto  presso  la  Corte  di  cassazione  lo invitava ad essere in &#13;
futuro  "meno  rissoso",  altrimenti  il  Consiglio  superiore  della &#13;
magistratura avrebbe preso provvedimenti punitivi nei suoi confronti; &#13;
        che   il   24 novembre  1999  il  Consiglio  superiore  della &#13;
magistratura   deliberava   la  non  promozione  del  dott.  Manzo  a &#13;
magistrato  di  Corte d'appello ed in seguito, secondo il ricorrente, &#13;
esercitava  pressioni  sul  Consiglio  giudiziario  presso  la  Corte &#13;
d'appello  di  Lecce, fino ad indurre tale organo a rivedere in senso &#13;
negativo  un precedente parere, reso all'unanimità, che prevedeva la &#13;
riassegnazione del dott. Manzo all'ufficio di giudice per le indagini &#13;
preliminari; &#13;
        che  in  relazione  a  tali  atti  il dott. Manzo chiamava in &#13;
giudizio  dinanzi  al  tribunale  di Potenza, per il risarcimento dei &#13;
danni  patiti, i componenti del Consiglio giudiziario presso la Corte &#13;
d'appello  di  Lecce  e,  dinanzi  al  tribunale  civile  di Roma, il &#13;
Consiglio superiore della magistratura, per sentir dichiarare che "il &#13;
Consiglio  convenuto  aveva  posto  in  essere nei suoi confronti, in &#13;
attuazione  di  una  premeditata  attività di mobbing, ripetuti atti &#13;
illegittimi,  illeciti  e  vessatori  che ne avevano infine minato la &#13;
salute"  chiedendo,  anche  in questo caso, il risarcimento dei danni &#13;
patrimoniali, non patrimoniali e morali; &#13;
        che  in  data  20 aprile 2000, la I Commissione del Consiglio &#13;
superiore della magistratura comunicava l'apertura in danno del dott. &#13;
Francesco  Manzo  del  procedimento  per  trasferimento d'ufficio per &#13;
incompatibilità  ambientale  ex art. 2 del regio decreto legislativo &#13;
31 maggio  1946,  n. 511  (Guarentigie  della  magistratura) e che in &#13;
seguito  il  Consiglio superiore della magistratura, con delibera del &#13;
10 maggio 2000, ne disponeva la rimozione dall'ufficio di giudice per &#13;
le indagini preliminari e lo assegnava alla I sezione penale; &#13;
        che il dott. Manzo, assumendo che le sue condizioni di salute &#13;
fossero  gravemente  peggiorate  in  seguito  a  tale  rimozione, con &#13;
querela  del  10 ottobre 2000 denunciava tutti i componenti in carica &#13;
del Consiglio superiore della magistratura, e chiunque altro ritenuto &#13;
responsabile,  per  "abuso  d'ufficio e lesioni personali gravi" alla &#13;
Procura della Repubblica presso il tribunale di Potenza; &#13;
        che,  quanto  alla  sussistenza  del  requisito oggettivo del &#13;
conflitto,  il  ricorrente  lamenta  che  tutti gli atti di cui si è &#13;
sopra  detto  siano  stati  adottati  con  "parzialità  e  malanimo" &#13;
dall'organo  di  autogoverno  della  magistratura, così da ledere le &#13;
prerogative costituzionali di indipendenza, autonomia, inamovibilità &#13;
e   difesa   in   giudizio  degli  organi  giurisdizionali  che  egli &#13;
rappresenta; &#13;
        che su tali premesse, il dott. Manzo chiede a questa Corte di &#13;
dichiarare  che  gli attuali componenti del Consiglio superiore della &#13;
magistratura,  a partire dal 3 dicembre 1998, siano incompatibili (ai &#13;
sensi  dell'art. 51  del  codice  di  procedura civile) rispetto alla &#13;
adozione  di  ogni  atto  incidente sul proprio status giuridico e di &#13;
annullare conseguentemente tutti gli atti emessi medio tempore; &#13;
        che,  secondo il ricorrente, non varrebbe a precludere la via &#13;
della  tutela  costituzionale delle prerogative della magistratura la &#13;
circostanza  che  i  medesimi  atti  che  sono  oggetto  del presente &#13;
conflitto   potrebbero   essere   impugnati   nelle   ordinarie  sedi &#13;
giurisdizionali, in quanto i due rimedi si differenzierebbero sia per &#13;
gli  interessi  alla  cui  protezione  sono  preordinati,  sia per la &#13;
titolarità e portata della tutela; &#13;
        che,  in  particolare,  ricorrente nel presente conflitto non &#13;
sarebbe  il  dott.  Manzo,  ma  gli  organi  giurisdizionali che egli &#13;
rappresenta,  i  quali  potrebbero  ben difendere le loro prerogative &#13;
costituzionali,  indipendentemente  dalla eventuale tutela azionabile &#13;
dal  funzionario  titolare  dell'Ufficio  giurisdizionale per ragioni &#13;
inerenti al suo rapporto di impiego; &#13;
        che  inoltre vizi di merito degli atti sarebbero denunciabili &#13;
solo  nella sede del conflitto costituzionale, essendo sottratti alla &#13;
cognizione del giudice comune; &#13;
        che   infine,   quanto   alla  legittimazione  soggettiva  al &#13;
conflitto,  il dott. Manzo sostiene che questa Corte, con la sentenza &#13;
n. 497  del  2000, avrebbe ammesso la possibilità per singoli organi &#13;
giurisdizionali  di sollevare conflitto di attribuzione nei confronti &#13;
del Consiglio superiore della magistratura. &#13;
    Considerato  che  il  ricorrente  assume  in  definitiva  che  la &#13;
particolare  posizione di indipendenza riconosciuta al magistrato sia &#13;
tutelabile,  in  sede  di  conflitto davanti a questa Corte, tutte le &#13;
volte   in   cui   siano   adottati  dal  Consiglio  superiore  della &#13;
magistratura atti incidenti sul suo status professionale; &#13;
        che  deve invece rilevarsi che nei confronti degli atti con i &#13;
quali il Consiglio superiore della magistratura, nell'esercizio delle &#13;
attribuzioni  conferitegli  dall'art. 105 della Costituzione, dispone &#13;
assunzioni,  assegnazioni,  trasferimenti  e  promozioni,  i  singoli &#13;
magistrati  che  se  ne  assumano lesi non possono opporre la propria &#13;
posizione  di  potere  dello  Stato,  ma  solo la propria qualità di &#13;
persone,  titolari  di  diritti  e  di interessi legittimi che devono &#13;
essere fatti valere dinanzi alle giurisdizioni comuni; &#13;
        che  nessuna  indicazione  in senso diverso può trarsi dalla &#13;
sentenza  n. 497  del  2000,  evocata dal ricorrente, con la quale è &#13;
stata  dichiarata  l'illegittimità  costituzionale dell'articolo 34, &#13;
secondo  comma,  del regio decreto legislativo 31 maggio 1946, n. 511 &#13;
(Guarentigie  della  magistratura), nella parte in cui esclude che il &#13;
magistrato   sottoposto   a  procedimento  disciplinare  possa  farsi &#13;
assistere da un avvocato del libero foro; &#13;
        che  l'esigenza  di  ampliamento delle opportunità di difesa &#13;
nei  procedimenti  disciplinari  innanzi al Consiglio superiore della &#13;
magistratura, alla quale la Corte ha riconosciuto in quella decisione &#13;
carattere  di cogenza, consegue, fra l'altro, alla considerazione che &#13;
quei  procedimenti  sono suscettibili di incidere sulla posizione del &#13;
soggetto  nella  vita lavorativa e quindi su beni della persona tra i &#13;
quali,  nel  caso dei magistrati, è compresa l'indipendenza inerente &#13;
al loro status professionale; &#13;
        che,  una volta ribadito che la tematica posta dal ricorrente &#13;
va inquadrata in termini di diritto di difesa del magistrato e non di &#13;
legittimazione  al  conflitto tra poteri, si deve solo aggiungere che &#13;
la  prospettiva nella quale si è collocata la citata sentenza non si &#13;
discosta  da quella assunta da questa Corte fin da quando fu posta la &#13;
questione se il buon adempimento della funzione affidata al Consiglio &#13;
superiore della magistratura postulasse la sottrazione di questo alla &#13;
interferenza  dei  soli  poteri politicamente attivi o anche a quella &#13;
del  potere  giurisdizionale:  questione  che  è  stata affrontata e &#13;
risolta  sulla  base  del  rilievo  che  la  tutela  giurisdizionale, &#13;
riconosciuta a tutti in conformità ad un principio coessenziale allo &#13;
stato  di  diritto,  non  poteva  essere  negata  ad una categoria di &#13;
cittadini,  i magistrati appunto, con l'effetto di lasciarli indifesi &#13;
di  fronte a provvedimenti del Consiglio superiore della magistratura &#13;
lesivi  dei  propri  diritti o interessi legittimi (v. sentenze n. 44 &#13;
del 1968 e n. 189 del 1992); &#13;
        che  altro  è  quindi affermare che lo status del magistrato &#13;
rende più stringente la necessità che la tutela giurisdizionale sia &#13;
piena  anche  nei  procedimenti  disciplinari, altro è dire che, nei &#13;
confronti  degli  atti  del  Consiglio  superiore  della magistratura &#13;
incidenti  su  quello  status,  il magistrato che ne sia destinatario &#13;
possa   essere   qualificato   potere   dello   Stato:   quest'ultima &#13;
affermazione, che trascenderebbe largamente il significato e l'ambito &#13;
che   la   Costituzione   assegna   al   conflitto   di  attribuzione &#13;
trasformandolo  in  mezzo  di  impugnazione  generale  degli atti del &#13;
Consiglio   superiore   della   magistratura,   non   rinviene  nella &#13;
giurisprudenza costituzionale alcun plausibile fondamento; &#13;
        che,  pertanto, il ricorso per conflitto di attribuzione deve &#13;
essere dichiarato inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  inammissibile  il ricorso per conflitto di attribuzione &#13;
di cui in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 12 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                      Il redattore: Mezzanotte &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 25 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
