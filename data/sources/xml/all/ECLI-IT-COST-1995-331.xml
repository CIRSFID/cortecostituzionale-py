<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>331</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:331</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/07/1995</data_decisione>
    <data_deposito>17/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  59,  primo    &#13;
 comma, della legge 27 luglio 1978, n. 392 (Disciplina delle locazioni    &#13;
 di  immobili  urbani),  promosso  con ordinanza emessa il 23 novembre    &#13;
 1994 dal Pretore di Bologna  nel  procedimento  civile  vertente  tra    &#13;
 Leonida  Guio  e  Donato  Losurato  ed  altra,  iscritta al n. 48 del    &#13;
 registro ordinanze 1995 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 6, prima serie speciale, dell'anno 1995;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di consiglio del  14  giugno  1995  il  Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Ritenuto  che  il  Pretore  di Bologna, con ordinanza emessa il 23    &#13;
 novembre  1994,  ha  sollevato,  in  riferimento  all'art.  3   della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art. 59,    &#13;
 primo comma, della legge 27 luglio 1978,  n.  392  (Disciplina  delle    &#13;
 locazioni di immobili urbani), nella parte in cui consente il recesso    &#13;
 del locatore soltanto "nei casi di cui all'articolo precedente", vale    &#13;
 a dire per i contratti in corso al momento di entrata in vigore della    &#13;
 legge  e  soggetti  a proroga secondo la disciplina allora vigente, e    &#13;
 non anche in ogni altro caso di proroga legale;                          &#13;
     che la questione è stata sollevata nel corso di un giudizio  sul    &#13;
 recesso  del  locatore,  per  necessità del figlio, dal contratto di    &#13;
 locazione ad uso di abitazione stipulato  dopo  l'entrata  in  vigore    &#13;
 della  legge  n.  392  del 1978 e soggetto alla proroga legale per un    &#13;
 biennio disposta, nel contesto di misure urgenti per  il  risanamento    &#13;
 della  finanza pubblica, dall'art. 11, comma 2-bis, del decreto-legge    &#13;
 11 luglio 1992, n. 333,  introdotto  dalla  legge  di  conversione  8    &#13;
 agosto 1992, n. 359;                                                     &#13;
      che  il  giudice  rimettente,  considerando  la norma denunciata    &#13;
 operante solo per i contratti in corso  al  momento  dell'entrata  in    &#13;
 vigore della legge n. 392 del 1978, ritiene che essa sia in contrasto    &#13;
 con  l'art.  3  della  Costituzione,  in quanto irragionevolmente non    &#13;
 consentirebbe di  applicare  l'istituto  del  recesso,  compiutamente    &#13;
 disciplinato dalla norma stessa, ad ogni altro caso di proroga legale    &#13;
 del  contratto  di  locazione  e  quindi  anche alla proroga biennale    &#13;
 disposta dall'art. 11, comma 2-bis,  del  decreto-legge  n.  333  del    &#13;
 1992,  pur  in  presenza  di  identiche  necessità  del  locatore di    &#13;
 ottenere l'immediata disponibilità dell'alloggio;                       &#13;
      che è intervenuto il Presidente  del  Consiglio  dei  Ministri,    &#13;
 rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato,    &#13;
 chiedendo che la questione sia dichiarata infondata;                     &#13;
    Considerato che l'ordinanza di rimessione  muove  dal  presupposto    &#13;
 interpretativo  dell'inapplicabilità  dell'istituto  del recesso per    &#13;
 necessità  del  locatore  nel  corso  della  proroga  biennale   del    &#13;
 contratto  di  locazione,  stabilita  dall'art.  11, comma 2-bis, del    &#13;
 decreto-legge n. 333 del 1992, e chiede l'estensione della disciplina    &#13;
 del recesso, prevista dall'art. 59 della legge n. 392 del 1978 per  i    &#13;
 contratti  compresi  nel  regime transitorio di quella legge, ad ogni    &#13;
 ipotesi di contratto soggetto a proroga legale;                          &#13;
      che   la   necessità  del  locatore  di  diretta  utilizzazione    &#13;
 dell'abitazione come causa di cessazione  della  proroga  legale  dei    &#13;
 contratti  di  locazione  ha  assunto,  nella  comune interpretazione    &#13;
 adeguatrice (sentenza n. 132 del 1972), funzione di strumento per  la    &#13;
 composizione dei contrapposti interessi, rimanendo sacrificati quelli    &#13;
 dei  conduttori,  altrimenti  prevalenti,  di fronte all'esigenza del    &#13;
 locatore proprietario di ottenere  la  disponibilità  dell'immobile,    &#13;
 secondo   un   principio  di  necessaria  applicazione  del  recesso,    &#13;
 elaborato dalla giurisprudenza costituzionale (sentenze  n.  291  del    &#13;
 1987  e  n.  22  del  1980)  ed affermato anche nella legislazione di    &#13;
 settore;                                                                 &#13;
      che  nel  caso  della  proroga  legale  oggetto   del   giudizio    &#13;
 principale,  disposta dall'art. 11, comma 2-bis, del decreto-legge n.    &#13;
 333 del 1992 per favorire gli accordi tra le  parti  in  deroga  alla    &#13;
 disciplina  legale  del  canone  nella  fase  di transizione verso la    &#13;
 libera determinazione del canone stesso, già trova  applicazione  il    &#13;
 principio  del  recesso  per  necessità del locatore. Difatti, nello    &#13;
 specifico contesto normativo in cui si colloca l'art.  11,  comma  2-bis,  del  decreto-legge n. 333 del 1992, il rinvio all'art. 59 della    &#13;
 legge n. 392 del 1978 individua le ipotesi che, stipulato un patto in    &#13;
 deroga, legittimano il diniego di rinnovare il contratto  alla  prima    &#13;
 scadenza,  secondo  un  principio da ritenere egualmente ed a maggior    &#13;
 ragione applicabile, in base all'interpretazione del sistema, quando,    &#13;
 in presenza di proroga biennale, debba essere garantita  al  locatore    &#13;
 che   ne   ha   necessità   la   possibilità   di  rientrare  nella    &#13;
 disponibilità dell'abitazione (sentenza n. 323 del  1993;  ordinanza    &#13;
 n. 226 del 1994);                                                        &#13;
      che  in  base  alla  corretta  interpretazione del sistema resta    &#13;
 superato il presupposto interpretativo dal quale muove l'ordinanza di    &#13;
 rimessione, sicché la questione di legittimità costituzionale  deve    &#13;
 essere dichiarata manifestamente infondata;                              &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 59, primo comma, della legge 27 luglio 1978,    &#13;
 n. 392 (Disciplina delle locazioni di immobili urbani), sollevata, in    &#13;
 riferimento all'art. 3 della Costituzione, dal Pretore di Bologna con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 10 luglio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 17 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
