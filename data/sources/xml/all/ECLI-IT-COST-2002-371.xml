<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>371</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:371</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>10/07/2002</data_decisione>
    <data_deposito>18/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 455 del codice &#13;
di procedura penale, promosso, nell'ambito di un procedimento penale, &#13;
dal Tribunale di Avellino con ordinanza del 6 novembre 2001, iscritta &#13;
al  n. 31  del  registro  ordinanze  2002 e pubblicata nella Gazzetta &#13;
Ufficiale della Repubblica n. 5, 1ª serie speciale, dell'anno 2002. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio  del 3 luglio 2002 il giudice &#13;
relatore Guido Neppi Modona. &#13;
    Ritenuto  che  con  ordinanza del 6 novembre 2001 il Tribunale di &#13;
Avellino  ha  sollevato,  in  riferimento  agli  artt. 24 e 111 della &#13;
Costituzione,  questione di legittimità costituzionale dell'art. 455 &#13;
del  codice  di procedura penale, nella parte in cui "non prevede che &#13;
il  giudice per le indagini preliminari, prima di emettere decreto di &#13;
giudizio   immediato   o  di  rigettare  la  richiesta  del  pubblico &#13;
ministero,  debba  consentire  l'intervento  della difesa, sia pure a &#13;
livello meramente cartolare"; &#13;
        che  il  tribunale  -  che  procede  a  seguito di decreto di &#13;
giudizio  immediato emesso dal giudice per le indagini preliminari su &#13;
richiesta  del  pubblico  ministero  premette  che  il  difensore  ha &#13;
eccepito  l'illegittimità  costituzionaledegli  artt. 453, 454 e 455 &#13;
cod.  proc.  pen.,  in relazione agli artt. 3, 24, 25 e 111 Cost., in &#13;
quanto  consentono  l'emissione  del decreto di giudizio immediato in &#13;
assenza  di  contraddittorio con la difesa, che, "se sentita, avrebbe &#13;
potuto contribuire ad orientare le determinazioni del giudicante"; &#13;
        che  ad avviso del rimettente la fase processuale conseguente &#13;
alla  richiesta  del pubblico ministero di giudizio immediato, che si &#13;
svolge  effettivamente "in assenza di ogni forma di contraddittorio e &#13;
senza possibilità alcuna, per la difesa, di interloquire", se poteva &#13;
conciliarsi  con il sistema normativo anteriore all'entrata in vigore &#13;
della legge costituzionale che ha modificato l'art. 111 Cost., appare &#13;
ora in evidente distonia con i principi del giusto processo; &#13;
        che le recenti riforme legislative (quali la legge sul giusto &#13;
processo, sulla difesa d'ufficio, sulle indagini difensive) sarebbero &#13;
appunto  volte  a  garantire  l'effettività del diritto di difesa in &#13;
ogni  stato  e  grado  del  procedimento  e  ad  assicurare  il pieno &#13;
contraddittorio  e  la  parità  delle  parti  sin  dalla  fase delle &#13;
indagini  preliminari,  dando  così attuazione ai principi enunciati &#13;
dall'art. 111  Cost.  "in ogni fase del procedimento, come emerge dal &#13;
contenuto  del  terzo  comma  [...]  che  attiene anche alle indagini &#13;
preliminari"; &#13;
        che,    anche    ove   "si   volesse   dissentire   da   tale &#13;
interpretazione",  non  vi  sarebbe  dubbio  che  "la  richiesta  del &#13;
pubblico  ministero  di  emissione del decreto di giudizio immediato, &#13;
integrando  una delle possibili forme di esercizio dell'azione penale &#13;
[...], determini il sorgere della fase processuale in senso proprio"; &#13;
        che  tale fase non potrebbe quindi prescindere dalle garanzie &#13;
del   contraddittorio  e  della  parità  tra  le  parti,  mentre  la &#13;
disciplina  censurata  "consente  l'emissione del decreto di giudizio &#13;
immediato  sulla  base  della  sola richiesta del pubblico ministero, &#13;
senza  alcuna possibilità di contraddittorio con la difesa, sia pure &#13;
a livello meramente cartolare"; &#13;
        che,  quanto  alla  rilevanza  della questione, il rimettente &#13;
precisa che "l'accoglimento della stessa comporterebbe la nullità di &#13;
ordine  generale  del  decreto di giudizio immediato e la regressione &#13;
del procedimento"; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo  che  la  questione  sia  dichiarata infondata, sul &#13;
presupposto che prima di richiedere il giudizio immediato il pubblico &#13;
ministero   debba   comunque   notificare  all'imputato  l'avviso  di &#13;
conclusione  delle indagini di cui all'art. 415-bis cod. proc. pen. e &#13;
in quanto l'art. 111 Cost., "nel prevedere l'effettività del diritto &#13;
di difesa", fa espresso riferimento alla sola fase del processo. &#13;
    Considerato  che il rimettente vorrebbe che l'art. 455 cod. proc. &#13;
pen. abilitasse  la  difesa  ad  interloquire,  sia pure mediante una &#13;
forma  di  contraddittorio  meramente "cartolare", sulla richiesta di &#13;
giudizio immediato del pubblico ministero; &#13;
        che  la  disciplina  censurata  violerebbe gli artt. 24 e 111 &#13;
Cost.,  grazie  ai  quali  l'effettività  del diritto di difesa deve &#13;
essere  garantita  in  ogni stato e grado del procedimento e il pieno &#13;
contraddittorio e la parità delle parti dovrebbero essere assicurati &#13;
sin dalla fase delle indagini preliminari; &#13;
        che   l'emissione   del   decreto   di   giudizio  immediato, &#13;
determinando l'inizio della fase processuale e costituendo il momento &#13;
di passaggio al dibattimento, non potrebbe comunque prescindere dalla &#13;
garanzia  del contraddittorio tra le parti, in condizioni di parità, &#13;
secondo quanto disposto dal secondo comma dell'art. 111 Cost; &#13;
        che,   diversamente   da  quanto  ritiene  il  rimettente,  i &#13;
presupposti  e  la  peculiare  struttura  del  giudizio immediato non &#13;
privano   la   difesa   della   possibilità  di  interloquire  prima &#13;
dell'emissione del decreto che dispone tale giudizio; &#13;
        che, infatti, il pubblico ministero può presentare richiesta &#13;
di giudizio immediato solo se, secondo quanto disposto dall'art. 453, &#13;
comma  1,  cod.  proc.  pen., la persona sottoposta alle indagini sia &#13;
stata  interrogata  sui  fatti  da cui emerge l'evidenza della prova, &#13;
ovvero  se  -  a  seguito  di  invito  a  presentarsi  emesso a norma &#13;
dell'art. 375, comma 3, secondo periodo, cod. proc. pen. e contenente &#13;
anche  l'indicazione  degli  elementi  e  delle  fonti da cui risulta &#13;
l'evidenza  della prova e l'avvertimento che potrà essere presentata &#13;
richiesta  di  giudizio  immediato  -  la  persona  indagata  non sia &#13;
comparsa, sempre che non abbia addotto un legittimo impedimento o non &#13;
sia irreperibile; &#13;
        che,  attraverso  l'interrogatorio svolto con l'osservanza di &#13;
tali  garanzie,  la  persona  sottoposta  alle  indagini  è posta in &#13;
condizione  di  esercitare le più opportune iniziative defensionali, &#13;
anche   mediante   la   presentazione  al  giudice  per  le  indagini &#13;
preliminari  di  memorie  ex  art. 121  cod.  proc.  pen., al fine di &#13;
contestare  la  fondatezza  dell'accusa  e,  quindi,  di  contrastare &#13;
l'eventuale  emissione  del decreto che dispone il giudizio immediato &#13;
(v. ordinanza n. 203 del 2002); &#13;
        che  quella  forma  di contraddittorio, quantomeno cartolare, &#13;
che  ad  avviso del giudice rimettente consentirebbe di porre rimedio &#13;
alla  supposta  incostituzionalità  della  norma  censurata, risulta &#13;
pertanto già assicurata dalla disciplina vigente; &#13;
        che  al  giudice  del dibattimento è altresì attribuito, ex &#13;
artt. 178,  comma  1, lettera c), e 180 cod. proc. pen., il potere di &#13;
sindacare  la  ritualità, formale e sostanziale, del presupposto del &#13;
previo  interrogatorio,  per  la  cui  validità  è  necessario  che &#13;
all'imputato,  con specifico riferimento al fatto per cui è tratto a &#13;
giudizio,  siano  state effettivamente contestate le prove d'accusa e &#13;
sia  stata  effettivamente  offerta  la  possibilità  di  esporre le &#13;
proprie linee difensive; &#13;
        che,  sotto  il  profilo  della possibilità di esercitare il &#13;
diritto  di  difesa  al  fine  di evitare l'emissione del decreto che &#13;
dispone  il  giudizio  immediato,  non è pertanto ravvisabile alcuna &#13;
violazione dei parametri evocati; &#13;
        che,   quanto   alle   censure   formulate   in   riferimento &#13;
all'art. 111,   secondo   comma,   Cost.,   questa   Corte  ha  avuto &#13;
recentemente  occasione di affermare che il principio per il quale il &#13;
processo   deve  svolgersi  nel  contraddittorio  tra  le  parti,  in &#13;
condizioni  di  parità,  non  è  evocabile  in relazione alle forme &#13;
introduttive  del  giudizio  (v.,  per  quanto  riguarda  il giudizio &#13;
abbreviato,  sentenza n. 115 del 2001), le quali, per quanto concerne &#13;
il   giudizio  immediato,  trovano  giustificazione  nelle  peculiari &#13;
esigenze  di  celerità  e  di  risparmio  di risorse processuali che &#13;
connotano tale rito alternativo (v. ordinanza n. 203 del 2002); &#13;
        che   la  questione  va  pertanto  dichiarata  manifestamente &#13;
infondata   in  relazione  ad  entrambi  i  parametri  costituzionali &#13;
richiamati dal rimettente. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art. 455  del  codice di procedura &#13;
penale,   sollevata,   in  riferimento  agli  artt. 24  e  111  della &#13;
Costituzione, dal Tribunale di Avellino, con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                     Il redattore: Neppi Modona &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 18 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
