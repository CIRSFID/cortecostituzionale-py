<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>80</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:80</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SPAGNOLI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/02/1995</data_decisione>
    <data_deposito>06/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Ugo SPAGNOLI; &#13;
 Giudici: prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi di legittimità costituzionale dell'art. 20 della legge 7    &#13;
 gennaio  1929,  n.  4  (Norme  generali  per  la  repressione   delle    &#13;
 violazioni delle leggi finanziarie), promossi con ordinanze emesse il    &#13;
 29  marzo e il 14 aprile 1994 dal Pretore di Catania nel procedimento    &#13;
 penale a carico di Mouduch Mohamed e di D'Amico  Luigi,  iscritta  ai    &#13;
 nn. 347 e 352 del registro ordinanze 1994 e pubblicate nella Gazzetta    &#13;
 Ufficiale  della  Repubblica  nn.  25  e  26,  prima  serie speciale,    &#13;
 dell'anno 1994;                                                          &#13;
    Visti gli atti di intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio dell'8 febbraio 1995 il Giudice    &#13;
 relatore Fernando Santosuosso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di due  procedimenti  penali  a  carico  di  Mohamed    &#13;
 Mouduch e D'Amico Luigi, imputati del delitto previsto e punito dagli    &#13;
 artt.  25, 282, 301 e 304 del d.P.R. 23 gennaio 1973, n. 43 per avere    &#13;
 commesso il reato di contrabbando di  tabacchi  lavorati  esteri,  il    &#13;
 Pretore  di  Catania  con  due  ordinanze  di  identico  contenuto ha    &#13;
 sollevato,  in  riferimento  all'art.  25,   secondo   comma,   della    &#13;
 Costituzione,  questione  di legittimità costituzionale dell'art. 20    &#13;
 della legge 7 gennaio 1929, n. 4 (Norme generali per  la  repressione    &#13;
 delle violazioni delle leggi finanziarie), nella parte in cui prevede    &#13;
 che  le  disposizioni  penali delle leggi finanziarie si applicano ai    &#13;
 fatti connessi durante la  loro  vigenza  ancorché  le  disposizioni    &#13;
 medesime  siano state successivamente abrogate o modificate. In punto    &#13;
 di rilevanza il giudice a quo osserva che agli imputati è addebitato    &#13;
 il fatto di avere commesso il reato di contrabbando di Kg 1,14 e  1,6    &#13;
 di   tabacchi   lavorati   esteri;   per   tale   condotta  ai  sensi    &#13;
 dell'impugnato art. 20 della legge n. 4 del 1929 dovrebbe  applicarsi    &#13;
 la  multa  prevista  dall'art.  282 del d.P.R. 23 gennaio 1973, n. 43    &#13;
 nonostante che tale norma sia stata modificata, in  epoca  successiva    &#13;
 al  momento del commesso reato, dalla legge 28 dicembre 1993, n. 562,    &#13;
 che non prevede più come reato le violazioni  finanziarie  punite  -    &#13;
 come  quelle  di specie - con la sola multa, e dalla legge 18 gennaio    &#13;
 1994,  n.  50,  in  base  alla  quale  costituisce  reato   solo   il    &#13;
 contrabbando di tabacchi lavorati esteri in quantità superiore ai 15    &#13;
 Kg.  Nel  merito, l'Autorità rimettente premette che il principio di    &#13;
 legalità espresso nell'art. 25, secondo comma,  della  Costituzione,    &#13;
 ai sensi del quale "nessuno può essere punito se non in forza di una    &#13;
 legge che sia entrata in vigore prima del fatto commesso", deve esser    &#13;
 interpretato  nel  senso  di ricomprendervi anche il principio di non    &#13;
 ultrattività; in altre parole, se nessuno può essere punito se  non    &#13;
 in forza di una legge, allo stesso modo, nessuno potrà essere punito    &#13;
 se  la  legge  non è più in vigore, indipendentemente dal fatto che    &#13;
 essa sia stata abrogata o non sia mai esistita.                          &#13;
    Il principio della non ultrattività dovrebbe  pertanto  ritenersi    &#13;
 intrinseco  al  principio  di  legalità,  e di conseguenza l'art. 20    &#13;
 della legge n. 4 del 1929, con il prevedere che le disposizioni delle    &#13;
 leggi penali finanziarie si applicano ai fatti  commessi  durante  la    &#13;
 loro  vigenza ancorché le disposizioni medesime siano state abrogate    &#13;
 o modificate, si porrebbe in contrasto con l'art. 25, secondo  comma,    &#13;
 della Costituzione così come interpretato.                              &#13;
    2.  -  Nel giudizio avanti alla Corte è intervenuto il Presidente    &#13;
 del Consiglio dei ministri  rappresentato  e  difeso  dall'Avvocatura    &#13;
 Generale  dello Stato concludendo per la manifesta infondatezza della    &#13;
 questione.                                                               &#13;
    Ha osservato la difesa erariale  che  l'art.  25,  secondo  comma,    &#13;
 della  Costituzione  pone  soltanto  il  divieto  della  legge penale    &#13;
 retroattiva ma non prescrive affatto, conformemente  all'insegnamento    &#13;
 dato   dalla   Corte   costituzionale   in   numerose   pronunce,  la    &#13;
 retroattività delle leggi sopravvenute più favorevoli al reo.<diritto>Considerato in diritto</diritto>1. - Il giudice a  quo  dubita  della  legittimità  costituzionale    &#13;
 dell'art.  20 della legge 7 gennaio 1929, n. 4 (Norme generali per la    &#13;
 repressione delle violazioni delle leggi finanziarie), nella parte in    &#13;
 cui prevede che le disposizioni delle  leggi  penali  finanziarie  si    &#13;
 applicano  ai  fatti  commessi  durante  la loro vigenza ancorché le    &#13;
 disposizioni medesime siano state, successivamente alla  consumazione    &#13;
 del reato, abrogate o modificate.                                        &#13;
    A  parere  del  rimettente tale previsione (pur ritenuta da questa    &#13;
 Corte costituzionalmente legittima con riguardo all'art. 3 sarebbe in    &#13;
 contrasto con  l'art.  25,  secondo  comma,  della  Costituzione  dal    &#13;
 momento  che nel principio di legalità deve ritenersi compreso anche    &#13;
 quello della non ultrattività della legge; e ciò in quanto, poiché    &#13;
 ai sensi della norma costituzionale richiamata "nessuno  può  essere    &#13;
 punito  se  non  in  forza di una legge", allo stesso modo non potrà    &#13;
 farsi luogo all'irrogazione della pena se la legge  non  è  più  in    &#13;
 vigore,  indipendentemente  dal  fatto  che  essa  sia stata abrogata    &#13;
 ovvero non sia mai esistita.                                             &#13;
    2. - La questione non è fondata.                                     &#13;
    L'impugnato art. 20 della legge n. 4 del 1929 sancisce in tema  di    &#13;
 successione  di  leggi  penali  tributarie  il  principio  della c.d.    &#13;
 ultrattività, ai sensi del quale  si  applica  sempre  la  legge  in    &#13;
 vigore  al momento del fatto, anche se essa sia stata successivamente    &#13;
 abrogata o modificata.                                                   &#13;
    Tale   principio   rappresenta   una   deroga   a   quello   della    &#13;
 retroattività della legge più favorevole al reo  stabilito  per  le    &#13;
 leggi penali comuni dall'art. 2, terzo comma, del codice penale.         &#13;
    Tuttavia,   affinché   possa  ritenersi  vulnerato  il  parametro    &#13;
 costituzionale invocato, è necessario dimostrare che la regola della    &#13;
 retroattività della legge penale favorevole  sia  stata  elevata  al    &#13;
 rango di principio costituzionale.                                       &#13;
    Dalla  lettura  dell'art.  25,  secondo comma, della Costituzione,    &#13;
 emerge, al contrario, che solo il  principio  della  irretroattività    &#13;
 della    legge    penale   incriminatrice   ha   acquistato   valenza    &#13;
 costituzionale ma non quello della retroattività  della  legge  più    &#13;
 favorevole  al  reo.  Da ciò consegue che, come deve essere ritenuto    &#13;
 conforme al richiamato disposto  costituzionale  il  principio  della    &#13;
 retroattività   della  disposizione  più  favorevole,  alla  stessa    &#13;
 conclusione dovrà pervenirsi in ordine alla  legge  che  preveda  la    &#13;
 irretroattività delle norme favorevoli.                                 &#13;
    In  altri  termini,  in  tema di successione nel tempo della legge    &#13;
 penale, il legislatore ordinario è vincolato solo al principio della    &#13;
 irretroattività della legge incriminatrice. Con particolare riguardo    &#13;
 alla ultrattività delle disposizioni penali delle leggi  finanziarie    &#13;
 relative  ai  tributi  dello  Stato,  sancita dall'impugnato art. 20,    &#13;
 questa Corte ha già avuto modo di affermare che essa  non  contrasta    &#13;
 con  il  principio  costituzionale  dell'art.  3,  e tanto meno, deve    &#13;
 essere  ora  ribadito,  con   l'art.   25,   secondo   comma,   della    &#13;
 Costituzione,  poiché  tale  norma  impone  solo  il  divieto  della    &#13;
 retroattività di nuove norme incriminatrici (fra le molte,  sentenza    &#13;
 n. 6 del 1978, ordinanze nn. 158 e 134 del 1977 e sentenza n. 164 del    &#13;
 1974).                                                                   &#13;
    Né,  infine,  può  essere condivisa l'affermazione del giudice a    &#13;
 quo secondo cui, di fronte al principio di legalità, è indifferente    &#13;
 che la legge non sia mai esistita o sia  stata  invece  abrogata,  in    &#13;
 quanto l'abrogazione presuppone l'esistenza e vigenza di una legge ed    &#13;
 opera inoltre con effetto ex nunc e non ex tunc.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 20 della legge 7 gennaio 1929, n. 4 (Norme generali per  la    &#13;
 repressione delle violazioni delle leggi finanziarie), in riferimento    &#13;
 all'art. 25, secondo comma, della Costituzione, sollevata dal Pretore    &#13;
 di Catania con le ordinanze indicate in epigrafe.                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 febbraio 1995.                             &#13;
                        Il Presidente: SPAGNOLI                           &#13;
                       Il redattore: SANTOSUOSSO                          &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 6 marzo 1995.                            &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
