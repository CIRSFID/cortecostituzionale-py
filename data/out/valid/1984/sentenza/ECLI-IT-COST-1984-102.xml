<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1984-04-05/102/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1984-04-05/102"/>
          <FRBRalias value="ECLI:IT:COST:1984:102" name="ECLI"/>
          <FRBRdate date="1984-04-05" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="102"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1984-04-05/102/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1984-04-05/102/ita@"/>
          <FRBRdate date="1984-04-05" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1984-04-05/102/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1984-04-05/102/ita@.xml"/>
          <FRBRdate date="1984-04-05" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1984-04-04" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1984-04-05" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="ELIA" href="/akn/ontology/person/it/ELIA" showAs="ELIA"/>
        <TLCPerson eId="leopoldoElia" href="/akn/ontology/person/it/leopoldoElia" showAs="LEOPOLDO ELIA"/>
        <TLCPerson eId="guglielmoRoehrssen" href="/akn/ontology/person/it/guglielmoRoehrssen" showAs="GUGLIELMO ROEHRSSEN"/>
        <TLCPerson eId="oronzoReale" href="/akn/ontology/person/it/oronzoReale" showAs="ORONZO REALE"/>
        <TLCPerson eId="albertoMalagugini" href="/akn/ontology/person/it/albertoMalagugini" showAs="ALBERTO MALAGUGINI"/>
        <TLCPerson eId="livioPaladin" href="/akn/ontology/person/it/livioPaladin" showAs="LIVIO PALADIN"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="VIRGILIO ANDRIOLI"/>
        <TLCPerson eId="giuseppeFerrari" href="/akn/ontology/person/it/giuseppeFerrari" showAs="GIUSEPPE FERRARI"/>
        <TLCPerson eId="francescoSaja" href="/akn/ontology/person/it/francescoSaja" showAs="FRANCESCO SAJA"/>
        <TLCPerson eId="giovanniConso" href="/akn/ontology/person/it/giovanniConso" showAs="GIOVANNI CONSO"/>
        <TLCPerson eId="ettoreGallo" href="/akn/ontology/person/it/ettoreGallo" showAs="ETTORE GALLO"/>
        <TLCPerson eId="aldoCorasaniti" href="/akn/ontology/person/it/aldoCorasaniti" showAs="ALDO CORASANITI"/>
        <TLCPerson eId="giovanniVitale" href="/akn/ontology/person/it/giovanniVitale" showAs="GIOVANNI VITALE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1984</akn4cc:anno>
        <akn4cc:numero>102</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1984:102</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>ELIA</akn4cc:presidente>
        <akn4cc:relatore>Francesco Saja</akn4cc:relatore>
        <akn4cc:data_decisione>1984-04-04</akn4cc:data_decisione>
        <akn4cc:data_deposito>1984-04-05</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge>, Presidente - Prof. <judge refersTo="#guglielmoRoehrssen">GUGLIELMO ROEHRSSEN</judge> - Avv. <judge refersTo="#oronzoReale">ORONZO REALE</judge> - Dott. BRUNETTO BUCCIARELLI 
 DUCCI - Avv. <judge refersTo="#albertoMalagugini">ALBERTO MALAGUGINI</judge> - Prof. <judge refersTo="#livioPaladin">LIVIO PALADIN</judge> - Prof. <judge refersTo="#virgilioAndrioli">VIRGILIO 
 ANDRIOLI</judge> - Prof. <judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge> - Dott. <judge refersTo="#francescoSaja">FRANCESCO SAJA</judge> - Prof. <judge refersTo="#giovanniConso">GIOVANNI CONSO</judge> - Prof. <judge refersTo="#ettoreGallo">ETTORE GALLO</judge> - Dott. <judge refersTo="#aldoCorasaniti">ALDO CORASANITI</judge>, Giudici.</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità  costituzionale  dell'art.  24,  comma  
 secondo, del t.u. leggi della Provincia di Bolzano 23 giugno 1970 n. 20  
 (ordinamento urbanistico provinciale), promosso con ordinanza emessa il  
 7  luglio  1977  dalla Corte di cassazione - Sezioni unite civili - sul  
 ricorso  del  Comune  di  Bressanone  contro  l'Amministrazione   delle  
 ferrovie dello Stato, iscritta al n.  573 del registro ordinanze 1977 e  
 pubblicata  nella  Gazzetta  Ufficiale della Repubblica n. 53 dell'anno  
 1978 e nel Bollettino ufficiale della Regione Trentino-Alto Adige n. 36  
 del 1978.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  
 ministri e della Provincia di Bolzano;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito  nell'udienza  pubblica  del  14  febbraio  1984  il  Giudice  
 relatore Francesco Saja;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>uditi l'avv.  Giuseppe  Guarino  per  la  Provincia  di  Bolzano  e  
 l'Avvocato dello Stato Emilio Sernicola per il Presidente del Consiglio  
 dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto:</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>  -  Con  citazione  del 9 settembre 1971 l'Amministrazione delle  
 ferrovie dello Stato, dopo aver esposto di aver ottenuto dal Comune  di  
 Bressanone  licenza  per la costruzione di due edifici di abitazione su  
 un'area del proprio demanio (area di pertinenza ferroviaria) e di avere  
 pagato una  parte  del  contributo  per  le  opere  di  urbanizzazione,  
 conveniva  il  detto  Comune  davanti al Tribunale di Trento, chiedendo  
 dichiararsi che i detti edifici non erano in realtà soggetti a licenza  
 edilizia e perciò non era neppure dovuta la contribuzione per le opere  
 di urbanizzazione, e chiedendo altresì la condanna  alla  restituzione  
 di quanto a tale titolo già pagato.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il   convenuto   si   costituiva   ed  eccepiva  il  difetto  della  
 giurisdizione ordinaria, invocando l'art.  24  del  testo  unico  delle  
 leggi  urbanistiche  della  Provincia  di Bolzano approvato con d.p. 23  
 giugno 1970 n. 20, nella parte in  cui  imponeva  la  licenza  edilizia  
 anche  per  tutti  gli  edifici da costruire sul demanio statale con la  
 sola eccezione delle opere per la difesa nazionale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il    Tribunale,   accogliendo   tale   eccezione,   riteneva   che  
 l'Amministrazione  statale  era  titolare  non  già  di   un   diritto  
 soggettivo  ma  di  un  mero  interesse legittimo e di conseguenza, con  
 decisione del 16 dicembre 1972,  declinava  la  propria  giurisdizione,  
 affermando quella del giudice amministrativo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La Corte d'appello di Trento per contro, con sentenza del 10 maggio  
 1974,  osservava  che  l'azione  era  fondata  non già sulla deduzione  
 dell'illegittimo esercizio  del  potere  da  parte  del  Comune  bensì  
 sull'assolata  carenza  del  potere  stesso  nonché  sulla  pretesa di  
 restituzione della somma già pagata; ciò che imponeva di ravvisare un  
 diritto soggettivo quale fondamento della causa petendi: ne  conseguiva  
 l'appartenenza  della  controversia  al  giudice  ordinario e quindi la  
 necessità di rimandare le parti davanti al tribunale ex art. 353  cod.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>proc. civ.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Contro  questa decisione il Comune proponeva ricorso per cassazione  
 e la Corte Suprema, con ordinanza del 7 luglio 1977 (reg. <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1977/573/!main">ord.  n.  573  
 del  1977</ref>  in G. U. n. 53 del 22 febbraio 1978), sollevava questione di  
 legittimità costituzionale della citata disposizione dell'art. 24 d.p.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>n.  20 del 1970.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ad avviso della Corte l'eventuale dichiarazione  di  illegittimità  
 costituzionale  di  questa  norma  avrebbe  consentito di ravvisare nei  
 fatti  di  causa  la  violazione  di  un  diritto  dell'Amministrazione  
 ferroviaria,   ciò   che  bastava  per  ritenere  la  rilevanza  della  
 questione. In proposito, la Corte ravvisava nella legislazione  statale  
 un  principio  generale di esclusione delle opere costruite dallo Stato  
 su aree del proprio demanio  dai  poteri  comunali  di  amministrazione  
 dell'attività  edilizia.  Questo  principio  generale  era  stato già  
 espresso dall'art. 29 della legge urbanistica 17 agosto 1942  n.  1150,  
 che  aveva  attribuito  al  Ministero  dei lavori pubblici il controllo  
 sulla  corrispondenza  delle  opere  progettate  dalle  amministrazioni  
 statali  agli  strumenti  urbanistici  locali,  onde  la giurisprudenza  
 civile aveva escluso ogni ingerenza dei comuni sulle opere eseguite  in  
 aree demaniali.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Successivamente,  l'<ref href="/akn/it/act/legge/stato/1967-08-06/765/!main#art_10">art.  10  della  legge  6 agosto 1967 n.   765</ref>,  
 modificando l'art. 31 della  citata  <ref href="/akn/it/act/legge/stato/1942/1150/!main">legge  n.  1150  del  1942</ref>,  aveva  
 stabilito  che,  per  dette  opere,  i  privati concessionari dovessero  
 chiedere la licenza  edilizia,  ma  che  le  amministrazioni  pubbliche  
 fossero  soggette  solo  al  controllo  di  rispondenza  agli strumenti  
 urbanistici esercitato dal Ministero dei lavori pubblici  d'intesa  con  
 le   amministrazioni   interessate   e  sentito  il  Comune.  La  legge  
 urbanistica 28 gennaio 1977 n. 10 aveva  confermato  nel  suo  art.  9,  
 ultimo  comma,  le  norme  di cui ai citati artt. 29 e 311. n. 1150 del  
 1942 e successive modificazioni.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il contrasto della norma impugnata con il detto principio  generale  
 dell'ordinamento  statale  sembrava  integrare,  per  conseguenza,  una  
 violazione dell'art. 11 dello Statuto  speciale  per  il  Trentino-Alto  
 Adige,  che attribuiva alle province la potestà legislativa in materia  
 urbanistica entro i limiti indicati dal precedente art.  4,  ossia  "in  
 armonia con i principi dell'ordinamento giuridico dello Stato".</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>  -  L'Azienda  autonoma  delle  ferrovie dello Stato interveniva  
 ripetendo  sostanzialmente  le  censure  già  formulate  dal   giudice  
 rimettente ed aggiungendo che il sopravvenuto art. 84 (recte 81) D.P.R.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>24</num>
            <content>
              <p>  luglio 1977 n.  616, riservando allo Stato, d'intesa con la regione  
 interessata,  l'accertamento  di conformità delle opere agli strumenti  
 ed alle norme urbanistiche, aveva confermato il principio generale già  
 esattamente identificato dalla Cassazione -                              
     3. -  La  Provincia  di  Bolzano,  costituitasi,  chiedeva  che  la  
 questione  fosse  dichiarata  inammissibile in quanto non rilevante nel  
 giudizio a quo, e, in subordine, infondata.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'irrilevanza  nel  giudizio  civile  risultava  dall'oggetto   del  
 giudizio stesso, concernente la ripetizione della somma pagata a titolo  
 di  contributo  per urbanizzazione, comunque dovuto ai sensi del quarto  
 comma  dell'impugnato  art.  24,  anche  se  il  secondo   comma,   che  
 prescriveva    la    licenza    edilizia,    fosse   stato   dichiarato  
 incostituzionale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Essa derivava altresì dalla  mancata  impugnazione  della  licenza  
 edilizia   davanti   al  giudice  amministrativo  e  dalla  conseguente  
 efficacia definitiva di essa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La Provincia osservava poi  che  l'amministrazione  urbanistica  di  
 tutto il territorio nazionale, comprese le aree del demanio statale era  
 affidata  dalla  legislazione  statale  ai comuni, ai quali spettava il  
 potere di adottare i piani regolatori generali, e dal <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-01-15/8/!main">D.P.R. 15 gennaio  
 1972 n.  8</ref> alle regioni, che esercitavano i poteri di controllo, e  che  
 non   era   pertanto   identificabile  nell'ordinamento  statale  alcun  
 principio  generale  di  riserva  all'amministrazione   statale   della  
 gestione urbanistica delle proprie aree demaniali.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>  - L'Avvocatura dello Stato depositava tardivamente, ossia senza  
 osservare il termine di dodici giorni liberi prima  dell'udienza  (art.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>10</num>
            <content>
              <p>  Norme integrative per i giudizi davanti alla Corte costituzionale),  
 una memoria illustrativa in difesa dell'Amministrazione delle ferrovie</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto:</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p> - Il primo comma dell'art. 24 del T.U. delle  leggi  provinciali  
 sull'ordinamento  urbanistico  approvato  con il decreto del Presidente  
 della Giunta provinciale di Bolzano il 23 giugno 1970 n. 20 dispone che  
 per eseguire nuove costruzioni edilizie deve  essere  chiesta  apposita  
 concessione  al sindaco del comune. Il secondo comma, nella sua seconda  
 parte, soggiunge che "per le opere da eseguirsi su  terreni  demaniali,  
 ad  eccezione  delle  opere  destinate  alla  difesa nazionale, è pure  
 richiesta la concessione". Il quadro normativo è completato dal quarto  
 comma,  il  quale  statuisce   che   "presupposto"   necessario   della  
 concessione    è   il   pagamento   di   un   contributo,   da   parte  
 dell'interessato,  per  le  opere  di  urbanizzazione,   nella   misura  
 stabilita dall'art. 36 dello stesso Testo unico.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il    giudice    a    quo,   accogliendo   un'eccezione   formulata  
 dall'Amministrazione ferroviaria  dello  Stato  già  nel  giudizio  di  
 secondo  grado,  ha denunciato il riportato secondo comma del cit. art.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>24</num>
            <content>
              <p>, per contrasto con gli artt.  4 e 11 dello  Statuto  speciale  della  
 Regione  Trentino  -  Alto  Adige  (riferito  ancora  dall'ordinanza di  
 rimessione alla 1. c. 26 febbraio 1948 n. 5,  mentre  questa  è  stata  
 sostituita dal t. u., approvato con <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-08-31/670/!main">D.P.R. 31 agosto 1972 n. 670</ref>, delle  
 leggi  costituzionali  concernenti  lo statuto medesimo) assumendone il  
 contrasto con "il principio generale dell'ordinamento giuridico statale  
 avente  ad  oggetto  l'autonomia  dello  Stato,  rispetto  ai   Comuni,  
 nell'attività di gestione di beni del proprio demanio".</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>  -  La  Provincia  autonoma di Bolzano ha eccepito l'irrilevanza  
 della proposta questione nel giudizio a quo.    Secondo  la  Provincia,  
 concernendo  fondamentalmente  il  giudizio  principale  l'onere  della  
 contribuzione per le opere di  urbanizzazione  ed  essendo  tale  onere  
 previsto  nel  quarto  comma  dell'art.  24  t.  u.  n.  20  del  1970,  
 l'indicazione da parte del giudice a quo  soltanto  del  secondo  comma  
 renderebbe irrilevante la questione stessa, in quanto, anche se cadesse  
 tale  ultima  disposizione, rimarrebbe pur sempre il ricordato onere di  
 contribuzione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'eccezione non può trovare accoglimento.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Invero, l'ordinanza di rimessione ha messo in risalto il  carattere  
 strumentale  dell'onere  suddetto  (vedasi, in particolare, pag. 11) il  
 cui adempimento, secondo la legge provinciale  in  oggetto  applicabile  
 alla specie (ma vedasi, successivamente, l'art. 8 - e in particolare la  
 lett.  F  -  della  l. p. 3 gennaio 1978 n. 1), costituisce presupposto  
 necessario e indispensabile dell'atto autorizzativo, che risulta quindi  
 privo di una propria autonomia. D'altra parte,  l'ordinanza  stessa  ha  
 precisato  che  il  giudizio principale aveva per oggetto una azione di  
 accertamento (negativo)  dell'obbligo  della  concessione  (e,  quindi,  
 dell'onere di contribuzione) nonché la repetitio di quanto già a tale  
 titolo indebitamente corrisposto.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ciò  esclude  che  possa  esservi  incertezza circa la prospettata  
 questione,  la  quale,  indipendentemente  dal  richiamo  esplicito   e  
 specifico  delle  singole  parti  della  ricordata  norma  provinciale,  
 risulta inequivocabilmente delineata, ossia riferita all'obbligo  della  
 concessione  per  tutte  le  costruzioni,  anche se da eseguire su beni  
 demaniali dello Stato, ed al connesso onere di contribuzione: pertanto,  
 deve concludersi che, risultando pienamente raggiunto lo scopo a cui è  
 preordinata l'indicazione della norma impugnata (<ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_23">art. 23  l.  11  marzo  
 1953  n.  87</ref>),  riesce  indifferente  il  mancato esplicito richiamo al  
 quarto comma dell'art. 24 t. u. ult. cit.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>  -  Va  quindi  esaminata  l'altra  eccezione  sollevata   dalla  
 Provincia,  secondo cui la proposta questione sarebbe irrilevante anche  
 sotto  un  diverso  profilo,  e  precisamente  perché  la  concessione  
 rilasciata  dal  Comune,  non  essendo  stata  impugnata,  è  divenuta  
 definitiva.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'eccezione, che si esaurisce in una  mera  affermazione  priva  di  
 qualsiasi elemento di sostegno, non può essere condivisa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Invero,  il  giudizio  principale  ha  per oggetto, come già si è  
 detto  e  come  pure  la  stessa  Provincia   di   Bolzano   riconosce,  
 l'accertamento  della  inesistenza dell'obbligo della concessione e del  
 connesso obbligo di  contribuzione  per  le  opere  di  urbanizzazione:</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>sicché  il  carattere definitivo della concessione stessa non solo non  
 contrasta con le domande proposte, ma sta alla base delle medesime,  in  
 quanto  esse hanno per presupposto una situazione giuridica conseguente  
 ad un procedimento amministrativo esaurito.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p> - Ai fini della rilevanza, deve invece la  Corte  osservare  che  
 nella fase del giudizio in cui è intervenuta l'ordinanza di rimessione  
 non  è  in discussione il merito della causa, ma soltanto la questione  
 relativa alla giurisdizione. Invero il Tribunale di Bolzano  ha  negato  
 la  propria  potestà  giurisdizionale,  rilevando che si controverteva  
 sull'esercizio del potere discrezionale del  Comune  di  rilasciare  la  
 concessione ed ha ritenuto che la causa apparteneva alla cognizione del  
 giudice amministrativo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La  Corte  di appello di Trento ha invece riformato tale decisione,  
 osservando trattarsi di una posizione di diritto soggettivo, in  quanto  
 l'Amministrazione  delle  ferrovie  negava  la sussistenza dell'obbligo  
 della concessione, così contestando la stessa esistenza  del  relativo  
 potere  del  Comune,  e  richiedeva conseguentemente la restituzione di  
 quanto, a suo avviso, indebitamente pagato a titolo di  contributo  per  
 le  opere  di  urbanizzazione:  perciò la Corte ha rimesso la causa al  
 primo giudice secondo la disposizione dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_353">art. 353 cod. proc. civ.</ref></p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Avverso questa decisione dal Comune di Bressanone è stato proposto  
 ricorso per cassazione limitatamente, com'è ovvio, all'unica questione  
 decisa e pertanto l'ambito del giudizio di legittimità è circoscritto  
 soltanto al problema di giurisdizione.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ciò posto, osserva la Corte che, se  nel  giudizio  principale  si  
 controverte  della  giurisdizione,  una questione di costituzionalità,  
 che sia rilevante, è configurabile soltanto se essa investe  la  norma  
 attributiva   della   potestà   giurisdizionale  ossia  se  dalla  sua  
 risoluzione dipende la sussistenza o meno nel  giudice  adito  di  tale  
 potestà.  Ma  questo  nesso  non  sussiste nel caso in esame, perché,  
 qualificata l'azione, come lo è stata dal giudice  a  quo,  nel  senso  
 suindicato    (accertamento   dell'insussistenza   dell'obbligo   della  
 concessione  e  quindi  del  relativo  potere  della  P.  A.    nonché  
 ripetizione dell'indebito), il petitum sostanziale concerne, secondo un  
 orientamento   che   costituisce   ius  receptum  nella  giurisprudenza  
 ordinaria, una posizione di diritto  soggettivo  (e  non  di  interesse  
 legittimo)  e  quindi  secondo  la  disciplina generale, a parte quanto  
 sarà detto nel numero successivo, la  cognizione  spetta  comunque  al  
 giudice ordinario.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il  giudice  a  quo  si  limita  in  proposito  ad affermare che la  
 questione di legittimità costituzionale del cit. art. 24 è  rilevante  
 in  quanto,  se  la norma fosse dichiarata illegittima e quindi venisse  
 meno, ricorrerebbe la violazione  di  un  diritto  dell'Amministrazione  
 ferroviaria.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ma l'affermazione non è pertinente alla questione di giurisdizione  
 bensì   al   merito   della  causa,  in  quanto  la  dichiarazione  di  
 illegittimità comporterebbe senz'altro la fondatezza della domanda per  
 essere venuta meno la norma che prevede l'obbligo della  concessione  e  
 l'onere di contribuzione ma non attribuirebbe al giudice amministrativo  
 la potestà di accertare l'insussistenza dell'obbligo della concessione  
 (e  quindi  del  relativo  potere della P. A.), nonché dell'obbligo di  
 contribuzione; correlativamente, se si verificasse  l'ipotesi  inversa,  
 il  giudice  dovrebbe  rigettare  la  domanda, non sussistendo la causa  
 petendi invocata dall'attore. In entrambi i casi, perciò, un'incidenza  
 della pronuncia di questa Corte sulla giurisdizione è  sicuramente  da  
 escludere, risolvendosi sempre il fondamento del petitum sostanziale in  
 una posizione di diritto soggettivo.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>5.</num>
            <content>
              <p> - Né sulla raggiunta conclusione può incidere l'<ref href="/akn/it/act/legge/stato/1977-01-28/10/!main#art_16">art.  16 della  
 l.  28  gennaio  1977 n. 10</ref>, contenente norme per l'edificabilità 'dei  
 suoli (già in vigore  alla  data  dell'ordinanza  di  rimessione,  che  
 tuttavia non lo ha tenuto presente); questo articolo devolve ai giudici  
 amministrativi  i ricorsi giurisdizionali " contro il provvedimento con  
 il  quale  la  concessione  viene  data  o  negata  nonché  contro  la  
 determinazione   o  la  liquidazione  del  contributo"  (nella  regione  
 Trentino-Alto Adige, non essendo  stato  ancora  istituito  il  TAR,  i  
 ricorsi   nella   materia   devoluta   al  giudice  amministrativo,  si  
 propongono, com'è noto, direttamente al Consiglio di Stato).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Non  è compito di questa Corte procedere all'interpretazione della  
 norma suddetta,  che  è  riferibile  all'intero  territorio  nazionale  
 (peraltro  essa  è  stata  anche espressamente richiamata dall'art. 12  
 della legge provinciale di Bolzano 3 gennaio 1978  n.  1),  essendo  la  
 potestà  in tema di ordinamento giudiziario riservata allo Stato anche  
 nei confronti delle Regioni a statuto speciale (cfr. sent. C. cost.  16  
 febbraio 1982 n. 43).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ma,  se pure le Sezioni Unite della Corte di Cassazione, nella loro  
 fondamentale  funzione  relativa  al   riparto   della   giurisdizione,  
 dovessero  ritenere  la  detta  norma  applicabile alla fattispecie, la  
 prospettata questione risulterebbe pur sempre irrilevante, in quanto la  
 cognizione della controversia apparterrebbe in  ogni  caso  al  giudice  
 amministrativo,  venendo  analogamente  in  discussione la legittimità  
 costituzionale  dell'art.  24  cit.  -  analogamente  a  quanto   sopra  
 osservato  - soltanto ai fini della fondatezza della domanda e non già  
 per  quanto  concerne  la  giurisdizione.  La  cognizione  del  giudice  
 amministrativo,  invero,  deriverebbe non già dalla natura del petitum  
 sostanziale,  bensì  dal  potere  discrezionale  del  legislatore   di  
 attribuire  a  detto  giudice anche la cognizione di diritti soggettivi  
 (<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_113">art. 113 Cost.</ref>), sicché essa in nessun caso potrebbe rilevare ai fini  
 qui considerati.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara inammissibile la questione di legittimità  costituzionale  
 dell'art.  24,  secondo  comma,  seconda parte, T. U. delle leggi della  
 Provincia di Bolzano 23 giugno  1970  n.  20  (ordinamento  urbanistico  
 provinciale)   sollevata   dall'ordinanza   indicata   in  epigrafe  in  
 riferimento agli artt. 4 e 11  dello  Statuto  speciale  della  Regione  
 Trentino-Alto Adige 26 febbraio 1948 n. 5, ora sostituiti dagli artt. 4  
 e  8  del  T.  U. - approvato con <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-08-31/670/!main">D.P.R.  31 agosto 1972 n. 670</ref> - delle  
 leggi costituzionali concernenti il medesimo statuto.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1984-04-04">4 aprile 1984</date>.</block>
      <p>F.to:   <signature><judge refersTo="#leopoldoElia">LEOPOLDO   ELIA</judge></signature>  -  <signature><judge refersTo="#guglielmoRoehrssen">GUGLIELMO  
                                   ROEHRSSEN</judge></signature> - <signature><judge refersTo="#oronzoReale">ORONZO REALE</judge></signature>  -  BRUNETTO  
                                   BUCCIARELLI     DUCCI    -    <signature><judge refersTo="#albertoMalagugini">ALBERTO  
                                   MALAGUGINI</judge></signature> - <signature><judge refersTo="#livioPaladin">LIVIO PALADIN</judge></signature> - <signature><judge refersTo="#virgilioAndrioli">VIRGILIO  
                                   ANDRIOLI</judge></signature>   -   <signature><judge refersTo="#giuseppeFerrari">GIUSEPPE   FERRARI</judge></signature>   -  
                                   <signature><judge refersTo="#francescoSaja">FRANCESCO  SAJA</judge></signature>  -  <signature><judge refersTo="#giovanniConso">GIOVANNI  CONSO</judge></signature> -  
                                   <signature><judge refersTo="#ettoreGallo">ETTORE GALLO</judge></signature> - <signature><judge refersTo="#aldoCorasaniti">ALDO CORASANITI</judge></signature>.</p>
      <p><signature><person refersTo="#giovanniVitale" as="#cancelliere">GIOVANNI VITALE</person> - <role refersTo="#cancelliere">Cancelliere</role></signature>.</p>
    </conclusions>
  </judgment>
</akomaNtoso>
