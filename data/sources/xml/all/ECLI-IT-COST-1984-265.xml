<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>265</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:265</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/11/1984</data_decisione>
    <data_deposito>03/12/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - &#13;
 Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. GIUSEPPE &#13;
 BORZELLINO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale dell'art. 17 del  &#13;
 d.P.R.  26  ottobre  1972  n.  636  ("Revisione  della  disciplina  del  &#13;
 contenzioso tributario") promossi con le seguenti ordinanze:             &#13;
     1)  ordinanza emessa il 26 aprile 1983 dalla Commissione Tributaria  &#13;
 di 1 grado di Ascoli Piceno sul ricorso di Mariani Sandro, iscritta  al  &#13;
 n.  814  del  registro  ordinanze  1983  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 60 dell'anno 1984;                         &#13;
     2) ordinanza emessa il 1 marzo 1982 dalla Commissione Tributaria di  &#13;
 grado di Novara sul ricorso di Bazzano Giuseppe, iscritta al n. 216 del  &#13;
 registro ordinanze 1984 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 224 dell'anno 1984.                                        &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio del  30  ottobre  1984  il  Giudice  &#13;
 relatore Ettore Gallo.                                                   &#13;
     Ritenuto  che  la  Commissione Tributaria di 1 grado di Novara, con  &#13;
 ord. 1 marzo 1982, pervenuta a questa Corte  soltanto  il  14  febbraio  &#13;
 1984  (n.  216/84),  e  la  Commissione Tributaria di 2 grado di Ascoli  &#13;
 Piceno con ord. 26 aprile 1983 (n. 814/83) hanno  sollevato  la  stessa  &#13;
 questione  incidentale  di  legittimità  costituzionale  nei confronti  &#13;
 dell'art. 17 d.P.R. 26 ottobre 1972 n.  636  ritenendolo  incompatibile  &#13;
 cogli  artt.  53 e 97 Cost. e - secondo l'ord. 216/84 - anche coll'art.  &#13;
 113, mentre per l'ord. 814/83 (che esclude il 113) anche  coll'art.  24  &#13;
 Cost. e ciò nella parte in cui sanziona l'improcedibilità del ricorso  &#13;
 per mancata allegazione di una sua copia,                                &#13;
     che  è  intervenuto  nei  giudizi  il Presidente del Consiglio dei  &#13;
 ministri, rappresentato e difeso dall'Avvocatura Generale dello  Stato,  &#13;
 chiedendo che sia dichiarata l'infondatezza della questione.             &#13;
     Considerato  che,  trattandosi  di un'identica questione, i giudizi  &#13;
 possono essere riuniti,                                                  &#13;
     che, nonostante una delle ordinanze (814/83) faccia riferimento  in  &#13;
 motivazione  al  d.P.R. 3 novembre 1981 n. 739, come quello che avrebbe  &#13;
 "modificato" l'impugnato art. 17, in realtà non ne tiene  alcun  conto  &#13;
 in  quanto mostra di ritenere che la modifica si applichi dal 1 gennaio  &#13;
 1982, data della decorrenza della vigenza del decreto,                   &#13;
     che,  al  contrario, trattandosi di norma di carattere processuale,  &#13;
 essa dev'essere applicata ai procedimenti in corso, tanto più che  non  &#13;
 si  tratta  di  semplice  "modifica"  ma  addirittura di "sostituzione"  &#13;
 integrale, operata dall'art. 8 del sopravvenuto decreto,                 &#13;
     che l'ord. 216/84 non ha nemmeno rilevato la detta  sopravvenienza,  &#13;
 colla quale il legislatore ha riparato proprio a quanto viene lamentato  &#13;
 dai  Giudici  di  merito,  eliminando  ogni  sanzione  per  la  mancata  &#13;
 allegazione della copia del  ricorso,                                    &#13;
     che, pertanto, la sollevata questione non ha più alcun fondamento.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riuniti i giudizi indicati  in  epigrafe,  dichiara  manifestamente  &#13;
 infondata  la  questione  di  legittimità  costituzionale dell'art. 17  &#13;
 d.P.R. 26 ottobre 1972 n. 636, sollevata dalla  Commissione  Tributaria  &#13;
 di  1 grado di Novara con ord. 1 marzo 1982 (pervenuta alla Corte il 14  &#13;
 febbraio 1984: reg. ord. 216/84), e dalla Commissione Tributaria  di  2  &#13;
 grado  di  Ascoli  Piceno  con  ord.  26  aprile  1983 (n. 814/83), con  &#13;
 riferimento agli artt. 24, 53, 97 e 113 Cost..                           &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 novembre 1984.       &#13;
                                   F.to:   LEOPOLDO   ELIA  -  GUGLIELMO  &#13;
                                   ROEHRSSEN  -   BRUNETTO   BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN  -  ANTONIO  LA   PERGOLA   -  &#13;
                                   VIRGILIO  ANDRIOLI - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI - GIUSEPPE BORZELLINO.      &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
