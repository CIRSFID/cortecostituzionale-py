<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>1079</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:1079</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/11/1988</data_decisione>
    <data_deposito>06/12/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 162 della l. 11    &#13;
 luglio  1980,  n.  312  ("Nuovo  assetto  retributivo-funzionale  del    &#13;
 personale  civile  e  militare  dello Stato"), promosso con ordinanza    &#13;
 emessa il 1° luglio 1987 dal T.A.R. per il Lazio sul ricorso proposto    &#13;
 da  Quaranta  Bruno contro il Ministero del lavori pubblici, iscritta    &#13;
 al n. 294 del registro ordinanze 1988  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  27,  prima serie speciale dell'anno    &#13;
 1988;                                                                    &#13;
    Visto  l'atto  di costituzione di Quaranta Bruno nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di  consiglio del 9 novembre 1988 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto  che  nel  corso  del giudizio proposto da Quaranta Bruno    &#13;
 contro il Ministero dei lavori pubblici  ed  inteso  ad  ottenere  la    &#13;
 riliquidazione   del  trattamento  di  quiescenza  sulla  base  della    &#13;
 qualifica di dirigente generale, il T.A.R. del Lazio - con  ordinanza    &#13;
 emessa  in  data  1°  luglio 1987 - ha sollevato, in riferimento agli    &#13;
 artt.  3  e  97  Cost.,  questione  di  legittimità   costituzionale    &#13;
 dell'art.  162  l.  11  luglio 1980 n. 312, in quanto, attribuendo in    &#13;
 sede di esodo volontario particolari benefici soltanto agli impiegati    &#13;
 del  ruolo ad esaurimento, verrebbe a modificare irrazionalmente, e a    &#13;
 favore di questi ultimi, il rapporto con  il  personale  appartenente    &#13;
 alla carriera dirigenziale;                                              &#13;
      che  la  Presidenza  del  Consiglio  dei ministri è intervenuta    &#13;
 chiedendo che la questione fosse dichiarata non fondata;                 &#13;
      che  il  Quaranta  si  è costituito ed ha presentato memoria in    &#13;
 prossimità della  discussione  in  camera  di  consiglio,  chiedendo    &#13;
 dichiararsi  la  fondatezza  della  questione,  o,  in subordine, una    &#13;
 pronuncia interpretativa di rigetto, che faccia salva la sua  pretesa    &#13;
 sostanziale;                                                             &#13;
    Considerato  che  la questione sollevata con la predetta ordinanza    &#13;
 è stata già dichiarata non fondata da questa Corte con sentenza  n.    &#13;
 521 del 1987;                                                            &#13;
      che  nella  predetta  decisione  si  è  osservato  come non sia    &#13;
 configurabile nessuna disparità  di  trattamento  nei  confronti  di    &#13;
 coloro che sono stati inquadrati nella qualifica di primo dirigente e    &#13;
 di coloro che, non avendo trovato capienza nella  predetta  qualifica    &#13;
 dirigenziale, sono stati promossi ispettori generali ad esaurimento;     &#13;
      che, infatti, le due categorie rappresentano situazioni tra loro    &#13;
 non omogenee e, come tali, non comparabili;                              &#13;
      che,  per conseguenza, il diverso trattamento censurato non può    &#13;
 considerarsi privo di giustificazione, in quanto  si  ricollega  alle    &#13;
 diverse  modalità di inquadramento previste per le due summenzionate    &#13;
 categorie  ed  alle  quali  il  legislatore  connette   una   diversa    &#13;
 disciplina secondo le proprie attribuzioni discrezionali;                &#13;
      che  dalle predette considerazioni resta assorbita la censura ex    &#13;
 art. 97 Cost., non rivestendo la stessa carattere autonomo rispetto a    &#13;
 quella rivolta all'art. 3 Cost.;                                         &#13;
    Ritenuto  che  nell'ordinanza di remissione non sono stati dedotti    &#13;
 ulteriori e nuovi profili rispetto a quelli esaminati con  la  citata    &#13;
 sentenza;                                                                &#13;
      che,    pertanto,    la   questione   deve   essere   dichiarata    &#13;
 manifestamente infondata;                                                &#13;
    Visti  gli  artt.  26 legge 11 marzo 1953, n. 87, e 9, delle norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 162  della  legge  11  luglio  1980  n.  312    &#13;
 ("Nuovo   assetto   retributivo-funzionale  del  personale  civile  e    &#13;
 militare dello Stato") sollevata in riferimento agli  artt.  3  e  97    &#13;
 Cost.   dal   Tribunale   Amministrativo   Regionale  del  Lazio  con    &#13;
 l'ordinanza indicata in epigrafe, in quanto  dichiarata  non  fondata    &#13;
 con sentenza n. 521 del 1987.                                            &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 novembre 1988.                             &#13;
                    Il Presidente e redattore: SAJA                       &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 6 dicembre 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
