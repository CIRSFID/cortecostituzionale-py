<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>430</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:430</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/11/1991</data_decisione>
    <data_deposito>27/11/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 4, primo comma,    &#13;
 della legge 8 marzo 1968 n. 152 (Nuove norme in materia previdenziale    &#13;
 per il personale degli Enti locali) promosso con ordinanza emessa  il    &#13;
 6  marzo  1991  dal  Pretore  di  Caltagirone sul ricorso proposto da    &#13;
 Napolitano Salvatore  ed  altri  c/INADEL  iscritta  al  n.  351  del    &#13;
 registro  ordinanze  1991 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 22, prima serie speciale, dell'anno 1991;                  &#13;
    Visto l'atto di costituzione  di  Napolitano  Salvatore  ed  altri    &#13;
 nonché  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza  pubblica  del  5  novembre  1991  il  Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Uditi  l'avv.  Eugenio Merlino per Napolitano Salvatore ed altri e    &#13;
 l'Avvocato dello Stato Gaetano Zotta per il Presidente del  Consiglio    &#13;
 dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel corso di un giudizio civile promosso contro l'INADEL da    &#13;
 numerosi iscritti, avente per oggetto la determinazione della base di    &#13;
 calcolo  dell'indennità  premio   di   servizio,   il   Pretore   di    &#13;
 Caltagirone,  con  ordinanza  del  6  marzo  1991,  ha  sollevato, in    &#13;
 riferimento all'art. 3, nonché agli artt. 1, 4, 35, 36, 38, 97 e  98    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art. 4, primo comma, della legge 8 marzo 1968,  n.  152,  "nella    &#13;
 parte in cui prevede che l'indennità premio di servizio sarà pari a    &#13;
 un  quindicesimo  della retribuzione contributiva degli ultimi dodici    &#13;
 mesi per ogni anno di iscrizione all'INADEL, e non di  un  dodicesimo    &#13;
 come previsto per gli omologhi dipendenti statali".                      &#13;
    Secondo   il   giudice   remittente,  la  rilevata  disparità  di    &#13;
 trattamento degli iscritti all'INADEL rispetto ai dipendenti  statali    &#13;
 "non trova ragionevole giustificazione".                                 &#13;
    2.  -  Nel  giudizio  davanti  alla  Corte  si  sono  costituiti i    &#13;
 ricorrenti  aderendo  all'ordinanza  di  rimessione  senza  ulteriori    &#13;
 argomentazioni.                                                          &#13;
    3.  -  È  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, chiedendo che la questione    &#13;
 sia dichiarata manifestamente inammissibile o comunque infondata.        &#13;
    Secondo l'Avvocatura, la quale richiama le  sentenze  nn.  26  del    &#13;
 1980  e  220  del  1988,  la  diversità  dei  sistemi  retributivo e    &#13;
 previdenziale delle due categorie di dipendenti pubblici esclude ogni    &#13;
 possibilità di comparazione ai fini dell'art. 3 della Costituzione.<diritto>Considerato in diritto</diritto>1. - Il Pretore di Caltagirone ritiene contrastante col  principio    &#13;
 di eguaglianza di cui all'art. 3, nonché con gli artt. 1, 4, 35, 36,    &#13;
 38,  97 e 98 della Costituzione, l'art. 4, primo comma, della legge 8    &#13;
 marzo 1968, n. 152, nella parte in cui determina la base  di  calcolo    &#13;
 dell'indennità premio di servizio per il personale degli enti locali    &#13;
 in  un  quindicesimo  della  retribuzione percepita nell'ultimo anno,    &#13;
 anziché in un dodicesimo come previsto dal d.P.R. 29 dicembre  1973,    &#13;
 n. 1032, per i dipendenti dello Stato.                                   &#13;
    2. - L'ordinanza di rimessione tralascia qualsiasi motivazione del    &#13;
 riferimento della questione, oltre che all'art. 3 della Costituzione,    &#13;
 agli  artt.  1,  4,  35,  36,  38,  97  e 98. Sotto questi profili la    &#13;
 questione è, pertanto, manifestamente inammissibile.                    &#13;
    3. - In riferimento al principio di eguaglianza, la questione  non    &#13;
 è fondata.                                                              &#13;
    Il  trattamento  giuridico-economico  dei  dipendenti  degli  enti    &#13;
 locali e il trattamento  dei  dipendenti  dello  Stato  costituiscono    &#13;
 sistemi  normativi  diversi,  come  risulta  dall'art. 1 del d.P.R. 5    &#13;
 marzo 1986, n. 68, che per gli uni e gli altri prevede  due  distinti    &#13;
 comparti   di  contrattazione  collettiva,  e  diversi  sono  pure  i    &#13;
 rispettivi sistemi previdenziali. La comparazione,  che  deve  essere    &#13;
 globale e non limitata a singole voci isolatamente considerate, mette    &#13;
 in   evidenza   un  trattamento  dei  dipendenti  degli  enti  locali    &#13;
 complessivamente più favorevole di quello  dei  dipendenti  statali.    &#13;
 Come  ha  rilevato  il  rappresentante  del  Governo durante i lavori    &#13;
 preparatori della legge n. 152 del 1968 (Camera  dep.,  IV  leg.,  II    &#13;
 Comm.,   seduta   del   10   gennaio  1968,  pag.  929),  il  computo    &#13;
 dell'indennità su una base più  ristretta  (un  quindicesimo  della    &#13;
 retribuzione  dell'ultimo anno, anziché un dodicesimo) è compensato    &#13;
 dalla liquidazione della pensione  sulla  base  del  100  per  cento,    &#13;
 mentre  per  gli statali si effettua sulla base dell'80 per cento. Si    &#13;
 aggiunga che dal 1° gennaio 1974 i dipendenti degli enti locali  sono    &#13;
 stati   ulteriormente   avvantaggiati   dall'inclusione   -  disposta    &#13;
 dall'art.  22  della  legge  3  giugno  1975,  n.  160,  con  effetto    &#13;
 retroattivo   alla   data   indicata   -   nella   base   di  calcolo    &#13;
 dell'indennità premio di servizio anche dell'indennità  integrativa    &#13;
 speciale,   che   invece  rimane  tuttora  esclusa  dal  calcolo  del    &#13;
 trattamento di buonuscita degli statali.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  4,  primo  comma,  della legge 8 marzo 1968, n. 152 (Nuove    &#13;
 norme in materia previdenziale per il personale degli  Enti  locali),    &#13;
 sollevata,  in riferimento all'art. 3 della Costituzione, dal Pretore    &#13;
 di Caltagirone con l'ordinanza indicata in epigrafe;                     &#13;
    Dichiara manifestamente inammissibile  la  medesima  questione  in    &#13;
 riferimento  agli artt. 1, 4, 35, 36, 38, 97 e 98 della Costituzione,    &#13;
 pure richiamati nella stessa ordinanza.                                  &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 novembre 1991.                             &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 novembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
