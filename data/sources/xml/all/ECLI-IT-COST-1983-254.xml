<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>254</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:254</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/07/1983</data_decisione>
    <data_deposito>28/07/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. &#13;
 ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli  artt.    1  e  2  &#13;
 della legge 31 gennaio 1949, n. 21 (Aumento del contributo obbligatorio  &#13;
 dovuto  dai  sanitari dipendenti da pubbliche amministrazioni in favore  &#13;
 dell'Opera  nazionale  per  l'assistenza  agli  orfani   dei   sanitari  &#13;
 italiani,  con sede in Perugia) e dell'art. 5 del d.P.R. 18 luglio 1957  &#13;
 (Approvazione dello statuto dell'Opera nazionale per l'assistenza  agli  &#13;
 orfani  dei  sanitari  italiani),  promosso  con ordinanza emessa il 28  &#13;
 ottobre 1980 dal Pretore  di  Reggio  Emilia  nel  procedimento  civile  &#13;
 vertente  tra  Davoli Carla ed altri e Azienda municipalizzata Farmacie  &#13;
 Comunali Riunite del Comune di Reggio Emilia ed altri, iscritta  al  n.  &#13;
 851  del  registro ordinanze 1980 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 44 del 1981;                                         &#13;
     Visti l'atto di costituzione dell'Opera nazionale  assistenza  agli  &#13;
 orfani  dei sanitari italiani e l'atto di intervento del Presidente del  &#13;
 Consiglio dei ministri;                                                  &#13;
     udito, nella camera di consiglio del 27  aprile  1983,  il  Giudice  &#13;
 relatore Michele Rossano.                                                &#13;
     Rilevato  che  con  ricorso  26  aprile  1980  al Pretore di Reggio  &#13;
 Emilia,  giudice  del  lavoro,  Carla  Davoli  ed  altri  40   sanitari  &#13;
 dipendenti  dell'Azienda  Municipalizzata Farmacie Comunali Riunite del  &#13;
 Comune di Reggio Emilia hanno proposto domanda diretta ad  ottenere  la  &#13;
 restituzione  dei  contributi  nella  misura  del  2%  dello stipendio,  &#13;
 indebitamente percepiti  dall'Opera  Nazionale  per  l'Assistenza  agli  &#13;
 orfani dei sanitari italiani (ONAOSI), e sollevato, in via subordinata,  &#13;
 la  questione di legittimità costituzionale degli artt. 1 e 2 legge 31  &#13;
 gennaio 1949, n. 21 (aumento del  contributo  obbligatorio  dovuto  dai  &#13;
 sanitari  dipendenti  da Pubbliche Amministrazioni in favore dell'Opera  &#13;
 Nazionale per l'assistenza  agli  orfani  dei  sanitari  italiani),  in  &#13;
 riferimento all'art. 3 della Costituzione;                               &#13;
     che  il Pretore di Reggio Emilia, con ordinanza 28 ottobre 1980, ha  &#13;
 ritenuto rilevante e  non  manifestamente  infondata  la  questione  di  &#13;
 legittimità costituzionale dei citati artt. 1 e 2 legge n. 21 del 1949  &#13;
 e dell'art. 5 Statuto dell'Opera Nazionale per l'assistenza agli orfani  &#13;
 dei  sanitari  italiani,  approvato  con  d.P.R.  18  luglio  1957,  in  &#13;
 riferimento all'art. 3 della Costituzione;                               &#13;
     che, secondo il giudice a quo, gli artt. 1 e 2 legge n. 21 del 1949  &#13;
 - che prevedono contributi a favore dell'ONAOSI di importo diverso  con  &#13;
 riguardo  alla  natura  autonoma o subordinata dell'attività sanitaria  &#13;
 svolta - e l'art. 5 dello Statuto dell'ONAOSI  sarebbero  in  contrasto  &#13;
 con l'art. 3 della Costituzione perché determinerebbero una disparità  &#13;
 di  trattamento,  non giustificata, tra sanitari liberi professionisti,  &#13;
 che versano  una  somma  fissa  ed  invariabile,  e  sanitari  pubblici  &#13;
 dipendenti,  ai  quali  è  imposto un contributo di maggiore entità e  &#13;
 variabile nel tempo perché calcolato nella percentuale  del  2%  degli  &#13;
 stipendi;                                                                &#13;
     Ritenuto  che  è  inammissibile  la questione concernente l'art. 5  &#13;
 dello Statuto dell'ONAOSI, approvato con d.P.R. 18 luglio 1957, perché  &#13;
 tale norma, per la sua natura regolamentare, è priva di forza di legge  &#13;
 e,  quindi,  non  è  assoggettabile  al  controllo   di   legittimità  &#13;
 costituzionale;                                                          &#13;
     Ritenuto,  inoltre, che la questione di legittimità costituzionale  &#13;
 dell'art. 2 legge n. 21 del  1949,  concernente  la  misura  fissa  del  &#13;
 contributo versato dai sanitari liberi esercenti, è priva di rilevanza  &#13;
 perché oggetto della controversia è solo l'obbligo del versamento, da  &#13;
 parte  dei  sanitari  pubblici  dipendenti,  del  contributo  a  favore  &#13;
 dell'ONAOSI,  stabilito   nella   misura   del   2%   dello   stipendio  &#13;
 dall'impugnato art. 1 legge n. 21 del 1949;                              &#13;
     Ritenuto  che  non sussiste il denunciato contrasto dell'articolo 1  &#13;
 legge n. 21 del 1949 con l'art. 3 della Costituzione per la  diversità  &#13;
 assoluta delle due situazioni poste a confronto dato che per i sanitari  &#13;
 dipendenti da Pubbliche amministrazioni è obbligatorio il contributo -  &#13;
 dovuto  all'ONAOSI  a  norma  del  r.d.l.  27  settembre 1938, n. 1325,  &#13;
 convertito nella legge 2 giugno 1939, n. 739 e fissato nella misura del  &#13;
 2% dello stipendio dal suddetto art.  1 legge n. 21  del  1949,  mentre  &#13;
 per  i  sanitari  liberi  professionisti  il  contributo  deriva  dalla  &#13;
 iscrizione volontaria all'ONAOSI;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a)  dichiara  la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità  costituzionale  dell'art.  5 Statuto dell'Opera Nazionale  &#13;
 per l'assistenza agli orfani dei sanitari italiani approvato con d.P.R.  &#13;
 18 luglio 1957;                                                          &#13;
     b)  dichiara  la  manifesta  inammissibilità  della  questione  di  &#13;
 legittimità costituzionale dell'art. 2 legge 31 gennaio 1949, n. 21;    &#13;
     c)   dichiara   la   manifesta   infondatezza  della  questione  di  &#13;
 legittimità costituzionale dell'art. 1 legge 31 gennaio 1949, n. 21.    &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 15 luglio 1983.         &#13;
                                   F.to: LEOPOLDO ELIA - MICHELE ROSSANO  &#13;
                                   -  ANTONINO  DE  STEFANO  - GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE  -  BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ETTORE GALLO.                          &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
