<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>45</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:45</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>OGGIONI</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/03/1976</data_decisione>
    <data_deposito>16/03/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. LUIGI OGGIONI, Presidente - Avv. &#13;
 ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - &#13;
 Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA &#13;
 - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - Prof. ANTONINO DE &#13;
 STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale degli artt. 12, 14,  &#13;
 secondo comma, e 22, primo comma, del d.P.R. 12 dicembre 1948, n.  1414  &#13;
 (norme  di  attuazione  dello  Statuto  speciale  per  il Trentino-Alto  &#13;
 Adige), promosso con ricorso del Presidente della Giunta provinciale di  &#13;
 Bolzano, notificato il 19 febbraio 1972, depositato in  cancelleria  il  &#13;
 29 successivo ed iscritto al n. 18 del registro ricorsi 1972.            &#13;
     Visto  l'atto  di  costituzione  del  Presidente  del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 29 ottobre 1975 il Giudice relatore  &#13;
 Michele Rossano;                                                         &#13;
     uditi l'avv. Giuseppe Guarino, per la Provincia di Bolzano,  ed  il  &#13;
 sostituto  avvocato  generale  dello  Stato  Giorgio  Azzariti,  per il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     La Provincia di Bolzano, con  ricorso  notificato  il  19  febbraio  &#13;
 1972,  chiese  che  venisse dichiarata la illegittimità costituzionale  &#13;
 degli artt. 12, 14, secondo comma, 22, primo comma, d.P.R. 12  dicembre  &#13;
 1948,  n.  1414  (Norme  di  attuazione  dello  Statuto speciale per il  &#13;
 Trentino-Alto Adige) per contrasto con gli artt. 46, secondo comma, 50,  &#13;
 secondo comma, legge costituzionale  26  febbraio  1948,  n.  5  (Testo  &#13;
 originario  dello  Statuto  della Regione Trentino-Alto Adige); con gli  &#13;
 artt.  6 (n.   1), 10 e 11, 5 (nn. 5, 10 e 28), 52, 44 (n. 2), 51 legge  &#13;
 costituzionale  10 novembre 1971, n. 1; in relazione anche all'art.  13  &#13;
 legge costituzionale 26 febbraio 1948, n. 5.                             &#13;
     Il  Presidente  del  Consiglio dei ministri, rappresentato e difeso  &#13;
 dall'Avvocato generale dello Stato, intervenne con atto  7  marzo  1972  &#13;
 chiedendo  che  il  ricorso della Provincia di Bolzano fosse dichiarato  &#13;
 inammissibile o, subordinatamente, respinto.                             &#13;
     All'udienza del 29 ottobre 1975 il  difensore  della  Provincia  di  &#13;
 Bolzano  ha  affermato che la materia del contendere è cessata perché  &#13;
 l'art. 53 del d.P.R. 1 febbraio 1973, n. 49, ha abrogato il  d.P.R.  12  &#13;
 dicembre 1948, n. 1414.                                                  &#13;
     Alla  stessa  udienza  l'avvocato  dello  Stato ha replicato che la  &#13;
 materia del contendere non è cessata per  quanto  concerne  l'art.  12  &#13;
 d.P.R.  12  dicembre 1948, n. 1414, perché tale articolo  non è stato  &#13;
 abrogato  dall'art.  53  d.P.R.  1  febbraio  1973,  n.  49,  come   è  &#13;
 testualmente  detto  in quest'ultimo articolo.  Ha, quindi, dedotto che  &#13;
 vi è ulteriore motivo di inammissibilità perché il  citato  art.  53  &#13;
 d.P.R. n. 49 del 1973 non è stato impugnato.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Provincia  di  Bolzano  ha denunciato gli artt. 12, 14 (secondo  &#13;
 comma), 22 (primo comma) d.P.R. 12 dicembre 1948,  n.  1414  (Norme  di  &#13;
 attuazione  dello  Statuto  speciale  per  il  Trentino-Alto Adige) per  &#13;
 contrasto con gli artt. 46 (secondo comma), 50 (secondo  comma),  legge  &#13;
 costituzionale  26  febbraio 1948, n. 5 (Testo originario dello Statuto  &#13;
 speciale per il Trentino-Alto Adige); con gli artt. 6 (n. 1), 10 e  11,  &#13;
 5  (nn. 5, 10 e 28), 52, 44 (n. 2), 51 legge costituzionale 10 novembre  &#13;
 1971, n. 1 (Modificazioni ed integrazioni dello Statuto speciale per il  &#13;
 Trentino-Alto Adige), in relazione anche  all'art.    13  citata  legge  &#13;
 costituzionale n. 5 del 1948.                                            &#13;
     Nel  corso  del  presente  giudizio  è  stato  emanato il d.P.R. 1  &#13;
 febbraio 1973, n. 49, contenente  norme  di  attuazione  dello  Statuto  &#13;
 speciale  per il Trentino-Alto Adige relative agli organi della Regione  &#13;
 e delle Province di Trento e Bolzano                                     &#13;
     L'art. 53 di tale decreto prescrive che il d.P.R. 12 dicembre 1948,  &#13;
 n. 1414, escluso l'art. 12, e alcuni titoli del d.P.R.  30 giugno 1951,  &#13;
 n. 574, sono abrogati.                                                   &#13;
     Pertanto, a seguito della espressa abrogazione degli  articoli  14,  &#13;
 secondo  comma,  e 22, primo comma, del citato decreto n. 1414 del 1948  &#13;
 è cessata la materia del contendere relativamente  alle  questioni  di  &#13;
 legittimità  costituzionale  degli  stessi  articoli,  e deve, quindi,  &#13;
 procedersi alla  conseguente  declaratoria,  in  parziale  accoglimento  &#13;
 della  richiesta avanzata dal difensore della Provincia all'udienza del  &#13;
 29 ottobre 1975, alla  quale  ha  aderito  l'avvocato  dello  Stato.  A  &#13;
 diversa   conclusione   si   perviene,  invece,  per  la  questione  di  &#13;
 legittimità costituzionale dell'art. 12 del menzionato decreto n. 1414  &#13;
 del 1948, in quanto tale articolo è tuttora in vigore,  essendo  stato  &#13;
 espressamente escluso dalle norme abrogate.                              &#13;
     Devesi, preliminarmente, rilevare che non è fondata l'eccezione di  &#13;
 inammissibilità  del  ricorso  sollevata  dall'Avvocatura  dello Stato  &#13;
 all'udienza del 29 ottobre 1975  sotto  il  profilo  che  non  e  stato  &#13;
 impugnato  l'art.  53 d.P.R. n. 49 del 1973. Con quest'ultimo articolo,  &#13;
 invero, il legislatore non ha introdotto una nuova norma dal  contenuto  &#13;
 identico a quella dell'impugnato art. 12 decreto n. 1414 del 1948, dato  &#13;
 che  si è limitato a prescrivere che tale articolo non è compreso tra  &#13;
 le   norme   dello   stesso  decreto  n.  1414  del  1948,  abrogate  e  &#13;
 genericamente indicate.                                                  &#13;
     La questione di legittimità costituzionale dell'art. 12 decreto n.  &#13;
 1414 del 1948 non è fondata.                                            &#13;
     L'art. 12 impugnato dispone che, qualora il Presidente della giunta  &#13;
 provinciale di Bolzano ometta di prendere i provvedimenti previsti  dal  &#13;
 secondo comma dell'art. 46 legge costituzionale 26 febbraio 1948, n. 5,  &#13;
 la  facoltà  di  prendere i provvedimenti stessi spetta al Commissario  &#13;
 del Governo.                                                             &#13;
     In caso di inadempienza del sindaco provvede  il  Presidente  della  &#13;
 giunta provinciale.                                                      &#13;
     E  l'art. 46, secondo comma, attribuisce al Presidente della giunta  &#13;
 provinciale di  Bolzano  la  competenza  di  adottare  i  provvedimenti  &#13;
 contingibili  ed  urgenti  in materia di sicurezza e di igiene pubblica  &#13;
 nell'interesse  delle  popolazioni  di  due   o   più   comuni.   Tale  &#13;
 disposizione  è stata recepita nell'art. 52, secondo comma, del d.P.R.  &#13;
 31  agosto  1972,  n.  670,  testo  unico  delle  leggi  costituzionali  &#13;
 concernenti lo Statuto speciale Trentino-Alto Adige.                     &#13;
     Non  è fondata la censura, dedotta nel ricorso, secondo cui l'art.  &#13;
 12  impugnato  lederebbe  l'autonomia  della   Provincia   perché   la  &#13;
 competenza  attribuita  al  Presidente della giunta provinciale sarebbe  &#13;
 esclusiva e, quindi, non consentirebbe il potere  di  sostituzione  del  &#13;
 Commissario  del  Governo nel caso di inadempienza del Presidente della  &#13;
 Provincia.                                                               &#13;
     Alle Province di Trento  e  di  Bolzano  sono  attribuite  forme  e  &#13;
 condizioni  particolari  di  autonomia secondo lo Statuto della Regione  &#13;
 (art. 3, terzo comma, tu. Statuto 31 agosto 1972, n. 670 cit.) e l'art.  &#13;
 87 dello stesso t.u. stabilisce, nel  n.  1,  che  al  Commissario  del  &#13;
 Governo  spetta  di  coordinare,  in  conformità  alle  direttive  del  &#13;
 Governo, lo svolgimento delle attribuzioni dello Stato e, nel n. 3,  di  &#13;
 compiere  atti  già  demandati  al  Prefetto in quanto non siano stati  &#13;
 affidati dallo statuto o da altre leggi ad organi della Regione e delle  &#13;
 Province o ad altri organi dello Stato.                                  &#13;
     Non è giustificato  il  riferimento,  nel  ricorso,  all'art.  46,  &#13;
 secondo  comma,  legge  costituzionale  26  febbraio  1948, n. 5 (testo  &#13;
 originario dello Statuto) - che attribuisce al Presidente della  giunta  &#13;
 provinciale  il  potere  di  adottare  i  provvedimenti  contingibili e  &#13;
 urgenti - e all'art. 6, nn. 1 e 10, legge costituzionale n. 1 del 1971,  &#13;
 che conferisce alle Province il potere di emanare norme legislative  in  &#13;
 materia di polizia locale e di igiene e sanità.                         &#13;
     Invero  i  provvedimenti  contingibili ed urgenti sono disciplinati  &#13;
 dall'art. 55 r.d. 3  marzo  1934,  n.  383,  testo  unico  della  legge  &#13;
 comunale  e  provinciale,  richiamato  in vigore dall'art.   25 legge 9  &#13;
 giugno 1974, n. 530, per quanto concerne le attribuzioni dei Consigli e  &#13;
 delle giunte comunali. Il citato art.  55  attribuisce  al  sindaco  il  &#13;
 potere  di  emanarli  nelle  materie di sicurezza pubblica e di igiene,  &#13;
 come è pacifico in dottrina e  in  giurisprudenza,  in  considerazione  &#13;
 della   esigenza  dell'urgenza.  Deve  ritenersi  -  nel  caso  che  la  &#13;
 situazione di pericolo riguardi due comuni  della  stessa  provincia  -  &#13;
 giustificata,  nell'interesse  preminente  dello  Stato, l'attribuzione  &#13;
 della  competenza  al  Presidente  della  giunta   provinciale,   quale  &#13;
 ufficiale  del  Governo,  senza escludere quella del sindaco in caso di  &#13;
 situazione di pericolo per un solo comune. E come il sindaco,  in  tale  &#13;
 qualità,  non  può  essere  considerato  in  senso  tecnico-giuridico  &#13;
 funzionario  del  comune,  come  è  pure  pacifico  in  dottrina  e in  &#13;
 giurisprudenza,  così  nemmeno  può  essere   considerato   tale   il  &#13;
 Presidente  della  giunta  provinciale  quando  adotta  i provvedimenti  &#13;
 contingibili e urgenti a termine dell'art.   46, secondo  comma,  dello  &#13;
 Statuto del 1948 e del secondo comma dell'art. 52 t.u. citato del 1972.  &#13;
 E,  poiché i provvedimenti del sindaco non hanno carattere definitivo,  &#13;
 in considerazione dei loro stessi presupposti, non li hanno  nemmeno  i  &#13;
 provvedimenti del Presidente della giunta provinciale.                   &#13;
     Ciò  posto,  l'urgenza e la temporaneità connesse alla situazione  &#13;
 di  pericolo  escludono  che  possa  essere   vulnerata   la   potestà  &#13;
 legislativa di cui trattasi (art. 8 e 9 t.u. citato del 1972).           &#13;
     Consegue  che  l'art.  12  impugnato  ha,  conformemente  alla  sua  &#13;
 funzione di norma di attuazione, integrato la norma del  secondo  comma  &#13;
 dell'art.  46  e  del  secondo  comma  dell'art.  52 citati, nei limiti  &#13;
 consentiti dal loro contenuto.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a) dichiara cessata la materia del  contendere  relativamente  alle  &#13;
 questioni di legittimità costituzionale degli artt. 14, secondo comma,  &#13;
 e  22,  primo  comma,  del  d.P.R.  12 dicembre 1948, n. 1414 (Norme di  &#13;
 attuazione dello Statuto speciale per il Trentino-Alto Adige), proposte  &#13;
 dalla Provincia di Bolzano con ricorso notificato il 19 febbraio 1972;   &#13;
     b) dichiara non fondata la questione di legittimità costituzionale  &#13;
 dell'art. 12 del d.P.R. 12 dicembre 1948, n. 1414 (Norme di  attuazione  &#13;
 dello  Statuto  speciale  per  il  Trentino-Alto Adige), proposta dalla  &#13;
 Provincia di Bolzano con ricorso notificato il 19 febbraio 1972.         &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 9 marzo 1976.                                 &#13;
                                   F.to: LUIGI OGGIONI - ANGELO DE MARCO  &#13;
                                   ERCOLE  ROCCHETTI  - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE -  TRIMARCHI  NICOLA  &#13;
                                   REALE  -  PAOLO  ROSSI    -  LEONETTO  &#13;
                                   AMADEI - GIULIO GIONFRIDA  -  EDOARDO  &#13;
                                   VOLTERRA   -   GUIDO  ASTUTI  MICHELE  &#13;
                                   ROSSANO - ANTONINO DE STEFANO.         &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
