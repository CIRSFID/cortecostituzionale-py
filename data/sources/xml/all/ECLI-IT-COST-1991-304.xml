<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>304</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:304</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GALLO E.</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/06/1991</data_decisione>
    <data_deposito>26/06/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Ettore GALLO; &#13;
 Giudici: dott. Aldo CORASANITI, dott. Francesco GRECO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  121,  secondo    &#13;
 comma,  del  testo  delle  norme  di  attuazione,  di coordinamento e    &#13;
 transitorie del codice  di  procedura  penale  (testo  approvato  con    &#13;
 decreto  legislativo  28 luglio 1989, n. 271), promosso con ordinanza    &#13;
 emessa il 26 novembre 1990 dal Giudice per  le  indagini  preliminari    &#13;
 presso  il  Tribunale  di  Ancona nel procedimento penale a carico di    &#13;
 Malatesta Domenico, iscritta al n.  150 del registro ordinanze 1991 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  11,  prima    &#13;
 serie speciale, dell'anno 1991;                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  del  5  giugno  1991  il  Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto  che,  provvedendo  in ordine alla richiesta del pubblico    &#13;
 ministero di convalida dell'arresto di Malatesta Domenico, del  quale    &#13;
 lo stesso p.m. aveva già disposto l'immediata liberazione - ai sensi    &#13;
 dell'art.  121,  primo comma, del testo delle norme di attuazione, di    &#13;
 coordinamento e transitorie del codice  di  procedura  penale  (testo    &#13;
 approvato  con  decreto  legislativo  28  luglio  1989, n. 271) -, il    &#13;
 giudice per le indagini preliminari presso il Tribunale di Ancona  ha    &#13;
 sollevato  questione  di  legittimità  costituzionale dell'art. 121,    &#13;
 secondo comma, del citato testo (secondo cui "nel caso di liberazione    &#13;
 prevista nel comma 1, il giudice nel fissare l'udienza di  convalida,    &#13;
 ne  dà  avviso,  senza  ritardo,  anche  alla  persona liberata") in    &#13;
 riferimento agli artt. 97 e 101, secondo comma, della Costituzione;      &#13;
      che il giudice remittente, premesso che la norma censurata è di    &#13;
 carattere strumentale rispetto alla disciplina di cui agli  artt.  da    &#13;
 389  a  391 del codice di procedura penale - secondo cui l'udienza di    &#13;
 convalida deve svolgersi soltanto qualora il pubblico  ministero  non    &#13;
 debba  ordinare la immediata liberazione dell'arrestato o del fermato    &#13;
 -  osserva  che  la  norma  stessa  evidenzierebbe  una  mancanza  di    &#13;
 coordinamento  con tale disciplina e una sostanziale superfluità, in    &#13;
 quanto, essendo il  soggetto  già  stato  rimesso  in  libertà,  la    &#13;
 convalida si riduce ad una mera ed inutile formalità, a meno che non    &#13;
 si  ritenga  -  ma  il  codice,  prosegue  il giudice a quo, dovrebbe    &#13;
 prevederlo esplicitamente - che all'esito dell'udienza  di  convalida    &#13;
 il  pubblico  ministero,  a seguito dell'interrogatorio reso dall' ex    &#13;
 arrestato, possa  modificare  le  proprie  richieste  e  chiedere  al    &#13;
 giudice l'applicazione di misure cautelari;                              &#13;
      che,   in   conclusione,  il  remittente  solleva  questione  di    &#13;
 legittimità costituzionale  del  citato  art.  121,  secondo  comma,    &#13;
 "nella  parte  in cui non statuisce per esplicito la possibilità per    &#13;
 il pubblico ministero di richiedere, in sede di udienza di convalida,    &#13;
 nei  confronti  di  persona  sottoposta  alle  indagini   preliminari    &#13;
 arrestata o fermata già scarcerata dalla stessa a.g.o. requirente ex    &#13;
 art.   121,   primo   comma,  disp.  att.  stesso  codice,  all'esito    &#13;
 dell'interrogatorio eventualmente reso dal prevenuto in detta sede  o    &#13;
 comunque  all'esito  di  ulteriori indagini preliminari eventualmente    &#13;
 svolte dal pubblico ministero nelle more fra  detta  scarcerazione  o    &#13;
 l'udienza di convalida, misure cautelari coercitive ex art. 291 nuovo    &#13;
 codice  di  procedura penale"; ciò per violazione dell'art. 97 della    &#13;
 Costituzione, "divenendo a tal  punto  l'udienza  di  convalida  mero    &#13;
 passaggio  obbligato  e formale dall'esito scontato e precostituito",    &#13;
 nonché   dell'art.   101,   secondo   comma,   della   Costituzione,    &#13;
 "assoggettandosi  il  giudice  non  più  alla legge ma alla volontà    &#13;
 delle parti", in quanto "a tal punto  da  detta  anomala  udienza  di    &#13;
 convalida..  ..  .. può soltanto scaturire o una mancata convalida o    &#13;
 una convalida meramente formale,  inibendosi  al  pubblico  ministero    &#13;
 ogni nuova richiesta all'esito dell'udienza";                            &#13;
      che  è  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 ministri, concludendo per l'inammissibilità (non risultando in alcun    &#13;
 modo che nella fattispecie il pubblico ministero  abbia  richiesto  o    &#13;
 avesse  intenzione di richiedere l'applicazione di misure cautelari),    &#13;
 o, in subordine, per l'infondatezza della questione;                     &#13;
    Considerato che l'eccezione di inammissibilità  per  irrilevanza,    &#13;
 sollevata  dall'Avvocatura  dello  Stato, non può essere accolta, in    &#13;
 quanto il giudice a quo, con la proposta questione, intende censurare    &#13;
 in radice la previsione dell'udienza di convalida  nel  caso  in  cui    &#13;
 l'arrestato  (o  il  fermato) sia già stato rimesso in libertà, ove    &#13;
 non sia  poi  riconosciuta  al  pubblico  ministero  la  facoltà  di    &#13;
 richiedere in detta udienza l'applicazione di misure coercitive;         &#13;
      che, nel merito, - e prescindendo dal rilievo che il remittente,    &#13;
 nel  lamentare  l'assenza di una norma "esplicita", dà l'impressione    &#13;
 di poter giungere in via ermeneutica al  risultato  auspicato  -,  la    &#13;
 questione  si fonda, comunque, su un presupposto chiaramente erroneo,    &#13;
 dato che nessuna disposizione preclude  al  pubblico  ministero,  che    &#13;
 abbia ordinato l'immediata liberazione dell'arrestato (o del fermato)    &#13;
 ritenendo   "di   non  dovere  richiedere  l'applicazione  di  misure    &#13;
 coercitive", di presentare poi al giudice una richiesta in tal senso,    &#13;
 in conseguenza del venir meno delle ragioni di  opportunità  che  lo    &#13;
 avevano  in  precedenza indotto a disporre la liberazione (a seguito,    &#13;
 ad esempio, di ulteriori indagini svolte nelle more, tanto più  che,    &#13;
 nella  ipotesi  in  esame,  secondo  la giurisprudenza della Corte di    &#13;
 cassazione, non si applicano i termini perentori di cui all'art.  390    &#13;
 del codice di procedura penale);                                         &#13;
      che,   peraltro,   non   può  non  rilevarsi  che  nei  casi  -    &#13;
 indubbiamente più frequenti -  in  cui  il  pubblico  ministero  non    &#13;
 richieda  misure coercitive, l'udienza di convalida non può comunque    &#13;
 considerarsi una "inutile formalità", come sostiene  il  remittente,    &#13;
 in  quanto,  pur non essendo imposta dall'art. 13, terzo comma, della    &#13;
 Costituzione (avendo il soggetto già riacquistato la  libertà),  la    &#13;
 sua   previsione   risponde   comunque  all'interesse  del  cittadino    &#13;
 all'accertamento  giudiziale  della  legittimità  del  provvedimento    &#13;
 restrittivo  adottato  nei  suoi confronti dall'autorità di pubblica    &#13;
 sicurezza, tanto più che nel caso di cui  alla  norma  impugnata  la    &#13;
 liberazione  è  stata determinata non da vizi procedurali, bensì da    &#13;
 una valutazione di opportunità da parte del pubblico ministero (cfr.    &#13;
 sentenza n. 515 del 1990);                                               &#13;
      che, in conclusione, la questione va  dichiarata  manifestamente    &#13;
 infondata sotto ogni profilo;                                            &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 121, secondo comma, del testo delle norme di    &#13;
 attuazione, di coordinamento e transitorie del  codice  di  procedura    &#13;
 penale  (testo  approvato  con decreto legislativo 28 luglio 1989, n.    &#13;
 271), sollevata, in riferimento agli artt. 97 e 101,  secondo  comma,    &#13;
 della Costituzione, dal giudice per le indagini preliminari presso il    &#13;
 Tribunale di Ancona con l'ordinanza in epigrafe.                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 17 giugno 1991.                                  &#13;
                         Il Presidente: GALLO                             &#13;
                          Il redattore: FERRI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 giugno 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
