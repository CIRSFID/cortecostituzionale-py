import re

from akomantoso.dom.elements import Background, Motivation, Division, P, Heading

from cortecostituzionale.parser.paragraphs import parse_paragraphs
from cortecostituzionale.parser.text_cleanup import cleanup


def parse_body(testo, target):
    fatto = testo.find("fatto")
    background = motivation = None
    if fatto is not None:
        background = Background()
        background_division = Division()
        if fatto.text:
            heading = Heading(text=fatto.text)
            background_division.append(heading)
            tail = fatto.tail
            m = re.search(r"^ ?\W", tail)
            if m is not None:
                heading._text += m.group()
                tail = tail[m.end():].strip()
            paragraphs = cleanup(tail)
            parse_paragraphs(paragraphs, background_division)
        background.append(background_division)
        target.append(background)

    diritto = testo.find("diritto")
    if diritto is not None:
        motivation = Motivation()
        motivation_division = Division()
        if diritto.text:
            heading = Heading(text=diritto.text)
            motivation_division.append(heading)
            tail = diritto.tail
            m = re.search(r"^ ?\W", tail)
            if m is not None:
                heading._text += m.group()
                tail = tail[m.end():].strip()
            paragraphs = cleanup(tail)
            parse_paragraphs(paragraphs, motivation_division)
        motivation.append(motivation_division)
        target.append(motivation)

    if testo.text is not None:
        if background or motivation:
            (background or motivation).append(P(text=testo.text))
        else:
            paragraphs = cleanup(testo.text)
            parse_paragraphs(paragraphs, target, structure=True)
