<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1966</anno_pronuncia>
    <numero_pronuncia>3</numero_pronuncia>
    <ecli>ECLI:IT:COST:1966:3</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Aldo Sandulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/01/1966</data_decisione>
    <data_deposito>13/01/1966</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art.  28,  secondo  &#13;
 comma,  n.  5,  del  Codice  penale, promosso con ordinanza emessa il 6  &#13;
 maggio 1965 dal Tribunale di Varese nel procedimento penale a carico di  &#13;
 Zona Carlo, iscritta al n. 101 del Registro ordinanze 1965 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n.  163 del 3 luglio 1965.     &#13;
     Udita nella camera di consiglio del 18 novembre 1965  la  relazione  &#13;
 del Giudice Aldo Sandalli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza emessa il 6 maggio 1965 nel giudizio penale a carico  &#13;
 di Zona Carlo il Tribunale di Varese, ritenutane la rilevanza  ai  fini  &#13;
 del decidere, e consideratala non manifestamente infondata, rimetteva a  &#13;
 questa  Corte la questione di legittimità costituzionale dell'art. 28,  &#13;
 secondo comma, n. 5, del Codice penale (in base al quale l'interdizione  &#13;
 perpetua dai pubblici uffici priva il condannato "degli stipendi, delle  &#13;
 pensioni e degli assegni che siano a carico dello Stato o di altro ente  &#13;
 pubblico"), per il fatto che la perdita delle pensioni e degli assegni,  &#13;
 da  esso  prevista,  contrasterebbe  con  gli artt. 3, primo comma, 27,  &#13;
 terzo comma, 38, secondo comma, e 47, primo comma, della  Costituzione.  &#13;
 Ciò:   1) a causa della disparità di trattamento tra correi a seconda  &#13;
 che il trattamento di quiescenza sia dovuto a essi a carico di un  ente  &#13;
 pubblico  o  di  un  soggetto  privato;  2)  a  causa del carattere non  &#13;
 rieducativo della pena accessoria in esame, essendo questa destinata  a  &#13;
 rendere  più  difficile  il  reinserimento  del  condannato nella vita  &#13;
 sociale;  3)  a  causa  della  imposizione  della  perdita   di   somme  &#13;
 costituenti  - come l'indennità di buonuscita - una forma di risparmio  &#13;
 coattivo, con conseguente indebito arricchimento da parte dell'ente; 4)  &#13;
 a  causa  della  imposizione  della  perdita  di  somme  derivanti   da  &#13;
 provvidenze  di  natura  previdenziale  e  destinate  ad  assicurare al  &#13;
 lavoratore mezzi  adeguati  alle  sue  esigenze  di  vita  in  caso  di  &#13;
 invalidità   e  vecchiaia,  e  costituenti,  peraltro,  una  forma  di  &#13;
 retribuzione differita.                                                  &#13;
     È evidente che, pur non menzionandolo  espressamente,  l'ordinanza  &#13;
 chiama  in  questione,  con  questo ultimo riferimento, anche l'art. 36  &#13;
 della  Costituzione,  attinente  al   diritto   dei   lavoratori   alla  &#13;
 retribuzione.                                                            &#13;
     L'ordinanza,   letta  in  udienza  presente  l'imputato,  è  stata  &#13;
 notificata al Presidente del Consiglio dei Ministri il 13 maggio 1965 e  &#13;
 comunicata ai Presidenti dei due rami del Parlamento l'8  dello  stesso  &#13;
 mese.  Essa  è  stata pubblicata nella Gazzetta Ufficiale del 3 luglio  &#13;
 1965, n.  163, edizione speciale.                                        &#13;
     Innanzi a questa Corte nessuno si è costituito.  Pertanto la causa  &#13;
 è stata trattata in camera di consiglio.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Il giudizio in esame trae origine da un reato commesso  da  un  &#13;
 impiegato  di un ente pubblico assistenziale, avente diritto, in quanto  &#13;
 tale,  a  un  trattamento  di  quiescenza  a  carico   della   pubblica  &#13;
 Amministrazione;  e  la questione di legittimità sottoposta alla Corte  &#13;
 con  l'ordinanza  di  rimessione  è  tutta  articolata  con  esclusivo  &#13;
 riferimento alla privazione dei lavoratori, nei confronti dei quali sia  &#13;
 stata comminata una pena cui si accompagni la interdizione dai pubblici  &#13;
 uffici,  dei  trattamenti economici ad essi spettanti, nei confronti di  &#13;
 pubbliche Amministrazioni,  in  conseguenza  appunto  del  rapporto  di  &#13;
 lavoro.  Il  giudizio  della  Corte  relativo  alla  legittimità della  &#13;
 disposizione dell'art. 28, secondo comma,  n.  5,  del  Codice  penale,  &#13;
 sottoposta  al  suo  esame  -  in  base  alla  quale l'interdizione dai  &#13;
 pubblici uffici comporta la privazione "degli stipendi, delle  pensioni  &#13;
 e  degli  assegni  che  siano  a  carico dello Stato o di un altro ente  &#13;
 pubblico" - deve  essere  perciò  limitato  alla  ipotesi  in  cui  il  &#13;
 condannato  sia  privato  dei  diritti  anzidetti, e non va esteso alle  &#13;
 ipotesi relative a  trattamenti  economici  non  aventi  titolo  in  un  &#13;
 rapporto  di  lavoro  (quali  le  pensioni  di  guerra, le pensioni "di  &#13;
 grazia" e simili).                                                       &#13;
     Entro i riferiti limiti la Corte ritiene fondata la  questione,  ai  &#13;
 sensi degli artt. 36 e 3 della Costituzione.                             &#13;
     Con  riferimento  all'art.  36, è da osservare che la retribuzione  &#13;
 dei lavoratori - tanto quella corrisposta nel  corso  del  rapporto  di  &#13;
 lavoro,  quanto quella differita, a fini previdenziali, alla cessazione  &#13;
 di  tale  rapporto,  e  corrisposta,  sotto  forma  di  trattamento  di  &#13;
 liquidazione   o  di  quiescenza,  a  seconda  dei  casi,  allo  stesso  &#13;
 lavoratore e ai suoi aventi causa -  rappresenta,  nel  vigente  ordine  &#13;
 costituzionale (che, tra l'altro, l'art. 1 della Costituzione definisce  &#13;
 fondato  sul  lavoro), una entità fatta oggetto, sul piano morale e su  &#13;
 quello patrimoniale, di particolare protezione.  L'art.  36  garantisce  &#13;
 espressamente   il  diritto  ad  una  retribuzione  proporzionata  alla  &#13;
 quantità e qualità del lavoro prestato ed in ogni caso sufficiente ad  &#13;
 assicurare  al  lavoratore  e  alla  famiglia  un'esistenza  libera   e  &#13;
 dignitosa. E non appare compatibile con i principi ispiratori di questo  &#13;
 precetto  costituzionale  collegare indiscriminatamente (come fa l'art.  &#13;
 28, n. 5, del Codice penale, integrato dall'art. 29), per il  personale  &#13;
 degli  enti  pubblici e i loro aventi causa, la perdita di tale diritto  &#13;
 al fatto che il titolare di esso abbia  riportato  la  condanna  a  una  &#13;
 certa pena detentiva.                                                    &#13;
     La  Corte  non intende escludere in via assoluta la possibilità di  &#13;
 misure del genere di quella in esame a carico di trattamenti  economici  &#13;
 traenti  titolo  da  un rapporto di lavoro. Non può ritenersi conforme  &#13;
 alla Costituzione però  che  una  sanzione  siffatta  venga  collegata  &#13;
 puramente  e  semplicemente  all'entità della pena detentiva inflitta,  &#13;
 così come attualmente dispone l'art. 29 del Codice penale.              &#13;
     È  da  aggiungere  poi,   con   riferimento   all'art.   3   della  &#13;
 Costituzione,  che  la  disposizione denunciata non appare conciliabile  &#13;
 col fatto che il trattamento retributivo avente titolo in  un  rapporto  &#13;
 di  lavoro riveste carattere non dissimile, nella sostanza, - e anche a  &#13;
 tale riguardo ha decisiva importanza l'art. 36 -,  a  seconda  che  sia  &#13;
 posto  a  carico  di  una  pubblica  Amministrazione  o  di un soggetto  &#13;
 privato.                                                                 &#13;
     In tale situazione, la disparità fatta dall'art.  28,  n.  5,  del  &#13;
 Codice   penale  correlato  con  l'art.  29,  in  danno  delle  persone  &#13;
 retribuite a carico di enti pubblici e dei loro aventi causa,  ai  fini  &#13;
 della   automatica  e  indiscriminata  perdita,  in  conseguenza  della  &#13;
 interdizione dai pubblici uffici per qualsiasi causa,  del  trattamento  &#13;
 economico  collegato  al  rapporto  di  lavoro,  non  appare ispirata a  &#13;
 ragioni sufficienti a giustificarla, né poggiata su idonea base.        &#13;
     Ritiene  perciò  la  Corte  che,  con   riferimento   ai   diritti  &#13;
 collegantisi  a  un  rapporto di lavoro, la norma del n. 5 dell'art. 28  &#13;
 del Codice penale sia, nella attuale  formulazione,  costituzionalmente  &#13;
 illegittima.                                                             &#13;
     2.  -  Rimane in tal modo assorbita la questione se la disposizione  &#13;
 denunciata  contrasti  anche  con  gli  artt.  27,  38   e   47   della  &#13;
 Costituzione.                                                            &#13;
     3.  -  La  Corte  ritiene  che  la  dichiarazione di illegittimità  &#13;
 costituzionale debba essere estesa - ai sensi dell'art. 27 della  legge  &#13;
 11  marzo  1953,  n.  87 - anche al terzo comma dell'art. 28 del Codice  &#13;
 penale, riguardante la interdizione  temporanea  dai  pubblici  uffici,  &#13;
 nella  parte  in  cui  dispone  che  per  la sua durata questa priva il  &#13;
 condannato della capacità di  acquistare,  esercitare  o  godere  quei  &#13;
 diritti  contemplati  nel  n. 5 del comma precedente, la privazione dei  &#13;
 quali in  conseguenza  della  interdizione  perpetua  viene  dichiarata  &#13;
 illegittima.  Ma  naturalmente  tale  estensione della dichiarazione di  &#13;
 illegittimità deve esser contenuta negli stessi limiti  in  cui  viene  &#13;
 dichiarata la illegittimità del secondo comma n. 5.                     &#13;
     4. - Inoltre la dichiarazione di illegittimità deve essere estesa,  &#13;
 fin  da  ora,  alle  seguenti  altre disposizioni, che sono tra le più  &#13;
 importanti di quelle aventi il medesimo contenuto:                       &#13;
     1) art. 183, comma primo, lett. a,  e  comma  terzo,  del  T.U.  21  &#13;
 febbraio 1895, n. 70, sulle pensioni civili e militari;                  &#13;
     2)  art.  29,  comma  primo, lett. a, e comma quarto, del R.D.L. 31  &#13;
 dicembre 1925, n. 2383, sul trattamento  di  quiescenza  dei  salariati  &#13;
 statali;                                                                 &#13;
     3)  art. 43, comma primo, n. 1, e comma secondo, del R.D.L. 3 marzo  &#13;
 1938, n.  680,  sull'ordinamento  della  Cassa  di  previdenza  per  le  &#13;
 pensioni degli impiegati degli enti locali;                              &#13;
     4)  art.  42,  comma  primo, n. 1, e comma secondo, e art. 43 della  &#13;
 legge  25  luglio  1941,  n.  934,  sull'ordinamento  della  Cassa   di  &#13;
 previdenza per le pensioni ai salariati degli enti locali;               &#13;
     5)  art.  36,  comma  primo,  e art. 37, comma primo, della legge 6  &#13;
 luglio 1939, n. 1035, sull'ordinamento della Cassa di previdenza per le  &#13;
 pensioni dei sanitari.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale  dell'art.  28,  secondo  &#13;
 comma,  n.  5,  del  Codice  penale,  limitatamente alla parte in cui i  &#13;
 diritti in esso previsti traggono titolo da un rapporto di lavoro;       &#13;
     dichiara inoltre, a norma dell'art. 27 della legge 11  marzo  1953,  &#13;
 n. 87, l'illegittimità costituzionale:                                  &#13;
     1)  del  terzo  comma  dello  stesso art. 28 del Codice penale, nei  &#13;
 medesimi limiti;                                                         &#13;
     2) dell'art. 183, comma primo, lett. a, e comma terzo, del T.U.  21  &#13;
 febbraio 1895, n. 70, sulle pensioni civili e militari;                  &#13;
     3)  dell'art.  29, comma primo, lett. a, e comma quarto, del R.D.L.  &#13;
 31 dicembre 1925, n. 2383, sul trattamento di quiescenza dei  salariati  &#13;
 statali;                                                                 &#13;
     4)  dell'art.  43, comma primo, n. 1, e comma secondo, del R.D.L. 3  &#13;
 marzo 1938, n. 680, sull'ordinamento della Cassa di previdenza  per  le  &#13;
 pensioni degli impiegati degli enti locali;                              &#13;
     5) dell'art. 42, comma primo, n. 1, e comma secondo, e dell'art. 43  &#13;
 della  legge  25  luglio  1941, n. 934, sull'ordinamento della Cassa di  &#13;
 previdenza per le pensioni ai salariati degli enti locali;               &#13;
     6) dell'art. 36, comma primo, e dell'art. 37,  comma  primo,  della  &#13;
 legge   6  luglio  1939,  n.  1035,  sull'ordinamento  della  Cassa  di  &#13;
 previdenza per le pensioni dei sanitari.                                 &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 7 gennaio 1966.         &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   COSTANTINO   MORTATI  -  &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO PAOLO BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
