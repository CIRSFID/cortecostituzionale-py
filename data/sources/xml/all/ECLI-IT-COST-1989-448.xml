<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>448</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:448</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/07/1989</data_decisione>
    <data_deposito>25/07/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 253, della    &#13;
 legge  Regione  Sicilia   15   marzo   1963,   n.   16   (Ordinamento    &#13;
 amministrativo degli Enti locali nella Regione siciliana) e dell'art.    &#13;
 265 regio decreto 3 marzo 1934, n. 383 (Approvazione del testo  unico    &#13;
 della legge comunale e provinciale), promosso con ordinanza emessa il    &#13;
 15 gennaio 1988 dalla Corte dei conti - Sez. giurisdizionale  per  la    &#13;
 Regione   Sicilia   nel  giudizio  di  responsabilità  promosso  dal    &#13;
 Procuratore Generale contro Turano Aurelio ed altri, iscritta  al  n.    &#13;
 229 del registro ordinanze 1989 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 19, prima serie speciale, dell'anno 1989;            &#13;
    Udito  nella  camera  di  consiglio  del  5 luglio 1989 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che,  nel  corso  di  un  giudizio  avente ad oggetto la    &#13;
 responsabilità di alcuni amministratori e di un dipendente  comunale    &#13;
 per  il  danno causato all'ente dall'irregolare gestione del servizio    &#13;
 di applicazione e riscossione di tributi comunali la Corte dei  conti    &#13;
 -  sez.  giur. Regione Sicilia, con ordinanza in data 15 gennaio 1988    &#13;
 (pervenuta a questa Corte il 22 aprile 1989), ha sollevato  questione    &#13;
 di legittimità costituzionale degli artt. 253, legge Regione Sicilia    &#13;
 15 marzo 1963, n. 16 (che approva l'Ordinamento amministrativo  degli    &#13;
 enti  locali nella regione siciliana) e 265 del regio decreto 3 marzo    &#13;
 1934, n. 383 (Approvazione del testo unico  della  legge  comunale  e    &#13;
 provinciale);                                                            &#13;
      che le norme impugnate vengono censurate nella parte in cui, non    &#13;
 consentendo che i dipendenti degli  enti  locali  siano  assoggettati    &#13;
 alla  giurisdizione  della  Corte  dei  conti, ove risultino coautori    &#13;
 degli eventi dannosi espressamente previsti  per  gli  amministratori    &#13;
 dagli artt. 244 dell'Ordinamento amministrativo della regione Sicilia    &#13;
 e 254-259 del testo unico della  legge  comunale  e  provinciale,  si    &#13;
 porrebbero in contrasto:                                                 &#13;
        a)  con  l'art.  3  della  Costituzione, per la ingiustificata    &#13;
 disparità di trattamento che, per un  medesimo  fatto  illecito,  si    &#13;
 verrebbe  a creare tra amministratori e dipendenti di uno stesso ente    &#13;
 locale, attesa la diversità non puramente procedurale, dei regimi di    &#13;
 accertamento  delle relative responsabilità (grado di colpa, termini    &#13;
 prescrizionali, iniziativa dell'azione,  potere  riduttivo),  nonché    &#13;
 per  violazione  della  regola del simultaneus processus che, tesa ad    &#13;
 evitare il rischio  di  decisioni  contrastanti  o  di  incompletezza    &#13;
 nell'esame dei fatti e del relativo contributo causale, troverebbe il    &#13;
 suo principale fondamento nel principio di  eguaglianza  di  tutti  i    &#13;
 cittadini di fronte alla legge;                                          &#13;
        b)  con  l'art. 97 della Costituzione, in quanto la suindicata    &#13;
 diversità di trattamento violerebbe il principio di imparzialità  e    &#13;
 di buon andamento della pubblica amministrazione;                        &#13;
        c)  con gli artt. 25 e 103, secondo comma, della Costituzione,    &#13;
 in quanto senza alcuna  logica  giustificazione,  si  sottrarrebbero,    &#13;
 anche  nelle  ipotesi di responsabilità connesse, i dipendenti degli    &#13;
 enti locali alla giurisdizione della Corte dei conti che, in  materia    &#13;
 di contabilità pubblica, è "giudice naturale";                         &#13;
      che  non  si sono costituite le parti né ha spiegato intervento    &#13;
 l'Avvocatura generale dello Stato;                                       &#13;
    Considerato  che  questa  Corte  con  sentenza  n. 411 del 1988 ha    &#13;
 dichiarato l'inammissibilità e successivamente, con le ordinanze nn.    &#13;
 549  e  794 del 1988 e 162 del 1989, la manifesta inammissibilità di    &#13;
 questioni identiche a  quelle  ora  sollevate,  con  motivazioni  che    &#13;
 coinvolgono  tutte le argomentazioni svolte a sostegno della presente    &#13;
 ordinanza di rimessione;                                                 &#13;
      che il giudice a quo non deduce profili nuovi o diversi, tali da    &#13;
 indurre questa Corte ad una modifica del proprio orientamento;           &#13;
      che  le  proposte  questioni  devono  essere pertanto dichiarate    &#13;
 manifestamente inammissibili;                                            &#13;
    Visti  gli artt. 26, secondo comma, legge 11 marzo 1953, n. 87 e 9    &#13;
 delle  norme  integrative  per   i   giudizi   davanti   alla   Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   delle  questioni  di    &#13;
 legittimità  costituzionale   degli   artt.   253   dell'Ordinamento    &#13;
 amministrativo  degli  enti locali nella regione siciliana, approvato    &#13;
 con legge reg. Sicilia 15 marzo 1963, n. 16 e 265 del regio decreto 3    &#13;
 marzo  1934,  n. 383, sollevate in riferimento agli artt. 3, 25, 97 e    &#13;
 103, comma secondo della Costituzione, dalla Corte  dei  conti,  sez.    &#13;
 giur. reg. Sicilia, con l'ordinanza indicata in epigrafe.                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: DI PAOLA                          &#13;
    Depositata in cancelleria il 25 luglio 1989.                          &#13;
                        Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
