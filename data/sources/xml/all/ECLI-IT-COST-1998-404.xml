<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>404</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:404</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/12/1998</data_decisione>
    <data_deposito>12/12/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 10, lettera c),    &#13;
 del  d.P.R.  29  settembre  1973,  n.  597  (Istituzione e disciplina    &#13;
 dell'imposta  sul  reddito  delle  persone  fisiche),  promosso   con    &#13;
 ordinanza   emessa  l'8  luglio  1997  dalla  Commissione  tributaria    &#13;
 regionale di Firenze sul ricorso proposto dall'Ufficio delle  imposte    &#13;
 dirette  di  Firenze  contro Barbieri Umberto, iscritta al n. 739 del    &#13;
 registro ordinanze 1997 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 44, prima serie speciale, dell'anno 1997.                  &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 25  novembre  1998  il  giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
   Ritenuto  che - nel corso di un giudizio di appello, riguardante il    &#13;
 preteso diritto d'un  contribuente  di  dedurre  dal  reddito  IRPEF,    &#13;
 relativo   agli  anni  1984,  1985  e  1986,  gli  interessi  passivi    &#13;
 corrisposti a  fronte  di  un  finanziamento  ipotecario  finalizzato    &#13;
 all'acquisto  di un immobile - la Commissione tributaria regionale di    &#13;
 Firenze, con ordinanza emessa l'8 luglio 1997, ha sollevato questione    &#13;
 di legittimità costituzionale dell'art. 10, lettera c),  del  d.P.R.    &#13;
 29  settembre 1973, n. 597 (Istituzione e disciplina dell'imposta sul    &#13;
 reddito delle persone fisiche), dove  è  prevista  la  deducibilità    &#13;
 degli  "interessi  passivi ed oneri accessori pagati in dipendenza di    &#13;
 mutui garantiti da ipoteca su immobili";                                 &#13;
     che, secondo la rimettente, tale deducibilità non è applicabile    &#13;
 ad analoghi rapporti contrattuali,  e  tuttavia  l'analisi  comparata    &#13;
 delle  figure  negoziali  del  mutuo (artt. 1813 e segg. cod. civ.) e    &#13;
 dell'apertura di credito bancario (artt. 1842  e  segg.  cod.  civ.),    &#13;
 alle  quali  viene  funzionalmente  collegata  la  prestazione  della    &#13;
 garanzia reale ipotecaria (rispettivamente ex artt. 2808 e 1844  cod.    &#13;
 civ.),   porterebbe   ad   escludere  la  sussistenza  di  differenze    &#13;
 strutturali ed effettuali per  i  due  tipi  di  contratto,  entrambi    &#13;
 produttivi  di  interessi  a  carico  del  beneficiario  ed  entrambi    &#13;
 finalizzati al finanziamento dell'acquisto dell'abitazione;              &#13;
     che,  sempre  secondo  la  rimettente,   la   limitazione   della    &#13;
 deducibilità  degli  interessi  passivi  al  solo contratto di mutuo    &#13;
 ipotecario pone dunque la denunciata norma in contrasto con gli artt.    &#13;
 3  e  53  della  Costituzione,  rispettivamente,  per  disparità  di    &#13;
 trattamento  di  situazioni  equivalenti  e per lesione del principio    &#13;
 della concorrenza alle spese  pubbliche  in  ragione  della  concreta    &#13;
 capacità contributiva di ciascun cittadino;                             &#13;
     che  è  intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, concludendo per l'infondatezza della sollevata questione.         &#13;
   Considerato  che  la  denunciata norma, ripetutamente sottoposta al    &#13;
 vaglio  di  costituzionalità,  è  stata  sempre  ritenuta  non   in    &#13;
 contrasto  con i princìpi di uguaglianza e di capacità contributiva    &#13;
 (v. sentenza n. 143 del 1982, che il giudice a  quo  ha  mostrato  di    &#13;
 ignorare  insieme  con tutta la successiva conforme giurisprudenza di    &#13;
 questa Corte);                                                           &#13;
     che in detta sentenza si è osservato come non  irragionevolmente    &#13;
 il  legislatore  abbia  ritenuto necessaria, al fine di consentire la    &#13;
 deducibilità  degli  interessi  passivi,  la  coesistenza  dei   due    &#13;
 requisiti  della natura reale del contratto e della pubblicità della    &#13;
 garanzia ipotecaria (v. anche le ordinanze n. 365 del 1983 e  n.  549    &#13;
 del 1987);                                                               &#13;
     che,  nell'assenza  di  ulteriori  argomentazioni  offerte  dalla    &#13;
 rimettente,  è  perciò  sufficiente,   onde   escludere   qualsiasi    &#13;
 contrasto  con  i  parametri  evocati,  ribadire  che  - nell'a'mbito    &#13;
 dell'ampia  discrezionalità  spettante  al  legislatore  in  materia    &#13;
 fiscale  (v.,  da  ultimo, sentenza n. 227 del 1998) - la limitazione    &#13;
 della deducibilità ai soli  interessi  passivi  derivanti  da  mutui    &#13;
 ipotecari  trova  giustificazione  nell'esigenza dell'Amministrazione    &#13;
 finanziaria di controllare, celermente e  proficuamente,  l'effettiva    &#13;
 sussistenza  del  negozio  da  cui  nascono  gli  interessi medesimi,    &#13;
 attraverso la congiunta concorrenza dei predetti requisiti,  uno  dei    &#13;
 quali  certamente  manca  nell'ipotesi - oggetto del giudizio a quo -    &#13;
 d'apertura di  credito  bancario,  contratto  meramente  consensuale,    &#13;
 ontologicamente  diverso  dal  mutuo  quanto a struttura, funzione ed    &#13;
 effetti (v. ordinanze n. 342 del 1985 e n. 263 del 1987);                &#13;
     che, pertanto, la questione è manifestamente infondata.             &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 10, lettera  c),  del  d.P.R.  29  settembre    &#13;
 1973, n. 597 (Istituzione e disciplina dell'imposta sul reddito delle    &#13;
 persone  fisiche), sollevata - in riferimento agli artt. 3 e 53 della    &#13;
 Costituzione - dalla Commissione tributaria regionale di Firenze, con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 10 dicembre 1998.                             &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 12 dicembre 1998.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
