<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>392</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:392</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1991</data_decisione>
    <data_deposito>31/10/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, prof. &#13;
 Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, primo comma,    &#13;
 lett.  b),  del  d.P.R. 31 ottobre 1967, n. 1401 (Norme di attuazione    &#13;
 dello Statuto speciale della Regione  Friuli-Venezia  Giulia  per  il    &#13;
 trasferimento    alla   Regione   di   beni   immobili   patrimoniali    &#13;
 disponibili), promosso con ordinanza emessa il 19 giugno  1990  dalla    &#13;
 Corte  di  Cassazione  sui  ricorsi  riuniti  proposti  dalla Regione    &#13;
 Friuli-Venezia Giulia contro il Ministero delle Finanze  ed  altri  e    &#13;
 dall'Ente  Ferrovie dello Stato contro Regione Friuli-Venezia Giulia,    &#13;
 iscritta al n. 307 del registro ordinanze  1991  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  18, prima serie speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Visti gli atti di costituzione della Regione Friuli-Venezia Giulia    &#13;
 e dell'Ente Ferrovie dello Stato;                                        &#13;
    Udito nell'udienza pubblica del 9 luglio 1991 il Giudice  relatore    &#13;
 Francesco Greco;                                                         &#13;
    Uditi  gli  avvocati  Gaspare  Pacia per la Regione Friuli-Venezia    &#13;
 Giulia e l'avvocato dello Stato Franco  Favara  per  l'Ente  Ferrovie    &#13;
 dello Stato;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  La  Regione  Friuli-Venezia Giulia chiedeva al Tribunale di    &#13;
 Trieste di accertare l'appartenenza al patrimonio  disponibile  dello    &#13;
 Stato,  alla  data  del 16 febbraio 1963, dei beni immobili destinati    &#13;
 all'esercizio della soppressa strada ferrata Trieste-Campo  Marzio-S.    &#13;
 Elia-Confine  affinché  potessero essere ritenuti trasferiti ad essa    &#13;
 istante dal 1° gennaio 1965 (ai sensi dell'art. 1, primo  comma,  del    &#13;
 d.P.R. 31 ottobre 1967, n. 1401).                                        &#13;
    Il  Tribunale  accoglieva  la  domanda,  ma  la Corte d'Appello di    &#13;
 Trieste, su impugnazione dell'Ente Ferrovie, la rigettava.               &#13;
    La Regione ricorreva per cassazione.                                  &#13;
    L'Ente Ferrovie proponeva ricorso incidentale, sostenendo  che  la    &#13;
 determinazione  dei  beni trasferiti alla Regione doveva avvenire per    &#13;
 atto legislativo.                                                        &#13;
    Con ordinanza del 19 giugno  1990,  pervenuta  alla  Corte  il  20    &#13;
 aprile  1991  (R.O.  307  del  1991) la Corte di Cassazione sollevava    &#13;
 questione di legittimità costituzionale dell'art. 1  del  d.P.R.  31    &#13;
 ottobre  1967,  n.  1401, nella parte in cui prevede il trasferimento    &#13;
 alla Regione Friuli-Venezia Giulia di tutti i beni immobili,  situati    &#13;
 nel  territorio  regionale,  l'appartenenza  dei  quali al patrimonio    &#13;
 disponibile  dello  Stato,  con riferimento alla data del 16 febbraio    &#13;
 1963, venga accertata con provvedimento giurisdizionale.                 &#13;
    A  parere  del  collegio  remittente,   la   disposizione   citata    &#13;
 violerebbe gli artt. 56, 57 e 65 dello Statuto speciale della Regione    &#13;
 attribuendo  al  giudice,  ovvero  all'autorità  amministrativa,  un    &#13;
 potere  sostitutivo  di  quello  legislativo,  imposto  dalle   norme    &#13;
 costituzionali  (Norme  di  attuazione  dello  Statuto,  indicate  in    &#13;
 decreti legislativi dall'art. 65 citato).                                &#13;
    2. - Nel giudizio è intervenuta la Regione Friuli-Venezia Giulia,    &#13;
 la quale ha osservato che l'art. 57 dello  Statuto  è  compreso  tra    &#13;
 quelle  disposizioni che, in forza dell'art. 63 dello stesso Statuto,    &#13;
 possono essere modificate  con  legge  ordinaria;  che  le  norme  di    &#13;
 attuazione  dello  Statuto, essendo norme contrattate per il previsto    &#13;
 intervento di una Commissione paritetica (Governo statale e Regione),    &#13;
 dovrebbero   essere   ritenute    accettate    se    non    impugnate    &#13;
 tempestivamente.                                                         &#13;
    Nel  merito ha sostenuto che l'art. 57 dello Statuto non contrasta    &#13;
 con l'art. 1 del d.P.R. 1401 del 1967  perché  non  prevede  che  le    &#13;
 norme  di  attuazione  provvedano  alla  determinazione  dei  beni da    &#13;
 trasferirsi  alla  Regione  con  immediatezza  e  con  specificazione    &#13;
 puntuale  di ogni singolo bene; e, peraltro, la determinazione è pur    &#13;
 sempre atto del legislatore di attuazione, anche se fatta  discendere    &#13;
 da  un  elemento  obiettivo di identificazione, indicato dallo stesso    &#13;
 legislatore.                                                             &#13;
    La eventuale divergenza della norma denunciata  rispetto  all'art.    &#13;
 57  dello  Statuto  si  collocherebbe  nell'area praeter legem che è    &#13;
 possibile attribuire alle norme di attuazione.                           &#13;
    3.  -  L'Avvocatura   Generale   dello   Stato,   intervenuta   in    &#13;
 rappresentanza  dell'Ente Ferrovie, nella memoria successiva all'atto    &#13;
 di costituzione ha contestato la qualificazione operata dalla  difesa    &#13;
 della  Regione,  delle norme di attuazione come norme "contrattate" e    &#13;
 l'ipotesi della loro accettazione in caso di mancata impugnazione; ha    &#13;
 poi  insistito  nell'affermazione  che  la  determinazione  dei  beni    &#13;
 trasferiti  alla Regione ai sensi dell'art. 57 dello Statuto speciale    &#13;
 debba essere effettuata con legge.<diritto>Considerato in diritto</diritto>1. - La Corte è chiamata a verificare se l'art. 1,  primo  comma,    &#13;
 lett.  b),  del  d.P.R.  31  ottobre 1967, n. 1401, recante "norme di    &#13;
 attuazione dello Statuto speciale della Regione Friuli-Venezia Giulia    &#13;
 per il trasferimento  alla  Regione  di  beni  immobili  patrimoniali    &#13;
 disponibili",  nella parte in cui stabilisce che sono trasferiti alla    &#13;
 Regione Friuli-Venezia Giulia i beni immobili situati nel  territorio    &#13;
 regionale,  l'appartenenza  dei quali al patrimonio disponibile dello    &#13;
 Stato,  con  riferimento  alla  data  del  16  febbraio  1963,  venga    &#13;
 accertata con provvedimento giurisdizionale, violi gli artt. 56, 57 e    &#13;
 65  dello  Statuto  speciale  della  Regione Friuli-Venezia Giulia, i    &#13;
 quali  demandano  a  norme  di  attuazione  dello   stesso   Statuto,    &#13;
 individuate,  in via generale ed esclusiva, in decreti legislativi da    &#13;
 emettere dopo che  sia  stata  sentita  una  commissione  paritetica,    &#13;
 l'indicazione  dei  beni  da trasferire dallo Stato alla Regione e le    &#13;
 modalità per la consegna degli stessi.                                  &#13;
    2. - La questione non è fondata.                                     &#13;
    L'art.  56  dello  Statuto  speciale  della Regione Friuli-Venezia    &#13;
 Giulia, approvato con legge costituzionale 31 gennaio 1963, n. 1,  ha    &#13;
 disposto il trasferimento alla Regione dei beni immobili patrimoniali    &#13;
 dello  Stato, siti nel territorio regionale, da ritenersi disponibili    &#13;
 alla data della sua entrata in vigore (16 febbraio 1963).                &#13;
    L'art. 65 dello stesso Statuto ha previsto  che  l'attuazione  del    &#13;
 trasferimento   avvenga   con   decreti   legislativi,   sentita  una    &#13;
 Commissione paritetica di sei  membri,  di  cui  tre  di  nomina  del    &#13;
 Governo e tre del Consiglio Regionale.                                   &#13;
    L'art.  1  del  decreto  del  Presidente  della  Repubblica del 31    &#13;
 ottobre 1967, n. 1401, emanato ai sensi e nelle  forme  del  suddetto    &#13;
 art.  65  dello  Statuto  speciale  in  attuazione dell'art. 57 dello    &#13;
 stesso Statuto speciale, ha determinato i beni immobili  patrimoniali    &#13;
 dello  Stato  che  si  trovavano  nel  territorio  regionale ed erano    &#13;
 disponibili alla data di entrata in vigore dello Statuto (16 febbraio    &#13;
 1963). E li ha indicati anzitutto (lett. a) in  uno  apposito  elenco    &#13;
 annesso al decreto.                                                      &#13;
    A  detti  beni  (lett.  b)  ha  aggiunto  quelli che, sia pure con    &#13;
 riferimento alla data  del  16  febbraio  1963,  sarebbero  risultati    &#13;
 appartenenti   allo   Stato   successivamente   o  con  provvedimento    &#13;
 giurisdizionale o con provvedimento amministrativo.                      &#13;
    Anzitutto è da escludere che i beni immobili  patrimoniali  dello    &#13;
 Stato da ritenersi disponibili siano solo quelli indicati nell'elenco    &#13;
 annesso al decreto del Presidente della Repubblica n. 1401 del 1967 e    &#13;
 che  sia  del  tutto  preclusa  l'eventuale esistenza, nel territorio    &#13;
 regionale, di altri beni patrimoniali disponibili dello Stato  omessi    &#13;
 dall'elenco  suddetto  perché al momento della sua redazione di essi    &#13;
 era incerta l'appartenenza allo Stato o il  loro  carattere  di  beni    &#13;
 disponibili.                                                             &#13;
    La disposizione non è in contrasto con lo statuto speciale.          &#13;
    Inoltre,  il  provvedimento  giurisdizionale,  alla pari di quello    &#13;
 amministrativo, ha solo la funzione di accertare l'appartenenza  allo    &#13;
 Stato  del  bene e la sua natura di "disponibile", ma non di attuarne    &#13;
 il   trasferimento   alla   Regione,   che,   invece,   si   verifica    &#13;
 esclusivamente  in base e per effetto della norma di attuazione dello    &#13;
 Statuto.                                                                 &#13;
    Altro  è  l'accertamento  dell'appartenenza  allo  Stato,   della    &#13;
 cessazione  della  demanialità  o  dell'indisponibilità  del bene e    &#13;
 altro è il trasferimento del bene alla Regione.                         &#13;
    Peraltro, non viene nemmeno in discussione la natura  dei  decreti    &#13;
 di  attuazione  dei  quali questa Corte ha ritenuto il carattere e la    &#13;
 veste legislativa (sent. 20 del 1956).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  1,  primo  comma, lett. b), del d.P.R. 31 ottobre 1967, n.    &#13;
 1401 (Norme  di  attuazione  dello  Statuto  speciale  della  Regione    &#13;
 Friuli-Venezia  Giulia  per  il  trasferimento  alla  Regione di beni    &#13;
 immobili patrimoniali disponibili), sollevata,  in  riferimento  agli    &#13;
 artt.  56, 57, 65 dello Statuto speciale della Regione Friuli-Venezia    &#13;
 Giulia, dalla Corte di Cassazione con l'ordinanza in epigrafe.           &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1991.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 31 ottobre 1991.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
