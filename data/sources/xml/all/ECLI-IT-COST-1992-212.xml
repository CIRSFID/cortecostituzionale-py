<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>212</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:212</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/05/1992</data_decisione>
    <data_deposito>11/05/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 53 del d.P.R.    &#13;
 20 dicembre 1979, n. 761 (Stato giuridico del personale delle  unità    &#13;
 sanitarie locali) e dell'art. 1, comma quarto-quinquies, del decreto-legge  27  dicembre  1989, n. 413 (Disposizioni urgenti in materia di    &#13;
 trattamento economico dei dirigenti dello Stato e delle categorie  ad    &#13;
 essi equiparate, nonché in materia di pubblico impiego), convertito,    &#13;
 con  modificazioni, nella legge 28 febbraio 1990, n. 37, promosso con    &#13;
 ordinanza emessa il  18  aprile  1991  dal  Tribunale  Amministrativo    &#13;
 Regionale  della  Calabria  sul  ricorso proposto da Pisano Francesco    &#13;
 contro la Regione Calabria ed altra, iscritta al n. 722 del  registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 51, prima serie speciale, dell'anno 1991;                             &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  del  18  marzo  1992  il  Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto che nel procedimento promosso dal dott. Francesco Pisano,    &#13;
 Direttore  Amministrativo  Capo  Servizio  presso  la  U.S.L.  19  di    &#13;
 Chiaravalle Centrale,  avverso  il  provvedimento  con  il  quale  il    &#13;
 CO.RE.CO.  aveva  annullato  la  delibera  della  predetta  U.S.L. di    &#13;
 trattenimento in servizio  del  ricorrente  fino  al  compimento  del    &#13;
 settantesimo  anno  di  età, il T.A.R. della Calabria, con ordinanza    &#13;
 del 18 aprile 1991 (R.O. n. 722 del 1991), ha sollevato questione  di    &#13;
 legittimità costituzionale dell'art. 53 del d.P.R. 20 dicembre 1979,    &#13;
 n.  761  e  dell'art. 1, comma quarto-quinquies, del decreto-legge 27    &#13;
 dicembre 1989, n. 413, convertito, con modificazioni, nella legge  28    &#13;
 febbraio  1990,  n.  37, nella parte in cui dette norme non prevedono    &#13;
 anche per il personale delle Unità Sanitarie Locali avente qualifica    &#13;
 dirigenziale l'estensione delle  disposizioni  di  cui  all'art.  15,    &#13;
 secondo e terzo comma, della legge 30 luglio 1973, n. 477 ed all'art.    &#13;
 10,   sesto  comma,  del  decreto-legge  6  novembre  1989,  n.  357,    &#13;
 convertito, con modificazioni, nella legge 27 dicembre 1989, n.  417,    &#13;
 che  prevedono il trattenimento in servizio fino al settantesimo anno    &#13;
 di età del personale ivi contemplato per conseguire il massimo della    &#13;
 pensione;                                                                &#13;
      che, ad avviso del giudice a quo, le  norme  censurate,  siccome    &#13;
 prive  di  razionalità e determinatrici di ingiustificata disparità    &#13;
 di trattamento, contrasterebbero con l'art. 3 della  Costituzione  e,    &#13;
 inoltre,  violerebbero  l'art. 38, secondo comma, della Costituzione,    &#13;
 in quanto non assicurerebbero  ad  una  categoria  di  lavoratori  un    &#13;
 adeguato trattamento previdenziale e di fine rapporto;                   &#13;
      che  nel  giudizio  ha spiegato intervento l'Avvocatura Generale    &#13;
 dello Stato, in  rappresentanza  del  Presidente  del  Consiglio  dei    &#13;
 ministri, che ha concluso per la infondatezza della questione;           &#13;
    Considerato  che  questa  Corte,  con sentenza n. 440 del 1991, ha    &#13;
 già dichiarato non fondata analoga questione, in base  al  principio    &#13;
 per  cui,  nel  vigente  quadro  di  riferimento  normativo,  non  è    &#13;
 configurabile una regola generale, per tutti i  pubblici  dipendenti,    &#13;
 di  collocamento a riposo oltre il limite del sessantacinquesimo anno    &#13;
 per il conseguimento del massimo trattamento pensionistico,  ma  solo    &#13;
 la sussistenza di deroghe a favore di determinate categorie, disposte    &#13;
 dal  legislatore  in  virtù  di  discrezionale  apprezzamento  delle    &#13;
 ragioni varie e diverse che di  volta  in  volta  si  presentano  per    &#13;
 ciascuna di esse (v. anche ord. n. 98 del 1992);                         &#13;
      che  non  sono  dedotti argomenti o motivi nuovi per una diversa    &#13;
 decisione;                                                               &#13;
      che,  pertanto,  la  questione  va   dichiarata   manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  53  del  d.P.R.  20  dicembre 1979, n. 761    &#13;
 (Stato giuridico del  personale  delle  unità  sanitarie  locali)  e    &#13;
 dell'art.  1,  comma  quarto-quinquies,  del decretolegge 27 dicembre    &#13;
 1989,  n.  413  (Disposizioni  urgenti  in  materia  di   trattamento    &#13;
 economico  dei  dirigenti  dello  Stato  e  delle  categorie  ad essi    &#13;
 equiparate, nonché in materia di pubblico impiego), convertito,  con    &#13;
 modificazioni,  nella  legge  28 febbraio 1990, n. 37, in riferimento    &#13;
 agli artt. 3 e 38, secondo comma, della Costituzione,  sollevata  dal    &#13;
 Tribunale  Amministrativo  Regionale della Calabria, con ordinanza in    &#13;
 epigrafe.                                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 4 maggio 1992.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria l'11 maggio 1992.                           &#13;
                       Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
