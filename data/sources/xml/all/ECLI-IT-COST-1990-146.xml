<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>146</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:146</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/03/1990</data_decisione>
    <data_deposito>26/03/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei   giudizi   di   legittimità  costituzionale  dell'art.  10  del    &#13;
 decreto-legge 29 gennaio 1983, n. 17 (Misure per il contenimento  del    &#13;
 costo  del  lavoro e per favorire l'occupazione), convertito in legge    &#13;
 25 marzo 1983, n. 79, promossi con le seguenti ordinanze:                &#13;
       1)  ordinanza  emessa il 27 febbraio 1989 dalla Corte dei Conti    &#13;
 sul ricorso proposto da Celani Giovanna ed altri contro la Presidenza    &#13;
 del Consiglio dei ministri, iscritta al n. 538 del registro ordinanze    &#13;
 1989 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  48,    &#13;
 prima serie speciale, dell'anno 1989.                                    &#13;
     2)  ordinanza  emessa  il  20 giugno 1988 dalla Corte dei Conti -    &#13;
 Sezione Giurisdizionale per la Sardegna -  sul  ricorso  proposto  da    &#13;
 Cinti  Giovanni  ed  altra, iscritta al n. 588 del registro ordinanze    &#13;
 1989 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  49,    &#13;
 prima serie speciale, dell'anno 1989.                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 ministri;                                                                &#13;
     Udito  nella  camera di consiglio del 21 febbraio 1990 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
     Ritenuto  che  la  Corte  dei Conti, con ordinanze 20 giugno 1988    &#13;
 (R.O. n. 588 del 1989) e 27 febbraio 1989 (R.O. n. 538 del 1989),  ha    &#13;
 sollevato,   in   riferimento  agli  artt.  3,  36,  38  e  97  della    &#13;
 Costituzione, questione di legittimità costituzionale  dell'art.  10    &#13;
 del decreto-legge 29 gennaio 1983, n. 17, come convertito nella legge    &#13;
 25 marzo 1983, n. 79, nella parte in cui  dispone  che  al  personale    &#13;
 avente   diritto   all'indennità  integrativa  speciale,  a  partire    &#13;
 dall'entrata in vigore di detto  decreto,  ove  presenti  domanda  di    &#13;
 pensionamento    anticipato,    la    misura    dell'indennità,   da    &#13;
 corrispondersi in aggiunta alla pensione, deve essere determinata  in    &#13;
 ragione  di  un quarantesimo, per ogni anno di servizio utile ai fini    &#13;
 del trattamento di quiescenza,  dell'importo  dell'indennità  stessa    &#13;
 spettante   al   personale  collocato  in  pensione  con  la  massima    &#13;
 anzianità di servizio;                                                  &#13;
     Considerato  che  questa  Corte,  con sentenza n. 531 del 1988 ha    &#13;
 già dichiarato la questione non fondata in riferimento agli artt. 36    &#13;
 e   38   della   Costituzione   ed,   in   seguito,  l'ha  dichiarata    &#13;
 manifestamente infondata con ordinanza n. 273 del 1989;                  &#13;
       che i profili nuovi sollevati attengono alla dedotta violazione    &#13;
 degli artt. 3 e 97 della Costituzione, in quanto: a) non sarebbe equo    &#13;
 né  razionale  permettere  il pensionamento anticipato a domanda, ma    &#13;
 ridurre  l'indennità  integrativa  in  proporzione  degli  anni   di    &#13;
 servizio;  b)  i  dipendenti destituiti, prima dell'entrata in vigore    &#13;
 del decreto-legge 2  novembre  1985,  n.  594,  avrebbero  goduto,  a    &#13;
 differenza di quelli dimissionari, di un più favorevole trattamento;    &#13;
       che   la   questione,   anche   sotto   tali   profili,  appare    &#13;
 manifestamente infondata, essendo logica e aderente al principio  del    &#13;
 buon  andamento  della  pubblica amministrazione, come già enunciato    &#13;
 nella  sentenza  n.  531  del  1988,  la  riduzione   dell'indennità    &#13;
 integrativa,  in  caso di pensionamento anticipato per dimissioni, in    &#13;
 proporzione degli anni di servizio prestato e nessun rilievo  potendo    &#13;
 avere,  in  sede  di  giudizio  di  legittimità  costituzionale,  la    &#13;
 circostanza che il legislatore solo con il decreto-legge, n. 594  del    &#13;
 1985 (e poi con il decreto-legge 28 febbraio 1986, n. 49, conv. nella    &#13;
 legge 18 aprile 1986, n. 120), abbia esteso il  trattamento  previsto    &#13;
 dalla norma impugnata ai dipendenti destituiti;                          &#13;
     Visti  gli artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  delle    &#13;
 questioni   di   legittimità   costituzionale   dell'art.   10   del    &#13;
 decreto-legge  29 gennaio 1983, n. 17 (Misure per il contenimento del    &#13;
 costo del  lavoro  e  per  favorire  l'occupazione),  convertito  con    &#13;
 modificazioni  nella  legge  25  marzo  1983,  n.  79,  sollevata  in    &#13;
 riferimento agli artt. 3, 36, 38 e 97 della Costituzione, dalla Corte    &#13;
 dei Conti, con le ordinanze indicate in epigrafe.                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 7 marzo 1990.                                 &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 marzo 1990.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
