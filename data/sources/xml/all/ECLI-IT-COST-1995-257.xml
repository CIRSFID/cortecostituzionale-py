<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>257</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:257</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/06/1995</data_decisione>
    <data_deposito>16/06/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  7,  comma  2,    &#13;
 lettera  m),  del  codice di procedura penale, promosso con ordinanza    &#13;
 emessa il 26 ottobre 1994 dal  Pretore  di  Milano  nel  procedimento    &#13;
 penale  a  carico  di  Anna  Denti,  iscritta  al  n. 52 del registro    &#13;
 ordinanze 1995 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 6, prima serie speciale, dell'anno 1995;                              &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 17 maggio 1995 il Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Ritenuto che con ordinanza emessa il 26 ottobre 1994, nel corso di    &#13;
 un procedimento penale nei confronti di Anna Denti, citata a giudizio    &#13;
 con l'imputazione di  truffa  aggravata  ai  danni  dello  Stato,  il    &#13;
 Pretore  di Milano ha sollevato, in riferimento agli artt. 3, 24, 25,    &#13;
 76 e 97, primo comma, della Costituzione, questione  di  legittimità    &#13;
 costituzionale  dell'art.  7,  comma  2,  lettera  m),  del codice di    &#13;
 procedura penale;                                                        &#13;
      che  la  disposizione  denunciata  stabilisce  la competenza del    &#13;
 pretore per giudicare del delitto di truffa  aggravata,  compresa  la    &#13;
 truffa  ai  danni  dello  Stato,  punibile con la reclusione da uno a    &#13;
 cinque anni e con la multa da lire seicentomila a tre  milioni  (art.    &#13;
 640, secondo comma, del codice penale);                                  &#13;
      che  il  giudice  rimettente considera il reato di truffa di non    &#13;
 facile indagine e ritiene che  sia  attribuito  alla  competenza  del    &#13;
 pretore   solo  seguendo  il  criterio  della  quantità  della  pena    &#13;
 edittale,  sicché  a  maggior  ragione  la  truffa   aggravata,   in    &#13;
 precedenza  non  attribuita alla competenza del pretore, non potrebbe    &#13;
 essere ritenuta di rapida  e  facile  indagine,  mentre  si  dovrebbe    &#13;
 considerare  che  tale  delitto  è  spesso collegato con altri reati    &#13;
 contro la pubblica amministrazione, di competenza del tribunale;         &#13;
      che,  ad  avviso  del  giudice   rimettente,   la   disposizione    &#13;
 denunciata  sarebbe  in  contrasto con vari parametri costituzionali:    &#13;
 con l'art. 76, essendo indeterminati i principi e  criteri  direttivi    &#13;
 della  legge  di delegazione (direttiva n. 12 di cui all'art. 2 della    &#13;
 legge 16 febbraio 1987, n. 81),  se  interpretata  in  senso  diverso    &#13;
 dalla  attribuzione  della competenza in base alla ricognizione della    &#13;
 situazione legislativa già esistente; con l'art. 25, per la  mancata    &#13;
 corretta  predeterminazione  del  giudice naturale; con l'art. 3, per    &#13;
 l'irragionevole disparità di trattamento tra cittadini;  con  l'art.    &#13;
 24,  per la compressione del diritto di difesa, mancando nel giudizio    &#13;
 dinanzi al pretore l'udienza preliminare; con l'art. 97, primo comma,    &#13;
 essendo stata la competenza del  pretore  estesa  in  modo  eccessivo    &#13;
 rispetto  alle  strutture ed alle capacità operative di tale ufficio    &#13;
 giudiziario ed in modo incompatibile con  l'obiettivo  della  massima    &#13;
 semplificazione del processo, fissato dalla legge delega;                &#13;
      che nel giudizio dinanzi alla Corte è intervenuto il Presidente    &#13;
 del  Consiglio  dei  ministri, rappresentato e difeso dall'Avvocatura    &#13;
 generale dello Stato, che ha concluso per  la  non  fondatezza  della    &#13;
 questione.                                                               &#13;
    Considerato che l'art. 7 del codice di procedura penale stabilisce    &#13;
 la  competenza  del  pretore  in  attuazione  della  direttiva  n. 12    &#13;
 dell'art. 2 della legge di delegazione n. 81  del  1987,  laddove  si    &#13;
 prevede  l'attribuzione alla cognizione per materia di quel giudice -    &#13;
 sulla base della pena edittale e della qualità del reato - di  tutte    &#13;
 le  contravvenzioni  e  dei  delitti  punibili  con la multa o con la    &#13;
 reclusione non superiore nel massimo a quattro anni, nonché di altri    &#13;
 delitti da indicare specificamente;                                      &#13;
      che la discrezionalità del legislatore delegato  è  delimitata    &#13;
 dalle particolari previsioni della norma delegante, dal complesso dei    &#13;
 criteri  direttivi  impartiti  e  dalle  ragioni e finalità generali    &#13;
 della delega. Quanto alla ripartizione della competenza  per  materia    &#13;
 nel  processo  penale, il legislatore delegante ha tenuto conto della    &#13;
 linea  di  tendenza  volta  ad  aumentare  il  numero  dei  reati  di    &#13;
 competenza del pretore, considerando che nel nuovo processo è venuto    &#13;
 meno  l'ostacolo  costituito  dal  cumulo,  nel pretore stesso, delle    &#13;
 funzioni requirenti e giudicanti, sicché il criterio generale  della    &#13;
 semplificazione  ha  consentito  di  ricorrere  ragionevolmente  alla    &#13;
 snellezza e celerità del processo pretorile per reati  in  relazione    &#13;
 ai quali le indagini non siano con esso incompatibili;                   &#13;
      che,  peraltro,  la  soluzione  adottata  dall'art.  7, comma 2,    &#13;
 lettera m), del codice di procedura penale per  la  truffa  aggravata    &#13;
 non   eccede   i   principi   e   criteri   direttivi  della  delega.    &#13;
 Nell'attribuire al  pretore,  già  competente  secondo  il  criterio    &#13;
 quantitativo  della  pena  per  il  reato  di truffa (art. 640, primo    &#13;
 comma, del codice penale), anche la competenza in ordine  all'ipotesi    &#13;
 di  truffa aggravata (art. 640, secondo comma, del codice penale) non    &#13;
 irragionevolmente è stata  considerata  prevalente,  ai  fini  della    &#13;
 semplificazione   del   processo,   l'attrazione   della   competenza    &#13;
 nell'ambito di quella già prevista per il reato base;                   &#13;
      che non risulta violato l'art. 25 della  Costituzione,  giacché    &#13;
 il  principio  del  giudice naturale precostituito per legge richiede    &#13;
 che la competenza degli organi giurisdizionali sia  predeterminata  e    &#13;
 sottratta    ad   ogni   possibilità   di   arbitrio   mediante   la    &#13;
 precostituzione per legge del giudice in base a  criteri  fissati  in    &#13;
 anticipo  e  non  in vista di singole controversie, mentre non assume    &#13;
 rilievo la presunta maggiore o minore idoneità o qualificazione  che    &#13;
 possa  essere  rivendicata  ovvero  riconosciuta  all'uno o all'altro    &#13;
 organo della giurisdizione (sentenza n. 460 del  1994;  ordinanza  n.    &#13;
 130 del 1995);                                                           &#13;
      che  l'attribuzione  alla  cognizione  del  pretore,  anziché a    &#13;
 quella del  tribunale,  del  delitto  di  truffa  aggravata  previsto    &#13;
 dall'art.  640,  secondo comma, del codice penale non è in contrasto    &#13;
 con l'art. 24 della Costituzione, giacché la difesa si svolge  e  si    &#13;
 sviluppa   in   un   complesso   di   presenze   attive  ed  efficaci    &#13;
 dell'imputato, che lo accompagnano in ogni stato e fase del processo,    &#13;
 qualunque  sia  il  giudice  chiamato  a  decidere   in   base   alla    &#13;
 ripartizione delle competenze (sentenza n. 72 del 1976);                 &#13;
      che,  inoltre,  nessuna  violazione  del  diritto  di  difesa è    &#13;
 determinata dalla mancanza dell'udienza preliminare nel  procedimento    &#13;
 dinanzi   al   pretore,   essendo  tra  l'altro  comunque  consentita    &#13;
 l'immediata declaratoria di determinate cause di non punibilità,  ai    &#13;
 sensi  dell'art.  129 del codice di procedura penale (ordinanza n. 22    &#13;
 del 1995);                                                               &#13;
      che non è violato il  principio  di  eguaglianza,  non  essendo    &#13;
 irragionevole  la  determinazione della competenza dei diversi organi    &#13;
 di giurisdizione, operata dal legislatore nell'ambito di  valutazioni    &#13;
 di natura politica;                                                      &#13;
      che,  infine,  con  riferimento  all'art. 97, primo comma, della    &#13;
 Costituzione, secondo la costante giurisprudenza di questa  Corte  il    &#13;
 principio  del  buon  andamento  della  pubblica amministrazione, pur    &#13;
 potendo  riferirsi  anche  agli  organi  dell'amministrazione   della    &#13;
 giustizia,    attiene    esclusivamente    alle   leggi   concernenti    &#13;
 l'ordinamento degli uffici giudiziari ed il loro funzionamento  sotto    &#13;
 l'aspetto  amministrativo,  mentre è del tutto estraneo alla materia    &#13;
 dell'esercizio della funzione giurisdizionale nel suo  complesso  (da    &#13;
 ultimo  ordinanze n. 39 del 1995 e n. 275 del 1994; sentenze n. 428 e    &#13;
 n. 376 del 1993) e quindi ai criteri di ripartizione delle competenze    &#13;
 tra organi giudiziari;                                                   &#13;
      che,   pertanto,   la   questione   deve    essere    dichiarata    &#13;
 manifestamente infondata.                                                &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 7,  comma  2,  lettera  m),  del  codice  di    &#13;
 procedura  penale, sollevata, in riferimento agli artt. 3, 24, 25, 76    &#13;
 e 97, primo comma, della Costituzione,  dal  Pretore  di  Milano  con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 giugno 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 16 giugno 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
