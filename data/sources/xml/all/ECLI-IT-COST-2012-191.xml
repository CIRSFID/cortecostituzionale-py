<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2012</anno_pronuncia>
    <numero_pronuncia>191</numero_pronuncia>
    <ecli>ECLI:IT:COST:2012:191</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>QUARANTA</presidente>
    <relatore_pronuncia>Mario Rosario Morelli</relatore_pronuncia>
    <redattore_pronuncia>Mario Rosario Morelli</redattore_pronuncia>
    <data_decisione>17/07/2012</data_decisione>
    <data_deposito>19/07/2012</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>illegittimità costituzionale</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della legge della Regione Lazio 5 agosto 2011, n. 9 (Istituzione dell'elenco regionale Made in Lazio - Prodotto in Lazio), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 19 ottobre 2011, depositato in cancelleria il 26 ottobre 2011 ed iscritto al n. 127 del registro ricorsi 2011.&#13;
 Visto  l'atto di costituzione della Regione Lazio;  &#13;
 udito nell'udienza pubblica del 22 maggio 2012 il Giudice relatore Mario Rosario Morelli;&#13;
 uditi  l'avvocato dello Stato Maurizio Di Carlo per il Presidente del Consiglio dei ministri e l'avvocato Renato Marini per la Regione Lazio.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.- Con ricorso notificato il 19 ottobre 2011 e depositato il 26 ottobre 2011, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha sollevato questione di legittimità costituzionale della legge della Regione Lazio 5 agosto 2011, n. 9 (Istituzione dell'elenco regionale Made in Lazio - Prodotto in Lazio), prevedente la realizzazione di un apposito elenco, disponibile sul sito istituzionale della Regione, articolato in tre sezioni destinate a distinguere - sotto le voci "Made in Lazio - tutto Lazio", "Realizzato nel Lazio" e "Materie prime del Lazio" ? rispettivamente i prodotti lavorati nel territorio regionale con materie prime regionali, quelli lavorati nel Lazio con materie prime derivanti da altri territori, e le materie prime appartenenti al Lazio commercializzate per la realizzazione di altri prodotti. &#13;
 Ad avviso del ricorrente, detta legge, pur non istituendo formalmente un marchio di qualità regionale, sarebbe, comunque, volta a tutelare e promuovere la produzione regionale  laziale, pubblicizzando elementi puramente geografici relativi ad alcune o a tutte le fasi di produzione, in modo da indurre il convincimento della esistenza di un protocollo di produzione e di lavorazione di alcuni prodotti tipici della Regione Lazio, che presentino caratteristiche e qualità superiori, così  determinando una interferenza nella circolazione dei prodotti fra le regioni, con l'agevolare la commercializzazione di quelli locali.&#13;
 Dal che l'ipotizzata violazione dell'articolo 117, primo comma, della Costituzione, per il profilo dell'inosservanza dei vincoli comunitari di cui agli articoli da 34 a 36 del Trattato sul funzionamento dell'Unione  europea (TFUE), e dell'articolo 120, primo comma, della Costituzione, per invasione della competenza legislativa statale esclusiva in materia di libera circolazione delle merci.&#13;
 2.- Si è costituita in giudizio la Regione Lazio, concludendo per l'infondatezza del ricorso. All'uopo ha sostenuto che la legge impugnata avrebbe «carattere del tutto neutro», non recando traccia di alcuna previsione istitutiva di marchio, ed ha sottolineato, ribadendolo anche in memoria, come essa sia finalizzata «esclusivamente alla tutela del consumatore,  al quale viene offerto, attraverso l'istituzione dell'elenco di cui trattasi, uno strumento di adeguata e trasparente informazione».<diritto>Considerato in diritto</diritto>1.- Il Presidente del Consiglio dei ministri dubita, come in narrativa detto, della legittimità costituzionale della legge della Regione Lazio 5 agosto 2011, n. 9 (Istituzione dell'elenco regionale Made in Lazio - Prodotto in Lazio), per contrasto con i precetti di cui agli articoli 117, primo comma, della Costituzione, in relazione agli articoli da 34 a 36 del Trattato sul funzionamento dell'Unione europea (TFUE), ed all'articolo 120, primo comma, della Costituzione.&#13;
 2.- La legge impugnata, allo scopo (dichiarato sub articolo 1) di «assicurare ai consumatori una adeguata e trasparente informazione sui prodotti del territorio regionale», detta norme per la realizzazione di un "elenco", tenuto dalla struttura competente in materia di marketing del Made in Lazio, suddiviso (sub art. 2) in tre sezioni, rispettivamente, denominate:&#13;
 	a) "Made in Lazio - tutto Lazio", per i prodotti le cui fasi di lavorazione hanno luogo nel territorio della Regione e per i quali si utilizzano materie prime della Regione stessa;&#13;
 	b) "Realizzato nel Lazio", per i prodotti le cui fasi di lavorazione hanno luogo nel territorio della Regione e per i quali si utilizzano materie prime di importazione o provenienti da altre Regioni;&#13;
 	c) "Materie prime del Lazio", per le materie prime originarie del Lazio che sono commercializzate per la realizzazione di altri prodotti.&#13;
 E reca (nei successivi articoli da 3 a 8) connesse disposizioni per l'inserimento dei prodotti in elenco, organizzazione degli uffici a ciò preposti  e copertura finanziaria.&#13;
 3.- Ad avviso del ricorrente, la previsione analitica delle riferite sezioni «prefigura tre diverse forme di marchiatura basate, sostanzialmente, su una implicita - ma non provata - valutazione di miglior qualità del prodotto, insita nella (sola) circostanza dell'origine territoriale»; la cui menzione «potrebbe, dunque, indurre i consumatori ad acquistare i prodotti laziali, piuttosto che prodotti simili provenienti da altri territori», così creando ostacolo alla libera circolazione delle merci garantita dalle disposizioni del TFUE (articoli 34 a 36).&#13;
 Dal che, appunto, la prospettata violazione dell'art. 117, primo comma, Cost., in relazione alla richiamata normativa europea, e dell'art. 120, primo comma, Cost., che preclude alle regioni l'adozione di provvedimenti che ostacolino in qualsiasi modo la circolazione delle cose tra i rispettivi territori.&#13;
 4.- La Regione ha sostenuto, in contrario, che il proprio intervento normativo - attraverso la prevista istituzione di uno strumento con finalità esclusivamente informative, e non attributive di alcun connotato di superiore qualità, in relazione ai prodotti del territorio laziale - si collochi su un piano meramente attuativo dei principi di cui all'articolo 2, comma 2, lettera c), del decreto legislativo 6 settembre 2005, n. 206 (Codice del consumo), che annovera fra i diritti fondamentali del consumatore quello, appunto, ad una adeguata informazione sul prodotto. &#13;
 Ma deve escludersi, in premessa, che questo argomento giovi alla resistente, la quale - ove fosse, in tesi, esatta una tale prospettazione - avrebbe, comunque, ecceduto dalle proprie competenze, legiferando in materia - quella dell'ordinamento civile, cui è riconducibile la disciplina del codice del consumo - riservata alla competenza esclusiva dello Stato.&#13;
 5.- In riferimento al precetto dell'art. 117, primo comma, Cost. (sui vincoli, all'esercizio della potestà legislativa di Stato e Regioni, derivanti dall'ordinamento comunitario) la questione è, comunque, fondata, assorbito rimanendo il profilo ulteriore di violazione dell'articolo 120, primo comma, Cost.&#13;
 Le disposizioni degli articoli da 34 a 36 del TFUE - che, nel caso in esame, rendono concretamente operativo il parametro dell'art. 117 Cost. - vietano, infatti, agli Stati membri di porre in essere restrizioni quantitative, all'importazione ed alla esportazione, "e qualsiasi misura di effetto equivalente".&#13;
 Nella giurisprudenza della Corte di giustizia (che conforma quelle disposizioni in termini di diritto vivente, ed alla quale occorre far riferimento ai fini della loro incidenza come norme interposte nello scrutinio di costituzionalità), la "misura di effetto equivalente" (alle vietate restrizioni quantitative) è costantemente intesa in senso ampio e fatta coincidere con "ogni normativa commerciale degli Stati membri che possa ostacolare, direttamente o indirettamente, in atto o in potenza, gli scambi intracomunitari" (Corte di giustizia, sentenze 6 marzo 2003, in causa C-6/2002, Commissione delle Comunità europee contro Repubblica Francese; 5 novembre 2002, in causa C-325/2000, Commissione delle Comunità europee contro Repubblica federale di Germania; 11 luglio 1974, in causa 8-1974, Dassonville contro Belgio).&#13;
 Orbene la legge della Regione Lazio in questa sede censurata, mirando a promuovere i prodotti realizzati  in ambito regionale, garantendone siffatta origine, produce, quantomeno "indirettamente" o "in potenza", gli effetti restrittivi sulla libera circolazione delle merci che, anche al legislatore regionale, è inibito di perseguire per vincolo dell'ordinamento comunitario.&#13;
 E ciò, di per sé, ne comporta la declaratoria di illegittimità costituzionale.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara l'illegittimità costituzionale della legge della Regione Lazio 5 agosto 2011, n. 9 (Istituzione dell'elenco regionale Made in Lazio - Prodotto in Lazio). &#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 17 luglio 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 19 luglio 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
