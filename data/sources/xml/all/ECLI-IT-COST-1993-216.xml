<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>216</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:216</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/03/1993</data_decisione>
    <data_deposito>05/05/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi di legittimità costituzionale dell'art. 5, primo comma,    &#13;
 della legge della Regione Umbria 19 luglio 1988,  n.  23  (Disciplina    &#13;
 della  navigazione  sul  Lago  Trasimeno),  promossi  con le seguenti    &#13;
 ordinanze:                                                               &#13;
      1) ordinanza emessa il 21 luglio 1992 dal Pretore di Perugia nel    &#13;
 procedimento civile vertente tra Spina Giovanni  e  la  Provincia  di    &#13;
 Perugia,  iscritta al n. 556 del registro ordinanze 1992 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  41,  prima   serie    &#13;
 speciale, dell'anno 1992;                                                &#13;
      2)  ordinanza  emessa il 29 luglio 1992 dal Pretore di Perugia -    &#13;
 sezione distaccata di Castiglione del Lago  nel  procedimento  civile    &#13;
 vertente  tra  Ceccarini Fabio e la Provincia di Perugia, iscritta al    &#13;
 n. 672 del  registro  ordinanze  1992  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  43, prima serie speciale, dell'anno    &#13;
 1992;                                                                    &#13;
    Visti gli atti di costituzione di Spina Giovanni e della Provincia    &#13;
 di Perugia nonché l'atto di intervento del Presidente del  Consiglio    &#13;
 dei Ministri;                                                            &#13;
    Udito  nell'udienza  pubblica  del  23  febbraio  1993  il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Udito l'Avvocato dello Stato Gaetano Zotta per il  Presidente  del    &#13;
 Consiglio dei Ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.1.  -  Nel  corso  di  un  procedimento  di  opposizione avverso    &#13;
 l'ordinanza-ingiunzione del presidente della Provincia di Perugia per    &#13;
 il  pagamento  di  una  sanzione  pecuniaria,  irrogata   per   avere    &#13;
 l'opponente  navigato  sul lago Trasimeno con una imbarcazione munita    &#13;
 di motore  a  benzina  di  potenza  fiscale  superiore  alla  massima    &#13;
 consentita  (9 cavalli fiscali) dall'art. 5, primo comma, della legge    &#13;
 della Regione Umbria 19 luglio 1988, n. 23, il  Pretore  di  Perugia,    &#13;
 con  ordinanza  del  21  luglio 1992 (R.O. n. 556/1992), ha sollevato    &#13;
 questione di legittimità costituzionale di quest'ultima norma.          &#13;
    Il giudice a quo, dopo aver premesso  che  il  secondo  comma  del    &#13;
 detto  art.  5  deroga al limite dei 9 cavalli fiscali - stabilito in    &#13;
 via generale dal primo comma dello stesso  articolo  -  per  le  sole    &#13;
 imbarcazioni  munite  di motore diesel, consentendo la navigazione di    &#13;
 queste  ultime  anche  con  potenza  fiscale  superiore,  purché  la    &#13;
 "potenza  effettiva"  massima,  come  indicata nel libretto d'uso del    &#13;
 motore, non superi i 25 cavalli fiscali,  dubita  della  legittimità    &#13;
 costituzionale  di detto articolo per asserito contrasto con l'art. 3    &#13;
 della Costituzione.                                                      &#13;
    Sostiene  difatti che la disparità di trattamento, fra gli utenti    &#13;
 di  motori  a  benzina  e  quelli  di  motori  diesel,  "non  sarebbe    &#13;
 razionalmente  giustificata"  in relazione alle finalità della norma    &#13;
 denunciata; finalità che,  nei  lavori  preparatori,  sono  indicate    &#13;
 nella  salvaguardia della sicurezza della navigazione e dell'ambiente    &#13;
 naturale e nel miglioramento  dello  sviluppo  turistico  della  zona    &#13;
 interessata.   Estranea   a  quelle  finalità  sarebbe,  secondo  il    &#13;
 rimettente, la potenza fiscale, prevista per i motori a benzina,  dal    &#13;
 momento  che  le  conseguenze  negative che si vogliono prevenire per    &#13;
 l'ambiente, la sicurezza e lo  sviluppo  turistico  possono  derivare    &#13;
 soltanto dalla potenza effettiva.                                        &#13;
    1.2.  -  Costituitasi  in  giudizio,  la  Provincia  di Perugia ha    &#13;
 eccepito l'inammissibilità della questione per difetto di  interesse    &#13;
 dell'opponente  nel  giudizio  a  quo,  non  avendo  questi impugnato    &#13;
 dinanzi al giudice amministrativo un'ordinanza emanata dal Presidente    &#13;
 della Provincia di Perugia che  aveva  disciplinato  la  materia  nei    &#13;
 termini poi recepiti nella norma denunciata.                             &#13;
    La   Provincia   deduce,  poi,  l'infondatezza  o  addirittura  la    &#13;
 manifesta infondatezza della questione, sostenendo  che  le  profonde    &#13;
 differenze tecniche fra motori diesel e motori a benzina giustificano    &#13;
 la diversità di disciplina, avendo il legislatore operato una scelta    &#13;
 basata  su  valutazioni  discrezionali  d'ordine tecnico che, data la    &#13;
 possibilità di manomissione dei motori  a  benzina  dei  quali  può    &#13;
 essere facilmente maggiorata la potenza, hanno sconsigliato dal poter    &#13;
 fare  riferimento  per  questi ultimi, come per i motori diesel, alla    &#13;
 potenza effettiva.                                                       &#13;
    Con successiva nota illustrativa la stessa Provincia di Perugia ha    &#13;
 prospettato a questa Corte l'opportunità di una  consulenza  tecnica    &#13;
 per  accertamenti  sulle  diverse  caratteristiche  dei  due  tipi di    &#13;
 motori.                                                                  &#13;
    1.3. - L'opponente nel giudizio a quo ha depositato una memoria ai    &#13;
 fini della costituzione nel presente giudizio,  ma  oltre  i  termini    &#13;
 prescritti dagli artt. 25, secondo comma, della legge n. 87/1953, e 3    &#13;
 delle   Norme   integrative   per   i   giudizi  innanzi  alla  Corte    &#13;
 costituzionale del 16 marzo 1956.                                        &#13;
    1.4. - È intervenuto il Presidente della Giunta  regionale  della    &#13;
 Regione Umbria, rappresentato e difeso dall'Avvocatura generale dello    &#13;
 Stato,  che ha eccepito la inammissibilità della questione sotto due    &#13;
 profili:                                                                 &#13;
       a) perché la disciplina prevista  per  i  motori  diesel,  con    &#13;
 riferimento  alla potenza effettiva, è derogatoria rispetto a quella    &#13;
 generale che fa riferimento alla potenza fiscale;                        &#13;
       b) perché il giudice  a  quo,  nel  chiedere  l'estensione  ai    &#13;
 motori  a benzina della disciplina prevista per i motori diesel, formula un quesito d'ordine legislativo cui la Corte costituzionale  non    &#13;
 può  dare  risposta  perché  riservato  alla  discrezionalità  del    &#13;
 legislatore.                                                             &#13;
    Infondata per l'interveniente è, poi,  la  questione  per  motivi    &#13;
 analoghi a quelli sostenuti dalla Provincia di Perugia.                  &#13;
    2.  - Con ordinanza emessa il 29 luglio 1992 (R.O. 672/1992) in un    &#13;
 altro  procedimento  di  opposizione  ad  ordinanza-ingiunzione   del    &#13;
 Presidente  della Provincia di Perugia per violazione del citato art.    &#13;
 5, primo comma, della legge della regione Umbria 19  luglio  1988  n.    &#13;
 23,  il  Pretore di Perugia, Sez. distaccata di Castiglione del Lago,    &#13;
 ha sollevato identica questione di legittimità costituzionale.          &#13;
    Non  si  sono  costituite  in  giudizio  le parti, né ha spiegato    &#13;
 intervento la Regione Umbria.<diritto>Considerato in diritto</diritto>1. - Con due distinte ordinanze è stata  sollevata  questione  di    &#13;
 legittimità  costituzionale  dell'art.  5,  primo comma, della legge    &#13;
 della regione Umbria 19 luglio 1988, n. 23,  il  quale  ammette  alla    &#13;
 navigazione  sul  Lago  Trasimeno le imbarcazioni aventi motore della    &#13;
 potenza massima di 9 cavalli fiscali, diversamente da quanto previsto    &#13;
 dal secondo comma dello stesso art. 5, il quale  consente  l'uso  dei    &#13;
 motori  diesel, di potenza superiore ai 9 cavalli fiscali, purché la    &#13;
 potenza effettiva massima non superi i  25  cavalli  effettivi.  Tale    &#13;
 disparità  di  trattamento, fra imbarcazioni con motori a benzina ed    &#13;
 imbarcazioni con motore diesel, sarebbe  irragionevole  in  relazione    &#13;
 alle   finalità  della  disciplina  che,  come  risulta  dai  lavori    &#13;
 preparatori,  è  ispirata  ad  esigenze  di  tutela  ambientale,  di    &#13;
 sviluppo  turistico  e  di  sicurezza  della  navigazione  -  per  il    &#13;
 perseguimento delle quali dovrebbe  avere  rilievo  solo  la  potenza    &#13;
 effettiva  - e dovrebbe quindi essere eliminata riconducendo anche le    &#13;
 imbarcazioni con motori a  benzina  (primo  comma)  nella  disciplina    &#13;
 prevista per i soli motori diesel (secondo comma).                       &#13;
    2.  -  Stante  l'identità  della  questione i due giudizi possono    &#13;
 essere riuniti e definiti con unica pronuncia.                           &#13;
    3. -  Va  disattesa  l'eccezione  di  inammissibilità  dedotta  -    &#13;
 relativamente   alla  ordinanza  del  Pretore  di  Perugia  (R.O.  n.    &#13;
 556/1992) - dalla Provincia di Perugia. È  difatti  ininfluente  che    &#13;
 l'opponente  non  abbia  impugnato  l'ordinanza  del Presidente della    &#13;
 Provincia stessa che aveva regolato la navigazione sul lago Trasimeno    &#13;
 nel senso poi recepito dalla legge regionale n. 23/1988,  poiché  il    &#13;
 giudice rimettente deve fare in ogni caso applicazione nel giudizio a    &#13;
 quo  della  legge  denunciata,  rispetto  alla quale è priva di ogni    &#13;
 valore tale precedente ordinanza e ciò è sufficiente ai fini  della    &#13;
 rilevanza della questione.                                               &#13;
    4.  - Deve essere invece condivisa l'eccezione di inammissibilità    &#13;
 formulata dalla Regione Umbria  interveniente, sotto il  profilo  del    &#13;
 carattere derogatorio della norma assunta a parametro di raffronto.      &#13;
    Come è stato posto in evidenza dalla difesa della Regione stessa,    &#13;
 la  norma  denunciata  è  frutto  di  una  valutazione discrezionale    &#13;
 d'ordine  tecnico  del  legislatore,  riferita  alle  caratteristiche    &#13;
 proprie dei motori a benzina, diverse da quelle dei motori diesel. Il    &#13;
 secondo comma dell'art. 5 costituisce perciò una deroga prevista per    &#13;
 le  imbarcazioni  con  motori  diesel  rispetto  alla prescrizione di    &#13;
 carattere  generale  contenuta  nel  primo  comma  (cioè  la   norma    &#13;
 denunciata),  che  ammette  alla  navigazione  sul  lago Trasimeno le    &#13;
 imbarcazioni aventi motore con potenza massima di 9 cavalli  fiscali.    &#13;
 Il  carattere  derogatorio  del secondo comma cit. impedisce che esso    &#13;
 possa essere assunto  come  termine  di  paragone  per  censurare  la    &#13;
 disciplina   generale   del   primo  comma,  in  base  alla  costante    &#13;
 giurisprudenza di questa Corte (v. ex plurimis, sent. n. 383/1992).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi  dichiara   inammissibile   la   questione   di    &#13;
 legittimità  costituzionale  dell'art.  5,  primo comma, della legge    &#13;
 della  Regione  Umbria  19  luglio  1988  n.  23  (Disciplina   della    &#13;
 navigazione  sul lago Trasimeno) sollevata, in riferimento all'art. 3    &#13;
 della Costituzione, dal Pretore di Perugia e dal Pretore di Perugia -    &#13;
 Sezione distaccata di Castiglione del Lago, con le ordinanze indicate    &#13;
 in epigrafe.                                                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 23 aprile 1993.                               &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 5 maggio 1993.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
