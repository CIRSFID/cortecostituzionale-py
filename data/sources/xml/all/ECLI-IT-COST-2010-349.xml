<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2010</anno_pronuncia>
    <numero_pronuncia>349</numero_pronuncia>
    <ecli>ECLI:IT:COST:2010:349</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>DE SIERVO</presidente>
    <relatore_pronuncia>Sabino Cassese</relatore_pronuncia>
    <redattore_pronuncia>Sabino Cassese</redattore_pronuncia>
    <data_decisione>29/11/2010</data_decisione>
    <data_deposito>01/12/2010</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'articolo 36, comma 4-ter, del decreto-legge 31 dicembre 2007, n. 248 (Proroga di termini previsti da disposizioni legislative e disposizioni urgenti in materia finanziaria), convertito con modificazioni dalla legge 28 febbraio 2008, n. 31, promossi dal Giudice di pace di Giarre con due ordinanze del 16 giugno 2009, iscritte ai nn. 150 e 151 del registro ordinanze 2010 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 22, prima serie speciale, dell'anno 2010.&#13;
 Visti gli atti di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 17 novembre 2010 il Giudice relatore Sabino Cassese.</epigrafe>
    <testo>Ritenuto che il Giudice di pace di Giarre, con due ordinanze del 16 giugno 2009 (reg. ord. n. 150 e n. 151 del 2010), ha sollevato, in riferimento agli artt. 24 e 97 della Costituzione, questione di legittimità costituzionale dell'art. 36, comma 4-ter, del decreto-legge 31 dicembre 2007, n. 248 (Proroga di termini previsti da disposizioni legislative e disposizioni urgenti in materia finanziaria), convertito, con modificazioni, dalla legge 28 febbraio 2008, n. 31, «nella parte in cui dispone che la nullità delle cartelle di pagamento, per omessa indicazione del responsabile del procedimento di iscrizione a ruolo e di quello di emissione e di notificazione della stessa cartella, operi solo relativamente ai ruoli consegnati agli agenti della riscossione a decorrere dal 1° giugno 2008 e che la mancata indicazione dei responsabili dei procedimenti nelle cartelle di pagamento relative a ruoli consegnati prima di tale data non è causa di nullità delle stesse»;&#13;
 che il giudice rimettente riferisce che «le doglianze della parte attrice», in entrambi i giudizi principali, si riferiscono a cartelle di pagamento emesse sulla scorta di ruoli relativi all'anno 2007 e notificate rispettivamente in data 11 ottobre 2007 (r.o. n. 151 del 2010) e 5 maggio 2008 (r.o. n. 150 del 2010);&#13;
 che il rimettente afferma che la decisione del giudizio principale «deve essere preceduta» dalla soluzione della questione di legittimità costituzionale della disposizione censurata;&#13;
 che la disciplina censurata, secondo il giudice a quo, si porrebbe in contrasto con l'art. 7, comma 2, lettera a), della legge 27 luglio 2000, n. 212 (Disposizioni in materia di statuto dei diritti del contribuente), la quale, come chiarito dalla Corte costituzionale nella sentenza n. 377 del 2007, ha previsto che gli atti dell'amministrazione finanziaria debbano tassativamente indicare il responsabile del procedimento amministrativo allo scopo di assicurare la trasparenza dell'attività amministrativa, la piena informazione del cittadino e la garanzia del diritto di difesa;&#13;
 che, di conseguenza, ad avviso del rimettente, la disciplina impugnata, nella parte in cui limita temporalmente la nullità degli atti privi dell'indicazione del responsabile solo ai ruoli consegnati agli agenti della riscossione a decorrere dal 1° giugno 2008, si porrebbe in «insanabile contrasto» con «i principi costituzionali del buon andamento e dell'imparzialità della pubblica amministrazione, sotto il profilo della trasparenza dell'attività amministrativa e della piena informazione del cittadino (art. 97 Cost.)», nonché con «l'inviolabile diritto di difesa del cittadino medesimo che deve essere in grado di poter esercitare in modo effettivo e non astratto eventuali azioni nei confronti del responsabile del procedimento (art. 24 Cost.)»; &#13;
 che è intervenuto, con riferimento ad entrambi i giudizi, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, osservando che la disposizione censurata sarebbe già stata dichiarata illegittima da questa Corte con la sentenza n. 58 del 2009 e chiedendo che la questione di legittimità costituzionale sollevata venga dichiarata inammissibile.&#13;
 Considerato che il Giudice di pace di Giarre, con due ordinanze del 16 giugno 2009, ha sollevato, in riferimento agli artt. 24 e 97 della Costituzione, questione di legittimità costituzionale dell'art. 36, comma 4-ter, del decreto-legge 31 dicembre 2007, n. 248 (Proroga di termini previsti da disposizioni legislative e disposizioni urgenti in materia finanziaria), convertito, con modificazioni, dalla legge 28 febbraio 2008, n. 31, «nella parte in cui dispone che la nullità delle cartelle di pagamento, per omessa indicazione del responsabile del procedimento di iscrizione a ruolo e di quello di emissione e di notificazione della stessa cartella, operi solo relativamente ai ruoli consegnati agli agenti della riscossione a decorrere dal 1° giugno 2008 e che la mancata indicazione dei responsabili dei procedimenti nelle cartelle di pagamento relative a ruoli consegnati prima di tale data non è causa di nullità delle stesse»;&#13;
 che deve essere preliminarmente disposta la riunione dei giudizi, in quanto concernenti la medesima disposizione e relativi a parametri identici;&#13;
 che sulle questioni di legittimità costituzionale della disposizione censurata questa Corte si è già pronunciata con la sentenza n. 58 del 2009 e, successivamente, con le ordinanze nn. 291 e 221 del 2009, nonché n. 13 del 2010;&#13;
 che, diversamente a quanto affermato dall'Avvocatura generale dello Stato nel presente giudizio, con le richiamate decisioni, questa Corte ha dichiarato non fondate, e poi manifestamente infondate, le questioni sollevate, chiarendo che la norma censurata «dispone per il futuro, comminando, per le cartelle prive dell'indicazione del responsabile del procedimento, la sanzione della nullità, la quale non era invece prevista in base al diritto anteriore»;&#13;
 che, in particolare, nella sentenza n. 58 del 2009, la Corte ha stabilito che la norma censurata non viola l'art. 24 Cost., perché «non incide sulla posizione di chi abbia ricevuto una cartella di pagamento anteriormente al termine da essa indicato», né contrasta con l'art. 97 Cost., che «non impone un particolare regime di invalidità per gli atti privi dell'indicazione del responsabile del procedimento»), né, infine, può ritenersi illegittima in quanto confliggente con «le previsioni della legge n. 212 del 2000», le quali non «hanno rango costituzionale», «neppure come norme interposte»;&#13;
 che, di conseguenza, per le ragioni già indicate nella sentenza n. 58 del 2009, la questione di legittimità costituzionale sollevata è manifestamente infondata. &#13;
 Visti gli artt. 26, comma 2, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</testo>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 riuniti i giudizi, &#13;
 dichiara la manifesta infondatezza delle questioni di legittimità costituzionale dell'articolo 36, comma 4-ter, del decreto-legge 31 dicembre 2007, n. 248 (Proroga di termini previsti da disposizioni legislative e disposizioni urgenti in materia finanziaria), convertito, con modificazioni, dalla legge 28 febbraio 2008, n. 31, sollevate, con riferimento agli art. 24 e 97 della Costituzione, dal Giudice di pace di Giarre con le ordinanze in epigrafe. &#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 29 novembre 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Sabino CASSESE, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria l'1 dicembre 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
