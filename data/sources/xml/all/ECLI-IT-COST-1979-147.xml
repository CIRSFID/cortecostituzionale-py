<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>147</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:147</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Alberto Malagugini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/11/1979</data_decisione>
    <data_deposito>06/12/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente Dott. GIULTO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - &#13;
 Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO &#13;
 BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - &#13;
 Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel   giudizio   di   legittimità  costituzionale  dell'art.  317,  &#13;
 penultimo comma, del codice di procedura penale, promosso con ordinanza  &#13;
 emessa il 16 agosto 1977 dal Giudice istruttore presso il tribunale  di  &#13;
 Lanusei,  nel  procedimento penale a carico di Melis Virgilio ed altri,  &#13;
 iscritta al n. 34  del  registro  ordinanze  1978  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 87 del 29 marzo 1978.             &#13;
     Udito  nella  camera  di  consiglio dell'8 novembre 1979 il Giudice  &#13;
 relatore Alberto Malagugini.                                             &#13;
     Ritenuto che con  l'ordinanza  indicata  in  epigrafe,  il  giudice  &#13;
 istruttore  presso  il  tribunale  di Lanusei ha sollevato questione di  &#13;
 legittimità costituzionale, con riferimento agli artt. 3  e  24  della  &#13;
 Costituzione,  dell'art.   317, terzo comma, cod.proc.pen., nella parte  &#13;
 in cui non prevede il diritto del consulente tecnico  e  del  difensore  &#13;
 dell'imputato  di  assistere  all'esame  di  testimoni cui il perito di  &#13;
 ufficio sia autorizzato ad assistere.                                    &#13;
     Considerato che, come risulta dalla stessa ordinanza di rimessione,  &#13;
 nessuno degli imputati nel procedimento a quo ha nominato un consulente  &#13;
 tecnico talché, a detta dello stesso giudice a quo, la rilevanza della  &#13;
 prospettata questione, "si può porre solo in termini ipotetici";        &#13;
     che  da  ciò   deriva,   limitatamente   all'aspetto   in   esame,  &#13;
 l'inammissibilità della questione medesima.                             &#13;
     Considerato,   inoltre,   per  quanto  riguarda  la  posizione  del  &#13;
 difensore dell'imputato, che questa Corte, con la sentenza  n.  63  del  &#13;
 1972,  ha  ritenuto  compatibili  con  gli artt. 3 e 24 Cost.le vigenti  &#13;
 disposizioni processuali che escludono  gli  esami  testimoniali  ed  i  &#13;
 confronti  dagli  atti  istruttori  cui  i  difensori  hanno diritto di  &#13;
 assistere;                                                               &#13;
     che tale disciplina generale si sottrae a censure  d'illegittimità  &#13;
 costituzionale  anche  nel  caso specifico degli esami testimoniali cui  &#13;
 sia stato ammesso ad assistere il perito d'ufficio, a  sensi  dell'art.  &#13;
 317  cod.proc.pen.,  essendo pertinenti anche in tale ipotesi i criteri  &#13;
 affermati con la sentenza n. 63 del 1972 (richiamata appunto in  questo  &#13;
 senso, in altra sentenza di questa Corte - n. 199 del 1974 - sul citato  &#13;
 art. 317 cod.proc.pen.);                                                 &#13;
     che   inconferente   è   il   richiamo,  fatto  nell'ordinanza  di  &#13;
 rimessione, ai criteri dell'importanza o dell'irripetibilità dell'atto  &#13;
 istruttorio,  altrove  valorizzati  da  questa   Corte   nel   motivare  &#13;
 l'illegittimità di norme escludenti l'assistenza del difensore ad atti  &#13;
 come  l'interrogatorio  dell'imputato  (sentenza  n.  90 del 1970) o la  &#13;
 testimonianza "a futura memoria" (sentenza n. 64 del 1972),  posto  che  &#13;
 l'esame  testimoniale,  effettuato  con  la presenza del perito, non è  &#13;
 formalmente diverso dalle altre testimonianze istruttorie né quanto ad  &#13;
 intrinseca importanza, né quanto a ripetibilità, e che il riesame del  &#13;
 teste  al  dibattimento,  in  contraddittorio  con  la  difesa,  potrà  &#13;
 logicamente   influire   sulla   stessa   valutazione   (od   eventuale  &#13;
 ripetizione) della perizia;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile, nella parte relativa al consulente tecnico,  &#13;
 e  manifestamente  infondata  nella   parte   relativa   al   difensore  &#13;
 dell'imputato,  la questione di legittimità costituzionale dell'art. 3  &#13;
 17, terzo comma, del codice di procedura penale, sollevata in relazione  &#13;
 agli artt. 3 e 24 della Costituzione dal Giudice istruttore  presso  il  &#13;
 tribunale di Lanusei.                                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 30 novembre 1979.       &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE   STEFANO   -   LEOPOLDO   ELIA  -  &#13;
                                   GUGLIELMO ROEHRSSEN - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
