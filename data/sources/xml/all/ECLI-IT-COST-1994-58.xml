<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>58</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:58</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/02/1994</data_decisione>
    <data_deposito>23/02/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare &#13;
 RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 51 e  53  del    &#13;
 d.P.R. 6 marzo 1978, n. 218 (Testo unico delle leggi sugli interventi    &#13;
 nel Mezzogiorno) e degli artt. 4, 16 e 17 della legge 25 giugno 1865,    &#13;
 n.   2359  (Disciplina  delle  espropriazioni  forzate  per  pubblica    &#13;
 utilità), promosso con ordinanza  emessa  il  1  dicembre  1992  dal    &#13;
 Consiglio di Stato - sezione IV giurisdizionale, sul ricorso proposto    &#13;
 dal Prefetto di Cosenza contro Bianco Angelo ed altri, iscritta al n.    &#13;
 597 del registro ordinanze 1993 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 41, prima serie speciale, dell'anno 1993;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 25  gennaio  1994  il  Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto  che  il  Consiglio  di  Stato,  nel corso di un giudizio    &#13;
 d'impugnazione di una sentenza del Tribunale amministrativo regionale    &#13;
 della Calabria - sezione di Catanzaro, che aveva annullato un decreto    &#13;
 di espropriazione di terreni  destinati  ad  opere  per  lo  sviluppo    &#13;
 industriale,   con  ordinanza  in  data  1  dicembre  1992  (R.O.  n.    &#13;
 597/1993), ha sollevato, in riferimento  agli  artt.  3  e  97  della    &#13;
 Costituzione,  questione di legittimità costituzionale "dell'art. 53    &#13;
 del d.P.R. 6 marzo 1978, n. 218 e dei connessi artt. 51 dello  stesso    &#13;
 d.P.R.  e 4, 16 e 17 della legge 25 giugno 1865, n. 2359, nella parte    &#13;
 in cui non dispongono, nei confronti dei soggetti espropriandi, forme    &#13;
 di comunicazione personale dei procedimenti espropriativi avviati";      &#13;
      che,  ad  avviso del collegio remittente, la normativa impugnata    &#13;
 si  porrebbe  in  contrasto  con  il  principio  di  buon   andamento    &#13;
 dell'amministrazione  (art.  97  della  Costituzione)  per la mancata    &#13;
 partecipazione al procedimento dei privati direttamente  interessati,    &#13;
 nonché  con  il principio di uguaglianza (art. 3 della Costituzione)    &#13;
 sotto il profilo della minore tutela, per i proprietari espropriandi,    &#13;
 della  effettività  del  principio  partecipativo,   rispetto   alla    &#13;
 disciplina  dettata  dagli artt. 10 e 11 della legge 22 ottobre 1971,    &#13;
 n. 865 in ordine ai procedimenti ablatori ivi previsti;                  &#13;
      che nel giudizio è intervenuto il Presidente del Consiglio  dei    &#13;
 ministri  con  il  patrocinio  dell'Avvocatura  generale dello Stato,    &#13;
 concludendo per la inammissibilità o la infondatezza della causa;       &#13;
    Considerato che non si rinviene nell'ordinanza alcun elemento  per    &#13;
 stabilire  se  da  parte  dell'espropriante  siano state fornite agli    &#13;
 espropriandi indicazioni utili  al  fine  di  consentire  l'esercizio    &#13;
 della  facoltà  di  formulare  osservazioni  prima dell'adozione del    &#13;
 provvedimento di espropriazione (cfr.  Cons.  Stato,  Ad.  Plen.,  18    &#13;
 giugno 1986, n. 6);                                                      &#13;
      che,  pertanto,  non  è  possibile stabilire la rilevanza della    &#13;
 questione, che è, quindi, manifestamente inammissibile;                 &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  degli  artt. 51 e 53 del d.P.R. 6 marzo    &#13;
 1978,  n.  218  (Testo  unico  delle  leggi  sugli   interventi   nel    &#13;
 Mezzogiorno)  e  4,  16  e  17  della  legge  25 giugno 1865, n. 2359    &#13;
 (Disciplina delle  espropriazioni  forzate  per  pubblica  utilità),    &#13;
 sollevata,  in  riferimento agli artt. 3 e 97 della Costituzione, dal    &#13;
 Consiglio di Stato con l'ordinanza indicata in epigrafe.                 &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 9 febbraio 1994.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                        Il redattore: PESCATORE                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 23 febbraio 1994.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
