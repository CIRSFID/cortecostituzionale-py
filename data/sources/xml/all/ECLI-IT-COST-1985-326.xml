<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>326</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:326</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/12/1985</data_decisione>
    <data_deposito>11/12/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - &#13;
 Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE &#13;
 FERRARI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE &#13;
 GALLO - Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. &#13;
 FRANCESCO GRECO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  92,  sesto  &#13;
 comma,  d.P.R. 31 maggio 1974, n. 416 (recte: d.P.R. 31 maggio 1974, n.  &#13;
 417) ("Norme sullo stato giuridico del personale docente, direttivo  ed  &#13;
 ispettivo  della  scuola  materna,  elementare, secondaria ed artistica  &#13;
 dello Stato"), promosso con ordinanza emessa il  5  novembre  1982  dal  &#13;
 Pretore  di Campobasso negli atti relativi ad un esposto proposto da De  &#13;
 Murtas Gherardo, iscrita al  n.  946  del  registro  ordinanze  1982  e  &#13;
 pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 121 dell'anno  &#13;
 1983.                                                                    &#13;
     Visto  l'atto  di  costituzione  del  Consiglio   Nazionale   degli  &#13;
 Ingegneri nonché l'atto di intervento del Presidente del Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio del 20 novembre 1985 il Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto che, con ordinanza in data 5  novembre  1982  (emessa  nel  &#13;
 contesto di "atti relativi" ad un esposto inoltrato dall'avv. De Murtas  &#13;
 del  Foro  di Lucca, che prospettava "eventuali responsabilità penali"  &#13;
 di ingegneri ed architetti, pubblici dipendenti, esercenti di fatto  la  &#13;
 libera   professione),  il  Pretore  di  Campobasso  ha  denunciato  di  &#13;
 incostituzionalità l'art. 92, comma sesto, del d.P.R. 31 maggio  1974,  &#13;
 n.  417,  il  quale  consente agli insegnanti di essere autorizzati dal  &#13;
 capo dell'istituto all'esercizio di "libere professioni che  non  siano  &#13;
 di   pregiudizio   alla   funzione   docente":  argomentando  che  tale  &#13;
 disposizione si porrebbe in contrasto sia con il precetto dell'art.  98  &#13;
 Cost.,  per  cui "i pubblici impiegati sono al servizio esclusivo della  &#13;
 Nazione", sia con il  principio  di  eguaglianza  (art.  3  Cost.),  in  &#13;
 ragione  del  trattamento  ingiustificatamente  preferenziale riservato  &#13;
 agli insegnanti rispetto  ad  altri  dipendenti  in  possesso  di  pari  &#13;
 requisiti, ai quali l'esercizio della libera professione è precluso;    &#13;
     che  nel  giudizio innanzi alla Corte si è costituito il Consiglio  &#13;
 nazionale   degli   ingegneri,   che   ha   eccepito    la    manifesta  &#13;
 inammissibilità  della questione e, in subordine, la sua infondatezza;  &#13;
 ed ha  anche  spiegato  intervento  il  Presidente  del  Consiglio  dei  &#13;
 ministri, rassegnando analoghe conclusioni.                              &#13;
     Considerato  che dalle premesse della stessa ordinanza di rinvio si  &#13;
 evince che non vi è allo  stato  -  oltreché  formale  imputazione  -  &#13;
 neppure  una  notitia  criminis  di  fatti  di concreto esercizio della  &#13;
 libera professione da parte di docenti  (tali  fatti  non  essendo  del  &#13;
 resto  ipotizzati  nemmeno  nell'esposto-denunzia  in  base al quale ha  &#13;
 indagato il Pretore a quo);                                              &#13;
     che dunque non vi  è  un  "giudizio"  (anche  a  livello  di  atti  &#13;
 preliminari),  nel  quale  sia  in  predicato di applicazione la citata  &#13;
 norma dell'art. 92, sicché la relativa questione  di  legittimità  è  &#13;
 meramente  teorica  ed ipotetica e perciò manifestamente inammissibile  &#13;
 (ex artt. 1 cost. 1/1948 e 23 l.  87/1953).                              &#13;
     Visti gli artt. 26, comma secondo, della legge 11 marzo 1953, n. 87  &#13;
 e 9, comma secondo, delle Norme integrative per i giudizi davanti  alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara   la   manifesta   inammissibilità   della  questione  di  &#13;
 legittimità costituzionale dell'art. 92, comma sesto,  del  d.P.R.  31  &#13;
 maggio   1974,   n.  417,  sollevata  dal  Pretore  di  Campobasso,  in  &#13;
 riferimento agli artt. 3 e 98 Cost., con l'ordinanza in epigrafe.        &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 6 dicembre 1985.        &#13;
                                   F.to:  LIVIO  PALADIN  - ORONZO REALE  &#13;
                                   BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  &#13;
                                   MALAGUGINI  -  ANTONIO  LA  PERGOLA -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE  FERRARI  &#13;
                                   -  FRANCESCO  SAJA - GIOVANNI CONSO -  &#13;
                                   ETTORE  GALLO  -  ALDO  CORASANITI  -  &#13;
                                   GIUSEPPE BORZELLINO - FRANCESCO GRECO  &#13;
                                   - RENATO DELL'ANDRO.                   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
