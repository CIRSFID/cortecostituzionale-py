<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1986</anno_pronuncia>
    <numero_pronuncia>280</numero_pronuncia>
    <ecli>ECLI:IT:COST:1986:280</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>LA PERGOLA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/12/1986</data_decisione>
    <data_deposito>19/12/1986</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ANTONIO LA PERGOLA, presidente - Prof. &#13;
 VIRGILIO ANDRIOLI - Dott. FRANCESCO SAJA - Prof. GIOVANNI CONSO - &#13;
 Prof. ETTORE GALLO - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO &#13;
 - Prof. RENATO DELL'ANDRO - Prof. GABRIELE PESCATORE - Avv. UGO &#13;
 SPAGNOLI - Prof. FRANCESCO PAOLO CASAVOLA - Prof. ANTONIO BALDASSARRE - &#13;
 Prof. VINCENZO CAIANIELLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale  dell'art.  219,  terzo  &#13;
 comma,  del  codice penale, promosso con ordinanza emessa il 5 febbraio  &#13;
 1985 dalla Corte  di  cassazione  sul  ricorso  proposto  da  Saccavino  &#13;
 Giannantonio,  iscritta  al  n.    599  del  registro  ordinanze 1985 e  &#13;
 pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  6,1ª  serie  &#13;
 speciale, dell'anno 1986.                                                &#13;
     Udito  nella  camera  di  consiglio del 12 novembre 1986 il Giudice  &#13;
 relatore Giovanni Conso.                                                 &#13;
     Ritenuto che la Corte di cassazione, con ordinanza del  5  febbraio  &#13;
 1985,  ha  denunciato,  in  riferimento  all'art. 3 della Costituzione,  &#13;
 l'illegittimità dell'art. 219, terzo comma, del codice penale,  "nella  &#13;
 parte  in cui non subordina il provvedimento di ricovero in una casa di  &#13;
 cura e di custodia dell'imputato condannato per delitto non colposo  ad  &#13;
 una  pena  diminuita per infermità psichica al previo accertamento del  &#13;
 Giudice di esecuzione della persistente pericolosità sociale derivante  &#13;
 dall'infermità medesima al tempo della sua esecuzione";                 &#13;
     e che, in particolare, il giudice  a  quo  denuncia  ingiustificata  &#13;
 disparità  di  trattamento: a) anzitutto, rispetto all'art. 204, terzo  &#13;
 comma, del codice  penale,  che,  subordinando  all'accertamento  della  &#13;
 qualità  di  persona  socialmente  pericolosa  l'esecuzione non ancora  &#13;
 iniziata delle misure di  sicurezza  aggiunte  a  pena  non  detentiva,  &#13;
 richiede   l'"effettiva   sussistenza   della   seminfermità  e  della  &#13;
 pericolosità  sociale  al  momento  dell'esecuzione  della  misura  di  &#13;
 sicurezza  e  non soltanto quindi al momento della sua applicazione con  &#13;
 la sentenza di condanna"; b) rispetto agli artt.  204,  secondo  comma,  &#13;
 205,  secondo  comma,  n.  2,  e  222,  primo comma, del codice penale,  &#13;
 dichiarati costituzionalmente illegittimi con sentenza n. 139 del 1982,  &#13;
 nella parte in cui non subordinano  il  provvedimento  di  ricovero  in  &#13;
 ospedale   psichiatrico   giudiziario   dell'imputato   prosciolto  per  &#13;
 infermità psichica al previo accertamento da parte del  giudice  della  &#13;
 cognizione  o  dell'esecuzione  della persistente pericolosità sociale  &#13;
 derivante dall'infermità medesima  al  tempo  dell'applicazione  della  &#13;
 misura  di  sicurezza;  c) rispetto agli artt. 204, secondo comma, 219,  &#13;
 primo e secondo comma, del codice penale, dichiarati costituzionalmente  &#13;
 illegittimi  con  sentenza  n.  249  del  1983,  nella parte in cui non  &#13;
 subordinano il provvedimento di ricovero in  una  casa  di  cura  e  di  &#13;
 custodia  dell'imputato  condannato per delitto non colposo ad una pena  &#13;
 diminuita per cagione di infermità psichica al previo accertamento  da  &#13;
 parte  del  giudice  della  persistente pericolosità sociale derivante  &#13;
 dall'infermità medesima, al tempo dell'applicazione  della  misura  di  &#13;
 sicurezza;                                                               &#13;
     considerato che, dopo la pronuncia dell'ordinanza di rimessione, è  &#13;
 entrata  in  vigore  la  legge  10 ottobre 1986, n. 663 (Modifiche alla  &#13;
 legge sull'ordinamento penitenziario e sulla  esecuzione  delle  misure  &#13;
 privative e limitative della libertà), il cui art. 31 ha espressamente  &#13;
 abrogato  con  il  primo  comma  l'intero  art.  204 del codice penale,  &#13;
 inoltre stabilendo nel secondo comma che "Tutte le misure di  sicurezza  &#13;
 personali  sono  ordinate  previo  accertamento  che  colui il quale ha  &#13;
 commesso il reato è persona socialmente pericolosa";                    &#13;
     che tali innovazioni normative rendono  necessario  restituire  gli  &#13;
 atti  al  giudice  a  quo  perché valuti se, ed eventualmente in quali  &#13;
 termini, la questione sollevata sia ancora rilevante.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina la restituzione degli atti alla Corte di cassazione.          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 16 dicembre 1986.                             &#13;
                                   F.to:  ANTONIO  LA PERGOLA - VIRGILIO  &#13;
                                   ANDRIOLI - FRANCESCO SAJA -  GIOVANNI  &#13;
                                   CONSO   -  ETTORE  GALLO  -  GIUSEPPE  &#13;
                                   BORZELLINO - FRANCESCO GRECO - RENATO  &#13;
                                   DELL'ANDRO - GABRIELE  PESCA  TORE  -  &#13;
                                   UGO   SPAGNOLI   -   FRANCESCO  PAOLO  &#13;
                                   CASAVOLA  -  ANTONIO  BALDASSARRE   -  &#13;
                                   VINCENZO CAIANIELLO.                   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
