import logging
import os

from cortecostituzionale.settings import DATA_DIR


def setup_logger(name="cortecost", log_file_path=None, level=logging.DEBUG):
    log_file_path = log_file_path or os.path.join(DATA_DIR, f"{name}.log")
    log_dir, _ = os.path.split(log_file_path)
    log_dir = log_dir or os.getcwd()
    os.makedirs(log_dir, exist_ok=True)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler = logging.FileHandler(log_file_path)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger
