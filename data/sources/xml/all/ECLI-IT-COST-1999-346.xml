<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>346</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:346</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/07/1999</data_decisione>
    <data_deposito>22/07/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  76,  terzo    &#13;
 comma,  numero 2) del d.P.R. 29 settembre 1973, n. 597 (Istituzione e    &#13;
 disciplina dell'imposta sul reddito delle persone fisiche),  promosso    &#13;
 con  ordinanza  emessa  il  26  febbraio  e  il  26  marzo 1998 dalla    &#13;
 Commissione tributaria regionale  per  l'Emilia-Romagna  sul  ricorso    &#13;
 proposto da Vigilante Domenico contro l'Ufficio delle imposte dirette    &#13;
 di  Bologna,  iscritta  al  n.  697  del  registro  ordinanze  1998 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  40,  prima    &#13;
 serie speciale, dell'anno 1998.                                          &#13;
   Udito  nella  camera  di  consiglio  del  12 maggio 1999 il giudice    &#13;
 relatore Annibale Marini.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel corso di un giudizio d'appello avente ad oggetto una istanza di    &#13;
 rimborso parziale dell'imposta  sul  reddito  delle  persone  fisiche    &#13;
 (IRPEF),  la  Commissione  tributaria regionale per l'Emilia-Romagna,    &#13;
 con ordinanza emessa il 26 febbraio  e  il  26  marzo  del  1998,  ha    &#13;
 sollevato,  in  riferimento  agli articoli 3 e 53 della Costituzione,    &#13;
 questione di legittimità costituzionale dell'art. 76,  terzo  comma,    &#13;
 numero  2,  del decreto del Presidente della Repubblica  29 settembre    &#13;
 1973, n. 597 (Istituzione e disciplina dell'imposta sul reddito delle    &#13;
 persone fisiche).                                                        &#13;
   Secondo  il  giudice  a  quo,  tale  norma  -   disponendo,   senza    &#13;
 possibilità  di  prova contraria, che "la plusvalenza ricavata dalla    &#13;
 rivendita  di  un  appartamento,  non  utilizzato  direttamente   dal    &#13;
 proprietario,  prima  che  siano trascorsi cinque anni dall'acquisto,    &#13;
 costituisca reddito derivante da operazione  speculativa  e  pertanto    &#13;
 tassabile ai fini IRPEF" - fisserebbe, in violazione del principio di    &#13;
 ragionevolezza  di  cui  all'art.  3  della Costituzione, quale unico    &#13;
 criterio di individuazione di una attività speculativa il decorso di    &#13;
 un determinato periodo di tempo (cinque anni), prescindendo  da  ogni    &#13;
 riferimento alla effettiva realtà socio-economica.                      &#13;
   Sotto  un  diverso  aspetto, l'irragionevolezza della norma sarebbe    &#13;
 comprovata  sia  dalla  possibilità   di   eluderne   l'applicazione    &#13;
 "formalizzando    l'atto    di    vendita   trascorsi   cinque   anni    &#13;
 dall'acquisto",  sia  dalla  sua  incidenza  su   quei   contribuenti    &#13;
 costretti  a  rivendere  l'immobile che non avevano potuto utilizzare    &#13;
 direttamente per motivi indipendenti dalla loro volontà.                &#13;
   La    plusvalenza    realizzata   per   effetto   della   rivendita    &#13;
 infraquinquennale dell'immobile sarebbe, poi, in mancanza  di  idonei    &#13;
 correttivi  della  svalutazione monetaria, puramente fittizia e tale,    &#13;
 dunque, da non poter essere assunta a base  del  calcolo  progressivo    &#13;
 dell'IRPEF  senza  violare  la  lettera e la ratio dell'art. 53 della    &#13;
 Costituzione.<diritto>Considerato in diritto</diritto>1. - La Commissione tributaria regionale per  l'Emilia Romagna, con    &#13;
 l'ordinanza  indicata  in epigrafe, solleva questione di legittimità    &#13;
 costituzionale dell'art. 76, terzo comma, numero  2,  del  d.P.R.  29    &#13;
 settembre  1973,  n.  597 a tenore del quale si considerano fatti con    &#13;
 fini speculativi  l'acquisto  e  la  vendita  di  beni  immobili  non    &#13;
 destinati  all'utilizzazione personale da parte dell'acquirente o dei    &#13;
 familiari se il tempo intercorrente tra l'acquisto e la  vendita  non    &#13;
 è  superiore  ai  cinque  anni, senza possibilità alcuna di provare    &#13;
 l'assenza dei fini stessi anche nell'ipotesi, come quella oggetto del    &#13;
 giudizio a quo, in cui la mancata utilizzazione personale sia  dovuta    &#13;
 a fatto del terzo o a forza maggiore.                                    &#13;
   Ed è, appunto, sotto tale aspetto che la norma risulterebbe lesiva    &#13;
 del principio di ragionevolezza di cui all'art. 3 della Costituzione.    &#13;
   Secondo  il  giudice  a  quo,  poi,  la stessa norma, assoggettando    &#13;
 all'IRPEF l'intera plusvalenza  monetaria  realizzata  dalla  vendita    &#13;
 dell'immobile  e,  quindi,  anche quella derivante dalla svalutazione    &#13;
 monetaria eventualmente verificatasi nel quinquennio,  violerebbe  il    &#13;
 principio  di  effettività  della  capacità  contributiva garantito    &#13;
 dall'art. 53 della Costituzione.                                         &#13;
   2. - La questione non è fondata.                                      &#13;
   2.1. - Il  rimettente,  pur  ricordando  come  questa  Corte  abbia    &#13;
 reiteratamente  disatteso la censura di illegittimità costituzionale    &#13;
 dell'art. 76, terzo comma, numero 2, del decreto del Presidente della    &#13;
 Repubblica 29 settembre  1973,  n.  597,  ritiene  che  l'intervenuto    &#13;
 mutamento  della realtà socio-economica di riferimento abbia reso la    &#13;
 norma denunciata priva di ragionevole  giustificazione  e,  pertanto,    &#13;
 lesiva dell'art.  3 della Costituzione.                                  &#13;
   In proposito è possibile osservare come sia la stessa premessa del    &#13;
 dubbio   di   costituzionalità   sollevato  a  risultare  del  tutto    &#13;
 indimostrata.                                                            &#13;
   I fattori evolutivi della società,  evocati  dal  giudice  a  quo,    &#13;
 quali  "la  composizione  dei  nuclei  familiari, il loro reddito, la    &#13;
 crescita o il  calo  demografico,  i  movimenti  migratori,  i  tempi    &#13;
 tecnici  e  burocratici  necessari  per costruire nuove abitazioni, i    &#13;
 tassi di interesse dei mutui, la normativa sulle locazioni",  pur  se    &#13;
 suscettibili  di influenzare in qualche misura il mercato immobiliare    &#13;
 e le motivazioni stesse dell'acquisto di beni immobili,    non  sono,    &#13;
 infatti,  tali  da rendere irragionevole la scelta del legislatore di    &#13;
 considerare speculativa, sulla base  dell'id quod plerumque  accidit,    &#13;
 l'operazione economica di chi acquista un immobile non destinato alla    &#13;
 utilizzazione  personale  sua  o  dei familiari e lo rivende entro il    &#13;
 successivo quinquennio.                                                  &#13;
   Se,  dunque,   sotto   l'aspetto   considerato,   la   censura   di    &#13;
 incostituzionalità della norma risulta priva di fondamento, identica    &#13;
 è  la conclusione cui si perviene qualora si consideri l'ulteriore e    &#13;
 diverso assunto del giudice   a  quo  circa  una  ipotetica,  estrema    &#13;
 facilità di elusione dell'imposta in conseguenza e per effetto della    &#13;
 possibilità   di   stipulazione  della  rivendita  infraquinquennale    &#13;
 dell'immobile in una forma che non sarebbe conoscibile dal fisco.        &#13;
   Indipendentemente  da  ogni  altra  considerazione  relativa   alla    &#13;
 stabilità  dell'acquisto  del bene in tal modo effettuato, l'ipotesi    &#13;
 nella specie prospettata deve propriamente  ricondursi  all'area  non    &#13;
 già  della  elusione,  ma  della  evasione  dell'imposta e non può,    &#13;
 dunque, comprovare l'asserita inadeguatezza della norma denunciata.      &#13;
   2.2.  -  Privo  di  consistenza  risulta,  infine,  il  dubbio   di    &#13;
 violazione  dell'art.  53  della  Costituzione  fondato sull'assenza,    &#13;
 nella tassazione delle plusvalenze, di correttivi della  svalutazione    &#13;
 monetaria idonei ad evitare di porre a base del calcolo dell'IRPEF un    &#13;
 reddito  imponibile  dovuto  al  fenomeno inflattivo e perciò stesso    &#13;
 fittizio.                                                                &#13;
   L'apprezzamento  della  incidenza  nel   campo   tributario   della    &#13;
 svalutazione monetaria è rimesso, infatti, come la giurisprudenza di    &#13;
 questa  Corte  ha  avuto modo di affermare, alla discrezionalità del    &#13;
 legislatore.                                                             &#13;
   Mentre il  principio  di  capacità  contributiva  non  può  certo    &#13;
 ritenersi  violato  per il solo fatto che una fluttuazione del valore    &#13;
 della moneta abbia accresciuto l'incidenza fiscale di  un  tributo  o    &#13;
 abbia modificato gli effetti dell'applicazione di una imposta.           &#13;
   È, dunque, evidente come la menzionata giurisprudenza, dalla quale    &#13;
 non  v'è  ragione  alcuna  di  discostarsi,  porti ad escludere quel    &#13;
 rilievo  costituzionale  della  svalutazione  monetaria  che,   nella    &#13;
 prospettazione del rimettente, rappresenta l'esclusivo supporto della    &#13;
 censura dallo stesso avanzata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 76, terzo comma, numero 2, del decreto del Presidente della    &#13;
 Repubblica 29  settembre  1973,  n.  597  (Istituzione  e  disciplina    &#13;
 dell'imposta  sul  reddito  delle  persone  fisiche),  sollevata,  in    &#13;
 riferimento agli artt. 3 e 53 della Costituzione,  dalla  Commissione    &#13;
 tributaria   regionale   per   l'Emilia-Romagna  con  l'ordinanza  in    &#13;
 epigrafe.                                                                &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 14 luglio 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Marini                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 luglio 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
