<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1965</anno_pronuncia>
    <numero_pronuncia>88</numero_pronuncia>
    <ecli>ECLI:IT:COST:1965:88</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1965</data_decisione>
    <data_deposito>22/12/1965</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI &#13;
 - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale della legge 9 febbraio  &#13;
 1963, n. 97, promosso con  ordinanza  emessa  il  6  ottobre  1964  dal  &#13;
 Pretore  di  Roma  nel  procedimento penale a carico di Zane J. Sandom,  &#13;
 iscritta al n. 11  del  Registro  ordinanze  1965  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica, n. 78 del 27 marzo 1965.            &#13;
     Udita  nella  camera  di consiglio del 28 ottobre 1965 la relazione  &#13;
 del Giudice Giovanni Cassandro.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un procedimento penale a carico del  signor  Zane  &#13;
 J. Sandom davanti al Pretore di Roma, è stata sollevata, nei confronti  &#13;
 dell'art.   39   della   Costituzione,  la  questione  di  legittimità  &#13;
 costituzionale  della  legge  9  febbraio  1963,  n.   97,   intitolata  &#13;
 "Estensione  dei contratti collettivi di lavoro del settore del credito  &#13;
 registrati in applicazione della legge 14  luglio  1959,  n.  741",  la  &#13;
 quale  nel  suo  articolo  unico  stabilisce  che  "le disposizioni dei  &#13;
 decreti del Presidente della Repubblica 2 gennaio 1962, nn.  479,  501,  &#13;
 564,  668  e  934,  emanate  in attuazione della delega contenuta nella  &#13;
 legge 14 luglio 1959, n. 741, prorogata  dall'art.  2  della  legge  10  &#13;
 ottobre  1960, n. 1027, e contenente minimi inderogabili di trattamento  &#13;
 economico e normativo si applicano nei confronti  dei  lavoratori  alle  &#13;
 dipendenze  di  aziende  di  credito, anche se esse abbiano meno di 100  &#13;
 dipendenti".                                                             &#13;
     Il Pretore ha ritenuto la questione rilevante e non  manifestamente  &#13;
 infondata e in conseguenza ha sospeso il giudizio e rinviato gli atti a  &#13;
 questa Corte.                                                            &#13;
     L'ordinanza,  dopo  le  notificazioni  e  comunicazioni di rito, è  &#13;
 stata pubblicata nella Gazzetta Ufficiale, n. 78 del 27 marzo 1965.      &#13;
     2. - I motivi per i quali il Pretore ha  considerato  la  questione  &#13;
 non  manifestamente  infondata,  sono da ricercare, a suo avviso, nella  &#13;
 violazione dei principi di libertà sindacale e di autonomia collettiva  &#13;
 professionale sanciti dall'art. 39 della Costituzione. Il sistema posto  &#13;
 dalla legge 14 luglio 1959, n. 741, ha potuto  trovare  giustificazione  &#13;
 per   la   carenza   legislativa  nella  materia  della  contrattazione  &#13;
 collettiva e per la tutela dell'interesse  pubblico  della  parità  di  &#13;
 trattamento dei lavoratori e dei datori di lavoro, e come eccezionale e  &#13;
 provvisorio,  sicché  ogni  altra  proroga,  oltre quella disposta con  &#13;
 legge 1 ottobre 1960,  n.  1027,  sarebbe  in  contrasto  col  precetto  &#13;
 costituzionale.                                                          &#13;
     Ora  la  legge  impugnata  è  appunto  nella  sostanza una proroga  &#13;
 ulteriore  e  perciò  costituzionalmente   illegittima   e,   inoltre,  &#13;
 estendendo coattivamente un contratto collettivo a categorie diverse di  &#13;
 datori  di  lavoro  (nel  caso, le piccole aziende di credito), avrebbe  &#13;
 ulteriormente violato il principio dell'autonomia sindacale.             &#13;
     3. - Le parti non si sono costituite.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con  la  sentenza  n.  106  dell'11  dicembre  1962  la  Corte  &#13;
 costituzionale,  pur negando che l'art. 39 della Costituzione contenga,  &#13;
 nel quarto comma, una riserva normativa o contrattuale  in  favore  dei  &#13;
 sindacati,  per  il regolamento dei rapporti di lavoro, segnatamente se  &#13;
 intesa  nel  senso  di  precludere  al  legislatore  ordinario  ogni  e  &#13;
 qualsiasi intervento in questa materia, affermò tuttavia che, soltanto  &#13;
 mediante  il  procedimento fissato dal ricordato art. 39, quarto comma,  &#13;
 era possibile estendere  l'efficacia  della  contrattazione  collettiva  &#13;
 erga  omnes,  anche,  cioè,  nei  confronti dei datori di lavoro e dei  &#13;
 lavoratori appartenenti alla medesima categoria,  ma  rimasti  estranei  &#13;
 alla  contrattazione.  L'infondatezza  della  questione di legittimità  &#13;
 costituzionale della legge 14  luglio  1959,  n.  741,  "recante  norme  &#13;
 transitorie  per  garantire minimi di trattamento economico e normativo  &#13;
 ai lavoratori", fu motivata dalla carenza  delle  norme  di  attuazione  &#13;
 dell'art.  39  della  Costituzione;  e  la  legge stessa fu considerata  &#13;
 transitoria,  provvisoria  ed  eccezionale,  rivolta  a  regolare   una  &#13;
 situazione  passata  e a tutelare l'interesse pubblico della parità di  &#13;
 trattamento  dei  lavoratori  e  dei  datori  di  lavoro.   E   codesta  &#13;
 particolare  natura  della  legge  fu confermata dalla dichiarazione di  &#13;
 illegittimità costituzionale, affermata contestualmente, della legge 1  &#13;
 ottobre 1960, n. 1027, che, conferendo al Governo il potere di  emanare  &#13;
 norme  uniformi  alle  clausole degli accordi economici e dei contratti  &#13;
 collettivi stipulati entro  i  dieci  mesi  successivi  all'entrata  in  &#13;
 vigore  della  legge  di delegazione, toglieva a questa il carattere di  &#13;
 transitorietà e di eccezionalità, che giustificava  la  dichiarazione  &#13;
 di infondatezza della relativa questione di costituzionalità.           &#13;
     2.  -  La  Corte  ritiene  che dai sopra richiamati motivi discenda  &#13;
 l'illegittimità della legge impugnata.  Essa,  infatti,  estendendo  i  &#13;
 contratti  di  lavoro  del  settore  del  credito  già  registrati  in  &#13;
 applicazione  della  legge  14  luglio  1959,  n.  741,  ai  lavoratori  &#13;
 dipendenti  da  aziende  di  credito  con  meno  di 100 dipendenti, non  &#13;
 soltanto non trova giustificazione  nella  necessità  di  estendere  i  &#13;
 minimi  di  trattamento  economico  e normativo agli appartenenti a una  &#13;
 medesima  categoria,  assicurando  la  parità   di   trattamento   dei  &#13;
 lavoratori  e  dei  datori  di  lavoro  che  si  trovano  in parità di  &#13;
 condizioni; non soltanto, emanata com'è a 4  anni  di  distanza  dalla  &#13;
 legge  14  luglio  1959,  n.  741,  non  può  essere  qualificata come  &#13;
 eccezionale e transitoria; non soltanto rappresenta un  intervento  del  &#13;
 legislatore  non  già  a  tutela di interessi generali, e dei precetti  &#13;
 costituzionali in materia di lavoro, dei quali esso è il destinatario;  &#13;
 ma, estendendo l'efficacia dei contratti di lavoro  stipulati  tra  una  &#13;
 certa  categoria  di aziende e i dipendenti di queste (aziende con più  &#13;
 di 100 dipendenti) ad un'altra  categoria  di  aziende  e  ai  relativi  &#13;
 dipendenti (aziende con meno di 100 dipendenti), ha violato la libertà  &#13;
 di  organizzazione  e di inquadramento che l'ordinamento costituzionale  &#13;
 non consente sia  limitata  o  annullata  dall'intervento  autoritativo  &#13;
 della   legge,   ma   considera  parte  essenziale  della  libertà  di  &#13;
 associazione sindacale (cfr. la citata sentenza n. 106 del 1962).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale  della  legge  9  febbraio  &#13;
 1963, n. 97, in riferimento all'art. 39 della Costituzione.              &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 dicembre 1965.       &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO - ANTONINO  PAPALDO  -  NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO - ANTONIO  &#13;
                                   MANCA  -  ALDO  SANDULLI  -  GIUSEPPE  &#13;
                                   BRANCA - MICHELE FRAGALI - COSTANTINO  &#13;
                                   MORTATI  -   GIUSEPPE   CHIARELLI   -  &#13;
                                   GIUSEPPE   VERZÌ  - GIOVANNI BATTISTA  &#13;
                                   BENEDETTI    -    FRANCESCO     PAOLO  &#13;
                                   BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
