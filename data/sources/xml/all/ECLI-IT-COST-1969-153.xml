<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>153</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:153</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Vincenzo Trimarchi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/12/1969</data_decisione>
    <data_deposito>17/12/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'articolo unico del  &#13;
 D.P.R. 2 gennaio 1962, n. 912, nella parte in  cui  rende  obbligatorio  &#13;
 erga omnes l'art. 36, comma nono, del contratto collettivo nazionale 28  &#13;
 febbraio  1941  per i dipendenti delle Casse di risparmio, dei Monti di  &#13;
 credito su pegno di prima categoria e degli enti equiparati, nel  testo  &#13;
 modificato  dall'art.  14 della Convenzione collettiva 14 ottobre 1953,  &#13;
 promosso con ordinanza emessa il 23 maggio 1968 dal pretore di  Mantova  &#13;
 nel  procedimento penale a carico di Mirandola Domenico, iscritta al n.  &#13;
 174 del Registro ordinanze 1968 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n. 248 del 28 settembre 1968.                           &#13;
    Udito  nella  camera  di  consiglio  del 10 dicembre 1969 il Giudice  &#13;
 relatore Vincenzo Michele Trimarchi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
    Con verbale n. 574 del 3 aprile 1968  l'Ispettorato  del  lavoro  di  &#13;
 Mantova   ha   elevato  contravvenzione  a  carico  dell'avv.  Domenico  &#13;
 Mirandola, nella sua qualità di Presidente della Cassa di risparmio di  &#13;
 Verona, Vicenza e Belluno "per avere fatto eseguire a n. 12  dipendenti  &#13;
 occupati  nella  sede  di Mantova, in periodi e per lavori non inerenti  &#13;
 alle operazioni di chiusura dei conti,  ore  di  lavoro  straordinario,  &#13;
 senza   darne   comunicazione   né   preventiva  né  successiva  alla  &#13;
 rappresentanza del personale"; e ciò in violazione  dell'art.  36  del  &#13;
 contratto  collettivo  nazionale  di  lavoro  28  febbraio  1941 per il  &#13;
 personale dipendente da Casse di risparmio, Monti di credito  su  pegno  &#13;
 di  prima  categoria  ed Enti equiparati, modificato dall'art. 14 della  &#13;
 Convenzione collettiva 14 ottobre 1953, resa  obbligatoria  erga  omnes  &#13;
 dall'articolo  unico  del  D.P.R.  2  gennaio  1962,  n.  912, e con la  &#13;
 conseguente applicabilità delle sanzioni previste  dall'art.  8  della  &#13;
 legge 14 luglio 1959, n. 741.                                            &#13;
    Il  pretore  di  Mantova,  iniziato  il  procedimento  penale contro  &#13;
 l'imputato, ha rilevato in particolare che era stata violata  la  norma  &#13;
 di  cui  al  nono  comma  dell'art.  36 del citato contratto collettivo  &#13;
 nazionale di lavoro, nel  testo  modificato  dalla  citata  Convenzione  &#13;
 collettiva,  e  secondo  la  quale  "per  il  lavoro  straordinario  di  &#13;
 qualsiasi natura, escluso quello inerente alle chiusure periodiche  dei  &#13;
 conti,  deve  essere  fatta dagli Istituti preventiva segnalazione alla  &#13;
 rappresentanza del personale".                                           &#13;
    Ma nel contempo ha ritenuto rilevante e non manifestamente infondata  &#13;
 la questione di legittimità  costituzionale  dell'articolo  unico  del  &#13;
 D.P.R.  n.  912  del  1962 nella parte in cui ha reso obbligatorio erga  &#13;
 omnes il surriportato comma, per le ragioni poste a base della sentenza  &#13;
 n. 26 del 1967 della Corte costituzionale  con  cui  era  stata  decisa  &#13;
 altra fattispecie sostanzialmente identica.                              &#13;
    E  con  ordinanza  del  23  maggio  1968  ha  sollevato d'ufficio la  &#13;
 questione.                                                               &#13;
    L'ordinanza è stata notificata all'imputato il 30 maggio  1968,  al  &#13;
 pubblico  ministero il 28 giugno 1968 e al Presidente del Consiglio dei  &#13;
 Ministri il 29 maggio 1968; è stata comunicata ai Presidenti delle due  &#13;
 Camere il  27  maggio  1968  ed  è  stata  pubblicata  nella  Gazzetta  &#13;
 Ufficiale n. 248 del 28 settembre 1968.                                  &#13;
    Davanti  a questa Corte non si è costituita alcuna delle parti, né  &#13;
 ha spiegato intervento il Presidente del Consiglio dei Ministri.  E  la  &#13;
 causa è stata trattata in camera di consiglio il 10 dicembre 1969.<diritto>Considerato in diritto</diritto>:                          &#13;
    1.  - Nell'ordinanza di rimessione del 23 maggio 1968 del pretore di  &#13;
 Mantova  non  sono  espressamente  indicate   le   disposizioni   della  &#13;
 Costituzione  che  si  ritengono  violate,  ma  dalla  motivazione  per  &#13;
 relationem può dedursi con sicurezza che la questione di  legittimità  &#13;
 costituzionale  dell'articolo  unico del D.P.R. 2 gennaio 1962, n. 912,  &#13;
 nei termini sopra precisati, è sollevata in riferimento agli artt.  76  &#13;
 e 77 della Costituzione.                                                 &#13;
    2.  -  La  questione  è  sostanzialmente  identica  a  quella  già  &#13;
 sottoposta all'esame di questa Corte, con ordinanza del 10 marzo  1965,  &#13;
 dalla  Corte  suprema  di  cassazione e decisa con sentenza n. 26 del 9  &#13;
 marzo 1967. In quella occasione, in relazione agli artt. 1  e  8  della  &#13;
 legge  14  luglio  1959,  n.  741, ed in riferimento agli artt. 76 e 77  &#13;
 della Costituzione, è stata dichiarata l'illegittimità costituzionale  &#13;
 dell'articolo unico del D.P.R. 2 gennaio 1962, n. 912, nella  parte  in  &#13;
 cui  rendeva  obbligatorio  erga  omnes  il comma nono dell'art. 41 del  &#13;
 contratto collettivo  nazionale  per  i  dipendenti  delle  aziende  di  &#13;
 credito  1  agosto  1955; e quest'ultima norma disponeva che "il lavoro  &#13;
 straordinario  di  qualsiasi  natura,  escluso  quello  inerente   alle  &#13;
 chiusure  periodiche  dei  conti,  dovrà  essere previamente segnalato  &#13;
 dalle aziende alle organizzazioni  sindacali  dei  lavoratori".  E  con  &#13;
 l'ordinanza  di rimessione di cui si tratta è denunciata, in relazione  &#13;
 ed in riferimento alle stesse norme  e  disposizioni,  l'illegittimità  &#13;
 costituzionale  dell'articolo  unico del D.P.R. 2 gennaio 1962, n. 912,  &#13;
 nella parte  in  cui  rende  obbligatorio  erga  omnes  il  comma  nono  &#13;
 dell'art.  36 del contratto collettivo nazionale 28 febbraio 1941 per i  &#13;
 dipendenti delle Casse di risparmio, dei Monti di credito su  pegno  di  &#13;
 prima  categoria  e degli enti equiparati, il quale dispone che "per il  &#13;
 lavoro straordinario di qualsiasi natura, escluso quello inerente  alle  &#13;
 chiusure  periodiche  dei  conti,  deve  essere  fatta  dagli  Istituti  &#13;
 preventiva segnalazione alla rappresentanza del personale".              &#13;
    La differenza riscontrabile tra i due testi, e cioè che  il  lavoro  &#13;
 straordinario  deve  essere  segnalato  in un caso "alle organizzazioni  &#13;
 sindacali  dei  lavoratori"  e  nell'altro  "alla  rappresentanza   del  &#13;
 personale", non incide sulla identità della questione perché la norma  &#13;
 nei due casi è posta per la eguale tutela dello stesso interesse.       &#13;
    Ricorrono   quindi   le   condizioni  perché  in  conformità  alla  &#13;
 precedente sentenza, analoga  pronuncia  di  incostituzionalità  debba  &#13;
 emettersi nei confronti della norma denunciata dal pretore di Mantova.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
    dichiara  l'illegittimità  costituzionale  dell'articolo  unico del  &#13;
 D.P.R. 2 gennaio 1962, n. 912, nella parte in  cui  rende  obbligatorio  &#13;
 erga omnes l'art. 36, comma nono, del contratto collettivo nazionale 28  &#13;
 febbraio  1941  per i dipendenti delle Casse di risparmio, dei Monti di  &#13;
 credito su pegno di prima categoria e degli enti equiparati, nel  testo  &#13;
 modificato dall'art. 14 della Convenzione collettiva 14 ottobre 1953.    &#13;
    Così deciso in Roma, in camera di consiglio, nella sede della Corte  &#13;
 costituzionale, Palazzo della Consulta, il 14 dicembre 1969.             &#13;
    GIUSEPPE  BRANCA  -  MICHELE FRAGALI - COSTANTINO MORTATI - GIUSEPPE  &#13;
 CHIARELLI GIUSEPPE  VERZÌ - GIOVANNI  BATTISTA  BENEDETTI  -  FRANCESCO  &#13;
 PAOLO  BONIFACIO  -  LUIGI OGGIONI - ANGELO DE MARCO - ENZO CAPALOZZA -  &#13;
 VINCENZO MICHELE TRIMARCHI - VEZIO CRISAFULLI - NICOLA  REALE  -  PAOLO  &#13;
 ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
