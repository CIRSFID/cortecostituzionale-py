<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>68</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:68</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/03/1996</data_decisione>
    <data_deposito>08/03/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi di legittimità costituzionale degli artt. 3, comma 1, e    &#13;
 6, comma 2, del decreto-legge 16 novembre  1994,  n.  629  (Modifiche    &#13;
 alla  disciplina  degli  scarichi  delle  pubbliche fognature e degli    &#13;
 insediamenti civili che non  recapitano  in  pubbliche  fognature)  e    &#13;
 dell'art. 3 e dell'intero testo del decreto-legge 16 gennaio 1995, n.    &#13;
 9 (Modifiche alla disciplina degli scarichi delle pubbliche fognature    &#13;
 e   degli   insediamenti  civili  che  non  recapitano  in  pubbliche    &#13;
 fognature), promossi con ordinanze emesse  il  2  dicembre  1994  dal    &#13;
 pretore  di  Brindisi;  il 20 marzo 1995 dal pretore di Roma, sezione    &#13;
 distaccata  di  Castelnuovo di Porto; il 2 marzo 1995, il 23 febbraio    &#13;
 1995, il 30 gennaio 1995, il 3 marzo 1995, il 9 febbraio 1995  (n.  2    &#13;
 ordinanze),  il  16 febbraio 1995 e il 9 febbraio 1995 dal pretore di    &#13;
 Ferrara; il 24 febbraio 1995 dal pretore di Roma, sezione  distaccata    &#13;
 di  Castelnuovo di Porto; il 3 marzo 1995 e il 10 febbraio 1995 (n. 2    &#13;
 ordinanze) dal pretore di Ferrara; il 2 dicembre 1994 dal pretore  di    &#13;
 Brindisi;  il 20 marzo 1995 e il 20 gennaio 1995 (n. 4 ordinanze) dal    &#13;
 pretore di Roma, sezione distaccata di Castelnuovo di  Porto;  il  16    &#13;
 febbraio  1995  dal  pretore di Ferrara, sezione distaccata di Cento;    &#13;
 iscritte rispettivamente ai nn. 585, 617, 635, 644,  645,  646,  647,    &#13;
 648,  673, 674, 694, 726, 727, 734, 747, 748, 749, 750, 751, 752, 838    &#13;
 del registro ordinanze 1995 e  pubblicate  nella  Gazzetta  Ufficiale    &#13;
 della  Repubblica, prima serie speciale, nn. 41, 42, 43, 44, 46, 47 e    &#13;
 50, dell'anno 1995;                                                      &#13;
   Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del 7 febbraio 1996 il giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
   Ritenuto che con due ordinanze di identico contenuto  emesse  il  2    &#13;
 dicembre 1994 (r.o. nn. 585 e 747 del 1995), nel corso di altrettanti    &#13;
 procedimenti  penali  promossi  nei confronti di persone imputate per    &#13;
 aver effettuato scarichi non autorizzati  o  eccedenti  i  limiti  di    &#13;
 accettabilità,  il  pretore di Brindisi ha sollevato, in riferimento    &#13;
 agli artt. 3, 10, 11,  32  e  77  della  Costituzione,  questioni  di    &#13;
 legittimità  costituzionale  del  decreto-legge 16 novembre 1994, n.    &#13;
 629  (Modifiche  alla  disciplina  degli  scarichi  delle   pubbliche    &#13;
 fognature e degli insediamenti civili che non recapitano in pubbliche    &#13;
 fognature),  denunciando  l'art.  3,  comma 1 (che modifica il regime    &#13;
 delle sanzioni stabilite dal terzo comma dell'art. 21 della legge  10    &#13;
 maggio   1976,  n.  319  per  gli  scarichi  eccedenti  i  limiti  di    &#13;
 accettabilità),  e  l'art.  6,  comma  2   (che   prevede   sanzioni    &#13;
 amministrative  pecuniarie  per  chi  effettua  senza  autorizzazione    &#13;
 scarichi civili e di pubbliche fognature nelle acque, sul suolo o nel    &#13;
 sottosuolo);                                                             &#13;
     che, con riferimento all'art. 77 della Costituzione,  il  pretore    &#13;
 di  Brindisi  ritiene che il decreto-legge, reiterato con il medesimo    &#13;
 contenuto,  manchi  del  requisito  della  necessità   ed   urgenza,    &#13;
 presupposto  necessario  per  l'adozione da parte del Governo di atti    &#13;
 aventi forza di legge;                                                   &#13;
     che le disposizioni denunciate sarebbero in contrasto  anche  con    &#13;
 il   principio  di  eguaglianza  (art.  3  della  Costituzione),  per    &#13;
 l'ingiustificata  disparità   di   trattamento   determinata   dalla    &#13;
 depenalizzazione  delle  contravvenzioni concernenti gli scarichi non    &#13;
 autorizzati o eccedenti i limiti  di  tollerabilità  provenienti  da    &#13;
 insediamenti   civili,   mentre   costituiscono  reato  gli  scarichi    &#13;
 egualmente non autorizzati o eccedenti  i  limiti  di  tollerabilità    &#13;
 provenienti  da insediamenti produttivi.  Inoltre sarebbe irrazionale    &#13;
 la previsione di sanzioni più lievi  per  condotte  che  causano  un    &#13;
 maggiore  danno  all'ambiente,  rispetto  alle sanzioni stabilite per    &#13;
 ipotesi di minore gravità;                                              &#13;
     che, secondo il pretore rimettente, sarebbero violati il  diritto    &#13;
 alla  salute  (art. 32, della Costituzione), configurato come diritto    &#13;
 all'ambiente salubre, e gli artt. 10 e 11 della Costituzione, perché    &#13;
 la disciplina adottata  contrasterebbe  con  i  vincoli  posti  dalla    &#13;
 normativa comunitaria, in particolare con la direttiva 91/271/CEE del    &#13;
 21 maggio 1991;                                                          &#13;
     che  anche  nei confronti del successivo decreto-legge 16 gennaio    &#13;
 1995, n. 9 (Modifiche alla disciplina degli scarichi delle  pubbliche    &#13;
 fognature e degli insediamenti civili che non recapitano in pubbliche    &#13;
 fognature),  che  ha  disciplinato  la  medesima  materia, sono state    &#13;
 sollevate  questioni   di   legittimità   costituzionale,   che   lo    &#13;
 coinvolgono  nella  sua interezza o si riferiscono al solo art. 3. Le    &#13;
 questioni sono state sollevate nel corso di altrettanti  procedimenti    &#13;
 penali,  indicando  diversi  parametri  costituzionali, con ordinanze    &#13;
 emesse dai pretori di Ferrara, il 30 gennaio (r.o. 645 del 1995),  il    &#13;
 9  febbraio (r.o.  nn. 647, 648 e 674 del 1995), il 10 febbraio (r.o.    &#13;
 nn. 727 e 734 del 1995), il 16 febbraio (r.o. n. 673 del 1995), il 23    &#13;
 febbraio (r.o. n. 644 del 1995), il 2 marzo (r.o. n. 635 del 1995) ed    &#13;
 il 3 marzo 1995 (r.o. nn.  646  e  726  del  1995);  dal  pretore  di    &#13;
 Ferrara,  sezione  distaccata  di  Cento,  con ordinanza emessa il 16    &#13;
 febbraio 1995 (r.o. n. 838 del 1995); dal pretore  di  Roma,  sezione    &#13;
 distaccata  di  Castelnuovo  di  Porto,  con  ordinanze  emesse il 20    &#13;
 gennaio (r.o.  nn. 749, 750, 751 e 752  del  1995),  il  24  febbraio    &#13;
 (r.o.  n.  694  del 1995) ed il 20 marzo 1995 (r.o. nn. 617 e 748 del    &#13;
 1995);                                                                   &#13;
     che tutte le ordinanze considerano l'art. 3, il quale modifica la    &#13;
 disciplina delle sanzioni, originariamente stabilita dal terzo  comma    &#13;
 dell'art.  21  della  legge n. 319 del 1976, per gli scarichi oltre i    &#13;
 limiti di accettabilità, o l'intero decreto-legge n. 9 del  1995  in    &#13;
 contrasto  con  gli  artt.  25  e  77  della Costituzione, mancando i    &#13;
 presupposti di necessità ed urgenza che  legittimano  l'adozione  da    &#13;
 parte  del Governo di atti con forza di legge. Inoltre la successione    &#13;
 di  decreti-legge  non  convertiti,  con  un   contenuto   in   parte    &#13;
 differente,  determinerebbe  un  trattamento  diverso  di fattispecie    &#13;
 identiche, con violazione dei principi  di  riserva  di  legge  e  di    &#13;
 certezza  del  diritto  in materia penale, perché le stesse condotte    &#13;
 sarebbero  valutate  diversamente  a  seconda  che  il  giudizio  sia    &#13;
 adottato sotto la vigenza di uno o dell'altro decreto-legge;             &#13;
     che,  in  particolare,  l'art.  3 del decreto-legge n. 9 del 1995    &#13;
 determinerebbe, in violazione  dell'art.  3  della  Costituzione,  un    &#13;
 trattamento  differente  di fattispecie identiche, se giudicate sotto    &#13;
 la vigenza di diversi decreti-legge, ed  una  irrazionale  disparità    &#13;
 nel  regime  delle  sanzioni,  essendo  punite  con  minore severità    &#13;
 condotte che  causano  un  maggiore  danno  all'ambiente  rispetto  a    &#13;
 violazioni  meno  gravi.  Viene inoltre prospettata la violazione del    &#13;
 diritto alla salute (art. 32, della Costituzione),  configurato  come    &#13;
 diritto  all'ambiente  salubre;  dell'art.  9,  secondo  comma, della    &#13;
 Costituzione, per  la  mancata  tutela  del  paesaggio,  inteso  come    &#13;
 ambiente  naturale; dell'art. 10, della Costituzione o degli artt. 10    &#13;
 e 11 della Costituzione, perché la disciplina  adottata  sarebbe  in    &#13;
 contrasto  con  i  vincoli  posti  dalla  normativa  comunitaria,  in    &#13;
 particolare  con  la  direttiva  91/271/CEE;  dell'art.   41,   della    &#13;
 Costituzionale,   giacché   si   determinerebbe  una  situazione  di    &#13;
 svantaggio per le imprese servite da scarichi che non  recapitano  in    &#13;
 pubbliche  fognature,  le  quali hanno dovuto effettuare investimenti    &#13;
 per adeguare i propri impianti alle norme  di  tutela  ambientale,  a    &#13;
 differenza  delle  imprese  i  cui  scarichi  recapitano in pubbliche    &#13;
 fognature;                                                               &#13;
     che,  nei  giudizi introdotti con le ordinanze r.o. nn. 585 e 747    &#13;
 del 1995 (relative a disposizioni contenute nel decreto-legge n.  629    &#13;
 del 1994), è intervenuto il Presidente del Consiglio  dei  ministri,    &#13;
 rappresentato  e  difeso dall'Avvocatura generale dello Stato, che ha    &#13;
 concluso per l'inammissibilità delle questioni, non essendo stato il    &#13;
 decreto-legge convertito nel termine di  sessanta  giorni  dalla  sua    &#13;
 pubblicazione;                                                           &#13;
   Considerato  che  le  ordinanze  di rimessione si riferiscono tutte    &#13;
 alla disciplina degli scarichi  delle  pubbliche  fognature  e  degli    &#13;
 insediamenti civili che non recapitano in pubbliche fognature: alcune    &#13;
 denunciano  disposizioni  del decreto-legge 16 novembre 1994, n. 629,    &#13;
 altre il decreto-legge 16 gennaio 1995, n. 9 (nella sua  interezza  o    &#13;
 limitatamente all'art. 3);                                               &#13;
     che   essendo   prospettate   questioni   identiche  o  connesse,    &#13;
 concernenti la disciplina della stessa  materia,  i  giudizi  possono    &#13;
 essere riuniti per essere decisi con unica pronuncia;                    &#13;
     che  i  due  decreti-legge  sottoposti a verifica di legittimità    &#13;
 costituzionale sono decaduti, non essendo stati convertiti  in  legge    &#13;
 entro  il  termine  di sessanta giorni dalla rispettiva pubblicazione    &#13;
 (come risulta dai comunicati  pubblicati  nella  Gazzetta  Ufficiale,    &#13;
 serie  generale,  nn.  12  e  65,  del 16 gennaio 1995 e del 18 marzo    &#13;
 1995);                                                                   &#13;
     che  la  materia  è  stata  successivamente   disciplinata   dal    &#13;
 decreto-legge  17  marzo 1995, n. 79 (Modifiche alla disciplina degli    &#13;
 scarichi delle pubbliche fognature e degli  insediamenti  civili  che    &#13;
 non  recapitano  in  pubbliche  fognature),  convertito in legge, con    &#13;
 modificazioni, con la  legge  17  maggio  1995,  n.  172.  Nella  sua    &#13;
 configurazione  definitiva,  la  disciplina della materia risulta per    &#13;
 più aspetti mutata  rispetto  a  quella  considerata  nelle  diverse    &#13;
 ordinanze  di rinvio. Tra l'altro è venuto meno il comma 2 dell'art.    &#13;
 3  del  decreto-legge  n.  9  del  1995;   non   è   più   prevista    &#13;
 l'autorizzazione   in  sanatoria,  già  contenuta  nell'art.  7  del    &#13;
 decreto-legge n. 9 del 1995; è esclusa l'applicazione di sanzioni ai    &#13;
 pubblici  amministratori  che  alla  data   di   accertamento   della    &#13;
 violazione dispongano di progetti esecutivi cantierabili, finalizzati    &#13;
 alla  depurazione  delle  acque  (norma  introdotta  nell'art.  3 del    &#13;
 decreto-legge n. 79 del 1995 dalla legge di conversione);                &#13;
     che, essendo mutato il quadro  normativo  complessivo,  gli  atti    &#13;
 vanno  restituiti ai giudici rimettenti, perché essi valutino se, in    &#13;
 base alla nuova disciplina,  le  questioni  sollevate  siano  tuttora    &#13;
 rilevanti nei giudizi principali (ordinanza n. 535 del 1995).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  ordina  la restituzione degli atti ai giudici    &#13;
 rimettenti indicati in epigrafe.                                         &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 4 marzo 1996.                                 &#13;
                         Il presidente:  Ferri                            &#13;
                        Il redattore:  Mirabelli                          &#13;
                       Il cancelliere:  Di Paola                          &#13;
   Depositata in cancelleria l'8 marzo 1996.                              &#13;
               Il direttore della cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
