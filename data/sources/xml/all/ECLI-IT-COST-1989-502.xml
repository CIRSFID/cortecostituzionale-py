<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>502</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:502</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Enzo Cheli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/10/1989</data_decisione>
    <data_deposito>15/11/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel   giudizio   di  legittimità  costituzionale  dell'art.1,  comma    &#13;
 secondo, della legge 12 agosto 1962, n.  1339  (Disposizioni  per  il    &#13;
 miglioramento  dei trattamenti di pensione corrisposti dalla Gestione    &#13;
 speciale per l'assicurazione obbligatoria  invalidità,  vecchiaia  e    &#13;
 superstiti  degli  artigiani  e  loro  familiari), dell'art. 1, comma    &#13;
 secondo, della legge 9 gennaio 1963, n. 9 (Elevazione dei trattamenti    &#13;
 minimi  di  pensione  e  riordinamento  delle  norme  in  materia  di    &#13;
 previdenza dei coltivatori  diretti  e  dei  coloni  e  mezzadri),  e    &#13;
 dell'art.  19,  comma  secondo,  della  legge  22 luglio 1966, n. 613    &#13;
 (Estensione dell'assicurazione  obbligatoria  per  l'invalidità,  la    &#13;
 vecchiaia  ed i superstiti agli esercenti attività commerciali ed ai    &#13;
 loro  familiari  coadiutori   e   coordinamento   degli   ordinamenti    &#13;
 pensionistici  per  i  lavoratori  autonomi),  promosso con ordinanza    &#13;
 emessa il 15 marzo 1989  dal  Pretore  di  Brescia  nei  procedimenti    &#13;
 civili riuniti vertenti tra Franchini Severina ed altri e l'I.N.P.S.,    &#13;
 iscritta al n. 251 del registro ordinanze  1989  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  22, prima serie speciale,    &#13;
 dell'anno 1989;                                                          &#13;
    Visto l'atto di costituzione di Tonin Angelica ed altri;              &#13;
    Udito nell'udienza pubblica del 3 ottobre 1989 il Giudice relatore    &#13;
 Enzo Cheli;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Il  Pretore  di  Brescia,  adito  da  numerosi  ricorrenti  che si    &#13;
 dolevano della mancata  integrazione  ad  opera  dell'INPS  del  loro    &#13;
 trattamento   assicurativo   di   reversibilità, ha  sollevato,  con    &#13;
 ordinanza del 15 marzo 1989, questione di legittimità costituzionale    &#13;
 dell'art.1,  secondo  comma,  della  legge  12  agosto 1962, n. 1339,    &#13;
 dell'art.1, secondo comma, della  legge  9  gennaio  1963,  n.  9,  e    &#13;
 dell'art.  19,  secondo comma, della legge 22 luglio 1966, n. 613, in    &#13;
 relazione all'art. 3 Cost., nella  parte  in  cui  tali  disposizioni    &#13;
 precludevano,  fino al 1° ottobre 1983, l'integrazione al trattamento    &#13;
 minimo in caso di concorso tra pensioni indirette gravanti sui  fondi    &#13;
 di  cui  alle  norme impugnate (Gestione speciale artigiani; Gestione    &#13;
 speciale coltivatori diretti, mezzadri e  coloni;  Gestione  speciale    &#13;
 commercianti)  e  pensioni  dirette o indirette gravanti sui medesimi    &#13;
 fondi o sul fondo lavoratori dipendenti I.N.P.S.                         &#13;
    Le questioni, sommariamente richiamate da parte del giudice a quo,    &#13;
 riguardano in particolare le ipotesi di cumulo tra:  a)  pensione  di    &#13;
 reversibilità  a  carico  della  Gestione  speciale  commercianti  e    &#13;
 pensione di invalidità a carico della Gestione speciale  coltivatori    &#13;
 diretti,  mezzadri  e  coloni  (Perugini  Narciso);  b)  pensione  di    &#13;
 reversibilità  a  carico  della  Gestione  commercianti  e  pensione    &#13;
 d'invalidità  o  di vecchiaia erogata dalla medesima Gestione (Carpi    &#13;
 Salvatore, Maffezzoni Rosa); c) pensione di  reversibilità  gravante    &#13;
 sulla  Gestione  coltivatori  diretti  e  pensione d'invalidità o di    &#13;
 vecchiaia I.N.P.S.(Franchini  Severina,  Frassine Cesarina, Gaffurini    &#13;
 Lucia,  Mora  Caterina); d) pensione di reversibilità a carico della    &#13;
 Gestione artigiani e pensione di invalidità o di vecchiaia  gravante    &#13;
 sulla  medesima  Gestione  (Magri  Maria,  Borghetti Nina, Franzoglio    &#13;
 Gemma, Gigola Angela, Bogarelli Paola, Poli Bianca, Dusi  Petronilla,    &#13;
 Stoppini  Chiarina,  Bonazzoli  Pierina) ovvero pensione di vecchiaia    &#13;
 I.N.P.S. (Tonin Angelica) ovvero pensione di vecchiaia a carico della    &#13;
 Gestione commercianti (Gussoni Giuseppe).                                &#13;
    Alcune  delle  parti si sono costituite in giudizio, chiedendo che    &#13;
 questa Corte estenda ai casi in esame i principi  già  affermati  in    &#13;
 numerose  pronunce  (e, in particolare, nelle sentenze n. 1086 e 1144    &#13;
 del  1988)  in  tema  di  integrazione  al  minimo   di   trattamenti    &#13;
 pensionistici concorrenti.<diritto>Considerato in diritto</diritto>1.  -  Le questioni oggetto del presente giudizio riguardano varie    &#13;
 ipotesi di cumulo di pensioni. Investita  di  questioni  identiche  o    &#13;
 analoghe,  questa  Corte  ha ritenuto incostituzionale la preclusione    &#13;
 dell'integrazione al minimo per i titolari di più pensioni allorché    &#13;
 per effetto del cumulo venga superato il trattamento minimo garantito    &#13;
 - rendendo così possibile la titolarità  di  più  integrazioni  al    &#13;
 minimo  sino  all'entrata  in  vigore  del decreto-legge 12 settembre    &#13;
 1983, n. 463, convertito, con modificazioni, nella legge 11  novembre    &#13;
 1983, n. 638, che ha disciplinato ex novo la materia (cfr. sentt. nn.    &#13;
 102 del 1982; 1086 e 1144 del 1988; 81, 179, 250 e 373 del 1989).        &#13;
    2.  -  Nella  parte  relativa  ai  casi  di cumulo tra pensione di    &#13;
 reversibilità e pensione d'invalidità o di vecchiaia gravante sulla    &#13;
 Gestione  commercianti, l'art. 19 della legge n. 613 del 1966 è già    &#13;
 stato oggetto  di  declaratoria  di  illegittimità  da  parte  delle    &#13;
 sentenze  nn. 179 e 250 del 1989, sì che le relative censure debbono    &#13;
 ritenersi inammissibili.                                                 &#13;
    La  questione riguardante l'art.1, secondo comma, della legge n. 9    &#13;
 del 1963, nella parte in cui esclude l'integrazione al minimo in caso    &#13;
 di  cumulo  tra  pensione  di  reversibilità a carico della Gestione    &#13;
 coltivatori  diretti  e  pensione  di  invalidità  o  di   vecchiaia    &#13;
 I.N.P.S.,  è già stata accolta con la sentenza n. 373 del 1989. Per    &#13;
 quanto concerne l'art. 1, secondo comma,  della  legge  n.  1339  del    &#13;
 1962, esso è stato dichiarato incostituzionale, con riguardo a tutte    &#13;
 le possibili ipotesi di cumulo, dalla sentenza n. 81 del 1989.  Anche    &#13;
 le  censure  relative  a  queste  norme  debbono pertanto dichiararsi    &#13;
 inammissibili.                                                           &#13;
    3.  -  La  medesima ratio, già seguita nelle richiamate sentenze,    &#13;
 deve trovare applicazione, in ossequio al principio  di  eguaglianza,    &#13;
 anche nei casi che non hanno ancora formato oggetto di esame da parte    &#13;
 di   questa   Corte.   È   pertanto   fondata   la   questione    di    &#13;
 costituzionalità dell'art. 19, secondo comma, della legge n. 613 del    &#13;
 1966, nella parte in  cui  esclude  l'integrazione  al  minimo  della    &#13;
 pensione  di  reversibilità  erogata dalla Gestione commercianti per    &#13;
 chi sia titolare anche di  pensione  di  invalidità  gravante  sulla    &#13;
 Gestione coltivatori diretti.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara:                                                              &#13;
      l'illegittimità  costituzionale  dell'art.  19,  secondo comma,    &#13;
 della legge 22 luglio 1966,  n.  613  (Estensione  dell'assicurazione    &#13;
 obbligatoria  per  l'invalidità,  la  vecchiaia  e i superstiti agli    &#13;
 esercenti attività commerciali ed ai  loro  familiari  coadiutori  e    &#13;
 coordinamento   degli  ordinamenti  pensionistici  per  i  lavoratori    &#13;
 autonomi) nella parte in cui esclude l'integrazione al  minimo  della    &#13;
 pensione   di   reversibilità   erogata   dalla   Gestione  speciale    &#13;
 commercianti ai titolari di pensione di invalidità  a  carico  della    &#13;
 Gestione  speciale  coltivatori  diretti,  qualora,  per  effetto del    &#13;
 cumulo,  il  complessivo  trattamento  risulti  superiore  al  minimo    &#13;
 anzidetto;                                                               &#13;
      la  manifesta  inammissibilità  della questione di legittimità    &#13;
 costituzionale dell'art. 19, secondo comma,  della  legge  22  luglio    &#13;
 1966  n.  613, già dichiarato costituzionalmente illegittimo, con le    &#13;
 sentenze di questa Corte nn. 179 e 250 del 1989, nella parte  in  cui    &#13;
 esclude  l'integrazione  al  minimo  della pensione di reversibilità    &#13;
 erogata dalla Gestione speciale commercianti ai titolari di  pensione    &#13;
 di   vecchiaia  o  d'invalidità  erogata  dalla  medesima  Gestione,    &#13;
 qualora, per effetto del cumulo, il complessivo  trattamento  risulti    &#13;
 superiore  al  minimo  anzidetto,  questione  sollevata, in relazione    &#13;
 all'art. 3 della Costituzione, dal Pretore di Brescia con l'ordinanza    &#13;
 di cui in epigrafe;                                                      &#13;
      la  manifesta  inammissibilità  della questione di legittimità    &#13;
 costituzionale dell'art. 1, secondo  comma,  della  legge  9  gennaio    &#13;
 1963,   n.  9  (Elevazione  dei  trattamenti  minimi  di  pensione  e    &#13;
 riordinamento delle norme in materia di  previdenza  dei  coltivatori    &#13;
 diretti  e  dei coloni e mezzadri) già dichiarato costituzionalmente    &#13;
 illegittimo, con la sentenza di questa Corte n.  373 del 1989,  nella    &#13;
 parte  in  cui  esclude  l'integrazione  al  minimo della pensione di    &#13;
 reversibilità erogata dalla Gestione speciale coltivatori diretti ai    &#13;
 titolari  di  pensione  diretta  I.N.P.S.,  qualora,  per effetto del    &#13;
 cumulo,  il  trattamento  complessivo  risulti  superiore  al  minimo    &#13;
 anzidetto,   questione  sollevata,  in  relazione  all'art.  3  della    &#13;
 Costituzione, dal Pretore  di  Brescia  con  l'ordinanza  di  cui  in    &#13;
 epigrafe;                                                                &#13;
      la  manifesta  inammissibilità  della questione di legittimità    &#13;
 costituzionale dell'art. 1, secondo comma, della legge 12 agosto 1962    &#13;
 n.  1339  (Disposizioni  per  il  miglioramento  dei  trattamenti  di    &#13;
 pensione corrisposti  dalla  Gestione  speciale  per  l'assicurazione    &#13;
 obbligatoria  invalidità,  vecchiaia  e superstiti degli artigiani e    &#13;
 loro  familiari),  già  dichiarato  costituzionalmente  illegittimo,    &#13;
 sotto  ogni profilo, con la sentenza di questa Corte n.  81 del 1989,    &#13;
 questione sollevata, in relazione all'art. 3 della Costituzione,  dal    &#13;
 Pretore di Brescia con l'ordinanza in epigrafe.                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 26 ottobre 1989.                              &#13;
                          Il Presidente: CONSO                            &#13;
                          Il redattore: CHELI                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 15 novembre 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
