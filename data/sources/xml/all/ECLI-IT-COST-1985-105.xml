<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>105</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:105</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROEHRSSEN</presidente>
    <relatore_pronuncia>Brunetto Bucciarelli Ducci</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/04/1985</data_decisione>
    <data_deposito>17/04/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GUGLIELMO ROEHRSSEN, Presidente - Avv. &#13;
 ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Dott. FRANCESCO SAIA - Prof. GIOVANNI CONSO - &#13;
 Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. GIUSEPPE BORZELLINO &#13;
 - Dott. FRANCESCO GRECO. Giudici.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.    35  del  &#13;
 d.P.R.  30 maggio 1955, n. 797 (T.U. sugli assegni familiari), promosso  &#13;
 con ordinanza emessa il 15  luglio  1977  dal  Pretore  di  Milano  nel  &#13;
 procedimento    civile    vertente    tra   Francescutto   Giuseppe   e  &#13;
 Pasticceria-Confetteria Cova ed altro, iscritta al n. 494 del  registro  &#13;
 ordinanze  1977  e pubblicata nella Gazzetta Ufficiale della Repubblica  &#13;
 n. 353 dell'anno 1977.                                                   &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del 30 gennaio 1985 il Giudice  &#13;
 relatore Brunetto Bucciarelli Ducci.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Il Pretore di Milano con  ordinanza  del  15  luglio  1977  ha  &#13;
 sollevato  questione  incidentale  di  legittimità  costituzionale, in  &#13;
 riferimento agli artt.  1,  2,  3,  4,  36  e  38  della  Costituzione,  &#13;
 dell'art.  35 d.P.R. 30 maggio 1955, n. 797, nella parte in cui dispone  &#13;
 per particolari categorie di lavoratori che i contributi e gli  assegni  &#13;
 familiari  loro  dovuti  siano  riferiti  a salari medi ed a periodi di  &#13;
 occupazione media mensile stabiliti convenzionalmente con  decreto  del  &#13;
 Ministro  del  Lavoro,  sentiti  il  Comitato  speciale per gli assegni  &#13;
 familiari e le associazioni professionali interessate.                   &#13;
     Dubita  il  giudice  a quo che tale sistema di determinazione delle  &#13;
 medie retributive, una  volta  applicato  -  per  effetto  del  sistema  &#13;
 normativo  vigente  -  alla  commisurazione  delle  pensioni,  violi il  &#13;
 principio della  integrale  rispondenza  fra  retribuzione  globale  di  &#13;
 fatto,  onere  contributivo  e  misura  del  trattamento pensionistico,  &#13;
 nonché il principio di uguaglianza, non definendo alcun criterio nella  &#13;
 individuazione  delle  "particolari  categorie"   di   lavoratori,   da  &#13;
 sottrarre  al  principio  generale  della determinazione della pensione  &#13;
 sulla base della retribuzione effettivamente percepita.                  &#13;
     Il procedimento di  merito  trae  origine  dalla  doglianza  di  un  &#13;
 pensionato  per  aver  ricevuto  dall'INPS  (Istituto  Nazionale per la  &#13;
 Previdenza  Sociale)  una  pensione  determinata  in  base  alle  medie  &#13;
 retributive   convenzionali   anziché   alla   retribuzione  realmente  &#13;
 percepita.                                                               &#13;
     2. - Si è costituito in giudizio il Presidente del  Consiglio  dei  &#13;
 ministri,  rappresentato e difeso dall'Avvocatura generale dello Stato,  &#13;
 osservando  in  via  preliminare  che  la   questione   sollevata   con  &#13;
 l'ordinanza  di  rimessione  avrebbe  ad  oggetto  non  tanto  la norma  &#13;
 impugnata  (art.  35  d.P.R.  n.    797/1955),  che  riguarda  la  base  &#13;
 imponibile  dei  contributi e degli assegni familiari, quanto l'art. 2,  &#13;
 sub 6, ultimo comma, della legge  4  aprile  1952,  n.    218  (che  ha  &#13;
 modificato  l'art.  6  R.D.L.  14  aprile  1939,  n.  636),  il  quale,  &#13;
 consentendo di rapportare la pensione ad una retribuzione non reale  ma  &#13;
 convenzionale,  verrebbe  eventualmente  a  contrastare  con  i  citati  &#13;
 parametri costituzionali.                                                &#13;
     Nel merito la difesa dello Stato contesta comunque che la normativa  &#13;
 impugnata contrasti con le norme costituzionali invocate.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - La Corte è chiamata a decidere se contrasti o  meno  con  gli  &#13;
 artt.  1,  2,  3, 4, 36 e 38 della Costituzione l'art. 35 del d.P.R. 30  &#13;
 maggio 1955, n.  797 (testo unico sugli assegni familiari), nella parte  &#13;
 in cui consentirebbe per alcune particolari categorie di lavoratori  di  &#13;
 commisurare i contributi previdenziali, e di conseguenza le pensioni di  &#13;
 vecchiaia   e   di   anzianità,  a  retribuzioni  medie  convenzionali  &#13;
 determinate  con  decreto  del  Ministro  del  Lavoro   anziché   alle  &#13;
 retribuzioni   effettivamente   percepite;   per  il  dubbio  che  tale  &#13;
 disposizione violi il  principio  della  integrale  corrispondenza  tra  &#13;
 retribuzioni  di fatto, onere contributivo e trattamento pensionistico,  &#13;
 nonché   il   principio   di   uguaglianza,   consentendo   arbitrarie  &#13;
 discriminazioni tra lavoratori.                                          &#13;
     2.  -  In  via  preliminare  va  individuata la disposizione che il  &#13;
 giudice a quo intende effettivamente impugnare, come giustamente rileva  &#13;
 la difesa dello Stato.                                                   &#13;
     L'art. 35 del d.P.R. n. 797 del  1955  dispone,  infatti,  che  per  &#13;
 particolari   categorie  di  lavoratori  i  contributi  e  gli  assegni  &#13;
 familiari possono essere riferiti rispettivamente ad  apposite  tabelle  &#13;
 di salari medi e di periodi di occupazione media mensile, stabilite con  &#13;
 decreto  del  Ministro del Lavoro, sentito il Comitato speciale per gli  &#13;
 assegni familiari  e  le  associazioni  professionali  interessate.  La  &#13;
 questione  sollevata però ha ad oggetto le modalità di commisurazione  &#13;
 delle pensioni (d'invalidità, vecchiaia e anzianità) per le  suddette  &#13;
 categorie  di lavoratori, da liquidarsi con decorrenza successiva al 30  &#13;
 aprile 1968 (art. 1 d.P.R. 27 aprile 1968, n.  488), denunciandosi  che  &#13;
 essa venga riferita alle retribuzioni medie convenzionali, in base alle  &#13;
 disposizioni  vigenti ai fini del calcolo dei contributi dovuti per gli  &#13;
 assegni  familiari (art. 17, comma primo, legge 4 aprile 1952, n. 218).  &#13;
 L'art.  35  impugnato   viene   quindi   in   considerazione   soltanto  &#13;
 mediatamente,  mentre la norma da cui discende la situazione denunciata  &#13;
 - come in contrasto  con  gli  invocati  parametri  costituzionali,  in  &#13;
 quanto  consente  di  rapportare  la  pensione  ad una retribuzione non  &#13;
 reale, ma convenzionale - è piuttosto l'art. 2 della  citata  legge  4  &#13;
 aprile  1952, n.   218 (riordinamento delle pensioni dell'assicurazione  &#13;
 obbligatoria), il quale recita nel primo comma: "I  contributi  per  le  &#13;
 assicurazioni  base per la invalidità, vecchiaia e superstiti ... sono  &#13;
 dovuti nella misura stabilita dalle tabelle A e B allegate al  presente  &#13;
 decreto  e  per  ogni  periodo di lavoro nelle medesime indicato" e nel  &#13;
 quarto comma: "Per particolari categorie di  lavoratori  ed  anche  per  &#13;
 limitate zone del territorio nazionale, il Ministero per il lavoro e la  &#13;
 previdenza  sociale, sentite le organizzazioni sindacali dei lavoratori  &#13;
 e dei datori di lavoro, può stabilire apposite tabelle di retribuzioni  &#13;
 medie agli effetti del calcolo del  contributo  e  fissare  altresì  i  &#13;
 periodi medi di attività lavorativa".                                   &#13;
     La   questione   sollevata  dal  giudice  a  quo  e  rilevante  nel  &#13;
 procedimento di merito riguarda pertanto più l'art. 2 della  legge  n.  &#13;
 218 del 1952 che non l'art. 35 d.P.R. n. 797 del 1955.                   &#13;
     3. - La questione comunque non è fondata.                           &#13;
     La normativa impugnata, così come sopra individuata, non contrasta  &#13;
 infatti con nessuno dei parametri costituzionali invocati.               &#13;
     Gli  artt.  1  e  2  della  Costituzione pongono rispettivamente il  &#13;
 lavoro a fondamento della Repubblica, affermando la preminenza di  ogni  &#13;
 attività  lavorativa  nel  sistema dei diritti-doveri del cittadino, e  &#13;
 tutelano i diritti inviolabili dell'uomo; ma dettano specifici  criteri  &#13;
 cui  dovrebbe  attenersi  il legislatore nel disciplinare i diritti dei  &#13;
 lavoratori.                                                              &#13;
     Quanto all'art. 3 della Costituzione  la  lamentata  differenza  di  &#13;
 disciplina  tra lavoratori a seconda che la pensione sia collegata alla  &#13;
 retribuzione  reale  o   a   quella   convenzionale   trova   razionale  &#13;
 giustificazione   nell'impossibilità   di  accertare  in  concreto  la  &#13;
 retribuzione  effettiva  di  talune   categorie   di   lavoratori   che  &#13;
 percepiscono  compensi  non  fissi  ma  variabili,  per essere i salari  &#13;
 calcolati in  misura  percentuale  sugli  incassi  delle  imprese  (es.  &#13;
 esercizi   pubblici)   o   correlati   con  servizi  non  continuativi.  &#13;
 L'adozione,  di  concerto  con  le  organizzazioni  di  categoria   dei  &#13;
 lavoratori  e  dei datori di lavoro, di medie retributive convenzionali  &#13;
 per determinate  categorie  di  lavoratori  e  per  zone  limitate  del  &#13;
 territorio  nazionale  costituisce  pertanto  un  criterio  legislativo  &#13;
 seguito, specie nel passato, ogni volta  che  non  fosse  possibile,  o  &#13;
 fosse  estremamente  oneroso, ricostruire le retribuzioni effettive per  &#13;
 le caratteristiche peculiari di alcune attività lavorative.    Nessuna  &#13;
 lesione quindi del principio di uguaglianza può essere attribuita alla  &#13;
 normativa impugnata.                                                     &#13;
     A  sua volta l'art. 4 della Costituzione ha valenza programmatica e  &#13;
 impegna il legislatore ad  intervenire  per  garantire  il  diritto  al  &#13;
 lavoro  a  tutti  i  cittadini, ma non comporta alcun divieto specifico  &#13;
 circa  il  collegamento  delle  pensioni  sociali   assicurative   alle  &#13;
 retribuzioni convenzionali anziché a quelle reali.                      &#13;
     Né  infine  si  ravvisano  violazioni  degli  artt.  36 e 38 della  &#13;
 Costituzione, rientrando nei poteri del legislatore ordinario - come si  &#13;
 è  più  volte  pronunciata  questa  Corte  -  "determinare,  con  una  &#13;
 razionale  considerazione delle esigenze di vita dei lavoratori e delle  &#13;
 effettive  disponibilità  finanziarie, l'ammontare delle prestazioni e  &#13;
 di modificarne la  misura  allo  scopo  di  rendere  sempre  attuale  e  &#13;
 costante  il  rapporto  tra  i termini che dovessero subire variazioni"  &#13;
 (sent. n. 128 del 1973).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'art. 35 del d.P.R. 30 maggio 1955, n. 797 sollevata dal Pretore di  &#13;
 Milano  con  l'ordinanza  del  15  luglio  1977  (r.o. n. 494/1977), in  &#13;
 relazione agli artt. 1, 2, 3, 4, 36 e 38 della Costituzione              &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 16 aprile 1985.         &#13;
                                   F.to:  GUGLIELMO  ROEHRSSEN  - ORONZO  &#13;
                                   REALE - BRUNETTO BUCCIARELLI DUCCI  -  &#13;
                                   ALBERTO  MALAGUGINI - LIVIO PALADIN -  &#13;
                                   ANTONIO   LA   PERGOLA   -   VIRGILIO  &#13;
                                   ANDRIOLI  - FRANCESCO SAJA - GIOVANNI  &#13;
                                   CONSO   -   ETTORE   GALLO   -   ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
