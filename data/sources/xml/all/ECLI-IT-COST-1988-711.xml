<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>711</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:711</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/06/1988</data_decisione>
    <data_deposito>23/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di  legittimità costituzionali degli artt. 40, secondo    &#13;
 comma,  della  legge  11  aprile  1955,  n.  379  (Miglioramenti  dei    &#13;
 trattamenti di quiescenza e modifiche agli ordinamenti degli Istituti    &#13;
 di Previdenza presso il Ministero del  Tesoro)  e  27,  primo  comma,    &#13;
 della  legge  26 luglio 1965, n. 965 (Miglioramenti ai trattamenti di    &#13;
 quiescenza delle Casse per  le  pensioni  ai  dipendenti  degli  Enti    &#13;
 locali  ed  agli  insegnanti,  modifiche agli ordinamenti delle Casse    &#13;
 pensioni  facenti  parte  degli  Istituti  di  Previdenza  presso  il    &#13;
 Ministero del Tesoro), promossi con le seguenti ordinanze:               &#13;
      1)  ordinanza  emessa  il  5  marzo 1984 dalla Corte dei Conti -    &#13;
 Sezione III Giurisdizionale  -  sul  ricorso  proposto  da  Venturini    &#13;
 Maria,  iscritta  al  n. 152 del registro ordinanze 1985 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 137- bis dell'anno 1985;    &#13;
      2)  ordinanza  emessa  il 19 giugno 1986 dalla Corte dei Conti -    &#13;
 Sezione giurisdizionale per la Sardegna -  sul  ricorso  proposto  da    &#13;
 Coni Anna Maria contro il Ministero del Tesoro, iscritta al n. 77 del    &#13;
 registro ordinanze 1987 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 14, prima serie speciale, dell'anno 1987;                  &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 24 febbraio 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  la  Corte  dei Conti, con ordinanza in data 5 marzo    &#13;
 1984 (R.O. n. 152 del 1985), ha sollevato questione  di  legittimità    &#13;
 costituzionale  dell'art.  40,  secondo  comma, della legge 11 aprile    &#13;
 1955 n. 379, in relazione  all'art.  3  Cost.,  nella  parte  in  cui    &#13;
 dispone  che le condizioni soggettive di inabilità a proficuo lavoro    &#13;
 previste  per  il  conferimento  del  diritto   al   trattamento   di    &#13;
 riversibilità  devono  sussistere  al  momento della morte del dante    &#13;
 causa;                                                                   &#13;
      che  la  stessa  Corte, Sezione giurisdizionale per la Sardegna,    &#13;
 con ordinanza del 19 giugno 1986 (R.O. n. 77 del 1987) ha  riproposto    &#13;
 identica  questione in ordine al citato art. 40, comma secondo, della    &#13;
 legge n.  379  del  1955,  estendendo,  peraltro,  la  censura  anche    &#13;
 all'art.  27,  comma  primo,  della  legge  26 luglio 1965 n. 965, in    &#13;
 quanto tali norme, ai fini della  riversibilità  della  pensione  in    &#13;
 favore   dell'orfano   maggiorenne   "dispongono  che  la  condizione    &#13;
 soggettiva della inabilità a  proficuo  lavoro  deve  sussistere  al    &#13;
 momento  suddetto  o,  comunque,  per i casi contemplati dall'art. 27    &#13;
 della legge n. 965 cit., all'atto dell'entrata in vigore della  legge    &#13;
 medesima";                                                               &#13;
      che,  ad  avviso  del  giudice  a quo, le disposizioni impugnate    &#13;
 incorrerebbero, infatti,  in  una  duplice  violazione  del  precetto    &#13;
 dell'eguaglianza:  sia in ragione della irragionevole discriminazione    &#13;
 operata tra orfani inabili,  secondo  il  momento  in  cui  lo  siano    &#13;
 divenuti,  sia  per  la  disparità  di trattamento, che ne consegue,    &#13;
 rispetto  agli  orfani  di  guerra,  aventi  diritto  alla   pensione    &#13;
 indiretta,   indipendentemente   dalla   data   di  insorgenza  della    &#13;
 invalidità;                                                             &#13;
      che,   nel  giudizio  innanzi  alla  Corte,  è  intervenuto  il    &#13;
 Presidente del Consiglio dei ministri, che ha eccepito l'infondatezza    &#13;
 di ogni censura di incostituzionalità;                                  &#13;
    Considerato  che  l'odierna  impugnativa dell'art. 40 legge n. 379    &#13;
 del 1955 sostanzialmente ripropone le identiche  questioni  che,  con    &#13;
 riguardo  a  norme  di  analogo  contenuto, questa Corte ha già più    &#13;
 volte dichiarato  infondate,  sul  rilievo,  per  un  verso,  che  la    &#13;
 disciplina  pensionistica di guerra, per la sua natura essenzialmente    &#13;
 risarcitoria, non è estensibile al regime  pensionistico  ordinario;    &#13;
 e,  per  altro  verso,  che  la differente valutazione, ai fini della    &#13;
 riversibilità,    dell'inabilità    dell'orfano    sussistente    o    &#13;
 sopravvenuta  alla  data  del decesso del dante causa trova razionale    &#13;
 giustificazione nella diversità delle situazioni comparate. La prima    &#13;
 delle  quali  configura uno stato di bisogno direttamente determinato    &#13;
 dalla interruzione della situazione di vivenza a carico del  defunto;    &#13;
 mentre  la  seconda  prospetta  uno  stato  di bisogno esclusivamente    &#13;
 dipendente dalla sopravvenuta causa invalidante e quindi sfornito  di    &#13;
 collegamento  causale  e temporale con la morte del familiare (v., da    &#13;
 ultimo, Corte cost. 1984 n. 142);                                        &#13;
      che, per quanto poi attiene alla disciplina sub art. 27 legge n.    &#13;
 965 del 1965, trattasi, all'evidenza, di norma transitoria -  dettata    &#13;
 con  specifico  riguardo  alla  peculiare  situazione  dei  figli dei    &#13;
 dipendenti o pensionati deceduti prima dell'entrata in  vigore  della    &#13;
 nuova  disciplina delle pensioni dei dipendenti degli enti locali - e    &#13;
 che non pone problemi di contrasto con l'art. 3 Cost.;                   &#13;
      che,   pertanto,  le  sollevate  questioni  sono  manifestamente    &#13;
 infondate;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza delle questioni di legittimità    &#13;
 costituzionale degli artt. 40, comma secondo, legge 11  aprile  1955,    &#13;
 n.  379 (Miglioramenti dei trattamenti di quiescenza e modifiche agli    &#13;
 ordinamenti degli Istituti di  Previdenza  presso  il  Ministero  del    &#13;
 Tesoro),   e  27,  comma  primo,  legge  26  luglio  1965,  n.   965,    &#13;
 (Miglioramenti ai  trattamenti  di  quiescenza  delle  Casse  per  le    &#13;
 pensioni   ai  dipendenti  degli  Enti  locali  ed  agli  insegnanti,    &#13;
 modifiche agli ordinamenti delle Casse pensioni facenti  parte  degli    &#13;
 Istituti di Previdenza presso il Ministero del Tesoro), sollevate, in    &#13;
 riferimento all'art. 3 Cost., dalla Corte dei Conti, con le ordinanze    &#13;
 in epigrafe.                                                             &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 9 giugno 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
