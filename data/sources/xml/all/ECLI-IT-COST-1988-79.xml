<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>79</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:79</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/01/1988</data_decisione>
    <data_deposito>26/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale della legge regionale    &#13;
 riapprovata il 23 aprile 1980 dal  Consiglio  regionale  dell'Umbria,    &#13;
 recante  "Provvidenze  per  la  partecipazione  degli  emigrati  alle    &#13;
 consultazioni elettorali", promosso con ricorso  del  Presidente  del    &#13;
 Consiglio  dei  ministri, notificato il 13 maggio 1980, depositato in    &#13;
 cancelleria il 20 (successivo) ed iscritto  al  n.  12  del  registro    &#13;
 ricorsi 1980;                                                            &#13;
    Visto l'atto di costituzione della Regione Umbria;                    &#13;
    Udito  nell'udienza  pubblica  del  10  novembre  1987  il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Uditi  l'Avvocato dello Stato Sergio Laporta, per il ricorrente, e    &#13;
 l'avv. Goffredo Gobbi per la Regione.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ricorso notificato in data 13 maggio 1980 e depositato il    &#13;
 successivo 20 maggio, il Presidente del  Consiglio  dei  ministri  ha    &#13;
 promosso   questione   di  legittimità  costituzionale  della  legge    &#13;
 regionale  indicata  in  oggetto  con  la  quale  è  stata  disposta    &#13;
 un'erogazione di L. 40.000 a favore dei cittadini emigrati all'estero    &#13;
 per la partecipazione alle consultazioni elettorali.                     &#13;
    L'Avvocatura dello Stato richiama le sentenze nn. 39 del 1973 e 90    &#13;
 del 1974 di questa Corte (e l'affermazione in esse contenuta  secondo    &#13;
 cui  le  agevolazioni agli elettori che si rechino a votare rientrano    &#13;
 nella disciplina dell'esercizio del  diritto  di  elettorato  attivo)    &#13;
 onde  sostenere  che  la  legge  impugnata interferisce nella materia    &#13;
 elettorale,  in  quanto  applicabile  anche  nel  caso  di   elezioni    &#13;
 politiche,  e  rende  addirittura  esplicito  l'intento  di agire "in    &#13;
 supplenza" della legislazione statale, atteso il tenore  dell'art.  1    &#13;
 che  individua  la  ratio  dell'intervento  in  favore degli emigrati    &#13;
 nell'attesa di analoghe provvidenze da parte dello Stato.                &#13;
    Ma  i  benefici  disposti  dalla legge verrebbero ad alterare - si    &#13;
 argomenta in ricorso - la parità di  tutti  i  cittadini  di  fronte    &#13;
 all'esercizio del diritto di voto.                                       &#13;
    Né  potrebbe  inquadrarsi  la  legge nell'ambito della competenza    &#13;
 regionale in materia di beneficenza pubblica: l'Avvocatura osserva  a    &#13;
 riguardo  che  le agevolazioni sono previste prescindendo dal censo o    &#13;
 lo stato di bisogno dei beneficiari, ma  soltanto  in  ragione  della    &#13;
 loro partecipazione alle elezioni.                                       &#13;
    2.  -  Il  Presidente  della  Regione Umbria ha depositato memoria    &#13;
 premettendo che la legge impugnata  si  inquadra  in  un  sistema  di    &#13;
 assistenza  agli  emigrati  delineato e progressivamente perfezionato    &#13;
 dalla Regione con le leggi regionali 27 luglio 1973, n. 28, 22 giugno    &#13;
 1979,  n.  31 e 2 maggio 1980 n. 40. Osserva in particolare la difesa    &#13;
 della Regione che le consultazioni elettorali non  rappresentano  che    &#13;
 un'occasione  per  attuare  interventi  di  assistenza  a  favore  di    &#13;
 cittadini che versano in situazione  di  particolare  svantaggio:  la    &#13;
 legge   sarebbe   infatti   del   tutto   estranea   alla  disciplina    &#13;
 dell'esercizio dell'elettorato attivo e  si  inquadrerebbe  viceversa    &#13;
 nella  competenza  regionale  in  materia  di beneficenza, secondo la    &#13;
 definizione datane dall'art. 22 del d.P.R. 27 luglio  1977,  n.  616.    &#13;
 Non  vi  sarebbe  conclusivamente  identità  tra le previsioni della    &#13;
 legge impugnata e la fattispecie oggetto della  sentenza  n.  39  del    &#13;
 1973,  nella  quale  la Corte ebbe a dichiarare illegittima una legge    &#13;
 che disponeva il rimborso del  biglietto  di  viaggio  a  favore  dei    &#13;
 lavoratori pugliesi emigrati. Attraverso l'erogazione disposta con la    &#13;
 denunciata  normativa  la  Regione  Umbria  si  propone  infatti   di    &#13;
 compensare il mancato guadagno degli emigrati.<diritto>Considerato in diritto</diritto>1. - La legge impugnata enuncia programmaticamente, all'art. 1, la    &#13;
 finalità di agevolare a favore  dei  cittadini  emigrati  all'estero    &#13;
 l'esercizio  del  diritto-dovere  di  cui  all'art.  48 Cost. sino al    &#13;
 momento  in  cui  non  verranno  disposte  dallo  Stato   provvidenze    &#13;
 specifiche in tal senso.                                                 &#13;
    L'art.  2  consente  ai  Comuni  della  Regione  di  erogare  agli    &#13;
 emigrati, iscritti in appositi elenchi, la somma di L. 40.000 per  la    &#13;
 partecipazione alle consultazioni elettorali, politiche, regionali ed    &#13;
 amministrative. L'art. 3 individua gli  aventi  diritto  in  tutti  i    &#13;
 votanti   residenti   all'estero.  L'art.  4  definisce,  infine,  il    &#13;
 meccanismo di rimborso agli  enti  locali  eroganti  da  parte  della    &#13;
 Giunta regionale.                                                        &#13;
    Il  Presidente  del  Consiglio  dei  ministri  ha  prospettato  la    &#13;
 violazione dell'art.  117  Cost.  per  l'interferenza  in  una  sfera    &#13;
 riservata   alla   competenza   statale   quale   è   la  disciplina    &#13;
 dell'esercizio  del  diritto  al  voto,   lamentando   altresì   che    &#13;
 l'intervento   regionale   abbia   determinato   una   disparità  di    &#13;
 trattamento tra gli elettori umbri e quelli delle altre regioni.         &#13;
    La  difesa  della Regione si fonda essenzialmente sull'assunto per    &#13;
 cui la legge in argomento rientrerebbe  nella  materia  -  di  sicura    &#13;
 competenza regionale - dell'assistenza e della beneficenza.              &#13;
    2. - La questione è fondata.                                         &#13;
    La  Corte, con la sentenza n. 39 del 1973, ha già avuto occasione    &#13;
 di affermare come soltanto lo Stato sia legittimato a  provvedere  in    &#13;
 materia  di  disciplina  delle  forme  e  dei  limiti  dell'esercizio    &#13;
 dell'elettorato politico attivo.                                         &#13;
    Va  qui  ribadito  il  principio  secondo  il  quale  deve  essere    &#13;
 assicurata un'assoluta parità di trattamento dei cittadini allorché    &#13;
 essi  esprimono il voto in ragione della delicatezza ed importanza di    &#13;
 tale   momento   di   esercizio   della   sovranità   popolare.   È    &#13;
 conseguentemente   escluso   che  la  Regione  possa,  anche  in  via    &#13;
 integrativa e sia pure con un intervento "in melius",  modificare  le    &#13;
 condizioni di svolgimento delle consultazioni politiche, come appunto    &#13;
 si verifica in concreto attraverso  le  provvidenze  stabilite  dalla    &#13;
 legge impugnata. La ratio di quest'ultima è del resto resa esplicita    &#13;
 testualmente,  mentre  la   totale   mancanza   di   un   titolo   di    &#13;
 individuazione    dei   beneficiari   diverso   dalla   qualità   di    &#13;
 emigranti-elettori, non consente di inserire la legge  nella  materia    &#13;
 dell'assistenza  e  della  beneficenza,  sia  pure  nella  più ampia    &#13;
 accezione conferita a tale espressione dall'art. 22 d.P.R. 24  luglio    &#13;
 1977, n. 616.                                                            &#13;
    Limitatamente   all'ipotesi   di   elezioni  politiche  va  quindi    &#13;
 affermato il contrasto della legge impugnata con gli                     &#13;
 artt. 117 e 3 Cost.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità  costituzionale  della  l.  reg.  Umbria    &#13;
 approvata  il  17  marzo  1980  e  riapprovata  il  23  aprile   1980    &#13;
 ("Provvidenze per la partecipazione degli emigrati alle consultazioni    &#13;
 elettorali") nella parte in cui prevede una erogazione di  denaro  in    &#13;
 favore  dei cittadini emigrati in occasione della loro partecipazione    &#13;
 alle elezioni politiche.                                                 &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
