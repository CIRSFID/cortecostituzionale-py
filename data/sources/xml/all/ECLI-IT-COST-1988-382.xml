<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>382</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:382</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/03/1988</data_decisione>
    <data_deposito>31/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 419, primo    &#13;
 comma,  del  codice  civile   (Mezzi   istruttori   e   provvedimenti    &#13;
 provvisori),  promosso  con  ordinanza  emessa  l'8  maggio  1981 dal    &#13;
 Tribunale di Torino, iscritta al n. 662 del registro ordinanze 1981 e    &#13;
 pubblicata  nella Gazzetta Ufficiale della Repubblica n. 12 dell'anno    &#13;
 1982;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 24 febbraio 1988 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Ritenuto che nel giudizio promosso da Adelaide Rovero per ottenere    &#13;
 la pronuncia  di  interdizione  del  fratello  Francesco  Rovero,  il    &#13;
 Tribunale  di Torino rilevava che il giudice istruttore si era recato    &#13;
 presso il domicilio dell'interdicendo senza poterlo esaminare perché    &#13;
 irreperibile,   non  essendo  questi  comparso,  poi,  neppure  nelle    &#13;
 successive udienze;                                                      &#13;
      che,  in  dipendenza  di  ciò,  il suddetto Tribunale sollevava    &#13;
 questione di legittimità costituzionale, in riferimento all'art.  24    &#13;
 Cost.,  dell'art.  419, primo comma, c.c., nella parte in cui esclude    &#13;
 la dichiarazione  di  interdizione  nel  caso  di  impossibilità  di    &#13;
 procedere all'esame dell'interdicendo resosi irreperibile;               &#13;
      che  ad  avviso  del  giudice  a  quo la disposizione denunciata    &#13;
 poteva essere  interpretata  solo  nel  senso  che  "la  mancanza  di    &#13;
 preventivo esame dell'interdicendo... da qualunque causa determinata,    &#13;
 preclude irrevocabilmente la pronuncia dell'interdizione";               &#13;
      che, sempre secondo il giudice rimettente, una preclusione così    &#13;
 rigorosa ed assoluta, oltre a poter  risultare  svantaggiosa  per  lo    &#13;
 stesso  interdicendo,  menomava  il  diritto  di  difesa dei soggetti    &#13;
 legittimati a chiedere l'interdizione, ponendosi così  in  contrasto    &#13;
 con l'art. 24 Cost.;                                                     &#13;
      che  nel  giudizio  dinanzi alla Corte ha spiegato intervento il    &#13;
 Presidente  del  Consiglio  dei  ministri,  rappresentato  e   difeso    &#13;
 dall'Avvocatura  Generale dello Stato, argomentando e concludendo per    &#13;
 l'infondatezza della questione;                                          &#13;
      che,  secondo  la  Presidenza,  la  disposizione  impugnata  non    &#13;
 produce gli effetti paralizzanti  del  procedimento  di  interdizione    &#13;
 ipotizzati  nell'ordinanza  di  rinvio,  dovendo  essere  chiaramente    &#13;
 interpretata come norma che consente la prosecuzione del procedimento    &#13;
 anche  in  caso  di irreperibilità dell'interdicendo, dopo che siano    &#13;
 stati esperiti senza esito  i  mezzi  di  ricerca  della  dimora  del    &#13;
 convenuto;                                                               &#13;
    Considerato  che  l'esame  ex  art.  419,  primo comma, c.c., deve    &#13;
 essere  inteso   come   indagine   sulla   complessiva   personalità    &#13;
 dell'interdicendo,  quale  emerge  non  solo dalle risposte date alle    &#13;
 domande del giudice ma  anche  dall'intero  comportamento  tenuto  in    &#13;
 occasione  dell'incontro con il giudice ovvero dalla stessa eventuale    &#13;
 scelta di sottrarsi a tale incontro;                                     &#13;
      che,   sulla   base   di   questa   linea   interpretativa,   la    &#13;
 giurisprudenza della Corte di cassazione ha affermato  che  l'obbligo    &#13;
 dell'esame   può   considerarsi   assolto   ed  il  procedimento  di    &#13;
 interdizione può proseguire ogniqualvolta l'interdicendo, che  abbia    &#13;
 già  rifiutato  di  comparire dinanzi al giudice istruttore, insista    &#13;
 nel suo rifiuto a farsi esaminare;                                       &#13;
      che,  al  pari  di  quanto  avviene  per  il rifiuto dell'esame,    &#13;
 l'irreperibilità,  ritualmente  accertata,  dell'interdicendo   (che    &#13;
 vanifica ogni tentativo del giudice di raggiungerlo) non ha l'effetto    &#13;
 di paralizzare il corso del procedimento di interdizione;                &#13;
      che,  in  tale  ipotesi  estrema  di  irreperibilità, l'assenza    &#13;
 dell'interdicendo non implica pregiudizio dei suoi  diritti  e  delle    &#13;
 sue ragioni nel giudizio di interdizione, soprattutto se si considera    &#13;
 che, in  tale  giudizio,  è  previsto  l'intervento  necessario  del    &#13;
 pubblico  ministero, finalizzato a garantire l'attuazione della legge    &#13;
 nel rispetto dell'interesse generale cui  è  ispirata  la  normativa    &#13;
 sull'interdizione;                                                       &#13;
      che   la   norma   impugnata,   nell'interpretazione  accolta  e    &#13;
 corrispondente al diritto  vivente,  non  lede  affatto  le  garanzie    &#13;
 contemplate nell'art. 24 Cost.;                                          &#13;
      che   per  le  suesposte  ragioni  la  questione  va  dichiarata    &#13;
 manifestamente infondata;                                                &#13;
    Visti  gli  artt.  26  legge  11  marzo 1953 n. 87 e 9 delle Norme    &#13;
 integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  419,  primo  comma,  c.c.  sollevata,   in    &#13;
 riferimento   all'art.   24   Cost.,  dal  Tribunale  di  Torino  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 marzo 1988.          &#13;
                    Il Presidente e redattore: SAJA                       &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 31 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
