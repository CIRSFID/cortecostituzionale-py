<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>16</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:16</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Antonino Papaldo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/03/1963</data_decisione>
    <data_deposito>16/03/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. GIOVANNI &#13;
 CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO &#13;
 SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ, &#13;
 Giudici,</collegio>
    <epigrafe>ha deliberato in camera di consiglio la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale del R.D.  24  settembre  &#13;
 1940,  n.  1949;  del  R.D.  24  settembre  1940, n. 1954; del D.L.L. 8  &#13;
 febbraio 1945, n. 75; del D.L.L. 2 aprile 1946, n. 142; del  D.L.C.P.S.  &#13;
 13 maggio 1947, n. 493; del D.L. 23 gennaio 1948, n. 59; della legge 22  &#13;
 novembre  1949,  n. 861; della legge 14 aprile 1956, n. 307, contenenti  &#13;
 norme per  l'accertamento,  la  determinazione  e  la  riscossione  dei  &#13;
 contributi  unificati  in  agricoltura; nonché di alcune circolari del  &#13;
 Ministro  per  il lavoro e la previdenza sociale, concernenti lo stesso  &#13;
 oggetto, promosso con ordinanza emessa il 19 agosto 1961 dal Pretore di  &#13;
 Torremaggiore nel procedimento civile vertente tra Antonucci Attilio  e  &#13;
 l'Esattore  comunale  di Torremaggiore, iscritta al n. 176 del Registro  &#13;
 ordinanze 1961 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 287 del 18 novembre 1961.                                             &#13;
     Udita  nella  camera di consiglio del 5 marzo 1963 la relazione del  &#13;
 Giudice Antonino Papaldo;                                                &#13;
     Ritenuto che  con  l'ordinanza  segnata  in  epigrafe,  emessa  dal  &#13;
 Pretore  di  Torremaggiore  dopo  che  la  causa  era stata trattata in  &#13;
 pubblica udienza ed assegnata a sentenza, è stata sollevata  questione  &#13;
 di  legittimità  costituzionale  dei  suindicati  atti aventi forza di  &#13;
 legge nonché di circolari e di decreti ministeriali, dei quali non  è  &#13;
 stata  denunziata specificamente alcuna disposizione, per contrasto con  &#13;
 gli articoli 2, 3 (in relazione  agli  artt.  70,  76  e  77),  41  (in  &#13;
 relazione  agli  artt.  42  e  44) e 76 della Costituzione, nonché con  &#13;
 l'art. 1920 del Codice civile e con l'art. 1 delle preleggi;             &#13;
     che nel giudizio di cui in epigrafe si è costituito il  Presidente  &#13;
 del  Consiglio  dei  Ministri  con deduzioni depositate il 16 settembre  &#13;
 1961  e  l'Esattore  delle  imposte  di  Torremaggiore  con   deduzioni  &#13;
 depositate il 25 settembre dello stesso anno;                            &#13;
     Considerato  che  con  sentenza  7  giugno 1962, n. 65, la Corte ha  &#13;
 dichiarato l'illegittimità costituzionale delle norme contenute  negli  &#13;
 artt.  4  e 5 del R.D.  24 settembre 1940, n. 1949, nonché dell'art. 5  &#13;
 del D.L. 23 gennaio 1948,  n.  59,  nella  parte  in  cui  consente  di  &#13;
 lasciare   sussistere   il  sistema  dell'accertamento  presuntivo;  ha  &#13;
 dichiarato non fondate le questioni aventi  per  oggetto  il  D.L.L.  2  &#13;
 aprile  1946, n. 142, il D.L.C.P.S. 13 maggio 1947, n.  493, e l'art. 1  &#13;
 del  D.L.  23  gennaio   1948,   n.   59,   previa   dichiarazione   di  &#13;
 inammissibilità,   per   la   loro  genericità,  delle  questioni  di  &#13;
 legittimità costituzionale  riflettenti  il  dedotto  contrasto  delle  &#13;
 leggi 22 novembre 1949, n. 861, e 14 aprile 1956, n. 307, del D.L.L.  8  &#13;
 febbraio  1945,  n.  75,  del  R.D.   29 settembre 1940, n. 1954, e del  &#13;
 D.P.R. 13 maggio 1957, n. 853, con  gli  artt.  2,  41,  42,  44  della  &#13;
 Costituzione;  ha  dichiarato  ugualmente  inammissibili  le  questioni  &#13;
 concernenti il contrasto delle disposizioni predette con  gli  artt.  1  &#13;
 delle  disposizioni  sulla  legge in generale e 1920 del Codice civile,  &#13;
 nonché le questioni aventi per oggetto le circolari ministeriali;       &#13;
     che nella ordinanza in esame permane la  stessa  genericità  delle  &#13;
 questioni  rilevata con la citata sentenza che, per questo, le dichiara  &#13;
 inammissibili e permane ugualmente la invocazione  di  un  giudizio  di  &#13;
 legittimità  costituzionale sopra norme che non hanno forza di legge o  &#13;
 in riferimento a norme che non hanno carattere  costituzionale,  mentre  &#13;
 le   sole  questioni  non  genericamente  prospettate  si  identificano  &#13;
 sostanzialmente con quelle già esaminate dalla Corte con la suindicata  &#13;
 sentenza, che in parte le accolse dichiarando l'inefficacia  di  alcune  &#13;
 norme  ed  in  parte  le  respinse  dichiarando non fondate le relative  &#13;
 questioni;                                                               &#13;
     che nella specie nessuna nuova apprezzabile ragione viene  addotta,  &#13;
 tale  da determinare la possibilità di un riesame delle questioni già  &#13;
 risolute con la precedente decisione;                                    &#13;
     Visti gli artt. 26, secondo comma, e 29 della legge 11 marzo  1953,  &#13;
 n. 87, e l'art. 9, secondo comma, delle Norme integrative per i giudizi  &#13;
 davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza delle questioni di legittimità  &#13;
 costituzionale delle norme relative ai  contributi  agricoli  unificati  &#13;
 indicate in epigrafe.                                                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 5 marzo 1963.           &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO - ANTONINO PAPALDO -  GIOVANNI  &#13;
                                   CASSANDRO   -   BIAGIO  PETROCELLI  -  &#13;
                                   ANTONIO  MANCA  -  ALDO  SANDULLI   -  &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI - GIUSEPPE  VERZÌ.</dispositivo>
  </pronuncia_testo>
</pronuncia>
