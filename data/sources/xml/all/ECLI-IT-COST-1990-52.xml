<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>52</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:52</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>31/01/1990</data_decisione>
    <data_deposito>02/02/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2, comma terzo,    &#13;
 della legge 6 ottobre 1986, n. 656 (Modifiche  ed  integrazioni  alla    &#13;
 normativa sulle pensioni di guerra), promosso con ordinanza emessa il    &#13;
 17 ottobre 1988 dalla Corte dei conti sul ricorso proposto da Gigante    &#13;
 Antonia  vedova  Petitti,  iscritta  al n. 326 del registro ordinanze    &#13;
 1989 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  27,    &#13;
 prima serie speciale, dell'anno 1989;                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 16 novembre 1989 il Giudice    &#13;
 relatore Vincenzo Caianiello;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel corso di un processo concernente il riconoscimento alla    &#13;
 riversibilità di una pensione privilegiata,  già  in  godimento  al    &#13;
 figlio  deceduto  della  ricorrente,  la  Corte  dei conti, dopo aver    &#13;
 escluso che sussistesse il requisito del limite di  reddito  previsto    &#13;
 dalle  norme  sulla riversibilità ordinaria (artt. 83 e 85 d.P.R. 29    &#13;
 dicembre 1973, n. 1092, e art. 24, comma  sesto,  legge  28  febbraio    &#13;
 1986,  n.  41),  ha  esaminato  il  ricorso  sotto il diverso profilo    &#13;
 dell'eventuale   spettanza   del    trattamento    privilegiato    di    &#13;
 riversibilità  che  l'art.  92 del d.P.R. 29 dicembre 1973, n. 1092,    &#13;
 riconosce ai congiunti "nella misura e alle condizioni previste dalle    &#13;
 disposizioni  in  materia  di pensioni di guerra", quando, come nella    &#13;
 fattispecie, la morte  sia  conseguenza  delle  stesse  infermità  o    &#13;
 lesioni per le quali è stata attribuita la pensione privilegiata.       &#13;
    Anche   in  relazione  a  tale  thema  decidendum,  la  ricorrente    &#13;
 risulterebbe però  sprovvista  del  necessario  requisito  economico    &#13;
 percependo,  al momento del decesso del figlio, una pensione vedovile    &#13;
 di ammontare imponibile superiore al limite previsto  in  materia  di    &#13;
 pensioni  di guerra dall'art. 12 del d.P.R. 30 dicembre 1981, n. 834.    &#13;
 Né potrebbe beneficiare dell'elevazione di tale limite che l'art.  2    &#13;
 della  legge  6 ottobre 1986 n. 656 ha disposto con decorrenza dal 1°    &#13;
 gennaio  1985:  a  tale  data,  infatti,  il  suo  reddito,   essendo    &#13;
 aumentato, risultava sempre ostativo al riconoscimento della pensione    &#13;
 di riversibilità.                                                       &#13;
    Poiché,  peraltro,  prima  dell'entrata in vigore della legge per    &#13;
 ultimo citata, l'art. 24, comma sesto, della legge 28 febbraio  1986,    &#13;
 n.  41,  ha  previsto per le pensioni ordinarie di riversibilità, un    &#13;
 meccanismo di calcolo e  di  adeguamento  automatico  del  limite  di    &#13;
 reddito,  la  Corte  dei conti ritiene rilevante e non manifestamente    &#13;
 infondata,  con  riferimento  all'art.  3  della   Costituzione,   la    &#13;
 questione  di  legittimità  costituzionale dell'art. 2, comma terzo,    &#13;
 della legge 6 ottobre 1986, n. 656, nella parte in  cui  non  prevede    &#13;
 che  il  limite  di reddito rilevante ai fini dell'attribuzione delle    &#13;
 pensioni  di  riversibilità  di  guerra  sia  calcolato,  a  partire    &#13;
 dall'anno   1986,   in   conformità   ai  criteri  previsti  per  la    &#13;
 riversibilità ordinaria dal predetto art.  24,  comma  sesto,  della    &#13;
 legge 28 febbraio 1986, n. 41.                                           &#13;
    Sostiene il giudice a quo che una volta individuato nell'ambito di    &#13;
 normative, per altri aspetti  diverse,  uno  stesso  criterio  (quale    &#13;
 quello  reddituale)  al fine di ammettere la sussistenza o meno della    &#13;
 condizione (stato di bisogno economico) richiesta in entrambi i  casi    &#13;
 per la prestazione pensionistica, sia poi del tutto irragionevole, ed    &#13;
 ingiustificatamente discriminatorio, quantificare in modo diverso  il    &#13;
 limite   reddituale  in  ragione  della  mera  diversità  di  regime    &#13;
 pensionistico.  Lo  stesso  requisito  della  condizione   economica,    &#13;
 previsto  per l'una e per l'altra specie di pensione, dovrebbe dunque    &#13;
 essere valutato ed accertato sulla  base  di  un  unico  ed  uniforme    &#13;
 criterio,  ciò  a  maggior  ragione  ove  si consideri che l'attuale    &#13;
 diversità  non  corrisponde  affatto  ad  una  linea   di   coerenza    &#13;
 legislativa:  negli  ultimi  anni,  infatti,  il limite di reddito in    &#13;
 materia di pensioni di guerra è stato prima pari, poi  superiore  ed    &#13;
 infine inferiore a quello previsto per le pensioni ordinarie.            &#13;
    2.  -  È intervenuta l'Avvocatura generale dello Stato escludendo    &#13;
 l'assimilabilità delle prestazioni pensionistiche poste a  raffronto    &#13;
 e,  conseguentemente,  l'irragionevolezza della diversità dei limiti    &#13;
 di reddito richiesti dalle due normative.                                &#13;
    Mentre  la  pensione  di  riversibilità ordinaria avrebbe infatti    &#13;
 natura retributiva  e  funzione  assistenziale,  essendo  diretta  ad    &#13;
 assicurare  al  superstite  la continuità del sostentamento che già    &#13;
 gravava sul defunto, la pensione  di  guerra  avrebbe  invece  natura    &#13;
 risarcitoria  e  la  sua  riversibilità risponderebbe ad esigenze di    &#13;
 ordine naturale ed etico, donde il  trattamento  peculiare  riservato    &#13;
 agli aventi causa del pensionato di guerra.<diritto>Considerato in diritto</diritto>1.   -  È  stata  sollevata,  in  riferimento  all'art.  3  della    &#13;
 Costituzione, questione di legittimità costituzionale  dell'art.  2,    &#13;
 comma  terzo,  della  legge  6 ottobre 1986, n. 656, il quale prevede    &#13;
 che, per le pensioni di guerra, "il limite di reddito,  nei  casi  in    &#13;
 cui  sia previsto come condizione per il conferimento dei trattamenti    &#13;
 od assegni pensionistici di guerra... è elevato a L.  7.500.000  con    &#13;
 decorrenza dal 1° gennaio 1985".                                         &#13;
    Si  sostiene  nell'ordinanza  di  rimessione  che,  in base a tale    &#13;
 disposizione  si  determinerebbe  una  ingiustificata  disparità  di    &#13;
 trattamento rispetto a quello più favorevole previsto per gli aventi    &#13;
 diritto a pensione ordinaria di riversibilità, per i quali il limite    &#13;
 di  reddito  è determinato dall'art. 24, comma sesto, della legge 28    &#13;
 febbraio  1986,  n.  41,  con  rinvio  a  quello  stabilito  per   la    &#13;
 concessione  delle pensioni agli invalidi civili totali, rivalutabile    &#13;
 annualmente secondo gli indici relativi ai lavoratori dell'industria,    &#13;
 rilevati dall'ISTAT agli effetti della scala mobile sui salari.          &#13;
    2. - La questione non è fondata.                                     &#13;
    Per  precisare  i  termini  della rilevanza di essa, è necessario    &#13;
 chiarire preliminarmente che il giudizio a quo,  come  risulta  dalla    &#13;
 stessa  ordinanza di rinvio, non riguarda una controversia in materia    &#13;
 di pensioni di guerra, bensì il diniego di pensione privilegiata  di    &#13;
 riversibilità  nei  confronti  della  madre di un pensionato statale    &#13;
 (già titolare di trattamento privilegiato di 1ª categoria)  deceduto    &#13;
 a causa delle infermità per le quali aveva conseguito il trattamento    &#13;
 privilegiato.                                                            &#13;
    La  norma  denunciata, contenuta nel testo unico sulle pensioni di    &#13;
 guerra, assume dunque rilevanza  nel  giudizio  a  quo  in  via  solo    &#13;
 indiretta,  per  effetto  del rinvio ad essa operata dall'art. 92 del    &#13;
 testo unico sulle pensioni civili (d.P.R. 29 dicembre 1973, n.  1092)    &#13;
 il  quale  stabilisce  che  quando la morte del dipendente statale è    &#13;
 conseguenza di infermità o lesioni dipendenti da fatti di  servizio,    &#13;
 "spetta  ai  congiunti  la pensione privilegiata nella misura ed alle    &#13;
 condizioni previste dalle disposizioni  in  materia  di  pensioni  di    &#13;
 guerra" e che il medesimo trattamento spetta anche nel caso in cui il    &#13;
 titolare di pensione privilegiata diretta o  di  assegno  rinnovabile    &#13;
 sia  deceduto  a  causa delle infermità o lesioni per le quali aveva    &#13;
 conseguito il trattamento privilegiato.                                  &#13;
    Così precisati i termini della questione, non appaiono pertinenti    &#13;
 gli aspetti posti in evidenza  nell'ordinanza  di  rimessione  e  che    &#13;
 sembrerebbero  voler invece investire, sotto il profilo del limite di    &#13;
 reddito richiesto per la concessione della  pensione,  la  situazione    &#13;
 della categoria degli aventi diritto a pensione di guerra.               &#13;
    Nell'ambito  della  questione,  precisata nei sensi anzidetti, non    &#13;
 appare però possibile porre a raffronto la disciplina concernente il    &#13;
 limite  di  reddito per la concessione della pensione privilegiata di    &#13;
 riversibilità con  la  corrispondente  disciplina  che  riguarda  la    &#13;
 pensione  ordinaria  di riversibilità, trattandosi di situazioni non    &#13;
 comparabili in quanto fra loro non omogenee.                             &#13;
    Difatti,  in  virtù  dell' art. 83 del testo unico delle pensioni    &#13;
 degli impiegati dello Stato, (d.P.R. n.  1092  del  1973  cit.)  alla    &#13;
 morte  del  dipendente  statale  o del pensionato, mancando gli altri    &#13;
 congiunti prioritariamente indicati in detto testo unico, la pensione    &#13;
 ordinaria  di  riversibilità  spetta  al  padre o, in mancanza, alla    &#13;
 madre "purché siano inabili a proficuo lavoro o in età superiore  a    &#13;
 sessanta  anni,  nonché nullatenenti e a carico del dipendente o del    &#13;
 pensionato".                                                             &#13;
    In  virtù  del rinvio operato dall'art. 92 del testo unico citato    &#13;
 alle "condizioni in  materia  di  pensioni  di  guerra"  la  pensione    &#13;
 privilegiata di riversibilità - cioè quella prevista, come nel caso    &#13;
 oggetto del giudizio a quo, quando la morte dell'impiegato statale  o    &#13;
 del pensionato dipenda da causa di servizio - in mancanza degli altri    &#13;
 congiunti indicati nell'art.  57  del  testo  unico  delle  norme  in    &#13;
 materia  di  pensioni  di  guerra  (d.P.R.  23 dicembre 1978, n. 915)    &#13;
 spetta invece, a titolo di assegno alimentare,  al  padre  che  abbia    &#13;
 raggiunto  l'età  di  anni  58,  o  sia comunque inabile a qualsiasi    &#13;
 proficuo  lavoro,  oppure,  alla  madre   vedova.   Relativamente   a    &#13;
 quest'ultima,  ai  fini della concessione della pensione privilegiata    &#13;
 di riversibilità, non è quindi richiesta alcuna altra condizione se    &#13;
 non  quella  del  limite  di reddito indicato nell'art. 70 del citato    &#13;
 testo unico, come in seguito modificato dalla disposizione sospettata    &#13;
 di incostituzionalità (art. 2, comma terzo, legge 1986, n. 656).        &#13;
    Come dunque risulta da detta normativa, mentre, mancando gli altri    &#13;
 congiunti, la pensione ordinaria di riversibilità spetta alla  madre    &#13;
 del  dipendente  statale  o del pensionato deceduti, solo se essa sia    &#13;
 inabile a proficuo lavoro o se  abbia  l'età  superiore  a  sessanta    &#13;
 anni,  sia  a  carico del dipendente o del pensionato deceduto, e sia    &#13;
 nullatenente (nel senso di  non  possedere  un  reddito  superiore  a    &#13;
 quello ora previsto dall'art. 24 della legge n. 41 del 1986, invocato    &#13;
 come   tertium   comparationis),   la   pensione   privilegiata    di    &#13;
 riversibilità  spetta,  invece,  in  mancanza degli altri congiunti,    &#13;
 alla madre superstite  che  si  trovi  nelle  condizioni  di  reddito    &#13;
 previste  dalla  norma  impugnata,  senza che sia richiesto né alcun    &#13;
 limite minimo di età, né il requisito della inabilità al lavoro.      &#13;
    Le  due  situazioni,  non  sono  perciò omogenee per cui non può    &#13;
 reputarsi irragionevole la diversità del limite di reddito  previsto    &#13;
 per  i  due tipi di pensione di riversibilità, una volta che, per la    &#13;
 concessione  della  pensione  privilegiata  di   riversibilità   nei    &#13;
 confronti  della madre, siano previste condizioni differenti rispetto    &#13;
 a quelle previste per la concessione, sempre nei confronti  di  detto    &#13;
 genitore, della pensione ordinaria di riversibilità.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 2,  comma  terzo,  della  legge  6  ottobre  1986,  n.  656    &#13;
 (Modifiche  ed  integrazioni alla normativa sulle pensioni di guerra)    &#13;
 sollevata, in riferimento all'art. 3 della Costituzione, dalla  Corte    &#13;
 dei conti, con l'ordinanza indicata in epigrafe.                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 31 gennaio 1990.                              &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 febbraio 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
