<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>425</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:425</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/11/1993</data_decisione>
    <data_deposito>03/12/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, &#13;
 avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio promosso con ricorso della Regione Umbria notificato il    &#13;
 20 maggio 1993, depositato in  Cancelleria  il  3  giugno  1993,  per    &#13;
 conflitto  di  attribuzione  sorto a seguito della circolare 11 marzo    &#13;
 1993, n.  D/349  del  Ministero  dell'Agricoltura  e  delle  Foreste,    &#13;
 contenente:  "Disposizioni  applicative  della  normativa comunitaria    &#13;
 concernente il regime di sostegno a favore dei coltivatori di  taluni    &#13;
 seminativi", ed iscritto al n. 18 del registro conflitti 1993;           &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza  pubblica  del  19  ottobre  1993  il  Giudice    &#13;
 relatore Francesco Guizzi;                                               &#13;
    Udito  l'Avvocato  dello  Stato Ivo M. Braguglia per il Presidente    &#13;
 del Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - La Regione Umbria ha sollevato conflitto di attribuzione  nei    &#13;
 confronti  dello  Stato,  per  violazione degli artt. 117 e 118 della    &#13;
 Costituzione (in relazione agli artt. 71, lett. b), e 77,  lett.  b),    &#13;
 del  d.P.R.  24 luglio 1977, n. 616), nonché dell'art. 14, secondo e    &#13;
 terzo  comma,  della  Costituzione,  chiedendo  l'annullamento  della    &#13;
 circolare  del  Ministero  dell'agricoltura  e delle foreste 11 marzo    &#13;
 1993, n. D/349, contenente disposizioni applicative  della  normativa    &#13;
 comunitaria a favore dei coltivatori di alcuni seminativi.               &#13;
    La  ricorrente osserva che con il regolamento del Consiglio CEE n.    &#13;
 1765/92 è mutato il regime di aiuti ai coltivatori di cereali,  semi    &#13;
 oleosi  e piante proteiche, e il principio del sostegno ai redditi è    &#13;
 subentrato a quello del sostegno alla produzione. L'azienda agricola,    &#13;
 per  ottenere  quanto  previsto  dal  regolamento,  deve   presentare    &#13;
 all'Azienda  di  Stato per gli interventi nel mercato agricolo (AIMA)    &#13;
 una domanda dove sono riportati i dati necessari per  l'applicazione,    &#13;
 in  sede  di  liquidazione,  dei  criteri  previsti  dalla  normativa    &#13;
 comunitaria.                                                             &#13;
    La circolare dispone che tale domanda sia  trasmessa  all'AIMA  in    &#13;
 duplice  copia:  l'Ente,  quindi,  inoltrerà  una copia alla regione    &#13;
 presso cui l'azienda  agricola  richiedente  ha  sede  "al  fine  del    &#13;
 successivo   espletamento   dei   controlli",   che   consistono   in    &#13;
 sopralluoghi aziendali programmati dall'amministrazione centrale.  In    &#13;
 tal  modo,  si  conferiscono alla regione - argomenta la ricorrente -    &#13;
 funzioni di controllo che non le competono. L'art. 71 del  d.P.R.  n.    &#13;
 616  del  1977  riserva infatti allo Stato le funzioni amministrative    &#13;
 concernenti gli interventi di interesse nazionale per la  regolazione    &#13;
 del  mercato  agricolo;  l'art.  77  dello stesso decreto delega alle    &#13;
 regioni  l'esercizio  delle   funzioni   amministrative   concernenti    &#13;
 l'attuazione   degli  interventi  di  regolazione  del  mercato,  con    &#13;
 eccezione, però, di quelli che sono riservati all'AIMA. La legge  14    &#13;
 agosto  1982,  n.  610  (Riordinamento  dell'Azienda di Stato per gli    &#13;
 interventi nel mercato agricolo - AIMA), stabilisce infine,  all'art.    &#13;
 3,   lett.   e),  che  l'AIMA  curi  l'erogazione  delle  provvidenze    &#13;
 finanziarie, avvalendosi della collaborazione delle  regioni  con  le    &#13;
 quali essa può stipulare convenzioni.                                   &#13;
    Da  tale  disciplina  -  conclude  la  Regione Umbria - emerge con    &#13;
 chiarezza come lo Stato si sia  riservato,  ai  sensi  dell'art.  117    &#13;
 della  Costituzione,  le  funzioni  amministrative di regolazione dei    &#13;
 mercati agricoli e la delega di quelle concernenti l'attuazione degli    &#13;
 interventi, eccettuate le funzioni riservate all'AIMA, mentre la cura    &#13;
 delle erogazioni finanziarie, tra le quali vanno ricomprese quelle in    &#13;
 esame, è compito dell'AIMA.                                             &#13;
    L'attribuzione  delle  competenze  di  controllo   alle   regioni,    &#13;
 disposta  con mera circolare, viola dunque il riparto delineato dagli    &#13;
 artt. 117 e 118 della Costituzione e concretizzato dal d.P.R. n.  616    &#13;
 del 1977.                                                                &#13;
    Vi   sarebbe   altresì,  secondo  la  ricorrente,  la  violazione    &#13;
 dell'art. 14, secondo e terzo comma, della Costituzione,  poiché  la    &#13;
 circolare  impone  alla  Regione  il  compimento di atti che ricadono    &#13;
 sotto la riserva di legge ivi espressamente prevista.                    &#13;
    2. - Si è costituito il Presidente del  Consiglio  dei  ministri,    &#13;
 rappresentato   e   difeso   dall'Avvocatura  generale  dello  Stato,    &#13;
 concludendo per il rigetto del ricorso.                                  &#13;
    In   una   memoria   presentata    nell'imminenza    dell'udienza,    &#13;
 l'Avvocatura generale riconosce la competenza dell'AIMA in materia di    &#13;
 controlli,  ai  sensi  di quanto previsto dall'art. 3, lett. e) della    &#13;
 legge n. 610 del 1982 e dall'art. 77, lett. b) del d.P.R. n. 616  del    &#13;
 1977.  Il  sospetto  avanzato dalla Regione Umbria - che la circolare    &#13;
 impugnata abbia attribuito  alle  regioni  funzioni  di  controllo  e    &#13;
 poteri  di  sopralluogo  -  sarebbe  però  privo  di  fondamento. Il    &#13;
 riferimento,  contenuto  nella  circolare,  alla  trasmissione  della    &#13;
 domanda  all'assessorato  regionale competente al fine del successivo    &#13;
 espletamento dei controlli aziendali, ha soltanto il  valore  di  una    &#13;
 "norma di chiusura", che consente all'AIMA di stipulare, se del caso,    &#13;
 una  convenzione  con  le  regioni  e  che fa salvo, in via del tutto    &#13;
 ipotetica, un autonomo potere di controllo previsto  dalla  normativa    &#13;
 regionale.                                                               &#13;
    La  circolare,  nel  titolo  II,  lett. b), richiama d'altronde il    &#13;
 sistema integrato di controllo previsto dai  regolamenti  comunitari,    &#13;
 che presuppone l'utilizzo di metodologie accentrate in seno all'AIMA:    &#13;
 il  sistema  di  controllo  prescelto è dunque diverso da quello che    &#13;
 potrebbero  esercitare  le  regioni  mediante  semplici  sopralluoghi    &#13;
 aziendali.                                                               &#13;
    Ulteriore  conferma  del  fatto  che  gli adempimenti siano curati    &#13;
 dall'AIMA si trae dal modello di verbale di controllo  (allegato  III    &#13;
 alla  circolare) dove la denominazione dell'organismo di controllo è    &#13;
 lasciata in bianco. Il che  non  sarebbe  avvenuto  se  la  circolare    &#13;
 avesse inteso affidarlo alle regioni.                                    &#13;
    Osserva  infine  l'Avvocatura che la circolare non prevede neppure    &#13;
 ipotesi di "avvalimento di uffici regionali"  per  l'espletamento  di    &#13;
 tale attività, di modo che il ricorso andrebbe rigettato nella parte    &#13;
 in cui chiede l'annullamento della circolare impugnata.                  &#13;
   3.  -  Ha  presentato  memoria anche la Regione Umbria, richiamando    &#13;
 quanto già esposto nel ricorso e precisando che il  decreto-legge  4    &#13;
 agosto  1993,  n. 272, che riordina le competenze regionali e statali    &#13;
 in materia agricola e forestale, non abroga gli  articoli  71,  lett.    &#13;
 b),  e  77,  lett.  b), del d.P.R. n. 616 del 1977, sì che il quadro    &#13;
 normativo sul riparto delle funzioni  tra  Stato  e  Regioni  sarebbe    &#13;
 identico,  su  questo  punto,  a  quello  vigente  al  momento  della    &#13;
 proposizione del ricorso.<diritto>Considerato in diritto</diritto>1.  - La Regione Umbria ha sollevato conflitto di attribuzione nei    &#13;
 confronti dello Stato,  ritenendo  che  la  circolare  del  Ministero    &#13;
 dell'agricoltura  e delle foreste 11 marzo 1993, n. D/349, contenente    &#13;
 disposizioni  per  l'applicazione  della  normativa  comunitaria  sul    &#13;
 sostegno  a  favore  dei  coltivatori di alcuni seminativi, alteri il    &#13;
 riparto di  attribuzioni  tra  lo  Stato  e  le  Regioni,  conferendo    &#13;
 arbitrariamente alla Regione poteri di controllo in materia riservata    &#13;
 allo  Stato,  e in particolare all'AIMA, in violazione degli articoli    &#13;
 117 e 118 della Costituzione, e degli articoli 71 e 77 del d.P.R.  24    &#13;
 luglio  1977,  n.  616.  Essa  lamenta altresì la illegittimità per    &#13;
 violazione della riserva di legge introdotta dall'art. 14, secondo  e    &#13;
 terzo comma, della Costituzione.                                         &#13;
    2. - Il ricorso va respinto, secondo quanto verrà ora precisato.     &#13;
    Il  conflitto  trae  origine  da quel passaggio della circolare in    &#13;
 base al quale la domanda presentata dal produttore per  ottenere  gli    &#13;
 aiuti  comunitari  viene  trasmessa  in copia dall'AIMA al competente    &#13;
 assessorato  regionale  dell'agricoltura,  al  fine  del   successivo    &#13;
 espletamento dei controlli aziendali.                                    &#13;
    Ora,  non  è  immaginabile  che  con  mera circolare possa essere    &#13;
 modificato il riparto di attribuzioni fra lo Stato e le Regioni, come    &#13;
 delineato dagli articoli 71 e 77 del citato d.P.R. 616  del  1977,  e    &#13;
 l'ambito  delle  competenze  dell'AIMA,  quale risulta dalla legge 14    &#13;
 agosto 1982, n. 610, e in particolare  dall'art.  3,  lett.  e),  che    &#13;
 affida  all'Azienda  di Stato il compito di provvedere all'erogazione    &#13;
 degli  aiuti  e  delle   compensazioni   finanziarie   disposte   dai    &#13;
 regolamenti  comunitari.  Proprio il citato art. 3 della legge n. 610    &#13;
 del 1982 prevede che per tale attività l'AIMA possa avvalersi  della    &#13;
 collaborazione   delle   regioni,   stipulando   con   esse  apposite    &#13;
 convenzioni. E in vista di siffatta  eventualità,  la  circolare  ha    &#13;
 disposto  l'invio della copia della domanda ai competenti assessorati    &#13;
 regionali: in funzione, dunque, di doverosa informazione, e non certo    &#13;
 al fine di attribuire loro nuovi compiti, oltretutto indeterminati.      &#13;
    Così correttamente interpretata, la  circolare  si  sottrae  alle    &#13;
 censure  mosse  dalla  Regione  Umbria,  il  cui  ricorso  va  dunque    &#13;
 respinto.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  che  spetta  allo  Stato,  e  per   esso   al   Ministero    &#13;
 dell'agricoltura   e   delle   foreste,   dettare,   con   circolare,    &#13;
 prescrizioni   sull'applicazione    della    normativa    comunitaria    &#13;
 concernente   il   sostegno   a  favore  dei  coltivatori  di  taluni    &#13;
 seminativi, e, in  particolare,  disporre  che  copia  della  domanda    &#13;
 presentata dal produttore all'Azienda di Stato per gli interventi nel    &#13;
 mercato  agricolo  (AIMA)  sia  trasmessa  al  competente assessorato    &#13;
 regionale, a fini di informazione.                                       &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 novembre 1993.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 3 dicembre 1993.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
