<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>472</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:472</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MARINI</presidente>
    <relatore_pronuncia>Paolo Maddalena</relatore_pronuncia>
    <redattore_pronuncia>Paolo Maddalena</redattore_pronuncia>
    <data_decisione>14/12/2005</data_decisione>
    <data_deposito>28/12/2005</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Annibale MARINI; Giudici: Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso con ordinanza dell'11 ottobre 2004 dal Giudice di pace di Palermo, nel procedimento civile promosso da Vincenzo De Lisi contro la Polizia stradale di Palermo, iscritta al n. 219 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2005. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 16 novembre 2005 il Giudice relatore Paolo Maddalena. &#13;
    Ritenuto che con ordinanza dell'11 ottobre 2004, emessa nel corso di un giudizio definito di “opposizione” promosso nei confronti della Polizia stradale di Palermo, il Giudice di pace di Palermo, su eccezione della parte ricorrente, ha sollevato, in riferimento all'art. 42 della Costituzione, questione di legittimità costituzionale dell'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), nella parte in cui prevede la sanzione accessoria del fermo amministrativo del veicolo per un periodo più lungo di quello necessario per la conferma di validità del documento di guida; &#13;
    che il remittente osserva che la norma denunciata impedisce l'uso del veicolo, per evitare il ripetersi della violazione, anche quando è cessata l'irregolarità ed è venuto meno il pericolo del reiterarsi della violazione, e quindi non sussiste alcuna giustificazione alla imposta limitazione del diritto di proprietà; &#13;
    che, quanto alla rilevanza della questione, il Giudice di pace ne motiva la sussistenza rilevando che «la soluzione della sollevata questione di legittimità costituzionale condiziona l'adozione di una qualsiasi decisione del ricorso n. 3052/03 di che trattasi»; &#13;
    che nel giudizio dinanzi alla Corte costituzionale è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per l'inammissibilità e comunque per la non fondatezza della questione; &#13;
    che la questione sarebbe inammissibile sia perché il giudice avrebbe omesso di descrivere la fattispecie sottoposta al suo esame, sia perché non avrebbe tenuto conto che la norma censurata non era più in vigore già all'epoca dell'ordinanza di remissione, essendo stata modificata dall'art. 2 del decreto-legge 27 giugno 2003, n. 151 (Modifiche ed integrazioni al codice della strada), nel testo risultante dalla legge di conversione 1° agosto 2003, n. 214; &#13;
    che nel merito, comunque, la questione sarebbe infondata, perché la determinazione delle condotte punibili e delle relative sanzioni, siano esse penali o amministrative, rientra nella più ampia discrezionalità legislativa, non spettando alla Corte rimodulare le scelte punitive del legislatore né stabilire la quantificazione delle sanzioni; &#13;
    che l'Avvocatura ricorda la giurisprudenza di questa Corte (ordinanze n. 33 e n. 278 del 2001, n. 136 e n. 319 del 2002), secondo cui la sanzione accessoria del fermo amministrativo del veicolo condotto da persona la cui patente di guida sia scaduta, anche nel caso in cui lo stesso appartenga a persona diversa dall'autore della violazione – esclusa l'ipotesi che la circolazione sia avvenuta contro la volontà del proprietario –, non risulta essere né sproporzionata né irragionevole, essendo coerente con la finalità, perseguita in generale dal sistema sanzionatorio del codice della strada, di dare una risposta effettiva ed immediata a condotte potenzialmente pericolose. &#13;
    Considerato che la questione di legittimità costituzionale, sollevata in riferimento all'art. 42 della Costituzione, investe l'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), nella parte in cui prevede la sanzione accessoria del fermo amministrativo del veicolo per un periodo più lungo di quello necessario per la conferma di validità del documento di guida; &#13;
    che il giudice a quo ha omesso del tutto di descrivere la fattispecie concreta devoluta alla sua cognizione; &#13;
    che, secondo la costante giurisprudenza di questa Corte, allorché sia carente o manchi la descrizione della fattispecie oggetto del giudizio a quo, e dunque quando dall'ordinanza di remissione non si comprenda con chiarezza quale sia l'oggetto di tale giudizio (evenienze che sussistono, entrambe, nel caso in esame), vi è l'impossibilità di vagliare l'effettiva applicabilità della norma censurata al caso dedotto (ordinanze n. 174 del 2004 e n. 396 del 2005); &#13;
    che, pertanto, la questione sollevata va dichiarata manifestamente inammissibile per difetto di motivazione sulla rilevanza. &#13;
    Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata, in riferimento all'art. 42 della Costituzione, dal Giudice di pace di Palermo con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 dicembre 2005.  &#13;
F.to:  &#13;
Annibale MARINI, Presidente  &#13;
Paolo MADDALENA, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 28 dicembre 2005.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
