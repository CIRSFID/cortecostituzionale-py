<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-02-04/22/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-02-04/22"/>
          <FRBRalias value="ECLI:IT:COST:1982:22" name="ECLI"/>
          <FRBRdate date="1982-02-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="22"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-02-04/22/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-02-04/22/ita@"/>
          <FRBRdate date="1982-02-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-02-04/22/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-02-04/22/ita@.xml"/>
          <FRBRdate date="1982-02-04" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1982-01-19" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1982-02-04" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="ELIA" href="/akn/ontology/person/it/ELIA" showAs="ELIA"/>
        <TLCPerson eId="leopoldoElia" href="/akn/ontology/person/it/leopoldoElia" showAs="LEOPOLDO ELIA"/>
        <TLCPerson eId="edoardoVolterra" href="/akn/ontology/person/it/edoardoVolterra" showAs="EDOARDO VOLTERRA"/>
        <TLCPerson eId="micheleRossano" href="/akn/ontology/person/it/micheleRossano" showAs="MICHELE ROSSANO"/>
        <TLCPerson eId="antoninoDeStefano" href="/akn/ontology/person/it/antoninoDeStefano" showAs="ANTONINO DE STEFANO"/>
        <TLCPerson eId="oronzoReale" href="/akn/ontology/person/it/oronzoReale" showAs="ORONZO REALE"/>
        <TLCPerson eId="albertoMalagugini" href="/akn/ontology/person/it/albertoMalagugini" showAs="ALBERTO MALAGUGINI"/>
        <TLCPerson eId="livioPaladin" href="/akn/ontology/person/it/livioPaladin" showAs="LIVIO PALADIN"/>
        <TLCPerson eId="arnaldoMaccarone" href="/akn/ontology/person/it/arnaldoMaccarone" showAs="ARNALDO MACCARONE"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="VIRGILIO ANDRIOLI"/>
        <TLCPerson eId="giuseppeFerrari" href="/akn/ontology/person/it/giuseppeFerrari" showAs="GIUSEPPE FERRARI"/>
        <TLCPerson eId="francescoSaja" href="/akn/ontology/person/it/francescoSaja" showAs="FRANCESCO SAJA"/>
        <TLCPerson eId="giovanniVitale" href="/akn/ontology/person/it/giovanniVitale" showAs="GIOVANNI VITALE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1982</akn4cc:anno>
        <akn4cc:numero>22</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1982:22</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>ELIA</akn4cc:presidente>
        <akn4cc:relatore>Michele Rossano</akn4cc:relatore>
        <akn4cc:data_decisione>1982-01-19</akn4cc:data_decisione>
        <akn4cc:data_deposito>1982-02-04</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge>, Presidente - Prof. <judge refersTo="#edoardoVolterra">EDOARDO VOLTERRA</judge> - Dott. <judge refersTo="#micheleRossano">MICHELE ROSSANO</judge> - Prof. <judge refersTo="#antoninoDeStefano">ANTONINO DE STEFANO</judge> - 
 Avv. <judge refersTo="#oronzoReale">ORONZO REALE</judge> - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. <judge refersTo="#albertoMalagugini">ALBERTO 
 MALAGUGINI</judge> - Prof. <judge refersTo="#livioPaladin">LIVIO PALADIN</judge> - Dott. <judge refersTo="#arnaldoMaccarone">ARNALDO MACCARONE</judge> - Prof. <judge refersTo="#virgilioAndrioli">VIRGILIO ANDRIOLI</judge> - Prof. <judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge> - Dott. <judge refersTo="#francescoSaja">FRANCESCO SAJA</judge>, 
 Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di  legittimità  costituzionale  dell'art.  1,  comma  
 secondo, della legge 10 novembre 1970. n. 869 (Disposizioni concernenti  
 il  personale  dell'Amministrazione  autonoma  dei  monopoli di Stato),  
 promosso con  ordinanza  emessa  il  10  novembre  1975  dal  Tribunale  
 Amministrativo  regionale  del  Lazio  sul ricorso proposto da De Rossi  
 Corrado ed altri contro il Ministero delle finanze e  l'Amministrazione  
 autonoma  dei  monopoli  di  Stato,  iscritta  al  n.  378 del registro  
 ordinanze 1976 e pubblicata nella Gazzetta Ufficiale  della  Repubblica  
 n. 164 del 23 giugno 1976.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti  gli  atti  di  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> di Zurlo Alfredo e del Ministero  
 delle finanze e l'atto di intervento del Presidente del  Consiglio  dei  
 ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito  nell'udienza  pubblica  del  24  novembre  1981  il  Giudice  
 relatore Michele Rossano;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>uditi l'avvocato Michele Costa per Zurlo Alfredo e l'avvocato dello  
 Stato Giorgio  Azzariti  per  il  Ministero  delle  finanze  e  per  il  
 Presidente del Consiglio dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto:</heading>
          <paragraph>
            <list>
              <intro>
                <p>Con  ricorso  10  maggio 1974 al Tribunale Amministrativo Regionale  
 per   il   Lazio   De   Rossi   Corrado   ed   altri   89    dipendenti  
 dell'Amministrazione   Autonoma  dei  Monopoli  di  Stato,  addetti  al  
 Deposito generi di monopoli  ed  alla  Manifattura  Tabacchi  di  Roma,  
 affermarono:  che  essi  prestavano  sette  ore di servizio giornaliero  
 continuativo,  mentre  avevano  l'obbligo  di   orario   di   sei   ore  
 giornaliere,  a  norma  del  <ref href="/akn/it/judgment/decreto/corteCostituzionale/1939-09-07/1/!main">decreto  17  settembre  1939</ref>  del Capo del  
 Governo,  concernente   tutti   gli   altri   uffici   della   medesima  
 Amministrazione  situati  in  Roma;  che  avevano più volte chiesto di  
 effettuare l'orario di sei ore o, quanto meno, di ricevere il  compenso  
 per  lavoro  straordinario  relativamente  alla  settima  ora,  ma  che  
 l'Amministrazione non aveva né ridotto l'orario,  né  corrisposto  il  
 compenso   richiesto.      Chiesero,   quindi,  al  suddetto  Tribunale  
 Amministrativo:</p>
              </intro>
              <point>
                <num>a)</num>
                <content>
                  <p> di dichiarare che essi avevano l'obbligo di  osservare  l'orario  
 di sei ore giornaliere;</p>
                </content>
              </point>
              <point>
                <num>b)</num>
                <content>
                  <p>  di  condannare  l'Amministrazione al pagamento del compenso per  
 lavoro straordinario per la settima ora giornaliera di servizio;</p>
                </content>
              </point>
              <point>
                <num>c)</num>
                <content>
                  <p> di condannare l'Amministrazione  al  pagamento  degli  interessi  
 corrispettivi, rivalutando il credito a norma dell'<ref href="/akn/it/act/regioDecreto/stato/1940-10-28/1443/!main#art_429">articolo 429 c.p.c.</ref></p>
                </content>
              </point>
            </list>
          </paragraph>
          <paragraph>
            <content>
              <p>Nello  stesso  ricorso  gli  istanti  affermarono  che - qualora si  
 ritenessero operante, per gli uffici di Roma  dell'Amministrazione  dei  
 Monopoli di Stato, la <ref href="/akn/it/act/legge/stato/1970-11-10/869/!main">legge 10 novembre 1970, n. 869</ref>, che fissava in 41  
 e  40  ore  settimanali la durata del lavoro del personale della stessa  
 Amministrazione - tale legge sarebbe in contrasto con l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.   3  della  
 Costituzione</ref> in quanto determinava una discriminazione non giustificata  
 tra uffici della stessa Amministrazione aventi tutti sede in Roma.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nella  memoria  25  ottobre  1975  i  ricorrenti affermarono che la  
 menzionata <ref href="/akn/it/act/legge/stato/1970/869/!main">legge n. 869 del 1970</ref> era in contrasto anche con  l'art.  36  
 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Con   ordinanza   10  novembre  1975  il  Tribunale  Amministrativo  
 Regionale del Lazio ritenne rilevante ai  fini  della  decisione  della  
 controversia e non manifestamente infondata la questione, sollevata dai  
 ricorrenti,  concernente  la  legittimità  costituzionale dell'art. 1,  
 comma secondo, <ref href="/akn/it/act/legge/stato/1970-11-10/869/!main">legge 10 novembre 1970,  n.  869</ref>,  in  riferimento  agli  
 <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36 della Costituzione</ref></mref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n. 164 del  
 23 giugno 1976.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nel giudizio davanti a questa Corte si è costituito Zurlo Alfredo,  
 con  atto  depositato il 12 luglio 1976, chiedendo che venga dichiarato  
 "illegittimo il secondo comma dell'art.  1 legge 10 novembre  1970,  n.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>869</num>
            <content>
              <p>,  nella  parte  in cui stabilisce un diverso orario di servizio del  
 personale impiegatizio degli uffici nei confronti di tutto il personale  
 dipendente  dalla  Amministrazione  Autonoma  dei  Monopoli  di   Stato  
 prestante  servizio  presso la Capitale, in relazione agli artt. 3 e 36  
 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Sono intervenuti il Presidente del Consiglio  dei  ministri  ed  il  
 Ministro  delle  Finanze, rappresentati e difesi dall'avvocato generale  
 dello Stato, con atti depositati il 13 luglio 1976,  chiedendo  che  la  
 questione di legittimità costituzionale sia dichiarata inammissibile o  
 infondata.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto:</heading>
          <paragraph>
            <content>
              <p>L'<ref href="/akn/it/act/legge/stato/1970-11-10/869/!main#art_1">art.  1  legge 10 novembre 1970, n. 869</ref> (disposizioni concernenti  
 il personale  dell'Amministrazione  Autonoma  dei  Monopoli  di  Stato)  
 prescrive:</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>"La   durata   settimanale   del  lavoro  ordinario  del  personale  
 impiegatizio ed operaio dell'Amministrazione autonoma dei  monopoli  di  
 Stato  è stabilita in 41 ore a partire dal 1 gennaio 1970 ed in 40 ore  
 a partire dal 1 gennaio 1971".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>"Per il personale  impiegatizio  degli  uffici  aventi  sede  nella  
 capitale  la durata della settimana lavorativa non può essere comunque  
 superiore   a   quella   stabilita   per    il    restante    personale  
 dell'Amministrazione".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Secondo  il  Tribunale Amministrativo Regionale per il Lazio l'art.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>1, comma secondo, <ref href="/akn/it/act/legge/stato/1970/869/!main">legge n. 869 del  1970</ref>,  sopra  trascritto,  -  nella  
 parte    che    stabilisce,    per    il    personale    degli   uffici  
 dell'Amministrazione Autonoma dei Monopoli di Stato, aventi sede  nella  
 Capitale,  un  orario  di servizio diverso da quello di tutti gli altri  
 impiegati della stessa  Amministrazione  che  prestano  servizio  negli  
 opifici  e  stabilimenti  situati anch'essi nella Capitale - sarebbe in  
 contrasto con gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36 della Costituzione</ref></mref>.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La   norma  impugnata  determinerebbe  infatti  una  disparità  di  
 trattamento, sul piano  giuridico  e  sul  piano  economico,  priva  di  
 razionale  giustificazione,  tra  impiegati  della  Amministrazione dei  
 Monopoli di Stato: quelli addetti ad uffici con sede  in  Roma,  per  i  
 quali  la durata del lavoro è di 36 ore settimanali, e tutti gli altri  
 impiegati, addetti ad uffici con sede  fuori  Roma  e  agli  opifici  e  
 stabilimenti  anche  se aventi sede in Roma, per i quali il primo comma  
 dello stesso <ref href="/akn/it/act/legge/stato/1970/869/!main#art_1">art. 1 legge n. 869 del 1970</ref> prevede la durata settimanale  
 del lavoro di 41 dal 1 gennaio 1970 e di 40 ore  dal  1  gennaio  1971,  
 ferma  restando la parità di retribuzione. E proprio perché tutti gli  
 impiegati  hanno  eguale  retribuzione,  nonostante  la  diversità  di  
 orario, sarebbe violato l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">art. 36 della Costituzione</ref>, che garantisce il  
 diritto  del  lavoratore  ad  una  retribuzione  commisurata anche alla  
 quantità del lavoro prestato.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>L'Avvocato  Generale  dello  Stato  ha   preliminarmente   eccepito  
 l'inammissibilità  della  questione  perché concerne il comma secondo  
 dell'<ref href="/akn/it/act/legge/stato/1970/869/!main#art_1">art. 1 legge n. 869 del 1970</ref>, che contiene norma derogatoria  alla  
 regola  generale  sulla  durata  settimanale  del  lavoro del personale  
 dell'Amministrazione dei Monopoli di Stato stabilita  dal  comma  primo  
 dello stesso articolo. Ma l'eccezione dev'esser disattesa, in quanto il  
 rilievo dell'Avvocatura si presta, se mai, ad incidere sul merito della  
 proposta impugnativa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Va   invece   considerata  un'altra  ragione  di  inammissibilità,  
 concernente la natura della disparità di  trattamento  denunciata  dal  
 giudice  a  quo.  In  effetti,  l'ordinanza  di  rimessione  ricorda ma  
 trascura espressamente di  affrontare  "i  problemi  insorti  circa  la  
 vigenza  attuale ed il valore della speciale normativa circa l'adozione  
 dell'orario continuato negli uffici statali e degli enti pubblici della  
 Capitale (per sei ore nei  giorni  feriali  e  per  quattro  ore  nelle  
 domeniche  e  negli altri giorni festivi) già prevista dal decreto del  
 Capo  del  governo  17  settembre  1939".  A  questo  proposito,  anzi,  
 l'ordinanza avverte che l'adunanza plenaria del Consiglio di Stato, con  
 la  decisione  n.  8  del  1  luglio  1973  (rectius 1 luglio 1975), ha  
 ritenuto  l'abrogazione  di  tale  disciplina,  "sul   riflesso   della  
 affermata sopravvenienza del termine di temporanea validità del citato  
 decreto".  Ma  alla  conclusione  del  Consiglio  di  Stato  (del resto  
 ribadita più  volte  nella  successiva  giurisprudenza  del  Consiglio  
 medesimo)  il  TAR  per  il  Lazio  si limita ad opporre una cosiddetta  
 "prassi interpretativa", in  forza  della  quale  gli  uffici  predetti  
 farebbero  pur  sempre  "costante applicazione" dell'orario a suo tempo  
 fissato dal <ref href="/akn/it/judgment/decreto/corteCostituzionale/1939-09-07/1/!main">decreto 17 settembre 1939</ref> del Capo del governo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Con ciò, tuttavia, quella denunciata dal giudice a quo si  risolve  
 -  secondo  la  stessa  prospettazione dell'ordinanza in esame - in una  
 disuguaglianza di mero fatto, che solo per effetto di un  salto  logico  
 potrebbe  venire  ricondotta  alla  disposizione impugnata (ovvero alle  
 norme cui essa fa rinvio, quale punto di riferimento  dell'orario  "per  
 il personale impiegatizio degli uffici aventi sede nella Capitale").</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per   definizione,  dunque,  la  sollevata  questione  si  dimostra  
 insuscettibile di essere risolta dalla Corte, nell'ambito del sindacato  
 sulla legittimità costituzionale delle leggi.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara  inammissibile la questione di legittimità costituzionale  
 dell'art. 1 comma secondo, <ref href="/akn/it/act/legge/stato/1970-11-10/869/!main">legge 10 novembre 1970, n. 869</ref> (disposizioni  
 concernenti il personale dell'Amministrazione Autonoma dei Monopoli  di  
 Stato)  proposta  dal  Tribunale  Amministrativo Regionale del Lazio in  
 riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_36">36 della Costituzione</ref></mref>,  con  l'ordinanza  in  
 epigrafe indicata.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1982-01-19">19 gennaio 1982</date>.</block>
      <p>F.to:   <signature><judge refersTo="#leopoldoElia">LEOPOLDO   ELIA</judge></signature>   -   <signature><judge refersTo="#edoardoVolterra">EDOARDO  
                                   VOLTERRA</judge></signature> - <signature><judge refersTo="#micheleRossano">MICHELE ROSSANO</judge></signature> - <signature><judge refersTo="#antoninoDeStefano">ANTONINO  
                                   DE  STEFANO</judge></signature> - <signature><judge refersTo="#oronzoReale">ORONZO REALE</judge></signature> - BRUNETTO  
                                   BUCCIARELLI    DUCCI    -     <signature><judge refersTo="#albertoMalagugini">ALBERTO  
                                   MALAGUGINI</judge></signature>  - <signature><judge refersTo="#livioPaladin">LIVIO PALADIN</judge></signature> - <signature><judge refersTo="#arnaldoMaccarone">ARNALDO  
                                   MACCARONE</judge></signature>   -    <signature><judge refersTo="#virgilioAndrioli">VIRGILIO    ANDRIOLI</judge></signature>  
                                   <signature><judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge></signature> - <signature><judge refersTo="#francescoSaja">FRANCESCO SAJA</judge></signature>.</p>
      <p>
        <signature><person refersTo="#giovanniVitale" as="#cancelliere">GIOVANNI VITALE</person> - <role refersTo="#cancelliere">Cancelliere</role></signature>
      </p>
    </conclusions>
  </judgment>
</akomaNtoso>
