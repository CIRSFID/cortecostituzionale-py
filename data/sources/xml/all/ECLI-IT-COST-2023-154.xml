<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2023</anno_pronuncia>
    <numero_pronuncia>154</numero_pronuncia>
    <ecli>ECLI:IT:COST:2023:154</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SCIARRA</presidente>
    <relatore_pronuncia>Nicolò Zanon</relatore_pronuncia>
    <redattore_pronuncia>Nicolò Zanon</redattore_pronuncia>
    <data_decisione>06/07/2023</data_decisione>
    <data_deposito>18/07/2023</data_deposito>
    <tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>ammissibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Silvana SCIARRA; Giudici : Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI, Stefano PETITTI, Angelo BUSCEMA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito delle delibere della Camera dei deputati del 27 luglio 2022 (doc. IV-quater, numeri 3 e 4), promosso dal Tribunale ordinario di Salerno, sezione prima civile, con ricorso depositato in cancelleria il 17 febbraio 2023 ed iscritto al n. 2 del registro conflitti tra poteri dello Stato 2023, fase di ammissibilità, la cui trattazione è stata fissata per l'adunanza in camera di consiglio del 5 luglio 2023.&#13;
 Udito nella camera di consiglio del 6 luglio 2023 il Giudice relatore Nicolò Zanon;&#13;
 deliberato nella camera di consiglio del 6 luglio 2023.&#13;
 Ritenuto che, con ricorso depositato il 17 febbraio 2023 (reg. confl. poteri n. 2 del 2023), il Tribunale ordinario di Salerno, sezione prima civile, ha promosso conflitto di attribuzione tra poteri dello Stato in riferimento alla «delibera» (più correttamente due delibere, relative alla posizione di due parlamentari) del 27 luglio 2022 (doc. IV-quater, numeri 3 e 4), con cui la Camera dei deputati ha affermato che le dichiarazioni rese da Giorgio Mulé e Roberto Occhiuto, allora deputati, costituiscono opinioni espresse da membri del Parlamento nell'esercizio delle loro funzioni e ricadono, pertanto, nella garanzia di insindacabilità di cui all'art. 68, primo comma, della Costituzione;&#13;
 che il giudizio nell'ambito del quale è sorto il ricorso ha ad oggetto una richiesta risarcitoria per danni non patrimoniali avanzata da Marisa Manzini, all'epoca dei fatti Procuratore aggiunto presso la Procura della Repubblica del Tribunale ordinario di Cosenza, nei confronti dei due ex parlamentari, i quali avrebbero a suo dire pronunciato frasi nei suoi confronti diffamatorie nel corso di una conferenza svoltasi il 13 maggio 2019 presso la sala stampa della Camera dei deputati;&#13;
 che durante la conferenza, indetta, secondo quanto afferma il Tribunale di Salerno, «con il dichiarato scopo di reagire ad un complotto ordito nei confronti del sindaco di Cosenza Occhiuto Mario», fratello dell'on. Roberto Occhiuto, «gli onorevoli Mulé e Occhiuto» avrebbero parlato, «tra l'altro», di «"mala giustizia"» e di «"metodi scorretti"»;&#13;
 che in questo modo, aggiunge il ricorrente, gli stessi avrebbero «insinua[to] il sospetto che la dr.ssa Manzini avesse agito sotto le direttive del senatore Morra, al fine di ottenere prestigiosi incarichi dalla Commissione Antimafia»;&#13;
 che nel corso del giudizio, i due convenuti hanno prospettato l'improcedibilità della domanda attorea a motivo della insindacabilità, ai sensi dell'art. 68, primo comma, Cost., delle proprie dichiarazioni, giacché rese nell'esercizio della funzione;&#13;
 che tuttavia, riferisce il Tribunale di Salerno, con propria ordinanza in data 13 gennaio 2022, lo stesso ha «sostanzialmente» rigettato tale richiesta «pur in mancanza di un espresso provvedimento in tal senso», avendo disposto la prosecuzione della fase istruttoria (è citata, sul valore di rigetto implicito dell'istanza in caso di silenzio, Corte di cassazione, sezione terza civile, sentenza 16 ottobre-5 dicembre 2014, n. 25739);&#13;
 che, prosegue il ricorrente, con successiva «delibera» del 27 luglio 2022, la Camera dei deputati ha tuttavia ritenuto le dichiarazioni in relazione alle quali è in corso il giudizio insindacabili ai sensi dell'art. 68, primo comma, Cost.;&#13;
 che, pertanto, sarebbe «evidente il contrasto tra quanto deliberato dalla Giunta per le autorizzazioni a procedere ed il contenuto sostanziale della [propria] ordinanza», «dovendosi anzitutto stabilire [...] se sussiste nesso eziologico tra le dichiarazioni rese e la funzione parlamentare svolta»;&#13;
 che, di conseguenza, conclude il ricorrente, «esiste un conflitto, la cui risoluzione non può che spettare alla Corte Costituzionale», alla quale è in definitiva chiesto di dichiarare «la ammissibilità del presente ricorso» ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), «con tutte le conseguenze di legge».&#13;
 Considerato che, con ricorso depositato il 17 febbraio 2023 (reg. confl. poteri n. 2 del 2023), il Tribunale ordinario di Salerno, sezione prima civile, ha promosso conflitto di attribuzione tra poteri dello Stato in riferimento alla «delibera» del 27 luglio 2022 (più correttamente due delibere, doc. IV-quater, numeri 3 e 4, relative alla posizione di due parlamentari), con cui la Camera dei deputati ha affermato che le dichiarazioni rese da Giorgio Mulé e Roberto Occhiuto, allora deputati, costituiscono opinioni espresse da membri del Parlamento nell'esercizio delle loro funzioni e ricadono, pertanto, nella garanzia di insindacabilità di cui all'art. 68, primo comma, Cost.;&#13;
 che, in questa fase del giudizio, la Corte è chiamata a deliberare, in camera di consiglio e senza contraddittorio, in ordine alla sussistenza dei requisiti soggettivo e oggettivo prescritti dall'art. 37, primo comma, della legge n. 87 del 1953, ossia a decidere se il conflitto insorga tra organi competenti a dichiarare definitivamente la volontà del potere cui appartengono e per la delimitazione della sfera di attribuzioni determinata per i vari poteri da norme costituzionali, restando impregiudicata ogni ulteriore questione, anche in punto di ammissibilità;&#13;
 che, sotto il profilo del requisito soggettivo, va riconosciuta la legittimazione del Tribunale di Salerno a promuovere conflitto di attribuzione tra poteri dello Stato, in quanto organo giurisdizionale in posizione di indipendenza costituzionalmente garantita, competente a dichiarare definitivamente, nell'esercizio delle funzioni attribuitegli, la volontà del potere cui appartiene (ex plurimis, ordinanze n. 34 e n. 1 del 2023, n. 35 del 2022 e n. 82 del 2020);&#13;
 che, parimenti, deve essere riconosciuta la legittimazione della Camera dei deputati ad essere parte del presente conflitto, quale organo competente a dichiarare in modo definitivo la propria volontà in ordine all'applicazione dell'art. 68, primo comma, Cost. (ex plurimis, ordinanze n. 34 e n. 1 del 2023, n. 35 del 2022 e n. 82 del 2020);&#13;
 che, per quanto attiene al profilo oggettivo, il Tribunale di Salerno lamenta la lesione della propria sfera di attribuzione costituzionalmente garantita, in conseguenza dell'esercizio asseritamente illegittimo, per inesistenza dei relativi presupposti, del potere spettante alla Camera dei deputati di dichiarare l'insindacabilità delle opinioni espresse da membri di quel ramo del Parlamento ai sensi dell'art. 68, primo comma, Cost.;&#13;
 che, dunque, esiste la materia di un conflitto la cui risoluzione spetta alla competenza di questa Corte (ex plurimis, ordinanze n. 34 e n. 1 del 2023, n. 35 del 2022 e n. 82 del 2020).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 1) dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87 (Norme sulla costituzione e sul funzionamento della Corte costituzionale), il ricorso per conflitto di attribuzione tra poteri dello Stato indicato in epigrafe, promosso dal Tribunale ordinario di Salerno, sezione prima civile, nei confronti della Camera dei deputati;&#13;
 2) dispone:&#13;
 a) che la cancelleria di questa Corte dia immediata comunicazione della presente ordinanza al Tribunale di Salerno;&#13;
 b) che il ricorso e la presente ordinanza siano, a cura del ricorrente, notificati alla Camera dei deputati, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di trenta giorni previsto dall'art. 26, comma 3, delle Norme integrative per i giudizi davanti alla Corte costituzionale.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 6 luglio 2023.&#13;
 F.to:&#13;
 Silvana SCIARRA, Presidente&#13;
 Nicolò ZANON, Redattore&#13;
 Valeria EMMA, Cancelliere&#13;
 Depositata in Cancelleria il 18 luglio 2023&#13;
 Il Cancelliere&#13;
 F.to: Valeria EMMA</dispositivo>
  </pronuncia_testo>
</pronuncia>
