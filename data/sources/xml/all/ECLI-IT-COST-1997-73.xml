<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>73</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:73</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/03/1997</data_decisione>
    <data_deposito>28/03/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, &#13;
 avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel   giudizio  sull'ammissibilità  del  conflitto  di  attribuzione    &#13;
 sollevato dal Comitato regionale di controllo sugli atti  degli  enti    &#13;
 locali  della regione Lazio, con ricorso depositato l'8 ottobre 1996,    &#13;
 iscritto al n. 66 del registro ammissibilità conflitti;                 &#13;
   Udito nella camera di consiglio del 12  febbraio  1997  il  giudice    &#13;
 relatore Piero Alberto Capotosti;                                        &#13;
   Ritenuto  che  il ricorrente ha sollevato conflitto di attribuzione    &#13;
 nei confronti della regione Lazio  in  riferimento  alla  legge  reg.    &#13;
 Lazio  25  luglio  1996,  n. 27, art.18, che ha modificato l'art. 30,    &#13;
 quarto comma, della  legge  reg.  Lazio  13  marzo  1992,  n.  26,  e    &#13;
 stabilito  che,  nel  caso  di  accertato  ritardo  od  omissione nel    &#13;
 compimento di atti obbligatori da  parte  dell'ente  assoggettato  al    &#13;
 controllo,  decorso  inutilmente  il  termine  a tal fine fissato, il    &#13;
 commissario incaricato di adottare l'atto deve  essere  nominato  dal    &#13;
 comitato  di  controllo,  o  dalla  sezione decentrata competente, su    &#13;
 designazione  dell'Assessore  competente  in  materia  di  personale,    &#13;
 d'intesa  con  l'Assessore  competente in materia di rapporti con gli    &#13;
 enti locali;                                                             &#13;
     che  il  ricorrente  assume  d'essere  legittimato   a   proporre    &#13;
 conflitto  in  considerazione  dei caratteri di autonomia e terzietà    &#13;
 della funzione esercitata e  deduce  che  il  potere  di  scelta  del    &#13;
 commissario  ad  acta costituisce componente essenziale del potere di    &#13;
 nomina, sicché la sua devoluzione ad un diverso organo della regione    &#13;
 disposta dalla norma dell'art. 18 della legge reg. Lazio  n.  27  del    &#13;
 1996   sarebbe   invasiva  della  propria  sfera  di  attribuzioni  e    &#13;
 violerebbe l'art.  130 della Costituzione, in relazione agli artt.  5    &#13;
 e 117;                                                                   &#13;
   Considerato  che  la  Corte è chiamata a decidere preliminarmente,    &#13;
 senza contraddittorio, ai sensi dell'art. 37, terzo e  quarto  comma,    &#13;
 della  legge  11 marzo 1953, n. 87, se il ricorso sia ammissibile, in    &#13;
 quanto esista "la materia di un conflitto la cui  risoluzione  spetti    &#13;
 alla  sua  competenza",  in  riferimento  alla presenza dei requisiti    &#13;
 soggettivi ed oggettivi, richiamati  dal  primo  comma  dello  stesso    &#13;
 articolo;                                                                &#13;
     che,  sotto  il  profilo  oggettivo, nella specie palesemente non    &#13;
 sussiste materia di conflitto, in quanto la legge regionale impugnata    &#13;
 non configura alcuna violazione, né diretta,  né  indiretta,  delle    &#13;
 norme  costituzionali che definiscono la sfera di attribuzioni che il    &#13;
 ricorrente rivendica, poiché l'art. 130 della Costituzione non  pone    &#13;
 limiti  al legislatore in ordine all'estensione ed alle modalità dei    &#13;
 controlli ivi previsti (ordinanza n. 512 del 1991);                      &#13;
     che, sotto lo stesso profilo, il ricorso è  inammissibile  anche    &#13;
 in   considerazione  dell'atto  in  riferimento  al  quale  è  stato    &#13;
 sollevato, dato che il conflitto di  attribuzione  tra  poteri  dello    &#13;
 Stato  non  può essere proposto contro atti legislativi, al di fuori    &#13;
 dei casi, nella  specie  non  ricorrenti,  puntualmente  identificati    &#13;
 dalla  giurisprudenza  della  Corte  (sentenza  n.  161 del 1995), in    &#13;
 quanto "la sperimentabilità del conflitto contro gli atti suindicati    &#13;
 finirebbe con il costituire un elemento di rottura del nostro sistema    &#13;
 di garanzia costituzionale, sistema che, per quanto concerne la legge    &#13;
 (e  gli  atti  equiparati),  è incentrato nel sindacato incidentale"    &#13;
 (sentenza n. 406 del 1989);                                              &#13;
     che tali rilievi assorbono il profilo soggettivo della  questione    &#13;
 di   ammissibilità   del   ricorso,  onde  la  Corte  è  dispensata    &#13;
 dall'esaminare  se  sussistano  la  legittimazione  attiva  e  quella    &#13;
 passiva a promuovere ed a resistere al presente conflitto, anche alla    &#13;
 luce  della  sentenza  resa  in  fattispecie analoga a quella che qui    &#13;
 costituisce oggetto di decisione (sentenza n. 161 del 1981).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara inammissibile il ricorso  per  conflitto  di  attribuzione    &#13;
 proposto  dal  Comitato  regionale di controllo sugli atti degli enti    &#13;
 locali della regione Lazio contro la regione  Lazio,  depositato  l'8    &#13;
 ottobre 1996.                                                            &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 marzo 1997.                                &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 28 marzo 1997.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
