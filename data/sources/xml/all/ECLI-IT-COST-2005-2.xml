<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>2</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:2</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ONIDA</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>10/01/2005</data_decisione>
    <data_deposito>11/01/2005</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Valerio ONIDA; Giudici: Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 7 del decreto-legge 29 marzo 2004, n. 80 (Disposizioni urgenti in materia di enti locali), recante modifiche al testo degli artt. 58, comma 1, lettera b), e 59, comma 6, del decreto legislativo 18 agosto 2000, n. 267 (Testo unico delle leggi sull'ordinamento degli enti locali), promosso con ordinanza del 17 aprile 2004 dalla Corte di cassazione sui ricorsi riuniti proposti da G. B. contro R. A. P. ed altri, nonché da G. T. contro G. B. ed altri, iscritta al n. 661 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 33, prima serie speciale, dell'anno 2004. &#13;
      Visti gli atti di costituzione di G. B., di R. A. P. ed altri e di A. N. ed altri nonché l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nell'udienza pubblica del 30 novembre 2004 il Giudice relatore Francesco Amirante; &#13;
      uditi gli avvocati Giandomenico Falcon e Marcello Scurria per A. N. ed altri, Fulvio Cintioli per R. A. P. ed altri, Angelo Falzea, Andrea Lo Castro e Mario Sanino per G. B. e l'avvocato dello Stato Giuseppe Fiengo per il Presidente del Consiglio dei ministri. &#13;
    Ritenuto che la Corte di cassazione ha sollevato, in riferimento all'art. 77, secondo comma, della Costituzione, questione di legittimità costituzionale dell'art. 7 del decreto-legge 29 marzo 2004, n. 80 (Disposizioni urgenti in materia di enti locali), recante modifiche al testo degli artt. 58, comma 1, lettera b), e 59, comma 6, del decreto legislativo 18 agosto 2000, n. 267 (Testo unico delle leggi sull'ordinamento degli enti locali);  &#13;
    che al giudice a quo è stata chiesta la cassazione della sentenza dichiarativa della decadenza del ricorrente dalla carica di sindaco del Comune di Messina, pronunciata in conseguenza della condanna per i delitti di cui agli artt. 81, 314, secondo comma, e 323 del codice penale (divenuta definitiva successivamente alla sua proclamazione); &#13;
    che il remittente motiva la rilevanza della questione osservando come la norma impugnata – soppressiva del delitto di peculato d'uso dal novero delle cause ostative alla candidatura ed al mantenimento della carica eventualmente conseguita (salvo il caso in cui la pena irrogata superi i sei mesi) – sia applicabile nella Regione siciliana nonché nel giudizio in corso dinanzi a lui, non perché retroattiva, ma in quanto espressione di un nuovo parametro cui il legislatore àncora il giudizio di indegnità rispetto alla conservazione della carica; &#13;
    che la non manifesta infondatezza deriverebbe dall'evidente difetto del necessario requisito della sussistenza del “caso straordinario di necessità ed urgenza”, non rilevabile dal preambolo del decreto-legge, riguardante problemi di funzionalità degli enti locali e non già la materia elettorale, non suscettibile, peraltro, di essere regolata attraverso la decretazione d'urgenza, come risulta dall'art. 15, comma 2, lettera b), della legge 23 agosto 1988, n. 400; &#13;
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, sollecitando la restituzione degli atti al giudice remittente, alla luce della sopravvenuta legge di conversione 28 maggio 2004, n. 140 e concludendo per l'inammissibilità, ovvero per l'infondatezza della questione, in ragione della finalità dell'impugnato decreto, asseritamente volto a superare alcune difficoltà interpretative ed applicative relative all'art. 58 del testo unico citato; &#13;
    che nel giudizio davanti a questa Corte si sono costituite numerose parti del giudizio a quo; &#13;
    che il ricorrente ha sostenuto l'identità del contenuto precettivo della norma impugnata rispetto a quello risultante dalla legge di conversione, chiedendo la declaratoria d'infondatezza sulla base del carattere d'indispensabilità posseduto dalla decretazione di urgenza quando occorra colmare con immediatezza un vuoto normativo e per la discrezionalità politica sottesa alla relativa valutazione; &#13;
    che i controricorrenti hanno preliminarmente eccepito l'inammissibilità della questione per l'inapplicabilità della norma nel giudizio a quo, in quanto essa riguarderebbe solo chi fosse in carica al momento della sua entrata in vigore e non chi fosse già decaduto di diritto, come il ricorrente, concludendo quindi per l'infondatezza della questione. &#13;
    Considerato che, secondo la Corte di cassazione, l'impugnata norma del decreto-legge n. 80 del 2004, nel sottrarre il delitto di peculato d'uso dal novero delle previsioni ostative alla candidatura di sindaco e comunque dalle cause di decadenza dalla carica, ove sia passata in giudicato la sentenza di condanna successivamente all'elezione, difetterebbe in modo evidente del requisito della straordinaria necessità e urgenza, con conseguente violazione dell'art. 77, secondo comma, della Costituzione; &#13;
    che, successivamente all'emissione dell'ordinanza di rimessione, il citato decreto-legge è stato convertito nella legge 28 maggio 2004, n. 140; &#13;
    che con tale legge sono state apportate modificazioni al testo del decreto e sono state altresì enunciate le ragioni della emanazione della norma censurata;  &#13;
    che è, pertanto, necessario disporre la restituzione degli atti alla Corte remittente per un nuovo esame della rilevanza della questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    ordina la restituzione degli atti alla Corte di cassazione. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 10 gennaio 2005. &#13;
F.to: &#13;
Valerio ONIDA, Presidente &#13;
Francesco AMIRANTE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria l'11  gennaio 2005. &#13;
Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
