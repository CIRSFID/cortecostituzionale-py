<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1979</anno_pronuncia>
    <numero_pronuncia>139</numero_pronuncia>
    <ecli>ECLI:IT:COST:1979:139</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Guido Astuti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>30/11/1979</data_decisione>
    <data_deposito>06/12/1979</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Prof. &#13;
 ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 6, comma  &#13;
 secondo,  della  legge  22  novembre  1962,  n.  1646  (Modifiche  agli  &#13;
 ordinamenti  degli  Istituti  di  previdenza  presso  il  Ministero del  &#13;
 tesoro) promosso con ordinanza emessa il 15 dicembre 1976  dalla  Corte  &#13;
 dei  conti,  sul  ricorso  di Graziadei Luciana, iscritta al n. 249 del  &#13;
 registro ordinanze 1978 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica  n. 208 del 26 luglio 1978.  Visto l'atto di costituzione di  &#13;
 Graziadei Luciana;                                                       &#13;
     udito nell'udienza pubblica del 10 ottobre 1979 il Giudice relatore  &#13;
 Guido Astuti;                                                            &#13;
     udito l'avv. Marco Vais per Graziadei Luciana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - La Corte  dei  conti  -  sezione  III  giurisdizionale  -  con  &#13;
 ordinanza  emessa  il  15 dicembre 1976 nel corso del giudizio promosso  &#13;
 con ricorso di Graziadei Luciana avverso la decisione di reiezione  (ai  &#13;
 sensi  dell'art. 6 della legge 22 novembre 1962, n. 1646) della domanda  &#13;
 di pensione di riversibilità per essere stato  il  matrimonio  con  il  &#13;
 coniuge,  già  dipendente dell'Amministrazione provinciale di Venezia,  &#13;
 celebrato dopo la cessazione dal servizio senza che fosse nata prole  e  &#13;
 per  essere  lo  stesso  durato  meno  di  due  anni,  ha sollevato, su  &#13;
 eccezione  della  parte,  questione  di   legittimità   costituzionale  &#13;
 dell'art. 6 della legge n. 1646 per contrasto con l'art. 3 Cost.         &#13;
     La  norma  impugnata  nel  condizionare,  tra l'altro, al trascorso  &#13;
 biennio di matrimonio la concessione del trattamento di riversibilità,  &#13;
 senza prendere in considerazione la particolare  situazione  di  coloro  &#13;
 che  hanno  celebrato  matrimoni  successivamente  allo scioglimento di  &#13;
 precedenti matrimoni (di uno dei coniugi) ma che non hanno  soddisfatto  &#13;
 la  predetta  condizione  per essere uno dei coniugi deceduto prima che  &#13;
 fosse trascorso il biennio, integrerebbe una  illogica  discriminazione  &#13;
 tra  coloro che, provenienti da divorzio, sono soggetti alla disciplina  &#13;
 della legge n. 1646/1962 (Casse pensioni degli Istituti  di  previdenza  &#13;
 presso  il  Ministero  del  tesoro),  e  coloro  il  cui trattamento è  &#13;
 regolato dalle leggi 12 agosto 1962, n. 1338 e 30 aprile 1969,  n.  153  &#13;
 nei  cui confronti, per effetto dell'art. 32 della legge 3 giugno 1975,  &#13;
 n. 160, si prescinde dal biennio di durata del matrimonio a  condizione  &#13;
 che  lo scioglimento del precedente matrimonio sia intervenuto entro il  &#13;
 31 dicembre 1975.                                                        &#13;
     Nella stessa  ordinanza  è  stata  prospettata  l'opportunità  di  &#13;
 estendere  l'eventuale  dichiarazione  di incostituzionalità, ai sensi  &#13;
 dell'art. 27 della legge 11 marzo  1953,  n.  87,  alla  norma  di  cui  &#13;
 all'art.  81,  terzo  comma,  del t.u. approvato con d.P.R. 29 dicembre  &#13;
 1973, n. 1092, sul trattamento pensionistico degli impiegati  civili  e  &#13;
 militari dello Stato che non prescinde, al pari dell'art. 6 della legge  &#13;
 n. 1646, dalla generale condizione del biennio di durata del matrimonio  &#13;
 nei confronti di coloro che provengono da divorzio.                      &#13;
     2. - Si è costituita nel presente giudizio la parte privata che ha  &#13;
 insistito perché la questione sia ritenuta fondata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con  l'ordinanza  indicata  in  epigrafe  la Corte dei conti  &#13;
 solleva, in riferimento all'art. 3 della Costituzione, la questione  di  &#13;
 legittimità della disposizione dell'art. 6, secondo comma, della legge  &#13;
 22  novembre  1962, n. 1646, che tra i requisiti per la concessione del  &#13;
 trattamento di  riversibilità,  pone  quello  che  il  matrimonio  del  &#13;
 pensionato  sia  durato  almeno  due  anni;  e  ciò  in  quanto  detta  &#13;
 disposizione  "non  prescinde  dalla  condizione   della   durata   del  &#13;
 matrimonio   (biennio)   quando  trattisi  di  matrimoni  successivi  a  &#13;
 divorzio".                                                               &#13;
     Si osserva nell'ordinanza che l'art. 32 della legge 3 giugno  1975,  &#13;
 n.  160,  riformando la disciplina delle pensioni erogate dall'Istituto  &#13;
 Nazionale per la previdenza sociale,  (per  le  quali,  ai  fini  della  &#13;
 concessione del trattamento di riversibilità è del pari richiesto che  &#13;
 il  matrimonio  sia  durato  almeno  due anni), ha disposto che da quel  &#13;
 requisito si prescinde quando il matrimonio sia stato contratto dopo lo  &#13;
 scioglimento d'un precedente matrimonio a norma della legge 1  dicembre  &#13;
 1970,  n.  898,  purché  entro il 31 dicembre 1975; e che pertanto, in  &#13;
 seguito a questa innovazione legislativa, si è verificata una illogica  &#13;
 discriminazione tra due categorie di soggetti  versanti  in  condizioni  &#13;
 sostanzialmente ed obbiettivamente identiche, i provenienti da divorzio  &#13;
 il  cui  trattamento  pensionistico  è  regolato  dalla  legge  dianzi  &#13;
 ricordata, e quelli il cui trattamento pensionistico  ricade  sotto  la  &#13;
 disciplina  della legge 22 novembre 1962, n. 1646 (Casse pensioni degli  &#13;
 Istituti di previdenza presso il Ministero del tesoro). L'ordinanza  di  &#13;
 rimessione   prospetta,   conseguentemente,   la  opportunità  che  la  &#13;
 eventuale dichiarazione di incostituzionalità dell'art. 6 della  legge  &#13;
 n.  1646 del 1962 venga estesa anche all'analoga disposizione dell'art.  &#13;
 81, terzo comma, del t.u.  delle norme sul  trattamento  di  quiescenza  &#13;
 dei  dipendenti  civili e militari dello Stato, approvato con d.P.R. 29  &#13;
 dicembre 1973, n. 1092.                                                  &#13;
     2.  - La questione è fondata. Questa Corte, con sentenza 9 gennaio  &#13;
 1975, n.  3  ebbe  già  a  dichiarare  non  fondata  la  questione  di  &#13;
 legittimità  dell'art. 6, secondo comma, della legge 22 novembre 1962,  &#13;
 n. 1646, e dell'art. 11, secondo comma, della legge 15  febbraio  1958,  &#13;
 n.  46 (modificato dall'art. 1 della legge 14 maggio 1969, n.252, e ora  &#13;
 sostituito dall'art. 81, terzo comma, del t.u. approvato con d.P.R.  29  &#13;
 dicembre  1973,  n.  1092),  questione  allora  prospettata sotto altri  &#13;
 profili e anche in riferimento a diversi  parametri  costituzionali;  e  &#13;
 ciò  osservando,  in  particolare,  che  "i  criteri limitativi per le  &#13;
 pensioni di riversibilità derivanti  da  matrimoni  conclusi  da  già  &#13;
 pensionati  sono  stati  dettati in via generale, dal legislatore, come  &#13;
 remora all'ipotesi, non infrequente, di  matrimoni  contratti  non  per  &#13;
 naturale  affetto,  e  quindi,  in  tal senso, sospettabili, sicché le  &#13;
 condizioni  restrittive,  volte  a  garantire,  in  qualche  modo,   la  &#13;
 genuinità e la serietà del tardivo coniugio, si risolvono anche nella  &#13;
 tutela  del pubblico erario contro maliziose e fraudolenti iniziative".  &#13;
 Ma quella pronuncia, anche nella  ricordata  motivazione,  che  qui  si  &#13;
 conferma,  circa  la  legittimità  -  in  via generale - dei requisiti  &#13;
 limitativi per la concessione delle  pensioni  di  riversibilità,  non  &#13;
 osta alla odierna decisione di accoglimento, che si impone in relazione  &#13;
 alla  denunciata  disparità di trattamento, determinata dalla legge di  &#13;
 riforma delle pensioni  erogate  dall'INPS,  con  l'introduzione  della  &#13;
 deroga  al  requisito della durata minima del matrimonio nella speciale  &#13;
 ipotesi di matrimonio successivo a divorzio di uno dei coniugi.          &#13;
     L'art. 32 della legge 3 giugno 1975, n. 160,  (che  sostituisce  il  &#13;
 secondo  comma  dell'art.  7  della  legge 12 agosto 1962, n. 1338, nel  &#13;
 testo risultante dall'art. 24 della legge  30  aprile  1969,  n.  153),  &#13;
 dispone  infatti  che  da  quel requisito si prescinde "per i matrimoni  &#13;
 celebrati successivamente alla sentenza di scioglimento del  precedente  &#13;
 matrimonio  di  uno  dei  due coniugi pronunciata a norma della legge 1  &#13;
 dicembre 1970, n. 898, ma  non  oltre  il  31  dicembre  1975".  Questa  &#13;
 disposizione,   intesa   a   consentire   l'accesso  alla  pensione  di  &#13;
 riversibilità nei casi non infrequenti  in  cui  l'instaurazione  d'un  &#13;
 regolare   rapporto  di  coniugio  in  età  avanzata  dipendeva  dalla  &#13;
 preesistente impossibilità  giuridica,  dovuta  alla  indissolubilità  &#13;
 d'un  preesistente  vincolo  matrimoniale  (e  in questa prospettiva la  &#13;
 norma prevede un termine per la sua efficacia  temporale,  limitata  ai  &#13;
 matrimoni  celebrati  non  oltre  il 31 dicembre 19751, ha prodotto una  &#13;
 ingiustificata disparità di trattamento rispetto alle altre  categorie  &#13;
 di  aventi  titolo  a  pensione  di  riversibilità, in base alle leggi  &#13;
 concernenti i  pensionati  assistiti  dalle  Casse  degli  Istituti  di  &#13;
 previdenza  del Ministero del tesoro, ed i pensionati civili e militari  &#13;
 dello Stato. La ratio della norma derogatrice ne  esige  la  estensione  &#13;
 anche  a beneficio delle ricordate categorie, non potendosi individuare  &#13;
 alcun  elemento  idoneo  a  giustificare  una  diversa  disciplina  del  &#13;
 requisito  di  cui  è  causa,  per  la  concessione  delle pensioni di  &#13;
 riversibilità.                                                          &#13;
     Di conseguenza, deve dichiararsi la  illegittimità  costituzionale  &#13;
 dell'art.  6,  secondo  comma,  della  legge 22 novembre 1962, n. 1646,  &#13;
 nonché - a norma dell'art. 27 della legge  11  marzo  1953,  n.  87  -  &#13;
 dell'art.  81,  terzo  comma, del t.u. approvato con d.P.R. 29 dicembre  &#13;
 1973, n. 1092.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  illegittimità  costituzionale  dell'art.  6, secondo  &#13;
 comma, della legge 22 novembre 1962, n. 1646, in relazione al  disposto  &#13;
 dell'art.  32  della  legge  22  novembre  1975,  n. 168, in quanto non  &#13;
 consente la  deroga  al  requisito  che  il  matrimonio  contratto  dal  &#13;
 pensionato  sia  durato almeno due anni, introdotta dall'art. 32 "per i  &#13;
 matrimoni celebrati successivamente alla sentenza di  scioglimento  del  &#13;
 precedente  matrimonio di uno dei due coniugi pronunciata a norma della  &#13;
 legge 1 dicembre 1970, n. 898, ma non oltre il 31 dicembre 1975";        &#13;
     dichiara, a norma dell'art. 27 della legge 11 marzo 1953, n. 87, la  &#13;
 illegittimità  costituzionale,  nella  stessa  parte  e  nei  medesimi  &#13;
 termini  sopra  indicati, dell'art. 81, terzo comma, del t.u. approvato  &#13;
 con d.P.R. 29 dicembre 1973, n. 1092.                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 30 novembre 1979.                             &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI   -   ANTONINO  DE  STEFANO  -  &#13;
                                   LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN -  &#13;
                                   ORONZO REALE -  BRUNETTO  BUCCIARELLI  &#13;
                                   DUCCI  -  ALBERTO  MALAGUGINI - LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA PERGOLA - VIRGILIO ANDRIOLI.        &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
