<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>206</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:206</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/04/1991</data_decisione>
    <data_deposito>13/05/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 1  del  decreto-legge  30  dicembre  1982,  n.  953  (Misure  in materia tributaria),    &#13;
 convertito con modificazioni nella legge 28  febbraio  1983,  n.  53,    &#13;
 promossi con quattro ordinanze emesse dalla Commissione tributaria di    &#13;
 secondo  grado di Ancona iscritte ai nn. 11, 12, 13 e 14 del registro    &#13;
 ordinanze 1991 e pubblicate nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 5, prima serie speciale, dell'anno 1991;                              &#13;
    Visti gli atti di intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 10 aprile 1991 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto che la Commissione tributaria di secondo grado di Ancona,    &#13;
 con due ordinanze del 17 ottobre, e con altre del 9 e 23 maggio 1985,    &#13;
 pervenute alla Corte costituzionale il 10 gennaio 1991 (R.O. nn.  11,    &#13;
 12,  13 e 14 del 1991), ha sollevato, in riferimento agli artt. 3, 23    &#13;
 e 53 della Costituzione,  questioni  di  legittimità  costituzionale    &#13;
 dell'art.  1  del  d.-l.  30  dicembre  1982,  n. 953, convertito con    &#13;
 modificazioni nella legge 28 febbraio 1983, n. 53, nella parte in cui    &#13;
 abolisce retroattivamente, per l'anno 1982, la detrazione forfettaria    &#13;
 del tre per cento degli oneri e delle spese  professionali,  prevista    &#13;
 dall'art.  50, terzo comma, del d.P.R. n. 597 del 1973;                  &#13;
      che, secondo il giudice a quo la norma impugnata contrasterebbe:    &#13;
 a)  con  l'art.  3  della  Costituzione,  avendo  posto in essere una    &#13;
 situazione di disparità tra professionisti, a seconda che abbiano  o    &#13;
 meno   conservato   la   documentazione  delle  spese  in  precedenza    &#13;
 detraibili in modo forfettario; b)  con  gli  artt.  23  e  53  della    &#13;
 Costituzione,  per  il  carattere retroattivo dell'eliminazione della    &#13;
 detrazione  forfettaria  del  tre  per  cento,  che,   impedendo   la    &#13;
 detrazione   dall'imponibile  delle  spese,  di  cui  non  sia  stata    &#13;
 conservata la documentazione, lederebbe il principio della  capacità    &#13;
 contributiva;                                                            &#13;
    Considerato  che è opportuno riunire i giudizi, data la identità    &#13;
 delle questioni sollevate;                                               &#13;
      che le questioni stesse sono  analoghe  ad  altre,  risolte  nel    &#13;
 senso della non fondatezza con ordinanza n. 51 del 1988, con la quale    &#13;
 questa   Corte   ha  affermato  che  la  determinazione  degli  oneri    &#13;
 deducibili, considerato il necessario collegamento con la  produzione    &#13;
 del  reddito  e il nesso di proporzionalità con il gettito generale,    &#13;
 costituisce materia di scelta discrezionale riservata al  legislatore    &#13;
 e  la  retroattività  della norma impugnata non risulta in contrasto    &#13;
 con il principio della capacità contributiva;                           &#13;
      che la eventuale mancata documentazione degli  oneri  sopportati    &#13;
 costituisce  una  circostanza  di mero fatto che, come tale, non può    &#13;
 dar luogo a censure di illegittimità costituzionale;                    &#13;
      che non sono stati addotti argomenti nuovi che  possano  indurre    &#13;
 ad una diversa decisione;                                                &#13;
    Visti  gli  artt.  26  della  legge 11 marzo 1953, n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi innanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la  manifesta  infondatezza   delle    &#13;
 questioni  di  legittimità  costituzionale  dell'art. 1 del decreto-legge 30 dicembre  1982,  n.  953  (Misure  in  materia  tributaria),    &#13;
 convertito  nella  legge  28  febbraio  1983,  n.  53,  sollevate, in    &#13;
 riferimento  agli  artt.  3,  23  e  53  della  Costituzione,   dalla    &#13;
 Commissione  tributaria  di secondo grado di Ancona, con le ordinanze    &#13;
 indicate in epigrafe.                                                    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 23 aprile 1991.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 13 maggio 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
