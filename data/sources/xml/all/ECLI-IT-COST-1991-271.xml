<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>271</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:271</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/05/1991</data_decisione>
    <data_deposito>12/06/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della legge della Regione    &#13;
 Lombardia 27 maggio 1985, n.  62  (Disciplina  degli  scarichi  degli    &#13;
 insediamenti  civili  e delle pubbliche fognature. Tutela delle acque    &#13;
 sotterranee dall'inquinamento), così come specificata nella delibera    &#13;
 della Giunta regionale del 24 giugno 1986, n. 4/10562,  promosso  con    &#13;
 ordinanza  emessa  il  27  giugno  1990 dalla Corte di cassazione sul    &#13;
 ricorso proposto da Zafferri Angelo, iscritta al n. 72  del  registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 8, prima serie speciale, dell'anno 1991;                              &#13;
    Visto  l'atto  di intervento del Presidente della Giunta regionale    &#13;
 lombarda;                                                                &#13;
    Udito nell'udienza pubblica del 7 maggio 1991 il Giudice  relatore    &#13;
 Francesco Greco;                                                         &#13;
    Udito l'avv. Valerio Onida per la Regione Lombardia;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Nel corso di un procedimento penale contro Angelo Zafferri -    &#13;
 imputato del reato di cui all'art. 21 legge 10 maggio 1976,  n.  319,    &#13;
 per  avere,  esercitando  la  riproduzione di fotografie su ceramica,    &#13;
 effettuato senza autorizzazione scarichi  inquinanti  nella  pubblica    &#13;
 fognatura  - la Corte di cassazione, con ordinanza del 27 giugno 1990    &#13;
 (R.O. n. 72 del 1991), ha sollevato, in riferimento all'art. 3  della    &#13;
 Costituzione,  questione  di  legittimità costituzionale della legge    &#13;
 della Regione Lombardia 27 maggio  1985,  n.  62,  come  interpretata    &#13;
 nella  deliberazione  della  Giunta  regionale  del 24 giugno 1986 n.    &#13;
 4/10562.                                                                 &#13;
    La Corte, presupponendo note le disposizioni  della  citata  legge    &#13;
 statale  n.  319 del 1976 (cosiddetta legge Merli), ed in particolare    &#13;
 l'art.  14,  che  esclude  l'autorizzazione  per  gli   scarichi   di    &#13;
 "insediamenti  civili" in pubbliche fognature e, quindi, sottrae tali    &#13;
 scarichi alla previsione penale del successivo art. 21, ha  osservato    &#13;
 che  la  citata  legge regionale nell'art. 1 classifica gli "scarichi    &#13;
 degli insediamenti civili", includendo nella categoria C  quelli  "di    &#13;
 acque  di  rifiuto  di  insediamenti adibiti a prestazione di servizi    &#13;
 individuati ai sensi del successivo art. 37".                            &#13;
    L'art.  37,  a  sua   volta,   demanda   alla   Giunta   regionale    &#13;
 l'individuazione dei detti scarichi.                                     &#13;
    La  Giunta,  infine, con la citata deliberazione considera tra gli    &#13;
 insediamenti civili anche i laboratori fotografici,  compresi  quelli    &#13;
 di  fotoriproduzione su ceramica, per gli scarichi dei quali, quindi,    &#13;
 non occorre l'autorizzazione, sicché  essi  sono  penalmente  leciti    &#13;
 ancorché non autorizzati, benché immettano nelle fognature sostanze    &#13;
 nocive, solitamente emesse dagli insediamenti produttivi.                &#13;
    Effetto   di   queste   disposizioni   regionali,   legislative  e    &#13;
 amministrative, è la depenalizzazione  di  comportamenti  sanzionati    &#13;
 penalmente   dalla  legislazione  statale  nel  rimanente  territorio    &#13;
 nazionale,  donde  la  prospettata  violazione   del   principio   di    &#13;
 eguaglianza.                                                             &#13;
    2.  -  L'ordinanza  è stata regolarmente comunicata, notificata e    &#13;
 pubblicata nella Gazzetta Ufficiale.                                     &#13;
    3. - È  intervenuto  nel  giudizio  il  Presidente  della  Giunta    &#13;
 regionale  lombarda,  il quale eccepisce anzitutto l'inammissibilità    &#13;
 della questione perché:                                                 &#13;
       a) l'ordinanza di rimessione non specifica né la  disposizione    &#13;
 di  legge  impugnata  né  la  ragione  per  la quale debba ritenersi    &#13;
 violato il principio di eguaglianza;                                     &#13;
       b)  la   questione   dell'assimibilità   dei   lavoratori   di    &#13;
 fotoceramica a quelli fotografici è effetto della interpretazione di    &#13;
 leggi, statali e regionali, e non riguarda la Costituzione;              &#13;
       c)  l'impugnazione  avrebbe dovuto avere per oggetto, caso mai,    &#13;
 il decreto-legge 10 agosto  1976,  n.  544,  convertito  in  legge  8    &#13;
 ottobre  1976  n.  690,  il  cui  art.  1-quater  detta  i criteri di    &#13;
 distinzione  tra  insediamenti  produttivi  e   insediamenti   civili    &#13;
 (ricorrenza di una aberratio ictus).                                     &#13;
    Nel merito sostiene la non fondatezza della questione perché:        &#13;
       a) non è riferita alla ripartizione della potestà legislativa    &#13;
 tra Stato e Regioni (art. 117 della Costituzione);                       &#13;
       b)  il  principio  di  eguaglianza  è male invocato, in quanto    &#13;
 l'attribuzione, da parte del Costituente, di una potestà legislativa    &#13;
 alle regioni serve appunto a differenziare la  disciplina  di  alcune    &#13;
 materie,   tra  cui  quella  della  difesa  delle  acque  contro  gli    &#13;
 inquinamenti, nelle diverse parti del territorio nazionale;              &#13;
       c) la  legge  regionale  impugnata  ha  solo  una  funzione  di    &#13;
 dettaglio e di integrazione della legislazione statale;                  &#13;
       d) la classificazione degli insediamenti produttivi e di quelli    &#13;
 civili  rientra  nella  discrezionalità del legislatore (sent. Corte    &#13;
 cost. n. 314 del 1983).<diritto>Considerato in diritto</diritto>1. - La Corte è chiamata a verificare se la legge  della  Regione    &#13;
 Lombardia  27  maggio  1985,  n. 62, interpretata secondo la delibera    &#13;
 della Giunta regionale 24 giugno 1986 n. 4/10562, nel  considerare  i    &#13;
 laboratori  di riproduzione fotografica su ceramica come insediamenti    &#13;
 civili anziché come insediamenti produttivi, e nel  sottrarne  così    &#13;
 gli  scarichi  inquinanti  non  autorizzati  alla comminatoria penale    &#13;
 contenuta nell'art.  21,  legge  statale  10  maggio  1976,  n.  319,    &#13;
 contrasti  con  l'art.  3  della Costituzione in quanto sottopone gli    &#13;
 stessi comportamenti ad un trattamento penale  differenziato  in  una    &#13;
 determinata parte del territorio nazionale.                              &#13;
    2. - La questione è inammissibile.                                   &#13;
    Gli  artt.  da  9  a  16  della  legge  10  maggio  1976,  n. 319,    &#13;
 distinguono gli scarichi da insediamenti produttivi e da insediamenti    &#13;
 civili, richiedendo per i primi l'autorizzazione regionale,  per  gli    &#13;
 altri  non.  L'art.  21 della stessa legge prevede le sanzioni penali    &#13;
 per gli scarichi effettuati senza la prescritta autorizzazione.          &#13;
    L'art.  1-quater  del  decreto-legge  10  agosto  1976,  n.   544,    &#13;
 convertito,  con  modificazioni, in legge 8 ottobre 1976, n. 690, dà    &#13;
 la nozione di  insediamento  produttivo  e  di  insediamento  civile,    &#13;
 comprendendovi,   tra   gli  altri,  quelli  da  immobili  adibiti  a    &#13;
 prestazione di servizi.                                                  &#13;
    L'art. 1 della legge della Regione Lombardia 27  maggio  1985,  n.    &#13;
 62,  classifica  gli  scarichi  degli  insediamenti civili in quattro    &#13;
 categorie, comprendendo nella  categoria  C  quelli  da  insediamenti    &#13;
 civili adibiti a prestazione di servizi. L'art. 37 successivo demanda    &#13;
 la  individuazione  dei  suddetti  alla  Giunta regionale, sentita la    &#13;
 competente commissione consiliare.                                       &#13;
    La Giunta regionale lombarda, nella seduta del 24 giugno 1986,  ha    &#13;
 effettuato  la prescritta individuazione comprendendo nel punto C gli    &#13;
 scarichi da studi  e  laboratori  fotografici  e  radiografici  senza    &#13;
 ulteriore specificazione.                                                &#13;
    Nella  fattispecie,  i giudici di merito, il Pretore e la Corte di    &#13;
 appello di Milano, hanno ritenuto che debbano essere compresi tra gli    &#13;
 insediamenti produttivi, anziché  tra  gli  insediamenti  civili,  i    &#13;
 laboratori  che  effettuano  riproduzioni  fotografiche  su ceramiche    &#13;
 perché vi si impiegano sostanze inquinanti e che, quindi,  per  essi    &#13;
 occorre  l'autorizzazione regionale. Hanno, quindi, condannato ad una    &#13;
 sanzione penale l'esercente di uno di  essi  in  quanto  non  si  era    &#13;
 munito della prescritta autorizzazione.                                  &#13;
    A  seguito  di  ricorso  dell'imputato,  la Corte di cassazione è    &#13;
 stata investita del controllo  di  legittimità  dell'interpretazione    &#13;
 effettuata dai giudici di merito.                                        &#13;
    Difetta,   quindi,   di   rilevanza   la  sollevata  questione  di    &#13;
 legittimità costituzionale perché la questione se i laboratori  che    &#13;
 effettuano   riproduzioni   fotografiche   su   ceramica   siano   da    &#13;
 comprendersi negli insediamenti produttivi anziché in quelli  civili    &#13;
 importa  esclusivamente  la  interpretazione  delle  leggi statali in    &#13;
 materia (legge n. 319 del 1976 e decreto-legge n. 544 del  1976),  il    &#13;
 che  rientra  nella  c.d.  funzione di nomofilachia che l'ordinamento    &#13;
 assegna alla Corte di cassazione.                                        &#13;
    Pertanto,  va  emessa  declaratoria  di   inammissibilità   della    &#13;
 questione sollevata.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   l'inammissibilità   della   questione  di  legittimità    &#13;
 costituzionale della legge della Regione Lombardia 27 maggio 1985, n.    &#13;
 62 (Disciplina degli  scarichi  degli  insediamenti  civili  e  delle    &#13;
 pubbliche     fognature.     Tutela     delle    acque    sotterranee    &#13;
 dall'inquinamento), interpretata secondo  la  delibera  della  Giunta    &#13;
 regionale 24 giugno 1986, n. 4/10562, in riferimento all'art. 3 della    &#13;
 Costituzione,  sollevata dalla Corte di cassazione con l'ordinanza in    &#13;
 epigrafe.                                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  Costituzionale,    &#13;
 Palazzo della Consulta il 23 maggio 1991.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 12 giugno 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
