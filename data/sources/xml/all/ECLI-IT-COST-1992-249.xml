<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>249</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:249</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/05/1992</data_decisione>
    <data_deposito>03/06/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, comma terzo,    &#13;
 della legge 26 luglio  1978,  n.  417  (Adeguamento  del  trattamento    &#13;
 economico  di  missione  e  di  trasferimento dei dipendenti statali)    &#13;
 promosso con ordinanza emessa il 18  aprile  e  21  giugno  1991  dal    &#13;
 Tribunale  amministrativo  regionale  per  la Basilicata, sul ricorso    &#13;
 proposto da Esposito Salvatore contro il Presidente del Consiglio dei    &#13;
 ministri ed altri, iscritta al n. 30 del registro  ordinanze  1992  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 7, prima    &#13;
 serie speciale, dell'anno 1992;                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  6 maggio 1992 il giudice    &#13;
 relatore Francesco Guizzi;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Esposito Salvatore, già dipendente distaccato del Comune  di    &#13;
 Angri   e,   successivamente,  inquadrato  nel  ruolo  del  personale    &#13;
 direttivo del Consiglio  di  Stato  e  dei  Tribunali  amministrativi    &#13;
 regionali   alle   dipendenze  della  Presidenza  del  Consiglio  dei    &#13;
 ministri, in servizio  a  Salerno,  presso  la  sede  distaccata  del    &#13;
 Tribunale  amministrativo  regionale  della  Campania, il 1° dicembre    &#13;
 1982 veniva chiamato ad espletare le stesse funzioni presso  la  sede    &#13;
 napoletana  di  quel  tribunale.  Il  ricorrente  aveva  percepito il    &#13;
 trattamento di missione sino al 30 aprile 1984, con il relativo onere    &#13;
 a carico dell'amministrazione di provenienza (il  comune  di  Angri);    &#13;
 ma,  una  volta  inquadrato  alle  dipendenze  della  Presidenza  del    &#13;
 Consiglio dei ministri, con decreto del  2  maggio  1984,  non  aveva    &#13;
 percepito   alcun   ulteriore,   similare,   trattamento   economico.    &#13;
 Proponeva, pertanto, ricorso contro la Presidenza del  Consiglio  dei    &#13;
 ministri,  il  Consiglio  di  Stato  e  il  Tribunale  amministrativo    &#13;
 regionale della Campania nonché del Consiglio  di  Presidenza  della    &#13;
 Giustizia Amministrativa per la declaratoria del proprio diritto alla    &#13;
 corresponsione  dell'indennità  di  missione per tutto il periodo di    &#13;
 lavoro   nella   diversa   sede    e    la    conseguente    condanna    &#13;
 dell'amministrazione al pagamento dell'indennità, oltre svalutazione    &#13;
 monetaria e interessi legali.                                            &#13;
    Investito  della  questione  il Tribunale amministrativo regionale    &#13;
 della Basilicata, con sentenza non  definitiva  (n.  347  del  1991),    &#13;
 accoglieva   parzialmente   la  domanda  accertando  il  diritto  del    &#13;
 ricorrente alla corresponsione del trattamento di  missione  per  240    &#13;
 giorni  a  decorrere dal 2 maggio 1984 e condannava l'amministrazione    &#13;
 resistente al pagamento del dovuto.                                      &#13;
    Il  Tribunale,  inoltre,  sollevava  d'ufficio  la  questione   di    &#13;
 legittimità  costituzionale dell'art. 1, terzo comma, della legge 26    &#13;
 luglio 1978,  n.  417,  (Adeguamento  del  trattamento  economico  di    &#13;
 missione e di trasferimento dei dipendenti statali) per contrasto con    &#13;
 gli  artt.  36 e 3 della Costituzione, e sospendeva la restante parte    &#13;
 del giudizio.                                                            &#13;
    2. - Osserva il tribunale che  la  norma  censurata,  vietando  il    &#13;
 pagamento  dell'indennità  "dopo  i  primi  240  giorni  di missione    &#13;
 continuativa nella medesima località" si pone  in  contrasto  con  i    &#13;
 detti     parametri     costituzionali,     perché     consentirebbe    &#13;
 all'amministrazione, legittimamente, di  far  prestare  servizio   ai    &#13;
 propri  dipendenti,  in  sedi diverse da quelle abituali, anche oltre    &#13;
 quel termine temporale.                                                  &#13;
    Essendo il trattamento di missione diretto  a  tenere  indenne  il    &#13;
 dipendente dalle maggiori spese che deve sopportare a causa della non    &#13;
 volontaria  prestazione  del servizio in luogo diverso dalla abituale    &#13;
 sede  di  servizio,  la  limitazione  temporale  posta  dalla   norma    &#13;
 impugnata  lederebbe  il  principio della giusta retribuzione sancito    &#13;
 dall'art. 36 della Costituzione, poiché il maggior  onere  economico    &#13;
 verrebbe  a  incidere  direttamente  sulla sua retribuzione, rendendo    &#13;
 meno  remunerato  il  lavoro  svolto  rispetto   all'analogo   lavoro    &#13;
 espletato  dagli  altri  dipendenti.  Sotto  tale  ultimo  aspetto si    &#13;
 coglierebbe, del pari, la violazione del principio di uguaglianza.       &#13;
    La rilevanza della questione consisterebbe nella  possibilità  di    &#13;
 riconoscere  il  diritto  all'indennità  di  missione, oltre il 240°    &#13;
 giorno, solo accogliendo la questione di legittimità  costituzionale    &#13;
 della norma censurata.                                                   &#13;
    3.  - Si è costituito il Presidente del Consiglio dei ministri, a    &#13;
 mezzo dell'Avvocatura generale dello Stato, con atto d'intervento con    &#13;
 il quale si chiede la  declaratoria  d'infondatezza  della  questione    &#13;
 sollevata.                                                               &#13;
    Ha  osservato  l'Avvocatura  che il tribunale remittente ha omesso    &#13;
 del tutto di considerare la natura, la ratio e i limiti dell'istituto    &#13;
 della  missione.  Questa,  infatti,  si  caratterizzerebbe  solo  per    &#13;
 soddisfare  un'esigenza  di carattere transitorio, spettando, in caso    &#13;
 contrario,  una  somma  una  tantum  a  titolo   di   indennità   di    &#13;
 trasferimento  e  di prima sistemazione. Per questa ragione il limite    &#13;
 massimo di permanenza nella medesima località, al fine del godimento    &#13;
 dell'indennità  di  missione,  sarebbe  di  soli  240  giorni,   non    &#13;
 prorogabili.                                                             &#13;
    Coerentemente  con  questa impostazione il Consiglio di Stato, con    &#13;
 la pronuncia in data 9 dicembre 1989, n. 789,  ha  osservato  che  il    &#13;
 pubblico  dipendente  inviato  in missione per un periodo superiore a    &#13;
 quello di 240 giorni indennizzati, "senza la predeterminazione di  un    &#13;
 termine  finale  pari  o inferiore ai 240 giorni, può legittimamente    &#13;
 opporsi, nelle forme e con le modalità consentite  dall'ordinamento,    &#13;
 alla determinazione dell'amministrazione di appartenenza, richiamando    &#13;
 quest'ultima  al  rispetto  del  divieto legislativo". E, perciò, ha    &#13;
 escluso  la   non   manifesta   infondatezza   della   questione   di    &#13;
 costituzionalità  della  norma  censurata  dai giudici del Tribunale    &#13;
 amministrativo regionale della Basilicata.                               &#13;
    In via subordinata, l'Avvocatura ha osservato che l'indennità  di    &#13;
 missione   non   potrebbe   mai   farsi   rifluire  nel  concetto  di    &#13;
 retribuzione, per il carattere temporaneo  ch'essa  deve  avere.  Del    &#13;
 resto,  la  giurisprudenza  della  Corte  Costituzionale avrebbe più    &#13;
 volte  affermato  che  la  tutela  apprestata  dall'art.   36   della    &#13;
 Costituzione non si estenderebbe                                         &#13;
 ad   ogni   compenso,   corrispettivo   d'una  qualsiasi  prestazione    &#13;
 accessoria ovvero corrispettivo di particolari sacrifici previsti per    &#13;
 talune categorie, dovendosi  avere  riguardo  alla  globalità  della    &#13;
 retribuzione  e non ai singoli emolumenti che la compongono (sentenze    &#13;
 nn. 131 del 1982, 176 del 1980 e 141 del 1979).<diritto>Considerato in diritto</diritto>1.  -  Il  Tribunale  amministrativo  regionale  della  Basilicata    &#13;
 dubita,  in  relazione  agli  artt.  36  e 3 della Costituzione della    &#13;
 legittimità costituzionale dell'art. 1, terzo comma, della legge  26    &#13;
 luglio  1978,  n.  417  (Adeguamento  del  trattamento  economico  di    &#13;
 missione e di trasferimento dei dipendenti statali),  che  limita  ai    &#13;
 primi  240  giorni  il  trattamento  di  missione  continuativa nella    &#13;
 medesima località, corrisposto  al  dipendente  in  servizio  presso    &#13;
 altra sede.                                                              &#13;
    2. - La questione è infondata.                                       &#13;
    Com'è  noto,  la  missione  consiste  in  un incarico di servizio    &#13;
 temporaneo  che  l'impiegato  deve  svolgere  fuori  dalla  sua  sede    &#13;
 ordinaria.  Esso  si caratterizza per la transitorietà dell'esigenza    &#13;
 organizzativa, in difetto della quale, dove l'impiego del  dipendente    &#13;
 fuori  della  sede  di  servizio venga disposto in via definitiva, si    &#13;
 deve ritenere trattarsi d'un vero e  proprio  trasferimento.  Con  la    &#13;
 conseguenza  che allo stesso dipendente non competerà il trattamento    &#13;
 economico  di  missione,  ma  una  vera  e  propria   indennità   di    &#13;
 trasferimento e di prima sistemazione.                                   &#13;
    Il  carattere transitorio dell'esigenza organizzativa è previsto,    &#13;
 in via generale per tutti i  pubblici  dipendenti,  dall'articolo  1,    &#13;
 terzo  comma, della legge 26 luglio 1978, n. 417, che limita ai primi    &#13;
 240 giorni il trattamento di  missione  continuativa  nella  medesima    &#13;
 località, corrisposto al dipendente in servizio presso altra sede.      &#13;
    Detta  temporaneità  della missione, inoltre, è stata più volte    &#13;
 ribadita  dalla  giurisprudenza  amministrativa  che   ha   affermato    &#13;
 l'inderogabilità del limite dei 240 giorni.                             &#13;
    Attenendosi   a   questa   linea  giurisprudenziale,  il  collegio    &#13;
 remittente ha prospettato la questione di legittimità costituzionale    &#13;
 di  tale  limite,  ma  dopo  che  essa  era   stata   ritenuta   già    &#13;
 manifestamente infondata dallo stesso Consiglio di Stato, il quale ha    &#13;
 osservato  che  il  pubblico dipendente "può legittimamente opporsi,    &#13;
 nelle forme e con  le  modalità  consentite  dall'ordinamento,  alla    &#13;
 determinazione   dell'amministrazione  di  appartenenza,  richiamando    &#13;
 quest'ultima al rispetto del divieto legislativo".                       &#13;
    3. - Osserva la Corte che la richiesta  di  tutela  da  parte  del    &#13;
 pubblico  dipendente,  il quale lamenti una sua utilizzazione oltre i    &#13;
 limiti massimi di legge in una sede  di  lavoro  posta  in  località    &#13;
 diversa   da   quella  ordinaria,  deve  essere  soddisfatta  con  le    &#13;
 prescritte   forme   e  nelle  competenti  sedi  della  giurisdizione    &#13;
 amministrativa, non certo attraverso un uso improprio del giudizio di    &#13;
 costituzionalità  delle  leggi.  Atteso  che  sia  le  pronunce  dei    &#13;
 tribunali amministrativi regionali, sia quelle del Consiglio di Stato    &#13;
 indicano  forme  e  strumenti  per  la tutela del pubblico dipendente    &#13;
 utilizzato oltre misura con un uso improprio  dello  strumento  della    &#13;
 missione in luogo del trasferimento di sede.                             &#13;
    Deve,  pertanto,  concludersi  per  l'infondatezza  della proposta    &#13;
 questione di costituzionalità.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata, la questione di  legittimità  costituzionale    &#13;
 dell'art.  1,  terzo  comma,  della  legge  26  luglio  1978,  n. 417    &#13;
 (Adeguamento del trattamento economico di missione e di trasferimento    &#13;
 dei dipendenti statali), sollevata, in riferimento agli artt. 36 e  3    &#13;
 della  Costituzione,  dal  Tribunale  amministrativo  regionale della    &#13;
 Basilicata con l'ordinanza in epigrafe.                                  &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 maggio 1992.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 3 giugno 1992.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
