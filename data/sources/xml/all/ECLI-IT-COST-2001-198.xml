<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>198</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:198</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/06/2001</data_decisione>
    <data_deposito>14/06/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di ammissibilità del conflitto tra poteri dello Stato &#13;
sorto  a  seguito  della  delibera  della  Camera  dei  deputati  del &#13;
16 novembre   1999  relativa  alla  insindacabilità  delle  opinioni &#13;
espresse  dall'on. Vittorio  Sgarbi  nei  confronti del dott. Lorenzo &#13;
Matassa,  promosso  dal  tribunale di Caltanissetta I sezione penale, &#13;
con  ricorso depositato il 1° dicembre 2000 ed iscritto al n. 173 del &#13;
registro ammissibilità conflitti. &#13;
    Udito  nella  camera  di  consiglio  del 4 aprile 2001 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto che, con ricorso datato 7 aprile 2000 e depositato nella &#13;
cancelleria   della  Corte  il  1° dicembre  2000,  il  tribunale  di &#13;
Caltanissetta,  I  sezione  penale,  investito  di  un  giudizio  con &#13;
l'imputazione  di  diffamazione  nei  confronti del deputato Vittorio &#13;
Sgarbi, ha sollevato conflitto di attribuzione tra poteri dello Stato &#13;
nei   confronti   della   Camera   dei  deputati  in  relazione  alla &#13;
deliberazione  con la quale l'Assemblea, nella seduta del 16 novembre &#13;
1999  (documento  IV-quater  n. 87),  ha dichiarato che i fatti per i &#13;
quali  era  in  corso  il  procedimento  penale concernevano opinioni &#13;
espresse  da  un  membro  del  Parlamento  nell'esercizio  delle  sue &#13;
funzioni,  in  quanto tali insindacabili (art. 68, primo comma, della &#13;
Costituzione); &#13;
        che  il  tribunale ricorrente ritiene che la deliberazione di &#13;
insindacabilità  riguarderebbe  dichiarazioni  per  le  quali non vi &#13;
sarebbe   il   necessario   nesso  con  la  funzione  parlamentare  e &#13;
menomerebbe,   quindi,   la   sfera  di  attribuzioni  dell'autorità &#13;
giudiziaria investita del giudizio. &#13;
    Considerato  che si deve, in questa fase, delibare esclusivamente &#13;
se  il  ricorso sia ammissibile, valutando, senza contraddittorio tra &#13;
le  parti,  se  sussistono  i requisiti soggettivo ed oggettivo di un &#13;
conflitto di attribuzione tra poteri dello Stato, impregiudicata ogni &#13;
definitiva  decisione  anche  in  ordine all'ammissibilità (art. 37, &#13;
terzo e quarto comma, della legge 11 marzo 1953, n. 87); &#13;
        che,   quanto   al  requisito  soggettivo,  il  tribunale  di &#13;
Caltanissetta,  I  sezione  penale,  è  legittimato  a  sollevare il &#13;
conflitto,  essendo  competente  a dichiarare definitivamente, per il &#13;
procedimento  del  quale  è  investito,  la  volontà del potere cui &#13;
appartiene,  in ragione dell'esercizio delle funzioni giurisdizionali &#13;
svolte in posizione di indipendenza costituzionalmente garantita; &#13;
        che,  parimenti, la Camera dei deputati, che ha deliberato la &#13;
dichiarazione  di  insindacabilità  delle  opinioni  espresse  da un &#13;
proprio membro, è legittimata ad essere parte del conflitto, essendo &#13;
competente  a  dichiarare  definitivamente la volontà del potere che &#13;
rappresenta; &#13;
        che,  per  quanto attiene al profilo oggettivo del conflitto, &#13;
il  tribunale  ricorrente  denuncia la lesione della propria sfera di &#13;
attribuzioni, garantita da norme costituzionali, in conseguenza della &#13;
deliberazione,  che  ritiene  illegittima, con la quale la Camera dei &#13;
deputati  ha  qualificato  le  dichiarazioni del parlamentare, per le &#13;
quali era in corso il giudizio, come insindacabili in quanto comprese &#13;
nell'esercizio  delle  funzioni  parlamentari  (art. 68, primo comma, &#13;
della Costituzione); &#13;
        che,  pertanto,  esiste  la  materia  di  un conflitto la cui &#13;
risoluzione spetta alla competenza della Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  ammissibile, ai sensi dell'art. 37 della legge 11 marzo &#13;
1953,  n. 87,  il conflitto di attribuzione proposto dal tribunale di &#13;
Caltanissetta,  I  sezione  penale,  nei  confronti  della Camera dei &#13;
deputati con il ricorso indicato in epigrafe; &#13;
    Dispone: &#13;
        a)  che  la  cancelleria  della Corte dia comunicazione della &#13;
presente  ordinanza  al  tribunale di Caltanissetta,I sezione penale, &#13;
ricorrente; &#13;
        b)  che  il ricorso e la presente ordinanza siano, a cura del &#13;
ricorrente,  notificati  alla Camera dei deputati entro il termine di &#13;
sessanta  giorni  dalla  comunicazione,  per  essere  successivamente &#13;
depositati,   con   la  prova  delle  eseguite  notificazioni,  nella &#13;
cancelleria  della  Corte  entro  il  termine  di  venti giorni dalla &#13;
notificazione   delle  stesse  (art. 26,  terzo  comma,  delle  norme &#13;
integrative per i giudizi davanti alla Corte costituzionale). &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 6 giugno 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 14 giugno 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
