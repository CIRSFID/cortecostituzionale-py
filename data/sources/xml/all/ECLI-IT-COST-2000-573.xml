<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>573</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:573</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/2000</data_decisione>
    <data_deposito>21/12/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
 Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
 Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
 FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di ammissibilità del conflitto tra poteri dello Stato,    &#13;
 sorto  a  seguito  dell'art.  9,  comma 7, del decreto legislativo 30    &#13;
 luglio  1999,  n. 303 (Ordinamento della Presidenza del Consiglio dei    &#13;
 ministri, a norma dell'art. 11 della legge 15 marzo 1997, n. 59), del    &#13;
 decreto  del  Presidente  del Consiglio dei ministri 23 dicembre 1999    &#13;
 (Disciplina   dell'autonomia   finanziaria   e   contabilità   della    &#13;
 Presidenza  del  Consiglio dei ministri) e del decreto del Presidente    &#13;
 del   Consiglio  dei  ministri  15  aprile  2000  (Ordinamento  delle    &#13;
 strutture  generali  della  Presidenza  del  Consiglio dei ministri),    &#13;
 promosso  dalla  Corte  dei conti con ricorso depositato il 26 giugno    &#13;
 2000 e iscritto al n. 160 del registro ammissibilità conflitti.         &#13;
     Udito  nella  camera di consiglio del 15 novembre 2000 il giudice    &#13;
 relatore Gustavo Zagrebelsky.                                            &#13;
     Ritenuto che il Presidente della Corte dei conti, a seguito delle    &#13;
 deliberazioni  n. 39  del  5  aprile  2000 e n. 48 del 18 maggio 2000    &#13;
 della  Sezione  del  controllo,  ha proposto ricorso per conflitto di    &#13;
 attribuzione  tra  poteri  dello  Stato nei confronti del Governo, in    &#13;
 relazione  all'art.  9,  comma  7,  del decreto legislativo 30 luglio    &#13;
 1999,   n. 303   (Ordinamento  della  Presidenza  del  Consiglio  dei    &#13;
 ministri,  a norma dell'art. 11 della legge 15 marzo 1997, n. 59), al    &#13;
 decreto  del  Presidente  del Consiglio dei ministri 23 dicembre 1999    &#13;
 (Disciplina   dell'autonomia   finanziaria   e   contabilità   della    &#13;
 Presidenza  del  Consiglio  dei ministri) e al decreto del Presidente    &#13;
 del Consiglio dei ministri 15 aprile 2000 (Ordinamento delle strutture   &#13;
 generali della Presidenza del Consiglio dei ministri), per violazione    &#13;
 degli   artt.  76  e  100,  secondo  comma,  della  Costituzione,  in    &#13;
 riferimento  alla legge 14 gennaio 1994, n. 20 (art. 3), e alla legge    &#13;
 15 marzo 1997, n. 59 (artt. 11, comma 1, lettera a), e 12, comma 2);     &#13;
         che,  per  quanto  concerne la legittimazione soggettiva, nel    &#13;
 ricorso   si   richiama   la  giurisprudenza  costituzionale  che  ha    &#13;
 riconosciuto  alla  Corte  dei conti nell'esercizio della funzione di    &#13;
 controllo  la  qualità  di  potere dello Stato (sentenze nn. 406 del    &#13;
 1989, 466 del 1993, 302 del 1995, 457 del 1999);                         &#13;
         che,  con riferimento ai presupposti oggettivi, la ricorrente    &#13;
 richiama  il  precedente  costituito dalla sentenza n. 457 del 1999 a    &#13;
 proposito  dell'ammissibilità  di  conflitti di attribuzione su atti    &#13;
 con forza di legge, affermando in particolare che il conflitto non ha    &#13;
 "ad  oggetto  -  a differenza del giudizio impugnatorio della legge -    &#13;
 l'annullamento  per  illegittimità  costituzionale di un determinato    &#13;
 atto  legislativo",  bensì  si  propone "di ottenere la rimozione di    &#13;
 qualsiasi  comportamento,  atto  o  attività  cui sia ascrivibile la    &#13;
 lesione delle suddette competenze";                                      &#13;
         che,  sulla  materia  oggetto  del  conflitto, nel ricorso si    &#13;
 rileva  che  la  legge  n. 20  del 1994, nell'introdurre una generale    &#13;
 funzione  di controllo sulla gestione, ha mantenuto con il suo art. 3    &#13;
 in  capo alla Corte dei conti il controllo preventivo di legittimità    &#13;
 su  alcune  categorie  di atti del Governo; ma l'art. 9, comma 7, del    &#13;
 decreto  legislativo  n. 303  del  1999  ha  inciso  su tale assetto,    &#13;
 sottraendo  al  controllo  i decreti del Presidente del Consiglio dei    &#13;
 ministri  emanati  in  base  agli artt. 7, 8 e 9 del medesimo decreto    &#13;
 legislativo,   attinenti   all'organizzazione  della  Presidenza  del    &#13;
 Consiglio,  alla sua autonomia contabile e di bilancio e al personale    &#13;
 in essa inquadrato;                                                      &#13;
         che,  inoltre,  i  decreti  del  Presidente del Consiglio dei    &#13;
 ministri  del  23  dicembre 1999 e del 15 aprile 2000 sono, ad avviso    &#13;
 della   ricorrente,   le   "più   cospicue  e  dirette  applicazioni    &#13;
 illegittime"  del  decreto  legislativo  n. 303  del 1999, e pertanto    &#13;
 vengono impugnati assieme a quest'ultimo;                                &#13;
         che,  ancora,  nel  ricorso  viene  lamentata  la  violazione    &#13;
 dell'art.  76  della  Costituzione,  per  eccesso  di delega riguardo    &#13;
 all'emanazione  del  citato  decreto  legislativo n. 303, poiché gli    &#13;
 artt.  11,  comma 1, lettera a), e 12, comma 2, della legge n. 59 del    &#13;
 1997, nel conferire la delega alla razionalizzazione dell'ordinamento    &#13;
 della   Presidenza   del  Consiglio,  non  avrebbero  in  alcun  modo    &#13;
 autorizzato  il  Governo  a  emanare  una  disciplina  riduttiva  del    &#13;
 controllo  preventivo  di  legittimità,  disciplina  che rappresenta    &#13;
 dunque  "una  non  consentita  incisione del regime dei controlli sui    &#13;
 "nuovi atti amministrativi del Governo";                                 &#13;
         che,    inoltre,    i   tre   atti   impugnati   violerebbero    &#13;
 complessivamente  le  attribuzioni  conferite  alla  Corte  dei conti    &#13;
 dall'art.  100,  secondo comma, della Costituzione, sottraendo a essa    &#13;
 competenze   che   la  ricorrente  ritiene  definite  da  un  "quadro    &#13;
 costituzionale   rigido   ed  immodificabile"  che  risulterebbe  dal    &#13;
 parallelismo  inderogabile  tra  il complesso degli artt. 92-95 della    &#13;
 Costituzione  e  i  termini  soggettivi e oggettivi della funzione di    &#13;
 controllo.                                                               &#13;
     Considerato   che   la  Corte  dei  conti,  in  persona  del  suo    &#13;
 Presidente,  sulla  base  delle  determinazioni  nn. 39 e 48 del 2000    &#13;
 della  Sezione  del  controllo, ha proposto conflitto di attribuzioni    &#13;
 tra poteri dello Stato contro il Governo della Repubblica, in persona    &#13;
 del  Presidente  del Consiglio dei ministri, in relazione all'art. 9,    &#13;
 comma  7, del decreto legislativo 30 luglio 1999, n. 303 (Ordinamento    &#13;
 della  Presidenza  del  Consiglio  dei ministri, a norma dell'art. 11    &#13;
 della  legge  15  marzo  1997,  n. 59), al decreto del Presidente del    &#13;
 Consiglio  dei  Ministri  23 dicembre 1999 (Disciplina dell'autonomia    &#13;
 finanziaria   e  contabilità  della  Presidenza  del  Consiglio  dei    &#13;
 ministri)  e  al decreto del Presidente del Consiglio dei ministri 15    &#13;
 aprile  2000  (Ordinamento  delle strutture generali della Presidenza    &#13;
 del  Consiglio  dei  ministri),  per violazione degli artt. 76 e 100,    &#13;
 secondo comma, della Costituzione, in relazione alla legge 14 gennaio    &#13;
 1994,  n. 20  (art.  3)  e alla legge 15 marzo 1997, n. 59 (artt. 11,    &#13;
 comma 1, lettera a), e 12, comma 2);                                     &#13;
         che,  nella presente fase del giudizio, a norma dell'art. 37,    &#13;
 terzo  e quarto comma, della legge 11 marzo 1953, n. 87, questa Corte    &#13;
 è  chiamata  a  deliberare senza contraddittorio sull'ammissibilità    &#13;
 del  ricorso  sotto  il  profilo  dell'esistenza della "materia di un    &#13;
 conflitto la cui risoluzione spetti alla sua competenza";                &#13;
         che,  dal  punto  di  vista  dei presupposti soggettivi, alla    &#13;
 Corte  dei  conti,  nell'esercizio  della  sua  funzione di controllo    &#13;
 preventivo   di  legittimità  sugli  atti  del  Governo,  spetta  la    &#13;
 legittimazione  a proporre conflitto costituzionale di attribuzioni a    &#13;
 norma  dell'art. 134 della Costituzione, in quanto tale funzione, sia    &#13;
 pure  di  natura  ausiliare,  è  caratterizzata, oltre che dalla sua    &#13;
 previsione  nell'art.  100,  secondo comma, della Costituzione, dalla    &#13;
 posizione  di  piena  indipendenza dell'organo chiamato a esercitarla    &#13;
 (sentenze nn. 406 del 1989 e 302 del 1995);                              &#13;
         che,  con riferimento ai presupposti oggettivi, il ricorso è    &#13;
 indirizzato  alla garanzia della sfera di attribuzioni determinata da    &#13;
 norme   costituzionali,  in  quanto  la  lesione  lamentata  concerne    &#13;
 competenze  della  Corte dei conti configurate dalla legge 14 gennaio    &#13;
 1994,  n. 20,  riconducibili  alla  previsione dell'art. 100, secondo    &#13;
 comma, della Costituzione;                                               &#13;
         che,  circa il profilo dell'idoneità a determinare conflitto    &#13;
 di  atti  aventi  natura  legislativa,  quali  il decreto legislativo    &#13;
 n. 303  del 1999 in questione, questa Corte ha già dato una risposta    &#13;
 affermativa  (sentenza  n. 457  del  1999; ordinanze nn. 280 e 23 del    &#13;
 2000, 323 del 1999);                                                     &#13;
         che  dal  ricorso è dato rilevare le ragioni del conflitto e    &#13;
 le  norme  costituzionali  che  regolano  la  materia, secondo quanto    &#13;
 prescrive  l'art.  26,  primo  comma,  delle  norme integrative per i    &#13;
 giudizi davanti alla Corte costituzionale;                               &#13;
         che pertanto il ricorso deve essere dichiarato ammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara  ammissibile, ai sensi dell'art. 37 della legge 11 marzo    &#13;
 1953, n. 87, nei confronti del Governo della Repubblica, il conflitto    &#13;
 di  attribuzioni  proposto  dalla  Corte  dei  conti  con  il ricorso    &#13;
 indicato in epigrafe;                                                    &#13;
     Dispone:                                                             &#13;
         a)   che   la  cancelleria  di  questa  Corte  dia  immediata    &#13;
 comunicazione della presente ordinanza all'organo ricorrente;            &#13;
         b)  che,  a  cura  del  ricorrente,  il ricorso e la presente    &#13;
 ordinanza  siano  notificati  al Governo della Repubblica, in persona    &#13;
 del  Presidente  del  Consiglio  dei  ministri,  entro  il termine di    &#13;
 sessanta  giorni  dalla  comunicazione  di  cui  sub  a),  per essere    &#13;
 successivamente  depositati  presso  la  cancelleria  di questa Corte    &#13;
 entro  il  termine  fissato  dall'art.  26,  terzo comma, delle norme    &#13;
 integrative per i giudizi davanti alla Corte costituzionale.             &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 dicembre 2000.                             &#13;
                      Il Presidente: Santosuosso                          &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 14 dicembre 2000.                       &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
