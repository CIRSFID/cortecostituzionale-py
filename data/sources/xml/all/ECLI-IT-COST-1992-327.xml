<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>327</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:327</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/06/1992</data_decisione>
    <data_deposito>08/07/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale  dell'articolo  13,  comma    &#13;
 primo,  del  decreto-legge 5 maggio 1957, n. 271 (Disposizioni per la    &#13;
 prevenzione e la  repressione  delle  frodi  nel  settore  degli  oli    &#13;
 minerali)  convertito,  con modificazioni, nella legge 2 luglio 1957,    &#13;
 n. 474, come sostituito dall'articolo  21  della  legge  31  dicembre    &#13;
 1962,   n.   1852  (Modificazioni  al  regime  fiscale  dei  prodotti    &#13;
 petroliferi) promossi con ordinanze emesse  il  21  ottobre,  18,  27    &#13;
 novembre, 6 dicembre 1991 e 12 febbraio 1992 dal Pretore di Prato nei    &#13;
 procedimenti  penali  a carico di Mazzi Giancarlo, ed altri, iscritte    &#13;
 rispettivamente ai  nn.  109,  133,  134,  193  e  185  del  registro    &#13;
 ordinanze 1992 e pubblicata nelle Gazzette Ufficiali della Repubblica    &#13;
 nn. 10, 12 e 16, prima serie speciale, dell'anno 1992;                   &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  17  giugno  1992  il  Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso  di  un  procedimento penale a carico di    &#13;
 Giancarlo Mazzi, imputato del reato di cui all'art. 13, primo  comma,    &#13;
 d.l.  5  maggio  1957,  n. 271, convertito in legge 2 luglio 1957, n.    &#13;
 474, il Pretore di Prato con ordinanza del 21 ottobre 1991 (reg. ord.    &#13;
 n. 109 del 1992) sollevava, in riferimento all'art. 27, terzo  comma,    &#13;
 della  Costituzione,  questione  di legittimità costituzionale della    &#13;
 norma ora citata, secondo cui "chiunque esercita un deposito  di  oli    &#13;
 minerali  carburanti,  combustibili  o  lubrificanti, una stazione di    &#13;
 servizio o un apparecchio di distribuzione automatica di  carburanti,    &#13;
 non  denunciati  a  termini  dell'art.  1, è punito con la multa dal    &#13;
 doppio al decuplo  dell'imposta  relativa  ai  prodotti  trovati  nel    &#13;
 deposito,  nella  stazione di servizio o nel distributore automatico,    &#13;
 e, in ogni caso, non inferiore a lire 300.000";                          &#13;
      che il Pretore rilevava che nel caso di specie gli oli  minerali    &#13;
 non denunciati corrispondevano a Kg. 4.085 di olio combustibile denso    &#13;
 ed  a Kg. 52 di olio diatermico, con un minimo di pena applicabile di    &#13;
 Lire 121.281.170;                                                        &#13;
      che  a  suo   avviso   tale   sanzione   era   irragionevolmente    &#13;
 sproporzionata alla gravità del fatto, tenendo conto anche di quanto    &#13;
 affermato  da  questa Corte con sentenza n. 313 del 1990, ciò che lo    &#13;
 induceva a dubitare del contrasto tra la norma  denunciata  e  l'art.    &#13;
 27, terzo comma, della Costituzione;                                     &#13;
      che  il  medesimo  Pretore  sollevava  la  stessa  questione con    &#13;
 ordinanze del 18 novembre 1991  (reg.  ord.  n.  133  del  1992,  nel    &#13;
 procedimento  contro Piero Franco Baroni), del 27 novembre 1991 (reg.    &#13;
 ord. n. 134 del 1992, nel procedimento contro Rosalinda  Lombardi  ed    &#13;
 altri) e 6 dicembre 1991 (reg. ord. n. 193 del 1991, nel procedimento    &#13;
 contro Vincenzo La Porta ed altro);                                      &#13;
      che  sempre  la stessa questione veniva sollevata dal Pretore di    &#13;
 Prato con ordinanza del 12 febbraio 1992 (reg. ord. n. 185 del  1992,    &#13;
 nel  procedimento  contro  Gabriele  Maurizio Guasti ed altro), nella    &#13;
 quale il Pretore afferma di conoscere l'ordinanza di questa Corte  n.    &#13;
 427   del  1991,  dichiarativa  della  manifesta  infondatezza  della    &#13;
 questione,  e  tuttavia  ritiene  di  prospettare   il   profilo   di    &#13;
 incostituzionalità   costituito   dall'inadeguatezza   della   norma    &#13;
 impugnata alla finalità rieducativa della pena;                         &#13;
      che la Presidenza del Consiglio  dei  ministri,  intervenuta  in    &#13;
 tutte  le cause, chiedeva dichiararsi la manifesta infondatezza della    &#13;
 questione, richiamando la sentenza di questa Corte n. 887 del 1988  e    &#13;
 l'ordinanza n. 129 del 1989;                                             &#13;
    Considerato   che   i   giudizi  debbono  essere  riuniti,  stante    &#13;
 l'identicità delle questioni;                                           &#13;
      che la sussistenza del reato di  cui  all'art.  13  del  decreto    &#13;
 legge 5 maggio 1957, n. 271, di omessa denuncia di un deposito di oli    &#13;
 minerali   destinato   al   consumo   diretto   dipende,  secondo  la    &#13;
 giurisprudenza della Cassazione, dalla  capacità  del  deposito  (in    &#13;
 quanto  solo  quelli  di  una certa consistenza possono costituire un    &#13;
 più facile veicolo per le frodi  fiscali),  mentre,  ai  fini  della    &#13;
 relativa  sanzione,  rileva  la  quantità di prodotto effettivamente    &#13;
 introdotta nello stesso deposito ovvero estratta;                        &#13;
      che, sotto quest'ultimo aspetto,  il  trattamento  sanzionatorio    &#13;
 appare  non  irragionevolmente commisurato alla quantità di prodotto    &#13;
 effettivamente transitata piuttosto che alla capacità del  deposito,    &#13;
 e  ciò  anche  in relazione alla natura permanente del reato, da cui    &#13;
 consegue la punibilità di tutta  la  condotta  mantenuta  nel  corso    &#13;
 della situazione illecita;                                               &#13;
      che, pertanto, poiché la gravità del reato è commisurata alla    &#13;
 quantità  del  prodotto  complessivamente  immesso nei recipienti, a    &#13;
 nulla  rileva  -  al  fine  di  censurare  il  relativo   trattamento    &#13;
 sanzionatorio   in   riferimento  all'art.  27,  terzo  comma,  della    &#13;
 Costituzione - la circostanza che la quantità di olio rinvenuta  nel    &#13;
 deposito appartenente all'imputato sia stata esigua;                     &#13;
      che   in  tal  senso  questa  Corte  già  si  è  espressa  con    &#13;
 l'ordinanza n. 497 del 1991;                                             &#13;
      che a diverso avviso non può indurre il richiamo,  operato  dal    &#13;
 giudice  rimettente,  alla  sentenza di questa Corte n. 313 del 1990,    &#13;
 nella  quale  sono  indicate  alcune  caratteristiche  generali   del    &#13;
 principio    di    proporzionalità    della   pena,   poiché   tali    &#13;
 caratteristiche sono riscontrabili nella norma censurata, tenuto conto   &#13;
 delle modalità di commisurazione della pena,  tali  da  superare  il    &#13;
 dubbio di inadeguatezza della sanzione alla finalità rieducativa;       &#13;
      che  pertanto  la questione dev'essere dichiarata manifestamente    &#13;
 infondata;                                                               &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale  dell'art.  13  del  decreto    &#13;
 legge  5  maggio  1957  n.  271 (Disposizioni per la prevenzione e la    &#13;
 repressione delle frodi nel settore degli oli  minerali),  convertito    &#13;
 in legge 2 luglio 1957 n. 474, sollevata, in riferimento all'art. 27,    &#13;
 terzo comma, della Costituzione dal Pretore di Prato con le ordinanze    &#13;
 indicate in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 29 giugno 1992.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria l'8 luglio 1992.                            &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
