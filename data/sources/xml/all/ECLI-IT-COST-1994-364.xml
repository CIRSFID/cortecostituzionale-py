<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>364</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:364</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PESCATORE</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/07/1994</data_decisione>
    <data_deposito>27/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Gabriele PESCATORE, &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, &#13;
 prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 34, quinto    &#13;
 comma, della  legge  30  dicembre  1991,  n.  413  (Disposizioni  per    &#13;
 ampliare   le  basi  imponibili,  per  razionalizzare,  facilitare  e    &#13;
 potenziare  l'attività  di   accertamento;   disposizioni   per   la    &#13;
 rivalutazione  obbligatoria  dei beni immobili delle imprese, nonché    &#13;
 per riformare il contenzioso  e  per  la  definizione  agevolata  dei    &#13;
 rapporti  tributari  pendenti;  delega al Presidente della Repubblica    &#13;
 per la concessione di amnistia per reati tributari;  istituzioni  dei    &#13;
 centri  di  assistenza  fiscale  e  del  conto fiscale), promosso con    &#13;
 ordinanza emessa il 20 marzo 1992  dalla  Commissione  tributaria  di    &#13;
 primo  grado  di  Milano  sui  ricorsi  riuniti proposti dalla s.p.a.    &#13;
 SERFIN  -  Servizi  finanziari  Aziendali  contro  l'Ufficio  imposte    &#13;
 dirette  di  Milano,  iscritta al n. 96 del registro ordinanze 1994 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  12,  prima    &#13;
 serie speciale, dell'anno 1994.                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio  dell'8  giugno  1994  il  Giudice    &#13;
 relatore Massimo Vari;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Con ordinanza del 20 marzo 1992 la Commissione tributaria di    &#13;
 primo grado di Milano, nel corso  di  un  giudizio  instaurato  dalla    &#13;
 SERFIN   -   Servizi   finanziari   aziendali  s.p.a.  nei  confronti    &#13;
 dell'Amministrazione  finanziaria,  ha  sollevato,   in   riferimento    &#13;
 all'art.    3   della   Costituzione,   questione   di   legittimità    &#13;
 costituzionale dell'art. 34, quinto comma, della  legge  30  dicembre    &#13;
 1991, n. 413.                                                            &#13;
    Il  giudice remittente assume che la norma censurata, subordinando    &#13;
 la sospensione del giudizio alla dichiarazione del  contribuente,  da    &#13;
 rendere in udienza, di volersi avvalere del condono tributario di cui    &#13;
 alla  medesima  legge n. 413 del 1991, avrebbe l'effetto di ridurre i    &#13;
 termini   per   inoltrare  la  relativa  domanda,  discriminando,  in    &#13;
 violazione dell'art. 3 della  Costituzione,  il  contribuente  stesso    &#13;
 rispetto  "agli  altri  contribuenti  che  non  versano  nella stessa    &#13;
 situazione". Secondo l'ordinanza  la  data  dell'udienza  avrebbe  un    &#13;
 effetto  di  "sbarramento",  nel  senso  che  se  il contribuente "ha    &#13;
 dichiarato di volersi avvalere del condono, non può più tornare sui    &#13;
 suoi passi", mentre "se non ha dichiarato  di  volersi  avvalere  del    &#13;
 condono,  non  può  più  farlo".  Ciò  in  quanto  il principio di    &#13;
 economia processuale  porta  ad  escludere  che  "il  giudizio  possa    &#13;
 proseguire  inutilmente  (nel  caso  di  dichiarazione di non volersi    &#13;
 avvalere del condono) o essere sospeso pretestuosamente (nel caso  di    &#13;
 dichiarazione di volersi avvalere del condono)".                         &#13;
    2.  -  È  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato e difeso dall'Avvocatura generale dello Stato,  che  ha    &#13;
 dedotto l'infondatezza della questione.                                  &#13;
    Secondo  la  memoria, la dichiarazione, da rendere all'udienza, ha    &#13;
 soltanto lo scopo di impedire l'assunzione in decisione della  causa,    &#13;
 fermo  restando  il  termine  ordinario  "per  presentare all'ufficio    &#13;
 domanda di condono e conseguentemente  procedere  al  pagamento"  del    &#13;
 dovuto.  Ottenuta, con la dichiarazione, la sospensione del giudizio,    &#13;
 il contribuente può ancora  scegliere,  nel  termine  ordinario,  se    &#13;
 presentare  o  meno la dichiarazione integrativa. Se questa non viene    &#13;
 presentata, cessa la causa di sospensione e il giudizio  riprende  il    &#13;
 suo corso.<diritto>Considerato in diritto</diritto>1.   -   La  Corte  è  chiamata  a  decidere  della  legittimità    &#13;
 costituzionale dell'art. 34, quinto comma, della  legge  30  dicembre    &#13;
 1991,  n.  413,  con  la  quale  sono  state dettate disposizioni per    &#13;
 agevolare la definizione  delle  situazioni  e  pendenze  tributarie.    &#13;
 Secondo  il  giudice  a quo, la norma impugnata, nel prevedere che "i    &#13;
 giudizi per i quali sia stata fissata l'udienza di  discussione  sono    &#13;
 sospesi  nell'udienza  medesima  a  richiesta  del  contribuente  che    &#13;
 dichiari di volersi avvalere delle disposizioni" in tema  di  condono    &#13;
 tributario,  ridurrebbe  alla  data  dell'udienza  i  termini  per la    &#13;
 presentazione  della  relativa  domanda,  provocando,  in  violazione    &#13;
 dell'art.  3  della  Costituzione, una disparità di trattamento "tra    &#13;
 chi ha una udienza già fissata e chi deve ancora essere convocato".     &#13;
    2. - La questione non è fondata.                                     &#13;
    La norma denunciata si  dà  carico  di  raccordare  la  procedura    &#13;
 amministrativa di condono con i processi tributari in svolgimento.       &#13;
    Essa  perciò  prevede la sospensione dei giudizi e dei termini di    &#13;
 ricorso o di impugnativa fino alla data  ultima  prevista  per  poter    &#13;
 chiedere  la  definizione delle pendenze, stabilendo, altresì, che i    &#13;
 giudizi per i quali sia stata fissata l'udienza di discussione  siano    &#13;
 sospesi  nell'udienza  medesima  a  richiesta  del  contribuente  che    &#13;
 dichiari di volersi avvalere della facoltà di  chiedere  il  condono    &#13;
 tributario.                                                              &#13;
    Nel  sistema delineato dal legislatore, è da escludere, tuttavia,    &#13;
 che tale richiesta di sospensione produca il  lamentato  effetto.  Il    &#13;
 termine  per  la presentazione dell'atto che dà avvio alla procedura    &#13;
 per la definizione della pendenza,  e  cioè  la  c.d.  dichiarazione    &#13;
 integrativa  prevista  dalla  norma  denunciata,  lungi  dal ridursi,    &#13;
 resta, infatti, quello stabilito,  in  via  ordinaria,  per  tutti  i    &#13;
 contribuenti,  come è dato evincere dal secondo periodo dello stesso    &#13;
 art. 34, quinto comma. Successivamente  alla  predetta  data,  viene,    &#13;
 invero,   emessa   ordinanza   di   estinzione   del   giudizio  solo    &#13;
 subordinatamente  all'esibizione   di   copia   della   dichiarazione    &#13;
 integrativa  e  della  ricevuta  comprovante  la consegna all'ufficio    &#13;
 postale   della   lettera   raccomandata   di   trasmissione    della    &#13;
 dichiarazione stessa.                                                    &#13;
   In  conclusione,  è  da  reputare  erronea la premessa dalla quale    &#13;
 muove il giudice  a  quo,  in  quanto  la  richiesta  prevista  dalla    &#13;
 disposizione  sottoposta  all'esame della Corte vale solo ad ottenere    &#13;
 la sospensione del giudizio, fermo restando che, fino  alla  scadenza    &#13;
 del  termine  ordinario,  questo contribuente può ancora decidere se    &#13;
 presentare  o  meno  la  dichiarazione  integrativa,   onde   nessuna    &#13;
 disparità  di  trattamento  egli subisce, sotto il profilo in esame,    &#13;
 rispetto  a  tutti  gli  altri.  Va  da  sé  che,  ove  quest'ultima    &#13;
 dichiarazione  non venga presentata, il giudizio, cessata la causa di    &#13;
 sospensione, riprende il suo corso.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  34,  quinto  comma,  della  legge 30 dicembre 1991, n. 413    &#13;
 (Disposizioni per ampliare le basi  imponibili,  per  razionalizzare,    &#13;
 facilitare e potenziare l'attività di accertamento; disposizioni per    &#13;
 la  rivalutazione  obbligatoria  dei  beni  immobili  delle  imprese,    &#13;
 nonché per riformare il contenzioso e per la  definizione  agevolata    &#13;
 dei   rapporti   tributari   pendenti;  delega  al  Presidente  della    &#13;
 Repubblica per  la  concessione  di  amnistia  per  reati  tributari;    &#13;
 istituzioni  dei  centri  di assistenza fiscale e del conto fiscale),    &#13;
 sollevata,  in  riferimento  all'art.  3  della   Costituzione,   con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 luglio 1994.                               &#13;
                       Il Presidente: PESCATORE                           &#13;
                          Il redattore: VARI                              &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 27 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
