<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1973</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:1973:19</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Luigi Oggioni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/02/1973</data_decisione>
    <data_deposito>27/02/1973</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Dott. &#13;
 GIUSEPPE VERZÌ - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - &#13;
 Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  509 del  &#13;
 codice di procedura penale, promosso con ordinanza emessa il 10  aprile  &#13;
 1970  dal  pretore di Varallo Sesia nel procedimento penale a carico di  &#13;
 Pisciotta Alfonso, iscritta al n. 21  del  registro  ordinanze  1971  e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 74 del 24 marzo  &#13;
 1971.                                                                    &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 7 febbraio 1973 il Giudice relatore  &#13;
 Luigi Oggioni;                                                           &#13;
     udito il sostituto avvocato generale dello Stato Michele  Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  procedimento  penale a carico di Pisciotta Alfonso, condannato  &#13;
 con decreto per il reato di acquisto di cose di  sospetta  provenienza,  &#13;
 il  pretore  di  Varallo  Sesia con ordinanza emessa il 10 aprile 1970,  &#13;
 premesso  che  il  condannato  aveva  proposto  opposizione   omettendo  &#13;
 l'enunciazione dei motivi e la richiesta di dibattimento, in violazione  &#13;
 dell'art.    509 c.p.p., e che, pertanto, la opposizione stessa avrebbe  &#13;
 dovuto essere  dichiarata  inammissibile,  ha  sollevato  questione  di  &#13;
 legittimità  costituzionale  della  norma  processuale  suddetta,  per  &#13;
 assunto contrasto con il diritto di difesa garantito dall'art. 24 della  &#13;
 Costituzione.                                                            &#13;
     In proposito il pretore, rilevato che il procedimento  per  decreto  &#13;
 sarebbe  stato  ritenuto  dalla  giurisprudenza  di questa Corte non in  &#13;
 contrasto col diritto di difesa in quanto questo  può  trovare  tutela  &#13;
 nel  giudizio  sull'opposizione,  osserva  che  la previsione a pena di  &#13;
 inammissibilità  delle  suddette  modalità  determinerebbe invece una  &#13;
 grave lesione del diritto di difesa proprio nella  fase  dibattimentale  &#13;
 conseguente all'opposizione dell'imputato.                               &#13;
     Invero  con l'imposizione di requisiti che, come quelli menzionati,  &#13;
 vadano  oltre  la  semplice  dichiarazione  di   opposizione,   nonché  &#13;
 l'indicazione  del decreto opposto ed il rispetto dei termini relativi,  &#13;
 l'opposizione al decreto penale sarebbe sostanzialmente  equiparata  ad  &#13;
 un'impugnazione,  mentre  non  vi  sarebbe  stato  un giudizio di primo  &#13;
 grado,  ed  in  tal  modo  si  determinerebbe  una  limitazione   della  &#13;
 possibilità  dell'imputato  di  avvalersi  del  diritto  di difesa nel  &#13;
 dibattimento che, per la prima volta, viene celebrato a suo carico.      &#13;
     Avanti alla Corte costituzionale si è costituito il Presidente del  &#13;
 Consiglio  dei  ministri  rappresentato  e  difeso,  come  per   legge,  &#13;
 dall'Avvocatura generale dello Stato, che ha depositato tempestivamente  &#13;
 le proprie deduzioni.                                                    &#13;
     L'Avvocatura osserva che la legittimità costituzionale delle norme  &#13;
 che  disciplinano  il  giudizio  per  decreto,  ivi compresa quella ora  &#13;
 impugnata, sarebbe  già  stata  affermata  dalla  Corte,  che  avrebbe  &#13;
 riconosciuto  conforme  alla  garanzia  del diritto di difesa il rinvio  &#13;
 dell'esercizio del diritto stesso al vero e  proprio  giudizio  che  si  &#13;
 svolge   a   seguito  dell'opposizione,  dato  che  tale  rinvio  trova  &#13;
 giustificazione  nelle  peculiari  caratteristiche   di   rapidità   e  &#13;
 semplicità del procedimento in esame.                                   &#13;
     E  siccome  la  Corte avrebbe anche riconosciuto compatibile con il  &#13;
 rispetto della detta garanzia la inammissibilità dell'opposizione  nel  &#13;
 caso   di   mancata   comparizione  dell'opponente  all'udienza,  senza  &#13;
 giustificazione, in quanto tale comportamento processuale costituirebbe  &#13;
 l'ammissione  che  è  venuto  a  mancare   l'interesse   a   coltivare  &#13;
 l'opposizione,   dovrebbe   a   maggior   ragione   ritenersi   che  la  &#13;
 dichiarazione di  opposizione  senza  la  specificazione  dei  relativi  &#13;
 motivi  denoterebbe chiaramente la carenza dell'interesse dell'imputato  &#13;
 ad affrontare il giudizio.                                               &#13;
     Conclude pertanto  chiedendo  dichiararsi  infondata  la  sollevata  &#13;
 questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Si  assume  nell'ordinanza  che  l'art.  509  del  codice di  &#13;
 procedura penale, prescrivendo che l'opponente a decreto  penale  debba  &#13;
 richiedere,  nella  dichiarazione  di  opposizione,  il dibattimento ed  &#13;
 indicare specificamente i motivi dell'opposizione stessa, violerebbe il  &#13;
 diritto di difesa  garantito  dall'art.  24  della  Costituzione.  Ciò  &#13;
 perché   tratterebbesi  di  richieste  ed  indicazioni  "razionalmente  &#13;
 superflue", sia in quanto opposizione e richiesta di dibattimento  sono  &#13;
 praticamente  equivalenti,  sia  in  quanto  non  vige  in  materia  il  &#13;
 principio che attiene  ai  gravami,  cosiddetti  devolutivi.  Pertanto,  &#13;
 l'imposizione  dei predetti requisiti e la conseguente inammissibilità  &#13;
 dell'opposizione in caso di loro  carenza,  si  risolverebbero  in  una  &#13;
 ingiustificata situazione ostativa dell'esercizio degli ampi diritti di  &#13;
 difesa riservati in sede dibattimentale.                                 &#13;
     2.  -  La Corte premette che il procedimento per decreto penale nel  &#13;
 suo  complesso,  certamente  conforme  alla  Costituzione  ed  efficace  &#13;
 strumento  di  realizzazione  di  un  rapido  giudizio,  ha particolare  &#13;
 struttura, regolata nel Capo IV del  codice  di  rito  tra  i  "giudizi  &#13;
 speciali" con normativa sulla opposizione (art. 509) distinta e diversa  &#13;
 dalla normativa sulla impugnazione mediante appello (art. 515). Mentre,  &#13;
 per   quest'ultimo,   i   motivi  proposti  delimitano  l'ambito  della  &#13;
 decisione, non è altrettanto per l'impugnativa di decreto  penale,  la  &#13;
 quale,   con   l'apertura  del  contraddittorio,  consente,  nel  pieno  &#13;
 esercizio  dei  diritti  di  difesa,  una  cognizione   ex   novo   del  &#13;
 fatto-reato, con autonomia di effetti, eventualmente anche peggiorativi  &#13;
 (articolo 510, secondo comma).                                           &#13;
     L'opposizione  de  qua,  nell'ambito del sistema, viene, quindi, ad  &#13;
 assumere una sua particolare configurazione.                             &#13;
     3. - Ciò premesso, la Corte osserva che, per  quanto  concerne  la  &#13;
 sollevata  questione  di costituzionalità dell'art. 509 nella parte in  &#13;
 cui prescrive che la dichiarazione di opposizione  debba  contenere  la  &#13;
 richiesta  di  dibattimento,  è  lo  stesso  articolo  ad escludere la  &#13;
 conseguenza della inammissibilità per l'omissione di formale richiesta  &#13;
 in tal senso, risultando tale  conseguenza  testualmente  riservata  al  &#13;
 caso della mancata specificazione dei motivi.                            &#13;
     La  giurisprudenza  della Cassazione ha ritenuto che l'enunciazione  &#13;
 della predetta richiesta debba considerarsi implicita e sottintesa  nel  &#13;
 fatto,  di  per  se  dimostrativo,  della  manifestazione di opporsi al  &#13;
 decreto.                                                                 &#13;
     Ne consegue la non fondatezza della questione.                       &#13;
     4. - Per quanto riguarda l'altro aspetto, concernente l'indicazione  &#13;
 dei motivi, la Corte  non  ritiene  del  tutto  esatta  l'affermazione,  &#13;
 contenuta  nell'ordinanza,  che  trattisi  di  requisito "razionalmente  &#13;
 superfluo". Non è superfluo perché, anche nell'interesse dello stesso  &#13;
 opponente, tende a conferire,  ad  ogni  futuro  ed  eventuale  effetto  &#13;
 valutativo  di  comportamento, veste di attendibilità all'atto con cui  &#13;
 si richiede il passaggio dalla fase senza contraddittorio a  quella  in  &#13;
 contraddittorio.   Ciò   senza   che,   come   parimenti  ritenuto  in  &#13;
 giurisprudenza,   sia    necessario    puntualizzare    gli    elementi  &#13;
 dell'opposizione, mantenendo così una rispondenza con la "sommarietà"  &#13;
 dei  motivi  da  indicare nel decreto opposto (art. 507, n. 3, c.p.p.),  &#13;
 anche considerato che lo stesso  interessato  ha  facoltà  di  opporsi  &#13;
 personalmente senza l'assistenza di legale (art. 509).                   &#13;
     Di  superfluità  può parlarsi soltanto nel significato di una non  &#13;
 incidenza in qualsiasi  direzione,  positiva  o  negativa,  dei  motivi  &#13;
 indicati  sugli  sviluppi successivi della procedura di opposizione, il  &#13;
 che è stato accennato in precedenza. La quale procedura, come indicato  &#13;
 nella sentenza n. 189 del 1972,  "sostanzialmente  si  risolve  in  una  &#13;
 richiesta di dibattimento": e, come si è visto, la mancanza di formale  &#13;
 richiesta in tal senso non conduce alla inammissibilità.                &#13;
     5.  -  Così ridimensionata l'esigenza della indicazione dei motivi  &#13;
 di opposizione, la questione posta  dall'ordinanza  va  considerata  in  &#13;
 funzione   della   legittimità   della  sanzione,  che  consegue  alla  &#13;
 inosservanza del precetto (sia  sotto  il  profilo  della  inosservanza  &#13;
 totale  che  parziale),  in  quanto assolutamente ostativa a priori del  &#13;
 libero ed ampio esercizio successivo dei diritti di difesa.              &#13;
     La Corte osserva che la limitata finalità  dell'onere  processuale  &#13;
 in esame e la sua circoscritta portata, poste a paragone della gravità  &#13;
 e  drasticità  delle  conseguenze  impeditive  comminate,  denotano la  &#13;
 sproporzione tra obbligo e sanzione e  l'incongruità  che  l'esercizio  &#13;
 dell'essenziale  diritto  della  difesa  giudiziale  in contradditorio,  &#13;
 debba essere precluso di fronte all'inadempimento di un onere  che  ha,  &#13;
 bensì,  una  sua  ragion d'essere, ma che tuttavia non è rilevante ai  &#13;
 fini processualistici.                                                   &#13;
     La  possibilità  di variazione e di adattamento delle modalità di  &#13;
 esercizio  del   diritto   di   difesa   a   seconda   delle   speciali  &#13;
 caratteristiche   strutturali   dei   singoli  procedimenti,  è  stata  &#13;
 riconosciuta valida dalla giurisprudenza di questa Corte  (sentenza  n.  &#13;
 55  del  1971):  ma  ciò,  tuttavia, nell'ambito delle caratteristiche  &#13;
 stesse, con modalità che a queste si adattino e  non  ne  prescindano,  &#13;
 come accade, invece, nella situazione in esame.                          &#13;
     Di  conseguenza,  va  dichiarata  l'illegittimità  dell'art.   509  &#13;
 c.p.p. nella parte riguardante l'inciso "a pena di inammissibilità".</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     a) dichiara l'illegittimità costituzionale  dell'art.  509,  commi  &#13;
 secondo  e  terzo,  del  codice di procedura penale, nella parte in cui  &#13;
 prevede   che   alla   mancata   indicazione   dei   motivi    consegue  &#13;
 l'inammissibilità dell'opposizione;                                     &#13;
     b) dichiara non fondata la questione di legittimità costituzionale  &#13;
 dell'art. 509 del codice di procedura penale, nella parte relativa alla  &#13;
 richiesta  di  dibattimento,  questione  sollevata  con  l'ordinanza in  &#13;
 epigrafe dal pretore di Varallo Sesia in riferimento all'art. 24  della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 14 febbraio 1973.                             &#13;
                                   GIUSEPPE CHIARELLI - GIUSEPPE   VERZÌ  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO  AMADEI - GIULIO GIONFRIDA -  &#13;
                                   EDOARDO VOLTERRA.                      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
