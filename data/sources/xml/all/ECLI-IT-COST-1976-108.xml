<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>108</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:108</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Ercole Rocchetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/04/1976</data_decisione>
    <data_deposito>06/05/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale dell'art. 25 del  &#13;
 r.d.  28  maggio 1931, n. 602 (disposizioni di attuazione del codice di  &#13;
 procedura penale), in relazione all'art. 495, terzo comma,  n.  2,  del  &#13;
 codice penale, promossi con le seguenti ordinanze:                       &#13;
     1)  ordinanza  emessa il 18 dicembre 1973 dal tribunale di Tolmezzo  &#13;
 nel procedimento penale a carico di Bastiani Manlio, iscritta al n. 132  &#13;
 del registro ordinanze 1974 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 139 del 29 maggio 1974;                                    &#13;
     2)  ordinanze  emesse  il 17 dicembre 1973 dal tribunale di Perugia  &#13;
 nei procedimenti penali a carico di Marcomigni  Pietro  e  di  Agnesini  &#13;
 Luigi,  iscritte  ai  nn.  172  e  173  del  registro  ordinanze 1974 e  &#13;
 pubblicate nella Gazzetta Ufficiale della  Repubblica  n.  159  del  19  &#13;
 giugno 1974 e n. 153 del 12 giugno 1974.                                 &#13;
     Udito  nella  camera  di  consiglio del 26 febbraio 1976 il Giudice  &#13;
 relatore Ercole Rocchetti.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel corso del procedimento penale  a  carico  di  Bastiani  Manlio,  &#13;
 imputato del delitto di cui all'art. 495, terzo comma, n. 2, del codice  &#13;
 penale,  per  aver  falsamente  dichiarato  ad  un  magistrato  che  lo  &#13;
 interrogava, quale imputato, di essere  incensurato,  il  tribunale  di  &#13;
 Tolmezzo,  con  ordinanza  12  ottobre  1971,  sollevava  d'ufficio  la  &#13;
 questione di legittimità costituzionale  di  detta  norma  del  codice  &#13;
 penale in riferimento all'art. 24, secondo comma, della Costituzione.    &#13;
     All'uopo  il  tribunale rappresentava che la ripetuta norma "appare  &#13;
 in contrasto con il  principio  della  inviolabilità  del  diritto  di  &#13;
 difesa,  garantito  dalla Carta e regolato per molti aspetti da precise  &#13;
 disposizioni delle leggi ordinarie, che legittimano l'imputato finanche  &#13;
 ad astenersi da qualsivoglia dichiarazione a lui pregiudizievole"        &#13;
     L'esigenza che presiede all'art. 495, terzo comma n. 2  del  codice  &#13;
 penale,  come  quella  di  evitare  false  dichiarazioni  dell'imputato  &#13;
 nell'interesse dei terzi e del buon funzionamento della giustizia, deve  &#13;
 essere mantenuta - argomentava l'ordinanza di rimessione -  nei  limiti  &#13;
 rigorosamente  compatibili  con l'anzidetto diritto alla difesa. E tali  &#13;
 limiti sembra vengano  superati  incriminando  le  false  dichiarazioni  &#13;
 dell'imputato sui propri precedenti penali.                              &#13;
     Con  ordinanza  n.  138  del  1973 tuttavia questa Corte, all'esito  &#13;
 dell'esame della proposta  questione  di  legittimità  costituzionale,  &#13;
 disponeva  la  restituzione  degli  atti  al  giudice  a quo, rilevando  &#13;
 testualmente quanto segue:                                               &#13;
     "Considerato che la disposizione impugnata considera reato, per  la  &#13;
 parte che qui viene in esame, la falsa dichiarazione dell'imputato "sul  &#13;
 proprio stato e sulle proprie qualità personali"  ",                    &#13;
     "che  l'invito  rivolto  dal giudice all'imputato se egli sia stato  &#13;
 "sottoposto ad altri  procedimenti  penali  "  e  se  abbia  "riportato  &#13;
 condanne  nello Stato o all'estero " è previsto nell'art.  25 del r.d.  &#13;
 28 maggio 1931, n. 602,  contenente  "disposizioni  di  attuazione  del  &#13;
 codice di procedura penale" " ;                                          &#13;
     "che  si  rende  necessaria la restituzione degli atti al giudice a  &#13;
 quo perché questi riesamini sotto il profilo della rilevanza  e  della  &#13;
 puntuale  determinazione  dell'oggetto  della questione di legittimità  &#13;
 costituzionale se nella  fattispecie  sottoposta  al  suo  esame  debba  &#13;
 trovare applicazione il disposto dell'art. 495 del codice penale".       &#13;
     Con  ordinanza 18 dicembre 1973 il ripetuto tribunale ha riproposto  &#13;
 la questione, precisando che, ferme rimanendo le argomentazioni addotte  &#13;
 con  la  precedente  ordinanza  12  ottobre  1971,  va   esaminata   la  &#13;
 legittimità costituzionale, in riferimento all'art. 24, secondo comma,  &#13;
 della  Costituzione,  dell'art.  25 del r.d. 28 maggio 1931, n. 602, in  &#13;
 relazione all'art. 495, terzo comma, n. 2, del  codice  penale,  "nella  &#13;
 parte in cui dispone che il magistrato deve chiedere all'imputato se è  &#13;
 stato  sottoposto  ad  altri  procedimenti  penali  e  se  ha riportato  &#13;
 condanne nello Stato o all'estero".                                      &#13;
     La  stessa  questione  proponeva  il  tribunale  di Perugia con due  &#13;
 ordinanze   del   17   dicembre,   di   identico   contenuto,   emesse,  &#13;
 rispettivamente,  nel  corso  dei procedimenti penali contro Marcomigni  &#13;
 Pietro e Agnesini Luigi, entrambi imputati del  menzionato  delitto  di  &#13;
 cui all'art. 495, terzo comma, n. 2, del codice penale.                  &#13;
     In   dette  ordinanze  si  fa  presente  che  le  norme  denunziate  &#13;
 "impongono, in sostanza, all'imputato (nel caso si  tratti  di  persona  &#13;
 già  condannata) dichiarazioni utilizzabili contro di lui, quanto meno  &#13;
 ai fini della determinazione della pena, ai sensi dell'art. 133, n.  2,  &#13;
 del  codice  penale e che, in forza di cio, si realizza una lesione del  &#13;
 principio - riconducibile nell'ambito del  diritto  di  difesa  di  cui  &#13;
 all'art.  24  Cost.  -  secondo  cui l'imputato non è tenuto a rendere  &#13;
 dichiarazioni a lui sfavorevoli"                                         &#13;
     In tutti e tre i giudizi seguiti avanti  questa  Corte  non  vi  è  &#13;
 stata   costituzione  di  parti,  né  intervento  del  Presidente  del  &#13;
 Consiglio dei ministri. Essi vanno  perciò  direttamente  esaminati  e  &#13;
 decisi in camera di consiglio.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Poiché  le  ordinanze  di  rimessione  propongono le stesse  &#13;
 questioni di costituzionalità, i  relativi  giudizi  vanno  riuniti  e  &#13;
 decisi con unica sentenza.                                               &#13;
     2.   -  Viene  sottoposta  alla  Corte  questione  di  legittimità  &#13;
 costituzionale dell'art. 495, comma terzo, n. 2, del codice penale, che  &#13;
 punisce la falsa dichiarazione dell'imputato sulla  propria  identità,  &#13;
 sul proprio stato e sulle proprie qualità personali, nonché dell'art.  &#13;
 25  delle  norme  di attuazione del codice di procedura penale (r.d. 28  &#13;
 maggio 1931, n. 602) il quale dispone che, tra le domande da rivolgersi  &#13;
 all'imputato allorché si procede al suo  interrogatorio,  deve  essere  &#13;
 compresa quella volta ad accertare se egli è stato sottoposto ad altri  &#13;
 procedimenti   penali,  e  se  ha  riportato  condanne  nello  Stato  o  &#13;
 all'estero.                                                              &#13;
     Secondo le ordinanze di rimessione, dal combinato disposto dei  due  &#13;
 articoli  risulterebbe  che  l'imputato dovrebbe essere sottoposto alla  &#13;
 sanzione prevista dall'art. 495, anche se dice il falso  in  merito  ai  &#13;
 suoi precedenti penali, alla cui rivelazione egli sarebbe costretto per  &#13;
 rispondere agli inquirenti che sono tenuti a interrogarlo in proposito.  &#13;
     Da   ciò   le  ordinanze  deducono  una  lesione  "del  principio,  &#13;
 riconducibile all'art. 24 della Costituzione,  secondo  cui  l'imputato  &#13;
 non è tenuto a rendere dichiarazioni a lui sfavorevoli".                &#13;
     3. - La questione non è fondata.                                    &#13;
     Non   è   dubbio  che,  se  l'imputato,  alla  domanda  rivoltagli  &#13;
 dall'inquirente sui suoi precedenti penali risponde in  modo  contrario  &#13;
 al  vero, egli incorre nelle sanzioni previste dall'art. 495 del codice  &#13;
 penale.                                                                  &#13;
     Ma non è esatto che, a tale domanda, egli sia tenuto a rispondere,  &#13;
 essendo certo che  può  rifiutarsi  di  fornire  le  notizie,  che  in  &#13;
 proposito   gli   vengano   richieste,   senza   incorrere   in  alcuna  &#13;
 responsabilità penale                                                   &#13;
     4. - Ciò risulta in modo del tutto palese dal  combinato  disposto  &#13;
 degli  artt. 78 e 366 del codice di procedura penale, che dettano norme  &#13;
 sui preliminari dell'interrogatorio.                                     &#13;
     Prescrive al riguardo l'art.  78  che  "l'autorità  giudiziaria  o  &#13;
 l'ufficiale   di   polizia   giudiziaria,   prima   che   abbia  inizio  &#13;
 l'interrogatorio, deve avvertire l'imputato, dandone atto nel  verbale,  &#13;
 che  egli  ha  facoltà  di non rispondere, salvo quanto dispone l'art.  &#13;
 366, primo comma".                                                       &#13;
     Quest'ultimo  articolo  prescrive  poi  che,  "prima  di  procedere  &#13;
 all'interrogatorio,  il  giudice  invita  l'imputato  a  dichiarare  le  &#13;
 proprie  generalità, ammonendolo delle conseguenze a cui si espone chi  &#13;
 si rifiuta di dare le proprie generalità o le dà false".               &#13;
     Coordinando le due norme, appare chiaro che l'imputato,  solo  alla  &#13;
 richiesta  delle  proprie  generalità  è  tenuto  a fornire risposta,  &#13;
 incorrendo in responsabilità penale qualora si rifiuti di  rispondere,  &#13;
 o dia false generalità.                                                 &#13;
     Che  poi  per generalità attinenti alla persona debbano intendersi  &#13;
 soltanto il nome, il cognome, la data e il luogo di nascita, oltre  che  &#13;
 dal  significato proprio del lemma, risulta, benché in modo indiretto,  &#13;
 dall'art. 25 delle norme di attuazione, che  è  stato  denunziato.  In  &#13;
 detto    articolo    si    precisa    infatti   che,   "nel   procedere  &#13;
 all'interrogatorio,  il  giudice  o  il   pubblico   ministero   invita  &#13;
 l'imputato  anche  a dichiarare se ha un soprannome o pseudonimo, se sa  &#13;
 leggere e  scrivere,  se  ha  beni  patrimoniali,  quali  sono  le  sue  &#13;
 condizioni  di  vita  individuale, famigliare e sociale, se ha adempito  &#13;
 agli obblighi del servizio militare, se è stato  sottoposto  ad  altri  &#13;
 procedimenti   penali   e  se  ha  riportato  condanne  nello  Stato  o  &#13;
 all'estero".                                                             &#13;
     Ora tutte queste notizie,  per  così  dire,  supplementari,  sulla  &#13;
 personalità  dell'imputato  sono richieste dall'art. 25 delle norme di  &#13;
 attuazione "anche" e cioè in aggiunta a quella principale,  sottaciuta  &#13;
 nell'articolo perché risultante dalle norme del codice, e che concerne  &#13;
 la   enunciazione  delle  generalità,  costituite  appunto  dal  nome,  &#13;
 cognome, luogo e data di nascita (v. art. 3 legge 31 ottobre  1955,  n.  &#13;
 1064). Ma, a fornire tali notizie accessorie, benché anch'esse dirette  &#13;
 ad  inquadrare  la personalità dell'imputato, questi non è obbligato,  &#13;
 appunto perché l'art.  366  citato  restringe  solo  alle  generalità  &#13;
 l'obbligo e la sanzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 495, terzo comma, n. 2, del codice penale e 25 del r.d.  28  &#13;
 maggio 1931, n. 602 (contenente disposizioni di attuazione al codice di  &#13;
 procedura penale), questioni proposte, con le ordinanze in epigrafe, in  &#13;
 riferimento all'art. 24, secondo comma, della Costituzione.              &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 aprile 1976.         &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO.                            &#13;
                                   LUIGI BROSIO - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
