<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>113</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:113</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/03/1999</data_decisione>
    <data_deposito>02/04/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art  1,  comma  170,    &#13;
 della  legge  23  dicembre  1996, n. 662 (Misure di razionalizzazione    &#13;
 della finanza pubblica), promosso con ordinanza emessa il  15  aprile    &#13;
 1997  dal  tribunale  amministrativo  regionale  per  la Campania sui    &#13;
 ricorsi riuniti proposti da Bossio Bianca contro il comune di Napoli,    &#13;
 iscritta al n. 688 del registro ordinanze  1997  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  42, prima serie speciale,    &#13;
 dell'anno 1997;                                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio del 24 febbraio 1999 il giudice    &#13;
 relatore Riccardo Chieppa;                                               &#13;
   Ritenuto che il tribunale amministrativo regionale della  Campania,    &#13;
 nel  corso  del  giudizio  promosso  da  una dipendente del comune di    &#13;
 Napoli, in servizio presso l'Avvocatura municipale con  la  qualifica    &#13;
 di  avvocato  dirigente, nei confronti del provvedimento con il quale    &#13;
 il predetto  comune  aveva  respinto  l'istanza  di  mantenimento  in    &#13;
 servizio  per  un  biennio oltre il compimento del sessantacinquesimo    &#13;
 anno di età, presentata ai  sensi  del  disposto  dell'art.  16  del    &#13;
 decreto  legislativo  30  dicembre 1992, n. 503, con ordinanza del 15    &#13;
 aprile 1997 (r.o.  n. 688 del 1997),  ha  sollevato,  in  riferimento    &#13;
 all'art.    3   della   Costituzione,   questione   di   legittimità    &#13;
 costituzionale dell'art. 1, comma 170, della legge 23 dicembre  1996,    &#13;
 n.  662  (Misure  di razionalizzazione della finanza pubblica), nella    &#13;
 parte in cui conferisce,  con  efficacia  retroattiva,  validità  ai    &#13;
 provvedimenti  di  diniego di mantenimento in servizio dei dipendenti    &#13;
 di  enti  pubblici,  adottati  sotto  la  vigenza  di  una  serie  di    &#13;
 decreti-legge,  succedutisi e mai convertiti, il cui art. 1, comma 2,    &#13;
 escludeva  l'applicabilità  del  predetto  art.   16   del   decreto    &#13;
 legislativo n. 503 del 1992, per gli enti locali che deliberassero lo    &#13;
 stato di dissesto e per tutta la durata del dissesto stesso;             &#13;
     che  il  Collegio  rimettente,  dopo  avere  esposto  le  vicende    &#13;
 relative alla  previsione  di  esclusione  degli  enti  in  stato  di    &#13;
 dissesto dalla sfera di applicabilità del citato art. 16 (contenente    &#13;
 l'attribuzione  della  facoltà per i dipendenti civili dello Stato e    &#13;
 degli  enti  pubblici  non  economici  di  permanere in servizio, con    &#13;
 effetto dalla data di entrata in vigore della legge 23 ottobre  1992,    &#13;
 n.  421,  per un periodo massimo di un biennio oltre i limiti di età    &#13;
 per il collocamento a riposo per essi  previsti),  contenuta  per  la    &#13;
 prima  volta nell'art.  1, comma 2, del d.-l. 15 giugno 1994, n. 376,    &#13;
 non convertito  in  legge,  e  quindi  reiterata  con  la  catena  di    &#13;
 successivi  decreti-legge  a  partire dal d.-l. 8 agosto 1994, n. 492    &#13;
 fino al d.l. 5 agosto 1996, n. 409, ha sottolineato che la esclusione    &#13;
 non è stata riprodotta nell'ultimo d.-l. 4  ottobre  1996,  n.  516,    &#13;
 neanch'esso convertito in legge;                                         &#13;
     che,  sempre  secondo  l'ordinanza di rimessione, l'art. 1, comma    &#13;
 170, della legge 23 dicembre 1996, n. 662,  disponendo  che  "restano    &#13;
 validi  gli  atti  e  i  provvedimenti  adottati e sono fatti salvi i    &#13;
 procedimenti  instaurati,  gli  effetti  prodottisi  e   i   rapporti    &#13;
 giuridici  sorti  sulla  base"  di  tutti  i  predetti  decreti-legge    &#13;
 compreso l'ultimo decreto, ha di fatto individuato un arco temporale,    &#13;
 ricompreso tra il 17 giugno 1994 ed il 4  ottobre  1996,  durante  il    &#13;
 quale  i  dipendenti  degli  enti  locali dissestati non hanno potuto    &#13;
 godere dei benefici previsti  dallo  stesso  art.  16,  in  tal  modo    &#13;
 violando  i  principi  costituzionali  di parità di trattamento e di    &#13;
 ragionevolezza;                                                          &#13;
     che il tribunale amministrativo regionale ritiene che  una  legge    &#13;
 retroattiva,   quale   quella   disciplinata   dall'art.   77   della    &#13;
 Costituzione, non potrebbe escludere per un determinato arco di tempo    &#13;
 l'attribuzione di benefici generalmente  concessi,  ove  non  possano    &#13;
 enuclearsi  ulteriori  motivi  che  giustifichino  la  disparità  di    &#13;
 trattamento, tenuto presente che, nel caso  di  specie,  la  "mancata    &#13;
 reiterazione  in via permanente della norma limitativa introdotta con    &#13;
 il d.l. n. 376 del 1994" non sarebbe collegata alla cessazione  dello    &#13;
 stato di dissesto;                                                       &#13;
     che  nel  giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 Ministri, con il patrocinio dell'Avvocatura generale dello Stato, che    &#13;
 ha concluso per  la  manifesta  infondatezza  della  questione,  tesi    &#13;
 successivamente sviluppata con memoria;                                  &#13;
   Considerato  che  deve  escludersi  la violazione dell'art. 3 della    &#13;
 Costituzione,  sotto  il  duplice   profilo   della   disparità   di    &#13;
 trattamento   e   della  irragionevolezza,  alla  luce  dei  principi    &#13;
 ricavabili dalle sentenze della Corte relative  al  trattenimento  in    &#13;
 servizio  oltre  i  limiti  di  età  (sentenza n. 162 del 1997) e ai    &#13;
 trattamenti differenziati a seconda della tipologia del  rapporto  di    &#13;
 impiego  e  dei  tempi  diversi  anche rispetto alla stessa categoria    &#13;
 (sentenza n. 422 del 1994; ordinanza n. 380 del 1994; sentenze n. 475    &#13;
 del 1993; n. 237 del 1994 e n. 395 del 1990);                            &#13;
     che, in particolare, l'art. 16 del d.lgs. n. 503 del  1992  -  da    &#13;
 interpretarsi  in  immediata  connessione con il correlato principio,    &#13;
 contenuto nell'art. 3, lettera b) della legge di  delega  23  ottobre    &#13;
 1992, n. 421, avente oggetto e criteri direttivi non estesi a tutti i    &#13;
 dipendenti  pubblici,  ma  limitati  al settore dei dipendenti civili    &#13;
 dello Stato e degli enti pubblici non economici - è disposizione  di    &#13;
 carattere  eccezionale,  con  finalità  di  contenimento della spesa    &#13;
 pubblica in ordine ai  trattamenti  di  previdenza  e  di  quiescenza    &#13;
 (sentenza n. 162 del 1997);                                              &#13;
     che,  del  resto,  non  esiste  un  principio  fondamentale della    &#13;
 legislazione  statale  in  base  al  quale  vi  sarebbe  un   diritto    &#13;
 incondizionato  del  dipendente  pubblico al mantenimento in servizio    &#13;
 per un biennio (sentenza n. 162 del 1997);                               &#13;
     che  pertanto  non  è  viziata,  né  sotto  il  profilo   della    &#13;
 disparità   di   trattamento,   né   sotto  quello  della  assoluta    &#13;
 irragionevolezza, la previsione (i cui effetti sono stati sanati)  di    &#13;
 restringere   drasticamente   le  possibilità  di  prosecuzione  del    &#13;
 rapporto di lavoro oltre i limiti di età per il personale degli enti    &#13;
 locali in condizioni di dissesto, sia in relazione  alle  contingenti    &#13;
 esigenze  di  contenimento  della spesa degli enti locali che abbiano    &#13;
 deliberato lo stato di  dissesto  in  considerazione  della  assoluta    &#13;
 insufficienza  di  mezzi  finanziari,  sia per gli effetti indiretti,    &#13;
 anche di prevenzione, nell'ambito degli organi burocratici  dell'ente    &#13;
 dissestato;                                                              &#13;
     che  non deve essere esclusa la legittimità, sotto gli anzidetti    &#13;
 profili, di un trattamento differenziato applicato ad una determinata    &#13;
 categoria di soggetti in tempi diversi, soprattutto quando si  tratta    &#13;
 di  legge  che  regola  i  rapporti  giuridici  sorti  sulla  base di    &#13;
 decreti-legge che non erano stati convertiti  e  avevano  determinato    &#13;
 una   cessazione  dal  servizio  allo  scadere  del  limite  di  età    &#13;
 prefigurato per la stessa categoria, con esclusione -  a  parte  ogni    &#13;
 problema  interpretativo  sulla  generale estensione della previsione    &#13;
 dell'art. 16 del d.lgs.   n. 503 del 1992  -  dell'applicabilità  di    &#13;
 norma di carattere eccezionale;                                          &#13;
     che  non  è assolutamente irragionevole la sanatoria anzidetta -    &#13;
 rientrante nella piena  discrezionalità  del  legislatore  tutte  le    &#13;
 volte  che  l'assetto  temporaneo  da  sanare  non  sia di per sé in    &#13;
 contrasto con la Costituzione -, in  quanto  appartiene  alla  scelta    &#13;
 della  legge  ex  art.  77,  ultimo  comma,  della  Costituzione,  la    &#13;
 previsione di conservare  gli  effetti  già  prodottisi  di  singoli    &#13;
 decreti-legge  non convertiti:   nella specie gli effetti dei divieti    &#13;
 di prosecuzione di  rapporti  di  lavoro  oltre  i  limiti  di  età,    &#13;
 adottati  nei  periodi di vigenza dei singoli decreti-legge decaduti,    &#13;
 escludendo  così  il  ripristino  o  la  reviviscenza  di  rapporti,    &#13;
 rispetto  ai  quali  non  vi  era  stata  prestazione  di servizio, e    &#13;
 conseguentemente evitando un aggravio economico per un arco temporale    &#13;
 tutt'altro che limitato;                                                 &#13;
     che la norma di sanatoria può ulteriormente essere  giustificata    &#13;
 -  secondo  la  tesi dell'Avvocatura dello Stato - dalla finalità di    &#13;
 contenere gli esuberi di personale che si  sarebbero  determinati  in    &#13;
 caso  di  riassunzioni,  con  successivo  ricorso alla mobilità, con    &#13;
 aggravio ulteriore di contributi a carico degli enti  (dissestati)  e    &#13;
 depauperamento  di forze di lavoro più giovani, mentre il fluire del    &#13;
 tempo (dal giugno 1994 all'ottobre 1996) con superamento del  periodo    &#13;
 più  critico dal punto di vista economico e del personale degli enti    &#13;
 locali, costituiva di per sé elemento diversificatore;                  &#13;
     che, alla stregua delle anzidette  argomentazioni,  la  questione    &#13;
 deve essere dichiarata manifestamente infondata sotto ogni profilo;      &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, della norme integrative per i giudizi  avanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 1, comma 170, della legge 23 dicembre  1996,    &#13;
 n.   662   (Misure  di  razionalizzazione  della  finanza  pubblica),    &#13;
 sollevata,  in  riferimento  all'art.  3  della   Costituzione,   dal    &#13;
 Tribunale  amministrativo  regionale  della  Campania con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 24 marzo 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Chieppa                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 2 aprile 1999.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
