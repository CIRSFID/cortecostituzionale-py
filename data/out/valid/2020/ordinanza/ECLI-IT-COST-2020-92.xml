<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/92/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/92"/>
          <FRBRalias value="ECLI:IT:COST:2020:92" name="ECLI"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="92"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/92/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/92/ita@"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/92/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-05-15/92/ita@.xml"/>
          <FRBRdate date="2020-05-15" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-04-07" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-05-15" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>92</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:92</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Giovanni Amoroso</akn4cc:relatore>
        <akn4cc:data_decisione>2020-04-07</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-05-15</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/1992-04-30/285/!main#art_222">art. 222 del decreto legislativo 30 aprile 1992, n. 285</ref> (Nuovo codice della strada), promosso dal Tribunale ordinario di Massa nel procedimento penale a carico di F. K., con ordinanza del 6 dicembre 2018, iscritta al n. 106 del registro ordinanze 2019 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 28, prima serie speciale, dell'anno 2019.
 Udito il Giudice relatore Giovanni Amoroso nella camera di consiglio del 6 aprile 2020, svolta, ai sensi del decreto della Presidente della Corte del 24 marzo 2020, punto 1), lettera a);
 deliberato nella camera di consiglio del 7 aprile 2020.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Ritenuto che, con ordinanza del 6 dicembre 2018, il Tribunale ordinario di Massa in persona del Giudice onorario di pace ha sollevato, in riferimento agli artt. 3 e 27, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/1992-04-30/285/!main#art_222">art. 222 del decreto legislativo 30 aprile 1992, n. 285</ref> (Nuovo codice della strada), nella parte in cui prevede l'applicazione della medesima sanzione accessoria della revoca quinquennale della patente di guida a fronte della condanna per reati relativi a condotte diverse sotto il profilo della colpa, dell'offensività e della pericolosità;
 che il rimettente riferisce di procedere nei confronti di una persona imputata del reato di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_590bis">art. 590-bis del codice penale</ref> e che, in ipotesi di condanna, dovrebbe applicare la sanzione accessoria della revoca della patente di guida, con divieto di conseguire una nuova patente per la durata di cinque anni;
 che il giudice a quo dubita della legittimità costituzionale della disposizione censurata, per violazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, nella parte in cui prevede l'applicazione della sanzione accessoria della revoca della patente di guida, per la durata di cinque anni, in caso di condanna sia per il reato di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_590bis">art. 590-bis cod. pen.</ref>, sia per il reato di cui all'<ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">art. 589-bis cod. pen.</ref>;
 che, in particolare, l'art. 222 cod. strada violerebbe l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, in quanto disciplina in modo analogo situazioni diverse, sia per gravità e pericolosità, sia per il diverso grado di rimproverabilità connesso alle diverse situazioni;
 che il citato parametro costituzionale sarebbe, altresì, violato, in quanto la medesima sanzione accessoria è prevista quale conseguenza della condanna per il delitto di lesioni stradali, a prescindere dalla ricorrenza delle circostanze aggravanti o attenuanti specifiche, e per l'omicidio stradale;
 che, inoltre, la disposizione censurata si porrebbe in contrasto con il principio della finalità rieducativa della pena di cui all'art. 27, terzo comma, Cost., in quanto prevede la medesima, grave, sanzione amministrativa accessoria per condotte non parificabili sotto il profilo del disvalore e della necessità di rieducazione del colpevole.
 Considerato che il Tribunale ordinario di Massa, in persona del Giudice onorario di pace, ha sollevato, in riferimento agli artt. 3 e 27, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/1992-04-30/285/!main#art_222">art. 222 del decreto legislativo 30 aprile 1992, n. 285</ref> (Nuovo codice della strada), nella parte in cui prevede l'applicazione della medesima sanzione accessoria della revoca quinquennale della patente di guida a fronte della condanna per reati relativi a condotte diverse;
 che l'ordinanza di rimessione è priva di un'adeguata descrizione della fattispecie oggetto del giudizio a quo, mancando del tutto la descrizione della condotta contestata all'imputato e della colpa allo stesso ascritta per violazione delle norme sulla disciplina della circolazione stradale;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che l'insufficiente descrizione della fattispecie impedisce il necessario controllo in punto di rilevanza e rende le questioni manifestamente inammissibili (ex mult<ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2019/103/!main">is, ordinanze n. 103 del 2019</ref>, n. 7 del 2018, n. 210 del 2017 e n. 237 del 2016);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, inoltre, questa Corte, con <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2019/88/!main">sentenza n. 88 del 2019</ref>, successiva all'ordinanza di rimessione, ha dichiarato l'illegittimità costituzionale dell'art. 222, comma 2, quarto periodo, cod. strada, nella parte in cui non prevede che, in caso di condanna, ovvero di applicazione della pena su richiesta delle parti a norma dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1988-09-22/447/!main#art_444">art. 444 del codice di procedura penale</ref>, per i reati di cui agli artt. 589-bis (Omicidio stradale) e 590-bis (Lesioni personali stradali gravi o gravissime) del <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main">codice penale</ref>, il giudice possa disporre, in alternativa alla revoca della patente di guida, la sospensione della stessa ai sensi del secondo e terzo periodo dello stesso comma 2 dell'art. 222 cod. strada allorché non ricorra alcuna delle circostanze aggravanti previste dai rispettivi commi secondo e terzo degli <mref><ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_589bis">artt. 589-bis</ref> e <ref href="/akn/it/act/regioDecreto/stato/1930-10-19/1398/!main#art_590bis">590-bis cod. pen.</ref></mref></p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara la manifesta inammissibilità delle questioni di legittimità costituzionale dell'<ref href="/akn/it/act/decretoLegislativo/stato/1992-04-30/285/!main#art_222">art. 222 del decreto legislativo 30 aprile 1992, n. 285</ref> (Nuovo codice della strada), sollevate, in riferimento agli artt. 3 e 27, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, dal Tribunale ordinario di Massa in persona del Giudice di pace con l'ordinanza indicata in epigrafe.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-04-07">7 aprile 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giovanniAmoroso" as="#relatore">Giovanni AMOROSO</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-05-15">15 maggio 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
