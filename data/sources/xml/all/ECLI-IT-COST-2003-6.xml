<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>6</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:6</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia>Annibale Marini</redattore_pronuncia>
    <data_decisione>13/01/2003</data_decisione>
    <data_deposito>15/01/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 5, comma 6, del decreto legislativo 30 dicembre 1992, n. 504 (Riordino della finanza degli enti territoriali, a norma dell'articolo 4 della legge 23 ottobre 1992, n. 421), promossi con due ordinanze del 18 marzo 2002 dalla Commissione tributaria provinciale di Genova sui ricorsi proposti da Carrara Matilde contro il Comune di Genova, iscritte ai nn. 306 e 307 del registro ordinanze 2002 e pubblicate nella Gazzetta Ufficiale della Repubblica n. 26, prima serie speciale, dell'anno 2002. &#13;
    Visti gli atti di costituzione del Comune di Genova nonché l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nell'udienza pubblica del 3 dicembre 2002 il Giudice relatore Annibale Marini; &#13;
    uditi l'avvocato Victor Uckmar per il Comune di Genova e l'avvocato dello Stato Aldo Linguiti per il Presidente del Consiglio dei ministri. &#13;
    Ritenuto che con due ordinanze di identico contenuto, emesse nel corso di distinti giudizi, la Commissione tributaria provinciale di Genova ha sollevato, in riferimento agli artt. 3 e 53 della Costituzione, questione di legittimità costituzionale dell'art. 5, comma 6, del decreto legislativo 30 dicembre 1992, n. 504 (Riordino della finanza degli enti territoriali, a norma dell'articolo 4 della legge 23 ottobre 1992, n. 421), nella parte in cui dispone che «in caso [...] di interventi di recupero a norma dell'articolo 31, comma 1, lettere c), d) ed e), della legge 5 agosto 1978, n. 457, la base imponibile [della imposta comunale sugli immobili] è costituita dal valore dell'area, la quale è considerata fabbricabile anche in deroga a quanto stabilito nell'articolo 2, senza computare il valore del fabbricato in corso d'opera, fino alla data di ultimazione dei lavori di costruzione, ricostruzione o ristrutturazione ovvero, se antecedente, fino alla data in cui il fabbricato [...] è comunque utilizzato»; &#13;
    che i giudizi a quibus hanno ad oggetto i ricorsi proposti da un contribuente avverso avvisi di accertamento ICI, emessi dal Comune di Genova, relativi all'area sulla quale sorge un immobile gravato da vincolo storico-artistico, adibito a civile abitazione, oggetto di lavori di recupero abitativo autorizzati dallo stesso Comune; &#13;
    che, ad avviso della Commissione rimettente, l'art. 5, comma 6, del decreto legislativo n. 504 del 1992, sul quale si fonda la pretesa impositiva del Comune, si porrebbe, innanzitutto, in contrasto con gli evocati parametri costituzionali in quanto considererebbe, irrazionalmente, come manifestazione di ricchezza tassabile la realizzazione di opere di riattamento immobiliare, ritenute, per tutta la loro durata, indice di sfruttamento edificatorio dell'area su cui in realtà già insiste l'edificio oggetto delle opere; &#13;
    che, inoltre, sempre ad avviso del rimettente, la norma comporterebbe una ingiustificata disparità di trattamento tra i possessori di fabbricati, a seconda che questi necessitino, o meno, di opere di manutenzione; &#13;
    che, con memorie di identico contenuto, si è costituito nei due giudizi il Comune di Genova concludendo per la inammissibilità della questione o, comunque, per la sua manifesta infondatezza; &#13;
    che, secondo la parte costituita, la disciplina generale dell'ICI si fonderebbe, per quanto attiene ai fabbricati, sulla loro redditività catastale, intesa quale espressione del reddito medio astrattamente  da essi retraibile;  &#13;
    che la norma impugnata risulterebbe coerente con tale impostazione in quanto - durante il periodo di inutilizzabilità del bene dovuto all'esecuzione di lavori di restauro, risanamento o ristrutturazione - sostituirebbe al criterio reddituale quello riferito al valore commerciale dell'area sulla quale il fabbricato insiste; &#13;
    che la circostanza che il valore commerciale dell'area fabbricabile possa, in concreto, risultare superiore a quello catastale del fabbricato che su di essa insiste non sarebbe tale da porre la norma censurata in contrasto con il principio di capacità contributiva né con quelli di ragionevolezza e di eguaglianza; &#13;
    che, in particolare, nel caso di specie, la significativa difformità fra la rendita catastale dell'immobile ed il valore dell'area discenderebbe, del tutto accidentalmente, dal trattamento fiscale di favore di cui l'immobile stesso beneficia in conseguenza della sua appartenenza alla categoria dei beni di interesse storico-artistico; &#13;
    che, d'altra parte, secondo la costituita difesa, il legislatore, nell'esercizio dell'ampia discrezionalità di cui gode, ben potrebbe ritenere indice di una particolare «forza patrimoniale», tale da giustificare un aggravio dell'onere fiscale, anche un significativo esborso economico volto al recupero dell'immobile; &#13;
    che, peraltro, l'aggravio dell'imposizione fiscale nei confronti di un bene improduttivo non sarebbe di per sé irragionevole, avendo la finalità di incentivare, avuto riguardo alla funzione sociale della proprietà, l'utilizzazione produttiva dei beni; &#13;
    che, infine, la disomogeneità delle situazioni messe a confronto giustificherebbe la differente disciplina applicabile a chi possiede beni che necessitano di interventi di manutenzione rispetto a quella riservata a chi tali interventi non deve eseguire; &#13;
    che, limitatamente al primo dei due giudizi, è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dalla Avvocatura generale dello Stato, concludendo per la inammissibilità o, comunque, per la infondatezza della questione; &#13;
    che per la parte pubblica la questione sarebbe inammissibile non avendo il rimettente chiarito se l'imposta accertata dall'ente impositore sia maggiore o minore di quella che il contribuente avrebbe dovuto pagare assumendo come base imponibile il valore catastale del fabbricato; &#13;
    che la questione stessa sarebbe, in ogni caso, infondata in quanto rientrerebbe nella discrezionalità del legislatore individuare come base imponibile ai fini ICI, durante la realizzazione di opere a causa delle quali un edificio non sia utilizzabile, il valore dell'area su cui l'edificio stesso insiste, senza che ciò urti né con il principio di capacità contributiva - stante, anzi, la maggiore congruità dell'imponibile così individuato - né con quello di eguaglianza, data la obiettiva diversità tra la situazione di chi sia proprietario di un immobile che necessiti di lavori di riattamento, e non lo possa quindi utilizzare durante il loro svolgimento, e quella di chi sia, invece, proprietario di un immobile che non necessiti di alcun intervento di manutenzione. &#13;
    Considerato, preliminarmente, che i due giudizi, avendo ad oggetto la medesima questione, vanno riuniti per essere decisi con unico provvedimento; &#13;
 che, in entrambi i casi, il giudizio verte sulla determinazione della base imponibile ai fini ICI relativamente ad un immobile, sottoposto a vincolo storico-artistico, interessato da lavori di riattamento interno, debitamente autorizzati; &#13;
    che la rilevanza della questione di legittimità costituzionale dell'art. 5, comma 6, del decreto legislativo 30 dicembre 1992, n. 504 (Riordino della finanza degli enti territoriali, a norma dell'articolo 4 della legge 23 ottobre 1992, n. 421), si fonda sull'implicito presupposto della applicabilità di tale norma alla fattispecie considerata; &#13;
    che il rimettente non dà tuttavia conto dell'esistenza di altra norma - l'art. 2, comma 5, del decreto-legge 23 gennaio 1993, n. 16 (Disposizioni in materia di imposte sui redditi, sui trasferimenti di immobili di civile abitazione, di termini per la definizione agevolata delle situazioni e pendenze tributarie, per la soppressione della ritenuta sugli interessi, premi ed altri frutti derivanti da depositi e conti correnti interbancari, nonché altre disposizioni tributarie), convertito, con modificazioni, nella legge 24 marzo 1993, n. 75 - specificamente riguardante la determinazione della base imponibile per i fabbricati di interesse storico o artistico, ispirata ad una ratio di evidente favore per tali immobili la cui tassazione risulta, pertanto, inferiore a quella degli altri fabbricati; &#13;
    che la mancata indicazione delle ragioni per le quali il rimettente ritiene applicabile alla fattispecie sottoposta al suo esame la norma censurata anziché la norma relativa agli immobili di interesse storico o artistico, si traduce in un difetto di motivazione sulla rilevanza della questione; &#13;
    che la questione stessa va perciò dichiarata manifestamente inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riuniti i giudizi, &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 5, comma 6, del decreto legislativo 30 dicembre 1992, n. 504 (Riordino della finanza degli enti territoriali, a norma dell'articolo 4 della legge 23 ottobre 1992, n. 421), sollevata, in riferimento agli artt. 3 e 53 della Costituzione, dalla Commissione tributaria provinciale di Genova, con le ordinanze in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 gennaio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Annibale MARINI, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 15 gennaio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
