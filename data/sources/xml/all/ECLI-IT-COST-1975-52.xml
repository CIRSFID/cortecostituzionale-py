<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>52</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:52</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/02/1975</data_decisione>
    <data_deposito>06/03/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  382  del  &#13;
 codice  di  procedura penale, promosso con ordinanza emessa il 2 luglio  &#13;
 1974 dal pretore di Vallo  della  Lucania  nel  procedimento  penale  a  &#13;
 carico di Crocamo Giuseppina, iscritta al n. 351 del registro ordinanze  &#13;
 1974  e pubblicata nella Gazzetta Ufficiale della Repubblica n. 284 del  &#13;
 30 ottobre 1974.                                                         &#13;
     Udito nella camera di consiglio  del  9  gennaio  1975  il  Giudice  &#13;
 relatore Paolo Rossi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di  un  procedimento  penale  a  carico di tale Crocamo  &#13;
 Giuseppina, instaurato a seguito di  querela  sporta  da  tale  Mascolo  &#13;
 Mario,   essendo   risultato,   da   perizia  psichiatrica,  la  totale  &#13;
 incapacità di intendere e di volere della  prevenuta,  il  pretore  ha  &#13;
 sollevato  d'ufficio,  in  riferimento  all'art.  3 della Costituzione,  &#13;
 questione incidentale di legittimità costituzionale dell'art. 382  del  &#13;
 codice  di  procedura penale, nella parte in cui impone la condanna del  &#13;
 querelante al pagamento  delle  spese  anticipate  dallo  Stato,  anche  &#13;
 nell'ipotesi   di   proscioglimento   derivante   da   circostanze  non  &#13;
 riconducibili al querelante stesso.                                      &#13;
     Nessuna parte si è costituita in questa sede.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte costituzionale è chiamata a decidere se contrasti o  meno  &#13;
 con  il principio costituzionale d'eguaglianza l'art. 382 del codice di  &#13;
 procedura penale, nella parte in cui prevede la condanna del querelante  &#13;
 alle spese del procedimento anticipate dallo Stato nel caso  l'imputato  &#13;
 sia   stato  prosciolto  perché  non  imputabile  per  incapacità  di  &#13;
 intendere e di volere.                                                   &#13;
     La questione è fondata.                                             &#13;
     La norma impugnata, nel sancire, anche  al  fine  di  evitare  liti  &#13;
 temerarie,  la  responsabilità  del  querelante per il pagamento delle  &#13;
 spese  processuali  nel  caso  l'imputato  sia  prosciolto,  stabilisce  &#13;
 giustificate  ma tassative eccezioni qualora il proscioglimento avvenga  &#13;
 per insufficienza di prove, per concessione del  perdono  giudiziale  o  &#13;
 per  causa estintiva del reato sopravvenuta dopo la presentazione della  &#13;
 querela. Tali ipotesi sono rette da una ratio unitaria, che  è  quella  &#13;
 di   esentare   chi   ha   esercitato   il  diritto  di  querela  dalla  &#13;
 responsabilità  in esame, quando l'assoluzione dell'imputato derivi da  &#13;
 circostanze non riconducibili al querelante,  cui  nessuna  colpa  può  &#13;
 essere  addebitata.  Ove  ricorrano  tali  estremi,  contrasta  con  il  &#13;
 principio d'eguaglianza la norma giuridica, come quella denunciata, che  &#13;
 egualmente imponga la condanna alle spese  processuali.  È  appena  il  &#13;
 caso di ricordare che, in applicazione del suddetto principio, la Corte  &#13;
 ha  già dichiarato l'illegittimità parziale dell'art. 382 c.p.p., per  &#13;
 la  responsabilità  corrispondentemente  prevista  nella  ipotesi   di  &#13;
 querela  contro ignoti per un reato realmente verificatosi (sentenza n.  &#13;
 165 del 1974).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 382  del  codice  &#13;
 di  procedura  penale,  nella  parte  in  cui  prevede  la condanna del  &#13;
 querelante alle spese del procedimento anticipate  dallo  Stato,  anche  &#13;
 nell'ipotesi  di  proscioglimento  dell'imputato non imputabile perché  &#13;
 incapace d'intendere e di volere.                                        &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 20 febbraio 1975.       &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI- LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI  - VEZIO CRISAFULLI - PAOLO  &#13;
                                   ROSSI -  LEONETTO  AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO.                               &#13;
                                   ARDUINO SALUSTRI Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
