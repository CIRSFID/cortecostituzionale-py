<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>182</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:182</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Enzo Capolozza</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/12/1972</data_decisione>
    <data_deposito>21/12/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Prof. &#13;
 COSTANTINO MORTATI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA &#13;
 BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - &#13;
 Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - &#13;
 Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. &#13;
 NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Prof. GIULIO &#13;
 GIONFRIDA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 604, secondo  &#13;
 comma, del codice di procedura penale, promosso con ordinanza emessa il  &#13;
 21 dicembre 1970 dal tribunale di Genova nel procedimento per incidente  &#13;
 di esecuzione sollevato da Delle Bande Franco, iscritta al  n.  63  del  &#13;
 registro  ordinanze  1971  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 99 del 21 aprile 1971.                                     &#13;
     Udito nella camera di consiglio del  9  novembre  1972  il  Giudice  &#13;
 relatore Enzo Capalozza.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di un procedimento per incidente di esecuzione promosso  &#13;
 da Franco Delle Bande per l'iscrizione nel casellario giudiziale di una  &#13;
 sentenza di appello, che aveva dichiarato estinto per amnistia un reato  &#13;
 di furto semplice, per il quale egli  era  stato  condannato  in  primo  &#13;
 grado,  il  tribunale  di  Genova,  su  istanza  dell'interessato,  con  &#13;
 ordinanza 21 dicembre 1970, riteneva  rilevante  e  non  manifestamente  &#13;
 infondato,  in  riferimento  agli  artt.  3 e 27 della Costituzione, il  &#13;
 dubbio di legittimità costituzionale dell'art. 604, secondo comma, del  &#13;
 codice di procedura penale, nella parte in cui, dopo aver  disposto  la  &#13;
 non  iscrizione nel casellario giudiziale delle sentenze di non doversi  &#13;
 procedere per amnistia, esclude il caso in cui  sia  stata  pronunziata  &#13;
 sentenza anche non irrevocabile di condanna.                             &#13;
     Ad  avviso  del  tribunale, si avrebbe, per ciò, una disparità di  &#13;
 trattamento tra gli imputati cui  sia  stata  applicata  l'amnistia  in  &#13;
 primo  grado,  rispetto  a  quelli  ai  quali  lo stesso beneficio sia,  &#13;
 invece, applicato nel giudizio di appello.                               &#13;
     Dinanzi a questa Corte non vi è stata  costituzione  della  parte,  &#13;
 né intervento della Presidenza del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  È  stato denunziato, in riferimento agli artt. 3 e 27 della  &#13;
 Costituzione, l'art.  604,  secondo  comma,  del  codice  di  procedura  &#13;
 penale,   nella  parte  che  attiene  alla  iscrizione  nel  casellario  &#13;
 giudiziale di talune sentenze di non doversi procedere per amnistia.     &#13;
     2.  - La questione non è fondata sotto entrambi i profili, pur non  &#13;
 riguardando, ovviamente - giusta la stessa  dizione  dell'ordinanza  di  &#13;
 rimessione  -  l'ipotesi  della cosiddetta amnistia impropria (art. 593  &#13;
 cod. proc. pen.).                                                        &#13;
     Nella vigente disciplina del casellario giudiziale - quale  risulta  &#13;
 dalle  modificazioni apportate all'istituto con la legge 14 marzo 1952,  &#13;
 n. 158, e dalla "novella" 18 giugno 1955, n. 517 - è, bensì, disposta  &#13;
 la non iscrizione delle sentenze di non doversi procedere per amnistia,  &#13;
 ma, nel contempo, in deroga a tale criterio, è richiesta  l'iscrizione  &#13;
 nel  caso in cui fosse stata prima pronunziata sentenza di condanna per  &#13;
 lo stesso fatto di reato dichiarato poi estinto.                         &#13;
     3. - Orbene, quanto all'art. 3 Cost., è da osservare  che  non  è  &#13;
 identica  la  posizione  dell'imputato,  al  quale, prima che sia stata  &#13;
 pronunziata condanna, venga applicata l'amnistia e quella dell'imputato  &#13;
 che,  già  condannato  con  sentenza  non  irrevocabile,  fruisca  del  &#13;
 beneficio  in  pendenza  dell'impugnazione:  e  ciò  anche perché, in  &#13;
 regime di rinunciabilità all'amnistia (vedasi la sentenza n.  175  del  &#13;
 1971  di  questa  Corte), il condannato che abbia impugnato la sentenza  &#13;
 può invocare una pronunzia di merito ex art. 152, secondo  comma,  del  &#13;
 codice di procedura penale.                                              &#13;
     Manca,  dunque,  quella diversità di trattamento (o quella patente  &#13;
 irragionevolezza) cui  tende  ovviare  il  precetto  costituzionale  di  &#13;
 raffronto.                                                               &#13;
     4.  - Circa l'altra censura, relativa alla violazione dell'art.  27  &#13;
 Cost., secondo cui l'imputato non è considerato  colpevole  sino  alla  &#13;
 sentenza  definitiva,  è  da obiettare che l'iscrizione nel casellario  &#13;
 giudiziale, la  quale  risponde  ad  esigenze  di  documentazione  (tra  &#13;
 l'altro,  di  rilevante  interesse  statistico),  di  per sé immune da  &#13;
 conseguenze  pregiudizievoli,  non  confligge  col  principio  di   non  &#13;
 colpevolezza,  in  quanto né tramuta l'amnistiato in colpevole, né al  &#13;
 colpevole lo equipara. E  se  pur  si  volesse  indagare  sulla  natura  &#13;
 giuridica  dell'istituto, tutt'al più potrebbe giungersi a qualificare  &#13;
 l'iscrizione un effetto non penale della (precedente)  condanna,  posto  &#13;
 che qualsiasi effetto penale sarebbe incompatibile con l'estinzione del  &#13;
 reato  (art.  151,  primo  comma,  cod.  pen.),  operato  dall'amnistia  &#13;
 (propria).                                                               &#13;
     5. - Per quel che concerne la certificazione  rilasciata  ad  "ogni  &#13;
 autorità  avente  giurisdizione  penale" e "a tutte le amministrazioni  &#13;
 pubbliche ed alle aziende incaricate di  pubblico  servizio  (...)  per  &#13;
 provvedere ad un atto delle loro funzioni" (art. 606 cod. proc. pen.) e  &#13;
 la  sua  incidenza  - ogniqualvolta il comportamento anteriore rilevi o  &#13;
 possa rilevare - nel campo del diritto penale  sostanziale  (artt.  49,  &#13;
 ultimo comma; 62 bis; 100; 115, ultimo comma; 133, secondo comma, n. 2;  &#13;
 163;  169;  175,  ecc.,  cod.  pen.)  e  nel  campo  del diritto penale  &#13;
 processuale (artt. 251 e seguenti; 277 e  seguenti,  ecc.,  cod.  proc.  &#13;
 pen.),  oltreché nell'ambito amministrativo e disciplinare, l'indagine  &#13;
 è preclusa dai limiti  della  questione  attualmente  sottoposta  alla  &#13;
 Corte,   stante   la  differente  funzione  della  iscrizione  e  della  &#13;
 certificazione.                                                          &#13;
     Né   varrebbe   l'argomento   che   la   certificazione   consegue  &#13;
 automaticamente    all'iscrizione,    per   estendere,   in   sede   di  &#13;
 interpretazione dell'ordinanza, l'esame della questione ad altre norme:  &#13;
 è vero il contrario, siccome risulta dal confronto  dell'articolo  606  &#13;
 cod. proc. pen. con gli artt. 608 e 609 e, per i minori non imputabili,  &#13;
 con  l'ultima  parte  del capoverso del medesimo art. 606 del codice di  &#13;
 procedura penale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata, in  riferimento  agli  artt.  3  e  27  della  &#13;
 Costituzione,  la  questione  di legittimità costituzionale, sollevata  &#13;
 con l'ordinanza in epigrafe, dell'art. 604, secondo comma,  del  codice  &#13;
 di   procedura  penale,  relativamente  all'iscrizione  nel  casellario  &#13;
 giudiziale della sentenza di non doversi  procedere  per  amnistia  che  &#13;
 abbia fatto seguito a sentenza non irrevocabile di condanna.             &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 13 dicembre 1972.       &#13;
                                   GIUSEPPE   CHIARELLI   -   COSTANTINO  &#13;
                                   MORTATI  - GIUSEPPE  VERZÌ - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - FRANCESCO  PAOLO  &#13;
                                   BONIFACIO - LUIGI OGGIONI - ANGELO DE  &#13;
                                   MARCO   -  ERCOLE  ROCCHETTI  -  ENZO  &#13;
                                   CAPALOZZA    -    VINCENZO    MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA.                    &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
