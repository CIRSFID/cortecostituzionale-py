<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>389</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:389</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/10/1996</data_decisione>
    <data_deposito>05/11/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 707 e 708 del    &#13;
 codice di procedura civile, promosso con ordinanza emessa il 27 marzo    &#13;
 1996  dal  Presidente  del Tribunale di Udine nel procedimento civile    &#13;
 vertente tra Cosatto Maria Luisa e Di Chiara Rino, iscritta al n. 576    &#13;
 del registro ordinanze 1996 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 26, prima serie speciale, dell'anno 1996;            &#13;
   Visto l'atto di costituzione di Cosatto Maria Luisa;                   &#13;
   Udito nell'udienza pubblica del 15 ottobre 1996 il giudice relatore    &#13;
 Fernando Santosuosso;                                                    &#13;
   Udito l'avvocato Paola Rossi Appiotti per Cosatto Maria Luisa;         &#13;
   Ritenuto  che, nel corso del procedimento di separazione giudiziale    &#13;
 tra Cosatto Maria Luisa e Di Chiara Rino, il Presidente del Tribunale    &#13;
 di Udine ha sollevato questione di legittimità costituzionale  degli    &#13;
 artt.  707  e  708  del  codice  di  procedura  civile in riferimento    &#13;
 all'art.  24 della Costituzione;                                         &#13;
     che il rimettente, dopo  aver  premesso  che  è  prassi  diffusa    &#13;
 nell'ambito  della  Corte  d'appello  di  Trieste,  seguita anche dal    &#13;
 Tribunale di Udine, che il presidente  del  tribunale,  esperito  con    &#13;
 esito  negativo il tentativo di riconciliazione, espleti un ulteriore    &#13;
 tentativo  diretto  a  tramutare   la   separazione   giudiziale   in    &#13;
 consensuale,   alla  presenza  dei  coniugi  ma  con  esclusione  dei    &#13;
 difensori,  poi  ammessi  in  udienza  solo  prima  della   eventuale    &#13;
 formalizzazione   a   verbale   dei   termini  dell'accordo,  qualora    &#13;
 raggiunto;                                                               &#13;
     che, a parere del giudice a quo, tale  prassi,  pur  giustificata    &#13;
 dalla  necessità  di  limitare  la  litigiosità  in  un  settore di    &#13;
 particolare rilevanza sociale, può essere in contrasto con il  pieno    &#13;
 esercizio  del diritto di difesa e con le disposizioni costituzionali    &#13;
 volte ad assicurare la tutela del nucleo familiare;                      &#13;
     che   nel  giudizio  davanti  alla  Corte  costituzionale  si  è    &#13;
 costituita Maria Luisa Cosatto, chiedendo che la questione  sollevata    &#13;
 venga dichiarata irrilevante o manifestamente infondata;                 &#13;
   Considerato  che le norme impugnate prevedono che i coniugi debbono    &#13;
 comparire personalmente davanti al presidente senza  l'assistenza  di    &#13;
 difensore,  che il presidente deve sentirli prima separatamente e poi    &#13;
 congiuntamente, procurando di conciliarli, e che, in  caso  negativo,    &#13;
 il  Presidente,  anche  d'ufficio,  dà con ordinanza i provvedimenti    &#13;
 temporanei ed urgenti;                                                   &#13;
     che questa Corte, con la  sentenza  n.  151  del  1971,  ha  già    &#13;
 dichiarato l'illegittimità costituzionale delle norme ora impugnate,    &#13;
 nella  parte  in  cui  ai  coniugi  comparsi personalmente davanti al    &#13;
 presidente del tribunale, e in  caso  di  mancata  conciliazione,  è    &#13;
 inibito di essere assistiti dai rispettivi difensori;                    &#13;
     che  nella  richiamata  pronuncia  la Corte ha specificato che il    &#13;
 diritto di farsi  assistere  dal  difensore  durante  lo  svolgimento    &#13;
 dell'udienza  presidenziale  nel giudizio di separazione sorge per le    &#13;
 parti successivamente al fallimento del tentativo  di  conciliazione,    &#13;
 poiché  solo  a quel punto "diventa attuale il contrasto, concreto o    &#13;
 potenziale, tra i contendenti sulla base delle domande  avanzate  con    &#13;
 il  ricorso  introduttivo o delle pretese direttamente prospettate al    &#13;
 presidente del tribunale";                                               &#13;
     che nella sentenza n. 201 del 1971 la Corte ha  chiarito  che  il    &#13;
 divieto  di essere assistiti dai difensori nel corso della prima fase    &#13;
 dell'udienza presidenziale non viola  il  principio  del  diritto  di    &#13;
 difesa  di  cui all'art. 24 della Costituzione, avendo il legislatore    &#13;
 voluto tutelare in modo preminente l'interesse, di  natura  pubblica,    &#13;
 alla  pacifica continuazione della convivenza tra i coniugi, evitando    &#13;
 il giudizio come strumento per risolvere i conflitti coniugali; ed al    &#13;
 conseguimento di questi fini - osserva la citata sentenza n. 201  del    &#13;
 1971  -  "mirano  i  coniugi  (personalmente)  ed  il  presidente del    &#13;
 tribunale che non potrà non far  valere  il  prestigio  derivantegli    &#13;
 dalla sua funzione";                                                     &#13;
     che,  per l'attuazione degli stessi interessi, nulla vieta che il    &#13;
 presidente del tribunale possa anche esplorare - sia in presenza  che    &#13;
 in  assenza  dei  difensori  -  la  potenziale  praticabilità di una    &#13;
 soluzione  non  contenziosa  di  detti  conflitti,   e   ciò   nello    &#13;
 svolgimento  di  quelle  funzioni  lato  sensu  conciliative  che gli    &#13;
 impongono di attivarsi per ridurre al minimo i traumi per i coniugi e    &#13;
 per i figli; fermo restando che la difesa tecnico-professionale possa    &#13;
 intervenire al momento di  stabilire  e  formalizzare  le  condizioni    &#13;
 dell'eventuale accordo;                                                  &#13;
     che  sono  tuttora ravvisabili tanto la distinzione operata dalle    &#13;
 sentenze nn. 151 e 201 del 1971 di questa Corte tra  la  prima  e  la    &#13;
 seconda   fase   dell'udienza   presidenziale   nel  procedimento  di    &#13;
 separazione personale, quanto la sostanziale  diversità  tra  questo    &#13;
 procedimento   e   quello   del  divorzio,  in  considerazione  della    &#13;
 differenza  del  grado  di  rottura  dei  rapporti  coniugali  e  dei    &#13;
 particolari  compiti affidati dalla legge al presidente del tribunale    &#13;
 nel primo procedimento;                                                  &#13;
     che, conclusivamente, il giudice rimettente doveva, in assenza di    &#13;
 un contrario diritto vivente  ed  in  armonia  con  i  principi  già    &#13;
 affermati  da  questa Corte, interpretare le norme impugnate in senso    &#13;
 conforme  a  Costituzione,  consentendo  l'assistenza  da  parte  dei    &#13;
 difensori  durante  la  seconda  fase  dell'udienza  presidenziale e,    &#13;
 tuttavia,  non  rinunciando  alla  funzione che gli è propria, ossia    &#13;
 quella di tentare ogni strada idonea al superamento della  crisi  del    &#13;
 nucleo familiare;                                                        &#13;
     che  una  siffatta  interpretazione, in armonia con le richiamate    &#13;
 sentenze, consente di escludere la sussistenza del lamentato vizio di    &#13;
 illegittimità costituzionale;                                           &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 707 e 708 del codice di procedura  civile,    &#13;
 sollevata,   in  riferimento  all'art.  24  della  Costituzione,  dal    &#13;
 Presidente  del  Tribunale  di  Udine  con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 17 ottobre 1996.                              &#13;
                         Il Presidente: Ferri                             &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 5 novembre 1996.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
