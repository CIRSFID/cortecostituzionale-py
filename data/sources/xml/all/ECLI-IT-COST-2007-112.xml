<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>112</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:112</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>19/03/2007</data_decisione>
    <data_deposito>29/03/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli articoli 70 e 71 del codice di procedura penale, promosso con ordinanza del 20 febbraio 2006 dal Tribunale di Latina nel procedimento penale a carico di G.B., iscritta al n. 218 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 28, prima serie speciale, dell'anno 2006. &#13;
      Udito nella camera di consiglio del 7 marzo 2007 il Giudice relatore Ugo De Siervo. &#13;
    Ritenuto che, con ordinanza pronunciata il 20 febbraio 2006 e pervenuta a questa Corte il 10 giugno 2006, il Tribunale di Latina in composizione collegiale, chiamato a pronunciarsi in sede dibattimentale sulla responsabilità penale di un imputato colpito da infermità mentale, ha sollevato questioni di legittimità costituzionale degli artt. 70 e 71 del codice di procedura penale, per contrasto con gli artt. 3, 24, secondo comma, 111, secondo comma, e 112 della Costituzione; &#13;
    che, premette il giudice a quo, l'infermità mentale dell'imputato è stata accertata tramite perizia ed è tale da impedirne la cosciente partecipazione al processo; &#13;
    che essa, aggiunge il rimettente, è inoltre «cronica e non transeunte», vale a dire non superabile con il decorso del tempo; &#13;
    che in tali condizioni, prosegue il Tribunale di Latina, le norme censurate impongono di sospendere il processo con ordinanza, e di rinnovare ogni sei mesi gli accertamenti peritali sullo stato di mente dell'imputato, senza stabilire invece che si debba «emettere sentenza per sopravvenuta incapacità a partecipare attivamente al dibattimento»; &#13;
    che tale omissione normativa pare al giudice a quo in contrasto anzitutto con gli artt. 24, 111 e 112 della Costituzione, poiché consentirebbe una «durata illimitata» del processo penale, che, viceversa, dovrebbe concludersi «in tempi ragionevoli», per di più sospendendo il corso della prescrizione; &#13;
    che essa verrebbe perciò a confliggere con lo stesso esercizio del diritto di difesa e comunque con la garanzia costituzionale di ragionevole durata del processo, vanificando la propria ratio, «dettata a tutela dell'imputato»; &#13;
    che, in secondo luogo, le norme oggetto di censura violerebbero l'art. 3 della Costituzione, poiché equiparerebbero irragionevolmente «due situazioni ontologicamente diverse», quali gli effetti dell'infermità mentale «cronica» e di quella «transeunte»; &#13;
    che la soluzione costituzionalmente obbligata per superare tale dedotta illegittimità costituzionale consisterebbe, conclude il rimettente, nel «consentire al giudice di emettere sentenza (…) meramente processuale e non produttiva di effetti preclusivi compresi quelli di cui all'art. 649 cod. proc. pen.». &#13;
    Considerato che il Tribunale di Latina dubita, in riferimento agli artt. 3, 24, secondo comma, 111, secondo comma, e 112 della Costituzione, della legittimità costituzionale degli artt. 70 e 71 del codice di procedura penale, nella parte in cui essi impongono al giudice di sospendere il procedimento penale, ove l'imputato non sia in grado di partecipare coscientemente al processo per infermità mentale, anziché di pronunciare sentenza «meramente processuale» e «non produttiva di effetti preclusivi»; &#13;
    che la questione avente ad oggetto l'art. 70 cod. proc. pen. è manifestamente inammissibile per difetto di rilevanza, poiché tale disposizione, che si limita a disciplinare gli «accertamenti sulla capacità dell'imputato», anteriori e prodromici all'eventuale adozione dell'ordinanza di sospensione del procedimento, è già stata applicata dal giudice a quo, che infatti dichiara di avere preliminarmente accertato l'infermità mentale dell'imputato, tramite perizia psichiatrica; &#13;
    che, quanto alla questione concernente l'art. 71 cod. proc. pen., il rimettente lamenta anzitutto che la sospensione del procedimento lede la garanzia costituzionale della ragionevole durata del processo, assicurata dall'art. 111, secondo comma, della Costituzione, nonché il diritto di difesa dell'imputato (art. 24, secondo comma, della Costituzione) e il principio di obbligatorietà dell'azione penale (art. 112 della Costituzione); &#13;
    che, quale soluzione ritenuta costituzionalmente obbligata, il giudice a quo invoca l'introduzione, da parte di questa Corte, di una regola processuale alternativa e sostitutiva di quella vigente, consistente nella pronuncia di una non meglio precisata sentenza dotata di effetti «meramente processuali»; &#13;
    che analoga questione di costituzionalità, così sollevata con riguardo all'art. 111, secondo comma, della Costituzione, è già stata dichiarata manifestamente inammissibile da questa Corte con l'ordinanza n. 33 del 2003, ove si è rilevato che un intervento di tal natura, «oltre a rientrare, quanto a casi e disciplina, nella esclusiva sfera della discrezionalità legislativa», «riverbererebbe i suoi effetti anche sul piano del decorso dei termini di prescrizione del reato», «così vanificando l'eventuale futura “ripresa” del procedimento, ove, in ipotesi risultasse errata la prognosi di irreversibilità della incapacità processuale dell'imputato» (si veda anche l'ordinanza n. 298 del 1991); &#13;
    che questa Corte ha parimenti già ritenuta non fondata analoga questione, sollevata in riferimento agli artt. 24 e 112 della Costituzione, affermando che non è «ravvisabile una lesione del diritto di difesa, derivando, anzi, dalla sospensione del processo l'impossibilità che venga pronunciata una decisione di condanna nei confronti di una persona che, non potendo partecipare coscientemente al processo, non è in grado di difendersi», poiché «fra il diritto di essere giudicato (che non esclude che all'esito del giudizio venga pronunciata condanna) e il diritto di autodifendersi deve, infatti, ritenersi prevalente quest'ultimo» (sentenza n. 281 del 1995); &#13;
    che questa Corte ha in tale sentenza ritenuto altresì che «non appare vulnerato neppure il principio di obbligatorietà dell'azione penale perché, a parte la possibilità per il pubblico ministero di compiere le indagini nei limiti previsti dall'art. 70, terzo comma, del codice di procedura penale, l'esercizio dell'azione penale è solo sospeso a tutela del diritto costituzionalmente tutelato all'autodifesa»; &#13;
    che, infine, il rimettente ritiene violato l'art. 3 della Costituzione, poiché la norma denunciata equiparerebbe «due situazioni ontologicamente diverse», quali l'infermità mentale «cronica» e quella «transeunte»; &#13;
    che, viceversa, tale assimilazione appare del tutto corrispondente alla ratio  sottesa all'art. 70 cod. proc. pen., posto che in entrambe le ipotesi l'imputato si trova menomato, fino a che perdura immutata l'infermità di mente, nella propria “libertà di autodeterminazione” (sentenza n. 281 del 1995 cit.), coessenziale all'esercizio del diritto di difesa, sicché il legislatore ha ritenuto di prevedere la sospensione del procedimento; &#13;
    che, pertanto, anche tale questione appare manifestamente infondata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 70 del codice di procedura penale, sollevata, in riferimento agli artt. 3, 24, secondo comma, 111, secondo comma, e 112 della Costituzione, dal Tribunale di Latina con l'ordinanza in epigrafe; &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 71 del codice di procedura penale, sollevata, in riferimento all'art. 111, secondo comma, della Costituzione, dal Tribunale di Latina con la medesima ordinanza; &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 71 del codice di procedura penale, sollevata, in riferimento agli artt. 3, 24, secondo comma, e 112 della Costituzione, dal Tribunale di Latina con la medesima ordinanza. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 19 marzo 2007. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Ugo DE SIERVO, Redattore &#13;
Maria Rosaria FRUSCELLA, Cancelliere &#13;
Depositata in Cancelleria il 29 marzo 2007. &#13;
Il Cancelliere &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
