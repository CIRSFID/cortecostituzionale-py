<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>4</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:4</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/01/1989</data_decisione>
    <data_deposito>18/01/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 314 del codice    &#13;
 penale militare di  pace,  promosso  con  l'ordinanza  emessa  il  26    &#13;
 gennaio  1988  dal  Tribunale  militare  di  Padova, nel procedimento    &#13;
 penale a carico di Giuliani Antonio, iscritta al n. 216 del  registro    &#13;
 ordinanze 1988 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 23, prima serie speciale dell'anno 1988;                              &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 30 novembre 1988 il Giudice    &#13;
 relatore Ettore Gallo;                                                   &#13;
    Ritenuto  che  il  Tribunale  di  Padova, con ordinanza 26 gennaio    &#13;
 1988, sollevava questione di  legittimità  costituzionale  dell'art.    &#13;
 314 c.p.m.p. con riferimento agli artt. 3 e 13 della Costituzione;       &#13;
      che la questione veniva sollevata in sede di riesame dell'ordine    &#13;
 di cattura emesso  il  23  dicembre  1987,  ai  sensi  dell'art.  314    &#13;
 c.p.m.p.,  dal Procuratore militare della Repubblica presso lo stesso    &#13;
 Tribunale, nei confronti di un imputato  del  delitto  di  diserzione    &#13;
 continuata, ai sensi dell'art. 148 n. 2 c.p.m.p. e 81, secondo comma,    &#13;
 cod.pen.;                                                                &#13;
      che, peraltro, l'imputato - arrestato in flagranza di reato l'11    &#13;
 gennaio 1988  -  era  stato  posto  in  libertà  provvisoria  il  18    &#13;
 successivo;                                                              &#13;
      che,  secondo  l'ordinanza,  la diversità di trattamento che si    &#13;
 verifica fra  il  militare  sottoposto  alla  giurisdizione  militare    &#13;
 (esposto  alla  cattura  facoltativa  per  ogni reato non colposo che    &#13;
 comporti  una  pena  non  superiore  nel  massimo  a  tre   anni   di    &#13;
 reclusione),  e  quello  sottoposto  alla giurisdizione ordinaria nei    &#13;
 casi di connessione (cattura  facoltativa  solo  per  i  delitti  non    &#13;
 colposi  puniti  con la reclusione superiore nel massimo a tre anni),    &#13;
 viola l'art. 3 Costituzione, ma anche l'art. 13 nella  parte  in  cui    &#13;
 esprime il principio di favor libertatis;                                &#13;
      che  è intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 ministri, rappresentato dall'Avvocatura Generale dello Stato, che  ha    &#13;
 chiesto   declararatoria   d'inammissibilità   della  questione  per    &#13;
 irrilevanza o, comunque, di infondatezza;                                &#13;
    Considerato   che,   essendo   stato  l'imputato  arrestato  nella    &#13;
 flagranza del reato, la sua cattura  era  obbligatoria  ex  art.  308    &#13;
 c.p.m.p.,  poco  rilevando  che  il P.M. avesse erroneamente invocato    &#13;
 nell'ordine di cattura l'art. 314  c.p.m.p.,  giacché  il  Tribunale    &#13;
 avrebbe dovuto comunque avere riguardo alla sostanza della situazione    &#13;
 giuridica nella quale l'imputato versava, e non a quella  artificiosa    &#13;
 che veniva a crearsi per l'erronea procedura instaurata; in guisa che    &#13;
 la legittimità dell'art. 314 c.p.m.p. sarebbe dovuta  semmai  venire    &#13;
 in  causa  per  l'erronea  procedura  instaurata;  in  guisa  che  la    &#13;
 legittimità dell'art. 314 c.p.m.p. sarebbe dovuta semmai  venire  in    &#13;
 causa   per   la   sua  erronea  utilizzazione,  e  non  per  la  sua    &#13;
 compatibilità costituzionale nella specie irrilevante;                  &#13;
      che, d'altra parte, il riferimento all'art. 3 Costituzione viene    &#13;
 fondato  sull'ipotesi  di  una   connessione   che   trasferisca   il    &#13;
 procedimento  alla  giurisdizione ordinaria: ipotesi la quale nemmeno    &#13;
 sussiste nella  specie,  essendo  il  procedimento  pacificamente  di    &#13;
 competenza del Giudice militare;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art.  314  c.p.m.p.,  sollevata  dal    &#13;
 Tribunale  militare  di  Padova  con  ordinanza 26 gennaio 1988 e con    &#13;
 riferimento agli artt. 3 e 13 della Costituzione.                        &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte Costituzionale, palazzo della Consulta il 9 gennaio 1989.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 gennaio 1989.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
