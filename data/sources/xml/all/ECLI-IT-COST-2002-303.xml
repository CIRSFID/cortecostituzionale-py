<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>303</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:303</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Francesco Amirante</relatore_pronuncia>
    <redattore_pronuncia>Francesco Amirante</redattore_pronuncia>
    <data_decisione>19/06/2002</data_decisione>
    <data_deposito>28/06/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 46, comma 3, &#13;
del  decreto  legislativo  31 dicembre 1992, n. 546 (Disposizioni sul &#13;
processo  tributario  in attuazione della delega al Governo contenuta &#13;
nell'art. 30  della  legge  30 dicembre  1991,  n. 413), promosso con &#13;
ordinanza  emessa  il  25 luglio  2001  dalla  Commissione tributaria &#13;
provinciale  di  Piacenza  sul ricorso proposto da Cacciola Francesco &#13;
contro  l'Ufficio  delle  entrate di Piacenza, iscritta al n. 909 del &#13;
registro  ordinanze  2001 e pubblicata nella Gazzetta Ufficiale della &#13;
Repubblica n. 45, 1ª serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 22 maggio 2002 il giudice &#13;
relatore Francesco Amirante. &#13;
    Ritenuto  che  nel  corso del procedimento tributario conseguente &#13;
all'impugnazione  di una cartella esattoriale relativa all'I.R.P.E.F. &#13;
del  1993,  la  Commissione  tributaria  provinciale  di  Piacenza ha &#13;
sollevato,  in  riferimento  agli  articoli  3,  24,  75  e  76 della &#13;
Costituzione,  questione di legittimità costituzionale dell'art. 46, &#13;
comma   3,   del   decreto   legislativo   31 dicembre  1992,  n. 546 &#13;
(Disposizioni  sul  processo tributario in attuazione della delega al &#13;
Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413); &#13;
        che  nel giudizio in corso l'agenzia delle entrate ha ammesso &#13;
che  la  cartella di pagamento era stata emessa per errore materiale, &#13;
precisando  che  l'ufficio  competente  aveva  provveduto al relativo &#13;
sgravio  e  chiedendo,  pertanto,  che  il  giudizio fosse dichiarato &#13;
estinto con compensazione delle spese; &#13;
        che  la Commissione, dopo aver rilevato di dover applicare la &#13;
norma  impugnata  -  in  base  alla  quale, in caso di estinzione del &#13;
giudizio  per  cessazione  della  materia del contendere, le relative &#13;
spese  restano  a  carico  della  parte che le ha anticipate - dubita &#13;
della legittimità costituzionale della medesima, sostenendo che essa &#13;
viola  gli artt. 3 e 24 della Costituzione; avendo l'amministrazione, &#13;
infatti,  riconosciuto  la  fondatezza  dellapretesa  del ricorrente, &#13;
quest'ultimo  è da considerarsi parte vittoriosa nel procedimento, e &#13;
non  vi  è ragione alcuna per escludere la condanna alle spese della &#13;
parte che, con propria colpa, ha dato origine al giudizio tributario; &#13;
        che   l'omessa   possibilità  di  una  condanna  alle  spese &#13;
confliggerebbe, inoltre, anche con l'art. 76 Cost., perché l'art. 30 &#13;
della  legge  30 dicembre 1991, n. 413, che contiene la delega per la &#13;
riforma   del   contenzioso  tributario,  al  comma  1,  lettera  g), &#13;
espressamente  prevede  che  le norme del processo tributario debbano &#13;
essere  adeguate a quelle del processo civile, sicché il legislatore &#13;
avrebbe  dovuto recepire le norme di cui agli artt. 91 e seguenti del &#13;
codice  di  procedura civile, secondo i quali la soccombenza implica, &#13;
di regola, la condanna alla rifusione delle spese di lite; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo  che  la  questione  sia  dichiarata manifestamente &#13;
infondata. &#13;
    Considerato che la Commissione tributaria provinciale di Piacenza &#13;
dubita  della  legittimità costituzionale dell'art. 46, comma 3, del &#13;
decreto  legislativo 31 dicembre 1992, n. 546, nella parte in cui non &#13;
prevede  la  possibilità,  in  caso  di  estinzione del giudizio per &#13;
cessazione  della materia del contendere, di condannare alle spese di &#13;
lite  la  parte  che, avendo riconosciuto la fondatezza delle pretese &#13;
della  controparte,  dovrebbe  essere  considerata  a  tal  fine come &#13;
soccombente; &#13;
        che  la questione, così come prospettata in riferimento agli &#13;
artt. 3  e  24  Cost., è già stata dichiarata non fondata da questa &#13;
Corte  con  la sentenza n. 53 del 1998, cui hanno fatto seguito altre &#13;
ordinanze  di manifesta infondatezza (n. 265 del 1999, n. 77 del 1999 &#13;
e n. 368 del 1998), né l'odierna ordinanza aggiunge, in relazione ai &#13;
menzionati parametri costituzionali, ulteriori profili di censura che &#13;
non siano stati scrutinati con i provvedimenti ora richiamati; &#13;
        che  la  questione  deve  ritenersi  manifestamente infondata &#13;
anche  in  relazione  ad  un  presunto  eccesso di delega, perché il &#13;
criterio  direttivo di carattere generale dettato dall'art. 30, comma &#13;
1,   lettera   g),   della   legge   n. 413   del  1991,  "è  quello &#13;
dell'adeguamento,  e  non  dell'uniformità, delle norme del processo &#13;
tributario  a  quelle del processo civile" (ordinanza n. 8 del 1999); &#13;
e,   d'altra   parte,   anche  nel  processo  civile  l'unica  regola &#13;
intangibile in materia di spese è quella per cui la parte vittoriosa &#13;
non può essere onerata del relativo carico; &#13;
        che la questione, pertanto, è manifestamente infondata anche &#13;
in riferimento all'art. 76 della Costituzione; &#13;
        che  l'ulteriore  parametro  di  cui  all'art. 75 Cost. viene &#13;
invocato  senza  alcuna  motivazione  ed è del tutto inconferente in &#13;
rapporto alla natura della questione in esame. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità   costituzionale  dell'art. 46,  comma  3,  del  decreto &#13;
legislativo  31 dicembre  1992,  n. 546  (Disposizioni  sul  processo &#13;
tributario   in   attuazione   della   delega  al  Governo  contenuta &#13;
nell'art. 30  della  legge  30 dicembre  1991, n. 413), sollevata, in &#13;
riferimento  agli  articoli  3, 24, 75 e 76 della Costituzione, dalla &#13;
Commissione tributaria provinciale di Piacenza con l'ordinanza di cui &#13;
in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 19 giugno 2002. &#13;
                       Il Presidente: Ruperto &#13;
                       Il redattore: Amirante &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 28 giugno 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
