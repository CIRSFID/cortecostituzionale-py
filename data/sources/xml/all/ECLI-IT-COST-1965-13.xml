<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1965</anno_pronuncia>
    <numero_pronuncia>13</numero_pronuncia>
    <ecli>ECLI:IT:COST:1965:13</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Biagio Petrocelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/03/1965</data_decisione>
    <data_deposito>12/03/1965</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA &#13;
 JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Prof. &#13;
 ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  236 del  &#13;
 Codice di  procedura  penale,  promosso  con  ordinanza  emessa  il  10  &#13;
 febbraio  1964 dal Pretore di Imola nel procedimento penale a carico di  &#13;
 Linari Renato,  iscritta  al  n.  46  del  Registro  ordinanze  1964  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale della Repubblica, n.  91 dell'11  &#13;
 aprile 1964.                                                             &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udita  nell'udienza  pubblica del 16 dicembre 1964 la relazione del  &#13;
 Giudice Biagio Petrocelli;                                               &#13;
     udito il vice avvocato generale dello Stato Dario Foligno,  per  il  &#13;
 Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Il  23  gennaio  1964  Linari  Renato  si presentava all'Ufficio di  &#13;
 pubblica sicurezza di Imola confessandosi autore di lesioni  volontarie  &#13;
 poco  prima  commesse  in  danno  di  tale  Minganti  Ezio. Ritenuta la  &#13;
 flagranza, il Linari fu arrestato e denunciato al Pretore di Imola.      &#13;
     Mentre questi procedeva ad  accertamenti  istruttori  relativamente  &#13;
 alla entità delle lesioni subite dal Minganti, la difesa dell'imputato  &#13;
 sollevò  questione  di  legittimità  costituzionale,  in  riferimento  &#13;
 all'art. 13, secondo  comma,  della  Costituzione,  dell'art.  236  del  &#13;
 Codice  di  procedura  penale,  che disciplina l'arresto facoltativo in  &#13;
 flagranza ad opera degli ufficiali e agenti di  polizia  giudiziaria  e  &#13;
 della forza pubblica.                                                    &#13;
     Con decreto del 5 febbraio 1964 il Pretore concesse all'imputato la  &#13;
 libertà  provvisoria; e, con ordinanza in data 10 febbraio successivo,  &#13;
 avendo  ritenuta  la  questione  di  legittimità  non   manifestamente  &#13;
 infondata,   sospese   il   giudizio  e  rimise  gli  atti  alla  Corte  &#13;
 costituzionale.                                                          &#13;
     L'ordinanza  è  stata  regolarmente   notificata,   comunicata   e  &#13;
 pubblicata  sulla  Gazzetta  Ufficiale n. 91 dell'11 aprile 1964. Si è  &#13;
 costituito in  giudizio  il  Presidente  del  Consiglio  dei  Ministri,  &#13;
 assistito  dall'Avvocatura generale dello Stato, con atto di intervento  &#13;
 depositato il 30 aprile 1964.                                            &#13;
     Ad  avviso  del  Pretore  mancherebbero  nella ipotesi disciplinata  &#13;
 dall'art. 236 quei caratteri di eccezionalità, di necessità e urgenza  &#13;
 ai quali l'art.  13  della  Costituzione  subordina  l'applicazione  di  &#13;
 misure  restrittive della libertà personale da parte dell'autorità di  &#13;
 pubblica sicurezza. Farebbe difetto il carattere  della  eccezionalità  &#13;
 essendo  la  flagranza  e  la  quasi  flagranza  fatti  tutt'altro  che  &#13;
 infrequenti nella pratica; e la facoltà  di  arresto  ai  sensi  della  &#13;
 norma impugnata non muoverebbe da una assoluta esigenza di applicazione  &#13;
 immediata  di  provvedimenti  restrittivi,  come sarebbe invece se tale  &#13;
 facoltà fosse limitata ai soli casi, per esempio, di pericolo di  fuga  &#13;
 dell'imputato o di sottrazione dei corpi di reato.                       &#13;
     Altri  due addebiti sono dall'ordinanza mossi alla norma impugnata:  &#13;
 questa mancherebbe di una tassativa indicazione dei casi nei quali può  &#13;
 procedersi all'arresto in flagranza, come è invece richiesto dall'art.  &#13;
 13 della Costituzione; e quindi  lascerebbe,  tra  l'altro,  una  piena  &#13;
 discrezionalità  in materia agli organi di pubblica sicurezza. Inoltre  &#13;
 attribuendo la  facoltà  di  arresto  anche  agli  agenti  di  polizia  &#13;
 giudiziaria  e  della  forza  pubblica,  violerebbe  i  limiti  segnati  &#13;
 dall'art. 13,  il  quale,  parlando  solo  di  "autorità  di  pubblica  &#13;
 sicurezza",  sembrerebbe  negare agli agenti la potestà di procedere a  &#13;
 provvedimenti restrittivi della libertà personale.                      &#13;
     L'Avvocatura dello Stato, premesso che il Pretore sarebbe caduto in  &#13;
 un errore materiale riferendosi al secondo piuttosto che al terzo comma  &#13;
 dell'art. 13 della Costituzione, rileva che il procedimento penale  nel  &#13;
 corso   del   quale  è  stata  sollevata  la  questione  era  in  fase  &#13;
 istruttoria. Non contesta  che  in  tale  sede  possa  promuoversi  una  &#13;
 questione di legittimità costituzionale, sempre però che ciò avvenga  &#13;
 ad  opera  del  giudice  istruttore. Nella ordinanza invece non è dato  &#13;
 individuare se il Pretore si sia pronunciato nella qualità di  giudice  &#13;
 istruttore ovvero in quella di pubblico ministero.                       &#13;
     L'Avvocatura,  inoltre,  eccepisce  il  difetto  di rilevanza della  &#13;
 questione,  non  avendo  l'ordinanza  indicato  in  alcun  modo   quale  &#13;
 influenza  potrebbe  avere  la  dedotta questione di legittimità sulla  &#13;
 definizione del giudizio.                                                &#13;
     Nel merito,  l'Avvocatura  dello  Stato  esprime  l'avviso  che  la  &#13;
 flagranza  non  sia affatto priva di quei caratteri di eccezionalità e  &#13;
 di necessità e  urgenza  alla  presenza  dei  quali  l'art.  13  della  &#13;
 Costituzione  subordina  i  provvedimenti  restrittivi  della  libertà  &#13;
 personale da parte dell'autorità di pubblica sicurezza; e rammenta che  &#13;
 in questo senso si è espressa anche la opinione della  maggioranza  in  &#13;
 seno all'Assemblea costituente. Inoltre la norma impugnata non manca di  &#13;
 indicare,  con  vario  criterio,  i  reati  per i quali può procedersi  &#13;
 all'arresto in flagranza; sicché sembrerebbe senz'altro rispettato  il  &#13;
 disposto costituzionale che esige la tassativa indicazione dei casi nei  &#13;
 quali l'arresto può aver luogo.                                         &#13;
     A  proposito dell'ultimo rilievo mosso dal Pretore, l'Avvocatura si  &#13;
 sofferma infine a considerare i vari sensi nei quali può intendersi la  &#13;
 espressione "autorità  di  pubblica  sicurezza",  per  pervenire  alla  &#13;
 conclusione  che  l'art.  13  non  può non essersi riferito anche agli  &#13;
 agenti di polizia giudiziaria e della forza  pubblica,  soprattutto  in  &#13;
 relazione  al  carattere  di necessità ed urgenza dei provvedimenti in  &#13;
 questione, che sembrerebbe escludere  una  potestà  limitata  ai  soli  &#13;
 organi gerarchicamente più elevati.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte  osserva  che, dopo l'arresto in flagranza, era stata dal  &#13;
 Pretore concessa all'imputato la libertà provvisoria. La questione  di  &#13;
 legittimità  costituzionale  dell'art.  236  del  Codice  di procedura  &#13;
 penale era stata poi sollevata senza alcun accenno alla rilevanza della  &#13;
 questione stessa.                                                        &#13;
     Deve ritenersi pertanto fondata l'eccezione relativa al difetto  di  &#13;
 rilevanza.                                                               &#13;
     L'ordinanza  si  diffonde  in varie argomentazioni sul merito della  &#13;
 questione, cercando  di  dar  fondamento  alla  dedotta  illegittimità  &#13;
 costituzionale  dell'art.  236  del  Codice  di procedura penale; ma è  &#13;
 priva di qualsiasi accenno in ordine al rapporto che  dovrebbe  correre  &#13;
 fra  la  soluzione  della  questione  e  la definizione del giudizio in  &#13;
 corso, non contenendo neanche una sommaria indicazione del  perché  il  &#13;
 giudizio di cui è stata disposta la sospensione non potrebbe, ai sensi  &#13;
 del  secondo  comma  dell'art.  23  della  legge  11 marzo 1953, n. 87,  &#13;
 trovare la sua definizione senza che prima sia risolta la questione  di  &#13;
 legittimità costituzionale. Non si tratta di insufficienza o vaghezza,  &#13;
 bensì  di  assoluto difetto del giudizio di rilevanza, del quale manca  &#13;
 qualsiasi  enunciazione.  Lo   stato   di   detenzione   dell'imputato,  &#13;
 conseguente   all'arresto   in   flagranza,  si  era  risoluto  con  la  &#13;
 concessione della libertà provvisoria, e per quanto riguarda il merito  &#13;
 del  sospeso  procedimento,  concernente  una  imputazione  di  lesioni  &#13;
 personali  volontarie,  nessun dato emerge dall'ordinanza di rimessione  &#13;
 che possa  comunque  farlo  apparire  in  connessione  con  la  dedotta  &#13;
 questione  di  legittimità  costituzionale:  ve  n'è  abbastanza  per  &#13;
 stabilire che nel caso attuale, trattasi di  un  difetto  di  rilevanza  &#13;
 risultante prima facie dal testo dell'ordinanza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara    inammissibile    la    questione   sulla   legittimità  &#13;
 costituzionale dell'art. 236 del Codice di procedura penale,  sollevata  &#13;
 con   ordinanza   del  10  febbraio  1964  dal  Pretore  di  Imola,  in  &#13;
 riferimento, all'art. 13  della Costituzione.                            &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 4 marzo 1965.                                 &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI - ALDO SANDULLI - GIUSEPPE  &#13;
                                   BRANCA - MICHELE FRAGALI - COSTANTINO  &#13;
                                   MORTATI   -   GIUSEPPE   CHIARELLI  -  &#13;
                                   GIUSEPPE  VERZÌ -  GIOVANNI  BATTISTA  &#13;
                                   BENEDETTI     -    FRANCESCO    PAOLO  &#13;
                                   BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
