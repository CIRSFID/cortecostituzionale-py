<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>302</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:302</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/11/1985</data_decisione>
    <data_deposito>22/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO - Prof. RENATO &#13;
 DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi riuniti di legittimità costituzionale degli artt.  21  &#13;
 e 91 del r.d. 16 marzo 1942, n. 267  (Disciplina  del  fallimento,  del  &#13;
 concordato   preventivo,   dell'amministrazione   controllata  e  della  &#13;
 liquidazione coatta amministrativa) promossi con le seguenti ordinanze:  &#13;
     1) ordinanza emessa il 28 febbraio  1978  dal  Tribunale  di  Santa  &#13;
 Maria  Capua  Vetere  sull'istanza proposta dal curatore del fallimento  &#13;
 Gargiulo Luisa, iscritta al  n.  263  del  registro  ordinanze  1978  e  &#13;
 pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 222 dell'anno  &#13;
 1978;                                                                    &#13;
     2) ordinanza emessa il 10 maggio  1984  dal  Tribunale  di  Orvieto  &#13;
 sull'istanza  proposta  dal  curatore  del fallimento della Fattoria S.  &#13;
 Litardo ed altri, iscritta al n. 903  del  registro  ordinanze  1984  e  &#13;
 pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 321 dell'anno  &#13;
 1984.                                                                    &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 22 ottobre 1985 il Giudice relatore  &#13;
 Virgilio Andrioli;                                                       &#13;
     udito  l'avv.  dello  Stato  Franco Chiarotti per il Presidente del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.1. - Con ordinanza emessa il 28 febbraio 1978 (notificata  il  17  &#13;
 marzo e comunicata il 3 aprile successivi; pubblicata nella G.U. n. 222  &#13;
 del  9 agosto 1978 e iscritta al n. 263 R.O. 1978) sulla istanza con la  &#13;
 quale il curatore,  nel  chiedere  la  dichiarazione  di  chiusura  del  &#13;
 fallimento  di  Luisa Gargiulo, aveva sollecitato anche la liquidazione  &#13;
 del proprio compenso nella misura di lire 200.000, prevista dall'art. 4  &#13;
 comma  primo d.m. 27 novembre 1978, e chiesto che, in difetto di attivo  &#13;
 realizzato, fosse la somma richiesta  posta  a  carico  dell'Erario  in  &#13;
 applicazione  dell'art. 91 commi primo e secondo r.d. 16 marzo 1942, n.  &#13;
 267, il Tribunale di S. Maria Capua Vetere ha giudicato rilevante e, in  &#13;
 riferimento agli artt. 23 e 36 comma primo  Cost.,  non  manifestamente  &#13;
 infondata  la  questione  di  legittimità  costituzionale dell'art. 91  &#13;
 comma secondo r.d. 16 marzo 1942, n. 267 nella parte in cui non prevede  &#13;
 che il compenso del curatore, in caso di mancanza  o  insufficienza  di  &#13;
 attivo, sia posto a carico dell'Erario.                                  &#13;
     1.2. - Avanti la Corte non si è costituita la parte del giudizio a  &#13;
 quo;  nell'interesse  del  Presidente  del  Consiglio  dei  ministri ha  &#13;
 spiegato  intervento,  con  atto  depositato   il   29   luglio   1978,  &#13;
 l'Avvocatura  generale  dello  Stato che, richiamando la sent. 114/1964  &#13;
 della Corte, ha  argomentato  e  concluso  per  la  infondatezza  della  &#13;
 proposta questione.                                                      &#13;
     2.1.  -  Con ordinanza emessa il 10 maggio 1984 (comunicata il 17 e  &#13;
 notificata il 21 dello stesso mese; pubblicata nella G.U. n. 321 del 21  &#13;
 novembre 1984 e iscritta al n. 903 R.O. 1984) il Tribunale di  Orvieto,  &#13;
 il quale, in accoglimento della opposizione dei falliti, aveva revocato  &#13;
 la  dichiarazione  di  fallimento della Fattoria San Litardo e dei soci  &#13;
 Lauro Ercole e D'Aragona Elena compensando interamente tra le parti  le  &#13;
 spese  di  giudizio,  prendeva in esame la istanza, con cui il curatore  &#13;
 aveva chiesto  liquidarsi  il  compenso  spettantegli  con  mandato  di  &#13;
 pagamento  da  iscriversi  nel Mod. 12 previa prenotazione a debito del  &#13;
 relativo articolo del campione fallimentare, e sollevava  d'ufficio  la  &#13;
 questione  di  legittimità  costituzionale degli artt. 21 e 91 r.d. 16  &#13;
 marzo 1942, n. 267, in riferimento agli artt. 3, 26 e 36  Cost.,  nella  &#13;
 misura  in  cui  non prevedono che il compenso del curatore, in caso di  &#13;
 revoca della sentenza dichiarativa di fallimento ed in assenza  di  una  &#13;
 pronuncia  di  responsabilità  per colpa del creditore o del debitore,  &#13;
 sia posto a carico dell'Erario.                                          &#13;
     2.2. - Avanti la Corte nessuna delle parti del giudizio a quo si è  &#13;
 costituita; è intervenuta con  atto  depositato  l'11  dicembre  1984,  &#13;
 l'Avvocatura  generale  dello Stato per il Presidente del Consiglio dei  &#13;
 ministri richiamando le argomentazioni svolte  nell'incidente  iscritto  &#13;
 al  n.  263/1978  e  instando  per la declaratoria d'infondatezza della  &#13;
 proposta questione.                                                      &#13;
     3. - Nella pubblica udienza del 22 ottobre  1985,  nella  quale  il  &#13;
 giudice  Andrioli  ha  svolto  relazione  congiunta  sui due incidenti,  &#13;
 l'avv. dello Stato Chiarotti si è rimesso agli scritti.<diritto>Considerato in diritto</diritto>:                          &#13;
     4.1. - I due  ricorsi,  stante  la  manifesta  connessione,  vanno,  &#13;
 riuniti ai fini di contestuale decisione.                                &#13;
     4.2.  -  In disparte il rilievo che nella prassi i giudici delegati  &#13;
 si inducono ad  indennizzare  i  professionisti,  cui  è  affidata  la  &#13;
 curatela  di fallimento che si appalesa privo di attivo suscettibile di  &#13;
 ripartizione, con la nomina a curatori  di  fallimenti,  nei  quali  la  &#13;
 ripartizione    di    attivo    sembra    probabile,    la    questione  &#13;
 d'incostituzionalità, in riferimento sia a revoca di dichiarazione  di  &#13;
 fallimento  sia  a  fallimento  chiuso  con  insufficienza o carenza di  &#13;
 attivo, è da giudicare infondata perché  nessuna  delle  disposizioni  &#13;
 costituzionali  addotte a parametri vale a giustificarla: non l'art. 23  &#13;
 perché la legalità della imposizione di prestazione patrimoniale  non  &#13;
 vuol  significare  operosità della prestazione stessa e bando a uffici  &#13;
 gratuiti  di  cui  non  difettano esempi nella patria legislazione; non  &#13;
 l'art. 36 perché il curatore fallimentare non può essere  qualificato  &#13;
 lavoratore nel senso al sostantivo assegnato nel Titolo III della Parte  &#13;
 I   della   Carta   Costituzionale,   né   infine   l'art.  3  perché  &#13;
 l'accettazione della nomina di curatore non è rivestita del  carattere  &#13;
 di  obbligatorietà  che  riviene  alla nomina del perito dall'art. 314  &#13;
 comma quarto c.p.p..</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riuniti gli incidenti iscritti ai nn. 263  R.O.  1978  e  903  R.O.  &#13;
 1984,                                                                    &#13;
     dichiara  non fondata la questione di illegittimità costituzionale  &#13;
 a) dell'art. 91 comma secondo r.d. 16 marzo 1942, n. 267, sollevata, in  &#13;
 riferimento agli artt. 23 e 36 comma  primo  Cost.,  dal  Tribunale  di  &#13;
 Santa  Maria Capua Vetere con ord. 28 febbraio 1978 (n. 263/1978) nella  &#13;
 parte in cui non prevede che il  compenso  del  curatore,  in  caso  di  &#13;
 mancanza o insufficienza di attivo, sia posto a carico dell'Erario e b)  &#13;
 degli  artt.  21  e  91  r.d.  16  marzo  1942,  n.   267, sollevata in  &#13;
 riferimento agli artt. 3 comma primo, 23 e 36 comma  primo  Cost.,  dal  &#13;
 Tribunale  di  Orvieto  con  ord.   10 maggio 1984 (n. 903/1984), nella  &#13;
 parte in cui non prevedono che il compenso del  curatore,  in  caso  di  &#13;
 revoca  della sentenza dichiarativa di fallimento ed in caso di assenza  &#13;
 di una pronuncia di responsabilità  per  colpa  del  creditore  o  del  &#13;
 curatore, sia posto a carico dell'Erario.                                &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 19 novembre 1985.                             &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO.   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
