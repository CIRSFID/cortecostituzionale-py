<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1967-07-03/85/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1967-07-03/85"/>
          <FRBRalias value="ECLI:IT:COST:1967:85" name="ECLI"/>
          <FRBRdate date="1967-07-03" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="85"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1967-07-03/85/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1967-07-03/85/ita@"/>
          <FRBRdate date="1967-07-03" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1967-07-03/85/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1967-07-03/85/ita@.xml"/>
          <FRBRdate date="1967-07-03" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1967-06-15" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1967-07-03" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="AMBROSINI" href="/akn/ontology/person/it/AMBROSINI" showAs="AMBROSINI"/>
        <TLCPerson eId="gaspareAmbrosini" href="/akn/ontology/person/it/gaspareAmbrosini" showAs="GASPARE AMBROSINI"/>
        <TLCPerson eId="antoninoPapaldo" href="/akn/ontology/person/it/antoninoPapaldo" showAs="ANTONINO PAPALDO"/>
        <TLCPerson eId="nicolaJaeger" href="/akn/ontology/person/it/nicolaJaeger" showAs="NICOLA JAEGER"/>
        <TLCPerson eId="biagioPetrocelli" href="/akn/ontology/person/it/biagioPetrocelli" showAs="BIAGIO PETROCELLI"/>
        <TLCPerson eId="antonioManca" href="/akn/ontology/person/it/antonioManca" showAs="ANTONIO MANCA"/>
        <TLCPerson eId="aldoSandulli" href="/akn/ontology/person/it/aldoSandulli" showAs="ALDO SANDULLI"/>
        <TLCPerson eId="giuseppeBranca" href="/akn/ontology/person/it/giuseppeBranca" showAs="GIUSEPPE BRANCA"/>
        <TLCPerson eId="micheleFragali" href="/akn/ontology/person/it/micheleFragali" showAs="MICHELE FRAGALI"/>
        <TLCPerson eId="giuseppeVerzì" href="/akn/ontology/person/it/giuseppeVerzì" showAs="GIUSEPPE VERZÌ"/>
        <TLCPerson eId="giovanniBattistaBenedetti" href="/akn/ontology/person/it/giovanniBattistaBenedetti" showAs="GIOVANNI BATTISTA BENEDETTI"/>
        <TLCPerson eId="francescoPaoloBonifacio" href="/akn/ontology/person/it/francescoPaoloBonifacio" showAs="FRANCESCO PAOLO BONIFACIO"/>
        <TLCPerson eId="luigiOggioni" href="/akn/ontology/person/it/luigiOggioni" showAs="LUIGI OGGIONI"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1967</akn4cc:anno>
        <akn4cc:numero>85</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1967:85</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>AMBROSINI</akn4cc:presidente>
        <akn4cc:relatore>Antonino Papaldo</akn4cc:relatore>
        <akn4cc:data_decisione>1967-06-15</akn4cc:data_decisione>
        <akn4cc:data_deposito>1967-07-03</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#gaspareAmbrosini">GASPARE AMBROSINI</judge>, Presidente - Prof. <judge refersTo="#antoninoPapaldo">ANTONINO PAPALDO</judge> - Prof. <judge refersTo="#nicolaJaeger">NICOLA JAEGER</judge> - Prof. <judge refersTo="#biagioPetrocelli">BIAGIO PETROCELLI</judge> - 
 Dott. <judge refersTo="#antonioManca">ANTONIO MANCA</judge> - Prof. <judge refersTo="#aldoSandulli">ALDO SANDULLI</judge> - Prof. <judge refersTo="#giuseppeBranca">GIUSEPPE BRANCA</judge> - 
 Prof. <judge refersTo="#micheleFragali">MICHELE FRAGALI</judge> - Dott. <judge refersTo="#giuseppeVerzì">GIUSEPPE VERZÌ</judge>- Dott. <judge refersTo="#giovanniBattistaBenedetti">GIOVANNI BATTISTA 
 BENEDETTI</judge> - Prof. <judge refersTo="#francescoPaoloBonifacio">FRANCESCO PAOLO BONIFACIO</judge> - Dott. <judge refersTo="#luigiOggioni">LUIGI OGGIONI</judge>, 
 Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1965-02-12/162/!main">D.P.R. 12  febbraio  
 1965,  n.  162</ref>, concernente "Norme per la repressione delle frodi nella  
 preparazione e nel commercio dei mosti, vini ed  aceti",  promosso  con  
 ordinanza  emessa  il  7  dicembre  1966  dal  pretore    di  Fermo nel  
 procedimento penale a carico di Savini Domenico, iscritta al n. 15  del  
 Registro  ordinanze  1967  e  pubblicata nella Gazzetta Ufficiale della  
 Repubblica n. 51 del 25 febbraio 1967;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udita nella camera di consiglio del 1 giugno 1967 la relazione  del  
 Giudice Antonino Papaldo;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Ritenuto  che  con l'ordinanza sopra indicata è stata sollevata la  
 questione di legittimità costituzionale del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1965-02-12/162/!main">D.P.R. 12  febbraio  1965,  
 n.  162</ref>,  in  riferimento  all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_76">art.    76 della Costituzione</ref> in quanto,  
 essendo la legge delega  9  ottobre  1964,  n.  991,  pubblicata  sulla  
 Gazzetta  Ufficiale  del  28  ottobre  1964,  entrata  in  vigore il 12  
 novembre 1964, il termine di tre mesi, prefissato per l'esercizio della  
 delega, sarebbe scaduto nel giorno 11 febbraio 1965,  mentre  la  legge  
 delegata è stata emanata il successivo giorno 12;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che  l'ordinanza  è  stata  notificata,  comunicata  e  pubblicata  
 ritualmente, ma nessuno si è costituito in questa sede;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Considerato che con diverse sentenze (numeri 13, 32  e 33 del 1967)  
 questa Corte ha dichiarato non fondata la questione predetta e che  non  
 sono  state  addotte e non sussistono ragioni che inducano a modificare  
 le precedenti decisioni;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli articoli 26, secondo comma, e 29  della  <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge  11  marzo  
 1953,  n. 87</ref>, e l'art.  9, secondo comma, delle Norme integrative per i  
 giudizi davanti alla Corte costituzionale;</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     dichiara la manifesta infondatezza della questione di  legittimità  
 costituzionale sollevata con l'ordinanza indicata in epigrafe ed ordina  
 la restituzione degli atti al pretore di Fermo.</block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  
 Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="1967-06-15">15 giugno 1967</date>.</block>
      <p><signature><judge refersTo="#gaspareAmbrosini">GASPARE  AMBROSINI</judge></signature> - <signature><judge refersTo="#antoninoPapaldo">ANTONINO PAPALDO</judge></signature>  
                                   - <signature><judge refersTo="#nicolaJaeger">NICOLA JAEGER</judge></signature> - <signature><judge refersTo="#biagioPetrocelli">BIAGIO PETROCELLI</judge></signature> -  
                                   <signature><judge refersTo="#antonioManca">ANTONIO  MANCA</judge></signature>  -  <signature><judge refersTo="#aldoSandulli">ALDO  SANDULLI</judge></signature>   -  
                                   <signature><judge refersTo="#giuseppeBranca">GIUSEPPE  BRANCA</judge></signature>  - <signature><judge refersTo="#micheleFragali">MICHELE FRAGALI</judge></signature> -  
                                   <signature><judge refersTo="#giuseppeVerzì">GIUSEPPE  VERZÌ</judge></signature> -  <signature><judge refersTo="#giovanniBattistaBenedetti">GIOVANNI  BATTISTA  
                                   BENEDETTI</judge></signature> - <signature><judge refersTo="#francescoPaoloBonifacio">FRANCESCO PAOLO BONIFACIO</judge></signature>  
                                   - <signature><judge refersTo="#luigiOggioni">LUIGI OGGIONI</judge></signature>.</p>
    </conclusions>
  </judgment>
</akomaNtoso>
