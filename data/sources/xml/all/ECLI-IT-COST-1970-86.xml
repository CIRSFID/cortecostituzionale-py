<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>86</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:86</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Michele Fragali</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/06/1970</data_decisione>
    <data_deposito>10/06/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt. 164,  &#13;
 secondo comma n. 1, e 168 del codice penale, promossi con  le  seguenti  &#13;
 ordinanze:                                                               &#13;
     1)  ordinanza  emessa  l'8 novembre 1968 dal pretore di Caltagirone  &#13;
 nel procedimento penale a carico di Parasole Filippo,  iscritta  al  n.  &#13;
 270  del  registro ordinanze 1968 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 25 del 29 gennaio 1969;                              &#13;
     2) ordinanza emessa il 2 dicembre 1968 dal tribunale di Livorno nel  &#13;
 procedimento penale a carico di Picchi Armando, iscritta al n. 271  del  &#13;
 registro  ordinanze  1968  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 25 del 29 gennaio 1969;                                    &#13;
     3) ordinanza emessa il 16 giugno 1969 dal  pretore  di  Torino  nel  &#13;
 procedimento penale a carico di Incoli Giovanni, iscritta al n. 399 del  &#13;
 registro  ordinanze  1969  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 280 del 5 novembre 1969.                                   &#13;
     Visti gli  atti  d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica del 24 marzo 1970 il Giudice relatore  &#13;
 Michele Fragali;                                                         &#13;
     uditi i sostituti avvocati generali dello Stato Franco Chiarotti  e  &#13;
 Franco Casamassima, per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     In  riferimento all'art. 3 della Costituzione sono state proposte a  &#13;
 questa Corte due questioni di legittimità costituzionale dell'art. 168  &#13;
 del codice penale.                                                       &#13;
     La prima (ordinanza 8 novembre 1968  del  pretore  di  Caltagirone)  &#13;
 concerne  la parte del n. 1 di quello articolo che dispone la revoca di  &#13;
 diritto della sospensione condizionale della pena  anche  nel  caso  in  &#13;
 cui, dopo una prima condanna a pena detentiva, per il nuovo reato debba  &#13;
 essere inflitta una pena pecuniaria. Il pretore rileva che la norma non  &#13;
 si  armonizza con l'art. 163 (rectius: art. 164) del codice penale, per  &#13;
 il  quale,  se vi è condanna a pena pecuniaria sospesa, nell'occasione  &#13;
 di un secondo  processo,  la  concessione  del  beneficio  può  essere  &#13;
 ripetuta  a  condizione  che il condannato paghi la pena pecuniaria nel  &#13;
 termine stabilito  dal  giudice,  salvo  che  il  condannato  si  trovi  &#13;
 nell'impossibilità  di  adempiere; osserva che se una prima condanna a  &#13;
 pena pecuniaria non impedisce, sia pure alla  condizione  predetta,  la  &#13;
 rinnovazione  del beneficio quando la condanna posteriore deve essere a  &#13;
 pena detentiva, a fortiori una condanna a pena  pecuniaria  susseguente  &#13;
 ad  una  condanna  a  pena detentiva merita di essere sospesa, sia pure  &#13;
 alla condizione citata. Poiché l'art. 168, n. 1, non dice  altrettanto  &#13;
 per la revoca, esso violerebbe l'art. 3.                                 &#13;
     La  seconda questione, promossa dal tribunale di Livorno (ordinanza  &#13;
 2 dicembre 1968) e dal pretore di Torino (ordinanza  16  giugno  1969),  &#13;
 riguarda  l'art.    168,  n.  2,  del codice penale, nella parte in cui  &#13;
 impone la revoca della sospensione condizionale della pena qualora, nei  &#13;
 termini di legge, il condannato riporti altra condanna, anche se questa  &#13;
 seconda condanna riguardi un reato legato al primo  dal  vincolo  della  &#13;
 continuazione. La prima ordinanza si richiama alla giurisprudenza della  &#13;
 cassazione,  secondo  la  quale  il reato continuato è una mera fictio  &#13;
 iuris che non fa venir meno l'autonomia  delle  distinte  violazioni  e  &#13;
 quindi  delle  rispettive sentenze di condanna; rileva che è meramente  &#13;
 casuale la circostanza della duplice successiva condanna, cosicché non  &#13;
 può impedirsi che si sospenda la pena inflitta  nel  secondo  processo  &#13;
 ove,  se  l'imputato fosse stato giudicato in un solo processo, avrebbe  &#13;
 potuto godere del beneficio. Il pretore di Torino si rifà invece  alla  &#13;
 ratio  comunemente  esposta  a sostegno dell'interpretazione favorevole  &#13;
 alla revoca nel caso predetto: essere cioè  la  revoca  stabilita  nel  &#13;
 presupposto  che  il  giudice  della  prima condanna, ove fosse stato a  &#13;
 conoscenza della continuazione, non avrebbe dato il beneficio;  osserva  &#13;
 inoltre  che  non  sempre  può  disporsi la riunione dei procedimenti,  &#13;
 potendo essi appartenere alla competenza di diverse autorità.           &#13;
     Il pretore di Torino denuncia pure la violazione  dell'articolo  27  &#13;
 della Costituzione, in quanto la revoca del beneficio viene a dipendere  &#13;
 dal   comportamento   dell'imputato,  anteriore  alla  concessione  del  &#13;
 beneficio, che non ha fatto rilevare di aver commesso  altri  reati  in  &#13;
 unità di disegno criminoso con quello da giudicare.                     &#13;
     2.  -  La  presidenza  del  Consiglio  è  intervenuta soltanto nei  &#13;
 giudizi promossi con le ordinanze del  pretore  di  Caltagirone  e  del  &#13;
 tribunale di Livorno.                                                    &#13;
     Sulla questione relativa all'art. 168, n. 1, del codice penale essa  &#13;
 ha  obiettato  che  la  situazione  del  condannato  a  pena  detentiva  &#13;
 condizionatamente  sospesa,  il  quale  riporti   successivamente   una  &#13;
 condanna  a  pena pecuniaria, si differenzia notevolmente da quella del  &#13;
 condannato  a  pena  pecuniaria  condizionatamente  sospesa,  il  quale  &#13;
 successivamente riporti una condanna a pena detentiva.                   &#13;
     Per  quanto  attiene  alla  questione riguardante l'art. 168, n. 2,  &#13;
 stesso codice, la presidenza del Consiglio  ha  contestato  l'esattezza  &#13;
 dell'orientamento della giurisprudenza, che comprende nella norma anche  &#13;
 l'ipotesi  di  seconda  condanna  a  seguito di continuazione; comunque  &#13;
 sostiene che l'asserita disuguaglianza non può dirsi  che  dipenda  da  &#13;
 una norma, dato che consegue al comportamento dell'imputato, e aggiunge  &#13;
 che  questo,  non  rivelando  al  giudice di avere commesso altri reati  &#13;
 legati a quelli in giudizio dal vincolo della continuazione, ha assunto  &#13;
 il rischio di subire la revoca della  prima  sospensione  quando  fosse  &#13;
 condannato con sentenza successiva.                                      &#13;
     3.  -  All'udienza del 24 marzo 1970 la difesa della presidenza del  &#13;
 Consiglio ha confermato le proprie tesi e conclusioni.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - È indubitabile che ciascuna delle  questioni  proposte  dalle  &#13;
 tre  ordinanze,  solo  per  ragioni  di  rilevanza  è  stata  riferita  &#13;
 separatamente ed esclusivamente a uno solo dei numeri di cui si compone  &#13;
 il primo comma dell'art. 168 del codice penale,  mentre,  per  la  loro  &#13;
 sostanza,  ogni questione riguarda tutte le fattispecie enunciate nello  &#13;
 stesso articolo.                                                         &#13;
     Le cause debbono perciò essere decise con una sola sentenza.        &#13;
     2. - Sulla seconda delle questioni,  esattamente  il  tribunale  di  &#13;
 Livorno e il pretore di Torino rilevano che il caso di cui sono oggetto  &#13;
 le loro ordinanze, riguardante fatti legati da nesso di continuità con  &#13;
 altri   puniti  con  sentenza  precedente,  non  può  essere  trattato  &#13;
 diversamente da quello in cui la continuazione è accertata  con  unica  &#13;
 sentenza;  per  cui,  come,  in  quest'ultimo caso, la pena può essere  &#13;
 sospesa, nel concorso dei presupposti di legge, con riguardo  al  reato  &#13;
 considerato   nella   sua  unità,  così  non  dovrebbe  revocarsi  la  &#13;
 sospensione della prima condanna quando la  seconda,  cumulata  con  la  &#13;
 prima,  non  oltrepassi i massimi indicati nell'art. 163 stesso codice.  &#13;
 Tale ragionamento investe il combinato disposto degli artt. 164,  comma  &#13;
 secondo  n.  1,  e 168, primo comma n. 2, nella parte in cui, quando il  &#13;
 secondo reato sia in relazione di continuità con altro già punito con  &#13;
 pena sospesa, si esclude che il giudice possa esercitare il  potere  di  &#13;
 concedere  o di negare per l'intera pena il beneficio della sospensione  &#13;
 condizionale e si impone che sia revocata  di  diritto  la  sospensione  &#13;
 condizionale già concessa.                                              &#13;
     Si  muove,  nelle ordinanze, dal contenuto che la giurisprudenza ha  &#13;
 dato alle norme denunciate:  infatti si è giudicato che la sospensione  &#13;
 della prima condanna deve essere revocata quando la  continuazione  del  &#13;
 reato  emerge  in  un  processo successivo. Con questo contenuto vivono  &#13;
 perciò le norme predette; ma esse,  nella  sostanza,  fanno  dipendere  &#13;
 l'esistenza  del  nesso  di  continuità  fra  due reati da circostanze  &#13;
 occasionali, e cioè  a  dire,  dal  fatto  che  la  continuazione  sia  &#13;
 accertata  in  un  solo tempo anziché in tempi successivi, circostanze  &#13;
 che non possono  elevarsi  a  fondamento  di  una  diversa  disciplina.  &#13;
 Assumere,  coll'Avvocatura,  che  la  scoperta  di fatti anteriori alla  &#13;
 prima condanna smentisca  la  presunzione  di  ravvedimento  posta  dal  &#13;
 giudice a giustificazione del beneficio accordato, vuol dire denunciare  &#13;
 l'irrazionalità  della  distinzione,  anziché  giustificarla: infatti  &#13;
 nemmeno nel primo giudizio la continuazione può  essere,  di  per  sé  &#13;
 sola,  ragione  di  rifiuto  del  beneficio  della sospensione, dovendo  &#13;
 sempre verificarsene la rilevanza per decidere se possa presumersi  che  &#13;
 l'imputato  si  asterrà dal commettere altri reati. La circostanza che  &#13;
 il  primo  giudice  non  era  a  notizia  che  l'imputato   aveva,   in  &#13;
 continuazione,  ancora  violato  la  legge  penale,  non  può  perciò  &#13;
 impedire al secondo giudice di compiere gli apprezzamenti  che  avrebbe  &#13;
 fatto  il primo, e imporgli di sostituire, al suo libero convincimento,  &#13;
 una  presunzione  legale  di  inopportunità  della  sospensione.  Tale  &#13;
 inopportunità non può spiegarsi nemmeno con il rilievo che l'imputato  &#13;
 non  rese  noto  al giudice di aver commesso i nuovi reati, perché, se  &#13;
 così  potesse  ragionarsi,  dalla  norma  si  farebbe   derivare   una  &#13;
 inconcepibile  sanzione  alla  reticenza dell'imputato; al quale invece  &#13;
 l'ordinamento  garantisce  piena libertà di comportamento processuale,  &#13;
 al riparo dalla presunzione della sua non colpevolezza.                  &#13;
     Il legame logico tra gli artt. 164  e  168  del  codice  penale  è  &#13;
 indiscutibile: ed è irrazionale inibire al giudice chiamato a decidere  &#13;
 sulla  revoca  della  sospensione  quegli  apprezzamenti  che egli può  &#13;
 compiere quando deve decidere se la pena debba sospendersi.              &#13;
     3.  -    L'altra  questione,  quella  proposta   dal   pretore   di  &#13;
 Caltagirone,  pone  in  risalto  l'incoerenza tra il principio adottato  &#13;
 nell'art. 168 del codice penale, che non distingue pena  da  pena  agli  &#13;
 effetti  della  revoca  di  una precedente sospensione, e l'art.   164,  &#13;
 quinto comma, stesso codice  che,  al  contrario,  agli  effetti  della  &#13;
 concessione  del  beneficio della sospensione, differenzia caso da caso  &#13;
 in relazione al tipo  di  pena  che  deve  comminarsi  con  la  seconda  &#13;
 sentenza.  Dato  il rilevato legame logico che esiste tra concessione e  &#13;
 revoca del beneficio, poggiare  i  poteri  del  giudice  riguardo  alla  &#13;
 sospensione della pena su presupposti meno rigidi di quelli ai quali si  &#13;
 informa  il  dovere  di  revocare la sospensione, è chiara prova della  &#13;
 violazione del principio di eguaglianza:  secondo la legge  il  giudice  &#13;
 dovrebbe revocare il beneficio in casi in cui gli è invece permesso di  &#13;
 concederlo  e  dovrebbe  concederlo in casi in cui egli è tenuto poi a  &#13;
 revocarlo.                                                               &#13;
     In particolare, il pretore di Caltagirone ha  esteso  il  confronto  &#13;
 fra  l'art.  164  predetto  e  il  successivo  art.  168 all'ipotesi di  &#13;
 successione  di  una  pena  pecuniaria  a  una  pena   detentiva,   non  &#13;
 espressamente  regolata  dall'art.  164,  quinto comma, e ritiene che a  &#13;
 fortiori il giudice può concedere il  beneficio  quando  ad  una  pena  &#13;
 detentiva  debba seguire la pena pecuniaria che l'imputato sia disposto  &#13;
 a pagare,  essendo  la  fattispecie  meritevole  di  un  più  benevolo  &#13;
 apprezzamento,  come  indice,  anziché  di  un aggravarsi della spinta  &#13;
 criminosa, al pari del caso contemplato dall'art. 164, quinto comma, di  &#13;
 una attenuazione della spinta stessa.                                    &#13;
     Il giudice a quo esattamente cioè opina che l'art.    164,  quinto  &#13;
 comma, permette all'imputato di godere del beneficio ove paghi entro un  &#13;
 congruo  termine  l'importo della pena pecuniaria, senza far differenze  &#13;
 tra il caso  in  cui  la  seconda  sentenza  deve  comminare  una  pena  &#13;
 detentiva e quello in cui deve ripetere una condanna a pena pecuniaria;  &#13;
 in altre parole consente a colui che in due tempi successivi sia punito  &#13;
 con pena pecuniaria e con pena detentiva, di fruire del beneficio della  &#13;
 sospensione  della  pena detentiva, indipendentemente dal fatto che gli  &#13;
 sia stata irrogata prima la pena pecuniaria e poi  quella  detentiva  o  &#13;
 viceversa. Così essendo, viene a dimostrarsi che l'art. 168 del codice  &#13;
 penale  viola  l'art.  3  della  Costituzione:  la  norma  impugnata si  &#13;
 manifesta lesiva della regola di eguaglianza nella  parte  in  cui  non  &#13;
 distingue fra le due ipotesi e permette che la revoca della sospensione  &#13;
 possa  pronunciarsi,  con  riguardo  al  caso di pena pecuniaria, anche  &#13;
 quando la  sospensione  dovrebbe  essere  concessa  secondo  quanto  è  &#13;
 prescritto nell'art. 164.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale degli artt.   164, comma  &#13;
 secondo n. 1, e 168 del codice penale, nella parte  in  cui  dispongono  &#13;
 che  il  giudice  non possa esercitare il potere di concedere o negare,  &#13;
 per la pena da comminare, il beneficio della sospensione condizionale o  &#13;
 debba  revocare  di  diritto  la  sospensione  già  concessa quando il  &#13;
 secondo reato si lega con il vincolo della continuità a quello  punito  &#13;
 con pena sospesa;                                                        &#13;
     dichiara  l'illegittimità costituzionale dello stesso art. 168 del  &#13;
 codice  penale,  nella  parte  in  cui,  per  l'ipotesi  di  successiva  &#13;
 irrogazione  di pena pecuniaria, non conferisce al giudice il potere di  &#13;
 subordinare la revoca della sospensione della pena detentiva al mancato  &#13;
 pagamento della pena pecuniaria.                                         &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 3 giugno 1970.                                &#13;
                                   GIUSEPPE  BRANCA - MICHELE FRAGALI  -  &#13;
                                   COSTANTINO   MORTATI    -    GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE   VERZÌ   -  &#13;
                                   GIOVANNI   BATTISTA    BENEDETTI    -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
