<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>36</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:36</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/02/1969</data_decisione>
    <data_deposito>17/03/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI &#13;
 BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Dott. NICOLA REALE, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 4 del R.D. 17  &#13;
 agosto 1935,  n.  1765,  contenente  disposizioni  per  l'assicurazione  &#13;
 obbligatoria degli infortuni sul lavoro e delle malattie professionali,  &#13;
 promosso  con ordinanza emessa il 1 febbraio 1967 dal tribunale di Roma  &#13;
 nel procedimento civile vertente tra Di Lonardo Francesco e la società  &#13;
 Squibb, iscritta al n. 92 del  Registro  ordinanze  1967  e  pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 157 del 24 giugno 1967.     &#13;
     Udita  nella  camera di consiglio del 27 febbraio 1969 la relazione  &#13;
 del Giudice Costantino Mortati.                                          &#13;
     Ritenuto che con ordinanza 1 febbraio 1967, pronunciata  nel  corso  &#13;
 della  causa  civile  vertente  tra  Di Lonardo Francesco e la società  &#13;
 Squibb, il tribunale di Roma ha  sollevato  questione  di  legittimità  &#13;
 costituzionale  dell'art.  4  del  R.D.  17  agosto  1935, n. 1765, per  &#13;
 violazione degli artt. 3, 35, 36, 38 e 41 della Costituzione;            &#13;
     che nel giudizio avanti la Corte costituzionale si è costituito il  &#13;
 Di Lonardo, il quale ha concluso per la dichiarazione  d'illegittimità  &#13;
 costituzionale  della  norma  impugnata  (nel  nuovo  testo  risultante  &#13;
 dall'art. 10 del D.P.R. 30 giugno 1965, n. 1124);                        &#13;
     Considerato che dopo la pronuncia dell'ordinanza di rimessione, con  &#13;
 sentenza n. 22 del 9 marzo 1967, questa  Corte,  mentre  ha  dichiarato  &#13;
 costituzionalmente illegittimi:  1) il terzo comma dell'art. 4 del R.D.  &#13;
 17  agosto 1935, n.  1765, nella parte in cui limita la responsabilità  &#13;
 civile del datore di lavoro per  infortunio  sul  lavoro  derivante  da  &#13;
 reato  all'ipotesi  in  cui  questo sia stato commesso dagli incaricati  &#13;
 della direzione o sorveglianza del  lavoro  e  non  anche  dagli  altri  &#13;
 dipendenti  del cui fatto debba rispondere secondo il Codice civile; 2)  &#13;
 il quinto comma dello  stesso  articolo,  in  quanto  consente  che  il  &#13;
 giudice  possa  accertare  che  il  fatto che ha provocato l'infortunio  &#13;
 costituisce reato soltanto  nelle  ipotesi  di  estinzione  dell'azione  &#13;
 penale  per  morte  dell'imputato  o  per  amnistia,  senza  menzionare  &#13;
 l'ipotesi di prescrizione del reato, e le  corrispondenti  disposizioni  &#13;
 dell'art.  10  del  decreto  del  Presidente della Repubblica 30 giugno  &#13;
 1965, n. 1124,  ha  dichiarato  invece  non  fondata  la  questione  di  &#13;
 legittimità  costituzionale  dell'art.  4,  primo e secondo comma, del  &#13;
 R.D. 17 agosto 1935, n. 1765, predetto, in riferimento  agli  artt.  3,  &#13;
 primo  e  secondo  comma,  35  e  38  della Costituzione, che era stata  &#13;
 sollevata dal tribunale di Milano;                                       &#13;
     che  la questione attualmente sollevata riguarda le parti dell'art.  &#13;
 4 in relazione alle quali la questione è stata dichiarata non fondata;  &#13;
     che non sussistono ragioni che inducano a modificare la  precedente  &#13;
 decisione;                                                               &#13;
     che  gli argomenti in essa svolti valgono anche per quanto riguarda  &#13;
 le dedotte violazioni degli artt. 36 e 41 della  Costituzione.  Infatti  &#13;
 il  richiamo  al  primo  di  detti articoli non assume autonomo rilievo  &#13;
 rispetto agli altri (35 e 38) presi in considerazione con la precedente  &#13;
 sentenza, e quanto all'art. 41 è agevole osservare come  il  principio  &#13;
 formulato  al  secondo  comma  trova,  per  quanto  riguarda  i casi di  &#13;
 inabilità al lavoro, le necessarie specificazioni nell'art. 38.         &#13;
     Visti gli artt. 26, comma secondo, e 29 della legge 11 marzo  1953,  &#13;
 n. 87, e l'art. 9, comma secondo, delle norme integrative per i giudizi  &#13;
 davanti a questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale dell'art. 4 del  R.D.  17  agosto  1935,  n.  1765,  per  &#13;
 violazione  degli artt. 3, 35, 36, 38 e 41 della Costituzione, promossa  &#13;
 con ordinanza 1 febbraio 1967 del tribunale di Roma.                     &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 febbraio 1969.       &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - NICOLA REALE.</dispositivo>
  </pronuncia_testo>
</pronuncia>
