<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>44</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:44</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/02/1996</data_decisione>
    <data_deposito>23/02/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Giuliano VASSALLI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 8 della legge    &#13;
 15 luglio  1966,  n.  604  (Licenziamenti  individuali),  così  come    &#13;
 modificato  dall'art.  2 della legge 11 maggio 1990, n. 108, promosso    &#13;
 con ordinanza emessa il  4  novembre  1994  dal  tribunale  di  Busto    &#13;
 Arsizio, nel procedimento civile vertente tra Impresa "N.T. - Nizzoli    &#13;
 trasporti"  e  America  Marcello,  iscritta  al  n.  542 del registro    &#13;
 ordinanze  1995  e  pubblicata  nella     Gazzetta  Ufficiale   della    &#13;
 Repubblica, n. 40, prima serie speciale, dell'anno 1995;                 &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del  24  gennaio  1996  il  giudice    &#13;
 relatore Fernando Santosuosso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Nel  corso  di  un giudizio avente ad oggetto l'impugnazione    &#13;
 della sentenza del pretore di Busto Arsizio - sezione  distaccata  di    &#13;
 Gallarate  -  con  la  quale  era  stata dichiarata l'inefficacia del    &#13;
 licenziamento intimato dall'impresa "N.T. -  Nizzoli  trasporti"  nei    &#13;
 confronti   di   America  Marcello,  e  conseguentemente  pronunciata    &#13;
 condanna del datore di lavoro alla reintegrazione del ricorrente  nel    &#13;
 posto di lavoro entro tre giorni o, in mancanza, a risarcire il danno    &#13;
 quantificato  in L. 9.901.500, oltre accessori, il tribunale di Busto    &#13;
 Arsizio, con ordinanza in data 4  novembre  1994,  ha  sollevato,  in    &#13;
 riferimento  agli  artt.  3  e  24  della  Costituzione, questione di    &#13;
 legittimità costituzionale dell'art. 8 della legge 15  luglio  1966,    &#13;
 n.  604  (Norme  sui  licenziamenti  individuali), nella parte in cui    &#13;
 attribuisce al  datore  di  lavoro  la  facoltà  di  scelta  fra  la    &#13;
 riassunzione del lavoratore ed il risarcimento del danno.                &#13;
   Rilevato  che  l'impresa aveva invitato il ricorrente a presentarsi    &#13;
 al lavoro e che lo stesso non si era presentato  proponendo  precetto    &#13;
 contro  la  datrice  di  lavoro per il pagamento del risarcimento del    &#13;
 danno, osserva il giudice a quo  che,  mentre  per  i  dipendenti  di    &#13;
 impresa  "maggiore"  compete al creditore/lavoratore la scelta fra la    &#13;
 reintegrazione o il risarcimento del danno ex  art. 18, quinto comma,    &#13;
 della legge 20 maggio 1970, n.  300,  per  le  imprese  "minori",  la    &#13;
 facoltà di scelta compete,  ex art. 8 della legge 15 luglio 1966, n.    &#13;
 604,  al debitore/datore di lavoro (come già ritenuto dalla Corte di    &#13;
 Cassazione, 3/1/1986, n. 33).                                            &#13;
   Premesso  che  le  fattispecie  debbono  essere  qualificate   come    &#13;
 obbligazione   alternativa  con  facoltà  di  scelta  a  favore  del    &#13;
 creditore/lavoratore  nella  legge  20  maggio  1970,   n.   300,   e    &#13;
 obbligazione facoltativa da parte del debitore/datore di lavoro nella    &#13;
 legge  15  luglio 1966, n.  604, il rimettente osserva che, in base a    &#13;
 tale principio, il lavoratore di impresa "minore" che non si presenti    &#13;
 in azienda dopo la scelta del datore di  riassumerlo,  perderebbe  il    &#13;
 diritto  al  risarcimento  ex  art.  1286,  secondo comma, del codice    &#13;
 civile.                                                                  &#13;
   Al dipendente di impresa "minore" verrebbe pertanto  assicurato  un    &#13;
 diverso   e  più  sfavorevole  trattamento  e  non  sarebbe  inoltre    &#13;
 consentito il  concreto  esercizio  dell'azione  per  la  tutela  del    &#13;
 diritto  poiché,  per un anno almeno - tempo necessario per ottenere    &#13;
 una sentenza esecutiva di primo grado - il  lavoratore  non  dovrebbe    &#13;
 lavorare per impedire che, all'esito della causa, perda il diritto al    &#13;
 risarcimento  e  non  possa più essere reintegrato, per aver trovato    &#13;
 altra occupazione.                                                       &#13;
   2. - Nel giudizio davanti alla Corte è intervenuto  il  Presidente    &#13;
 del  Consiglio  dei  Ministri,  rappresentato  dall'Avvocatura  dello    &#13;
 Stato, chiedendo  che  la  questione  sia  dichiarata  manifestamente    &#13;
 infondata.                                                               &#13;
   L'Avvocatura  osserva  come  il  legislatore  del 1990 abbia inteso    &#13;
 confermare la compresenza  di  due  normative,  quella  della  tutela    &#13;
 "reale"  ex art. 18 della legge n. 300 del 1970 e quella della tutela    &#13;
 "obbligatoria" ex  art. 8 della legge n. 604 del 1966.                   &#13;
   Inoltre, la considerazione della scarsa potenzialità economica dei    &#13;
 datori di lavoro di minori dimensioni, se non è riuscita ad impedire    &#13;
 l'estensione di una disciplina  vincolistica  del  licenziamento,  ha    &#13;
 tuttavia  provocato  una  fissazione  a  livello  minimale dell'onere    &#13;
 indennitario a carico del datore che ha  proceduto  al  licenziamento    &#13;
 ingiustificato.                                                          &#13;
   Sottolinea  ancora  l'Avvocatura  che  da  questo  quadro normativo    &#13;
 emerge chiaramente che il legislatore del  1990  ha  privilegiato  la    &#13;
 prospettiva  occupazionale su quella meramente (e più limitatamente)    &#13;
 risarcitoria, per cui risulta del tutto ellittico il caso  in  esame,    &#13;
 in  quanto il lavoratore vorrebbe poter scegliere una forma di tutela    &#13;
 (pagamento dell'indennità  "ridotta")  che  è  obiettivamente  più    &#13;
 sfavorevole di quella in primis apprestata dal legislatore.<diritto>Considerato in diritto</diritto>1.  -  Il  tribunale  di  Busto  Arsizio  dubita della legittimità    &#13;
 costituzionale dell'art.  8  della  legge  15  luglio  1966,  n.  604    &#13;
 (Licenziamenti  individuali), così come modificato dall'art. 2 della    &#13;
 legge 11 maggio 1990, n.  108, nella parte in cui prevede il  diritto    &#13;
 di  scelta fra la riassunzione ed il risarcimento a favore del datore    &#13;
 di lavoro.                                                               &#13;
   A parere del giudice a quo sussisterebbe  contrasto  con  l'art.  3    &#13;
 della  Costituzione,  in  quanto  il  lavoratore  di impresa "minore"    &#13;
 godrebbe di un trattamento più sfavorevole rispetto al lavoratore di    &#13;
 impresa "maggiore", nonché con  l'art.  24  della  Costituzione  non    &#13;
 essendo   in  concreto  concessa  tutela  al  lavoratore  di  impresa    &#13;
 "minore".                                                                &#13;
   2. - La questione non è fondata dovendosi  interpretare  la  norma    &#13;
 impugnata nei sensi che saranno di seguito precisati.                    &#13;
   Questa  Corte  ha  più volte (sentenze nn. 398 del 1994, 189 e 102    &#13;
 del 1975, 55 del 1974) indicato i motivi razionali  che  giustificano    &#13;
 la  diversificazione  del  regime  dei  licenziamenti  individuali in    &#13;
 ragione delle dimensioni dell'impresa, evidenziando  che  essi  vanno    &#13;
 ricercati  nelle  esigenze  di funzionalità delle unità produttive,    &#13;
 soprattutto ai fini  occupazionali,  nonché  nel  diverso  grado  di    &#13;
 fiduciarietà  e  di  tensione psicologica riscontrabile nei rapporti    &#13;
 diretti  fra  dipendente  e  piccolo   imprenditore   rispetto   alla    &#13;
 situazione nella grande impresa.                                         &#13;
   Nel  confermare  questa giurisprudenza, va ribadito che, nella sola    &#13;
 ipotesi di imprese minori,  la  legge  ragionevolmente  riconosce  al    &#13;
 datore  di lavoro la scelta in ordine alla possibilità di riassumere    &#13;
 il lavoratore illegittimamente licenziato, ovvero di  risarcirgli  il    &#13;
 danno conseguente all'accertata illegittimità del licenziamento.        &#13;
   3.  -  La  ragionevolezza  della  differente disciplina tra impresa    &#13;
 minore e maggiore non risolve  tuttavia  il  problema  relativo  alle    &#13;
 ulteriori  conseguenze  scaturenti  dalla predetta scelta operata dal    &#13;
 datore di lavoro e precisamente quello della  esatta  interpretazione    &#13;
 dell'espressione  normativa che impone all'imprenditore l'obbligo, in    &#13;
 mancanza della riassunzione, di risarcire il danno; ciò  costituisce    &#13;
 la sostanza della sollevata questione di costituzionalità.              &#13;
   In   proposito,   il   giudice  a  quo  dubita  della  legittimità    &#13;
 costituzionale della norma interpretata in modo conforme  agli  artt.    &#13;
 1286  e  ss.  del  codice  civile,  e cioè nel senso che, operata la    &#13;
 scelta fra due prestazioni, ciò  determina  l'irrevocabilità  della    &#13;
 stessa, e il debitore resta liberato dalla seconda prestazione.          &#13;
   L'interpretazione   da  cui  muove  l'ordinanza  di  rimessione  è    &#13;
 aderente ad un  orientamento  della  Corte  di  cassazione,  tuttavia    &#13;
 contrastato  da  un  maggior numero di pronunce della stessa, secondo    &#13;
 cui il risarcimento previsto dalla norma  impugnata  costituisce  una    &#13;
 delle  conseguenze della illegittimità del licenziamento: ed invero,    &#13;
 si è  affermato  che,  in  mancanza  (per  qualsiasi  motivo)  della    &#13;
 reintegrazione  (tutela reale e primaria), è dovuta la seconda delle    &#13;
 tutele, e cioè quella obbligatoria, consistente nella monetizzazione    &#13;
 del danno derivante dall'illegittimo licenziamento, ogni  qual  volta    &#13;
 non si ripristini il rapporto.                                           &#13;
   4.  -  Questo  diverso  orientamento giurisprudenziale è condiviso    &#13;
 dalla quasi totalità della dottrina e risulta anche da una risalente    &#13;
 pronuncia di questa Corte (sentenza n. 194 del 1970), la  quale  ebbe    &#13;
 ad   affermare  testualmente:  "Né,  ad  orientare  diversamente  il    &#13;
 giudizio della Corte, valgono i  rilievi  contenuti  nelle  ordinanze    &#13;
 circa  la  ingiustizia  cui  condurrebbe  la  norma che, si sostiene,    &#13;
 escluderebbe l'obbligo del pagamento dell'indennità, nel caso che il    &#13;
 ripristino del rapporto di lavoro non possa aver luogo per causa  non    &#13;
 imputabile al datore di lavoro".                                         &#13;
   "La Corte esclude che tali inconvenienti possano verificarsi ove si    &#13;
 ritenga   -   come  deve  ritenersi  perché  la  norma  conservi  la    &#13;
 riconosciuta  conformità  ai  principi  costituzionali  -   che   il    &#13;
 pagamento  della  indennità,  qualora il rapporto non si ripristini,    &#13;
 sia sempre dovuto e lo sia per il solo fatto del  mancato  ripristino    &#13;
 di  esso,  senza  che a nulla rilevi quale sia il soggetto e quale la    &#13;
 ragione per cui ciò abbia a verificarsi".                               &#13;
   5. - Con tale pronuncia, quindi, questa Corte ha già fatto propria    &#13;
 quella interpretazione della norma che la rende conforme ai  principi    &#13;
 costituzionali.  E,  nella  presente  occasione, non risultano validi    &#13;
 motivi per discostarsi dalla richiamata pronuncia.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata, nei sensi di cui in motivazione, la questione    &#13;
 di  legittimità  costituzionale  dell'art.  8  della legge 15 luglio    &#13;
 1966, n.  604  (Licenziamenti  individuali),  così  come  modificato    &#13;
 dall'art.    2  della  legge  11  maggio  1990,  n. 108, sollevata in    &#13;
 riferimento agli artt. 3 e 24 della Costituzione,  dal  tribunale  di    &#13;
 Busto Arsizio con l'ordinanza indicata in epigrafe.                      &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 febbraio 1996.                             &#13;
                         Il Presidente:  Ferri                            &#13;
                      Il redattore:  Santosuosso                          &#13;
                       Il cancelliere:  Di Paola                          &#13;
   Depositata in cancelleria il 23 febbraio 1996.                         &#13;
               Il direttore della cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
