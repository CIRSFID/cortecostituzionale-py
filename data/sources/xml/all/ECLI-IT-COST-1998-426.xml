<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>426</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:426</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1998</data_decisione>
    <data_deposito>23/12/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, &#13;
 prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo &#13;
 MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. &#13;
 Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 322-bis del    &#13;
 codice di procedura penale,  promosso  con  ordinanza  emessa  il  24    &#13;
 dicembre  1997  dal  Tribunale  di  Firenze nel procedimento penale a    &#13;
 carico di P.F. ed altro, iscritta al n.  98  del  registro  ordinanze    &#13;
 1998  e  pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 9,    &#13;
 prima serie speciale, dell'anno 1998.                                    &#13;
   Udito nella camera di consiglio del 30 settembre  1998  il  giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto  che  il  Tribunale  di  Firenze,  adito  quale giudice di    &#13;
 appello  avverso  i  provvedimenti  cautelari  reali,  ha   sollevato    &#13;
 questione di legittimità costituzionale dell'art. 322-bis del codice    &#13;
 di procedura penale, "nella parte in cui non prevede che le parti ivi    &#13;
 indicate  possano  proporre  appello  anche  contro  le  ordinanze in    &#13;
 materia di sequestro conservativo" in riferimento agli artt. 3  e  24    &#13;
 della Costituzione;                                                      &#13;
     che  il  rimettente premette che il pubblico ministero, a seguito    &#13;
 del rigetto di un'istanza di sequestro conservativo dei beni mobili e    &#13;
 immobili di due imputati, presentata  ai  sensi  degli  artt.  316  e    &#13;
 seguenti   cod.  proc.  pen.,  aveva  proposto  appello  contro  tale    &#13;
 provvedimento,  eccependo  in  subordine,  ove  il  tribunale  avesse    &#13;
 ritenuto  inammissibile  il  gravame, l'illegittimità costituzionale    &#13;
 dell'art. 322-bis cod.  proc. pen;                                       &#13;
     che  il giudice a quo ritenuto di non poter addivenire, stante il    &#13;
 principio  di  tassatività  dei  mezzi  di  impugnazione,   ad   una    &#13;
 interpretazione  estensiva  della  disciplina  delle  impugnazioni in    &#13;
 materia  di  sequestro  conservativo,  ritiene   non   manifestamente    &#13;
 infondata  la  questione  di costituzionalità della disciplina sopra    &#13;
 indicata, richiamandosi, in particolare, ai principi  espressi  nella    &#13;
 sentenza   n.   253   del  1994,  con  cui  la  Corte  ha  dichiarato    &#13;
 l'illegittimità costituzionale dell'art.   669-terdecies cod.  proc.    &#13;
 civ.,  nella  parte in cui non ammette il reclamo avverso l'ordinanza    &#13;
 con cui è stata  rigettata  la  domanda  di  sequestro  conservativo    &#13;
 proposta nel processo civile;                                            &#13;
     che, in riferimento all'art. 3 Cost., ad avviso del rimettente la    &#13;
 mancata  previsione  nel  procedimento penale dell'appello avverso il    &#13;
 provvedimento di diniego del  sequestro  conservativo  determinerebbe    &#13;
 una  ingiustificata disparità di trattamento tra il destinatario del    &#13;
 provvedimento di sequestro, che ha la facoltà di chiedere il riesame    &#13;
 a norma dell'art. 318 cod. proc. pen., e il pubblico  ministero,  che    &#13;
 rimane  privo  di  tutela a fronte di un provvedimento di rigetto del    &#13;
 sequestro da lui richiesto;                                              &#13;
     che non sussisterebbero del resto plausibili ragioni del  diverso    &#13;
 trattamento  riservato  in  tema  di impugnazioni al destinatario del    &#13;
 provvedimento che dispone il sequestro  conservativo  e  al  pubblico    &#13;
 ministero,  non  essendo  ravvisabile nella disciplina che abilita il    &#13;
 pubblico  ministero  a  chiedere  il  sequestro  conservativo  quella    &#13;
 "disparità delle condizioni materiali di partenza", richiamata dalla    &#13;
 sentenza  n.  253 del 1994 come presupposto che potrebbe giustificare    &#13;
 un trattamento differenziato;                                            &#13;
     che  ad  avviso  del   rimettente   la   simmetrica   equivalenza    &#13;
 riconosciuta   nel   processo   civile   nella  materia  de  qua  tra    &#13;
 attore-ricorrente e convenuto-resistente in caso di accoglimento o di    &#13;
 rigetto della domanda di sequestro conservativo  dovrebbe  riproporsi    &#13;
 nel processo penale nei confronti delle posizioni dell'imputato e del    &#13;
 pubblico  ministero  in  caso  di  accoglimento  o  di  rigetto della    &#13;
 richiesta della misura cautelare;                                        &#13;
     che la mancata previsione  dell'appello  del  pubblico  ministero    &#13;
 avverso il provvedimento di diniego del sequestro conservativo appare    &#13;
 al  giudice  a  quo lesiva del principio costituzionale che impone di    &#13;
 garantire alle parti un "giusto processo" con particolare riferimento    &#13;
 all'art.  24  Cost.,  in  quanto  la  diversità  di  disciplina  tra    &#13;
 sequestro  conservativo  e preventivo che si è determinata a seguito    &#13;
 dell'introduzione  dell'art.  322-bis  cod.  proc.  pen.  non   trova    &#13;
 ragionevole     giustificazione,     ed    è    anzi    contraddetta    &#13;
 dall'attribuzione al pubblico ministero della "potestà" di  chiedere    &#13;
 la misura cautelare senza che poi gli venga assicurata l'effettività    &#13;
 della tutela giurisdizionale;                                            &#13;
     che,   infine,  il  rimettente  rileva  che  una  disciplina  che    &#13;
 assicurasse "in maniera  uniforme  i  requisiti  minimi  imposti  dal    &#13;
 sistema  di  garanzie  costituito  dagli  artt.  3  e  24  Cost." con    &#13;
 riferimento alla posizione  delle  parti  nell'esercizio  dei  propri    &#13;
 diritti, dovrebbe necessariamente prevedere l'estensione dell'appello    &#13;
 avverso i provvedimenti in materia di sequestro conservativo a favore    &#13;
 di tutte le parti del processo.                                          &#13;
   Considerato  - per quanto concerne la censura relativa alla dedotta    &#13;
 sperequazione tra la posizione dell'imputato, cui è riconosciuta  la    &#13;
 facoltà  di  proporre  reclamo  avverso  l'ordinanza  che dispone il    &#13;
 sequestro conservativo, e quella del pubblico  ministero,  privo  del    &#13;
 potere   di  impugnare  il  provvedimento  di  diniego  della  misura    &#13;
 cautelare - questa Corte ha  avuto  occasione  di  affermare  in  via    &#13;
 generale  che  la  diversità  dei  poteri  spettanti  in  materia di    &#13;
 impugnazioni all'imputato e al  pubblico  ministero  è  giustificata    &#13;
 dalla differente garanzia rispettivamente loro assicurata dagli artt.    &#13;
 24  e 112 della Costituzione (sentenza n. 98 del 1994); che il potere    &#13;
 di   impugnazione   del   pubblico    ministero    non    costituisce    &#13;
 estrinsecazione   necessaria   dei   poteri   inerenti  all'esercizio    &#13;
 dell'azione penale (sentenza n. 280 del 1995); che il principio della    &#13;
 parità tra accusa e difesa non comporta necessariamente  l'identità    &#13;
 tra  i  poteri  processuali  del  pubblico  ministero e dell'imputato    &#13;
 (sentenza n. 363 del 1991, con particolare riferimento  alla  mancata    &#13;
 previsione dei poteri di impugnazione in capo al pubblico ministero);    &#13;
     che  la  peculiarità della posizione del pubblico ministero e la    &#13;
 piena autonomia del sistema  processuale  penale  rispetto  a  quello    &#13;
 civile rendono ininfluente il richiamo alla sentenza n. 253 del 1994,    &#13;
 in  quanto  il  difetto  di  simmetria  tra  istituti in qualche modo    &#13;
 analoghi dell'uno e dell'altro procedimento non costituisce,  di  per    &#13;
 sé,  indice  di  irragionevolezza  o  di violazione del principio di    &#13;
 eguaglianza (v. da ultimo sentenze n. 326 del 1997 e nn. 51 e 53  del    &#13;
 1998);                                                                   &#13;
     che,  infine,  dal confronto tra la disciplina prevista dall'art.    &#13;
 322-bis cod. proc.  pen.,  che  riconosce  sia  all'imputato  che  al    &#13;
 pubblico   ministero  la  facoltà  di  proporre  appello  contro  le    &#13;
 ordinanze in materia di sequestro preventivo, e quella  sottoposta  a    &#13;
 scrutinio  di  legittimità  costituzionale  a  causa  della  mancata    &#13;
 previsione in capo al  pubblico  ministero  del  potere  di  proporre    &#13;
 appello   avverso   il   provvedimento   di   diniego  del  sequestro    &#13;
 conservativo, non  può  desumersi  alcuna  violazione  dell'art.  24    &#13;
 Cost.:  è  infatti  del  tutto  ragionevole  e plausibile che l'art.    &#13;
 322-bis cod. proc.  pen.  riconosca  il  potere  di  impugnazione  al    &#13;
 pubblico  ministero,  in quanto il sequestro preventivo persegue fini    &#13;
 pubblicistici volti alla prevenzione dei reati (v.  sentenza  n.  334    &#13;
 del  1994),  mentre  il  sequestro conservativo di cui al primo comma    &#13;
 dell'art. 316 cod. proc. pen. è finalizzato  alla  soddisfazione  di    &#13;
 interessi patrimoniali, sia pure facenti capo allo Stato;                &#13;
     che,   pertanto,   la   questione  va  dichiarata  manifestamente    &#13;
 infondata.                                                               &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  322-bis  del  codice  di procedura penale,    &#13;
 sollevata, in riferimento agli artt. 3 e 24 della  Costituzione,  dal    &#13;
 Tribunale di Firenze, con l'ordinanza in epigrafe.                       &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 dicembre 1998.                             &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 dicembre 1998.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
