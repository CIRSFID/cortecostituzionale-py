<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>162</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:162</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/06/1975</data_decisione>
    <data_deposito>26/06/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO - Presidente &#13;
 - Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  401 del  &#13;
 codice di  procedura  penale,  promosso  con  ordinanza  emessa  il  28  &#13;
 settembre  1973  dal  tribunale  di  Mantova  nel procedimento penale a  &#13;
 carico di Giberti Furio, iscritta al n. 38 del registro ordinanze  1974  &#13;
 e  pubblicata  nella  Gazzetta  Ufficiale della Repubblica n. 69 del 13  &#13;
 marzo 1974.                                                              &#13;
     Udito nella camera di consiglio  del  10  aprile  1975  il  Giudice  &#13;
 relatore Paolo Rossi.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di un procedimento penale a carico di Giberti Furio, il  &#13;
 tribunale  di  Mantova  ha  sollevato,  in  riferimento  al   principio  &#13;
 d'eguaglianza   e  al  diritto  di  difesa,  questione  incidentale  di  &#13;
 legittimità costituzionale dell'art. 401 c.p.p., nella  parte  in  cui  &#13;
 limita la deducibilità delle nullità incorse nell'istruzione sommaria  &#13;
 al  termine  di soli 5 giorni dalla notifica del decreto di citazione a  &#13;
 giudizio, anziché consentirla nei 5 giorni dalla notifica al difensore  &#13;
 dell'avviso della data fissata per il dibattimento (art. 410 c.p.p.).    &#13;
     Osserva il giudice a quo che, mentre per i procedimenti istruiti in  &#13;
 via formale la difesa ha diritto di dedurre le nullità nel termine  di  &#13;
 5  giorni  dall'avviso  di  deposito  degli  atti, che è notificato al  &#13;
 difensore, nel caso di procedimenti esperiti con il  rito  sommario  il  &#13;
 termine  decorre  dalla  notifica  del  decreto di citazione a giudizio  &#13;
 all'imputato il quale  può  essere  privo,  quantomeno  in  fatto,  di  &#13;
 assistenza tecnica nel breve termine concessogli.                        &#13;
     Tale  brevità, congiunta alla possibilità che il difensore riceva  &#13;
 la notifica dell'avviso previsto dall'art.  410 c.p.p. dopo la scadenza  &#13;
 dei 5 giorni, violerebbe il diritto di difesa, garantito  dall'art.  24  &#13;
 della  Costituzione.   La minore tutela che ne deriva per colui nei cui  &#13;
 confronti si sia proceduto con istruzione sommaria sarebbe  inoltre  in  &#13;
 contrasto con l'art. 3 della Costituzione.                               &#13;
     Nessuna parte si è costituita o è intervenuta in questa sede.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte  deve  decidere se contrasti o meno con gli artt. 3 e 24,  &#13;
 secondo comma, della Costituzione, l'art. 401 del codice  di  procedura  &#13;
 penale, nella parte in cui fa decorrere il termine di cinque giorni per  &#13;
 la   deduzione   delle  nullità  relative  intercorse  nell'istruzione  &#13;
 sommaria  dalla  notifica  all'imputato  del  decreto  di  citazione  a  &#13;
 giudizio,  anziché  dalla notificazione al difensore dell'avviso della  &#13;
 data fissata per il dibattimento.                                        &#13;
     La questione è fondata.                                             &#13;
     Secondo la giurisprudenza di questa Corte il diritto di difesa, pur  &#13;
 potendo atteggiarsi in funzione  delle  peculiari  caratteristiche  dei  &#13;
 diversi  tipi  di  procedimenti e dei superiori interessi di giustizia,  &#13;
 deve essere garantito in modo effettivo e  adeguato  alle  circostanze,  &#13;
 con modalità che a queste si adattino.                                  &#13;
     Il diritto di difesa, inoltre, va inteso come effettiva potestà di  &#13;
 assistenza  tecnica  e professionale, ed il compito del difensore è di  &#13;
 importanza essenziale, tanto che esso deve  considerarsi  esercizio  di  &#13;
 funzione pubblica.                                                       &#13;
     L'art.  401  c.p.p.  disciplina  le  modalità  formali e temporali  &#13;
 secondo cui far valere le nullità relative intercorse nel procedimento  &#13;
 con istruzione sommaria, concedendo all'imputato un breve  termine  per  &#13;
 denunciarle, a pena di decadenza. Tale termine, tuttavia, decorre dalla  &#13;
 conoscibilità  dell'atto  da  parte del diretto interessato, e non del  &#13;
 suo difensore, benché la cognizione  di  elementi  tecnici  rientranti  &#13;
 nella    specifica   competenza   professionale   del   difensore   sia  &#13;
 indispensabile per rendersi conto delle nullità e far rilevare i  vizi  &#13;
 invalidanti.                                                             &#13;
     La  norma  impugnata  risulta  quindi  contrastante  con l'invocato  &#13;
 principio costituzionale, garantito dall'art. 24 della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale dell'art. 401  del  codice  &#13;
 di  procedura  penale,  nella  parte  in cui fa decorrere il termine di  &#13;
 cinque giorni per  la  deduzione  delle  nullità  relative  intercorse  &#13;
 nell'istruzione  sommaria,  dalla  notifica all'imputato del decreto di  &#13;
 citazione  a  giudizio  anziché  dalla  notificazione   al   difensore  &#13;
 dell'avviso della data fissata per il dibattimento.                      &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 17 giugno 1975.         &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   -   NICOLA  REALE  -  PAOLO  ROSSI  -  &#13;
                                   LEONETTO AMADEI - GIULIO GIONFRIDA  -  &#13;
                                   EDOARDO  VOLTERRA  -  GUIDO  ASTUTI -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
