<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>249</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:249</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Alfio Finocchiaro</relatore_pronuncia>
    <redattore_pronuncia>Alfio Finocchiaro</redattore_pronuncia>
    <data_decisione>23/06/2008</data_decisione>
    <data_deposito>02/07/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 645, secondo comma, 647 e 165, primo comma, del codice di procedura civile e dell'art. 71 delle disposizioni di attuazione dello stesso codice, promosso con ordinanza del 27 luglio 2007 dal Tribunale ordinario di Patti, sezione distaccata di Sant'Agata di Militello nel procedimento civile vertente tra la Sirio Impianti s.n.c. di Faranda Leone e Bonfiglio Carmelo e la R2 s.n.c. di Rubino Aldo &amp;amp; C., iscritta al n. 834 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 3, prima serie speciale, dell'anno 2008. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 21 maggio 2008 il Giudice relatore Alfio Finocchiaro. &#13;
    Ritenuto che il Tribunale ordinario di Patti, sezione distaccata di Sant'Agata di Militello, nel corso di un giudizio di opposizione a decreto ingiuntivo, con ordinanza depositata il 27 luglio 2007, ha sollevato questione di legittimità costituzionale degli artt. 645, secondo comma, 647 e 165, primo comma, del codice di procedura civile e dell'art. 71 delle disposizioni di attuazione del codice di procedura civile, per violazione degli artt. 3, 24 e 111, primo e secondo comma, della Costituzione, nella parte in cui prevedono che il termine per la costituzione dell'opponente a decreto ingiuntivo decorra dalla data di notificazione dell'atto, anziché da quella della consegna dello stesso all'ufficiale giudiziario; &#13;
    che, nella specie, consegnato l'atto di opposizione all'ufficiale giudiziario il 4 ottobre 2004, lo stesso era stato notificato l'8 ottobre 2004, mentre l'opponente si era costituito il 18 ottobre 2004; &#13;
    che – rileva il rimettente – individuare nella consegna dell'atto all'ufficiale giudiziario il momento di inizio della decorrenza del termine di costituzione dell'opponente introdurrebbe in ogni caso (sia o no questo termine dimidiato) una regola coerente con i princípi più volte affermati dalla Corte costituzionale in materia di notificazione e, in aggiunta, pienamente allineata con i princípi del processo giusto e di durata ragionevole; &#13;
    che ciò significherebbe ancorare il dies a quo della costituzione dell'opponente ad un evento – la consegna dell'atto all'ufficiale giudiziario – che, nell'arco del procedimento notificatorio, appare essere l'unico esattamente conoscibile non solo dal giudice (mancando invece una norma che consenta di avere certezza legale della acquisita conoscenza o conoscibilità, in capo a colui che introduce il giudizio d'ingiunzione, dell'avvenuta notificazione), ma anche dallo stesso opponente, il quale sarebbe posto in grado di sapere da quale giorno il suo termine di costituzione inizia a decorrere; &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, che ha concluso per la manifesta inammissibilità e comunque la manifesta infondatezza della questione proposta. &#13;
    Considerato che il Tribunale ordinario di Patti, sezione distaccata di Sant'Agata di Militello, dubita della legittimità costituzionale del combinato disposto degli artt. 645, secondo comma, 647 e 165, primo comma, del codice di procedura civile e dell'art. 71 delle disposizioni di attuazione del codice di procedura civile, nella parte in cui prevede che il termine per la costituzione dell'opponente a decreto ingiuntivo decorra dalla data di notificazione dell'atto, anziché da quella della consegna di esso all'ufficiale giudiziario, per violazione degli artt. 3, 24 e 111, primo e secondo comma, Cost.; &#13;
    che il giudice rimettente ritiene necessario introdurre una regola coerente con i princípi più volte affermati da questa Corte in materia di notificazione, giungendo ad una soluzione – quella della decorrenza del termine per la costituzione dell'opponente dalla consegna dell'atto all'ufficiale giudiziario – allineata ai princípi del giusto processo e della durata ragionevole dello stesso, di cui al nuovo testo dell'art. 111, primo e secondo comma, Cost.; &#13;
    che la rilevanza della questione consiste nel fatto che se il combinato disposto delle norme denunciate fosse dichiarato incostituzionale nella parte in cui prevede che il termine per la costituzione dell'opponente a decreto ingiuntivo decorra dalla data di notificazione dell'atto, anziché da quella della consegna dello stesso all'ufficiale giudiziario, l'opposizione dovrebbe essere dichiarata improcedibile; &#13;
    che nell'ordinanza non vi è un adeguato sviluppo argomentativo del denunciato contrasto con i parametri invocati; &#13;
    che non vi è alcun accenno agli artt. 3 e 24 Cost., mentre anche il riferimento all'art. 111, primo e secondo comma, Cost., è puramente assiomatico, posto che l'attinenza della prospettata necessità di far decorrere il termine di costituzione dalla consegna dell'atto notificando all'ufficiale giudiziario al principio del giusto processo è tutta da dimostrare; &#13;
    che la mancata motivazione della non manifesta infondatezza per insufficiente riferimento ai parametri invocati, è causa di manifesta inammissibilità (ordinanze n. 114 del 2007; n. 39 del 2005; n. 126 del 2003). &#13;
    Visti gli articoli 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale del combinato disposto degli artt. 645, secondo comma, 647 e 165, primo comma, del codice di procedura civile e dell'art. 71 delle disposizioni di attuazione del codice di procedura civile, sollevata, in riferimento agli artt. 3, 24 e 111, primo e secondo comma, della Costituzione, dal Tribunale ordinario di Patti, sezione distaccata di Sant'Agata di Militello, con l'ordinanza in epigrafe. &#13;
     &#13;
Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 giugno 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Alfio FINOCCHIARO, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 2 luglio 2008.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
