<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>297</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:297</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Antonio La Pergola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/11/1985</data_decisione>
    <data_deposito>13/11/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Prof. RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 12, secondo  &#13;
 comma, legge 22 maggio 1978, n. 194 ("Norme per la tutela sociale della  &#13;
 maternità e sull'interruzione volontaria della gravidanza"),  promosso  &#13;
 con  l'ordinanza  emessa  il  12 settembre 1984 dal Giudice tutelare di  &#13;
 Torino sull'istanza proposta da D'Emanuele Simona iscritta al n.   1199  &#13;
 del registro ordinanze 1984 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 44 bis dell'anno 1985.                                     &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nella camera di consiglio  del  9  ottobre  1985  il  Giudice  &#13;
 relatore Antonio La Pergola.                                             &#13;
                              Ritenuto che:                               &#13;
     1.  -  il  giudice  tutelare  di Torino con l'ordinanza indicata in  &#13;
 epigrafe ha sollevato, in riferimento all'art. 3  Cost.,  questione  di  &#13;
 legittimità costituzionale dell'art. 12 della legge 22 maggio 1978, n.  &#13;
 194  ("Norme per la tutela sociale della maternità e sull'interruzione  &#13;
 volontaria della gravidanza"). Questa disposizione, nel disciplinare il  &#13;
 caso in cui la gestante che promuove la  procedura  per  l'interruzione  &#13;
 della  gravidanza  sia  di  età  inferiore  ai diciotto anni, richiede  &#13;
 l'assenso  di  chi  esercita  la  potestà  o  la  tutela,  o  in  casi  &#13;
 particolari l'intervento del giudice tutelare;                           &#13;
     2.  -  il  meccanismo  previsto  dalla  norma  censurata violerebbe  &#13;
 l'indicato    parametro    costituzionale    in    quanto     creerebbe  &#13;
 un'ingiustificata disparità di trattamento:                             &#13;
     a)  tra gestanti maggiorenni e gestanti minorenni, ponendo soltanto  &#13;
 le seconde "nella condizione di  dover  accettare  una  maternità  non  &#13;
 desiderata   per  il  concorrere  della  volontà  contraria  vuoi  dei  &#13;
 genitori, vuoi del giudice", senza che tale risultato si giustifichi in  &#13;
 considerazione del limite gravante in via generale sulla  capacità  di  &#13;
 agire della minore;                                                      &#13;
     b)  entro  la  stessa  cerchia  delle  minorenni,  in  quanto  alle  &#13;
 gestanti, a parità di condizioni, verrebbe impedito ovvero  consentito  &#13;
 di  interrompere  la gravidanza secondo "il diverso atteggiamento che i  &#13;
 genitori o il tutore possono assumere nei  confronti  dell'interruzione  &#13;
 della  gravidanza  e  del nascituro", per via del proprio convincimento  &#13;
 religioso o di altri fattori di costume, culturali, ecc.;                &#13;
     3.  -  il  giudice  a  quo,  pur essendo a conoscenza che la stessa  &#13;
 questione è stata dichiarata infondata con  sentenza  n.  108  (recte,  &#13;
 109)  del  1981,  chiede  che  la  Corte  la  riesamini,  insistendo in  &#13;
 particolare,  sull'assunto  che  la  disparità  di  trattamento  sopra  &#13;
 prospettata al punto 2 sub a) non trovi idoneo o razionale supporto nel  &#13;
 criterio  discretivo  adottato dal legislatore in ordine alla capacità  &#13;
 di agire;                                                                &#13;
     4. - il Presidente del  Consiglio  dei  ministri,  rappresentato  e  &#13;
 difeso  dall'Avvocatura  Generale dello Stato, chiede che la questione,  &#13;
 sulla  base  della  sentenza  n.   109   del   1981,   sia   dichiarata  &#13;
 manifestamente infondata.                                                &#13;
     Considerato  che  la questione, già sollevata dallo stesso giudice  &#13;
 con ordinanze del maggio 1979 e del luglio 1980,  è  stata  dichiarata  &#13;
 non  fondata  dalla  Corte  con  la  citata  sentenza n. 109 del 1981 e  &#13;
 manifestamente infondata con ordinanza n. 80 del 1985;                   &#13;
     che  l'ordinanza  introduttiva  del  presente  giudizio  non  offre  &#13;
 argomenti  che  possano  indurre  la  Corte  a modificare le precedenti  &#13;
 pronunce.                                                                &#13;
     Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.  &#13;
 87,  e  9, secondo comma, delle Norme integrative per i giudizi innanzi  &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la manifesta infondatezza della questione di  legittimità  &#13;
 costituzionale  dell'art.  12  della  legge  22  maggio  1978,  n. 194,  &#13;
 sollevata, in riferimento all'art. 3 Cost.,  dal  giudice  tutelare  di  &#13;
 Torino con l'ordinanza in epigrafe.                                      &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 novembre 1985.       &#13;
                                   F.to: LIVIO PALADIN - ORONZO REALE  -  &#13;
                                   ALBERTO   MALAGUGINI   -  ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   RENATO DELL'ANDRO.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
