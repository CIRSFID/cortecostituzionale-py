<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>118</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:118</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>14/04/2008</data_decisione>
    <data_deposito>24/04/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 19, comma 2, lettere c) e  d) del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), promosso con ordinanza del 3 aprile 2007 dal Giudice di pace di Novara sul ricorso proposto da H.A. contro il Prefetto di Novara, iscritta al n. 637 del registro ordinanze 2007 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 37, prima serie speciale, dell'anno 2007. &#13;
      Visto l'atto di intervento del Presidente del Consiglio dei ministri;  &#13;
    udito nella camera di consiglio del 2 aprile 2008 il Giudice relatore Maria Rita Saulle.  &#13;
    Ritenuto che, con ordinanza del 3 aprile 2007, il giudice di pace di Novara ha sollevato, in riferimento agli artt. 2, 3, 29, 30 e 31 della Costituzione, questione di legittimità costituzionale dell'art. 19, comma 2, lettere c) e d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), nella parte in cui non estende il divieto di espulsione «allo straniero clandestino che sia convivente di una cittadina italiana, nonché padre del nascituro»;  &#13;
    che il giudizio a quo ha ad oggetto il ricorso proposto da H. A., cittadino tunisino, avverso il provvedimento di espulsione emesso nei suoi confronti dal Prefetto di Novara; &#13;
    che il rimettente, dopo aver riferito che il provvedimento impugnato è stato adottato in quanto il ricorrente non ha provveduto al rinnovo del permesso di soggiorno, osserva che dalle dichiarazioni rese da H. A. e dalla sua convivente, di nazionalità italiana, nonché da fonti di prova documentale, risulterebbe che il ricorrente, oltre a convivere con la suddetta cittadina italiana, ha provveduto a riconoscere il nascituro concepito nel corso di tale relazione; &#13;
    che, a parere del rimettente, la mancata previsione, nei casi di specie, di un divieto di espulsione per lo straniero clandestino si pone in contrasto con l'art. 30 della Costituzione che, nel riconoscere piena tutela ai figli nati fuori dal matrimonio, riconosce il diritto di questi ultimi a crescere accanto ai propri genitori, i quali, a loro volta, hanno il diritto-dovere di educarli e istruirli; &#13;
    che, sempre secondo il giudice a quo, l'art. 19, comma 2, lettere c) e d), del d.lgs. n. 286 del 1998, viola l'art. 2 della Costituzione, rientrando tra i diritti inviolabili dell'uomo quello alla paternità ed all'unità della famiglia; &#13;
    che, a parere del rimettente, la disposizione censurata violerebbe, altresì, l'art. 3 della Costituzione, ponendo in essere una disparità di trattamento tra il figlio minore di genitori sposati e quello di genitori semplicemente conviventi, non potendo l'assenza del vincolo coniugale ridondare a danno del minore, il cui superiore interesse trova specifica tutela nell'art. 31 della Costituzione; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sollevata sia dichiarata manifestamente inammissibile o manifestamente infondata; &#13;
    che, a parere della difesa erariale, difetterebbe il requisito della rilevanza, in quanto risulta privo di prova l'assunto, da cui muove il rimettente, secondo il quale vi sarebbe una reale ed effettiva relazione affettiva tra il ricorrente nel giudizio principale e la cittadina italiana, potendo la loro dedotta unione essere solo funzionale alla regolarizzazione della situazione di irregolarità dello straniero clandestino; &#13;
    che l'Avvocatura, dopo aver richiamato la giurisprudenza costituzionale che ha dichiarato manifestamente infondate analoghe questioni – sul presupposto che nella convivenza more uxorio difetta il requisito della certezza dei rapporti giuridici che giustifica il divieto di espulsione del solo marito cittadino extracomunitario – rileva che, in materia di immigrazione, dare rilevanza a rapporti di fatto potrebbe comportare una sostanziale elusione delle norme che disciplinano i flussi migratori. &#13;
    Considerato che il giudice di pace di Novara dubita della legittimità costituzionale dell'art. 19, comma 2, lettere c) e d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), in riferimento agli artt. 2, 3, 29, 30 e 31 della Costituzione, nella parte in cui non estende il divieto di espulsione «allo straniero clandestino che sia convivente di una cittadina italiana, nonché padre del nascituro»; &#13;
    che la sollevata questione si fonda su un doppio presupposto: l'esistenza di un rapporto di convivenza tra una cittadina italiana e uno straniero irregolare e il concepimento durante tale rapporto di un nascituro; &#13;
    che, a prescindere da quanto affermato da questa Corte sui rapporti tra famiglia legittima e relazioni di fatto e sulla relativa certezza delle relazioni familiari da queste ultime conseguenti (ex plurimis, ordinanza n. 444 del 2006), il rimettente nella propria ordinanza, in punto di rilevanza, afferma che se la questione sollevata «venisse ritenuta fondata dalla Ecc.ma Corte il ricorso dovrebbe essere accolto (laddove, su un piano di fatto, il ricorrente fosse in grado di provare – come è verosimile che avvenga – sia la convivenza con la cittadina italiana che la condizione di padre del nascituro)»; &#13;
    che, pertanto, la sollevata questione è posta in modo meramente ipotetico ed eventuale, non essendo certa l'esistenza del rapporto di convivenza e della conseguente paternità dedotti dal ricorrente nel giudizio principale e, pertanto, ne va dichiarata la manifesta inammissibilità (ex plurimis, ordinanza n. 311 del 2007); &#13;
     Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
      dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 19, comma 2, lettere c) e d), del decreto legislativo 25 luglio 1998, n. 286 (Testo unico delle disposizioni concernenti la disciplina dell'immigrazione e norme sulla condizione dello straniero), sollevata, in riferimento agli artt. 2, 3, 29, 30 e 31 della Costituzione, dal giudice di pace di Novara, con l'ordinanza indicata in epigrafe. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 aprile 2008. &#13;
F.to: &#13;
Franco BILE, Presidente &#13;
Maria Rita SAULLE, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 24 aprile 2008. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
