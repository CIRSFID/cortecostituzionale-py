<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>572</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:572</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/2000</data_decisione>
    <data_deposito>21/12/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
 Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
 Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria &#13;
 FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi di legittimità costituzionale dell'art. 656, commi 5, 8    &#13;
 e  9,  del  codice  di  procedura penale, come modificato dall'art. 1    &#13;
 della  legge  27  maggio 1988, n. 165 (Modifiche all'articolo 656 del    &#13;
 codice  di  procedura  penale ed alla legge 26 luglio 1975, n. 354, e    &#13;
 successive modificazioni), promossi con ordinanze emesse il 30 luglio    &#13;
 1999 e il 27 ottobre 1999 dal tribunale di Genova nei procedimenti di    &#13;
 esecuzione  nei confronti di T. Q. C. H. e D. A., iscritte ai nn. 592    &#13;
 e  729  del  registro  ordinanze  1999  e  pubblicate  nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 43, 1ª serie speciale, dell'anno 1999 e    &#13;
 n. 3, 1ª serie speciale, dell'anno 2000.                                 &#13;
     Visti  gli  atti  di  intervento del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
     Udito  nella  camera di consiglio del 15 novembre 2000 il giudice    &#13;
 relatore Giovanni Maria Flick.                                           &#13;
     Ritenuto che il tribunale di Genova, con due ordinanze di analogo    &#13;
 tenore  emesse  il  30 luglio 1999 ed il 27 ottobre 1999 nel corso di    &#13;
 distinti procedimenti di esecuzione penale, ha sollevato questione di    &#13;
 legittimità costituzionale dell'art. 656, commi 5, 8 e 9, del codice    &#13;
 di  procedura  penale,  come  modificato  dall'art.  1 della legge 27    &#13;
 maggio  1998,  n. 165  (Modifiche  all'articolo  656  del  codice  di    &#13;
 procedura  penale  ed alla legge 26 luglio 1975, n. 354, e successive    &#13;
 modificazioni),  per  contrasto  con  gli artt. 3, primo comma, e 112    &#13;
 della Costituzione, nella parte in cui prevede (comma 5 dell'art. 656    &#13;
 cod.   proc.   pen.)   la   consegna  al  condannato  dell'ordine  di    &#13;
 carcerazione  e  del  decreto di sospensione di tale ordine, in luogo    &#13;
 della loro notificazione ai sensi degli artt. 148 ss. cod. proc. pen;    &#13;
 e, per contrasto con gli artt. 3 e 27 della Costituzione, nella parte    &#13;
 in cui non prevede (commi 8 e 9 dell'art. 656 cod. proc. pen.) che la    &#13;
 condizione   di   irreperibilità   o  di  latitanza  del  condannato    &#13;
 costituisca,  rispettivamente,  causa  di  revoca  della  sospensione    &#13;
 dell'ordine di esecuzione delle pene detentive e causa ostativa della    &#13;
 sospensione stessa;                                                      &#13;
         che  il  rimettente premette, in punto di fatto, che, dopo il    &#13;
 passaggio  in giudicato di una sentenza di condanna a pena detentiva,    &#13;
 il pubblico ministero - in ossequio al nuovo testo dell'art. 656 cod.    &#13;
 proc.  pen.,  introdotto  dalla  citata legge n. 165 del 1998 - aveva    &#13;
 emesso ordine di esecuzione della pena e decreto di sospensione della    &#13;
 medesima, disponendo la consegna di entrambi gli atti al condannato;     &#13;
         che  la  consegna  era  risultata  peraltro  impossibile, non    &#13;
 essendo  stato  il  condannato reperito neppure in esito ad ulteriori    &#13;
 ricerche;                                                                &#13;
         che   il   pubblico   ministero   aveva  quindi  promosso  un    &#13;
 procedimento  di esecuzione, chiedendo ad esso giudice rimettente, in    &#13;
 via  principale,  di  disporre,  sulla  base  di  una interpretazione    &#13;
 estensiva  del  comma  8  dell'art.  656  cod.  proc. pen., la revoca    &#13;
 dell'ordine   di   sospensione,  considerando  l'irreperibilità  del    &#13;
 condannato  come  equivalente alla mancata presentazione dell'istanza    &#13;
 indicata  nel  medesimo  comma 8; e, in via subordinata, di sollevare    &#13;
 questione di legittimità costituzionale della nuova normativa;          &#13;
         che  -  ad  avviso  del  giudice  a quo - mentre la richiesta    &#13;
 principale  del  pubblico  ministero non potrebbe essere accolta alla    &#13;
 luce  del  chiaro  dettato  dell'art.  656, comma 8, cod. proc. pen.,    &#13;
 meritevole    di    considerazione   risulterebbe,   invece,   quella    &#13;
 subordinata;                                                             &#13;
         che  l'art.  656,  comma  5,  cod.  proc.  pen., nuovo testo,    &#13;
 stabilisce,  in  effetti,  che  l'ordine  di  esecuzione  della  pena    &#13;
 detentiva ed il decreto di sospensione della stessa - che il pubblico    &#13;
 ministero è tenuto ad emettere simultaneamente nei casi ivi previsti    &#13;
 -   debbano   essere   consegnati  personalmente  al  condannato:  la    &#13;
 "consegna",  prescritta  dalla  norma,  non  potrebbe  essere difatti    &#13;
 intesa nel senso di "notificazione" - così da legittimare il ricorso    &#13;
 alle articolate procedure di cui agli artt. 148 ss. cod. proc. pen. -    &#13;
 ostando  a  tale  interpretazione  sia  il dato testuale che i lavori    &#13;
 preparatori della legge n. 165 del 1998;                                 &#13;
         che, così congegnata, la disposizione violerebbe tuttavia la    &#13;
 Carta   costituzionale   sotto  due  profili:  da  un  lato,  perché    &#13;
 determinerebbe  una  ingiustificata  disparità  di  trattamento  tra    &#13;
 imputato  e  condannato, essendo previsto per il primo (che pure può    &#13;
 giovarsi  della  presunzione  costituzionale  di non colpevolezza) un    &#13;
 regime  più flessibile e meno "garantito" di quello stabilito per il    &#13;
 secondo,  sia  in  rapporto alla disciplina delle notificazioni che a    &#13;
 quella  dell'eseguibilità  della  misura della custodia cautelare in    &#13;
 carcere;  dall'altro  lato,  perché  precluderebbe  l'attività  del    &#13;
 pubblico  ministero  volta a dare esecuzione alle sentenze, attività    &#13;
 che   costituisce   parte   integrante   dell'obbligo   di  esercizio    &#13;
 dell'azione penale sancito dall'art. 112 della Costituzione;             &#13;
         che una ulteriore compromissione del principio di uguaglianza    &#13;
 deriverebbe,  poi  -  secondo  il  giudice rimettente - dalla mancata    &#13;
 previsione,  nei  commi  8  e  9 dell'art. 656 cod. proc. pen., della    &#13;
 irreperibilità  e  della  latitanza del condannato, rispettivamente,    &#13;
 come causa di revoca del decreto di sospensione e come causa ostativa    &#13;
 della   sua  emissione:  in  tal  modo,  infatti,  la  condizione  di    &#13;
 irreperibilità   o  di  latitanza  -  impedendo  l'esecuzione  della    &#13;
 sentenza  -  godrebbe  di  un  trattamento  "privilegiato" rispetto a    &#13;
 quella  dei  condannati  che,  in  quanto radicati nel territorio per    &#13;
 ragioni  di  lavoro o familiari, risultino agevolmente rintracciabili    &#13;
 dalle  forze  di polizia, e quindi costretti ad attivare le procedure    &#13;
 contemplate  dall'art.  656  cod.  proc.  pen.  al fine di evitare la    &#13;
 carcerazione;                                                            &#13;
         che, da ultimo, la disciplina complessiva posta dall'art. 656    &#13;
 cod.  proc. pen. colliderebbe anche con l'art. 27, terzo comma, della    &#13;
 Costituzione,  consentendo  al  condannato  irreperibile di sottrarsi    &#13;
 all'esecuzione  della  pena  mentre  decorre  il  relativo termine di    &#13;
 prescrizione (nella specie, di breve durata), onde la pena stessa non    &#13;
 potrebbe  esplicare  né  la  sua funzione rieducativa, né quelle di    &#13;
 diversa natura;                                                          &#13;
         che  nei  giudizi  di  costituzionalità  è  intervenuto  il    &#13;
 Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso    &#13;
 dall'Avvocatura  generale  dello  Stato,  il quale ha concluso per la    &#13;
 declaratoria di non fondatezza delle questioni.                          &#13;
     Considerato  che,  a  fronte  della  sostanziale  identità delle    &#13;
 questioni, dev'essere disposta la riunione dei relativi giudizi;         &#13;
         che,  successivamente  alle ordinanze di rimessione, è stato    &#13;
 emanato  il  d.l.  24 novembre 2000, n. 341 (Disposizioni urgenti per    &#13;
 l'efficacia  e  l'efficienza  dell'Amministrazione  della giustizia),    &#13;
 pubblicato  nella  Gazzetta  Ufficiale della Repubblica n. 275 del 24    &#13;
 novembre  2000,  il  cui  art.  10 ha modificato l'art. 656, comma 5,    &#13;
 secondo  periodo, cod. proc. pen., sostituendo - negli stessi termini    &#13;
 proposti  dal  petitum del tribunale rimettente - le parole (riferite    &#13;
 all'ordine  di  esecuzione  della  pena  detentiva  ed  al decreto di    &#13;
 sospensione  della  stessa)  "sono  consegnati",  con  le altre "sono    &#13;
 notificati";                                                             &#13;
         che,  pertanto,  va  disposta  la  restituzione degli atti al    &#13;
 giudice  rimettente  perché  valuti  se, a seguito della intervenuta    &#13;
 modifica  legislativa  della disposizione denunciata, la questione di    &#13;
 legittimità  costituzionale  sollevata  sia  tuttora  rilevante  nei    &#13;
 procedimenti a quibus.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti i giudizi,                                                   &#13;
     Ordina la restituzione degli atti al tribunale di Genova.            &#13;
     Così  deciso,  in  Roma,  nella sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 dicembre 2000.                             &#13;
                      Il Presidente: Santosuosso                          &#13;
                          Il redattore: Flick                             &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 21 dicembre 2000.                       &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
