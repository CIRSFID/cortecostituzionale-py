<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1968</anno_pronuncia>
    <numero_pronuncia>17</numero_pronuncia>
    <ecli>ECLI:IT:COST:1968:17</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/03/1968</data_decisione>
    <data_deposito>28/03/1968</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. GIUSEPPE BRANCA - &#13;
 Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE &#13;
 CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO &#13;
 DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della  legge  approvata  &#13;
 dall'Assemblea  regionale  siciliana  nella  seduta  del  21 marzo 1967  &#13;
 recante "Istituzione del ruolo del personale salariato di IV  categoria  &#13;
 addetto alla pulizia", promosso con ricorso del Commissario dello Stato  &#13;
 per  la Regione siciliana notificato il 29 marzo 1967, depositato nella  &#13;
 cancelleria della  Corte  costituzionale  il  4  aprile  successivo  ed  &#13;
 iscritto al n. 10 del Registro ricorsi 1967.                             &#13;
     Visto   l'atto   di   costituzione  del  Presidente  della  Regione  &#13;
 siciliana;                                                               &#13;
     udita nell'udienza pubblica del 14 febbraio 1968 la  relazione  del  &#13;
 Giudice  Giovanni  Battista  Benedetti;  uditi  il  sostituto  avvocato  &#13;
 generale dello Stato Giuseppe Guglielmi, per il  ricorrente,  e  l'avv.  &#13;
 Salvatore Villari, per la Regione siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nella  seduta  del 27 marzo 1967 l'Assemblea regionale siciliana ha  &#13;
 approvato la legge concernente l'"Istituzione del ruolo  del  personale  &#13;
 salariato  di  IV  categoria  addetto alla pulizia" ruolo in cui dovrà  &#13;
 essere inquadrato il personale che attualmente attende al  servizio  di  &#13;
 pulizia  degli  uffici  della  Regione.  Sul trattamento di assistenza,  &#13;
 previdenza e quiescenza di  tale  personale  la  legge  stabilisce  che  &#13;
 verrà  regolato  dalle disposizioni contenute nella legge regionale 23  &#13;
 febbraio 1962, n. 2, e dispone che il periodo di  servizio  antecedente  &#13;
 alla   data   di   inquadramento  è  riscattabile,  su  domanda  degli  &#13;
 interessati, sin dalla data di assunzione in  servizio.  In  dipendenza  &#13;
 del  maggiori  oneri, non coperti dai contributi di riscatto, derivanti  &#13;
 al  fondo  di  quiescenza,  la  legge  prevede  la  concessione  di  un  &#13;
 contributo  annuo  nel  limite  di  lire  20  milioni  per  dieci anni,  &#13;
 provvedendo  alla  spesa  afferente  l'esercizio  in   corso   mediante  &#13;
 riduzione  di  pari  somma  del capitolo 643 del bilancio regionale del  &#13;
 1967.                                                                    &#13;
     Con  ricorso notificato il 29 marzo 1967 il Commissario dello Stato  &#13;
 per  la  Regione  siciliana  ha  impugnato  detta  legge  denunciandone  &#13;
 l'illegittimità  costituzionale  in  riferimento  all'art.  81, ultimo  &#13;
 comma, della Costituzione, perché l'art. 4 assicura  il  finanziamento  &#13;
 dell'onere   ricadente   nell'esercizio   finanziario   corrente  senza  &#13;
 contenere alcuna indicazione dei mezzi di copertura  per  gli  esercizi  &#13;
 futuri.   Secondo  il  Commissario  dello  Stato  il  denunziato  vizio  &#13;
 d'illegittimità assume maggiore rilevanza ove si  consideri  che  alla  &#13;
 copertura  della  spesa  relativa  all'esercizio  in corso si fa fronte  &#13;
 mediante prelievo di somme iscritte in  bilancio  per  spese  in  conto  &#13;
 capitale e aventi altra destinazione.                                    &#13;
     Il  Presidente  della  Regione,  rappresentato  e  difeso dall'avv.  &#13;
 Salvatore Villari, si è costituito nel presente giudizio con  deposito  &#13;
 di  deduzioni  in  cancelleria  in  data 8 aprile 1967. La difesa della  &#13;
 Regione, dopo aver rilevato che la copertura per  l'esercizio  corrente  &#13;
 è da ritenersi costituzionalmente legittima dato che nel caso in esame  &#13;
 i fondi stanziati nel cap. 643 non sono stati ancora assegnati, venendo  &#13;
 al  motivo  di incostituzionalità formulato in ordine alla mancanza di  &#13;
 copertura per gli esercizi successivi, osserva che, in applicazione del  &#13;
 criteri enunciati dalla Corte nella sentenza n. 1 del  1966,  all'onere  &#13;
 delle  spese  future  può  farsi fronte con la riduzione di spese già  &#13;
 autorizzate.                                                             &#13;
     Nella specie, pertanto, deve essere ammessa la  previsione  che  la  &#13;
 spesa  del cap. 643 venga contenuta oppure che lo stesso capitolo possa  &#13;
 aver maggiore capienza aumentandone gli stanziamenti con  riduzioni  di  &#13;
 altri  capitoli.  L'importante  è  che l'aumento e la riduzione - come  &#13;
 rilevato  dalla  Corte  -  siano  fatti  con  "ragionevolezza   in   un  &#13;
 equilibrato  rapporto  con  la  spesa  che  s'intende  effettuare negli  &#13;
 esercizi futuri".                                                        &#13;
     Conclude,  quindi,  la  difesa  chiedendo  che  la   Corte   voglia  &#13;
 dichiarare l'infondatezza del ricorso.                                   &#13;
     In  una  memoria  depositata  il 16 gennaio 1968 l'Avvocatura dello  &#13;
 Stato, in rappresentanza e difesa del Commissario dello  Stato  per  la  &#13;
 Regione  siciliana,  afferma che la legge impugnata è incostituzionale  &#13;
 perché non contiene alcuna indicazione, neppure  generica,  del  mezzi  &#13;
 per  far  fronte  alla nuova spesa per gli esercizi futuri e che a tale  &#13;
 omissione non può sopperire la precisazione fatta  in  giudizio  dalla  &#13;
 difesa  regionale.  Questo vizio - secondo l'Avvocatura - è assorbente  &#13;
 ed esonera da ogni indagine sulla idoneità del mezzo indicato per  far  &#13;
 fronte   alle  spese  dell'esercizio  in  corso,  rilievo  quest'ultimo  &#13;
 formulato  non  come  motivo  di  ricorso   ma   piuttosto   come   una  &#13;
 considerazione    di    merito   che   colora   il   vero   motivo   di  &#13;
 incostiuzionalità.                                                      &#13;
     La difesa della Regione, in una memoria depositata  il  31  gennaio  &#13;
 1968, afferma che è infondato il rilievo dell'Avvocatura sul carattere  &#13;
 assorbente  del  vizio relativo alla mancata copertura per gli esercizi  &#13;
 futuri.                                                                  &#13;
     L'art. 4 della legge impugnata stabilisce infatti un limite massimo  &#13;
 e non una misura fissa di  spesa,  consentendo  così  alle  successive  &#13;
 leggi  di  bilancio  di  valutare,  tra  le  componenti necessarie alla  &#13;
 determinazione della somma da iscrivere ad un dato capitolo, quella che  &#13;
 attiene alla spesa in questione, e nello stesso tempo non vincolando in  &#13;
 modo tassativo a mantenere  lo  stanziamento  in  misura  costante.  Il  &#13;
 problema   della   copertura,   che  va  affrontato  al  momento  della  &#13;
 deliberazione della spesa,  non  deve  essere  necessariamente  risolto  &#13;
 indicando  nuove  entrate, ma può essere risolto modificando con legge  &#13;
 di spesa gli stanziamenti di bilancio  non  rigidamente  vincolati  nel  &#13;
 quantum o, naturalmente, modificando le stesse leggi di spesa su cui si  &#13;
 basa il bilancio.                                                        &#13;
     La difesa della Regione soggiunge che l'obbligo della copertura sia  &#13;
 per  le  spese relative all'esercizio in corso sia per quelle afferenti  &#13;
 agli esercizi futuri riguarda soltanto quelle che possono  considerarsi  &#13;
 nuove  o  maggiori  spese  in  senso tecnico. La legge impugnata è una  &#13;
 legge di spesa perché determina  una  componente  di  un  capitolo  di  &#13;
 spesa,  ma  non comporta né una nuova né una maggiore spesa nel senso  &#13;
 tecnico in cui tale espressione va intesa.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Nel  ricorso  indicato  in  epigrafe  la   legge   approvata  &#13;
 dall'Assemblea  regionale  siciliana  nella  seduta  del 21 marzo 1967,  &#13;
 concernente l'"Istituzione del ruolo  del  personale  salariato  di  IV  &#13;
 categoria   addetto   alla   pulizia",   è   stata   denunciata   come  &#13;
 costituzionalmente illegittima,  in  riferimento  all'art.  81,  ultimo  &#13;
 comma,  della  Costituzione,  per  un  unico  motivo:  la  mancanza  di  &#13;
 indicazione del mezzi di  copertura,  per  gli  esercizi  successivi  a  &#13;
 quello  in corso, della maggior spesa derivante dal fondo di quiescenza  &#13;
 per il riscatto del periodo  di  servizio  prestato  dal  personale  in  &#13;
 questione anteriormente alla data della sistemazione in ruolo.           &#13;
     L'indagine  della  Corte  va  pertanto  limitata  a tale censura di  &#13;
 incostituzionalità e non può  estendersi  all'osservanza  del  citato  &#13;
 precetto   costituzionale  in  relazione  alla  copertura  della  spesa  &#13;
 afferente l'anno in corso.                                               &#13;
     È vero che in merito alla scelta fatta dal  legislatore  regionale  &#13;
 per sopperire alla maggiore spesa dell'esercizio 1967 è stata posta in  &#13;
 dubbio  l'ammissibilità  della riduzione del capitolo 643 del bilancio  &#13;
 riguardante spese per lavori  di  manutenzione  e  miglioramento  degli  &#13;
 edifici  della  Regione,  ma - come risulta espressamente dal ricorso e  &#13;
 dalla successiva memoria dell'Avvocatura  dello  Stato  -  trattasi  di  &#13;
 rilievo svolto non come ulteriore motivo di incostituzionalità, bensì  &#13;
 per dare maggiore rilevanza al motivo già dedotto.                      &#13;
     2.  -  La Corte ritiene che il difetto denunciato sia sufficiente a  &#13;
 concretare  la   violazione   dell'art.   81,   ultimo   comma,   della  &#13;
 Costituzione.  Non  è dubbio che il riscatto, autorizzato con l'art. 2  &#13;
 della legge impugnata, del periodo di servizio prestato  dal  personale  &#13;
 addetto  alla  pulizia anteriormente alla data di sistemazione in ruolo  &#13;
 comporti un maggior onere per il fondo  di  quiescenza,  previdenza  ed  &#13;
 assistenza  per i dipendenti della Regione. L'art. 3 parla di "maggiori  &#13;
 oneri, non coperti dai contributi di riscatto, derivanti al  fondo  per  &#13;
 effetto  della  presente  legge",  precisandone l'ammontare complessivo  &#13;
 nonché la ripartizione annuale (lire 20 milioni per  dieci  anni).  Il  &#13;
 successivo  art.  4  si  limita  a  stabilire  che "all'onere ricadente  &#13;
 nell'esercizio in corso si fa fronte mediante riduzione di  pari  somma  &#13;
 del  capitolo  643  del bilancio della Regione per l'esercizio 1967" ma  &#13;
 nulla assolutamente dispone  in  ordine  al  modo  di  fronteggiare  il  &#13;
 riconosciuto   aumento   di   spesa  per  i  successivi  nove  esercizi  &#13;
 finanziari.                                                              &#13;
     A tale omissione  non  possono  certo  supplire  le  considerazioni  &#13;
 svolte  dalla difesa della Regione circa i possibili mezzi di copertura  &#13;
 della maggiore spesa per gli esercizi successivi a quello in corso, fra  &#13;
 i quali sono alternativamente indicati il contenimento della spesa  per  &#13;
 la  manutenzione  degli edifici regionali nella misura risultante dalla  &#13;
 riduzione eseguita per l'esercizio 1967 oppure la  riduzione  di  altre  &#13;
 non precisate spese.                                                     &#13;
     Alla  copertura degli oneri per gli esercizi successivi non si può  &#13;
 provvedere con le leggi approvative dei bilanci di detti esercizi.  È,  &#13;
 infatti,  giurisprudenza costante della Corte che debba essere la legge  &#13;
 sostanziale, dalla quale deriva la nuova o maggiore spesa, a indicare i  &#13;
 mezzi per farvi fronte non solo per l'esercizio in corso ma  anche  per  &#13;
 quelli successivi fra i quali la spesa sia stata ripartita.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale  della  legge  approvata  &#13;
 dall'Assemblea regionale siciliana  nella  seduta  del  21  marzo  1967  &#13;
 recante  "Istituzione del ruolo del personale salariato di IV categoria  &#13;
 addetto alla pulizia".                                                   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 21 marzo 1968.                                &#13;
                                   ALDO  SANDULLI  - BIAGIO PETROCELLI -  &#13;
                                   ANTONIO MANCA  -  GIUSEPPE  BRANCA  -  &#13;
                                   MICHELE  FRAGALI - COSTANTINO MORTATI  &#13;
                                   -  GIUSEPPE  CHIARELLI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
