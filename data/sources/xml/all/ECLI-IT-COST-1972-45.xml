<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>45</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:45</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CHIARELLI</presidente>
    <relatore_pronuncia>Luigi Oggioni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/03/1972</data_decisione>
    <data_deposito>15/03/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE CHIARELLI, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Dott. GIOVANNI BATTISTA &#13;
 BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - &#13;
 Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - &#13;
 Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. &#13;
 NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.    373  del  &#13;
 codice di procedura civile, promosso con ordinanza emessa il 30 gennaio  &#13;
 1971 dal pretore di Rodi Garganico nel procedimento civile vertente tra  &#13;
 Apicella  Michele  e Francesco e Miglionico Rocco Giuseppe, iscritta al  &#13;
 n.  137  del  registro  ordinanze  I971  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 119 del 12 maggio 1971.                    &#13;
     Visto l'atto di costituzione di Miglionico Rocco Giuseppe;           &#13;
     udito nell'udienza pubblica del 26 gennaio 1972 il Giudice relatore  &#13;
 Luigi Oggioni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ordinanza  emessa  il  30  gennaio  1971  il  pretore  di Rodi  &#13;
 Garganico, quale giudice dell'esecuzione, ha  rilevato  che  Miglionico  &#13;
 Rocco  aveva  iniziato procedimento esecutivo contro Apicella Michele e  &#13;
 Francesco, opponenti, in forza della sentenza della Corte di appello di  &#13;
 Bari del 17  marzo  1970  con  cui  i  predetti  Apicella  erano  stati  &#13;
 condannati al pagamento di lire 8.664.000, oltre le spese, in favore di  &#13;
 esso  procedente, e che la richiesta di sospensione della esecuzione di  &#13;
 detta  sentenza,  avanzata  alla  stessa  Corte  di  appello  ai  sensi  &#13;
 dell'art. 373 del codice di procedura civile in pendenza di ricorso per  &#13;
 cassazione,  era  stata  respinta  con ordinanza del 29 settembre 1970.  &#13;
 Ciò premesso il giudice  a  quo  ha  osservato  che  la  detta  norma,  &#13;
 attribuendo  al  giudice  di  appello  la  competenza  a  decidere, con  &#13;
 ordinanza espressamente dichiarata non impugnabile, sulla richiesta  di  &#13;
 sospensione  della propria sentenza gravata di ricorso in Cassazione, e  &#13;
 senza quindi che a  questa  ultima  spetti  alcun  potere  in  materia,  &#13;
 potrebbe  "rendere  concretamente  inutile  e  vano  il  riconoscimento  &#13;
 meramente  astratto  del  buon  diritto  del  debitore  sottoposto   ad  &#13;
 esecuzione", diritto peraltro certamente compromesso, sempre a dire del  &#13;
 pretore,  dall'intervento  dello  stesso  giudice che ha pronunciato la  &#13;
 sentenza di merito e  che  quindi  "non  assicurerebbe  l'obbiettività  &#13;
 inerente all'attività del giudice".                                     &#13;
     Il  giudice  a  quo,  ravvisata  nei  denunciati  inconvenienti  la  &#13;
 violazione degli artt. 24 e  111  della  Costituzione,  e  ritenuta  la  &#13;
 rilevanza  della  questione  così  prospettata  in  quanto l'eventuale  &#13;
 dichiarazione di illegittimità della norma impugnata "comprometterebbe  &#13;
 il diritto del Miglionico a promuovere  l'esecuzione",  ha  sospeso  il  &#13;
 giudizio e trasmesso gli atti a questa Corte, per l'ulteriore corso.     &#13;
     L'ordinanza,   ritualmente   notificata   e  comunicata,  è  stata  &#13;
 pubblicata sulla Gazzetta Ufficiale n. 119 del 12 maggio 1971.           &#13;
     Nel presente giudizio si è costituito il Miglionico, rappresentato  &#13;
 e difeso dall'avv. Carmine Pucillo, che ha  depositato  tempestivamente  &#13;
 le proprie deduzioni.                                                    &#13;
     La  difesa  eccepisce  anzitutto l'irrilevanza della questione e in  &#13;
 proposito  osserva  che  il  giudizio  di  legittimità  costituzionale  &#13;
 investirebbe  una norma che attiene alla sospensione della esecutività  &#13;
 della sentenza e non del procedimento di  esecuzione,  e  che  dovrebbe  &#13;
 essere  comunque  applicata  non  dal  giudice  dell'esecuzione, ma dal  &#13;
 giudice che  ha  pronunciato  la  sentenza  impugnata  per  cassazione.  &#13;
 Difetterebbe  pertanto  il  nesso di pregiudizialità necessaria fra la  &#13;
 questione prospettata e la definizione del giudizio ordinario.           &#13;
     Nel  merito,  confutando  le  tesi  svolte  dalla  controparte  nel  &#13;
 giudizio  a quo per sostenere l'illegittimità della norma impugnata in  &#13;
 relazione all'art. 25 della Costituzione, profilo questo  che  peraltro  &#13;
 non   risulta   accolto  nell'ordinanza  di  rinvio,  si  diffonde  nel  &#13;
 sostenerne  l'infondatezza.  Osserva  comunque  che  nessuna   analogia  &#13;
 potrebbe  ravvisarsi  fra  il  procedimento  in esame e il procedimento  &#13;
 inibitorio previsto dall'art. 351 c.p.c., che riguarda  la  sospensione  &#13;
 della clausola di provvisoria esecuzione della sentenza di primo grado,  &#13;
 trattandosi   di   ipotesi   ovviamente   del   tutto   diverse,  onde,  &#13;
 contrariamente a quanto asserito dalla controparte nel giudizio a  quo,  &#13;
 nessun  argomento  potrebbe  trarsi dal previsto controllo del collegio  &#13;
 sul provvedimento circa la sospensione adottata dal giudice istruttore,  &#13;
 ai sensi dell'art. 351 c.p.c., per inferirne  la  illegittimità  della  &#13;
 norma impugnata.                                                         &#13;
     La  circostanza,  poi,  che sia lo stesso giudice collegiale che ha  &#13;
 emesso la sentenza ad essere  chiamato  a  decidere  sulla  istanza  di  &#13;
 sospensione  non  integrerebbe  alcuna  lesione  del diritto di difesa,  &#13;
 anche perché,  trattandosi  di  provvedimento  da  adottarsi  dopo  la  &#13;
 conclusione  della  fase  istruttoria,  ed  anzi  successivamente  alla  &#13;
 emissione della sentenza di secondo grado sarebbe logica e conforme  al  &#13;
 nostro sistema processuale l'attribuzione del detto potere direttamente  &#13;
 al collegio con esclusione dell'intervento del magistrato istruttore.    &#13;
     Riferendosi   infine   alla   natura  meramente  ordinatoria  della  &#13;
 sospensione di cui all'art. 373 c.p.c., afferma  che  tratterebbesi  di  &#13;
 provvedimento come tale non impugnabile per cassazione, per cui sarebbe  &#13;
 anche   infondata   la  censura  di  incostituzionalità  sollevata  in  &#13;
 relazione all'art. 111 della Costituzione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con la suindicata ordinanza di  rinvio,  il  pretore  di  Rodi  &#13;
 Garganico,  in  sede  di  processo di esecuzione ed in veste di giudice  &#13;
 preposto all'emissione  dei  correlativi  provvedimenti,  ha  posto  la  &#13;
 questione di legittimità dell'art. 373 del codice di procedura civile.  &#13;
 Ciò nel senso che, essendo l'ordinanza di sospensione dell'esecuzione,  &#13;
 (sospensione  già  richiesta  davanti  al giudice di merito di secondo  &#13;
 grado e dallo stesso respinta)  dichiarata  dal  citato  articolo  "non  &#13;
 impugnabile"  davanti  alla  Corte  di cassazione, alla quale, come nel  &#13;
 caso,   sia  stato  proposto  ricorso  principale,  ne  deriverebbe  la  &#13;
 violazione, sul punto, dell'esercizio del diritto di  difesa,  tutelato  &#13;
 dall'art.  24,  secondo  comma, Costituzione "in ogni stato e grado del  &#13;
 giudizio",  nonché  la  violazione  dell'art.  111   capoverso   della  &#13;
 Costituzione.                                                            &#13;
     2.  - La questione va anzitutto esaminata agli effetti e nei limiti  &#13;
 del controllo di rilevanza, riservato a questa Corte.                    &#13;
     Secondo  l'ordinanza,  la  rilevanza  consisterebbe  in  ciò   che  &#13;
 l'eventuale dichiarazione di illegittimità dell'art. 373 c.p.c., nella  &#13;
 parte  riguardante  la non impugnabilità del provvedimento del giudice  &#13;
 di merito, costituirebbe impedimento al promovimento attuale della  sua  &#13;
 esecuzione.                                                              &#13;
     Ma,   così   motivando,   l'ordinanza  omette  di  considerare  la  &#13;
 particolare natura e l'oggetto del giudizio a quo.                       &#13;
     L'art. 23, comma secondo, della legge 11  marzo  1953,  n  87,  sul  &#13;
 funzionamento  della Corte costituzionale, condiziona il rinvio ad essa  &#13;
 Corte  da  parte  dell'autorità  giurisdizionale  di  provenienza,  al  &#13;
 requisito  che "il giudizio non possa essere definito indipendentemente  &#13;
 dalla risoluzione della questione di legittimità costituzionale".       &#13;
     La giurisprudenza di questa Corte  ha  costantemente  ritenuto  che  &#13;
 deve  trattarsi  di questione da risolversi come presupposto necessario  &#13;
 del giudizio a quo e con  incidenza  sulle  norme  cui  il  giudice  è  &#13;
 direttamente chiamato a dare applicazione nell'esercizio del suo potere  &#13;
 decisorio  (sentenze n. 109 del 1964; nn. 60 e 132 del 1970; nn. 46, 78  &#13;
 e 150 del 1971; n. 7 del 1972).                                          &#13;
     Nel processo di esecuzione, di cui alla  ordinanza  di  rinvio,  il  &#13;
 contenuto  e,  nello  stesso  tempo,  i limiti delle istanze rivolte al  &#13;
 giudice dagli opponenti al pignoramento, sono  consistiti  nell'assunto  &#13;
 che si trattava di "esecuzione inficiata di nullità alla base, perché  &#13;
 promossa senza il relativo precetto, essendo perento quello notificato"  &#13;
 e  nel  chiedere,  pertanto,  preliminarmente,  allo  stesso giudice la  &#13;
 speciale sospensione dell'esecuzione, ai sensi dell'art. 624 c.p.c.      &#13;
     Ora, è di palese evidenza che il giudice a quo invece di limitarsi  &#13;
 a conoscere delle  norme  applicabili  nell'ambito  del  caso  concreto  &#13;
 sottoposto alla sua decisione, è risalito, onde sollevare la questione  &#13;
 di costituzionalità, ad antecedenti relativi ad una fase pregressa del  &#13;
 processo di cognizione. Il che era e rimane irrilevante, perché, nella  &#13;
 specie,  il  giudice  non  doveva, né direttamente né indirettamente,  &#13;
 applicare la norma impugnata, la quale concerne un procedimento (quello  &#13;
 cioè relativo alla sospensione  dell'esecuzione  di  una  sentenza  di  &#13;
 secondo  grado)  totalmente  estraneo  alla  sua competenza ed altresì  &#13;
 all'oggetto   del   giudizio   innanzi   a   lui   proposto,   limitato  &#13;
 all'accertamento dell'inefficacia del precetto.                          &#13;
     Di  conseguenza,  in  conformità  della  citata  giurisprudenza di  &#13;
 questa  Corte,  intervenuta  in  situazioni  analoghe,  la   questione,  &#13;
 proposta,  sia  in  riferimento all'art. 24 sia in riferimento all'art.  &#13;
 111 Costituzione, va dichiarata inammissibile per difetto di rilevanza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile,  perché  irrilevante,  la  questione   di  &#13;
 legittimità  costituzionale  dell'art.  373  del  codice  di procedura  &#13;
 civile sollevata con  l'ordinanza  in  epigrafe  del  pretore  di  Rodi  &#13;
 Garganico, in riferimento agli artt. 24 e 111 Costituzione.              &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 9 marzo 1972.                                 &#13;
                                   GIUSEPPE CHIARELLI - MICHELE  FRAGALI  &#13;
                                   -   COSTANTINO   MORTATI  -  GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - FRANCESCO  PAOLO  &#13;
                                   BONIFACIO - LUIGI OGGIONI - ANGELO DE  &#13;
                                   MARCO   -  ERCOLE  ROCCHETTI  -  ENZO  &#13;
                                   CAPALOZZA    -    VINCENZO    MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
