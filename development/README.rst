Corte costituzionale
====================

* clone this repo
* ``cd /to/this/repo/development``
* ``python3 -m venv venv``
* ``source venv/bin/activate``
* ``pip install -r requirements.txt``
* ``pip install -e .``

Usage
.....

Be sure to have a ``.env`` file in ``this-repo/development/cortecostituzionale``
with the following info:

::

    EXIST_DB_HOST=<your host>
    EXIST_DB_PORT=<your port>
    EXIST_DB_USER=<your user>
    EXIST_DB_PASSWORD=<your pwd>
    EXIST_DB_COLLECTION=<your collection>

* to launch the script:

::

    usage: main.py [-h] [--override] [--push]

    optional arguments:
      -h, --help  show this help message and exit
      --override  Overrides existing docs
      --push      Push to eXistDB

It will download missing documents,
parse everething that is not parsed and if ``--push``,
push everything to eXistDB

