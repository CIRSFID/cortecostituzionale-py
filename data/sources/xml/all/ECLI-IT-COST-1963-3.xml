<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>3</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:3</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/02/1963</data_decisione>
    <data_deposito>12/02/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA &#13;
 JAEGER - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO &#13;
 SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE &#13;
 VERZÌ, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  16,  primo  &#13;
 comma,  secondo  periodo, del D.P.R.   26 aprile 1957, n. 818, promosso  &#13;
 con ordinanza emessa il 14 luglio 1961  dal  Tribunale  di  Padova  nel  &#13;
 procedimento  civile  vertente tra Scalise Carlo e l'Istituto nazionale  &#13;
 della previdenza sociale, iscritta al n.  159  del  Registro  ordinanze  &#13;
 1961 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  245 del  &#13;
 30 settembre 1961.                                                       &#13;
     Visto l'atto di costituzione in giudizio di Scalise Carlo;           &#13;
     udita  nell'udienza  pubblica del 14 novembre 1962 la relazione del  &#13;
 Giudice Giuseppe Verzì;                                                 &#13;
     udito l'avv. Franco Agostini, per Scalise Carlo.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel procedimento civile vertente fra  Scalise  Carlo  e  l'Istituto  &#13;
 nazionale   della   previdenza  sociale,  il  Tribunale  di  Padova  ha  &#13;
 pronunziato ordinanza,  in  data  14  luglio  1961,  con  la  quale  ha  &#13;
 sollevato  di  ufficio  la  questione  di  legittimità  costituzionale  &#13;
 dell'art.  16, primo comma, secondo periodo, del D.P.R. 26 aprile 1957,  &#13;
 n. 818, in relazione agli artt. 5, terzo comma,  e  37  della  legge  4  &#13;
 aprile 1952, n. 218, ed in riferimento all'art. 76 della Costituzione.   &#13;
     Nell'ordinanza si premette che questa Corte, con sentenza n. 35 del  &#13;
 30  giugno  1960,  ha dichiarato la illegittimità costituzionale della  &#13;
 norma delegata contenuta nella prima parte del primo comma dell'art. 16  &#13;
 suindicato, siccome eccedente i limiti della delega di cui all'art.  37  &#13;
 della  legge  4  aprile 1952, n. 218.  Secondo tale norma "i contributi  &#13;
 volontari per l'assicurazione per  l'invalidità,  la  vecchiaia  ed  i  &#13;
 superstiti  non  possono  essere  versati per i periodi durante i quali  &#13;
 l'assicurato  sia  iscritto   a   forme   di   previdenza   sostitutive  &#13;
 dell'assicurazione  o  per  i  periodi  che comportino diritto ad altro  &#13;
 trattamento  obbligatorio  di  pensione".  Si  assume  che   ugualmente  &#13;
 viziata,  per  le  stesse  ragioni,  sia  anche  la norma contenuta nel  &#13;
 secondo periodo del primo comma dello stesso art.   16,  per  il  quale  &#13;
 "parimenti,  i  contributi  non  possono  essere  versati per i periodi  &#13;
 successivi  alla  data  di   decorrenza   della   pensione   a   carico  &#13;
 dell'assicurazione  obbligatoria,  o  delle  forme  di previdenza o dei  &#13;
 trattamenti sopracitati". Si adduce che  la  norma  in  parola  non  ha  &#13;
 carattere  transitorio, né può essere considerata norma di attuazione  &#13;
 o di coordinamento; al contrario, pone una restrizione al  diritto  del  &#13;
 lavoratore     alla    prosecuzione    volontaria    dell'assicurazione  &#13;
 obbligatoria, ignota alla legge delegante 4 aprile  1952,  n.  218,  la  &#13;
 quale   ha   disciplinato   altresì   compiutamente   (art.  5)  detta  &#13;
 prosecuzione, indicando i casi nei quali essa è preclusa.               &#13;
     L'ordinanza,  ritualmente  notificata  e   comunicata,   è   stata  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 245 del 30  &#13;
 settembre 1961.                                                          &#13;
     Nel presente giudizio si è costituito soltanto il pensionato Carlo  &#13;
 Scalise, rappresentato e difeso per atto notaio Grassi di Padova  dagli  &#13;
 avvocati   Vezio   Crisafulli  e  Franco  Agostini.    Nelle  deduzioni  &#13;
 depositate in cancelleria  l'11  ottobre  1961,  anche  lo  Scalise  si  &#13;
 richiama  alla  statuizione  della sentenza n. 35 del 30 giugno 1960 di  &#13;
 questa Corte; ed aggiunge, poi, le medesime argomentazioni addotte  dal  &#13;
 Tribunale  per  dimostrare la illegittimità costituzionale della norma  &#13;
 in questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     La norma dell'art. 16, primo comma, secondo periodo, del D.P.R.  26  &#13;
 aprile  1957,  n. 818 - la quale stabilisce che "i contributi volontari  &#13;
 per l'assicurazione invalidità, vecchiaia  e  superstiti  non  possono  &#13;
 essere  versati  per  periodi  successivi alla data di decorrenza della  &#13;
 pensione a carico dell'assicurazione  obbligatoria  o  delle  forme  di  &#13;
 previdenza   suindicate"   (forme   di   previdenza  sostitutive  della  &#13;
 assicurazione od altro trattamento obbligatorio di pensione) - modifica  &#13;
 sostanzialmente la regolamentazione della prosecuzione volontaria nelle  &#13;
 assicurazioni obbligatorie  della  previdenza  sociale,  e  pone  delle  &#13;
 restrizioni al diritto del lavoratore, non contenute nella legge n. 218  &#13;
 del  4  aprile  1952,  né  in altre leggi vigenti in materia. Inoltre,  &#13;
 siffatte   restrizioni   contrastano   con   i    principi    affermati  &#13;
 implicitamente  nella  legge  del 1952, siccome risulta anche dal fatto  &#13;
 che l'I.N.P.S.  - in applicazione dell'impugnato art. 16 - ha  revocato  &#13;
 le   autorizzazioni  alla  prosecuzione  volontaria  dell'assicurazione  &#13;
 obbligatoria concesse quando non era ancora entrata in vigore la  legge  &#13;
 delegata;  e lo stesso legislatore delegato ha dovuto dettare una norma  &#13;
 transitoria, per cui  "si  considerano  comunque  validi  a  tutti  gli  &#13;
 effetti  i  contributi  volontari  versati sino alla data di entrata in  &#13;
 vigore del presente decreto" (art. 16, ultimo comma).                    &#13;
     Per altro, le ragioni che  sono  state  poste  a  fondamento  della  &#13;
 decisione  di  questa  Corte  n.  35  del  30 giugno 1960 - la quale ha  &#13;
 dichiarato l'illegittimità  costituzionale,  per  eccesso  di  delega,  &#13;
 della  norma  contenuta  nel  primo  periodo  dello  stesso primo comma  &#13;
 dell'art. 16 - valgono anche per la norma in esame.                      &#13;
     L'art. 5 della legge  4  aprile  1952,  n.    218  -  la  quale  ha  &#13;
 riordinato  la  materia  dell'assicurazione  obbligatoria  invalidità,  &#13;
 vecchiaia e superstiti, modificando ed integrando le leggi del  1935  e  &#13;
 del  1939  -  ha  disciplinato compiutamente la prosecuzione volontaria  &#13;
 dell'assicurazione obbligatoria, concedendo tale diritto a "tutti"  gli  &#13;
 assicurati   della  previdenza  sociale,  in  caso  di  interruzione  o  &#13;
 cessazione del rapporto di lavoro, oppure qualora venga meno  l'obbligo  &#13;
 assicurativo  per  il  compimento  dell'età pensionabile, subordinando  &#13;
 tale concessione  soltanto  a  due  condizioni:    1)  l'autorizzazione  &#13;
 dell'Istituto   della   previdenza   sociale;   2)  la  sussistenza  di  &#13;
 determinati   requisiti   di   contribuzione   obbligatoria.    Nessuna  &#13;
 limitazione vien posta dalla legge in relazione a condizioni soggettive  &#13;
 dell'assicurato  ed  in ispecie ad altri rapporti assicurativi ai quali  &#13;
 lo stesso partecipi.                                                     &#13;
     Né è a dire che il divieto per  i  pensionati  di  usufruire  del  &#13;
 beneficio della prosecuzione volontaria dell'assicurazione obbligatoria  &#13;
 sia  disposto da altre vigenti leggi con le quali quella del 1952 debba  &#13;
 essere  coordinata.  Perché,  al  contrario,  la  legislazione   sulle  &#13;
 assicurazioni  della  previdenza  sociale  si è sempre più ispirata a  &#13;
 principi di favore per il lavoratore fino ad ammettere il cumulo  della  &#13;
 pensione  della previdenza sociale con altre pensioni. Come è stato di  &#13;
 già affermato nella sentenza n.  35 del 1960 di questa  Corte,  l'art.  &#13;
 10  della  legge  del  1952 prevede il cumulo di più pensioni a carico  &#13;
 dell'assicurazione  obbligatoria  per  l'invalidità  la  vecchiaia   e  &#13;
 superstiti  e  dei  trattamenti  sostitutivi di tale assicurazione, sia  &#13;
 pure al fine di regolare i minimi di pensione, mentre dagli  artt.  38,  &#13;
 n.  2, e 42 della legge 4 ottobre 1935, n. 1827, si desume soltanto che  &#13;
 non possono coesistere due assicurazioni  obbligatorie  per  lo  stesso  &#13;
 rapporto  di  lavoro,  e  non  già  che  sia  proibita la prosecuzione  &#13;
 volontaria  dell'assicurazione  generale  contemporaneamente  ad  altra  &#13;
 assicurazione obbligatoria.                                              &#13;
     È,  dunque,  ammesso il cumulo di pensioni; onde il pensionato non  &#13;
 può  essere  privato  del  beneficio  della  prosecuzione   volontaria  &#13;
 dell'assicurazione  obbligatoria per la invalidità, la vecchiaia o per  &#13;
 la tubercolosi,  al  fine  di  conservare  i  diritti  derivanti  dalla  &#13;
 assicurazione stessa o di raggiungere i requisiti minimi per il diritto  &#13;
 a pensione (art. 5, primo comma, della legge n. 218 del 1952).           &#13;
     La Corte ritiene, pertanto, che la norma dell'art. 16, primo comma,  &#13;
 secondo  periodo,  del  D.P.R.  n. 818 dell'anno 1957 - che toglie tale  &#13;
 beneficio al pensionato - eccede  dai  limiti  della  delega  conferita  &#13;
 dall'art. 37 della legge 4 aprile 1952, n. 218.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la illegittimità costituzionale del primo comma, secondo  &#13;
 periodo, dell'art. 16 del D.P.R. 26 aprile 1957, n. 818,  in  relazione  &#13;
 agli  artt.  5, terzo comma, e 37 della legge 4 aprile 1952, n. 218, ed  &#13;
 in riferimento all'art. 76 della Costituzione.                           &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 5 febbraio 1963.                              &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - BIAGIO PETROCELLI -  ANTONIO  &#13;
                                   MANCA  -  ALDO  SANDULLI  -  GIUSEPPE  &#13;
                                   BRANCA - MICHELE FRAGALI - COSTANTINO  &#13;
                                   MORTATI  -   GIUSEPPE   CHIARELLI   -  &#13;
                                   GIUSEPPE  VERZÌ.</dispositivo>
  </pronuncia_testo>
</pronuncia>
