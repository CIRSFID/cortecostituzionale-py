<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>583</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:583</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/12/1989</data_decisione>
    <data_deposito>22/12/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale degli artt. 4, 5 e 20    &#13;
 della legge 24 luglio 1985, n.  409  (Istituzione  della  professione    &#13;
 sanitaria  di  odontoiatria  e  disposizioni  relative  al diritto di    &#13;
 stabilimento ed alla libera  prestazione  di  servizi  da  parte  dei    &#13;
 dentisti cittadini di Stati membri delle Comunità europee), promosso    &#13;
 con ordinanza emessa il 26 gennaio 1989 dal Tribunale di Mantova  nel    &#13;
 procedimento  civile  vertente tra Accorsi Franco ed altri e l'Ordine    &#13;
 dei Medici e degli Odontoiatri della Provincia di  Mantova,  iscritta    &#13;
 al  n.  340  del  registro ordinanze 1989 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 29,  prima  serie  speciale,  dell'anno    &#13;
 1989;                                                                    &#13;
    Udito  nella  camera  di consiglio del 16 novembre 1989 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso  di  un  giudizio  volto ad accertare la    &#13;
 sussistenza in capo agli attori del diritto a  mantenere  la  propria    &#13;
 iscrizione  all'Albo  dei medici-chirurghi, senza per ciò perdere il    &#13;
 diritto ad esercitare la professione di odontoiatra, il Tribunale  di    &#13;
 Mantova,  con  ordinanza  in  data  26 gennaio 1989, ha sollevato, in    &#13;
 riferimento agli artt.  3  e  41  della  Costituzione,  questione  di    &#13;
 legittimità  costituzionale  degli  artt.  4,  5 e 20 della legge 24    &#13;
 luglio 1985, n.  409  (Istituzione  della  professione  sanitaria  di    &#13;
 odontoiatra  e  disposizioni  relative  al diritto di stabilimento ed    &#13;
 alla libera prestazione di servizi da parte dei dentisti cittadini di    &#13;
 Stati membri delle Comunità europee);                                   &#13;
      che  ad  avviso del giudice a quo le norme impugnate, prevedendo    &#13;
 per i laureati in medicina e chirurgia iscritti al relativo corso  di    &#13;
 laurea anteriormente al 28 gennaio 1980, privi di specializzazione in    &#13;
 odontoiatria ma abilitati all'esercizio professionale, la facoltà di    &#13;
 optare,  nel  termine di cinque anni, per l'iscrizione all'Albo degli    &#13;
 odontoiatri, contrasterebbero:                                           &#13;
        a)  con  l'art.  3  della  Costituzione  per  la disparità di    &#13;
 trattamento che si verrebbe a creare tra medici generici che, volendo    &#13;
 continuare  ad  esercitare  la  professione  di  odontoiatra  perdono    &#13;
 l'iscrizione all'Albo dei medici chirurghi, e medici specialisti  che    &#13;
 invece possono mantenerla;                                               &#13;
        b)  con  l'art.  41  della  Costituzione  per l'ingiustificata    &#13;
 limitazione all'iniziativa economica dei medici chirurghi non  muniti    &#13;
 di diploma di specializzazione;                                          &#13;
      che  non  si  sono  costituite le parti private, né ha spiegato    &#13;
 intervento l'Avvocatura Generale dello Stato;                            &#13;
    Considerato  che con sentenza n. 100 del 1989 questa Corte ha già    &#13;
 accolto la questione ora sottoposta al suo esame;                        &#13;
      che  il  giudice  a  quo non prospetta profili di illegittimità    &#13;
 nuovi o diversi rispetto a quelli già esaminati da  questa  Corte  e    &#13;
 per    i   quali   le   norme   impugnate   sono   state   dichiarate    &#13;
 costituzionalmente illegittime;                                          &#13;
      che   pertanto   la   questione   va  dichiarata  manifestamente    &#13;
 inammissibile;                                                           &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt. 4, 5  e  20  della  legge  24    &#13;
 luglio  1985  n.  409  (Istituzione  della  professione  sanitaria di    &#13;
 odontoiatra e disposizioni relative al  diritto  di  stabilimento  ed    &#13;
 alla libera prestazione di servizi da parte dei dentisti cittadini di    &#13;
 Stati membri delle Comunità europee), sollevata, in riferimento agli    &#13;
 artt.  3  e  41  della  Costituzione  dal  Tribunale  di  Mantova con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 dicembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 22 dicembre 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
