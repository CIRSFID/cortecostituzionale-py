<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>110</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:110</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/03/1999</data_decisione>
    <data_deposito>02/04/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  21, primo    &#13;
 comma, n. 2, della legge 29 ottobre 1971, n. 889 (Norme in materia di    &#13;
 previdenza  per  gli  addetti  ai  pubblici  servizi  di  trasporto),    &#13;
 promosso  con ordinanza emessa il 6 maggio 1997 dal Tribunale di Bari    &#13;
 sul ricorso proposto dall'INPS contro Difino Chiara, iscritta  al  n.    &#13;
 427 del registro ordinanze 1997 e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 28, prima serie speciale, dell'anno 1997.            &#13;
   Visto l'atto di costituzione dell'INPS;                                &#13;
   Udito  nell'udienza  pubblica  del 9 marzo 1999 il giudice relatore    &#13;
 Fernando Santosuosso.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. -  Nel  corso  di  una  controversia  in  materia  previdenziale    &#13;
 promossa  a  seguito  del  rigetto  della  domanda,  proposta  in via    &#13;
 amministrativa, di erogazione della pensione  di  riversibilità  del    &#13;
 defunto  marito,  il  Tribunale  di  Bari, in funzione di giudice del    &#13;
 lavoro,  ha  sollevato  questione  di   legittimità   costituzionale    &#13;
 dell'art  21,  primo comma, n. 2, della legge 29 ottobre 1971, n. 889    &#13;
 (Norme in materia di previdenza per gli addetti ai  pubblici  servizi    &#13;
 di  trasporto)  in  riferimento  agli  artt.  3,  29,  31  e 38 della    &#13;
 Costituzione.                                                            &#13;
   Osserva il Tribunale che nel caso in esame  la  domanda  era  stata    &#13;
 respinta  in  quanto  il  matrimonio,  contratto  dal  marito dopo il    &#13;
 collocamento in pensione ed in età superiore  ai  settantadue  anni,    &#13;
 era  durato meno di due anni. Ora la norma impugnata, analogamente ad    &#13;
 altre già colpite da declaratoria di  illegittimità  costituzionale    &#13;
 (il rimettente richiama la sentenza n. 189 del 1991 di questa Corte),    &#13;
 appare  in  contrasto con gli invocati parametri, poiché è priva di    &#13;
 ragionevole giustificazione, costituisce una remora  alla  formazione    &#13;
 di  un  nucleo  familiare e nega quella garanzia di assistenza che si    &#13;
 ottiene grazie all'istituto della pensione di riversibilità.            &#13;
   In ordine al requisito della rilevanza, il giudice  a  quo  precisa    &#13;
 che  dall'eventuale accoglimento della presente questione deriverebbe    &#13;
 anche  l'accoglimento  della  domanda  di  pensione  avanzata   dalla    &#13;
 ricorrente.                                                              &#13;
   2.  -  Nel  giudizio  davanti  a  questa  Corte  si  è  costituito    &#13;
 l'Istituto nazionale della  previdenza  sociale,  osservando  che  la    &#13;
 sollevata  questione  trova  un  precedente  del  tutto analogo nella    &#13;
 citata sentenza n.  189  del  1991  e  rimettendosi,  pertanto,  alla    &#13;
 decisione che si vorrà adottare in questa sede.<diritto>Considerato in diritto</diritto>1.  -  Il  Tribunale di Bari, in funzione di giudice del lavoro, ha    &#13;
 sollevato questione di legittimità costituzionale dell'art 21, primo    &#13;
 comma, n. 2, della legge 29 ottobre 1971, n. 889 (Norme in materia di    &#13;
 previdenza per gli addetti  ai  pubblici  servizi  di  trasporto)  in    &#13;
 riferimento agli artt. 3, 29, 31 e 38 della Costituzione. La norma è    &#13;
 stata  impugnata  in quanto preclude la concessione della pensione di    &#13;
 riversibilità in  favore  del  coniuge  allorquando  il  matrimonio,    &#13;
 contratto  dal  pensionato in età superiore ai settantadue anni, sia    &#13;
 durato meno di due anni.                                                 &#13;
   2. - La questione è fondata.                                          &#13;
   La  norma  impugnata,  dettata  per  il  regime previdenziale degli    &#13;
 addetti  ai  pubblici  servizi  di   trasporto,   è   di   contenuto    &#13;
 sostanzialmente  analogo  a diverse altre, già colpite da precedenti    &#13;
 declaratorie di illegittimità costituzionale (v., fra le  altre,  le    &#13;
 sentenze n.  1 del 1992, n. 450 del 1991, n. 189 del 1991, n. 123 del    &#13;
 1990  e n.  587 del 1988). Nelle pronunce ora richiamate questa Corte    &#13;
 ha ravvisato l'intrinseca  irrazionalità  di  norme  che  -  facendo    &#13;
 derivare  effetti giuridici negativi dall'età dello sposo pensionato    &#13;
 e  dalla  breve   durata   del   vincolo   coniugale   -   incidevano    &#13;
 indirettamente  sulla  libertà matrimoniale, cui si collegano valori    &#13;
 di rilevanza costituzionale; osservandosi d'altra parte che i  mutati    &#13;
 costumi sociali mostrano di frequente la tendenza di persone non più    &#13;
 giovani a contrarre matrimonio.                                          &#13;
   A  queste  osservazioni può aggiungersi che anche la norma oggi in    &#13;
 esame,  analogamente  a  quelle  già  espunte  dal   sistema,   crea    &#13;
 un'irragionevole discriminazione, in presenza di matrimoni ugualmente    &#13;
 validi,  sulla  base  di  elementi estranei all'essenza e ai fini del    &#13;
 vincolo coniugale, quali l'età avanzata del contraente e  la  durata    &#13;
 del matrimonio.                                                          &#13;
   Deriverebbe  così  una sorta di efficacia limitata del matrimonio,    &#13;
 non ricollegandosi ad esso i normali diritti di natura previdenziale,    &#13;
 come appunto la  pensione  di  riversibilità.  Tale  conclusione  è    &#13;
 evidentemente  irrazionale sì da tradursi in una lesione dell'art. 3    &#13;
 della Costituzione.                                                      &#13;
   Ogni  altro  profilo  di  denunziata  incostituzionalità   rimane,    &#13;
 pertanto, assorbito.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  l'illegittimità costituzionale dell'art 21, primo comma,    &#13;
 n. 2, della legge 29 ottobre  1971,  n.  889  (Norme  in  materia  di    &#13;
 previdenza per gli addetti ai pubblici servizi di trasporto).            &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 marzo 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Santosuosso                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 2 aprile 1999.                            &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
