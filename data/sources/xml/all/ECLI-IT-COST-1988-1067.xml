<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>1067</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:1067</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/11/1988</data_decisione>
    <data_deposito>06/12/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, avv. Mauro FERRI, prof. Luigi MENGONI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 6, commi 1° e    &#13;
 3°, del d.l. 29 gennaio 1983, n. 17 (Misure per il  contenimento  del    &#13;
 costo  del  lavoro e per favorire l'occupazione), convertito in legge    &#13;
 25 marzo 1983, n. 79, e dell'art. 2, comma 2°,  del  d.l.  17  aprile    &#13;
 1984,  n.  70  (Misure  urgenti  in  materia  di  tariffe,  di prezzi    &#13;
 amministrati e di indennità di contingenza), convertito in legge  12    &#13;
 giugno  1984,  n.  219 promosso con l'ordinanza emessa il 27 novembre    &#13;
 1987 dal Tribunale di Cosenza nel procedimento  civile  vertente  tra    &#13;
 INPS  e  Pisano  Francesco  iscritta al n. 179 del registro ordinanze    &#13;
 1988 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  20,    &#13;
 prima serie speciale, dell'anno 1988;                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 9 novembre 1988 il Giudice    &#13;
 relatore Luigi Mengoni;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Il Tribunale di Cosenza, con ordinanza del 27 novembre 1987,    &#13;
 ha sollevato questione di legittimità costituzionale degli artt.  6,    &#13;
 primo e terzo comma, del d.l. 29 gennaio 1983 n. 17, convertito nella    &#13;
 legge 25 marzo 1983 n. 79, e 2, secondo comma,  del  d.l.  17  aprile    &#13;
 1984 n. 70, convertito nella legge 12 giugno 1984 n. 219, nella parte    &#13;
 in cui, ai fini dell'attribuzione della maggiorazione  degli  assegni    &#13;
 familiari,  ricomprendono  nel  calcolo annuale del reddito familiare    &#13;
 complessivo, assoggettabile all'IRPEF nell'anno precedente al periodo    &#13;
 di  paga  in corso, anche gli emolumenti relativi ad anni anteriori e    &#13;
 soggetti a tassazione separata, escludendo soltanto i trattamenti  di    &#13;
 fine rapporto.                                                           &#13;
    Tale  disciplina  è  ritenuta contrastante con l'art. 3 Cost., in    &#13;
 quanto comporta per i lavoratori subordinati,  che  percepiscono  con    &#13;
 ritardo emolumenti afferenti ad anni precedenti il periodo di paga in    &#13;
 corso, un trattamento deteriore rispetto agli altri  lavoratori  che,    &#13;
 nell'identica  situazione, hanno percepito tempestivamente gli stessi    &#13;
 emolumenti.                                                              &#13;
    Il  giudice remittente ravvisa inoltre una violazione dell'art. 31    &#13;
 Cost., "atteso che la normativa in esame pone limiti ingiustificati e    &#13;
 irrazionali a provvidenze tendenti ad agevolare con misure economiche    &#13;
 la famiglia".                                                            &#13;
    2.  - Nel giudizio davanti alla Corte non vi è stata costituzione    &#13;
 di parti. È intervenuto il Presidente del  Consiglio  dei  Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura dello Stato, contestando la    &#13;
 fondatezza della questione.                                              &#13;
    Osserva  l'Avvocatura  che  il  criterio  di  cassa  adottato  dal    &#13;
 legislatore attraverso il richiamo all'imponibilità  IRPEF  risponde    &#13;
 razionalmente  all'esigenza  di  adeguare  l'intervento  di  sostegno    &#13;
 economico al parametro concreto della disponibilità di mezzi che  si    &#13;
 verifica,  nel  periodo  in  considerazione, per la famiglia cui deve    &#13;
 provvedere il lavoratore". Perciò,  come  non  è  possibile  tenere    &#13;
 conto,  nel  valutare  le  condizioni economiche del lavoratore in un    &#13;
 determinato anno, di ciò che egli avrebbe dovuto percepire,  ma  che    &#13;
 di  fatto  non  gli  è stato corrisposto - onde l'attribuzione delle    &#13;
 maggiorazioni in questione e la definizione della loro misura possono    &#13;
 risultare condizionate favorevolmente per il lavoratore da un ritardo    &#13;
 nella corresponsione degli emolumenti dovutigli  -,  così  non  v'è    &#13;
 ragione  di  non  considerare, sempre ai fini della valutazione della    &#13;
 situazione economica in cui versa la famiglia del  lavoratore  in  un    &#13;
 determinato  anno le entrate in questo conseguite, pur se relative ad    &#13;
 emolumenti maturati in anni precedenti.                                  &#13;
    Data la diversità concettuale tra l'imponibile annuale IRPEF (che    &#13;
 ha carattere individuale) e il reddito  (cumulativo)  della  famiglia    &#13;
 rilevante  ai  fini  della normativa denunziata, "non vale richiamare    &#13;
 l'esclusione dal coacervo del reddito familiare  del  trattamento  di    &#13;
 fine   rapporto  (giustificata  dalle  peculiari  finalità  di  tale    &#13;
 erogazione) per inferirne la  necessità  di  analoga  esclusione  di    &#13;
 altri  proventi solo perché soggetti, a pari del trattamento di fine    &#13;
 rapporto, a diverso e separato computo in sede di  definizione  degli    &#13;
 imponibili IRPEF per lo stesso anno di tassazione".                      &#13;
    Privo    di   autonomo   rilievo   appare   infine,   a   giudizio    &#13;
 dell'Avvocatura,   il   riferimento   all'art.   31   Cost.   Rientra    &#13;
 nell'autonomia del legislatore ordinario la definizione dei limiti di    &#13;
 attuazione dei sostegni economici alle famiglie,  in  relazione  alle    &#13;
 diverse situazioni reddituali.<diritto>Considerato in diritto</diritto>1.   -   Il   Tribunale   di  Cosenza  dubita  della  legittimità    &#13;
 costituzionale, alla stregua degli artt. 3 e 31 Cost.,  dell'art.  6,    &#13;
 primo  e  terzo  comma, del d.l. 29 gennaio 1983 n. 17, convertito in    &#13;
 legge 25 marzo 1983  n.  79,  nella  parte  in  cui,  ai  fini  della    &#13;
 maggiorazione  degli  assegni  familiari  ai  lavoratori  dipendenti,    &#13;
 prevista nel precedente art.  5,  include  nel  computo  del  reddito    &#13;
 familiare  complessivo, assoggettabile all'IRPEF nell'anno precedente    &#13;
 il periodo  paga  in  corso,  anche  gli  arretrati  di  retribuzione    &#13;
 maturati   in  anni  anteriori  e  soggetti  a  tassazione  separata:    &#13;
 inclusione  confermata,  per  argomento  a  contrario,  dall'art.  2,    &#13;
 secondo comma, del successivo d.l. 17 aprile 1984 n.70, convertito in    &#13;
 legge 12 giugno 1984 n. 219,  pure  impugnato,  che  ha  escluso  dal    &#13;
 computo i soli trattamenti di fine rapporto.                             &#13;
    2. - Le questioni non sono fondate.                                   &#13;
    La  maggiorazione  degli assegni familiari è concessa dal d.l. n.    &#13;
 17 del 1983 in ragione di un rapporto, che non deve essere  inferiore    &#13;
 al  70  per cento, tra i flussi salariali nella famiglia e il reddito    &#13;
 familiare  complessivo  assoggettabile  all'imposta   personale   sul    &#13;
 reddito.  Poiché il rapporto deve essere verificato anno per anno in    &#13;
 base alle risultanze delle  dichiarazioni  annuali  dei  redditi  dei    &#13;
 componenti  il  nucleo familiare, è ragionevole che nel coarcevo dei    &#13;
 redditi, da assumere come parametro per stabilire la spettanza o meno    &#13;
 della  maggiorazione  di  cui  è  causa,  siano  compresi  anche gli    &#13;
 arretrati di retribuzione percepiti  nel  periodo  paga  considerato,    &#13;
 posto che essi pure concorrono a integrare la disponibilità di mezzi    &#13;
 economici della famiglia in tale periodo.                                &#13;
    Se  non  fossero  conteggiati  nell'anno  di percezione, i redditi    &#13;
 soggetti  a  tassazione  separata   dovrebbero   essere   conteggiati    &#13;
 nell'anno   di  maturazione.  Ma  questa  soluzione  in  primo  luogo    &#13;
 contrasterebbe con la ratio della legge, in quanto la spettanza della    &#13;
 maggiorazione   in  quell'anno  verrebbe  determinata  in  base  alla    &#13;
 capacità economica potenziale, non  effettiva,  della  famiglia;  in    &#13;
 secondo  luogo  offenderebbe il principio di economicità, addossando    &#13;
 all'INPS l'onere di rifare i calcoli per quell'anno e provvedere alle    &#13;
 rettifiche  conseguenti,  e  ai lavoratori l'obbligo di restituire le    &#13;
 somme che risultassero non spettanti.                                    &#13;
    La  sentenza auspicata dal giudice a quo non già eliminerebbe una    &#13;
 discriminazione, in realtà inesistente, a danno dei  lavoratori  che    &#13;
 ricevono  in  ritardo  emolumenti  maturati  in  un anno anteriore al    &#13;
 periodo paga in corso, bensì creerebbe una discriminazione a sfavore    &#13;
 di    "coloro    che   nell'identica   situazione   hanno   percepito    &#13;
 tempestivamente gli stessi emolumenti": i primi,  infatti,  avrebbero    &#13;
 il  privilegio  di non vedere computati tali emolumenti né nell'anno    &#13;
 in cui sono maturati, né nell'anno in cui sono stati  (tardivamente)    &#13;
 corrisposti.                                                             &#13;
    3.  -  Il  principio  dell'art.  3  Cost.  non  è violato nemmeno    &#13;
 dall'art. 2 del d.l. n. 70 del 1984, convertito nella  legge  n.  219    &#13;
 del   1984,   che  ha  escluso  dal  computo  del  reddito  familiare    &#13;
 complessivo i trattamenti di fine rapporto. Il  trattamento  di  fine    &#13;
 rapporto  non  è formato da retribuzioni arretrate, anche se ai fini    &#13;
 fiscali è trattato come tale, e comunque l'eccezione prevista  dalla    &#13;
 legge   citata   si   giustifica  in  considerazione  della  funzione    &#13;
 previdenziale propria del detto trattamento, la quale si proietta nel    &#13;
 futuro  ben oltre il periodo di riferimento del calcolo ai fini della    &#13;
 maggiorazione degli assegni familiari.                                   &#13;
    4.  -  Priva  di  consistenza  è,  infine,  la pretesa violazione    &#13;
 dell'art. 31 Cost. La determinazione delle forme e della misura delle    &#13;
 provvidenze   economiche  a  sostegno  dei  nuclei  familiari,  e  in    &#13;
 particolare  delle  famiglie  numerose,  è  materia  di  valutazione    &#13;
 discrezionale del legislatore.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondate  le questioni di legittimità costituzionale    &#13;
 dell'art. 6 del d.l. 29 gennaio 1983 n. 17, convertito  in  legge  25    &#13;
 marzo  1983 n. 79 ("Misure per il contenimento del costo del lavoro e    &#13;
 per favorire l'occupazione"), e dell'art. 2, secondo comma, del  d.l.    &#13;
 17  aprile  1984  n.  70,  convertito  in legge 12 giugno 1984 n. 219    &#13;
 ("Misure urgenti in materia di tariffe, di prezzi amministrati  e  di    &#13;
 indennità di contingenza"), sollevate, in riferimento agli artt. 3 e    &#13;
 31 Cost., dal  Tribunale  di  Cosenza  con  l'ordinanza  indicata  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 novembre 1988.                             &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 6 dicembre 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
