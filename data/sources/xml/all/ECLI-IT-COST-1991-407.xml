<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>407</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:407</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/11/1991</data_decisione>
    <data_deposito>12/11/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  58,  comma    &#13;
 quarto,  della  legge  8  giugno  1990,  n.  142  (Ordinamento  delle    &#13;
 autonomie locali) promosso con ordinanza emessa il  23  ottobre  1990    &#13;
 dalla Corte dei Conti - Sezione prima giurisdizionale nel giudizio di    &#13;
 responsabilità  nei confronti di Zandonella Gorgolon Flavio ed altri    &#13;
 iscritta al n. 330 del registro ordinanze  1991  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  21,  prima serie speciale    &#13;
 dell'anno 1991;                                                          &#13;
    Udito nella camera di consiglio del  9  ottobre  1991  il  Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto che, in un procedimento di responsabilità amministrativa    &#13;
 contro  i  componenti  della giunta del Comune di Comelico Superiore,    &#13;
 uno dei quali deceduto nel corso del giudizio prima  dell'entrata  in    &#13;
 vigore  della  legge  8  giugno 1990, n. 142, la Corte dei Conti, con    &#13;
 ordinanza del 23 ottobre 1990, pervenuta alla Corte costituzionale il    &#13;
 7 maggio 1991, ha sollevato, in riferimento agli artt. 3 e  97  della    &#13;
 Costituzione,  questione di legittimità costituzionale dell'art. 58,    &#13;
 comma 4, della legge citata, nella  parte  in  cui  dispone  -  senza    &#13;
 prevedere la retroattività del beneficio - l'intrasmissibilità agli    &#13;
 eredi  della responsabilità nei confronti degli amministratori e dei    &#13;
 dipendenti dei comuni e delle province;                                  &#13;
      che,  ad  avviso  del  giudice   remittente,   la   disposizione    &#13;
 impugnata,   avendo  natura  di  norma  sostanziale  regolatrice  del    &#13;
 rapporto, non è applicabile nei processi iniziati prima dell'entrata    &#13;
 in vigore della legge n. 142 del 1990;                                   &#13;
      che il mancato conferimento di retroattività alla  norma  viola    &#13;
 il  principio di eguaglianza e anche i principi di imparzialità e di    &#13;
 buon andamento della pubblica amministrazione;                           &#13;
      che peraltro, sempre ad avviso del giudice remittente, ove fosse    &#13;
 dichiarata  l'incostituzionalità  della  norma  sotto  questo  primo    &#13;
 profilo,  con  conseguente  applicabilità  della medesima al caso di    &#13;
 specie, la questione  di  legittimità  costituzionale  acquisterebbe    &#13;
 rilevanza  sotto  altri  profili:  a)  per contrasto col principio di    &#13;
 eguaglianza, in quanto la norma riserva un  trattamento  privilegiato    &#13;
 agli  amministratori  e  ai  dipendenti  dei comuni e delle province,    &#13;
 discriminando  ingiustificatamente  gli amministratori e i dipendenti    &#13;
 degli altri enti  pubblici  e  dello  Stato;  b)  per  contrasto  col    &#13;
 principio  di  razionalità,  essendo  la  norma  contraddittoria col    &#13;
 criterio di unificazione del regime di responsabilità  dei  pubblici    &#13;
 amministratori   e  dipendenti  dello  Stato  e  degli  enti  locali,    &#13;
 enunciato  nel  comma  1  dell'art.  58;  c)  per  contrasto  infine,    &#13;
 emergente  da  entrambe  le  ragioni  indicate, con i principi di cui    &#13;
 all'art. 97, primo comma, Cost.;                                         &#13;
    Considerato che la questione è articolata in due  capi  tra  loro    &#13;
 contraddittori:  nel  primo si lamenta l'inapplicabilità della norma    &#13;
 al caso di specie a cagione della  sua  irretroattività,  che  viene    &#13;
 denunciata  come  ingiustificatamente  discriminatoria  a danno degli    &#13;
 eredi di amministratori comunali o provinciali deceduti nel corso  di    &#13;
 giudizi  di  responsabilità  già  pendenti  alla data di entrata in    &#13;
 vigore  della  legge  n.  142  del  1990;  nel  secondo,  subordinato    &#13;
 all'accoglimento  del  primo, si lamenta l'applicabilità della norma    &#13;
 che ne conseguirebbe nel giudizio  a  quo,  e  si  impugna  la  norma    &#13;
 medesima  in  quanto  la sua applicazione concreterebbe un privilegio    &#13;
 ritenuto contrastante  sia  col  principio  di  eguaglianza,  essendo    &#13;
 riservato  ai  soli  amministratori  e  dipendenti dei comuni e delle    &#13;
 province,  sia  col  principio  di   razionalità   per   la   deroga    &#13;
 ingiustificata  che  esso  porta al principio della successione degli    &#13;
 eredi nei rapporti di debito del defunto (artt. 752 e 754 cod. civ.);    &#13;
      che, pertanto,  la  questione,  oltre  che  contraddittoria,  è    &#13;
 irrilevante   perché   l'accoglimento   di   essa,  con  conseguente    &#13;
 caducazione della norma impugnata, comporterebbe una definizione  del    &#13;
 giudizio   a   quo,   in  punto  di  successione  degli  eredi  nella    &#13;
 responsabilità dell'amministratore deceduto, non diversa  da  quella    &#13;
 cui  il giudice remittente perverrebbe applicando de plano il diritto    &#13;
 vigente;                                                                 &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 58,  comma  4,  della  legge  8    &#13;
 giugno  1990, n. 142 (Ordinamento delle autonomie locali), sollevata,    &#13;
 in riferimento agli artt. 3 e 97 della Costituzione, dalla Corte  dei    &#13;
 Conti con l'ordinanza indicata in epigrafe.                              &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 novembre 1991.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 12 novembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
