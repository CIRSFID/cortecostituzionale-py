<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1987-10-08/311/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1987-10-08/311"/>
          <FRBRalias value="ECLI:IT:COST:1987:311" name="ECLI"/>
          <FRBRdate date="1987-10-08" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="311"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1987-10-08/311/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1987-10-08/311/ita@"/>
          <FRBRdate date="1987-10-08" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/1987-10-08/311/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/1987-10-08/311/ita@.xml"/>
          <FRBRdate date="1987-10-08" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1987-09-30" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1987-10-08" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="francescoSaja" href="/akn/ontology/person/it/francescoSaja" showAs="Francesco SAJA"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="Virgilio ANDRIOLI"/>
        <TLCPerson eId="giovanniConso" href="/akn/ontology/person/it/giovanniConso" showAs="Giovanni CONSO"/>
        <TLCPerson eId="aldoCorasaniti" href="/akn/ontology/person/it/aldoCorasaniti" showAs="Aldo CORASANITI"/>
        <TLCPerson eId="giuseppeBorzellino" href="/akn/ontology/person/it/giuseppeBorzellino" showAs="Giuseppe BORZELLINO"/>
        <TLCPerson eId="renatoDellAndro" href="/akn/ontology/person/it/renatoDellAndro" showAs="Renato DELL'ANDRO"/>
        <TLCPerson eId="gabrielePescatore" href="/akn/ontology/person/it/gabrielePescatore" showAs="Gabriele PESCATORE"/>
        <TLCPerson eId="ugoSpagnoli" href="/akn/ontology/person/it/ugoSpagnoli" showAs="Ugo SPAGNOLI"/>
        <TLCPerson eId="francescoPaoloCasavola" href="/akn/ontology/person/it/francescoPaoloCasavola" showAs="Francesco Paolo CASAVOLA"/>
        <TLCPerson eId="antonioBaldassarre" href="/akn/ontology/person/it/antonioBaldassarre" showAs="Antonio BALDASSARRE"/>
        <TLCPerson eId="vincenzoCaianiello" href="/akn/ontology/person/it/vincenzoCaianiello" showAs="Vincenzo CAIANIELLO"/>
        <TLCPerson eId="SAJA" href="/akn/ontology/person/it/SAJA" showAs="SAJA"/>
        <TLCPerson eId="MINELLI" href="/akn/ontology/person/it/MINELLI" showAs="MINELLI"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1987</akn4cc:anno>
        <akn4cc:numero>311</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1987:311</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>SAJA</akn4cc:presidente>
        <akn4cc:relatore>Francesco Saja</akn4cc:relatore>
        <akn4cc:data_decisione>1987-09-30</akn4cc:data_decisione>
        <akn4cc:data_deposito>1987-10-08</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Presidente: dott. <judge refersTo="#francescoSaja">Francesco SAJA</judge>; Giudici: prof. <judge refersTo="#virgilioAndrioli">Virgilio ANDRIOLI</judge>, prof. <judge refersTo="#giovanniConso">Giovanni CONSO</judge>, dott. <judge refersTo="#aldoCorasaniti">Aldo 
 CORASANITI</judge>, prof. <judge refersTo="#giuseppeBorzellino">Giuseppe BORZELLINO</judge>, prof. <judge refersTo="#renatoDellAndro">Renato DELL'ANDRO</judge>, 
 prof. <judge refersTo="#gabrielePescatore">Gabriele PESCATORE</judge>, avv. <judge refersTo="#ugoSpagnoli">Ugo SPAGNOLI</judge>, prof. <judge refersTo="#francescoPaoloCasavola">Francesco Paolo 
 CASAVOLA</judge>, prof. <judge refersTo="#antonioBaldassarre">Antonio BALDASSARRE</judge>, prof. <judge refersTo="#vincenzoCaianiello">Vincenzo CAIANIELLO</judge>;</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nei giudizi di legittimità costituzionale degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_6">artt. 6</ref>, <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_14">14</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_15">15 del    
 d.P.R. 26 ottobre 1972, n. 643</ref></mref> (Istituzione dell'imposta                 
 comunale sull'incremento di valore degli immobili), e 8 secondo comma    
 della <ref href="/akn/it/act/legge/stato/1977-12-16/904/!main">legge 16 dicembre 1977, n. 904</ref>                                     
 (Modificazioni alla disciplina dell'imposta sul reddito delle persone    
 giuridiche e al regime tributario dei dividendi e                        
 degli  aumenti  di  capitale,  adeguamento  del capitale minimo delle    
 società e altre norme in materia fiscale e societaria) promossi  con    
 le  seguenti ordinanze: 1) ordinanza emessa il 20 febbraio 1978 dalla    
 Commissione Tributaria di II grado di Palermo sul ricorso proposto da    
 Guzzarda  Carmela ed altro, iscritta al n. 102 del registro ordinanze    
 1986 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 26, 1ª    
 serie  speciale,  dell'anno 1986; 2) due ordinanze emesse il 9 maggio    
 1980 dalla Commissione tributaria di II grado di Napoli  sui  ricorsi    
 proposti  dall'Ufficio del registro di Castellammare di Stabia contro    
 soc. Cooperativa Week Ends, iscritte ai nn. 124 e  125  del  registro    
 ordinanze 1986 e pubblicate nella Gazzetta Ufficiale della Repubblica    
 n. 30, 1ª serie speciale, dell'anno 1986; 3) ordinanza emessa  il  12    
 maggio  1978  dalla  Commissione  tributaria  di  I grado di Roma sui    
 ricorsi riuniti proposti dalla s.p.a. Assicurazioni  Generali  contro    
 l'Ufficio  del  registro atti Pubblici di Roma, iscritta al n. 78 del    
 registro ordinanze 1987 e pubblicata nella Gazzetta  Ufficiale  della    
 Repubblica n. 14, 1ª serie speciale, dell'anno 1987;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    
 ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udito  nella  camera  di  consiglio  del 17 giugno 1987 il Giudice    
 relatore Francesco Saja;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Ritenuto che la Commissione tributaria di II grado di Palermo, con    
 ordinanza emessa il 20 febbraio 1978  ha  sollevato,  in  riferimento    
 agli  <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt.  3</ref>  e  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53 Cost.</ref></mref>, questione di legittimità costituzionale    
 degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_6">artt. 6</ref>  e  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_14">14  del  d.P.R.  26  ottobre  1972,  n.  643</ref></mref>,  come    
 modificato  dagli  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1974-12-23/688/!main#art_1">artt.  1  del  d.P.R. 23 dicembre 1974, n. 688</ref> e 8    
 della <ref href="/akn/it/act/legge/stato/1977-12-16/904/!main">l. 16 dicembre 1977, n.  904</ref>,  nella  parte  in  cui,  ai  fini    
 dell'applicazione  dell'Invim,  non  prevedono  che  l'incremento  di    
 valore imponibile  sia  depurato  della  componente  imputabile  alla    
 svalutazione monetaria;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che  la Commissione tributaria di I grado di Roma, con ordinanza    
 emessa il 12 maggio 1978, ha sollevato, in  riferimento  ai  medesimi    
 <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt.   3</ref>  e  <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53  Cost.</ref></mref>,  questione  di  legittimità  costituzionale    
 dell'art. 8, secondo comma, della <ref href="/akn/it/act/legge/stato/1977-12-16/904/!main">l. 16 dicembre 1977, n. 904</ref>,  nella    
 parte  in  cui  esclude  dal  campo  di applicazione della norma quei    
 rapporti che non hanno costituito oggetto di  accertamento  da  parte    
 dell'Ufficio, sebbene essi non siano divenuti definitivi ed ancorché    
 il relativo presupposto si sia verificato anteriormente alla data  di    
 entrata in vigore della legge;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che,  con  identiche  ordinanze emesse in data 9 maggio 1980, la    
 Commissione tributaria  di  II  grado  di  Napoli  ha  sollevato,  in    
 riferimento    all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.    3   Cost.</ref>,   questione   di   legittimità    
 costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_15">art.  15  del  d.P.R.  n.  643  del  1972</ref>,  senza    
 qualificare la questione stessa.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Considerato  che identica questione di legittimità costituzionale    
 degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_6">artt. 6</ref> e  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_14">14  del  d.P.R.  n.  643  del  1972</ref></mref>  è  già  stata    
 dichiarata  non  fondata  con  la  <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1979/126/!main">sentenza  n.  126  del 1979</ref> e che,    
 rispetto ai profili ivi esaminati, non  ne  risultano  addotti  altri    
 diversi od ulteriori;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che  l'<ref href="/akn/it/act/legge/stato/1977-12-16/904/!main#art_8">art.  8,  l.  16  dicembre 1977, n. 904</ref> era espressamente    
 finalizzato all'applicazione della detrazione prevista dall'art.  14,    
 <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main">d.P.R.  26  ottobre  1972,  n.  643</ref>,  sì  da essere rimasto travolto    
 dall'abrogazione di questa ultima disposizione, sancita  dall'art.  1    
 del  <ref href="/akn/it/act/decretoLegge/stato/1979-11-12/571/!main">d.l. 12 novembre 1979, n. 571</ref> (convertito in <ref href="/akn/it/act/legge/stato/1980-01-12/2/!main">l. 12 gennaio 1980,    
 n. 2</ref>);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che  la  Commissione  tributaria  di  II  grado di Napoli non ha    
 menomamente motivato circa la rilevanza  nel  giudizio  a  quo  della    
 questione,  peraltro  prospettata soltanto per relationem, onde ne va    
 dichiarata la manifesta inammissibilità;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti  gli <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_26">artt. 26, della l. 11 marzo 1953, n. 87</ref> e 9 delle Norme    
 integrative per i giudizi innanzi alla Corte costituzionale;</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi                              
                        <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                           
   Dichiara  la manifesta infondatezza della questione di legittimità    
 costituzionale degli <mref><ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_6">artt. 6</ref> e <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972-10-26/643/!main#art_14">14 del d.P.R. 26 ottobre 1972, n. 643</ref></mref>,    
 sollevata,  in riferimento agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_53">53 Cost.</ref></mref>, dalla Commissione    
 tributaria di II grado di Palermo con l'ordinanza di cui in epigrafe;</block>
        <paragraph>
          <content>
            <p>Dichiara  la  manifesta  inammissibilità:  a)  della questione di    
 legittimità costituzionale dell'art. 8, secondo comma, della  <ref href="/akn/it/act/legge/stato/1977-12-16/904/!main">l.  16    
 dicembre  1977,  n.  904</ref>, sollevata, in riferimento agli artt. 3 e 53    
 Cost., dalla Commissione tributaria di I  grado  di  Roma;  b)  della    
 questione  di  legittimità costituzionale dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1972/643/!main#art_15">art. 15 d.P.R. n. 643    
 del 1972</ref>, come modificato dall'<ref href="/akn/it/act/legge/stato/1980/2/!main#art_1">art. 1 della                              
 l.  n.  2  del 1980</ref>, sollevata, in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref> dalla    
 Commissione tributaria di  II  grado  di  Napoli,  con  le  ordinanze    
 indicate in epigrafe.</p>
          </content>
        </paragraph>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1987-09-30">30 settembre 1987</date>.</block>
      <p>Il <signature><judge refersTo="#SAJA" as="#presidente">Presidente: SAJA</judge><role refersTo="#presidente">Presidente</role>: SAJA</signature>                               
                         Il <signature><judge refersTo="#SAJA" as="#relatore">Relatore: SAJA</judge><role refersTo="#relatore">Relatore</role>: SAJA</signature>                                
    Depositata in cancelleria l'<date refersTo="#publicationDate" date="1987-10-08">8 ottobre 1987</date>.</p>
      <p>
        <signature>Il <role refersTo="#cancelliere">direttore della cancelleria</role>: <person refersTo="#MINELLI" as="#cancelliere">MINELLI</person></signature>
      </p>
    </conclusions>
  </judgment>
</akomaNtoso>
