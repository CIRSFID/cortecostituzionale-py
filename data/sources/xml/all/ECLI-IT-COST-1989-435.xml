<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>435</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:435</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/07/1989</data_decisione>
    <data_deposito>25/07/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 19, commi primo    &#13;
 e secondo, del decreto  legge  30  settembre  1982,  n.  688  (Misure    &#13;
 urgenti  in  materia  di  entrate fiscali), convertito nella legge 27    &#13;
 novembre 1982, n. 873, promosso con ordinanza  emessa  il  20  maggio    &#13;
 1988 dalla Corte d'appello di Torino nel procedimento civile vertente    &#13;
 tra  l'Amministrazione  delle  Finanze  dello  Stato  e   la   S.p.a.    &#13;
 Montecatini-Edison,  iscritta  al n. 52 del registro ordinanze 1989 e    &#13;
 pubblicata nella Gazzetta Ufficiale  della  Repubblica  n.  7,  prima    &#13;
 serie speciale, dell'anno 1989;                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 17 maggio 1989 il Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso  di  un  giudizio  avente  ad oggetto la    &#13;
 restituzione di diritti  per  servizi  amministrativi,  indebitamente    &#13;
 corrisposti  per  l'importazione di merci provenienti da paesi - come    &#13;
 si evince dall'esame del  fascicolo  a  quo  -  aderenti  al  General    &#13;
 Agreement  on  Tariffs  and  Trade  (G.A.T.T.), la Corte d'appello di    &#13;
 Torino, con ordinanza in data 20 maggio 1988 (r.o. n. 52  del  1989),    &#13;
 ha  sollevato  questione  di legittimità costituzionale dell'art. 19    &#13;
 del decreto legge 30  settembre  1982,  n.  688  (Misure  urgenti  in    &#13;
 materia di entrate fiscali), convertito in legge 27 novembre 1982, n.    &#13;
 873;                                                                     &#13;
      che  la  norma  denunciata  viene  censurata nella parte in cui,    &#13;
 subordinando con  effetto  retroattivo  la  ripetizione  di  tributi,    &#13;
 indebitamente  pagati,  alla  prova documentale che l'ammontare degli    &#13;
 stessi non sia stato riversato su  altri  soggetti,  si  porrebbe  in    &#13;
 contrasto con gli artt. 3 e 24 della Costituzione;                       &#13;
      che  è  intervenuta l'Avvocatura generale dello Stato chiedendo    &#13;
 che la questione venga dichiarata manifestamente infondata;              &#13;
    Considerato  che  il giudice a quo non esprime alcun apprezzamento    &#13;
 circa il requisito della non manifesta infondatezza,  limitandosi  ad    &#13;
 affermare  che  la  legittimità costituzionale della norma impugnata    &#13;
 non appare "incontrovertibile secondo gli attuali orientamenti  della    &#13;
 dottrina e della giurisprudenza";                                        &#13;
      che  sotto  tale  profilo  la  questione  va pertanto dichiarata    &#13;
 manifestamente inammissibile risultando violata  la  prescrizione  di    &#13;
 cui  all'art.  23,  secondo  comma,  della legge 11 marzo 1953, n. 87    &#13;
 (vedi ord. n. 102 del 1983);                                             &#13;
      che,  peraltro,  ad  identiche conclusioni deve pervenirsi anche    &#13;
 per quanto concerne il requisito della rilevanza, in quanto l'atto di    &#13;
 rimessione non appare sufficientemente motivato, non essendo in alcun    &#13;
 modo precisato se si tratta  di  importazioni  provenienti  da  paesi    &#13;
 comunitari  o  extracomunitari,  e,  in  quest'ultimo  caso,  se  sia    &#13;
 comunque applicabile la normativa CEE,  dovendosi,  nell'eventualità    &#13;
 che  ricorresse  una  di  tali  ipotesi, escludere la rilevanza della    &#13;
 questione (vedi in proposito ord. n. 1077 del 1988);                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità  costituzionale  dell'art.  19  del  decreto  legge   30    &#13;
 settembre  1982,  n.  688  (Misure  urgenti  in  materia  di  entrate    &#13;
 fiscali), convertito in legge 27 novembre 1982, n. 873, sollevata, in    &#13;
 riferimento  agli  artt.  3  e  24  della  Costituzione,  dalla Corte    &#13;
 d'Appello di Torino, con l'ordinanza indicata in epigrafe.               &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 luglio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: DI PAOLA                          &#13;
    Depositata in cancelleria il 25 luglio 1989.                          &#13;
                        Il cancelliere: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
