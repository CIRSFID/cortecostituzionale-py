<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>899</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:899</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>07/07/1988</data_decisione>
    <data_deposito>26/07/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, &#13;
 dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, &#13;
 prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 8, terzo comma,    &#13;
 della legge 20 novembre 1982, n. 890 (Notificazione di atti  a  mezzo    &#13;
 posta  e di comunicazioni a mezzo posta connesse con la notificazione    &#13;
 di atti giudiziari), promosso con ordinanza  emessa  il  20  novembre    &#13;
 1985 dal Tribunale di Brescia nel procedimento civile vertente tra la    &#13;
 Ditta Centro Veneto e la S.p.a. Magazzini Rossi, iscritta al  n.  337    &#13;
 del  registro  ordinanze  1986  e pubblicata nella Gazzetta Ufficiale    &#13;
 della Repubblica n. 35, prima serie speciale, dell'anno 1986;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio dell'11 maggio 1988 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto  che  il  Tribunale  di Brescia, con ordinanza in data 20    &#13;
 novembre 1985, ha sollevato, in riferimento agli artt. 3 e 24  Cost.,    &#13;
 questione  di  legittimità costituzionale dell'art. 8 della legge 20    &#13;
 novembre 1982, n. 890 (Notificazione di  atti  a  mezzo  posta  e  di    &#13;
 comunicazioni  a  mezzo  posta  connesse con la notificazione di atti    &#13;
 giudiziari);                                                             &#13;
      che  la  questione è stata sollevata nel corso del procedimento    &#13;
 civile di opposizione a decreto ingiuntivo,  vertente  tra  la  Ditta    &#13;
 Centro Veneto e la s.p.a. Magazzini Rossi;                               &#13;
      che  l'opposizione  a decreto ingiuntivo da parte della predetta    &#13;
 Ditta era risultata proposta oltre il termine di venti  giorni  dalla    &#13;
 notifica  del  decreto, eseguita, ai sensi dell'art. 8 della legge 20    &#13;
 novembre 1982, n. 890, con deposito  del  piego  raccomandato  presso    &#13;
 l'Ufficio  postale per il prescritto periodo di dieci giorni, decorsi    &#13;
 i quali il piego era stato restituito al mittente;                       &#13;
      che   la  s.p.a.  Magazzini  Rossi  aveva,  pertanto,  sollevato    &#13;
 eccezione pregiudiziale di tardività della opposizione, in  base  al    &#13;
 disposto della citata norma;                                             &#13;
      che, ad avviso del giudice a quo, il diritto alla difesa sancito    &#13;
 dall'art.  24  della  Costituzione  potrebbe  subire   una   indebita    &#13;
 compressione in virtù della disposizione che stabilisce la immediata    &#13;
 restituzione al mittente del  piego  non  ritirato  entro  il  decimo    &#13;
 giorno  dal deposito, nel caso di temporanea assenza del destinatario    &#13;
 dell'atto, al quale,  in  tale  ipotesi,  rimarrebbe  definitivamente    &#13;
 preclusa  la possibilità di avere cognizione del contenuto dell'atto    &#13;
 stesso;                                                                  &#13;
      che  nell'ordinanza  di  rimessione  si  prospetta, inoltre, una    &#13;
 ingiustificata disparità di trattamento fra  il  destinatario  della    &#13;
 notifica   a   mezzo   posta   e  quello  della  notifica  effettuata    &#13;
 personalmente dall'ufficiale giudiziario, in quanto in questo  ultimo    &#13;
 caso la temporanea assenza del notificando dà luogo a deposito nella    &#13;
 casa comunale, ove la copia dell'atto  rimane  a  disposizione  senza    &#13;
 limiti di tempo;                                                         &#13;
      che  nel  giudizio  ha  spiegato  intervento  il  Presidente del    &#13;
 Consiglio  dei  ministri,  rappresentato  e  difeso   dall'Avvocatura    &#13;
 Generale  dello  Stato,  che  ha  concluso per la inammissibilità e,    &#13;
 comunque, nel merito, per la infondatezza della questione, anche alla    &#13;
 luce  di  quanto  già deciso da questa Corte con sentenza n. 213 del    &#13;
 1975;                                                                    &#13;
    Considerato che la questione, così come prospettata è già stata    &#13;
 esaminata  da  questa  Corte  e  decisa  nel  senso  della  manifesta    &#13;
 inammissibilità con ordinanza n. 429 del 1988 in base al rilievo che    &#13;
 la relativa censura sollecita, in sostanza,  l'apprestamento  di  una    &#13;
 nuova  disciplina  della  notifica  a  mezzo  posta,  con riferimento    &#13;
 all'ipotesi della temporanea  assenza  del  destinatario,  suggerendo    &#13;
 alcuni  correttivi finalizzati ad assicurare una migliore garanzia di    &#13;
 effettiva conoscenza da  parte  del  destinatario;  ed  all'ulteriore    &#13;
 rilievo  che  quelli  indicati  sono,  all'evidenza,  solo alcuni dei    &#13;
 possibili correttivi in senso garantista della disciplina denunciata,    &#13;
 la  cui  concreta  adozione resta riservata alle scelte discrezionali    &#13;
 del legislatore;                                                         &#13;
      che l'ordinanza in epigrafe non prospetta elementi nuovi, idonei    &#13;
 a consentire una diversa valutazione dei termini della questione,  la    &#13;
 quale, pertanto, si palesa manifestamente inammissibile;                 &#13;
    Visti  gli  artt. 26, secondo comma, legge 11 marzo 1953, n. 87, e    &#13;
 9, secondo comma, delle Norme integrative per i giudizi davanti  alla    &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 8 della legge 20 novembre 1982,    &#13;
 n.  890  (Notificazione  di  atti  a mezzo posta e di comunicazioni a    &#13;
 mezzo posta  connesse  con  la  notificazione  di  atti  giudiziari),    &#13;
 sollevata,  in  riferimento agli artt. 3 e 24 Cost., dal Tribunale di    &#13;
 Brescia con l'ordinanza in epigrafe.                                     &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 7 luglio 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 26 luglio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
