<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>146</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:146</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia>Carlo Mezzanotte</redattore_pronuncia>
    <data_decisione>22/04/2002</data_decisione>
    <data_deposito>03/05/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale dell'art. 13, comma 2, del &#13;
decreto   legislativo  25 luglio  1998,  n. 286  (Testo  unico  delle &#13;
disposizioni  concernenti  la  disciplina  dell'immigrazione  e norme &#13;
sulla  condizione  dello straniero), promossi con ordinanze emesse il &#13;
31 luglio 2000 (due ordinanze) e il 15 febbraio 2001 dal Tribunale di &#13;
Vicenza,  in  composizione  monocratica,  rispettivamente iscritte al &#13;
n. 111,  n. 136  e  n. 274  del  registro ordinanze 2001 e pubblicate &#13;
nella  Gazzetta  Ufficiale  della  Repubblica  n. 8, n. 9 e n. 16, 1a &#13;
serie speciale, dell'anno 2001. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 10 ottobre 2001 il giudice &#13;
relatore Carlo Mezzanotte. &#13;
    Ritenuto  che,  con  tre  ordinanze  di analogo contenuto in data &#13;
31 luglio  2000 (r.o. n. 111 e n. 136 del 2001) e in data 15 febbraio &#13;
2001  (r.o. n. 274 del 2001), emesse nel corso di procedimenti civili &#13;
promossi da stranieri che avevano proposto ricorso avverso il decreto &#13;
di  espulsione, il Tribunale di Vicenza, in composizione monocratica, &#13;
ha   sollevato,  in  riferimento  agli  articoli  2,  3  e  35  della &#13;
Costituzione,  questione di legittimità costituzionale dell'art. 13, &#13;
comma  2, del decreto legislativo 25 luglio 1998, n. 286 (Testo unico &#13;
delle  disposizioni  concernenti  la  disciplina  dell'immigrazione e &#13;
norme   sulla   condizione  dello  straniero),  nella  parte  in  cui &#13;
stabilisce che il prefetto deve disporre automaticamente l'espulsione &#13;
dello  straniero,  una  volta accertati i presupposti previsti, e non &#13;
gli   consente  di  prendere  in  considerazione  situazioni  che  ne &#13;
legittimerebbero la permanenza in Italia; &#13;
        che,  quanto alla rilevanza, nelle ordinanze di rimessione si &#13;
osserva  che  i  ricorrenti  hanno  prospettato situazioni personali, &#13;
quali  la  occupazione  lavorativa,  la  disponibilità di alloggio e &#13;
l'esistenza di carichi familiari, che sarebbero astrattamente idonee, &#13;
nell'ambito  delle  quote di ingresso, a legittimare la loro presenza &#13;
in Italia, ma la disposizione censurata non permetterebbe di ritenere &#13;
esistente   un  potere  discrezionale  del  prefetto  in  materia  di &#13;
espulsione,   in   quanto,   una   volta  accertata  l'esistenza  dei &#13;
presupposti  previsti,  l'emanazione  da  parte  sua  del  decreto di &#13;
espulsione dovrebbe considerarsi automatica; &#13;
        che,  ad  avviso  del  giudice a quo la mancata previsione di &#13;
attenuazione della automaticità della espulsione quando lo straniero &#13;
abbia  dimostrato  di versare in una situazione che legittimerebbe la &#13;
sua  permanenza  in  Italia, violerebbe i principi di solidarietà di &#13;
cui  all'art. 2  della  Costituzione  e  sarebbe  in contrasto con il &#13;
principio  di  eguaglianza,  in  quanto  lo straniero in possesso dei &#13;
requisiti  per  la  concessione  del permesso di soggiorno al momento &#13;
della  pronuncia  del decreto di espulsione subirebbe "un trattamento &#13;
diverso  e  peggiore  rispetto  a  colui  che  si  trova nella stessa &#13;
situazione di fatto ma ha a monte il titolo di permanenza"; &#13;
        che, secondo il remittente, la disposizione censurata sarebbe &#13;
in  contrasto  anche  con  l'art. 35  della  Costituzione,  in quanto &#13;
l'espulsione  automatica  dello  straniero e il divieto di rientro in &#13;
Italia  ai sensi dell'art. 11, commi 13 e 14, del decreto legislativo &#13;
n. 286  del  1998, pregiudicherebbero il suo diritto al lavoro, e, in &#13;
particolare,  il  suo diritto alla retribuzione e alla stabilità del &#13;
posto di lavoro; &#13;
        che  in  tutti  giudizi  è  intervenuto  il  Presidente  del &#13;
Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura &#13;
generale  dello  Stato,  e ha chiesto che la questione sia dichiarata &#13;
non fondata. &#13;
    Considerato che le ordinanze propongono la medesima questione e i &#13;
relativi   giudizi   possono   essere   riuniti   per  essere  decisi &#13;
congiuntamente; &#13;
        che  il  remittente,  nel sollevare questione di legittimità &#13;
costituzionale   dell'art.  13,  comma  2,  del  decreto  legislativo &#13;
25 luglio  1998,  n. 286,  nella  parte  in  cui  stabilisce  che  il &#13;
prefetto,  una  volta accertata l'esistenza dei presupposti di legge, &#13;
deve  necessariamente disporre l'espulsione dello straniero, pretende &#13;
che alla autorità amministrativa siano affidati poteri discrezionali &#13;
tali  da  non  costringerla  all'osservanza  di  prescrizioni  legali &#13;
strettamente vincolanti; &#13;
        che  quello  che  il  giudice  remittente  chiama automatismo &#13;
espulsivo  altro  non  è  che  un  riflesso del principio di stretta &#13;
legalità  che  permea  l'intera  disciplina  dell'immigrazione e che &#13;
costituisce  anche  per gli stranieri presidio ineliminabile dei loro &#13;
diritti,  consentendo  di  scongiurare  possibili  arbitri  da  parte &#13;
dell'autorità amministrativa; &#13;
        che  le ragioni umanitarie e solidaristiche che ad avviso del &#13;
remittente dovrebbero guidare la scelta dell'autorità amministrativa &#13;
non  sono  ignote al decreto legislativo n. 286 del 1998: questo, nel &#13;
prevedere,  all'art. 19,  svariate  ipotesi  di divieto di espulsione &#13;
dello  straniero,  soddisfa l'esigenza che siano tutelate particolari &#13;
"situazioni  personali"  senza  tuttavia  abdicare  al  principio  di &#13;
legalità,  il  quale  soltanto  può  assicurare  un ordinato flusso &#13;
migratorio (sentenza n. 353 del 1997); &#13;
        che,   pertanto,   la   questione   deve   essere  dichiarata &#13;
manifestamente infondata. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art.  13,  comma  2,  del  decreto &#13;
legislativo  25 luglio  1998,  n. 286 (Testo unico delle disposizioni &#13;
concernenti  la disciplina dell'immigrazione e norme sulla condizione &#13;
dello  straniero)  sollevata,  in riferimento agli articoli 2, 3 e 35 &#13;
della   Costituzione,  dal  Tribunale  di  Vicenza,  in  composizione &#13;
monocratica, con le ordinanze indicate in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 22 aprile 2002. &#13;
                       Il Presidente: Ruperto &#13;
                      Il redattore: Mezzanotte &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 3 maggio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
