<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>93</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:93</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Oronzo Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>29/03/1985</data_decisione>
    <data_deposito>01/04/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - &#13;
 Prof. LIVIO PALADIN - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO &#13;
 ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - Prof. &#13;
 GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO CORASANITI - Prof. &#13;
 GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt. 10 e  &#13;
 11  della  legge  2  aprile  1968,  n.  482  (Disciplina generale delle  &#13;
 assunzioni obbligatorie presso la pubblica amministrazione e le aziende  &#13;
 private), promossi con le seguenti ordinanze:                            &#13;
     1) ordinanza emessa il 9  giugno  1981  dal  pretore  di  Asti  nel  &#13;
 procedimento  penale  a  carico di Gallo Luigi ed altro, iscritta al n.  &#13;
 546 del registro ordinanze 1981 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n. 318 dell'anno 1981;                                  &#13;
     2)  ordinanza  emessa  il  12 gennaio 1982 dal pretore di Salò nel  &#13;
 procedimento civile vertente tra Levangi Virgilio e la S.p.a.  Ferriera  &#13;
 Valsabbia,  iscritta al n. 107 del registro ordinanze 1982 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 185 dell'anno 1982;         &#13;
     3) ordinanza emessa il 21  giugno  1982  dal  pretore  di  Borgo  a  &#13;
 Mozzano  nel procedimento penale a carico di Palagi Pietro, iscritta al  &#13;
 n.  569  del  registro  ordinanze  1982  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 25 dell'anno 1983.                         &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 ministri:                                                                &#13;
     udito nell'udienza pubblica del 15 gennaio 1985 il Giudice relatore  &#13;
 Oronzo Reale;                                                            &#13;
     udito l'Avvocato dello Stato Carlo Carbone per  il  Presidente  del  &#13;
 Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  tre  diverse ordinanze, i pretori di Asti (ordinanza emessa in  &#13;
 data 9 giugno 1981, n. 546 del reg. ord.  1981),  di  Salò  (ordinanza  &#13;
 emessa in data 12 gennaio 1982, n. 107 del reg. ord. 1982) e di Borgo a  &#13;
 Mozzano  (ordinanza emessa in data 21 giugno 1982, n. 569 del reg. ord.  &#13;
 1982)  hanno  sollevato  questione   di   legittimità   costituzionale  &#13;
 dell'art.  10  della  legge  2  aprile  1968,  n.  482,  che disciplina  &#13;
 l'obbligo di assunzione di lavoratori  invalidi  da  parte  di  aziende  &#13;
 private,  laddove  detta  norma  non  prevede la facoltà del datore di  &#13;
 lavoro   di   rifiutare   l'assunzione  di  lavoratore  precedentemente  &#13;
 licenziato per giusta causa, per preteso contrasto con gli artt. 3 e 41  &#13;
 della Costituzione.                                                      &#13;
     Devesi rilevare che la questione suddetta è  stata  sollevata  dai  &#13;
 pretori  di  Asti e di Borgo a Mozzano nel corso di procedimenti penali  &#13;
 che vedono imputati datori di lavoro rifiutatisi di assumere lavoratori  &#13;
 invalidi precedentemente licenziati per giusta causa, mentre il pretore  &#13;
 di Salò, nel corso  di  un  procedimento  di  lavoro  promosso  da  un  &#13;
 lavoratore  invalido  avviato all'assunzione obbligatoria e non assunto  &#13;
 siccome aveva in  precedenza  rassegnato  le  dimissioni  dalla  stessa  &#13;
 azienda per non incorrere in denuncia penale, ha sollevato questione di  &#13;
 costituzionalità  oltreché  dell'art.  10  anche  dell'art.  11 della  &#13;
 citata legge n. 482 del 1968 nella parte in  cui  le  dette  norme  non  &#13;
 prevedono  la  possibilità,  per  il  datore  di  lavoro, di rifiutare  &#13;
 l'assunzione  di  lavoratori  precedentemente  licenziati  o   comunque  &#13;
 dimessisi  per motivi riguardanti il rapporto fiduciario tra dipendente  &#13;
 e datore di lavoro.                                                      &#13;
     A sostegno della prospettata questione  si  assume  che  il  quinto  &#13;
 comma  dell'art.  15  della legge 29 aprile 1949, n. 264, espressamente  &#13;
 prevede, per ciò che attiene al collocamento ordinario, che il  datore  &#13;
 di  lavoro possa rifiutare di assumere lavoratori, avviati dall'ufficio  &#13;
 competente, i quali siano stati da lui licenziati per giusta causa.      &#13;
     Tale facoltà non è prevista in caso di collocamento obbligatorio;  &#13;
 la disparità di trattamento che ne consegue violerebbe l'art. 3  della  &#13;
 Costituzione,  in quanto la ratio delle disposizioni di cui all'art. 15  &#13;
 della legge n. 264 del 1949, appare pienamente operante anche nel  caso  &#13;
 di collocamento obbligatorio.                                            &#13;
     Una   siffatta  disciplina  violerebbe  altresì  l'art.  41  della  &#13;
 Costituzione,  in  quanto  inciderebbe  sull'organizzazione   economica  &#13;
 dell'impresa   imponendo   all'imprenditore   di  rivedere  la  propria  &#13;
 organizzazione produttiva per reinserire un  lavoratore  in  precedenza  &#13;
 licenziato per giusta causa.                                             &#13;
     La  rilevanza  della cennata questione è motivata in tutti e tre i  &#13;
 giudizi;  va  al  riguardo  rilevato  che  il  pretore  di  Salò  cita  &#13;
 giurisprudenza  secondo la quale le dimissioni del lavoratore originate  &#13;
 da timore di denuncia penale sono  assimilabili  al  licenziamento  per  &#13;
 giusta causa.                                                            &#13;
     È intervenuto in tutti e tre i giudizi il Presidente del Consiglio  &#13;
 dei  ministri,  rappresentato dall'Avvocatura generale dello Stato, per  &#13;
 chiedere che la questione sia  dichiarata  "non  rilevante  o  comunque  &#13;
 infondata".                                                              &#13;
     Assume  l'Avvocatura  nell'atto  di  intervento  che  il  dubbio di  &#13;
 costituzionalità avanzato dai tre giudici a quo sia frutto di  un  non  &#13;
 adeguato sforzo ermeneutico; in realtà, l'art. 15, quinto comma, della  &#13;
 legge  n.  264  del  1949  sarebbe  riferibile  anche  al  collocamento  &#13;
 obbligatorio, "in quanto enunciativo di un principio generale".          &#13;
     Se la distinzione fra  le  due  ipotesi  di  avviamento  al  lavoro  &#13;
 riguarda  la  modalità  con  cui  si  perviene  alla  costituzione del  &#13;
 rapporto, per il resto la disciplina generale del  rapporto  di  lavoro  &#13;
 rimane  immutata;  ne  consegue  che  non  potrebbe  dirsi  elusa dalla  &#13;
 disciplina peculiare del collocamento obbligatorio  la  necessità  che  &#13;
 l'inserimento  della  persona nell'organizzazione produttiva risponda a  &#13;
 quei naturali requisiti del rapporto di lavoro attinenti alla reciproca  &#13;
 fiducia e collaborazione tra le parti.                                   &#13;
     Un  dato  normativo  in  tal senso sarebbe ravvisabile nell'art. 10  &#13;
 della legge n. 482 del  1968,  laddove  chiarisce  che  il  trattamento  &#13;
 giuridico   e   normativo   del  rapporto  instauratosi  a  seguito  di  &#13;
 collocamento obbligatorio è identico a quello dell'ordinario  rapporto  &#13;
 di lavoro.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Le  ordinanze dei tre pretori di Asti, di Salò e di Borgo a  &#13;
 Mozzano indicate in epigrafe e riassunte in narrativa, ancorché emesse  &#13;
 due in sede di giudizio penale e una in sede di procedimento di lavoro,  &#13;
 sollevano la stessa questione. I relativi giudizi possono quindi essere  &#13;
 riuniti e decisi con unica sentenza.                                     &#13;
     2. - L'art. 15, comma quinto, della legge 29 aprile  1949,  n.  264  &#13;
 (Provvedimenti  in  materia di avviamento al lavoro e di assistenza dei  &#13;
 lavoratori disoccupati)  stabilisce  che  "il  datore  di  lavoro  può  &#13;
 rifiutare  di  assumere  lavoratori, avviati dall'ufficio competente, i  &#13;
 quali siano stati precedentemente da lui licenziati per giusta causa".   &#13;
     Una simile disposizione non  è  contenuta  né  nell'art.  11  né  &#13;
 nell'art.  10  della  legge  2 aprile 1968, n. 482 (Disciplina generale  &#13;
 delle assunzioni obbligatorie presso le pubbliche amministrazioni e  le  &#13;
 aziende  private)  che  regolano  rispettivamente l'obbligo dei privati  &#13;
 datori di lavoro  con  oltre  35  dipendenti  di  "assumere  lavoratori  &#13;
 appartenenti  alle  categorie  indicate  nel precedente titolo" (cioè:  &#13;
 invalidi di guerra  e  per  servizio,  invalidi  del  lavoro,  invalidi  &#13;
 civili, non vedenti, sordomuti, orfani e vedove di guerra, per servizio  &#13;
 e  per  lavoro), e l'applicazione, "a coloro che sono assunti al lavoro  &#13;
 in forza della presente  legge",  del  "normale  trattamento  economico  &#13;
 giuridico e normativo".                                                  &#13;
     I pretori di Asti e di Borgo a Mozzano (n. 546 del reg. ord. 1981 e  &#13;
 n.  569  del reg. ord. 1982), chiamati a giudicare in sede penale della  &#13;
 violazione dell'obbligo di  assunzione  di  due  lavoratori  già  loro  &#13;
 dipendenti  licenziati  per giusta causa, e il pretore di Salò (n. 107  &#13;
 del reg. ord. 1982), chiamato in sede civile a ordinare l'assunzione di  &#13;
 un lavoratore che in precedenza  si  era  dimesso  volontariamente  per  &#13;
 evitare  il  licenziamento  (situazione  che il pretore, in conformità  &#13;
 della  giurisprudenza  della   Cassazione,   reputa   assimilabile   al  &#13;
 licenziamento  per  giusta  causa),  ritengono  tutti  che  la  mancata  &#13;
 espressa previsione anche  nella  normativa  relativa  alle  assunzioni  &#13;
 obbligatorie   della   facoltà  del  datore  di  lavoro  di  rifiutare  &#13;
 l'assunzione di lavoratori precedentemente da lui licenziati per giusta  &#13;
 causa vada interpretata come negazione  di  tale  facoltà.    Dal  che  &#13;
 conseguirebbe per il pretore di Borgo a Mozzano la violazione dell'art.  &#13;
 3  della  Costituzione,  per i pretori di Asti e di Salò la violazione  &#13;
 degli artt.  3 e 41 della Costituzione.                                  &#13;
     3. - La questione non è fondata.                                    &#13;
     Come giustamente eccepisce l'Avvocatura dello Stato nel suo atto di  &#13;
 intervento,  è  errata  la  interpretazione  della  normativa  che  ne  &#13;
 costituisce il presupposto.                                              &#13;
     Infatti  è  vero  che  né  l'art.  10 della legge n. 482 del 1968  &#13;
 denunciato di incostituzionalità dai pretori di  Asti  e  di  Borgo  a  &#13;
 Mozzano,  né  l'art.  11,  insieme  col  10, denunciato dal pretore di  &#13;
 Salò, riproducono le disposizioni dell'art. 15 della legge  29  aprile  &#13;
 1949,  n.  264 che accorda al datore di lavoro la facoltà di rifiutare  &#13;
 l'assunzione   di   lavoratori   avviati    dall'Ufficio    competente,  &#13;
 precedentemente  dallo stesso datore licenziati per giusta causa, ma da  &#13;
 ciò  non  consegue che il legislatore della legge n.  482 abbia voluto  &#13;
 escludere tale facoltà,  perché  essa  era  scritta,  con  valore  di  &#13;
 principio  generale,  nella  legge  n.  264  che regola l'avviamento al  &#13;
 lavoro,  attraverso  gli  uffici  di   collocamento,   dei   lavoratori  &#13;
 involontariamente disoccupati.                                           &#13;
     Basti  considerare  che  anche  gli  aventi  diritto all'assunzione  &#13;
 obbligatoria di cui alla legge n. 482 appartengono alla  categoria  dei  &#13;
 disoccupati  involontari  che  vengono assunti attraverso gli uffici di  &#13;
 collocamento.                                                            &#13;
     Infatti tanto il datore di lavoro che deve assumere lavoratori  per  &#13;
 le  esigenze  della  sua  azienda,  a sua richiesta e nel numero da lui  &#13;
 determinato, tanto il datore di lavoro tenuto  ad  assumere  lavoratori  &#13;
 disoccupati delle categorie indicate nella legge n. 482, sono obbligati  &#13;
 ad  assumere  i lavoratori iscritti nelle rispettive liste dell'Ufficio  &#13;
 di collocamento (titolo II della legge n.   264 del  1949;  titolo  III  &#13;
 della  legge  n.  482 del 1968).  Quindi deve ritenersi che anche nelle  &#13;
 assunzioni obbligatorie valgano i principi  generali  del  collocamento  &#13;
 dei  lavoratori  disoccupati non incompatibili con i principi specifici  &#13;
 delle assunzioni obbligatorie.                                           &#13;
     Si può dunque pervenire alla conclusione (accolta espressamente in  &#13;
 qualche decisione di merito e che appare implicita nella giurisprudenza  &#13;
 delle Sezioni Unite della Cassazione che hanno ritenuto  valida,  anche  &#13;
 per  i  contratti di assunzione obbligatoria, l'inserzione del patto di  &#13;
 prova) che la disposizione del quinto comma dell'art. 15 della legge n.  &#13;
 264 sia fra  quelle  applicabili  anche  nell'ambito  delle  assunzioni  &#13;
 obbligatorie di cui alla legge n. 482 del 1968.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata, nei sensi di cui in motivazione, la questione  &#13;
 di legittimità costituzionale degli artt. 10 e 11 della legge 2 aprile  &#13;
 1968,  n. 482 (Disciplina generale delle assunzioni obbligatorie presso  &#13;
 la  pubblica  amministrazione  e  le  aziende  private)  sollevata,  in  &#13;
 riferimento agli artt.  3 e 41 della Costituzione, dai pretori di Asti,  &#13;
 Salò e Borgo a Mozzano con le ordinanze di cui in epigrafe (n. 516 del  &#13;
 reg. ord. 1981 e nn. 107 e 569 del reg. ord. 1982).                      &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 29 marzo 1985.                                &#13;
                                   F.to: LEOPOLDO ELIA - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
