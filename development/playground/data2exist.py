import json
import os
from time import sleep

from akomantoso.exist import exist_put
from tqdm import tqdm
from lxml import etree
from cortecostituzionale.settings import EXIST_DB


tqdm.monitor_interval = 0

prefix = "{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}"


success = set()


def etree_to_string(xml):
    return etree.tostring(xml, encoding="utf-8", method="xml", pretty_print=True).decode("utf-8")


def put_file(filepath):
    try:
        with open(filepath, "r") as f:
            akn = etree.fromstring(f.read())
        uri_el = akn.find(f"{prefix}judgment").find(f"{prefix}meta").find(f"{prefix}identification").\
            find(f"{prefix}FRBRManifestation").find(f"{prefix}FRBRuri")
        uri = uri_el.get("value")
        exist_put(uri, etree_to_string(akn), exist_db=EXIST_DB, silent=True)
        success.add(filepath)
    except KeyboardInterrupt:
        write_success()
        raise
    except Exception as e:
        tqdm.write(f"{e}")
        write_success()


def write_success():
    with open("success.json", "w") as f:
            f.write(json.dumps(list(success), ensure_ascii=False, indent=4))


def put_all_files():
    base_path = "/Users/biagio/repos/CIRSFID/cortecostituzionale-py/data/out/valid"
    global success
    with open("success.json", "r") as f:
        success = set(json.load(f))

    for filename in tqdm([fp for fp in os.listdir(base_path) if fp not in success]):
        filepath = os.path.join(base_path, filename)
        put_file(filepath)
        sleep(0.1)
    write_success()


if __name__ == '__main__':
    put_all_files()
