<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>412</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:412</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/10/1999</data_decisione>
    <data_deposito>29/10/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. Guido &#13;
 NEPPI MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 391 del codice    &#13;
 di procedura penale, promosso con ordinanza emessa il 7  luglio  1998    &#13;
 dal  giudice  per le indagini preliminari c/o il Tribunale di Perugia    &#13;
 nel procedimento penale a carico di M. C., iscritta  al  n.  867  del    &#13;
 registro  ordinanze  1998 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 49, prima serie speciale, dell'anno 1998.                  &#13;
   Udito nella camera di consiglio del 29 settembre  1999  il  giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto  che  il giudice per le indagini preliminari del Tribunale    &#13;
 di Perugia ha sollevato, in riferimento agli artt. 3, 24 e  97  della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art.  391    &#13;
 del  codice  di  procedura  penale nella parte in cui non prevede "la    &#13;
 necessaria presenza di un rappresentante  della  Polizia  giudiziaria    &#13;
 che   ha  partecipato  alle  operazioni  di  arresto  e  con  diretta    &#13;
 cognizione dei fatti o, comunque, non  consente  al  giudice  per  le    &#13;
 indagini   preliminari   procedente   di  chiedere  l'intervento  del    &#13;
 predetto, anche a chiarimento dei fatti";                                &#13;
     che  il  rimettente,  investito  della  richiesta  del   pubblico    &#13;
 ministero  di convalida dell'arresto e di applicazione della custodia    &#13;
 cautelare  in  carcere,  espone,  in  fatto,  che   nell'udienza   di    &#13;
 convalida,  relativa  a  due  soggetti arrestati in flagranza, uno di    &#13;
 essi aveva negato, in contrasto con quanto risultava nel  verbale  di    &#13;
 arresto,  di  avere  detenuto  un  quantitativo  di stupefacente e di    &#13;
 essersene disfatto nel momento in  cui  era  intervenuta  la  polizia    &#13;
 giudiziaria,  sostenendo  che  la  sostanza stupefacente era detenuta    &#13;
 dall'altro arrestato;                                                    &#13;
     che  quest'ultimo,  a  sua  volta  interrogato  nell'udienza   di    &#13;
 convalida,  aveva  confermato  tale versione, assumendosi l'esclusiva    &#13;
 responsabilità del fatto;                                               &#13;
     che, a parere del rimettente, in tale situazione il giudice della    &#13;
 convalida si  vedrebbe  costretto  a  decidere  sulle  richieste  del    &#13;
 pubblico  ministero  senza  avere  la possibilità di compiere alcuna    &#13;
 indagine  volta  a  verificare  quale  delle  contrastanti   versioni    &#13;
 emergenti  dal  verbale  di  arresto  e  dalle  dichiarazioni dei due    &#13;
 arrestati corrisponda alla realtà dei fatti;                            &#13;
     che,   al   riguardo,   il  rimettente  rileva  che  nell'analoga    &#13;
 situazione della convalida dell'arresto nel  procedimento  avanti  al    &#13;
 pretore,  l'art.  566  cod. proc. pen. ha opportunamente previsto che    &#13;
 l'ufficiale o agente di polizia giudiziaria che ha eseguito l'arresto    &#13;
 venga autorizzato a rendere una relazione orale, così consentendo al    &#13;
 pretore di chiedere chiarimenti sui fatti e di "poter  instaurare  un    &#13;
 minimo di contraddittorio su circostanze dubbie o incerte";              &#13;
     che  la  norma censurata contrasterebbe quindi con l'art. 3 della    &#13;
 Costituzione per violazione del principio di uguaglianza,  in  quanto    &#13;
 opera  una  irragionevole discriminazione tra arrestati a seconda che    &#13;
 si tratti di reati di competenza del tribunale  o  del  pretore;  con    &#13;
 l'art. 24 Cost., perché l'arrestato "si troverebbe nella sostanziale    &#13;
 impossibilità  di  controbattere  a  quanto  risulta  in  atti in un    &#13;
 momento determinante per la propria libertà personale";  con  l'art.    &#13;
 97 Cost., sotto il profilo del buon andamento degli uffici, in quanto    &#13;
 si preclude al giudice di "compiere quanto necessario per raggiungere    &#13;
 un  serio  e  maturato  convincimento"  e alla polizia giudiziaria di    &#13;
 fornire chiarimenti a fronte di contestazioni della difesa.              &#13;
   Considerato  che  nel  sistema  del  codice  di  procedura   penale    &#13;
 l'udienza  di convalida dell'arresto, disciplinata dall'art. 391 cod.    &#13;
 proc.   pen., è costruita come un  momento  di  necessaria  garanzia    &#13;
 sullo status libertatis volto esclusivamente a verificare, allo stato    &#13;
 degli  atti e nei tempi brevissimi imposti dall'art. 13, terzo comma,    &#13;
 Cost., le condizioni di  legittimità  dell'arresto  sulla  base  del    &#13;
 relativo  verbale, trasmesso dal pubblico ministero a norma dell'art.    &#13;
 122 disp.    att.  cod.  proc.  pen.  unitamente  alla  richiesta  di    &#13;
 convalida;                                                               &#13;
     che  a  tale  funzione è congeniale la struttura dell'udienza di    &#13;
 convalida, ove l'unica presenza necessaria è quella del difensore, a    &#13;
 garanzia del diritto di difesa dell'arrestato, di cui viene  disposto    &#13;
 l'interrogatorio,  salvo  che  non abbia potuto o si sia rifiutato di    &#13;
 comparire (art. 391, commi  1  e  3,  cod.  proc.  pen.),  mentre  il    &#13;
 pubblico ministero, se ritiene di non comparire, trasmette al giudice    &#13;
 le  richieste  in  ordine alla libertà personale con gli elementi su    &#13;
 cui le stesse si fondano (art. 390, comma  3-bis  cod.  proc.  pen.),    &#13;
 ovvero, se comparso, si limita ad indicare i motivi dell'arresto e ad    &#13;
 illustrare  le  eventuali richieste in ordine alla libertà personale    &#13;
 (art. 391, comma 3, cod.  proc. pen.);                                   &#13;
     che, a fronte di tale disciplina, il rimettente vorrebbe  che  al    &#13;
 giudice  dell'udienza  di convalida venisse riconosciuto il potere di    &#13;
 sentire l'agente o l'ufficiale di polizia giudiziaria che ha eseguito    &#13;
 l'arresto  e  redatto   il   relativo   verbale,   eventualmente   in    &#13;
 contraddittorio  con la persona arrestata, al fine di procedere ad un    &#13;
 più preciso accertamento dei fatti  in  caso  di  contrasti  tra  il    &#13;
 verbale di arresto e le dichiarazioni rese dagli arrestati in sede di    &#13;
 interrogatorio;                                                          &#13;
     che   ad   avviso  del  rimettente  l'impossibilità  di  sentire    &#13;
 l'ufficiale o agente di polizia giudiziaria si porrebbe in  contrasto    &#13;
 con  l'art.   3 Cost., sia perché precluderebbe irragionevolmente al    &#13;
 giudice di compiere gli accertamenti necessari  alla  sua  decisione,    &#13;
 sia perché - confrontata con la disciplina dell'udienza di convalida    &#13;
 dell'arresto e del contestuale giudizio direttissimo nel procedimento    &#13;
 davanti  al  pretore,  prevista  dall'art.  566  cod.  proc.  pen.  -    &#13;
 determinerebbe una ingiustificata disparità di trattamento  tra  gli    &#13;
 arrestati per reati di competenza del tribunale e quelli per reati di    &#13;
 competenza  del  pretore,  in quanto solo nel procedimento davanti al    &#13;
 pretore la legge  prevede  che  l'ufficiale  o  l'agente  di  polizia    &#13;
 giudiziaria  che  presenta  l'arrestato  al pretore sia autorizzato a    &#13;
 rendere una relazione orale (art. 566, comma 3, cod. proc. pen.);        &#13;
     che, da  un  punto  di  vista  generale,  non  contrasta  con  il    &#13;
 principio  di  ragionevolezza,  stante  la  funzione  e  la struttura    &#13;
 dell'udienza di convalida, che l'art. 391 cod. proc. pen. non preveda    &#13;
 che il giudice possa acquisire nuovi elementi di  valutazione  e,  in    &#13;
 particolare,  sentire  come  testimoni gli agenti o gli ufficiali che    &#13;
 hanno eseguito l'arresto, in quanto tali incombenti si porrebbero  in    &#13;
 radicale  contrasto con i tempi brevissimi entro cui deve svolgersi e    &#13;
 concludersi l'udienza di convalida e con  le  finalità  dell'udienza    &#13;
 stessa;                                                                  &#13;
     che,  in  particolare, non è conferente il tertium comparationis    &#13;
 addotto dal rimettente a sostegno della supposta violazione dell'art.    &#13;
 3 Cost.: l'art. 566 cod.  proc.  pen.  si  riferisce,  infatti,  alla    &#13;
 diversa   situazione  della  convalida  e  del  contestuale  giudizio    &#13;
 direttissimo nel procedimento  davanti  al  pretore,  e  pertanto  il    &#13;
 confronto  potrebbe  in ipotesi porsi tra gli omologhi istituti della    &#13;
 convalida e del  contestuale  giudizio  direttissimo  rispettivamente    &#13;
 previsti  per il procedimento davanti al pretore (art. 566 cod. proc.    &#13;
 pen.) e per i reati di competenza del tribunale (art. 449 cod.  proc.    &#13;
 pen.);                                                                   &#13;
     che,  peraltro,  la relazione orale di cui all'art. 566, comma 3,    &#13;
 cod. proc. pen. è priva  di  quelle  connotazioni  testimoniali  che    &#13;
 vorrebbe  attribuirle  il rimettente, ma risponde piuttosto, stante i    &#13;
 tempi brevissimi per la presentazione dell'arrestato direttamente  in    &#13;
 udienza   ai  fini  della  celebrazione  del  giudizio  direttissimo,    &#13;
 all'esigenza  di  consentire  agli  ufficiali  o  agenti  di  polizia    &#13;
 giudiziaria  che hanno eseguito l'arresto di surrogare o integrare la    &#13;
 comunicazione scritta della notizia di reato prevista dall'art.  347,    &#13;
 comma 1, cod.  proc. pen;                                                &#13;
     che,  quindi,  la  relazione  orale,  che  il rimettente vorrebbe    &#13;
 introdurre all'interno dell'udienza di  convalida,  non  assolverebbe    &#13;
 comunque  alla  funzione  di  consentire  al  giudice  di  sentire  a    &#13;
 chiarimento  gli  agenti  verbalizzanti   onde   dirimere   eventuali    &#13;
 contrasti   tra   il   verbale   di  arresto  e  il  contenuto  delle    &#13;
 dichiarazioni rese dagli arrestati;                                      &#13;
     che infine, ove si attribuisse al  giudice,  al  di  fuori  della    &#13;
 specifica  e diversa ipotesi della relazione orale avanti al pretore,    &#13;
 la facoltà di citare d'ufficio e di esaminare gli ufficiali o agenti    &#13;
 di polizia giudiziaria che hanno eseguito l'arresto,  tale  attività    &#13;
 si  qualificherebbe  come una acquisizione di carattere testimoniale,    &#13;
 dalla quale  deriverebbe,  a  titolo  di  necessaria  attuazione  del    &#13;
 principio  del diritto alla prova, la conseguente ammissione di altri    &#13;
 testimoni eventualmente  indicati  dal  pubblico  ministero  e  dalla    &#13;
 difesa,  snaturando  così la struttura e la funzione dell'udienza di    &#13;
 convalida, sino all'impossibilità di concluderla  nel  rispetto  dei    &#13;
 termini   imposti   dalla   stessa   Costituzione   a   tutela  della    &#13;
 inviolabilità della libertà personale;                                 &#13;
     che  prive  di  fondamento  sono  anche  le  censure  mosse   con    &#13;
 riferimento agli artt. 24 e 97 Cost;                                     &#13;
     che,  ai  fini  della  contestazione  nel  corso  dell'udienza di    &#13;
 convalida delle risultanze  emergenti  dal  verbale  di  arresto,  il    &#13;
 diritto  di  difesa risulta sufficientemente garantito dalla presenza    &#13;
 necessaria del difensore e dall'interrogatorio dell'arrestato;           &#13;
     che   comunque,   in   caso   di    effettiva    lacunosità    o    &#13;
 contraddittorietà degli elementi sottoposti alla sua valutazione, il    &#13;
 giudice,   in  omaggio  al  principio  del  favor  libertatis  dovrà    &#13;
 disattendere la richiesta del pubblico  ministero,  non  convalidando    &#13;
 l'arresto;                                                               &#13;
     che, in ordine all'art. 97 Cost., il principio del buon andamento    &#13;
 dei pubblici uffici non si riferisce all'attività giurisdizionale in    &#13;
 senso   stretto,   bensì   all'organizzazione   e  al  funzionamento    &#13;
 dell'amministrazione della giustizia (v. da ultimo  sentenze  n.  381    &#13;
 del 1999 e n. 53 del 1998);                                              &#13;
     che  pertanto  la questione deve essere dichiarata manifestamente    &#13;
 infondata sotto tutti i profili  prospettati.                            &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  391  del  codice  di   procedura   penale,    &#13;
 sollevata,  in  riferimento agli artt. 3, 24 e 97 della Costituzione,    &#13;
 dal giudice per le indagini preliminari del Tribunale di Perugia, con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 25 ottobre 1999.                              &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 29 ottobre 1999.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
