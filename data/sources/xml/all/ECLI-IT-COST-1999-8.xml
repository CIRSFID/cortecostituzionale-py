<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>8</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:8</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/01/1999</data_decisione>
    <data_deposito>21/01/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, &#13;
 prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo &#13;
 MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. &#13;
 Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 39 del decreto    &#13;
 legislativo 31 dicembre  1992,  n.  546  (Disposizioni  sul  processo    &#13;
 tributario  in attuazione della delega al Governo contenuta nell'art.    &#13;
 30 della legge 30 dicembre 1991,  n.  413),  promosso  con  ordinanza    &#13;
 emessa  il  9 dicembre 1997 dalla Commissione tributaria regionale di    &#13;
 Bari sul ricorso proposto dall'ufficio IVA di Bari contro  Zeta  Emme    &#13;
 s.r.l.,  iscritta  al n. 183 del registro ordinanze 1998 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  13,  prima   serie    &#13;
 speciale, dell'anno 1998.                                                &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del 30 settembre  1998  il  giudice    &#13;
 relatore Annibale Marini.                                                &#13;
   Ritenuto  che  la  commissione  tributaria  regionale  di Bari, con    &#13;
 ordinanza del 9 dicembre 1997 ha sollevato, in  riferimento  all'art.    &#13;
 76  della  Costituzione,  questione  di  legittimità  costituzionale    &#13;
 dell'art.   39 del decreto  legislativo  31  dicembre  1992,  n.  546    &#13;
 (Disposizioni  sul  processo tributario in attuazione della delega al    &#13;
 Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413),    &#13;
 per violazione dei principi e criteri direttivi fissati nell'art. 30,    &#13;
 comma 1, lettera g), della legge delega  30  dicembre  1991,  n.  413    &#13;
 (Disposizioni  per  ampliare  le basi imponibili, per razionalizzare,    &#13;
 facilitare e potenziare l'attività di accertamento; disposizioni per    &#13;
 la  rivalutazione  obbligatoria  dei  beni  immobili  delle  imprese,    &#13;
 nonché  per  riformare il contenzioso e per la definizione agevolata    &#13;
 dei  rapporti  tributari  pendenti;  delega   al   Presidente   della    &#13;
 Repubblica  per  la  concessione  di  amnistia  per  reati tributari;    &#13;
 istituzioni dei centri di assistenza fiscale e del conto fiscale);       &#13;
     che,  a   parere   della   rimettente,   la   norma   denunciata,    &#13;
 nell'escludere  dalle  ipotesi di sospensione necessaria del processo    &#13;
 tributario quella in cui  la  decisione  della  causa  dipenda  dalla    &#13;
 definizione  di  altra controversia pendente dinanzi allo stesso o ad    &#13;
 altro giudice, violerebbe il criterio direttivo di cui al citato art.    &#13;
 30, comma 1, lettera g), della legge n. 413 del  1991,  che  sancisce    &#13;
 l'adeguamento  delle  norme  del  processo  tributario  a  quelle del    &#13;
 processo   civile   nel   quale   la   sospensione   necessaria   per    &#13;
 pregiudizialità  sarebbe invece prevista dall'art. 295 del codice di    &#13;
 procedura civile;                                                        &#13;
     che nel giudizio è intervenuto il Presidente del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, chiedendo che venga dichiarata l'infondatezza della  questione    &#13;
 prospettata.                                                             &#13;
   Considerato  che,  come  più  volte  affermato da questa Corte, la    &#13;
 determinazione  dei  "principi  e   criteri   direttivi",   richiesti    &#13;
 dall'art.      76  della  Costituzione  per  una  valida  delegazione    &#13;
 legislativa, non può eliminare ogni margine di scelta nell'esercizio    &#13;
 della delega (sentenze n. 198 del 1998, n. 362 del 1995, n.  158  del    &#13;
 1985, n. 56 del 1971, ordinanze n. 21 del 1988, n. 321 del 1987,);       &#13;
     che  i  principi  e  criteri  direttivi se servono, da un lato, a    &#13;
 circoscrivere il campo della delega, sì da evitare  che  essa  venga    &#13;
 esercitata   in   modo   divergente   dalle   finalità  che  l'hanno    &#13;
 determinata, dall'altro, devono  consentire  al  potere  delegato  la    &#13;
 possibilità  di  valutare  le  particolari  situazioni giuridiche da    &#13;
 regolamentare nella fisiologica attività di "riempimento" che lega i    &#13;
 due livelli normativi (sentenze n. 198 del 1998, n. 362 del 1995,  n.    &#13;
 158  del  1985,  n. 56 del 1971, ordinanze n. 21 del 1988, n. 321 del    &#13;
 1987);                                                                   &#13;
     che il criterio direttivo  di  carattere  generale,  dettato  dal    &#13;
 legislatore  delegante  nell'art.  30, comma 1, lettera g), è quello    &#13;
 dell'adeguamento, e non dell'uniformità, delle  norme  del  processo    &#13;
 tributario a quelle del processo civile;                                 &#13;
     che  ulteriore  criterio  direttivo,  di  carattere specifico, è    &#13;
 quello della sollecita definizione del processo  tributario  previsto    &#13;
 dal  citato  art. 30, comma 1, lettera g), numero 3, per l'ipotesi di    &#13;
 sospensione, interruzione ed estinzione del processo;                    &#13;
     che,  con  riferimento  alla  disciplina  della  sospensione  nel    &#13;
 processo  tributario,  questa Corte ha affermato che "il legislatore,    &#13;
 limitando i casi di sospensione del processo, ha inteso rendere  più    &#13;
 rapida  e  agevole  la definizione del processo tributario oberato di    &#13;
 una rilevante mole di contenzioso", sicché le commissioni tributarie    &#13;
 possono decidere, incidenter tantum ogni questione pregiudiziale alle    &#13;
 controversie ad esse devolute (sentenza n. 31 del 1998);                 &#13;
     che, con riferimento alla sospensione  per  pregiudizialità  nel    &#13;
 processo  civile  disciplinata  dall'art. 295 del codice di procedura    &#13;
 civile, questa Corte ha rilevato che la recente riforma di tale norma    &#13;
 "nell'attenuare il nesso di pregiudizialità penale in consonanza con    &#13;
 l'autonomia voluta dal nuovo codice di procedura penale per le azioni    &#13;
 civili restitutorie e risarcitorie, ha espresso, più in generale, il    &#13;
 disfavore nei confronti  del  fenomeno  sospensivo  in  quanto  tale"    &#13;
 (sentenza n. 182 del 1996);                                              &#13;
     che,   alla  luce  delle  suesposte  considerazioni,  la  mancata    &#13;
 previsione - nella norma denunciata - della  ipotesi  di  sospensione    &#13;
 necessaria  di  cui all'art. 295 cod. proc. civ. non vi'ola i criteri    &#13;
 direttivi della legge delega n. 413 del 1991  e,  dunque,  l'art.  76    &#13;
 della Costituzione;                                                      &#13;
     che, pertanto, la questione deve essere dichiarata manifestamente    &#13;
 infondata.                                                               &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 39 del decreto legislativo 31 dicembre 1992,    &#13;
 n. 546 (Disposizioni sul  processo  tributario  in  attuazione  della    &#13;
 delega  al  Governo  contenuta  nell'art.  30 della legge 30 dicembre    &#13;
 1991,  n.  413)  sollevata,  in   riferimento   all'art.   76   della    &#13;
 Costituzione,  dalla  commissione  tributaria  regionale  di Bari con    &#13;
 l'ordinanza in epigrafe.                                                 &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 gennaio 1999.                              &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Marini                             &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 21 gennaio 1999.                          &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
