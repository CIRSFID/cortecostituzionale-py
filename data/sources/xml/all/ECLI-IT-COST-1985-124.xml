<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>124</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:124</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Giovanni Conso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/04/1985</data_decisione>
    <data_deposito>26/04/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Prof. ANTONIO &#13;
 LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO &#13;
 CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  14  della  &#13;
 legge  11  febbraio 1971, n. 11 (Nuova disciplina dell'affitto di fondi  &#13;
 rustici), promosso con ordinanza emessa il 20 ottobre 1976 dal TAR  del  &#13;
 Piemonte   sul   ricorso   proposto   da   Luigi  di  Rovasenda  contro  &#13;
 l'Ispettorato Provinciale Agrario di Vercelli ed altri, iscritta al  n.  &#13;
 31  del  registro  ordinanze 1977 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 59 dell'anno 1977.                                   &#13;
     Visto l'atto di costituzione di Montorio Mario e  Remigio,  nonché  &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
     udito  nell'udienza  pubblica  del  12  dicembre  1984  il  Giudice  &#13;
 relatore Giovanni Conso;                                                 &#13;
     uditi l'avvocato Emilio Romagnoli per Montorio Mario  e  Remigio  e  &#13;
 l'avvocato  dello Stato Paolo Cosentino per il Presidente del Consiglio  &#13;
 dei ministri.                                                            &#13;
     Ritenuto che il Tribunale amministrativo regionale del  Piemonte  -  &#13;
 nel  corso del giudizio promosso da Luigi di Rovasenda, proprietario di  &#13;
 un fondo concesso in  affitto,  avverso  il  silenzio  dell'Ispettorato  &#13;
 Provinciale  Agrario  di  Vercelli  sul  ricorso  avanzato dal suddetto  &#13;
 proprietario contro l'atto con cui gli affittuari  coltivatori  diretti  &#13;
 Mario  Montorio  e  Remigio  Montorio  gli  avevano  comunicato il loro  &#13;
 intento di apportare migliorie al fondo -  ha,  con  ordinanza  del  20  &#13;
 ottobre  1976,  denunciato, in riferimento "agli artt. 24 e 113, 3 e 97  &#13;
 della Costituzione",  l'illegittimità  dell'art.  14  della  legge  11  &#13;
 febbraio  1971, n. 11 (Nuova disciplina dell'affitto di fondi rustici),  &#13;
 "nella parte in cui equipara a  rigetto  per  decorso  del  termine  di  &#13;
 novanta  giorni  l'inerzia  dell'Ispettorato  Provinciale  Agrario  sul  &#13;
 ricorso del proprietario del fondo dato in affitto avverso la  proposta  &#13;
 di migliorie dell'affittuario coltivatore diretto";                      &#13;
     che nel presente giudizio si sono costituite le parti private Mario  &#13;
 Montorio e Remigio Montorio, rappresentate dall'avv.  Emilio Romagnoli,  &#13;
 le  quali  nella  comparsa  di  costituzione hanno chiesto che la Corte  &#13;
 dichiari "manifestamente infondata" la  proposta  questione,  ed  hanno  &#13;
 ribadito  tale  richiesta  nella  memoria  depositata il 2 maggio 1984,  &#13;
 deducendo che, nonostante le questioni processuali  siano  ormai  state  &#13;
 superate a seguito dell'entrata in vigore della legge 3 maggio 1982, n.  &#13;
 203,  la  nuova  disciplina  lascia, purtuttavia, "integra la rilevanza  &#13;
 della questione giacché, ove questa venisse dichiarata non fondata, il  &#13;
 Tribunale amministrativo regionale e, in caso di appello, il  Consiglio  &#13;
 di  Stato dovranno decidere sul silenzio-diniego in base alla legge che  &#13;
 lo configurò e che vigeva quando esso ebbe a formarsi";                 &#13;
     che è  intervenuta  la  Presidenza  del  Consiglio  dei  ministri,  &#13;
 rappresentata  e difesa dall'Avvocatura Generale dello Stato, chiedendo  &#13;
 che la questione sia dichiarata non fondata;                             &#13;
     considerato che l'ordinanza di rimessione, muovendo dal presupposto  &#13;
 che la norma impugnata conferiva  all'inerzia  dell'amministrazione  un  &#13;
 significato  legale tipico (quello, cioè, di autorizzare l'affittuario  &#13;
 ad eseguire l'attività voluta), sottolinea come una tale fictio  iuris  &#13;
 finisse per rendere solo apparente la tutela giurisdizionale, stante la  &#13;
 sostanziale  impossibilità  di sindacare in sede di impugnativa l'atto  &#13;
 fittizio fondato su una tanto lata discrezionalità;                     &#13;
     che, dopo la pronuncia dell'ordinanza di rimessione, è entrata  in  &#13;
 vigore  la  legge  n. 203 del 1982 (Norme sui contratti agrari), il cui  &#13;
 art. 16 - anche sulla base delle osservazioni svolte  da  questa  Corte  &#13;
 nella  sentenza  n.  153  del  1977, pur essa successiva alla pronuncia  &#13;
 dell'ordinanza di rimessione - ha innovato la procedura di abilitazione  &#13;
 alla esecuzione dei miglioramenti fondiari,  prevedendo,  fra  l'altro,  &#13;
 l'intervento  obbligatorio  dell'Ispettorato  Provinciale  Agrario,  il  &#13;
 quale, ove non si pervenga ad un "accordo in ordine alla proposta e  ai  &#13;
 connessi   regolamenti   dei  rapporti  tra  le  parti",  è  tenuto  a  &#13;
 pronunciarsi  entro  60  giorni  (con  "decisione"  che   deve   essere  &#13;
 comunicata  ad  entrambe  le  parti)  "motivando, in senso favorevole o  &#13;
 contrario, in ordine alle opere richieste", così da modellare  secondo  &#13;
 lo   schema   del   silenzio-rifiuto   l'inadempimento  della  pubblica  &#13;
 amministrazione all'obbligo di provvedere;                               &#13;
     che  l'art.  53  della  stessa  legge  n.  203  del  1982   estende  &#13;
 l'applicazione delle nuove disposizioni "a tutti i rapporti comunque in  &#13;
 corso anche se oggetto di controversie che non siano state definite con  &#13;
 sentenza   passato   in  giudicato  salvo  che  la  sentenza  sia  già  &#13;
 esecutiva";                                                              &#13;
     considerato, peraltro, che la nuova disciplina in ordine all'omessa  &#13;
 pronuncia dell'autorità amministrativa, ove risultasse applicabile  al  &#13;
 processo  a  quo,  farebbe  venir  meno  la  rilevanza  della  proposta  &#13;
 questione, richiedendosi, secondo il regime attualmente in vigore,  una  &#13;
 pronuncia amministrativa espressa, da emanarsi, oltre tutto, sulla base  &#13;
 di presupposti diversi da quelli prescritti dalla norma impugnata;       &#13;
     che, essendo il giudice a quo il solo legittimato a stabilire quale  &#13;
 normativa   debba   regolare   il  caso  di  specie,  è  al  Tribunale  &#13;
 amministrativo del Piemonte che spetta di decidere se l'art.  16  della  &#13;
 legge  n.  203  del  1982  possa far considerare tuttora applicabile la  &#13;
 norma denunciata (v., per analoghe pronunce di questa Corte, successive  &#13;
 all'entrata in vigore della  legge  n.  203  del  1982,  sia  pure  con  &#13;
 riguardo a norme di natura diversa da quella in esame, le ordinanze nn.  &#13;
 251, 249, 211 e 155 del 1982).</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione  degli  atti  al  Tribunale amministrativo  &#13;
 regionale del Piemonte.                                                  &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 23 aprile 1985.                               &#13;
     F.  to:  LEOPOLDO  ELIA  -  GUGLIELMO  ROEHRSSEN  -  ORONZO REALE -  &#13;
 BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  MALAGUGINI  -  LIVIO  PALADIN  -  &#13;
 ANTONIO  LA  PERGOLA - VIRGILIO ANDRIOLI - GIUSEPPE FERRARI - FRANCESCO  &#13;
 SAJA - GIOVANNI CONSO - ETTORE  GALLO  -  ALDO  CORASANITI  -  GIUSEPPE  &#13;
 BORZELLINO - FRANCESCO GRECO.                                            &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
