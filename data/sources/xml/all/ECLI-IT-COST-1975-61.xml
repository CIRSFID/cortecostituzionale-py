<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>61</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:61</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giulio Gionfrida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/03/1975</data_decisione>
    <data_deposito>12/03/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di  legittimità  costituzionale  dell'art.  40  &#13;
 della  legge  6  dicembre  1971,  n.  1034  (Istituzione  dei tribunali  &#13;
 amministrativi regionali), promossi con ordinanze emesse l'11 e  il  26  &#13;
 aprile  1974  dal  tribunale  amministrativo  regionale per la Sicilia,  &#13;
 rispettivamente sui ricorsi di  Aiazzi  Clelia  contro  la  Commissione  &#13;
 provinciale  di  controllo  di  Agrigento  e  di Terrani Santi ed altri  &#13;
 contro la Commissione provinciale di controllo di Messina, iscritte  ai  &#13;
 nn.  261  e 338 del registro ordinanze 1974 e pubblicate nella Gazzetta  &#13;
 Ufficiale della Repubblica n. 231 del 4 settembre 1974 e n. 265 del  10  &#13;
 ottobre 1974.                                                            &#13;
     Visti  gli  atti  d'intervento  del  Presidente  del  Consiglio dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 22 gennaio 1975 il Giudice relatore  &#13;
 Giulio Gionfrida;                                                        &#13;
     udito  il sostituto avvocato generale dello Stato Michele Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     A  seguito  di  ricorso  proposto  da  Clelia  Aiazzi  avverso   un  &#13;
 provvedimento  della Commissione provinciale di controllo di Agrigento,  &#13;
 l'adito T.A.R.  della  Sicilia  -  ritenuto  che  l'atto  nella  specie  &#13;
 impugnato   rientrava  nella  competenza  del  Consiglio  di  giustizia  &#13;
 amministrativa per la Regione siciliana, ex art. 5 del  d.l.  6  maggio  &#13;
 1948,  n.  654, in quanto "provvedimento definitivo di autorità avente  &#13;
 sede nella Regione" - con ordinanza 11 aprile 1974, in accoglimento  di  &#13;
 eccezione  della ricorrente, ha sollevato questione di legittimità, in  &#13;
 riferimento agli art. 125, comma secondo, 3,  comma  primo,  24,  commi  &#13;
 primo  e  secondo, e 113, comma primo, della Costituzione, dell'art. 40  &#13;
 della legge 6 dicembre 1971, n. 1034,  nella  parte,  appunto,  in  cui  &#13;
 limita la competenza del T.A.R. per la Regione siciliana.                &#13;
     Identica  questione  di  costituzionalità dell'art. 40 della legge  &#13;
 1971 n. 1034 citata è stata riproposta dal T.A.R. per la Sicilia,  con  &#13;
 successiva  ordinanza  26  aprile  1974,  emessa  sul  ricorso di Santi  &#13;
 Terrani ed altri contro atto della Commissione provinciale di controllo  &#13;
 di Messina.                                                              &#13;
     Innanzi a questa Corte, è intervenuto in  entrambi  i  giudizi  il  &#13;
 Presidente  del  Consiglio dei ministri, per il tramite dell'Avvocatura  &#13;
 dello  Stato,  che  ha  concluso  nel  senso  di  una  declaratoria  di  &#13;
 infondatezza della sollevata questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con  la  legge  6  dicembre  1971,  n.  1034, istitutiva dei  &#13;
 tribunali amministrativi regionali, il legislatore  ha  sopperito  alla  &#13;
 situazione    determinatasi    a   seguito   della   dichiarazione   di  &#13;
 incostituzionalità della G.P.A. e dei Consigli comunali e  provinciali  &#13;
 quali  organi  del  contenzioso elettorale, e, in pari tempo, ha inteso  &#13;
 dare  attuazione  al  precetto  dell'art.  125,  comma  secondo,  della  &#13;
 Costituzione,  il  quale,  appunto, prevede che siano costituiti, nella  &#13;
 Regione, "organi di giustizia amministrativa di primo grado".            &#13;
     Per quanto, in particolare, riguarda la Sicilia, dispone  tuttavia,  &#13;
 l'art.  40  della  legge succitata che "fino a quando non si procederà  &#13;
 alla revisione dell'attuale sistema di giustizia  amministrativa  nella  &#13;
 Regione  siciliana  la  competenza del T.A.R.   istituito in Sicilia è  &#13;
 limitata alle materie indicate nell'art. 2, lett.   a,  e  nell'art.  6  &#13;
 della  presente  legge"  e,  cioè,  ai soli ricorsi già di competenza  &#13;
 delle G.P.A. ed a quelli relativi al contenzioso elettorale.             &#13;
     La legittimità di tale disposizione è  appunto  posta  in  dubbio  &#13;
 nelle ordinanze di rimessione.                                           &#13;
     2.  -  La  norma  violerebbe, innanzitutto, gli artt. 3 e 125 della  &#13;
 Costituzione,  giacché  verrebbe  a  creare   una   "disarmonia"   tra  &#13;
 l'ordinamento  della  giustizia  amministrativa in Sicilia e quello del  &#13;
 restante territorio nazionale.                                           &#13;
     In quest'ultimo, infatti, i T.A.R. sono  previsti  come  organi  di  &#13;
 giustizia   amministrativa   decentrati  con  competenza  di  carattere  &#13;
 generale - aventi, per di più, giurisdizione esclusiva in  materia  di  &#13;
 concessioni  di  beni  o servizi pubblici (ex art. 5 legge 1971 cit.) -  &#13;
 mentre in Sicilia la competenza del locale tribunale amministrativo  è  &#13;
 - come detto - limitata alle sole materie del contenzioso elettorale ed  &#13;
 a  quelle  già  di  competenza  delle  G.P.A.; poiché permane, per il  &#13;
 resto,  la  giurisdizione del Consiglio di giustizia amministrativa per  &#13;
 la Sicilia, istituito con d.l. 6 maggio 1948, n. 654.                    &#13;
     Per di  più,  relativamente  ai  "provvedimenti  definitivi  delle  &#13;
 amministrazioni  regionali  e  delle  altre autorità aventi sede nella  &#13;
 Regione" - per i quali non è ammesso l'appello  all'Adunanza  plenaria  &#13;
 del  Consiglio  di  Stato (previsto dall'art. 5, comma terzo, d.l. 1948  &#13;
 cit.   contro   le   sole   decisioni   su   ricorsi    avverso    atti  &#13;
 dell'Amministrazione  statale)  -  la competenza del C.G.A. è di unico  &#13;
 grado.  E  ciò  comporterebbe  una  ulteriore  ragione  di  violazione  &#13;
 dell'art. 125 della Costituzione, sotto il profilo del contrasto con il  &#13;
 principio di tutela del doppio grado.                                    &#13;
     Risulterebbero, inoltre - sempre secondo il giudice a quo - violati  &#13;
 gli  artt.  24  e  113  della Costituzione, in quanto, nelle materie di  &#13;
 competenza del Consiglio di giustizia  amministrativa  per  la  Regione  &#13;
 siciliana,   le   parti  sarebbero  tenute  ad  avvalersi  di  avvocati  &#13;
 cassazionisti, con aggravio di  spese  e  conseguente  limitazione  del  &#13;
 diritto di difesa.                                                       &#13;
     Infine,  altro  motivo  di violazione degli artt. 24 e 113, nonché  &#13;
 dell'art. 3 della Costituzione, sarebbe rappresentato  dal  fatto  che,  &#13;
 rispetto  alla giurisdizione del C.G.A., la definitività costituirebbe  &#13;
 tuttora  presupposto  per  l'impugnabilità  dell'atto  amministrativo,  &#13;
 laddove,  per  la  giurisdizione  dei  T.A.R.  ciò  non  sarebbe  più  &#13;
 richiesto ex art. 20 legge 1034 del 1971 citata.                         &#13;
     3. - Appare preliminare  la  questione  concernente  la  violazione  &#13;
 degli artt. 3 e 125 della Costituzione.                                  &#13;
     Senza   dubbio,   una  volta  che  il  legislatore  ha  deciso,  in  &#13;
 conformità del precetto dell'art.125  Cost.,  istituendo  i  tribunali  &#13;
 regionali  quali  organi di giurisdizione di primo grado, di attuare un  &#13;
 sistema di giustizia amministrativa che si articola in un doppio  grado  &#13;
 di   giurisdizione,   un  diverso  sistema  nell'ambito  della  Regione  &#13;
 siciliana non potrebbe ritenersi legittimo, a meno che ciò  non  fosse  &#13;
 reso necessario dal rispetto di altre norme di rango costituzionale.     &#13;
     Quest'ultima è, appunto, la tesi dell'Avvocatura. La quale ravvisa  &#13;
 l'ostacolo costituzionale anzidetto nell'art. 23 dello Statuto speciale  &#13;
 siciliano (approvato con legge costituzionale 1948, n. 5) - secondo cui  &#13;
 "gli  organi  giurisdizionali centrali avranno in Sicilia le rispettive  &#13;
 sezioni per gli affari concernenti la  Regione"  -  in  attuazione  del  &#13;
 quale  è  stato,  con  il  menzionato  d.l.  1948,  n.  654, creato il  &#13;
 Consiglio di giustizia amministrativa  per  la  Sicilia,  come  sezione  &#13;
 giurisdizionale del Consiglio di Stato.                                  &#13;
     La  conseguente  insopprimibilità  del C.G.A. ed immodificabilità  &#13;
 delle sue  competenze  ad  opera  del  legislatore  ordinario  avrebbe,  &#13;
 appunto,  posto  il  problema  della  coesistenza,  in Sicilia, di tale  &#13;
 organo con l'istituendo T.A.R. e necessitato la sua soluzione nel senso  &#13;
 di  attribuizione  al  T.A.R.  siciliano  delle  sole   competenze   in  &#13;
 precedenza attribuite ad organi diversi dal C.G.A.                       &#13;
     La  norma  denunziata,  sancendo  tale  soluzione, si sottrarrebbe,  &#13;
 perciò, ai formulati rilievi di incostituzionalità.                    &#13;
     4. - Siffatte argomentazioni non hanno consistenza.                  &#13;
     La  istituzione  del  T.A.R.   in   Sicilia   con   la   competenza  &#13;
 generalizzata  propria  degli altri T.A.R. non contrasta con lo Statuto  &#13;
 siciliano, le cui norme aventi carattere speciale  prevarrebbero,  alla  &#13;
 stregua  dell'art.  116 Cost., in caso di contrasto, sulle disposizioni  &#13;
 contenute nel titolo V della stessa Costituzione.                        &#13;
     L'art.  23  dello  Statuto  attiene soltanto al decentramento degli  &#13;
 organi giurisdizionali centrali per gli affari concernenti la  Regione,  &#13;
 al   che   non   contrasta   la  istituzione  di  organi  di  giustizia  &#13;
 amministrativa di primo grado con competenza generalizzata.              &#13;
     Né vale  rilevare  che,  se  al  T.A.R.  della  Sicilia  si  fosse  &#13;
 attribuita  una sfera di competenza corrispondente a quella degli altri  &#13;
 tribunali  amministrativi  regionali,   ciò   avrebbe   inciso   sulla  &#13;
 competenza del Consiglio di giustizia amministrativa quale prevista dal  &#13;
 d.l.   6   maggio   1948,   n.  654,  in  quanto,  riguardo  agli  atti  &#13;
 amministrativi  contemplati   nell'art.   6   di   tale   provvedimento  &#13;
 legislativo,   il  C.G.A.  da  giudice  di  primo  od  unico  grado  si  &#13;
 trasformerebbe in giudice di seconda o ultima istanza.                   &#13;
     A parte che il predetto decreto  legislativo  ha  valore  di  legge  &#13;
 ordinaria, basta considerare che l'art. 5, comma primo, nel determinare  &#13;
 la  suaccennata competenza del Consiglio di giustizia amministrativa si  &#13;
 riferisce  a  quelle  stesse  "attribuzioni  devolute  dalla  legge  al  &#13;
 Consiglio  di  Stato"  ed  è pienamente rispondente allo spirito della  &#13;
 norma, anche in correlazione al disposto  dell'art.  23  dello  Statuto  &#13;
 siciliano  (il quale è appunto richiamato nell'art. 1 cpv. del decreto  &#13;
 legislativo), che, nei limiti in cui si verifichi la trasformazione del  &#13;
 Consiglio  di  Stato  in  giudice  di  appello,  questa   si   rifletta  &#13;
 corrispondentemente   sulla   competenza  del  Consiglio  di  giustizia  &#13;
 amministrativa, così come per altro previsto nel secondo  comma  dello  &#13;
 stesso  art.  5, con riguardo alle funzioni in grado di appello avverso  &#13;
 le decisioni delle Giunte provinciali amministrative "o degli organi di  &#13;
 giustizia amministrativa di primo grado che  eventualmente  saranno  ad  &#13;
 esse sostituite".                                                        &#13;
     La questione è pertanto fondata.                                    &#13;
     5.  -  La dichiarazione di parziale illegittimità, nei sensi sopra  &#13;
 esposti, della norma denunciata vale di per sé ad eliminare  anche  le  &#13;
 disarmonie  prospettate  dalle  ordinanze di rimessione sotto gli altri  &#13;
 profili concernenti la giurisdizione in materia di concessioni di  beni  &#13;
 o  servizi pubblici, la difesa tecnica delle parti e la definitività o  &#13;
 meno   dell'atto   amministrativo   quale   presupposto    della    sua  &#13;
 impugnabilità: profili, il cui esame resta, pertanto, assorbito.        &#13;
     È  ovvio che questa pronuncia della Corte, mentre per il resto non  &#13;
 tocca  la  disciplina   dettata   per   il   Consiglio   di   giustizia  &#13;
 amministrativa  dal  d.l. 6 maggio 1948, n. 654, deve intendersi di per  &#13;
 sé sufficiente a restringere la portata dell'ultimo inciso del secondo  &#13;
 comma dell'art. 40 della legge n. 1034 del 1971.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la illegittimità costituzionale dell'art. 40 della  legge  &#13;
 6  dicembre  1971,  n.  1034  (Istituzione dei tribunali amministrativi  &#13;
 regionali), nella parte in  cui  limita  la  competenza  del  tribunale  &#13;
 amministrativo regionale istituito nella Regione siciliana alle materie  &#13;
 indicate nell'art. 2, lett. a, e nell'art. 6 della legge medesima.       &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 5 marzo 1975.                                 &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIOVANNI  &#13;
                                   BATTISTA  BENEDETTI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE ROCCHETTI  -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI  - VEZIO CRISAFULLI - PAOLO  &#13;
                                   ROSSI -  LEONETTO  AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO.                               &#13;
                                   ARDUINO SALUSTRI Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
