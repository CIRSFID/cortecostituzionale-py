<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>36</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:36</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/02/1975</data_decisione>
    <data_deposito>25/02/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. &#13;
 MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di  legittimità  costituzionale  dell'art.  77  &#13;
 della legge 10 agosto 1950, n. 648, e dell'art. 75 della legge 18 marzo  &#13;
 1968,   n.  313  (Riordinamento  della  legislazione  pensionistica  di  &#13;
 guerra), promossi con le seguenti ordinanze:                             &#13;
     1) ordinanza emessa il 19 maggio  1972  dalla  Corte  dei  conti  -  &#13;
 sezione V pensioni di guerra - sul ricorso di Donati Bruno, iscritta al  &#13;
 n.  380  del  registro  ordinanze  1972  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 21 del 24 gennaio 1973;                    &#13;
     2) ordinanza emessa il 16 maggio  1973  dalla  Corte  dei  conti  -  &#13;
 sezione  III  pensioni  di  guerra  -  sul ricorso di Catanuto Ignazio,  &#13;
 iscritta al n. 278 del  registro  ordinanze  1973  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 223 del 29 agosto 1973.           &#13;
     Visto l'atto di costituzione di Catanuto Ignazio;                    &#13;
     udito  nell'udienza  pubblica  del  18  dicembre  1974  il  Giudice  &#13;
 relatore Giovanni Battista Benedetti;                                    &#13;
     udito l'avv. Nino Gaeta, per Catanuto Ignazio.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con decreto 15 marzo 1962 il Ministero  del  tesoro  negava  a  &#13;
 Donati  Bruno la pensione indiretta di guerra per la morte del fratello  &#13;
 Pietro, sul rilievo  che  il  richiedente,  maggiorenne,  era  divenuto  &#13;
 inabile  a qualsiasi proficuo lavoro in epoca successiva alla morte del  &#13;
 padre al quale era già stata attribuita la predetta pensione.           &#13;
     In sede di esame del ricorso proposto dall'interessato avverso tale  &#13;
 decreto  la Corte dei conti, sezione V giurisdizionale, ha sollevato di  &#13;
 ufficio la questione di  legittimità  costituzionale,  in  riferimento  &#13;
 all'art.  3  della  Costituzione,  degli artt. 77 della legge 10 agosto  &#13;
 1950, n.  648, e 75 della legge 18 marzo 1968,  n.  313,  limitatamente  &#13;
 alla  parte  in cui stabiliscono che la pensione indiretta di guerra è  &#13;
 concessa ai collaterali maggiorenni del militare deceduto per causa  di  &#13;
 servizio  di  guerra  o  del  civile  deceduto per atto di guerra, solo  &#13;
 quando la loro inabilità a qualsiasi  proficuo  lavoro  sussista  alla  &#13;
 data  di  morte  dei predetti oppure al momento in cui dovrebbe in loro  &#13;
 favore devolversi la pensione indiretta già liquidata al padre o  alla  &#13;
 madre.                                                                   &#13;
     Ad  avviso  del giudice a quo le norme impugnate, col richiedere la  &#13;
 sussistenza del requisito di inabilità ad una certa data,  creerebbero  &#13;
 una  disparità di trattamento dei collaterali rispetto ai genitori per  &#13;
 i quali, invece, l'inabilità è riferibile a qualsiasi momento  (artt.  &#13;
 71 legge n. 648 del 1950 e 64 legge n. 313 del 1968). E tale disparità  &#13;
 di  trattamento  sarebbe ancor più evidente ed ingiustificata rispetto  &#13;
 agli assimilati (allevatori, matrigna e patrigno) che sono soggetti  di  &#13;
 diritto pensionistico estranei alla famiglia.                            &#13;
     Nel giudizio dinanzi a questa Corte nessuno si è costituito.        &#13;
     2. - Questione in tutto identica a quella testé enunciata è stata  &#13;
 sollevata  dalla  III sezione giurisdizionale della Corte dei conti con  &#13;
 ordinanza 16 maggio 1973 emessa in sede di esame del  ricorso  proposto  &#13;
 da  Catanuto  Ignazio avverso il decreto con cui il Ministro del tesoro  &#13;
 respingeva la sua domanda intesa ad ottenere la pensione indiretta  per  &#13;
 la  morte  in  guerra del fratello Pietro, sul rilievo che l'inabilità  &#13;
 allegata dal richiedente non sussisteva alla data di morte del genitore  &#13;
 al quale era stata in precedenza già devoluta la pensione suddetta.     &#13;
     Si  osserva  nell'ordinanza  che  nel  corrispondere  la   pensione  &#13;
 indiretta  agli aventi diritto, secondo un ordine di precedenza fissato  &#13;
 dalla legge,  lo  Stato  adempie  ad  un  obbligo  alimentare  verso  i  &#13;
 congiunti  bisognosi  del caduto. Non si vede perché quest'obbligo che  &#13;
 vale per genitori, assimilati e collaterali inabili ad una  certa  data  &#13;
 non  debba  valere anche nei confronti di fratelli e sorelle del caduto  &#13;
 che diventano inabili in momento  successivo  a  quello  fissato  dalle  &#13;
 norme impugnate.                                                         &#13;
     Tali   norme,  ad  avviso  del  giudice  a  quo,  attuerebbero  una  &#13;
 discriminazione  anche   nell'ambito   della   stessa   categoria   dei  &#13;
 collaterali  facendo  distinzione  fra  loro a seconda del tempo in cui  &#13;
 diventano inabili.                                                       &#13;
     Nel giudizio dinanzi a questa Corte si è  costituita  soltanto  la  &#13;
 parte  Ignazio  Catanuto, rappresentata e difesa dall'avv.  Nino Gaeta,  &#13;
 il quale, nelle deduzioni depositate in cancelleria,  conclude  per  la  &#13;
 fondatezza   della   proposta   questione   rilevando  che  le  ragioni  &#13;
 d'incostituzionalità    si    compendiano     essenzialmente     nella  &#13;
 disuguaglianza di trattamento risultante dalle disposizioni sui diritti  &#13;
 alla  riversibilità  delle pensioni di guerra, che non coincide con la  &#13;
 tradizionale disciplina legale degli alimenti ex art.  433  cod.  civ.,  &#13;
 nonché  con  le  restrizioni delle condizioni del collaterale rispetto  &#13;
 agli estranei.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - due giudizi opportunamente riuniti, vengono decisi  con  unica  &#13;
 sentenza   poiché   identica   è   la   questione   di   legittimità  &#13;
 costituzionale che con essi viene proposta.  Secondo le  ordinanze  gli  &#13;
 artt.  77 della legge 10 agosto 1950, n. 648, e 75 della legge 18 marzo  &#13;
 1968,  n.  313,  sarebbero  in  contrasto col principio di uguaglianza,  &#13;
 enunciato dall'art. 3 Cost., nella parte in cui  -  disponendo  che  la  &#13;
 pensione  indiretta  di  guerra  è concessa ai collaterali maggiorenni  &#13;
 inabili a proficuo lavoro solo quando l'inabilità sussista  alla  data  &#13;
 del  decesso  del militare o del civile oppure intervenga anche dopo la  &#13;
 suddetta data, ma prima che raggiungano la maggiore età  o  prima  del  &#13;
 giorno  dal  quale  dovrebbe devolversi in loro favore la pensione già  &#13;
 liquidata al padre o alla madre - dette  norme  escludono  dal  diritto  &#13;
 alla    pensione    i    collaterali   maggiorenni   divenuti   inabili  &#13;
 successivamente agli eventi suddetti.                                    &#13;
     2. - La questione proposta è fondata.                               &#13;
     La pensione indiretta  di  guerra  dei  membri  della  famiglia  di  &#13;
 origine  del  militare o del civile deceduto ha un innegabile carattere  &#13;
 alimentare in quanto, per la concessione della stessa, la  legge  esige  &#13;
 che  a  tali  soggetti,  in  conseguenza della morte del militare o del  &#13;
 civile, siano venuti a mancare i necessari mezzi di sussistenza.         &#13;
     Nella concreta disciplina di tale condizione, però, il legislatore  &#13;
 non ha posto sullo stesso piano questi soggetti, ma  ha  assicurato  ai  &#13;
 genitori un trattamento più favorevole rispetto a quello dettato per i  &#13;
 collaterali. La legge infatti stabilisce che la pensione è concessa al  &#13;
 padre  che  abbia  compiuto  58  anni di età o sia inabile a qualsiasi  &#13;
 proficuo  lavoro,  nonché  alla  madre  vedova  -  prescindendo  dalle  &#13;
 condizioni  di  età o inabilità - ; in tutto simile è il trattamento  &#13;
 fatto dal legislatore alla  categoria  degli  assimilati  ai  genitori,  &#13;
 ossia agli allevatori, al patrigno e alla matrigna (cfr. artt. 71 legge  &#13;
 n. 648 del 1950 e 64 legge n. 313 del 1968). Per i collaterali, invece,  &#13;
 il  riconoscimento  del  diritto  a pensione è stato subordinato ad un  &#13;
 requisito d'inabilità che deve sussistere a date  di  riferimento  ben  &#13;
 precise:  ossia  al  momento  della  morte del militare o del civile, o  &#13;
 prima del raggiungimento della  maggiore  età  o,  infine,  prima  del  &#13;
 giorno  dal  quale  dovrebbe  devolversi  al collaterale maggiorenne la  &#13;
 pensione già liquidata al padre o alla madre (artt. 77 legge n. 648  e  &#13;
 75  legge n. 313). Dal beneficio pensionistico restano conseguentemente  &#13;
 esclusi i fratelli e  le  sorelle  maggiorenni  del  caduto  che  siano  &#13;
 divenuti  inabili  a qualsiasi proficuo lavoro in una data successiva a  &#13;
 quella stabilita dalle impugnate norme.                                  &#13;
     3.  -  L'esame  di  questa  disciplina  pone  in  evidenza  che  il  &#13;
 trattamento  riservato ai collaterali maggiorenni è deteriore rispetto  &#13;
 a quello previsto per i genitori ed  i  soggetti  a  questi  assimilati  &#13;
 (allevatori,  patrigno  e  matrigna)  per  i  quali  il requisito della  &#13;
 inabilità non è ancorato ad alcun  specifico  momento  o  evento.  La  &#13;
 difformità  di  trattamento  è  ancor  più  evidente  rispetto  agli  &#13;
 assimilati ove si consideri che essi, non essendo legati da vincoli  di  &#13;
 sangue  col militare o civile deceduto, seguono i membri della famiglia  &#13;
 originaria e  quindi  anche  i  collaterali  nell'ordine  di  vocazione  &#13;
 stabilito dalla legge per l'attribuzione del diritto alla pensione.      &#13;
     Le  norme  denunciate attuano peraltro una evidente discriminazione  &#13;
 nell'ambito della stessa categoria  dei  collaterali  giacché,  se  la  &#13;
 legge collega il riconoscimento del diritto a pensione alla presenza di  &#13;
 un  reale  stato  di  bisogno  e  se  questo discende dall'inabilità a  &#13;
 qualsiasi  proficuo  lavoro,  nessuna  razionale  giustificazione  può  &#13;
 attribuirsi  alla  operata  distinzione  fra  collaterali a seconda del  &#13;
 momento in cui il requisito di invalidità insorga.                      &#13;
     Nessuna   obbiettiva  diversità  di  situazioni  riesce  invero  a  &#13;
 scorgersi tra i collaterali che siano inabili a  proficuo  lavoro  alle  &#13;
 date  indicate  dalle  norme  in  esame e quelli che lo diventino in un  &#13;
 momento successivo ricorrendo in entrambi i  casi  il  requisito  dello  &#13;
 stato   di   bisogno  che  il  legislatore  ha  assunto  a  presupposto  &#13;
 indispensabile per la  concessione  del  trattamento  pensionistico  di  &#13;
 guerra.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  l'illegittimità  costituzionale dell'art. 77 della legge  &#13;
 10 agosto 1950, n. 648, sul  "Riordinamento  delle  disposizioni  sulle  &#13;
 pensioni  di  guerra",  e  del  corrispondente art. 75 della successiva  &#13;
 legge 18 marzo 1968,  n.  313,  limitatamente  alla  parte  in  cui  su  &#13;
 ordinano  il  diritto  alla pensione indiretta di guerra dei fratelli e  &#13;
 sorelle maggiorenni comunque inabili a qualsiasi proficuo  lavoro  alla  &#13;
 condizione che l'inabilità sussista alla data del decesso del militare  &#13;
 o  del  civile o che divengano inabili anche dopo tale data ma prima di  &#13;
 raggiungere la maggiore età o prima  del  giorno  dal  quale  dovrebbe  &#13;
 devolversi  in  loro  favore la pensione già liquidata al padre o alla  &#13;
 madre.                                                                   &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 20 febbraio 1975.                             &#13;
                                   FRANCESCO  PAOLO BONIFACIO - GIOVANNI  &#13;
                                   BATTISTA BENEDETTI - LUIGI OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI  - VEZIO CRISAFULLI - PAOLO  &#13;
                                   ROSSI -  LEONETTO  AMADEI  -  EDOARDO  &#13;
                                   VOLTERRA  -  GUIDO  ASTUTI  - MICHELE  &#13;
                                   ROSSANO.                               &#13;
                                   ARDUINO SALUSTRI Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
