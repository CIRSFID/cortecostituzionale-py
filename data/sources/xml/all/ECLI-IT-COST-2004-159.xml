<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>159</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:159</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ZAGREBELSKY</presidente>
    <relatore_pronuncia>Paolo Maddalena</relatore_pronuncia>
    <redattore_pronuncia>Paolo Maddalena</redattore_pronuncia>
    <data_decisione>24/05/2004</data_decisione>
    <data_deposito>28/05/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfonso QUARANTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso con ordinanza del 20 maggio 2003 dal Giudice di pace di Vignola nel procedimento civile vertente tra Di Rito Roberto e il Prefetto di Modena, iscritta al n. 662 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2003. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 10 marzo 2004 il Giudice relatore Paolo Maddalena. &#13;
    Ritenuto che, con ordinanza del 20 maggio 2003, il Giudice di pace di Vignola ha sollevato questione di legittimità costituzionale dell'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), nella parte in cui prevede, per la fattispecie di guida con patente scaduta di validità, la sanzione accessoria del fermo del veicolo per un periodo di due mesi, per contrasto con gli artt. 76 e 77 della Costituzione, in relazione all'art. 2, comma 1, lettera mm), della legge 22 marzo 2001, n. 85 (Delega al Governo per la revisione del nuovo codice della strada); &#13;
    che, in punto di fatto, il giudice a quo espone di essere stato adito dal proprietario di un'automobile per l'annullamento della sanzione accessoria del fermo del veicolo disposta dall'autorità di pubblica sicurezza per guida con patente scaduta di validità; &#13;
    che lo stesso giudice espone altresì di aver disposto, con il decreto di fissazione dell'udienza, la sospensione della sanzione accessoria e la restituzione del veicolo al ricorrente; &#13;
    che, ciò premesso in fatto, il remittente osserva che l'art. 126, comma 7, del codice della strada prevede, per la guida con patente scaduta di validità, oltre alla sanzione amministrativa pecuniaria, anche la sanzione accessoria del ritiro della patente e del fermo del veicolo; &#13;
    che il giudice a quo evidenzia come l'art. 2, comma 1, lettera mm), della legge delega n. 85 del 2001 preveda l'eliminazione della sanzione accessoria del fermo del veicolo da parte del legislatore delegato; &#13;
    che, tuttavia, il decreto legislativo 15 gennaio 2002, n. 9 (Disposizioni integrative e correttive del nuovo codice della strada, a norma dell'art. 1, comma 1, della legge 22 marzo 2001, n. 85), emanato in attuazione della predetta legge delega, ha lasciato inalterato il regime sanzionatorio per la guida con patente scaduta di validità; &#13;
    che, secondo il remittente, la previsione, nella norma denunciata, della sanzione accessoria del fermo del veicolo contrasterebbe con gli artt. 76 e 77 della Costituzione; &#13;
    che è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata manifestamente inammissibile, in quanto il remittente ha censurato “non la norma delegata emanata in attuazione della richiamata legge delega n. 85 del 2001, ma una precedente disposizione recata da diverso atto legislativo”. &#13;
    Considerato che, successivamente al deposito dell'ordinanza di remissione, l'art. 2 del decreto-legge 27 giugno 2003, n. 151 (Modifiche ed integrazioni al codice della strada), convertito nella legge 1° agosto 2003, n. 214, ha eliminato la sanzione accessoria del fermo del veicolo per la fattispecie di guida con patente scaduta di validità; &#13;
    che, tuttavia, la suddetta modifica non può trovare applicazione nel giudizio a quo, atteso che, ai sensi dell'art. 1 della legge 24 aprile 1981, n. 689 (Modifiche al sistema penale), in materia di illeciti amministrativi, la condotta sanzionata resta assoggettata alla legge del tempo del suo verificarsi, con conseguente inapplicabilità della disciplina posteriore più favorevole; &#13;
    che il giudice a quo ha errato nella individuazione della norma da censurare, in quanto egli avrebbe dovuto impugnare, per contrasto con gli artt. 76 e 77 della Costituzione, non l'art. 126, comma 7, del d.lgs. n. 285 del 1992 rimasto inalterato, ma il d.lgs. n. 9 del 2002, nella parte in cui non avrebbe previsto la soppressione della sanzione accessoria del fermo del veicolo;  &#13;
    che la giurisprudenza di questa Corte ha osservato che i parametri costituzionali invocati “reggono soltanto i rapporti fra legge delegante e decreto legislativo delegato, … ed è pertanto fuor d'opera assumerli quale stregua del giudizio di costituzionalità … qualora sia questione di una norma contenuta in un atto estraneo a quei rapporti” (sentenza n. 218 del 1987);  &#13;
    che, pertanto, la questione è da ritenere manifestamente inammissibile. &#13;
    Visti l'art. 26, secondo comma, della legge 11 marzo 1953, n. 87, e l'art. 9, secondo comma, delle norme integrative per i giudizi innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 126, comma 7, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), sollevata, in riferimento agli artt. 76 e 77 della Costituzione, dal Giudice di pace di Vignola con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 maggio 2004. &#13;
    F.to: &#13;
    Gustavo ZAGREBELSKY, Presidente &#13;
    Paolo MADDALENA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 28 maggio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
