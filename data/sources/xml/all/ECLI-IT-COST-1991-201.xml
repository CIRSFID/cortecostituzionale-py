<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>201</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:201</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GALLO E.</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/04/1991</data_decisione>
    <data_deposito>02/05/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Ettore GALLO; &#13;
 Giudici: dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, &#13;
 prof. Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. &#13;
 Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 12 della  legge    &#13;
 3  gennaio 1981, n. 1 (Modificazioni alla legge 24 marzo 1958, n. 195    &#13;
 e al decreto del Presidente della Repubblica 16  settembre  1958,  n.    &#13;
 916,  sulla  costituzione  e il funzionamento del Consiglio superiore    &#13;
 della magistratura), promosso con ordinanza emessa il 13 luglio  1990    &#13;
 dalla Corte di cassazione, sezioni unite civili, sul ricorso proposto    &#13;
 da  Verrina Gabriele contro Ministero di grazia e giustizia ed altri,    &#13;
 iscritta al n. 62 del registro  ordinanze  1991  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  7,  prima serie speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Udito nella camera di consiglio del  10  aprile  1991  il  Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto  che  nel corso di un giudizio in cui il ricorrente aveva    &#13;
 rilevato come  il  decreto  di  fissazione  della  discussione  orale    &#13;
 dinanzi  alla  Sezione  disciplinare  del  Consiglio  superiore della    &#13;
 magistratura, gli fosse stato  notificato  ad  oltre  un  anno  dalla    &#13;
 sentenza di rinvio della Corte di cassazione, quest'ultima, a sezioni    &#13;
 unite,  con  ordinanza  emessa  il  13  luglio 1990, ha sollevato, in    &#13;
 relazione agli artt. 3, 24, 101 e 104 della  Costituzione,  questione    &#13;
 di  legittimità  costituzionale  dell'art.  12 della legge 3 gennaio    &#13;
 1981,  n.  1,  nella  parte  in  cui  non  prevede  l'estinzione  del    &#13;
 procedimento nell'ipotesi considerata;                                   &#13;
      che  il  giudice  a  quo,  richiamandosi  a  una  sua precedente    &#13;
 ordinanza di rimessione, rileva che, mentre nella fase di  avvio  del    &#13;
 procedimento  disciplinare è contemplata tale estinzione per il caso    &#13;
 di mancato rispetto del termine di un anno, una analoga sanzione  non    &#13;
 opera  allorché  il  procedimento stesso consegua al rinvio da parte    &#13;
 della Suprema Corte;                                                     &#13;
      che tale lacuna,  a  parere  del  giudice  rimettente,  oltre  a    &#13;
 segnare  un'ingiustificata disparità di trattamento, parrebbe lesiva    &#13;
 del diritto di difesa dell'incolpato, esposto sine  die  al  giudizio    &#13;
 disciplinare a causa dell'omessa prefissione del termine;                &#13;
    Considerato  che  la medesima questione è stata già esaminata da    &#13;
 questa Corte che,  con  sentenza  n.  579  del  1990,  ha  dichiarato    &#13;
 l'illegittimità  costituzionale  della  stessa  norma  ora impugnata    &#13;
 nella parte in cui non estende i termini ivi fissati al  procedimento    &#13;
 di rinvio;                                                               &#13;
      che  di  conseguenza  la  questione  qui  proposta  deve  essere    &#13;
 dichiarata manifestamente inammissibile;                                 &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 12 della legge 3 gennaio  1981,    &#13;
 n. 1 (Modificazioni alla legge 24 marzo 1958, n. 195 e al decreto del    &#13;
 Presidente   della  Repubblica  16  settembre  1958,  n.  916,  sulla    &#13;
 costituzione  e  il  funzionamento  del  Consiglio  superiore   della    &#13;
 magistratura),  sollevata, in riferimento agli artt. 3, 24, 101 e 104    &#13;
 della Costituzione, dalla Corte di cassazione con l'ordinanza di  cui    &#13;
 in  epigrafe,  già  dichiarato  costituzionalmente  illegittimo  con    &#13;
 sentenza n. 579 del 1990.                                                &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 12 aprile 1991.                               &#13;
                         Il Presidente: GALLO                             &#13;
                        Il redattore: CASAVOLA                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 maggio 1991.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
