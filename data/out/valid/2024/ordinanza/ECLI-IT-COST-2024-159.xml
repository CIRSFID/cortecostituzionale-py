<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-08-02/159/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-08-02/159"/>
          <FRBRalias value="ECLI:IT:COST:2024:159" name="ECLI"/>
          <FRBRdate date="2024-08-02" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="159"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-08-02/159/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-08-02/159/ita@"/>
          <FRBRdate date="2024-08-02" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-08-02/159/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2024-08-02/159/ita@.xml"/>
          <FRBRdate date="2024-08-02" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2024-06-18" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2024-08-02" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="BARBERA" href="/akn/ontology/person/it/BARBERA" showAs="BARBERA"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2024</akn4cc:anno>
        <akn4cc:numero>159</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2024:159</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>BARBERA</akn4cc:presidente>
        <akn4cc:relatore>Luca Antonini</akn4cc:relatore>
        <akn4cc:data_decisione>2024-06-18</akn4cc:data_decisione>
        <akn4cc:data_deposito>2024-08-02</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta da:
 Presidente: <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>; Giudici : <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI, Giovanni PITRUZZELLA, Antonella SCIARRONE ALIBRANDI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale degli artt. 9, 11 e 14 della legge della Regione Puglia 12 agosto 2022, n. 20, recante «Norme per il riuso e la riqualificazione edilizia e modifiche alla legge regionale [15] novembre 2007, n. 33 (Recupero dei sottotetti, dei porticati, di locali seminterrati e interventi esistenti e di aree pubbliche non autorizzate)», promosso dal Presidente del Consiglio dei ministri con ricorso notificato e depositato in cancelleria il 18 ottobre 2022, iscritto al n. 80 del registro ricorsi 2022 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 48, prima serie speciale, dell'anno 2022.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Puglia;
 udito nella camera di consiglio del 18 giugno 2024 il Giudice relatore Luca Antonini;
 deliberato nella camera di consiglio del 18 giugno 2024.
 Ritenuto che, con ricorso depositato il 18 ottobre 2022 e iscritto al reg. ric. n. 80 del 2022, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso, tra le altre, questioni di legittimità costituzionale degli artt. 9, 11 e 14 della legge della Regione Puglia 12 agosto 2022, n. 20, recante «Norme per il riuso e la riqualificazione edilizia e modifiche alla legge regionale [15] novembre 2007, n. 33 (Recupero dei sottotetti, dei porticati, di locali seminterrati e interventi esistenti e di aree pubbliche non autorizzate)»;
 che, a chiusura del Capo I (Norme per il riuso e la riqualificazione edilizia) dell'impugnata legge regionale, l'art. 9 reca una «[d]isposizione transitoria» della disciplina introdotta dagli articoli precedenti, prevedendo che «[l]e pratiche edilizie inoltrate e protocollate ai sensi della legge regionale 14/2009 presso gli sportelli unici per l'edilizia dei comuni pugliesi, prima della data del 29 luglio 2022, sono istruite e concluse secondo le prescrizioni della medesima» legge della Regione Puglia 30 luglio 2009, n. 14 (Misure straordinarie e urgenti a sostegno dell'attività edilizia e per il miglioramento della qualità del patrimonio edilizio residenziale), avente a oggetto il cosiddetto "Piano casa";
 che, secondo il ricorrente, tale previsione violerebbe l'art. 117, terzo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, realizzando una deroga sia «all'ordinario principio tempus regit actum», sia al principio «della c.d. doppia conformità», espresso dall'istituto dell'accertamento di conformità di cui all'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-06-06/380/!main#art_36">art. 36 del d.P.R. 6 giugno 2001, n. 380</ref>, recante «Testo unico delle disposizioni legislative e regolamenti in materia edilizia. (Testo A)», principio fondamentale nella materia «governo del territorio»;
 che gli artt. 11 e 14, modificando, rispettivamente, gli artt. 1, comma 3, lettera a), e 4, comma 1, della legge della Regione Puglia 15 novembre 2007, n. 33 (Recupero dei sottotetti, dei porticati, di locali seminterrati e interventi esistenti e di aree pubbliche non autorizzate), differiscono «alla data di entrata in vigore della presente disposizione» il termine del 30 giugno 2021, in precedenza stabilito per il recupero volumetrico degli edifici e per il recupero abitativo dei sottotetti;
 che, con doglianze comuni a entrambe le disposizioni appena richiamate, il ricorrente argomenta anzitutto la violazione degli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_97">97 Cost.</ref></mref>, laddove le stesse siano interpretate nel senso di consentire gli interventi edilizi sugli immobili esistenti al 19 novembre 2007, data di entrata in vigore della legge reg. Puglia n. 33 del 2007, determinandosi in tal modo una «cancellazione retroattiva» della disciplina medesima, con pregiudizio di coloro che si siano avvalsi della facoltà di recupero dei sottotetti nel frattempo realizzati, e producendosi altresì l'incertezza delle sorti delle opere già realizzate;
 che, invece, ove interpretate, più plausibilmente, nel senso di fare riferimento alla data di entrata in vigore della legge reg. Puglia n. 20 del 2022, vale a dire il 16 agosto 2022, le disposizioni impugnate costituirebbero «una ulteriore proroga della portata applicativa della disciplina concernente il recupero dei sottotetti [...], illegittima sotto molteplici profili»;
 che, infatti, collocando gli interventi di trasformazione urbanistica ed edilizia al di fuori delle previsioni del piano paesaggistico, le stesse violerebbero sia l'art. 117, secondo comma, lettera s), Cost., contrastando con la scelta del legislatore statale «di rimettere alla pianificazione la disciplina d'uso dei beni paesaggistici», esplicitata dagli <mref><ref href="/akn/it/act/decretoLegislativo/stato/2004-01-22/42/!main#art_135">artt. 135</ref>, <ref href="/akn/it/act/decretoLegislativo/stato/2004-01-22/42/!main#art_143">143</ref> e <ref href="/akn/it/act/decretoLegislativo/stato/2004-01-22/42/!main#art_145">145 del decreto legislativo 22 gennaio 2004, n. 42</ref></mref> (Codice dei beni culturali e del paesaggio, ai sensi dell'<ref href="/akn/it/act/legge/stato/2002-07-06/137/!main#art_10">articolo 10 della legge 6 luglio 2002, n. 137</ref>), sia, di conseguenza, l'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_9">art. 9 Cost.</ref>, che sancisce la tutela del paesaggio quale interesse primario e assoluto;
 che, inoltre, risulterebbe violato l'art. 117, terzo comma, Cost., in relazione all'art. 41-quinquies, ottavo e nono comma, della <ref href="/akn/it/act/legge/stato/1942-08-17/1150/!main">legge 17 agosto 1942, n. 1150</ref> (Legge urbanistica), al decreto del Ministro per i lavori pubblici adottato di concerto con il Ministro per l'interno 2 aprile 1968, n. 1444 (Limiti inderogabili di densità edilizia, di altezza, di distanza fra i fabbricati e rapporti massimi tra spazi destinati agli insediamenti residenziali e produttivi e spazi pubblici o riservati alle attività collettive, al verde pubblico o a parcheggi da osservare ai fini della formazione dei nuovi strumenti urbanistici o della revisione di quelli esistenti, ai sensi dell'<ref href="/akn/it/act/legge/stato/1967-08-06/765/!main#art_17">art. 17 della legge 6 agosto 1967, n. 765</ref>), nonché alla intesa tra Stato e Regioni del 1° aprile 2009 (Intesa, ai sensi dell'<ref href="/akn/it/act/legge/stato/2003-06-05/131/!main#art_8__para_6">articolo 8, comma 6, della legge 5 giugno 2003, n. 131</ref>, tra Stato, regioni e gli enti locali, sull'atto concernente misure per il rilancio dell'economia attraverso l'attività edilizia), «sul c.d. primo piano casa», espressione di principi che, rispettivamente: a) consentirebbero interventi di trasformazione edilizia e urbanistica soltanto nel quadro della pianificazione urbanistica; b) richiederebbero il necessario rispetto degli standard urbanistici; c) precluderebbero di riconoscere premialità edilizie per gli immobili abusivi oggetto di sanatoria;
 che, infine, gli artt. 11 e 14 della legge reg. Puglia n. 20 del 2022 violerebbero gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">artt. 3</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_9">9 Cost.</ref></mref>, per via della «irragionevolezza intrinseca della previsione della possibilità di recupero "a regime" dei volumi edilizi relativi a sottotetti anche di recente realizzazione»;
 che, con atto depositato il 14 novembre 2022, si è costituita in giudizio la Regione Puglia, eccependo l'inammissibilità e la non fondatezza delle questioni di legittimità costituzionale degli artt. 9, 11 e 14 della legge reg. Puglia n. 20 del 2022;
 che, con memoria depositata il 27 maggio 2024, in prossimità dell'udienza pubblica fissata per il 18 giugno 2024, la resistente ha dato conto della adozione della legge della Regione Puglia 19 dicembre 2023, n. 36, recante «Disciplina regionale degli interventi di ristrutturazione edilizia ai sensi dell'<ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2001-06-06/380/!main#art_3__para_1__point_d">articolo 3, comma 1, lettera d), del decreto del Presidente della Repubblica 6 giugno 2001, n. 380</ref> (Testo unico delle disposizioni legislative e regolamentari in materia edilizia) e disposizioni diverse», il cui art. 8, comma 1, ha disposto che «[i]l capo I e gli articoli 11 e 14 della legge regionale 12 agosto 2022, n. 20 [...], sono abrogati»;
 che, a seguito di tale ultimo intervento normativo, su conforme deliberazione del Consiglio dei ministri del 29 maggio 2024, il Presidente del Consiglio dei ministri, in data 5 giugno 2024, ha depositato atto di rinuncia all'intero ricorso, «essendo venuto meno l'interesse» a coltivarlo, in ragione della abrogazione di tutte le disposizioni impugnate e non risultando che medio tempore queste «abbiano trovato applicazione»;
 che, il 12 giugno 2024, su conforme deliberazione della Giunta resa in data 11 giugno 2024, la Regione Puglia ha depositato atto di accettazione della predetta rinuncia al ricorso;
 che, in seguito alla rinuncia del ricorrente, il Presidente di questa Corte, con decreto del 12 giugno 2024, ha fissato la trattazione del ricorso alla camera di consiglio del 18 giugno 2024.
 Considerato che il Presidente del Consiglio dei ministri ha rinunciato all'intero ricorso indicato in epigrafe, previa deliberazione del Consiglio dei ministri;
 che la rinuncia è stata accettata dalla Regione Puglia;
 che la rinuncia al ricorso, accettata dalla controparte costituita, determina, ai sensi dell'art. 25 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.
 Visti l'art. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e gli artt. 24, comma 1, e 25 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara estinto il processo.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2024-06-18">18 giugno 2024</date>.
 F.to:
 <signature><judge refersTo="#augustoAntonioBarbera" as="#presidente">Augusto Antonio BARBERA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#lucaAntonini" as="#relatore">Luca ANTONINI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Direttore della Cancelleria</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2024-08-02">2 agosto 2024</date>
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
