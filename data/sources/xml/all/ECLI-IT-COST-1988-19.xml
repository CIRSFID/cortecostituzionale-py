<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:19</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/01/1988</data_decisione>
    <data_deposito>19/01/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 80, secondo    &#13;
 comma, del d.P.R. 26 ottobre 1972, n. 634 (Disciplina dell'imposta di    &#13;
 registro)  e 32, secondo comma, ultima parte, del d.P.R. 29 settembre    &#13;
 1973, n. 601 (Disciplina delle agevolazioni tributarie), promosso con    &#13;
 ordinanza  emessa il 29 dicembre 1980 dalla Commissione tributaria di    &#13;
 1° grado di Monza, iscritta al n. 465 del registro ordinanze  1981  e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 297 dell'anno    &#13;
 1981;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Ritenuto  che,  con  ordinanza emessa il 29 dicembre 1980 (R.O. n.    &#13;
 465/1981), la Commissione tributaria di  primo  grado  di  Monza,  su    &#13;
 ricorso  proposto  da Brusa Ermete, ha sollevato, in riferimento agli    &#13;
 artt.  3  e  47  Cost.,  questione  di  legittimità   costituzionale    &#13;
 dell'art.   80,  secondo  comma,  d.P.R.  26  ottobre  1972,  n.  634    &#13;
 (Disciplina dell'imposta di registro), nonché dell'art.  32,  ultima    &#13;
 parte  del  secondo  comma,  del  d.P.R.  29  settembre  1973, n. 601    &#13;
 (Disciplina delle agevolazioni tributarie);                              &#13;
      che ha spiegato intervento l'Avvocatura generale dello Stato;       &#13;
    Considerato che le censure sono rivolte alla mancata estensione "a    &#13;
 tutte le ipotesi di alloggi economici  e  popolari"  del  trattamento    &#13;
 fiscale di cui alle premesse norme;                                      &#13;
      che:                                                                &#13;
        a)  quanto  all'art.  80,  secondo  comma, d.P.R. n. 634/1972,    &#13;
 nella  parte  che  mantiene  in  vigore  le   agevolazioni   per   le    &#13;
 cooperative,  non  è dato invocare la pretesa violazione dell'art. 3    &#13;
 Cost., presupponente la esistenza di situazioni  identiche  o  quanto    &#13;
 meno  omogenee;  nella  specie, infatti, il trattamento differenziato    &#13;
 trova fondamento nella diversità, dal punto di vista soggettivo, fra    &#13;
 la  posizione  delle  cooperative  edilizie e dei loro consorzi - tra    &#13;
 l'altro costituzionalmente protetta ex art. 45 Cost. - e quella degli    &#13;
 enti  pubblici  e  dei  Comuni  che intervengono nella costruzione di    &#13;
 alloggi economici o popolari;                                            &#13;
        b)  quanto all'art. 32, secondo comma, del d.P.R. 29 settembre    &#13;
 1973, n. 601, del pari, non sussiste alcun contrasto della norma  con    &#13;
 il  principio di eguaglianza, giacché le agevolazioni, ivi previste,    &#13;
 non comportano alcuna discriminazione soggettiva, bensì  pongono  un    &#13;
 ambito  di  applicazione individuato in base al criterio che gli atti    &#13;
 siano  finalizzati  alla  realizzazione  dei  programmi  pubblici  di    &#13;
 edilizia residenziale;                                                   &#13;
        c)  quanto,  ancora,  alla  censura  di  illegittimità  delle    &#13;
 predette due norme in riferimento all'art. 47 Cost., nella  parte  in    &#13;
 cui  tale  articolo  "favorisce l'accesso del risparmio popolare alla    &#13;
 proprietà dell'abitazione", appartiene  alle  scelte  legislative  -    &#13;
 attenendo la norma all'indirizzo generale di politica economica dello    &#13;
 Stato (sent. n. 29 del 1975) - stabilire i modi ed i limiti onde dare    &#13;
 attuazione  al  suddetto principio, così come in effetti operato con    &#13;
 le richiamate disposizioni agevolative;                                  &#13;
    Visti  gli  artt.  26  della  legge 11 marzo 1953, n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 80, secondo comma, d.P.R. 26  ottobre  1972,    &#13;
 n.  634 (Disciplina dell'imposta di registro) e dell'art. 32, secondo    &#13;
 comma, del  d.P.R.  29  settembre  1973,  n.  601  (Disciplina  delle    &#13;
 agevolazioni  tributarie)  sollevata in riferimento all'art. 3 Cost.,    &#13;
 dalla Commissione tributaria di primo grado di Monza, con l'ordinanza    &#13;
 in epigrafe;                                                             &#13;
    Dichiara manifestamente inammissibile la questione di legittimità    &#13;
 costituzionale  delle  norme  di  cui   al   precedente   punto   del    &#13;
 dispositivo,  sollevata,  in  riferimento  all'art.  47 Cost., con la    &#13;
 medesima ordinanza.                                                      &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 13 gennaio 1988.        &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: BORZELLINO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 gennaio 1988.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
