<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>245</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:245</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ZAGREBELSKY</presidente>
    <relatore_pronuncia>Alfio Finocchiaro</relatore_pronuncia>
    <redattore_pronuncia>Alfio Finocchiaro</redattore_pronuncia>
    <data_decisione>08/07/2004</data_decisione>
    <data_deposito>20/07/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai Signori: Presidente: Gustavo ZAGREBELSKY; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 291 del codice civile, promosso con ordinanza dell'8 luglio 2000 dal Tribunale di La Spezia sul ricorso proposto da Bassi Luigi, iscritta al n. 759 del registro ordinanze 2003 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 39, prima serie speciale, dell'anno 2003. &#13;
    Udito nella camera di consiglio del 26 maggio 2004 il Giudice relatore Alfio Finocchiaro.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.- Il Tribunale di La Spezia, nel corso di un procedimento per l'adozione di maggiorenne, con ordinanza dell'8 luglio 2000, ha sollevato questione di legittimità costituzionale dell'art. 291 del codice civile, in riferimento all'art. 3 della Costituzione, nella parte in cui consente l'adozione di persona maggiore di età da parte di chi abbia discendenti naturali riconosciuti in età minore.  &#13;
    Il giudice a quo espone che il ricorrente, il quale ha chiesto, ai sensi dell'art. 291 cod. civ., l'adozione della figlia maggiorenne della sua attuale convivente, nata dal matrimonio di questa con persona deceduta, ha dichiarato di non avere avuto figli dal proprio precedente matrimonio - di cui era stata già dichiarata la cessazione degli effetti civili con sentenza passata in giudicato - nonché di avere un figlio naturale riconosciuto minore, nato dall'unione more uxorio con la madre della maggiorenne di cui chiedeva l'adozione. &#13;
    Il Tribunale - poiché l'art. 291 cod. civ., come risulta dopo la sentenza della Corte costituzionale n. 557 del 1988, consente l'adozione di maggiorenne a chi non ha discendenti legittimi o legittimati e a chi ha figli legittimi o legittimati maggiorenni che abbiano prestato il consenso, precludendo l'adozione a chi ha discendenti legittimi o legittimati minori, perché non in grado di esprimere il consenso, mentre permette l'adozione di maggiorenne da parte di chi ha discendenti naturali riconosciuti in età minore - prospetta una disparità di trattamento tra la posizione del figlio minore legittimo o legittimato e quella del figlio minore naturale riconosciuto dell'adottante e solleva d'ufficio la questione di costituzionalità. &#13;
    Quanto alla rilevanza, il giudice rimettente sottolinea che dalla decisione della Corte costituzionale dipende l'accoglimento o il rigetto della domanda di adozione. &#13;
    2.- Nel giudizio innanzi a questa Corte non vi è stata costituzione di parti private, né è intervenuto il Presidente del Consiglio dei ministri.<diritto>Considerato in diritto</diritto>1.- Il Tribunale di La Spezia, nel corso di un procedimento diretto all'adozione di maggiorenne da parte di soggetto con figlio minore naturale riconosciuto, ha sollevato d'ufficio questione di legittimità costituzionale dell'art. 291 cod. civ. - quale risulta dopo la sentenza della Corte Costituzionale n. 557 del 1988 - che consente l'adozione di persona maggiore di età da parte di chi abbia discendenti naturali riconosciuti in età minore, per violazione dell'art. 3 della Costituzione, sussistendo disparità di trattamento tra la posizione del figlio minore legittimo o legittimato e quella del figlio minore naturale riconosciuto dell'adottante, atteso che l'adozione di persona maggiore d'età è preclusa nel primo caso mentre è consentita nel secondo. &#13;
    2.- La questione è fondata. &#13;
    L'art. 291, primo comma, cod. civ., nel testo sostituito dall'art. 1 della legge 5 giugno 1967, n. 431, sull'adozione speciale, disponeva che l'adozione è permessa alle persone che non hanno discendenti legittimi o legittimati, che hanno compiuto gli anni trentacinque e che superano di almeno diciotto anni l'età di coloro che intendono adottare.  &#13;
    Successivamente, questa Corte, con sentenza n. 557 del 1988, ha dichiarato la illegittimità costituzionale della norma citata nella parte in cui non consente l'adozione a persone che abbiano discendenti legittimi o legittimati maggiorenni e consenzienti. &#13;
    A seguito della pronuncia di incostituzionalità, la dottrina e la giurisprudenza sono concordi nell'interpretare l'art. 291 cod. civ. nel senso che il divieto di adozione di maggiorenni si applica a coloro che hanno figli legittimi o legittimati minori, o, se maggiorenni, non consenzienti, e non anche a coloro che hanno figli naturali riconosciuti. &#13;
    Questa interpretazione, imposta dal tenore della disposizione, evidenzia una illegittima disparità disparità di trattamento fra figli legittimi e figli naturali riconosciuti ed in pregiudizio dei secondi, in quanto le ragioni di indole morale e patrimoniale, che consentono ai primi di opporsi all'adozione, valgono anche per i figli naturali. &#13;
    D'altro canto, nella situazione presa in esame non sono ipotizzabili profili di incompatibilità con i diritti dei membri della famiglia legittima che giustifichino un trattamento normativo differenziato. &#13;
     Da quanto precede deriva che deve dichiararsi l'illegittimità costituzionale dell'art. 291 cod. civ., nella parte in cui non prevede che l'adozione di maggiorenni non possa essere pronunciata in presenza di figli naturali riconosciuti dall'adottante, minorenni o, se maggiorenni, non consenzienti.</testo>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara l'illegittimità costituzionale dell'art. 291 del codice civile nella parte in cui non prevede che l'adozione di maggiorenni non possa essere pronunciata in presenza di figli naturali, riconosciuti dall'adottante, minorenni o, se maggiorenni, non consenzienti. &#13;
    Così deciso in Roma, nella sede della Corte Costituzionale, Palazzo della Consulta  l'8  luglio 2004. &#13;
F.to: &#13;
Gustavo ZAGREBELSKY, Presidente &#13;
Alfio FINOCCHIARO, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 20 luglio 2004. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
