import os
import xml.etree.ElementTree as ET
from collections import Counter
from tqdm import tqdm

from SPARQLWrapper import SPARQLWrapper, JSON

tqdm.monitor_interval = 0

ENDPOINT = 'https://dati.cortecostituzionale.it/sparql/endpoint'
AKN = '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}'
AKNCC = '{http://cortecostituzionale.it/metadata}'
PREFIX_STRING = """
PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
PREFIX dcc: <https://dati.cortecostituzionale.it/ontology/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX d2r: <http://sites.wiwiss.fu-berlin.de/suhl/bizer/d2r-server/config.rdf#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX schema-org: <http://schema.org/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX meta: <http://www4.wiwiss.fu-berlin.de/bizer/d2r-server/metadata#>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX map: <https://dati.cortecostituzionale.it/ontology/#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
"""
TYPE_MAP = {
    'S': 'Sentenza',
    'O': 'Ordinanza'
}


def get_document_list(directory):
    doc_list = []
    for obj in os.listdir(directory):
        obj_path = os.path.join(directory, obj)
        if os.path.isfile(obj_path):
            doc_list.append(obj_path)
        elif os.path.isdir(obj_path):
            doc_list.extend(get_document_list(obj_path))
    return doc_list


def get_pronunciation_method_dict_from_endpoint():
    # wrap the SPARQL end-point
    endpoint = SPARQLWrapper(ENDPOINT)
    # set the query string
    endpoint.setQuery(PREFIX_STRING + """
	SELECT DISTINCT ?pronuncia ?numero ?anno ?tipo_procedimento 
	WHERE {
	   ?pronuncia dbpedia-owl:number ?numero ;
	              dcc:tipoProcedimento ?tipo_procedimento;
	              time:year ?anno.
	}
	ORDER BY ASC(?anno && ?numero)
	""")
    # select the return format (e.g. XML, JSON etc...)
    endpoint.setReturnFormat(JSON)
    # execute the query and convert into Python objects
    # Note: The JSON returned by the SPARQL endpoint is converted to nested Python dictionaries, so additional parsing is not required.
    results = endpoint.query().convert()
    doc_dict_list = results["results"]["bindings"]
    doctype_dict = {}
    for doc_dict in doc_dict_list:
        doc_id = '{}:{}'.format(doc_dict['anno']['value'], doc_dict['numero']['value'])
        doctype_dict[doc_id] = doc_dict['tipo_procedimento']['value']
    return doctype_dict


def get_pronunciation_meta_dict(constitutional_court_docs_path, debug=True):
    pronunciation_method_dict = get_pronunciation_method_dict_from_endpoint()
    pronunciation_meta_dict = {}
    doc_list = get_document_list(constitutional_court_docs_path)
    if debug:
        print('Processing documents of the Constitutional Court..')
    for obj_path in tqdm(doc_list) if debug else doc_list:
        if not obj_path.endswith(('.xml',)):
            continue
        tree = ET.parse(obj_path)
        root = tree.getroot()

        id_element = root.findall(".//{0}FRBRalias".format(AKN))[0]
        doc_id = id_element.attrib.get('value')
        group, state, court_type, year, number = doc_id.split(':')
        # print('    ', group, state, court_type, year, number)
        pid = '{}:{}'.format(year, number)

        # print('Get decision event date:')
        decision_event_element = next(filter(lambda x: x.attrib.get('refersTo') == "#decisionEvent",
                                             root.findall(".//{0}workflow/{0}step".format(AKN))))
        year, month, day = decision_event_element.attrib.get('date').split('-')
        # print('    ', year, month, day)

        # print('Get type:')
        type_element = root.findall(".//{0}proprietary/{1}tipo".format(AKN, AKNCC))[0]
        ptype = TYPE_MAP[type_element.text]
        # print('    ', ptype)

        # print('Get references to constitution:')
        href_iter = (
            element.attrib.get('href')
            for element in root.findall(".//{0}ref".format(AKN))
        )
        constitution_article_id_list = [
            href.split('#')[-1]
            for href in href_iter
            if href.startswith((
                # '/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_117__para_2',
                '/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_117__para_3'
            ))
        ]
        # if len(constitution_article_id_list):
        #    print(constitution_article_id_list)
        constitution_reference_counter = Counter(constitution_article_id_list)
        # print('    ', constitution_reference_dict)
        pronunciation_meta_dict[pid] = {
            'full_name': doc_id,
            'decision_date': {
                'year': year,
                'month': month,
                'day': day,
            },
            'type': ptype,
            'constitution_article_reference_counter': dict(constitution_reference_counter),
            'pronunciation_method': pronunciation_method_dict.get(pid, None),
        }
    return pronunciation_meta_dict

# import sys
# CONSTITUTION_DOC_PATH = sys.argv[1]
# pronunciation_meta_dict = get_pronunciation_meta_dict(CONSTITUTION_DOC_PATH)
# pronunciation_meta_json = json.dumps(pronunciation_meta_dict, indent=4, sort_keys=True)
# print(pronunciation_meta_json)
# # print(get_pronunciation_method_dict_from_endpoint())
# with open('pronunciation_meta.json','w') as f:
# 	f.write(pronunciation_meta_json)
