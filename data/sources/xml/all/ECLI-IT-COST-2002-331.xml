<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>331</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:331</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia>Piero Alberto Capotosti</redattore_pronuncia>
    <data_decisione>01/07/2002</data_decisione>
    <data_deposito>09/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Giovanni Maria FLICK, Francesco AMIRANTE, &#13;
Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 51, comma 6, &#13;
della  legge  27 dicembre 1997, n. 449 (Misure per la stabilizzazione &#13;
della  finanza  pubblica), promosso con ordinanza emessa il 12 maggio &#13;
2000 dal Tribunale amministrativo regionale del Friuli-Venezia Giulia &#13;
sul  ricorso  proposto  da  L. B. contro l'Università degli studi di &#13;
Trieste  ed  altri,  iscritta al n. 882 del registro ordinanze 2001 e &#13;
pubblicata  nella Gazzetta Ufficiale della Repubblica n. 43, 1ª serie &#13;
speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio dell'8 maggio 2002 il giudice &#13;
relatore Piero Alberto Capotosti. &#13;
    Ritenuto   che   il   Tribunale   amministrativo   regionale  del &#13;
Friuli-Venezia  Giulia,  nel  giudizio  promosso  da  una funzionaria &#13;
amministrativa,  inquadrata  all'VIII  livello  presso l'Osservatorio &#13;
astronomico  di  Padova, per l'annullamento del provvedimento n. 1284 &#13;
del  rettore  dell'Università  degli  studi di Trieste, reso in data &#13;
19 gennaio  2000,  con  il quale si subordinava il conferimento di un &#13;
assegno per la collaborazione ad attività di ricerca alle dimissioni &#13;
dal  posto  di  dipendente di ruolo dell'Osservatorio astronomico, in &#13;
applicazione  dell'art. 51,  comma  6,  della legge 27 dicembre 1997, &#13;
n. 449  (Misure  per  la stabilizzazione della finanza pubblica), con &#13;
ordinanza  del 12 maggio 2000, ha sollevato questione di legittimità &#13;
costituzionale  della suddetta norma, in riferimento all'art. 3 della &#13;
Costituzione; &#13;
        che  il  rimettente  premette  di aver giudicato infondato il &#13;
ricorso   sotto   diversi   profili  di  legittimità  relativi  alla &#13;
disposizione impugnata; &#13;
        che,  ad  avviso del giudice a quo, la disposizione censurata &#13;
viola  peraltro  l'art. 3  della Costituzione "nella parte in cui non &#13;
distingue  tra  dipendenti  di  enti di ricerca e tra personale degli &#13;
stessi impegnato in tale attività o in quella amministrativa"; &#13;
        che  nel  giudizio è intervenuto il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  il  quale,  sia  nell'atto  di  intervento  che nella memoria &#13;
depositata  in  prossimità della camera di consiglio, ha chiesto che &#13;
la questione sia dichiarata infondata, posto che la ratio della norma &#13;
denunciata  sarebbe  quella  di  "offrire  agli  enti  di  ricerca la &#13;
possibilità  di  avvalersi, per un periodo determinato, dell'apporto &#13;
scientifico   di   persone   estranee  all'ambito  di  detti  enti  e &#13;
particolarmente qualificate". &#13;
    Considerato   che  il  Tribunale  amministrativo  regionale,  nel &#13;
sollevare  la  questione di legittimità costituzionale, premette che &#13;
la  ricorrente ha eccepito, a conforto della domanda di annullamento, &#13;
molteplici  censure  di  legittimità nei confronti del provvedimento &#13;
rettorale   impugnato,   proprio   in   relazione  alla  disposizione &#13;
denunziata; &#13;
        che  l'ordinanza  di rimessione espressamente dichiara che il &#13;
ricorso proposto avverso il predetto provvedimento rettorale è stato &#13;
ritenuto  infondato  e  sono  state rigettate le censure sollevate in &#13;
riferimento  alla  norma  impugnata e cioè l'art. 51, comma 6, della &#13;
legge n. 449 del 1997; &#13;
        che,  pertanto,  la  questione  è irrilevante per difetto di &#13;
pregiudizialità,  in quanto il rimettente nel giudicare infondato in &#13;
relazione  alla  predetta  norma il ricorso pendente dinanzi a sé ha &#13;
così  fatto applicazione della disposizione censurata e si è quindi &#13;
precluso  la possibilità di rimetterla in discussione e di sollevare &#13;
l'eccezione  di  legittimità  costituzionale  (ordinanze  n. 264 del &#13;
1998; n. 67 del 1998; n. 300 del 1997); &#13;
        che,   d'altra   parte,  il  profilo  della  rilevanza  della &#13;
questione  sollevata  appare  privo di motivazione, non consentendo a &#13;
questa  Corte  il  necessario  controllo  sulla  sussistenza  di tale &#13;
condizione di proponibilità; &#13;
        che,   pertanto,   la   questione   deve   essere  dichiarata &#13;
manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità   costituzionale  dell'art. 51,  comma  6,  della  legge &#13;
27 dicembre 1997, n. 449 (Misure per la stabilizzazione della finanza &#13;
pubblica),  sollevata  dal  Tribunale  amministrativo  regionale  del &#13;
Friuli-Venezia  Giulia, in riferimento all'art. 3 della Costituzione, &#13;
con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 1 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                       Il redattore: Capotosti &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 9 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
