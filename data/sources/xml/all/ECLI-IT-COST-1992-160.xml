<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>160</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:160</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/03/1992</data_decisione>
    <data_deposito>02/04/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  18,  comma    &#13;
 quinto,  della legge 20 maggio 1970, n. 300 (Norme sulla tutela della    &#13;
 libertà e  dignità  dei  lavoratori,  della  libertà  sindacale  e    &#13;
 dell'attività   sindacale   nei   luoghi   di  lavoro  e  norme  sul    &#13;
 collocamento), nel testo modificato dall'art. 1 della legge 11 maggio    &#13;
 1990, n. 108 promosso con ordinanza emessa il 1 luglio 1991 dal  Pretore  di  Milano  nel procedimento civile vertente tra Piccin Loris e    &#13;
 s.p.a. COGEI, iscritta al  n.  712  del  registro  ordinanze  1991  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 49, prima    &#13;
 serie speciale, dell'anno 1991.                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  4  marzo 1992 il Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto che, nel corso di un procedimento per decreto ingiuntivo,    &#13;
 promosso da Loris Piccin contro la COGEI s.p.a., al fine di  ottenere    &#13;
 il  pagamento  della  somma  risarcitoria del danno per licenziamento    &#13;
 ingiustificato e dell'indennità sostituiva della reintegrazione  nel    &#13;
 posto  di  lavoro prevista dall'art. 18, quinto comma, della legge 20    &#13;
 maggio 1970, n. 300, nel testo modificato dall'art. 1 della legge  11    &#13;
 maggio 1990, n. 108, il Pretore di Milano, con ordinanza del 1 luglio    &#13;
 1991,  ha  sollevato  questione  di legittimità costituzionale della    &#13;
 disposizione citata per contrasto con gli artt.  3,  24  e  41  della    &#13;
 Costituzione;                                                            &#13;
      che,  ad  avviso  del  giudice  remittente,  la norma denunciata    &#13;
 viola: a) l'art. 3  della  Costituzione  sia  sotto  il  profilo  del    &#13;
 principio  di eguaglianza, in quanto sovverte senza giustificazione i    &#13;
 principi del diritto civile in materia di clausola penale, sia  sotto    &#13;
 il  profilo  del  principio di razionalità, apparendo esorbitante la    &#13;
 somma risarcitoria di venti mensilità attribuita  in  ogni  caso  al    &#13;
 lavoratore  che dichiari di non volere la reintegrazione nel posto di    &#13;
 lavoro, indipendentemente dalla prova del  danno  effettivo  e  senza    &#13;
 possibilità  di  prova  contraria;  b)  l'art. 24 della Costituzione    &#13;
 perché,  "collegando la scelta del lavoratore alla semplice sentenza    &#13;
 di reintegrazione di primo grado,  sembra  precludere  al  datore  di    &#13;
 lavoro   la  possibilità  di  proseguire  il  giudizio  chiedendo  e    &#13;
 ottenendo eventualmente la conferma del licenziamento con la  riforma    &#13;
 della  sentenza  di  prime  cure  "; c) l'art. 41 della Costituzione,    &#13;
 perché da essa la libertà di iniziativa economica  privata  "sembra    &#13;
 eccessivamente coartata";                                                &#13;
      che nel giudizio davanti alla Corte è intervenuto il Presidente    &#13;
 del  Consiglio  dei  ministri,  rappresentato  dall'Avvocatura  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata infondata.              &#13;
    Considerato che, in riferimento agli artt. 3 e 41, secondo  comma,    &#13;
 della  Costituzione.,  la  questione  è  già  stata  dichiarata non    &#13;
 fondata da questa Corte con sentenza n. 81 del 1992;                     &#13;
      che   l'accostamento   all'istituto   della   clausola   penale,    &#13;
 prospettato  dall'odierna  ordinanza  di  rimessione per sostenere la    &#13;
 natura risarcitoria dell'indennità sostitutiva della  reintegrazione    &#13;
 nel  posto  di  lavoro,  è  ancor  meno producente dell'accostamento    &#13;
 all'istituto delle dimissioni indennizzate proposto dalla  precedente    &#13;
 ordinanza  del  Pretore  di  Varese,  onde  deve essere richiamata la    &#13;
 motivazione  della  sentenza  citata  nel  senso  che,   non   avendo    &#13;
 l'indennità in discorso funzione di ristoro di un danno, né di pena    &#13;
 privata,  la  disciplina  della  clausola  penale non può fornire un    &#13;
 tertium comparationis ai fini dell'art. 3 della Costituzione;            &#13;
      che la norma denunciata non costituisce per se stessa un  limite    &#13;
 del  potere  organizzativo  dell'impresa, bensì presuppone il limite    &#13;
 stabilito dal primo comma (non impugnato) dell'art. 18 della legge n.    &#13;
 300 del 1970, concorrendo  a  determinare,  secondo  una  valutazione    &#13;
 discrezionale del legislatore, le conseguenze dell'accertamento della    &#13;
 sua  violazione,  onde fuor di proposito è invocato l'art. 41, primo    &#13;
 comma, della Costituzione;                                               &#13;
      che palesemente priva di consistenza è  la  pretesa  violazione    &#13;
 del  diritto  di  difesa,  atteso che la norma non può in alcun modo    &#13;
 essere interpretata come preclusiva del diritto del datore di  lavoro    &#13;
 di  impugnare  la sentenza di primo grado e di pretendere, in caso di    &#13;
 conferma del licenziamento  da  parte  del  giudice  di  appello,  la    &#13;
 restituzione  delle  somme  risarcitorie pagate in base alla sentenza    &#13;
 riformata e  dell'indennità  sostitutiva  della  reintegrazione  nel    &#13;
 posto ulteriormente pretesa dal lavoratore;                              &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9 delle Norme integrative per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 18, quinto  comma,  della  legge  20  maggio    &#13;
 1970,  n.  300,  (Norme  sulla  tutela  della libertà e dignità dei    &#13;
 lavoratori, della libertà sindacale e dell'attività  sindacale  nei    &#13;
 luoghi  di  lavoro  e  norme  sul  collocamento) nel testo modificato    &#13;
 dall'art. 1 della legge 11 maggio 1990, n. 108                           &#13;
  (Disciplina   dei   licenziamenti   individuali),   sollevata,    in    &#13;
 riferimento  agli artt. 3, 24 e 41 della Costituzione, dal Pretore di    &#13;
 Milano con l'ordinanza indicata in epigrafe.                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 marzo 1992.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 2 aprile 1992.                           &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
