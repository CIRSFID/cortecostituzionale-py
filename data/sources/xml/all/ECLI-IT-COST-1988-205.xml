<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>205</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:205</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/02/1988</data_decisione>
    <data_deposito>18/02/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, &#13;
 dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele &#13;
 PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, &#13;
 prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 892 e 894 del    &#13;
 codice civile, in  riferimento  all'art.  899,  ultimo  comma,  dello    &#13;
 stesso  codice,  promosso  con ordinanza emessa il 21 maggio 1987 dal    &#13;
 Pretore di Torino  nel  procedimento  civile  vertente  tra  Rappelli    &#13;
 Ferdinando  ed  altra  e  Borsano  Luigina,  iscritta  al  n. 362 del    &#13;
 registro ordinanze 1987 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 36, prima serie speciale dell'anno 1987;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 27 gennaio 1988 il Giudice    &#13;
 relatore Aldo Corasaniti;                                                &#13;
    Ritenuto  che  il  Pretore  di Torino, nel procedimento civile fra    &#13;
 Rappelli  Ferdinando   ed   altra   e   Borsano   Luigina,   vertente    &#13;
 sull'estirpazione  di  alberi  piantati a distanza inferiore a quella    &#13;
 legale, con ordinanza emessa il 21 maggio 1987 ha sollevato d'ufficio    &#13;
 questione di legittimità costituzionale, in riferimento agli artt. 3    &#13;
 e 24 Cost., della normativa risultante dagli artt.  892  e  894  cod.    &#13;
 civ.,  in  quanto  riconosce  al  vicino  il potere incondizionato di    &#13;
 esigere l'estirpazione degli alberi sorgenti a distanza  non  legale,    &#13;
 senza   subordinarlo   alla   preventiva  valutazione  dell'autorità    &#13;
 giudiziaria sulla necessità o convenienza del taglio, che è  invece    &#13;
 prevista dall'art. 899, comma terzo, cod.civ., in relazione al taglio    &#13;
 degli alberi in comunione piantati o sorgenti sul confine;               &#13;
      che, ad avviso del giudice a quo, tale diversità di trattamento    &#13;
 sarebbe irragionevole, poiché viene riconosciuta maggiore tutela  al    &#13;
 proprietario  il quale, piantando gli alberi sul confine, ha compiuto    &#13;
 un atto produttivo di  maggior  danno  per  il  vicino,  rispetto  al    &#13;
 proprietario  il quale abbia posto in essere la meno grave violazione    &#13;
 consistente nella piantagione di alberi a  distanza  non  legale  dal    &#13;
 confine,  dal momento che solo il primo può invocare, per evitare il    &#13;
 taglio, la valutazione del giudice  sulla  necessità  o  convenienza    &#13;
 dello stesso, con conseguente violazione degli artt. 3 e 24 Cost.;       &#13;
      che  è  intervenuto  il  Presidente del Consiglio dei ministri,    &#13;
 rappresentato dall'Avvocatura dello Stato, chiedendo che la questione    &#13;
 sia dichiarata infondata;                                                &#13;
    Considerato, quanto alla dedotta violazione dell'art. 3 Cost., che    &#13;
 le due situazioni poste a raffronto sono,  con  tutta  evidenza,  non    &#13;
 omogenee,  in  quanto  gli  artt. 892 e 894 cod. civ. contemplano una    &#13;
 ipotesi  di  comportamento  posto  in  essere   unilateralmente   dal    &#13;
 proprietario, violando un limite stabilito dalla legge al suo diritto    &#13;
 di proprietà esclusiva (piantagione di alberi a distanza dal confine    &#13;
 inferiore  a  quella prescritta dalla legge stessa) e ledendo in pari    &#13;
 tempo il diritto, anche esso  di  proprietà  esclusiva,  del  vicino    &#13;
 (all'osservanza  della  detta  distanza),  sicché  l'intervento  del    &#13;
 giudice è vincolato nel contenuto in quanto tende  a  restaurare  il    &#13;
 diritto  leso,  mentre  l'art. 899 cod. civ. contempla una ipotesi di    &#13;
 mancato accordo dei compartecipi relativamente a un atto di  gestione    &#13;
 di  cosa  comune (taglio di alberi in comunione sul confine), sicché    &#13;
 l'intervento del giudice implica valutazioni discrezionali in  quanto    &#13;
 tende a sostituire un atto di gestione concordato;                       &#13;
      che  non  ricorre  la  dedotta violazione del diritto di difesa,    &#13;
 poiché questo attiene alle  garanzie  processuali,  e  non  già  al    &#13;
 diverso atteggiarsi della disciplina del rapporto sostanziale;           &#13;
      che,   pertanto,   la  questione  va  dichiarata  manifestamente    &#13;
 infondata sotto entrambi i profili.                                      &#13;
    Visti  gli  artt. 26, comma secondo, della legge 11 marzo 1953, n.    &#13;
 87, e 9, comma secondo, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 892 e 894 cod. civ., in  riferimento  agli    &#13;
 artt.  3  e 24 Cost., sollevata dal Pretore di Torino con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte Costituzionale,    &#13;
 palazzo della Consulta il 10 febbraio 1988.                              &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CORASANITI                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 18 febbraio 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
