<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>557</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:557</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1990</data_decisione>
    <data_deposito>19/12/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. &#13;
 Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 11, primo e    &#13;
 quarto comma, del decreto-legge 3 maggio 1988, n. 140 (Misure urgenti    &#13;
 per  il personale della scuola), convertito, con modificazioni, nella    &#13;
 legge 4 luglio 1988, n. 246, promosso con ordinanza emessa l'8  marzo    &#13;
 1990  dal  Tribunale  amministrativo regionale del Veneto sui ricorsi    &#13;
 riuniti proposti da Riolfo Anna Maria ed altre contro il Provveditore    &#13;
 agli  studi  di  Venezia  ed  altri,  iscritta al n. 499 del registro    &#13;
 ordinanze 1990 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 33, prima serie speciale, dell'anno 1990.                             &#13;
    Visto  l'atto  di  costituzione  di  Riolfo  Anna  Maria ed altre,    &#13;
 nonché  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 28 novembre 1990 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto che il Tribunale amministrativo regionale del Veneto, con    &#13;
 ordinanza emessa l'8 marzo 1990, ha sollevato,  in  riferimento  agli    &#13;
 artt.   3   e   97  della  Costituzione,  questione  di  legittimità    &#13;
 costituzionale dell'art. 11, primo e quarto comma, del  decreto-legge    &#13;
 3  maggio  1988, n. 140, convertito, con modificazioni, nella legge 4    &#13;
 luglio 1988, n. 246: a) nella parte (primo comma) in cui il beneficio    &#13;
 della immissione in ruolo viene previsto a favore dei soli insegnanti    &#13;
 elementari in servizio  nell'anno  scolastico  1981-1982  con  nomina    &#13;
 conferita  dal  Provveditore  agli  studi, restando così esclusi gli    &#13;
 insegnanti elementari in servizio nello stesso  anno  scolastico  per    &#13;
 l'espletamento  di  supplenze  conferite  dal direttore didattico; b)    &#13;
 nella parte (quarto comma) in cui si fissa la data del  10  settembre    &#13;
 1982  come  termine oltre il quale non può tenersi conto dei servizi    &#13;
 di supplenza annuale prestati come insegnanti di scuola elementare;      &#13;
      che il giudice a quo sottolinea, sotto il primo profilo, come la    &#13;
 distinzione, sancita dal primo comma della norma  impugnata,  fra  le    &#13;
 due categorie di supplenti - a seconda che abbiano ottenuto la nomina    &#13;
 da parte del  Provveditore  agli  studi  ovvero  da  parte  del  Capo    &#13;
 d'istituto  -  si  radichi  su  un  dato meramente formale e pertanto    &#13;
 inidoneo a giustificare una disparità di trattamento tanto rilevante    &#13;
 ai fini della immissione in ruolo;                                       &#13;
      che,  a  parere  del  giudice  rimettente, anche l'altra ipotesi    &#13;
 d'immissione in ruolo prevista dal quarto  comma  della  disposizione    &#13;
 impugnata - laddove fissa il limite temporale, del 10 settembre 1982,    &#13;
 oltre il quale non può  tenersi  conto  del  servizio  di  supplenza    &#13;
 prestato - sarebbe non solo irrazionale ma anche lesiva del principio    &#13;
 costituzionale di  buon  andamento  della  pubblica  Amministrazione,    &#13;
 escludendo  dal  beneficio  dell'immissione  in  ruolo  insegnanti di    &#13;
 recente formazione e privilegiando personale che successivamente alla    &#13;
 data indicata potrebbe avere accumulato minore esperienza didattica;     &#13;
      che  nel giudizio è intervenuto il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura   dello   Stato,    &#13;
 chiedendo   che   le   questioni  vengano  dichiarate  manifestamente    &#13;
 infondate.                                                               &#13;
    Considerato  che questa Corte ha in diverse occasioni sottolineato    &#13;
 l'impossibilità di comparare la situazione dei docenti con  incarico    &#13;
 conferito  dal  Provveditore agli studi con quella dei beneficiari di    &#13;
 nomine effettuate da Capi d'istituto, per la diversità di  posizioni    &#13;
 espresse  dalla collocazione in differenti graduatorie nonché per la    &#13;
 garanzia procedimentale che assiste il conferimento dell'incarico  in    &#13;
 sede  provinciale  (ordinanze  n. 204 del 1990, n. 1053 del 1988 e n.    &#13;
 634 del 1987; sentenza n. 282 del 1987);                                 &#13;
      che,  quanto all'altro profilo d'incostituzionalità prospettato    &#13;
 dal giudice  a  quo,  questa  Corte  ha  anche  di  recente  ribadito    &#13;
 (sentenza  n.  190  del  1990) come non contrasti con il principio di    &#13;
 eguaglianza, né possa incorrere nella censura di irragionevolezza un    &#13;
 differenziato trattamento che venga applicato alla medesima categoria    &#13;
 di soggetti in momenti di tempo  diversi  ed  allorché  sopravvenuti    &#13;
 dati  legislativi  e  sociologici  abbiano  modificato il contesto di    &#13;
 riferimento.                                                             &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 11, primo e quarto comma, del  decreto-legge    &#13;
 3 maggio 1988, n. 140 (Misure urgenti per il personale della scuola),    &#13;
 convertito, con modificazioni, nella legge 4  luglio  1988,  n.  246,    &#13;
 sollevata,  in  riferimento agli artt. 3 e 97 della Costituzione, dal    &#13;
 Tribunale  amministrativo  regionale  del  Veneto   con   l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 dicembre 1990.                             &#13;
                          Il Presidente: CONSO                            &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 dicembre 1990.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
