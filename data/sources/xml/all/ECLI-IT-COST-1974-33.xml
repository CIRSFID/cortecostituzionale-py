<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1974</anno_pronuncia>
    <numero_pronuncia>33</numero_pronuncia>
    <ecli>ECLI:IT:COST:1974:33</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/02/1974</data_decisione>
    <data_deposito>13/02/1974</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. GIUSEPPE VERZÌ - Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI &#13;
 OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - &#13;
 Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.  67,  primo  &#13;
 comma,  del  r.d.  17 agosto 1935, n. 1765, dell'art.  16, primo comma,  &#13;
 della legge 19 gennaio 1963, n. 15, e dell'art. 112, primo  comma,  del  &#13;
 d.P.R.   30   giugno   1965,   n.   1124,   recanti   disposizioni  per  &#13;
 l'assicurazione obbligatoria contro  gli  infortuni  sul  lavoro  e  le  &#13;
 malattie  professionali,  promosso  con ordinanza emessa il 22 dicembre  &#13;
 1970 dal tribunale di Caltanissetta nel  procedimento  civile  vertente  &#13;
 tra Nicastro Giovanni e l'Istituto nazionale per l'assicurazione contro  &#13;
 gli  infortuni  sul  lavoro,  iscritta al n. 440 del registro ordinanze  &#13;
 1971 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  16  del  &#13;
 19 gennaio 1972.                                                         &#13;
     Visti  gli  atti  d'intervento  del  Presidente  del  Consiglio dei  &#13;
 ministri e di costituzione dell'Istituto nazionale per  l'assicurazione  &#13;
 contro gli infortuni sul lavoro;                                         &#13;
     udito  nell'udienza  pubblica  del  18  dicembre  1973  il  Giudice  &#13;
 relatore Paolo Rossi;                                                    &#13;
     uditi gli avvocati Valerio Flamini e Vincenzo Cataldi, per l'INAIL,  &#13;
 ed il sostituto avvocato generale dello Stato Giorgio Azzariti, per  il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso di una controversia promossa da Giovanni Nicastro contro  &#13;
 l'INAIL, al fine di ottenere il riconoscimento del diritto alla rendita  &#13;
 vitalizia conseguente a malattia professionale, eccependosi,  da  parte  &#13;
 convenuta,  l'inammissibilità  della domanda per avvenuta prescrizione  &#13;
 triennale ex art. 67, primo comma, del r.d. 17 agosto 1935, n. 1765, il  &#13;
 tribunale  di  Caltanissetta  ha  sollevato  questione  incidentale  di  &#13;
 legittimità  costituzionale  degli  artt.  67, primo comma, del citato  &#13;
 r.d. n. 1765 del 1935, 16, primo comma, della legge 19 gennaio 1963, n.  &#13;
 15, e 112, primo  comma,  del  d.P.R.  30  giugno  1965,  n.  1124,  in  &#13;
 riferimento all'art. 38, secondo comma, della Costituzione.              &#13;
     Osserva il giudice a quo che le norme impugnate, nella parte in cui  &#13;
 sanciscono  la  prescrizione  triennale  dell'azione  per conseguire la  &#13;
 rendita per inabilità permanente, contrastano con l'art.  38,  secondo  &#13;
 comma, della Costituzione, secondo cui i lavoratori hanno diritto a che  &#13;
 siano  loro  assicurati  adeguati mezzi di vita in caso di infortunio e  &#13;
 malattia,  giacché  il  diritto  così   garantito   dovrebbe   essere  &#13;
 annoverato tra quelli della personalità, di per sé imprescrittibili.   &#13;
     L'ordinanza di rimessione prosegue rilevando che seppure le singole  &#13;
 pretese   patrimoniali   possono  essere  prescrittibili,  non  sarebbe  &#13;
 legittima la prescrittibilità  del  diritto  al  riconoscimento  della  &#13;
 rendita.                                                                 &#13;
     È   intervenuto  in  giudizio  il  Presidente  del  Consiglio  dei  &#13;
 ministri, rappresentato e difeso dall'Avvocatura generale dello  Stato,  &#13;
 con atto del 26 luglio 1971, chiedendo dichiararsi l'infondatezza della  &#13;
 questione sollevata.                                                     &#13;
     Premette  la difesa dello Stato che la Corte costituzionale, con la  &#13;
 sentenza n.  116  del  1969,  illustrate  le  finalità  proprie  della  &#13;
 prescrizione  in  esame,  ha  espressamente  riconosciuto  che la norma  &#13;
 impugnata assicura pienamente "il diritto dei lavoratori  acché  siano  &#13;
 garantiti  mezzi  adeguati  alle  loro  esigenze  di  vita  in  caso di  &#13;
 malattia" (art. 38, secondo comma, della  Costituzione),  con  la  sola  &#13;
 eccezione  di  quella  parte  della norma che "dispone che l'azione per  &#13;
 conseguire dall'INAIL la rendita per inabilità permanente si prescrive  &#13;
 con il decorso del termine ivi previsto anche nel caso in cui entro  lo  &#13;
 stesso termine tale inabilità non abbia ridotto l'attitudine al lavoro  &#13;
 in  misura  superiore  al minimo indennizzabile", disposizione, questa,  &#13;
 dichiarata conseguentemente illegittima.                                 &#13;
     Prosegue quindi l'Avvocatura generale rilevando che l'articolo  38,  &#13;
 secondo comma, della Costituzione nulla dispone circa l'assoggettamento  &#13;
 a  prescrizione  del  diritto da esso garantito, mentre la stessa Corte  &#13;
 costituzionale ha già precisato, con la sentenza n. 63 del  1966  "che  &#13;
 la  garanzia  costituzionale  di  un diritto non vieta, di per sé, che  &#13;
 esso si estingua per il decorso del tempo... poiché se alla base della  &#13;
 prescrizione sta un'esigenza di certezza dei rapporti giuridici, questa  &#13;
 tocca di regola qualunque diritto, compresi  quelli  costituzionalmente  &#13;
 garantiti".  Né  la  Costituzione inibisce al legislatore ordinario di  &#13;
 stabilire termini prescrizionali diversi, come risulta  dalla  sentenza  &#13;
 n.  57  del 1962, mentre nessun argomento potrebbe trarsi dal principio  &#13;
 secondo cui sono imprescrittibili i diritti indisponibili, essendo esso  &#13;
 desunto dall'art. 2934 del codice civile, che è norma ordinaria.        &#13;
     Si è costituito innanzi a questa Corte l'INAIL, in persona del suo  &#13;
 Presidente, rappresentato e difeso dagli  avvocati  Valerio  Flamini  e  &#13;
 Vincenzo  Cataldi,  con atto del 22 gennaio 1972, chiedendo dichiararsi  &#13;
 l'infondatezza della questione sollevata.                                &#13;
     La difesa dell'INAIL, ricordate le precedenti decisioni della Corte  &#13;
 costituzionale nella materia in esame, osserva che le  norme  impugnate  &#13;
 si  inquadrano pienamente nell'ambito della tutela assicurata dall'art.  &#13;
 38, secondo comma,  della  Costituzione,  mentre  l'imposizione  di  un  &#13;
 termine   entro   cui   far   valere   il   diritto  al  riconoscimento  &#13;
 dell'indennizzabilità dell'evento lesivo, trova giustificazione  nella  &#13;
 necessità  che  gli accertamenti obiettivi per stabilire l'esistenza e  &#13;
 la gravità del danno non siano esperiti a  troppa  distanza  di  tempo  &#13;
 dall'evento  produttivo  del  danno.  La  determinazione  triennale del  &#13;
 termine di prescrizione appare congrua sia per  assicurare  l'effettivo  &#13;
 esercizio  del  diritto del lavoratore, sia per garantire la speditezza  &#13;
 del procedimento di erogazione delle prestazioni.                        &#13;
     Alla pubblica udienza le parti hanno  insistito  nelle  conclusioni  &#13;
 prese.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte  costituzionale  è chiamata a decidere se gli artt.  67,  &#13;
 primo comma, del r.d. 17 agosto 1935, n. 1765, 16, primo  comma,  della  &#13;
 legge  19 gennaio 1963, n. 15, e 112, primo comma, del d.P.R. 30 giugno  &#13;
 1965,  n.  1124,  nella  parte  in  cui  dispongono  che  l'azione  per  &#13;
 conseguire   dall'INAIL  la  rendita  per  l'inabilità  permanente  si  &#13;
 prescrive nel termine triennale, contrastino o meno con il diritto  dei  &#13;
 lavoratori  ad  ottenere adeguati mezzi di vita in caso di infortunio o  &#13;
 malattia, di  cui  all'art.  38,  secondo  comma,  della  Costituzione,  &#13;
 assumendosi    che    il   diritto   alla   rendita   dovrebbe   essere  &#13;
 imprescrittibile.                                                        &#13;
     La questione non è fondata.                                         &#13;
     Questa Corte, in numerose decisioni, ha già affermato  che  l'art.  &#13;
 38,  secondo  comma,  della  Costituzione  "attiene all'adeguamento dei  &#13;
 mezzi   di   carattere   previdenziale   alle    esigenze    di    vita  &#13;
 dell'infortunato,   piuttosto   che   alle   modalità   necessarie   a  &#13;
 conseguirli,  a  meno  che  esse  siano  tali  da   comprometterne   il  &#13;
 conseguimento".  Ha  pure  ritenuto  pienamente legittime le regole con  &#13;
 cui,  nel  rispetto  degli  altri   precetti   costituzionali,   "viene  &#13;
 condizionata  l'insorgenza  di dati diritti o di questi è disciplinato  &#13;
 l'esercizio" (sentenze n. 10 del 1970  e  n.  80  del  1971).  Da  tali  &#13;
 principi  consegue  facilmente la soluzione del caso in esame. La norma  &#13;
 impugnata, invero, nel sancire il  termine  triennale  di  prescrizione  &#13;
 decorrente,    di   regola,   dalla   manifestazione   della   malattia  &#13;
 professionale, assolve, nel  contempo,  a  due  esigenze  facenti  capo  &#13;
 all'INAIL  e all'assicurato: quella di mettere l'istituto in condizioni  &#13;
 di dar corso  alla  procedura  di  accertamento  dell'indennizzabilità  &#13;
 della  malattia  professionale,  poco  tempo  dopo che questa si sia in  &#13;
 fatto  manifestata,  e   quell'altra,   propria   dell'assicurato,   di  &#13;
 conseguire  con  prontezza  le  prestazioni,  tra  cui  la  rendita per  &#13;
 inabilità permanente.                                                   &#13;
     Non sussiste quindi l'illegittimità prospettata, posto che  questa  &#13;
 Corte,  esaminando  altra  volta  con  la sentenza n.   116 del 1969 le  &#13;
 disposizioni   oggi   nuovamente   impugnate,   ha   già    dichiarato  &#13;
 l'incostituzionalità  di  quella  parte  della norma che consentiva il  &#13;
 decorrere dei termini di prescrizione anche nei casi in cui la malattia  &#13;
 professionale non si fosse immediatamente manifestata in tutta  la  sua  &#13;
 gravità.  È stato così eliminato il rischio di una vanificazione dei  &#13;
 diritti dell'assicurato che sono concretamente esercitabili nel termine  &#13;
 triennale.  D'altronde non può ignorarsi il principio  generale,  pure  &#13;
 espressamente  affermato  da  questa  Corte,  secondo  cui,  essendo la  &#13;
 prescrizione un modo  generale  d'estinzione  dei  diritti,  ed  attesa  &#13;
 l'esigenza  di certezza che è alla base della prescrizione e che tocca  &#13;
 qualunque  diritto,  essa  opera  anche   nei   confronti   di   quelli  &#13;
 costituzionalmente garantiti (sentenza n. 63 del 1966).                  &#13;
     Neppure  può  farsi  riferimento ai diritti inviolabili dell'uomo,  &#13;
 come è adombrato nell'ordinanza  di  remissione  pur  senza  esplicito  &#13;
 richiamo  all'art.  2  della  Carta.  L'art.  2 proclama l'inderogabile  &#13;
 valore di quei sommi beni  che  formano  il  patrimonio  irretrattabile  &#13;
 della  persona  umana,  rimettendone la tutela specifica ad altre norme  &#13;
 costituzionali o a leggi ordinarie:  nella  specie  all'art.  38  della  &#13;
 Costituzione che non risulta affatto violato dalle norme impugnate.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 67, primo comma, del r.d. 17 agosto 1935, n. 1765,16, primo  &#13;
 comma, della legge 19 gennaio 1963, n. 15,  e  112,  primo  comma,  del  &#13;
 d.P.R.   30   giugno   1965,   n.   1124,   recanti   disposizioni  per  &#13;
 l'assicurazione obbligatoria contro  gli  infortuni  sul  lavoro  e  le  &#13;
 malattie  professionali,  sollevata, in riferimento all'art 38, secondo  &#13;
 comma,  della  Costituzione,   con   l'ordinanza   del   tribunale   di  &#13;
 Caltanissetta in epigrafe indicata.                                      &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 5 febbraio 1974.                              &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIUSEPPE  &#13;
                                    VERZÌ- GIOVANNI BATTISTA BENEDETTI -  &#13;
                                   LUIGI  OGGIONI  -  ANGELO  DE MARCO -  &#13;
                                   ERCOLE ROCCHETTI - ENZO  CAPALOZZA  -  &#13;
                                   VINCENZO  MICHELE  TRIMARCHI  - VEZIO  &#13;
                                   CRISAFULLI -  NICOLA  REALE  -  PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI.                                &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
