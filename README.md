# Corte Costituzionale

* requires Python 3.7+

* quickstart:

```
git clone git@gitlab.com:CIRSFID/cortecostituzionale-py.git
cd cortecostituzionale-py/development
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

* add the project's path to PYTHONPATH:
`export PYTHONPATH=${PYTHONPATH}:/path/to/the/repo/development/`

* add a `.env` file to `development/cortecostituzionale` to configure eXistDB:

```
EXIST_DB_HOST=yourhost           # defaults to "localhos"
EXIST_DB_PORT=portnumber        # defaults to 8080
EXIST_DB_USER=youruser          # defaults to "admin"
EXIST_DB_PASSWORD=yourpwd       # defaults to ""
EXIST_DB_COLLECTION=collection  # defaults to "corte-costituzionale"
```

* to run the scraper + the parser:
```
python cortecostituzionale/main.py
```

* pro-tip: add `export PYTHONPATH=${PYTHONPATH}:/path/to/the/repo/development/` to .bashrc

* cron: `0 0 * * * /path/to/the/repo/development/daemon.sh`

* to re-parse the files run `main.py --override`
