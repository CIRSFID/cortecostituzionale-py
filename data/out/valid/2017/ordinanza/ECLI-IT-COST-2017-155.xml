<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-07-04/155/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-07-04/155"/>
          <FRBRalias value="ECLI:IT:COST:2017:155" name="ECLI"/>
          <FRBRdate date="2017-07-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="155"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-07-04/155/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-07-04/155/ita@"/>
          <FRBRdate date="2017-07-04" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-07-04/155/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2017-07-04/155/ita@.xml"/>
          <FRBRdate date="2017-07-04" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2017-06-07" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2017-07-04" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>ammissibile</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="GROSSI" href="/akn/ontology/person/it/GROSSI" showAs="GROSSI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="paoloGrossi" href="/akn/ontology/person/it/paoloGrossi" showAs="Paolo GROSSI"/>
        <TLCPerson eId="alessandroCriscuolo" href="/akn/ontology/person/it/alessandroCriscuolo" showAs="Alessandro CRISCUOLO"/>
        <TLCPerson eId="filomenaPerrone" href="/akn/ontology/person/it/filomenaPerrone" showAs="Filomena PERRONE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2017</akn4cc:anno>
        <akn4cc:numero>155</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2017:155</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>GROSSI</akn4cc:presidente>
        <akn4cc:relatore>Alessandro Criscuolo</akn4cc:relatore>
        <akn4cc:data_decisione>2017-06-07</akn4cc:data_decisione>
        <akn4cc:data_deposito>2017-07-04</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>ammissibile</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#paoloGrossi">Paolo GROSSI</judge>; Giudici : <judge refersTo="#alessandroCriscuolo">Alessandro CRISCUOLO</judge>, <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>, <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della deliberazione del Senato della Repubblica (doc. IV-ter, n. 7-A) relativa alla insindacabilità, ai sensi dell'art. 68, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>, delle opinioni espresse dal senatore Antonio Gentile nei confronti di Franco Petramala, promosso dal Tribunale ordinario di Cosenza, seconda sezione civile, con ordinanza-ricorso depositata in cancelleria l'8 febbraio 2017 ed iscritta al n. 1 del registro conflitti tra poteri dello Stato 2017, fase di ammissibilità.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Udito nella camera di consiglio del 7 giugno 2017 il Giudice relatore Alessandro Criscuolo.
 Ritenuto che il Tribunale ordinario di Cosenza, seconda sezione civile, in composizione monocratica, con ordinanza-ricorso del 17 gennaio 2017 (d'ora in avanti: ricorso), depositata il successivo 8 febbraio 2017, ha sollevato conflitto di attribuzione tra poteri dello Stato in ordine alla deliberazione del 16 settembre 2015, con cui il Senato della Repubblica, approvando la proposta della Giunta delle elezioni e delle immunità parlamentari (doc. IV-ter, n. 7-A), ha affermato che le dichiarazioni rese da Antonio Gentile, senatore all'epoca dei fatti, nei confronti di Franco Petramala - per le quali pende, davanti a detto giudice, giudizio civile di risarcimento danni - concernono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni, come tali insindacabili ai sensi dell'art. 68, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che il ricorrente premette di essere investito di un giudizio civile per il risarcimento dei danni conseguenti «a diffamazione a mezzo stampa» instaurato da Franco Petramala e dall'Azienda sanitaria provinciale di Cosenza nei confronti, tra gli altri, del senatore Antonio Gentile;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che secondo quanto riferito dal medesimo giudice: a) il dottor Petramala ha proposto domanda di risarcimento dei danni derivati dal contenuto, da lui ritenuto diffamatorio, dell'editoriale dal titolo «Le gravi colpe della politica sul sistema sanitario calabrese», a firma del convenuto Antonio Gentile e pubblicato su «Il Quotidiano della Calabria» del 30 luglio 2009, del comunicato ANSA del 3 agosto 2009, poi in parte riportato in un articolo dal titolo «Gentile: Petramala sia rimosso», a firma dello stesso senatore e pubblicato su «Il Quotidiano della Calabria» del 4 agosto 2009, nonché dell'editoriale, a firma del citato parlamentare e pubblicato su «Il Quotidiano della Calabria» del 28 agosto 2009; b) in particolare, ad avviso dell'attore, sarebbero state lesive della sua immagine e onorabilità le affermazioni (analiticamente riportate nell'atto introduttivo) del senatore Gentile concernenti la presunta insussistenza dei requisiti di legge per la nomina dello stesso a direttore generale dell'Azienda sanitaria provinciale di Cosenza, nonché la gestione dell'azienda medesima; c) al riguardo, nell'editoriale del 30 luglio 2009, il senatore Gentile, aveva, tra l'altro, scritto «Ci sono due direttori, due manager, a Cosenza e Catanzaro, che amministrano senza avere i requisiti. Uno, addirittura è stato finanche candidato alle elezioni regionali, in spregio alla normativa vigente [...] E perché mai un direttore generale senza alcun requisito continua a governare nell'illegalità se non per il senso di impunità che lo pervade?»; nel comunicato Ansa del 3 agosto 2009, poi in parte riportato in un articolo de «Il Quotidiano della Calabria» del 4 agosto 2009, il senatore Gentile, aveva, tra l'altro, affermato: «La eco data dal più importante giornale italiano e da altri quotidiani nazionali alla gestione clientelare dell'Asp, alle autoassunzioni, agli accreditamenti facili, alle spese folli e alla veridicità di quanto più volte affermato dal centrodestra fa il paio con l'assenza di requisiti del direttore generale. Noi chiediamo che si interrompa subito questa gestione disastrosa e che si annullino, successivamente, tutti gli atti illegittimi che hanno prodotto benefici per persone senza titoli e senza diritti e che hanno dilatato ulteriormente la spesa sanitaria»; infine, nell'editoriale pubblicato su «Il Quotidiano della Calabria» il 28 agosto 2009, il senatore Gentile, aveva, tra l'altro, scritto: «Manager senza titoli e senza requisiti, protagonisti peraltro di violazioni aperte di legge, lasciati impunemente a gestire un territorio malato e senza alcuna interlocuzione degna di questo nome»; d) a seguito dell'eccezione d'insindacabilità ai sensi dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_68">art. 68 Cost.</ref> sollevata dal senatore Gentile, previa separazione della domanda risarcitoria proposta dal dottor Petramala nei confronti del parlamentare dalle altre e sospensione del giudizio, gli atti venivano trasmessi al Senato della Repubblica che, nella seduta del 16 settembre 2015, deliberava che le dichiarazioni, indicate dall'attore nel giudizio civile come generatrici del danno, costituivano opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni ed erano perciò, insindacabili, ai sensi dell'art. 68, primo comma, Cost.; e) nella proposta della Giunta, approvata dal Senato della Repubblica con delibera di insindacabilità del 16 settembre 2015, le dichiarazioni extra moenia in oggetto erano state funzionalmente collegate all'atto di sindacato ispettivo del 16 settembre 2009 - nel quale il senatore Gentile aveva sostenuto l'insussistenza dei requisiti di legge per la nomina del dottor Petramala a direttore generale dell'ASP di Cosenza, essendo stato candidato alle elezioni regionali tenutesi nel distretto nel quale svolgeva i propri compiti dirigenziali - che, pur essendo successivo rispetto agli articoli di stampa, sarebbe stato, tuttavia, prevedibile, sulla base di elementi «embrionali» contenuti in un precedente atto di sindacato ispettivo del 28 luglio 2009;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, ad avviso del giudice a quo, non sussisterebbero i presupposti della prerogativa di insindacabilità deliberata dal Senato della Repubblica, non potendosi individuare lo specifico «nesso funzionale» tra l'attività parlamentare e le dichiarazioni rese extra moenia che, alla luce della giurisprudenza costituzionale, è ravvisabile solo se sussista: a) un «legame di ordine temporale fra l'attività parlamentare e l'attività esterna», tale che questa venga ad assumere una finalità divulgativa della prima; b) una sostanziale «corrispondenza di significato tra le opinioni espresse nell'esercizio delle funzioni e gli atti esterni», al di là delle formule letterali usate, non essendo sufficiente né un semplice collegamento tematico o una corrispondenza contenutistica parziale, né un mero «contesto politico» entro cui le dichiarazioni extra moenia possano collocarsi, né, infine, il riferimento alla generica attività parlamentare o l'inerenza a temi di rilievo generale, seppur dibattuti in Parlamento (è richiamata, ex multis,  la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/144/!main">sentenza n. 144 del 2015</ref>);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che nel caso in esame, secondo il ricorrente, difetterebbe, in primo luogo, il legame temporale ovvero la «sostanziale contestualità» tra l'atto funzionale e le dichiarazioni esterne, ciò in quanto l'attività parlamentare ritenuta rilevante dal Senato (l'atto di sindacato ispettivo del 16 settembre 2009) si colloca a distanza di 48 giorni dalla prima esternazione e di 19 dall'ultima;
 che, peraltro, il giudice a quo ritiene, nella specie, insussistente quel rapporto di «sostanziale contestualità» ipotizzabile anche tra esternazioni extra moenia e atti tipici ad esse successivi, allorquando questi ultimi siano già preannunciati nelle prime o prevedibili sulla base di una specifica situazione, non essendo sufficiente la brevità del lasso temporale intercorrente tra le opinioni espresse al di fuori del Parlamento e gli atti di funzione (è richiamata la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2006/335/!main">sentenza n. 335 del 2006</ref>);
 che il Tribunale ricorrente - diversamente dal Senato della Repubblica - non ravvisa alcuna «connessione oggettiva» tra l'atto ispettivo del 16 settembre 2009 e quello del 28 luglio 2009, in quanto, mentre in quest'ultimo, il senatore Gentile ha denunciato una specifica condotta asseritamente non imparziale tenuta dal dottor Petramala nel concreto espletamento delle funzioni di direttore generale dell'ASP di Cosenza (ovvero la mancata stabilizzazione di un addetto stampa, «impegnato in politica con il Popolo della libertà»), nell'atto ispettivo del 16 settembre 2009, il senatore Gentile ha denunciato l'illegittimità della nomina del dottor Petramala a direttore generale per insussistenza dei requisiti di legge, in considerazione di una precedente candidatura di quest'ultimo alle elezioni regionali;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, pertanto, ad avviso del giudice ricorrente, difetterebbe il requisito della prevedibilità dell'atto ispettivo del 16 settembre 2009 sulla base dell'atto ispettivo del 28 luglio 2009 e, dunque, anche nell'interpretazione estensiva datane dalla Corte costituzionale nella <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2006/335/!main">sentenza n. 335 del 2006</ref>, il requisito del «collegamento temporale» tra l'attività parlamentare e le dichiarazioni espresse extra moenia;
 che il Tribunale ritiene, altresì, insussistente, nella specie, il requisito della «corrispondenza contenutistica» - che non è sufficiente se solo parziale - tra le opinioni espresse nell'esercizio delle funzioni e gli atti esterni;
 che - osserva il ricorrente - mentre l'atto ispettivo del 16 settembre 2009 affronta unicamente  la tematica della insussistenza dei requisiti di legge per la nomina del dottor Petramala a direttore generale dell'ASP di Cosenza, le esternazioni che hanno originato la pretesa risarcitoria presentano un contenuto più ampio, investendo vari aspetti dell'attività gestoria svolta in concreto da quest'ultimo nella detta qualità;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, quindi, ad avviso del ricorrente, la «corrispondenza contenutistica» sarebbe solo parziale, essendo affermata nella relazione della Giunta esclusivamente con riferimento alle opinioni espresse extra moenia - concernenti i «manager senza titoli e assunti illegalmente» - tralasciando il raffronto con le ulteriori dichiarazioni;
 che, infine, secondo il Tribunale ricorrente, non sarebbe ravvisabile alcun collegamento funzionale, stante la consistente distanza temporale, tra le esternazioni extra moenia in oggetto e gli altri atti di sindacato ispettivo del 9 febbraio 2010 e del 26 ottobre 2010, pure menzionati nella proposta della Giunta (dei quali non è riportato il contenuto);
 che il ricorrente conclude chiedendo che venga dichiarato che non spettava al Senato della Repubblica deliberare che quelle manifestate dal senatore Gentile negli articoli di stampa menzionati costituiscono opinioni espresse dal parlamentare nell'esercizio delle sue funzioni ai sensi dell'art. 68, primo comma, Cost. e che la deliberazione di insindacabilità venga annullata.
 Considerato che, in questa fase del giudizio, la Corte è chiamata, a norma dell'art. 37, terzo e quarto comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), a deliberare, senza contraddittorio, se il ricorso sia ammissibile in quanto vi sia la «materia di un conflitto la cui risoluzione spetti alla sua competenza», sussistendone i requisiti soggettivo ed oggettivo e restando impregiudicata ogni ulteriore questione, anche in punto di ammissibilità;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che la forma dell'ordinanza rivestita dall'atto introduttivo può ritenersi idonea ad instaurare il giudizio ove sussistano, come nella specie, gli estremi sostanziali di un valido ricorso (tra le ultime, <mref><ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2016/139/!main">ordinanze n. 139</ref> e <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2016/91/!main">91 del 2016</ref></mref>, n. 137 del 2015);</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, sotto il profilo del requisito soggettivo, va riconosciuta la legittimazione del Tribunale ordinario di Cosenza a sollevare conflitto, in quanto organo giurisdizionale, in posizione di indipendenza costituzionalmente garantita, competente a dichiarare definitivamente, nell'esercizio delle funzioni attribuitegli, la volontà del potere cui appartiene;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, parimenti, deve essere riconosciuta la legittimazione del Senato della Repubblica ad essere parte del presente conflitto, quale organo competente a dichiarare in modo definitivo la propria volontà in ordine all'applicazione dell'art. 68, primo comma, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, per quanto attiene al profilo oggettivo, il giudice ricorrente lamenta la lesione della propria sfera di attribuzione, costituzionalmente garantita, in conseguenza di un esercizio ritenuto illegittimo, per inesistenza dei relativi presupposti, del potere spettante al Senato della Repubblica di dichiarare l'insindacabilità delle opinioni espresse da un membro di quel ramo del Parlamento ai sensi dell'art. 68, primo comma, Cost.;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, dunque, esiste la materia di un conflitto la cui risoluzione spetta alla competenza di questa Corte.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 1) dichiara ammissibile, ai sensi dell'<ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_37">art. 37 della legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), il conflitto di attribuzione tra poteri dello Stato, proposto dal Tribunale ordinario di Cosenza, seconda sezione civile, in composizione monocratica nei confronti del Senato della Repubblica, con il ricorso indicato in epigrafe;</block>
        <paragraph>
          <num>2)</num>
          <content>
            <p> dispone:</p>
          </content>
        </paragraph>
        <p>
          <docAuthority>a) che la cancelleria della Corte costituzionale dia immediata comunicazione della presente ordinanza al ricorrente Tribunale ordinario di Cosenza, seconda sezione civile, in composizione monocratica;</docAuthority>
        </p>
        <p>
          <docAuthority>b) che il ricorso e la presente ordinanza siano notificati, a cura del ricorrente, al Senato della Repubblica, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione di cui al punto a), per essere successivamente depositati, con la prova dell'avvenuta notifica, nella cancelleria di questa Corte entro il termine di trenta giorni previsto dall'art. 24, comma 3, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</docAuthority>
        </p>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2017-06-07">7 giugno 2017</date>.</block>
      <p>F.to:
 <signature><judge refersTo="#paoloGrossi" as="#presidente">Paolo GROSSI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#alessandroCriscuolo" as="#relatore">Alessandro CRISCUOLO</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#filomenaPerrone" as="#cancelliere">Filomena PERRONE</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2017-07-04">4 luglio 2017</date>.
 Il Cancelliere
 F.to: Filomena PERRONE</p>
    </conclusions>
  </judgment>
</akomaNtoso>
