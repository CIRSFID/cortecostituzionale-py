<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>99</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:99</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Giuseppe Chiarelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/06/1970</data_decisione>
    <data_deposito>16/06/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale dell'art.   1304 del  &#13;
 codice della navigazione e degli artt. 1 e  2  del  R.D.L.  6  febbraio  &#13;
 1936, n. 337 (norme per la risoluzione del rapporto di lavoro marittimo  &#13;
 a  tempo indeterminato), promosso con ordinanza emessa il 3 maggio 1968  &#13;
 dalla Corte di cassazione nel procedimento civile vertente tra Di Carlo  &#13;
 Renato e la Società italiana radio marittima (SIRM),  iscritta  al  n.  &#13;
 237  del  registro ordinanze 1968 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 329 del 28 dicembre 1968.                            &#13;
     Visti gli atti di costituzione di Di Carlo Renato e della SIRM;      &#13;
     udito nell'udienza pubblica del 20 maggio 1970 il Giudice  relatore  &#13;
 Giuseppe Chiarelli;                                                      &#13;
     uditi  l'avv.  Benedetto  Bussi  per  il Di Carlo, e l'avv. Filippo  &#13;
 Biamonti, per la SIRM.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Renato Di Carlo, con citazione 11 aprile  1962,  premesso  di  aver  &#13;
 prestato  servizio  come  ufficiale  marconista  alle  dipendenze della  &#13;
 Società Italiana Radio Marittima (SIRM) dal 1927 al 1962, conveniva in  &#13;
 giudizio la società davanti al tribunale  di  Roma  per  il  pagamento  &#13;
 delle  differenze  di  liquidazione che assumeva gli fossero dovute. In  &#13;
 merito alla indennità di anzianità fondava la domanda sulla legge  18  &#13;
 dicembre 1960, n. 1561, contenente norme relative alla detta indennità  &#13;
 spettante agli impiegati privati.                                        &#13;
     La  SIRM,  costituitasi  in  giudizio,  eccepiva  sul  punto che le  &#13;
 indennità di anzianità e  di  preavviso  erano  state  legittimamente  &#13;
 computate  in  aderenza  al  contratto  collettivo nazionale 29 ottobre  &#13;
 1959, non essendo nella specie applicabile la detta legge n.  1561  del  &#13;
 1960  (modificativa  dell'art.  10 della legge sull'impiego privato del  &#13;
 1924), ai sensi del combinato disposto dell'art. 1304 del codice  della  &#13;
 navigazione e dell'art. 1 del R.D.L. 6 febbraio 1936, n. 337.            &#13;
     Il  tribunale  respingeva  la  domanda  e,  in  seguito  al gravame  &#13;
 proposto dal Di Carlo, la Corte di appello di  Roma,  sez.  mag.  lav.,  &#13;
 ritenuto  che  il  rapporto di lavoro intercorso tra le parti avesse la  &#13;
 natura di contratto di arruolamento e non di  impiego  privato,  e  che  &#13;
 pertanto  non  fosse  ad  esso  applicabile  la legge n. 1561 del 1960,  &#13;
 rigettava l'appello.                                                     &#13;
     Il  Di  Carlo  ricorreva  in  Cassazione,  deducendo  tra   l'altro  &#13;
 l'illegittimità  costituzionale  degli artt. 1 e 2 del decreto legge 6  &#13;
 febbraio 1936, n. 337, e dell'art.  1304 del codice della  navigazione,  &#13;
 in riferimento agli artt. 3, 35, 36 e 39 della Costituzione.             &#13;
     La  Corte  di  cassazione, con ordinanza 3 maggio 1968, ha ritenuto  &#13;
 non manifestamente infondata la questione solo in riferimento  all'art.  &#13;
 39  della Costituzione, osservando che gli artt. 1 e 2 D.L. n.  337 del  &#13;
 1936, sia  che  il  contenuto  di  essi  venga  riferito  ai  contratti  &#13;
 collettivi  corporativi  o  a quelli post - corporativi, "verrebbero ad  &#13;
 escludere la potestà del legislatore  di  regolare  la  materia  delle  &#13;
 indennità  di anzianità spettanti ai prestatori d'opera nel contratto  &#13;
 di arruolamento, con ciò affermandosi  la  esistenza  di  una  riserva  &#13;
 normativa  o  contrattuale  in  favore  dei  sindacati,  che  non  può  &#13;
 ritenersi contenuta nell'art. 39 della Costituzione".                    &#13;
     Si è costituito nel presente giudizio il Di Carlo, rappresentato e  &#13;
 difeso dall'avv. Benedetto Bussi, con deduzioni depositate l'11 gennaio  &#13;
 1969. Anche in esse si sostiene che gli artt.  1304  del  codice  della  &#13;
 navigazione  e  1  e  2  del  predetto  D.L.,  escludendo il potere del  &#13;
 legislatore di regolare uno degli aspetti fondamentali,  dal  punto  di  &#13;
 vista  sociale  ed  economico,  del  rapporto  di  lavoro del personale  &#13;
 arruolato, creano una riserva normativa e  contrattuale  a  favore  dei  &#13;
 sindacati, in contrasto con l'art. 39 della Costituzione.                &#13;
     Si  è  anche  costituita  la  SIRM,  rappresentata  e difesa dagli  &#13;
 avvocati Enrico e Filippo Biamonti,  con  deduzioni  depositate  il  14  &#13;
 gennaio  1969.  Ivi  si  afferma  che  le  disposizioni impugnate hanno  &#13;
 perseguito unicamente la finalità di svincolare i rapporti  di  lavoro  &#13;
 marittimo  dalla  legge  sull'impiego  privato,  per  riconoscere  alle  &#13;
 categorie di lavoratori interessate un  trattamento  speciale,  dettato  &#13;
 dalla  peculiarità del rapporto. Le dette disposizioni non hanno però  &#13;
 costituito una riserva di regolamento collettivo,  non  precludendo  al  &#13;
 legislatore   la   emanazione   di   successive   norme   inderogabili,  &#13;
 modificative della attuale disciplina; ma tale eventualità non  si  è  &#13;
 verificata con la legge n. 1561 del 1960, che prevede in modo esplicito  &#13;
 una modifica soltanto della legge sull'impiego privato.                  &#13;
     Le  difese  di  entrambe  le parti hanno successivamente presentato  &#13;
 memorie illustrative.  In quella della SIRM si osserva tra l'altro  che  &#13;
 la  questione  di  legittimità  costituzionale sarebbe infondata anche  &#13;
 rispetto agli artt. 3, 35 e 36 della Costituzione,  in  riferimento  ai  &#13;
 quali era stata pure prospettata dalla difesa del Di Carlo.              &#13;
     Nella   discussione  orale  sono  stati  sviluppati  gli  argomenti  &#13;
 rispettivamente dedotti.<diritto>Considerato in diritto</diritto>:                          &#13;
     La questione di legittimità costituzionale  dell'art.    1304  del  &#13;
 codice  della  navigazione  e  degli  artt.  1  e 2 del decreto legge 6  &#13;
 febbraio 1936, n. 337, è stata proposta, dall'ordinanza di  rimessione  &#13;
 a  questa  Corte,  con  riferimento all'art. 39 della Costituzione e in  &#13;
 tali termini va decisa.                                                  &#13;
     Ritiene la Corte che nelle norme impugnate non possa ravvisarsi una  &#13;
 riserva normativa a favore della contrattazione collettiva.              &#13;
     L'art.  1 del predetto decreto legge, richiamato nell'art. 1304 del  &#13;
 codice della navigazione, si limita a escludere l'applicabilità  della  &#13;
 legge  sull'impiego privato al contratto di lavoro marittimo. Ma l'aver  &#13;
 sottratto  tale  rapporto,  in  considerazione   dei   suoi   peculiari  &#13;
 caratteri,  alla  sfera  di  applicazione  di  detta legge non implica,  &#13;
 ovviamente, rinuncia del legislatore  a  regolare  la  materia,  e  non  &#13;
 costituisce  pertanto impedimento a una disciplina legislativa di esso,  &#13;
 che è stata compresa,  successivamente  al  decreto  del  1936,  nello  &#13;
 stesso codice della navigazione.                                         &#13;
     Un  simile  impedimento  non  risulta  neanche, per quanto riguarda  &#13;
 l'indennità  di  anzianità  e  il  preavviso  di  licenziamento,  dal  &#13;
 collegamento  dell'art.  1  con  l'art.  2 del D.L. n. 337 del 1936. La  &#13;
 disposizione contenuta in quest'ultimo articolo  rimanda  ai  contratti  &#13;
 collettivi la determinazione della misura dell'indennità e del termine  &#13;
 di  preavviso,  nonché  delle  relative  norme e modalità.   Con tale  &#13;
 richiamo  il  legislatore  non  ha   attribuito   agli   organi   della  &#13;
 contrattazione  collettiva  (sindacati) un potere normativo sottratto a  &#13;
 ogni proprio successivo intervento. Il riferimento alla  contrattazione  &#13;
 collettiva,  che  dovrà sempre operare nei limiti delle norme di legge  &#13;
 relative ai predetti istituti (v.  artt.  351  -  362  cod.  nav.)  non  &#13;
 costituisce pertanto una riserva normativa a favore dei sindacati, allo  &#13;
 stesso  modo  che  non  costituisce riserva il riferimento ai contratti  &#13;
 collettivi di cui agli artt. 2118 e 2120 del codice civile.              &#13;
     Nel caso in esame, il legislatore,  col  richiamo  alla  disciplina  &#13;
 collettiva,  come  col  richiamo,  contenuto  nello  stesso  art. 2, ai  &#13;
 regolamenti  organici  delle  società  sovvenzionate  e  ai  contratti  &#13;
 soggetti  ad  approvazione ministeriale, non si è spogliato del potere  &#13;
 di  disciplinare  direttamente  la  materia;  né    dall'aver  escluso  &#13;
 l'applicabilità  al rapporto della legge sull'impiego privato derivava  &#13;
 alcun limite all'intervento dello stesso legislatore, il quale  avrebbe  &#13;
 sempre  potuto  disporre,  secondo le sue discrezionali vatutazioni, un  &#13;
 adeguamento dell'indennità di anzianità alla misura stabilita con  le  &#13;
 modifiche  apportate  alla legge sull'impiego privato con la successiva  &#13;
 legge 18 dicembre 1960, n. 1561,  per  se  stessa  non  applicabile  al  &#13;
 rapporto di lavoro marittimo.                                            &#13;
     In  fatto  un  adeguamento  è  avvenuto  con  successivi contratti  &#13;
 collettivi. Se, nel caso di  specie,  di  tale  adeguamento  non  possa  &#13;
 giovarsi  il  lavoratore  è  questione  non rapportabile al denunziato  &#13;
 vizio di legittimità costituzionale per violazione dell'art. 39  della  &#13;
 Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 1 e 2 del R.D.L. 6 febbraio 1936,  n.  337  (Norme  per  la  &#13;
 risoluzione  del  rapporto di lavoro marittimo a tempo indeterminato) e  &#13;
 dell'art. 1304 del codice della navigazione, sollevata con  l'ordinanza  &#13;
 di cui in epigrafe, in riferimento all'art. 39 della Costituzione.       &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 4 giugno 1970.                                &#13;
                                   GIUSEPPE BRANCA - MICHELE FRAGALI   -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -    GIOVANNI    BATTISTA  &#13;
                                   BENEDETTI - FRANCESCO PAOLO BONIFACIO  &#13;
                                   -  LUIGI  OGGIONI - ANGELO DE MARCO -  &#13;
                                   ERCOLE  ROCCHETTI - ENZO CAPALOZZA  -  &#13;
                                   VEZIO CRISAFULLI  -  NICOLA  REALE  -  &#13;
                                   PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
