<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1971</anno_pronuncia>
    <numero_pronuncia>188</numero_pronuncia>
    <ecli>ECLI:IT:COST:1971:188</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FRAGALI</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/11/1971</data_decisione>
    <data_deposito>17/11/1971</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. MICHELE FRAGALI, Presidente - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - &#13;
 Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - &#13;
 Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti di legittimità costituzionale dell'art. 596,  &#13;
 primo comma, del codice penale, nonché dell'art. 5, primo comma, lett.  &#13;
 d, della legge di delegazione per la concessione d'amnistia  e  indulto  &#13;
 21  maggio  1970,  n.  282,  e  dell'art.  5,  primo  comma, lett. d, e  &#13;
 penultimo comma, del relativo decreto presidenziale 22 maggio 1970,  n.  &#13;
 283, promossi con le seguenti ordinanze:                                 &#13;
     1)  ordinanza emessa il 1 ottobre 1970 dal tribunale di Bologna nel  &#13;
 procedimento penale a carico di Borghi Gianfranco, iscritta al  n.  121  &#13;
 del registro ordinanze 1971 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 106 del 28 aprile 1971;                                    &#13;
     2)  ordinanza  emessa  l'8  marzo  1971  dal pretore di Venasca nel  &#13;
 procedimento penale a carico di Panero Giuseppina, iscritta al  n.  168  &#13;
 del registro ordinanze 1971 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 151 del 16 giugno 1971;                                    &#13;
     3)  ordinanza  emessa  il  23  marzo 1971 dal tribunale di Roma nel  &#13;
 procedimento penale a carico di Ravelli Rocco e Foti Giovanni Battista,  &#13;
 iscritta al n. 174 del  registro  ordinanze  1971  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 163 del 30 giugno 1971.           &#13;
     Udito  nella  camera  di  consiglio  del 28 ottobre 1971 il Giudice  &#13;
 relatore Costantino Mortati.                                             &#13;
     Ritenuto che con ordinanza 1 ottobre 1970,  pronunciata  nel  corso  &#13;
 del  procedimento  penale  contro  Borghi  Gianfranco,  il tribunale di  &#13;
 Bologna ha sollevato questione di legittimità costituzionale dell'art.  &#13;
 5, lett. d, del d.P.R. 22 maggio 1970, n. 283, recante  concessione  di  &#13;
 amnistia e indulto, nella parte in cui esclude dall'amnistia il delitto  &#13;
 di diffamazione commesso col mezzo della stampa e mediante attribuzione  &#13;
 di  un  fatto  determinato,  quando  sia  stata concessa la facoltà di  &#13;
 prova, in riferimento all'art. 3 della Costituzione;                     &#13;
     che  con  ordinanza  8  marzo  1971,  pronunciata  nel  corso   del  &#13;
 procedimento  penale contro Panero Giuseppina, il pretore di Venasca ha  &#13;
 sollevato  questione  di  legittimità  costituzionale   dell'art.   5,  &#13;
 penultimo comma, dello stesso decreto presidenziale, nella parte in cui  &#13;
 esclude  dall'amnistia il reato di frode in commercio (art. 515, codice  &#13;
 penale), in riferimento all'articolo 3 della Costituzione;               &#13;
     che  con  ordinanza  23  marzo  1971,  pronunciata  nel  corso  del  &#13;
 procedimento penale contro Ravelli Rocco e Foti Giovanni  Battista,  il  &#13;
 tribunale di Roma ha sollevato questione di legittimità costituzionale  &#13;
 dell'art. 596, primo comma, del codice penale, che preclude di dedurre,  &#13;
 a  discolpa  dell'imputato  di diffamazione, ogni prova della verità o  &#13;
 della  notorietà  del  fatto  attribuito  alla  persona   offesa,   in  &#13;
 riferimento  all'art.  21,  primo  comma,  della  Costituzione, nonché  &#13;
 dell'art.  5,  lett.  d,  della  legge  21  maggio  1970,  n.  282,  di  &#13;
 delegazione  per  la  concessione di amnistia e indulto, nella parte in  &#13;
 cui esclude dall'amnistia il reato di diffamazione commesso  col  mezzo  &#13;
 della  stampa  e  mediante attribuzione di un fatto determinato, quando  &#13;
 sia stata data facoltà di prova,  in  riferimento  all'art.  3,  primo  &#13;
 comma, della Costituzione;                                               &#13;
     che  nessuno si è costituito nei relativi giudizi, i quali possono  &#13;
 essere riuniti avendo oggetti attinenti alla stessa materia.             &#13;
     Considerato che, dopo la pronuncia delle ordinanze  di  rimessione,  &#13;
 con  la  sentenza  n. 175 del 5 luglio 1971, questa Corte ha dichiarato  &#13;
 infondate le questioni come sopra proposte, che erano  state  sollevate  &#13;
 anche da altri giudici in termini strettamente corrispondenti;           &#13;
     che  non sussistono ragioni che inducano a modificare la precedente  &#13;
 decisione.                                                               &#13;
     Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.  &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi dinanzi a  &#13;
 questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza delle questioni di legittimità  &#13;
 costituzionale dell'art. 596, primo comma, del codice  penale,  nonché  &#13;
 dell'art.  5,  primo  comma, lett. d, della legge di delegazione per la  &#13;
 concessione di amnistia e indulto 21 maggio 1970, n. 282,  e  dell'art.  &#13;
 5,  primo  comma,  lett.  d,  e  penultimo  comma, del relativo decreto  &#13;
 presidenziale 22 maggio 1970, n.  283, sollevate con le  ordinanze  dei  &#13;
 tribunali  di  Bologna  e  di  Roma  e  del  pretore  di Venasca e già  &#13;
 dichiarate non fondate con sentenza n. 175 del 5 luglio 1971.            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 10 novembre 1971.       &#13;
                                   MICHELE  FRAGALI - COSTANTINO MORTATI  &#13;
                                   -  GIUSEPPE  CHIARELLI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   - NICOLA REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
