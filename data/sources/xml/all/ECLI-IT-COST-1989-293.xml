<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>293</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:293</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/05/1989</data_decisione>
    <data_deposito>25/05/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, avv. Mauro &#13;
 FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 9, della legge    &#13;
 27 dicembre 1956, n. 1423 (Misure di prevenzione nei confronti  delle    &#13;
 persone   pericolose   per   la  sicurezza  pubblica  e  la  pubblica    &#13;
 moralità), come modificato dall'art. 8 della legge 14 ottobre  1974,    &#13;
 n.  497  (Nuove norme contro la criminalità), promosso con ordinanza    &#13;
 emessa il 13 aprile 1988 dal  Pretore  di  Cosenza  nel  procedimento    &#13;
 penale  a  carico  di  Musacco  Mario,  iscritta al n. 4 del registro    &#13;
 ordinanze 1989 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 4, prima serie speciale, dell'anno 1989.                              &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 12 aprile 1989 il Giudice    &#13;
 relatore Ettore Gallo;                                                   &#13;
    Ritenuto  che  il  Pretore di Cosenza -con ordinanza del 13 aprile    &#13;
 1988, pervenuta alla Corte  il  3  gennaio  1989-  ha  sollevato,  in    &#13;
 riferimento   all'art.   13   della  Costituzione,  la  questione  di    &#13;
 legittimità costituzionale dell'art. 9 della legge 27 dicembre 1956,    &#13;
 n. 1423 (Misure di prevenzione nei confronti delle persone pericolose    &#13;
 per la sicurezza pubblica e la pubblica moralità),  come  modificato    &#13;
 dall'art.  8  della legge 14 ottobre 1974, n. 497 (Nuove norme contro    &#13;
 la criminalità);                                                        &#13;
      che  la norma impugnata è censurata nella parte in cui consente    &#13;
 l'arresto per violazione degli obblighi  inerenti  alla  sorveglianza    &#13;
 speciale anche fuori dei casi di flagranza;                              &#13;
      che   tale   previsione   -   se   è   giustificata  quando  la    &#13;
 contravvenzione agli obblighi  inerenti  alla  sorveglianza  speciale    &#13;
 abbia  carattere  permanente  - limita invece oltre misura il diritto    &#13;
 allo status libertatis, anche sul piano della repressione per  coloro    &#13;
 che  siano  sottoposti  a  misure  di prevenzione, quando l'evento da    &#13;
 reprimere ha  carattere  istantaneo  e  per  di  più  configura  una    &#13;
 fattispecie in sé contravvenzionale (guida di autoveicolo);             &#13;
    Considerato  che questioni analoghe sono state già dichiarate non    &#13;
 fondate dalla Corte con le sentenze nn. 64/1970 e 126/1983;              &#13;
      che  in particolare la sentenza n. 64/1970 ha non solo precisato    &#13;
 che nella norma  impugnata  ricorrono  tutti  gli  estremi  richiesti    &#13;
 dall'art.13   della   Costituzione,  ma  anche  chiarito  che,  nella    &#13;
 specifica  situazione  descritta  in  detta   norma   è   pienamente    &#13;
 ragionevole la previsione che possa ricorrere, fuori della flagranza,    &#13;
 "una  situazione  di  urgenza  che  renda   necessario   l'intervento    &#13;
 restrittivo  della  libertà  personale, ove si tenga presente che il    &#13;
 provvedimento si rivolge a soggetti nei cui confronti  già  si  sono    &#13;
 verificate le condizioni di pericolosità sociale... e che hanno, per    &#13;
 di più,  contravvenuto  agli  obblighi  relativi;  che  hanno  cioè    &#13;
 trasgredito ad una - quale che sia - delle prescrizioni stabilite nel    &#13;
 provvedimento di applicazione della sorveglianza speciale";              &#13;
      che  dall'ordinanza  di  rimessione  non  emergono  elementi che    &#13;
 rendano opportuno un riesame delle decisioni citate;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  infondata  la  questione  di legittimità    &#13;
 costituzionale dell'art. 9 della legge 27  dicembre  1956,  n.  1423,    &#13;
 come  modificato  dall'art.  8  della  legge 14 ottobre 1974, n. 497,    &#13;
 sollevata, in riferimento all'art. 13 della Costituzione dal  Pretore    &#13;
 di Cosenza con l'ordinanza indicata in epigrafe.                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 17 maggio 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 25 maggio 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
