<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>701</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:701</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/06/1988</data_decisione>
    <data_deposito>23/06/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art.139, n.1, della    &#13;
 legge  16  febbraio  1913,  n.  89  (Legge  notarile),  promosso  con    &#13;
 ordinanza  emessa  il  3 luglio 1987 dal Giudice Istruttore presso il    &#13;
 Tribunale di Roma nel procedimento penale a carico di Bellelli Elvira    &#13;
 ed  altri, iscritta al n.717 del registro ordinanze 1987 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 51 prima  ss.  dell'anno    &#13;
 1987;                                                                    &#13;
    Visti gli atti di costituzione di Bellelli Elvira ed altri nonché    &#13;
 l'atto di intervento del Presidente del Consiglio dei ministri;          &#13;
    Udito nell'udienza pubblica del 10 maggio 1988 il Giudice relatore    &#13;
 Ettore Gallo;                                                            &#13;
    Uditi  l'avv.  Marco  Zanotti  per  Bellelli  Elvira  ed  altri  e    &#13;
 l'Avvocato dello Stato Gaetano Zotta per il Presidente del  Consiglio    &#13;
 dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Il  Giudice  Istruttore  presso  il  Tribunale di Roma, con    &#13;
 ordinanza  3  luglio  1987,  sollevava  questione   di   legittimità    &#13;
 costituzionale  dell'art.139  n.1  della  l.  16  febbraio 1913 n. 89    &#13;
 (legge notarile), con riferimento all'art.27, secondo comma, Cost.       &#13;
    Riferiva  il  giudice nell'ordinanza che, avendo emesso mandato di    &#13;
 cattura nei confronti di tre  persone  esercenti  la  professione  di    &#13;
 notaio, imputate di corruzione continuata per atti contrari ai doveri    &#13;
 d'ufficio,  falsità  ideologica  in  atti  pubblici,   aggravata   e    &#13;
 continuata,  e  associazione per delinquere, aveva altresì disposto,    &#13;
 quale  obbligatoria  conseguenza  ex  lege,  prevista   dalla   norma    &#13;
 impugnata, l'inabilitazione all'esercizio della professione notarile.    &#13;
 Successivamente, però, aveva  concesso  agli  imputati  la  libertà    &#13;
 provvisoria.                                                             &#13;
    A  quel punto, la difesa aveva chiesto la revoca del provvedimento    &#13;
 di inabilitazione sostenendo che, a seguito della  concessione  della    &#13;
 libertà  provvisoria,  il  mandato di cattura doveva ritenersi ormai    &#13;
 inoperante. Ma il  giudice  istruttore,  assumendo,  invece,  che  il    &#13;
 mandato   di   cattura,  nonostante  la  concessione  del  beneficio,    &#13;
 conservava la sua validità giuridica come titolo costituito  fino  a    &#13;
 quando  non  fosse  revocato  per  cause varie, oppure sostituito con    &#13;
 mandato di comparizione ex art. 260,  secondo  comma,  cod.proc.pen.,    &#13;
 respingeva l'istanza.                                                    &#13;
    Contestualmente,   però,   riconoscendo   che   il  principio  di    &#13;
 presunzione di non colpevolezza dell'imputato, di  cui  all'art.  27,    &#13;
 secondo  comma,  Cost. potrebbe effettivamente comportare un sospetto    &#13;
 d'illegittimità costituzionale nei confronti della norma  impugnata,    &#13;
 che  impone  obbligatoriamente  una sanzione interdittiva, riferibile    &#13;
 soltanto alla condanna definitiva, sollevava d'ufficio  la  questione    &#13;
 di cui sopra, sospendendo il procedimento in riferimento ad essa.        &#13;
    2.  -  Si  costituivano,  nel  giudizio innanzi a questa Corte, le    &#13;
 parti private, rappresentate e difese dal prof. avv. Nicola Mazzacuva    &#13;
 e dall'avvocato Marco Zanotti, ambo del foro di Bologna.                 &#13;
    Le   difese,   associandosi  all'argomento  espresso  dal  Giudice    &#13;
 nell'ordinanza, mettono a confronto la giurisprudenza contraria delle    &#13;
 Sezioni  Unite civili della Corte di Cassazione con quella favorevole    &#13;
 del Consiglio di Stato, osservando  che,  in  definitiva,  le  stesse    &#13;
 Sezioni   Unite   hanno   finito  per  riconoscere  la  validità  di    &#13;
 quest'ultima,  limitatamente,  però,  alla  posizione  del  pubblico    &#13;
 impiegato: e ciò perché l'attività di questi è di regola soggetta    &#13;
 al  controllo  gerarchico,  diretto  ed  immediato,  del   superiore.    &#13;
 Osservano,  però,  le difese che il punto non è questo, ma soltanto    &#13;
 quello di decidere  se  la  concessione  della  libertà  provvisoria    &#13;
 equivalga  o  non  a revoca del mandato di cattura. Se la risposta è    &#13;
 negativa, il  pubblico  impiegato  non  può  godere  di  trattamento    &#13;
 privilegiato  ma,  se è positiva, non è possibile, senza violare la    &#13;
 legge  ordinaria  e  la  stessa  Costituzione   (art.   3),   negarne    &#13;
 applicazione  ai  professionisti.  E ciò a prescindere dal fatto che    &#13;
 poi, anche per questi ultimi, è previsto il controllo  degli  Organi    &#13;
 disciplinari  anche prima della ripresa dell'attività professionale,    &#13;
 data la possibilità di  irrogare  una  misura  sospensiva  cautelare    &#13;
 facoltativa.                                                             &#13;
    D'altra  parte,  la  contraria  opinione non tiene alcun conto del    &#13;
 novum legislativo in materia di pene accessorie, ex art.140 novellato    &#13;
 cod.    pen.,    che   ha   abolito   ogni   vincolante   automatismo    &#13;
 nell'applicazione provvisoria di pene accessorie.                        &#13;
    3.  - È pure intervenuto nel giudizio il Presidente del Consiglio    &#13;
 dei ministri, rappresentato dall'Avvocatura Generale dello Stato, che    &#13;
 ritiene   irrilevante,   o  quanto  meno  inammissibile,  e  comunque    &#13;
 infondata la sollevata questione.                                        &#13;
    Irrilevante  perché sulla questione il Giudice Istruttore avrebbe    &#13;
 potuto e dovuto soltanto pronunziare una sentenza di incompetenza:  e    &#13;
 ciò  in  quanto,  ai  sensi  dell'art.  263 del r.d. 9 ottobre 1914,    &#13;
 n.1326, che è il regolamento di esecuzione  della  cosidetta  "legge    &#13;
 notarile" impugnata, soltanto il Tribunale civile può deliberare con    &#13;
 sentenza sull'istanza di riammissione all'esercizio della professione    &#13;
 notarile,  sentito il parere del Consiglio dell'ordine e del P.M.: il    &#13;
 Giudice Istruttore era, perciò, incompetente  e  il  giudizio  della    &#13;
 Corte non potrebbe avere alcuna rilevanza sull'ulteriore corso.          &#13;
    Ad ogni modo, la questione sarebbe inammissibile anche sotto altro    &#13;
 profilo, in quanto, avendo l'ordinanza del Giudice Istruttore  natura    &#13;
 di  sentenza,  sia pure pronunziata da giudice incompetente, egli non    &#13;
 avrebbe più alcun potere di revocarla,  potendo  l'ordinanza  essere    &#13;
 soltanto  gravata di appello. Essendo, quindi, il giudizio davanti al    &#13;
 Giudice  Istruttore  ormai  concluso,  la  sollevata   questione   è    &#13;
 irrilevante.                                                             &#13;
    In ogni caso, sulla rilevanza manca qualsiasi motivazione.            &#13;
   Infine,   la   questione   sarebbe   comunque   infondata,  perché    &#13;
 l'inabilitazione all'esercizio della professione notarile non è  una    &#13;
 sanzione  penale  accessoria ma un provvedimento cautelare, e perciò    &#13;
 non varrebbero le considerazioni svolte ex adverso. Sarebbe, infatti,    &#13;
 perfettamente  razionale  che, in presenza di indizi di colpevolezza,    &#13;
 il legislatore si sia preoccupato di garantire la  collettività  dai    &#13;
 rischi connessi alla continuazione, da parte dell'indiziato, di così    &#13;
 delicata funzione.                                                       &#13;
    In  prossimità  dell'udienza,  la  difesa  di  parte  privata  ha    &#13;
 depositato una lunga memoria, dove sono stati discussi  ampiamente  i    &#13;
 profili sostanziali e processuali della questione.<diritto>Considerato in diritto</diritto>1.  - La proposta questione è inammissibile per un duplice ordine    &#13;
 di motivi. Innanzitutto perché il giudice rimettente non dedica  una    &#13;
 sola  parola  della  motivazione  alla  rilevanza  della questione in    &#13;
 ordine al giudizio a lui sottoposto. Ben è vero che egli  ordina  la    &#13;
 sospensione  del  procedimento  in corso limitatamente alla sollevata    &#13;
 questione, così sottintendendo che il procedimento stesso, sul punto    &#13;
 investito  dalla  questione, sarebbe poi dovuto continuare innanzi ad    &#13;
 esso Giudice Istruttore sulla base di  quanto  questa  Corte  avrebbe    &#13;
 deciso.  Non  dice  e  non  spiega,  però,  l'ordinanza in qual modo    &#13;
 potrebbe quel giudizio continuare  e  quali  ulteriori  provvedimenti    &#13;
 potrebbe  il giudice adottare in ordine al già assunto provvedimento    &#13;
 di inabilitazione dei tre notai all'esercizio delle funzioni, ex art.    &#13;
 139 n. 1 l. 16 febbraio 1913 n.  89.                                     &#13;
    In  secondo  luogo,  perché,  in realtà, è evidente che nessuna    &#13;
 prosecuzione del giudizio concernente la disposta  inabilitazione  si    &#13;
 sarebbe più potuta verificare innanzi al Giudice rimettente. Questi,    &#13;
 infatti, da una parte, si era già spogliato del  giudizio  emettendo    &#13;
 il     provvedimento    di    rigetto    dell'istanza    di    revoca    &#13;
 dell'inabilitazione (cui peraltro non era competente) e,  dall'altra,    &#13;
 è  escluso  che,  quand'anche la questione fosse stata accolta, egli    &#13;
 avrebbe poi potuto revocare l'abnorme ordinanza con cui  ha  disposto    &#13;
 dato  che,  secondo la giurisprudenza della Corte di Cassazione, essa    &#13;
 ha  sostanziale  valore  di   sentenza,   essendo   questo   l'esatto    &#13;
 provvedimento che si sarebbe dovuto adottare.                            &#13;
    È  il Tribunale civile, ai sensi dell'art. 263 del Regolamento di    &#13;
 esecuzione della legge impugnata approvato con r.d.  9  ottobre  1914    &#13;
 n.1326,  il  solo  competente a decidere sull'istanza di riammissione    &#13;
 all'esercizio della professione notarile, previo parere  del  P.M.  e    &#13;
 del Consiglio dell'ordine.                                               &#13;
    2.  -  La difesa di parte privata, che nelle memorie non aveva mai    &#13;
 preso in  considerazione  la  questione  di  rilevanza,  ha  invocato    &#13;
 all'udienza  la  giurisprudenza di questa Corte, secondo cui la Corte    &#13;
 deve comunque pronunziarsi sulla questione dedotta anche se frattanto    &#13;
 la  vicenda  processuale si è estinta o si è composta. Senonché la    &#13;
 richiamata  giurisprudenza  si  riferisce  alle  ipotesi  in  cui  la    &#13;
 questione  dedotta, benché successivamente estintosi il procedimento    &#13;
 nel quale incidentalmente era sorta, era nata però, viva e vitale in    &#13;
 quanto  sicuramente  rilevante  nel momento in cui veniva sollevata e    &#13;
 gli atti venivano trasmessi a questa Corte:  si  trattava,  in  altri    &#13;
 termini, d'irrilevanza sopravvenuta.                                     &#13;
    Nella  specie, invece, la questione nasceva già morta, perché la    &#13;
 sua irrilevanza era originaria, avendo il giudice già  emanato,  sul    &#13;
 punto  costituzionalmente  sospetto,  un  provvedimento sul quale non    &#13;
 sarebbe più potuto ritornare.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art.139 n.1 della l. 16 febbraio 1913 n.  89  (legge  notarile),    &#13;
 sollevata  dal  Giudice  Istruttore  presso  il Tribunale di Roma con    &#13;
 ordinanza 3 luglio 1987.                                                 &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 9 giugno 1988.          &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GALLO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 giugno 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
