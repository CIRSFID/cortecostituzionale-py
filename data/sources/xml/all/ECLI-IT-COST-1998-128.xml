<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>128</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:128</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/04/1998</data_decisione>
    <data_deposito>16/04/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità  costituzionale  dell'art. 2, comma 4,    &#13;
 della legge 28 dicembre 1995, n.  549  (Misure  di  razionalizzazione    &#13;
 della  finanza pubblica), promossi con ordinanze emesse il 14 gennaio    &#13;
 dal pretore di Sanremo, il 15 gennaio dal pretore di Caserta, l'8 (n.    &#13;
 2 ordd.) e 24 gennaio, l'11 febbraio ed il 15 gennaio dal pretore  di    &#13;
 Bari  ed  il  24  giugno  1997 (n. 2 ordd.) dal pretore di Benevento,    &#13;
 sezione distaccata di Montesarchio, rispettivamente iscritte  ai  nn.    &#13;
 249,  350,  367, 368, 369, 370, 371, 640 e 641 del registro ordinanze    &#13;
 1997 e pubblicate nella Gazzetta Ufficiale della Repubblica  nn.  20,    &#13;
 25, 26 e 40, prima serie speciale, dell'anno 1997.                       &#13;
   Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei    &#13;
 Ministri,                                                                &#13;
   Udito nella camera di consiglio del 10  dicembre  1997  il  giudice    &#13;
 relatore Piero Alberto Capotosti.                                        &#13;
   Ritenuto    che   i   Pretori   di   Sanremo,   Caserta,   Bari   e    &#13;
 Benevento-sezione distaccata di Montesarchio, in funzione di  giudici    &#13;
 del  lavoro,  con ordinanze pronunziate dal primo il 14 gennaio 1997,    &#13;
 dal secondo il 15 gennaio 1997, dal terzo l'8  gennaio  1997,  il  15    &#13;
 gennaio  1997,  il  24  gennaio  1997  e  l'11  febbraio 1997 (cinque    &#13;
 ordinanze concernenti altrettanti giudizi), e dal quarto il 24 giugno    &#13;
 1997 (due ordinanze relative a  distinti  giudizi),  hanno  sollevato    &#13;
 questione  di legittimità costituzionale dell'art. 2, comma 4, della    &#13;
 legge 28 dicembre 1995, n. 549  (Misure  di  razionalizzazione  della    &#13;
 finanza  pubblica),  i primi tre rimettenti in riferimento agli artt.    &#13;
 3, primo comma, 4, 32 e 33, quinto comma, della Costituzione,  ed  il    &#13;
 quarto  in  riferimento  agli  artt.  3,  primo  comma,  4 e 97 della    &#13;
 Costituzione;                                                            &#13;
     che i giudici a quibus aditi da medici di  medicina  generale  di    &#13;
 libera  scelta  per  l'accertamento  del  diritto  a continuare nello    &#13;
 svolgimento  delle  prestazioni  in  regime  di  convenzione  con  le    &#13;
 rispettive  aziende  sanitarie  locali  oltre  il  limite  di età di    &#13;
 settanta anni stabilito dall'art. 2, comma 4, della legge n. 549  del    &#13;
 1995,  hanno  prospettato  le censure di costituzionalità dopo avere    &#13;
 accolto le domande cautelari proposte dai ricorrenti,  ma  prima  del    &#13;
 provvedimento di eventuale conferma;                                     &#13;
     che, secondo i rimettenti, detta norma, prevedendo la risoluzione    &#13;
 del  rapporto di convenzione tra i medici di base (nonché i pediatri    &#13;
 di libera scelta) e le aziende sanitarie locali al raggiungimento del    &#13;
 settantesimo anno di età dei professionisti, realizza una disparità    &#13;
 di   trattamento   rispetto   ad   altri   soggetti    pure    legati    &#13;
 all'amministrazione   sanitaria   da   rapporti   convenzionali,   in    &#13;
 particolare, gli specialisti ambulatoriali e, in  genere,  gli  altri    &#13;
 esercenti attività libero-professionale;                                &#13;
     che,  ad  avviso  dei  Pretori a quibus la disposizione censurata    &#13;
 viola, inoltre, l'art. 4 della Costituzione, in quanto introduce  una    &#13;
 limitazione  al  diritto  al  lavoro, si pone in contrasto con l'art.    &#13;
 33,  quinto  comma,  della  Costituzione,  dato  che  stabilisce  una    &#13;
 condizione    ulteriore    per    l'esercizio    dell'attività   dei    &#13;
 professionisti rispetto all'esame di Stato, arreca vulnus all'art. 32    &#13;
 della Costituzione, perché priva gli  assistiti  della  facoltà  di    &#13;
 scegliere  il medico cui affidare la cura di sé ed infine lede anche    &#13;
 l'art. 97 della Costituzione;                                            &#13;
     che in tutti i giudizi è intervenuto il Presidente del Consiglio    &#13;
 dei Ministri, rappresentato e  difeso  dall'Avvocatura  dello  Stato,    &#13;
 chiedendo,  con distinti atti, di analogo contenuto, che la questione    &#13;
 sia dichiarata infondata, ovvero manifestamente infondata.               &#13;
   Considerato che  i  giudizi  riguardano  un'identica  questione  e,    &#13;
 quindi, vanno riuniti, per essere decisi contestualmente;                &#13;
     che  tale questione, in riferimento agli artt. 3, primo comma, 4,    &#13;
 32 e 33, quinto comma, della Costituzione, è stata  già  dichiarata    &#13;
 non  fondata  con  la  sentenza  18  luglio 1997, n. 293, pronunziata    &#13;
 successivamente alle ordinanze di rimessione;                            &#13;
     che nella predetta decisione, in  particolare,  questa  Corte  ha    &#13;
 negato  l'omogeneità  delle  situazioni  poste  in  comparazione  e,    &#13;
 quindi, il presupposto stesso dell'eccepita lesione del principio  di    &#13;
 eguaglianza;  ha  affermato  l'incongruità  del richiamo dell'art. 4    &#13;
 della Costituzione, in quanto detta norma concerne soltanto l'accesso    &#13;
 al mercato del lavoro; ha escluso la violazione dell'art. 33,  quinto    &#13;
 comma,  della  Costituzione,  dato  che quest'ultima disposizione non    &#13;
 include l'ipotesi del mantenimento  di  un  particolare  rapporto  di    &#13;
 lavoro  avente  ad oggetto prestazioni tipicamente professionali; ha,    &#13;
 infine, puntualizzato, che  la  disposizione  in  esame  realizza  un    &#13;
 ragionevole bilanciamento tra l'efficienza del servizio e la garanzia    &#13;
 del  singolo, il quale può continuare ad avvalersi delle prestazioni    &#13;
 del medico in regime libero-professionale e, quindi, neppure  vulnera    &#13;
 l'art. 32 della Costituzione;                                            &#13;
     che   i   giudici   a   quibus   nel   sollevare  le  censure  di    &#13;
 costituzionalità, non adducono argomenti nuovi e diversi rispetto  a    &#13;
 quelli  già  esaminati  da  questa  Corte  e  che, d'altra parte, le    &#13;
 ordinanze del pretore di Benevento-sezione distaccata di Montesarchio    &#13;
 neppure prospettano le ragioni che dovrebbero  confortare  l'eccepita    &#13;
 lesione dell'art.  97 della Costituzione.                                &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti i giudizi,                                                     &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 2, comma 4, della legge 28 dicembre 1995, n.    &#13;
 549 (Misure di razionalizzazione della finanza  pubblica),  sollevata    &#13;
 in riferimento agli artt. 3, primo comma, 4, 32 e 33, quinto comma, e    &#13;
 97  della  Costituzione  dai  Pretori  di  Sanremo,  Caserta,  Bari e    &#13;
 Benevento-sezione distaccata di Montesarchio con le ordinanze di  cui    &#13;
 in epigrafe.                                                             &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 aprile 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 16 aprile 1998.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
