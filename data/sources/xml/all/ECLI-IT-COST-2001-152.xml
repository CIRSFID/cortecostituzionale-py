<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>152</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:152</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/05/2001</data_decisione>
    <data_deposito>17/05/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
Presidente:, Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY,Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI,Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi di legittimità costituzionale dell'art. 2, comma 1, &#13;
della legge 16 dicembre 1999, n. 479 (Modifiche alle disposizioni sul &#13;
procedimento davanti al tribunale in composizione monocratica e altre &#13;
modifiche  al  codice  di  procedura  penale.  Modifiche al codice di &#13;
procedura  penale  e  all'ordinamento  giudiziario.  Disposizioni  in &#13;
materia  di  contenzioso  civile pendente, di indennità spettanti al &#13;
giudice  di  pace e di esercizio della professione forense), promossi &#13;
con  due  ordinanze  emesse  il  9 maggio 2000 dal giudice di pace di &#13;
Avola nei procedimenti civili vertenti tra R. P. e A. V. e tra P.F. e &#13;
UNIASS  S.p.A.  ed  altri,  iscritte  ai  nn. 479  e 480 del registro &#13;
ordinanze 2000 e pubblicate nella Gazzetta Ufficiale della Repubblica &#13;
n. 38, 1ª serie speciale, dell'anno 2000. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio  del 4 aprile 2001 il giudice &#13;
relatore Piero Alberto Capotosti. &#13;
    Ritenuto  che  il  giudice  di  pace  di  Avola, con due distinte &#13;
ordinanze,  entrambe  emesse il 9 maggio 2000, ha sollevato questione &#13;
di  legittimità  costituzionale dell'art. 2, comma 1, della legge 16 &#13;
dicembre  1999,  n. 479 (Modifiche alle disposizioni sul procedimento &#13;
davanti al tribunale in composizione monocratica e altre modifiche al &#13;
codice di procedura penale. Modifiche al codice di procedura penale e &#13;
all'ordinamento  giudiziario.  Disposizioni in materia di contenzioso &#13;
civile  pendente,  di  indennità  spettanti  al giudice di pace e di &#13;
esercizio  della  professione forense), in riferimento agli artt. 3 e &#13;
25, primo comma, della Costituzione; &#13;
        che  i  giudizi  principali, iniziati in data anteriore al 30 &#13;
aprile  1995  dinanzi alla pretura circondariale di Siracusa, sezione &#13;
distaccata  di  Noto,  a  seguito della soppressione dell'ufficio del &#13;
pretore  e  della  mancata  istituzione di una corrispondente sezione &#13;
distaccata  di tribunale, sono stati attribuiti, a norma dell'art. 47 &#13;
del  decreto  legislativo  19  febbraio  1998,  n. 51,  alla  sezione &#13;
distaccata  di  Avola  del Tribunale di Siracusa, alla quale è stata &#13;
accorpata la circoscrizione della sezione distaccata di Noto; &#13;
        che  successivamente  i  medesimi  giudizi,  rientranti nella &#13;
competenza  per  valore  del  giudice  di pace in base alla normativa &#13;
vigente  alla  data di entrata in vigore della legge n. 479 del 1999, &#13;
in applicazione di quest'ultima legge sono stati trasmessi al giudice &#13;
di  pace  del  luogo  dove ha sede la sezione distaccata di tribunale &#13;
alla quale erano stati precedentemente attribuiti; &#13;
        che   il  giudice  rimettente  richiama  nelle  ordinanze  le &#13;
argomentazioni  delle  parti,  secondo  cui la norma impugnata, nella &#13;
parte in cui prevede la competenza per territorio del giudice di pace &#13;
del  luogo  in  cui  ha  sede  l'ufficio  giudiziario  o  la  sezione &#13;
distaccata dinanzi a cui il giudizio è pendente alla data di entrata &#13;
in  vigore  della medesima legge, viola l'art. 25, primo comma, della &#13;
Costituzione,  in  quanto  sottrae  le  cause al giudice naturale, da &#13;
individuarsi nel giudice di pace di Noto; &#13;
        che  la  norma  impugnata contrasterebbe inoltre con l'art. 3 &#13;
della Costituzione, in quanto determina una disparità di trattamento &#13;
rispetto  agli  altri  giudizi  di  competenza  del  giudice di pace, &#13;
nonché  a  quelli  già  pendenti  dinanzi  ai  soppressi  uffici di &#13;
conciliazione, che devono essere trattati nella sede di Noto; &#13;
        che, in uno dei giudizi dinanzi alla Corte, è intervenuto il &#13;
Presidente   del   Consiglio   dei   ministri,   con   il  patrocinio &#13;
dell'Avvocatura   generale   dello   Stato,   il  quale  ha  eccepito &#13;
l'inammissibilità  e  l'infondatezza della questione di legittimità &#13;
costituzionale, osservando che la norma impugnata è volta ad evitare &#13;
gli  inconvenienti  derivanti  dal mutamento del luogo di svolgimento &#13;
del giudizio, e si riferisce a cause che non erano originariamente di &#13;
competenza  del  giudice  di  pace,  e  per  le  quali  non è quindi &#13;
individuabile  come  giudice  naturale  il  giudice  di  pace  di  un &#13;
determinato luogo. &#13;
    Considerato  che  le  questioni  di  legittimità  costituzionale &#13;
sollevate  dal  giudice  di  pace  di Avola hanno entrambe ad oggetto &#13;
l'art. 2,  comma  1, della legge n. 479 del 1999, nella parte in cui, &#13;
in  riferimento  ai  giudizi  civili pendenti davanti al pretore alla &#13;
data del 30 aprile 1995, e rientranti nella competenza per valore del &#13;
giudice  di  pace in base alla normativa vigente alla data di entrata &#13;
in  vigore  della  legge  n. 479  del 1999, prevede la competenza per &#13;
territorio  del  giudice  di  pace del luogo in cui ha sede l'ufficio &#13;
giudiziario  o  la  sezione  distaccata  dinanzi a cui il giudizio è &#13;
pendente alla data di entrata in vigore della stessa legge; &#13;
        che  l'identità delle questioni sollevate dalle ordinanze di &#13;
rimessione ne rende opportuno l'esame congiunto; &#13;
        che  il  trasferimento  previsto  dalla legge n. 479 del 1999 &#13;
s'inscrive  nel quadro dei provvedimenti volti ad agevolare l'entrata &#13;
in  funzione  del  giudice  unico  di  primo grado e ad accelerare la &#13;
definizione  dei  giudizi  civili  pendenti  alla  data di entrata in &#13;
vigore   della   riforma  del  processo  civile,  i  quali,  a  norma &#13;
dell'art. 90  della  legge  26  novembre  1990, n. 353, continuano ad &#13;
essere regolati dalle disposizioni anteriormente vigenti; &#13;
        che,  nell'ambito  di  tale  disciplina,  la  norma impugnata &#13;
individua  il  giudice competente sulla base di un criterio generale, &#13;
valido  per  tutti  i  giudizi  c.d.  "di vecchio rito" già pendenti &#13;
dinanzi  ai  pretori  e successivamente trasmessi ai tribunali, e non &#13;
già in riferimento a singole controversie; &#13;
        che   non   sussiste   pertanto   la   denunciata  violazione &#13;
dell'art. 25,  primo comma, della Costituzione, in quanto, secondo il &#13;
consolidato  orientamento  di  questa  Corte, la garanzia del giudice &#13;
naturale  non  è  lesa  quando  il giudice sia designato in modo non &#13;
arbitrario  né  a  posteriori oppure direttamente dal legislatore in &#13;
conformità  alle regole generali, ovvero attraverso atti di soggetti &#13;
ai quali sia attribuito il relativo potere nel rispetto della riserva &#13;
di  legge  stabilita  dall'art. 25,  primo  comma, della Costituzione &#13;
(cfr.  ordinanza n. 159 del 2000, sentenza n. 419 del 1998, ordinanza &#13;
n. 176 del 1998); &#13;
        che,  entro  i  predetti limiti, l'individuazione del giudice &#13;
competente  a  decidere  determinate  controversie  è  rimessa  alla &#13;
discrezionalità  del  legislatore, il quale, nel regolare la fase di &#13;
transizione  tra diverse discipline ordinamentali e processuali, può &#13;
introdurre,  rispetto  agli  ordinari  criteri  di ripartizione della &#13;
competenza,  deroghe  fondate  su  un  ragionevole  bilanciamento dei &#13;
diversi  interessi  coinvolti nel processo (cfr. ordinanza n. 201 del &#13;
1997, sentenze n. 51 del 1997 e n. 143 del 1996); &#13;
        che  la  deroga  ai  criteri  generali  di ripartizione della &#13;
competenza  per  territorio  prevista  dalla norma impugnata risponde &#13;
alla   finalità   di   semplificare   l'individuazione  del  giudice &#13;
competente  a  definire  i  procedimenti  già  pendenti  dinanzi  ai &#13;
pretori,  anche  in  relazione all'eventuale configurabilità di fori &#13;
alternativi,  evitando nel contempo alle parti ed ai loro difensori i &#13;
disagi  derivanti da ulteriori modificazioni nel luogo di svolgimento &#13;
del giudizio; &#13;
        che  la  peculiarità  delle  esigenze  tenute  presenti  dal &#13;
legislatore  appare  sufficiente  a  giustificare  la  diversità del &#13;
trattamento  riservato  ai  giudizi  in questione rispetto agli altri &#13;
pendenti dinanzi al giudice di pace individuato in base agli ordinari &#13;
criteri  di ripartizione della competenza, escludendo quindi anche la &#13;
sussistenza    della    denunciata   violazione   dell'art. 3   della &#13;
Costituzione. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi; &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art. 2,  comma 1,  della  legge 16 &#13;
dicembre  1999,  n. 479 (Modifiche alle disposizioni sul procedimento &#13;
davanti al tribunale in composizione monocratica e altre modifiche al &#13;
codice di procedura penale. Modifiche al codice di procedura penale e &#13;
all'ordinamento  giudiziario.  Disposizioni in materia di contenzioso &#13;
civile  pendente,  di  indennità  spettanti  al giudice di pace e di &#13;
esercizio  della professione forense), sollevata, in riferimento agli &#13;
artt. 3 e 25, primo comma, della Costituzione, dal giudice di pace di &#13;
Avola con le ordinanze indicate in epigrafe; &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 9 maggio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                       Il redattore: Capotosti &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 17 maggio 2001 &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
