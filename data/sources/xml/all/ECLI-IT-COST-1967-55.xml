<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>55</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:55</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Verzì</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/04/1967</data_decisione>
    <data_deposito>05/05/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI &#13;
 - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - &#13;
 Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE &#13;
 CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della  legge  2  aprile  &#13;
 1958,  n.  322  (Ricongiunzione  delle  posizioni  previdenziali ai Eni  &#13;
 dell'accertamento del diritto e della determinazione del trattamento di  &#13;
 previdenza e  di  quiescenza),  promosso  con  ordinanza  emessa  il  3  &#13;
 novembre  1964  dalla  Corte di appello di Bari nel procedimento civile  &#13;
 vertente tra Fortunato Saverio e il Banco di Napoli, iscritta al n. 193  &#13;
 del Registro ordinanze 1964 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 39 del 13 febbraio 1965.                                   &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri e di costituzione di Fortunato Saverio;                         &#13;
     udita  nell'udienza  pubblica del 15 febbraio 1967 la relazione del  &#13;
 Giudice Verzì;                                                          &#13;
     udito il sostituto avvocato generale dello Stato Luciano  Tracanna,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  giudizio  di opposizione al decreto ingiuntivo, che condannava  &#13;
 il Banco di Napoli al pagamento di una somma a favore del dott. Saverio  &#13;
 Fortunato per residuo indennità relative a cessato rapporto di lavoro,  &#13;
 il Tribunale di Bari revocava tale decreto affermando  la  legittimità  &#13;
 della  legge  2  aprile  1958,  n. 322, la quale impone al Banco stesso  &#13;
 l'obbligo  di  trattenere,  nella  liquidazione  a  favore  del   dott.  &#13;
 Fortunato,  l'ammontare  dei  contributi e degli interessi maturati per  &#13;
 tutta la durata del periodo di lavoro,  da  versare  all'I.N.P.S.  agli  &#13;
 effetti pensionistici.                                                   &#13;
     In  sede di appello, il dott. Fortunato riproponeva la questione di  &#13;
 legittimità costituzionale della suindicata legge, in  riferimento  al  &#13;
 quarto  comma  dell'art.  81 della Costituzione, in quanto prevederebbe  &#13;
 oneri a carico dello Stato, senza indicare i mezzi per farvi fronte.  E  &#13;
 la Corte d'appello di Bari, con ordinanza del 3 novembre 1964, ritenuta  &#13;
 la  non  manifesta infondatezza della questione e la sua rilevanza agli  &#13;
 effetti della decisione, disponeva la sospensione  del  giudizio  e  la  &#13;
 trasmissione degli atti a questa Corte.                                  &#13;
     Secondo  l'ordinanza,  premesso  che  lo  Stato  contribuisce  alle  &#13;
 prestazioni assicurative sociali, nei modi e nei limiti previsti  dagli  &#13;
 artt.  59 del R.D. L. 4 ottobre 1935, n. 1827, convertito nella legge 6  &#13;
 aprile 1936, n. 1155, 35 del R.D. L. 14 aprile 1939, n. 636, convertito  &#13;
 nella legge 6 luglio 1939, n. 1272, 16 e 31 della legge 4 aprile  1952,  &#13;
 n.  218  e  13 della legge 20 febbraio 1958, n. 55, la legge impugnata,  &#13;
 ampliando il numero dei lavoratori aventi diritto all'assicurazione per  &#13;
 l'invalidità, vecchiaia e superstiti impone allo  Stato  una  maggiore  &#13;
 spesa, per la quale non sono indicati i mezzi di copertura.              &#13;
     L'ordinanza   è   stata   regolarmente  comunicata,  notificata  e  &#13;
 pubblicata sulla Gazzetta Ufficiale  della  Repubblica  n.  39  del  13  &#13;
 febbraio  1965. Nel presente giudizio si è costituito il dott. Saverio  &#13;
 Fortunato ed è intervenuto il Presidente del Consiglio dei Ministri.    &#13;
     Con l'atto di costituzione del 22 dicembre 1964, e con  la  memoria  &#13;
 del  28  gennaio  1967,  il  dott.  Fortunato  rileva  che  lo  Stato -  &#13;
 nell'esplicazione della sua  attività  di  assistenza  sociale  -  pur  &#13;
 avvalendosi  dell'opera  di  diversi  enti  pubblici,  che  controlla e  &#13;
 dirige, non si spoglia della responsabilità inerente ai  suoi  doveri,  &#13;
 sicché  ogni  onere ricade sempre, o immediatamente o mediatamente, su  &#13;
 di esso. La ripetuta legge, aumentando il numero degli  aventi  diritto  &#13;
 alla assicurazione generale obbligatoria accresce la spesa dello Stato,  &#13;
 che  deve concorrere negli oneri relativi ai sensi della legge 4 aprile  &#13;
 1952, n. 218, e 20 febbraio 1958, n. 55,  ed  ai  sensi  dell'art.  59,  &#13;
 primo  comma,  lett.  a  del  R.D.  L.  4 ottobre 1935, n. 1827, che ne  &#13;
 concreta il  concorso  nella  quota  di  lire  100  all'anno  per  ogni  &#13;
 pensione.                                                                &#13;
     L'Avvocatura  generale  dello Stato ritiene invece che la questione  &#13;
 sia infondata. Osserva in primo luogo che il  contenuto  e  la  portata  &#13;
 dell'unico   articolo  della  legge  non  sono  quelli  indicati  dalla  &#13;
 ordinanza di rimessione:   la  norma  si  limiterebbe  a  prevedere  il  &#13;
 coordinamento,  ai  fini  pensionistici,  della  assicurazione generale  &#13;
 della previdenza sociale con le posizioni previdenziali già acquisite;  &#13;
 ed  all'uopo,  stabilisce  che  l'ente  il  quale  deve  liquidare  una  &#13;
 indennità  una  tantum  al dipendente che, al momento della cessazione  &#13;
 dal servizio non abbia diritto a pensione,  deve  trattenere  l'importo  &#13;
 corrispondente al contributo da versare all'I.N.P.S. per costituire una  &#13;
 posizione  assicurativa  per  tutto  il  periodo  di  tempo  del lavoro  &#13;
 prestato.                                                                &#13;
     La  norma  prevede  in  astratto  un  ampliamento  del  numero  dei  &#13;
 lavoratori  aventi  diritto  alla  assicurazione  generale,  ma l'onere  &#13;
 relativo per lo Stato è  meramente  ipotetico  ed  eventuale,  siccome  &#13;
 connesso  alla  gestione  mutualistica.  E  non  è  né  immediato né  &#13;
 determinabile nell'importo e nei mezzi per farvi fronte  in  quanto  la  &#13;
 legge  impugnata  non dà luogo a prestazioni immediate.  Anche secondo  &#13;
 la giurisprudenza di questa Corte, la  norma  costituzionale  circa  la  &#13;
 indicazione  del  mezzo  di copertura di una nuova spesa presuppone che  &#13;
 questa sia determinata, o quanto meno determinabile.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte ritiene che la legge 2 aprile 1958, n. 322,  si  inserisce  &#13;
 nel  complesso  delle  norme  della  gestione della previdenza sociale,  &#13;
 senza un contenuto innovativo rispetto alle precedenti leggi di  spesa,  &#13;
 e che quindi non sussiste la denunziata violazione dell'art. 81, quarto  &#13;
 comma, della Costituzione.                                               &#13;
     L'assicurazione  per l'invalidità, la vecchiaia ed i superstiti è  &#13;
 obbligatoria per tutte le persone di una determinata età, che prestano  &#13;
 lavoro retribuito alle dipendenze di altri, con esclusione soltanto  di  &#13;
 alcune  categorie espressamente indicate dal R.D. L. 14 aprile 1939, n.  &#13;
 636 (artt. 3 e 4), convertito nella legge 6 luglio 1939, n.  1272.  Nel  &#13;
 caso  di  particolari  trattamenti  di  quiescenza  o di previdenza, è  &#13;
 previsto  l'esonero  dall'obbligo  della  assicurazione  generale,   da  &#13;
 concedersi  con  decreto ministeriale, e sottoposto a varie condizioni,  &#13;
 fra   le   quali   quella   "che   sia   stabilito   il   trasferimento  &#13;
 all'assicurazione  obbligatoria dell'intera riserva matematica relativa  &#13;
 ai contributi dell'assicurazione stessa nei casi  di  cessazione  dalla  &#13;
 iscrizione  o  di  soppressione della cassa, fondo o gestione speciale"  &#13;
 (art. 28 del R.D. L. n. 636 del 1939, modificato dalla  legge  n.  1272  &#13;
 del  1939).  In  altri  termini,  poiché la detta assicurazione ha per  &#13;
 scopo l'assegnazione di  una  pensione  agli  assicurati  nel  caso  di  &#13;
 invalidità o di vecchiaia, e di una pensione ai superstiti nel caso di  &#13;
 morte  dell'assicurato  o del pensionato, con le norme sopraindicate si  &#13;
 intende assicurare la pensione anche ai lavoratori dipendenti  da  enti  &#13;
 esonerati,  qualora le forme sostitutive di previdenza non garantiscano  &#13;
 un trattamento pensionistico. Ed, in applicazione di siffatti principi,  &#13;
 la  legge   impugnata   coordina   le   due   posizioni   assicurative,  &#13;
 ricongiungendo quella sostitutiva alla assicurazione generale, mediante  &#13;
 il  versamento  alla previdenza sociale dei contributi relativi a tutto  &#13;
 il periodo del cessato rapporto di lavoro. Il  datore  di  lavoro  deve  &#13;
 detrarre  l'ammontare  di tali contributi dalle indennità spettanti al  &#13;
 dipendente al momento della cessazione del  rapporto  per  effetto  del  &#13;
 trattamento  sostitutivo. Appare pertanto evidente che questa legge non  &#13;
 ha  un  contenuto  nuovo  o   diverso   dalle   precedenti   previsioni  &#13;
 legislative,  ma  si  limita  a  dare una particolare regolamentazione,  &#13;
 rientrante nella gestione della previdenza sociale e  nelle  previsioni  &#13;
 economiche finanziarie della stessa.                                     &#13;
     Occorre   altresì  considerare  che  l'aumento  del  numero  degli  &#13;
 iscritti all'assicurazione generale non è, di per sé  solo,  elemento  &#13;
 decisivo  al fine della indagine per accertare se la legge in esame sia  &#13;
 generatrice di una nuova spesa per l'Erario. Ed invero tale  numero  è  &#13;
 soggetto  a continue variazioni, incerte ed imprevedibili, in aumento o  &#13;
 in diminuzione, per il continuo affluire di nuovi assicurati come per i  &#13;
 diversi eventi che seguono le vicende  della  vita  di  costoro,  onde,  &#13;
 nella  grande  massa  degli  iscritti,  si verificano compensazioni, in  &#13;
 virtù  delle  quali  le  differenze  eventuali  di  spesa  perdono  di  &#13;
 rilevanza.  Comunque, per le ragioni esposte sopra, il contributo dello  &#13;
 Stato per il futuro eventuale trattamento pensionistico è da ritenersi  &#13;
 già  compreso  nelle  previsioni  di  spese  contenute   nelle   leggi  &#13;
 sopraindicate  del 1939, rientrando nei complessi rapporti generali fra  &#13;
 Stato e Istituto della previdenza sociale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  costituzionale  &#13;
 dell'unico  articolo  della legge 2 aprile 1958, n. 322 (Ricongiunzione  &#13;
 delle posizioni previdenziali ai fini dell'accertamento del  diritto  e  &#13;
 della  determinazione  del  trattamento  di previdenza e di quiescenza)  &#13;
 proposta con ordinanza della Corte d'appello di  Bari  del  3  novembre  &#13;
 1964, in riferimento all'art. 81, comma quarto, della Costituzione.      &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 27 aprile 1967.                               &#13;
                                   GASPARE AMBROSINI - ANTONINO  PAPALDO  &#13;
                                   -   GIOVANNI   CASSANDRO   -   BIAGIO  &#13;
                                   PETROCELLI -  ANTONIO  MANCA  -  ALDO  &#13;
                                   SANDULLI  - GIUSEPPE BRANCA - MICHELE  &#13;
                                   FRAGALI  -   COSTANTINO   MORTATI   -  &#13;
                                   GIUSEPPE  CHIARELLI - GIUSEPPE  VERZÌ  &#13;
                                   -  GIOVANNI  BATTISTA   BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
