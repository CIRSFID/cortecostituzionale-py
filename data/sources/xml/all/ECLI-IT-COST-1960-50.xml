<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1960</anno_pronuncia>
    <numero_pronuncia>50</numero_pronuncia>
    <ecli>ECLI:IT:COST:1960:50</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Mario Cosatti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1960</data_decisione>
    <data_deposito>06/07/1960</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Avv. &#13;
 GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - Dott. &#13;
 MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. GIUSEPPE &#13;
 CASTELLI AVOLIO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA, Giudici,</collegio>
    <epigrafe>ha deliberato in camera di consiglio la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale delle norme contenute  &#13;
 nell'art. 235, secondo comma, del regolamento per la riscossione  delle  &#13;
 imposte  di  consumo  approvato  con  R.  D. 30 aprile 1936, n.   1138,  &#13;
 promosso con ordinanza 18 novembre 1959 del Pretore di Pontedecimo  (in  &#13;
 sede  distaccata di Busalla) nel procedimento penale a carico di Poggio  &#13;
 Michele Giorgio, Torre Giovanna e Minaglia Ernesto, iscritta  al  n.  1  &#13;
 del Registro ordinanze 1960 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 25 del 30 gennaio 1960.                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri,                                                                &#13;
     Ritenuto  che  gli  imputati  Poggio,  Torre  e  Minaglia  venivano  &#13;
 rinviati  a  giudizio  innanzi al Pretore di Pontedecimo per rispondere  &#13;
 del reato previsto dall'art. 55 del Testo unico  della  finanza  locale  &#13;
 approvato con R. D. 14 settembre 1931, n.  1175;                         &#13;
     che  la difesa dei predetti sollevava eccezione di improcedibilità  &#13;
 dell'azione penale, lamentando che i verbali di  denuncia  erano  stati  &#13;
 inoltrati direttamente al Pretore e non per il tramite della competente  &#13;
 autorità  comunale,  onde  quest'ultima  non  aveva potuto prendere in  &#13;
 esame  le  domande,  presentate  in  sede  amministrativa,  dirette  ad  &#13;
 ottenere  -  ai sensi dell'art. 235, secondo comma, del regolamento per  &#13;
 la riscossione delle imposte di consumo approvato con  R.D.  30  aprile  &#13;
 1936,  n.  1138 - che l'ipotesi delittuosa di cui all'art. 55 del Testo  &#13;
 unico fosse degradata in quella contravvenzionale prevista dall'art. 59  &#13;
 con conseguente possibilità di oblazione;                               &#13;
     che il Pretore sollevava di ufficio la  questione  di  legittimità  &#13;
 costituzionale  dell'art. 235, secondo comma, in riferimento alle norme  &#13;
 contenute negli articoli 102 e 112 della Costituzione,  osservando  che  &#13;
 "una  volta rimessi i verbali di contravvenzione per frode alla imposta  &#13;
 di  consumo   all'Autorità   giudiziaria   dovrebbe   essere   inibito  &#13;
 all'autorità  amministrativa  di  accogliere domanda di oblazione, non  &#13;
 potendo  la  medesima  alterare  la   imputazione   determinandone   la  &#13;
 modificazione  in  modo  rilevante per la stessa Autorità giudiziaria,  &#13;
 alla quale soltanto compete la valutazione del fatto ai fini della  sua  &#13;
 configurazione giuridica";                                               &#13;
    che  l'ordinanza  del  Pretore  è  stata regolarmente notificata al  &#13;
 Presidente del Consiglio dei Ministri, comunicata ai  Presidenti  delle  &#13;
 Camere   legislative   e  pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 25 del 30 gennaio 1960;                                    &#13;
     che nessuna delle  parti  private  si  è  costituita  in  giudizio  &#13;
 davanti a questa Corte;                                                  &#13;
     che   l'Avvocatura   dello   Stato,  nell'atto  di  intervento  del  &#13;
 Presidente del Consiglio  dei  Ministri  depositato  nella  cancelleria  &#13;
 della  Corte  il 16 dicembre 1959, ha concluso, in linea pregiudiziale,  &#13;
 per l'inammissibilità della proposta questione, prospettando  tuttavia  &#13;
 dubbi   sulla  natura  giuridica  della  norma  impugnata  (legislativa  &#13;
 delegata o regolamentare) in relazione al disposto dell'art. 344 del T.  &#13;
 U. della finanza locale; nel merito,  ha  concluso  per  l'infondatezza  &#13;
 della questione;                                                         &#13;
     Considerato  che l'art. 235 è contenuto in un provvedimento che ha  &#13;
 le  tipiche  caratteristiche  di  un  regolamento;   reca   il   titolo  &#13;
 "Regolamento  per  la riscossione delle imposte di consumo" ed è stato  &#13;
 come tale approvato con R. Decreto n. 1138 del 1936,  udito  il  parere  &#13;
 del  Consiglio  di  Stato e sentito il Consiglio dei Ministri, ai sensi  &#13;
 dell'art. 1, n. 1 (esecuzione delle  leggi),  della  legge  31  gennaio  &#13;
 1926, n. 100, citato nelle premesse del regolamento stesso;              &#13;
     che  del  resto  la  facoltà  concessa  al Governo dal primo comma  &#13;
 dell'art.  344 del T. U. di emanare norme integrative e transitorie era  &#13;
 cessata dalla data di pubblicazione del R.D.L. 20 luglio 1934, n. 1467,  &#13;
 concernente modificazioni alle norme  in  materia  di  finanza  locale,  &#13;
 citato nelle premesse del decreto n. 1138 del 1936;                      &#13;
     che  pertanto  si appalesa la manifesta infondatezza della proposta  &#13;
 questione, poiché il giudizio di legittimità costituzionale, a  norma  &#13;
 dell'art. 134 della Costituzione, deve avere per oggetto una legge o un  &#13;
 atto avente forza di legge;                                              &#13;
     Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.  &#13;
 87, e 9, secondo comma, delle Norme integrative per i  giudizi  davanti  &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  manifesta  infondatezza  della questione proposta dal  &#13;
 Pretore di Pontedecimo (in sede distaccata di Busalla),  con  ordinanza  &#13;
 18  novembre  1959,  sulla  legittimità  costituzionale dell'art. 235,  &#13;
 secondo comma, del regolamento per  la  riscossione  delle  imposte  di  &#13;
 consumo   approvato  con  R.  Decreto  30  aprile  1936,  n.  1138,  in  &#13;
 riferimento agli artt. 102 e 112 della Costituzione.                     &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 24 giugno 1960.         &#13;
                                   GAETANO  AZZARITI  - GIUSEPPE CAPPI -  &#13;
                                   TOMASO PERASSI - GASPARE AMBROSINI  -  &#13;
                                   MARIO  COSATTI  -  FRANCESCO PANTALEO  &#13;
                                   GABRIELI - GIUSEPPE CASTELLI AVOLIO -  &#13;
                                   NICOLA JAEGER - GIOVANNI CASSANDRO  -  &#13;
                                   BIAGIO  PETROCELLI  - ANTONIO MANCA -  &#13;
                                   ALDO SANDULLI - GIUSEPPE BRANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
