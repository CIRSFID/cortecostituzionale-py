<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>380</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:380</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/10/1991</data_decisione>
    <data_deposito>10/10/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, prof. &#13;
 Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  degli  artt.  2,  comma    &#13;
 primo,  e  4  del  decreto-legge  1°  febbraio 1977, n. 12 (Norma per    &#13;
 l'applicazione dell'indennità di contingenza), convertito  in  legge    &#13;
 31  marzo  1977,  n. 91, promosso con ordinanza emessa il 16 novembre    &#13;
 1990 dalla Corte di Cassazione sul ricorso proposto da S.p.a.   Zust-    &#13;
 Ambrosetti contro Maravigna Ernestina iscritta al n. 173 del registro    &#13;
 ordinanze 1991 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 12, prima serie speciale, dell'anno 1991;                             &#13;
    Visto l'atto di costituzione di Maravigna Ernestina;                  &#13;
    Udito  nell'udienza pubblica del 9 luglio 1991 il Giudice relatore    &#13;
 Luigi Mengoni;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso del giudizio promosso dalla S.p.a.  Zust-Ambrosetti    &#13;
 per  l'annullamento  della sentenza del Tribunale di Genova 21 giugno    &#13;
 1989, confermativa della sentenza  del  Pretore  con  cui  era  stata    &#13;
 respinta  la  domanda  riconvenzionale  proposta dalla detta società    &#13;
 contro la dipendente Ernestina Maravigna per ottenere la restituzione    &#13;
 delle  somme  indebitamente  corrisposte  a  titolo  di indennità di    &#13;
 contingenza in relazione agli scatti maturati  dopo  il  1°  febbraio    &#13;
 1977,  la Corte di cassazione, con ordinanza del 16 novembre 1990, ha    &#13;
 sollevato, in riferimento agli  artt.  3  e  36  della  Costituzione,    &#13;
 questione  di legittimità costituzionale degli artt. 2, primo comma,    &#13;
 e 4 del d.-l. 1° febbraio 1977, n. 12, convertito in legge  31  marzo    &#13;
 1977,  n.  91, nella parte in cui escludono, senza limiti temporali e    &#13;
 con comminatoria di nullità delle clausole  contrattuali  contrarie,    &#13;
 la  computabilità  dell'indennità  di  contingenza sulle mensilità    &#13;
 aggiuntive  a  quelle  previste   dalla   contrattazione   collettiva    &#13;
 prevalente nel settore dell'industria.                                   &#13;
    2.  -  Si  è costituita la resistente rilevando che questa Corte,    &#13;
 con sentenza n. 124 del 1991,  ha  già  dichiarato  la  sopravvenuta    &#13;
 illegittimità  costituzionale  della  prima  delle  norme impugnate.    &#13;
 Atteso che l'altra norma, cioè l'art. 4, secondo comma,  del  citato    &#13;
 decreto, è una norma sanzionatrice delle clausole contrattuali stipulate  in  deroga  ai  massimi  legali  imperativamente stabiliti dal    &#13;
 decreto, la parte costituita in giudizio conclude nel  senso  che  "o    &#13;
 l'art. 4 è già caduto e qualsiasi ulteriore sanzione formale dovrà    &#13;
 ritenersi  superflua,  oppure  tale  norma dovrà essere fatta cadere    &#13;
 anche formalmente".<diritto>Considerato in diritto</diritto>1. - Sono impugnati dalla Corte di cassazione, per  contrasto  con    &#13;
 gli  artt.  3 e 36 della Costituzione, gli artt. 2, primo comma, e 4,    &#13;
 secondo comma, del d.-l. 1° febbraio 1977, n. 12, convertito in legge    &#13;
 31 marzo 1991, n. 91, nella parte in cui escludono, senza  limiti  di    &#13;
 tempo  e  con  comminatoria  di  nullità  delle clausole difformi di    &#13;
 regolamento  o  di  contratto,  la  computabilità  degli  incrementi    &#13;
 dell'indennità  di  contingenza  maturati  dopo  il 1° febbraio 1977    &#13;
 nelle mensilità di retribuzione ulteriori alla tredicesima, previste    &#13;
 in settori diversi da quello dell'industria.                             &#13;
    2. - In riferimento all'art. 2, primo comma, del d.-l. n.  12  del    &#13;
 1977,  la  questione  è  manifestamente  inammissibile  perché tale    &#13;
 disposizione è già stata dichiarata costituzionalmente  illegittima    &#13;
 in  parte  qua, a decorrere dal 28 febbraio 1986, con sentenza n. 124    &#13;
 del 1991.                                                                &#13;
    3.  -  Conseguentemente  la  questione  deve   essere   dichiarata    &#13;
 inammissibile  anche  in relazione all'art. 4, secondo comma, sebbene    &#13;
 non investito dal dispositivo della citata sentenza,  perché  questa    &#13;
 norma,  non  essendo  suscettibile di applicazione autonoma, non può    &#13;
 formare   oggetto   di   un   autonomo   giudizio   di   legittimità    &#13;
 costituzionale.   Si   tratta,   infatti,   di   norma  secondaria  o    &#13;
 strumentale, avente funzione di sanzione (nella forma della nullità)    &#13;
 delle disposizioni di regolamento o di contratto in contrasto con  le    &#13;
 norme imperative primarie del decreto. Come tale, essa è applicabile    &#13;
 solo  congiuntamente  a  tali  norme,  e quindi diventa inapplicabile    &#13;
 nella misura in cui venga meno, in  forza  di  una  dichiarazione  di    &#13;
 illegittimità costituzionale, l'efficacia delle norme primarie.         &#13;
    Diversamente dall'invalidità conseguenziale prevista dall'art. 27    &#13;
 della  legge  11  marzo  1953, n. 87, l'inapplicabilità di una norma    &#13;
 secondaria, legata da un  nesso  di  complementarità  e  insieme  di    &#13;
 subordinazione    funzionale    alla    norma   primaria   dichiarata    &#13;
 costituzionalmente  illegittima,  è  una conseguenza automatica, che    &#13;
 non va dichiarata dalla Corte, ma  deve  essere  valutata  volta  per    &#13;
 volta  dal  giudice del merito. In questo senso la questione relativa    &#13;
 all'art. 4 del decreto n. 12 del 1977 è assorbita  dalla  questione,    &#13;
 già risolta, relativa all'art. 2, primo comma.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale dell'art. 2, primo comma, del d.-l.    1°    &#13;
 febbraio  1977,  n.  12  (Norma per l'applicazione dell'indennità di    &#13;
 contingenza), convertito nella legge 31 marzo 1977,  n.  91,  perché    &#13;
 già  dichiarato  costituzionalmente  illegittimo  in  parte  qua,  a    &#13;
 decorrere dal 28  febbraio  1986,  con  sentenza  n.  124  del  1991,    &#13;
 questione   sollevata,  in  riferimento  agli  artt.  3  e  36  della    &#13;
 Costituzione, dalla Corte di cassazione con l'ordinanza  indicata  in    &#13;
 epigrafe;                                                                &#13;
    Dichiara   l'inammissibilità   della  questione  di  legittimità    &#13;
 costituzionale dell'art. 4 del citato  decreto-legge,  sollevata,  in    &#13;
 riferimento  alle  stesse  norme,  dalla  Corte sopra nominata con la    &#13;
 medesima ordinanza.                                                      &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, l'8 ottobre 1991.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 10 ottobre 1991.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
