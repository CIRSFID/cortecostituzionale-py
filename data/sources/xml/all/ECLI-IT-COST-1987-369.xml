<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>369</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:369</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1987</data_decisione>
    <data_deposito>04/11/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 285, comma    &#13;
 terzo, del r.d. 14 settembre 1931 n. 1175 (Testo unico per la finanza    &#13;
 locale),  promosso  con ordinanza emessa il 25 marzo 1982 dalla Corte    &#13;
 di appello di Roma, iscritta al n. 600 del registro ordinanze 1982  e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 357 dell'anno    &#13;
 1982;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 14 ottobre 1987 il Giudice    &#13;
 relatore Francesco Saja;                                                 &#13;
    Considerato  che  nel  corso  di  un  procedimento vertente tra la    &#13;
 s.a.s. Poggio Rose ed il Comune di Roma  ed  avente  per  oggetto  il    &#13;
 pagamento   dell'imposta   sull'incremento   di   valore  delle  aree    &#13;
 fabbricabili, la Corte d'appello di Roma con ordinanza del  25  marzo    &#13;
 1982 (reg. ord. n. 600 del 1982) sollevava, in riferimento agli artt.    &#13;
 3, 24 e 113 Cost., questione di legittimità costituzionale dell'art.    &#13;
 285,  terzo  comma, del t.u. per la finanza locale approvato con r.d.    &#13;
 14 settembre 1931 n. 1175, che permette al contribuente di  ricorrere    &#13;
 all'autorità  giudiziaria  entro  sei  mesi  dalla pubblicazione del    &#13;
 ruolo d'imposta;                                                         &#13;
      che  la  Corte rilevava come - qualora fosse mancata, come nella    &#13;
 specie, ogni notificazione dei provvedimenti degli uffici  finanziari    &#13;
 -  l'onere  del contribuente di consultare atti con più destinatari,    &#13;
 la cui pubblicazione viene resa nota  mediante  affissione  in  tempi    &#13;
 diversi,  poteva  causare un'ingiustificata disparità di trattamento    &#13;
 tra cittadini soggetti ad imposte locali e quelli soggetti ad imposte    &#13;
 erariali,  ai  quali  ultimi  i ruoli venivano comunque notificati; e    &#13;
 ciò,  in  definitiva,  poteva  rendere  eccessivamente  difficile  o    &#13;
 impossibile la tutela giurisdizionale;                                   &#13;
      che  la  Presidenza  del  Consiglio  dei  ministri, intervenuta,    &#13;
 chiedeva che le questioni fossero dichiarate non fondate, poiché  la    &#13;
 diversità  delle  situazioni  dei  contribuenti,  soggetti a tributi    &#13;
 locali ed a tributi erariali, non consentiva di invocare il principio    &#13;
 di  eguaglianza, e perché l'ampiezza del termine semestrale impediva    &#13;
 di ravvisare qualsiasi lesione al diritto di tutela in giudizio;         &#13;
    Considerato   che   -   ferma  restando  la  discrezionalità  del    &#13;
 legislatore nella determinazione delle  diverse  possibili  forme  di    &#13;
 tutela  giurisdizionale  e  in  particolare,  per  quanto riguarda il    &#13;
 diritto tributario, di differenziare detta  tutela  a  seconda  delle    &#13;
 diverse  specie  di  imposte - non appare irragionevole la scelta del    &#13;
 legislatore il quale ha  statuito,  seguendo  un'antica  e  accettata    &#13;
 tradizione,  che  i  ruoli delle imposte locali vengano resi pubblici    &#13;
 mediante deposito ed affissione e che dalla pubblicazione decorra  il    &#13;
 termine per adire l'autorità giudiziaria;                               &#13;
      che la giurisprudenza di questa Corte in materia di impugnazioni    &#13;
 nel processo fallimentare, e segnatamente la sent. n. 255  del  1974,    &#13;
 appare  male invocata dall'ordinanza di rimessione, giacché con essa    &#13;
 la decorrenza dei  termini  dall'affissione  dell'atto  da  impugnare    &#13;
 venne  ritenuta  incostituzionale  essenzialmente  in  ragione  della    &#13;
 brevità dei termini stessi, mentre nel caso  in  esame  il  termine,    &#13;
 addirittura  semestrale,  non  rende  impossibile  né eccessivamente    &#13;
 difficile l'esercizio del diritto;                                       &#13;
      che  in  conclusione  la  questione  si  appalesa manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt.  26  l.  11  marzo  1953  n.  87 e 9 delle Norme    &#13;
 integrative per i giudizi innanzi alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  non  fondata la questione di legittimità    &#13;
 costituzionale dell'art. 285, terzo comma, r.d. 14 settembre 1931  n.    &#13;
 1175,  sollevata  in  riferimento  agli artt. 3, 24 e 113 Cost. dalla    &#13;
 Corte d'appello di Roma con l'ordinanza indicata in epigrafe.            &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1987                               &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: SAJA                                 &#13;
    Depositata in cancelleria il 4 novembre 1987.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
