<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-05-27/98/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-05-27/98"/>
          <FRBRalias value="ECLI:IT:COST:2020:98" name="ECLI"/>
          <FRBRdate date="2020-05-27" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="98"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-05-27/98/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-05-27/98/ita@"/>
          <FRBRdate date="2020-05-27" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2020-05-27/98/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2020-05-27/98/ita@.xml"/>
          <FRBRdate date="2020-05-27" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-05-05" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-05-27" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>98</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:98</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Daria de Pretis</akn4cc:relatore>
        <akn4cc:data_decisione>2020-05-05</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-05-27</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 10, comma 4, della legge della Regione Toscana 16 aprile 2019, n. 18 (Disposizioni per la qualità del lavoro e per la valorizzazione della buona impresa negli appalti di lavori, forniture e servizi. Disposizioni organizzative in materia di procedure di affidamento di lavori. Modifiche alla l.r. 38/2007), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 17-20 giugno 2019, depositato in cancelleria il 24 giugno 2019, iscritto al n. 72 del registro ricorsi 2019 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 33, prima serie speciale, dell'anno 2019.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Toscana;
 udito il Giudice relatore Daria de Pretis ai sensi del decreto della Presidente della Corte del 20 aprile 2020, punto 1) lettere a) e c), in collegamento da remoto, senza discussione orale, in data 5 maggio 2020;
 deliberato nella camera di consiglio del 5 maggio 2020.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Presidente del Consiglio dei ministri ha impugnato l'art. 10, comma 4, della legge della Regione Toscana 16 aprile 2019, n. 18 (Disposizioni per la qualità del lavoro e per la valorizzazione della buona impresa negli appalti di lavori, forniture e servizi. Disposizioni organizzative in materia di procedure di affidamento di lavori. Modifiche alla l.r. 38/2007).
 La norma censurata è inserita nel capo II della legge regionale, che disciplina (come risulta dal suo art. 8) le «procedure negoziate per l'affidamento di lavori di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2016/50/!main#art_36">articolo 36 del d.lgs. 50/2016</ref>» (cioè, dei contratti di valore inferiore alla soglia comunitaria), e stabilisce che, «[i]n considerazione dell'interesse meramente locale degli interventi, le stazioni appaltanti possono prevedere di riservare la partecipazione alle micro, piccole e medie imprese con sede legale e operativa nel territorio regionale per una quota non superiore al 50 per cento e in tal caso la procedura informatizzata assicura la presenza delle suddette imprese fra gli operatori economici da consultare».
 Secondo il ricorrente, la possibilità di riservare la partecipazione, per una quota non superiore al 50 per cento, «alle micro, piccole e medie imprese con sede legale e operativa nel territorio regionale» sarebbe illegittima per violazione dell'art. 117, secondo comma, lettera e), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>. Essa, infatti, si porrebbe in contrasto con l'<ref href="/akn/it/act/decretoLegislativo/stato/2016-04-18/50/!main#art_30__para_1">art. 30, comma 1, del decreto legislativo 18 aprile 2016, n. 50</ref> (Codice dei contratti pubblici), «che impone il rispetto dei principi di libera concorrenza e non discriminazione». La previsione della «riserva regionale» comporterebbe «una indebita restrizione del mercato escludendo gli operatori economici non toscani dalla possibilità di essere affidatari di pubbliche commesse».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Secondo il ricorrente, non vale l'obiezione che la norma non discriminerebbe in base alla territorialità, «prevedendo anche solo l'esistenza di una sede operativa nel territorio regionale come requisito di accesso agli appalti». Infatti, l'esistenza di una sede operativa prossima al luogo di esecuzione della prestazione «può essere richiesta solo in relazione a particolari modalità di esecuzione della specifica prestazione [...] non in modo generalizzato e valevole per tutti i contratti». La norma impugnata comporterebbe una «limitazione della concorrenza che non è giustificata da alcuna ragione se non quella - vietata - di attribuire una posizione di privilegio alle imprese del territorio per favorire l'economia regionale».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il ricorrente rileva che, secondo la giurisprudenza costituzionale, in materia di appalti pubblici gli aspetti relativi alle procedure di selezione e ai criteri di aggiudicazione «sono riconducibili alla tutela della concorrenza» (vengono citate le <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/186/!main">sentenze n. 186 del 2010</ref>, n. 320 del 2008 e n. 401 del 2007), di esclusiva competenza del legislatore statale, ragion per cui le regioni non potrebbero «prevedere in materia una disciplina difforme da quella statale».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Osserva poi che «l'art. 36 [cod. contratti pubblici] prevede che l'affidamento degli appalti di valore inferiore alle soglie comunitarie avvenga consultando elenchi di operatori economici senza alcuna indicazione di provenienza, o svolgendo indagini di mercato senza alcuna limitazione territoriale». La norma statale «prevede sì che - con criteri di rotazione - sia assicurata l'effettiva partecipazione delle micro, piccole e medie imprese, ma non consente alcuna discriminazione quanto alla loro localizzazione».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La norma regionale impugnata risulterebbe dunque invasiva della competenza esclusiva statale in materia di tutela della concorrenza e sarebbe indebitamente difforme dalla disciplina dettata dallo Stato.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>- Con memoria depositata il 25 luglio 2019 si è costituita in giudizio la Regione Toscana.
 Secondo la resistente, la norma impugnata rispetterebbe le disposizioni del codice dei contratti pubblici, in particolare l'art. 30, «coniugando, al contempo, il principio di libera concorrenza e non discriminazione ed il favor partecipationis per le microimprese, le piccole e le medie imprese».
 La Regione rileva che, nelle procedure negoziate di affidamento dei lavori, si riscontra un elevato numero di manifestazioni di interesse, con conseguente «difficoltà ad individuare modalità per la riduzione del numero dei soggetti da invitare»; e che il criterio del sorteggio, cui si è fatto ricorso, è stato contestato dagli operatori economici. La norma impugnata mirerebbe a contemperare il principio di concorrenza con la tutela delle piccole imprese.
 La Regione ricorda che, in base al comma 1 della disposizione impugnata, «[l]e stazioni appaltanti, quando procedono mediante indagine di mercato, individuano nella determina a contrarre il numero degli operatori da consultare, nel rispetto dei limiti minimi previsti dall'<ref href="/akn/it/act/decretoLegislativo/stato/2016/50/!main#art_36">articolo 36 del d.lgs. 50/2016</ref>». In base al comma 2, «[l]e stazioni appaltanti, nel caso in cui pervenga un numero di manifestazioni superiore a quello indicato nella determina a contrarre, possono ridurre il numero degli operatori mediante sorteggio». Il comma 3 prevede che le stazioni appaltanti utilizzino il Sistema telematico di acquisti della Regione Toscana (START) sia per effettuare l'indagine di mercato sia per l'eventuale sorteggio. Infine, la norma censurata (comma 4) prevede la possibilità di una riserva di partecipazione a favore delle «micro, piccole e medie imprese con sede legale e operativa nel territorio regionale», nel solo caso in cui gli interventi siano di «interesse meramente locale». Tale riserva sarebbe «aggiuntiva rispetto alla quota minima di partecipazione prevista dall'art. 36» cod. contratti pubblici e non determinerebbe «alcuna incidenza sulle modalità di partecipazione previste ai sensi dell'<ref href="/akn/it/act/decretoLegislativo/stato/2016/50/!main#art_36">articolo 36 del d.lgs. 50/2016</ref>, né tantomeno sul numero minimo di partecipanti ivi indicato».
 La norma impugnata preserverebbe l'effettiva partecipazione delle micro, piccole e medie imprese, mitigando la casualità del sorteggio.
 Per tali ragioni, la Regione Toscana chiede che la questione di legittimità costituzionale sia dichiarata infondata.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Nel giudizio in esame il Presidente del Consiglio dei ministri censura l'art. 10, comma 4, della legge della Regione Toscana 16 aprile 2019, n. 18 (Disposizioni per la qualità del lavoro e per la valorizzazione della buona impresa negli appalti di lavori, forniture e servizi. Disposizioni organizzative in materia di procedure di affidamento di lavori. Modifiche alla l.r. 38/2007).
 La norma impugnata è inserita nel capo II della legge regionale, che disciplina (come risulta dal suo art. 8) le «procedure negoziate per l'affidamento di lavori di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2016/50/!main#art_36">articolo 36 del d.lgs. 50/2016</ref>» (cioè, dei contratti di valore inferiore alla soglia comunitaria), e stabilisce che, «[i]n considerazione dell'interesse meramente locale degli interventi, le stazioni appaltanti possono prevedere di riservare la partecipazione alle micro, piccole e medie imprese con sede legale e operativa nel territorio regionale per una quota non superiore al 50 per cento e in tal caso la procedura informatizzata assicura la presenza delle suddette imprese fra gli operatori economici da consultare».
 Secondo il ricorrente, tale norma viola l'art. 117, secondo comma, lettera e), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> e gli <mref><ref href="/akn/it/act/decretoLegislativo/stato/2016-04-18/50/!main#art_30__para_1">artt. 30, comma 1</ref>, e <ref href="/akn/it/act/decretoLegislativo/stato/2016-04-18/50/!main#art_30__para_36">36 del decreto legislativo 18 aprile 2016, n. 50</ref></mref> (Codice dei contratti pubblici), in quanto la possibilità di riservare la partecipazione alle gare, per una quota non superiore al 50 per cento, «alle micro, piccole e medie imprese con sede legale e operativa nel territorio regionale» comporterebbe una «limitazione della concorrenza che non è giustificata da alcuna ragione se non quella - vietata - di attribuire una posizione di privilegio alle imprese del territorio per favorire l'economia regionale».</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>- Il ricorso è fondato.
 La norma impugnata è inserita in un capo che «disciplina le modalità di svolgimento delle indagini di mercato e di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e gestione degli elenchi degli operatori economici da consultare nell'ambito delle procedure negoziate per l'affidamento di lavori di cui all'<ref href="/akn/it/act/decretoLegislativo/stato/2016/50/!main#art_36">articolo 36 del d.lgs. 50/2016</ref>, in applicazione delle linee guida approvate dall'Autorità nazionale anticorruzione» (art. 8 della legge reg. Toscana n. 18 del 2019).
 Il citato art. 36 regola, fra l'altro, l'affidamento dei lavori di importo inferiore alla soglia comunitaria, che è fissata - per gli appalti pubblici di lavori e per le concessioni - in euro 5.350.000 (art. 35, commi 1, lettera a, e 3 del <ref href="/akn/it/act/decretoLegislativo/stato/2016/50/!main">d.lgs. n. 50 del 2016</ref>). A seguito della modifica introdotta dal <ref href="/akn/it/act/decretoLegge/stato/2019-04-18/32/!main">decreto-legge 18 aprile 2019, n. 32</ref> (Disposizioni urgenti per il rilancio del settore dei contratti pubblici, per l'accelerazione degli interventi infrastrutturali, di rigenerazione urbana e di ricostruzione a seguito di eventi sismici), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2019-06-14/55/!main">legge 14 giugno 2019, n. 55</ref>, lo stesso art. 36 prevede che, «salva la possibilità di ricorrere alle procedure ordinarie», le stazioni appaltanti procedono all'affidamento di lavori «mediante la procedura negoziata di cui all'articolo 63» in due ipotesi: quello dei lavori di importo compreso fra 150.000 euro e 350.000 euro e quello dei lavori di importo compreso fra 350.000 euro e 1.000.000 di euro. In entrambi i casi è richiesta la previa consultazione di almeno dieci o, rispettivamente, quindici operatori economici, ove esistenti, «nel rispetto di un criterio di rotazione degli inviti, individuati sulla base di indagini di mercato o tramite elenchi di operatori economici». L'avviso «sui risultati della procedura di affidamento contiene l'indicazione anche dei soggetti invitati». L'art. 63 disciplina la procedura negoziata senza previa pubblicazione di un bando di gara.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Le procedure di affidamento dei contratti sotto soglia sono poi regolate dalle Linee guida dell'Autorità Nazionale Anticorruzione (ANAC), adottate con delibera 26 ottobre 2016, n. 1097, in attuazione del previgente art. 36, comma 7, cod. contratti pubblici. In base alle Linee guida, la procedura «prende avvio con la determina a contrarre ovvero con atto a essa equivalente secondo l'ordinamento della singola stazione appaltante». Successivamente la procedura «si articola in tre fasi: a) svolgimento di indagini di mercato o consultazione di elenchi per la selezione di operatori economici da invitare al confronto competitivo; b) confronto competitivo tra gli operatori economici selezionati e invitati e scelta dell'affidatario; c) stipulazione del contratto» (punti 4.2 e 4.3). Nell'avviso di indagine di mercato la stazione appaltante si può riservare la facoltà di selezionare i soggetti da invitare mediante sorteggio (punti 4.1.5 e 4.2.3). Il vigente art. 216, comma 27-octies, cod. contratti pubblici prevede l'adozione di un regolamento governativo ma stabilisce che «le linee guida e i decreti adottati in attuazione delle previgenti disposizioni di cui agli articoli [...] 36, comma 7, [...] rimangono in vigore o restano efficaci fino alla data di entrata in vigore del regolamento di cui al presente comma».
 3.- Così tratteggiato il contesto normativo in cui si inserisce la norma impugnata, è opportuno precisare la sua esatta portata, anche alla luce di alcune affermazioni del ricorrente.
 Per un verso infatti la disposizione non fissa un requisito di accesso alle procedure negoziate, sicché non si può dire che essa escluda a priori le imprese non toscane dalla partecipazione agli appalti in questione, essendo la riserva di partecipazione (che le stazioni appaltanti possono prevedere) limitata al massimo al 50 per cento delle imprese invitate al confronto competitivo.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per altro verso non è nemmeno esatto dire che essa richiede in via alternativa la sede legale o la sede operativa nel territorio regionale: la disposizione utilizza la congiunzione «e», né ci sono ragioni logiche che portino a superarne la lettera. Dalla possibile riserva di partecipazione sono dunque escluse le micro, piccole e medie imprese che hanno solo una delle due sedi nel territorio regionale.
 Inoltre, poiché la norma impugnata precisa che si tratta di una possibile riserva della «partecipazione», si deve ritenere che il suo oggetto si collochi nel secondo passaggio della procedura sopra ricordata, cioè nella fase dell'invito a presentare un'offerta, dopo lo svolgimento della consultazione degli operatori economici. In virtù della disposizione censurata, la stazione appaltante può prevedere che un certo numero di offerte (non più del 50 per cento) debba provenire da micro, piccole e medie imprese toscane, e può così svincolarsi dal rispetto dei criteri generali previsti per la selezione delle imprese da invitare. In altri termini, la norma impugnata può giustificare l'invito di imprese toscane che dovrebbero essere escluse a favore di imprese non toscane, in quanto - in ipotesi - maggiormente qualificate sulla base dei criteri stessi.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>- Così precisata la portata della disposizione impugnata, essa risulta costituzionalmente illegittima per violazione dell'art. 117, secondo comma, lettera e), Cost.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>È opportuno ricordare che, in base alla giurisprudenza di questa Corte, «le disposizioni del codice dei contratti pubblici [...] regolanti le procedure di gara sono riconducibili alla materia della tutela della concorrenza, e [...] le Regioni, anche ad autonomia speciale, non possono dettare una disciplina da esse difforme (tra le tante, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/263/!main">sentenze n. 263 del 2016</ref>, n. 36 del 2013, n. 328 del 2011, n. 411 e n. 322 del 2008)» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/39/!main">sentenza n. 39 del 2020</ref>). Ciò vale «anche per le disposizioni relative ai contratti sotto soglia (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/263/!main">sentenze n. 263 del 2016</ref>, n. 184 del 2011, n. 283 e n. 160 del 2009, n. 401 del 2007), [...] senza che rilevi che la procedura sia aperta o negoziata (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2008/322/!main">sentenza n. 322 del 2008</ref>)» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/39/!main">sentenza n. 39 del 2020</ref>).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Occorre ricordare inoltre che, in tale contesto, questa Corte ha più volte dichiarato costituzionalmente illegittime norme regionali di protezione delle imprese locali, sia nel settore degli appalti pubblici (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2013/28/!main">sentenze n. 28 del 2013</ref> e n. 440 del 2006) sia in altri ambiti (ad esempio, <mref><ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/221/!main">sentenze n. 221</ref> e n. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2018/83/!main">83 del 2018</ref></mref> e n. 190 del 2014).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>La norma impugnata disciplina in generale una fase della procedura negoziata di affidamento dei lavori pubblici sotto soglia ed è dunque riconducibile all'ambito materiale delle procedure di aggiudicazione dei contratti pubblici, che, in quanto attinenti alla «tutela della concorrenza», sono riservate alla competenza esclusiva del legislatore statale (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2013/28/!main">sentenza n. 28 del 2013</ref>).
 Considerata nel suo contenuto, poi, la norma censurata prevede la possibilità di riservare un trattamento di favore per le micro, piccole e medie imprese radicate nel territorio toscano e, dunque, anche sotto questo profilo è di ostacolo alla concorrenza, in quanto, consentendo una riserva di partecipazione, altera la par condicio fra gli operatori economici interessati all'appalto.
 La norma impugnata, in effetti, contrasta con entrambi i parametri interposti invocati dal ricorrente: con l'art. 30, comma 1, cod. contratti pubblici perché viola i principi di libera concorrenza e non discriminazione in esso sanciti, e con l'art. 36, comma 2, dello stesso codice perché introduce una possibile riserva di partecipazione (a favore delle micro, piccole e medie imprese locali) non consentita dalla legge statale.
 Gli argomenti difensivi spesi dalla Regione non risultano idonei a mutare tali conclusioni. La resistente ha sottolineato la necessità di ricorrere al sorteggio per individuare gli operatori da invitare e ha ricordato le contestazioni a tale metodo di selezione da parte degli operatori stessi; ha invocato il «favor partecipationis per le microimprese, le piccole e le medie imprese»; ha rilevato che la riserva sarebbe «aggiuntiva rispetto alla quota minima di partecipazione prevista dall'art. 36» cod. contratti pubblici. Nessuna di queste considerazioni, tuttavia, risulta idonea a giustificare una norma che, come quella impugnata, non è diretta a favorire le micro, piccole e medie imprese tout court, quanto invece le «micro, piccole e medie imprese con sede legale e operativa nel territorio regionale», nel perseguimento di un obiettivo che altera la concorrenza in contrasto con quanto previsto dalla normativa statale in materia, come sopra illustrato.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'art. 10, comma 4, della legge della Regione Toscana 16 aprile 2019, n. 18 (Disposizioni per la qualità del lavoro e per la valorizzazione della buona impresa negli appalti di lavori, forniture e servizi. Disposizioni organizzative in materia di procedure di affidamento di lavori. Modifiche alla l.r. 38/2007).
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-05-05">5 maggio 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#dariaDePretis" as="#relatore">Daria de PRETIS</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-05-27">27 maggio 2020</date>.
 Il Direttore della Cancelleria
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
