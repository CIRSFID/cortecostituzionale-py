<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>434</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:434</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/10/1992</data_decisione>
    <data_deposito>10/11/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANELLO, &#13;
 prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 53 del d.P.R.    &#13;
 20  dicembre 1979, n. 761 (Stato giuridico del personale delle unità    &#13;
 sanitarie locali) e 6, primo comma, del d.l. 22 dicembre 1981, n. 791    &#13;
 (Disposizioni in materia previdenziale), convertito con modificazioni    &#13;
 nella legge 26 febbraio 1982, n. 54, promosso con ordinanza emessa il    &#13;
 12 novembre  1991  dal  Tribunale  amministrativo  regionale  per  la    &#13;
 Sicilia,  sezione  di  Catania, sul ricorso proposto da Maria Antonia    &#13;
 Calogero contro la U.S.L. n. 41 di Messina/Nord ed altri, iscritta al    &#13;
 n. 303 del  registro  ordinanze  1992  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  23, prima serie speciale, dell'anno    &#13;
 1992;                                                                    &#13;
    Udito nella camera di consiglio del 21  ottobre  1992  il  Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto che il Tribunale amministrativo regionale per la Sicilia,    &#13;
 sezione  di  Catania  -  nel  corso  di  un  giudizio proposto da una    &#13;
 infermiera dipendente di una  unità  sanitaria  locale,  avverso  la    &#13;
 deliberazione con la quale era stata collocata a riposo al compimento    &#13;
 del   sessantacinquesimo  anno  di  età,  pur  non  avendo  maturata    &#13;
 l'anzianità minima per il conseguimento del diritto a pensione - con    &#13;
 ordinanza 12 novembre 1991 ha  sollevato  questione  di  legittimità    &#13;
 costituzionale,  in  riferimento  agli  artt.  3, 4 e 38 Cost., degli    &#13;
 artt. 53 del d.P.R. 20 dicembre 1979, n. 761 e 6,  comma  primo,  del    &#13;
 d.l.  22 dicembre 1981, n. 791, convertito nella l. 26 febbraio 1982,    &#13;
 n. 54 "nella parte in cui non prevedono il mantenimento  in  servizio    &#13;
 fino  al  settantesimo  anno  di  età,  dei  dipendenti delle unità    &#13;
 sanitarie locali i quali non  abbiano  maturato  l'anzianità  minima    &#13;
 richiesta per ottenere il trattamento di quiescenza";                    &#13;
      che  secondo  il giudice a quo - tenuto conto che, a norma della    &#13;
 l. 19 febbraio 1991, n. 50, per i primari ospedalieri che non abbiano    &#13;
 raggiunto il numero di  anni  di  servizio  effettivo  necessari  per    &#13;
 conseguire il massimo della pensione è previsto che possano chiedere    &#13;
 di  essere  trattenuti  in  servizio  fino  al raggiungimento di tale    &#13;
 anzianità e, comunque, fino al settantesimo anno di età - è iniquo    &#13;
 ed irrazionale non consentire al  personale  dei  livelli  inferiori,    &#13;
 munito  di  minor  reddito  e  certamente  di  più ridotta capacità    &#13;
 economica, di permanere in servizio oltre il limite  d'età  previsto    &#13;
 in via generale, per maturare almeno il diritto alla pensione minima;    &#13;
    Considerato  che  questa Corte, con sentenza 9 marzo 1992, n. 90 -    &#13;
 resa in un giudizio  riguardante  un  aiuto  ospedaliero  -  ha  già    &#13;
 dichiarato  l'illegittimità  costituzionale  dell'intero primo comma    &#13;
 dell'art. 53 nella  parte  in  cui  non  consente  al  personale  ivi    &#13;
 contemplato   (sanitario   e  tecnico  laureato,  amministrativo,  di    &#13;
 assistenza religiosa e professionale e categorie "restanti"), che  al    &#13;
 raggiungimento  del  limite  di età per il collocamento a riposo non    &#13;
 abbia compiuto il numero degli anni richiesti per ottenere il  minimo    &#13;
 della  pensione,  di  rimanere,  su  richiesta,  in  servizio fino al    &#13;
 conseguimento di tale anzianità minima e,  comunque,  non  oltre  il    &#13;
 settantesimo anno di età;                                               &#13;
      che l'art. 6, comma primo, del d.l. 22 dicembre 1981, n. 781 è,    &#13;
 pertanto,  modificato  con  riferimento al suddetto personale in base    &#13;
 all'art. 53 del  d.P.R.  n.  761  del  1979,  come  risultante  dalla    &#13;
 suindicata declaratoria d'illegittimità costituzionale;                 &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9 delle norme integrative  per  i  giudizi  davanti  alla  Corte    &#13;
 costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt. 53  del  d.P.R.  20  dicembre    &#13;
 1979,  n.  761  (Stato giuridico del personale delle unità sanitarie    &#13;
 locali) e  6,  comma  primo,  del  d.l.  22  dicembre  1981,  n.  791    &#13;
 (Disposizioni  in  materia  previdenziale), convertito nella legge 26    &#13;
 febbraio 1982, n. 54, sollevata in riferimento agli artt. 3, 4  e  38    &#13;
 della  Costituzione,  dal  Tribunale  amministrativo regionale per la    &#13;
 Sicilia, sezione di Catania, con ordinanza 12 novembre 1991.             &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 23 ottobre 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                        Il redattore: PESCATORE                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 10 novembre 1992.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
