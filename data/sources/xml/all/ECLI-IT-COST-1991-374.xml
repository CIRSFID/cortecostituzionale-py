<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>374</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:374</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/1991</data_decisione>
    <data_deposito>23/07/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 52,  alinea  b,    &#13;
 comma  secondo,  del  d.P.R.  29 settembre 1973, n. 602 (Disposizioni    &#13;
 sulla riscossione delle imposte sul reddito) promosso  con  ordinanza    &#13;
 emessa  il  22  dicembre 1990 dal Pretore di Bergamo nel procedimento    &#13;
 civile vertente  tra  Geromel  Renata  e  Lazzaroni  Vilia  ed  altra    &#13;
 iscritta  al  n.  303  del registro ordinanze 1991 e pubblicata nella    &#13;
 Gazzetta Ufficiale della Repubblica  n.  18,  prima  serie  speciale,    &#13;
 dell'anno 1991;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  10  luglio  1991  il  Giudice    &#13;
 relatore Renato Granata;                                                 &#13;
    Ritenuto  che  con  ordinanza  del  22 dicembre 1990 il Pretore di    &#13;
 Bergamo ha sollevato - in riferimento all'art. 24 della  Costituzione    &#13;
 -   questione  di  legittimità  costituzionale  in  via  incidentale    &#13;
 dell'art. 52 d.P.R. 29 settembre  1973  n.  602  (disposizioni  sulla    &#13;
 riscossione delle imposte sul reddito) nella parte in cui stabilisce,    &#13;
 tra l'altro, che per i beni mobili pignorati nella casa di abitazione    &#13;
 del  debitore  esecutato  - l'opposizione di terzo all'esecuzione (ex    &#13;
 art. 619 cod. proc. civ.) non possa essere proposta dal coniuge e dai    &#13;
 parenti ed affini sino al terzo grado del contribuente  (nonché  dei    &#13;
 coobbligati);                                                            &#13;
      che   in   particolare   il   giudice   rimettente  ritiene  che    &#13;
 l'esecuzione fiscale debba conformarsi  alle  regole  dell'esecuzione    &#13;
 ordinaria  ed  innanzi  tutto a quella dell'opposizione di terzo, non    &#13;
 essendo possibile inibire a quest'ultimo - ove versi in  rapporto  di    &#13;
 coniugio,  di  parentela  o  di affinità con il debitore esecutato -    &#13;
 l'accesso alle difese giudiziali;                                        &#13;
      che  l'Avvocatura   generale   dello   Stato,   intervenuta   in    &#13;
 rappresentanza del Presidente del Consiglio dei Ministri, ha concluso    &#13;
 perché la questione venga dichiarata manifestamente infondata;          &#13;
    Considerato  che,  questa Corte, con ordinanze n. 283 del 1984, n.    &#13;
 123 del 1986, nonché nn. 191 e 484 del 1989, ha  già  dichiarato  -    &#13;
 anche  con riferimento al parametro indicato dal giudice rimettente -    &#13;
 la   manifesta   infondatezza   della   questione   di   legittimità    &#13;
 costituzionale  dell'art. 52, comma 2, lettera b) d.P.R. 29 settembre    &#13;
 1973 n. 602 nella parte in cui non consente al coniuge, ai parenti ed    &#13;
 affini fino al terzo grado del contribuente di  proporre  opposizione    &#13;
 di terzo ex                                                              &#13;
 art.  619  cod. proc. civ. per quanto riguarda i beni pignorati nella    &#13;
 casa di abitazione comune;                                               &#13;
      che in precedenza la medesima  questione  -  avente  ad  oggetto    &#13;
 l'art.  207,  lett. b, del d.P.R. 29 gennaio 1958 n. 645 (testo unico    &#13;
 delle leggi sulle imposte dirette) che nel  previgente  regime  della    &#13;
 riscossione  delle imposte prevedeva una disciplina del tutto analoga    &#13;
 a quella dettata dall'art. 52 cit. - era stata dichiarata non fondata    &#13;
 con sentenza n. 42 del 1964 e manifestamente infondata con successive    &#13;
 ordinanze (ex plurimis, n. 71 del 1971 e n. 36 del 1974);                &#13;
      che  il  giudice  rimettente - peraltro omettendo di tener conto    &#13;
 della giurisprudenza di questa Corte - non ha  addotto  motivi  nuovi    &#13;
 che possano fondare una diversa decisione;                               &#13;
      che  pertanto  va  dichiarata  la  manifesta  infondatezza della    &#13;
 questione sollevata;                                                     &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953 n. 87    &#13;
 e 9, secondo comma, delle norme integrative  per  i  giudizi  dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  52  d.P.R.  29  settembre  1973   n.   602    &#13;
 (Disposizioni sulla riscossione delle imposte sul reddito) sollevata,    &#13;
 in riferimento all'art. 24 della Costituzione, dal Pretore di Bergamo    &#13;
 con l'ordinanza in epigrafe.                                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 luglio 1991.                                &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: GRANATA                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 luglio 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
