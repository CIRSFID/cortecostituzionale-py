<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>127</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:127</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Antonio Manca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/11/1967</data_decisione>
    <data_deposito>23/11/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. &#13;
 LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt.  231,  primo  &#13;
 comma,  e  398, secondo comma, del Codice di procedura penale, promossi  &#13;
 con le seguenti ordinanze;                                               &#13;
     1) ordinanza emessa il 9 febbraio 1967  dal  pretore  di  Gela  nel  &#13;
 procedimento  penale a carico di Moscato Melchiorre, iscritta al n.  75  &#13;
 del Registro ordinanze 1967 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 109 del 29 aprile 1967;                                    &#13;
     2) ordinanza emessa il 24 febbraio 1967 dal pretore  di  Barcellona  &#13;
 Pozzo  di  Gotto  nel  procedimento penale a carico di Crinò Giovanni,  &#13;
 iscritta al n. 91  del  Registro  ordinanze  1967  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 157 del 24 giugno 1967.           &#13;
     Udita  nella  camera di consiglio del 18 ottobre 1967  la relazione  &#13;
 del Giudice Antonio Manca;                                               &#13;
     Ritenuto che con ordinanza emessa il 9 febbraio 1967 il pretore  di  &#13;
 Gela,  nel  procedimento  penale  a    carico di Moscato Melchiorre, ha  &#13;
 sollevato la questione  di legittimità costituzionale degli artt. 231,  &#13;
 prima parte,  e 398, secondo comma, del Codice di procedura  penale, in  &#13;
 relazione all'art. 24 della costituzione, nelle  parti in  cui  rendono  &#13;
 facoltativo  per  il pretore il  procedere alla istruzione sommaria nei  &#13;
 giudizi per i reati di competenza del pretore medesimo;                  &#13;
     che detta ordinanza, letta in udienza e ritualmente  notificata  al  &#13;
 Presidente  del  Consiglio  dei Ministri e comunicata ai Presidenti dei  &#13;
 due rami del Parlamento, iscritta al n. 75 del registro ordinanze 1967,  &#13;
 è stata pubblicata nella Gazzetta Ufficiale n. 109 del 29 aprile 1967;  &#13;
     che non vi è stata Costituzione di parti, né intervento davanti a  &#13;
 questa Corte;                                                            &#13;
     Ritenuto che la stessa questione è stata  sollevata  altresì,  in  &#13;
 relazione  agli  artt.  3  e  24  della Costituzione, con ordinanza del  &#13;
 pretore di Barcellona Pozzo di Gotto, nel procedimento penale a  carico  &#13;
 di Crinò Giovanni;                                                      &#13;
     che  anche questa ordinanza, ritualmente notificata e comunicata ai  &#13;
 sensi dell'art.  23 della legge 11 marzo 1953, n. 87,  iscritta  al  n.  &#13;
 91  del  Registro  ordinanze  1967,  è stata pubblicata nella Gazzetta  &#13;
 Ufficiale n. 157 del 24 giugno 1967;                                     &#13;
     che  non vi è stata Costituzione di parti né intervento davanti a  &#13;
 questa Corte;                                                            &#13;
     Considerato che con sentenza n. 46 del 12 aprile 1967, questa Corte  &#13;
 ha dichiarato non fondate:                                               &#13;
     a) la questione di legittimità costituzionale  dell'art.  231  del  &#13;
 Codice  di  procedura penale, nella parte in cui attribuisce al pretore  &#13;
 la facoltà di emettere il decreto di citazione per il giudizio,  senza  &#13;
 compiere  atti  di istruzione sommaria, in riferimento all'art. 3 della  &#13;
 Costituzione;                                                            &#13;
     b) la questione di legittimità costituzionale dell'art. 398  dello  &#13;
 stesso   Codice,  nella  parte  in  cui  non  prevede  l'obbligo  della  &#13;
 contestazione del fatto, qualora non si proceda al compimento  di  atti  &#13;
 di  istruzione  sommaria,  in  riferimento  all'art. 24, secondo comma,  &#13;
 della Costituzione;                                                      &#13;
     che i principi enunciati nella richiamata sentenza,  devono  essere  &#13;
 riaffermati, non essendo stata dedotta e non sussistendo alcuna ragione  &#13;
 in contrario;                                                            &#13;
     Visti gli artt. 26, secondo comma, 29 della legge 11 marzo 1953, n.  &#13;
 87,  e  9, secondo comma, delle Norme integrative per i giudizi davanti  &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la manifesta infondatezza della questione sollevata con le  &#13;
 ordinanze  dei  pretori  di  Gela  e  di  Barcellona  Pozzo  di  Gotto,  &#13;
 riguardante  la  legittimità  costituzionale  degli  artt.  231, primo  &#13;
 comma, e 398,  secondo  comma,  del  Codice  di  procedura  penale,  in  &#13;
 riferimento agli artt. 3 e 24 della Costituzione;                        &#13;
     ordina   la  restituzione  degli  atti  alle  competenti  autorità  &#13;
 giudiziarie.                                                             &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 15 novembre 1967.       &#13;
                                   GASPARE  AMBROSINI - ANTONINO PAPALDO  &#13;
                                   - NICOLA JAEGER - GIOVANNI  CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   ALDO  SANDULLI  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
