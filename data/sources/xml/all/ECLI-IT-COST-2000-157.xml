<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>157</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:157</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Riccardo Chieppa</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/05/2000</data_decisione>
    <data_deposito>24/05/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Cesare RUPERTO, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità costituzionale dell'art. 27, comma 4,    &#13;
 della  legge della Regione Calabria 13 maggio 1996, n. 8 (Norme sulla    &#13;
 dirigenza  e  sull'ordinamento  degli uffici del Consiglio), promosso    &#13;
 con  ordinanza  emessa  il 6 maggio 1998 dal Tribunale amministrativo    &#13;
 regionale  della  Calabria,  sezione staccata di Reggio Calabria, sul    &#13;
 ricorso  proposto  da  Esposito  Adriana  Stella  contro  la  Regione    &#13;
 Calabria  ed  altro, iscritta al n. 759 del registro ordinanze 1998 e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 42, prima    &#13;
 serie speciale, dell'anno 1998.                                          &#13;
     Visti  gli  atti di costituzione di Esposito Adriana Stella e del    &#13;
 Consiglio della Regione Calabria;                                        &#13;
     Udito   nell'udienza  pubblica  dell'11 aprile  2000  il  giudice    &#13;
 relatore Riccardo Chieppa;                                               &#13;
     Udito  l'avvocato  Silvio  Dattola per il Consiglio della Regione    &#13;
 Calabria.                                                                &#13;
     Ritenuto  che  una  partecipante  alla  selezione  riservata  per    &#13;
 l'assunzione  da  parte  del  Consiglio  regionale  della Calabria ha    &#13;
 impugnato   davanti   al  Tribunale  amministrativo  regionale  della    &#13;
 Calabria  -  sezione  staccata  di Reggio Calabria - la deliberazione    &#13;
 dell'Ufficio  di  Presidenza dello stesso Consiglio, con la quale era    &#13;
 stata  dichiarata  non idonea, per mancanza del requisito del periodo    &#13;
 lavorativo non inferiore a 24 mesi alla data del 31 maggio 1993 (1050    &#13;
 ore  anziché 3744 ore), investendo con i motivi la sua esclusione ed    &#13;
 i  criteri di valutazione del servizio prestato, nonché la procedura    &#13;
 seguita per l'esclusione;                                                &#13;
         che  il  Tribunale  amministrativo regionale della Calabria -    &#13;
 sezione  staccata  di  Reggio  Calabria  -,  con  ordinanza emessa il    &#13;
 6 maggio  1998,  ha  sollevato,  in riferimento agli artt. 51, 3 e 97    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art. 27,  comma  4, della legge della Regione Calabria 13 maggio    &#13;
 1996,  n. 8,  nella  parte in cui stabilisce che successivamente alla    &#13;
 rideterminazione  della pianta organica, conseguente alla rilevazione    &#13;
 dei   carichi  di  lavoro,  l'Ufficio  di  Presidenza  provvederà  a    &#13;
 ricoprire  i  posti  vacanti, nel rispetto della normativa vigente in    &#13;
 materia  di  assunzioni, con riserva a favore del personale che abbia    &#13;
 comunque  compiuto un periodo lavorativo non inferiore a 24 mesi alla    &#13;
 data del 31 maggio 1993 presso il Consiglio regionale;                   &#13;
         che nel giudizio innanzi alla Corte si è costituita la parte    &#13;
 privata,  che ha condiviso le ragioni addotte dal giudice a quo nella    &#13;
 ordinanza di rimessione, nonché il Consiglio della Regione Calabria,    &#13;
 che ha concluso per la infondatezza della questione;                     &#13;
         che il 28 marzo 2000 il Consiglio regionale della Calabria ha    &#13;
 presentato  una memoria, con la quale ha ribadito le conclusioni già    &#13;
 rassegnate;                                                              &#13;
         che  in  sede  di  discussione  orale la difesa del Consiglio    &#13;
 regionale  si  è  richiamata  ad  una  sopravvenuta  legge regionale    &#13;
 3 marzo 2000, n. 6, con interpretazione autentica dell'art. 27, comma    &#13;
 4,  della  legge regionale 13 maggio 1996, n. 8, oggetto del presente    &#13;
 giudizio,  prospettando, tra l'altro, la cessazione della materia del    &#13;
 contendere.                                                              &#13;
     Considerato   che,   in   epoca   successiva  alla  ordinanza  di    &#13;
 rimessione,  è  stata  approvata  e  pubblicata  la  legge regionale    &#13;
 3 marzo 2000, n. 6, con interpretazione autentica dell'art. 27, comma    &#13;
 4,  della  legge regionale 13 maggio 1996, n. 8, oggetto del presente    &#13;
 giudizio;                                                                &#13;
         che si rende, pertanto, necessaria la restituzione degli atti    &#13;
 al  giudice  rimettente,  spettando ad esso di valutare se, alla luce    &#13;
 dell'intervenuto  mutamento  del quadro normativo, cui fa riferimento    &#13;
 l'anzidetta  ordinanza  di  rimessione,  le questioni sollevate siano    &#13;
 tuttora rilevanti per la definizione del giudizio a quo;                 &#13;
         che,   con   l'occasione,  il  giudice  a  quo  potrà  anche    &#13;
 verificare  la  completezza  del  contraddittorio  in  relazione agli    &#13;
 effetti   sull'intera   selezione   concorsuale  della  questione  di    &#13;
 legittimità costituzionale sollevata d'ufficio dal medesimo giudice,    &#13;
 risultando da tempo intervenuta la conclusione della procedura con la    &#13;
 graduatoria della selezione e l'individuazione dei soggetti portatori    &#13;
 di posizione qualificata.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Ordina la restituzione degli atti al giudice a quo.                  &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 maggio 2000.                               &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Chieppa                            &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 24 maggio 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
