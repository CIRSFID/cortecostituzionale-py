<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>50</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:50</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/02/1994</data_decisione>
    <data_deposito>23/02/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art.  18, primo    &#13;
 comma, del d.P.R. 22 dicembre 1986, n. 917  (Approvazione  del  testo    &#13;
 unico delle imposte sui redditi), promosso con ordinanza emessa il 24    &#13;
 marzo 1993 dalla Commissione tributaria di primo grado di Mantova sul    &#13;
 ricorso  proposto  da  Jacobellis  Vincenzo  contro  l'Intendenza  di    &#13;
 Finanza di Mantova, iscritta al n. 352 del registro ordinanze 1993  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 28, prima    &#13;
 serie speciale, dell'anno 1993;                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 17 novembre 1993 il Giudice    &#13;
 relatore Massimo Vari;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un giudizio avente ad oggetto  la  richiesta  di    &#13;
 rimborso  di  imposte IRPEF versate sull'indennità di fine rapporto,    &#13;
 corrisposta dall'Ente nazionale di previdenza e assistenza medici  ad    &#13;
 un  proprio  iscritto,  a seguito di attività prestata per conto dei    &#13;
 disciolti enti mutualistici e del Servizio  sanitario  nazionale,  la    &#13;
 Commissione  tributaria  di primo grado di Mantova, con ordinanza del    &#13;
 24 marzo 1993, ha sollevato, in riferimento agli artt. 3 e 53,  primo    &#13;
 comma,  della  Costituzione, questione di legittimità costituzionale    &#13;
 dell'art. 18, primo comma, del  d.P.R.  22  dicembre  1986,  n.  917,    &#13;
 contenente  il  testo unico delle imposte sui redditi, nella parte in    &#13;
 cui non prevede che l'imponibile sul quale determinare l'imposta  per    &#13;
 le  indennità  di cui alla lettera c) del primo comma dell'art. 16 -    &#13;
 tra le quali rientra quella corrisposta  dall'Ente  previdenziale  ai    &#13;
 medici   addetti  alla  medicina  generale  -  sia  computato  previa    &#13;
 detrazione di una somma pari  alla  percentuale  di  tale  indennità    &#13;
 corrispondente  al  rapporto, alla data in cui è maturato il diritto    &#13;
 alla percezione, tra il contributo posto a carico del  percipiente  e    &#13;
 l'aliquota  complessiva  del contributo obbligatorio versato all'Ente    &#13;
 previdenziale medesimo.                                                  &#13;
    Detto criterio di computo, previsto dallo  art.  17  dello  stesso    &#13;
 decreto  per  le  indennità di fine rapporto afferenti a rapporti di    &#13;
 lavoro dipendente, andrebbe,  ad  avviso  del  remittente,  applicato    &#13;
 anche  nel  caso qui considerato, essendo identiche la formazione, la    &#13;
 gestione  e  l'erogazione  delle  due  indennità.   L'ordinanza   di    &#13;
 rimessione  richiama, inoltre, la sentenza della Corte costituzionale    &#13;
 n. 178 del 1986, secondo la  quale  sarebbe  illogico  ed  arbitrario    &#13;
 ritenere  che  l'indennità di buonuscita si profili come reddito per    &#13;
 la parte afferente in via virtuale  alla  contribuzione  versata  dal    &#13;
 dipendente.                                                              &#13;
    2.  -  È  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,   il  quale  ha  chiesto  che  la  questione  sollevata  venga    &#13;
 dichiarata infondata, in ragione delle peculiarità  del  trattamento    &#13;
 di  previdenza  previsto  per  i  medici  che  prestano la loro opera    &#13;
 professionale per conto del Servizio sanitario nazionale, trattamento    &#13;
 che è diverso dalla indennità di fine rapporto che gli enti a  ciò    &#13;
 preposti,  quali  l'E.N.P.A.S.  e  l'I.N.A.D.E.L.,  corrispondono  ai    &#13;
 dipendenti pubblici, onde appare improponibile il raffronto  ai  fini    &#13;
 dell'art. 3 della Costituzione.                                          &#13;
    Inoltre,  secondo l'Avvocatura dello Stato, la norma impugnata non    &#13;
 può dirsi in contrasto con l'art. 53 della Costituzione,  in  quanto    &#13;
 la  particolare  contribuzione  a  carico  dei medici copre, oltre al    &#13;
 trattamento  di  malattia,  il  rischio  della   invalidità   e   il    &#13;
 trattamento a favore dei superstiti, e può dar luogo ad una pensione    &#13;
 sostitutiva  del  trattamento  ordinario,  donde  l'impossibilità di    &#13;
 sceverare, nel contributo a carico del medico, la parte che afferisce    &#13;
 al trattamento ordinario, l'unico, cioè, che ha qualche analogia con    &#13;
 l'indennità di fine rapporto.<diritto>Considerato in diritto</diritto>1. - La Corte è chiamata a decidere se l'art.  18,  primo  comma,    &#13;
 del  d.P.R. 22 dicembre 1986, n. 917, contrasti con gli artt. 3 e 53,    &#13;
 primo comma, della Costituzione, nella parte in cui non prevede,  per    &#13;
 le  indennità  di cui alla lettera c) del primo comma dell'art. 16 -    &#13;
 tra le  quali  rientra  l'indennità  di  fine  rapporto  corrisposta    &#13;
 dall'E.N.P.A.M.   ai   medici   di   medicina   generale,  a  seguito    &#13;
 dell'attività prestata per conto dei disciolti enti  mutualistici  e    &#13;
 del  Servizio  sanitario nazionale - la riduzione dell'imponibile per    &#13;
 una somma pari alla  misura  di  tale  indennità  corrispondente  al    &#13;
 contributo  posto  a  carico  del percipiente, a differenza di quanto    &#13;
 stabilito,  invece,  dall'art.  17,  per   le   analoghe   indennità    &#13;
 corrisposte  in  occasione  della  cessazione  di  rapporti di lavoro    &#13;
 dipendente.                                                              &#13;
    Secondo il giudice a quo, premesso che  il  rapporto  intrattenuto    &#13;
 dai  medici  addetti alla medicina generale con il Servizio sanitario    &#13;
 nazionale costituisce un rapporto di lavoro autonomo, continuativo  e    &#13;
 coordinato,  il cui trattamento tributario, quanto all'indennità qui    &#13;
 considerata, trova  fondamento  nella  lettera  c)  del  primo  comma    &#13;
 dell'art.  16  del d.P.R. 22 dicembre 1986, n. 917, la determinazione    &#13;
 dell'imposta deve essere,  conseguentemente,  effettuata  secondo  la    &#13;
 disciplina  dell'art.  18  del  citato  d.P.R.  Detta  norma sarebbe,    &#13;
 tuttavia, da considerare  ingiustificatamente  discriminatoria  nella    &#13;
 parte  in  cui  non prevede, a differenza di quanto dispone l'art. 17    &#13;
 con riferimento  alle  indennità  afferenti  a  rapporti  di  lavoro    &#13;
 dipendente,  che  dall'imponibile  IRPEF  sia detratta una somma pari    &#13;
 alla percentuale dell'indennità  corrispondente  al  rapporto,  alla    &#13;
 data  in  cui  è maturato il diritto alla percezione, tra l'aliquota    &#13;
 del  contributo  posto  a  carico  del   percipiente   e   l'aliquota    &#13;
 complessiva del contributo versato all'ente di previdenza.               &#13;
    2. - La questione non è fondata.                                     &#13;
    Osserva  la  Corte  che  il  d.P.R.  22  dicembre  1986,  n.  917,    &#13;
 nell'indicare,  all'art.  16,  primo  comma,  i  redditi  soggetti  a    &#13;
 tassazione  separata,  considera  distintamente  i  trattamenti  e le    &#13;
 indennità corrisposte  per  la  cessazione  di  rapporti  di  lavoro    &#13;
 dipendente (lettera a) e le indennità percepite per la cessazione di    &#13;
 rapporti  di  collaborazione  coordinata  e continuativa (lettera c),    &#13;
 facendo da ciò discendere, quanto al computo dell'imposta, giusta la    &#13;
 disciplina  prevista,  rispettivamente,  dagli   artt.   17   e   18,    &#13;
 conseguenze che concernono non solo i criteri di determinazione della    &#13;
 base  imponibile,  nell'ambito  dei  quali  la  parte  di  indennità    &#13;
 corrispondente alla quota dei contributi versati dall'iscritto è uno    &#13;
 solo degli elementi presi in considerazione dallo stesso art. 17,  ma    &#13;
 anche  le  modalità di individuazione dell'aliquota di tassazione da    &#13;
 applicarsi.                                                              &#13;
    L'assetto normativo così dato  si  ricollega,  sotto  il  profilo    &#13;
 sistematico,   ai   differenti   regimi  di  imposizione  tributaria,    &#13;
 apprestati in generale dal legislatore, con previsione di  specifiche    &#13;
 regole,  da  una  parte,  per  i  redditi  di  lavoro  dipendente  e,    &#13;
 dall'altra, per i redditi  di  lavoro  autonomo,  in  corrispondenza,    &#13;
 rispettivamente, del capo IV e del capo V del d.P.R. n. 917 del 1986.    &#13;
    La  diversità  degli  assetti normativi nei quali ricadono le due    &#13;
 indennità, mentre esclude  che  possa  qui  procedersi,  così  come    &#13;
 vorrebbe,   invece,   l'ordinanza   di   rimessione,   alla  parziale    &#13;
 trasposizione di criteri regolatori  dall'uno  all'altro,  porta,  al    &#13;
 tempo  stesso,  a  ritenere insussistente la denunciata disparità di    &#13;
 trattamento.                                                             &#13;
    Non  si   può   prescindere,   oltretutto,   dalle   peculiarità    &#13;
 ordinamentali   del   fondo   di  previdenza  nell'ambito  della  cui    &#13;
 disciplina rientra l'indennità corrisposta  dall'ente  previdenziale    &#13;
 ai  medici  di medicina generale, come è dato desumere dallo Statuto    &#13;
 dell'Ente stesso (d.P.R.  2  settembre  1959,  n.  931  e  successive    &#13;
 modificazioni)  e  dalle  correlative  norme  regolamentari  (decreto    &#13;
 ministeriale 15 ottobre 1976 e successive modificazioni). Dalle fonti    &#13;
 normative in questione si evince che l'indennità di cui  si  tratta,    &#13;
 basata  su  forme  di  contribuzione  la  cui  misura è rimessa agli    &#13;
 accordi collettivi nazionali che regolano i  rapporti  con  i  medici    &#13;
 interessati,  si  costituisce  con  versamenti  che  coprono non solo    &#13;
 l'erogazione  della   stessa,   ma   altri   eventi,   quali   quello    &#13;
 dell'invalidità, temporanea e permanente, e del trattamento a favore    &#13;
 dei  superstiti  e può dar luogo ad una pensione in alternativa alla    &#13;
 indennità medesima.                                                     &#13;
   Tutto ciò comprova, in linea generale, che trattasi di un istituto    &#13;
 che ha sue caratteristiche intrinseche e un suo regime giuridico  che    &#13;
 non  consentono  l'accostamento,  sotto  il  profilo tributario, alle    &#13;
 indennità di fine rapporto cui si applica la disciplina dell'art. 17    &#13;
 del d.P.R. n. 917 del 1986. Sussistono, in  definitiva,  elementi  di    &#13;
 differenziazione  tali  da  non  rendere  comparabili,  ai  fini  del    &#13;
 giudizio di costituzionalità, gli istituti  posti  a  raffronto  dal    &#13;
 giudice   a   quo,   mentre   è   da  escludere,  nell'ambito  della    &#13;
 discrezionalità  esercitata  dal  legislatore   nell'apprestare   le    &#13;
 distinte  discipline  di cui trattasi, la denunciata violazione degli    &#13;
 artt. 3 e 53, primo comma, della Costituzione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  18,  primo  comma,  del  d.P.R.  22  dicembre 1986, n. 917    &#13;
 (Approvazione del testo unico delle imposte sui  redditi)  sollevata,    &#13;
 in  riferimento  agli  artt. 3 e 53, primo comma, della Costituzione,    &#13;
 dall'ordinanza in epigrafe.                                              &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 febbraio 1994.                              &#13;
                        Il Presidente: CASAVOLA                           &#13;
                          Il redattore: VARI                              &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 23 febbraio 1994.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
