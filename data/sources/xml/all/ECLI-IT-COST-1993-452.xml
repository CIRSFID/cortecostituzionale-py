<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>452</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:452</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/12/1993</data_decisione>
    <data_deposito>20/12/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. Luigi MENGONI, prof. &#13;
 Enzo CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, avv. Massimo VARI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 63 e 67 della    &#13;
 legge  30  dicembre  1991,  n. 413 (Disposizioni per ampliare le basi    &#13;
 imponibili, per razionalizzare, facilitare e  potenziare  l'attività    &#13;
 di  accertamento;  disposizioni per la rivalutazione obbligatoria dei    &#13;
 beni immobili delle imprese, nonché per riformare il  contenzioso  e    &#13;
 per  la definizione agevolata dei rapporti tributari pendenti; delega    &#13;
 al Presidente della Repubblica per la  concessione  di  amnistia  per    &#13;
 reati  tributari;  istituzioni dei centri di assistenza fiscale e del    &#13;
 conto fiscale) e dell'art. 1, terzo  comma,  del  d.P.R.  20  gennaio    &#13;
 1992,  n.  23 (Concessione di amnistia per reati tributari), promosso    &#13;
 con ordinanza emessa il 23 dicembre 1992 dal Tribunale di Pesaro  nel    &#13;
 procedimento  penale  a  carico di Ricci Giuseppe, iscritta al n. 191    &#13;
 del registro ordinanze 1993 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 19, prima serie speciale, dell'anno 1993;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del 20  ottobre  1993  il  Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto  che  nel  corso  di  un  giudizio penale concernente una    &#13;
 imputazione   di   omesso   versamento   di   ritenute   di   acconto    &#13;
 effettivamente  operate  e  dichiarate  nel  modello  770 da parte di    &#13;
 sostituto d'imposta (art. 2, ultimo comma del decreto-legge 10 luglio    &#13;
 1982, n. 429, convertito in legge 7 agosto 1982, n. 516, nel testo in    &#13;
 vigore alla data del 1987, tempo del fatto), il Tribunale di  Pesaro,    &#13;
 con   ordinanza   del   23  dicembre  1992,  ha  sollevato  questione    &#13;
 incidentale di legittimità costituzionale degli  articoli  63  e  67    &#13;
 della  legge 30 dicembre 1991, n. 413 e dell'art. 1, terzo comma, del    &#13;
 d.P.R. 20 gennaio 1992, n. 23;                                           &#13;
      che il Tribunale premette che, nel caso di specie, l'imputato ha    &#13;
 omesso di versare le ritenute d'acconto effettuate e  da  lui  stesso    &#13;
 dichiarate  nella  dichiarazione  annuale  di sostituto d'imposta del    &#13;
 1987, e che, in base  ad  attestazione  dell'organo  esattoriale,  le    &#13;
 dette  ritenute  non  erano  state  ancora  versate  alla data del 25    &#13;
 febbraio 1992;                                                           &#13;
      che    quest'ultima    circostanza   preclude   l'applicabilità    &#13;
 dell'amnistia concessa con il d.P.R. 20 gennaio 1992, n. 23,  emanato    &#13;
 in  attuazione  della  delega  contenuta  nell'art. 67 della legge 30    &#13;
 dicembre 1991, n. 413, giacché l'art. 1, comma terzo, del richiamato    &#13;
 provvedimento di clemenza stabilisce che, per i  reati  commessi  dai    &#13;
 sostituti  di  imposta,  l'amnistia  si applica, a coloro che abbiano    &#13;
 fatto dichiarazione (nel  modello  770)  delle  ritenute  operate,  a    &#13;
 condizione  che  le ritenute medesime siano state versate entro il 23    &#13;
 gennaio 1992; mentre per i sostituti d'imposta che non abbiano  fatto    &#13;
 dichiarazione   delle  ritenute  nel  modello  770,  l'applicabilità    &#13;
 dell'amnistia è condizionata al fatto che l'importo  delle  ritenute    &#13;
 sia  compreso  in  quello  indicato  nella  dichiarazione integrativa    &#13;
 presentata, a norma dell'art. 63 della legge  n.  413  del  1991,  in    &#13;
 sostituzione di quella omessa o in aumento di quella già presentata,    &#13;
 ed  il  termine  di  presentazione della dichiarazione integrativa è    &#13;
 stabilito al 30 giugno 1992, mentre quello  dei  relativi  versamenti    &#13;
 scade nel luglio 1993;                                                   &#13;
      che  il  Tribunale rimettente sospetta di incostituzionalità la    &#13;
 disciplina sopra riferita, in rapporto al principio  di  eguaglianza:    &#13;
 al  sostituto  d'imposta  che  ha  adempiuto  all'obbligo "primario e    &#13;
 fondamentale"  della  dichiarazione  viene  ad  essere  accordato  un    &#13;
 trattamento  penale deteriore rispetto a chi, nella stessa situazione    &#13;
 debitoria,  abbia  omesso  non  solo  il  versamento  ma   anche   la    &#13;
 dichiarazione;  a  quest'ultimo,  infatti, viene accordato un termine    &#13;
 più ampio per regolarizzare la propria posizione, rispetto al primo;    &#13;
      che i sospetti di illegittimità  costituzionale  si  rafforzano    &#13;
 ulteriormente,  per  il giudice a quo, alla luce di quanto stabilisce    &#13;
 l'art.  3  del  decreto-legge  24  novembre  1992,  n.  455,  che  ha    &#13;
 "riaperto"  i termini del condono tributario consentendo ai sostituti    &#13;
 di imposta di presentare le dichiarazioni integrative di cui all'art.    &#13;
 63 della legge n. 413 del 1991 fino al 31 marzo 1993, poiché  questa    &#13;
 normativa  sembrerebbe  aver  "riaperto"  i  termini  anche  ai  fini    &#13;
 dell'applicazione dell'amnistia, nel  senso  che  gli  effetti  delle    &#13;
 situazioni  ricomprese  nel  condono tributario in tal modo prorogato    &#13;
 rileverebbero anche in sede penale ai fini dell'amnistia  di  cui  al    &#13;
 d.P.R. n. 23 del 1992;                                                   &#13;
      che  è  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, che ha concluso per l'infondatezza della questione;               &#13;
    Considerato  che,  nella  prospettazione  del  giudice rimettente,    &#13;
 l'eliminazione  della  lamentata  disparità   di   trattamento   tra    &#13;
 l'ipotesi  del sostituto di imposta che abbia omesso di versare entro    &#13;
 il termine stabilito le  ritenute  d'acconto  (da  lui  effettuate  e    &#13;
 dichiarate)  e  quella  -  assunta a termine di comparazione - in cui    &#13;
 l'applicazione   del   beneficio   estintivo   è   collegata    alla    &#13;
 presentazione della dichiarazione integrativa, sarebbe possibile solo    &#13;
 accordando  alla  prima  ipotesi il medesimo termine stabilito per la    &#13;
 seconda  quanto  a   verificazione   della   condizione   applicativa    &#13;
 dell'amnistia rispettivamente prevista nei due casi (versamento delle    &#13;
 ritenute; presentazione della dichiarazione integrativa);                &#13;
      che, pertanto, la questione sollevata tende ad ampliare l'ambito    &#13;
 di  applicabilità  dell'amnistia,  il  che  è  precluso  in sede di    &#13;
 giudizio di legittimità costituzionale: un intervento  quale  quello    &#13;
 richiesto  inciderebbe  in un ambito la cui determinazione è rimessa    &#13;
 alla esclusiva competenza del legislatore: (ex plurimis, ord. nn. 340    &#13;
 e  628  del  1987; sent. nn. 59 e 79 del 1980, n. 32 del 1976, n. 154    &#13;
 del 1974) e  comporterebbe  il  non  consentito  effetto  di  mutare,    &#13;
 ampliandolo,  il  termine  di applicabilità del beneficio, stabilito    &#13;
 dalla legge sulla base del precetto contenuto nell'art.  79,  secondo    &#13;
 comma, della Costituzione;                                               &#13;
      che,  d'altra  parte, la concorrente prospettazione - in termini    &#13;
 dubitativi - della possibile ulteriore efficacia in sede penale della    &#13;
 riapertura dei  termini  per  la  presentazione  delle  dichiarazioni    &#13;
 integrative (basata sul decreto-legge n. 455 del 1992, non convertito    &#13;
 in  legge,  ma  ricollegabile  al successivo decreto-legge 23 gennaio    &#13;
 1993, n. 16, convertito in legge 24 marzo 1993,  n.  75)  costituisce    &#13;
 una  questione  di carattere interpretativo, rimessa alla valutazione    &#13;
 del giudice ed estranea al sindacato di costituzionalità delle norme    &#13;
 impugnate;                                                               &#13;
      che sotto  entrambi  i  profili  ora  detti  la  questione  deve    &#13;
 ritenersi manifestamente inammissibile;                                  &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli articoli 63 e  67  della  legge  30    &#13;
 dicembre  1991, n. 413 (Disposizioni per ampliare le base imponibili,    &#13;
 per  razionalizzare,   facilitare   e   potenziare   l'attività   di    &#13;
 accertamento; disposizioni per la rivalutazione obbligatoria dei beni    &#13;
 immobili delle imprese, nonché per riformare il contenzioso e per la    &#13;
 definizione  agevolata  dei  rapporti  tributari  pendenti; delega al    &#13;
 Presidente della Repubblica per la concessione di amnistia per  reati    &#13;
 tributari;  istituzioni  dei centri di assistenza fiscale e del conto    &#13;
 fiscale) e dell'art. 1, terzo comma, del d.P.R. 20 gennaio  1992,  n.    &#13;
 23  (Concessione  di  amnistia  per  reati  tributari), sollevata, in    &#13;
 riferimento all'art. 3 della Costituzione, dal  Tribunale  di  Pesaro    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 dicembre 1993.                             &#13;
                        Il Presidente: CASAVOLA                           &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 20 dicembre 1993.                        &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
