<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>511</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:511</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1991</data_decisione>
    <data_deposito>30/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI; prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 10 della legge    &#13;
 della Regione Abruzzo 3 marzo 1988, n. 25 (Norme in  materia  di  usi    &#13;
 civici  e gestione delle terre civiche) promosso con ordinanza emessa    &#13;
 il 16 aprile 1991 dal  Commissario  regionale  per  il  riordinamento    &#13;
 degli  usi  civici in Abruzzo nel procedimento demaniale vertente tra    &#13;
 il Comune di Avezzano ed il Consorzio per il  nucleo  industriale  di    &#13;
 Avezzano  iscritta al n. 488 del registro ordinanze 1991 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  28,  prima   serie    &#13;
 speciale, dell'anno 1991;                                                &#13;
    Udito  nella  camera  di  consiglio del 4 dicembre 1991 il Giudice    &#13;
 relatore Luigi Mengoni;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Nel corso di un giudizio avente  per  oggetto  l'accertamento    &#13;
 della natura di demanio civico o allodiale di alcuni terreni alienati    &#13;
 dal  Comune  di  Avezzano  al  Consorzio per il nucleo industriale di    &#13;
 quella città, avendo il rappresentante  del  Comune  depositato  una    &#13;
 deliberazione  della  giunta comunale in data 11 aprile 1991, con cui    &#13;
 si chiede la sclassificazione dei terreni medesimi ai sensi dell'art.    &#13;
 10, secondo comma, della legge della Regione Abruzzo 3 marzo 1988, n.    &#13;
 25  (modificata  dalla  legge  reg.  8  settembre  1988,  n.  77), il    &#13;
 Commissario regionale  per  il  riordinamento  degli  usi  civici  in    &#13;
 Abruzzo,  reputando  che  la  definizione  del giudizio dipenda dalla    &#13;
 delibera che il Consiglio regionale adotterà in merito alla domanda,    &#13;
 ha  sollevato,  con  ordinanza  del  16  aprile  1991,  questione  di    &#13;
 legittimità costituzionale del citato art. 10, secondo comma.           &#13;
    2. - Ad avviso del giudice a quo, la norma denunciata viola l'art.    &#13;
 117  Cost.,  in  quanto  "vulnera i principi fondamentali posti dalla    &#13;
 legge nazionale 16 giugno 1927, n. 1766, dell'imprescrittibilità  dei    &#13;
 diritti    di    uso    civico,   nonché   dell'inusucapibilità    e    &#13;
 dell'indisponibilità delle terre collettive .. sottoposte al vincolo    &#13;
 dell'immutabilità della loro destinazione"; viola l'art. 118 perché    &#13;
 attribuisce alla Regione "poteri che non sono  certamente  di  natura    &#13;
 amministrativa,  ma  legislativa";  viola  infine  l'art.  42  Cost.,    &#13;
 perché  "i   diritti   proprietari   della   collettività   vengono    &#13;
 praticamente  espropriati  senza  che  alla  medesima sia corrisposto    &#13;
 alcun compenso a titolo di indennizzo".<diritto>Considerato in diritto</diritto>1. - L'art. 10 della legge della Regione Abruzzo 3 marzo 1988,  n.    &#13;
 25,  modificato  dalla legge reg. 8 settembre 1988, n. 77, dispone al    &#13;
 secondo comma:  "Nei  casi  in  cui,  per  effetto  di  utilizzazioni    &#13;
 improprie  ormai  consolidate,  porzioni  di terre civiche abbiano da    &#13;
 tempo  irreversibilmente  perduto  la  conformazione  fisica   e   la    &#13;
 destinazione   funzionale   di  terreni  agrari,  ovvero  boschivi  e    &#13;
 pascolivi, il Consiglio regionale, su richiesta motivata  del  Comune    &#13;
 territorialmente  interessato,  ovvero  dell'Amministrazione separata    &#13;
 frazionale, sentito il Comune, se  trattasi  di  beni  di  pertinenza    &#13;
 frazionale,  può disporre la sclassificazione di dette terre dal regime demaniale civico". La disposizione è impugnata dal  Commissario    &#13;
 regionale   per   il  riordinamento  degli  usi  civici  per  preteso    &#13;
 contrasto:                                                               &#13;
       a)  con  l'art.  117  Cost.,  perché   "vulnera   i   principi    &#13;
 fondamentali  posti  dalla  legge  nazionale 16 giugno 1927, n. 1766,    &#13;
 dell'imprescrittibilità  dei  diritti   di   uso   civico,   nonché    &#13;
 dell'inusucapibilità    e    dell'indisponibilità    delle    terre    &#13;
 collettive",  soggette  a  "vincolo  di  immutabilità   della   loro    &#13;
 destinazione";                                                           &#13;
       b)  con  l'art. 118 Cost., perché non rispetta "i limiti della    &#13;
 delega stabilita dall'art. 66 del d.P.R. 24  luglio  1977,  n.  616",    &#13;
 attribuendo  alla  Regione  "poteri che non sono certamente di natura    &#13;
 amministrativa, ma legislativa";                                         &#13;
       c) con l'art. 42, terzo comma, Cost., perché con  la  prevista    &#13;
 "sclassificazione"  di  terre  civiche  i diritti di proprietà della    &#13;
 collettività  "vengono  praticamente  espropriati  senza  che   alla    &#13;
 medesima sia corrisposto alcun compenso a titolo di indennizzo".         &#13;
    2. La questione non è fondata.                                       &#13;
    Per  valutare  correttamente  se  la  norma denunciata si mantenga    &#13;
 nella cornice dei principi fondamentali risultanti  dalla  legge  del    &#13;
 1927  sugli  usi  civici,  occorre  considerare che le diverse e più    &#13;
 remunerative possibilità di occupazione, prodotte  dal  sopravvenuto    &#13;
 sviluppo  industriale  del  Paese  anche  nelle zone tradizionalmente    &#13;
 agricole,  hanno  ridotto  a  dimensioni  modestissime  le   economie    &#13;
 familiari  di  produzione per il consumo, determinando un progressivo    &#13;
 abbandono  dell'esercizio  degli  usi  civici  collegati   a   quelle    &#13;
 economie.  Tale  fenomeno  ha  comportato  che terreni gravati da usi    &#13;
 civici, di cui si è quasi perduto il ricordo,  sono  stati  alienati    &#13;
 dai   Comuni  trascurando  le  condizioni  e  le  procedure  previste    &#13;
 dall'art.  12  della  legge  del  1927,  per  finalità  di  pubblico    &#13;
 interesse   connesse  ai  bisogni  di  urbanizzazione  (dal  1927  la    &#13;
 popolazione  italiana  è  pressoché  raddoppiata)  o   ai   bisogni    &#13;
 dell'industrializzazione, apportatrice di nuovi posti di lavoro.         &#13;
    La regolarizzazione di siffatte situazioni alla stregua del citato    &#13;
 art.   12,   come   vorrebbe  il  giudice  a  quo,  è  difficilmente    &#13;
 praticabile, sia perché presuppone l'assegnazione dei terreni a  una    &#13;
 o  l'altra  delle  categorie distinte dall'art. 11, mentre essi hanno    &#13;
 ormai perduto da tempo l'originaria destinazione agricola o boschivo-pastorale, sia perché impone l'onere di  rinnovazione  dell'atto  di    &#13;
 vendita  con  un  nuovo  prezzo  calcolato tenendo conto dell'attuale    &#13;
 destinazione urbanistica o industriale dei terreni. Oltre a tutto, il    &#13;
 Comune sarebbe esposto al rischio di vedersi citato in  giudizio,  ai    &#13;
 sensi  dell'art. 1338 cod.  civ., con una domanda di risarcimento dei    &#13;
 danni sofferti dall'acquirente per avere confidato, senza sua  colpa,    &#13;
 nella   validità  del  precedente  contratto.  Sulla  base  di  quel    &#13;
 contratto e del prezzo allora convenuto è stata fatta, nel  caso  in    &#13;
 esame,  l'analisi dei costi-benefici dell'insediamento industriale in    &#13;
 vista del quale i terreni di cui è causa  sono  stati  alienati  dal    &#13;
 Comune di Avezzano.                                                      &#13;
    3.  -  Occorre  perciò,  pur  nel  quadro  della legge nazionale,    &#13;
 trovare spazi a leggi regionali di sanatoria. La  soluzione  adottata    &#13;
 dall'art.  10  della legge abruzzese utilizza a tale scopo il modello    &#13;
 della "sclassificazione" dei beni demaniali  (art.  829  cod.  civ.),    &#13;
 fondandosi  sul  fatto che le terre civiche ivi considerate "hanno da    &#13;
 tempo  perduto  irreversibilmente  la  conformazione  fisica   e   la    &#13;
 destinazione   funzionale   di   terreni  agrari  ovvero  boschivi  o    &#13;
 pascolivi". Non si tratta di una "sdemanializzazione"  esonerata  dal    &#13;
 presupposto  della  previa  assegnazione  dei terreni a categoria. La    &#13;
 sclassificazione è un atto di  natura  meramente  dichiarativa,  che    &#13;
 accerta  la perdita delle caratteristiche che qualificavano i terreni    &#13;
 come beni di demanio collettivo, con conseguente esclusione di questa    &#13;
 specifica ragione  di  nullità  della  vendita  stipulata  senza  le    &#13;
 condizioni dell'art. 12 della legge del 1927, e quindi, se la vendita    &#13;
 fosse  già  avvenuta, restando esclusa la necessità di rinnovazione    &#13;
 del contratto.                                                           &#13;
    La norma denunciata non viola il  limite  indicato  dall'art.  117    &#13;
 Cost.,  ma  anzi  risponde a un principio generale della legislazione    &#13;
 statale, desumibile dagli artt. 39 e 41 del r.d. 26 febbraio 1928, n.    &#13;
 332,  nel  senso  che  sono   consentite   in   ogni   caso   -   con    &#13;
 l'autorizzazione del Ministro dell'agricoltura (sentito il parere del    &#13;
 Commissario  regionale  per gli usi civici), e ora della Regione (non    &#13;
 soggetta al requisito del detto parere preventivo) - l'alienazione  o    &#13;
 la  concessione,  previo  mutamento di destinazione, di terre civiche    &#13;
 quando le forme di utilizzazione previste dalla  legge  n.  1766  del    &#13;
 1927  non  siano più possibili o risultino antieconomiche, mentre la    &#13;
 diversa destinazione sopravvenuta rappresenta un reale beneficio  per    &#13;
 la generalità degli abitanti.                                           &#13;
    Questo  principio  si riflette nell'ultimo comma dell'art. 6 della    &#13;
 legge regionale (non impugnato):  di  esso  il  successivo  art.  10,    &#13;
 secondo  comma, costituisce un adattamento ordinato alla sanatoria di    &#13;
 mutamenti di destinazione già intervenuti, dei  quali  il  Consiglio    &#13;
 regionale riconosce la rispondenza a finalità di interesse pubblico,    &#13;
 in pari tempo dichiarando che sono cessate definitivamente le ragioni    &#13;
 che   giustificavano   l'originario   vincolo  di  destinazione,  con    &#13;
 conseguente passaggio dei  terreni  nel  patrimonio  disponibile  del    &#13;
 Comune.                                                                  &#13;
    4.  -  La  seconda  censura,  indicata  al  punto  1,  sub  b), è    &#13;
 contraddittoria con la precedente. Il motivo di impugnazione  sub  a)    &#13;
 presuppone   il   riconoscimento   alla  Regione  di  una  competenza    &#13;
 legislativa concorrente in materia di usi civici.  Al  contrario,  il    &#13;
 motivo  sub  b) aderisce a una dottrina minoritaria, non condivisa da    &#13;
 questa Corte (cfr. sentenza n. 511 del 1988), la  quale  contesta  la    &#13;
 valutazione  degli  usi  civici  come  submateria  dell'agricoltura e    &#13;
 foreste,  sottesa  all'art.  66  del  d.P.R.  n.  616  del  1977.  Il    &#13;
 trasferimento  delle  funzioni  amministrative  in questa materia, in    &#13;
 quanto estranea all'elenco dell'art. 117 Cost.,  dovrebbe  intendersi    &#13;
 in  realtà  come  delega  ai  sensi  dell'art.  118,  secondo comma,    &#13;
 assistita dal limitato potere  normativo  previsto  dall'art.  7  del    &#13;
 citato  decreto,  che  la  statuizione  della  norma in esame avrebbe    &#13;
 ecceduto.                                                                &#13;
    Caduta   la   premessa,   perde   consistenza    il    riferimento    &#13;
 dell'impugnazione all'art. 118 Cost.                                     &#13;
    5.  -  Non  appare  violato, infine, l'art. 42, terzo comma, Cost.    &#13;
 L'atto di sclassificazione  non  è  assimilabile  all'espropriazione    &#13;
 forzata,  essendo  nella specie ordinato alla regolarizzazione di una    &#13;
 vendita, già avvenuta, finalizzata a un insediamento industriale che    &#13;
 rappresenta un reale beneficio per la  collettività.  Nella  diversa    &#13;
 ipotesi,  in  cui  il  mutamento  di  destinazione  dei terreni fosse    &#13;
 intervenuto indipendentemente da una alienazione da parte del Comune,    &#13;
 il prezzo ricavato dalla  vendita  successiva  alla  sclassificazione    &#13;
 dovrà  essere  destinato  alla  realizzazione  di opere pubbliche di    &#13;
 interesse della collettività, secondo la prescrizione  dell'art.  6,    &#13;
 sesto  comma,  della legge regionale. Questa norma, da sottintendersi    &#13;
 anche nell'art. 10, secondo  comma,  corrisponde  all'art.  24  della    &#13;
 legge del 1927, escluso l'obbligo, che certo non può considerarsi un    &#13;
 principio  vincolante per il legislatore regionale, dell'investimento    &#13;
 del prezzo in titoli del debito pubblico intestati al Comune.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.  10,  secondo  comma,  della  legge della Regione Abruzzo 3    &#13;
 marzo 1988, n. 25 (Norme in materia di usi civici  e  gestione  delle    &#13;
 terre  civiche),  sollevata,  in riferimento agli artt. 117, 118 e 42    &#13;
 della Costituzione, dal Commissario regionale  per  il  riordinamento    &#13;
 degli usi civici con l'ordinanza indicata in epigrafe.                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 dicembre 1991.                             &#13;
                       Il presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 30 dicembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
