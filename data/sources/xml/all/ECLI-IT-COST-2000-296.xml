<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>296</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:296</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/07/2000</data_decisione>
    <data_deposito>17/07/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo &#13;
 MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità  costituzionale dell'art. 34, commi 1,    &#13;
 lettera  c),  3,  4,  5,  6, 7 e 8, del decreto legislativo 12 maggio    &#13;
 1995,  n. 196  (Attuazione  dell'art. 3  della  legge  6  marzo 1992,    &#13;
 n. 216,  in  materia  di  riordino  dei ruoli, modifica alle norme di    &#13;
 reclutamento,  stato ed avanzamento del personale non direttivo delle    &#13;
 Forze  armate), promossi con undici ordinanze emesse il 9 luglio 1999    &#13;
 dal  Tribunale  amministrativo  regionale  della  Lombardia,  sezione    &#13;
 distaccata di Brescia, rispettivamente iscritte ai nn. 497, 498, 499,    &#13;
 554, 555, 556, 557, 558, 666, 667 e 668 del registro ordinanze 1999 e    &#13;
 pubblicate nella Gazzetta Ufficiale della Repubblica nn. 39, 42 e 50,    &#13;
 prima serie speciale, dell'anno 1999.                                    &#13;
     Visti  gli  atti  di  intervento del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di  consiglio  del 7 giugno 2000 il giudice    &#13;
 relatore Francesco Guizzi.                                               &#13;
     Ritenuto  che  un  gruppo  di  sottufficiali  dell'Esercito,  con    &#13;
 distinti   ricorsi   (tutti,  però,  di  contenuto  identico  e  col    &#13;
 patrocinio  del medesimo avvocato), ha impugnato dinanzi al Tribunale    &#13;
 amministrativo  regionale della Lombardia i vari decreti ministeriali    &#13;
 con  i  quali ciascuno di essi era stato inquadrato nel nuovo grado e    &#13;
 nel  nuovo ruolo, ai sensi della norma transitoria di cui all'art. 34    &#13;
 del   decreto   legislativo   12   maggio  1995,  n. 196  (Attuazione    &#13;
 dell'art. 3  della legge 6 marzo 1992, n. 216, in materia di riordino    &#13;
 dei  ruoli, modifica alle norme di reclutamento, stato ed avanzamento    &#13;
 del personale non direttivo delle Forze armate);                         &#13;
         che   i  ricorrenti  hanno  eccepito  la  illegittimità  del    &#13;
 provvedimento  impugnato,  poiché  l'art. 34,  citato, lederebbe gli    &#13;
 artt. 3, 36 e 97 della Costituzione;                                     &#13;
         che,  a  loro  avviso, il decreto legislativo di riordino dei    &#13;
 ruoli   delle  Forze  armate  avrebbe  disposto,  per  i  marescialli    &#13;
 dell'Esercito,  un trattamento deteriore rispetto a quello per i pari    &#13;
 grado  dell'Arma  dei carabinieri previsto dal decreto legislativo 12    &#13;
 maggio 1995, n. 198 (Attuazione dell'art. 3 della legge 6 marzo 1992,    &#13;
 n. 216,  in  materia di riordino dei ruoli, e modifica delle norme di    &#13;
 reclutamento,  stato ed avanzamento del personale non direttivo e non    &#13;
 dirigente dell'Arma dei carabinieri), e ciò in distonia con la ratio    &#13;
 della  legge  delega  volta  a  omogeneizzare  le  attribuzioni  e  i    &#13;
 trattamenti  economici degli appartenenti alle varie Forze armate e a    &#13;
 quelle di polizia;                                                       &#13;
         che  il giudice adi'to con undici ordinanze ha sollevato, con    &#13;
 riferimento  agli  artt. 3,  36 e 97 della Costituzione, questione di    &#13;
 legittimità  costituzionale  dell'art. 34, commi 1, lettera c) 3, 4,    &#13;
 5, 6, 7 e 8 del decreto legislativo n. 196 del 1995;                     &#13;
         che,  secondo  il  Collegio rimettente, il rapporto d'impiego    &#13;
 dei  sottufficiali  delle  Forze  armate  è sempre stato equiparato,    &#13;
 tendenzialmente,  a  quello  dei  sottufficiali dei carabinieri, come    &#13;
 dimostra  l'art. 2 della legge 6 marzo 1992, n. 216, che demandava al    &#13;
 Governo  di  realizzare  una  "disciplina omogenea" nel trattamento e    &#13;
 nelle  attribuzioni  dei sottufficiali delle forze di polizia, "anche    &#13;
 ad  ordinamento  militare" (con esplicito richiamo ai carabinieri), e    &#13;
 di quelli delle altre Forze armate;                                      &#13;
         che  il  decreto  legislativo  n. 196  del  1995,  emanato in    &#13;
 attuazione  di  tale  delega,  avrebbe  equiparato  a vari effetti la    &#13;
 condizione  dei  sottufficiali dell'Arma dei carabinieri a quella dei    &#13;
 sottufficiali delle altre Forze armate;                                  &#13;
         che,  tuttavia, l'equiparazione tra sottufficiali delle Forze    &#13;
 armate  e  dell'Arma  dei carabinieri non sarebbe stata garantita per    &#13;
 quanto concerne il regime transitorio del passaggio ai nuovi ruoli;      &#13;
     che,  infatti,  l'art. 34 del decreto legislativo n. 196 del 1995    &#13;
 ha previsto un reinquadramento dei marescialli e dei sergenti già in    &#13;
 servizio,   il  quale  risulterebbe,  nel  suo  complesso,  deteriore    &#13;
 rispetto a quello disposto per i sottufficiali dei carabinieri;          &#13;
         che,  mentre  i marescialli ordinari dell'Esercito sono stati    &#13;
 inquadrati  nel  grado  di  maresciallo  ordinario, i loro pari grado    &#13;
 dell'Arma  dei  carabinieri  lo  sono  stati nel grado di maresciallo    &#13;
 capo; e ancora, mentre i sergenti maggiori dell'Esercito, iscritti ai    &#13;
 quadri di avanzamento ordinari e straordinari relativi agli anni 1994    &#13;
 e   1995  ma  non  promossi,  sono  stati  inquadrati  nel  grado  di    &#13;
 maresciallo  ordinario,  i  loro  colleghi  di  grado  corrispondente    &#13;
 dell'Arma  dei  carabinieri  lo  sono  stati nel grado di maresciallo    &#13;
 capo;                                                                    &#13;
         che  l'asserita  disparità di trattamento lederebbe l'art. 3    &#13;
 della  Costituzione,  sotto il profilo della razionalità, poiché il    &#13;
 legislatore  avrebbe  previsto, per i sottufficiali dei carabinieri e    &#13;
 per  quelli delle altre Forze armate, una perfetta corrispondenza nei    &#13;
 gradi,  nel  livello  retributivo  e nel percorso di carriera, mentre    &#13;
 avrebbe   disciplinato   in  modo  diverso  soltanto  l'inquadramento    &#13;
 transitorio;                                                             &#13;
         che  la  norma  censurata  sarebbe  altresì in contrasto con    &#13;
 l'art. 97  della  Costituzione per il iato che il legislatore avrebbe    &#13;
 creato  fra  la  ratio  della  norma (rendere omogeneo il rapporto di    &#13;
 servizio  dei  carabinieri  e  delle  altre  Forze armate) e il mezzo    &#13;
 prescelto   per   conseguirla,  avendo  le  norme  sull'inquadramento    &#13;
 transitorio previsto un "massiccio meccanismo di promozioni" soltanto    &#13;
 in favore dei sottufficiali dei carabinieri;                             &#13;
         che,  infine,  la  norma  in  esame,  nel generare differenze    &#13;
 retributive   fra   soggetti   appartenenti  "allo  stesso  livello",    &#13;
 lederebbe  l'art. 36 della Costituzione, con violazione del principio    &#13;
 di proporzionalità e adeguatezza della retribuzione.                    &#13;
     Considerato  che  per l'identità della materia i giudizi debbono    &#13;
 essere riuniti e decisi con unica pronuncia;                             &#13;
         che  le  modifiche  all'assetto organizzatorio della pubblica    &#13;
 amministrazione,  ivi  comprese  quelle  dettate  in via transitoria,    &#13;
 rientrano  nella  sfera di discrezionalità riservata al legislatore,    &#13;
 come  questa  Corte  ha più volte ritenuto con specifico riferimento    &#13;
 all'organizzazione  e  all'inquadramento  del  personale  delle Forze    &#13;
 armate  e  di  polizia (ordinanza n. 189 del 1999, sentenza n. 63 del    &#13;
 1998 e ordinanza n. 324 del 1993);                                       &#13;
         che  la  discrezionalità  legislativa  incontra  soltanto  i    &#13;
 limiti dell'arbitrarietà o della manifesta irragionevolezza, i quali    &#13;
 non sono stati superati nel caso di specie;                              &#13;
         che, infatti, il giudice a quo mira a una pronuncia additiva,    &#13;
 con  l'estensione  ai sottufficiali delle Forze armate del meccanismo    &#13;
 di  inquadramento  transitorio  previsto, per l'Arma dei carabinieri,    &#13;
 dagli artt. 46 e 49 del decreto legislativo n. 198 del 1995: ciò sul    &#13;
 presupposto   che  l'inquadramento,  la  carriera  e  il  trattamento    &#13;
 economico  dei  sottufficiali dei carabinieri e quella dei pari grado    &#13;
 appartenenti  alle  altre Forze armate debbano, ai sensi dell'art. 3,    &#13;
 comma   1,  della  legge  n. 216  del  1992,  essere  necessariamente    &#13;
 omogenei,  eccezion  fatta  per  la  speciale indennità spettante ai    &#13;
 carabinieri  per  lo  svolgimento  delle  funzioni  di  polizia  loro    &#13;
 assegnate;                                                               &#13;
         che  in  realtà  né  la legge n. 216 del 1992, né le norme    &#13;
 successive,   hanno   inteso   perseguire  un'assoluta  identità  di    &#13;
 posizioni  e  trattamenti, e che anzi si deve ritenere esattamente il    &#13;
 contrario,  anche  alla luce della legge 31 marzo 2000, n. 78 (Delega    &#13;
 al  Governo  in  materia  di  riordino dell'Arma dei carabinieri, del    &#13;
 Corpo  forestale  dello  Stato,  del Corpo della Guardia di finanza e    &#13;
 della Polizia di Stato. Norme in materia di coordinamento delle Forze    &#13;
 di polizia), la quale, all'art. 1, ha delegato il Governo a prevedere    &#13;
 la  "collocazione  autonoma"  dell'Arma dei carabinieri, con rango di    &#13;
 Forza armata;                                                            &#13;
         che,  inoltre,  le  funzioni  svolte e i compiti demandati ai    &#13;
 sottufficiali  dei carabinieri (di cui agli artt. 12 e 13 del decreto    &#13;
 legislativo  n. 198  del  1995)  differiscono sensibilmente da quelli    &#13;
 previsti  dagli artt. 5 e 6 del decreto legislativo n. 196 del 1995 e    &#13;
 affidati ai sottufficiali delle altre Forze armate;                      &#13;
         che  tali  diversità  rendono  le  rispettive  posizioni non    &#13;
 comparabili,  sì che la scelta compiuta dal legislatore con la norma    &#13;
 censurata non può dirsi manifestamente irragionevole né palesemente    &#13;
 arbitraria (cfr. ordinanza n. 324 del 1993);                             &#13;
         che  non  sussiste  neppure  la violazione dell'art. 36 della    &#13;
 Costituzione,  giacché  il  legislatore,  così  come  gode di ampia    &#13;
 discrezionalità,   pur   con   i  limiti  indicati,  nel  modificare    &#13;
 l'organizzazione amministrativa e nell'adottare le conseguenti misure    &#13;
 di   perequazione   economica,   nel   contempo   gode  della  stessa    &#13;
 discrezionalità   nel  differenziare  il  trattamento  economico  di    &#13;
 categorie  in  precedenza egualmente retribuite; e che, in ogni caso,    &#13;
 lo  "scorrimento"  verso  l'alto  di  una  categoria  retributiva non    &#13;
 comporta  la  necessità  di  innalzare  anche  i livelli superiori o    &#13;
 inferiori (cfr. ordinanza n. 189 del 1999);                              &#13;
         che,   infine,   il   parametro   di  cui  all'art. 97  della    &#13;
 Costituzione  non  è  violato,  poiché  le  variazioni dell'assetto    &#13;
 organizzatorio   della   pubblica   amministrazione  rientrano  nella    &#13;
 discrezionalità  del legislatore e non ledono, di per sé, il canone    &#13;
 del buon andamento (sentenza n. 63 del 1998);                            &#13;
         che  pertanto  la questione, come sollevata, deve dichiararsi    &#13;
 manifestamente infondata.                                                &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti i giudizi;                                                   &#13;
     Dichiara   la   manifesta   infondatezza   della   questione   di    &#13;
 legittimità  costituzionale dell'art. 34, commi 1, lettera c), 3, 4,    &#13;
 5,  6,  7  e  8,  del  decreto legislativo del 12 maggio 1995, n. 196    &#13;
 (Attuazione  dell'art. 3 della legge 6 marzo 1992, n. 216, in materia    &#13;
 di  riordino dei ruoli, modifica alle norme di reclutamento, stato ed    &#13;
 avanzamento   del   personale  non  direttivo  delle  Forze  armate),    &#13;
 sollevata,  con riferimento agli artt. 3, 36 e 97 della Costituzione,    &#13;
 dal  Tribunale  amministrativo  regionale  della  Lombardia,  sezione    &#13;
 distaccata di Brescia, con le ordinanze in epigrafe.                     &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta l'11 luglio 2000.                                 &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Guizzi                             &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 17 luglio 2000.                         &#13;
               Il direttore della cancelleria: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
