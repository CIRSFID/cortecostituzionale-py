<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>518</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:518</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>20/11/2002</data_decisione>
    <data_deposito>04/12/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Cesare RUPERTO; Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 500, comma 4, 513 e 210, comma 5, del codice di procedura penale, promosso con ordinanza del 18 febbraio 2002 dal Tribunale di Novara nel procedimento penale a carico di A.M. ed altri, iscritta al n. 287 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 26, prima serie speciale, dell'anno 2002. &#13;
    Udito nella camera di consiglio del 23 ottobre 2002 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che con ordinanza emessa il 18 febbraio 2002 nel corso di un procedimento penale relativo a fatti di corruzione, il Tribunale di Novara ha sollevato, in riferimento agli artt. 3, primo comma, e 111, quinto comma, della Costituzione, questione di legittimità costituzionale degli artt. 500, comma 4, 513 e 210, comma 5, del codice di procedura penale, «nella parte in cui non prevedono l'acquisizione e l'utilizzabilità dei verbali delle dichiarazioni utilizzate per le contestazioni nei casi in cui risulti provato che il testimone ha reso in dibattimento dichiarazioni false o reticenti»; &#13;
    che il giudice a quo premette, in punto di fatto, che una persona già imputata in procedimento connesso — sentita in dibattimento nella veste di «testimone assistito» ai sensi dell'art. 197-bis cod. proc. pen., avendo definito la propria posizione con sentenza irrevocabile di applicazione della pena ex art. 444 cod. proc. pen. —  era risultata palesemente reticente, tanto da indurre il Tribunale a trasmettere gli atti al pubblico ministero affinché procedesse nei suoi confronti per il reato di falsa testimonianza, di cui all'art. 372 cod. pen.;  &#13;
    che la configurabilità di tale ipotesi criminosa — ulteriormente avvalorata da successive risultanze dibattimentali — non legittimerebbe, peraltro, l'acquisizione al fascicolo del dibattimento e l'utilizzazione ai fini della decisione dei verbali delle dichiarazioni rese dal testimone nel corso delle indagini preliminari ed utilizzate per le contestazioni; &#13;
    che l'acquisizione e l'utilizzazione anzidette sono consentite, infatti, dall'art. 500, comma 4, cod. proc. pen. solo quando risulti che il testimone è stato sottoposto a violenza, minaccia, offerta o promessa di denaro o di altra utilità affinché non deponga o deponga il falso, e non anche nel caso in cui la deposizione falsa o reticente sia frutto di libera scelta del testimone medesimo; &#13;
    che per tale aspetto, tuttavia, l'art. 500, comma 4, cod. proc. pen. — richiamato dagli artt. 513 e 210, comma 5, del medesimo codice — si porrebbe in contrasto con l'art. 111, quinto comma, Cost., che demanda alla legge ordinaria di regolare i casi in cui la formazione della prova non ha luogo in contraddittorio «per effetto di provata condotta illecita»; &#13;
    che il riferimento alla «condotta illecita», contenuto nella norma costituzionale, non potrebbe ritenersi infatti limitato ai soli comportamenti dei terzi che influiscono sulla libera scelta del testimone, ma risulterebbe al contrario comprensivo anche del comportamento autonomamente tenuto da quest'ultimo in dibattimento, posto che la condotta illecita verrebbe in considerazione quale causa, non già della testimonianza falsa o reticente, quanto piuttosto della deroga alla formazione della prova in contraddittorio; &#13;
    che, inoltre, il diverso trattamento dell'ipotesi in cui la falsa testimonianza consegua a violenza o minaccia ovvero a subornazione del teste, rispetto a quella in cui essa sia dovuta ad una scelta del teste non determinata da tali condotte, risulterebbe del tutto irragionevole, in quanto l'«inquinamento probatorio» si realizzerebbe comunque, a prescindere dai motivi che hanno indotto il testimone a tacere o a mentire. &#13;
      Considerato che questa Corte, scrutinando analoga questione, ha già avuto modo di escludere che l'art. 500, comma 4, cod. proc. pen. contrasti con i parametri costituzionali evocati dall'odierno giudice rimettente, nella parte in cui consente di utilizzare in modo pieno le dichiarazioni precedentemente rese dal testimone soltanto nei casi di subornazione ovvero di violenza o minaccia esercitate sul testimone stesso, e non anche quando la sua deposizione appaia integrativa del reato di falsa testimonianza (cfr. ordinanza n. 453 del 2002); &#13;
    che, al riguardo, questa Corte ha in particolare chiarito come — contrariamente a quanto sostenuto dal rimettente — l'art. 111, quinto comma, Cost., nel prefigurare una deroga al principio della formazione della prova in contraddittorio «per effetto di provata condotta illecita», abbia inteso riferirsi alle sole «condotte illecite» poste in essere «sul» dichiarante (quali la violenza, la minaccia o la subornazione), e non anche a quelle realizzate «dal» dichiarante stesso in occasione dell'esame in contraddittorio (quale, in primis, la falsa testimonianza, anche nella forma della reticenza): e ciò alla luce sia della ratio del precetto costituzionale,  che del suo necessario coordinamento con la previsione del secondo periodo del quarto comma del medesimo art. 111, che immediatamente lo precede; &#13;
      che questa Corte ha rilevato, altresì, come l'eterogeneità delle situazioni poste a confronto — intimidazione o subornazione che coarta od orienta ab externo l'atteggiamento dibattimentale del testimone, da un lato; libera scelta del teste di rendere dichiarazioni non veritiere o di tacere in dibattimento, dall'altro — renda palese l'insussistenza della dedotta violazione dell'art. 3 Cost.; &#13;
      che il giudice a quo non prospetta argomenti ulteriori e diversi rispetto a quelli già esaminati; &#13;
      che la questione deve essere dichiarata pertanto manifestamente infondata. &#13;
      Visti gli artt. 26, comma 2, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
      dichiara la manifesta infondatezza della questione di legittimità costituzionale degli artt. 500, comma 4, 513 e 210, comma 5, del codice di procedura penale, sollevate, in riferimento agli artt. 3, primo comma, e 111, quinto comma, della Costituzione, dal Tribunale di Novara con l'ordinanza in epigrafe. &#13;
      Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 novembre 2002. &#13;
F.to: &#13;
Cesare RUPERTO, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria il 4 dicembre 2002. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
