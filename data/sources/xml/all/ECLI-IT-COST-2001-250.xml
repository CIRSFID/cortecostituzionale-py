<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>250</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:250</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Franco Bile</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/07/2001</data_decisione>
    <data_deposito>12/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  ammissibilità  del  conflitto di attribuzione tra &#13;
poteri  dello  Stato  sorto a seguito della delibera della Camera dei &#13;
deputati  del  23 maggio  2000,  relativa  all'insindacabilità delle &#13;
opinioni  espresse  dall'on. Pietro  Armani  nei  confronti del prof. &#13;
Romano  Prodi,  promosso dal tribunale di Roma, prima sezione civile, &#13;
con  ricorso  depositato il 18 gennaio 2001 ed iscritto al n. 176 del &#13;
registro ammissibilità conflitti. &#13;
    Udito  nella  camera  di  consiglio  del 4 aprile 2001 il giudice &#13;
relatore Franco Bile. &#13;
    Ritenuto  che  con  ricorso del 27 dicembre 2000, il tribunale di &#13;
Roma   (in   composizione  monocratica)  ha  sollevato  conflitto  di &#13;
attribuzione  tra  poteri  dello Stato nei confronti della Camera dei &#13;
deputati,  in  relazione alla delibera - adottata nella seduta del 23 &#13;
maggio 2000 su conforme proposta della Giunta per le autorizzazioni a &#13;
procedere  - con la quale la Camera ha dichiarato che i fatti oggetto &#13;
del  giudizio  civile di risarcimento del danno, instaurato dal prof. &#13;
Romano  Prodi  nei  confronti, tra gli altri, del deputato on. Pietro &#13;
Armani,  concernevanoopinioni da quest'ultimo espresse nell'esercizio &#13;
delle   sue   funzioni,   ai   sensi   dell'art. 68,   primo   comma, &#13;
della Costituzione; &#13;
        che,   in  ordine  allo  svolgimento  della  vicenda  che  ha &#13;
provocato  l'adozione  dell'atto impugnato, il ricorso riferisce che, &#13;
con atto notificato il 4-6 marzo 2000, il prof. Prodi aveva convenuto &#13;
in  giudizio  avanti al tribunale l'on. Armani, la società Europa di &#13;
Edizioni  S.p.a,  e  il dott. Mario Cervi, direttore responsabile del &#13;
quotidiano  "Il Giornale", per sentirli condannare in solido - previo &#13;
accertamento  della  commissione  del  reato  di  diffamazione  -  al &#13;
risarcimento  dei  danni  derivati  dalla  pubblicazione sul medesimo &#13;
quotidiano,  in  data  30 novembre 1999, di un'intervista al suddetto &#13;
deputato,  avente  ad  oggetto  presunti  retroscena  sul  cosiddetto &#13;
"affare  SME",  nell'asserito  presupposto  che nell'intervista erano &#13;
gravemente diffamatorie le affermazioni secondo cui ilprof. Prodi: &#13;
          a) aveva voluto vendere la SME all'ing. De Benedetti; &#13;
          b) aveva accettato un prezzo curiosamente basso; &#13;
          c)  aveva  trattato  segretamente con De Benedetti, fin dal &#13;
marzo 1985; &#13;
          d)  era stato molto vago nei particolari forniti in data 24 &#13;
aprile  1985 al Consiglio dell'IRI e aveva taciuto la circostanza che &#13;
acquirente era De Benedetti; &#13;
          e)   aveva   informato  i  consiglieri  IRI  solo  per  via &#13;
telefonica; &#13;
          f)  aveva  convocato  una  conferenza  stampa  per porre il &#13;
Consiglio di amministrazione davanti al fatto compiuto; &#13;
          g)  aveva  di  fatto  coartato  il  medesimo  Consiglio  ad &#13;
approvare la vendita; &#13;
        che   il   ricorrente   riferisce   che   la  Giunta  per  le &#13;
autorizzazioni   a   procedere,   nel  proporre  una  valutazione  di &#13;
insindacabilità delle suddette dichiarazioni, aveva dato rilievo: &#13;
          1)  ad una precedente intervista rilasciata dall'on. Armani &#13;
a  "Il  Corriere  della  Sera" il 7 dicembre 1995, per manifestare le &#13;
medesime  opinioni, cui non era seguita alcuna iniziativa giudiziaria &#13;
del prof. Prodi; &#13;
          2)  a  precedenti  dichiarazioni  critiche del deputato nei &#13;
confronti del prof. Prodi, con riferimento alla tentata vendita della &#13;
SME,  in  occasione  di  un  suo  intervento  in  aula  nel corso del &#13;
dibattito sulle comunicazioni del Governo; &#13;
          3)  a  numerosi interventi di altri deputati in discussioni &#13;
alla  Camera  sul tema della vendita della SME ed in particolare agli &#13;
interventi   dell'on. Bruno  nella  seduta  del  15  maggio  1998,  e &#13;
dell'on. Becchetti e dell'on. Garra, rispettivamente nella seduta del &#13;
30  novembre  1999 e nella seduta della Commissione permanente affari &#13;
costituzionali  del  lo  dicembre  1999,  che avevano fatto specifico &#13;
riferimento al contenuto dell'intervista in questione; &#13;
          che,    ad    avviso   del   ricorrente,   la   prerogativa &#13;
dell'insindacabilità,   di   cui  all'art. 68,  primo  comma,  della &#13;
Costituzione,  non  copre,  per  costante giurisprudenza della Corte, &#13;
tutte  le  opinioni espresse dal parlamentare nello svolgimento della &#13;
sua attività politica, ma solo quelle legate da nesso funzionale con &#13;
le  attività  svolte nella sua qualità di membro della Camera, onde &#13;
oggetto  di protezione non sarebbe l'attività politica in genere del &#13;
parlamentare,  ampiamente  considerata,  "né  il contesto politico", &#13;
bensì  "l'esercizio  della  funzione  parlamentare e delle attività &#13;
consequenziali  e  presupposte, con la precisazione che tali funzioni &#13;
devono riguardare ambiti e modi giuridicamente definiti"; &#13;
          che  ne  discenderebbe,  secondo  il  ricorrente,  "che  la &#13;
semplice  comunanza  di  argomento  tra  la dichiarazione lesiva e le &#13;
opinioni  espresse in sede parlamentare non può bastare ad estendere &#13;
alla   prima   l'immunità  che  copre  la  seconda",  in  quanto  il &#13;
significato  del  nesso  funzionale  tra  dichiarazione  ed attività &#13;
parlamentare  si  dovrebbe  cogliere sub specie di "identificabilità &#13;
della   dichiarazione   stessa   quale   espressione   di   attività &#13;
parlamentare"  e,  dunque, "il problema specifico della riproduzione, &#13;
all'esterno  degli  organi  parlamentari,  di dichiarazioni già rese &#13;
nell'esercizio  di  funzioni  parlamentari"  potrebbe  dar  luogo  ad &#13;
insindacabilità  "solo  ove  sia  riscontrabile  una  corrispondenza &#13;
sostanziale   di  contenuti  con  l'atto  parlamentare,  non  essendo &#13;
sufficiente a questo riguardo una mera comunanza di tematiche"; &#13;
          che,  sulla  base di questi principi, il ricorrente censura &#13;
la  delibera  di  insindacabilità,  osservando,  in particolare, che &#13;
nessuna rilevanza può riconoscersi alle circostanze prima ricordate, &#13;
in quanto: &#13;
        a) la precedente intervista, di analogo contenuto, rilasciata &#13;
il  7 febbraio  1995  dall'on. Armani  a  "Il  Corriere  della Sera", &#13;
contiene  una  manifestazione  di pensiero non inerente alla funzione &#13;
parlamentare; &#13;
        b) l'intervento in aula dell'on. Armani, tenuto nel corso del &#13;
dibattito sulle comunicazioni del Governo, seppure svolto in ambito e &#13;
modi   propri   della  funzione  parlamentare,  manca  del  requisito &#13;
dell'identità  sostanziale  di  contenuto con l'opinione manifestata &#13;
nella  sede  esterna,  con la quale ha soltanto una mera comunanza di &#13;
tema; &#13;
        c)  gli  altri  interventi,  nelle  discussioni  alla Camera, &#13;
peraltro  di  deputati  diversi  dall'on. Armani  e talora successivi &#13;
all'intervista, hanno contenuto generico ed approssimativo; &#13;
          che  il  ricorrente  ritiene  pertanto che le dichiarazioni &#13;
dell'on. Armani  non  siano  state rese nell'esercizio delle funzioni &#13;
parlamentari,  onde  - non essendo per esse invocabile l'immunità di &#13;
cui  all'art. 68,  primo comma, della Costituzione - la deliberazione &#13;
di insindacabilità deve essere annullata. &#13;
    Considerato  che in questa fase del giudizio la Corte è chiamata &#13;
a   deliberare,  senza  contraddittorio  e  prima  facie,  in  ordine &#13;
all'ammissibilità  del ricorso sotto il profilo dell'identificazione &#13;
dei poteri dello Stato, che si contrappongono, e dell'esistenza della &#13;
materia  di  un  conflitto  la  cui  risoluzione  spetti alla propria &#13;
competenza,  restando  impregiudicata ogni ulteriore decisione, anche &#13;
in punto di ammissibilità, con riguardo altresì all'incidenza della &#13;
menzionata  delibera  parlamentare  sul  procedimento pendente avanti &#13;
all'autorità giudiziariaricorrente; &#13;
          che,  sotto  il  profilo  soggettivo,  la legittimazione ad &#13;
essere  parte  nei  conflitti  di attribuzione tra poteri dello Stato &#13;
spetta  -  per  costante  giurisprudenza  di  questa Corte - tanto ai &#13;
singoli   organi   giurisdizionali,   nell'esercizio   dell'attività &#13;
giurisdizionale  esercitata in piena indipendenza, quanto alla Camera &#13;
dei  deputati,  organo  competente  a  dichiarare  definitivamente la &#13;
propria volontà in ordine all'applicabilità dell'art. 68 Cost.; &#13;
          che,  sotto  il  profilo oggettivo, l'autorità giudiziaria &#13;
ricorrente  ritiene  che la propria potestas iudicandi - attribuzione &#13;
costituzionalmente   garantita   -  sia  stata  lesa  dall'esercizio, &#13;
asseritamente  illegittimo,  del  potere,  spettante  alla  Camera di &#13;
appartenenza  del parlamentare, di dichiarare insindacabili, ai sensi &#13;
dell'art. 68, primo comma, Cost., le opinioni da lui espresse; &#13;
          che,  pertanto,  ricorrono  i  requisiti sia soggettivi che &#13;
oggettivi  necessari  al  fine di ritenere ammissibile il ricorso per &#13;
conflitto di attribuzione tra poteri dello Stato.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara  ammissibile, ai sensi dell'art. 37 della legge 11 marzo &#13;
1953,  n. 87,  il  ricorso  per  conflitto di attribuzione fra poteri &#13;
dello Stato proposto dal tribunale di Roma nei confronti della Camera &#13;
dei deputati con il ricorso indicato in epigrafe; &#13;
    Dispone: &#13;
        a)  che  la  cancelleria  della Corte dia comunicazione della &#13;
presente  ordinanza  al  tribunale  di  Roma,  autorità  giudiziaria &#13;
ricorrente; &#13;
        b)  che, a cura dell'autorità giudiziaria ricorrente, l'atto &#13;
introduttivo  del  giudizio  e la presente ordinanza siano notificati &#13;
alla  Camera  dei  deputati entro il termine di sessanta giorni dalla &#13;
comunicazione   di  cui  al  punto  a),  per  essere  successivamente &#13;
depositati  nella  cancelleria  di  questa  Corte entro il termine di &#13;
venti giorni dalla notificazione, ai sensi dell'art. 26, terzo comma, &#13;
delle   norme   integrative   per   i   giudizi  davanti  alla  Corte &#13;
costituzionale. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Bile &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 12 luglio 2001. &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
