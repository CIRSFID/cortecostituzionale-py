<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>216</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:216</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>MORTATI</presidente>
    <relatore_pronuncia>Vezio Crisafulli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/12/1972</data_decisione>
    <data_deposito>30/12/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. COSTANTNO MORTATI, Presidente - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. &#13;
 ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI - Avv. LEONETTO AMADEI - Prof. GIULIO GIONFRIDA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  dell'art.  15  della  &#13;
 legge  17  febbraio  1968,  n.  108 (norme per la elezione dei Consigli  &#13;
 regionali delle Regioni a  statuto  normale),  promosso  con  ordinanza  &#13;
 emessa  il  13  giugno  1970  dall'Ufficio centrale circoscrizionale di  &#13;
 Benevento per l'elezione del Consiglio  regionale  della  Campania  sul  &#13;
 ricorso  di  Tibaldi Antonio, iscritta al n. 230 del registro ordinanze  &#13;
 1970 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 235  del  &#13;
 16 settembre 1970.                                                       &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  22  novembre  1972  il  Giudice  &#13;
 relatore Vezio Crisafulli;                                               &#13;
     udito  il sostituto avvocato generale dello Stato Giorgio Azzariti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con  ordinanza  emessa  il 13 giugno 1970 l'Ufficio centrale  &#13;
 circoscrizionale di Benevento per l'elezione  del  Consiglio  regionale  &#13;
 della  Campania  ha sollevato questione di legittimità costituzionale,  &#13;
 per contrasto con gli artt. 3 (anche con riferimento all'eguaglianza di  &#13;
 fatto)  e  48,  comma  secondo  (specificatamente  sotto  il   riflesso  &#13;
 dell'eguaglianza  del  voto)  della  Costituzione,  della  norma di cui  &#13;
 all'art. 15 della legge 17 febbraio 1968, n. 108, nella  parte  in  cui  &#13;
 essa  dispone  l'attribuzione di seggi nel Collegio unico regionale, in  &#13;
 contrasto con la preventiva assegnazione dei seggi spettanti a ciascuna  &#13;
 provincia  della  regione,  in  base   alle   rispettive   popolazioni,  &#13;
 consentendo  - al limite - che una provincia possa addirittura rimanere  &#13;
 senza propri rappresentanti.                                             &#13;
     Ritenuta  la  propria  competenza  a  denunciare  la  questione  di  &#13;
 legittimità  costituzionale,  l'Ufficio  rileva  che nella specie, per  &#13;
 effetto della norma in esame, la Provincia  di  Benevento  risulterebbe  &#13;
 avere   solo   tre  consiglieri,  invece  dei  quattro  preventivamente  &#13;
 assegnatile in base al numero dei suoi abitanti, rimanendo così  senza  &#13;
 effetto il voto di 80.000 elettori.                                      &#13;
     2.  -  È  intervenuto  in giudizio il Presidente del Consiglio dei  &#13;
 ministri, rappresentato e difeso dall'Avvocato  generale  dello  Stato,  &#13;
 con  deduzioni  depositate  il 6 ottobre 1970 nelle quali con argomenti  &#13;
 tratti sia dalla giurisprudenza ordinaria che da quella  costituzionale  &#13;
 contesta la natura giurisdizionale dell'ufficio elettorale come pure la  &#13;
 sua   appartenenza   all'organizzazione   giudiziaria,  deducendone  la  &#13;
 mancanza di legittimazione ad adire la Corte.                            &#13;
     Nel merito la questione sarebbe infondata, in quanto  il  principio  &#13;
 costituzionale dell'eguaglianza del voto concernerebbe soltanto la pari  &#13;
 efficacia  potenziale, ma non anche i risultati concreti della volontà  &#13;
 espressa dall'elettore, i quali dipendono  esclusivamente  dal  sistema  &#13;
 adottato  dal legislatore ordinario nel suo discrezionale apprezzamento  &#13;
 delle mutevoli esigenze  connesse  alle  consultazioni  popolari  (cfr.  &#13;
 sentenze n. 43 del 1961 e 6 e 60 del 1963). Nella specie, del resto, la  &#13;
 scelta  del  legislatore, senza incidere sui diritti costituzionali dei  &#13;
 cittadini, avrebbe ritenuto  prevalente  l'esigenza  di  assicurare,  a  &#13;
 tutela  delle  minoranze, una proporzionale rappresentanza politica per  &#13;
 tutti gli elettori  nel  Consiglio  regionale,  rispetto  all'altra  di  &#13;
 garantire  una  proporzionale  rappresentanza  territoriale  a ciascuna  &#13;
 provincia.                                                               &#13;
     Le conclusioni  dell'Avvocatura  dello  Stato  sono,  perciò,  per  &#13;
 l'inammissibilità  o,  comunque,  per  l'infondatezza  della questione  &#13;
 proposta.                                                                &#13;
     3. - Alla pubblica udienza  l'Avvocatura  ha  insistito  nelle  sue  &#13;
 conclusioni,  con  particolare  riguardo  alla  inammissibilità  della  &#13;
 questione.<diritto>Considerato in diritto</diritto>:                          &#13;
     Come riferito in narrativa, l'Ufficio  circoscrizionale  elettorale  &#13;
 di  Benevento  si  è posto preliminarmente il quesito circa la propria  &#13;
 qualificazione come  giudice,  ai  fini  della  idoneità  a  sollevare  &#13;
 questioni  di  legittimità  costituzionale delle norme di legge che è  &#13;
 chiamato ad applicare e lo ha risolto in senso affermativo,  ravvisando  &#13;
 il  ricorrere  di  entrambi  i criteri - soggettivo ed oggettivo - che,  &#13;
 alla stregua della giurisprudenza di questa Corte, permettono, ai  fini  &#13;
 predetti,  di  riconoscere  ad  un organo il carattere di giudice ed al  &#13;
 procedimento davanti ad esso svolgentesi natura di giudizio. La  difesa  &#13;
 dello  Stato  contesta,  invece,  tali  conclusioni,  richiamando anche  &#13;
 argomenti di ordine pratico, inerenti alla particolare celerità che la  &#13;
 legge ha voluto imprimere alle operazioni dirette  al  risultato  della  &#13;
 proclamazione degli eletti, e chiede pertanto che la questione proposta  &#13;
 sia dichiarata inammissibile.                                            &#13;
     L'eccezione pregiudiziale è fondata.                                &#13;
     Sotto   il  profilo  soggettivo,  infatti,  gli  uffici  elettorali  &#13;
 circoscrizionali sono organi temporanei, costituiti di volta  in  volta  &#13;
 "presso il tribunale" nella cui giurisdizione è il comune capoluogo di  &#13;
 provincia, i quali, mentre non si identificano con lo stesso tribunale,  &#13;
 nemmeno  danno  vita ad altrettante sezioni o particolari articolazioni  &#13;
 del medesimo, e non  sono  perciò  istituzionalmente  incardinati  nel  &#13;
 potere  giurisdizionale  dello  Stato.  La legge prescrive, bensì, che  &#13;
 siano composti  di  tre  "magistrati",  designati  dal  presidente  del  &#13;
 tribunale, ma non dice anche che debbano necessariamente essere formati  &#13;
 da  "giudici  ad  esso  appartenenti"  (che  anzi,  gli analoghi uffici  &#13;
 elettorali centrali previsti, per le elezioni comunali nei  comuni  con  &#13;
 più  di  diecimila  abitanti,  dall'art.  71 del testo unico 16 maggio  &#13;
 1960, n. 570, sono soltanto presieduti dal presidente del  tribunale  o  &#13;
 da  altro magistrato da questo delegato e costituiti, per il rimanente,  &#13;
 dai componenti dell'ufficio elettorale della prima sezione, i quali non  &#13;
 sono  affatto  magistrati;  mentre,  per  l'art.  67,  nei  comuni  con  &#13;
 popolazione    inferiore,    in    luogo    degli   uffici   elettorali  &#13;
 circoscrizionali, funzionano speciali collegi comprendenti i presidenti  &#13;
 delle varie sezioni). D'altronde, a differenza  da  quanto  per  regola  &#13;
 generale  avviene  per  gli organi giurisdizionali, ordinari o speciali  &#13;
 che siano, gli uffici circoscrizionali  constano,  in  realtà,  di  un  &#13;
 numero  variabile  di  membri,  essendo  attribuita  al  presidente del  &#13;
 tribunale  facoltà  di  aggregarvi,   a   richiesta   dei   rispettivi  &#13;
 presidenti,  "altri  magistrati,  nel  numero  necessario  per  il più  &#13;
 sollecito espletamento delle operazioni".                                &#13;
     Sotto il profilo oggettivo,  poi,  e  cioè  avendo  riguardo  alla  &#13;
 funzione esplicata, è da rilevare, anzitutto, che questa si inserisce,  &#13;
 come  una  sua fase particolare e strettamente delimitata nei compiti e  &#13;
 nel tempo, entro l'arco di un complesso procedimento -  al  quale,  nel  &#13;
 suo   insieme,   non   potrebbe   in  alcun  modo  riconoscersi  natura  &#13;
 giurisdizionale né a questa assimilabile - che muove dalla  formazione  &#13;
 delle  liste  e  dalla  presentazione e verifica delle candidature, per  &#13;
 sfociare nel procedimento di convalida degli eletti,  di  competenza  -  &#13;
 per  quanto  qui  interessa  -  dei  consigli  regionali.  Procedimento  &#13;
 nettamente distinto e diverso da quelli, senza dubbio  giurisdizionali,  &#13;
 che  potranno  svolgersi,  in  seguito  a  ricorsi contro le operazioni  &#13;
 elettorali, davanti ai tribunali amministrativi regionali, quali organi  &#13;
 di giustizia amministrativa di primo grado, e poi, in sede di  appello,  &#13;
 davanti  al  Consiglio  di  Stato,  ovvero, per questioni relative alla  &#13;
 eleggibilità, successivamente alla intervenuta convalida, dinanzi alla  &#13;
 autorità  giudiziaria  ordinaria,  attraverso   ben   tre   gradi   di  &#13;
 giurisdizione.                                                           &#13;
     Il  fine,  cui  la  funzione degli uffici elettorali chiaramente è  &#13;
 preordinata, non è già di dichiarare o attuare nel caso  concreto  la  &#13;
 volontà  della  legge, ma consiste piuttosto nel dare soddisfazione al  &#13;
 pubblico interesse alla pronta costituzione, sia pure in una formazione  &#13;
 provvisoria,  che  potrà  in  seguito  subire  dei  mutamenti,   delle  &#13;
 Assemblee   elettive  degli  enti  regionali;  ed  a  tale  scopo  essi  &#13;
 pervengono  -  in esecuzione vincolata delle norme di legge - compiendo  &#13;
 una serie di attività materiali e  di  conteggio,  che,  com'è  stato  &#13;
 anche  recentemente  ritenuto  dalla  Corte di cassazione, in relazione  &#13;
 agli analoghi uffici istituiti per le elezioni politiche, sono semplici  &#13;
 operazioni amministrative, dalle quali esula un momento suscettibile di  &#13;
 configurarsi come propriamente decisorio. Potrebbe  sembrare,  a  prima  &#13;
 vista,  fare  eccezione  quel  riesame  delle  schede  contenenti  voti  &#13;
 contestati e provvisoriamente non assegnati, a  conclusione  del  quale  &#13;
 "l'ufficio...  tenendo presenti le annotazioni riportate a verbale e le  &#13;
 proteste e i reclami presentati in proposito,  decide,  ai  fini  della  &#13;
 proclamazione, sull'assegnazione o meno dei voti relativi".  Ma - anche  &#13;
 a   prescindere   dal  rilievo  che,  nella  specie,  la  questione  di  &#13;
 costituzionalità è stata sollevata nel corso  della  fase  terminale,  &#13;
 prevista  dall'ultimo  comma dell'art.  15 della legge del 1968, che è  &#13;
 di mera e stretta esecuzione di operazioni già effettuate dall'ufficio  &#13;
 regionale - l'eccezione è soltanto apparente. Infatti, il  compito  di  &#13;
 decidere per intanto ed in linea provvisoria, sulle schede contestate e  &#13;
 non  assegnate, è attribuito agli uffici elettorali all'unico scopo di  &#13;
 pervenire al più presto alla proclamazione (a sua  volta,  provvisoria  &#13;
 anch'essa)  degli  eletti, affinché i consigli siano posti in grado di  &#13;
 insediarsi  ed  iniziare  il  proprio  funzionamento  Ed  è  superfluo  &#13;
 soffermarsi  a  sottolineare  quanto  circoscritto  sia  anche siffatto  &#13;
 compito, eventuale  e  puramente  strumentale,  spettante  agli  uffici  &#13;
 elettorali  circoscrizionali,  dal  momento  che rimangono sottratte al  &#13;
 riesame  le  schede  contenenti  voti  del  pari   contestati,   ma   -  &#13;
 diversamente da quelli - tuttavia assegnati dagli uffici di sezione.     &#13;
     Né  può  trascurarsi,  infine,  la considerazione, sulla quale ha  &#13;
 insistito, specie nella discussione orale, la difesa dello  Stato,  che  &#13;
 le  operazioni  elettorali successive alle votazioni e culminanti nella  &#13;
 proclamazione  degli  eletti  non  possono,  per  loro  natura,  subire  &#13;
 sospensioni  di  più  o meno lunga durata, né il loro compimento può  &#13;
 essere  procrastinato  a  volontà  dagli   uffici   elettorali,   come  &#13;
 accadrebbe  ove  questi  fossero - in contrasto con tutti i rilievi che  &#13;
 precedono - considerati giudici, legittimati pertanto a porre questioni  &#13;
 di legittimità costituzionale a questa Corte.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di legittimità  costituzionale  &#13;
 dell'art. 15 della legge 17 febbraio 1968, n.  108, "nella parte in cui  &#13;
 dispone  l'attribuzione  di  seggi  nel  collegio  unico  regionale, in  &#13;
 contrasto con la preventiva assegnazione dei seggi spettanti a ciascuna  &#13;
 provincia   della   regione",   sollevata    dall'Ufficio    elettorale  &#13;
 circoscrizionale di Benevento con l'ordinanza di cui in epigrafe.        &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 18 dicembre 1972.                             &#13;
                                   COSTANTINO MORTATI - GIUSEPPE   VERZÌ  &#13;
                                   -   GIOVANNI   BATTISTA  BENEDETTI  -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   -   NICOLA  REALE  -  PAOLO  ROSSI  -  &#13;
                                   LEONETTO AMADEI - GIULIO GIONFRIDA.    &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
