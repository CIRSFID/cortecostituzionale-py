<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>19</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:19</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/01/1990</data_decisione>
    <data_deposito>23/01/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale degli artt. 20, terzo    &#13;
 comma, e 28,  primo  comma,  del  d.P.R.  26  ottobre  1972,  n.  636    &#13;
 (Revisione  della  disciplina  del contenzioso tributario), nel testo    &#13;
 novellato dal d.P.R. 3 novembre 1981, n. 739, promosso con  ordinanza    &#13;
 emessa  il 10 aprile 1989 dalla Commissione Tributaria di primo grado    &#13;
 di Verbania sul ricorso proposto da Girardello  Antonio  e  l'Ufficio    &#13;
 Imposte  Dirette  di Arona, iscritta al n. 371 del registro ordinanze    &#13;
 1989 e pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  35,    &#13;
 prima serie speciale, dell'anno 1989;                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    udito  nella  camera  di consiglio del 29 novembre 1989 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto che, nel procedimento instaurato da Girardello Antonio di    &#13;
 impugnazione dell'iscrizione a ruolo di imposte I.R.P.E.F. e I.L.O.R.    &#13;
 per gli anni 1979 e 1980, la Commissione Tributaria di primo grado di    &#13;
 Verbania, con ordinanza del 10 aprile 1989 (R.O. n. 371 del 1989), ha    &#13;
 sollevato  questione  di  legittimità costituzionale degli artt. 20,    &#13;
 terzo comma, e 28, primo comma, del d.P.R. 26 ottobre 1972,  n.  636,    &#13;
 nel  testo  novellato dal d.P.R. 3 novembre 1981, n. 739, nella parte    &#13;
 in cui non prevedono per i componenti  delle  Commissioni  Tributarie    &#13;
 l'obbligo  del  segreto  sulla camera di consiglio e, in particolare,    &#13;
 sul processo di formazione della decisione del collegio;                 &#13;
      che,  a  parere  della  Commissione  remittente,  risulterebbero    &#13;
 violati gli artt.  108,  secondo  comma,  e  3,  primo  comma,  della    &#13;
 Costituzione,  in  quanto sarebbe leso il principio dell'indipendenza    &#13;
 dei  giudici  delle  giurisdizioni  speciali  e   si   verificherebbe    &#13;
 disparità  di  trattamento  nei confronti di tutti gli altri giudici    &#13;
 per i quali vige il rispetto del segreto della camera di consiglio;      &#13;
      che   l'Avvocatura   Generale   dello   Stato,   intervenuta  in    &#13;
 rappresentanza del Presidente del Consiglio dei ministri, ha concluso    &#13;
 per  la  inammissibilità  della  questione,  per mancata motivazione    &#13;
 sulla rilevanza e per insussistenza della  stessa  rilevanza,  e  nel    &#13;
 merito per la infondatezza;                                              &#13;
    Considerato  che  questa  Corte nella sentenza n. 18 del 1989, con    &#13;
 cui ha dichiarato la non fondatezza della questione  di  legittimità    &#13;
 costituzionale  dell'art.  16 della legge 13 aprile 1988, n. 117, che    &#13;
 ha disciplinato ex novo la materia, in riferimento agli artt.  101  e    &#13;
 104  della  Costituzione,  ha  affermato  che  nel nostro ordinamento    &#13;
 costituzionale non esiste un nesso imprescindibile  tra  indipendenza    &#13;
 del   giudice   e  segretezza  e  che  nessuna  norma  costituzionale    &#13;
 stabilisce il segreto delle deliberazioni degli organi giudiziari  di    &#13;
 qualunque ordine e grado, quale garanzia della loro indipendenza, né    &#13;
 a tal fine impone il segreto delle opinioni dissenzienti;                &#13;
      che  il  segreto costituisce materia di scelta legislativa senza    &#13;
 alcun rapporto con l'indipendenza del giudice ed è un valore  morale    &#13;
 che  si  realizza  in tutta la sua pienezza proprio quando si esplica    &#13;
 nella trasparenza del comportamento;                                     &#13;
      che, in applicazione dei surrichiamati principi, la questione in    &#13;
 esame va dichiarata manifestamente infondata;                            &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 20, terzo comma, e 28,  primo  comma,  del    &#13;
 d.P.R.  26  ottobre  1972,  n.  636  (Revisione  della disciplina del    &#13;
 contenzioso tributario), nel testo novellato dal  d.P.R.  3  novembre    &#13;
 1981,  n.  739,  in  riferimento  agli artt. 108, secondo comma, e 3,    &#13;
 primo  comma,  della  Costituzione,   sollevata   dalla   Commissione    &#13;
 Tributaria  di  primo  grado  di Verbania con l'ordinanza indicata in    &#13;
 epigrafe;                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 18 gennaio 1990.                              &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 gennaio 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
