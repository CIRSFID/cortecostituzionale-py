<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>423</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:423</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/10/1992</data_decisione>
    <data_deposito>09/11/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  23,  primo    &#13;
 comma,  della  legge  31  maggio  1975,  n.  191  (Nuove norme per il    &#13;
 servizio di leva), in relazione all'art. 22, n.  6,  come  sostituito    &#13;
 dall'art.  3  della  legge  11  agosto  1991,  n.  269  (Modifiche ed    &#13;
 integrazioni agli articoli 21 e 22 della legge  31  maggio  1975,  n.    &#13;
 191,  ed all'articolo 100 del decreto del Presidente della Repubblica    &#13;
 14 febbraio 1964, n. 237, come sostituito dall'articolo 7 della legge    &#13;
 24 dicembre 1986, n. 958, in materia di  dispensa  e  di  rinvio  del    &#13;
 servizio  di  leva), promosso con ordinanza emessa il 9 dicembre 1991    &#13;
 dal  Tribunale  amministrativo  regionale  della  Sicilia  -  Sezione    &#13;
 distaccata di Catania, sul ricorso proposto da Davide Barnabà  contro    &#13;
 il  Distretto  Militare  di  Catania ed altri, iscritta al n. 244 del    &#13;
 registro ordinanze 1992 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 20, prima serie speciale dell'anno 1992;                   &#13;
    Udito  nella  camera  di  consiglio del 21 ottobre 1992 il Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Ritenuto che con ordinanza emessa il 9 dicembre 1991, il Tribunale    &#13;
 amministrativo  regionale  della  Sicilia  -  Sezione  distaccata  di    &#13;
 Catania,  sul  ricorso  proposto  da Davide Barnabà contro  Distretto    &#13;
 Militare di Catania ed altri (Reg. ord.  n.  244/1992)  ha  sollevato    &#13;
 questione di legittimità costituzionale, con riferimento all'art. 3,    &#13;
 primo  comma  e  all'art.  52,  secondo  comma,  della  Costituzione,    &#13;
 dell'art. 23, primo comma, della legge n. 191 del 1975  (Nuove  norme    &#13;
 per  il  servizio di leva), in relazione all'art. 22, numero 6, della    &#13;
 stessa legge, come sostituito dall'art. 3 della legge 11 agosto 1991,    &#13;
 n. 269 (Modifiche ed integrazioni agli articoli 21 e 22  della  legge    &#13;
 31  maggio  1975,  n.  191,  ed  all'articolo  100  del  decreto  del    &#13;
 Presidente della Repubblica 14 febbraio 1964, n. 237, come sostituito    &#13;
 dall'articolo 7 della legge 24 dicembre 1986, n. 958, in  materia  di    &#13;
 dispensa  e  di  rinvio  del  servizio  di  leva), nella parte in cui    &#13;
 esclude dall'ammissione al beneficio della dispensa  dall'obbligo  di    &#13;
 prestazione del servizio militare colui che abbia un fratello di età    &#13;
 inferiore  ai  40 anni, il quale abbia fruito di riduzione o dispensa    &#13;
 dalla ferma di leva;                                                     &#13;
    Considerato che il  riferimento  all'età  di  40  anni  contenuto    &#13;
 nell'art.  23  della legge 31 maggio 1975 n. 191 concerne globalmente    &#13;
 anche altre ipotesi previste dalla normativa;                            &#13;
      che,   dunque,  la  sua  rimozione  contrasterebbe  con  criteri    &#13;
 ispiratori di disciplina, ordinata a sistema dal legislatore  secondo    &#13;
 una sua discrezionalità, costituendo ingerenza nella sfera riservata    &#13;
 alle valutazioni del Parlamento;                                         &#13;
      che,  in  conseguenza, la questione va dichiarata manifestamente    &#13;
 inammissibile;                                                           &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale,  in  riferimento  agli  artt.  3,  primo    &#13;
 comma,  e  52, secondo comma, della Costituzione, dell'art. 23, primo    &#13;
 comma, della legge 31 maggio 1975 n. 191 (Nuove norme per il servizio    &#13;
 di leva) in relazione all'art. 22, numero 6, della stessa legge, come    &#13;
 sostituito dall'art. 3 della legge n. 269  del  1991,  sollevata  dal    &#13;
 Tribunale   amministrativo   della  Sicilia,  Sezione  distaccata  di    &#13;
 Catania, con l'ordinanza in epigrafe.                                    &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 22 ottobre 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: BORZELLINO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 9 novembre 1992.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
