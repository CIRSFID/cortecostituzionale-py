<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>578</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:578</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/12/1989</data_decisione>
    <data_deposito>22/12/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 3, terzo comma,    &#13;
 della legge 21 marzo 1953, n.161 (Modificazioni al testo unico  delle    &#13;
 leggi  sulla  Corte  dei  conti), promosso con ordinanza emessa il 22    &#13;
 novembre 1988 dalla Corte dei conti sul ricorso proposto  da  Canneva    &#13;
 Lorenzo,  iscritta al n. 346 del registro ordinanze 1989 e pubblicata    &#13;
 nella  Gazzetta  Ufficiale  della  Repubblica  n.  29,  prima   serie    &#13;
 speciale, dell'anno 1989;                                                &#13;
    Visto l'atto di costituzione di Esposito Anna ed altri;               &#13;
    Udito  nell'udienza  pubblica  del  28  novembre  1989  il Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Uditi l'avv. Luigi Esposito per Esposito Anna ed altri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  ordinanza  del 22 novembre 1988 (pervenuta il 28 giugno 1989)    &#13;
 emessa dalla Corte dei conti sul ricorso proposto da Canneva  Lorenzo    &#13;
 (n.346/1989)   è   stata   sollevata   questione   di   legittimità    &#13;
 costituzionale dell'art. 3, terzo comma, della legge 21  marzo  1953,    &#13;
 n.161  (Modificazioni  al  testo  unico  delle  leggi sulla Corte dei    &#13;
 conti), nella parte in cui escluderebbe la facoltà del ricorrente di    &#13;
 farsi  rappresentare  da  un  difensore  nei  giudizi sui ricorsi per    &#13;
 pensione di guerra, in riferimento all'art. 24 Cost.                     &#13;
    Dall'ordinanza  si  evince  che il giudizio a quo si è interrotto    &#13;
 per morte del ricorrente ed è stato poi proseguito dagli  eredi  del    &#13;
 medesimo,  con  comparsa  sottoscritta  dal  difensore  e procuratore    &#13;
 speciale. L'adito giudice, premesso che nella specie il processo  non    &#13;
 poteva   ritenersi   validamente  radicato  alla  luce  del  disposto    &#13;
 dell'art. 3, comma terzo, della legge n. 161 del  1953,  è  d'avviso    &#13;
 che  la  norma  escluderebbe, nei giudizi sui ricorsi per pensione di    &#13;
 guerra, la facoltà del ricorrente di  farsi  "rappresentare"  da  un    &#13;
 difensore  (ma  solo  di  farsi  "assistere");  da qui la conseguente    &#13;
 questione di legittimità per asserito contrasto con l'art. 24  Cost.    &#13;
    Nel  giudizio  innanzi a questa Corte si sono costituiti gli eredi    &#13;
 ricorrenti che hanno  concluso  anch'essi  per  una  declaratoria  di    &#13;
 illegittimità  delle  disposizioni,  "a  meno  che (dette norme) non    &#13;
 siano interpretate nel senso che il  legislatore  usando  espressioni    &#13;
 improprie ha inteso rendere facoltativa la rappresentanza e la difesa    &#13;
 della parte attraverso l'opera di un professionista  patrocinante  in    &#13;
 cassazione".<diritto>Considerato in diritto</diritto>1.1  -  L'art.  3,  comma  terzo, della legge 21 marzo 1953, n.161    &#13;
 (Modificazioni al testo unico delle  leggi  sulla  Corte  dei  conti)    &#13;
 stabilisce  che  nei  giudizi avanti alla detta Corte sui ricorsi per    &#13;
 pensione  di  guerra  resta  consentito  alle  parti  di  intervenire    &#13;
 personalmente,   giusta   l'art.  18  del  regolamento  di  procedura    &#13;
 approvato con r.d. 13 agosto 1933, n.  1038;  che  tuttavia,  ove  il    &#13;
 ricorrente  sia  assistito da un avvocato gli onorari di quest'ultimo    &#13;
 sono ridotti a un quarto.                                                &#13;
   1.2  -  Da  quest'ultima  disposizione,  relativa  agli onorari, il    &#13;
 giudice  a  quo  vorrebbe  far  derivare  l'impossibilità   per   il    &#13;
 ricorrente  di  poter  usufruire,  ai  fini  di  rappresentanza,  del    &#13;
 patrocinio di un difensore, con ciò assumendosi violate le  garanzie    &#13;
 contenute nell'art. 24 Cost.                                             &#13;
    2.  -  Senonché,  va  considerato che il legislatore del 1953 nel    &#13;
 mantenere ferme, per i ricorsi in questione, "le norme attualmente in    &#13;
 vigore"  ha  chiaramente  richiamato  la  facoltà  per  le  parti di    &#13;
 "intervenire  personalmente  od  a   mezzo   dell'avvocato   che   le    &#13;
 rappresenti"  (art.  18  r.d.  n.  1038  del 1933). E il patrocinante    &#13;
 iscritto  nell'albo  speciale  per  la  Corte  di  cassazione,  unico    &#13;
 abilitato  alla  difesa anche per tutti i giudizi di competenza della    &#13;
 Corte dei conti, esercita coevamente  funzioni  di  assistenza  e  di    &#13;
 rappresentanza.                                                          &#13;
    Comunque  va  chiarito che successivamente alla normativa del 1953    &#13;
 la specifica legislazione in materia ha espressamente  determinato  i    &#13;
 poteri  del  difensore inerenti anche all'esercizio dell'attività di    &#13;
 rappresentanza; sicché per i giudizi di cui trattasi è  esplicabile    &#13;
 il pieno patrocinio legale.                                              &#13;
    Di  nessun  rilievo,  perciò, resta l'affermazione del Collegio a    &#13;
 quo, secondo cui  l'ipotesi,  normativamente  contemplata  (cfr.,  da    &#13;
 ultimo,  art.  25  del d.P.R. 30 dicembre 1981, n. 834 col definitivo    &#13;
 riordinamento  delle  pensioni  di  guerra),  di   patrocinio   nella    &#13;
 prosecuzione  dopo  decesso  del  ricorrente,  costituirebbe  una non    &#13;
 applicabile "disposizione stereotipata".                                 &#13;
    Conclusivamente,  e  in tali sensi, la questione va dichiarata non    &#13;
 fondata, poiché  risultano  assicurati  gli  essenziali  compiti  di    &#13;
 ministero legale di cui ai principi di garanzia previsti dall'art. 24    &#13;
 Cost. (cfr. già sent. n. 46 del 1957).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata, nei sensi di cui in motivazione, la questione    &#13;
 di legittimità costituzionale dell'art. 3, terzo comma, della  legge    &#13;
 21 marzo 1953, n. 161 (Modificazioni al testo unico delle leggi sulla    &#13;
 Corte dei conti), in riferimento all'art. 24 Cost.,  sollevata  dalla    &#13;
 Corte dei conti con l'ordinanza in epigrafe.                             &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 dicembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: BORZELLINO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 22 dicembre 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
