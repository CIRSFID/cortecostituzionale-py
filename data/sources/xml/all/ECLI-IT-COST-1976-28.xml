<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>28</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:28</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>OGGIONI</presidente>
    <relatore_pronuncia>Nicola Reale</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/02/1976</data_decisione>
    <data_deposito>19/02/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. LUIGI OGGIONI, Presidente - Avv. &#13;
 ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA &#13;
 REALE - Prof. PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO &#13;
 GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. &#13;
 MICHELE ROSSANO - Prof. ANTONINO DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  del Titolo I della  &#13;
 legge 28 luglio 1967, n. 641, così  come  modificato  dalla  legge  17  &#13;
 febbraio  1968,  n.  106  (norme  in  materia  di edilizia scolastica),  &#13;
 promosso  con  ricorso  del  Presidente  della  Giunta  provinciale  di  &#13;
 Bolzano,  notificato  il 19 febbraio 1972, depositato in cancelleria il  &#13;
 29 successivo ed iscritto al n. 34 del registro ricorsi 1972.            &#13;
     Visto l'atto di  costituzione  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  26  novembre  1975  il  Giudice  &#13;
 relatore Nicola Reale;                                                   &#13;
     uditi l'avv. Umberto Coronas, per la Provincia di  Bolzano,  ed  il  &#13;
 sostituto  avvocato  generale  dello  Stato  Giorgio  Azzariti,  per il  &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ricorso notificato il 19 febbraio 1972 al  Presidente  del  &#13;
 Consiglio  dei  ministri  e  depositato  il  29 febbraio successivo, il  &#13;
 Presidente  della  Giunta  provinciale  di  Bolzano  chiede   che   sia  &#13;
 dichiarata  l'illegittimità costituzionale del Titolo I della legge 28  &#13;
 luglio 1967, n. 641 (nel testo risultante dalle integrazioni  apportate  &#13;
 con  la  legge 17 febbraio 1968, n. 106) per contrasto con gli artt. 5,  &#13;
 n. 28, 68 ter e 17 quater della legge costituzionale 10 novembre  1971,  &#13;
 n.  1,  recante modificazioni e integrazioni dello Statuto speciale per  &#13;
 il Trentino-Alto Adige di cui alla  legge  costituzionale  26  febbraio  &#13;
 1948, n. 5, nonché con l'art. 13 del predetto Statuto.                  &#13;
     Le  norme  impugnate,  le  quali  prevedono  che  la  costituzione,  &#13;
 l'ampliamento, il completamento ed il riattamento di edifici,  compresi  &#13;
 le  palestre  e  gli  impianti  sportivi, destinati alle scuole statali  &#13;
 elementari, secondarie ed artistiche, nonché agli istituti statali  di  &#13;
 educazione  sono  eseguite  in base a programmi (statali) quinquennali,  &#13;
 contrasterebbero - secondo quanto  si  assume  nel  ricorso  -  con  le  &#13;
 sopravvenute  norme  dello  Statuto speciale per il Trentino-Alto Adige  &#13;
 sopra ricordate, le quali attribuiscono alla competenza esclusiva della  &#13;
 Provincia di Bolzano la materia  dell'edilizia  scolastica  precludendo  &#13;
 così ogni possibilità di intervento di organi ad essa estranei sia in  &#13;
 sede legislativa che in sede amministrativa.                             &#13;
     2.  -  Si è costituito in giudizio il Presidente del Consiglio dei  &#13;
 ministri, rappresentato e difeso dall'Avvocatura generale dello  Stato,  &#13;
 con  foglio  di  deduzioni  depositato  il  9 marzo 1972, chiedendo una  &#13;
 pronunzia di inammissibilità o di  reiezione  del  ricorso.  Sotto  il  &#13;
 primo  profilo  si  osserva  che  le sopraggiunte modifiche statutarie,  &#13;
 quand'anche   fossero   suscettibili   di    immediata    applicazione,  &#13;
 determinerebbero  un  problema  non già di conflitto ma di abrogazione  &#13;
 per successione di norme,  la  cui  soluzione  non  spetta  alla  Corte  &#13;
 costituzionale.     Comunque,  l'esame  delle  disposizioni  denunziate  &#13;
 darebbe la riprova che l'asserito contrasto non esiste o che esso  può  &#13;
 risolversi  solo  attraverso  un'opera di coordinamento legislativo tra  &#13;
 norme statali e provinciali.                                             &#13;
     3. - All'udienza del 26 novembre 1975, preso atto della entrata  in  &#13;
 vigore  delle  norme  di  attuazione relative all'edilizia, scolastica,  &#13;
 emanate con d.P.R. 1 novembre 1973, n. 687,  entrambe  le  parti  hanno  &#13;
 chiesto che sia dichiarata la cessazione della materia del contendere.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  - Con il ricorso in epigrafe la Provincia di Bolzano chiede che  &#13;
 sia dichiarata l'illegittimità costituzionale  delle  norme  contenute  &#13;
 nel  Titolo  I  della legge statale 28 luglio 1967, n. 641 (concernente  &#13;
 l'edilizia  scolastica),  nel  testo  risultante   dalle   integrazioni  &#13;
 apportate  con la legge 17 febbraio 1968, n. 106, per contrasto con gli  &#13;
 artt. 5, n. 28, 68 ter  e  17  quater  della  legge  costituzionale  10  &#13;
 novembre 1971, n. 1, recante modificazioni e integrazioni dello Statuto  &#13;
 speciale  del  Trentino-Alto  Adige di cui alla legge costituzionale 26  &#13;
 febbraio 1948, n. 5, nonché con l'art. 13 del  predetto  Statuto.  Nel  &#13;
 ricorso si assume che le norme impugnate, le quali riservano allo Stato  &#13;
 il  compito  di  provvedere  in  base  a  programmi  quinquennali, alla  &#13;
 costruzione, all'ampliamento, al completamento e  al    riattamento  di  &#13;
 edifici  destinati  alle  scuole  elementari, secondarie ed artistiche,  &#13;
 nonché agli istituti  statali  di  educazione,    contrastino  con  le  &#13;
 sopravvenute   norme  dello  Statuto  speciale    per  la  Regione  del  &#13;
 Trentino-Alto  Adige,  sopra  ricordate,  che  hanno  attribuito   alla  &#13;
 competenza   esclusiva   della   Provincia   di   Bolzano   la  materia  &#13;
 dell'edilizia scolastica (art. 5, n.    28);  prevedendo  altresì  che  &#13;
 nelle  materie  riservate  alla competenza esclusiva della Provincia la  &#13;
 quota ad essa spettante di investimenti nel settore  deve  essere  alla  &#13;
 medesima  devoluta  (art.  68 ter) ed,   inoltre, che "qualora lo Stato  &#13;
 intervenga con propri fondi nelle  Provincie di Trento  e  Bolzano,  in  &#13;
 esecuzione di piani  straordinari di edilizia scolastica, l'impiego dei  &#13;
 fondi  stessi  è  effettuato  di  intesa  con  la  Provincia" (art. 17  &#13;
 quater).                                                                 &#13;
     2. - Il ricorso è inammissibile.                                    &#13;
     Per vero,  tanto  le  Regioni  quanto  le  Provincie  ad  autonomia  &#13;
 speciale,  secondo i principi affermati da questa Corte con la sentenza  &#13;
 n. 13 del 1974 e successivamente ribaditi con l'ordinanza  n.  269  del  &#13;
 1974,   per  rimuovere  dalle  materie  attrbuite  alla  loro  potestà  &#13;
 legislativa le preesistenti  leggi  statali  che  eccedono  dai  limiti  &#13;
 imposti alla competenza del legislatore nazionale in virtù di norme di  &#13;
 rango costituzionale, emanate in epoca successiva all'entrata in vigore  &#13;
 di  quelle  leggi,  non  hanno  interesse a proporre ricorso al fine di  &#13;
 ottenere  la  declaratoria  di  illegittimità  costituzionale  perché  &#13;
 possono  sostituirle  con  proprie  leggi, ovviamente nell'ambito delle  &#13;
 proprie  competenze  e  nel  rispetto  dei  limiti  prefissati  a  tale  &#13;
 attività,  il  risultato  della  quale rimane soggetto al sindacato di  &#13;
 questa Corte.                                                            &#13;
     La Provincia ricorrente, cui l'art. 5 n. 28,  legge  costituzionale  &#13;
 10  novembre  1971, n. 1 (ora art. 8, n. 28, d.P.R.  31 agosto 1972, n.  &#13;
 670, t.u. delle leggi  sullo  Statuto  speciale  per  il  Trentino-Alto  &#13;
 Adige),   innovando   alla  legislazione  preesistente,  ha  attribuito  &#13;
 competenza normativa primaria in materia di edilizia scolastica, non si  &#13;
 è avvalsa della summenzionata facoltà e non ha legiferato in materia,  &#13;
 nemmeno  dopo  l'entrata  in vigore delle norme di attuazione di cui al  &#13;
 d.P.R. 1 novembre 1973, n. 687 (pubblicato nella Gazzetta Ufficiale  n.  &#13;
 296 del 16 novembre 1973), intervenuta nel corso del presente giudizio,  &#13;
 con  piena  osservanza  della normativa di cui agli artt. 57 e seguenti  &#13;
 della già citata legge costituzionale n.  1 del 1971 (ora artt. 107  e  &#13;
 segg. del tu.).                                                          &#13;
     Pertanto  stante  che  con  il  ricorso si assume l'invasione della  &#13;
 competenza legislativa della  Provincia  di  Bolzano,  è  evidente  il  &#13;
 difetto d'interesse di quest'ultima a proporre il presente giudizio.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile  il  ricorso  in  epigrafe  con il quale la  &#13;
 Provincia  di  Bolzano,  in  riferimento  all'art.   13   della   legge  &#13;
 costituzionale  26  febbraio  1948,  n.  5  (Statuto  speciale  per  il  &#13;
 Trentino-Alto Adige), nonché agli artt. 5, n. 28, 68 ter e  17  quater  &#13;
 della   legge   costituzionale  10  novembre  1971,  n.  1  (contenente  &#13;
 modificazioni e integrazioni al  predetto  Statuto),  ha  impugnato  il  &#13;
 Titolo  I  della  legge  28  luglio 1967, n. 641 (così come modificato  &#13;
 dalla successiva legge 17 febbraio 1968, n. 106), contenente norme  per  &#13;
 l'edilizia scolastica.                                                   &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 12 febbraio 1976.                             &#13;
                                   F.to: LUIGI OGGIONI - ANGELO DE MARCO  &#13;
                                   - ERCOLE ROCCHETTI - ENZO CAPALOZZA -  &#13;
                                   VINCENZO MICHELE  TRIMARCHI  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA  - EDOARDO VOLTERRA - GUIDO  &#13;
                                   ASTUTI - MICHELE ROSSANO  -  ANTONINO  &#13;
                                   DE STEFANO.                            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
