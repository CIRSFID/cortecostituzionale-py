<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>306</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:306</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>PESCATORE</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/07/1994</data_decisione>
    <data_deposito>15/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Gabriele PESCATORE; &#13;
 Giudici: avv. Ugo SPAGNOLI, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, primo comma,    &#13;
 del  d.P.R. 20 gennaio 1992, n. 23 (Concessione di amnistia per reati    &#13;
 tributari), promosso con  ordinanza  emessa  l'11  ottobre  1993  dal    &#13;
 Tribunale  di  Genova  nel  procedimento penale a carico di De Pietro    &#13;
 Rocco, iscritta al n. 761 del registro ordinanze  1993  e  pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 1, prima serie speciale,    &#13;
 dell'anno 1994;                                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio dell'11  maggio  1994  il  Giudice    &#13;
 relatore Massimo Vari.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.1.  -  Con  ordinanza  dell'11 ottobre 1993 (R.O. 761 del 1993),    &#13;
 emessa nel corso di un procedimento penale  a  carico  di  De  Pietro    &#13;
 Rocco, il Tribunale di Genova ha sollevato, in riferimento all'art. 3    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art. 1, primo comma, del d.P.R. 20 gennaio 1992,  n.  23,  nella    &#13;
 parte  in cui non consente a coloro che abbiano già composto, in via    &#13;
 amministrativa,  pendenze   tributarie   in   forza   di   precedenti    &#13;
 provvedimenti  di condono, "di beneficiare dell'amnistia, per periodi    &#13;
 d'imposta non definibili secondo le disposizioni del titolo VI  della    &#13;
 legge 30 dicembre 1991, n. 413".                                         &#13;
    Premette  il  remittente  che l'imputato si è avvalso del condono    &#13;
 tributario previsto dall'art. 8 del decreto-legge  n.  83  del  1991,    &#13;
 convertito nella legge n. 154 del 1991, per "violazioni tributarie di    &#13;
 tipo  formale  relative all'anno 1990", riconducibili alle previsioni    &#13;
 dell'art.  1,  sesto  comma,  del  decreto-legge  n.  429  del  1982,    &#13;
 convertito,  con  modificazioni,  nella  legge  n. 516 del 1982 (come    &#13;
 modificato dalla legge n. 154 del 1991), fermi restando  gli  effetti    &#13;
 penali  della  compiuta  violazione, punita con eguale sanzione anche    &#13;
 sotto il vigore della già menzionata legge n. 154 del 1991.             &#13;
    In  relazione  a  ciò,  l'ordinanza  assume  una  disparità   di    &#13;
 trattamento,  priva  di  giustificazione, tra chi ha definito in sede    &#13;
 amministrativa pendenze tributarie prima dell'entrata in  vigore  del    &#13;
 d.P.R.  20  gennaio  1992,  n.  23, senza conseguire l'estinzione del    &#13;
 reato, e chi, invece, avvalendosi delle disposizioni della  legge  n.    &#13;
 413  del  1991,  ha  composto  in  epoca  successiva  dette pendenze,    &#13;
 fruendo, così, dell'amnistia.                                           &#13;
    1.2. - È intervenuto il Presidente del  Consiglio  dei  ministri,    &#13;
 rappresentato e difeso dall'Avvocatura generale dello Stato. Rilevato    &#13;
 che "l'ordinanza di rinvio non è chiaramente comprensibile", per non    &#13;
 essere  resi  espliciti  "il  reato oggetto del processo" nonché "la    &#13;
 ragione della esclusione della amnistiabilità", e supponendo che "il    &#13;
 Tribunale dovesse pronunziarsi su di una violazione di tipo formale",    &#13;
 l'Avvocatura osserva che, nella  specie,  l'esclusione  dall'amnistia    &#13;
 del   reato  su  cui  il  giudice  remittente  deve,  verosimilmente,    &#13;
 pronunziarsi discende non dall'avere  l'imputato  usufruito,  per  le    &#13;
 sanzioni  amministrative,  del  beneficio  (c.d.  minicondono) di cui    &#13;
 all'art. 8 del decreto-legge n. 83 del 1991, bensì dalla circostanza    &#13;
 che detta violazione non è riconducibile ad alcuna delle ipotesi  di    &#13;
 reato  alle  quali  l'art.  1  del d.P.R. n. 23 del 1992 ricollega la    &#13;
 possibilità di fruire del provvedimento di clemenza.<diritto>Considerato in diritto</diritto>1. - La Corte è chiamata a stabilire se l'art.  1,  primo  comma,    &#13;
 del d.P.R. 20 gennaio 1992, n. 23, violi l'art. 3 della Costituzione,    &#13;
 nella  parte  in  cui  non consente, a coloro che si siano avvalsi di    &#13;
 precedenti provvedimenti di condono,  di  beneficiare  dell'amnistia,    &#13;
 per  periodi  di  imposta  non definibili secondo le disposizioni del    &#13;
 titolo VI della legge 30 dicembre 1991, n. 413.                          &#13;
    Il giudice remittente, chiamato in sede  penale  a  giudicare  una    &#13;
 violazione  tributaria  sanzionata  dall'art.  1,  sesto  comma,  del    &#13;
 decreto-legge n. 429 del 1982, convertito, con  modificazioni,  nella    &#13;
 legge  n.  516  del  1982, assume l'illegittimità della disposizione    &#13;
 impugnata, in quanto  la  stessa  discriminerebbe  coloro  che  hanno    &#13;
 sanato  in  sede  amministrativa  le pendenze tributarie in base alle    &#13;
 disposizioni di cui all'art. 8 del  decreto-legge  n.  83  del  1991,    &#13;
 senza  conseguire  il beneficio dell'estinzione del reato, rispetto a    &#13;
 coloro che, invece, avvalendosi delle disposizioni della legge n. 413    &#13;
 del 1991, hanno definito in epoca successiva dette pendenze,  potendo    &#13;
 fruire dell'amnistia prevista per l'appunto dalla norma impugnata.       &#13;
    2.  -  Va  premesso,  quanto  all'individuazione  dell'oggetto del    &#13;
 processo a quo, ai fini del controllo della rilevanza della  proposta    &#13;
 questione,  che può ritenersi sufficiente il richiamo dell'ordinanza    &#13;
 all'art.  1,  sesto  comma,  del  decreto-legge  n.  429  del   1982,    &#13;
 convertito, con modificazioni, nella legge n. 516 del 1982.              &#13;
    La  questione  è  da ritenere, tuttavia, inammissibile, in quanto    &#13;
 viene richiesto alla Corte un intervento additivo volto ad  estendere    &#13;
 la  portata dell'amnistia prevista dal d.P.R. 20 gennaio 1992, n. 23,    &#13;
 sì da ricomprendervi accanto alle pendenze tributarie  definite,  in    &#13;
 via amministrativa, alla stregua delle disposizioni di cui alla legge    &#13;
 n. 413 del 1991, anche quelle che avevano formato oggetto del condono    &#13;
 fiscale previsto dal precedente decreto-legge n. 83 del 1991.            &#13;
    In   proposito,   è   sufficiente   rammentare   il   consolidato    &#13;
 orientamento di questa Corte secondo il quale, in sede di giudizio di    &#13;
 legittimità costituzionale, non sono consentiti interventi volti  ad    &#13;
 ampliare  la  sfera  di applicabilità dei provvedimenti di amnistia,    &#13;
 dal momento che la determinazione di un siffatto ambito  è  rimessa,    &#13;
 sulla  base  del  precetto contenuto nell'art. 79 della Costituzione,    &#13;
 all'esclusiva competenza del legislatore (da ultimo, ordinanza n. 452    &#13;
 del 1993).                                                               &#13;
    Non va, d'altro canto, ignorata  l'altra  affermazione,  del  pari    &#13;
 desumibile  dalla  giurisprudenza  della  Corte,  secondo la quale la    &#13;
 differente disciplina delle situazioni tributarie esaurite,  rispetto    &#13;
 a  quelle  ancora  pendenti,  non  può, in linea generale, reputarsi    &#13;
 lesiva del  principio  di  eguaglianza  (sentenza  n.  32  del  1976;    &#13;
 ordinanza n. 539 del 1987).                                              &#13;
    3. - Le esposte considerazioni esimono la Corte dall'affrontare la    &#13;
 questione  prospettata nella memoria dell'Avvocatura dello Stato, con    &#13;
 conclusioni  peraltro  negative,  della  possibilità   o   meno   di    &#13;
 ricondurre la fattispecie all'esame del giudice a quo ad uno dei casi    &#13;
 contemplati  dalla legge successivamente emanata e, conseguentemente,    &#13;
 della facoltà o meno per il contribuente di avvalersi, nonostante la    &#13;
 definizione della pendenza ai sensi del precedente  decreto-legge  n.    &#13;
 83  del  1991,  del procedimento di condono fiscale e del conseguente    &#13;
 beneficio dell'amnistia, in base al combinato disposto del d.P.R.  20    &#13;
 gennaio 1992, n. 23 e della legge 30 dicembre 1991, n. 413. Trattasi,    &#13;
 infatti,  di  questione  di  carattere  interpretativo, che esula dal    &#13;
 thema decidendum e dal sindacato  di  costituzionalità  della  norma    &#13;
 impugnata,  nei  termini  in  cui  esso è stato rimesso all'esame di    &#13;
 questa Corte.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art.  1,  primo  comma,  del  d.P.R.  20  gennaio  1992,  n.  23    &#13;
 (Concessione   di   amnistia   per  reati  tributari)  sollevata,  in    &#13;
 riferimento  all'art.  3  della  Costituzione,  con  l'ordinanza   in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, 6 luglio 1994.                                   &#13;
                       Il Presidente: PESCATORE                           &#13;
                          Il redattore: VARI                              &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 15 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
