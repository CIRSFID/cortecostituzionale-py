<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>362</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:362</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Paolo Casavola</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/03/1988</data_decisione>
    <data_deposito>24/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 3 ultimo comma,    &#13;
 del   decreto-legge   15   dicembre   1979,   n.   629    ("Dilazione    &#13;
 dell'esecuzione  dei  provvedimenti  di  rilascio  per  gli  immobili    &#13;
 adibiti  ad  uso  di   abitazione   e   provvedimenti   urgenti   per    &#13;
 l'edilizia"),  come  modificato  dalla legge, 15 febbraio 1980, n. 25    &#13;
 ("Conversione in  legge,  con  modificazioni,  del  decreto-legge  15    &#13;
 dicembre  1979,  n.  629  concernente  dilazione  dell'esecuzione dei    &#13;
 provvedimenti di rilascio per gli immobili ad  uso  di  abitazione  e    &#13;
 provvedimenti urgenti per l'edilizia"), promosso con ordinanza emessa    &#13;
 il 28 luglio 1980 dal Pretore di Otranto,  iscritta  al  n.  662  del    &#13;
 registro  ordinanze  1980 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 311 dell'anno 1980;                                        &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 24 febbraio 1988 il Giudice    &#13;
 relatore Francesco Paolo Casavola;                                       &#13;
    Ritenuto che, nel corso di un procedimento per la fissazione della    &#13;
 data dell'esecuzione, relativamente  ad  uno  sfratto  per  morosità    &#13;
 adottato  ai  sensi della normativa vigente prima della legge n. 392,    &#13;
 del 1978 -  ed  esecutivo  al  momento  dell'entrata  in  vigore  del    &#13;
 decreto-legge  15  dicembre  1979, n. 629 ("Dilazione dell'esecuzione    &#13;
 dei provvedimenti di rilascio per gli  immobili  adibiti  ad  uso  di    &#13;
 abitazione  e provvedimenti urgenti per l'edilizia") - il conduttore,    &#13;
 comparso dinanzi al Pretore di Otranto,  aveva  sanato  la  morosità    &#13;
 all'udienza del 28 marzo 1980, ma il locatore aveva insistito perché    &#13;
 la esecuzione venisse fissata  ai  termini  dell'art.  1  del  citato    &#13;
 decreto-legge  629  del  1979,  così  come sostituito dalla legge 15    &#13;
 febbraio 1980, n. 25 (e cioè entro il 30 giugno 1980), in quanto  il    &#13;
 conduttore  non avrebbe potuto godere della dilazione compresa tra il    &#13;
 1° dicembre 1980 ed il 31 ottobre 1981, avendo  sanato  la  morosità    &#13;
 successivamente al 31 gennaio 1980;                                      &#13;
      che il giudice a quo, con ordinanza emessa il 28 giugno 1980, ha    &#13;
 sollevato, il riferimento dell'art. 3 della  Costituzione,  questione    &#13;
 di   legittimità  costituzionale  dell'art.  3,  ultimo  comma,  del    &#13;
 decreto-legge 15 dicembre 1979, n. 629, convertito, con modificazioni    &#13;
 in  legge  15  febbraio  1980, n. 25, nella parte in cui consente, in    &#13;
 pendenza di uno sfratto esecutivo, che  il  conduttore  moroso  possa    &#13;
 beneficiare  di  una fissazione della data dell'esecuzione successiva    &#13;
 al 30 giugno 1980, soltanto se abbia sanato la morosità entro il  31    &#13;
 gennaio  1980, nulla prevedendo in favore di chi dopo tale data abbia    &#13;
 effettuato la sanatoria;                                                 &#13;
      che  a  parere del pretore rimittente, l'omessa previsione di un    &#13;
 quaelsiasi beneficio per il conduttore che abbia sanato la  morosità    &#13;
 successivamente  al  31 gennaio 1980 (ma pur sempre prima della nuova    &#13;
 data  dell'esecuzione   fissata   dal   giudice)   ne   equiparerebbe    &#13;
 irrazionalmente  la  situazione  a quella del locatario che non abbia    &#13;
 affatto provveduto al pagamento di canoni e spese,  permanendo  nella    &#13;
 morosità;                                                               &#13;
      che  è  intevenuta  l'Avvocatura dello Stato, in rappresentanza    &#13;
 del Presidente del Consiglio dei Ministri, la quale ha  concluso  per    &#13;
 l'infondatezza della questione;                                          &#13;
    Considerato che la questione concerne un provvedimento di rilascio    &#13;
 divenuto esecutivo al 30 ottobre 1979 ed  a  suo  tempo  sospeso  per    &#13;
 effetto  del  decreto-legge 17 ottobre 1979, n. 505 (poi decaduto per    &#13;
 mancata conversione);                                                    &#13;
      che  il  citato decreto-legge, nell'accordare una dilazione sino    &#13;
 al 31 gennaio 1980, già prevedeva  il  beneficio  di  una  ulteriore    &#13;
 graduazione  per  gli sfratti fondati sulla morosità nel caso in cui    &#13;
 questa fosse stata sanata entro la predetta scadenza del  31  gennaio    &#13;
 1980;                                                                    &#13;
      che pertanto è del tutto razionale la scelta legislativa di non    &#13;
 consentire altri differimenti nel pagamento del debito da così lungo    &#13;
 tempo  scaduto, attesto che una diversa soluzione avrebbe legittimato    &#13;
 una morosità destinata a protrarsi ben oltre l'anzidetto termine;       &#13;
      che   peraltro   al   giudice,   nell'esercizio   della  propria    &#13;
 discrezionale  facoltà  di  fissare   l'esecuzione,   è   data   la    &#13;
 possibilità  di  differenziare le posizioni di chi non abbia affatto    &#13;
 pagato i canoni scaduti rispetto a colui che  abbia  invece,  seppure    &#13;
 tardivamente,  sanato  la  morosità, accordando dilazioni di diversa    &#13;
 durata entro la scadenza del 30 giugno 1980;                             &#13;
      che pertanto la proposta questione è manifestamente infondata;     &#13;
    Visti gli artt. 26, secondo comma, legge 11 marzo 1953, n. 87 e 9,    &#13;
 secondo comma, delle Norme integrative per  i  giudizi  davanti  alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale  dell'art.  3  ultimo  comma,  del  decreto-legge   15    &#13;
 dicembre  1979,  n. 629 ("Dilazione dell'esecuzione dei provvedimenti    &#13;
 di  rilascio  per  gli  immobili  adibiti  ad  uso  di  abitazione  e    &#13;
 provvedimenti    urgenti    per    l'edilizia"),    convertito,   con    &#13;
 modificazioni, in legge  15  febbraio  1980,  n.  25,  sollevata,  in    &#13;
 riferimento all'art. 3 della Costituzione, dal Pretore di Otranto con    &#13;
 l'ordinanza di cui in epigrafe.                                          &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte Costituzionale, Palazzo della Consulta, l'11 marzo 1988.           &#13;
                          Il Presidente: SAJA                             &#13;
                         Il redattore: CASAVOLA                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 24 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
