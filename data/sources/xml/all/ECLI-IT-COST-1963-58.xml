<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>58</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:58</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Biagio Petrocelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>27/04/1963</data_decisione>
    <data_deposito>03/05/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. GIOVANNI &#13;
 CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO &#13;
 SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti di legittimità costituzionale degli artt. 6,  &#13;
 7, 8 e 9 della legge 7 luglio 1901, n. 283, e  della  legge  28  giugno  &#13;
 1928, n. 1415, promossi con le sottoelencate ordinanze:                  &#13;
     1)  ordinanza  emessa  il  3  luglio 1961 dal Pretore di Casoli nel  &#13;
 procedimento civile  vertente  tra  Taraborrelli  Domenico  e  D'Orazio  &#13;
 Sergio,  iscritta  al  n.  120 del Registro ordinanze 1961 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 232 del 16 settembre 1961;  &#13;
     2) ordinanza emessa il 26 aprile 1962 dal giudice  conciliatore  di  &#13;
 Venezia nel procedimento civile vertente tra Marinello Bruno e Visnardi  &#13;
 Bruno,  iscritta  al  n.  121  del Registro ordinanze 1962 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 190 del 28 luglio 1962.     &#13;
     Udita nella camera di consiglio del 5 marzo 1963 la  relazione  del  &#13;
 Giudice Biagio Petrocelli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  procedimento  civile  in  materia  di  lavoro tra Taraborrelli  &#13;
 Domenico e D'Orazio Sergio, in corso dinanzi al Pretore di  Casoli,  il  &#13;
 procuratore   del   convenuto   sollevava   questione  di  legittimità  &#13;
 costituzionale degli artt.  6, 7, 8 e 9 della legge 7 luglio  1901,  n.  &#13;
 283,  e della intera legge 28 giugno 1928, n. 1415, in riferimento allo  &#13;
 art. 33, quinto comma, della Costituzione.                               &#13;
     Con ordinanza del 3 luglio 1961 il Pretore  riteneva  la  questione  &#13;
 rilevante  ai  fini  del  giudizio  e  non  manifestamente infondata, e  &#13;
 disponeva la trasmissione degli atti a questa Corte.                     &#13;
     Nella ordinanza si osserva che le norme impugnate, in virtù  delle  &#13;
 quali  persone  in  possesso del solo diploma di scuola media superiore  &#13;
 possono essere autorizzate ad esercitare  il  patrocinio  legale  nelle  &#13;
 Preture,  violano  il  principio  stabilito dall'art. 33, quinto comma,  &#13;
 della Costituzione, secondo il quale "non è consentito l'esercizio  di  &#13;
 alcuna attività professionale senza l'esame statale di abilitazione".   &#13;
     L'ordinanza,   regolarmente   notificata  e  comunicata,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica del  16  settembre  &#13;
 1961, n. 232.                                                            &#13;
     Analoga  questione  veniva  sollevata  di ufficio, nel procedimento  &#13;
 civile tra Marinello Bruno e Visnardi Bruno, dal  giudice  conciliatore  &#13;
 di  Venezia, con ordinanza del 26 aprile 1962, regolar mente notificata  &#13;
 e comunicata e pubblicata sulla Gazzetta Ufficiale n. 190 del 28 luglio  &#13;
 1962.                                                                    &#13;
     Non vi è stata costituzione di parti.<diritto>Considerato in diritto</diritto>:                          &#13;
     Le due questioni, data l'identità dell'oggetto, vanno  decise  con  &#13;
 unica   sentenza.   Le   norme  impugnate  riguardano  l'esercizio  del  &#13;
 patrocinio presso le Preture e presso gli uffici  di  conciliazione  da  &#13;
 parte   di  soggetti  che,  pur  non  essendo  avvocati  o  procuratori  &#13;
 esercenti, si trovino in  possesso  di  determinati  requisiti  (notai,  &#13;
 laureati in legge, studenti universitari che abbiano superato gli esami  &#13;
 in  talune discipline); ovvero, essendo in possesso di requisiti minori  &#13;
 (licenza liceale,  di  istituto  tecnico,  licenza  normale  superiore,  &#13;
 diploma   di  segretario  comunale,  ex  funzionari  di  cancellerie  e  &#13;
 segreterie   giudiziarie),   abbiano   conseguita   l'abilitazione   al  &#13;
 patrocinio  nei  modi prescritti dall'art. 7 della legge 7 luglio 1901,  &#13;
 n. 283.                                                                  &#13;
     La Corte ritiene infondata la proposta  questione  di  legittimità  &#13;
 costituzionale. Le disposizioni impugnate, nell'ammettere al patrocinio  &#13;
 davanti  alle  Preture  o agli uffici di conciliazione i soggetti sopra  &#13;
 indicati, alle condizioni prevedute dagli artt.  6,  7,  8  e  9  della  &#13;
 citata  legge  7 luglio 1901 e dalla legge 28 giugno 1928, n. 1415, non  &#13;
 sono in contrasto con l'art. 33, quinto comma, della Costituzione. Esse  &#13;
 riguardano  forme particolari di patrocinio, relative a giudizi davanti  &#13;
 ai conciliatori, e, a date condizioni, anche davanti  ai  Pretori,  nei  &#13;
 quali  è  consentito  che  anche  le stesse parti possono direttamente  &#13;
 sostenere le proprie ragioni, e per cui non è richiesta  una  speciale  &#13;
 preparazione  tecnica. Tali forme di patrocinio possono ragionevolmente  &#13;
 ritenersi non incluse fra quelle per cui l'art. 33  della  Costituzione  &#13;
 prevede l'esame di Stato.                                                &#13;
     Le  stesse  considerazioni possono valere, analogamente, per quella  &#13;
 special forma  di  patrocinio  consentita,  davanti  alle  Preture  del  &#13;
 distretto  di Corte di appello, ai laureati in giurisprudenza che siano  &#13;
 praticanti procuratori (art. 8 del R.D.L. 27 novembre 1933,  n.  1578).  &#13;
 Per   questa  categoria  vale  anche  la  considerazione  che  trattasi  &#13;
 evidentemente di un'attività preparatoria e di tirocinio  che  precede  &#13;
 il vero e proprio esercizio professionale.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     decidendo sui due giudizi riuniti di cui in epigrafe,                &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 degli artt. 6,7,8 e 9 della legge 7 luglio 1901, n. 283, e della  legge  &#13;
 28  giugno  1928,  n.  1415,  proposta  con le ordinanze del Pretore di  &#13;
 Casoli e del conciliatore  di  Venezia,  in  riferimento  all'art.  33,  &#13;
 quinto comma, della Costituzione.                                        &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 27 aprile 1963.         &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO - ANTONINO PAPALDO -  GIOVANNI  &#13;
                                   CASSANDRO   -   BIAGIO  PETROCELLI  -  &#13;
                                   ANTONIO  MANCA  -  ALDO  SANDULLI   -  &#13;
                                   GIUSEPPE  BRANCA  - MICHELE FRAGALI -  &#13;
                                   COSTANTINO     MORTATI-      GIUSEPPE  &#13;
                                   CHIARELLI - GIUSEPPE  VERZÌ.</dispositivo>
  </pronuncia_testo>
</pronuncia>
