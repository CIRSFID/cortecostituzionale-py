<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>50</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:50</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/02/2001</data_decisione>
    <data_deposito>06/03/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Riccardo CHIEPPA, Gustavo &#13;
ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido &#13;
NEPPI MODONA, Annibale MARINI, Franco BILE Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 72,  terzo &#13;
comma,   del   regio  decreto  30 gennaio  1941,  n. 12  (Ordinamento &#13;
giudiziario),  promosso  con  ordinanza  emessa  il 1° marzo 2000 dal &#13;
tribunale  di  Pisa, iscritta al n. 343 del registro ordinanze 2000 e &#13;
pubblicata nella Gazzetta Ufficiale della Repubblica n. 26 - 1ª serie &#13;
speciale - dell'anno 2000. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 24 gennaio 2001 il giudice &#13;
relatore Valerio Onida. &#13;
    Ritenuto  che, con ordinanza emessa il 1° marzo 2000, pervenuta a &#13;
questa  Corte il 22 maggio 2000, il tribunale di Pisa in composizione &#13;
monocratica, nel corso di un dibattimento penale in cui l'imputato è &#13;
chiamato a rispondere di maltrattamenti in famiglia o verso fanciulli &#13;
(art. 572  cod. pen.), rilevato che le funzioni di pubblico ministero &#13;
all'udienza  erano state delegate dal Procuratore della Repubblica ad &#13;
un ufficiale di polizia giudiziaria, ha sollevato d'ufficio questione &#13;
di  legittimità costituzionale, in riferimento agli articoli 111 e 3 &#13;
della  Costituzione,  dell'art. 72,  terzo  comma,  del regio decreto &#13;
30 gennaio   1941,   n. 12  (Ordinamento  giudiziario)  -  nel  testo &#13;
modificato,  da  ultimo,  dall'art. 58  della legge 16 dicembre 1999, &#13;
n. 479  -,  il quale, nell'ambito della disciplina della delega delle &#13;
funzioni  del  pubblico  ministero  nei  procedimenti  nei  quali  il &#13;
tribunale  giudica  in  composizione  monocratica, prevede che "nella &#13;
materia  penale,  è  seguito altresì il criterio di non delegare le &#13;
funzioni  del pubblico ministero in relazione a procedimenti relativi &#13;
a  reati diversi da quelli per cui si procede con citazione diretta a &#13;
giudizio   secondo   quanto  previsto  dall'art. 550  del  codice  di &#13;
procedura penale"; &#13;
        che  il remittente ricorda come, secondo "una interpretazione &#13;
corrente",   detta   prescrizione  venga  intesa  nel  senso  che  la &#13;
violazione  dell'indicato  criterio  non  vale  a privare il delegato &#13;
della  legittimazione  al  compimento  degli  atti  e ad inficiare di &#13;
nullità  il  procedimento nel quale egli abbia svolto le funzioni di &#13;
pubblico ministero; &#13;
        che  ne  conseguirebbe  che,  in  un processo per un reato di &#13;
rilevante  gravità,  che presenta particolari profili di difficoltà &#13;
di  acquisizione  e  valutazione  della  prova  e  di  qualificazione &#13;
giuridica  della  condotta, quale quello in corso davanti allo stesso &#13;
giudice  a  quo le funzioni di pubblico ministero sarebbero svolte da &#13;
un  ufficiale  di  polizia giudiziaria, senza che sia consentito alle &#13;
altre  parti  di  prospettare,  e  al giudice di adottare, un rimedio &#13;
processuale   che  garantisca  la  presenza  in  dibattimento  di  un &#13;
magistrato di carriera; &#13;
        che  la  norma  in  questione  sarebbe,  in  primo  luogo, in &#13;
contrasto   con   l'art. 111   della  Costituzione,  in  quanto,  nei &#13;
procedimenti  per reati più gravi e di complesso accertamento (quali &#13;
sarebbero  quelli che non ammettono la citazione diretta a giudizio), &#13;
non   sarebbe   garantita   la   partecipazione,   nella   veste   di &#13;
rappresentante  della pubblica accusa, di un soggetto qualificato per &#13;
professionalità  e  indipendenza,  onde  il  pubblico  ministero  di &#13;
udienza  verserebbe  in  condizione  minoritaria  rispetto alle altre &#13;
parti,  in  contrasto  con  il  principio  di parità delle parti nel &#13;
contraddittorio; &#13;
        che,  inoltre,  sarebbe  violato l'art. 3 della Costituzione, &#13;
per  la  disparità di trattamento che si verificherebbe, per effetto &#13;
della  valutazione  meramente  discrezionale  del  procuratore  della &#13;
Repubblica delegante, nei procedimenti per reati che non ammettono la &#13;
citazione diretta, a seconda che sia o meno esercitata la facoltà di &#13;
delega,  restando  così  condizionata  in  modo arbitrario la tutela &#13;
dell'interesse  della  parte  offesa  a  che  il ruolo della pubblica &#13;
accusa  sia  esercitato  in  condizioni di sostanziale parità con la &#13;
difesa dell'imputato; &#13;
        che  è  intervenuto  in giudizio il Presidente del Consiglio &#13;
dei  ministri,  concludendo  per  l'inammissibilità,  e comunque per &#13;
l'infondatezza, della questione. &#13;
    Considerato  che  il remittente non censura la previsione, di per &#13;
sé,   della  possibilità  di  delegare  le  funzioni  del  pubblico &#13;
ministero  in dibattimento ad un ufficiale di polizia giudiziaria, ma &#13;
lamenta  che  la  norma denunciata non ponga una preclusione assoluta &#13;
alla  delegabilità  di  tali  funzioni  nel caso di reati diversi da &#13;
quelli per i quali si procede con citazione diretta a giudizio; &#13;
        che,  posta  l'ammissibilità  della  delega,  rientra  nella &#13;
discrezionale  valutazione del legislatore la scelta dei procedimenti &#13;
per   i   quali   ammetterla,  e  quindi  anche  la  scelta,  per  la &#13;
individuazione  dei  procedimenti nei quali può essere esercitata la &#13;
delega    stessa,   di   un   criterio   di   preclusione   assoluta, &#13;
processualmente  sanzionato,  ovvero  di  un criterio più elastico e &#13;
derogabile,  il  cui  eventuale  cattivo uso da parte del procuratore &#13;
della Repubblica delegante resti privo di conseguenze processuali; &#13;
        che,  in  ogni  caso, non ha fondamento l'assunto secondo cui &#13;
l'utilizzo  da  parte  del  titolare  della funzione requirente della &#13;
facoltà  di  delega  prevista  dalla  legge  - utilizzo naturalmente &#13;
vincolato,  oltre  che  alle  condizioni  prescritte, alla necessità &#13;
della prudente valutazione, da parte del delegante, delle circostanze &#13;
del   caso   concreto   -   potrebbe  dar  luogo  ad  un  difetto  di &#13;
contraddittorio  per  inidoneità del pubblico ministero delegato, in &#13;
contrasto con l'art. 111 della Costituzione; &#13;
        che,  infatti,  il  principio  della parità fra le parti nel &#13;
contraddittorio  è  rispettato  quando  siano  assolte le condizioni &#13;
volute dalla legge per l'investitura e per l'esercizio della funzione &#13;
requirente;  mentre  l'eventuale  difetto di idoneità in concreto da &#13;
parte  di chi sia stato delegato è circostanza di mero fatto che non &#13;
può  riflettersi  sulla  legittimità  della  norma  che  prevede la &#13;
facoltà di delega; &#13;
        che,  parimenti,  non  è  fondata  la  censura di violazione &#13;
dell'art. 3   della  Costituzione,  per  disparità  di  trattamento, &#13;
poiché  il principio di eguaglianza non esclude affatto, di per sé, &#13;
la  possibilità  che la legge attribuisca poteri discrezionali, come &#13;
quello,  in esame, di delega delle funzioni requirenti, poteri il cui &#13;
esercizio,  nei  limiti  e  alle  condizioni  prescritte dalla stessa &#13;
legge, non si risolve in disparità di trattamento, ma dà luogo solo &#13;
a  diverse situazioni di fatto correlate alle valutazioni compiute in &#13;
concreto dal titolare di detti poteri; &#13;
        che,  d'altra parte, l'eventuale uso scorretto o inopportuno, &#13;
da  parte  del  titolare  dell'ufficio  del pubblico ministero, della &#13;
discrezionalità  insita nella facoltà di delega, pur quando non dia &#13;
luogo  a  rimedi nell'ambito del singolo processo, si risolverebbe in &#13;
disfunzione  dell'ufficio medesimo, suscettibile di essere rilevata e &#13;
corretta   attraverso   gli  appropriati  strumenti  organizzativi  e &#13;
disciplinari; &#13;
        che  pertanto  la  questione, così come sollevata, si palesa &#13;
manifestamente infondata sotto ogni profilo. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta   infondatezza   della   questione   di &#13;
legittimità  costituzionale  dell'art. 72,  terzo  comma,  del regio &#13;
decreto  30 gennaio 1941, n. 12 (Ordinamento giudiziario), sollevata, &#13;
in  riferimento  agli  articoli  111  e  3  della  Costituzione,  dal &#13;
tribunale di Pisa con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 21 febbraio 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Onida &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 6 marzo 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
