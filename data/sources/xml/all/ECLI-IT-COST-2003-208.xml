<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>208</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:208</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia>Guido Neppi Modona</redattore_pronuncia>
    <data_decisione>03/06/2003</data_decisione>
    <data_deposito>11/06/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 32, comma 2, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), promosso, nell'ambito di un procedimento penale, dal Giudice dell'udienza preliminare del Tribunale per i minorenni di Catania con ordinanza del 18 marzo 2002, iscritta al n. 466 del registro ordinanze 2002 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 43, prima serie speciale, dell'anno 2002. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 7 maggio 2003 il Giudice relatore Guido Neppi Modona. &#13;
    Ritenuto che il Giudice dell'udienza preliminare del Tribunale per i minorenni di Catania ha sollevato, in riferimento all'art. 111, commi quarto e quinto, della Costituzione (commi non indicati nel dispositivo, ma espressamente menzionati in motivazione), questione di legittimità costituzionale dell'art. 32, comma 2, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), «nella parte in cui non subordina al consenso dell'imputato la sentenza di condanna a pena pecuniaria o ad una sanzione sostitutiva»; &#13;
    che il rimettente premette che nel corso dell'udienza preliminare, sentito l'imputato, il pubblico ministero ne aveva chiesto la condanna a pena pecuniaria per i reati di getto pericoloso di cose e rifiuto di indicazioni sulla propria identità personale, invitando il giudice a valutare se sollevare questione di legittimità costituzionale della norma anzidetta nei termini sopra esposti;  &#13;
    che il giudice a quo ritiene che la disposizione censurata permette di pronunciare sentenza di condanna in assenza del consenso dell'imputato alla definizione del processo nella fase dell'udienza preliminare; &#13;
    che la norma si porrebbe in contrasto con i commi quarto e quinto dell'art. 111 Cost., che assicurano all'imputato il diritto ad essere giudicato sulla base di prove formatesi nel contraddittorio tra le parti, demandando alla legge di regolare i casi in cui, per consenso dell'imputato, si può derogare a tale principio; &#13;
    che il rimettente rileva inoltre che nel procedimento minorile sono previste ipotesi di definizione del procedimento in udienza preliminare, quali le sentenze di concessione del perdono giudiziale o di non luogo a procedere per irrilevanza del fatto, che presuppongono un «giudizio di responsabilità» e che - a seguito delle modifiche recate in attuazione del nuovo art. 111 Cost. all'art. 32, comma 1, del d.P.R. n. 448 del 1988 - possono essere pronunciate soltanto se la persona imputata ha prestato il proprio consenso «ad essere giudicata allo stato degli atti»; &#13;
    che «inspiegabilmente» il legislatore non ha invece esteso la necessità del consenso del minorenne al comma 2 dell'art. 32, che contempla una «ipotesi di definibilità del giudizio in malam partem», mantenendo inalterata la possibilità di addivenire ad una condanna a pena pecuniaria o a sanzione sostitutiva a prescindere «dalla volontà dell'imputato, se non, addirittura, col dissenso esplicito di quest'ultimo»; &#13;
    che è intervenuto nel giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione sia dichiarata inammissibile o comunque infondata; &#13;
    che ad avviso dell'Avvocatura la disposizione censurata, prevedendo, in particolare, una diminuzione della pena sino alla metà del minimo edittale, delinea, sia pure con aspetti peculiari, uno schema di definizione del procedimento analogo a quello del decreto penale di condanna, la cui applicazione è preclusa nel processo minorile (art. 25 del d.P.R n. 448 del 1988); &#13;
    che la normativa in esame si giustificherebbe perciò con l'esigenza di «non prevedere per il minorenne un trattamento deteriore rispetto al maggiorenne nel caso in cui si proceda per reati che, per il maggiorenne, consentirebbero la definizione con decreto penale di condanna». &#13;
    Considerato che il rimettente dubita della legittimità costituzionale dell'art. 32, comma 2, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), in riferimento all'art. 111, commi quarto e quinto, della Costituzione, nella parte in cui non richiede il consenso dell'imputato per la pronuncia della sentenza di condanna a pena pecuniaria o ad una sanzione sostitutiva; consenso richiesto, invece, dal comma 1 del medesimo art. 32 per la sentenza di non luogo a procedere;  &#13;
    che, per quanto interessa la questione oggetto del presente giudizio, il comma 1 dell'art. 32, come sostituito dall'art. 22 della legge 1° marzo 2001, n. 63, prevede, nel primo periodo, che nell'udienza preliminare il giudice debba chiedere all'imputato se consente alla definizione del processo in quella stessa fase e, nel secondo periodo, che, ove il consenso venga prestato, il giudice possa pronunciare sentenza di non luogo a procedere nei casi di cui all'art. 425 cod. proc. pen., ovvero per concessione del perdono giudiziale o per irrilevanza del fatto; &#13;
    che, prima di tale modifica, l'art. 1, comma 5, del decreto-legge 7 gennaio 2000, n. 2, nel testo risultante dalla legge di conversione 25 febbraio 2000, n. 35, aveva previsto, a titolo di attuazione transitoria dell'art. 111 Cost., che "nell'udienza preliminare dei processi penali in corso nei confronti di imputato minorenne, il giudice, se ritiene di poter decidere allo stato degli atti, informa l'imputato della possibilità di consentire che il procedimento a suo carico sia definito in quella fase"; &#13;
    che la disciplina transitoria, destinata ad incidere, sia per il suo contenuto che per la sua collocazione, su qualsiasi forma di definizione dell'udienza preliminare, risulta pertanto diversa rispetto alla disciplina a regime, che sembrerebbe riguardare le sole ipotesi di sentenza di non luogo a procedere elencate nell'art. 32, comma 1; &#13;
    che peraltro dai lavori preparatori della legge n. 63 del 2001 emerge la mera intenzione di «trasformare in norma a regime la norma transitoria prevista nel decreto-legge 7 gennaio 2000, n. 2, convertito in legge dalla legge 25 febbraio 2000 n. 35, relativa al procedimento minorile» (cfr. Camera dei deputati - XIII legislatura - Resoconto della II Commissione permanente in sede referente, seduta del 5 ottobre 2000: illustrazione dell'emendamento 18.06 al d.d.l. C. 6590); &#13;
    che, a fronte dell'iter legislativo sommariamente richiamato, il tenore testuale del primo periodo del comma 1 dell'art. 32 del d.P.R. n. 448 del 1988, unitamente alla ratio della norma, indubbiamente finalizzata a riconoscere al minorenne la facoltà di non prestare il consenso alla pronuncia in udienza preliminare di sentenze che comunque presuppongono un accertamento di responsabilità (cfr. sentenza n. 195 del 2002), permetterebbe di ritenere, secondo larga parte degli interpreti, che il consenso vada riferito in via generale alla possibilità di definire il processo nell'udienza preliminare e non ad uno specifico esito dell'udienza stessa; &#13;
    che il rimettente omette qualsiasi motivazione circa l'impossibilità di seguire una interpretazione idonea ad attribuire alla norma censurata il significato che egli ritiene conforme a Costituzione; &#13;
    che la questione va pertanto dichiarata manifestamente inammissibile. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, secondo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 32, comma 2, del d.P.R. 22 settembre 1988, n. 448 (Approvazione delle disposizioni sul processo penale a carico di imputati minorenni), sollevata, in riferimento all'art. 111, commi quarto e quinto, della Costituzione, dal Giudice dell'udienza preliminare del Tribunale per i minorenni di Catania, con l'ordinanza in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 giugno 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Guido NEPPI MODONA, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria l'11  giugno 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
