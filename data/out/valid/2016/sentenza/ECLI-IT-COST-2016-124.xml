<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-01/124/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-01/124"/>
          <FRBRalias value="ECLI:IT:COST:2016:124" name="ECLI"/>
          <FRBRdate date="2016-06-01" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="124"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-01/124/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-01/124/ita@"/>
          <FRBRdate date="2016-06-01" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-01/124/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/2016-06-01/124/ita@.xml"/>
          <FRBRdate date="2016-06-01" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2016-04-05" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2016-06-01" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="GROSSI" href="/akn/ontology/person/it/GROSSI" showAs="GROSSI"/>
        <TLCPerson eId="giuseppeFrigo" href="/akn/ontology/person/it/giuseppeFrigo" showAs="Giuseppe FRIGO"/>
        <TLCPerson eId="alessandroCriscuolo" href="/akn/ontology/person/it/alessandroCriscuolo" showAs="Alessandro CRISCUOLO"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="paoloGrossi" href="/akn/ontology/person/it/paoloGrossi" showAs="Paolo GROSSI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2016</akn4cc:anno>
        <akn4cc:numero>124</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2016:124</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>GROSSI</akn4cc:presidente>
        <akn4cc:relatore>Giorgio Lattanzi</akn4cc:relatore>
        <akn4cc:data_decisione>2016-04-05</akn4cc:data_decisione>
        <akn4cc:data_deposito>2016-06-01</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>illegittimità costituzionale</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#paoloGrossi">Paolo GROSSI</judge>; Giudici : <judge refersTo="#giuseppeFrigo">Giuseppe FRIGO</judge>, <judge refersTo="#alessandroCriscuolo">Alessandro CRISCUOLO</judge>, <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>, <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'art. 3, comma 1, della legge della Regione Toscana 30 dicembre 2014, n. 88, recante «Modifiche alla legge regionale 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio"). Disposizioni in materia di ambiti territoriali di caccia», promosso dal Presidente del Consiglio dei ministri, con ricorso notificato il 26 febbraio-2 marzo 2015, depositato in cancelleria il 3 marzo 2015 ed iscritto al n. 29 del registro ricorsi 2015.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Toscana;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito nell'udienza pubblica del 5 aprile 2016 il Giudice relatore Giorgio Lattanzi;
 uditi l'avvocato dello Stato Marco Corsini per il Presidente del Consiglio dei ministri e l'avvocato Stefano Crisci per la Regione Toscana.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Con ricorso spedito per la notificazione il 26 febbraio 2015, ricevuto il successivo 2 marzo e depositato il 3 marzo 2015 (reg. ric. n. 29 del 2015), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questione di legittimità costituzionale dell'art. 3, comma 1, della legge della Regione Toscana 30 dicembre 2014, n. 88, recante «Modifiche alla legge regionale 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio"). Disposizioni in materia di ambiti territoriali di caccia», in riferimento all'art. 117, secondo comma, lettera s), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 La disposizione impugnata sostituisce l'art. 11 della legge della Regione Toscana 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio"), che disciplina gli ambiti territoriali di caccia.
 In particolare, il nuovo testo della disposizione sostituita stabilisce che gli ambiti territoriali di caccia sono nove, con confini corrispondenti a quelli delle Province e l'accorpamento delle Province di Firenze e Prato in un unico ambito (art. 11, comma 2, della legge regionale n. 3 del 1994, come introdotto dalla disposizione censurata).
 Inoltre con il piano faunistico venatorio possono essere istituiti dei sottoambiti, privi di organi, per garantire una zonizzazione il più possibile omogenea e rispondente alle caratteristiche dei singoli contesti territoriali (art. 11, comma 3, della medesima legge regionale).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il ricorrente ritiene lesiva della competenza esclusiva dello Stato in materia di «tutela dell'ambiente e dell'ecosistema» (art. 117, secondo comma, lettera s, Cost.) l'attribuzione agli ambiti di una dimensione provinciale, in contrasto con quanto previsto dall'<ref href="/akn/it/act/legge/stato/1992-02-11/157/!main#art_14__para_1">art. 14, comma 1, della legge 11 febbraio 1992, n. 157</ref> (Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio), secondo il quale
 le Regioni ripartiscono il territorio destinato alla caccia in ambiti territoriali di dimensioni subprovinciali, possibilmente omogenei e delimitati da confini naturali.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Questa Corte avrebbe già ritenuto che l'<ref href="/akn/it/act/legge/stato/1992/157/!main#art_14">art. 14 della legge n. 157 del 1992</ref>, per tale parte, indichi uno standard minimo di tutela della fauna, cui le Regioni non possono derogare. Sarebbe infatti in gioco la necessità di suddividere il territorio aperto alla caccia in ambiti omogenei, al fine di valorizzare il ruolo delle comunità ivi insediate e di costituire aree dai confini naturali, anziché soltanto amministrativi.
 2.- Si è costituita in giudizio la Regione Toscana, chiedendo che il ricorso sia dichiarato inammissibile e non fondato, ove non venga dichiarata la cessazione della materia del contendere.
 A questo proposito, la Regione rileva che la disposizione impugnata è stata modificata dall'art. 1, commi 1 e 2, della legge della Regione Toscana 20 marzo 2015, n. 32, recante «Modifiche alla legge regionale 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio")».
 Nel corpo dell'art. 11, comma 2, della legge regionale n. 3 del 1994 si è aggiunto che la ripartizione in nove ambiti, coincidenti con il territorio delle Province, è disposta «ai soli fini della organizzazione amministrativa».
 Nell'art. 11, comma 3, le parole «possono essere istituiti sottoambiti» sono state sostituite dalle parole «sono istituiti sottoambiti», a significare che tale istituzione è doverosa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Lo ius superveniens determinerebbe la cessazione della materia del contendere, posto che la disposizione impugnata non ha avuto applicazione nel testo originario.
 In ogni caso, la previsione di sottoambiti renderebbe infondata la censura.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p>- Il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso questione di legittimità costituzionale dell'art. 3, comma 1, della legge della Regione Toscana 30 dicembre 2014, n. 88, recante «Modifiche alla legge regionale 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio"). Disposizioni in materia di ambiti territoriali di caccia», in  riferimento all'art. 117, secondo comma, lettera s), della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>.
 La disposizione impugnata sostituisce l'art. 11 della legge della Regione Toscana 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio"), che disciplina gli ambiti territoriali di caccia.
 Nel testo vigente fino a tale sostituzione, l'art. 11, comma 2, della legge regionale n. 3 del 1994 prevedeva che gli ambiti territoriali di caccia avessero «dimensioni subprovinciali», in conformità all'<ref href="/akn/it/act/legge/stato/1992-02-11/157/!main#art_14__para_1">art. 14, comma 1, della legge 11 febbraio 1992, n. 157</ref> (Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio).
 L'art. 3, comma 1, impugnato, introduce un nuovo testo dell'art. 11 della legge regionale n. 3 del 1994.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il comma 2 dell'art. 11 stabilisce che in Toscana vi sono nove ambiti territoriali di caccia, aventi i medesimi confini delle Province, con l'eccezione delle Province di Firenze e Prato che formano un unico ambito.
 Il comma 3 seguente aggiunge che con il piano faunistico venatorio «possono essere istituiti dei sottoambiti», privi di organi.
 Il ricorrente reputa tali previsioni in contrasto con la dimensione subprovinciale degli ambiti prevista dall'<ref href="/akn/it/act/legge/stato/1992/157/!main#art_14__para_1">art. 14, comma 1, della legge n. 157 del 1992</ref>, e, conseguentemente, con la competenza esclusiva dello Stato in materia di «tutela dell'ambiente e dell'ecosistema» (art. 117, secondo comma, lettera s, Cost.), di cui la prima disposizione sarebbe espressiva.
 2.- Il ricorso contiene l'impugnazione dell'art. 3, comma 1, nella sua interezza, ovvero con riferimento anche agli ulteriori commi di cui si compone il nuovo testo dell'art. 11 della legge regionale n. 3 del 1994; tuttavia, le censure, in conformità alla delibera di autorizzazione del Consiglio dei ministri, vertono esclusivamente sui commi 2 e 3, e a questi perciò il ricorso in via interpretativa deve ritenersi univocamente circoscritto.
 3.- Nelle more del giudizio, è sopraggiunto l'art. 1, commi 1 e 2, della legge della Regione Toscana 20 marzo 2015, n. 32, recante «Modifiche alla legge regionale 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio")».
 Il comma 1 di tale articolo modifica l'art. 11, comma 2, della legge regionale n. 3 del 1994, stabilendo che gli ambiti territoriali di caccia sono nove e corrispondono al territorio delle Province «ai soli fini della organizzazione amministrativa».</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Il comma 2, invece, incide sull'art. 11, comma 3, della medesima legge regionale, prevedendo che, in seno agli ambiti, «sono istituiti sottoambiti», mentre precedentemente la disposizione stabiliva che «possono essere istituiti dei sottoambiti».
 La Regione Toscana, nel costituirsi, ha sostenuto che lo ius superveniens implica la cessazione della materia del contendere, perché il sottoambito è divenuto obbligatorio e viene modellato secondo le «peculiarità ambientali, naturalistiche e faunistiche» del territorio, mentre l'ambito risponde a fini di mera organizzazione amministrativa.
 4.- L'eccezione concernente la cessazione della materia del contendere non è fondata, dato che la modifica normativa non ha carattere satisfattivo.
 L'<ref href="/akn/it/act/legge/stato/1992/157/!main#art_14">art. 14 della legge n. 157 del 1992</ref>, indicato dall'Avvocatura generale dello Stato quale norma interposta, e dedicato alla gestione programmata della caccia sul territorio agro-silvo-pastorale, collega direttamente la ripartizione territoriale in ambiti di dimensioni subprovinciali alla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>, presso questi ultimi, di organi direttivi, cui sono conferiti rilevanti compiti gestionali. Ciò è stato fatto allo scopo di realizzare uno stretto vincolo tra il cacciatore ed il territorio, «valorizzando, al tempo stesso, il ruolo della comunità che, in quel territorio, è insediata e che è primariamente chiamata, attraverso gli organi direttivi degli ambiti», ad occuparsi delle risorse faunistiche (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2000/4/!main">sentenza n. 4 del 2000</ref>; inoltre, <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/2001/299/!main">ordinanza n. 299 del 2001</ref>).
 È perciò evidente che la dimensione subprovinciale degli ambiti, nel disegno del legislatore statale, chiama necessariamente in causa proprio l'organizzazione amministrativa, sicché avere limitato a quest'ultima la funzione degli ambiti provinciali non risolve affatto la questione proposta dal ricorrente. Per di più, i sottoambiti toscani «sono privi di organi» (art. 11, comma 3, della legge regionale n. 3 del 1994) e non possono quindi in alcun modo svolgere i compiti propri degli ambiti territoriali di caccia con dimensione subprovinciale.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Ciò posto, e alla luce della portata precettiva e lesiva sostanzialmente equivalente tra la norma impugnata e lo ius superveniens, la questione di legittimità costituzionale va trasferita sul nuovo testo dell'art. 11, commi 2 e 3, della legge regionale n. 3 del 1994 (ex plurim<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2016/40/!main">is, sentenza n. 40 del 2016</ref>).
 5.- La questione è fondata.
 Questa Corte ha ripetutamente riconosciuto che la <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> degli ambiti territoriali di caccia, prevista dall'<ref href="/akn/it/act/legge/stato/1992/157/!main#art_14">art. 14 della legge n. 157 del 1992</ref>, manifesta uno standard inderogabile di tutela dell'ambiente e dell'ecosistema, con riferimento sia alla dimensione subprovinciale dell'ambito (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2013/142/!main">sentenze n. 142 del 2013</ref> e n. 4 del 2000), sia alla composizione degli organi direttivi (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2010/268/!main">sentenze n. 268 del 2010</ref> e n. 165 del 2009).
 Infatti, «il legislatore statale ha voluto, attraverso la ridotta dimensione degli ambiti stessi, pervenire ad una più equilibrata distribuzione dei cacciatori sul territorio» e «conferire specifico rilievo [...] alla dimensione della comunità locale, più ristretta e più legata sotto il profilo storico e ambientale alle particolarità del territorio» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2013/142/!main">sentenza n. 142 del 2013</ref>).
 Il carattere provinciale dell'ambito voluto invece dal legislatore toscano, al quale si lega l'istituzione di sottoambiti privi di funzioni amministrative, tradisce questa finalità. Esso diluisce, infatti, una sfera di interessi (connessi alla caccia e alla tutela dell'ambiente: <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2000/4/!main">sentenza n. 4 del 2000</ref>), incentrata sul territorio locale, nella ripartizione per Province, ove la dimensione territoriale implica più ampie, e meno specifiche, esigenze di decentramento amministrativo.
 Deve pertanto dichiararsi l'illegittimità costituzionale dell'art. 11, commi 2 e 3, della legge della Regione Toscana n. 3 del 1994, nel testo modificato dall'art. 1, commi 1 e 2, della legge regionale n. 32 del 2015.</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara l'illegittimità costituzionale dell'art. 11, commi 2 e 3, della legge della Regione Toscana 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio"), nel testo modificato dall'art. 1, commi 1 e 2, della legge della Regione Toscana 20 marzo 2015, n. 32, recante «Modifiche alla legge regionale 12 gennaio 1994, n. 3 (Recepimento della <ref href="/akn/it/act/legge/stato/1992-02-11/157/!main">legge 11 febbraio 1992, n. 157</ref> "Norme per la protezione della fauna selvatica omeoterma e per il prelievo venatorio")», previo trasferimento della questione di legittimità costituzionale sulle nuove disposizioni.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2016-04-05">5 aprile 2016</date>.
 F.to:
 <signature><judge refersTo="#paoloGrossi" as="#presidente">Paolo GROSSI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#giorgioLattanzi" as="#relatore">Giorgio LATTANZI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria l'<date refersTo="#publicationDate" date="2016-06-01">1 giugno 2016</date>.
 Il Cancelliere
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
