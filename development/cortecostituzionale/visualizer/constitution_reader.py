import json


def is_dict(element):
    return isinstance(element, dict)


def get_article_list(json_tree):
    article_list = []
    if not is_dict(json_tree) or 'c' not in json_tree:
        return article_list
    for c in json_tree['c']:
        if c.get('tag', None) == 'article':
            article_list.append(c)
        else:
            article_list.extend(get_article_list(c))
    return article_list


def get_date_dict(date_str):
    if date_str is None:
        return None
    year, month, day = date_str.split('-')
    return {
        'year': year,
        'month': month,
        'day': day
    }


def date_dict_to_number(date_dict):
    if date_dict is None:
        return float('inf')
    while len(date_dict['year']) != 4:
        date_dict['year'] = '0' + date_dict['year']
    while len(date_dict['month']) != 2:
        date_dict['month'] = '0' + date_dict['month']
    while len(date_dict['day']) != 2:
        date_dict['month'] = '0' + date_dict['day']
    return int(date_dict['year'] + date_dict['month'] + date_dict['day'])


def get_version_dict_list(
        article_dict):  # “ver”: [array di versioni]. Ogni versione ha “s”: data-inizio-vigenza, “e”: data-fine-vigenza. Se “e” è null, vigenza=today.
    return sorted(
        (
            {
                'start': get_date_dict(ver['s']),
                'end': get_date_dict(ver['e']),
                # 'content': ver['c']
            }
            for ver in article_dict['ver']
        ),
        key=lambda x: date_dict_to_number(x['end'])
    )


def get_constitution_article_version_dict(constitution_doc_path):
    constitution_article_version_dict = {}
    with open(constitution_doc_path, 'r') as f:
        constitution_dict = json.load(f)
        article_list = get_article_list(constitution_dict)
        for article in article_list:
            article_id = article['num']
            constitution_article_version_dict[article_id] = get_version_dict_list(article)
    return constitution_article_version_dict

# import sys
# CONSTITUTION_DOC_PATH = sys.argv[1]
# print(get_constitution_article_version_dict(CONSTITUTION_DOC_PATH))
