<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2014</anno_pronuncia>
    <numero_pronuncia>205</numero_pronuncia>
    <ecli>ECLI:IT:COST:2014:205</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASSESE</presidente>
    <relatore_pronuncia>Mario Rosario Morelli</relatore_pronuncia>
    <redattore_pronuncia>Mario Rosario Morelli</redattore_pronuncia>
    <data_decisione>09/07/2014</data_decisione>
    <data_deposito>16/07/2014</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Sabino CASSESE; Giudici : Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI, Giancarlo CORAGGIO, Giuliano AMATO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 1, comma 51, della legge 28 giugno 2012, n. 92 (Disposizioni in materia di riforma del mercato del lavoro in una prospettiva di crescita), e dell'art. 51, comma 1, numero 4), del codice di procedura civile, promosso dal Tribunale ordinario di Siena - sezione lavoro, nel procedimento civile vertente tra Borsini Luciana e la Novartis Vaccines and Diagnostics srl, con ordinanza del 16 agosto 2013, iscritta al n. 20 del registro ordinanze 2014 e pubblicata nella Gazzetta Ufficiale  della Repubblica n. 10, prima serie speciale, dell'anno 2014.&#13;
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 23 giugno 2014 il Giudice relatore Mario Rosario Morelli.&#13;
 Ritenuto che - nel corso di un giudizio di opposizione, instaurato ai sensi dell'art. 1, comma 51, della legge 28 giugno 2012, n. 92 (Disposizioni in materia di riforma del mercato del lavoro in una prospettiva di crescita), sia dalla lavoratrice (che aveva ottenuto, nella forma dell'ordinanza all'esito della fase sommaria di cui al comma 49 dello stesso art. 1 della citata legge, il provvedimento di reintegrazione nel suo posto di lavoro) che dalla società datrice di lavoro - il Tribunale ordinario di Siena - sezione lavoro, ha sollevato, con l'ordinanza in epigrafe, questione di legittimità costituzionale del predetto art. 1, comma 51, della legge n. 92 del 2012 e dell'art. 51, comma 1, numero 4), cod. proc. civ., nella parte in cui la prima disposizione non prevede che il giudizio di opposizione abbia svolgimento davanti al medesimo giudice persona fisica della fase sommaria e la seconda non esclude dalla sua operatività la fattispecie in parola, prospettandone il contrasto con gli artt. 3, primo e secondo comma, 24, primo e secondo comma, 25, primo comma, 97 e 111, primo comma, della Costituzione;&#13;
 che, in ordine al censurato comma 51 dell'art. 1 della legge n. 92 del 2012 (il quale testualmente dispone che l'opposizione di che trattasi va proposta «innanzi al tribunale che ha emesso il provvedimento opposto»), il rimettente premette che non si è ancora formato un «diritto vivente», in presenza di decisioni di segno diverso dei giudici di merito e in assenza di una pronunzia della Corte di cassazione e, quindi, argomenta che l'istanza di sollecitazione ad astenersi, proposta nei suoi confronti (in quanto rivestente la qualità di giudice che ha trattato e definito anche la prima fase sommaria), sarebbe accoglibile nel caso in cui - con riferimento alla struttura ed alla natura del procedimento previsto, nel suo contesto complessivo, dai commi da 47 a 68 del medesimo art. 1 della legge n. 92 del 2012 - la fase (eventuale) di opposizione instaurata (avverso l'ordinanza conclusiva della fase sommaria) fosse ricondotta ad un giudizio di carattere impugnatorio;&#13;
 che preferibile, e costituzionalmente più compatibile, sarebbe, però, a suo avviso, l'interpretazione, secondo cui andrebbe, invece, escluso un tale contenuto impugnatorio della opposizione in esame, sul presupposto di una «natura bifasica» del giudizio di primo grado disciplinato dal predetto art. 1 della legge n. 92 del 2012;&#13;
 che, a conforto di tale seconda opzione ermeneutica, deporrebbe, sempre secondo il rimettente, la considerazione della sua idoneità ad assolvere allo scopo di velocizzare il rimedio dell'impugnativa del licenziamento ed a quello di evitare la configurazione di inconvenienti pratici nell'organizzazione (anche tabellare) degli uffici giudiziari (soprattutto di quelli di piccole dimensioni), mentre la soluzione opposta (implicante l'attribuzione a due distinti giudici - persone fisiche delle due differenti fasi, sommaria e di opposizione) - comporterebbe un aggravio del sistema di organizzazione giudiziaria (lesivo del criterio di ragionevole proporzionalità) e un potenziale rallentamento dell'inerente giudizio;&#13;
 che è intervenuto in questa sede il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, la quale ha concluso per l'inammissibilità o, in subordine, per la non fondatezza della questione.&#13;
 Considerato che - a prescindere dai profili di inadeguatezza della motivazione, quanto alla prospettazione di inconvenienti fattuali, come tali non rilevanti ai fini del sindacato di legittimità costituzionale (da ultimo, ordinanze n. 166 e n. 112 del 2013) ed alla evocazione di plurimi parametri costituzionali in modo cumulativo e sostanzialmente generico - la questione in esame è, comunque, manifestamente inammissibile per il motivo, assorbente, che essa si risolve nell'improprio tentativo di ottenere da questa Corte, con uso distorto dell'incidente di costituzionalità, l'avallo dell'interpretazione proposta dal rimettente in ordine ad un contesto normativo che egli pur riconosce suscettibile di duplice lettura (ex multis, ordinanze n. 196 del 2013, n. 304 e n. 185 del 2012, e n. 139 del 2011);&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, commi 1 e 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale dell'art. 1, comma 51, della legge 28 giugno 2012, n. 92 (Disposizioni in materia di riforma del mercato del lavoro in una prospettiva di crescita) e dell'art. 51, primo comma, numero 4), del codice di procedura civile, sollevata, in riferimento agli artt. 3, primo e secondo comma, 24, primo e secondo comma, 25, primo comma, 97 e 111, primo comma, della Costituzione, dal Tribunale ordinario di Siena - sezione lavoro, con l'ordinanza in epigrafe.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 9 luglio 2014.&#13;
 F.to:&#13;
 Sabino CASSESE, Presidente&#13;
 Mario Rosario MORELLI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 16 luglio 2014.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
