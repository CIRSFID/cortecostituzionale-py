from bs4 import BeautifulSoup


def parse():
    patterns = []
    soup = BeautifulSoup(open("foo.html"), "html.parser")
    for tr in soup.find_all("tr"):
        td = tr.find("td")
        span = td.find("span")
        full_name = span.text
        cognome, nome = full_name.split(", ")
        patterns.append('"' + f"{cognome},? ?{nome}" + '"')
        patterns.append('"' + f"{nome} {cognome}" + '"')
        patterns.append('"' + cognome + '"')
        #patterns.append('"'+f"(({cognome}|{nome}) ?)" + "{1,2}"+'"')
    print(",\n".join(patterns))


if __name__ == '__main__':
    parse()
