<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2008</anno_pronuncia>
    <numero_pronuncia>246</numero_pronuncia>
    <ecli>ECLI:IT:COST:2008:246</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>23/06/2008</data_decisione>
    <data_deposito>02/07/2008</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 10, comma 4, della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), promosso con ordinanza del 14 marzo 2006 dalla Corte d'appello di Messina nel procedimento penale a carico di C. P. ed altri, iscritta al n. 427 del registro ordinanze 2006 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 43, prima serie speciale, dell'anno 2006. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
    udito nella camera di consiglio del 7 maggio 2008 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che la Corte d'appello di Messina ha sollevato, in riferimento all'art. 3 della Costituzione, questione di legittimità costituzionale dell'art. 10, comma 4, della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui non equipara alla sentenza di condanna di una Corte d'appello che abbia riformato una sentenza di assoluzione, la sentenza di proscioglimento per prescrizione emessa a seguito di concessione delle circostanze attenuanti generiche»;  &#13;
    che la Corte rimettente premette di essere investita del giudizio di rinvio a seguito dell'annullamento, da parte della Corte di cassazione, della sentenza pronunciata dalla Corte d'appello di Reggio Calabria che aveva dichiarato non doversi procedere per prescrizione nei confronti di alcuni imputati, in ordine al reato di omicidio colposo loro ascritto, previa concessione delle circostanze attenuanti generiche; &#13;
    che detta sentenza d'appello era stata emessa in riforma della pronuncia del giudice di prime cure, appellata dal pubblico ministero, con la quale i predetti imputati erano stati in origine assolti perché il fatto non sussiste; &#13;
    che il giudice a quo rileva che l'art. 10, comma 4, della legge n. 46 del 2006 prevede che la disposizione di cui al comma 2 del medesimo articolo - secondo cui l'appello proposto dall'imputato o dal pubblico ministero, prima della data di entrata in vigore della legge, viene dichiarato inammissibile con ordinanza non impugnabile - si applica anche nel caso in cui sia annullata una sentenza di condanna di una corte d'appello che abbia riformato una sentenza di assoluzione; &#13;
    che tale disposizione non risulterebbe applicabile «direttamente» al caso per cui si procede né sarebbe suscettibile di interpretazione analogica, in quanto derogatoria «del principio generale dell'appellabilità delle sentenze»; &#13;
    che proprio la circostanza che la norma non equipara alla sentenza di condanna di una corte d'appello, che abbia riformato una sentenza di assoluzione, la sentenza di proscioglimento per prescrizione emessa a seguito di concessione delle circostanze attenuanti generiche, risulta, secondo la rimettente, in contrasto con l'art. 3 Cost.; &#13;
    che la Corte rimettente -  che ritiene rilevante la questione stante la sua pregiudizialità rispetto alla definizione del giudizio - reputa infatti che tale disciplina determini «una palese disparità di trattamento tra situazioni processuali assimilabili, essendo la pronuncia di proscioglimento necessariamente preceduta da un sostanziale giudizio di responsabilità»; &#13;
    che nel giudizio è intervenuto il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, concludendo per la manifesta infondatezza della questione; &#13;
    che la difesa erariale ritiene non condivisibile la premessa da cui muove l'ordinanza di rimessione circa l'equiparabilità di una sentenza di non luogo a procedere per intervenuta prescrizione, sia pur emessa a seguito della concessione di attenuanti generiche, ad una sentenza di condanna; &#13;
    che, infatti, la sentenza di non luogo a procedere per intervenuta prescrizione, a seguito della concessione di attenuanti generiche, è condizionata esclusivamente all'accertamento «che dagli atti non risulta evidente che non ci sono i presupposti per addivenire ad una formula assolutoria di merito»; e, a differenza dell'altra, non presuppone «necessariamente un sostanziale giudizio di condanna»; &#13;
    che venendo meno tale presupposto di assimilabilità - conclude l'Avvocatura generale - non è configurabile la violazione del principio di uguaglianza denunciata dal giudice a quo. &#13;
    Considerato che la Corte d'appello di Messina dubita, in riferimento all'art. 3 della Costituzione, della legittimità costituzionale dell'art. 10, comma 4, della legge 20 febbraio 2006, n. 46 (Modifiche al codice di procedura penale, in materia di inappellabilità delle sentenze di proscioglimento), «nella parte in cui non equipara alla sentenza di condanna di una Corte d'appello che abbia riformato una sentenza di assoluzione, la sentenza di proscioglimento per prescrizione emessa a seguito di concessione delle circostanze attenuanti generiche»; &#13;
    che il comma 4 censurato fa espresso richiamo al comma 2 del medesimo articolo, stabilendo che tale disposizione – secondo cui l'appello proposto dall'imputato o dal pubblico ministero prima della data di entrata in vigore della legge viene dichiarato inammissibile con ordinanza non impugnabile – «si applica anche nel caso in cui sia annullata, su punti diversi dalla pena o dalla misura di sicurezza, una sentenza di condanna di una corte d'assise d'appello o di una corte d'appello che abbia riformato una sentenza di assoluzione»; &#13;
    che con la sentenza n. 26 del 2007, successiva all'ordinanza di rimessione, questa Corte – contestualmente alla declaratoria di incostituzionalità dell'art. 1 della legge 20 febbraio 2006, n. 46, sostitutivo dell'art. 593 del codice di procedura penale – ha dichiarato l'illegittimità costituzionale dell'art. 10, comma 2, della citata legge n. 46 del 2006, «nella parte in cui prevede che l'appello proposto contro una sentenza di proscioglimento dal pubblico ministero prima della data di entrata in vigore della medesima legge è dichiarato inammissibile»; &#13;
    che, pertanto, risultando mutato a seguito della richiamata pronuncia di questa Corte il quadro normativo di riferimento, gli atti devono essere restituiti al giudice rimettente per un nuovo esame della rilevanza della questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE  &#13;
     ordina la restituzione degli atti alla Corte d'appello di Messina. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 23 giugno 2008.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Giovanni Maria FLICK, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 2 luglio 2008.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
