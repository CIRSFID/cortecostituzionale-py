<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-07-29/175/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-07-29/175"/>
          <FRBRalias value="ECLI:IT:COST:2020:175" name="ECLI"/>
          <FRBRdate date="2020-07-29" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="175"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-07-29/175/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-07-29/175/ita@"/>
          <FRBRdate date="2020-07-29" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-07-29/175/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2020-07-29/175/ita@.xml"/>
          <FRBRdate date="2020-07-29" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2020-06-25" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2020-07-29" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>manifesta inammissibilità</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO PER CONFLITTO DI ATTRIBUZIONE TRA ENTI</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="CARTABIA" href="/akn/ontology/person/it/CARTABIA" showAs="CARTABIA"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="francescoViganò" href="/akn/ontology/person/it/francescoViganò" showAs="Francesco VIGANÒ"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="robertoMilana" href="/akn/ontology/person/it/robertoMilana" showAs="Roberto MILANA"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2020</akn4cc:anno>
        <akn4cc:numero>175</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2020:175</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>CARTABIA</akn4cc:presidente>
        <akn4cc:relatore>Franco Modugno</akn4cc:relatore>
        <akn4cc:data_decisione>2020-06-25</akn4cc:data_decisione>
        <akn4cc:data_deposito>2020-07-29</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO PER CONFLITTO DI ATTRIBUZIONE TRA ENTI</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>manifesta inammissibilità</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#martaCartabia">Marta CARTABIA</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#francescoViganò">Francesco VIGANÒ</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>, Stefano PETITTI,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio per conflitto di attribuzione tra enti, sorto a seguito dei comportamenti formali posti in essere dalle Regioni Lombardia e Veneto, con riferimento alle funzioni in materia di riapertura delle attività produttive e commerciali nell'ambito della cosiddetta "fase 2" dell'emergenza epidemiologica da COVID-19, promosso dal Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori (CODACONS), con ricorso notificato il 21 aprile 2020, depositato in cancelleria il 23 aprile 2020, iscritto al n. 3 del registro conflitti tra enti 2020 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 19, prima serie speciale, dell'anno 2020.
 Visti gli atti di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> della Regione Lombardia e della Regione Veneto;
 udito il Giudice relatore Franco Modugno nella camera di consiglio del 24 giugno 2020, svolta ai sensi del decreto della Presidente della Corte del 20 aprile 2020, punto 1), lettera a);
 deliberato nella camera di consiglio del 25 giugno 2020.
 Ritenuto che, con ricorso notificato il 21 aprile 2020 e depositato il 23 aprile 2020, il Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori (CODACONS) ha promosso conflitto di attribuzioni nei confronti delle Regioni Lombardia e Veneto, in riferimento ai «comportamenti formali posti in essere» da queste, per violazione delle attribuzioni costituzionalmente riservate allo Stato dagli artt. 117, commi secondo, lettere d), q) e h), terzo, e 120 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;
 che il ricorrente dichiara di agire in via suppletiva dello Stato e chiede a questa Corte di «accertare e dichiarare a chi spettano, in base alle norme costituzionali sopra richiamate, le attribuzioni per la gestione della c.d. fase 2, nell'ambito dell'emergenza COVID19»;
 che il CODACONS, denunciando la situazione di generale «disordine istituzionale» nei rapporti fra Governo e Regioni nella gestione dell'emergenza epidemiologica da COVID-19, ritiene che le Regioni Veneto e Lombardia, «che sono anche le zone più colpite dal contagio, hanno espresso numerose volte dichiarazioni di segno opposto a quelle dello Stato»;
 che, in particolare, il ricorrente lamenta, per un verso, che la Regione Lombardia avrebbe «ondeggiato fra misure stringenti, l'obbligatorietà della mascherina per i cittadini lombardi, e fughe in avanti»; per l'altro, che la Regione Veneto avrebbe, invece, manifestato la volontà di riapertura anticipata rispetto alle previsioni statali delle attività produttive e commerciali, essendo stato, a tal fine, addirittura presentato un «Piano autonomo di riapertura di ben 17 pagine»;
 che, pertanto, secondo il CODACONS si sarebbe determinata una situazione di «grave caos», che rischierebbe «di infrangere l'unità dello Stato», poiché molte Regioni porrebbero in essere comportamenti gravemente lesivi delle attribuzioni dello Stato e «il Governo quale rappresentante dello Stato» assisterebbe «inerte a questa continua invasione delle sue competenze»;
 che, in punto di diritto, il ricorrente, pur riconoscendo come «pacifica la struttura del conflitto di attribuzioni tra Stato e Regioni nonché tra Regioni», dichiara di agire in via suppletiva dello Stato, poiché - aggiunge - l'azione, così promossa, non impedirebbe al soggetto originariamente titolare di agire, in qualsiasi momento, per assumere in proprio la titolarità del conflitto;
 che, riguardo alla legittimazione a sollevare il conflitto, secondo il ricorrente, la giurisprudenza di questa Corte ammetterebbe che «soggetti ed organi diversi dallo Stato-apparato possono essere parti di un conflitto tra poteri [...] qualora risultino titolari di una "pubblica funzione costituzionalmente rilevante e garantita"»;
 che, su tali basi, sussisterebbe la legittimazione sotto il profilo soggettivo, in quanto il CODACONS sarebbe «per legge [...] chiamato a tutelare gli interessi ed i diritti di consumatori e utenti», tra cui, in base al proprio statuto, il diritto alla salute e il diritto alla trasparenza, anche in ragione, come si ricaverebbe dal Consiglio di Stato, adunanza plenaria, sentenze 20 febbraio 2020, n. 6 e 11 gennaio 2007, n. 1 e <ref href="/akn/it/judgment/sentenza/corteDiCassazione/2011-08-08/1CIV/!main">Corte di cassazione, sezione terza civile, sentenza 18 agosto 2011</ref>, n. 17351, del «compito di un certo rilievo pubblicistico» che lo stesso svolgerebbe;
 che, in aggiunta, la legittimazione del ricorrente troverebbe fondamento anche nell'art. 4-ter delle Norme integrative per i giudizi davanti alla Corte costituzionale, il quale, permettendo agli «enti esponenziali, come il Codacons, di partecipare ai giudizi di legittimità costituzionale sulle leggi», avrebbe «[c]ostituzionalizza[to], per così dire, la posizione degli Enti esponenziali»;
 che, infine, il potere riconosciuto a tali enti di sostituirsi allo Stato si ricaverebbe anche da «molteplici» norme che regolano il potere sostitutivo: l'art. 120, secondo comma, Cost., che disciplina il potere sostitutivo delle Stato nei confronti delle Regioni, delle Città metropolitane, delle Province e dei Comuni, «l'art. 9 TUEL», che prevedrebbe «la possibilità per gli Enti esponenziali di far valere in giudizio le azioni e i ricorsi che spettano al Comune e alla Provincia», nonché «[l]'art. 310 Cod. Ambiente che attribui[rebbe] agli enti esponenziali un potere sostitutivo in materia di danno ambientale, quando, ad. es. l'ente locale rimane inerte»;
 che, per quanto riguarda la sussistenza del profilo oggettivo, il ricorrente premette che, in sede di conflitto di attribuzione tra Stato e Regioni, potrebbero essere sindacati anche comportamenti degli organi istituzionali, come emergerebbe dalla sentenza di questa Corte n. 1 del 2013;
 che, su tale presupposto, il CODACONS ritiene che, tanto la Regione Veneto, con un programma contenente puntuali prescrizioni, quanto la Regione Lombardia, con «dichiarazioni, comunicati stampa e interviste televisive, proprie e personali posizioni sulla gestione della crisi», avrebbero posto in essere comportamenti formali lesivi delle competenze statali;
 che, con tali comportamenti, avrebbero violato le attribuzioni statali sancite dall'art. 117, commi secondo, lettere d), q) e h), in materia di "sicurezza dello Stato", "profilassi internazionale"; e "ordine pubblico e sicurezza" e terzo, Cost. in materia di "tutela della salute", in quanto si afferma essere evidente che nell'emergenza epidemiologica da COVID-19, venendo inciso «il diritto primario alla salute, i principi fondamentali per la c.d. fase 2 spettano allo Stato, e solo norme dal carattere integrativo spettano alle Regioni», nonché l'art. 120, secondo comma, Cost., che attribuirebbe al Governo il ruolo di «granate [recte: garante] dell'unità di azione e indirizzo dello Stato», minato «dalle reiterate condotte poste in essere dalla Regione Lombardia e della [recte: dalla] Regione [Veneto]»;
 che il ricorrente ha, infine, proposto istanza cautelare di sospensione dei gravi comportamenti formali posti in essere dalle Regioni Veneto e Lombardia, chiedendo, altresì, che venga ordinato alle stesse di astenersi dal porre in essere ulteriori comportamenti lesivi delle attribuzioni statali;
 che, con successiva nota, depositata in data 29 aprile 2020, il ricorrente ha integrato i riferimenti agli atti ritenuti lesivi, producendo, oltre a una serie di articoli di stampa, l'ordinanza del Presidente della Giunta regionale della Regione Veneto 24 aprile 2020, n. 42, recante «Misure urgenti in materia di contenimento e gestione dell'emergenza epidemiologica da virus COVID-19. Ulteriori disposizioni.» e un «[d]ocumento predisposto dal comune di Milano con le sue proposte per la cosiddetta fase 2 pubblicato sul sito istituzionale dello stesso Comune il 24 aprile 2020»;
 che la Regione Lombardia si è costituita in giudizio con atto depositato il 25 maggio 2020, chiedendo che il ricorso sia dichiarato inammissibile e, comunque sia, infondato;
 che la difesa di parte resistente, premessa un'ampia ricostruzione della disciplina normativa dell'emergenza sanitaria, sociale ed economica causata dalla diffusione della pandemia da COVID-19, tesa a dimostrare che «l'adozione di misure restrittive delle libertà individuali e della iniziativa economica, attraverso DPCM», può essere accompagnata da interventi regionali più restrittivi per adattare le singole disposizioni alla situazione sanitaria della singola Regione, afferma che la Regione Lombardia avrebbe «sempre cercato il coordinamento con il Governo» e si sarebbe sempre attenuta alla «"centralizzazione" delle istruzioni per fare fronte alla pandemia»;
 che, in diritto, la Regione Lombardia ritiene il ricorso inammissibile per carenza del requisito soggettivo, innanzitutto perché non sarebbe configurabile il potere di agire in via suppletiva in sede di conflitto di attribuzione fra Stato e Regioni, ai sensi dell'<ref href="/akn/it/act/legge/stato/1953-03-11/87/!main#art_39">art. 39 della legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), né vi sarebbero altre norme di legge che attribuirebbero ad associazioni, quali il CODACONS, il potere di agire in rappresentanza dello Stato in sede di conflitto fra enti, e, sotto questo secondo aspetto, le previsioni normative richiamate dal ricorrente a sostegno della propria azione nulla avrebbero «in comune con il procedimento oggetto dell'odierna disamina»;
 che, pertanto, il CODACONS non potrebbe - in linea con quanto affermato dalla giurisprudenza di questa Corte - vantare alcuna titolarità di funzioni costituzionalmente rilevanti, tali da legittimarlo alla proposizione del conflitto;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, ai fini della legittimazione, neppure rileverebbe l'art. 4-ter delle Norme integrative, in quanto quest'ultimo non avrebbe apportato alcuna modifica alla legittimazione a introdurre giudizi davanti alla Corte costituzionale, ma si sarebbe limitato a prevedere la possibilità, per le formazioni sociali senza scopo di lucro, oppure per i soggetti istituzionali, se portatori di interessi collettivi o diffusi attinenti alla questione in discussione (rispetto ai quali - così si sostiene - CODACONS non avrebbe, comunque sia, alcuna titolarità), di presentare brevi opinioni scritte;
 che, secondo la Regione Lombardia il conflitto sarebbe inammissibile anche per carenza del requisito oggettivo;
 che la resistente (richiamando ampiamente la giurisprudenza costituzionale) sostiene, infatti, che, seppur in sede di conflitto di attribuzione sia stata ammessa la possibilità di sindacare non solo atti, bensì anche comportamenti degli organi istituzionali, oggetto del conflitto dovrebbe, comunque sia, essere un comportamento significante, a rilevanza esterna, poiché finalizzato all'esercizio di una precisa competenza ed effettivamente lesivo (per invasione o menomazione) delle altrui competenze;
 che tali elementi difetterebbero nel ricorso, in quanto il CODACONS avrebbe preteso di individuare tali comportamenti in «dichiarazioni, comunicati stampa e interviste televisive»;
 che, in tal modo, il ricorrente non solo avrebbe diretto la propria azione verso comportamenti che non possono essere considerati alla stregua di comportamenti significanti e che non possono essere qualificati neppure come atti preparatori o non definitivi della pubblica amministrazione, ma non avrebbe neppure esplicitato in modo chiaro quali sarebbero poi le dichiarazioni lesive della potestà statale, non avendo chiaramente individuato le dichiarazioni, né fornito elementi atti a comprendere il comportamento regionale reputato lesivo delle competenze statali;
 che tali carenze, a parere della Regione Lombardia, starebbero a dimostrare che questa Corte sarebbe stata adita «a scopo meramente consultivo», dal che deriverebbe l'inammissibilità del ricorso, in quanto la giurisprudenza costituzionale avrebbe negato la possibilità di ricorrere al conflitto di attribuzione per tale finalità (si richiama espressamente la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2013/1/!main">sentenza n. 1 del 2013</ref> e le decisioni in questa citate);
 che, in disparte l'inammissibilità, secondo la Regione Lombardia sarebbe in ogni caso intervenuta la cessazione della materia del contendere, determinata da «un sopravvenuto mutamento della situazione esistente al momento della proposizione del ricorso», in quanto il d.P.C.m. 17 maggio 2020 (Disposizioni attuative del <ref href="/akn/it/act/decretoLegge/stato/2020-03-25/19/!main">decreto-legge 25 marzo 2020, n. 19</ref>, recante misure urgenti per fronteggiare l'emergenza epidemiologica da COVID-19, e del <ref href="/akn/it/act/decretoLegge/stato/2020-05-16/33/!main">decreto-legge 16 maggio 2020, n. 33</ref>, recante ulteriori misure urgenti per fronteggiare l'emergenza epidemiologica da COVID-19)  sarebbe stato adottato conformemente alle «linee guida per la riapertura delle attività economiche e produttive della Conferenza delle Regioni e delle Province Autonome del 16 maggio 2020»;
 che, nel merito, la resistente, rileva come le censure contenute nel ricorso sarebbero meramente assertive, poiché il CODACONS si sarebbe limitato a elencare le disposizioni costituzionali violate e non avrebbe argomentato sui denunciati profili di lesione delle attribuzioni costituzionali;
 che, infine, la Regione Lombardia afferma non doversi dare seguito all'istanza cautelare, in quanto questa sarebbe stata «proposta da Codacons in relazione all'imminenza della data del 4 maggio», anche in ragione del fatto che il trascorrere del tempo non avrebbe comportato le conseguenze dannose paventate, ma anzi avrebbe visto assumere atti legislativi e provvedimenti amministrativi che sarebbero stati «ritenuti idonei alla gestione dell'andamento dell'epidemia dalle Autorità di volta in volta competenti»;
 che si è costituita in giudizio anche la Regione Veneto, con atto depositato il 28 maggio 2020, chiedendo che il ricorso sia dichiarato inammissibile e, comunque sia, infondato;
 che, in fatto, la resistente offre un'ampia, puntuale e articolata ricostruzione degli avvenimenti che hanno segnato la fase dell'emergenza epidemiologica da COVID-19, tesa a dimostrare che la resistente, «pur convinta delle proprie idee, sollecitate dalla cittadinanza e dal mondo economico», non avrebbe posto in essere comportamenti contrari al criterio della leale collaborazione, ciò che del resto spiegherebbe «perché [...] lo Stato non ha sollevato conflitt[o] di attribuzioni»;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che, secondo la Regione Veneto, il canone della leale collaborazione sarebbe stato seguito anche per il «"Piano per la ripartenza"», rispetto al quale il Presidente della Giunta regionale, pur avendo espresso il timore delle ricadute sociali ed economiche per la sua mancata e pronta attuazione, avrebbe, in ogni caso, riconosciuto la spettanza al Governo della «decisione finale»;
 che, in punto di diritto, la resistente ritiene il ricorso inammissibile in quanto difetterebbe in capo al CODACONS il requisito soggettivo richiesto: «non essendo né lo Stato né una Regione»;
 che non pertinente sarebbe, peraltro, il richiamo fatto dal ricorrente alla giurisprudenza sul conflitto di attribuzione tra poteri, disciplinato dall'<ref href="/akn/it/act/legge/stato/1953/87/!main#art_37">art. 37 della legge n. 87 del 1953</ref>;
 che, inoltre, fornirebbe prova indiretta dell'inammissibilità del conflitto proprio il richiamo, nel ricorso introduttivo, delle pronunce della Corte di cassazione e del Consiglio di Stato, le quali si limiterebbero a valorizzare l'apporto in sede di giudizio civile e di quello amministrativo degli enti associativi per «l'applicazione, in parte qua, di fonti non di rango costituzionale», contesti, questi ultimi, nei quali le questioni non possiederebbero alcun tono costituzionale e non atterrebbero alla «spettanza o meno di un potere»;
 che neppure pertinente, ai fini della sussistenza del requisito soggettivo, sarebbe il richiamo all'art. 4-ter delle Norme integrative: la disposizione, recante «la non equivoca dicitura "Amici curiae"», il quale non disciplinerebbe «alcuna soggettività costituzionale», ma permetterebbe soltanto ad alcuni soggetti «di ampliare l'orizzonte conoscitivo della Corte»;
 che, inoltre, la proposizione di un ricorso non sarebbe affatto un atto dovuto, ma la scelta, da parte dello Stato e delle Regioni, di attivarsi sarebbe questione che coinvolge profili di politica costituzionale e istituzionale, e del resto gli enti associativi, pur svolgendo «attività di sicuro rilievo comunitario», non dovrebbero interferire con i compiti affidati dalla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> agli enti territoriali;
 che, altresì, difetterebbe il requisito oggettivo, poiché il ricorso sarebbe sprovvisto dei caratteri dell'attualità e della concretezza, in quanto l'elaborazione di un piano, il quale peraltro è stato anche trasmesso al Governo, non potrebbe ritenersi in alcun modo lesivo delle attribuzioni dello Stato;
 che, inoltre, dal ricorso non si ricaverebbero neppure precise indicazioni per la determinazione e qualificazione dei comportamenti lesivi;
 che altro profilo di inammissibilità risiederebbe nella contestazione generica e congiunta dell'operato della Regione Lombardia e della Regione Veneto, senza che sia possibile distinguere le posizioni dell'una e dell'altra al fine dell'individuazione delle presunte menomazioni delle attribuzioni statali;
 che nel merito, la Regione Veneto ritiene il ricorso infondato, in quanto il rispetto del principio di leale collaborazione nelle «relazioni centro-periferia», emerso dalla ricostruzione della parte in fatto, renderebbe le denunciate lesioni degli artt. 117, commi secondo, lettere d), q) e h), terzo, e 120, comma secondo, Cost., insussistenti «per carenza di atti e comportamenti lesivi delle competenze spettanti allo Stato»;
 che la resistente, infine, sostiene che non potrebbe darsi seguito all'istanza cautelare, in quanto sarebbe priva di qualsiasi fondamento giuridico, non avendo la Regione Veneto leso alcuna attribuzione dello Stato;
 che, con memoria depositata in prossimità dell'udienza, il ricorrente risponde alle eccezioni sollevate dalle Regioni Lombardia e Veneto e, nella sostanza, ribadisce le deduzioni svolte nel ricorso;
 che anche la Regione Lombardia e la Regione Veneto hanno depositato memorie, nelle quali, a loro volta, hanno ribadito le deduzioni svolte nell'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref>.
 Considerato che il Coordinamento delle associazioni e dei comitati di tutela dell'ambiente e dei diritti degli utenti e dei consumatori (CODACONS) ha promosso conflitto di attribuzione tra enti nei confronti delle Regioni Lombardia e Veneto, in riferimento «ai comportamenti formali posti in essere» da queste, per violazione delle attribuzioni costituzionalmente riservate allo Stato dagli artt. 117, commi secondo, lettere d), q) e h), terzo, e 120, comma secondo, della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>;
 che il ricorrente dichiara espressamente di agire in via suppletiva dello Stato e chiede a questa Corte di «accertare e dichiarare a chi spettano, in base alle norme costituzionali sopra richiamate, le attribuzioni per la gestione della c.d. fase 2, nell'ambito dell'emergenza COVID19»;
 che va innanzitutto rilevato, sotto il profilo soggettivo, che l'art. 39, terzo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref> (Norme sulla <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> e sul funzionamento della Corte costituzionale), attribuisce la legittimazione a proporre ricorso per lo Stato al «Presidente del Consiglio dei ministri o [ad] un Ministro da [questi] delegato» e per la Regione al «Presidente della Giunta regionale in seguito a deliberazione della Giunta stessa»;
 che, in forza del chiaro tenore letterale della richiamata disposizione, la costante giurisprudenza di questa Corte ha ritenuto che «"nessun elemento letterale o sistematico [...] consente di superare la chiara limitazione soggettiva che si ricava dagli <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_134">artt. 134 della Costituzione</ref> e 39, terzo comma, della citata <ref href="/akn/it/act/legge/stato/1953/87/!main">legge n. 87 del 1953</ref>"» (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2009/130/!main">sentenza n. 130 del 2009</ref>);
 che, consapevole di ciò, il ricorrente deduce di «agire in via suppletiva dello Stato» e che tale azione sarebbe ammissibile in quanto non impedirebbe al soggetto originariamente titolare di agire, in qualsiasi momento, per assumere in proprio la titolarità del conflitto;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>che la predetta azione suppletiva è del tutto estranea al nostro ordinamento e la sua prospettazione è quindi inidonea a superare la chiara e inequivoca limitazione soggettiva alla proposizione del conflitto tra enti;
 che il conflitto è carente anche del requisito oggettivo;
 che, secondo la giurisprudenza di questa Corte, è ritenuto idoneo a innescare un conflitto intersoggettivo di attribuzione anche un comportamento, purché questo sia un «comportamento significante, imputabile allo Stato o alla Regione», «dotato di efficacia e rilevanza esterna e - anche se preparatorio o non definitivo - diretto, in ogni caso, «"ad esprimere in modo chiaro ed inequivoco la pretesa di esercitare una data competenza, il cui svolgimento possa determinare una invasione nella altrui sfera di attribuzioni o, comunque, una menomazione altrettanto attuale delle possibilità di esercizio della medesima" (<ref href="/akn/it/judgment/sentenza/corteCostituzionale/2011/332/!main">sentenza n. 332 del 2011</ref>; nello stesso senso, <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2006/382/!main">sentenze n. 382 del 2006</ref>, n. 211 del 1994 e n. 771 del 1988)» (così <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2020/22/!main">sentenza n. 22 del 2020</ref>);
 che, nel caso di specie, i menzionati elementi non possono ritenersi sussistenti;
 che, infatti, oggetto di contestazione sono comportamenti - ovverosia dichiarazioni riportate dagli organi di stampa e ritenute «imputabili» alla Regione Lombardia, peraltro non meglio specificate né rispetto ai contenuti né con riguardo ai soggetti istituzionali che le avrebbero poste in essere, e il «Progetto per la riapertura delle attività produttive» della Regione Veneto del 17 aprile 2020 - evidentemente sprovvisti dei richiamati requisiti di efficacia e rilevanza esterna e, comunque sia, intrinsecamente inidonei a esprimere in modo chiaro e inequivoco la pretesa di esercitare una competenza invasiva della sfera di attribuzioni costituzionali statali;
 che, pertanto, la minaccia di lesione è puramente congetturale e il conflitto è promosso a fini meramente consultivi, come del resto emerge chiaramente dal ricorso, con il quale viene espressamente chiesto a questa Corte di «accertare e dichiarare a chi spettano, in base alle norme costituzionali sopra richiamate, le attribuzioni per la gestione della c.d. fase 2, nell'ambito dell'emergenza COVID19»;
 che, in conclusione, l'iniziativa del ricorrente si mostra come una forzatura dei meccanismi di instaurazione del conflitto tra enti, dal che deriva la manifesta inammissibilità del conflitto e l'assorbimento della connessa istanza di sospensione.</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 2, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara la manifesta inammissibilità del ricorso per conflitto di attribuzione promosso dal Coordinamento delle associazioni e dei comitati dell'ambiente e dei diritti degli utenti e dei consumatori (CODACONS) nei confronti delle Regioni Lombardia e Veneto, con il ricorso indicato in epigrafe.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2020-06-25">25 giugno 2020</date>.
 F.to:
 <signature><judge refersTo="#martaCartabia" as="#presidente">Marta CARTABIA</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#francoModugno" as="#relatore">Franco MODUGNO</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#robertoMilana" as="#cancelliere">Roberto MILANA</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2020-07-29">29 luglio 2020</date>.
 Il Cancelliere
 F.to: Roberto MILANA</block>
    </conclusions>
  </judgment>
</akomaNtoso>
