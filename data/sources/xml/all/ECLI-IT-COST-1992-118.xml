<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>118</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:118</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Mauro Ferri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/03/1992</data_decisione>
    <data_deposito>19/03/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale degli artt. 431, 512 e 514    &#13;
 del codice di procedura penale promossi con le seguenti ordinanze:       &#13;
      1)  ordinanza  emessa il 19 luglio 1991 dal Pretore di Bergamo -    &#13;
 sezione distaccata di Clusone, nel procedimento penale  a  carico  di    &#13;
 Lunghi  Gualtiero,  iscritta  al n. 607 del registro ordinanze 1991 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  40,  prima    &#13;
 serie speciale, dell'anno 1991;                                          &#13;
      2)  ordinanza  emessa  il  5  luglio 1991 dal Tribunale di Busto    &#13;
 Arsizio nel procedimento penale a carico di Rossa  Ornella,  iscritta    &#13;
 al  n.  645  del  registro ordinanze 1991 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 42,  prima  serie  speciale,  dell'anno    &#13;
 1991;                                                                    &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di  consiglio  del  4  marzo  1992  il  Giudice    &#13;
 relatore Mauro Ferri;                                                    &#13;
    Ritenuto  che,  con  ordinanza  del  19 luglio 1991, il Pretore di    &#13;
 Bergamo - sezione distaccata di Clusone,  premesso  che  il  pubblico    &#13;
 ministero,  a  seguito  della  morte della parte offesa che era stata    &#13;
 indicata quale  teste,  ha  chiesto  che  venga  data  lettura  delle    &#13;
 dichiarazioni  da essa precedentemente rese alla polizia giudiziaria,    &#13;
 ha  sollevato  -  in  riferimento  agli  artt.  76,  77  e  3   della    &#13;
 Costituzione  -  questione di legittimità costituzionale degli artt.    &#13;
 431 e  512  del  codice  di  procedura  penale  nella  parte  in  cui    &#13;
 "escludono  che,  per  il  caso di sopravvenuta irripetibilità di un    &#13;
 atto compiuto o ricevuto dalla polizia giudiziaria,  in  relazione  a    &#13;
 reati  di competenza pretorile, sia data lettura degli atti medesimi,    &#13;
 o  comunque  che  essi  atti  siano  acquisiti   al   fascicolo   del    &#13;
 dibattimento ed utilizzati per la decisione";                            &#13;
      che,  ad  avviso  del remittente, la direttiva n. 76 della legge    &#13;
 delega imponeva di prevedere che potesse  darsi  lettura  degli  atti    &#13;
 indicati  al  n. 57 (atti .. non ripetibili compiuti o ricevuti dalla    &#13;
 polizia  giudiziaria  e  dal  pubblico   ministero),   per   cui   la    &#13;
 irragionevole  esclusione, operata dal legislatore delegato nell'art.    &#13;
 512, della estensione della possibilità di lettura anche degli  atti    &#13;
 divenuti in concreto irripetibili pare contrastare con gli artt. 76 e    &#13;
 77  della  Costituzione;  inoltre,  lo  stesso art. 512 violerebbe il    &#13;
 principio di eguaglianza in quanto, circoscrivendo la possibilità di    &#13;
 lettura ai soli atti compiuti dal pubblico ministero  e  dal  giudice    &#13;
 dell'udienza  preliminare,  e  quindi  nei soli processi eccedenti la    &#13;
 competenza   pretorile,   consente    un    trattamento    probatorio    &#13;
 differenziato  a  secondo  che i reati siano o meno di competenza del    &#13;
 pretore;                                                                 &#13;
      che  analoghe  censure, osserva infine il giudice a quo, possono    &#13;
 essere mosse all'art. 431 del codice  di  procedura  penale,  ove  si    &#13;
 ritenga  che  gli  atti  ivi  elencati  come irripetibili non possano    &#13;
 formare  oggetto  di  successivo  inserimento   nel   fascicolo   per    &#13;
 sopravvenuta   ricorrenza  della  condizione  che,  se  preesistente,    &#13;
 avrebbe legittimato il pubblico ministero ad inserirveli;                &#13;
      che, con ordinanza del 5 luglio  1991,  il  Tribunale  di  Busto    &#13;
 Arsizio,   premesso  che  la  difesa  dell'imputata  aveva  formulato    &#13;
 numerose eccezioni in  ordine  al  contenuto  del  fascicolo  per  il    &#13;
 dibattimento, chiedendo l'espunzione da quest'ultimo di diversi atti,    &#13;
 tra  i  quali  il verbale delle dichiarazioni rese dalla parte offesa    &#13;
 alla polizia giudiziaria,  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale "del combinato disposto degli artt. 431, 512 e 514 del    &#13;
 codice  di  procedura  penale, in riferimento agli artt. 3 e 97 della    &#13;
 Costituzione, nella parte in cui esso  non  prevede  che  il  giudice    &#13;
 possa   dare  lettura,  a  richiesta  di  parte,  dei  verbali  delle    &#13;
 dichiarazioni rese alla polizia giudiziaria dalle  persone  informate    &#13;
 sui  fatti,  quando,  per  fatti  o  circostanze imprevedibili, ne è    &#13;
 divenuta impossibile la ripetizione";                                    &#13;
      che il Collegio remittente osserva  che  le  dichiarazioni  rese    &#13;
 alla  polizia  giudiziaria  nel  corso  delle indagini preliminari da    &#13;
 persone informate sui fatti, nonostante siano  divenute  irripetibili    &#13;
 per  morte  del  dichiarante,  ovvero  proprio  a  causa di un evento    &#13;
 imprevedibile all'atto della  loro  assunzione,  non  possono  essere    &#13;
 lette   in  dibattimento  ad  istanza  di  parte  e  conseguentemente    &#13;
 acquisite al fascicolo d'ufficio, per  la  elementare  considerazione    &#13;
 che  gli  atti  assunti dalla polizia giudiziaria non rientrano nella    &#13;
 previsione  dell'art.  512  del  codice  di  procedura  penale   (che    &#13;
 comprende  solo gli atti assunti dal pubblico ministero o dal giudice    &#13;
 nel corso dell'udienza preliminare);                                     &#13;
      che sembra evidente,  a  suo  avviso,  l'irragionevolezza  della    &#13;
 vigente  disciplina  nella misura in cui essa regola in modo diverso,    &#13;
 senza alcuna apprezzabile giustificazione logica, due atti istruttori    &#13;
 ontologicamente simili,  quali  le  informazioni  rese  alla  polizia    &#13;
 giudiziaria  ex  art. 351 e quelle rese al pubblico ministero ex art.    &#13;
 362 del codice di procedura penale da parte di persone  in  grado  di    &#13;
 riferire  notizie  utili per le indagini, con particolare riferimento    &#13;
 all'ipotesi  in  cui,  prima  del  dibattimento,  si  verifichi   per    &#13;
 circostanze imprevedibili la morte del dichiarante;                      &#13;
      che  le  conseguenze  possono essere anche più gravi, rileva il    &#13;
 giudice  a  quo,  qualora  la  deposizione   testimoniale,   divenuta    &#13;
 irripetibile  per morte del dichiarante, sia stata resa dalla persona    &#13;
 offesa dal reato e, a maggior ragione, se tale atto  istruttorio  sia    &#13;
 in concreto l'unica prova di cui disponga l'accusa contro l'imputato:    &#13;
 in  tale  particolare ipotesi, la disciplina in oggetto confligge con    &#13;
 il  diritto  di  tutte  le  parti  offese  ad  un  ugual  trattamento    &#13;
 processuale, in quanto, allorché la persona offesa abbia collaborato    &#13;
 con   la   giustizia   rendendo   dichiarazioni   accusatorie  contro    &#13;
 l'imputato, poi divenute irripetibili per  morte  di  costei,  sembra    &#13;
 profondamente ingiusto, alla luce dell'art. 3 della Costituzione, che    &#13;
 tali  dichiarazioni  possano essere utilizzate processualmente contro    &#13;
 l'imputato solo se rese al pubblico  ministero,  mentre  non  abbiano    &#13;
 alcun valore probatorio se rese alla polizia giudiziaria;                &#13;
      che  tali ultime considerazioni consentono di ritenere, conclude    &#13;
 il remittente, l'illegittimità  costituzionale  della  normativa  in    &#13;
 esame  anche in riferimento all'art. 97 della Costituzione, in quanto    &#13;
 l'attività di indagine svolta dalla polizia giudiziaria ex art.  351    &#13;
 del  codice  di  procedura  penale  può  risultare irrimediabilmente    &#13;
 compromessa a causa  della  sopravvenienza  di  fatti  e  circostanze    &#13;
 oggettivamente  imprevedibili,  con violazione del principio del buon    &#13;
 andamento  della  P.A.,  che  impone   di   valorizzare   l'attività    &#13;
 legittimamemte   compiuta   dagli   organi   amministrativi   per  la    &#13;
 realizzazione degli scopi ad essi affidati dall'ordinamento;             &#13;
      che è intervenuto in  entrambi  i  giudizi  il  Presidente  del    &#13;
 Consiglio   dei   ministri,   concludendo  per  l'infondatezza  delle    &#13;
 questioni;                                                               &#13;
    Considerato che, per  la  sostanziale  identità  delle  questioni    &#13;
 sollevate, i giudizi vanno riuniti ed esaminati congiuntamente;          &#13;
      che  questa  Corte,  con  sentenza n. 24 del 1992, ha dichiarato    &#13;
 l'illegittimità costituzionale  dell'art.  195,  quarto  comma,  del    &#13;
 codice  di procedura penale (nonché della corrispondente parte della    &#13;
 direttiva n. 31 della legge-delega 16 febbraio 1987, n. 81), il quale    &#13;
 prevedeva, per gli ufficiali ed agenti  di  polizia  giudiziaria,  il    &#13;
 divieto  di  rendere  testimonianza  indiretta, cioè di "deporre sul    &#13;
 contenuto delle dichiarazioni acquisite da testimoni";                   &#13;
      che, a causa della  possibile  incidenza  della  caducazione  di    &#13;
 detto   divieto   nei   procedimenti   pendenti  dinanzi  ai  giudici    &#13;
 remittenti, appare opportuno disporre la restituzione degli  atti  ai    &#13;
 medesimi  giudici,  affinché,  alla  luce del nuovo quadro normativo    &#13;
 della materia, valutino  se  le  sollevate  questioni  siano  tuttora    &#13;
 rilevanti;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i giudizi, ordina la restituzione degli atti al Pretore di    &#13;
 Bergamo - sezione distaccata di Clusone,  e  al  Tribunale  di  Busto    &#13;
 Arsizio.                                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 4 marzo 1992.                                 &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: FERRI                             &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 19 marzo 1992.                           &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
