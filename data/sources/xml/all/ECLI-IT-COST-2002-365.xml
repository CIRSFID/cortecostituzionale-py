<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2002</anno_pronuncia>
    <numero_pronuncia>365</numero_pronuncia>
    <ecli>ECLI:IT:COST:2002:365</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>10/07/2002</data_decisione>
    <data_deposito>18/07/2002</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, &#13;
Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità costituzionale dell'art. 500, comma 2, &#13;
del  codice  di  procedura penale, come modificato dall'art. 16 della &#13;
legge  1 marzo 2001, n. 63 (Modifiche al codice penale e al codice di &#13;
procedura  penale  in materia di formazione e valutazione della prova &#13;
in  attuazione  della  legge  costituzionale di riforma dell'art. 111 &#13;
della  Costituzione), promossi con ordinanze emesse il 25 giugno 2001 &#13;
dal Tribunale di Rossano, il 14 giugno 2001 dal Tribunale militare di &#13;
Torino  ed  il  28 settembre  2001  dal  Tribunale  di Castrovillari, &#13;
rispettivamente iscritte ai nn. 931 e 939 del registro ordinanze 2001 &#13;
ed  al  n. 26 del registro ordinanze 2002 e pubblicate nella Gazzetta &#13;
Ufficiale  della Repubblica 1ª serie speciale, nn. 47 e 48, dell'anno &#13;
2001 e n. 5, dell'anno 2002. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 22 maggio 2002 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto  che  con  due  ordinanze di identico contenuto, emesse, &#13;
rispettivamente,  il  25 giugno  2001  (r.o.  n. 931  del 2001) ed il &#13;
28 settembre  2001  (r.o. n. 26 del 2002), il Tribunale di Rossano ed &#13;
il  Tribunale  di  Castrovillari  hanno  sollevato, in relazione agli &#13;
artt. 3,  24,  primo  comma,  111,  primo e quarto comma, e 112 della &#13;
Costituzione, questione di legittimità costituzionale dell'art. 500, &#13;
comma  2,  del  codice  di  procedura penale, "nella parte in cui non &#13;
prevede  che  le  dichiarazioni  lette  per  la contestazione possano &#13;
essere  acquisite al fascicolo del dibattimento e valutate come prova &#13;
dei fatti affermati"; &#13;
        che  la  norma  censurata  violerebbe, innanzitutto, l'art. 3 &#13;
Cost., sotto il profilo dell'irragionevolezza dell'attuale sistema di &#13;
assunzione  e  di  valutazione  della  prova  nel processo penale, in &#13;
quanto  farebbe  dipendere  le  risultanze  probatorie  da  "fenomeni &#13;
soggettivi  extraprocessuali  (come  la capacità o meno di ricordare &#13;
del teste)"; &#13;
        che  sarebbe  altresì compromesso il diritto di difesa della &#13;
persona  offesa,  con  violazione  dell'art. 24,  primo comma, Cost., &#13;
risultando  inconsistente  l'effettività  della  tutela  dei diritti &#13;
della  parte  civile  costituita, per le ipotesi di testi reticenti o &#13;
che  affermano di non ricordare il contenuto delle dichiarazioni rese &#13;
nella fase delle indagini preliminari; &#13;
        che  risulterebbe violato, ancora, l'art. 111, primo e quarto &#13;
comma, Cost., sotto il profilo del contrasto con il "principio di non &#13;
dispersione  dei  mezzi  di  prova"  -  affermato da questa Corte con &#13;
sentenza  n. 255  del  1992  -  anch'esso costituente espressione del &#13;
"giusto  processo": e ciò in quanto la dichiarazione resa nella fase &#13;
delle   indagini   preliminari,   attraverso   il   meccanismo  delle &#13;
contestazioni,  diverrebbe  oggetto  di  pieno contraddittorio tra le &#13;
parti  e,  dunque,  "parte  essenziale  di un procedimento probatorio &#13;
ispirato al modello costituzionale"; &#13;
        che la norma censurata contrasterebbe, infine, con l'art. 112 &#13;
Cost., in quanto, introducendo limitazioni probatorie di portata tale &#13;
da privare di efficacia la legge penale, vanificherebbe il diritto di &#13;
azione   "privando   di   effettiva   tutela  i  diritti  inviolabili &#13;
riconosciuti dalla Costituzione e salvaguardati dalla legge penale"; &#13;
        che,  con ordinanza emessa il 14 giugno 2001 (r.o. n. 939 del &#13;
2001),  il  Tribunale militare di Torino ha sollevato, in riferimento &#13;
agli  artt. 3  e  101  della  Costituzione, questione di legittimità &#13;
costituzionale  dell'art. 500,  comma  2,  del  codice  di  procedura &#13;
penale,  "nella  parte  in cui non prevede che le dichiarazioni lette &#13;
per   le   contestazioni   possano  essere  valutate  ai  fini  della &#13;
credibilità  del  teste e come prova dei fatti in esse affermati, se &#13;
sussistono altri elementi che ne confermano la attendibilità"; &#13;
        che,  a parere del rimettente, la norma censurata si porrebbe &#13;
in contrasto con l'art. 3 Cost., sotto il profilo della irragionevole &#13;
diseguaglianza  di  trattamento  tra imputati in relazione al tipo di &#13;
processo  prescelto dall'indagato, poiché nel caso di opzione per il &#13;
rito  abbreviato tutte le dichiarazioni rese nel corso delle indagini &#13;
preliminari avrebbero piena utilizzazione probatoria, a differenza di &#13;
quanto  previsto  per il rito ordinario: con la possibilità, quindi, &#13;
"di  decisioni  giudiziali opposte, in presenza degli stessi elementi &#13;
di fatto conosciuti dal giudice"; &#13;
        che  sarebbe  altresì  compromesso  il  principio del libero &#13;
convincimento  del  giudice  espresso nell'art. 101 Cost., atteso che &#13;
l'organo giudicante, anche quando si convinca della veridicità delle &#13;
dichiarazioni   rese   in  fase  di  indagini,  non  può  pienamente &#13;
utilizzarle a fini probatori e, dunque, si vede costretto a formulare &#13;
in   sentenza   "affermazioni   del   tutto   contrarie   al  proprio &#13;
convincimento motivatamente raggiunto"; &#13;
        che  nei  giudizi  di  costituzionalità  è  intervenuto  il &#13;
Presidente   del  Consiglio  dei  ministri,  rappresentato  e  difeso &#13;
dall'Avvocatura  generale  dello  Stato,  il  quale ha chiesto che le &#13;
questioni siano dichiarate infondate. &#13;
    Considerato  che  le  ordinanze di rimessione sollevano questioni &#13;
fra loro del tutto analoghe e che, pertanto, i relativi giudizi vanno &#13;
riuniti per essere definiti con un'unica decisione; &#13;
        che,  al di là della varietà dei parametri invocati e delle &#13;
singole  scansioni  argomentative,  tutti  i  profili investiti dalle &#13;
questioni  in  esame  risultano  già ampiamente scrutinati da questa &#13;
Corte con l' ordinanza n. 36 del 2002; &#13;
        che   in  tale  pronuncia  questa  Corte  ha  in  particolare &#13;
rimarcato  "come  l'art. 111  della  Costituzione abbia espressamente &#13;
attribuito  risalto  costituzionale al principio del contraddittorio, &#13;
anche  nella  prospettiva  della impermeabilità del processo, quanto &#13;
alla  formazione  della  prova,  rispetto  al  materiale  raccolto in &#13;
assenza    della    dialettica    delle   parti":   con   conseguente &#13;
predisposizione,   per   la  fase  del  dibattimento,  di  meccanismi &#13;
normativi  idonei  alla  salvaguardia  "da  contaminazioni probatorie &#13;
fondate  su  atti  unilateralmente  raccolti nel corso delle indagini &#13;
preliminari" (cfr., oltre la già citata ordinanza n. 36 del 2002, la &#13;
sentenza n. 32 del 2002); &#13;
        che,  in  tale prospettiva, appare quindi pienamente coerente &#13;
con   i  principi  sanciti  dalla  citata  norma  costituzionale  che &#13;
l'istituto   delle  contestazioni  non  operi  "quale  meccanismo  di &#13;
acquisizione  illimitato ed incondizionato" di dichiarazioni raccolte &#13;
prima  ed  al di fuori del contraddittorio: esigenza, questa, che "la &#13;
composita  disciplina  dettata  dall'art. 500  del  codice di rito ha &#13;
soddisfatto  con  l'attuale  formulazione, prevedendo, da un lato, un &#13;
parametro    di   valutazione   oggettivamente   circoscritto   delle &#13;
dichiarazioni  lette  per  le contestazioni e, dall'altro, ipotesi di &#13;
eccezionale utilizzabilità pleno iure"; &#13;
        che pertanto la norma censurata, espressione di una precisa e &#13;
ragionevole  opzione  del  legislatore  in  attuazione  dei  principi &#13;
sanciti  nel  novellato  art. 111  della  Costituzione,  non  risulta &#13;
neppure  in  contrasto  -  come  parimenti  evidenziato  nella citata &#13;
ordinanza  n. 36  del  2002  - con gli altri parametri invocati dagli &#13;
odierni  rimettenti:  non  con  quello  del  libero convincimento del &#13;
giudice,  poiché  detto principio "non può che riferirsi alle prove &#13;
legittimamente   formate   ed   acquisite";   non  con  quelli  della &#13;
obbligatorietà  dell'azione  penale e dell'effettività della tutela &#13;
giurisdizionale  dei diritti, poiché essi non possono ritenersi lesi &#13;
da  limiti  di  utilizzazione  probatoria che si configurano "come la &#13;
naturale  e coerente conseguenza di scelte sistematiche, in linea con &#13;
i   principi  costituzionali";  non  con  quello  di  eguaglianza  in &#13;
relazione  al  diverso  regime  di  utilizzazione probatoria nel rito &#13;
abbreviato,  avuto  riguardo  alle evidenti peculiarità di tale rito &#13;
speciale;  e  neppure,  infine,  con  il  principio  dell'obbligo  di &#13;
motivazione,  essenzialmente  inteso  come  esigenza  di  un prodotto &#13;
argomentativo  coerente  e  privo  di  vizi logici: invero, "i limiti &#13;
probatori  relativi alle dichiarazioni lette per le contestazioni non &#13;
incidono  affatto  sulla coerenza intrinseca della motivazione che il &#13;
giudice  è chiamato a svolgere [...] posto che, ove così non fosse, &#13;
[...]  qualsiasi prova non utilizzabile (perché, ad esempio, assunta &#13;
contro  i divieti previsti dalla legge) comprometterebbe l'obbligo di &#13;
motivazione, per il sol fatto di essere apparsa "persuasiva" nel foro &#13;
interno del giudicante"; &#13;
        che, pertanto, le questioni proposte devono essere dichiarate &#13;
manifestamente infondate. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    Dichiara   la   manifesta   infondatezza   delle   questioni   di &#13;
legittimità  costituzionale  dell'art. 500,  comma  2, del codice di &#13;
procedura  penale,  sollevate, in riferimento agli artt. 3, 24, primo &#13;
comma,  101,  111, primo e quarto comma, e 112 della Costituzione dal &#13;
Tribunale  di Rossano, dal Tribunale di Castrovillari e dal Tribunale &#13;
militare di Torino, con le ordinanze in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 10 luglio 2002. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                       Il cancelliere:Di Paola &#13;
    Depositata in cancelleria il 18 luglio 2002. &#13;
               Il direttore della cancelleria:Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
