<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>459</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:459</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CAIANIELLO</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/10/1995</data_decisione>
    <data_deposito>26/10/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Vincenzo CAIANIELLO; &#13;
 Giudici: avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco &#13;
 GUIZZI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. &#13;
 Gustavo ZAGREBELSKY;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 13 della legge    &#13;
 della Regione Basilicata 16  febbraio  1987,  n.  2  (Disciplina  dei    &#13;
 criteri  generali per l'assegnazione e la fissazione dei canoni degli    &#13;
 alloggi di edilizia residenziale  pubblica)  promosso  con  ordinanza    &#13;
 emessa  il  28 marzo 1995 dal Pretore di Potenza sul ricorso proposto    &#13;
 da Tofalo Francesco contro il Sindaco del Comune di Balvano, iscritta    &#13;
 al n. 439 del registro ordinanze 1995  e  pubblicata  nella  Gazzetta    &#13;
 Ufficiale  della  Repubblica  n.  35, prima serie speciale, dell'anno    &#13;
 1995;                                                                    &#13;
    Udito nella camera di consiglio del 18  ottobre  1995  il  Giudice    &#13;
 relatore Vincenzo Caianiello.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Nel  corso di un giudizio proposto avverso un decreto sindacale di    &#13;
 decadenza dalla assegnazione di  alloggio  di  edilizia  residenziale    &#13;
 pubblica,  il Pretore di Potenza, con ordinanza del 28 marzo 1995, ha    &#13;
 sollevato questione di legittimità costituzionale dell'art. 13 della    &#13;
 legge della Regione Basilicata 16 febbraio 1987, n. 2 (Disciplina dei    &#13;
 criteri generali per l'assegnazione e la fissazione dei canoni  degli    &#13;
 alloggi  di  edilizia  residenziale  pubblica) che, nel prevedere che    &#13;
 contro il  provvedimento  del  Sindaco  l'interessato  può  proporre    &#13;
 ricorso  al  Pretore  - così riproducendo la disposizione statale di    &#13;
 cui al tredicesimo comma dell'art. 11 del d.P.R. 30 dicembre 1972, n.    &#13;
 1035 - si porrebbe in contrasto con l'art.  108  della  Costituzione,    &#13;
 perché  disciplinerebbe  una  materia  oggetto  di  riserva di legge    &#13;
 statale in quanto attinente alla giurisdizione.<diritto>Considerato in diritto</diritto>1. - Viene  sollevata  questione  di  legittimità  costituzionale    &#13;
 dell'art.  13  della legge della Regione Basilicata 16 febbraio 1987,    &#13;
 n. 2  (Disciplina  dei  criteri  generali  per  l'assegnazione  e  la    &#13;
 fissazione   dei   canoni  degli  alloggi  di  edilizia  residenziale    &#13;
 pubblica),  nella parte in cui prevede che contro il provvedimento di    &#13;
 decadenza dalla assegnazione di  alloggio  di  edilizia  residenziale    &#13;
 pubblica  "l'interessato  può  proporre ricorso al Pretore del luogo    &#13;
 nel cui mandamento è situato l'alloggio". Si  assume  nell'ordinanza    &#13;
 di  rimessione  che  la norma regionale, che riproduce il disposto di    &#13;
 cui al tredicesimo comma dell'art. 11 del d.P.R. 30 dicembre 1972, n.    &#13;
 1035, violi l'art. 108 della Costituzione e quindi  il  principio  di    &#13;
 riserva di legge statale in materia di giurisdizione.                    &#13;
    2.  -  Va  premesso  che  la  censura deve essere riferita al solo    &#13;
 undicesimo comma dell'art. 13 della legge regionale in esame, che  è    &#13;
 quello   nel   quale  è  contenuta  la  disposizione  esplicitamente    &#13;
 sottoposta al giudizio di costituzionalità.                             &#13;
    3. - La questione è fondata.                                         &#13;
    Secondo la costante giurisprudenza costituzionale è precluso alle    &#13;
 regioni di dettare norme che, come quella impugnata, prevedano rimedi    &#13;
 giurisdizionali, in quanto la  materia  processuale  e  quella  della    &#13;
 giurisdizione  è  riservata  dall'art.  108  della Costituzione alla    &#13;
 esclusiva competenza del legislatore statale (v. ex plurimis sentenze    &#13;
 nn. 457 e 303 del 1994, 210 del 1993, 505 e 489  del  1991,  594  del    &#13;
 1990, 727 del 1988, 81 del 1976).                                        &#13;
    La  violazione  del  suddetto  parametro  costituzionale  non può    &#13;
 nemmeno essere esclusa sulla base del rilievo che la norma  regionale    &#13;
 impugnata  si  è  limitata  a  riprodurre  la  disposizione  statale    &#13;
 contenuta nell'art. 11, tredicesimo comma,  del  d.P.R.  30  dicembre    &#13;
 1972,  n.  1035,  perché  le  regioni in nessun caso possono emanare    &#13;
 leggi in materie soggette a riserva  di  legge  statale,  comportando    &#13;
 ciò  un'indebita novazione della fonte con la forza e le conseguenze    &#13;
 che ne derivano (sentenze nn. 457 del 1994, 210 del 1993, 615  e  203    &#13;
 del 1987, 128 del 1963).                                                 &#13;
    Deve  pertanto  essere dichiarata la illegittimità costituzionale    &#13;
 della norma denunciata.                                                  &#13;
    4. - Inoltre, poiché l'art. 13 cit. riproduce, al dodicesimo e al    &#13;
 tredicesimo   comma,   le   omologhe   disposizioni   contenute   nel    &#13;
 quattordicesimo  e  nel quindicesimo comma dell'art. 11 del d.P.R. n.    &#13;
 1035 del 1972 cit. - e cioè che "il pretore  adito  ha  facoltà  di    &#13;
 sospendere  l'esecuzione  del  decreto"  e  che  "il provvedimento di    &#13;
 sospensione può essere dato dal pretore  con  decreto  in  calce  al    &#13;
 ricorso"  -  e  poiché dette norme, una volta dichiarata illegittima    &#13;
 quella  contenuta  nell'undicesimo  comma  del  medesimo   art.   13,    &#13;
 resterebbero  prive  di  autonomo  significato,  essendo strettamente    &#13;
 conseguenziali a quella annullata ed attenendo  anch'esse  a  materia    &#13;
 processuale  di  riserva  statale,  deve, ai sensi dell'art. 27 della    &#13;
 legge  11  marzo  1953   n.   87,   dichiararsi   la   illegittimità    &#13;
 costituzionale  anche  dei  citati  dodicesimo  e  tredicesimo  comma    &#13;
 dell'art. 13 della legge regionale in esame.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara:                                                              &#13;
       a)  la  illegittimità  costituzionale  dell'undicesimo   comma    &#13;
 dell'art. 13 della legge della Regione Basilicata 16 febbraio 1987 n.    &#13;
 2 (Disciplina dei criteri generali per l'assegnazione e la fissazione    &#13;
 dei canoni degli alloggi di edilizia residenziale pubblica);             &#13;
       b)  in  applicazione  dell'art. 27 della legge 11 marzo 1953 n.    &#13;
 87, l'illegittimità costituzionale del dodicesimo e del  tredicesimo    &#13;
 comma dell'art. 13 della stessa legge regionale.                         &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 ottobre 1995.                              &#13;
                 Il Presidente e redattore: CAIANIELLO                    &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 26 ottobre 1995.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
