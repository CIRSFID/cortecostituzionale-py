<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="sentenza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-04-29/84/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-04-29/84"/>
          <FRBRalias value="ECLI:IT:COST:1982:84" name="ECLI"/>
          <FRBRdate date="1982-04-29" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="84"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-04-29/84/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-04-29/84/ita@"/>
          <FRBRdate date="1982-04-29" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/sentenza/corteCostituzionale/1982-04-29/84/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/sentenza/corteCostituzionale/1982-04-29/84/ita@.xml"/>
          <FRBRdate date="1982-04-29" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="1982-04-16" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="1982-04-29" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>unknown</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>unknown</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="ELIA" href="/akn/ontology/person/it/ELIA" showAs="ELIA"/>
        <TLCPerson eId="leopoldoElia" href="/akn/ontology/person/it/leopoldoElia" showAs="LEOPOLDO ELIA"/>
        <TLCPerson eId="micheleRossano" href="/akn/ontology/person/it/micheleRossano" showAs="MICHELE ROSSANO"/>
        <TLCPerson eId="antoninoDeStefano" href="/akn/ontology/person/it/antoninoDeStefano" showAs="ANTONINO DE STEFANO"/>
        <TLCPerson eId="guglielmoRoehrssen" href="/akn/ontology/person/it/guglielmoRoehrssen" showAs="GUGLIELMO ROEHRSSEN"/>
        <TLCPerson eId="oronzoReale" href="/akn/ontology/person/it/oronzoReale" showAs="ORONZO REALE"/>
        <TLCPerson eId="albertoMalagugini" href="/akn/ontology/person/it/albertoMalagugini" showAs="ALBERTO MALAGUGINI"/>
        <TLCPerson eId="livioPaladin" href="/akn/ontology/person/it/livioPaladin" showAs="LIVIO PALADIN"/>
        <TLCPerson eId="virgilioAndrioli" href="/akn/ontology/person/it/virgilioAndrioli" showAs="VIRGILIO ANDRIOLI"/>
        <TLCPerson eId="giuseppeFerrari" href="/akn/ontology/person/it/giuseppeFerrari" showAs="GIUSEPPE FERRARI"/>
        <TLCPerson eId="francescoSaja" href="/akn/ontology/person/it/francescoSaja" showAs="FRANCESCO SAJA"/>
        <TLCPerson eId="giovanniConso" href="/akn/ontology/person/it/giovanniConso" showAs="GIOVANNI CONSO"/>
        <TLCPerson eId="giovanniVitale" href="/akn/ontology/person/it/giovanniVitale" showAs="GIOVANNI VITALE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>1982</akn4cc:anno>
        <akn4cc:numero>84</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:1982:84</akn4cc:ecli>
        <akn4cc:tipo>S</akn4cc:tipo>
        <akn4cc:presidente>ELIA</akn4cc:presidente>
        <akn4cc:relatore>Virgilio Andrioli</akn4cc:relatore>
        <akn4cc:data_decisione>1982-04-16</akn4cc:data_decisione>
        <akn4cc:data_deposito>1982-04-29</akn4cc:data_deposito>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori: Prof. <judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge>, Presidente - Dott. <judge refersTo="#micheleRossano">MICHELE ROSSANO</judge> - Prof. <judge refersTo="#antoninoDeStefano">ANTONINO DE STEFANO</judge> - Prof. <judge refersTo="#guglielmoRoehrssen">GUGLIELMO 
 ROEHRSSEN</judge> - Avv. <judge refersTo="#oronzoReale">ORONZO REALE</judge> - Dott. BRUNETTO BUCCIARELLI DUCCI - 
 Avv. <judge refersTo="#albertoMalagugini">ALBERTO MALAGUGINI</judge> - Prof. <judge refersTo="#livioPaladin">LIVIO PALADIN</judge> - Prof. <judge refersTo="#virgilioAndrioli">VIRGILIO ANDRIOLI</judge> 
 - Prof. <judge refersTo="#giuseppeFerrari">GIUSEPPE FERRARI</judge> - Dott. <judge refersTo="#francescoSaja">FRANCESCO SAJA</judge> - Prof. <judge refersTo="#giovanniConso">GIOVANNI 
 CONSO</judge>, Giudici,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>SENTENZA</docType>
        </p>
        <paragraph>
          <list>
            <intro>
              <p>nei  giudizi  riuniti  di legittimità costituzionale dell'<mref><ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_1">art. 19,  
 commi 1</ref> e <ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_2">2, della  legge  24  dicembre  1969,  n.  990</ref></mref>  (Assicurazione  
 obbligatoria  della  responsabilità  civile dei veicoli a motore e dei  
 natanti), promossi con le seguenti ordinanze:</p>
            </intro>
            <point>
              <num>1)</num>
              <content>
                <p> ordinanza emessa il 18 gennaio 1977 dal pretore  di  Padova  nel  
 procedimento  penale a carico di Vettorato Luigi, iscritta al n. 93 del  
 registro ordinanze 1977 e pubblicata  nella  Gazzetta  Ufficiale  della  
 Repubblica n. 100 del 13 aprile 1977;</p>
              </content>
            </point>
            <point>
              <num>2)</num>
              <content>
                <p>  ordinanza  emessa  il  25  ottobre 1978 dal pretore di Bari nel  
 procedimento  civile  vertente  tra  Impedovo  Sebastiano  e   Nicolosi  
 Giuseppina  ed  altri, iscritta al n. 341 del registro ordinanze 1979 e  
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  175  del  27  
 giugno 1979;</p>
              </content>
            </point>
            <point>
              <num>3)</num>
              <content>
                <p>  ordinanza  emessa il 29 maggio 1980 dal Tribunale di Padova nel  
 procedimento civile vertente tra Berto  Sergio  e  Varotto  Luigina  ed  
 altra,  iscritta  al  n.  675  del registro ordinanze 1980 e pubblicata  
 nella Gazzetta Ufficiale della Repubblica n. 311 del 12 novembre 1980.</p>
              </content>
            </point>
          </list>
        </paragraph>
        <paragraph>
          <content>
            <p>Visti l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> dell'Assicuratrice Italiana s.p.a.   e  
 gli atti di intervento del Presidente del Consiglio dei ministri;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito  nell'udienza  pubblica del 10 marzo 1982 il Giudice relatore  
 Virgilio Andrioli;</p>
          </content>
        </paragraph>
        <paragraph>
          <content>
            <p>udito  l'avvocato  dello  Stato Renato Carafa per il Presidente del  
 Consiglio dei ministri.</p>
          </content>
        </paragraph>
      </introduction>
      <background>
        <division>
          <heading>Ritenuto in fatto:</heading>
          <paragraph>
            <num>1.</num>
            <content>
              <p> - Nel procedimento penale a carico di Vettorato  Luigi  imputato  
 per il reato di lesioni colpose in danno di Cavraro Giuseppe, cagionate  
 dal Vettorato medesimo mentre era alla guida del ciclomotore di 50 cc.,  
 per  il  quale  aveva  contratto  assicurazione  per la responsabilità  
 civile presso la Compagnia  Centrale  di  Assicurazione  poi  posta  in  
 liquidazione  coatta  amministrativa,  il  Cavraro,  costituitosi parte  
 civile,  citò  in  veste  di  responsabile   civile   il   Commissario  
 liquidatore  della  Compagnia,  il quale chiese di essere estromesso in  
 quanto il risarcimento, previsto dall'<ref href="/akn/it/act/legge/stato/1969-12-04/2/!main#art_19">art.  19 l. 24 dicembre  1969</ref>  n.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>990</num>
            <content>
              <p>,  ipotizza  che  il danno sia stato cagionato dalla circolazione di  
 veicoli per i quali sussiste l'obbligo dell'assicurazione  (presupposto  
 insussistente  per il ciclomotore dell'imputato). Il Pretore di Padova,  
 con ordinanza emessa il 18 gennaio 1977 (comunicata l'8 e notificata il  
 24 del successivo mese di febbraio, pubblicata nella G.U. n. 100 del 13  
 aprile 1977 e iscritta al n. 93  R.O.    1977),  sollevò  d'ufficio  e  
 giudicò  non  manifestamente  infondata  la  questione di legittimità  
 costituzionale dell'<mref><ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_1">art.  19 comma 1</ref> <ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_c">c) l. 24 dicembre 1969 n. 990</ref></mref>,  in  
 riferimento  all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.  3  Cost.</ref>,  nella  parte  in  cui  non prevede il  
 risarcimento dei danni - a carico del Fondo di garanzia per le  vittime  
 della  strada  -  cagionato  dalla  circolazione  di  veicoli  che, non  
 soggetti all'obbligo di assicurazione per responsabilità civile, siano  
 nondimeno assicurati presso imprese che,  al  momento  del  sinistro  o  
 successivamente,  siano  soggette a liquidazione coatta amministrativa,  
 perché, potendo anche i veicoli esclusi  dall'obbligo  arrecare  danni  
 con   la   loro   circolazione,   sarebbe,   a  giudizio  del  Pretore,  
 irragionevole discriminare tra coloro che sono tenuti all'assicurazione  
 obbligatoria  e  coloro  che,  per  contrarre  assicurazione  pur   non  
 essendovi tenuti, dimostrano maggiore previdenza, e, a maggior ragione,  
 tra  vittime  di  sinistri, delle quali sono garantite dal Fondo quelle  
 coinvolte in sinistri provocati  da  veicoli  non  identificati  o  non  
 coperti d'assicurazione e ne sono escluse quelle danneggiate da veicoli  
 coperti da assicurazione, sia pure non obbligatoria, presso impresa poi  
 assoggettata a liquidazione coatta amministrativa.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Avanti  la  Corte nessuna delle parti si è costituita; ha spiegato  
 intervento il Presidente del Consiglio dei ministri con atto depositato  
 il 3 maggio 1977, con il quale l'Avvocatura  generale  dello  Stato  ha  
 concluso  per  la infondatezza della proposta questione argomentando da  
 ciò  che  l'esclusione   di   determinati   autoveicoli   dall'obbligo  
 dell'assicurazione  per  un  verso  giustifica la loro esclusione dalla  
 partecipazione al Fondo e per altro verso determina la irrilevanza - ai  
 fini  del   prospettato   reinserimento   -   della   stipulazione   di  
 assicurazione volontaria.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>2.</num>
            <content>
              <p>  -  Nel  giudizio,  introdotto  avanti  il  Pretore  di Bari con  
 citazione  notificata  i  18/19  aprile  1978  con  la  quale  Impedovo  
 Sebastiano aveva chiesto il risarcimento del danno subito dalla propria  
 vettura  in  incidente  stradale  cagionato  da  Nicolosi Giuseppina in  
 Mirenghi e quantificato in  lire  510.000;  anche  nel  contraddittorio  
 della  Columbia  Assicurazioni  s.p.a.,  posta  in  liquidazione coatta  
 amministrativa, dell'Assicuratrice Italiana  s.p.a.  designata  per  la  
 liquidazione dei sinistri a carico del Fondo di garanzia e dell'INA, si  
 costituirono  la  Assicuratrice  Italiana  e  la  Nicolosi, che eccepì  
 l'incostituzionalità dell'<ref href="/akn/it/act/legge/stato/1969/990/!main#art_19">art. 19 l.   990/1969</ref>  nella  parte  in  cui  
 prevede  l'obbligo  risarcitorio,  in  caso di impresa assicuratrice in  
 l.c.a. "per i danni ... alle cose il cui ammontare sia superiore a lire  
 100.000 e per la parte eccedente  tale  ammontare".  Eccezione  accolta  
 dall'adito  Pretore con ordinanza emessa il 25 ottobre 1978, comunicata  
 il 19 dicembre dello  stesso  anno  e  notificata  il  14  marzo  1979,  
 pubblicata  nella  G.U.  n. 175 del 27 giugno 1979 e iscritta al n. 341  
 R.O. 1979,  con  la  quale  giudicò  rilevante  e  non  manifestamente  
 infondata la questione in tali termini prospettata, non solo perché è  
 irrazionale la discriminazione tra assicurati presso compagnia in bonis  
 e assicurati presso compagnia poi messa in l.c.a., ma anche perché non  
 meno  irrazionale è la esclusione, nell'interno della prima categoria,  
 di coloro che hanno cagionato un danno inferiore a lire 100.000 o danni  
 di  misura  maggiore  per  la  parte  non  eccedente   tale   ammontare  
 (discriminazione  che,  sempre  a giudizio del Pretore, si risolverebbe  
 per la parte non coperta in discriminazione per  i  sinistri  di  minor  
 entità e per i ceti meno abbienti).</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Avanti  la Corte si è costituito nell'interesse dell'Assicuratrice  
 Italiana s.p.a. l'avv. Giorgio Spadafora in virtù di procura  speciale  
 in  margine  all'atto  di  deduzioni  depositato il 17 luglio 1979, nel  
 quale ha concluso per la infondatezza della proposta questione muovendo  
 dalla esplicitazione delle finalità  perseguite  dal  legislatore  con  
 l'istituzione del Fondo di garanzia, che si svolgono all'unisono con le  
 operatività  del medesimo, di cui all'art. 19 a) e b), sotto l'insegna  
 di criteri di razionalità  che  non  sono  offesi  dall'esclusione  di  
 talune  categorie di veicoli e argomentando con specifico riguardo alla  
 misura del danno - tra l'altro - dalla relazione Andreotti alla Camera.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Per la infondatezza ha concluso anche il Presidente del  Consiglio  dei  
 ministri  intervenendo  con  atto  depositato il 17 luglio 1979, con il  
 quale l'Avvocatura generale dello Stato ha posto in rilievo  la  scarsa  
 illustrazione,   nella   ordinanza  di  rimessione,  delle  conseguenze  
 dell'applicazione al caso concreto della disposizione impugnata, che si  
 convertirebbe in dubbia rilevanza della questione  e,  nel  merito,  ha  
 chiarito  che le diversità di situazioni (intervento del Fondo in base  
 ad un principio di solidarietà sociale  e  puro  e  semplice  rapporto  
 assicurativo   contrattuale),   giustificherebbe   nel  primo  caso  la  
 fissazione di  un  limite,  a  sua  volta  coordinato  ad  un'obiettiva  
 esigenza di nazionalizzazione e di economia del servizio.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>3.</num>
            <content>
              <p>  -  Nel  giudizio promosso da Berto Sergio con atto di citazione  
 notificato i 3 e 4 marzo 1979 a Varotto Luigina che gli aveva  prodotto  
 il  danno nell'incidente stradale provocato con il motociclo di cui era  
 alla guida, e alla Compagnia di Assicurazione Concordia per conseguirne  
 la condanna in solido al risarcimento (giudizio  interrotto  a  seguito  
 della   messa   della   convenuta   Compagnia  in  liquidazione  coatta  
 amministrativa  e  poi  riassunto   nei   confronti   del   Commissario  
 liquidatore  della  Compagnia  medesima, autorizzato a provvedere anche  
 per conto del Fondo di garanzia per  le  vittime  della  strada  e,  in  
 deroga  all'<ref href="/akn/it/act/legge/stato/1969/990/!main#art_19__para_3">art.  19  comma  3 l. 990/1969</ref>, alla liquidazione dei danni  
 verificatisi anteriormente alla pubblicazione del decreto di  messa  in  
 l.c.a.),  l'adito Tribunale di Padova, in accoglimento di eccezione del  
 Berto e  della  Varotto,  dichiarò  non  manifestamente  infondata  la  
 questione  di  legittimità  costituzionale  dell'art.  19  comma  1 l.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>990</num>
            <content>
              <p>/1969 in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art.  3  Cost.</ref>  (disposizione  impugnata  e  
 norma-parametro  indicate  soltanto  nella  motivazione  dell'ordinanza  
 collegiale  per  relationem  alla  motivazione  con  cui   il   giudice  
 istruttore  aveva rimesso la causa al collegio) con ordinanza emessa il  
 29 maggio 1980  (notificata  e  comunicata  il  successivo  25  giugno,  
 pubblicata  nella G.U. n. 311 del 12 novembre 1980 e iscritta al n. 675  
 R.O. 1980), in cui ravvisò la violazione del principio di  uguaglianza  
 in  ciò  che  -  in  caso  d'insolvenza  della impresa assicuratrice -  
 verrebbero pregiudicati i danneggiati da veicoli a motore che, pur  non  
 soggetti  per  la  minore  cilindrata  ad  obbligo  assicurativo,  sono  
 occasione di pericolo al punto di indurne  i  proprietari  a  stipulare  
 volontariamente l'assicurazione della responsabilità civile. Avanti la  
 Corte  nessuna  delle parti si è costituita; ha spiegato intervento il  
 Presidente del  Consiglio  dei  ministri  con  atto  depositato  il  25  
 novembre  1980,  con  il quale l'Avvocatura generale dello Stato, sulla  
 premessa che la minore pericolosità della  circolazione  dei  veicoli,  
 esonerati  dall'assicurazione  obbligatoria, integrerebbe diversità di  
 situazione tale da escludere nel non previsto intervento del Fondo ogni  
 carattere  discriminatorio,  ha  concluso  per  la  infondatezza  della  
 proposta questione.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>4.</num>
            <content>
              <p>  -  Alla  pubblica  udienza  del  10  marzo 1982, nella quale il  
 giudice Andrioli ha svolto la relazione, l'avvocato dello Stato  Carafa  
 si  è  rimesso  allo scritto; la difesa della costituita Assicuratrice  
 Italiana s.p.a. non si è presentata.</p>
            </content>
          </paragraph>
        </division>
      </background>
      <motivation>
        <division>
          <heading>Considerato in diritto:</heading>
          <paragraph>
            <num>5.</num>
            <content>
              <p> - La  connessione  delle  questioni  prospettate  giustifica  la  
 riunione dei tre procedimenti di cui formano oggetto.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>6.</num>
            <content>
              <p>  -  Dispone  l'<ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19">art. 19 l. 24 dicembre 1969 n. 990</ref> (assicurazione  
 obbligatoria della responsabilità civile derivante dalla  circolazione  
 dei  veicoli  a motore e dei natanti) che "è costituito presso l'INA un  
 "Fondo di garanzia per le vittime della strada" per il risarcimento dei  
 danni causati dalla circolazione dei veicoli o dei natanti per i  quali  
 a norma della presente legge vi è obbligo di assicurazione nei casi in  
 cui: ... c) il veicolo o natante risulti assicurato con polizza facente  
 parte del portafoglio italiano, presso una impresa la quale, al momento  
 del  sinistro,  si  trovi  in  stato di liquidazione coatta, o vi venga  
 posta successivamente" (comma 1) e  che  "Nella  ipotesi  di  cui  alla  
 lettera  c)  è  dovuto  il  risarcimento  per i danni alle cose il cui  
 ammontare sia superiore a lire 100.000 e per la  parte  eccedente  tale  
 ammontare"  (comma  2,  periodo  2).  Soggiunge  l'art. 5 comma 1 della  
 stessa legge che "non v'è obbligo  di  assicurazione  ai  sensi  della  
 presente  legge  per  i  ciclomotori  che  non siano muniti di targa di  
 riconoscimento (e per le macchine agricole)".</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Le  riprodotte  disposizioni  hanno  offerto  occasione  alle   due  
 questioni di legittimità, nelle quali viene assunto a parametro l'art.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>3  Cost.  e  si  denuncia violazione del principio di uguaglianza e del  
 criterio  direttivo  di  ragionevolezza  che  la  norma  costituzionale  
 garantisce,  e,  pertanto,  è  denunciata  la  incostituzionalità  I)  
 dell'art. 19 comma 1 c) nella parte in cui non estende il  risarcimento  
 da parte del Fondo di garanzia per le vittime della strada - in caso di  
 insolvenza  dell'impresa  assicuratrice  -  anche alla ipotesi di danni  
 cagionati da veicoli non  soggetti  ad  assicurazione  obbligatoria  ma  
 tuttavia  assicurati,  e  II)  dell'art.  19  comma 2 nella parte nella  
 quale, con riferimento all'ipotesi di cui alla lettera c) del comma  1,  
 esclude  dal  risarcimento  da  parte  del  Fondo  di  danni  che siano  
 inferiori a 100.000 lire.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>7.</num>
            <content>
              <p>1. - Né l'una né l'altra questione sono fondate.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>7.</num>
            <list>
              <alinea>
                <content>
                  <p>2.  -  In merito alla prima giova rilevare che il Fondo gestione,  
 come risulta anche dal regolamento di  esecuzione  della  <ref href="/akn/it/act/legge/stato/1969/990/!main">l.  990/1969</ref>,  
 appr.  con  <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1970-11-24/973/!main">d.P.R.  24  novembre  1970 n. 973</ref> (G.U. 14 dicembre 1970 n.</p>
                </content>
              </alinea>
              <point>
                <num>315)</num>
                <content>
                  <p>, è costituito da prelievi, cui le imprese di  assicurazione  sono  
 tenute  limitatamente  ai  contratti  stipulati con riferimento a danni  
 soggetti ad obbligo di  assicurazione,  talché  la  partecipazione  al  
 Fondo  di contratti riferiti a danni che da tale obbligo sono esenti si  
 risolverebbe in violazione del principio di uguaglianza in  pregiudizio  
 (non  già  di coloro che riportano danni esenti dall'obbligo, sibbene)  
 di coloro che soffrono danni che all'obbligo sono soggetti.</p>
                </content>
              </point>
            </list>
          </paragraph>
          <paragraph>
            <content>
              <p>La circostanza che proprietari di veicoli, pur non essendo astretti  
 al   ripetuto   obbligo,   stipulino   spontaneamente   contratti    di  
 assicurazione   per   coprire   entro   certi  limiti  i  rischi  della  
 circolazione, è priva di rilievo perché tali contratti non  rientrano  
 nei conti consortili dei quali si alimenta il Fondo di garanzia.</p>
            </content>
          </paragraph>
          <paragraph>
            <content>
              <p>Nulla naturalmente vieterebbe al legislatore ordinario di procedere  
 a più ampia strutturazione del Fondo chiamando a contributo più vasta  
 congerie  di  veicoli  circolanti,  ma  la  mancata  attuazione di tale  
 possibilità non va scambiata con attentato alla parità di trattamento  
 per inferirne la violazione dell'art. 3.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>7.</num>
            <content>
              <p>3.  -  Del   pari   infondata   è   l'altra   questione   basata  
 sull'enucleazione, dalla sfera dei danni coperti dal Fondo di garanzia,  
 di  valori  pecuniari  inferiori  alle  lire  100.000,  non solo per le  
 ragioni esposte sub 7.2, ma anche  perché  tali  valori  non  sono  ex  
 necesse  correlati  -  per riferirsi essi alla determinazione monetaria  
 del danno -  con  la  consistenza  patrimoniale  delle  vittime  e  dei  
 responsabili.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>7.</num>
            <content>
              <p>4.  -  Né,  sul  piano  del  diritto  positivo  sul quale va pur  
 condotto lo scrutinio di conformità alla  Carta  Costituzionale  delle  
 disposizioni che lo compongono, vanno dimenticate le caratteristiche ad  
 un   tempo   strutturali   e   funzionari  che  distinguono  la  comune  
 assicurazione della responsabilità  civile,  disciplinata  dal  <ref href="/akn/it/act/regioDecreto/stato/1942-03-16/262/!main">codice  
 civile</ref>  e dalle norme generali del <ref href="/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/1959/449/!main">d.P.R. 449/1959</ref>, dalla R.C.A., retta  
 dalla <ref href="/akn/it/act/legge/stato/1969/990/!main">l. 990/1969</ref> in parte  novellata  dalla  <ref href="/akn/it/act/legge/stato/1971/50/!main">l.  50/1971</ref>  e  dal  d.l.</p>
            </content>
          </paragraph>
          <paragraph>
            <num>57</num>
            <content>
              <p>/1976  conv.  in  <ref href="/akn/it/act/legge/stato/1977/30/!main">l.  30/1977</ref>,  e  che  hanno  l'apicale  espressione  
 nell'azione diretta contro l'assicuratore e l'impresa designata per  la  
 liquidazione  agli  aventi  diritto  di  somme provenienti dal Fondo di  
 garanzia (per altre caratteristiche della R.C.A. <ref href="/akn/it/judgment/sentenza/corteCostituzionale/1981/202/!main">sent.  202/1981</ref>).</p>
            </content>
          </paragraph>
        </division>
      </motivation>
      <decision>
        <block name="formulaRito">per questi motivi                             
                         <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>                          
     a)   dichiara   non   fondata   la   questione   di    legittimità  
 costituzionale,  in  riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, dell'<mref><ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_1">art. 19 comma 1</ref>  
 <ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_c">c) l. 24 dicembre 1969 n. 990</ref></mref>,  nella  parte  in  cui  non  prevede  il  
 risarcimento  dei danni - a carico del Fondo di garanzia per le vittime  
 della strada  -  cagionati  dalla  circolazione  di  veicoli  che,  non  
 soggetti  all'obbligo  di  assicurazione  della responsabilità civile,  
 siano nondimeno assicurati presso imprese che, al momento del  sinistro  
 o  successivamente,  si  trovano  in  istato  di  liquidazione  coatta;</block>
        <paragraph>
          <list>
            <alinea>
              <content>
                <p>questione sollevata con <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1977-01-07/1/!main">ordinanze 17 gennaio 1977</ref> del Pretore di Padova  
 e 29 maggio 1980 del Tribunale di Padova;</p>
              </content>
            </alinea>
            <point>
              <num>b)</num>
              <content>
                <p> dichiara non fondata la questione di legittimità costituzionale  
 in riferimento all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, dell'<ref href="/akn/it/act/legge/stato/1969-12-24/990/!main#art_19__para_2">art. 19 comma 2  l.  24  dicembre  
 1969  n.  990</ref>,  sollevata  con <ref href="/akn/it/judgment/ordinanza/corteCostituzionale/1978-10-05/2/!main">ordinanza 25 ottobre 1978</ref> del Pretore di  
 Bari.</p>
              </content>
            </point>
          </list>
        </paragraph>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  
 Palazzo della Consulta, il <date refersTo="#decisionDate" date="1982-04-16">16 aprile 1982</date>.</block>
      <p>F.to: <signature><judge refersTo="#leopoldoElia">LEOPOLDO ELIA</judge></signature> - <signature><judge refersTo="#micheleRossano">MICHELE ROSSANO</judge></signature>  
                                   - <signature><judge refersTo="#antoninoDeStefano">ANTONINO  DE  STEFANO</judge></signature>  -  <signature><judge refersTo="#guglielmoRoehrssen">GUGLIELMO  
                                   ROEHRSSEN</judge></signature>  -  <signature><judge refersTo="#oronzoReale">ORONZO REALE</judge></signature> - BRUNETTO  
                                   BUCCIARELLI    DUCCI    -     <signature><judge refersTo="#albertoMalagugini">ALBERTO  
                                   MALAGUGINI</judge></signature> - <signature><judge refersTo="#livioPaladin">LIVIO PALADIN</judge></signature> - <signature><judge refersTo="#virgilioAndrioli">VIRGILIO  
                                   ANDRIOLI</judge></signature>   -   <signature><judge refersTo="#giuseppeFerrari">GIUSEPPE   FERRARI</judge></signature>   -  
                                   <signature><judge refersTo="#francescoSaja">FRANCESCO SAJA</judge></signature> - <signature><judge refersTo="#giovanniConso">GIOVANNI CONSO</judge></signature>.</p>
      <p>
        <signature><person refersTo="#giovanniVitale" as="#cancelliere">GIOVANNI VITALE</person> - <role refersTo="#cancelliere">Cancelliere</role></signature>
      </p>
    </conclusions>
  </judgment>
</akomaNtoso>
