<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>386</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:386</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Giuseppe Ferrari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1985</data_decisione>
    <data_deposito>30/12/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Prof. ANTONIO LA PERGOLA - &#13;
 Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO &#13;
 SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO &#13;
 CORASANITI - Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO - Prof. &#13;
 RENATO DELL'ANDRO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio promosso con ricorso del Presidente del Consiglio  dei  &#13;
 ministri  notificato il 21 ottobre 1981, depositato in Cancelleria il 9  &#13;
 novembre 1981 ed iscritto al n.  41 del Registro 1981 per conflitto  di  &#13;
 attribuzione sorto a seguito dell'ordinanza 30 ottobre 1979 del Pretore  &#13;
 di  Menaggio  con  la  quale viene intimato "all'ANAS, Compartimento di  &#13;
 Milano, di eseguire nel termine di giorni 30 tutte le opere e i  lavori  &#13;
 necessari  per  l'adeguata  manutenzione  della  strada  statale  340 e  &#13;
 l'illuminazione delle gallerie lungo il tratto Menaggio-Dongo, pena non  &#13;
 soltanto l'incriminazione ai sensi degli artt. 328 e 650 C.P., ma anche  &#13;
 l'eventuale ordine di chiusura al traffico di detta strada".             &#13;
     Udito nella camera di consiglio dell'11 dicembre  1985  il  Giudice  &#13;
 relatore Giuseppe Ferrari.                                               &#13;
     Ritenuto   che  con  ricorso  depositato  il  18  gennaio  1980  il  &#13;
 Presidente  del  Consiglio  dei  ministri  ha  sollevato  conflitto  di  &#13;
 attribuzione  avverso il provvedimento 30 ottobre 1979, con il quale il  &#13;
 Pretore di Menaggio aveva ordinato "all'ANAS, Compartimento di  Milano,  &#13;
 di  eseguire  nel termine di giorni trenta dalla ricezione del decreto,  &#13;
 tutte le opere e i lavori necessari per l'adeguata  manutenzione  della  &#13;
 SS.   340   e   l'illuminazione   delle   gallerie   lungo   il  tratto  &#13;
 Menaggio-Dongo, pena non soltanto l'incriminazione ai sensi degli artt.  &#13;
 328 e 650 c.p., ma anche l'eventuale ordine di chiusura al traffico  di  &#13;
 detta strada";                                                           &#13;
     che,  pertanto,  secondo  quanto si assumeva in ricorso, il Pretore  &#13;
 aveva, non solo esercitato un potere attribuito al Ministro dei  Lavori  &#13;
 Pubblici  quale  presidente  dell'ANAS,  ma  anche  invaso  la sfera di  &#13;
 attribuzioni garantita al potere  esecutivo  da  norme  costituzionali,  &#13;
 travalicando i limiti posti al potere giurisdizionale dagli artt. 101 e  &#13;
 102, nonché dall'art. 113 della Costituzione;                           &#13;
     che,  con  ordinanza  n.  132  del 1981, questa Corte dichiarava il  &#13;
 ricorso ammissibile ai sensi dell'art. 37 della legge n. 87  del  1953,  &#13;
 disponendo  che  ricorso  e  ordinanza fossero notificati al Pretore di  &#13;
 Menaggio, a cura del ricorrente Presidente del Consiglio dei  ministri,  &#13;
 entro sessanta giorni dalla comunicazione dell'ordinanza stessa;         &#13;
     che  il  Presidente  del  Consiglio  dei  ministri  provvedeva alla  &#13;
 notificazione del ricorso e dell'ordinanza al Pretore di Menaggio il 21  &#13;
 ottobre 1981, mentre  quest'ultima  risulta  comunicata  all'Avvocatura  &#13;
 dello Stato il 14 luglio 1981;                                           &#13;
     che  nessuna delle parti s'è costituita in giudizio in questa fase  &#13;
 del procedimento.                                                        &#13;
     Considerato che il ricorrente non ha  effettuato  la  notificazione  &#13;
 del  ricorso e dell'ordinanza entro il termine di sessanta giorni dalla  &#13;
 comunicazione dell'ordinanza fissato dalla Corte, che è da  osservarsi  &#13;
 a  pena  di  decadenza  secondo  quanto  si  rileva  dal Regolamento di  &#13;
 procedura dinanzi al Consiglio di Stato  in  sede  giurisdizionale  (in  &#13;
 connessione  con  l'art.  36  del testo unico delle leggi sul Consiglio  &#13;
 stesso, approvato con r.d. 26 giugno 1924, n.   1054), applicabile  nei  &#13;
 procedimenti  davanti  alla Corte costituzionale in virtù del richiamo  &#13;
 di cui all'art. 22 della legge n. 87 del 1953;                           &#13;
     che,  in  particolare,  non  varrebbe  invocare  in  contrario   la  &#13;
 sospensione  del  decorso  dei  termini processuali nel periodo feriale  &#13;
 (leggi 14 luglio 1965, n. 818 e 7 ottobre 1969,  n.    742)  la  quale,  &#13;
 secondo quanto già in precedenza ritenuto dalla Corte (con sentenza n.  &#13;
 15  del 1967 e con ordinanza 14 gennaio 1970, pronunziata nel corso del  &#13;
 giudizio cui  si  riferisce  la  sentenza  n.  18  del  1970),  non  è  &#13;
 applicabile ai giudizi innanzi a se stessa;                              &#13;
     che  il  ricorso non è stato ritualmente proseguito, per cui ne va  &#13;
 dichiarata la manifesta inammissibilità.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la manifesta inammissibilità del ricorso per conflitto di  &#13;
 attribuzione  tra  poteri  dello  Stato  proposto  dal  Presidente  del  &#13;
 Consiglio  dei  ministri  nei  confronti  del  Pretore  di Menaggio con  &#13;
 ricorso in data 18 gennaio 1980 (reg. confl. n. 41 del 1981).            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 19 dicembre 1985.       &#13;
                                   F.to:   LIVIO   PALADIN  -  GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO REALE - ANTONIO LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI  - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO -  ALDO  &#13;
                                   CORASANITI  -  GIUSEPPE  BORZELLINO -  &#13;
                                   FRANCESCO GRECO - RENATO DELL'ANDRO.   &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
