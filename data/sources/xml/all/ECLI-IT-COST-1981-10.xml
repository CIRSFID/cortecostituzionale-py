<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>10</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:10</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1981</data_decisione>
    <data_deposito>28/01/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - &#13;
 Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt. 47 e  &#13;
 48,   terzo   comma,   della  legge  26  luglio  1975,  n.  354  (norme  &#13;
 sull'ordinamento penitenziario e sull'esecuzione delle misure privative  &#13;
 e limitative della libertà), come modificati dall'art. 4  della  legge  &#13;
 12 gennaio 1977, n. 1, promossi con le seguenti ordinanze:               &#13;
     1.  -  Ordinanza  emessa  il  18  aprile  1977  dal Tribunale per i  &#13;
 minorenni di Roma sull'istanza proposta da Bullo Giovanni, iscritta  al  &#13;
 n.  281  del  registro  ordinanze  1977  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 205 del 27 luglio 1977;                    &#13;
     2.  -  Ordinanza  emessa  il  15  marzo  1977  dalla   Sezione   di  &#13;
 sorveglianza  della  Corte d'appello di Napoli sull'istanza proposta da  &#13;
 Giordano Raffaele, iscritta al n. 293 del  registro  ordinanze  1977  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale della Repubblica n.   230 del 24  &#13;
 agosto 1977;                                                             &#13;
     3. -  Ordinanza  emessa  il  20  febbraio  1980  dalla  Sezione  di  &#13;
 sorveglianza  della Corte d'appello di Genova nel procedimento relativo  &#13;
 a Federigi Gino, iscritta al n.  311  del  registro  ordinanze  1980  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n. 180 del 2  &#13;
 luglio 1980.                                                             &#13;
     Visti gli atti di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di consiglio dell'11 dicembre 1980 il Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto che il Tribunale per i minorenni di  Roma,  con  ordinanza  &#13;
 emessa  il  18 aprile 1977, ha sollevato - in riferimento agli artt. 3,  &#13;
 25, secondo comma, e 27, terzo comma, della Costituzione - questione di  &#13;
 legittimità costituzionale dell'art. 48, terzo comma, della  legge  26  &#13;
 luglio  1975,  n. 354, "nella parte in cui esclude la concessione della  &#13;
 semilibertà ai condannati per i delitti di rapina,  rapina  aggravata,  &#13;
 estorsione,  estorsione  aggravata,  sequestro  di  persona  a scopo di  &#13;
 rapina o  di  estorsione";  che  la  sezione  di  sorveglianza  per  il  &#13;
 distretto della Corte d'appello di Napoli, con ordinanza - emessa il 15  &#13;
 marzo  1977,  ha  impugnato  a  sua  volta  - in riferimento agli artt.  &#13;
 2,3,27, terzo comma, e  111,  primo  comma,  della  Costituzione  -  il  &#13;
 combinato  disposto  degli artt. 47 cpv. e 48, terzo comma, della legge  &#13;
 n. 354 del 1975, come modificati dall'art. 4 della legge n. 1 del 1977;  &#13;
 che la sezione di sorveglianza per il distretto della  Corte  d'appello  &#13;
 di  Genova,  con  ordinanza  datata  20  febbraio  1980,  ha  sollevato  &#13;
 anch'essa - ma in riferimento al solo  art.  3  Cost.  -  questione  di  &#13;
 legittimità  costituzionale  dell'art. 48, terzo comma, della legge n.  &#13;
 354 del 1975;                                                            &#13;
     che in tutti i giudizi è intervenuto il Presidente  del  Consiglio  &#13;
 dei  ministri,  chiedendo  che  la Corte dichiari infondate le predette  &#13;
 questioni (a meno di considerarle inammissibili - secondo una tesi  cui  &#13;
 l'Avvocatura  dello  Stato  ha  fatto  cenno,  nell'atto  di intervento  &#13;
 relativo al giudizio instaurato dalla  sezione  di  sorveglianza  della  &#13;
 Corte  d'appello di Genova - non essendo sindacabili le "valutazioni di  &#13;
 natura politica affidate alla discrezionalità del legislatore");   che  &#13;
 i  giudizi  stessi  possono  essere decisi con unica ordinanza, data la  &#13;
 sostanziale affinità delle questioni  così  prospettate,  malgrado  i  &#13;
 diversi  parametri  costituzionali  rispettivamente richiamati dai vari  &#13;
 giudici a quibus.                                                        &#13;
     Considerato che la Corte si è già pronunciata  -  in  riferimento  &#13;
 agli  artt.  3,  primo  e  secondo  comma,  e  27,  terzo  comma, della  &#13;
 Costituzione - sulla questione di costituzionalità dell'art. 48, terzo  &#13;
 comma, della legge 26 luglio 1975, n. 354 (sul divieto di concedere  il  &#13;
 beneficio  della  semilibertà,  nelle  ipotesi  previste dall'art. 47,  &#13;
 secondo comma, della legge medesima); dichiarandola non fondata, con la  &#13;
 sentenza n. 107 del 1980, e manifestamente infondata,  con  l'ordinanza  &#13;
 n. 8 del 1981;                                                           &#13;
     che   il   richiamo   agli  artt.  2  e  111,  primo  comma,  della  &#13;
 Costituzione, effettuato dalla  sezione  di  sorveglianza  della  Corte  &#13;
 d'appello di Napoli, non sposta i termini reali del problema, in quanto  &#13;
 la  pretesa  lesione dell'art. 2 viene appunto sostenuta in vista degli  &#13;
 artt.  3,  secondo  comma,  e  27,  terzo  comma,  della   Costituzione  &#13;
 (assumendosi, ancora una volta, che la norma impugnata comprometterebbe  &#13;
 l'eguale  "sviluppo"  della  personalità  dei  soggetti  detenuti,  in  &#13;
 violazione "dei corrispettivi doveri di solidarietà", e renderebbe "la  &#13;
 pena diseducativa e quindi contraria al senso di umanità"); mentre  la  &#13;
 lesione  del  primo  comma  dell'art.   111 Cost., sulla motivazione di  &#13;
 tutti i provvedimenti giurisdizionali, viene prospettata "solo  in  via  &#13;
 subordinata", rinnovando in definitiva la censura per cui le competenti  &#13;
 autorità  giudiziarie sarebbero in tal caso private della loro normale  &#13;
 "discrezionalità", senza che ciò trovi fondamento in alcuna  "ragione  &#13;
 oggettiva":    sicché  la  questione  può considerarsi manifestamente  &#13;
 infondata, per le ragioni già addotte nella sentenza n.  107 del 1980;  &#13;
     che  sostanzialmente  nuova  è  invece  la  censura  promossa  dal  &#13;
 Tribunale per i minorenni di Roma per la pretesa violazione del secondo  &#13;
 comma  dell'art.  25 Cost., derivante dall'aver precluso la concessione  &#13;
 della semilibertà, nelle ipotesi indicate dall'art. 47 cpv., circa gli  &#13;
 stessi  soggetti   che   abbiano   "commesso   il   fatto   (criminoso)  &#13;
 anteriormente  all'entrata  in vigore della legge 354 del 1975"; e che,  &#13;
 tuttavia, anche in tal senso deve dichiararsi la manifesta infondatezza  &#13;
 della questione in esame: poiché la norma così denunciata non  incide  &#13;
 affatto  sull'ambito temporale di efficacia della legge penale in forza  &#13;
 della quale fu  emessa  sentenza  di  condanna,  ma  concerne  soltanto  &#13;
 l'applicabilità  della  ben  diversa disciplina attinente al regime di  &#13;
 espiazione della pena detentiva, già inflitta nel momento dell'entrata  &#13;
 in vigore della disciplina stessa -  che  per  la  prima  volta,  giova  &#13;
 sottolinearlo,  ha  introdotto  la  misura  della  semilibertà  -  con  &#13;
 particolare riguardo alla  concessione  di  benefici  alternativi  alla  &#13;
 detenzione,  per  effetto  di una discrezionale valutazione del giudice  &#13;
 quanto al positivo,  sopravvenuto  assolvimento  del  fine  rieducativo  &#13;
 della pena.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la manifesta infondatezza della questione di legittimità  &#13;
 costituzionale dell'art. 48, terzo comma  (in  relazione  all'art.  47,  &#13;
 secondo  comma),  della  legge 26 luglio 1975, n. 354, sollevata con le  &#13;
 ordinanze indicate in epigrafe - in riferimento agli artt.  2,  3,  25,  &#13;
 secondo  comma, 27, terzo comma, 111, primo comma, della Costituzione -  &#13;
 dal Tribunale per i minorenni di Roma, dalla  sezione  di  sorveglianza  &#13;
 per  il  distretto  della  Corte d'appello di Napoli e dalla sezione di  &#13;
 sorveglianza per il distretto della Corte d'appello di Genova.           &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 22 gennaio 1981.        &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   -  LEOPOLDO  ELIA  -  ORONZO  REALE -  &#13;
                                   BRUNETTO BUCCIARELLI DUCCI -  ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE FERRARI.  &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
