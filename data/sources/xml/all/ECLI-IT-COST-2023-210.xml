<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2023</anno_pronuncia>
    <numero_pronuncia>210</numero_pronuncia>
    <ecli>ECLI:IT:COST:2023:210</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BARBERA</presidente>
    <relatore_pronuncia>Stefano Petitti</relatore_pronuncia>
    <redattore_pronuncia>Stefano Petitti</redattore_pronuncia>
    <data_decisione>08/11/2023</data_decisione>
    <data_deposito>30/11/2023</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>restituzione atti - jus superveniens</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Augusto Antonio BARBERA; Giudici : Giulio PROSPERETTI, Giovanni AMOROSO, Francesco VIGANÒ, Luca ANTONINI, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI, Marco D'ALBERTI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 224, comma 3, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), promosso dal Giudice di pace di La Spezia, nel procedimento vertente tra L. d.M. e la Prefettura di La Spezia, con ordinanza del 27 giugno 2022, iscritta al n. 132 del registro ordinanze 2022 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 46, prima serie speciale, dell'anno 2022.&#13;
 Udito nella camera di consiglio dell'8 novembre 2023 il Giudice relatore Stefano Petitti;&#13;
 deliberato nella camera di consiglio dell'8 novembre 2023.&#13;
 Ritenuto che, con ordinanza del 27 giugno 2022, iscritta al n. 132 del registro ordinanze 2022, il Giudice di pace di La Spezia ha sollevato questioni di legittimità costituzionale dell'art. 224, comma 3, del decreto legislativo 30 aprile 1992, n. 285 (Nuovo codice della strada), in riferimento agli artt. 3, 4, 16, 27, terzo comma, 34 e 117, primo comma, della Costituzione, quest'ultimo in relazione agli artt. 8, 11 e 29 della Dichiarazione universale dei diritti umani, adottata dall'Assemblea generale delle Nazioni Unite il 10 dicembre 1948, e agli artt. 47 e 49, comma 3, della Carta dei diritti fondamentali dell'Unione europea (CDFUE), nella parte in cui non prevede che, in caso di estinzione del reato di guida in stato di ebbrezza a seguito di esito positivo della messa alla prova, il prefetto, previo accertamento della sussistenza delle condizioni di legge, disponga la riduzione alla metà della sanzione della sospensione della patente di guida, così come stabilito dall'art. 186, comma 9-bis, cod. strada, aggiunto dall'art. 33, comma 1, lettera d), della legge 29 luglio 2010, n. 120 (Disposizioni in materia di sicurezza stradale);&#13;
 che, per quanto riferisce l'ordinanza di rimessione, il giudizio a quo concerne l'opposizione avverso una ordinanza del prefetto irrogativa della sanzione amministrativa della sospensione della patente di guida per un periodo di duecentoventitré giorni, detratti i sei mesi già scontati in via cautelare, per la violazione dell'art. 186, comma 2, lettera b), e comma 2-bis, cod. strada;&#13;
 che il rimettente afferma che il processo penale per guida in stato di ebbrezza è stato definito con declaratoria di estinzione per esito positivo della messa alla prova, ai sensi dell'art. 168-bis del codice penale;&#13;
 che il giudice a quo riferisce di essere a conoscenza del fatto che questioni analoghe alle odierne sono state sollevate da altro giudice di pace e non sono state ancora decise, ritenendo tuttavia opportuno evidenziare ulteriori profili di illegittimità costituzionale della medesima disposizione censurata;&#13;
 che, invero, ad avviso del rimettente, quest'ultima, oltre che con il principio di eguaglianza - violato per il deteriore trattamento normativo riservato all'esito della messa alla prova di cui all'art. 168-bis cod. pen. rispetto ai benefici riconosciuti dall'art. 186, comma 9-bis, cod. strada, in caso di ammissione dell'imputato, dopo la condanna, ai lavori di pubblica utilità -, contrasterebbe anche con i principi di proporzionalità e rieducazione della pena, desumibili non solo dall'art. 27, terzo comma, Cost., ma anche dalle richiamate norme sovranazionali interposte.&#13;
 Considerato che il Giudice di pace di La Spezia ha sollevato questioni di legittimità costituzionale dell'art. 224, comma 3, cod. strada, nella parte in cui non prevede che, in caso di estinzione del reato di guida in stato di ebbrezza a seguito di esito positivo della messa alla prova, il prefetto, previo accertamento della sussistenza delle condizioni di legge, disponga la riduzione alla metà della sanzione della sospensione della patente di guida, così come stabilito dall'art. 186, comma 9-bis, cod. strada, aggiunto dall'art. 33, comma 1, lettera d), della legge n. 120 del 2010;&#13;
 che, ad avviso del rimettente, la disposizione censurata contrasterebbe con gli artt. 3, 4, 16, 27, terzo comma, 34 e 117, primo comma, Cost., quest'ultimo in relazione agli artt. 8, 11 e 29 della Dichiarazione universale dei diritti umani e agli artt. 47 e 49, comma 3, CDFUE;&#13;
 che il presupposto interpretativo da cui muove il rimettente risulta modificato dalla sentenza n. 163 del 2022, pubblicata successivamente all'ordinanza di rimessione, con cui questa Corte ha dichiarato l'illegittimità costituzionale dell'art. 224, comma 3, cod. strada, nella parte in cui non prevede che, nel caso di estinzione del reato di guida sotto l'influenza dell'alcool di cui all'art. 186, comma 2, lettere b) e c), del medesimo codice, per esito positivo della messa alla prova, il prefetto, applicando la sanzione amministrativa accessoria della sospensione della patente, ne riduca la durata della metà;&#13;
 che detta sentenza ha affermato che l'illegittimità costituzionale dell'art. 224, comma 3, cod. strada discende dalla manifesta irragionevolezza della conseguenza applicativa per cui, al cospetto di una prestazione analoga, qual è il lavoro di pubblica utilità, e a fronte del medesimo effetto dell'estinzione del reato, la sanzione amministrativa accessoria della sospensione della patente era ridotta alla metà dal giudice in caso di svolgimento positivo del lavoro sostitutivo, mentre risultava escluso il beneficio dell'identica riduzione ove applicata dal prefetto in caso di esito positivo della messa alla prova;&#13;
 che, tuttavia, la sentenza n. 163 del 2022 ha ravvisato l'irragionevolezza dell'art. 224, comma 3, cod. strada, nei limiti dei casi regolati dalla fattispecie dell'art. 186, comma 9-bis, dello stesso codice, utilizzata come norma di raffronto, la quale ammette il lavoro di pubblica utilità, cui si correla la funzione premiale del suo positivo svolgimento, nelle sole ipotesi di reato di guida in stato di ebbrezza diverse da quelle contemplate dal comma 2-bis del medesimo articolo, ipotesi cui - per quanto si evince dall'ordinanza di rimessione - è invece riconducibile la fattispecie oggetto del giudizio a quo;&#13;
 che, secondo la costante giurisprudenza di questa Corte, a fronte del sopraggiungere di pronunce di illegittimità costituzionale, spetta al giudice rimettente valutarne in concreto l'incidenza, sia in ordine alla rilevanza, sia in riferimento alla non manifesta infondatezza delle questioni di legittimità costituzionale sollevate (ex plurimis, ordinanze n. 184, n. 183 e n. 49 del 2020; n. 182 del 2019; n. 154 del 2018);&#13;
 che, pertanto, deve essere disposta la restituzione degli atti al rimettente per un nuovo esame della rilevanza e della non manifesta infondatezza delle questioni sollevate, alla luce del mutato contesto normativo.&#13;
 Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 11, comma 1, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 ordina la restituzione degli atti al Giudice di pace di La Spezia.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, l'8 novembre 2023.&#13;
 F.to:&#13;
 Augusto Antonio BARBERA, Presidente&#13;
 Stefano PETITTI, Redattore&#13;
 Roberto MILANA, Direttore della Cancelleria&#13;
 Depositata in Cancelleria il 30 novembre 2023&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
