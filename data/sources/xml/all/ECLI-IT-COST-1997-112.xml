<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>112</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:112</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/04/1997</data_decisione>
    <data_deposito>22/04/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel giudizio di legittimità costituzionale dell'art. 263, del codice    &#13;
 civile, promosso con ordinanza emessa il 21 giugno 1996 dal tribunale    &#13;
 di Napoli nel procedimento civile vertente tra Dama Rosanna (n.q.)  e    &#13;
 Caterino  Aldo,  iscritta  al  n.  1141 del registro ordinanze 1996 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  43,  prima    &#13;
 serie speciale, dell'anno 1996;                                          &#13;
   Visti gli atti di costituzione di Dama Rosanna (n.q.) e di Caterino    &#13;
 Aldo;                                                                    &#13;
   Udito  nell'udienza pubblica dell'11 marzo 1997 il giudice relatore    &#13;
 Fernanda Contri;                                                         &#13;
   Udito l'avvocato Erminia Delcogliano per Caterino Aldo.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -    Nel  corso  di  un  procedimento   di   impugnazione   del    &#13;
 riconoscimento  di  figlio  naturale  per  difetto di veridicità, il    &#13;
 tribunale di Napoli,  con  ordinanza  in  data  21  giugno  1996,  ha    &#13;
 sollevato,   in   riferimento   agli  artt.  2,  3,  30  e  31  della    &#13;
 Costituzione, questione di legittimità costituzionale dell'art.  263    &#13;
 del  codice civile, nella parte in cui non prevede che l'impugnazione    &#13;
 in esame possa essere accolta solo quando sia  ritenuta  dal  giudice    &#13;
 rispondente all'interesse del minore.                                    &#13;
   Il  giudice  rimettente, dopo aver premesso che una declaratoria di    &#13;
 non    veridicità    del    riconoscimento,    cui     conseguirebbe    &#13;
 l'allontanamento   del   minore   dal  contesto  familiare,  potrebbe    &#13;
 risultare  gravemente  pregiudizievole  all'interesse  del   medesimo    &#13;
 minore,  osserva  che  una  decisione,  la  quale  non  consideri  il    &#13;
 preminente  interesse  del  minore  alla  conservazione  dell'attuale    &#13;
 status,    si    risolverebbe   nella   mera   certificazione   della    &#13;
 corrispondenza tra il dato naturale e la situazione giuridica,  senza    &#13;
 alcuna  valutazione  dell'esigenza  di  tutela  del minore, che trova    &#13;
 invece un solido fondamento nei principi stabiliti dagli artt. 2,  3,    &#13;
 30 e 31 della Costituzione.                                              &#13;
   Ad  avviso  del  rimettente,  l'evidente lacuna normativa dell'art.    &#13;
 263 del codice civile può essere superata unicamente  attraverso  un    &#13;
 intervento  della  Corte costituzionale, che attribuisca al tribunale    &#13;
 il  potere  di  valutare,  nel  merito,  la  rispondenza  dell'azione    &#13;
 all'interesse  del  minore,  consentendo di rigettare o di dichiarare    &#13;
 inammissibile la stessa nell'ipotesi in cui, pur sussistendo tutti  i    &#13;
 presupposti   normativi,   gli   effetti   dell'accoglimento  possano    &#13;
 rivelarsi pregiudizievoli per il minore.                                 &#13;
   2.  -  Nel  giudizio  innanzi  a  questa  Corte si è costituito il    &#13;
 curatore speciale del minore, nominato dal tribunale per i  minorenni    &#13;
 per  l'impugnazione  del  riconoscimento, ai sensi dell'art. 74 della    &#13;
 legge  4  maggio  1983,  n.  184,  chiedendo  che  la  questione  sia    &#13;
 dichiarata inammissibile e infondata.                                    &#13;
   Sostiene  il  curatore che l'interesse pubblico alla certezza degli    &#13;
 status familiari, intesa quale rispondenza tra la realtà formalmente    &#13;
 accertata  e  la  realtà  sostanziale,  è   privilegiato   rispetto    &#13;
 all'interesse  privato e che comunque non vi è alcuna divergenza tra    &#13;
 il  detto  interesse  pubblico  e  quello  del  minore,   poiché   a    &#13;
 quest'ultimo  deve  essere  garantita  un'identità reale, conforme a    &#13;
 quella effettiva.                                                        &#13;
   La norma censurata soddisfa quindi il diritto primario  del  minore    &#13;
 alla  certezza  della  propria  identità  e  persegue  il suo stesso    &#13;
 interesse.                                                               &#13;
   3. - Si è costituito  anche  il  convenuto  nel  giudizio  a  quo,    &#13;
 rilevando  a sua volta come la necessità di privilegiare l'interesse    &#13;
 del minore sia stata affermata dal legislatore nelle  diverse  azioni    &#13;
 in materia di riconoscimento di figli naturali, quali quelle previste    &#13;
 dagli  artt.  250,  251 e 252 del codice civile, e ricordando come la    &#13;
 stessa Corte costituzionale  in  diverse  pronunce  abbia  attribuito    &#13;
 preminente  rilievo  all'interesse  del  minore (sentenze nn. 303 del    &#13;
 1996, 429 del 1991 e 341 del 1990).                                      &#13;
   La detta parte, pur  ritenendo  che  una  corretta  interpretazione    &#13;
 degli artt. 263 e 264 del codice civile e dell'art. 74 della legge n.    &#13;
 184  del 1983 consenta al giudice di valutare l'interesse del minore,    &#13;
 ha comunque chiesto l'accoglimento della questione.<diritto>Considerato in diritto</diritto>1. -   Il tribunale di Napoli ha  sollevato,  in  riferimento  agli    &#13;
 artt.  2,  3,  30  e 31 della Costituzione, questione di legittimità    &#13;
 costituzionale dell'art. 263 del codice civile, nella  parte  in  cui    &#13;
 non   prevede   che  l'impugnazione  del  riconoscimento  del  figlio    &#13;
 minorenne per difetto di veridicità possa essere accolta solo quando    &#13;
 sia ritenuta dal giudice rispondente all'interesse del minore stesso.    &#13;
   2. - Ad avviso del tribunale rimettente, la  norma  censurata,  che    &#13;
 non  considera  in alcun modo il preminente interesse del minore alla    &#13;
 conservazione dell'ambiente  familiare  nel  quale  è  inserito,  si    &#13;
 porrebbe  in  contrasto  con  gli  indicati parametri costituzionali,    &#13;
 espressione delle esigenze di tutela dei minori.                         &#13;
   3. - La questione non è fondata.                                      &#13;
   Come questa Corte ha già avuto modo di affermare (sentenza n.  158    &#13;
 del  1991),  l'impugnazione  del  riconoscimento   per   difetto   di    &#13;
 veridicità  è  ispirata  al "principio di ordine superiore che ogni    &#13;
 falsa apparenza di stato deve cadere", in quanto  nella  verità  del    &#13;
 rapporto di filiazione è stato individuato un valore necessariamente    &#13;
 da  tutelare.  L'attribuzione  della  legittimazione  ad  agire anche    &#13;
 all'autore   in   mala   fede   del   falso   riconoscimento   e   la    &#13;
 imprescrittibilità    dell'azione   dimostrano   infatti   come   il    &#13;
 legislatore,  nel  conformare  l'istituto  in  esame,  abbia   voluto    &#13;
 privilegiare  il  favor  veritatis, in funzione di un'imprescindibile    &#13;
 esigenza di certezza dei rapporti di filiazione.                         &#13;
   La  tutela  della  verità  deve  porsi  in  relazione  anche  alla    &#13;
 necessità   di   impedire   che   attraverso   fraudolenti  atti  di    &#13;
 riconoscimento siano eluse le norme in materia di adozione, poste  ad    &#13;
 esclusiva tutela dei minori. Il legislatore, infatti, per contrastare    &#13;
 il  diffondersi di prassi illecite, ha ritenuto di dover istituire un    &#13;
 sistema di controllo degli  atti  di  riconoscimento,  effettuati  da    &#13;
 parte  di  persona  coniugata,  di  figli  naturali  non riconosciuti    &#13;
 dall'altro genitore, attribuendo al Tribunale  per  i  minorenni,  ai    &#13;
 sensi dell'art. 74 della legge n. 184 del 1983, il potere di disporre    &#13;
 opportune   indagini   al   fine  di  accertare  la  veridicità  del    &#13;
 riconoscimento e, conseguentemente, il potere di nominare  al  minore    &#13;
 un  curatore  speciale  per  l'impugnazione  del  riconoscimento,  in    &#13;
 presenza di fondati motivi per ritenere che questo non sia veritiero.    &#13;
   La finalità così perseguita  dal  legislatore  deve  individuarsi    &#13;
 proprio  nell'attuazione  del  diritto del minore all'acquisizione di    &#13;
 uno stato corrispondente alla realtà biologica, ovvero, qualora ciò    &#13;
 non sia possibile, all'acquisizione di  uno  stato  corrispondente  a    &#13;
 quello  dei  figli  legittimi, ma solo attraverso le garanzie offerte    &#13;
 dalle norme sull'adozione.                                               &#13;
   Non si può contrapporre al favor veritatis il favor  minoris,  dal    &#13;
 momento che la falsità del riconoscimento lede il diritto del minore    &#13;
 alla propria identità.                                                  &#13;
   4. - Non ignora questa Corte che il perseguimento della verità del    &#13;
 rapporto di filiazione può costituire causa di grave pregiudizio per    &#13;
 il minore, che può essere costretto, talvolta anche dopo molti anni,    &#13;
 ad  un  repentino allontanamento dall'ambiente familiare nel quale è    &#13;
 stato inserito, eventualmente anche con frode. Tale effetto  tuttavia    &#13;
 non  deriva  dalla pretesa incostituzionalità della norma censurata,    &#13;
 la quale, si è detto, intende tutelare il diritto alla  verità  del    &#13;
 rapporto di filiazione, ma è per lo più connessa ai tempi di durata    &#13;
 delle  varie fasi e dei gradi del giudizio di impugnazione, durante i    &#13;
 quali  si  possono  consolidare   legami   affettivi,   difficilmente    &#13;
 rimovibili.                                                              &#13;
   A  tali  situazioni  ben può porsi rimedio con il ricorso ad altri    &#13;
 strumenti, tipici di tutela del  minore,  quali  l'adozione  in  casi    &#13;
 particolari,  di  cui  all'art.  44 lettera c) della legge n. 184 del    &#13;
 1983, molto spesso applicati dai Tribunali per i  minorenni.  In  tal    &#13;
 modo  si  rispetta  l'esigenza di verità del rapporto di filiazione,    &#13;
 riconosciuta dal nostro ordinamento, e nel  contempo  si  tutelano  i    &#13;
 legami  affettivi  instaurati  dal minore, che potrebbe restare nella    &#13;
 famiglia nella quale  si  è  formata  e  si  è  sviluppata  la  sua    &#13;
 personalità, acquisendo lo stato di figlio adottivo.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 263 del codice civile, sollevata, in riferimento agli artt.    &#13;
 2, 3, 30 e  31  della  Costituzione,  dal  tribunale  di  Napoli  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 aprile 1997.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 22 aprile 1997.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
