<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2005</anno_pronuncia>
    <numero_pronuncia>93</numero_pronuncia>
    <ecli>ECLI:IT:COST:2005:93</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONTRI</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Maria Flick</redattore_pronuncia>
    <data_decisione>24/02/2005</data_decisione>
    <data_deposito>08/03/2005</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Fernanda CONTRI; Giudici: Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 188 del decreto legislativo 28 luglio 1989, n. 271 (Norme di attuazione, di coordinamento e transitorie del codice di procedura penale), promosso con ordinanza del 9 dicembre 2003 dal Giudice per le indagini preliminari del Tribunale di Milano nel procedimento di esecuzione nei confronti di G. P., iscritta al n. 560 del registro ordinanze 2004 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 25, prima serie speciale, dell'anno 2004. &#13;
    Udito nella camera di consiglio del 26 gennaio 2005 il Giudice relatore Giovanni Maria Flick. &#13;
    Ritenuto che il Giudice delle indagini preliminari presso il Tribunale di Milano, con ordinanza emessa il  9 dicembre 2003, ha sollevato, in relazione all'art. 3, primo comma, della Costituzione, questione di legittimità costituzionale dell'art. 188 del decreto legislativo 28 luglio 1989, n. 271 (Norme di attuazione, di coordinamento e transitorie del codice di procedura penale) «nella parte in cui non indica in complessivi anni cinque di reclusione o di arresto il limite non superabile di pena in caso di applicazione della disciplina del reato continuato nella fase dell'esecuzione»; &#13;
    che il rimettente – chiamato a delibare, quale giudice dell'esecuzione, la richiesta di un condannato per l'applicazione della disciplina della continuazione in relazione a due diverse sentenze, entrambe pronunciate a norma dell'art. 444 cod. proc. pen. e con le quali sono state applicate pene detentive in misura complessivamente pari ad anni tre e mesi sei di reclusione – rileva che, in forza del citato art. 188 delle disposizioni di attuazione del codice di procedura penale, l'applicazione in sede esecutiva dell'istituto della continuazione è sempre subordinata, tra gli altri, al presupposto che non venga superato il limite di pena di due anni di reclusione o di arresto, soli o congiunti a pena pecuniaria; &#13;
    che la giustificazione di tale limite risiedeva, essenzialmente, nella complementarietà logica rispetto alla generale disciplina del “patteggiamento”, quale originariamente prevista dall'art. 444 cod. proc. pen., caratterizzata, inizialmente, da analogo limite per la pena applicabile su richiesta delle parti;  &#13;
    che, tuttavia, tale complementarietà – necessaria per evitare che il condannato, in sede esecutiva, potesse ottenere trattamenti sanzionatori più favorevoli di quelli  fruibili in sede di cognizione con il ricorso al citato rito speciale – era venuta meno a seguito della modificazione del primo comma dell'art. 444 cod. proc. pen., operata con l'art. 1 della legge 12 giugno 2003, n. 134: modificazione che ha reso possibile l'applicazione di una pena detentiva, su richiesta delle parti, non più fino ad un limite massimo di due anni – come in origine – ma di cinque anni, soli o congiunti a pena pecuniaria; &#13;
    che, nondimeno, la novella legislativa suddetta non ha modificato il testo dell'art. 188 disp. att. cod. proc. pen., con la conseguenza che risulta invariato il limite massimo di due anni per la pena detentiva da applicare, nella fase dell'esecuzione, in caso di ricorso alla disciplina del reato continuato;  &#13;
    che, secondo il giudice a quo, tale disomogeneità si traduce in un «oggettivo trattamento discriminatorio» tra chi, imputato di più fatti di reato, può ottenere, nella fase del giudizio di merito mediante la disciplina del reato continuato, l'applicazione di una pena detentiva fino a cinque anni; e chi, invece, ottiene l'applicazione della medesima disciplina sulla continuazione nella fase del procedimento di esecuzione, dove continua a vigere il diverso – e sensibilmente inferiore – limite di pena detentiva dei due anni;  &#13;
    che tale diversità di trattamento – essendo conseguenza di situazioni estrinseche e casuali, non ascrivibili alla condotta dell'imputato – sarebbe idonea a recare vulnus al principio di eguaglianza, con violazione dell'art. 3, primo comma, della Costituzione. &#13;
    Considerato che, successivamente alla pronuncia dell'ordinanza di rimessione, l'art. 1 della legge 2 agosto 2004, n. 205 (Modifica dell'articolo 188 delle norme di  attuazione, di coordinamento e transitorie del codice di procedura penale, di cui al decreto legislativo 28 luglio 1989, n. 271) ha modificato, nel senso auspicato del rimettente, la norma censurata;  &#13;
    che, infatti, a seguito della novella, l'art. 188 disp. att. cod. proc. pen. prevede ora che, nel caso di più sentenze di applicazione della pena su richiesta delle parti, pronunciate in procedimenti distinti contro la stessa persona, questa ed il pubblico ministero possono chiedere al giudice dell'esecuzione l'applicazione della disciplina del concorso formale o del reato continuato, quando concordano sull'entità della sanzione sostitutiva o della pena: sempre che quest'ultima non superi complessivamente cinque anni, soli o congiunti a pena pecuniaria, ovvero due anni, soli o congiunti a pena pecuniaria, nei casi previsti nel comma 1-bis dell'art. 444 del codice; &#13;
    che, pertanto, si impone il riesame della perdurante rilevanza della questione da parte del rimettente, al quale compete valutare l'applicabilità dello jus superveniens alla fattispecie sottoposta al suo esame.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    ordina la restituzione degli atti al Giudice delle indagini preliminari presso il Tribunale di Milano. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 febbraio 2005. &#13;
F.to: &#13;
Fernanda CONTRI, Presidente &#13;
Giovanni Maria FLICK, Redattore &#13;
Giuseppe DI PAOLA, Cancelliere &#13;
Depositata in Cancelleria l'8  marzo 2005. &#13;
Il Direttore della Cancelleria &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
