<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1980</anno_pronuncia>
    <numero_pronuncia>154</numero_pronuncia>
    <ecli>ECLI:IT:COST:1980:154</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/11/1980</data_decisione>
    <data_deposito>27/11/1980</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 3 e 5 della  &#13;
 legge   approvata   dall'Assemblea  regionale  siciliana  nella  seduta  &#13;
 notturna del 16-17 maggio 1979, recante "Norme per la prevenzione e  la  &#13;
 cura  delle  malattie  da  gozzo", promosso con ricorso del Commissario  &#13;
 dello Stato per la Regione siciliana, notificato  il  21  maggio  1979,  &#13;
 depositato  in cancelleria il 30 maggio successivo ed iscritto al n. 10  &#13;
 del registro ricorsi 1979.                                               &#13;
     Visto l'atto di costituzione del Presidente della Giunta  regionale  &#13;
 siciliana;                                                               &#13;
     udito  nell'udienza  pubblica  del  12  novembre  1980  il  Giudice  &#13;
 relatore Virgilio Andrioli;                                              &#13;
     uditi l'avvocato dello Stato Giorgio Azzariti per il ricorrente,  e  &#13;
 l'avv. Luigi Maniscalco Basile per la Regione.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  ricorso notificato il 21 maggio 1979, depositato il successivo  &#13;
 30, pubblicato nella Gazzetta Ufficiale n. 161 del  13  giugno  1979  e  &#13;
 iscritto  al  n.  10  Reg. Ric. 1979, il Commissario dello Stato per la  &#13;
 Regione siciliana ha  impugnato  a)  l'art.  3  del  disegno  di  legge  &#13;
 "recante  norme  per la prevenzione e la cura delle malattie da gozzo",  &#13;
 approvato dall'Assemblea regionale siciliana nella seduta notturna  del  &#13;
 16-17  maggio  1979  ("L'Assessore  regionale per la sanità, sentiti i  &#13;
 comuni e i medici provinciali interessati, autorizza, previo parere del  &#13;
 comitato regionale per la programmazione sanitaria, la  iodazione,  per  &#13;
 periodi  di  tempo  determinati, dell'acqua potabile da distribuire nei  &#13;
 comuni interessati alla endemia gozzigena.                               &#13;
     La  iodazione  dell'acqua  è  effettuata   a   cura   dei   comuni  &#13;
 interessati,  che possono avvalersi degli enti gestori degli acquedotti  &#13;
 comunali, salva la vigilanza degli  organi  sanitari  comunali")  e  b)  &#13;
 l'art.  5  del medesimo disegno di legge limitatamente all'inciso "lire  &#13;
 50  milioni  per  le  finalità  dell'art.  3   (iodazione   dell'acqua  &#13;
 potabile)",  per  violazione degli artt.   32 Cost. e 17 lett. b) dello  &#13;
 Statuto speciale.                                                        &#13;
     La  violazione dell'art. 32 viene dal Commissario ravvisata in ciò  &#13;
 che l'impugnato art. 3, con  attribuire  all'Assessorato  regionale  il  &#13;
 potere  di  autorizzare la iodazione dell'acqua potabile da distribuire  &#13;
 ai comuni interessati all'endemia gozzigena, configura per le  relative  &#13;
 popolazioni  un  trattamento sanitario, la cui obbligatorietà non può  &#13;
 essere imposta se non per legge.                                         &#13;
     La violazione dell'art. 17 lett. b) - a sensi del quale l'Assemblea  &#13;
 regionale, nelle materie dell'igiene e della sanità pubblica, può, al  &#13;
 fine di soddisfare alle condizioni particolari e agli interessi  propri  &#13;
 della  Regione,  emanare  leggi,  ma  entro  i  limiti  dei principi ed  &#13;
 interessi generali, cui si informa la legislazione dello Stato -  viene  &#13;
 dallo  stesso Commissario individuata in ciò che l'art. 33 della legge  &#13;
 23 dicembre 1978, n. 833 (istitutiva del servizio sanitario nazionale),  &#13;
 con precisare  che  trattamenti  sanitari  obbligatori  possono  essere  &#13;
 imposti  ai  cittadini  nei soli casi previsti dalle leggi dello Stato,  &#13;
 costituisce, per i servizi sanitari obbligatori, una riserva di legge a  &#13;
 favore dello Stato che le norme impugnate infrangono.                    &#13;
     Mediante deduzioni depositate il 5 giugno 1979, il Presidente della  &#13;
 Giunta regionale siciliana, per  il  quale  si  è  costituito,  giusta  &#13;
 procura  25  maggio 1979 per notar Di Giovanni, l'avv. Luigi Maniscalco  &#13;
 Basile, resiste al ricorso obiettando che la somministrazione di  acqua  &#13;
 potabile  iodata,  per  non  comportare l'assoggettamento del corpo del  &#13;
 soggetto passivo a visite mediche o a interventi terapeutici, non  deve  &#13;
 definirsi  trattamento  sanitario,  che  la  iodazione  contribuisce  a  &#13;
 rendere potabile l'acqua, che i cittadini han  la  scelta  tra  l'acqua  &#13;
 scorrente  negli  acquedotti  comunali  e le acque minerali, che tra le  &#13;
 leggi, cui l'art. 32 Cost. si riferisce, sono comprese anche  le  leggi  &#13;
 regionali.    Denuncia  poi  la Regione l'illegittimità dell'art.   33  &#13;
 della legge istitutiva del servizio  sanitario  nazionale  perché,  se  &#13;
 interpretato  nel  senso  prospettato  dal Commissario, si risolverebbe  &#13;
 nell'attribuzione allo Stato di  competenza  legislativa  esclusiva  in  &#13;
 materia,   in  cui  lo  Statuto  attribuisce  alla  Regione  competenza  &#13;
 legislativa concorrente (interpretazione quella del Commissario che, ad  &#13;
 avviso della difesa della Regione, sarebbe smentita dall'art. 80  della  &#13;
 stessa legge statale n. 833/1978).                                       &#13;
     Sotto  la  data  del  23 settembre 1980 l'Avvocatura generale dello  &#13;
 Stato ha depositato nella cancelleria della Corte parere  espresso  dal  &#13;
 Consiglio  Superiore  di  Sanità  nella  seduta  del  28  giugno 1979,  &#13;
 allegato a missiva diretta dal Commissario alla stessa Avvocatura.       &#13;
     Alla pubblica udienza del 12 novembre 1980, nella quale il  Giudice  &#13;
 Andrioli  ha  svolto  la relazione, le difese delle parti, pur concordi  &#13;
 nel reputare estraneo al dibattito nella presente sede  il  parere  del  &#13;
 Consiglio  Superiore di Sanità, esibito dall'Avvocatura generale dello  &#13;
 Stato, hanno illustrato le contrapposte argomentazioni insistendo nelle  &#13;
 già prese conclusioni<diritto>Considerato in diritto</diritto>:                          &#13;
     Sul presupposto che la iodazione, per periodi di tempo determinati,  &#13;
 dell'acqua potabile, da distribuirsi per il tramite degli enti  gestori  &#13;
 di  acquedotti  comunali, nei comuni interessati all'endemia gozzigena,  &#13;
 integri  gli  estremi  del  trattamento  sanitario   obbligatorio,   il  &#13;
 Commissario dello Stato contesta alla Regione la giuridica possibilità  &#13;
 di  realizzare tale operazione senza offendere, con le norme impugnate,  &#13;
 gli artt. 32, secondo comma della Costituzione della  Repubblica  e  17  &#13;
 lett. b) dello Statuto regionale.                                        &#13;
     La  impugnazione  del  Commissario  è  priva  di  fondamento e per  &#13;
 motivare il dispositivo di rigetto, che va a formulare, può  la  Corte  &#13;
 dispensarsi  dal verificare la sussistenza in concreto del presupposto,  &#13;
 su cui si  asside  la  impugnazione,  perché  né  l'uno  né  l'altro  &#13;
 parametro di costituzionalità è violato.                               &#13;
     L'invocazione  dell'art.  32,  secondo  comma,  anzi, è, a chi ben  &#13;
 guardi, un fuor d'opera perché la riserva di legge così stabilita non  &#13;
 esclude in assoluto le  leggi  regionali,  quanto  meno  delle  Regioni  &#13;
 differenziate  che  sono  titolari di specifiche funzioni in materia di  &#13;
 igiene e sanità pubblica, e ne dà conferma l'art.   80, primo  comma,  &#13;
 della  legge  n. 833/1978, istitutiva del servizio sanitario nazionale,  &#13;
 che fa salve le competenze statutarie delle Regioni a statuto speciale.  &#13;
     Talché tutto si riduce a domandarsi se l'Assemblea possa oppur  no  &#13;
 disporre  l'applicazione,  nel  territorio  regionale,  di  trattamenti  &#13;
 sanitari del tipo in questione, riguardanti non già la  risoluzione  e  &#13;
 la soddisfazione - sul piano regionale - di problemi ed esigenze aventi  &#13;
 dimensioni  nazionali,  ma  la  predisposizione  di misure destinate ad  &#13;
 applicarsi  in  singoli  comuni  dell'Isola,   per   "soddisfare   alle  &#13;
 condizioni particolari ed agli interessi propri della Regione", secondo  &#13;
 la  testuale  espressione  dell'art.  17  St. Sic., e proprio l'art. 17  &#13;
 lett. b) impone di  rispondere  in  senso  affermativo  perché  nessun  &#13;
 principio  o  interesse  generale,  cui s'informi la legislazione dello  &#13;
 Stato in subiecta materia, fissa limiti alla potestà legislativa della  &#13;
 Regione, la quale, per contro, è giustificata dalla triste realtà che  &#13;
 ha indotto l'Assemblea regionale a legiferare a più di  un  trentennio  &#13;
 dal suo insediamento (sempre, però, in anticipo rispetto ai competenti  &#13;
 organi  dello  Stato  che  non  hanno  sinora  avvertito  l'esigenza di  &#13;
 tradurre in disposizioni, aventi autorità per il territorio nazionale,  &#13;
 i criteri da seguire per combattere 1 'endemia gozzigena).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondate le questioni di legittimità costituzionale  -  &#13;
 per  violazione degli artt. 32 della Costituzione della Repubblica e 17  &#13;
 b) dello Statuto speciale della Regione siciliana  -  dell'art.  3  del  &#13;
 disegno  di  legge  approvato  dall'Assemblea regionale siciliana nella  &#13;
 seduta notturna del 16-17  maggio  1979  e  dell'art.  5  del  medesimo  &#13;
 disegno  di  legge  limitatamente  all'inciso  "lire  50 milioni per le  &#13;
 finalità dell'art. 3 (iodazione dell'acqua potabile)",  sollevate  con  &#13;
 il  ricorso  proposto il 21 maggio 1979 dal Commissario dello Stato per  &#13;
 la Regione siciliana.                                                    &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 21 novembre 1980.                             &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   - LEOPOLDO ELIA - GUGLIELMO ROEHRSSEN  &#13;
                                   - ORONZO REALE - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI   -  LIVIO  PALADIN  -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE FERRARI.  &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
