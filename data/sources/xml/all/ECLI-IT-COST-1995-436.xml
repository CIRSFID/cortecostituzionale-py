<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>436</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:436</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CAIANIELLO</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>06/09/1995</data_decisione>
    <data_deposito>15/09/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Vincenzo CAIANIELLO; &#13;
 Giudici: avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, &#13;
 dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco &#13;
 GUIZZI, prof. Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. &#13;
 Massimo VARI, dott. Cesare RUPERTO, dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 147 del codice    &#13;
 penale militare di pace promosso con ordinanza emessa il  19  ottobre    &#13;
 1994  dal  giudice  per  le  indagini preliminari presso il Tribunale    &#13;
 militare di  Roma  nel  procedimento  penale  a  carico  di  Brunelli    &#13;
 Vincenzo,  iscritta al n. 21 del registro ordinanze 1995 e pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 5, prima serie speciale,    &#13;
 dell'anno 1995;                                                          &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 12 luglio 1995 il Giudice    &#13;
 relatore Vincenzo Caianiello;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Il Giudice per le indagini preliminari  presso  il  Tribunale    &#13;
 militare  di  Roma,  in sede di decisione sulla richiesta di rinvio a    &#13;
 giudizio per il reato di allontanamento illecito  previsto  dall'art.    &#13;
 147,  primo  comma,  del codice penale militare di pace, solleva, con    &#13;
 ordinanza  del   19   ottobre   1994,   questione   di   legittimità    &#13;
 costituzionale  del  citato art. 147, in riferimento all'art. 3 della    &#13;
 Costituzione, "nella parte in cui punisce l'assenza dal servizio, per    &#13;
 un giorno, non ripetuta".                                                &#13;
    Osserva il  giudice  a  quo  che  la  norma  sanziona,  sia  nella    &#13;
 fattispecie di cui al primo comma (allontanamento non autorizzato dal    &#13;
 servizio,  che ricomprende secondo la giurisprudenza anche il mancato    &#13;
 rientro dalla libera uscita), sia in quella di cui al  secondo  comma    &#13;
 (ritardata  presentazione  al  servizio), le assenze dal servizio che    &#13;
 raggiungano la durata di almeno un  giorno  (e  che  non  superino  i    &#13;
 quattro, scattando altrimenti la fattispecie della diserzione).          &#13;
    L'incriminazione penale di una assenza rientrante nei limiti detti    &#13;
 è  sospettata  di  incostituzionalità,  in riferimento al parametro    &#13;
 dell'eguaglianza, nel raffronto con quanto dispone il regolamento  di    &#13;
 disciplina  militare (d.P.R. 18 luglio 1986, n. 545; adottato ex art.    &#13;
 5,  primo  comma,  della legge 11 luglio 1978, n. 382), che, al punto    &#13;
 31) dell'allegato C, stabilisce che può essere applicata la sanzione    &#13;
 disciplinare della consegna di  rigore  -  la  quale  consiste  nella    &#13;
 permanenza  fino  a  quindici giorni in apposito spazio dell'ambiente    &#13;
 militare - nei  confronti  del  militare  che  incorra  nel  "ritardo    &#13;
 ingiustificato  e  ripetuto superiore alle otto ore nel rientro dalla    &#13;
 libera uscita, dalla licenza o dal permesso".                            &#13;
    Ritiene il giudice a quo  che  la  presenza  del  requisito  della    &#13;
 ripetizione   nell'ambito   della   condotta   sanzionabile   in  via    &#13;
 disciplinare, e l'assenza invece di analogo requisito di  abitualità    &#13;
 nell'ambito   della  fattispecie  penale  denunziata,  determini  una    &#13;
 irragionevole   "rottura   nella    gradualità    del    trattamento    &#13;
 sanzionatorio  fra  condotte  di  assenza  dal  servizio  di  diversa    &#13;
 gravità". Da un lato - osserva il rimettente - un ritardo che, anche    &#13;
 per poco tempo, non raggiunga la durata minima di un giorno e  dunque    &#13;
 non  configuri  il  reato  di allontanamento illecito non può essere    &#13;
 sanzionato con la consegna di rigore ove non sia ripetuto: dunque per    &#13;
 comportamenti immediatamente contigui quanto a gravità si passerebbe    &#13;
 direttamente da una sanzione disciplinare di  minore  gravità  della    &#13;
 consegna di rigore alla sanzione penale.                                 &#13;
    Dall'altro,  la  ripetizione,  anche  per  una serie indefinita di    &#13;
 volte, di ritardi inferiori a un  giorno  ma  prossimi  a  quelli  di    &#13;
 rilievo penale, verrebbe comunque ad essere punita una sola volta con    &#13;
 la sanzione disciplinare della consegna di rigore, pur rappresentando    &#13;
 una  condotta  "complessivamente"  più grave di quella di un ritardo    &#13;
 isolato di almeno un giorno, penalmente rilevante.                       &#13;
    Unica soluzione per ovviare a simili inconvenienti è,  ad  avviso    &#13;
 del  rimettente,  quella di introdurre anche per la previsione penale    &#13;
 denunciata un requisito di abitualità analogo a quello stabilito  in    &#13;
 ambito  disciplinare,  onde  temperare  il  rigore  della  disciplina    &#13;
 punitiva, immutata dal 1941 ed ispirata a valori  diversi  da  quelli    &#13;
 recati dalla legge sulla disciplina militare del 1978.                   &#13;
    Si  osserva  inoltre  nell'ordinanza  di  rinvio  che l'intervento    &#13;
 richiesto alla  Corte  costituzionale  risulterebbe  in  armonia  con    &#13;
 recenti  proposte  di  modifica  legislativa  e  segnatamente con uno    &#13;
 schema di legge-delega  per  la  riforma  della  legislazione  penale    &#13;
 militare  di pace apprestato da apposita commissione ministeriale. In    &#13;
 detto elaborato si sottolinea  la  scarsa  o  nulla  offensività  di    &#13;
 condotte  occasionali  di  assenza  dal reparto per breve tempo, e al    &#13;
 contempo si rimarca l'esigenza di prevenire assenze ripetute, per  le    &#13;
 quali  la sanzione disciplinare appare un deterrente non sufficiente,    &#13;
 attraverso la prefigurazione "di un  reato  abituale".  Lo  strumento    &#13;
 ipotizzato  de  iure  condendo  coinciderebbe  dunque  con il tipo di    &#13;
 addizione richiesta alla Corte costituzionale.                           &#13;
    Non incide da  ultimo,  ai  fini  della  sollevata  questione,  il    &#13;
 requisito di procedibilità della richiesta del comandante di corpo (    &#13;
 ex  art.  260  c.p.m.p.),  giacché  nessun  criterio  è  fornito ai    &#13;
 comandanti  nell'esercizio  di  tale  potere  -  non   quello   della    &#13;
 "reiterazione"  o abitualità della condotta - e comunque perché nel    &#13;
 caso di specie la richiesta risulta proposta.                            &#13;
    2.- È intervenuto in giudizio il  Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato, che, allegando la sostanziale identità  della  questione  con    &#13;
 altra  sollevata sulla stessa norma, e già dichiarata manifestamente    &#13;
 infondata   (ordinanza   n.  448  del  1992),  conclude  per  analoga    &#13;
 declaratoria.<diritto>Considerato in diritto</diritto>1. - Il Giudice per le indagini preliminari  presso  il  Tribunale    &#13;
 militare  di  Roma dubita della legittimità costituzionale dell'art.    &#13;
 147 del codice penale militare di pace, che  punisce  come  reato  di    &#13;
 allontanamento  illecito l'assenza dal servizio militare superiore ad    &#13;
 un  giorno  (ed  inferiore  a  cinque  giorni  a  partire  dai  quali    &#13;
 l'infrazione   è  punita  come  reato  di  diserzione).  Si  profila    &#13;
 nell'ordinanza di rinvio il contrasto con l'art. 3 della Costituzione    &#13;
 per irragionevolezza del trattamento sanzionatorio che configura come    &#13;
 reato una assenza occasionale ed anche non  ripetuta,  nel  raffronto    &#13;
 con quanto stabilito in ambito disciplinare dal regolamento approvato    &#13;
 con d.P.R. n. 545 del 1986, il quale prevede per l'applicazione della    &#13;
 consegna  di  rigore  il  requisito  della  ripetizione  del  ritardo    &#13;
 ingiustificato e superiore alle otto ore, nel  rientro  dalla  libera    &#13;
 uscita o dal permesso.                                                   &#13;
    Si   propone   nell'ordinanza  di  rinvio  che,  per  riportare  a    &#13;
 ragionevolezza il sistema complessivo, si debba introdurre,  in  sede    &#13;
 di  giudizio  di  costituzionalità,  nell'ambito  della  fattispecie    &#13;
 prevista  come  reato  dalla  norma  impugnata,  il  requisito  della    &#13;
 abitualità  analogo a quello previsto per la fattispecie, sanzionata    &#13;
 come illecito disciplinare, del ritardo ingiustificato superiore alle    &#13;
 otto ore.                                                                &#13;
    2. - La questione non è fondata.                                     &#13;
    A parte il considerare che la soluzione proposta nell'ordinanza di    &#13;
 rinvio  suppone  un  intervento  di  natura  legislativa,  implicando    &#13;
 valutazioni e scelte discrezionali non consentite in sede di giudizio    &#13;
 di costituzionalità, sta di fatto che l'ordinanza di rinvio pone tra    &#13;
 loro   a   raffronto  situazioni  non  omogenee  che  il  legislatore    &#13;
 assoggetta a regimi sanzionatori diversi, rispettivamente  di  natura    &#13;
 disciplinare  in  un  caso  e  di  natura  penale  in  un altro. Tale    &#13;
 diversità di regime non fa apparire irragionevole la  configurazione    &#13;
 delle  rispettive fattispecie in modo diverso ed impedisce perciò di    &#13;
 poter  operare,  in  sede  di  giudizio  di   costituzionalità,   la    &#13;
 trasposizione  di un elemento della fattispecie indicata come tertium    &#13;
 comparationis  ed  assoggettata   a   sanzione   disciplinare,   alla    &#13;
 fattispecie  penalmente  sanzionata dalla norma denunciata. Non senza    &#13;
 rilevare,  in  ogni  caso,  che  il  profilo  di   censura   relativo    &#13;
 all'asserita  mancanza  di  gradualità  nel  passaggio dall'illecito    &#13;
 disciplinare a quello penale quanto  alle  condotte  di  assenza  dal    &#13;
 servizio  non tiene conto della possibilità, stabilita dall'art. 65,    &#13;
 settimo comma, lettera a) del già citato d.P.R. n. 545 del 1986,  di    &#13;
 sanzionare con la consegna di rigore fatti di rilievo penale ma per i    &#13;
 quali  il  comandante  di  corpo  non abbia formulato la richiesta di    &#13;
 procedimento; un potere questo che, se  oculatamente  esercitato,  è    &#13;
 idoneo ad attenuare il lamentato "salto" dalle punizioni disciplinari    &#13;
 più lievi della consegna di rigore alle pene militari.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  non  fondata  la questione di legittimità costituzionale    &#13;
 dell'art. 147 del codice  penale  militare  di  pace,  sollevata,  in    &#13;
 riferimento  all'art.  3  della  Costituzione,  dal  Giudice  per  le    &#13;
 indagini preliminari  presso  il  Tribunale  militare  di  Roma,  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 6 settembre 1995.                             &#13;
                 Il Presidente e redattore: CAIANIELLO                    &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 15 settembre 1995.                       &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
