<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>563</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:563</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Renato Dell'Andro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/12/1987</data_decisione>
    <data_deposito>18/12/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 136, primo    &#13;
 comma, della legge 25 settembre 1940, n. 1424 (Legge doganale) e 329,    &#13;
 primo  comma,  del  d.P.R.  23  gennaio 1973, n. 43 (Approvazione del    &#13;
 testo unico delle  disposizioni  legislative  in  materia  doganale),    &#13;
 promosso  con  ordinanza  emessa  il 15 ottobre 1979 dal Tribunale di    &#13;
 Savona, iscritta al n. 210 del registro ordinanze 1980  e  pubblicata    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 138 dell'anno 1980;         &#13;
    Visto  l'atto  di  costituzione della S.p.A. Campanella - Cantieri    &#13;
 navali e l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera di consiglio del 30 settembre 1987 il Giudice    &#13;
 relatore Renato Dell'Andro;                                              &#13;
    Ritenuto  che, con l'ordinanza in epigrafe, il Tribunale di Savona    &#13;
 ha sollevato, in riferimento agli artt. 3, primo  comma,  24, secondo    &#13;
 comma,   e   27,   terzo  comma,  della  Costituzione,  questione  di    &#13;
 legittimità costituzionale degli artt. 136, primo comma, della legge    &#13;
 25  settembre  1940, n. 1424 (Legge doganale) e 329, primo comma, del    &#13;
 d.P.R. 23 gennaio 1973, n. 43 (Approvazione  del  testo  unico  delle    &#13;
 disposizioni  legislative in materia doganale), in quanto configurano    &#13;
 la civile responsabilità del titolare di stabilimenti industriali in    &#13;
 cui viene commesso delitto di contrabbando;                              &#13;
      che  è  intervenuto in giudizio il Presidente del Consiglio dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  Generale  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata non fondata;            &#13;
      che  innanzi  alla Corte si è costituita la S.p.A. "Campanella"    &#13;
 Cantieri Navali, rappresentata e  difesa  dall'Avv.  Corrado  Medina,    &#13;
 concludendo per l'accoglimento della questione di costituzionalità;     &#13;
    Considerato  che  gli  artt. 136, primo comma, della legge n. 1424    &#13;
 del 1940, e 329, primo comma, del d.P.R. n. 43 del  1973,  contengono    &#13;
 regole  di  diritto  sostanziale  che  non incidono in alcun modo sui    &#13;
 poteri  delle  parti  nel  processo  né,  tanto   meno,   comportano    &#13;
 limitazioni alla facoltà di prova delle parti stesse;                   &#13;
      che,  di  conseguenza,  non  può  dirsi violato il diritto alla    &#13;
 difesa di cui all'art. 24, secondo comma, Cost.;                         &#13;
      che,   d'altra  parte,  questa  Corte,  nel  pronunciarsi  sulla    &#13;
 legittimità  costituzionale  delle  obbligazioni  civili  da   reato    &#13;
 previste  dall'art. 196 Cod. pen., ha escluso la natura di pena delle    &#13;
 obbligazioni medesime (cfr. Sent. n. 40 del 1966);                       &#13;
      che  la  previsione  dell'obbligazione  civile  contenuta  nelle    &#13;
 disposizioni impugnate costituisce una specificazione  del  principio    &#13;
 di  cui  all'art.  196  Cod.  pen.  e, quindi, del tutto inconferente    &#13;
 appare la censura sollevata in riferimento all'art. 27, terzo  comma,    &#13;
 che attiene alla funzione rieducativa della pena e non delle sanzioni    &#13;
 civili conseguenti all'accertamento del reato;                           &#13;
      che,  infine,  la  diversità  di natura tra sanzione pecuniaria    &#13;
 infliggibile al condannato ed  obbligazione  civile  del  committente    &#13;
 (cfr.,  ancora,  Sent.  n.  40  del 1966) fa sì che non possa essere    &#13;
 paragonata la posizione dell'autore del  reato  di  contrabbando  con    &#13;
 quella dell'obbligato civilmente ai sensi degli articoli impugnati;      &#13;
      che,  pertanto,  la  questione  di  legittimità  costituzionale    &#13;
 sollevata dal Tribunale di  Savona  è  da  ritenersi  manifestamente    &#13;
 infondata  anche  in riferimento all'assunta violazione del principio    &#13;
 di eguaglianza;                                                          &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1956, n.    &#13;
 87, e 9, secondo comma, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte Costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 136, primo comma, della legge 25 settembre    &#13;
 1940, n. 1424, e 329, primo comma, del d.P.R. 23 gennaio 1973, n. 43,    &#13;
 sollevata, in riferimento agli artt.  3,  primo  comma,  24,  secondo    &#13;
 comma,  e  27,  terzo  comma,  Cost.,  dal  Tribunale  di  Savona con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 10 dicembre 1987.       &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: DELL'ANDRO                           &#13;
    Depositata in cancelleria il 18 dicembre 1987.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
