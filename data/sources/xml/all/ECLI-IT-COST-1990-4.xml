<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>4</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:4</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>13/12/1989</data_decisione>
    <data_deposito>02/01/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 1, primo e    &#13;
 terzo comma; 7, primo e terzo comma; 8, quarto comma  e  9,  primo  e    &#13;
 terzo  comma,  della  legge  13 aprile 1988, n. 117 (Risarcimento dei    &#13;
 danni  cagionati  nell'esercizio   delle   funzioni   giudiziarie   e    &#13;
 responsabilità civile dei magistrati), promosso con ordinanza emessa    &#13;
 il 5 aprile 1989 dal Pretore di Roma nel procedimento civile vertente    &#13;
 tra Accordato Roberto e la s.r.l. "RI Immobiliare" iscritta al n. 349    &#13;
 del registro ordinanze 1989 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 35, prima serie speciale, dell'anno 1989;            &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di consiglio del 29 novembre 1989 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto  che il Vice pretore di Roma, con ordinanza 5 aprile 1989    &#13;
 (R.O. n.  349  del  1989)  ha  sollevato  questione  di  legittimità    &#13;
 costituzionale  degli  artt. 1, primo e terzo comma; 7, primo e terzo    &#13;
 comma; 8, quarto comma e 9, primo  e  terzo  comma,  della  legge  13    &#13;
 aprile 1988, n. 117;                                                     &#13;
      che  secondo  tale  ordinanza  essi  violerebbero l'art. 3 della    &#13;
 Costituzione,  in  quanto  irrazionalmente   equiparano   il   limite    &#13;
 quantitativo di responsabilità del vice pretore onorario, in sede di    &#13;
 rivalsa, a quella dei  magistrati  di  tribunale  e  stabiliscono  un    &#13;
 regime  unitario  di  responsabilità per i vice pretori onorari ed i    &#13;
 giudici togati, non ostante che le funzioni dei primi,  a  differenza    &#13;
 di  quelle  dei secondi, sarebbero disciplinate non solo dalla legge,    &#13;
 ma anche dalla prassi e vi sarebbe una differenza di  situazioni  che    &#13;
 renderebbe     irragionevole    la    disciplina    unitaria    della    &#13;
 responsabilità;                                                         &#13;
    Considerato  che  questa  Corte, con ordinanza n. 155 del 1989, ha    &#13;
 già ritenuto manifestamente infondata  la  questione  relativa  alla    &#13;
 determinazione  della  misura  massima  della rivalsa dello Stato nei    &#13;
 confronti degli estranei che partecipano o concorrono  a  partecipare    &#13;
 all'esercizio   della   funzione  giudiziaria,  così  come  disposta    &#13;
 dall'art. 8, ultimo comma,  della  legge  13  aprile  1988,  n.  117,    &#13;
 poiché rientra nell'esercizio della discrezionalità legislativa non    &#13;
 censurabile in quanto  non  irragionevole  -  la  determinazione  dei    &#13;
 limiti  massimi  di  tale  rivalsa  in relazione al reddito di lavoro    &#13;
 complessivo dei giudici non togati;                                      &#13;
      che   nessun  rilievo  può  avere,  ai  fini  del  giudizio  di    &#13;
 costituzionalità l'asserita esistenza di situazioni differenziate in    &#13;
 base a "prassi", proprie dell'attività dei vice pretori onorari;        &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale degli artt. 1, primo e terzo comma; 7, primo  e  terzo    &#13;
 comma;  8,  quarto  comma  e  9,  primo e terzo comma, della legge 13    &#13;
 aprile 1988, n. 117 (Risarcimento dei danni cagionati  nell'esercizio    &#13;
 delle  funzioni giudiziarie e responsabilità civile dei magistrati),    &#13;
 sollevata  in  riferimento  all'art.  3   della   Costituzione,   con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 13 dicembre 1989.                             &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 gennaio 1990.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
