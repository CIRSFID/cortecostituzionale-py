<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>98</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:98</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/03/1995</data_decisione>
    <data_deposito>30/03/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 1 della legge 22    &#13;
 giugno 1988, n. 221  (provvedimenti  a  favore  del  personale  delle    &#13;
 cancellerie e segreterie giudiziarie), come interpretato dall'art. 3,    &#13;
 sessantunesimo   comma,   della   legge  24  dicembre  1993,  n.  537    &#13;
 (interventi  correttivi  di  finanza  pubblica),  promossi  con   due    &#13;
 ordinanze  emesse  il  13  luglio  e il 2 febbraio 1994 dal Tribunale    &#13;
 amministrativo regionale del Lazio sui ricorsi proposti  da  Giuseppa    &#13;
 Abatini  ed  altri contro la Presidenza del Consiglio dei ministri ed    &#13;
 altri e da Carlo Santella ed altri contro il Ministero  di  grazia  e    &#13;
 giustizia  ed  altri,  iscritte  rispettivamente ai nn. 656 e 701 del    &#13;
 registro ordinanze 1994 e pubblicate nella Gazzetta  Ufficiale  della    &#13;
 Repubblica nn. 46 e 49, prima serie speciale, dell'anno 1994;            &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito nella camera di  consiglio  dell'8  marzo  1995  il  Giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
    Ritenuto  che,  nel  corso di altrettanti procedimenti promossi da    &#13;
 dipendenti del  Ministero  di  grazia  e  giustizia  e  da  personale    &#13;
 amministrativo    in   servizio   presso   organi   della   giustizia    &#13;
 amministrativa, il Tribunale amministrativo regionale del Lazio,  con    &#13;
 due  distinte  ordinanze di identico contenuto emesse il 13 luglio ed    &#13;
 il 2 febbraio 1994, ha sollevato, in riferimento agli artt.  3  e  36    &#13;
 della   Costituzione,   questioni   di   legittimità  costituzionale    &#13;
 dell'art. 1 della legge 22  giugno  1988,  n.  221  (provvedimenti  a    &#13;
 favore  del  personale  delle  cancellerie e segreterie giudiziarie),    &#13;
 come interpretato dall'art. 3, sessantunesimo comma, della  legge  24    &#13;
 dicembre 1993, n. 537 (Interventi correttivi di finanza pubblica);       &#13;
      che l'art. 3, sessantunesimo comma, della legge n. 537 del 1993,    &#13;
 autoqualificandosi    come    norma   interpretativa,   prevede   che    &#13;
 l'indennità  concessa  dalla  legge  19  febbraio  1981,  n.  27  ai    &#13;
 magistrati  ed  attribuita dall'art. 1 della legge 22 giugno 1988, n.    &#13;
 221 al personale delle cancellerie  e  delle  segreterie  giudiziarie    &#13;
 (successivamente    estesa    al   personale   amministrativo   delle    &#13;
 magistrature speciali dalla legge  15  febbraio  1989,  n.  51),  sia    &#13;
 corrisposta  al  personale  amministrativo  giudiziario  nella misura    &#13;
 vigente al 1 gennaio 1988, senza l'adeguamento  automatico  triennale    &#13;
 stabilito per i magistrati;                                              &#13;
      che    le    ordinanze   di   rimessione,   nel   rilevare   che    &#13;
 l'interpretazione autentica dettata dalla norma impugnata si discosta    &#13;
 dalla costante lettura giurisprudenziale, prospettano  il  dubbio  di    &#13;
 legittimità  costituzionale  in quanto l'esclusione dell'adeguamento    &#13;
 triennale sarebbe in contrasto con il principio di eguaglianza  (art.    &#13;
 3 della Costituzione), determinando una non ragionevole disparità di    &#13;
 trattamento  tra  magistrati  e personale di cancelleria e segreteria    &#13;
 nei meccanismi di calcolo di una analoga indennità. Inoltre  sarebbe    &#13;
 leso   il   principio   di   adeguatezza   e  proporzionalità  della    &#13;
 retribuzione (art. 36 della Costituzione), giacché  l'indennità  è    &#13;
 da  tempo  componente  del trattamento economico del personale che ne    &#13;
 beneficia;                                                               &#13;
      che in entrambi i  giudizi  è  intervenuto  il  Presidente  del    &#13;
 Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale dello Stato, che ha concluso per la  manifesta  infondatezza    &#13;
 delle questioni;                                                         &#13;
    Considerato  che  le  ordinanze di rimessione hanno per oggetto la    &#13;
 stessa  norma  e  prospettano  i  medesimi  profili  di  legittimità    &#13;
 costituzionale, sicché i giudizi vanno riuniti per essere decisi con    &#13;
 unica pronuncia;                                                         &#13;
      che identiche questioni di legittimità costituzionale sono già    &#13;
 state  esaminate  e  dichiarate non fondate con la sentenza n. 15 del    &#13;
 1995, che ha ritenuto non  irragionevole  la  mancata  estensione  al    &#13;
 personale  amministrativo  del  meccanismo  di adeguamento automatico    &#13;
 previsto per i magistrati, data la mancanza di omogeneità tra le due    &#13;
 categorie di dipendenti e le diverse modalità di determinazione  del    &#13;
 loro    trattamento    retributivo,   basato,   per   i   magistrati,    &#13;
 sull'aggiornamento periodico nella misura percentuale pari alla media    &#13;
 degli incrementi realizzati  dai  pubblici  dipendenti  e  non  sulle    &#13;
 regole  comuni  del  pubblico  impiego,  che  si  applicano invece al    &#13;
 personale   amministrativo   giudiziario.  Inoltre  il  principio  di    &#13;
 proporzionalità  e  sufficienza  della  retribuzione   non   implica    &#13;
 l'indicizzazione  della  retribuzione,  che  deve essere in ogni caso    &#13;
 valutata nel  suo  complesso  e  non  con  riferimento  ad  una  sola    &#13;
 indennità, che concorre a comporre il trattamento retributivo;          &#13;
      che le ordinanze introduttive del presente giudizio non adducono    &#13;
 motivazioni  diverse  ed  ulteriori  rispetto a quelle già esaminate    &#13;
 dalla Corte;                                                             &#13;
      che,   conseguentemente,   le    questioni    di    legittimità    &#13;
 costituzionale devono essere dichiarate manifestamente infondate;        &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  delle    &#13;
 questioni di legittimità costituzionale dell'art. 1 della  legge  22    &#13;
 giugno  1988,  n.  221  (Provvedimenti  a  favore del personale delle    &#13;
 cancellerie  e  segreterie  giudiziarie),  così  come   interpretato    &#13;
 dall'art.  3,  sessantunesimo comma, della legge 24 dicembre 1993, n.    &#13;
 537  (Interventi  correttivi  di  finanza  pubblica),  sollevate,  in    &#13;
 riferimento  agli  artt.  3  e  36  della Costituzione, dal Tribunale    &#13;
 amministrativo regionale del  Lazio  con  le  ordinanze  indicate  in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 20 marzo 1995.                                &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                        Il redattore: MIRABELLI                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 30 marzo 1995.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
