<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1972</anno_pronuncia>
    <numero_pronuncia>205</numero_pronuncia>
    <ecli>ECLI:IT:COST:1972:205</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>MORTATI</presidente>
    <relatore_pronuncia>Francesco Paolo Bonifacio</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/12/1972</data_decisione>
    <data_deposito>29/12/1972</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. COSTANTINO MORTATI, Presidente - Dott. &#13;
 GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO &#13;
 PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO - Avv. &#13;
 ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI - Avv. LEONETTO AMADEI - Prof. GIULIO GIONFRIDA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei giudizi riuniti di legittimità costituzionale dell'art.  2054,  &#13;
 primo e secondo comma, del codice  civile,  promossi  con  le  seguenti  &#13;
 ordinanze:                                                               &#13;
     1)  ordinanza  emessa  il 23 novembre 1970 dal pretore di Lucca nel  &#13;
 procedimento civile vertente tra Tambellini Giuseppe e  Napoli  Benito,  &#13;
 iscritta  al  n.  379  del  registro  ordinanze 1970 e pubblicata nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 42 del 17 febbraio 1971;          &#13;
     2) ordinanza emessa il 9 gennaio  1971  dal  pretore  di  Roma  nel  &#13;
 procedimento  civile  vertente tra Paleani Paola e Stramaccioni Sergio,  &#13;
 iscritta al n. 62  del  registro  ordinanze  1971  e  pubblicata  nella  &#13;
 Gazzetta Ufficiale della Repubblica n. 99 del 21 aprile 1971.            &#13;
     Visto   l'atto   d'intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza  pubblica  del  22  novembre  1972  il  Giudice  &#13;
 relatore Francesco Paolo Bonifacio;                                      &#13;
     udito  il sostituto avvocato generale dello Stato Michele Savarese,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  Con  ordinanza  del  23 novembre 1970 il pretore di Lucca ha  &#13;
 proposto, in riferimento all'art. 3 della Costituzione,  una  questione  &#13;
 di  legittimità costituzionale concernente i primi due commi dell'art.  &#13;
 2054 del codice civile.                                                  &#13;
     Muovendo dal presupposto che il secondo comma di tale articolo - in  &#13;
 forza del quale nel caso di scontro tra veicoli senza guida  di  rotaie  &#13;
 si  presume,  fino a prova contraria, che ciascuno dei conducenti abbia  &#13;
 concorso egualmente a produrre il danno subito dai  singoli  veicoli  -  &#13;
 riguardi  solo l'ipotesi che tutti i veicoli coinvolti nella collisione  &#13;
 abbiano subi'to danni e che, invece, nel  caso  di  scontro  con  danni  &#13;
 unilaterali  debba  applicarsi  il  disposto  del primo comma (si debba  &#13;
 presumere, cioè, la  colpa  del  solo  veicolo  non  danneggiato),  il  &#13;
 giudice  a  quo ravvisa in tale disciplina una violazione del principio  &#13;
 di  eguaglianza:    sarebbe,  infatti,  arbitrario   ed   irragionevole  &#13;
 assoggettare  l'ipotesi  dello scontro a due diversi regimi secondo che  &#13;
 ci siano stati danni reciproci o danni unilaterali.                      &#13;
     A  tale  conclusione  dovrebbe  portare,  secondo  l'ordinanza   di  &#13;
 rimessione,   la   constatazione   di   una  duplice  incoerenza  delle  &#13;
 disposizioni impugnate: a)  la  ratio  del  secondo  comma  poggia  sul  &#13;
 criterio dell'id quod plerumque accidit, per il quale il verificarsi di  &#13;
 uno  scontro tra veicoli non è a priori attribuibile all'uno piuttosto  &#13;
 che all'altro conducente coinvolto nell'incidente, sicché  appare  non  &#13;
 ragionevole  limitare  l'efficacia  della norma alla sola ipotesi dello  &#13;
 scontro con danni  reciproci;  b)  la  ratio  del  primo  comma  poggia  &#13;
 chiaramente  sul principio cuius commoda et eius incommoda, attuando il  &#13;
 quale il legislatore ha voluto addossare una  presunzione  di  colpa  a  &#13;
 carico  di chi si avvale di un mezzo ritenuto pericoloso, ed è perciò  &#13;
 arbitrario presumere, nel caso di scontro con danni  unilaterali,  solo  &#13;
 la  colpa  del  conducente  del veicolo non danneggiato: in questo caso  &#13;
 entrambi i soggetti si sono avvalsi di un veicolo  e  coerentemente  la  &#13;
 legge dovrebbe porre per entrambi una presunzione di egual concorso.     &#13;
     Constatata tale duplice incoerenza, il pretore di Lucca osserva che  &#13;
 essa  scomparirebbe ove, attraverso una dichiarazione di illegittimità  &#13;
 costituzionale, la presunzione a carico di entrambi i conducenti  fosse  &#13;
 estesa  ad ogni ipotesi di scontro tra veicoli, ne siano derivati danni  &#13;
 reciproci o unilaterali.                                                 &#13;
     2. - Innanzi a questa Corte è intervenuto - con atto di  deduzioni  &#13;
 del  24  febbraio  1971  -  il  Presidente  del Consiglio dei ministri,  &#13;
 rappresentato dall'Avvocatura generale dello Stato.                      &#13;
     La difesa dello Stato, dopo aver esposto gli argomenti in  base  ai  &#13;
 quali  la  costante giurisprudenza della Corte di cassazione ha escluso  &#13;
 che il secondo comma dell'art. 2054 cod. civ. si applichi  nell'ipotesi  &#13;
 di  scontro  con  danni unilaterali e dopo aver ricordato gli argomenti  &#13;
 addotti dalla dottrina e da una parte della giurisprudenza di merito  a  &#13;
 sostegno  dell'opposta  tesi,  sostiene che la norma in esame, anche se  &#13;
 interpretata nel primo senso, non travalica i  confini  entro  i  quali  &#13;
 legittimamente  si  muove la discrezionalità politica del legislatore.  &#13;
 Non potendosi nella  specie  rilevare  un  manifesto  arbitrio  ed  una  &#13;
 patente  irragionevolezza, la questione - così conclude l'Avvocatura -  &#13;
 è infondata.                                                            &#13;
     3. - Anche ad avviso del pretore di Roma - ordinanza 9 gennaio 1971  &#13;
 - il secondo comma dell'art. 2054 cod. civ.,  interpretato  secondo  il  &#13;
 costante indirizzo giurisprudenziale della Corte di cassazione, darebbe  &#13;
 luogo   ad   una   disparità  di  trattamento,  non  sorretta  da  una  &#13;
 giustificazione razionale, secondo che  dallo  scontro  derivino  danni  &#13;
 reciproci  od  unilaterali: la disposizione sarebbe dunque illegittima,  &#13;
 in  riferimento  all'art. 3 Cost., nella parte in cui essa subordina la  &#13;
 presunzione di colpa reciproca alla reciprocità dei danni.              &#13;
     4. - In quest'ultimo giudizio non si  è  costituita  alcuna  delle  &#13;
 parti e non è intervenuto il Presidente del Consiglio dei ministri.     &#13;
     5. - Nell'udienza pubblica l'Avvocatura dello Stato si è riportata  &#13;
 alle conclusioni contenute nell'atto d'intervento.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.   -   Le   due   ordinanze   indicate   in  epigrafe  propongono  &#13;
 sostanzialmente la stessa questione di  legittimità  costituzionale  e  &#13;
 pertanto  i  relativi  giudizi  vengono  riuniti  e  decisi  con  unica  &#13;
 sentenza.                                                                &#13;
     2. - Il secondo comma dell'art. 2054 cod. civ. stabilisce  che  nel  &#13;
 caso  di  scontro  tra  veicoli si presume, fino a prova contraria, che  &#13;
 ciascuno dei conducenti abbia egualmente concorso a produrre  il  danno  &#13;
 subito  dai  singoli veicoli.  Secondo la costante giurisprudenza della  &#13;
 Corte di cassazione, la disposizione viene  intesa  nel  senso  che  la  &#13;
 presunzione  di  eguale  concorso  opera  solo  se  entrambi  i veicoli  &#13;
 coinvolti nella collisione abbiano riportato danni, e non anche se  uno  &#13;
 di  essi  sia  rimasto  indenne: a causa della così delineata sfera di  &#13;
 applicazione del secondo comma, quest'ultimo caso deve trovare  altrove  &#13;
 la  sua  disciplina,  precisamente  nel  disposto del primo comma dello  &#13;
 stesso articolo, con la  conseguente  presunzione  a  carico  del  solo  &#13;
 conducente del veicolo non danneggiato.                                  &#13;
     Sul   presupposto   di  siffatta  interpretazione  -  intorno  alla  &#13;
 validità della quale,  stante  il  consolidato  ed  univoco  indirizzo  &#13;
 giurisprudenziale  cui  si è fatto cenno, non è opportuno indugiare -  &#13;
 questa Corte  è  chiamata  a  decidere  se  la  diversità  di  regime  &#13;
 giuridico  concernente  lo scontro, secondo che ne siano derivati danni  &#13;
 reciproci o unilaterali, dia luogo, in violazione dell'art. 3 Cost., ad  &#13;
 una illegittima disparità di trattamento.                               &#13;
     3. - La questione è fondata.                                        &#13;
     Vero è che le due situazioni che qui vanno raffrontate  -  scontro  &#13;
 con  danni  reciproci,  scontro  con danni unilaterali - presentano fra  &#13;
 loro una qualche diversità, ma ciò,  tuttavia,  non  è  di  per  sé  &#13;
 sufficiente  a  far  concludere  che  legittimamente  esse  siano state  &#13;
 sottoposte  a  discipline  differenziate.  Conformemente  ai   principi  &#13;
 affermati  da  questa  Corte  nella giurisprudenza concernente l'art. 3  &#13;
 Cost., occorre infatti verificare  se  il  legislatore,  dando  rilievo  &#13;
 all'elemento  di diversificazione (danni reciproci o danni unilaterali)  &#13;
 piuttosto che all'elemento comune alle  due  fattispecie  (scontro  tra  &#13;
 veicoli),  non  abbia  arbitrariamente  considerato diverse due ipotesi  &#13;
 che, almeno  ai  fini  che  qui  interessano,  avrebbero  dovuto  esser  &#13;
 valutate come eguali.                                                    &#13;
     In quest'ordine di idee occorre porre in rilievo che la presunzione  &#13;
 di  un  egual concorso nello scontro, posta a carico dei conducenti dal  &#13;
 secondo comma dell'art. 2054 cod. civ., è del tutto  svincolata  dalla  &#13;
 proporzione dei danni derivati ai singoli veicoli: la maggiore o minore  &#13;
 entità  di  tali  danni  gioca,  è ovvio, sul quantum dovuto dall'uno  &#13;
 all'altro soggetto, ma nessuna influenza  spiega  sulla  determinazione  &#13;
 della  quota  della  loro corresponsabilità, che per tutti è presunta  &#13;
 eguale. Ciò significa che,  conformemente  ad  intuitive  esigenze  di  &#13;
 razionalità,  le  conseguenze della collisione alla quale i conducenti  &#13;
 hanno materialmente concorso non sono  assunte  ad  indice  della  loro  &#13;
 (maggiore  o minore) responsabilità nell'aver provocato lo scontro, ed  &#13;
 è  perciò arbitrario ed irragionevole che tale funzione esse assumano  &#13;
 quando non entrambi i veicoli  siano  stati  danneggiati.  Nel  vigente  &#13;
 regime  dello scontro con danni unilaterali la responsabilità presunta  &#13;
 del solo conducente del veicolo non danneggiato vien  fatta  discendere  &#13;
 da un elemento accidentale e casuale, da una circostanza, cioè, che è  &#13;
 razionalmente inidonea a far presumere, in mancanza di prova contraria,  &#13;
 che nel determinare la collisione non abbia concorso anche la colpa del  &#13;
 conducente  del  veicolo  danneggiato.  La  conseguente  disparità  di  &#13;
 trattamento  risulta  di  tutta  evidenza  ove  si  consideri  che   la  &#13;
 operatività della presunzione di egual concorso, collegata ad un fatto  &#13;
 esterno  rispetto all'azione dei soggetti, è affidata al mero caso: al  &#13;
 limite, l'assenza di danno o la presenza di un danno di minima  entità  &#13;
 determina l'applicazione di regole giuridiche profondamente diverse.     &#13;
     In  definitiva  si deve concludere che, quanto alla responsabilità  &#13;
 dei conducenti, la fattispecie "scontro"  è  sostanzialmente  identica  &#13;
 quali  che  siano le conseguenze dannose che ne son derivate e non può  &#13;
 perciò  non  essere  assoggettata  ad  una  disciplina  unitaria:   la  &#13;
 differenza    di    regime,    dipendente   da   mera   accidentalità,  &#13;
 inevitabilmente comporta una disparità di  trattamento  di  situazioni  &#13;
 sostanzialmente  eguali  e,  di  conseguenza, la violazione dell'art. 3  &#13;
 della Costituzione.                                                      &#13;
     4. - Per le considerazioni esposte, il secondo comma dell'art. 2054  &#13;
 cod. civ. deve essere dichiarato illegittimo nella parte in cui esclude  &#13;
 che, in mancanza di prova contraria, la presunzione di  egual  concorso  &#13;
 dei  conducenti  valga  anche  nell'ipotesi  in  cui  uno  dei  veicoli  &#13;
 coinvolti nello scontro non abbia subito danni.                          &#13;
     Con questa statuizione è esaurito l'intero  thema  decidendum.  La  &#13;
 questione  proposta  dal  pretore di Lucca ha investito invero anche il  &#13;
 primo comma dello stesso articolo, ma tale denuncia è stata  formulata  &#13;
 sul  presupposto  che  all'ipotesi di scontro con danni unilaterali non  &#13;
 fosse applicabile, de iure condito, il secondo comma:  presupposto  che  &#13;
 ovviamente   vien  meno  a  seguito  della  dichiarazione  di  parziale  &#13;
 illegittimità di quest'ultimo.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara l'illegittimità costituzionale  dell'art.  2054,  secondo  &#13;
 comma,  del  codice civile, limitatamente alla parte in cui nel caso di  &#13;
 scontro fra veicoli esclude che la presunzione di  egual  concorso  dei  &#13;
 conducenti operi anche se uno dei veicoli non abbia riportato danni.     &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 14 dicembre 1972.                             &#13;
                                       COSTANTINO  MORTATI  -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI - VEZIO  CRISAFULLI  &#13;
                                   -   NICOLA  REALE  -  PAOLO  ROSSI  -  &#13;
                                   LEONETTO AMADEI . GIULIO GIONFRIDA.    &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
