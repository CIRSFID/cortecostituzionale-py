<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1957</anno_pronuncia>
    <numero_pronuncia>50</numero_pronuncia>
    <ecli>ECLI:IT:COST:1957:50</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AZZARITI</presidente>
    <relatore_pronuncia>Biagio Petrocelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/04/1957</data_decisione>
    <data_deposito>13/04/1957</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Dott. GAETANO AZZARITI, Presidente - Avv. &#13;
 GIUSEPPE CAPPI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - &#13;
 Prof. ERNESTO BATTAGLINI - Dott. MARIO COSATTI - Prof. FRANCESCO &#13;
 PANTALEO GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO &#13;
 PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO, Prof. BIAGIO &#13;
 PETROCELLI- Dott. ANTONIO MANCA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 11 e 13 del  &#13;
 R.D.L. 14 novembre 1926, n. 1923, promosso con  l'ordinanza  16  luglio  &#13;
 1956  del  Giudice  istruttore  presso  il  Tribunale  di  Bolzano  nel  &#13;
 procedimento penale a carico  di  Nonna  Ruperto,  Bartolotti  Augusto,  &#13;
 Gilardelli  Alberto ed altri, pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 266 del 20  ottobre  1956  ed  iscritta  al  n.  304  del  &#13;
 Registro ordinanze 1956.                                                 &#13;
     Vista  la  dichiarazione di intervento del Presidente del Consiglio  &#13;
 dei Ministri;                                                            &#13;
     udita all'udienza pubblica del  13  marzo  1957  la  relazione  del  &#13;
 Giudice Biagio Petrocelli;                                               &#13;
     udito il sostituto avvocato generale dello Stato Achille Salerni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Davanti  al Giudice istruttore di Bolzano era in corso procedimento  &#13;
 penale a  carico  di  Nonna  Ruperto,  Bartolotti  Augusto,  Gilardelli  &#13;
 Alberto  ed  altri, per concorso nel reato previsto dagli artt. 11 e 13  &#13;
 del R. D.  L. 14 novembre 1926, n. 1923, per avere esportato fuori  dei  &#13;
 confini  della  Repubblica  110  tonnellate  di  zolfo doppio raffinato  &#13;
 ventilato, senza licenza ministeriale,  merce  che  non  poteva  essere  &#13;
 esportata  senza  licenza,  e  che  fu  esportata servendosi di licenza  &#13;
 intestata ad altri  nomi  e  per  altra  merce.  La  difesa  del  Nonna  &#13;
 presentava  richiesta  di  sospensione  del  procedimento, assumendo la  &#13;
 illegittimità costituzionale del R. D.L. 14  novembre  1926,  n.  1923  &#13;
 (senza  specificare  gli  articoli).  Il Giudice istruttore, in data 16  &#13;
 luglio 1956, ritenuto che la sollevata eccezione di incostituzionalità  &#13;
 appariva non manifestamente  infondata  rimetteva  gli  atti  a  questa  &#13;
 Corte, sospendendo il giudizio.                                          &#13;
     L'ordinanza  fu notificata al Presidente del Consiglio dei Ministri  &#13;
 il 23 luglio 1956, comunicata ai Presidenti della Camera e  del  Senato  &#13;
 il 18 luglio 1956, e pubblicata nella Gazzetta Ufficiale del 20 ottobre  &#13;
 1956.    Il  12  agosto  1956 furono depositati nella cancelleria della  &#13;
 Corte l'atto di intervento e le deduzioni dell'Avvocatura  dello  Stato  &#13;
 in  rappresentanza del Presidente del Consiglio dei Ministri. Non vi è  &#13;
 stata costituzione nell'interesse degli imputati.                        &#13;
     Nel   suo   atto  di  intervento  l'Avvocatura  dello  Stato  così  &#13;
 concludeva: "piaccia alla Corte:  in  via  principale,  dichiarare  non  &#13;
 esser  luogo  a giudizio di legittimità costituzionale della questione  &#13;
 come sopra dedotta, e  quindi  dichiarare  inammissibile  la  questione  &#13;
 medesima;  in via subordinata, dichiarare non sussistere illegittimità  &#13;
 costituzionale delle norme denunciate e,  conseguentemente,  dichiarare  &#13;
 la legittimità costituzionale delle disposizioni medesime".             &#13;
     Al   riguardo   la   stessa  Avvocatura  eccepiva  il  difetto  del  &#13;
 presupposto del giudizio di legittimità, osservando che  si  riscontra  &#13;
 nella  specie  una  nullità  di  ordine  formale,  poiché  il giudice  &#13;
 dell'incidente si è limitato con la sua ordinanza al rilievo  generico  &#13;
 che  gli artt. 11 e 13 del R. D.L. 14 novembre 1926, n. 1923, sarebbero  &#13;
 in contrasto con l'art. 41 della Costituzione,  mentre  avrebbe  dovuto  &#13;
 dare  sufficiente  giustificazione  dei motivi che lo inducevano a tale  &#13;
 declaratoria.                                                            &#13;
     Nel merito, l'Avvocatura rilevava la insussistenza in  concreto  di  &#13;
 una  questione di legittimità costituzionale in quanto il rilascio dei  &#13;
 "permessi" di  esportazione  previsti  dalla  legge  è  affidato  alla  &#13;
 amministrazione statale, nel quadro degli accordi commerciali stipulati  &#13;
 con   altri  Stati  e  delle  esigenze  della  economia  nazionale.  La  &#13;
 valutazione della possibilità e della convenienza del rilascio di ogni  &#13;
 singolo permesso è fatta con ampia discrezionalità, la quale, appunto  &#13;
 perché  tale,  non  intacca  diritti  soggettivi,   ma   eventualmente  &#13;
 interessi   legittimi.   Tali   atti   amministrativi   per   la   loro  &#13;
 discrezionalità  non  sono  soggetti  a  controllo  o   sindacato   di  &#13;
 legittimità.                                                            &#13;
     Infine,  l'Avvocatura  dello  Stato  sottolineava  la  mancanza  di  &#13;
 fondamento della questione sollevata,  rilevando  che  le  disposizioni  &#13;
 concernenti  le  sanzioni  comminate  per  le  infrazioni ai divieti di  &#13;
 importazione e di esportazione vanno poste  in  relazione  a  tutto  il  &#13;
 sistema  che  regola  le importazioni e le esportazioni medesime: vanno  &#13;
 cioè inquadrate nel sistema del commercio con l'estero,  il  quale  ne  &#13;
 risulterebbe  sovvertito  qualora  si  potesse  prescindere dalle norme  &#13;
 speciali in materia dettate nell'interesse dell'economia nazionale.  La  &#13;
 stessa  norma  costituzionale  conferma  che  l'iniziativa privata, pur  &#13;
 essendo libera,  non  può  svolgersi  in  contrasto  con  la  utilità  &#13;
 sociale.<diritto>Considerato in diritto</diritto>:                          &#13;
     La questione di legittimità costituzionale è, nella ordinanza del  &#13;
 Giudice  istruttore di Bolzano, contrariamente a quanto si assume nelle  &#13;
 deduzioni   dell'Avvocatura   dello   Stato,   concretamente   proposta  &#13;
 relativamente  agli  articoli  11 e 13 del R. D.L. 14 novembre 1926, n.  &#13;
 1923, e in riferimento all'art. 41 della Costituzione.                   &#13;
     Nel merito l'asserito contrasto è del tutto insussistente.          &#13;
     L'art. 11 del citato R. D.L. 14 novembre 1926 prevede come delitto,  &#13;
 punibile con la reclusione fino a tre mesi e con la multa fino  a  lire  &#13;
 5.000, il fatto di chiunque in qualsiasi modo esporta merce della quale  &#13;
 sia  vietata  l'esportazione,  o  non  la  reintroduce  nello Stato nei  &#13;
 termini stabiliti dalle norme relative se spedita in cabotaggio, oppure  &#13;
 devia verso uno Stato estero merce destinata a un  porto  italiano.  La  &#13;
 norma  si estende a chiunque trasgredisca alle disposizioni relative ai  &#13;
 divieti  di  importazione.  Le  stesse  pene  si  applicano,  in  forza  &#13;
 dell'art.  13, a chi, avendo ottenuto di importare e di esportare merce  &#13;
 in deroga ai divieti, ne faccia ad  altri  cessione  senza  fornire  al  &#13;
 cessionario anche le merci.                                              &#13;
     Si  vorrebbe  configurare  un  contrasto  tra queste disposizioni e  &#13;
 l'art. 41 della Costituzione,  in  quanto  il  primo  comma  di  questo  &#13;
 articolo  dichiara  libera  la  iniziativa economica privata, mentre le  &#13;
 disposizioni suddette importano  divieti,  penalmente  sanzionati,  che  &#13;
 costituiscono altrettante limitazioni di quella libertà.                &#13;
     Al  contrario  è  da  osservare  che,  come  questa  Corte ha già  &#13;
 rilevato nella sua sentenza n. 29 del 1957, l'art.    41  contiene  una  &#13;
 generica   dichiarazione  della  libertà  nella  iniziativa  economica  &#13;
 privata;  ma  a  tale   libertà   necessariamente   corrispondono   le  &#13;
 limitazioni   rese   indispensabili   dalle  superiori  esigenze  della  &#13;
 comunità statale. È lo stesso art. 41, nei commi secondo e terzo, che  &#13;
 sancisce le limitazioni alla  libertà  di  iniziativa  dichiarata  nel  &#13;
 primo  comma.  La  iniziativa infatti, in virtù del secondo comma, non  &#13;
 può svolgersi in contrasto con l'utilità sociale né in modo da recar  &#13;
 danno alla sicurezza, alla  libertà  e  alla  dignità  umana;  ma  è  &#13;
 soprattutto   da  considerare  il  terzo  comma,  il  quale  affida  al  &#13;
 legislatore ordinario la determinazione dei programmi e  dei  controlli  &#13;
 opportuni  affinché  l'iniziativa  privata  possa essere indirizzata e  &#13;
 coordinata ai fini sociali.                                              &#13;
     I divieti di esportazione e di importazione sono uno degli  aspetti  &#13;
 con cui si manifesta, nell'interesse sociale, il controllo della libera  &#13;
 iniziativa  privata.  Questi  divieti  vanno inquadrati nelle norme che  &#13;
 regolano il commercio con l'estero, norme che costituiscono un  sistema  &#13;
 di  delimitazioni  e  di  controllo  indispensabili  alla  difesa della  &#13;
 economia e della finanza dello Stato, e  tali  che  senza  di  essi  la  &#13;
 disciplina dei rapporti economici e finanziari ne sarebbe sovvertita.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la  questione proposta con ordinanza del 16  &#13;
 luglio 1956, dal Giudice istruttore presso  il  Tribunale  di  Bolzano,  &#13;
 sulla  legittimità costituzionale delle norme contenute negli artt. 11  &#13;
 e 13 del R. D.L. 14 novembre 1926, n. 1923, in riferimento all'art.  41  &#13;
 della Costituzione.                                                      &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 5 aprile 1957.                                &#13;
                                   GAETANO AZZARITI - GIUSEPPE  CAPPI  -  &#13;
                                   TOMASO  PERASSI - GASPARE AMBROSINI -  &#13;
                                   ERNESTO BATTAGLINI - MARIO COSATTI  -  &#13;
                                   FRANCESCO    PANTALEO    GABRIELI   -  &#13;
                                   GIUSEPPE CASTELLI AVOLIO  -  ANTONINO  &#13;
                                   PAPALDO  -  NICOLA  JAEGER - GIOVANNI  &#13;
                                   CASSANDRO  -  BIAGIO   PETROCELLI   -  &#13;
                                   ANTONIO MANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
