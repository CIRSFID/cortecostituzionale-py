<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>95</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:95</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Enzo Capolozza</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>23/04/1975</data_decisione>
    <data_deposito>29/04/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Avv. GIOVANNI BATTISTA BENEDETTI - Dott. LUIGI OGGIONI - Avv. ANGELO DE &#13;
 MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO &#13;
 MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. &#13;
 PAOLO ROSSI - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - Prof. &#13;
 EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi riuniti di legittimità costituzionale degli artt. 74,  &#13;
 ultimo comma, e 231 del codice di procedura  penale,  promossi  con  le  &#13;
 seguenti ordinanze:                                                      &#13;
     1)  ordinanza  emessa  il  19  febbraio 1973 dal pretore di Bari su  &#13;
 querela di Loseto Chiara, iscritta al n.  140  del  registro  ordinanze  &#13;
 1973 e pubblicata nella Gazzetta Ufficiale della Repubblica n.  163 del  &#13;
 27 giugno 1973;                                                          &#13;
     2)  ordinanza  emessa  il  30  aprile  1973  dal pretore di Bari su  &#13;
 denuncia a carico  di  Guerrieri  Pasquale,  iscritta  al  n.  242  del  &#13;
 registro  ordinanze  1973  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 205 dell'8 agosto 1973;                                    &#13;
     3) ordinanza emessa  il  6  novembre  1973  dal  pretore  di  Borgo  &#13;
 Valsugana  su  denuncia anonima a carico di Zotta Livio Bailo, iscritta  &#13;
 al n. 452 del registro  ordinanze  1973  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 48 del 20 febbraio 1974;                   &#13;
     4)  ordinanza  emessa  il  1  marzo 1974 dal pretore di Montorio al  &#13;
 Vomano nel procedimento penale a carico di Croce Mario, iscritta al  n.  &#13;
 167  del  registro ordinanze 1974 e pubblicata nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 146 del 5 giugno 1974.                               &#13;
     Udito nella camera di consiglio del  6  febbraio  1975  il  Giudice  &#13;
 relatore Enzo Capalozza.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1.  -  A  seguito di una querela per lesioni colpose, il pretore di  &#13;
 Bari emetteva decreto di archiviazione, che rinnovava  dopo  una  nuova  &#13;
 denunzia-querela della parte offesa per lo stesso fatto.                 &#13;
     Il  procuratore  della  Repubblica,  che  aveva  vistato  il  primo  &#13;
 decreto, chiedeva, per il secondo,  che  fosse  dato  corso  all'azione  &#13;
 penale.                                                                  &#13;
     Nel  disattendere  tale  richiesta,  il  pretore,  con ordinanza 19  &#13;
 febbraio  1973,  sollevava  questione  di  legittimità  costituzionale  &#13;
 dell'art. 74, ultimo comma, del codice di procedura penale, nella parte  &#13;
 in  cui  stabilisce  che  "il procuratore della Repubblica...   può...  &#13;
 disporre invece che si  proceda",  in  riferimento  agli  artt.    101,  &#13;
 secondo comma, 3, primo comma, e 24, secondo comma, della Costituzione.  &#13;
     Dopo  aver  richiamato la sentenza n. 102 del 1964 di questa Corte,  &#13;
 che  ebbe  a  dichiarare  non  fondata  la  questione  di  legittimità  &#13;
 costituzionale  della stessa norma per differenti profili, il giudice a  &#13;
 quo precisa di voler contestare non il diritto  del  procuratore  della  &#13;
 Repubblica  di  richiedere  il  procedimento  penale  a  seguito di una  &#13;
 diversa valutazione, ma il modo con il quale il  dissenso  verrebbe  da  &#13;
 lui  unilateralmente risolto, con effetti vincolanti per il pretore, in  &#13;
 contrasto con la indipendenza del giudice e  senza  il  contraddittorio  &#13;
 né  la  garanzia  di  difesa del soggetto in ordine al quale era stato  &#13;
 emesso il decreto di archiviazione.                                      &#13;
     2. - Analoga questione, negli stessi termini,  è  stata  sollevata  &#13;
 con  successive ordinanze dal medesimo pretore di Bari (30 aprile 1973)  &#13;
 e dal pretore di Borgo Valsugana (6 novembre 1973)  e,  in  riferimento  &#13;
 all'art.  25,  primo  comma, oltre che agli artt. 101, secondo comma, e  &#13;
 24, secondo comma, Cost., dal pretore di Montorio al  Vomano  (1  marzo  &#13;
 1974),  per  il  quale la norma denunziata verrebbe anche a distogliere  &#13;
 l'imputato dal giudice naturale.                                         &#13;
     3. - Con la medesima sua ordinanza, il pretore di Borgo Valsugana -  &#13;
 premesso che il decreto di archiviazione si riferiva a denuncia anonima  &#13;
 di una sopraelevazione abusiva, in ordine  alla  quale  il  procuratore  &#13;
 della  Repubblica  aveva  richiesto  di  procedere  all'accertamento di  &#13;
 eventuali violazioni della legislazione edilizia - ha sollevato, in via  &#13;
 subordinata, questione di legittimità costituzionale,  in  riferimento  &#13;
 agli  artt.  3,  primo comma, e 24, secondo comma, Cost., dell'art. 231  &#13;
 cod. proc.  pen., "in quanto non esclude che il pretore possa  disporre  &#13;
 indagini sul contenuto di una delazione anonima che addebita un reato a  &#13;
 persona individuata".<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Con le ordinanze in epigrafe, la Corte è chiamata a decidere:  &#13;
 a)  se  l'art. 74, ultimo comma, del codice di procedura penale - nella  &#13;
 parte in cui stabilisce che "il procuratore della Repubblica ...può...  &#13;
 disporre invece che si proceda" dopo il decreto  di  archiviazione  del  &#13;
 pretore  -  contrasti con gli artt. 101, secondo comma, 3, primo comma,  &#13;
 24,  secondo  comma,  e  25,  primo  comma,  della   Costituzione;   b)  &#13;
 subordinatamente, se l'art. 231 cod. proc. pen., "in quanto non esclude  &#13;
 che  il  pretore possa disporre indagini sul contenuto di una delazione  &#13;
 anonima che addebita un reato a persona individuata", violi  gli  artt.  &#13;
 24, secondo comma, e 3, primo comma, della Costituzione.                 &#13;
     2.  - Le quattro ordinanze hanno per oggetto la stessa questione (e  &#13;
 quella del pretore di Borgo Valsugana anche una questione  subordinata)  &#13;
 e,  pertanto,  i  relativi  giudizi possono essere riuniti e decisi con  &#13;
 unica sentenza.                                                          &#13;
     3.  -  A  prescindere  dalle  controversie dottrinarie sulla natura  &#13;
 giuridica del decreto di non promovibilità dell'azione penale, sono da  &#13;
 tener  fermi  i  criteri  accolti  dalla  Cassazione,  secondo  cui  il  &#13;
 cosiddetto provvedimento di archiviazione, a differenza della sentenza,  &#13;
 ha  per  presupposto  la  mancanza  di  un  processo  e non dà luogo a  &#13;
 preclusioni: dunque, non limita in alcun modo la  possibilità  per  il  &#13;
 pubblico  ministero  o per il pretore (quando si tratta di reati di sua  &#13;
 competenza) di procedere in qualunque momento, pur se non siano insorte  &#13;
 nuove prove e senza che occorra un preventivo atto di revoca.            &#13;
     4. - In ordine alla denunziata violazione  dell'art.  101,  secondo  &#13;
 comma,  Cost.,  è  da  richiamare  la sentenza n. 102 del 1964, per la  &#13;
 quale il provvedimento di archiviazione "non chiude né definisce alcun  &#13;
 procedimento istruttorio, che anzi dichiara di non volere  iniziare  e,  &#13;
 riferendosi soltanto all'esperimento di indagini, conserva il carattere  &#13;
 di  atto  che non preclude l'esercizio dell'azione penale". Con ciò si  &#13;
 è voluto intendere che il procuratore della Repubblica, quando dispone  &#13;
 che si proceda, esercita, egli, l'azione  dinanzi  al  pretore,  cioè,  &#13;
 eccezionalmente,  attua  il  principio  della  domanda nel procedimento  &#13;
 pretorile.                                                               &#13;
     D'altro canto, come è stato precisato dalla ridetta sentenza della  &#13;
 Corte, nella vicenda prevista dall'art. 74, ultimo  comma,  cod.  proc.  &#13;
 pen.,  "i  due  diversi  organi  operano, non in ragione di un rapporto  &#13;
 gerarchico da superiore ad inferiore, ma nell'esplicazione di  funziani  &#13;
 diverse,  necessarie per il raggiungimento dei fini che il procedimento  &#13;
 persegue".                                                               &#13;
     5. - Non può avere peso il rilievo, avanzato dal pretore di  Bari,  &#13;
 che  il  conflitto  di  opinioni  sul punto se debba o meno esercitarsi  &#13;
 l'azione penale "viene  risolto  unilateralmente  dallo  stesso  organo  &#13;
 portatore  del  dissenso  e  con l'effetto automatico di far modificare  &#13;
 all'altro organo il proprio atteggiamento":  invero,  la  giurisdizione  &#13;
 non  viene  sottratta  al  pretore, che conserva le sue attribuzioni di  &#13;
 organo giudicante, la sua libertà di valutazione e di convincimento  e  &#13;
 ben   può   emettere   sentenza   istruttoria   o   dibattimentale  di  &#13;
 proscioglimento.                                                         &#13;
     Per converso, sarebbe, oltre a tutto, irragionevole ed abnorme  che  &#13;
 il  procuratore  della  Repubblica,  cui spetta l'esercizio dell'azione  &#13;
 penale e che è abilitato  ad  impugnare  la  sentenza  (istruttoria  o  &#13;
 dibattimentale)  del  pretore,  fosse  disarmato a fronte di un decreto  &#13;
 pretorile di archiviazione (non  impugnabile  per  il  principio  della  &#13;
 tassatività  delle  impugnazioni di cui all'art. 190 cod. proc. pen.).  &#13;
 Comunque - anche per queste considerazioni - la Corte non ha motivo  di  &#13;
 modificare la sua precedente statuizione.                                &#13;
     6.  -  Non  sussiste la violazione dell'art. 3, primo comma, Cost.,  &#13;
 perché l'equidistanza del pubblico ministero e dell'imputato  rispetto  &#13;
 al  giudice  può  e  deve  essere  garantita  nel  processo, ma non è  &#13;
 configurabile tra il pubblico ministero, titolare dell'azione penale, e  &#13;
 "la persona cui il decreto è favorevole": si tratta di  due  fasi,  di  &#13;
 due  aspetti del tutto diversi che postulano un'altrettanto diversa (ed  &#13;
 opposta) dialettica procedimentale.                                      &#13;
     7. - Quanto al preteso privilegio, che, secondo il pretore di Bari,  &#13;
 sarebbe attribuito al denunciante per il fatto che può sollocitare  il  &#13;
 pubblico  ministero ad avanzare la richiesta di procedimento, è facile  &#13;
 obiettare, da un lato, che il denunciante collabora  con  la  giustizia  &#13;
 allorché  integra, in sede funzionalmente competente e qualificata, la  &#13;
 notitia criminis, dall'altro, che il pubblico ministero non  interviene  &#13;
 per  impulso  e  per interesse privati, bensì nell'esercizio di un suo  &#13;
 potere- dovere costituzionalmente sancito.                               &#13;
     8. - Non sussiste la violazione dell'art. 24, secondo comma, Cost.,  &#13;
 dappoiché, quando l'azione  penale  dà  inizio  ad  un  procedimento,  &#13;
 scattano  tutte le garanzie difensive che per il procedimento, appunto,  &#13;
 sono disposte per legge: e scattano anche prima in sede  preistruttoria  &#13;
 (sentenza  n.  86  del 1968 e legge n. 932 del 1969), epperò sempre in  &#13;
 funzione del procedimento stesso.                                        &#13;
     9. - Non sussiste, infine, la violazione dell'art. 25, primo comma,  &#13;
 Cost., giacché il meccanismo accolto nell'art. 74, ultimo comma,  cod.  &#13;
 proc.  pen.,  non  toglie  la certezza circa il giudice che in concreto  &#13;
 deve giudicare (giova ricordare che gli artt. 30 e 31 cod. proc.  pen.,  &#13;
 in   quanto   toglievano   tale   certezza,   sono   stati   dichiarati  &#13;
 costituzionalmente illegittimi con la sentenza n. 88 del 1962). E,  per  &#13;
 di  più,  nella  nozione  di giudice di cui all'art. 25 Cost. non deve  &#13;
 ritenersi compreso il pubblico ministero (sentenza n. 148 del 1963).     &#13;
     10. - La questione di  legittimità  costituzionale  dell'art.  231  &#13;
 cod.  proc.  pen.,  promossa  in  via  subordinata dal pretore di Borgo  &#13;
 Valsugana, è stata -  nelle  more  di  questo  giudizio  -  dichiarata  &#13;
 infondata,  in  riferimento  agli  stessi  precetti  costituzionali ora  &#13;
 richiamati (artt. 3 e 24) con la sentenza n. 300 del 1974.               &#13;
     Non vengono addotti argomenti diversi e la Corte non ha ragione  di  &#13;
 mutare la sua giurisprudenza.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1) dichiara non fondata la questione di legittimità costituzionale  &#13;
 dell'art.  74,  ultimo  comma,  del  codice  di  procedura  penale,  in  &#13;
 riferimento agli artt. 3, primo comma, 24,  secondo  comma,  25,  primo  &#13;
 comma,  e  101,  secondo  comma,  della Costituzione, sollevata, con le  &#13;
 quattro ordinanze in epigrafe, dai pretori di Bari, di Borgo  Valsugana  &#13;
 e di Montorio al Vomano;                                                 &#13;
     2)  dichiara  manifestamente infondata la questione di legittimità  &#13;
 costituzionale dell'art. 231 del codice di procedura penale, sollevata,  &#13;
 in subordine, con l'ordinanza del pretore di  Borgo  Valsugana  e  già  &#13;
 dichiarata non fondata con sentenza n.  300 del 1974.                    &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 23 aprile 1975.         &#13;
                                   FRANCESCO PAOLO BONIFACIO -  GIOVANNI  &#13;
                                   BATTISTA  BENEDETTI - LUIGI OGGIONI -  &#13;
                                   ANGELO DE MARCO - ERCOLE  ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NTCOLA  &#13;
                                   REALE - PAOLO ROSSI - LEONETTO AMADEI  &#13;
                                   - GIULIO GIONFRIDA - EDOARDO VOLTERRA  &#13;
                                   - GUIDO ASTUTI - MICHELE ROSSANO.      &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
