<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>129</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:129</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Paolo Rossi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/06/1970</data_decisione>
    <data_deposito>13/07/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI &#13;
 - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE &#13;
 MARCO Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  dell'art.    663  del  &#13;
 codice  penale;  degli  artt. 2 e 4 della legge 23 gennaio  1941, n.166  &#13;
 (norme integrative  della  disciplina  delle  pubbliche    affissioni);  &#13;
 dell'art.  9 del D.L.C.P.S. 8 novembre 1947, n. 1417  (disciplina delle  &#13;
 pubbliche affissioni e della pubblicità affine); dell'articolo 113 del  &#13;
 testo unico delle leggi di P.S.  approvato con R.D. 18 giugno 1931,  n.  &#13;
 773;  e  dell'art.  398  del codice di   procedura penale, promosso con  &#13;
 ordinanza emessa il 3   dicembre 1968 dal pretore  di  Ronciglione  nel  &#13;
 procedimento  penale   a carico di Mattei Arcangelo, iscritta al n.  23  &#13;
 del registro   ordinanze 1969 e  pubblicata  nella  Gazzetta  Ufficiale  &#13;
 della  Repubblica n. 66 del 12 marzo 1969.                               &#13;
     Visto  l'atto  d'intervento  del  Presidente    del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito nell'udienza pubblica del 3  giugno 1970 il Giudice  relatore  &#13;
 Paolo Rossi;                                                             &#13;
     udito il sostituto avvocato generale  dello Stato Franco Chiarotti,  &#13;
 per il Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  di  un  procedimento  penale    a carico di tale Mattei  &#13;
 Arcangelo, imputato della contravvenzione di  cui    all'art.  663  del  &#13;
 codice  penale  in  relazione  agli artt. 2 e 4 della  legge 23 gennaio  &#13;
 1941, n. 166, 9 del D.L.C.P.S. 8 novembre 1947, n.  1417,  113,  quinto  &#13;
 comma,  del  testo unico delle   leggi di P.S. 18 giugno 1931, n.  773,  &#13;
 per aver affisso un   manifesto manoscritto fuori degli  spazi  a  ciò  &#13;
 destinati, il  pretore ha sollevato d'ufficio questione di legittimità  &#13;
 costituzionale di tutte le norme ora citate, in riferimento all'art. 21  &#13;
 della  Costituzione, ed altresì dell'art. 398 del codice di  procedura  &#13;
 penale, in riferimento agli artt. 3 e 24 della Costituzione.             &#13;
     In  particolare,  nell'ordinanza  di    rimessione  si  rileva  che  &#13;
 l'attuale formulazione dell'art. 398 del codice di   procedura  penale,  &#13;
 consentendo  al  pretore  di citare a giudizio  l'imputato senza averlo  &#13;
 previamente interrogato,  pur    nell'ipotesi  in  cui  l'autorità  di  &#13;
 polizia  giudiziaria  abbia  elevato  un  verbale  di  contravvenzione,  &#13;
 contrasterebbe  con  gli  artt.  3  e    24  della  Costituzione,   sul  &#13;
 presupposto  che  l'atto  di  polizia    dia sempre origine ad una fase  &#13;
 processuale  nell'ambito     della  quale  potrebbe   essere   precluso  &#13;
 all'imputato l'esercizio  del diritto di difesa.                         &#13;
     In secondo luogo, il giudice a quo  osserva che la discrezionalità  &#13;
 attribuita  dalle  impugnate norme alla  pubblica Amministrazione nella  &#13;
 scelta degli spazi da destinare  alle affissioni, essendo  praticamente  &#13;
 illimitata  ed  insindacabile,  consentirebbe di impedire il diritto di  &#13;
 esprimere liberamente il   proprio pensiero, garantito  dall'art.    21  &#13;
 della  Costituzione,  specialmente se l'autorità non abbia adottato in  &#13;
 proposito  determinazione alcuna.                                        &#13;
     Si è costituita in giudizio  la  Presidenza    del  Consiglio  dei  &#13;
 ministri,  rappresentata e difesa dall'Avvocatura  generale dello Stato  &#13;
 con  atto  depositato  l'11  febbraio  1969,    chiedendo   dichiararsi  &#13;
 l'infondatezza di entrambe le questioni sollevate                        &#13;
     Alla  prima  eccezione  la difesa dello   Stato ha obiettato che la  &#13;
 questione è priva di consistenza, giacché o gli  atti compiuti  dalla  &#13;
 polizia  costituiscono  sostanzialmente  atti   istruttori direttamente  &#13;
 utilizzabili nel processo, e allora il pretore   deve  necessariamente,  &#13;
 in  base  alla normativa vigente in  proposito a seguito delle sentenze  &#13;
 della Corte costituzionale n.  33 del 1966 e n. 86 del 1968,  procedere  &#13;
 all'interrogatorio dell'imputato e alla contestazione del fatto; oppure  &#13;
 gli atti compiuti  non sono del genere ora menzionato, ed allora non si  &#13;
 verte   in una fase autonoma del procedimento, nella quale debba essere  &#13;
 assicurato il diritto di  difesa,  con  la  conseguenza  che  il    suo  &#13;
 esercizio è legittimamente rimesso alla fase dibattimentale.            &#13;
     In  ordine  alla  seconda  questione   l'Avvocatura premette che la  &#13;
 legge 5 luglio 1961, n. 641, e il decreto 8   novembre 1947,  n.  1417,  &#13;
 impongono  all'autorità  comunale  di    determinare,  d'intesa  con i  &#13;
 proprietari e sentita la commissione  edilizia, gli spazi da  destinare  &#13;
 ad  affissioni;  qualora  tale  procedura  non  sia  stata applicata è  &#13;
 previsto l'intervento sostitutivo    del  Prefetto:  ciò  al  fine  di  &#13;
 contemperare varie esigenze  pubbliche, a volte contrastanti, quali, da  &#13;
 un  lato  quella della   tutela della estetica e del decoro cittadini e  &#13;
 della sicurezza del  traffico, e d'altro canto,  quella  di  assicurare  &#13;
 spazi sufficienti  ed adeguati per consentire l'affissione di manifesti  &#13;
 ed      avvisi.      Pertanto   il   potere   spettante   in  proposito  &#13;
 all'amministrazione  comunale, attribuito per esigenze pubbliche  degne  &#13;
 di  particolare tutela, non può realizzare un mezzo di limitazione del  &#13;
 diritto di manifestare liberamente il proprio pensiero.                  &#13;
     La   difesa   dello   Stato,   ricorda,  infine,     che  la  Corte  &#13;
 costituzionale ebbe a  riconoscere,  nella  prima  sua    sentenza,  la  &#13;
 legittimità  del  quinto  comma dell'art. 113 del T.U. delle  leggi di  &#13;
 p.s. a suo tempo denunciato per gli stessi motivi ora riproposti.<diritto>Considerato in diritto</diritto>:                          &#13;
     La Corte  costituzionale  è  chiamata  a    decidere  le  seguenti  &#13;
 questioni:                                                               &#13;
     1) se non contrasti con gli artt.3 e 24  della Costituzione, l'art.  &#13;
 398  del  codice  di procedura penale, secondo  cui il pretore, qualora  &#13;
 non siano stati compiuti atti istruttori,  può citare  direttamente  a  &#13;
 giudizio  l'imputato  senza    averlo previamente interrogato sul fatto  &#13;
 addebitatogli;                                                           &#13;
     2) se gli artt. 2 e 4 della legge 23    gennaio  1941,  n.  166,  9  &#13;
 D.L.C.P.S.  8 novembre 1947, n. 1417, 113 T.U.  leggi di P.S. 18 giugno  &#13;
 1931, n. 773, e 663 del codice  penale - nella parte in cui contemplano  &#13;
 come contravvenzione   l'affissione di  stampati  o  manoscritti  fuori  &#13;
 degli  spazi  a  ciò destinati dall'autorità - non contrastino con il  &#13;
 diritto di   manifestare liberamente il  proprio  pensiero  (art.    21  &#13;
 Cost.),  sul    presupposto che il potere dell'autorità in ordine alla  &#13;
 determinazione  o  meno  degli   spazi   stessi   sia   illimitato   ed  &#13;
 insindacabile.                                                           &#13;
     1.  -  La prima questione è già stata  decisa negativamente dalla  &#13;
 Corte (sentenze n. 16 del 1970, n.  4  del    1970,  n.  46  del  1967,  &#13;
 ordinanza  n.  4  del 1968 e n. 102 del  1970), né sono dedotti motivi  &#13;
 che inducano a diverso avviso.                                           &#13;
     2. - Nemmeno la seconda questione può trovare accoglimento.         &#13;
     L'assunto del giudice a quo in  ordine  alla  insindacabilità  del  &#13;
 potere dell'autorità comunale di determinare  gli spazi destinati alle  &#13;
 affissioni  è infondato. L'impugnato  art. 9 del D.L.C.P.S. 8 novembre  &#13;
 1947,  n.  1417,  stabilisce    l'obbligo  dell'autorità   locale   di  &#13;
 provvedere,  sentita  la  commissione  edilizia  e  previo consenso dei  &#13;
 proprietari interessati, alla  oculata scelta degli spazi da  destinare  &#13;
 ad   affissione;   prevede   che   qualora   non   sia  possibile  tale  &#13;
 determinazione per mancato    accordo  dei  soggetti  interessati  deve  &#13;
 disporre  in  via  sostitutiva    il  prefetto, con decreto definitivo,  &#13;
 sentiti l'ufficio del genio  civile e la sovraintendenza ai monumenti.   &#13;
     Risulta,  quindi,  evidente  che  le  norme  impugnate   non   solo  &#13;
 consentono  alle  autorità  locali  di disciplinare   concretamente il  &#13;
 servizio delle affissioni pubbliche, ma impongono    la  determinazione  &#13;
 degli  spazi  da  destinare  alle esigenze   della generalità, ed ogni  &#13;
 cittadino  può  sempre,  in  caso  di     ritardi  od   irregolarità,  &#13;
 sollecitare  il prefetto ad intervenire autoritativamente. È appena il  &#13;
 caso di ricordare, infine, che   esistono comunque apposite  norme  del  &#13;
 codice  penale  che   garantiscono l'osservanza dei doveri d'ufficio da  &#13;
 parte dei  pubblici ufficiali (artt.  328 e 323 c.p.).                   &#13;
     Dimostrata  pertanto  l'infondatezza     dell'assunto   presupposto  &#13;
 nell'ordinanza  di rimessione, occorre esaminare se il potere-dovere di  &#13;
 determinazione degli spazi di cui trattasi,  conferito dalle  impugnate  &#13;
 norme,  non  costituisca  una    illegittima  limitazione al diritto di  &#13;
 manifestare il proprio pensiero,  garantito dall'art. 21, primo  comma,  &#13;
 della Costituzione.                                                      &#13;
     Le   ragioni  che  hanno  indotto  il  legislatore    a  consentire  &#13;
 l'affissione di manoscritti e stampati - e non  solo  ai    privati  ma  &#13;
 anche  alle  amministrazioni  statali  e  pubbliche solo negli appositi  &#13;
 spazi (art. 2 legge 23 gennaio 1941, n.    166)  risultano  chiaramente  &#13;
 dalla normativa vigente in proposito.  Emerge in particolare l'esigenza  &#13;
 di valutare altri interessi  pubblici degni di primaria considerazione,  &#13;
 come  la  sicurezza  della    viabilità,  e  la  tutela dei monumenti,  &#13;
 dell'estetica cittadina,  del paesaggio.  È noto infatti, che  secondo  &#13;
 la  costante  giurisprudenza  di questa Corte "il concetto di limite è  &#13;
 insito nel  concetto di diritto e nell'ambito dell'ordinamento le varie  &#13;
 sfere    giuridiche  devono  di  necessità  limitarsi  reciprocamente,  &#13;
 perché  possano  coesistere nell'ordinata convivenza civile" (sentenza  &#13;
 n. 1 del 1956).                                                          &#13;
     In un caso analogo, esaminando il  divieto, penalmente  sanzionato,  &#13;
 di affissione dei manifesti di  propaganda elettorale fuori degli spazi  &#13;
 stabiliti,   denunciato  sempre  in    riferimento  all'art.  21  della  &#13;
 Costituzione, questa Corte ebbe ad   enunciare  taluni  principi    che  &#13;
 possono  essere utilmente richiamati nella fattispecie in esame. Invero  &#13;
 venne allora affermato  che norme siffatte, in  quanto  si  limitano  a  &#13;
 disciplinare    l'esercizio di un diritto, "appaiono estrinsecazione di  &#13;
 un potere del legislatore ordinario del quale la Corte, in  riferimento  &#13;
 a   varie    fattispecie  e  con  ripetute  e  costanti  pronunzie,  ha  &#13;
 riconosciuto  la piena legittimità, sempre che il  diritto  attribuito  &#13;
 dalla Costituzione non venga ad essere snaturato". E va ricordato  che,  &#13;
 proprio  in applicazione di questo principio allora per la  prima volta  &#13;
 affermato, la Corte nella citata sentenza del 1956  (n. 1) ritenne  che  &#13;
 non fosse in contrasto con l'art. 21 della Costituzione il quinto comma  &#13;
 dell'art. 113 del T.U.  delle leggi di  p.s., nel quale è disposto che  &#13;
 "le   affissioni   non  possono    farsi  fuori  dei  luoghi  destinati  &#13;
 dall'autorità competente"    (sentenza  n.  48  del  1964;  cfr.  pure  &#13;
 sentenza n. 49 del 1965 e ordinanza n. 97 del 1965).</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara la manifesta infondatezza  della questione di legittimità  &#13;
 costituzionale   dell'art.   398  del  codice  di    procedura  penale,  &#13;
 sollevata, in riferimento agli artt. 3 e 24 della    Costituzione,  dal  &#13;
 pretore di Ronciglione con ordinanza del  3 dicembre 1968;               &#13;
     dichiara  non  fondata la questione di  legittimità costituzionale  &#13;
 degli artt. 2 e 4  della  legge  23  gennaio  1941,    n.  166,  9  del  &#13;
 D.L.C.P.S.  8  novembre 1947, n. 1417, 113  del testo unico delle leggi  &#13;
 di P.S. 18 giugno 1931, n. 773, e  663 del codice penale, sollevata, in  &#13;
 riferimento all'art. 21 della Costituzione, con la  medesima  ordinanza  &#13;
 sopraindicata.                                                           &#13;
     Così  deciso  in  Roma,  nella  sede della   Corte costituzionale,  &#13;
 Palazzo della Consulta, il 24 giugno 1970.                               &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE   VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO  CAPALOZZA  -  VEZIO  &#13;
                                   CRISAFULLI  -  NICOLA  REALE  - PAOLO  &#13;
                                   ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
