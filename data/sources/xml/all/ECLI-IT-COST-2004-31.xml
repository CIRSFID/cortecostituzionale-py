<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2004</anno_pronuncia>
    <numero_pronuncia>31</numero_pronuncia>
    <ecli>ECLI:IT:COST:2004:31</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>13/01/2004</data_decisione>
    <data_deposito>23/01/2004</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 5, commi 3, 5, 6, 7, 8, e 10, dell'art. 8, dell'art. 22, comma 2, e dell'art. 24 della legge della Regione Liguria 10 luglio 2002, n. 29 recante (Misure di sostegno per gli interventi di recupero e di riqualificazione dei centri storici e norme per lo snellimento delle procedure di rilascio dei titoli edilizi), promosso con ricorso del Presidente del Consiglio dei ministri, notificato il 20 settembre 2002, depositato in cancelleria il 27 successivo ed iscritto al n. 61 del registro ricorsi 2002. &#13;
    Visto l'atto di costituzione della Regione Liguria; &#13;
    udito nell'udienza pubblica del 25 marzo 2003 il Giudice relatore Ugo De Siervo; &#13;
    uditi l'Avvocato dello Stato Franco Favara per il Presidente del Consiglio dei ministri e l'avvocato Gigliola Benghi per la Regione Liguria. &#13;
    Ritenuto che, con ricorso notificato il 20 settembre 2002 il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha impugnato gli artt. 5, commi 3, 5, 6, 7, 8, e 10; 8; 22, comma 2; e 24 della legge della Regione Liguria 10 luglio 2002, n. 29 (Misure di sostegno per gli interventi di recupero e di riqualificazione dei centri storici e norme per lo snellimento delle procedure di rilascio dei titoli edilizi), pubblicata nel Bollettino Ufficiale n. 11 del 24 luglio 2002; &#13;
    che il ricorrente ritiene, in particolare, il comma 3, primo periodo, dell'art. 5 della legge impugnata, contrastante con l'art. 117, comma secondo, lettera l), della Costituzione, in quanto volto ad integrare la normativa penale statale;  &#13;
    che inoltre i commi 5, periodo secondo, 6, 7, 8, dell'articolo 5 della legge regionale sarebbero lesivi della competenza statale in tema di "tutela dell'ambiente, dell'ecosistema e dei beni culturali" di cui all'articolo 117, secondo comma, lettera s), della Costituzione., non rilevando in senso inverso che talune delle disposizioni impugnate non si allontanino significativamente dalla normativa statale vigente in materia; &#13;
    che, secondo il ricorso, l'art. 5, comma 10, della legge impugnata eccederebbe la competenza regionale contrastando con l'art. 117, terzo comma, della Costituzione, il quale affida alla competenza concorrente di Stato e Regioni la materia "governo del territorio", e ciò in quanto violerebbe un principio fondamentale stabilito dalla legislazione statale, prevedendo termini difformi da quelli contemplati da quest'ultima in relazione alla denuncia di inizio attività; &#13;
    che, in particolare, la difformità sarebbe percepibile in relazione a quanto previsto dalla legge 21 dicembre 2001, n. 443 (Delega al Governo in materia di infrastrutture ed insediamenti produttivi strategici ed altri interventi per il rilancio  delle attività produttive); &#13;
    che anche l'art. 8 della legge della Regione Liguria oggetto del presente giudizio sarebbe costituzionalmente illegittimo, in quanto contrastante con un "principio fondamentale dell'ordinamento", e ciò in quanto consentirebbe "all'ente locale Comune di non osservare norme legislative e regolamentari in tema di salvaguardia igienico-sanitaria", non indicando peraltro neanche le norme suscettibili di essere disapplicate mediante atti amministrativi; &#13;
    che l'Avvocatura dello Stato impugna anche l'art. 22, comma 2, della legge regionale n. 29 del 2002, il quale violerebbe l'art. 120 della Costituzione, attribuendo al difensore civico regionale l'esercizio di poteri sostitutivi, attribuzione quest' ultima che sarebbe preclusa alle Regioni in mancanza di una previa normativa statale emanata in attuazione della citata disposizione costituzionale;  &#13;
    che, infine, l'art. 24 della legge impugnata risulterebbe, secondo il ricorso introduttivo del giudizio, eccedente la competenza regionale, laddove prevede la "sostituzione di disposizioni statali", in quanto ciò non sarebbe consentito alle leggi regionali regionali, neanche limitatamente al solo territorio della Regione che adotti la legge in questione;  &#13;
    che è intervenuta la Regione Liguria secondo la quale le censure sarebbero infondate sia in relazione al nuovo Titolo V della Costituzione che "con riferimento alle recenti disposizioni legislative statali introdotte dall'art. 13, commi 7 e 8, della legge 1 agosto 2002, n. 166";  &#13;
    che, in particolare, secondo la resistente, andrebbero respinte le censure concernenti l'art. 5, comma 5, della legge impugnata, in quanto tale norma si limiterebbe a "riprodurre una disposizione di esonero dal rilascio della autorizzazione paesistico-ambientale già contenuta nell'art. 82, comma 9, del d.P.R. n. 616 del 1977, come introdotto dall'art. 1 della legge n. 431/1985 e successivamente novellato dall'art. 152, comma 1, lettera a) del d.lgs. n. 480/1999"; &#13;
    che, inoltre, la infondatezza delle censure concernenti il citato comma 5 deriverebbe dalla circostanza secondo la quale l'esonero in questione risulterebbe applicabile soltanto "ad interventi di recupero del patrimonio edilizio che non alterino lo stato dei luoghi e l'aspetto esteriore degli edifici"; &#13;
    che, quanto ai commi 6, 7 e 8 del medesimo art. 5, la Regione resistente afferma che con essi sarebbero stati recepiti i principi fondamentali stabiliti dall'art. 1, commi 8, 9 e 10 della legge n. 443 del 2001, "relativi all'ammissibilità della DIA in zone assoggettate a vincoli di varia natura", limitandosi a colmare lacune della normativa statale; &#13;
    che le censure concernenti il comma 10 dell'art. 5 sarebbero infondate in quanto tale disposizione non consentirebbe in nessun caso il venir meno del potere-dovere dei Comuni di esercitare la vigilanza sull'attività urbanistico-edilizia; &#13;
    che, secondo la difesa regionale, il disposto dell'art. 8 della legge regionale n. 29 del 2002 si porrebbe in linea con principi desumibili da molte disposizioni statali; &#13;
    che anche la censura riguardante l'art. 22, comma 2, sarebbe infondata, in quanto tale norma regolerebbe situazioni non contemplate dall'art. 120 della Costituzione, nell'ambito del quale il potere sostitutivo del Governo si eserciterebbe "al verificarsi di presupposti inesistenti nel caso di specie"; &#13;
    che, infine, in relazione alla censura concernente la pretesa delle leggi regionali di sostituire disposizioni legislative statali, la Regione osserva come tale meccanismo fosse pacificamente ammesso, prima della recente riforma del Titolo V della Costituzione, nell'ambito delle materie oggetto di competenza legislativa concorrente, in particolare per la sostituzione di disposizioni statali con normativa regionale di dettaglio.  &#13;
    Considerato che in data 5 marzo 2003 è stata approvata la legge della Regione Liguria 12 marzo del 2003, n. 7, recante "Modifiche alla legge regionale 10 luglio 2002, n. 29 (Misure di sostegno per gli interventi di recupero e riqualificazione dei centri storici e norme per lo snellimento delle procedure di rilascio dei titoli edilizi)"; &#13;
    che in data 18 marzo 2003 la difesa erariale ha depositato una nota informativa con quale "si esibisce il testo della legge reg. Liguria 12 marzo 2003, n. 7" la quale, secondo la medesima nota, "circoscrive ulteriormente la materia controversa"; &#13;
    che l'Avvocatura dello Stato ha depositato, in data 1° aprile 2003, atto di rinuncia al ricorso conformemente alla deliberazione del Consiglio dei ministri del 28 marzo 2003 adottata su proposta del Ministro per gli affari regionali ; &#13;
    che in tale proposta si ritengono venute meno le ragioni del ricorso in quanto con la legge regionale n. 7 del 2003 "la Regione abroga la norma in materia di intervento sostitutivo regionale e chiarisce con interpretazione autentica, la portata della disposizione in materia di requisiti igienico - sanitari degli edifici"; &#13;
    che, inoltre, nella medesima proposta si nota che per quanto riguarda la disposizione che opera una sostituzione delle norme statali in materia di D.I.A., "con nota del Presidente della Giunta regionale n. 47041/663 del 24.3.03, richiamando una consolidata prassi di tecnica legislativa adottata dalla Regione, viene data un adeguata interpretazione della norma che sembra poter far superare le eccezioni sollevate dal Governo"; &#13;
    che la Regione Liguria ha depositato in data 24 novembre 2003 atto di accettazione della rinuncia; &#13;
    che, ai sensi dell'art. 25, ultimo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale, la rinuncia al ricorso, seguita dalla relativa accettazione della controparte, produce l'effetto di estinguere il processo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
    LA CORTE COSTITUZIONALE &#13;
    dichiara estinto per rinuncia accettata dalla controparte il processo relativo al ricorso promosso dal Presidente del Consiglio dei ministri nei confronti della legge della Regione Liguria 10 luglio 2002, n. 29 (Misure di sostegno per gli interventi di recupero e di riqualificazione dei centri storici e norme per lo snellimento delle procedure di rilascio dei titoli edilizi) sollevata con il ricorso del Presidente del Consiglio dei ministri, indicato in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 13 gennaio 2004. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Ugo DE SIERVO, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 23 gennaio 2004. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
