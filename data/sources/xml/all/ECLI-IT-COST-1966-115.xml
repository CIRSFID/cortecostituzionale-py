<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1966</anno_pronuncia>
    <numero_pronuncia>115</numero_pronuncia>
    <ecli>ECLI:IT:COST:1966:115</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Antonio Manca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/11/1966</data_decisione>
    <data_deposito>19/11/1966</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO - Dott. &#13;
 LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha deliberato in camera di consiglio la seguente<titolo>ORDINANZA</titolo>nei  giudizi  riuniti  di legittimità costituzionale dell'art. 398  &#13;
 del Codice di procedura penale promossi con le seguenti ordinanze:       &#13;
     1) ordinanze emesse il  24  gennaio  ed  il  1  febbraio  1966  dal  &#13;
 Tribunale  di  Ferrara  nei  procedimenti  penali  a  carico di Mazzoni  &#13;
 Giuseppe, Bonazza Natale, Alberghini Leonello e Grandi Rino,  e  il  12  &#13;
 febbraio 1966 dal Pretore di Novara nel procedimento penale a carico di  &#13;
 Giubertoni  Raoul,  iscritte  ai  nn.  41, 42, 43, 44 e 55 del Registro  &#13;
 ordinanze 1966 e pubblicate nella Gazzetta Ufficiale  della  Repubblica  &#13;
 n. 105 del 30 aprile 1966;                                               &#13;
     2)  ordinanze  emesse  il 16 febbraio 1966 dal Pretore di Benevento  &#13;
 nel procedimento penale a carico di Zollo Antonio e il  1  aprile  1965  &#13;
 dal  Pretore  di  Montichiari  nel procedimento penale a carico di Peli  &#13;
 Renato ed altro, iscritto ai nn. 64 e 66 del Registro ordinanze 1966  e  &#13;
 pubblicate  nella  Gazzetta  Ufficiale  della  Repubblica n. 118 del 14  &#13;
 maggio 1966;                                                             &#13;
     3) ordinanze emesse il 12 febbraio 1966 dal Pretore  di  Cagli  nel  &#13;
 procedimento  penale a carico di Fiorani Luigi ed altri, il 14 febbraio  &#13;
 1966 dal  Pretore  di  Rovigo  nel  procedimento  penale  a  carico  di  &#13;
 Martinelli  Giancarlo  ed  altri  e  il 24 febbraio 1966 dal Pretore di  &#13;
 Livorno nel  procedimento  penale  a  carico  di  Carboncini  Vincenzo,  &#13;
 iscritte  ai  nn.  78, 80 e 83 del Registro ordinanze 1966 e pubblicate  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 124 del 21 maggio 1966;     &#13;
     4) ordinanza emessa il 24 gennaio 1966 dal Tribunale di Ferrara nel  &#13;
 procedimento penale a carico di Costariol Giuseppe, iscritta al  n.  89  &#13;
 del Registro ordinanze 1966 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 143 dell'11 giugno 1966.                                   &#13;
     Udita  nella  camera  di consiglio del 19 ottobre 1966 la relazione  &#13;
 del Giudice Antonio Manca;                                               &#13;
     Ritenuto in fatto che, con le ordinanze indicate nell'epigrafe,  è  &#13;
 stata  sollevata  la questione di legittimità costituzionale dell'art.  &#13;
 398 del Codice di procedura penale, nella parte in cui consente che nei  &#13;
 procedimenti pretorili l'imputato venga tratto a giudizio, senza che lo  &#13;
 stesso sia stato interrogato in istruttoria, ovvero senza che il  fatto  &#13;
 sia  stato  enunciato  in  un  mandato di cattura, di comparizione o di  &#13;
 accompagnamento rimasto senza effetto;                                   &#13;
     che  le  predette  ordinanze  sono  state  ritualmente  notificate,  &#13;
 comunicate e pubblicate nella Gazzetta Ufficiale;                        &#13;
     che, in questa sede, non vi è stata costituzione di parte;          &#13;
     Considerato  che,  con  sentenza  n.  33 del 20 aprile 1966, questa  &#13;
 Corte ha  dichiarato  l'illegittimità  dell'art.  398  del  Codice  di  &#13;
 procedura penale nella parte in cui, nei procedimenti di competenza del  &#13;
 Pretore,  non  prevede  la  contestazione  del fatto e l'interrogatorio  &#13;
 dell'imputato, qualora si proceda al compimento di atti di istruzione;   &#13;
     che, pertanto tale disposizione ha cessato di avere efficacia e non  &#13;
 può avere applicazione dal giorno successivo alla pubblicazione  della  &#13;
 detta  sentenza (artt. 136, primo comma, della Costituzione e 30, terzo  &#13;
 comma, della legge 11 marzo 1953, n. 87);                                &#13;
     Visti gli artt. 26, secondo comma, e 29, della citata legge  n.  87  &#13;
 del  1953  e  l'art.  9,  secondo  comma, delle Norme integrative per i  &#13;
 giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     riunite le cause indicate nell'epigrafe,                             &#13;
     dichiara la manifesta infondatezza della questione di  legittimità  &#13;
 costituzionale dell'art. 398 del Codice di procedura penale nella parte  &#13;
 in  cui,  nei  procedimenti  di  competenza del Pretore, non prevede la  &#13;
 contestazione del fatto e l'interrogatorio  dell'imputato,  qualora  si  &#13;
 proceda al compimento di atti di istruzione.                             &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'8 novembre 1966.         &#13;
                                   GASPARE AMBROSINI - ANTONINO  PAPALDO  &#13;
                                   -  NICOLA JAEGER - GIOVANNI CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   ALDO SANDULLI  -  GIUSEPPE  BRANCA  -  &#13;
                                   MICHELE  FRAGALI - COSTANTINO MORTATI  &#13;
                                   -  GIUSEPPE  CHIARELLI   -   GIUSEPPE  &#13;
                                    VERZÌ  - GIOVANNI BATTISTA BENEDETTI  &#13;
                                   - FRANCESCO PAOLO BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
