<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2010</anno_pronuncia>
    <numero_pronuncia>148</numero_pronuncia>
    <ecli>ECLI:IT:COST:2010:148</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>DE SIERVO</presidente>
    <relatore_pronuncia>Sabino Cassese</relatore_pronuncia>
    <redattore_pronuncia>Sabino Cassese</redattore_pronuncia>
    <data_decisione>14/04/2010</data_decisione>
    <data_deposito>23/04/2010</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Ugo DE SIERVO; Giudici : Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 2, 9, commi 1 e 4, lettere a) e b), 13, comma 3, lettera b), e 15, comma 3, della legge della Regione Calabria 17 agosto 2009, n. 25 (Norme per lo svolgimento di «elezioni primarie» per la selezione di candidati all'elezione di Presidente della Giunta regionale), promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 22-27 ottobre 2009, depositato in cancelleria il 27 ottobre 2009 ed iscritto al n. 99 del registro ricorsi 2009.&#13;
 Udito nella camera di consiglio del 24 marzo 2010 il Giudice relatore Sabino Cassese.</epigrafe>
    <testo>Ritenuto che il Presidente del Consiglio dei ministri, con ricorso in via principale ritualmente notificato e depositato in data 27 ottobre 2009 (reg. ric. n. 99 del 2009), ha proposto questione di legittimità costituzionale degli articoli 2, 9, commi 1 e 4, lettere a) e b), 13, comma 3, lettera b), 15, comma 3, della legge della Regione Calabria 17 agosto 2009, n. 25 (Norme per lo svolgimento di «elezioni primarie» per la selezione di candidati all'elezione di Presidente della Giunta regionale), per contrasto con gli articoli 48, 49, 51, primo comma, 117, secondo comma, lettera l), e 122, primo comma, della Costituzione; &#13;
 che la legge della Regione Calabria n. 25 del 2009, con l'intento di introdurre meccanismi di democrazia partecipativa, ha disciplinato le elezioni primarie quali modalità di partecipazione degli elettori alla selezione delle candidature presentate dai partiti e dai gruppi politici organizzati per le elezioni regionali;&#13;
 che, in particolare, l'art. 2 della suddetta legge regionale ha previsto che «I partiti ed i gruppi politici che intendono presentare liste elettorali per l'elezione del Consiglio regionale (...), partecipano alle "elezioni primarie" e, a pena della esclusione dal rimborso di cui all'articolo 15, alle elezioni regionali candidano alla carica di Presidente della Giunta regionale il candidato della rispettiva lista che ha ottenuto il maggior numero di voti nella "elezione primaria"»; che gli artt. 13, comma 3, lettera b), e 15, comma 3, hanno previsto la mancata restituzione della cauzione e del rimborso spese per i soggetti che non abbiano candidato alla carica di Presidente della Giunta regionale il candidato che abbia ottenuto il maggior numero di voti nelle elezioni primarie; che l'art. 9 della medesima legge regionale, dopo aver disposto, al comma 1, che «ciascun elettore esprime il proprio voto scegliendo la scheda della lista, o della coalizione di liste, per la quale intende votare», ha previsto, al comma 4, che «Il Presidente ovvero il Vicepresidente della sezione: a) consegna la scheda della lista richiesta dall'elettore; ciascun elettore può esprimere il voto per una sola lista di candidati alla carica di Presidente della Giunta regionale; b) deposita ciascuna scheda restituita dall'elettore dopo l'espressione del voto nell'urna riservata alle schede della lista per la quale l'elettore ha espresso il voto»;&#13;
 che il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha sostenuto, in primo luogo, che la legge regionale violerebbe gli artt. 49, 51, primo comma, e 122, primo comma, Cost., non essendo la Regione competente a disciplinare il sistema di selezione dei candidati dei partiti politici alle elezioni, vincolando, l'art. 2, la formazione delle liste elettorali ai risultati delle elezioni primarie, e irrogando, i successivi artt. 13, comma 3, lettera b), e 15, comma 3, sanzioni pecuniarie derivanti dal mancato rispetto dell'obbligo di candidare alla carica di presidente della giunta regionale il vincitore delle elezioni primarie; in secondo luogo, che l'art. 9, commi 1 e 4, della legge regionale, consentendo di desumere la manifestazione di voto e ledendo il diritto alla riservatezza dell'elettore, contrasterebbe con gli artt. 48, secondo comma, e 117, secondo comma, lettera l), Cost.;&#13;
 che la Regione Calabria, non costituitasi in giudizio, ha provveduto a modificare le disposizioni impugnate con due successive leggi regionali (la legge della Regione Calabria 29 ottobre 2009, n. 38, e la legge della Regione Calabria 3 dicembre 2009, n. 44);&#13;
 che, con atto depositato presso la cancelleria di questa Corte l'11 febbraio 2010, l'Avvocatura generale dello Stato, per conto del Presidente del Consiglio dei ministri, in virtù del diritto sopravvenuto, ha dichiarato di rinunciare al ricorso n. 99 del 2009;&#13;
 che l'atto di rinuncia è stato ritualmente notificato alla Regione Calabria in data 12 febbraio 2010.&#13;
 Considerato che, in mancanza di costituzione in giudizio della parte convenuta, la rinuncia al ricorso determina, ai sensi dell'art. 23 delle norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.</testo>
    <dispositivo>per questi motivi&#13;
 La Corte costituzionale&#13;
 dichiara estinto il processo.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 14 aprile 2010.&#13;
 F.to:&#13;
 Ugo DE SIERVO, Presidente&#13;
 Sabino CASSESE, Redattore&#13;
 Giuseppe DI PAOLA, Cancelliere&#13;
 Depositata in Cancelleria il 23 aprile 2010.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
