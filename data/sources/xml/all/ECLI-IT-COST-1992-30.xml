<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>30</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:30</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1992</data_decisione>
    <data_deposito>03/02/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel   giudizio   promosso  con  ricorso  della  Provincia  di  Trento    &#13;
 notificato il 22 giugno 1991, depositato in cancelleria il  6  luglio    &#13;
 successivo, per conflitto di attribuzione sorto a seguito del decreto    &#13;
 del  Presidente  del  Consiglio  dei  Ministri  in data 1' marzo 1991    &#13;
 (Ripartizione tra i bacini di  rilievo  nazionale,  interregionale  e    &#13;
 regionale  dei  fondi  disponibili  nel  periodo 1989-93 da destinare    &#13;
 all'attuazione degli  schemi  previsionali  e  programmatici  per  il    &#13;
 riassetto  organizzativo  e  funzionale  della  difesa del suolo), ed    &#13;
 iscritto al n. 34 del registro conflitti 1991;                           &#13;
    Udito nell'udienza  pubblica  del  19  novembre  1991  il  giudice    &#13;
 relatore prof. Antonio Baldassarre;                                      &#13;
    Udito l'Avvocato Valerio Onida per la Provincia di Trento;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ricorso regolarmente notificato e depositato la Provincia    &#13;
 autonoma  di  Trento  ha  sollevato  conflitto  di  attribuzione  nei    &#13;
 confronti dello Stato, in  relazione  all'art.  3,  secondo  e  terzo    &#13;
 comma, del decreto del Presidente del Consiglio dei ministri 1' marzo    &#13;
 1991  (Ripartizione tra i bacini di rilievo nazionale, interregionale    &#13;
 e  regionale  dei  fondi disponibili nel periodo 1989-93 da destinare    &#13;
 all'attuazione degli  schemi  previsionali  e  programmatici  per  il    &#13;
 riassetto  organizzativo  e  funzionale della difesa del suolo), che,    &#13;
 nel  prevedere  l'emanazione  futura  di  due  atti  di  indirizzo  e    &#13;
 coordinamento  privi  di  qualsiasi  base  legislativa, violerebbe le    &#13;
 attribuzioni  statutariamente  spettanti  alla  Provincia   autonoma,    &#13;
 ponendosi  in  contrasto  con la consolidata giurisprudenza di questa    &#13;
 Corte, la quale esige per quegli atti un  fondamento  sostanziale  in    &#13;
 disposizioni  di  legge, diretto a precisare gli interessi unitari da    &#13;
 salvaguardare e i criteri  per  l'esercizio  della  potestà  statale    &#13;
 d'indirizzo.                                                             &#13;
    Più  precisamente, la ricorrente - dopo aver precisato che l'art.    &#13;
 3, al secondo comma, prevede l'emanazione  di  atti  di  indirizzo  e    &#13;
 coordinamento  destinati a determinare i criteri d'integrazione delle    &#13;
 attività conoscitive e le modalità per lo svolgimento delle  stesse    &#13;
 e,  al terzo comma, predetermina l'adozione di atti dello stesso tipo    &#13;
 diretti a stabilire le procedure,  gli  obiettivi  e  i  criteri  per    &#13;
 l'aggiornamento dei capitolati per l'esecuzione delle opere - osserva    &#13;
 che  nell'ordinamento  non  sussiste alcuna disposizione di legge che    &#13;
 offra fondamento sostanziale ai predetti atti.                           &#13;
    Infatti, tale non può essere la disposizione che li  prevede,  la    &#13;
 quale  è  contenuta  in  un decreto del Presidente del Consiglio dei    &#13;
 ministri, cioè in un  atto  amministrativo  cui  l'art.  31,  quarto    &#13;
 comma, della legge n. 183 del 1989 affida solo il compito di disporre    &#13;
 la  ripartizione  fra i bacini e le regioni (o province autonome) dei    &#13;
 fondi  disponibili  per  l'attuazione  degli  schemi  previsionali  e    &#13;
 programmatici.  Né quel fondamento può esser rintracciato nell'art.    &#13;
 2, terzo comma,  lettera  d),  della  legge  n.  400  del  1988,  pur    &#13;
 menzionato  dalla  disposizione impugnata, poiché, come questa Corte    &#13;
 ha più volte sottolineato, si tratta di norma che mira semplicemente    &#13;
 a determinare, all'interno della complessa istituzione  del  Governo,    &#13;
 l'organo  attributario  in via diretta e immediata della competenza a    &#13;
 deliberare i predetti atti, e non  già  a  stabilire  il  fondamento    &#13;
 legislativo  sostanziale  della relativa disciplina. Né, allo stesso    &#13;
 fine, può essere invocato l'art. 31, quarto comma,  della  legge  n.    &#13;
 183  del  1989,  il  quale  prevede il potere di adottare decreti del    &#13;
 Presidente del Consiglio dei ministri in ordine alla  predisposizione    &#13;
 degli  schemi  previsionali e programmatici, potere che è stato già    &#13;
 esercitato con il d.P.C.M. 23 marzo 1990. Né,  infine,  sempre  allo    &#13;
 stesso scopo, si può fare riferimento all'art. 4, primo comma, della    &#13;
 legge  n.  183  del  1989  (peraltro  non  richiamato  dalle premesse    &#13;
 dell'atto impugnato), che prevede l'emanazione di "ogni altro atto di    &#13;
 indirizzo e coordinamento  nel  settore  disciplinato"  dalla  stessa    &#13;
 legge,  poiché  il  decreto  contestato lungi dal porsi come atto di    &#13;
 esercizio della potestà d'indirizzo e  coordinamento  ivi  prevista,    &#13;
 pretende, piuttosto, di fondare esso stesso una potestà dello stesso    &#13;
 tipo.                                                                    &#13;
    In  conclusione,  afferma  la  ricorrente,  se l'impugnato art. 3,    &#13;
 secondo comma,  sembra  avere  una  vaga  assonanza  con  la  materia    &#13;
 disciplinata  dall'art. 2, secondo comma, della legge n. 183 del 1989    &#13;
 (modalità di  coordinamento  e  di  collaborazione  fra  i  soggetti    &#13;
 pubblici   del  settore  in  ordine  alle  attività  conoscitive  da    &#13;
 adottarsi  con  deliberazione  del  Presidente  del   Consiglio   dei    &#13;
 ministri),  al  contrario  il comma successivo dispone su una materia    &#13;
 (procedure,  obiettivi  e  criteri  per l'adozione dei capitolati per    &#13;
 l'esecuzione delle opere) che non può in alcun modo  essere  oggetto    &#13;
 di  atti  di indirizzo e coordinamento, trattandosi di profili che lo    &#13;
 Stato può disciplinare soltanto nei confronti delle  amministrazioni    &#13;
 proprie o di enti da esso dipendenti.<diritto>Considerato in diritto</diritto>1.  -  La  Provincia  autonoma di Trento ha sollevato conflitto di    &#13;
 attribuzione nei confronti dello Stato, in relazione al  decreto  del    &#13;
 Presidente del Consiglio dei ministri del 1' marzo 1991 (Ripartizione    &#13;
 tra  i  bacini  di  rilievo nazionale, interregionale e regionale dei    &#13;
 fondi disponibili nel periodo  1989-93  da  destinare  all'attuazione    &#13;
 degli   schemi   previsionali   e   programmatici  per  il  riassetto    &#13;
 organizzativo e funzionale della  difesa  del  suolo),  con  riguardo    &#13;
 all'art. 3, secondo e terzo comma, che prevede l'adozione di due atti    &#13;
 di  indirizzo e coordinamento, aventi ad oggetto, rispettivamente, "i    &#13;
 criteri  di  integrazione  e  di  coordinamento  tra   le   attività    &#13;
 conoscitive  dello  Stato, delle autorità di bacino e delle regioni,    &#13;
 nonché le modalità, anche tecniche, per  lo  svolgimento  di  dette    &#13;
 attività"   e   "le   procedure,  gli  obiettivi  e  i  criteri  per    &#13;
 l'aggiornamento dei capitolati per l'esecuzione delle opere attinenti    &#13;
 la difesa del suolo". Secondo la ricorrente, poiché le potestà  ivi    &#13;
 previste  sarebbero  totalmente prive di un fondamento sostanziale in    &#13;
 disposizioni di legge, vòlto a precisare gli  interessi  unitari  da    &#13;
 salvaguardare e i criteri per lo svolgimento delle relative funzioni,    &#13;
 le disposizioni impugnate costituirebbero esercizio illegittimo di un    &#13;
 potere    statale,    ridondante    in   lesione   delle   competenze    &#13;
 statutariamente  assegnate  alla  Provincia  autonoma  di  Trento  in    &#13;
 relazione alle proprie attività di organizzazione amministrativa.       &#13;
    2. - Il ricorso va accolto.                                           &#13;
   Con  la  sentenza n. 150 del 1982, questa Corte, nell'inquadrare la    &#13;
 funzione statale di indirizzo e coordinamento nell'ambito delle norme    &#13;
 costituzionali relative al rapporto tra  la  potestà  legislativa  e    &#13;
 amministrativa dello Stato e l'autonomia delle regioni (e delle province  autonome),  ha  enunciato  il  principio che l'esercizio in via    &#13;
 amministrativa, da parte dello Stato, della  funzione  d'indirizzo  e    &#13;
 coordinamento  "è giustificato solo se trova un legittimo e apposito    &#13;
 supporto nella legislazione statale". Da  questo  principio  derivano    &#13;
 due  corollari:  a)  che ogni esercizio della potestà di indirizzo e    &#13;
 coordinamento dev'essere appositamente previsto  da  norme  di  legge    &#13;
 statale,  dirette  a istituire la relativa funzione con riguardo a un    &#13;
 determinato ambito di  attività  attribuito  alle  competenze  delle    &#13;
 regioni  o  delle  province autonome; b) che, come è stato precisato    &#13;
 dalla stessa sentenza n. 150 del 1982 e come è stato  confermato  da    &#13;
 successive  pronunzie  di questa Corte (v., da ultimo, sentt. nn. 338    &#13;
 del  1989,  37,  49  e  359  del  1991),  gli  atti  di  indirizzo  e    &#13;
 coordinamento    possono    validamente    incidere    sull'autonomia    &#13;
 costituzionalmente garantita alle regioni e  alle  province  autonome    &#13;
 soltanto  sulla base di disposizioni di legge vòlte a delimitare "il    &#13;
 possibile contenuto sostanziale degli atti di questo tipo".              &#13;
    Le due disposizioni del d.P.C.M. 1' marzo 1991, in relazione  alle    &#13;
 quali  è stato sollevato il conflitto di attribuzione in esame, sono    &#13;
 lesive delle competenze provinciali.                                     &#13;
    2.1.  -  L'art.  3,  terzo  comma,  del  decreto  impugnato, nello    &#13;
 stabilire che, con atto di indirizzo e coordinamento,  da  adottarsi,    &#13;
 ai  sensi  dell'art.  2  della  legge  n.  400  del 1988, entro il 31    &#13;
 dicembre 1991, saranno definiti "le procedure,  gli  obiettivi  ed  i    &#13;
 criteri  per  l'aggiornamento  dei  capitolati per l'esecuzione delle    &#13;
 opere attinenti alla difesa del suolo", fa riferimento a una potestà    &#13;
 statale di indirizzo e coordinamento di cui non  v'è  traccia  nella    &#13;
 vigente legislazione statale. In altri termini, quello previsto dalla    &#13;
 disposizione  ora citata è un potere che non ha il proprio titolo di    &#13;
 legittimazione né nella legge n. 183 del 1989  -  il  cui  art.  31,    &#13;
 quarto  comma,  prevede  semplicemente la distribuzione dei fondi per    &#13;
 l'attuazione  degli  schemi  previsionali  e  programmatici  per   il    &#13;
 riassetto organizzativo e funzionale della difesa del suolo -, né in    &#13;
 altra legge dello Stato.                                                 &#13;
    Del resto, la stessa norma legislativa invocata dalla disposizione    &#13;
 impugnata - vale a dire, l'art. 2, terzo comma, della legge 23 agosto    &#13;
 1988,  n.  400 - non può certo fungere da norma istitutiva di quello    &#13;
 specifico  potere,  poiché,  come  questa  Corte  ha  già  detto  a    &#13;
 proposito  della  stessa  disposizione  di legge (v. sent. n. 242 del    &#13;
 1989) o di altre disposizioni similari (v. sentt. nn. 150  del  1982,    &#13;
 139  e  345 del 1990), ivi compreso l'art. 4, primo comma, lettera f)    &#13;
 (v. sent. n. 85 del 1990), si tratta di norma legislativa che non  è    &#13;
 diretta   ad  attribuire  al  Governo  una  specifica  competenza  ad    &#13;
 esercitare funzioni di indirizzo e coordinamento verso le  regioni  o    &#13;
 le  province autonome, ma che mira, più semplicemente, a individuare    &#13;
 all'interno  della   complessa   istituzione   governativa   l'organo    &#13;
 attributario,   in  via  diretta  e  immediata,  della  competenza  a    &#13;
 deliberare gli atti di indirizzo e coordinamento.                        &#13;
    Poiché,  pertanto,  quella  impugnata  è  una  disposizione  non    &#13;
 legislativa  che pretende di istituire un nuovo potere di indirizzo e    &#13;
 coordinamento, non si può minimamente dubitare che, in  armonia  con    &#13;
 la consolidata giurisprudenza di questa Corte, l'art. 3, terzo comma,    &#13;
 sia  frutto  di un esercizio illegittimo di un potere statale, lesivo    &#13;
 dell'autonomia costituzionalmente garantita alla Provincia di Trento.    &#13;
    2.2. - L'art. 3, secondo comma, contiene una disposizione similare    &#13;
 che, tuttavia, ha qualche aggancio legislativo. Esso  stabilisce  che    &#13;
 "con  atto  di  indirizzo  e  coordinamento,  da  adottarsi  ai sensi    &#13;
 dell'art. 2 della legge n. 400/1988, sono determinati,  entro  il  30    &#13;
 giugno  1991,  i  criteri  di  integrazione e di coordinamento tra le    &#13;
 attività conoscitive dello Stato, delle autorità di bacino e  delle    &#13;
 regioni,  nonché le modalità, anche tecniche, per lo svolgimento di    &#13;
 dette attività".                                                        &#13;
    Si tratta, come è evidente, di  una  disposizione  che  tocca  la    &#13;
 materia  regolata  dagli  artt.  2, primo e secondo comma, e 4, primo    &#13;
 comma, della legge 18 maggio 1989, n. 183. Tali  articoli  prevedono,    &#13;
 innanzitutto,  che  l'attività  conoscitiva, relativa alle finalità    &#13;
 della predetta legge  e  riferita  all'intero  territorio  nazionale,    &#13;
 debba  essere  sottoposta  ai  criteri, ai metodi e agli standards di    &#13;
 raccolta, elaborazione e consultazione,  nonché  alle  modalità  di    &#13;
 coordinamento  e  di  collaborazione tra i soggetti pubblici comunque    &#13;
 operanti nel settore,  al  fine  di  garantire  "la  possibilità  di    &#13;
 un'omogenea  elaborazione ed analisi e della costituzione e gestione,    &#13;
 ad  opera  dei  servizi  tecnici  nazionali,  di  un  unico   sistema    &#13;
 informativo,  cui  vanno raccordati i sistemi informativi regionali e    &#13;
 quelli  delle province autonome". Gli stessi articoli precisano, poi,    &#13;
 che i suddetti criteri e metodi debbono essere approvati con  decreto    &#13;
 del  Presidente  del Consiglio dei ministri, previa deliberazione del    &#13;
 Consiglio dei ministri e su proposta del Ministro dei lavori pubblici    &#13;
 (art. 4, primo comma, lettera a).                                        &#13;
    Questa Corte ha già precisato nella sentenza n. 85 del  1990  che    &#13;
 gli  atti  governativi previsti dalle disposizioni legislative appena    &#13;
 ricordate non possono essere ricondotti all'esercizio della  funzione    &#13;
 di indirizzo e coordinamento.                                            &#13;
    Pertanto,  in  considerazione  del  fatto che si tratta di un atto    &#13;
 amministrativo  che  pretende  di  istituire  una  nuova  ipotesi  di    &#13;
 esercizio  della  funzione  governativa di indirizzo e coordinamento,    &#13;
 non si può non ritenere che anche l'art. 3, secondo comma, è lesivo    &#13;
 delle competenze statutariamente assicurate alla  Provincia  autonoma    &#13;
 di Trento (art. 8, Statuto speciale per il Trentino-Alto Adige).</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  che  non  spetta  allo  Stato  prevedere, con decreto del    &#13;
 Presidente del Consiglio  dei  ministri,  l'adozione  degli  atti  di    &#13;
 indirizzo  e  coordinamento  indicati  nell'art.  3,  secondo e terzo    &#13;
 comma, del d.P.C.M. 1' marzo  1991  (Ripartizione  tra  i  bacini  di    &#13;
 rilievo  nazionale,  interregionale e regionale dei fondi disponibili    &#13;
 nel  periodo  1989-1993  da  destinare  all'attuazione  degli  schemi    &#13;
 previsionali   e  programmatici  per  il  riassetto  organizzativo  e    &#13;
 funzionale della difesa del suolo) e,  conseguentemente,  annulla  il    &#13;
 suddetto art. 3, secondo e terzo comma, del d.P.C.M. 1' marzo 1991.      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 gennaio 1992.                              &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: BALDASSARRE                          &#13;
                       Il cancelliere: FRUSCELLA                          &#13;
    Depositata in cancelleria il 3 febbraio 1992.                         &#13;
                       Il cancelliere: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
