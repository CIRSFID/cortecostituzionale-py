<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1970</anno_pronuncia>
    <numero_pronuncia>111</numero_pronuncia>
    <ecli>ECLI:IT:COST:1970:111</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Michele Fragali</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>17/06/1970</data_decisione>
    <data_deposito>26/06/1970</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - AVV. ERCOLE ROCCHETTI &#13;
 - Prof. ENZO CAPALOZZA - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - &#13;
 Prof. PAOLO ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti di legittimità costituzionale dell'art. 501,  &#13;
 primo e ultimo comma, del codice di procedura penale, promossi  con  le  &#13;
 seguenti ordinanze:                                                      &#13;
     1)  ordinanza  emessa il 22 maggio 1968 dalla Corte di cassazione -  &#13;
 sezione seconda penale - nel procedimento  penale  a  carico  di  Leone  &#13;
 Italo,  iscritta  al  n.  221  del registro ordinanze 1968 e pubblicata  &#13;
 nella Gazzetta Ufficiale della Repubblica n. 305 del 30 novembre 1968;   &#13;
     2) ordinanza emessa il 10 dicembre 1968 dalla  Corte  d'appello  di  &#13;
 Caltanissetta  nel  procedimento  penale  a  carico di Coljanni Michele  &#13;
 Vincenzo Nunzio, iscritta  al  n.  8  del  registro  ordinanze  1969  e  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica  n. 52 del 26  &#13;
 febbraio 1969.                                                           &#13;
     Udito nella camera di  consiglio  del  6  maggio  1970  il  Giudice  &#13;
 relatore Michele Fragali.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     La  Corte  di  cassazione  (ordinanza 22 maggio 1968) e la Corte di  &#13;
 appello di Caltanissetta (ordinanza 10 dicembre 1968) hanno  sottoposto  &#13;
 a  questa  Corte  la questione di legittimità costituzionale del comma  &#13;
 primo dell'art. 501  del  codice  di  procedura  penale,  limitatamente  &#13;
 all'inciso "prima che sia cominciata la discussione finale", e l'ultimo  &#13;
 comma  di detto articolo. Le norme sono state ritenute in contrasto con  &#13;
 l'articolo   24   della   Costituzione,    perché    non    consentono  &#13;
 l'interrogatorio  del  contumace  comparso  durante il dibattimeno dopo  &#13;
 l'inizio della discussione; in tal  modo,  secondo  le  due  ordinanze,  &#13;
 l'imputato   rimane   privato  dell'esercizio  di  un  diritto  che  è  &#13;
 fondamentale per la sua difesa.                                          &#13;
     Non v'è stata costituzione di parte.<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Le  due  cause  riguardano  la stessa norma e debbono essere  &#13;
 decise con unica sentenza.                                               &#13;
     2. -  Il  divieto  di  procedere  ad  interrogatorio  dell'imputato  &#13;
 contumace   comparso  durante  la  discussione  finale,  oggetto  delle  &#13;
 ordinanze indicate in epigrafe e delle norme con esse  denunziate,  non  &#13;
 è in contrasto con la garanzia costituzionale del diritto di difesa.    &#13;
     La  Corte  ha  altre  volte  deciso  che questo diritto può essere  &#13;
 regolato dalla legge, sia nel modo sia nel tempo, per evitare sviamenti  &#13;
 dallo scopo della protezione datagli, e soprattutto per evitare che sia  &#13;
 attuato in maniera ingiustificatamente dilatoria o del  tutto  sterile,  &#13;
 così  da  pregiudicare l'ordinata amministrazione della giustizia, che  &#13;
 è un'esigenza di interesse generale, e  lo  stesso  svolgimento  della  &#13;
 funzione  giurisdizionale,  che non è nella disponibilità privata. Il  &#13;
 diritto di difesa, è vero, va garantito in  ogni  stato  e  grado  del  &#13;
 procedimento;   ma,  non  potendo  affidarsene  l'esercizio  alla  mera  &#13;
 discrezione dell'interessato, la legge può ritenere conchiusa una fase  &#13;
 del processo ed immutabilmente fissate le situazioni  che  vi  si  sono  &#13;
 costituite,  perché  il  processo  possa progredire verso la decisione  &#13;
 finale e se ne impedisca l'indefinito protrarsi: è questa  un'esigenza  &#13;
 logica  prima che giuridica (sentenza di questa Corte 25 marzo 1970, n.  &#13;
 50). A tale esigenza risponde la norma denunciata.                       &#13;
     La fase istruttoria, nel dibattimento,  si  conclude  con  l'inizio  &#13;
 della  discussione  finale, secondo valutazioni di politica giudiziaria  &#13;
 insindacabili in sede costituzionale (sentenza predetta); e si  intende  &#13;
 come  si  favorirebbero  le  pretestuosità  e  la dilatorietà, ove la  &#13;
 comparizione del contumace potesse interrompere  quella  discussione  o  &#13;
 potesse  farla  rinnovare.  La norma denunciata non esclude, del resto,  &#13;
 l'applicazione dell'art. 468, terzo  comma,  del  codice  di  procedura  &#13;
 penale,  che,  a pena di nullità, impone al presidente o al pretore di  &#13;
 dare la parola all'imputato e al difensore alla fine della discussione,  &#13;
 se lo domandano; ed è ovvio  che,  in  tal  modo,  pur  non  potendosi  &#13;
 esigere che in quella fase le sue dichiarazioni assumano il contenuto e  &#13;
 la  forma di un interrogatorio, il contumace comparso è messo in grado  &#13;
 di esporre i punti  essenziali  della  propria  difesa,  dei  quali  il  &#13;
 giudice  non  può  non  tener  conto nella decisione che è chiamato a  &#13;
 pronunciare.                                                             &#13;
     Inoltre,  la  proibizione  contenuta  nelle  norme  denunciate  non  &#13;
 preclude  l'interrogatorio  nella fase di appello né  la presentazione  &#13;
 di prove nuove e l'istanza di rinnovare le prove assunte; il giudice è  &#13;
 financo abilitato a rinnovare in tutto o in parte il dibattimento (art.  &#13;
 50 cod. proc. pen.). In modo che l'unico  pregiudizio  che  il  divieto  &#13;
 stesso  produce  al contumace comparso sta, tutt'al più, nella perdita  &#13;
 della prima fase del primo grado di cognizione; ma ciò accade  perché  &#13;
 l'imputato non vi era presente.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 del primo comma dell'art.  501  del  codice  di  procedura  penale  per  &#13;
 l'inciso "prima che sia cominciata la discussione finale" e dell'ultimo  &#13;
 comma  dello  stesso  articolo,  promossa dalla Corte di cassazione con  &#13;
 ordinanza 22 maggio 1968 e dalla Corte d'appello di  Caltanissetta  con  &#13;
 ordinanza   10   dicembre   1968,  in  riferimento  all'art.  24  della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte Costituzionale, Palazzo della Consulta il 17 giugno 1970.          &#13;
                                   GIUSEPPE BRANCA - MICHELE  FRAGALI  -  &#13;
                                   COSTANTINO    MORTATI    -   GIUSEPPE  &#13;
                                   CHIARELLI   -   GIUSEPPE   VERZÌ   -  &#13;
                                   GIOVANNI    BATTISTA    BENEDETTI   -  &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI  -  ERCOLE  ROCCHETTI  - ENZO  &#13;
                                   CAPALOZZA - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
