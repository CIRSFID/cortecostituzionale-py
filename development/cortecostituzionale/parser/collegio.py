from akomantoso.dom.elements import Block
from akomantoso.dom.meta import TLCPerson
from akomantoso.utils import to_camelCase

from cortecostituzionale.parser.text_cleanup import cleanup
from cortecostituzionale.parser.parser import parser


def _parse_giudici(text):
    inlines = []
    for m in parser.parse(text, "judges"):
        value = " ".join(m.value.strip().split())
        eid = to_camelCase(value)
        judge = (
            "judge",
            (m.start, m.start + len(m.value.strip())),
            dict(refersTo=TLCPerson(eid=eid, href=f"/akn/ontology/person/it/{eid}", show_as=value))
        )
        inlines.append(judge)
    return inlines


def parse_collegio(collegio):
    block = Block(name="collegio")
    if collegio.text:
        block._text = collegio.text if collegio.text is not None else ""
    titolo = collegio.find("titolo")
    if titolo is not None:
        titolo_text = titolo.text
        block._text += titolo_text
        s, e = block._text.rfind(titolo_text), len(block._text)
        block.add_inline(("courtType", (s, e), dict(refersTo="#corteCostituzionale")))
        block._text += "\n"
        block._text += " ".join(cleanup(titolo.tail))
    block.add_inlines(*_parse_giudici(block.text))
    return block
