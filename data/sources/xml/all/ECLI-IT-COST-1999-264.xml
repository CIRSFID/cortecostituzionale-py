<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>264</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:264</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/06/1999</data_decisione>
    <data_deposito>23/06/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale dell'art. 222 del decreto    &#13;
 legislativo  30  aprile  1992, n. 285 (Nuovo codice della strada), in    &#13;
 relazione agli artt. 218, commi 1, 2 e 5 stesso decreto  legislativo,    &#13;
 133  del  codice  penale;  444  e 445 del codice di procedura penale,    &#13;
 promosso con ordinanza emessa il  14  ottobre  1998  dal  pretore  di    &#13;
 Brescia  nel  procedimento  penale  a  carico  di  Amadei Pierangelo,    &#13;
 iscritta al n. 15 del registro  ordinanze  1999  e  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  n.  4,  prima serie speciale,    &#13;
 dell'anno 1999.                                                          &#13;
   Visti l'atto di costituzione di Amadei Pierangelo nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
   Udito nella camera di consiglio  del  25  maggio  1999  il  giudice    &#13;
 relatore Cesare Ruperto.                                                 &#13;
   Ritenuto  che  il  pretore  di  Brescia  - nel corso di un giudizio    &#13;
 penale,  a  seguito  dell'annullamento,  da  parte  della  Corte   di    &#13;
 cassazione,  della  sentenza  resa  ex  art.444 cod. proc. pen. dallo    &#13;
 stesso giudice, limitatamente all'omessa applicazione della  sanzione    &#13;
 amministrativa  accessoria  della sospensione della patente di guida,    &#13;
 di cui all'art.   222 del codice della strada  -  ha  sollevato,  con    &#13;
 ordinanza   del   14   ottobre   1998,   questione   di  legittimità    &#13;
 costituzionale dell'art.  222 del decreto legislativo 30 aprile 1992,    &#13;
 n. 285 (Nuovo codice della strada), in relazione all'art. 218,  commi    &#13;
 1, 2 e 5 dello stesso decreto legislativo, all'art. 133 cod. pen., ed    &#13;
 agli artt. 444 e 445 cod. proc. pen;                                     &#13;
     che,  secondo  il  rimettente, le norme denunciate - imponendo al    &#13;
 giudice  l'applicazione  d'ufficio  della   sanzione   amministrativa    &#13;
 accessoria  anche  con la sentenza emessa ai sensi dell'art. 444 cod.    &#13;
 proc. pen., senza che il punto  possa  formare  oggetto  dell'accordo    &#13;
 delle  parti - contrastano: a) con gli artt. 101 e 111 Cost., perché    &#13;
 il giudice del patteggiamento è tenuto a fissare, entro il minimo ed    &#13;
 il massimo di legge, la durata della  sospensione  della  patente  di    &#13;
 guida,  senza aver cognizione del merito della causa e senza disporre    &#13;
 (diversamente dall'autorità amministrativa) di  alcun  parametro  di    &#13;
 giudizio;  b)  con  l'art.  24 Cost., perché l'imputato non può né    &#13;
 interloquire in giudizio, né  impugnare  per  motivi  di  merito  la    &#13;
 decisione sulla durata della sospensione della patente di guida;         &#13;
     che  si  è  costituita  in  giudizio la parte privata, chiedendo    &#13;
 l'accoglimento della sollevata questione,  in  relazione  a  tutti  i    &#13;
 parametri  costituzionali  evocati, stante il contrasto dell'art. 222    &#13;
 del codice della strada: a) con gli artt. 132 e 133 cod.  pen.,  "che    &#13;
 limitano  il potere discrezionale del giudice nell'applicazione della    &#13;
 pena  con  l'obbligo  di  indicare   i   motivi   che   tale   potere    &#13;
 giustificano",  nonché,  "soprattutto",  con il principio che impone    &#13;
 l'obbligo di motivazione di  tutti  i  provvedimenti  giurisdizionali    &#13;
 (art. 111 Cost.), sotto il profilo che il giudice del patteggiamento,    &#13;
 dovendo  prescindere dall'accertamento dell'oggettiva sussistenza del    &#13;
 reato e della colpevolezza dell'accusato,  non  può  motivare  sulla    &#13;
 durata della sospensione della patente di guida; b) con il diritto di    &#13;
 difesa  garantito dall'art.  24 Cost., sotto il profilo che in ordine    &#13;
 alla durata della sospensione della patente di guida  l'imputato  non    &#13;
 può  interloquire, né addurre prove a proprio favore, né impugnare    &#13;
 la decisione se non per motivi di legittimità;                          &#13;
     che è intervenuto in giudizio il Presidente  del  Consiglio  dei    &#13;
 Ministri,  rappresentato  e  difeso  dall'Avvocatura  generale  dello    &#13;
 Stato,  concludendo  per  la  declaratoria  di   infondatezza   della    &#13;
 sollevata  questione  e  richiamando  la  memoria presentata a questa    &#13;
 Corte su analoghe  questioni,  sollevate  dallo  stesso  pretore  con    &#13;
 ordinanza  del  16  gennaio 1998 e trattate nella Camera di consiglio    &#13;
 del 13 gennaio 1999.                                                     &#13;
   Considerato che il giudice a quo, nel prospettare la  questione  di    &#13;
 legittimità  costituzionale,  muove  dal presupposto che la sanzione    &#13;
 amministrativa della sospensione della  patente  di  guida,  per  una    &#13;
 durata  determinata  dal  giudice  entro  il  minimo ed il massimo di    &#13;
 legge,  sia  accessoria  all'accertamento  del  reato   (secondo   la    &#13;
 formulazione  della rubrica dell'art. 222 cod. strada) e, perciò, ad    &#13;
 una dichiarazione di responsabilità incompatibile con  la  pronuncia    &#13;
 di  applicazione  della  pena  su  richiesta  delle  parti  ai  sensi    &#13;
 dell'art. 444 cod. proc.  pen;                                           &#13;
     che questa Corte ha già rilevato l'erroneità di  tale  assunto,    &#13;
 poiché  la  sanzione  amministrativa di cui all'art. 222 cod. strada    &#13;
 non presuppone, logicamente  o  normativamente,  la  declaratoria  di    &#13;
 responsabilità  penale  attraverso una sentenza di condanna in senso    &#13;
 proprio,  bastando  invece  l'accertamento  del  mero  fatto   lesivo    &#13;
 dell'interesse  pubblico:  accertamento  di  certo compatibile con la    &#13;
 pronuncia di cui all'art. 444 cod. proc. pen. (ordinanza  n.  25  del    &#13;
 1999);                                                                   &#13;
     che,   in   particolare,  contrariamente  a  quanto  opinato  dal    &#13;
 rimettente,  il  diritto  vivente  -  interpretandosi   l'espressione    &#13;
 "accertamento  del  reato",  contenuta  nella  rubrica  dell'articolo    &#13;
 stesso, nel senso di  accertamento,  nell'ambito  e  nei  limiti  del    &#13;
 procedimento  di  cui  all'art. 444 cod. proc. pen., del fatto lesivo    &#13;
 dell'interesse pubblico, al quale consegue l'applicazione di una pena    &#13;
 - impone al giudice di merito, per  la  determinazione  della  durata    &#13;
 della   sospensione   della  patente  di  guida  e  per  la  relativa    &#13;
 motivazione, di attenersi ai  parametri  di  cui  all'art.  218  cod.    &#13;
 strada;                                                                  &#13;
     che  la  libertà  nella  scelta del procedimento di cui all'art.    &#13;
 444  cod  proc.  pen.  e  la   discrezionalità   nella   valutazione    &#13;
 prognostica degli effetti conseguenti a tale scelta, escludono che la    &#13;
 mancata  impugnabilità  per  vizi  di  merito  della  determinazione    &#13;
 giudiziale della durata della  sospensione  della  patente  di  guida    &#13;
 costituisca  lesione  del  diritto  di  difesa garantito dall'art. 24    &#13;
 della Costituzione;                                                      &#13;
     che, pertanto, la questione proposta è manifestamente  infondata    &#13;
 in riferimento a tutti i parametri evocati.                              &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art. 222 del decreto legislativo 30 aprile 1992,    &#13;
 n. 285 (Nuovo codice della strada),  in  relazione  agli  artt.  218,    &#13;
 commi  1,  2  e  5  dello  stesso decreto legislativo, 133 del codice    &#13;
 penale, 444 e 445 del codice di  procedura  penale,  sollevata  -  in    &#13;
 riferimento agli artt. 101, 111 e 24 della Costituzione - dal pretore    &#13;
 di Brescia con l'ordinanza indicata in epigrafe.                         &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 giugno 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 giugno 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
