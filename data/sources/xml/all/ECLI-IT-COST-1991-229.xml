<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>229</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:229</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Luigi Mengoni</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/05/1991</data_decisione>
    <data_deposito>24/05/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt. 2 e 3,    &#13;
 n. 1 del codice di procedura penale, promossi con n. 3 ordinanze  dal    &#13;
 Giudice  per  le  indagini preliminari presso il Tribunale di Ancona,    &#13;
 iscritti rispettivamente ai nn. 90, 105 e 143 del registro  ordinanze    &#13;
 1991 e pubblicate nelle Gazzette Ufficiali della Repubblica nn. 9, 10    &#13;
 e 11, prima serie speciale dell'anno 1991;                               &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  22  aprile  1991  il  Giudice    &#13;
 relatore Luigi Mengoni;                                                  &#13;
    Ritenuto che, nel corso di due processi penali a carico di persone    &#13;
 imputate  del  reato  di  bancarotta  fraudolenta,  il Giudice per le    &#13;
 indagini preliminari presso il Tribunale  di  Ancona,  risultando  la    &#13;
 pendenza  del  giudizio  civile  di opposizione alla dichiarazione di    &#13;
 fallimento, ha  sollevato,  in  esito  all'udienza  preliminare,  con    &#13;
 ordinanze  in  data  2  e  29  ottobre 1990 (R.O. 143/1991, 90/1991),    &#13;
 questione di legittimità costituzionale degli artt. 2 e 3, n. 1, del    &#13;
 nuovo codice di procedura penale, nella parte in cui  non  consentono    &#13;
 la  sospensione  del  processo  fino  al passaggio in giudicato della    &#13;
 sentenza  civile  o  amministrativa  che  definisce   una   questione    &#13;
 pregiudiziale  di  stato  diversa  da  quelle concernenti lo stato di    &#13;
 famiglia o di cittadinanza;                                              &#13;
      che  lo  stesso  giudice  -   investito   della   richiesta   di    &#13;
 archiviazione  formulata  dal  pubblico  ministero in un procedimento    &#13;
 avviato su querela di una persona ritenutasi diffamata da enunciative    &#13;
 di fatti contenute nella motivazione di  un  provvedimento  impugnato    &#13;
 davanti  al  tribunale  amministrativo  regionale - ha sollevato, con    &#13;
 ordinanza in data 30 novembre 1990 (R.O. 105/1991), analoga questione    &#13;
 di  legittimità  costituzionale  dell'art.  3,  n.  1,  del   codice    &#13;
 predetto,  in  quanto non gli consente di sospendere il procedimento,    &#13;
 soprassedendo alla decisione sulla richiesta del pubblico  ministero,    &#13;
 fino   al   passaggio   in   giudicato  della  sentenza  del  giudice    &#13;
 amministrativo;                                                          &#13;
      che  la  violazione  dei  parametri costituzionali richiamati è    &#13;
 fondata  sul  confronto  della  norma  denunciata  con   l'art.   479    &#13;
 cod.proc.pen.,  il  quale  autorizza  il  giudice  del dibattimento a    &#13;
 sospendere il processo pur quando insorgano  questioni  pregiudiziali    &#13;
 alla  decisione  sull'esistenza  del reato diverse da quelle indicate    &#13;
 nell'art. 3, e quindi anche in caso di controversia sulla qualità di    &#13;
 fallito in pendenza di  opposizione  alla  sentenza  dichiarativa  di    &#13;
 fallimento;                                                              &#13;
      che,  oltre  alla  violazione  del  principio di eguaglianza, è    &#13;
 ravvisato  un  contrasto  col  principio  di  razionalità,   perché    &#13;
 "potrebbe  configurarsi  un  eventuale  conflitto  fra  sentenza  del    &#13;
 dibattimento penale (nell'ipotesi di rinvio a giudizio)  e  giudicato    &#13;
 civile  ovvero  fra  sentenza  di  non luogo a procedere del G.I.P. e    &#13;
 giudicato civile che dovesse respingere l'opposizione al fallimento",    &#13;
 nonché con l'art. 2 Cost.;                                              &#13;
      che nei giudizi davanti alla Corte è intervenuto il  Presidente    &#13;
 del  Consiglio  dei  ministri,  rappresentato  dall'Avvocatura  dello    &#13;
 Stato, chiedendo che la questione sia dichiarata infondata;              &#13;
    Considerato che i giudizi incidentali promossi dalle tre ordinanze    &#13;
 hanno per oggetto la medesima questione, per cui si  rende  opportuno    &#13;
 disporne la riunione affinché siano decisi con unico provvedimento;     &#13;
      che   nessuna  motivazione  è  addotta  dal  giudice  a  quo  a    &#13;
 fondamento dell'asserito contrasto delle norme impugnate con l'art. 2    &#13;
 Cost.;                                                                   &#13;
      che nel nuovo  sistema  processuale  non  possono  essere  messe    &#13;
 validamente  a  confronto,  ai  fini dell'art. 3 Cost., la disciplina    &#13;
 della fase preliminare e la disciplina della fase  del  dibattimento,    &#13;
 la prima essendo "stata congegnata, nel suo regime ordinario, come un    &#13;
 procedimento allo stato degli atti" (sentenza n. 64 del 1991);           &#13;
      che   con   tale   impostazione  appare  coerente  il  carattere    &#13;
 eccezionale del potere del giudice  di  sospendere  il  processo  per    &#13;
 ragioni  di  pregiudizialità,  così come, per una ratio analoga, è    &#13;
 eccezionale la facoltà  di  promuovere  il  supplemento  istruttorio    &#13;
 previsto dall'art. 422;                                                  &#13;
      che  la  limitazione  del  potere  del  giudice  per le indagini    &#13;
 preliminari ai casi indicati nell'art. 3, n. 1, si  giustifica  anche    &#13;
 alla  stregua  di  un altro criterio-guida della riforma del processo    &#13;
 penale, cioè l'attenuazione dell'interdipendenza tra giudizio penale    &#13;
 e giudizio civile o amministrativo, onde la sospensione del  processo    &#13;
 nei  casi  previsti  dalla legge è sempre una facoltà del giudice e    &#13;
 mai un obbligo, diversamente da quanto disponeva l'art. 19 del codice    &#13;
 del 1930;                                                                &#13;
      che la decisione del giudice penale che  risolve  una  questione    &#13;
 civile  o  amministrativa non ha efficacia vincolante in nessun altro    &#13;
 processo (art. 2, secondo comma, cod. proc. pen.);                       &#13;
      che nel caso della terza ordinanza (R.O. 105/1991) la  questione    &#13;
 è  irrilevante,  essendo  stata  sollevata  prima  che avesse inizio    &#13;
 l'azione penale e quindi in assenza di una imputazione;                  &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87,  e  9  delle  Norme  integrative per i giudizi davanti alla Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi,  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale degli artt. 2 e 3, n. 1, del    &#13;
 nuovo codice di procedura  penale,  sollevata,  in  riferimento  agli    &#13;
 artt.  2  e  3  della  Costituzione,  dal  Giudice  per  le  indagini    &#13;
 preliminari presso  il  Tribunale  di  Ancona  con  le  ordinanze  in    &#13;
 epigrafe, iscritte nel R.O. nn. 90 e 143/1991;                           &#13;
    Dichiara   la   manifesta   inammissibilità  della  questione  di    &#13;
 legittimità costituzionale dell'art. 3, n. 1, del  nuovo  codice  di    &#13;
 procedura  penale,  sollevata,  in riferimento agli artt. 2 e 3 della    &#13;
 Costituzione, dal Giudice sopra nominato con l'ordinanza in epigrafe,    &#13;
 iscritta nel R.O. n. 105/1991.                                           &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 20 maggio 1991.                               &#13;
                       Il Presidente: CORASANITI                          &#13;
                         Il redattore: MENGONI                            &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 24 maggio 1991.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
