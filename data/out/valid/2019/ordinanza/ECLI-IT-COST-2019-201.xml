<akomaNtoso xmlns:akn4cc="http://cortecostituzionale.it/metadata" xmlns:akn4coa="http://corteconti.it/akn4coa" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <judgment name="ordinanza">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-07-24/201/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-07-24/201"/>
          <FRBRalias value="ECLI:IT:COST:2019:201" name="ECLI"/>
          <FRBRdate date="2019-07-24" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#author"/>
          <FRBRcountry value="it"/>
          <FRBRnumber value="201"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-07-24/201/ita@/!main"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-07-24/201/ita@"/>
          <FRBRdate date="2019-07-24" name="Date"/>
          <FRBRauthor href="#corteCostituzionale" as="#editor"/>
          <FRBRlanguage language="ita"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-07-24/201/ita@/!main.xml"/>
          <FRBRuri value="/akn/it/judgment/ordinanza/corteCostituzionale/2019-07-24/201/ita@.xml"/>
          <FRBRdate date="2019-07-24" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <workflow source="#corteCostituzionale">
        <step date="2019-07-03" by="corteCostituzionale" refersTo="#decisionEvent"/>
        <step date="2019-07-24" by="corteCostituzionale" refersTo="#publicationEvent"/>
      </workflow>
      <analysis source="#cirsfidUnibo">
        <otherAnalysis source="#corteCostituzionale">
          <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
          <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        </otherAnalysis>
      </analysis>
      <references source="#cirsfidUnibo">
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="corteCostituzionale" href="/akn/ontology/organizations/it/corteCostituzionale" showAs="Corte Costituzionale"/>
        <TLCPerson eId="LATTANZI" href="/akn/ontology/person/it/LATTANZI" showAs="LATTANZI"/>
        <TLCPerson eId="martaCartabia" href="/akn/ontology/person/it/martaCartabia" showAs="Marta CARTABIA"/>
        <TLCPerson eId="marioRosarioMorelli" href="/akn/ontology/person/it/marioRosarioMorelli" showAs="Mario Rosario MORELLI"/>
        <TLCPerson eId="giancarloCoraggio" href="/akn/ontology/person/it/giancarloCoraggio" showAs="Giancarlo CORAGGIO"/>
        <TLCPerson eId="giulianoAmato" href="/akn/ontology/person/it/giulianoAmato" showAs="Giuliano AMATO"/>
        <TLCPerson eId="silvanaSciarra" href="/akn/ontology/person/it/silvanaSciarra" showAs="Silvana SCIARRA"/>
        <TLCPerson eId="dariaDePretis" href="/akn/ontology/person/it/dariaDePretis" showAs="Daria de PRETIS"/>
        <TLCPerson eId="nicolòZanon" href="/akn/ontology/person/it/nicolòZanon" showAs="Nicolò ZANON"/>
        <TLCPerson eId="francoModugno" href="/akn/ontology/person/it/francoModugno" showAs="Franco MODUGNO"/>
        <TLCPerson eId="augustoAntonioBarbera" href="/akn/ontology/person/it/augustoAntonioBarbera" showAs="Augusto Antonio BARBERA"/>
        <TLCPerson eId="giulioProsperetti" href="/akn/ontology/person/it/giulioProsperetti" showAs="Giulio PROSPERETTI"/>
        <TLCPerson eId="giovanniAmoroso" href="/akn/ontology/person/it/giovanniAmoroso" showAs="Giovanni AMOROSO"/>
        <TLCPerson eId="lucaAntonini" href="/akn/ontology/person/it/lucaAntonini" showAs="Luca ANTONINI"/>
        <TLCPerson eId="giorgioLattanzi" href="/akn/ontology/person/it/giorgioLattanzi" showAs="Giorgio LATTANZI"/>
        <TLCPerson eId="aldoCarosi" href="/akn/ontology/person/it/aldoCarosi" showAs="Aldo CAROSI"/>
        <TLCPerson eId="filomenaPerrone" href="/akn/ontology/person/it/filomenaPerrone" showAs="Filomena PERRONE"/>
        <TLCRole eId="editor" href="/akn/ontology/role/it/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/role/it/author" showAs="Author of the Document"/>
        <TLCRole eId="presidenteCollegio" href="/akn/ontology/role/it/presidenteCollegio" showAs="Presidente del Collegio"/>
        <TLCRole eId="relatore" href="/akn/ontology/role/it/relatore" showAs="Relatore"/>
        <TLCRole eId="cancelliere" href="/akn/ontology/role/it/relatore" showAs="Cancelliere"/>
      </references>
      <proprietary source="#cirsfidUnibo">
        <akn4cc:anno>2019</akn4cc:anno>
        <akn4cc:numero>201</akn4cc:numero>
        <akn4cc:ecli>ECLI:IT:COST:2019:201</akn4cc:ecli>
        <akn4cc:tipo>O</akn4cc:tipo>
        <akn4cc:presidente>LATTANZI</akn4cc:presidente>
        <akn4cc:relatore>Aldo Carosi</akn4cc:relatore>
        <akn4cc:data_decisione>2019-07-03</akn4cc:data_decisione>
        <akn4cc:data_deposito>2019-07-24</akn4cc:data_deposito>
        <akn4cc:tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</akn4cc:tipo_procedimento>
        <akn4cc:dispositivo>estinzione del processo</akn4cc:dispositivo>
        <akn4cc:parser_version>0.0.4</akn4cc:parser_version>
      </proprietary>
    </meta>
    <header>
      <block name="collegio"><courtType refersTo="#corteCostituzionale">LA CORTE COSTITUZIONALE</courtType>
composta dai signori:
 Presidente: <judge refersTo="#giorgioLattanzi">Giorgio LATTANZI</judge>; Giudici : <judge refersTo="#aldoCarosi">Aldo CAROSI</judge>, <judge refersTo="#martaCartabia">Marta CARTABIA</judge>, <judge refersTo="#marioRosarioMorelli">Mario Rosario MORELLI</judge>, <judge refersTo="#giancarloCoraggio">Giancarlo CORAGGIO</judge>, <judge refersTo="#giulianoAmato">Giuliano AMATO</judge>, <judge refersTo="#silvanaSciarra">Silvana SCIARRA</judge>, <judge refersTo="#dariaDePretis">Daria de PRETIS</judge>, <judge refersTo="#nicolòZanon">Nicolò ZANON</judge>, <judge refersTo="#francoModugno">Franco MODUGNO</judge>, <judge refersTo="#augustoAntonioBarbera">Augusto Antonio BARBERA</judge>, <judge refersTo="#giulioProsperetti">Giulio PROSPERETTI</judge>, <judge refersTo="#giovanniAmoroso">Giovanni AMOROSO</judge>, <judge refersTo="#lucaAntonini">Luca ANTONINI</judge>,</block>
    </header>
    <judgmentBody>
      <introduction>
        <p>ha pronunciato la seguente</p>
        <p>
          <docType>ORDINANZA</docType>
        </p>
        <paragraph>
          <content>
            <p>nel giudizio di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/2017-12-27/205/!main#art_1__para_841">art. 1, comma 841, della legge 27 dicembre 2017, n. 205</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2018 e bilancio pluriennale per il triennio 2018-2020), promosso dalla Regione autonoma Valle d'Aosta/Vallée d'Aoste con ricorso spedito per la notifica il 26 febbraio 2018, depositato in cancelleria il 6 marzo 2018, iscritto al n. 23 del registro ricorsi 2018 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 17, prima serie speciale, dell'anno 2018.
 Visto l'atto di <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">costituzione</ref> del Presidente del Consiglio dei ministri;
 udito nella camera di consiglio del 22 maggio 2019 il Giudice relatore Aldo Carosi.
 Ritenuto che, con il ricorso indicato in epigrafe, la Regione autonoma Valle d'Aosta/Vallée d'Aoste ha promosso questioni di legittimità costituzionale dell'<ref href="/akn/it/act/legge/stato/2017-12-27/205/!main#art_1__para_841">art. 1, comma 841, della legge 27 dicembre 2017, n. 205</ref> (Bilancio di previsione dello Stato per l'anno finanziario 2018 e bilancio pluriennale per il triennio 2018-2020), in riferimento agli artt. 2, lettera a), 3, lettera f), 4, primo comma, 12, primo comma, 48-bis e 50 della legge costituzionale 26 febbraio 1948, n. 4 (Statuto speciale per la Valle d'Aosta), in relazione agli articoli da 2 a 7 della <ref href="/akn/it/act/legge/stato/1981-11-26/690/!main">legge 26 novembre 1981, n. 690</ref> (Revisione dell'ordinamento finanziario della regione Valle d'Aosta), nonché in riferimento agli artt. 117, terzo comma, e 119 della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref> e 10 della legge costituzionale 18 ottobre 2001, n. 3 (Modifiche al titolo V della parte seconda della <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main">Costituzione</ref>), e ai principi di ragionevolezza, di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, e di leale collaborazione, di cui agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_5">artt. 5</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_120">120 Cost.</ref></mref>;
 che la disposizione impugnata prevede che, «[n]elle more della definizione dei complessivi rapporti finanziari fra lo Stato e la regione Valle d'Aosta che tenga conto, tra l'altro, delle sentenze della <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/77/!main">Corte costituzionale n. 77 del 2015</ref> e n. 154 del 2017, gli accantonamenti a carico della regione Valle d'Aosta a titolo di concorso alla finanza pubblica sono ridotti di 45 milioni di euro per l'anno 2018, 100 milioni di euro per l'anno 2019 e 120 milioni di euro annui a decorrere dall'anno 2020»;
 che la Regione autonoma ha proposto l'impugnazione in via cautelativa, per l'ipotesi in cui la disposizione venga intesa nel senso che l'accantonamento da ridurre includa anche quello di cui all'<ref href="/akn/it/act/decretoLegge/stato/2012-07-06/95/!main#art_16__para_3">art. 16, comma 3, del decreto-legge 6 luglio 2012, n. 95</ref> (Disposizioni urgenti per la revisione della spesa pubblica con invarianza dei servizi ai cittadini nonché misure di rafforzamento patrimoniale delle imprese del settore bancario), convertito, con modificazioni, nella <ref href="/akn/it/act/legge/stato/2012-08-07/135/!main">legge 7 agosto 2012, n. 135</ref>, stabilendo l'applicabilità del concorso per gli anni 2018, 2019 e, successivamente, a decorrere dall'anno 2020;
 che, ad avviso della ricorrente, reiterando e stabilizzando il meccanismo dell'accantonamento con riferimento a un contributo avulso dal contesto in cui era previsto - connotato dall'applicabilità del patto di stabilità - e non più dovuto, la norma inciderebbe unilateralmente sulle compartecipazioni erariali spettanti alla Regione, in violazione degli artt. 48-bis e 50 dello statuto reg. Valle d'Aosta in relazione agli articoli da 2 a 7 della <ref href="/akn/it/act/legge/stato/1981/690/!main">legge n. 690 del 1981</ref>, modificabili o derogabili solo secondo le procedure statutariamente previste;
 che la disposizione contrasterebbe altresì con il principio di leale collaborazione di cui agli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_5">artt. 5</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_120">120 Cost.</ref></mref>, principio cardine nei rapporti finanziari tra Stato e autonomie speciali, nella fattispecie disatteso dall'unilaterale previsione del contributo;
 che tali violazioni, inoltre, si tradurrebbero inevitabilmente nella lesione dell'autonomia organizzativa e finanziaria regionale, presidiata dagli artt. 2, lettera a), 3, lettera f), 4, primo comma, e 12, primo comma, dello statuto reg. Valle d'Aosta e dagli artt. 117, terzo comma, e 119 Cost. e 10 della legge cost. n. 3 del 2001;
 che risulterebbe altresì violato il principio di ragionevolezza di cui all'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_3">art. 3 Cost.</ref>, violazione che ridonderebbe sulla menzionata autonomia organizzativa e finanziaria della Regione, atteso che il ripristino del meccanismo dell'accantonamento di cui all'<ref href="/akn/it/act/decretoLegge/stato/2012/95/!main#art_16__para_3">art. 16, comma 3, del d.l. n. 95 del 2012</ref> lo stabilizzerebbe nel tempo, nonostante fosse destinato a operare solo fino all'adozione delle norme di attuazione di cui all'<ref href="/akn/it/act/legge/stato/2009-05-05/42/!main#art_27">art. 27 della legge 5 maggio 2009, n. 42</ref> (Delega al Governo in materia di federalismo fiscale, in attuazione dell'<ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_119">articolo 119 della Costituzione</ref>);
 che, infine, la norma censurata contrasterebbe con gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_136">artt. 136</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_137">137 Cost.</ref></mref>, contraddicendo le indicazioni cui la <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/77/!main">sentenza n. 77 del 2015</ref> di questa Corte aveva subordinato la legittimità del contributo e del meccanismo di accantonamento - implicante la loro temporaneità - e, quindi, il giudicato formatosi sul punto;
 che si è costituito in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, deducendo l'infondatezza del ricorso;
 che il resistente evidenzia preliminarmente come l'accantonamento previsto dall'<ref href="/akn/it/act/decretoLegge/stato/2012/95/!main#art_16__para_3">art. 16, comma 3, del d.l. n. 95 del 2012</ref> sia applicabile anche agli anni 2017 e 2018;
 che, poiché a decorrere dal 2019 il concorso alla finanza pubblica in questione, proprio alla stregua della citata <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/77/!main">sentenza n. 77 del 2015</ref> e dell'interpretazione ivi condivisa, non sarebbe più dovuto, l'<ref href="/akn/it/act/legge/stato/2017/205/!main#art_1__para_841">art. 1, comma 841, della legge n. 205 del 2017</ref>, nella prospettiva di una rimodulazione dei rapporti finanziari con le autonomie speciali e nell'esercizio del potere dello Stato di imporre loro il concorso alla finanza pubblica quale misura di coordinamento, avrebbe provveduto a una provvisoria riduzione per gli anni a venire;
 che risulterebbero così contemperate, da un lato, le esigenze di copertura finanziaria cui il precedente concorso alla finanza pubblica delle autonomie speciali sopperiva, l'ossequio ai vincoli che la astringono e il coordinamento spettante allo Stato e, dall'altro, il rispetto del principio di leale collaborazione e di autonomia finanziaria degli enti a statuto speciale;
 che la norma non contrasterebbe con il principio consensualistico, in quanto detto vincolo di metodo, funzionale a conciliare gli equilibri di finanza pubblica con l'autonomia speciale, sarebbe immanente nella previsione della riduzione degli accantonamenti nelle more di una ridefinizione dei complessivi rapporti finanziari tra Stato e Regione, nella consapevolezza della necessità di una successiva fase dialogica funzionale a una loro negoziazione;
 che gli accantonamenti al netto della riduzione prevista non violerebbero le norme dell'ordinamento finanziario regionale sulle compartecipazioni ai tributi erariali né, conseguentemente, l'autonomia organizzativa e finanziaria della ricorrente, in quanto sarebbero funzionali al raggiungimento degli obiettivi di risanamento della finanza pubblica, cui anche le autonomie speciali sono chiamate, senza che da ciò possa esonerarle il passaggio dal regime del patto di stabilità a quello del pareggio di bilancio;
 che, infine, non vi sarebbe contrasto con gli <mref><ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_136">artt. 136</ref> e <ref href="/akn/it/act/costituzione/stato/1947-12-22/cost/!main#art_137">137 Cost.</ref></mref>, per violazione del giudicato costituzionale formatosi in relazione alla menzionata <ref href="/akn/it/judgment/sentenza/corteCostituzionale/2015/77/!main">sentenza n. 77 del 2015</ref> - cui, peraltro, la norma censurata intenderebbe prestare ossequio - atteso che essa stessa ammetterebbe l'imposizione di misure di contenimento della spesa nell'esercizio della funzione di coordinamento della finanza pubblica.
 Considerato che, con riguardo al ricorso indicato in epigrafe, vi è stata rinuncia da parte della Regione autonoma Valle d'Aosta/Vallée d'Aoste, depositata il 28 gennaio 2019, previa conforme deliberazione della Giunta regionale;
 che la rinuncia è stata accettata dal Presidente del Consiglio dei ministri, con atto depositato il 26 febbraio 2019, su conforme deliberazione del Consiglio dei ministri in data 14 febbraio 2019;
 che la rinuncia al ricorso in via principale accettata dalla controparte costituita determina, ai sensi dell'art. 23 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.
 Visti gli artt. 26, secondo comma, della <ref href="/akn/it/act/legge/stato/1953-03-11/87/!main">legge 11 marzo 1953, n. 87</ref>, e 9, comma 2, delle Norme integrative per i giudizi davanti alla Corte costituzionale.</p>
          </content>
        </paragraph>
      </introduction>
      <decision>
        <block name="formulaRito">per questi motivi
 <docAuthority>LA CORTE COSTITUZIONALE</docAuthority>
 dichiara estinto il processo.
 </block>
      </decision>
    </judgmentBody>
    <conclusions>
      <block name="formulaFinale">Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il <date refersTo="#decisionDate" date="2019-07-03">3 luglio 2019</date>.
 F.to:
 <signature><judge refersTo="#giorgioLattanzi" as="#presidente">Giorgio LATTANZI</judge>, <role refersTo="#presidente">Presidente</role></signature>
 <signature><judge refersTo="#aldoCarosi" as="#relatore">Aldo CAROSI</judge>, <role refersTo="#relatore">Redattore</role></signature>
 <signature><person refersTo="#filomenaPerrone" as="#cancelliere">Filomena PERRONE</person>, <role refersTo="#cancelliere">Cancelliere</role></signature>
 Depositata in Cancelleria il <date refersTo="#publicationDate" date="2019-07-24">24 luglio 2019</date>.
 Il Cancelliere
 F.to: Filomena PERRONE</block>
    </conclusions>
  </judgment>
</akomaNtoso>
