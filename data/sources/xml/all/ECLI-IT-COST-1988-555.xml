<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>555</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:555</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/05/1988</data_decisione>
    <data_deposito>19/05/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, Avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità costituzionale degli artt. 4, 5 e 21,    &#13;
 commi primo e quarto,  della  legge  26  marzo  1986  n.  86  recante    &#13;
 "Ristrutturazione   dei   ruoli   dell'A.N.A.S.  e  decentramento  di    &#13;
 competenze",  promosso  con  ricorso  della  Provincia  autonoma   di    &#13;
 Bolzano, notificato il 30 aprile 1986, depositato in Cancelleria il 7    &#13;
 maggio successivo ed iscritto al n. 16 del registro ricorsi 1986;        &#13;
    Visto  l'atto  di  costituzione  del  Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nell'udienza pubblica del 22 marzo 1988 il Giudice relatore    &#13;
 Francesco Saja;                                                          &#13;
    Uditi  gli  avvocati Sergio Panunzio e Roland Riz per la Provincia    &#13;
 autonoma di Bolzano e l'Avvocato dello Stato  Franco  Favara  per  il    &#13;
 Presidente del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ricorso  notificato  il 30 aprile 1986 la Provincia di    &#13;
 Bolzano ha impugnato la legge 26  marzo  1986  n.  86  relativa  alla    &#13;
 "Ristrutturazione   dell'Anas   (Azienda   Nazionale  Autonoma  delle    &#13;
 Strade)". Deduce la ricorrente che gli artt. 4, 5 e  21  della  legge    &#13;
 impugnata  violano  l'art. 10 Cost. e la disciplina statutaria (artt.    &#13;
 8, 11, 12 comma primo, 13 comma primo e  quinto,  15,  20  d.P.R.  26    &#13;
 luglio  1976 n. 752, 89 comma primo e quinto, 100 e 107 dello Statuto    &#13;
 speciale per il Trentino-Alto Adige, approvato con d.P.R.  31  agosto    &#13;
 1972  n.  670),  sia nella parte in cui prevedono che i compartimenti    &#13;
 dell'Anas abbiano sede nei capoluoghi di regione  -  con  conseguente    &#13;
 trasferimento  di  quello  di  Bolzano  a  Trento - sia in quella che    &#13;
 istituisce nuovi  ruoli  di  tecnici  (geologi  e  architetti)  senza    &#13;
 assoggettare  i relativi concorsi di assunzione ai principi di tutela    &#13;
 delle minoranze linguistiche.                                            &#13;
    2.  -  Si  è costituito il Presidente del Consiglio dei ministri,    &#13;
 deducendo la non fondatezza del ricorso e chiedendone il rigetto.<diritto>Considerato in diritto</diritto>1.  -  Con  la prima censura la Provincia ricorrente deduce che le    &#13;
 disposizioni di  cui  agli  artt.  4  e  21  della  legge  impugnata,    &#13;
 disponendo  il  trasferimento  del  compartimento  Anas  a  Trento da    &#13;
 Bolzano  e  l'istituzione  in  quest'ultima  città  di  una  sezione    &#13;
 distaccata,  vanificherebbe  l'autonomia  provinciale  e lederebbe il    &#13;
 principio di tutela delle minoranze linguistiche, dettato dalle norme    &#13;
 statutarie  e  di  attuazione  per  il personale degli uffici statali    &#13;
 nella Provincia medesima.                                                &#13;
    La questione, proposta peraltro in maniera non del tutto lineare e    &#13;
 coerente, risulta infondata.                                             &#13;
    Al   riguardo  va  premesso  che  alla  Provincia  di  Bolzano  è    &#13;
 attribuita ex art. 8 n. 17 cit. d.P.R. n. 670 del  1972  soltanto  la    &#13;
 competenza  in  materia  di viabilità di interesse provinciale e che    &#13;
 rientra,  invece,  nelle  attribuzioni  dello  Stato  la   disciplina    &#13;
 concernente l'organizzazione dei servizi relativi alle strade statali    &#13;
 e dei  connessi  apparati.  Pertanto  deve  ritenersi  che  la  legge    &#13;
 impugnata  si  è  correttamente  adeguata  alla  ripartizione  delle    &#13;
 competenze stabilite dallo Statuto, e che lo Stato ha  esercitato  un    &#13;
 proprio  potere  riorganizzando  l'azienda competente a costruire e a    &#13;
 gestire l'uso delle strade statali, localizzando,  tra  l'altro,  per    &#13;
 l'intero   territorio   nazionale   la  sede  dei  compartimenti  nei    &#13;
 capoluoghi di regione.                                                   &#13;
    Da   ciò  discende  chiaramente  l'infondatezza  della  questione    &#13;
 sollevata, con la quale  si  è  addirittura  invocato  l'Accordo  De    &#13;
 Gasperi-Gruber,  quasi  che  il  semplice  spostamento della sede del    &#13;
 compartimento   importi    un    attentato    all'esistenza    stessa    &#13;
 dell'autonomia provinciale.                                              &#13;
    Naturalmente   rimangono   immutate  le  garanzie  spettanti  alla    &#13;
 Provincia altoatesina sia  per  quanto  concerne  la  stabilità  del    &#13;
 personale  in  servizio  nei ruoli locali, che ai sensi dell'art. 89,    &#13;
 commi quinto e sesto, dello Statuto può essere trasferito  solo  per    &#13;
 esigenze  di  servizio, sia per la percentuale dello stesso personale    &#13;
 di lingua tedesca, comunque  destinabile  ad  altra  sede:  eventuali    &#13;
 provvedimenti  della  pubblica  amministrazione  che  ledessero  tali    &#13;
 garanzie sarebbero certamente viziati e gli interessati  troverebbero    &#13;
 tutela davanti alla giurisdizione amministrativa.                        &#13;
    Anche  sotto  tale  profilo,  dunque,  la questione deve ritenersi    &#13;
 infondata.                                                               &#13;
 2.  La  seconda  censura concerne l'istituzione nella pianta organica    &#13;
 dell'Anas di diciannove posti di geologo e nove  di  architetto,  per    &#13;
 l'assunzione  dei quali non sarebbe stato recepito, nell'art. 5 della    &#13;
 legge impugnata, il principio del bilinguismo.                           &#13;
    Questa   seconda   censura,   nei  limiti  appresso  indicati,  va    &#13;
 condivisa.                                                               &#13;
    Lo  Stato  ha  eccepito  che per le caratteristiche geologiche del    &#13;
 territorio della Provincia di Bolzano il nuovo personale non dovrebbe    &#13;
 trovare   impiego  nell'ambito  della  Provincia  medesima  e  quindi    &#13;
 correttamente non si sarebbe fatto riferimento ai principi  invocati.    &#13;
    Tale  assunto  non  può  essere evidentemente accolto in quanto i    &#13;
 geologi e gli architetti, pur dovendo essere destinati alla Direzione    &#13;
 generale  del  Ministero  ed  ai diversi compartimenti, hanno compiti    &#13;
 estesi all'intero ambito regionale con la conseguenza,  attinente  al    &#13;
 caso  di  specie,  che  quelli  destinati  al compartimento di Trento    &#13;
 debbono  occuparsi,  se  necessario,  anche  del   territorio   della    &#13;
 Provincia  di  Bolzano. Conseguentemente, con riguardo al contingente    &#13;
 di cui all' art. 1, secondo comma, d.P.R. n. 752/1976 come modificato    &#13;
 dall'art.  1 d.P.R. n. 327/1982, avrebbe dovuto trovare attuazione la    &#13;
 disposizione relativa alla conoscenza delle due lingue.                  &#13;
    Per  contro  la  norma impugnata nulla dispone in proposito, onde,    &#13;
 limitatamente al ricordato contingente,  essa  non  si  sottrae  alla    &#13;
 dichiarazione di incostituzionalità.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara  non  fondata,  nei  sensi  di  cui  in  motivazione,  la    &#13;
 questione di legittimità costituzionale degli artt. 4  e  21,  commi    &#13;
 primo  e  quarto,  della  legge  26  marzo 1986 n. 86, sollevata - in    &#13;
 riferimento all'art. 10 Cost., 89, commi primo  e  quinto,  100,  107    &#13;
 dello  Statuto  speciale  per  il  Trentino-Alto Adige, approvato con    &#13;
 d.P.R. 31 agosto 1972 n. 670 - dalla Provincia autonoma  di  Bolzano,    &#13;
 con il ricorso indicato in epigrafe;                                     &#13;
    Dichiara  la illegittimità costituzionale dell'art. 5 l. 26 marzo    &#13;
 1986 n.  86,  nella  parte  in  cui  non  osserva  il  principio  del    &#13;
 bilinguismo  relativamente  al personale del compartimento di Trento,    &#13;
 destinato al contingente con  competenza  anche  sulla  Provincia  di    &#13;
 Bolzano.                                                                 &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 maggio 1988.                                &#13;
                    Il Presidente e redattore: SAJA                       &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 maggio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
