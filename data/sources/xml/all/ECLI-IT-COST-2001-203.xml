<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>203</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:203</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Massimo Vari</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/06/2001</data_decisione>
    <data_deposito>22/06/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 3-octies comma &#13;
5,  del  decreto legislativo 30 dicembre 1992, n. 502 (Riordino della &#13;
disciplina  in  materia  sanitaria,  a  norma dell'art. 1 della legge &#13;
23 ottobre  1992,  n. 421),  nel  testo  introdotto  dall'art. 3  del &#13;
decreto  legislativo 13 luglio (recte: 19 giugno) 1999, n. 229 (Norme &#13;
per  la  razionalizzazione  del Servizio sanitario nazionale, a norma &#13;
dell'art. 1  della  legge  30 novembre  1998, n. 419), e dell'art. 1, &#13;
comma   2,   seconda  parte,  della  legge  26 febbraio  1999,  n. 42 &#13;
(Disposizioni  in  materia  di  professioni  sanitarie), promosso con &#13;
ordinanza  emessa il 21 settembre 2000 dalla Corte dei conti, sezione &#13;
del  controllo,  iscritta  al  n. 808  del  registro ordinanze 2000 e &#13;
pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 1, prima &#13;
serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
Ministri; &#13;
    Udito  nella  camera  di  consiglio del 26 aprile 2001 il giudice &#13;
relatore Massimo Vari. &#13;
    Ritenuto  che,  con  ordinanza del 21 settembre 2000 (r.o. n. 808 &#13;
del  2000),  la  Corte dei conti, sezione del controllo, ha sollevato &#13;
questione di legittimità costituzionale: &#13;
    a)   dell'art. 3-octies   comma   5,   del   decreto  legislativo &#13;
30 dicembre  1992,  n. 502  (Riordino  della  disciplina  in  materia &#13;
sanitaria,  a norma dell'art. 1 della legge 23 ottobre 1992, n. 421), &#13;
nel  testo  introdotto  dall'art. 3 del decreto legislativo 13 luglio &#13;
(recte:  19 giugno)  1999, n. 229 (Norme per la razionalizzazione del &#13;
Servizio   sanitario  nazionale,  a  norma  dell'art. 1  della  legge &#13;
30 novembre 1998, n. 419), per contrasto con gli artt. 117 e 76 della &#13;
Costituzione  e  con  le  corrispondenti norme degli statuti speciali &#13;
delle  regioni,  "nella  parte  in  cui  attribuisce  la  potestà di &#13;
definire   gli   ordinamenti  didattici  delle  figure  professionali &#13;
operanti  nell'area  socio-sanitaria  ad un regolamento del Ministero &#13;
della  sanità  di  concerto  con  il  Ministero  per la solidarietà &#13;
sociale"; &#13;
    b)  dell'art. 1,  comma 2, seconda parte, della legge 26 febbraio &#13;
1999,  n. 42  (Disposizioni in materia di professioni sanitarie), per &#13;
contrasto con l'art. 117 della Costituzione; &#13;
        che,  ad  avviso  del  rimettente,  la  normativa  di  delega &#13;
contenuta  negli  artt. 1  e  2  della legge 30 novembre 1998, n. 419 &#13;
(Delega  al  Governo  per la razionalizzazione del Servizio sanitario &#13;
nazionale   e  per  l'adozione  di  un  testo  unico  in  materia  di &#13;
organizzazione  e  funzionamento  del  Servizio  sanitario nazionale. &#13;
Modifiche  al decreto legislativo 30 dicembre 1992, n. 502), consente &#13;
che  solo  l'individuazione  dei profili professionali di livello non &#13;
dirigenziale  operanti nell'area delle prestazioni socio-sanitarie ad &#13;
elevata  integrazione  sanitaria  avvenga  mediante  l'adozione di un &#13;
regolamento  del  Ministro della sanità, di concerto con il Ministro &#13;
dell'università  e  della  ricerca  scientifica  e  tecnologica e di &#13;
quello  per  la  solidarietà  sociale,  mentre  la  definizione  dei &#13;
relativi  ordinamenti  didattici spetta agli atenei, sulla base di un &#13;
decreto  del  Ministro  dell'università, emanato di concerto con gli &#13;
altri ministri interessati; &#13;
        che,  pertanto,  ne  deriva  il  contrasto dell'art. 3-octies &#13;
comma  5, del decreto legislativo n. 502 del 1992 con l'art. 76 della &#13;
Costituzione,  per  il  difetto, nella legge delega, di una norma che &#13;
attribuisca "all'autorità amministrativa il potere di intervenire in &#13;
materia con un proprio regolamento"; &#13;
        che,   ad   avviso   della   Corte  dei  conti,  la  medesima &#13;
disposizione  collide,  inoltre,  con l'art. 117 della Costituzione e &#13;
con  gli statuti speciali delle regioni in quanto prevede l'adozione, &#13;
da  parte  del  Ministro  della  sanità, e quindi dello Stato, di un &#13;
regolamento  in  "materia  che  la disciplina sul trasferimento delle &#13;
funzioni riserva alla competenza regionale"; &#13;
        che,  quanto  all'art. 1, comma 2, seconda parte, della legge &#13;
26 febbraio  1992,  n. 42,  che  detta  disposizioni  in  materia  di &#13;
professioni     sanitarie    invocato    dall'amministrazione    "nel &#13;
controdedurre    in    ordine    alla    legittimità   del   decreto &#13;
interministeriale   in   esame   ed   alla   ritenuta  rispondenza  a &#13;
Costituzione  delle  norme di cui esso risulta applicazione" la Corte &#13;
dei   conti   ne   sostiene   l'incostituzionalità   per  violazione &#13;
dell'art. 117  della  Costituzione,  se  interpretato  nel  senso  di &#13;
attribuire  a  decreti  ministeriali  la disciplina degli ordinamenti &#13;
didattici   in  materia  di  "formazione  post-base",  di  competenza &#13;
regionale; &#13;
        che  è intervenuto il Presidente del Consiglio dei ministri, &#13;
rappresentato e difeso dall'Avvocatura generale dello Stato, il quale &#13;
ha  concluso  chiedendo  che  la Corte disponga la restituzione degli &#13;
atti  alla  Corte  dei  conti,  alla  stregua  dello ius superveniens &#13;
costituito   dall'art. 9   della  legge  24 ottobre  2000,  n. 323  e &#13;
dall'art. 12  della  legge  8 novembre 2000, n. 328, disposizioni che &#13;
dimostrerebbero,  ad  avviso della parte pubblica, che il legislatore &#13;
ordinario  "ha  risolto i dubbi sulla legittimità costituzionale del &#13;
comma 5 dell'art. 3-octies". &#13;
    Considerato  che,  in  effetti,  successivamente all'ordinanza in &#13;
epigrafe, è intervenuto l'art. 9 della legge 24 ottobre 2000, n. 323 &#13;
(Riordino   del  settore  termale),  che,  nell'istituire  la  figura &#13;
dell'operatore   termale,   ha   previsto  che  il  relativo  profilo &#13;
professionale    sia    disciplinato    ai    sensi   del   comma   5 &#13;
dell'art. 3-octies   del   decreto   legislativo   n. 502  del  1992, &#13;
introdotto dall'art. 3 del decreto legislativo n. 229 del 1999; &#13;
        che,  inoltre,  l'art. 12 della legge 8 novembre 2000, n. 328 &#13;
(Legge   quadro   per  la  realizzazione  del  sistema  integrato  di &#13;
interventi  e  servizi sociali), nel disciplinare la formazione delle &#13;
"figure  professionali  sociali",  ha stabilito che "restano ferme le &#13;
disposizioni   di   cui  all'art. 3-octies  del  decreto  legislativo &#13;
30 dicembre   1992,   n. 502,   introdotto  dall'art. 3  del  decreto &#13;
legislativo 19 giugno 1999, n. 229, relative ai profili professionali &#13;
dell'area sociosanitaria ad elevata integrazione socio-sanitaria"; &#13;
        che,  a  seguito  delle accennate innovazioni legislative, si &#13;
rende   necessario,   in  via  del  tutto  preliminare,  disporre  la &#13;
restituzione degli atti alla Corte dei conti per un nuovo esame della &#13;
questione.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Ordina  la  restituzione degli atti alla Corte dei conti, sezione &#13;
del controllo. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 4 giugno 2001. &#13;
                       Il Presidente: RUPERTO &#13;
                         Il redattore: VARI &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 22 giugno 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
