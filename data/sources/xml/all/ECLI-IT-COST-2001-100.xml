<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>100</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:100</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Giovanni Maria Flick</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/03/2001</data_decisione>
    <data_deposito>04/04/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI &#13;
MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, &#13;
Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio di legittimità costituzionale dell'art. 4-ter commi 2, &#13;
3,  lettera  b),  4,  5,  6 e 7 del decreto-legge 7 aprile 2000, n.82 &#13;
(Modificazioni  alla  disciplina  dei  termini  di custodia cautelare &#13;
nella  fase  del giudizio abbreviato), convertito, con modificazioni, &#13;
in  legge  5 giugno  2000,  n.144,  promosso  con ordinanza emessa il &#13;
5 luglio  2000  dalla Corte di assise di appello di Caltanissetta nel &#13;
procedimento  penale  a  carico di S. R. ed altri, iscritta al n. 612 &#13;
del  registro  ordinanze  2000  e pubblicata nella Gazzetta Ufficiale &#13;
della Repubblica n. 44, 1ª serie speciale, dell'anno 2000. &#13;
    Udito  nella  camera  di consiglio del 7 febbraio 2001 il giudice &#13;
relatore Giovanni Maria Flick. &#13;
    Ritenuto  che, con ordinanza emessa il 5 luglio 2000, la Corte di &#13;
assise   di  appello  di  Caltanissetta  ha  sollevato  questione  di &#13;
legittimità   costituzionale   dell'art. 4-ter   commi   2,   3  con &#13;
riferimento  alla  lettera  b) 4, 5, 6 e 7 del decreto-legge 7 aprile &#13;
2000, n.82, convertito, con modificazioni, nella legge 5 giugno 2000, &#13;
n. 144,  per contrasto con gli artt. 3, primo comma; 27, terzo comma; &#13;
97, primo comma; 101, secondo comma; 102, primo comma, e 111, secondo &#13;
comma, ultima parte, della Costituzione; &#13;
        che  il giudice a quo premette che, nel corso del giudizio in &#13;
grado  di  appello  nei confronti di tre imputati condannati in primo &#13;
grado  alla  pena  dell'ergastolo  per  il  reato di duplice omicidio &#13;
aggravato  in  concorso,  era stata disposta la parziale rinnovazione &#13;
dell'istruttoria dibattimentale, con assunzione in contraddittorio di &#13;
nuove prove; &#13;
        che,  all'udienza  nella  quale era previsto il completamento &#13;
dell'istruttoria  stessa con il passaggio alla fase della discussione &#13;
finale, tutti gli imputati avevano chiesto di essere giudicati con il &#13;
rito  abbreviato,  in  applicazione  delle disposizioni contenute nei &#13;
commi  2,  3  lettera  b) 4, 5, 6 e 7 prima parte dell'art. 4-ter del &#13;
decreto-legge  7 aprile  2000,  n.82,  convertito, con modificazioni, &#13;
nella legge 5 giugno 2000, n. 144; &#13;
        che   l'ammissibilità  di  tale  richiesta  -  da  ritenersi &#13;
ritualmente  avanzata,  ricorrendo tutti i presupposti previsti dalla &#13;
norma  citata - portava il rimettente a dubitare della compatibilità &#13;
costituzionale  del citatoart. 4-ter del decreto-legge 7 aprile 2000, &#13;
n. 82,  convertito,  con  modificazioni,  nella  legge 5 giugno 2000, &#13;
n. 144,  in  relazione a molteplici parametri, primo tra tutti quello &#13;
dell'art. 3 Cost; &#13;
        che,  invero,  la  norma  censurata  -  giustificata  con  la &#13;
generale  finalità  di consentire all'imputato, che non abbia potuto &#13;
accedere al giudizio abbreviato ordinario per l'avvenuta scadenza del &#13;
termine  di  proposizione  della  richiesta, di usufruire comunque di &#13;
tale  rito  speciale  -  sarebbe  fonte di disparità di trattamento: &#13;
infatti,  per gli imputati in procedimenti riguardanti reati punibili &#13;
con  pene  diverse  da  quelle  dell'ergastolo,  il  limite temporale &#13;
concesso  per  la proposizione della richiesta di giudizio abbreviato &#13;
è  quello  dell'inizio  dell'istruttoria  nel  dibattimento di primo &#13;
grado  (art. 4-ter  comma  1);  invece,  per  gli  imputati  di reati &#13;
punibili  con l'ergastolo la richiesta può essere avanzata fino alla &#13;
conclusione  dell'istruttoria  dibattimentale  nel  giudizio di primo &#13;
grado (art. 4-ter comma 2) e perfino nel giudizio di appello, ove sia &#13;
stata disposta la rinnovazione dell'istruttoria dibattimentale, prima &#13;
della conclusione della stessa (art. 4-ter comma 3); &#13;
        che  tale disciplina, in sé intrinsecamente discriminatoria, &#13;
risulterebbe altresì inconferente, e come tale priva di qualsivoglia &#13;
ragionevolezza,  rispetto  agli scopi propri del giudizio abbreviato, &#13;
individuabili   nella   preminente  esigenza  deflattiva,  perseguita &#13;
attraverso   la   radicale   rinunzia   dell'imputato   a  difendersi &#13;
nell'ordinario rito accusatorio, con conseguente diminuzione di pena: &#13;
viceversa,  tale  ragionevole  coerenza  di sistema verrebbe meno nel &#13;
meccanismo   procedurale   delineato   nel   citato   art. 4-ter  con &#13;
riferimento   ai   procedimenti   per  reati  punibili  con  la  pena &#13;
dell'ergastolo,  ove  la  possibilità di richiedere il rito speciale &#13;
fino  al  momento  della  chiusura  dell'istruttoria dibattimentale e &#13;
persino  in  grado  di  appello,  configurerebbe  una  vero  "diritto &#13;
potestativo sulla pena" in capo a tale categoria di imputati; &#13;
        che,  infatti,  costoro possono avanzare la loro richiesta "a &#13;
costo  zero", conoscendo cioè la situazione probatoria consolidatasi &#13;
nell'istruttoria   dibattimentale   e,   dunque,   potendo  valutare, &#13;
immediatamente  prima della discussione finale, la convenienza o meno &#13;
di  un  giudizio  di  merito  fondato esclusivamente sulle risultanze &#13;
dell'istruttoria  dibattimentale  o  "anche  sulla  base  degli  atti &#13;
contenuti  nel  fascicolo  di  cui all'art. 416, comma 2" (art. 4-ter &#13;
comma  2,  ultima  parte),  vale  a  dire  sugli  atti delle indagini &#13;
preliminari; &#13;
        che ulteriori profili di irragionevolezza di tale facoltà di &#13;
accesso  al  rito  abbreviato,  anche  ad  istruttoria dibattimentale &#13;
pressoché  conclusa, sarebbero rappresentati dalla completa elisione &#13;
dello scopo di economia processuale proprio del rito speciale e dalla &#13;
irragionevole  possibilità  di  una  decisione di merito fondata, in &#13;
pari misura, su due "blocchi" di prove, assunte con logiche, metodi e &#13;
criteri contrapposti; &#13;
        che  sussisterebbe,  ancora,  violazione  dell'art. 3,  primo &#13;
comma,  della  Costituzione, sotto il profilo della preclusione dell' &#13;
accesso  allo  speciale rito disciplinato dall' art. 4-ter citato per &#13;
gli imputati in processi nei quali, al momento dell'entrata in vigore &#13;
della  legge  5 giugno  2000,  n. 144, era in corso la discussione in &#13;
primo  o  in  secondo  grado, nonché per gli imputati nei giudizi di &#13;
secondo  grado  in  cui  non  sia  o non sia stata ancora disposta la &#13;
riapertura dell'istruzione dibattimentale; &#13;
        che,  secondo  il  giudice a quo la norma in esame violerebbe &#13;
anche  l'art. 27, terzo comma, della Costituzione, nella parte in cui &#13;
tale  precetto  costituzionale  esige  che  la pena da infliggere sia &#13;
proporzionata  ed  adeguata  all'effettiva  gravità  del  fatto:  il &#13;
meccanismo  introdotto  dalla  norma  censurata, non contemplando una &#13;
concreta   verifica   dell'efficacia   della  pena  che  ne  risulta, &#13;
vanificherebbe  la funzione rieducativa della stessa ed attribuirebbe &#13;
un  beneficio  all'imputato  in  assenza  di comportamenti valutabili &#13;
sotto il profilo della rieducazione, disarticolando così la sanzione &#13;
penale  dal  principio  di  adeguatezza  alla gravità del fatto, con &#13;
ulteriore  violazione  del principio di eguaglianza rispetto a quegli &#13;
imputati  per  i  quali  la  pena  viene, invece, determinata in modo &#13;
congruente  con  i  principi,  i  criteri e la funzione dell'art. 133 &#13;
cod.pen; &#13;
        che  sarebbe  vulnerato,  ancora,  l'art. 101, secondo comma, &#13;
della  Costituzione,  in  quanto,  nel  modello  di  rito  abbreviato &#13;
delineato  nella  norma  censurata,  l'automatismo  tra dichiarazione &#13;
dell'imputato  e  riduzione  della  pena,  risolvendosi in un diritto &#13;
potestativo  sulla  pena  attribuito  all'imputato,  costituirebbe un &#13;
vincolo  o  una "sovradeterminazione" nella decisione per il giudice, &#13;
essendo   quest'ultimo  privato  del  potere  di  stabilire  la  pena &#13;
adeguata,  attribuitogli in via generale dall'art. 133 codice penale, &#13;
o  comunque  limitato nell'esercizio di esso, con una norma in deroga &#13;
di carattere singolare e contingente; &#13;
        che,  inoltre,  proprio in quanto esautora il giudice da ogni &#13;
potere   discrezionale   nell'applicare   la   riduzione   di   pena, &#13;
l'art. 4-ter citato introdurrebbe una "diminuente speciale ad effetto &#13;
bloccato",  imposta  per  legge ai processi in corso, con conseguente &#13;
violazione  anche  dell'art. 102, primo comma, della Costituzione, in &#13;
quanto la riduzione della pena non sarebbe più opera del giudice, ma &#13;
"scelta  diretta del legislatore che nel caso concreto si sostituisce &#13;
al giudice"; &#13;
        che,  infine,  il  giudice a quo rileva come l'art. 4-ter del &#13;
decreto-legge  7 aprile  2000,  n.82,  convertito, con modificazioni, &#13;
nella  legge  5 giugno  2000,  n. 144,  si ponga in contrasto con gli &#13;
artt. 97,  primo  comma,  e 111, secondo comma, della Costituzione in &#13;
quanto,   traducendosi   la  disciplina  censurata  in  un  sensibile &#13;
allungamento  dei  tempi  di  definizione del procedimento, la stessa &#13;
comprometterebbe,  ad  un tempo, tanto il principio di buon andamento &#13;
dell'amministrazione della giustizia, che il nuovo, specifico vincolo &#13;
costituzionale della "ragionevole durata del processo". &#13;
    Considerato   che   il  decreto-legge  24 novembre  2000,  n. 341 &#13;
(Disposizioni     urgenti     per    l'efficacia    e    l'efficienza &#13;
dell'amministrazione della giustizia), convertito, con modificazioni, &#13;
dalla legge 19 gennaio 2001, n. 4, ha apportato sensibili innovazioni &#13;
al quadro normativo di riferimento della questione sollevata; &#13;
        che,  in  particolare,  l'art. 7  del decreto-legge n.341 del &#13;
2000 ha stabilito, al comma 1, con norma di interpretazione autentica &#13;
dell'art. 442,   comma  2,  ultimo  periodo,  cod.  proc.  pen.,  che &#13;
"l'espressione   "pena   dell'ergastolo"   deve  intendersi  riferita &#13;
all'ergastolo  senza  isolamento diurno" ed ha altresì aggiunto, con &#13;
il  comma  2, un ulteriore periodo al capoverso del medesimo art. 442 &#13;
codice  procedimento penale, prevedendo che "alla pena dell'ergastolo &#13;
con  isolamento  diurno,  nei  casi  di  concorso di reati e di reato &#13;
continuato, è sostituita quella dell'ergastolo"; &#13;
        che, in correlazione a tali previsioni, l'art. 8 del medesimo &#13;
decreto-legge  n. 341  del  2000  ha  inoltre introdotto una speciale &#13;
disciplina transitoria per i procedimenti penali in corso, stabilendo &#13;
che  l'imputato,  in determinate ipotesi, possa revocare la richiesta &#13;
di  giudizio  abbreviato  ovvero  la  richiesta  di  cui  al  comma 2 &#13;
dell'art. 4-ter del decreto-legge n. 82 del 2000; &#13;
        che,  pertanto,  gli atti devono essere restituiti al giudice &#13;
rimettente,  perché valuti se la questione sollevata possa ritenersi &#13;
tuttora rilevante nel giudizio a quo.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Ordina la restituzione degli atti al giudice rimettente. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 21 marzo 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Flick &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in Cancelleria il 4 aprile 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
