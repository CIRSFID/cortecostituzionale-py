<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1976</anno_pronuncia>
    <numero_pronuncia>100</numero_pronuncia>
    <ecli>ECLI:IT:COST:1976:100</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>21/04/1976</data_decisione>
    <data_deposito>28/04/1976</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio  di  legittimità  costituzionale  dell'art.  205  del  &#13;
 d.P.R.   29 gennaio 1958, n. 645 (testo unico delle leggi sulle imposte  &#13;
 dirette), promosso con ordinanza emessa il 24 aprile 1973  dal  pretore  &#13;
 di  Sarzana  nel  procedimento civile vertente tra l'Istituto nazionale  &#13;
 della previdenza sociale e Gianfranchi Anna Maria ed altro, iscritta al  &#13;
 n.  230  del  registro  ordinanze  1973  e  pubblicata  nella  Gazzetta  &#13;
 Ufficiale della Repubblica n. 198 del 1 agosto 1973.                     &#13;
     Visto   l'atto   di   costituzione  dell'Istituto  nazionale  della  &#13;
 previdenza sociale;                                                      &#13;
     udito  nell'udienza  pubblica  del  12  febbraio  1976  il  Giudice  &#13;
 relatore Michele Rossano;                                                &#13;
     udito l'avv. Sergio Traverso, per l'INPS.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  del  procedimento  esecutivo  mobiliare  - promosso nei  &#13;
 confronti di Anna Maria Gianfranchi su istanza dell'Istituto  nazionale  &#13;
 della   previdenza   sociale,   creditore  dell'importo  di  contributi  &#13;
 previdenziali  non  versati  e  delle  conseguenti  sanzioni  civili  -  &#13;
 l'Esattore  delle  imposte  dirette di Sarzana, con atto 28 marzo 1973,  &#13;
 dichiarava al pretore di Sarzana, quale giudice  dell'esecuzione  -  ai  &#13;
 sensi  dell'art.  205 t.u.   29 gennaio 1958, n. 645, delle leggi sulle  &#13;
 imposte dirette - di volersi surrogare all'INPS.                         &#13;
     Il pretore di Sarzana,  con  ordinanza  24  aprile  1973,  riteneva  &#13;
 rilevante  e  non  manifestamente infondata - in riferimento all'art. 3  &#13;
 della Costituzione - la questione, sollevata dall'INPS, concernente  la  &#13;
 legittimità  costituzionale  dell'art.  205 d.P.R. 29 gennaio 1958, n.  &#13;
 645 (t.u. delle leggi sulle imposte dirette) nella  parte  in  cui  non  &#13;
 prevede l'esclusione della facoltà di surroga dell'esattore qualora il  &#13;
 creditore procedente vanti un credito per contributi previdenziali, che  &#13;
 -  ai sensi dell'art. 66 legge 30 aprile 1969, n. 153 - è collocato al  &#13;
 primo posto nell'ordine di prelazione di cui all'art. 2778  del  codice  &#13;
 civile e precede i crediti per tributi.                                  &#13;
     L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n. 198 del  &#13;
 1 agosto 1973.                                                           &#13;
     Nel  giudizio  davanti a questa Corte si è costituito solo l'INPS,  &#13;
 depositando comparsa in data 9 agosto 1973, con la quale ha chiesto che  &#13;
 venga dichiarata la illegittimità costituzionale  dell'art.  205  t.u.  &#13;
 delle leggi sulle imposte dirette " nella parte in cui non prevede alla  &#13;
 contemplata facoltà di surroga dell'esattore alcuna limitazione per le  &#13;
 ipotesi  in  cui  l'originario  creditore  procedente  vanti un credito  &#13;
 privilegiato potiore a quello rappresentato dall'esattore stesso".<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con l'ordinanza indicata in epigrafe, il pretore ha ritenuto  &#13;
 rilevante e non infondata la questione se l'art. 205 d.P.R. 29  gennaio  &#13;
 1958,  n. 645 (t.u. delle leggi sulle imposte dirette) sia in contrasto  &#13;
 con l'art. 3, primo comma, della Costituzione, in quanto  "non  prevede  &#13;
 alla  facoltà di surroga dell'esattore alcuna limitazione nell'ipotesi  &#13;
 in cui l'originario creditore procedente vanti un credito di privilegio  &#13;
 assoluto e potiore rispetto a quelli tributari".  Secondo  il  pretore,  &#13;
 l'art.  66  legge  30  aprile  1969,  n.  153,  sulla  revisione  degli  &#13;
 ordinamenti pensionistici e in materia  di  sicurezza  sociale,  avendo  &#13;
 modificato  il  n.  1 dell'art. 2778 cod. civ. nel senso di collocare i  &#13;
 crediti per contributi di previdenza sociale in primo grado in luogo di  &#13;
 quelli  tributari,  avrebbe  posto  sullo   stesso   piano   situazioni  &#13;
 giuridiche  profondamente  diverse  e diversamente realizzabili in sede  &#13;
 esecutiva, in quanto il grado di privilegio, per effetto della modifica  &#13;
 legislativa, di cui all'art. 66 citato,  escluderebbe  "sia  sul  piano  &#13;
 formale  che  su  quello  sostanziale  un  razionale  fondamento  della  &#13;
 facoltà di surroga prevista dall'art. 205 t.u.  citato  e  violerebbe,  &#13;
 quindi, l'art. 3 della Costituzione".                                    &#13;
     2. - La questione non è fondata.                                    &#13;
     La  legge  29  luglio  1975, n. 426, entrata in vigore nel corso di  &#13;
 questo giudizio, stabilisce che le sue disposizioni si applicano "anche  &#13;
 per i crediti sorti anteriormente alla sua entrata in vigore  e  se  il  &#13;
 privilegio è stato fatto valere anteriormente".                         &#13;
     Essa, che ha modificato l'ordine dei privilegi stabiliti dal codice  &#13;
 civile,  con l'art. 12 ha sostituito l'art. 2778 cod. civ., affermando,  &#13;
 nella  nuova  formula,  la  stessa  salvezza  concernente  i  privilegi  &#13;
 indicati  nell'art. 2777 stesso codice e, quindi, anche quelli relativi  &#13;
 ai crediti di istituti, enti e  fondi  speciali  nella  formula  stessa  &#13;
 indicati  al  n.  1  e già previsti dall'art. 66 della legge 30 aprile  &#13;
 1969, n. 153, nonché quelli indicati nell'art.  2753 codice civile.     &#13;
     Da tali disposizioni emerge che l'ordine  di  prelazione  stabilito  &#13;
 nel  n.  1  dell'art. 2778 per contributi dovuti ad enti che gestiscono  &#13;
 forme di assicurazione obbligatoria, ivi indicate, non ha  alterato  la  &#13;
 posizione,  ad  essi  potiore, dei crediti dichiarati da leggi speciali  &#13;
 genericamente preferiti ad ogni  altro  credito  nei  limiti  stabiliti  &#13;
 dall'art.  2777, come già ebbe ad affermare la Corte di cassazione con  &#13;
 riguardo all'art. 66 della legge 30 aprile 1969, n. 153,  ora  abrogato  &#13;
 dall'art. 16 della legge 29 luglio 1975, n. 426.                         &#13;
     Alla  stregua  di  tale  disciplina  -  anteriore  ed  attuale - va  &#13;
 considerato che il diritto di surroga dell'esattore a termini dell'art.  &#13;
 205 t.u. citato, in quanto volto alla sollecita riscossione dei tributi  &#13;
 e non collegato al grado del privilegio, non incide affatto sull'ordine  &#13;
 dei privilegi che deve essere  accertato  dal  giudice,  a  termini  di  &#13;
 legge, nei casi concreti.                                                &#13;
     D'altra  parte,  non  va  trascurato che neppure nei casi in cui il  &#13;
 credito privilegiato per tributi  sia  di  grado  posteriore  a  quello  &#13;
 previsto  nel  n.  1  dell'art.  2778 cod. civ. è ammessa una indagine  &#13;
 preliminare circa la prevedibile  possibilità  di  soddisfacimento  in  &#13;
 concreto  del credito tributario. Come questa Corte ebbe a rilevare con  &#13;
 sentenza  4  giugno  1970,  n.  95,  l'art.  3  della  Costituzione  è  &#13;
 applicabile  quando  vi  sia  omogeneità  di  situazioni  da  regolare  &#13;
 legislativamente in modo unitario e coerente, non quando si  tratti  di  &#13;
 situazioni che, pur derivanti da basi comuni, differiscano tra loro per  &#13;
 aspetti  distintivi particolari: come nel caso in esame, caratterizzato  &#13;
 dalla finalità di  natura  pubblicistica  di  agevolare  la  sollecita  &#13;
 riscossione di tributi erariali.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  non  fondata  la questione di legittimità costituzionale  &#13;
 dell'art. 205 del d.P.R. 29 gennaio 1958, n.  645  (testo  unico  delle  &#13;
 leggi  sulle  imposte  dirette),  sollevata dal pretore di Sarzana, con  &#13;
 ordinanza  24  aprile  1973,  in  riferimento   all'art.      3   della  &#13;
 Costituzione.                                                            &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 21 aprile 1976.                               &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO.                            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
