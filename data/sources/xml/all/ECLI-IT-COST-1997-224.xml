<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>224</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:224</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Ruperto</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/06/1997</data_decisione>
    <data_deposito>03/07/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 222 del  codice    &#13;
 penale  militare  di  pace  in relazione all'art. 260, secondo comma,    &#13;
 dello stesso codice, promosso con ordinanza emessa il 9  maggio  1996    &#13;
 dal  tribunale militare di Padova nel procedimento penale a carico di    &#13;
 Policella Daniel, iscritta al n. 1356 del registro ordinanze  1996  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 3, prima    &#13;
 serie speciale, dell'anno 1997;                                          &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  18 giugno 1997 il giudice    &#13;
 relatore Cesare Ruperto;                                                 &#13;
   Ritenuto che nel corso  di  un  procedimento  penale  a  carico  di    &#13;
 Policella  Daniel, imputato del reato di percosse nei confronti di un    &#13;
 commilitone (costituitosi parte civile),  il  tribunale  militare  di    &#13;
 Padova,  con  ordinanza  emessa  il  9 maggio 1996, ha sollevato - in    &#13;
 riferimento agli artt. 2, 3, 24, primo comma,  e  52,  ultimo  comma,    &#13;
 della   Costituzione   -  questione  di  legittimità  costituzionale    &#13;
 dell'art. 222  del  codice  penale  militare  di  pace  in  relazione    &#13;
 all'art. 260, secondo comma, dello stesso codice;                        &#13;
     che,  a  giudizio del rimettente, tale norma - nella parte in cui    &#13;
 prevede che i reati per i quali la legge  stabilisce  la  pena  della    &#13;
 reclusione  militare non superiore nel massimo a sei mesi sono puniti    &#13;
 a richiesta del comandante di corpo - violerebbe: a) l'art.  2  della    &#13;
 Costituzione, determinando una "confisca" della tutela in sede penale    &#13;
 della  persona  militare  in  favore  di  un  non  meglio precisabile    &#13;
 "interesse pubblico militare", anche nell'ipotesi in cui il fatto sia    &#13;
 grave e non possa perciò  dirsi  prevalente  l'offesa  all'interesse    &#13;
 militare, non essendo "sufficiente ad esaurire i diritti del singolo"    &#13;
 il  riconoscimento a favore della persona offesa di un'azione civile;    &#13;
 b)  l'art.  3  della  Costituzione,  poiché  l'"espropriazione"  del    &#13;
 diritto  di  tutela  del  cittadino  militare in sede penale porrebbe    &#13;
 quest'ultimo in situazione di ingiustificata disparità  rispetto  al    &#13;
 cittadino  civile; c) l'art.  24, primo comma, della Costituzione, in    &#13;
 quanto - pregiudicata la possibilità di costituirsi parte civile nel    &#13;
 processo militare - il militare offeso dovrebbe  subire  la  maggiore    &#13;
 lungaggine  dell'esercizio  dell'azione  civile; d) l'art. 52, ultimo    &#13;
 comma, della Costituzione, poiché non è da reputarsi  necessaria  -    &#13;
 per   l'assolvimento   dei   compiti  propri  delle  Forze  Armate  -    &#13;
 l'attribuzione al solo comandante di corpo della facoltà di decidere    &#13;
 se richiedere o meno la perseguibilità dei  fatti  in  sede  penale,    &#13;
 potendosi configurare una identica facoltà concorrente del militare;    &#13;
     che  è  intervenuto  il  Presidente  del Consiglio dei Ministri,    &#13;
 rappresentato e difeso dall'Avvocatura dello Stato,  concludendo  per    &#13;
 l'inammissibilità o comunque per l'infondatezza della questione;        &#13;
   Considerato  che,  ripetutamente  sottoposta  al  vaglio  di questa    &#13;
 Corte, la questione di costituzionalità della disciplina di  cui  al    &#13;
 secondo  comma  dell'art.  260  del codice penale militare di pace è    &#13;
 stata dichiarata non fondata o manifestamente infondata;                 &#13;
     che, da ultimo, identica questione -  riguardante  la  previsione    &#13;
 della punibilità a richiesta del comandante di corpo dei reati per i    &#13;
 quali  la  legge  stabilisce  la  pena  della reclusione militare non    &#13;
 superiore nel massimo a sei mesi -, sollevata dal medesimo  tribunale    &#13;
 militare  di  Padova  in  riferimento agli stessi parametri in questa    &#13;
 sede  evocati,  è  stata  dichiarata  manifestamente  infondata  con    &#13;
 ordinanza n. 396 del 1996;                                               &#13;
     che  il  rimettente non offre ulteriori diversi motivi a sostegno    &#13;
 della questione, la quale dunque risulta manifestamente infondata.       &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 222 del codice penale militare  di  pace  in    &#13;
 relazione   all'art.   260,   secondo  comma,  dello  stesso  codice,    &#13;
 sollevata, in riferimento agli artt. 2, 3, 24,  primo  comma,  e  52,    &#13;
 ultimo  comma,  della Costituzione, dal tribunale militare di Padova,    &#13;
 con l'ordinanza in epigrafe.                                             &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 19 giugno 1997.                               &#13;
                        Il presidente: Granata                            &#13;
                         Il redattore: Ruperto                            &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 3 luglio 1997.                            &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
