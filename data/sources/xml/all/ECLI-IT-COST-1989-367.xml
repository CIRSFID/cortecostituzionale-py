<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>367</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:367</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/06/1989</data_decisione>
    <data_deposito>27/06/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 665 del codice    &#13;
 di procedura civile, promosso con ordinanza emessa il 5 dicembre 1988    &#13;
 dal Pretore di La Spezia nel procedimento civile vertente tra Ruggeri    &#13;
 Salvatore ed altra  e  Baglioni  Giovanna,  iscritta  al  n.  69  del    &#13;
 registro  ordinanze  1989 e pubblicata nella Gazzetta Ufficiale della    &#13;
 Repubblica n. 8, prima serie speciale, dell'anno 1989;                   &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consigli  dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del 17 maggio 1989 il Giudice    &#13;
 relatore Francesco Greco;                                                &#13;
    Ritenuto che il Pretore di La Spezia, con ordinanza emessa in data    &#13;
 5 dicembre 1988 (R.O.  n.  69  del  1989),  nel  procedimento  civile    &#13;
 vertente  tra  Ruggeri  Salvatore  ed  altra e Baglioni Giovanna, per    &#13;
 convalida  di  sfratto  di  immobile  destinato  ad  uso  diverso  da    &#13;
 abitazione,  con  istanza  di  emissione  di  ordinanza  di immediato    &#13;
 rilascio ex art. 665 del  codice  di  procedura  civile  in  caso  di    &#13;
 opposizione,  ha  sollevato  questione di legittimità costituzionale    &#13;
 dell'art. 665 del codice di  procedura  civile  che  prevede  appunto    &#13;
 l'emissione   di   una   ordinanza   non   impugnabile   di  rilascio    &#13;
 immediatamente esecutiva nel caso in cui l'opposizione  dell'intimato    &#13;
 non sia fondata su prova scritta;                                        &#13;
      che,  ad  avviso  del giudice remittente, risulterebbero violati    &#13;
 gli artt. 3 e 24 della Costituzione in quanto si oblitera  il  giusto    &#13;
 equilibrio  fra diritto di agire e diritto di difendersi in giudizio,    &#13;
 nonostante la previsione, quale condizione ostativa  della  emissione    &#13;
 dell'ordinanza di cui trattasi, di gravi motivi;                         &#13;
      che  nel  giudizio  è  intervenuta  l'Avvocatura Generale dello    &#13;
 Stato, in rappresentanza del Presidente del Consiglio  dei  ministri,    &#13;
 che ha concluso per la infondatezza della questione;                     &#13;
    Considerato  che, come questa Corte ha già affermato (sentenza n.    &#13;
 238  del  1975),  il  procedimento  in  questione  è   adeguatamente    &#13;
 giustificato  dalla  specialità  della  materia e dalla peculiarità    &#13;
 degli interessi da tutelare e che le sue caratteristiche  non  ledono    &#13;
 il  diritto  della  difesa garantito dall'art. 24 della Costituzione,    &#13;
 né creano disparità di trattamento tra  coloro  che  utilizzano  la    &#13;
 procedura  ordinaria  e coloro che ricorrono alla procedura speciale,    &#13;
 la quale soddisfa esigenze di rapidità ed immediatezza;                 &#13;
      che,  peraltro,  l'ordinanza  de  qua può non essere emessa nel    &#13;
 caso di sussistenza di gravi motivi  discrezionalmente  valutati  dal    &#13;
 giudice  e  alla  mancata  immediata  impugnazione,  nel procedimento    &#13;
 interinale, si sopperisce nel giudizio  di  merito  che  prosegue  in    &#13;
 forma ordinaria, a cognizione piena e con possibilità di svolgimento    &#13;
 ampio ed integrale di tutte le prospettazioni difensive (sentenza  n.    &#13;
 94 del 1973);                                                            &#13;
      che,   pertanto,   la   questione  sollevata  è  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi dinanzi    &#13;
 alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
    Dichiara la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 665  del  codice  di  procedura  civile,  in    &#13;
 riferimento  agli  artt.  3  e  24  della Costituzione, sollevata dal    &#13;
 Pretore di La Spezia con la ordinanza in epigrafe.                       &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 14 giugno 1989.                               &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 giugno 1989.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
