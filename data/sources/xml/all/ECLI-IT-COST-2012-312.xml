<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2012</anno_pronuncia>
    <numero_pronuncia>312</numero_pronuncia>
    <ecli>ECLI:IT:COST:2012:312</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>QUARANTA</presidente>
    <relatore_pronuncia>Aldo Carosi</relatore_pronuncia>
    <redattore_pronuncia>Aldo Carosi</redattore_pronuncia>
    <data_decisione>12/12/2012</data_decisione>
    <data_deposito>20/12/2012</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta inammissibilità</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Alfonso QUARANTA; Giudici : Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Giuseppe TESAURO, Paolo Maria NAPOLITANO, Giuseppe FRIGO, Alessandro CRISCUOLO, Paolo GROSSI, Giorgio LATTANZI, Aldo CAROSI, Marta CARTABIA, Sergio MATTARELLA, Mario Rosario MORELLI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale del mancato riconoscimento alla donna esercente la libera professione di avvocato del «diritto di usufruire del periodo di maternità così come previsto dall'Ordinamento italiano per le altre lavoratrici», promosso dal Tribunale ordinario di Perugia, sezione distaccata di Foligno, nel procedimento penale a carico di A.M. ed altra con ordinanza del 23 gennaio 2012, iscritta al n. 158 del registro ordinanze 2012 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 34, prima serie speciale, dell'anno 2012.&#13;
 Visto l'atto di intervento del Presidente del Consiglio dei ministri;&#13;
 udito nella camera di consiglio del 20 novembre 2012 il Giudice relatore Aldo Carosi.</epigrafe>
    <testo>Ritenuto che il Tribunale ordinario di Perugia, sezione distaccata di Foligno, con ordinanza del 23 gennaio 2012, ha sollevato questione di legittimità costituzionale del mancato riconoscimento alla donna esercente la libera professione di avvocato del «diritto di usufruire del periodo di maternità così come previsto dall'Ordinamento italiano per le altre lavoratrici» in riferimento agli articoli 3, 31, secondo comma, e 37 della Costituzione, nonché al diritto di difesa;&#13;
 che il rimettente riferisce dell'avvenuto deposito di un'istanza di rinvio dell'udienza dibattimentale penale da parte del difensore dell'imputato, per impedimento dovuto alla maternità, come risulterebbe evincibile dalla documentazione allegata all'istanza medesima;&#13;
 che ad avviso del giudice a quo il mancato riconoscimento alla donna esercente l'attività di avvocato libero professionista del «diritto di usufruire del periodo di maternità» accordato alle altre lavoratrici contrasterebbe con gli artt. 3, 31, comma 2, e 37 Cost., che esprimono i principi di uguaglianza e di protezione della maternità e dell'infanzia;&#13;
 che, inoltre, il mancato riconoscimento di tale diritto, impedendo al giudice di sospendere la decorrenza dei termini prescrizionali per legittimo impedimento del difensore ed, in generale, all'avvocato donna di svolgere al meglio la funzione difensiva a tutela del proprio cliente, determinerebbe la violazione del diritto di difesa;&#13;
 che è intervenuto nel giudizio di costituzionalità, con atto depositato il 18 settembre 2012, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, chiedendo che la questione venga dichiarata inammissibile o, in subordine, infondata;&#13;
 che, sotto il primo profilo, l'intervenuto rileva come il rimettente abbia completamente omesso di indicare la disposizione della cui costituzionalità dubita, violando il dettato dell'art. 23, terzo comma, della legge 11 marzo 1953, n. 87;&#13;
 che, peraltro, ove il rimettente intendesse riferirsi all'art. 16, comma 1, del decreto legislativo 26 marzo 2001, n. 151 (Testo unico delle disposizioni legislative in materia di tutela e sostegno della maternità e della paternità, a norma dell'articolo 15 della L. 8 marzo 2000, n. 53), ad avviso del Presidente del Consiglio la questione dovrebbe ritenersi irrilevante, atteso che il giudice a quo avrebbe potuto pronunciarsi sulla richiesta di rinvio facendo applicazione della disciplina dell'impedimento a comparire del difensore, di cui al combinato disposto degli artt. 484, comma 2-bis, e 420-ter, comma 5, del codice di procedura penale;&#13;
 che, infine, a sostegno dell'infondatezza della questione, l'intervenuto evidenzia l'eterogeneità della situazione in cui versano le lavoratrici dipendenti rispetto a quella in cui si trovano le lavoratrici libere professioniste.&#13;
 Considerato che il Tribunale ordinario di Perugia, sezione distaccata di Foligno, con ordinanza del 23 gennaio 2012, ha sollevato questione di legittimità costituzionale del mancato riconoscimento alla donna esercente la libera professione di avvocato del «diritto di usufruire del periodo di maternità così come previsto dall'Ordinamento italiano per le altre lavoratrici» per violazione degli articoli 3, 31, secondo comma, e 37 della Costituzione - che esprimono i principi di uguaglianza e di protezione della maternità e dell'infanzia - e del diritto di difesa, impedendo al giudice di sospendere la decorrenza dei termini prescrizionali per legittimo impedimento del difensore ed, in generale, all'avvocato donna di svolgere al meglio la funzione difensiva a tutela del proprio cliente;&#13;
 che è intervenuto nel giudizio di costituzionalità, con atto depositato il 18 settembre 2012, il Presidente del Consiglio dei ministri, chiedendo che la questione venga dichiarata inammissibile, per mancata indicazione della disposizione oggetto di censura o per difetto di rilevanza, ovvero, in subordine, infondata, perché le lavoratrici dipendenti e quelle libere professioniste verserebbero in situazioni non omogenee;&#13;
 che l'ordinanza è affetta da diversi profili di inammissibilità;&#13;
 che un primo profilo d'inammissibilità deriva dalla mancata indicazione della norma censurata (ordinanze n. 307 del 2011, n. 227 del 2007 e n. 85 del 2003), che, all'esame dell'intero contesto dell'atto di rimessione, non risulta identificabile nemmeno per indicazione implicita, stante anche la carenza descrittiva della fattispecie concreta;&#13;
 che l'impossibilità, per le enunciate ragioni, di una precisa individuazione della norma censurata si riverbera inevitabilmente sulla rilevanza della questione, non potendosi valutare la necessità di applicazione della norma stessa;&#13;
 che, inoltre, il rimettente non dà contezza dei motivi che gli impediscono di applicare alla fattispecie al suo esame la disciplina dell'impedimento a comparire del difensore (artt. 484, comma 2-bis, e 420-ter, comma 5, del codice di procedura penale) senza sollevare la questione di legittimità, sperimentando così la praticabilità di una soluzione interpretativa idonea a superare la prospettata questione (ordinanza n. 101 del 2011);&#13;
 che l'omesso accertamento dell'applicabilità e degli eventuali effetti sul giudizio principale del regime di cui al combinato disposto degli artt. 484, comma 2-bis, e 420-ter, comma 5, cod. proc. pen. si risolve in un difetto di motivazione sulla rilevanza della questione ed è profilo preliminare rispetto a quello della mancata esplicitazione da parte del giudice a quo delle ragioni ritenute ostative ad un'interpretazione costituzionalmente orientata;&#13;
 che, peraltro, anche tale doveroso tentativo ermeneutico (ordinanze n. 194 del 2012, n. 192 del 2010 e n. 154 del 2010) è stato completamente omesso;&#13;
 che, pertanto, la sollevata questione risulta manifestamente inammissibile.</testo>
    <dispositivo>per questi motivi &#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara la manifesta inammissibilità della questione di legittimità costituzionale del mancato riconoscimento alla donna esercente la libera professione di avvocato del «diritto di usufruire del periodo di maternità così come previsto dall'Ordinamento italiano per le altre lavoratrici», sollevata, in riferimento agli articoli 3, 31, secondo comma, e 37 della Costituzione ed al diritto di difesa, dal Tribunale ordinario di Perugia, sezione distaccata di Foligno, con l'ordinanza in epigrafe.&#13;
 	Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 12 dicembre 2012.&#13;
 F.to:&#13;
 Alfonso QUARANTA, Presidente&#13;
 Aldo CAROSI, Redattore&#13;
 Gabriella MELATTI, Cancelliere&#13;
 Depositata in Cancelleria il 20 dicembre 2012.&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Gabriella MELATTI</dispositivo>
  </pronuncia_testo>
</pronuncia>
