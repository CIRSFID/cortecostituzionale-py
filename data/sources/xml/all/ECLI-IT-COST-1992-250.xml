<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1992</anno_pronuncia>
    <numero_pronuncia>250</numero_pronuncia>
    <ecli>ECLI:IT:COST:1992:250</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>20/05/1992</data_decisione>
    <data_deposito>03/06/1992</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI, prof. Francesco GUIZZI, &#13;
 prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 4  del  decreto    &#13;
 legislativo  luogotenenziale  27  luglio  1945,  n.  475  (Divieto di    &#13;
 abbattimento di alberi di olivo), promosso con ordinanza emessa il 23    &#13;
 ottobre  1991  dal  Pretore  di  Foggia  -  Sezione   distaccata   di    &#13;
 Trinitapoli  nel procedimento civile vertente tra Palmitessa Giuseppe    &#13;
 e Ufficio Regionale Contenzioso di Foggia - Regione Puglia,  iscritta    &#13;
 al  n.  19  del  registro  ordinanze 1992 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n.  6,  prima  serie  speciale,  dell'anno    &#13;
 1992;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito nella camera di consiglio del  15  aprile  1992  il  Giudice    &#13;
 relatore Vincenzo Caianiello;                                            &#13;
    Ritenuto   che   nel  corso  di  un  giudizio  di  opposizione  ad    &#13;
 un'ordinanza-ingiunzione,  con  la  quale  l'ufficio  contenzioso  di    &#13;
 Foggia della Regione Puglia aveva irrogato la sanzione amministrativa    &#13;
 prevista  dall'art.  4  del  decreto  legislativo  luogotenenziale 27    &#13;
 luglio 1945, n. 475 (Divieto di abbattimento di alberi di olivo),  il    &#13;
 Pretore  di Foggia - sezione distaccata di Trinitapoli, con ordinanza    &#13;
 del 23 ottobre 1991, ha sollevato d'ufficio questioni di legittimità    &#13;
 costituzionale del citato art. 4 che,  prevedendo  per  l'illegittimo    &#13;
 abbattimento  di  alberi  di olivo una sanzione pecuniaria di importo    &#13;
 pari al decuplo del valore delle  piante  abbattute,  considerate  in    &#13;
 piena   produttività,  da  stabilirsi  dall'ispettorato  provinciale    &#13;
 dell'agricoltura, si porrebbe in contrasto:                              &#13;
      a)  con  l'art.  25,  secondo  comma,  della  Costituzione,  che    &#13;
 sancisce il principio della "riserva assoluta di legge" in materia di    &#13;
 determinazione    della   entità   della   sanzione,   pacificamente    &#13;
 estendibile  al  campo   dell'illecito   amministrativo   anche   per    &#13;
 l'inequivoco  tenore dell'art. 1 della legge 24 novembre 1981, n. 689    &#13;
 (Modifiche  del  sistema  penale),  che  riproduce  espressamente  il    &#13;
 principio  costituzionale  di legalità. Nella specie il legislatore,    &#13;
 una volta descritta puntualmente la  condotta  vietata  e  quindi  il    &#13;
 precetto,  si sarebbe illegittimamente rimesso, per quanto attiene al    &#13;
 profilo sanzionatorio, alle determinazioni di  un  organo  periferico    &#13;
 dello  Stato  (recte: della regione) svincolate da qualunque criterio    &#13;
 prefissato ed implicanti quindi apprezzamenti diversi circa il valore    &#13;
 del bene oggetto di tutela;                                              &#13;
      b) con l'art. 23 della Costituzione - nel caso si ritenga che la    &#13;
 sanzione prevista dalla norma  impugnata  abbia  natura  riparatoria-recuperatoria  e  non afflittiva - il quale parametro, pur se esprime    &#13;
 il  principio  della  riserva  relativa  di  legge  in   materia   di    &#13;
 prestazioni  patrimoniali,  non  consentirebbe  in  ogni  caso, senza    &#13;
 criteri  legislativi  di  riferimento,  di  affidare   ad   un   atto    &#13;
 amministrativo  "meramente  discrezionale"  la  determinazione  della    &#13;
 sanzione;                                                                &#13;
       c) con l'art. 3 della Costituzione, poiché  la  determinazione    &#13;
 della  misura  della sanzione verrebbe operata in ambito territoriale    &#13;
 circoscritto a quello della provincia, e, in quanto affidata a uffici    &#13;
 diversi che possono adottare criteri non omogenei per la  valutazione    &#13;
 del  valore delle piante, darebbe vita a una ingiustificata, iniqua e    &#13;
 irragionevole  disparità  di  trattamento  riguardo   a   situazioni    &#13;
 sostanzialmente identiche;                                               &#13;
      d) con l'art. 111, primo comma, della Costituzione, che sancisce    &#13;
 l'obbligo di motivazione per tutti i provvedimenti giurisdizionali, in   &#13;
 quanto,  potendo  il  giudice  alla  stregua  dei  criteri  stabiliti    &#13;
 dall'art. 11 della legge n. 689 del 1981 (gravità della  violazione,    &#13;
 comportamento  successivo  all'infrazione,  personalità e condizioni    &#13;
 economiche del trasgressore) operare  una  riduzione  della  sanzione    &#13;
 irrogata,  gli  sarebbe  impedito  di sorreggere la sua decisione con    &#13;
 motivazione convincente e  ragionevole,  a  causa  di  un  meccanismo    &#13;
 sanzionatorio rigidamente ancorato al solo valore delle piante senza,    &#13;
 per  di  più,  la  previsione  di  un  limite minimo e massimo della    &#13;
 sanzione, "entro i quali far uso del potere di  commisurazione  della    &#13;
 sanzione alla concreta gravità del fatto";                              &#13;
      che  è intervenuto nel giudizio il Presidente del Consiglio dei    &#13;
 ministri, per il  tramite  della  Avvocatura  generale  dello  Stato,    &#13;
 sostenendo    l'inammissibilità   delle   questioni   sollevate   in    &#13;
 riferimento all'art. 23 della Costituzione, in quanto non  suffragata    &#13;
 da  idonea  motivazione,  e  all'art.  111  della  Costituzione,  non    &#13;
 pertinente al dubbio prospettato, poiché il  dovere  processuale  di    &#13;
 motivare  le  sentenze non ha nulla a che vedere con la previsione di    &#13;
 una sanzione di importo fisso, la cui misura è  comunque  sottoposta    &#13;
 al  sindacato  giurisdizionale; nel merito ha rilevato l'infondatezza    &#13;
 degli ulteriori profili di censura  in  riferimento  agli  artt.  25,    &#13;
 secondo  comma  e  3 della Costituzione, poiché non può in concreto    &#13;
 invocarsi la lesione del  principio  di  legalità  sol  perché  una    &#13;
 sanzione,  commisurata  ad  un  "valore",  è  determinata  all'esito    &#13;
 dell'accertamento in concreto di tale valore.                            &#13;
    Considerato  che  devono  essere   disattese   le   eccezioni   di    &#13;
 inammissibilità   delle  questioni  illustrate  sub  b),  in  quanto    &#13;
 l'ordinanza di rimessione contiene una motivazione sia pur  sintetica    &#13;
 a  sostegno  della censura, e sub d) perché le considerazioni svolte    &#13;
 attengono al merito della questione;                                     &#13;
      che la censura sub a) non è fondata, poiché, come questa Corte    &#13;
 ha già affermato (sent. n. 447  del  1988),  il  parametro  invocato    &#13;
 (art.  25,  secondo comma, della Costituzione) non è riferibile alle    &#13;
 sanzioni amministrative, depenalizzate o meno, applicandosi  ad  esse    &#13;
 l'art. 23 della Costituzione;                                            &#13;
      che,  per quanto riguarda la questione sub b), la giurisprudenza    &#13;
 costituzionale, a partire dalla sentenza n. 4 del 1957, ha  indicato,    &#13;
 relativamente   alle   prestazioni   obbligatorie  "autoritariamente"    &#13;
 imposte, i limiti e le garanzie sufficienti a far ritenere rispettato    &#13;
 il principio della riserva di legge relativa stabilito  dall'art.  23    &#13;
 della  Costituzione,  all'uopo  precisando  che  la prestazione debba    &#13;
 avere "base" in una legge e che la legge stessa stabilisca i  criteri    &#13;
 idonei a regolare eventuali margini di discrezionalità lasciati alla    &#13;
 pubblica  amministrazione  nella  determinazione  in  concreto  della    &#13;
 prestazione (sentenze nn. 290 del 1987, 167 del 1986) ed inoltre,  al    &#13;
 fine  di  escludere  che  la  discrezionalità  possa trasformarsi in    &#13;
 arbitrio,  che  la  legge  determini  direttamente  l'oggetto   della    &#13;
 prestazione  stessa  ed i criteri per quantificarla (sentenze nn. 127    &#13;
 del 1988, 290 del 1987, 34 del 1986, 257 del 1982, 29  del  1979,  67    &#13;
 del 1973, 56 del 1972, 65 del 1962);                                     &#13;
      che,  applicandosi  tali  principi  alle sanzioni amministrative    &#13;
 come quella prevista dalla  norma  impugnata,  la  questione  non  è    &#13;
 fondata  perché  tale  norma,  diversamente  da  quanto ritenuto dal    &#13;
 giudice rimettente,  indica  gli  elementi  necessari  e  sufficienti    &#13;
 proprio  per  evitare arbitrii dell'amministrazione, facendo espresso    &#13;
 riferimento, quale base di calcolo della sanzione, al "valore"  delle    &#13;
 piante  di olivo illegittimamente abbattute, ovverosia ad un dato non    &#13;
 arbitrario ma ricavabile dalle leggi di mercato, in modo da  modulare    &#13;
 l'entità    della   sanzione   al   disvalore   economico   connesso    &#13;
 all'infrazione, con un accertamento dal quale esula qualsiasi profilo    &#13;
 di discrezionalità;                                                     &#13;
      che, per quanto concerne la questione sub c), non può ritenersi    &#13;
 violato  il  principio  di  eguaglianza  per  effetto   della   norma    &#13;
 censurata,  la  quale,  nell'indicare il modo di determinazione della    &#13;
 sanzione, fa riferimento ad un criterio che si giustifica proprio  in    &#13;
 relazione  al  diverso  valore economico dell'albero di olivo e della    &#13;
 sua produzione nelle varie zone del Paese;                               &#13;
      che, anche la questione sub d) non è fondata perché,  come  è    &#13;
 stato già riconosciuto da questa Corte con riferimento a fattispecie    &#13;
 penali,  la  pena  pecuniaria  fissa, commisurata cioè al valore del    &#13;
 bene oggetto di tutela, è compatibile con il principio di  legalità    &#13;
 "senza  che  a  tale  sistema  si  oppongano precetti costituzionali"    &#13;
 (sentenze nn. 50 del 1980, 167 del 1971, 67 del 1963, 15 del 1962);      &#13;
      che  a  identiche  conclusioni  deve  pervenirsi  anche  per  le    &#13;
 sanzioni  amministrative  c.d. rigide, perché, una volta che sotto i    &#13;
 già esaminati profili il  criterio  di  determinazione  non  risulta    &#13;
 ingiustificato,  rispetto  ad  esse  non  può  essere invocato, come    &#13;
 invece richiede l'ordinanza di rimessione, l'art. 11 della  legge  24    &#13;
 novembre 1981 n. 689, che consente al giudice di graduare la sanzione    &#13;
 secondo  i  criteri  ivi indicati, essendo tale previsione ovviamente    &#13;
 dettata soltanto per le sanzioni amministrative fissate tra un limite    &#13;
 minimo e un limite massimo;                                              &#13;
      che, quindi, di fronte ad  una  sanzione  pari  al  decuplo  del    &#13;
 valore  del  bene,  il  giudice  dovrà, usando gli strumenti offerti    &#13;
 dall'art.  23  della  citata  legge  n.  689  del   1981,   valutare,    &#13;
 adeguatamente  motivando  il suo giudizio, la congruenza della "base"    &#13;
 di determinazione della  sanzione,  ovverosia  del  valore  del  bene    &#13;
 oggetto  di  tutela,  senza  che  ciò rappresenti un limite alla sua    &#13;
 funzione, né all'obbligo di motivazione previsto dall'art. 111 della    &#13;
 Costituzione, perché tale obbligo  sussiste  ovviamente  nell'ambito    &#13;
 che  la  legge,  che  prevede  l'illecito e la sanzione, riserva alle    &#13;
 valutazioni e all'apprezzamento del giudice;  e  tale  ambito,  nella    &#13;
 specie,  è costituito dal presupposto assunto a base del calcolo per    &#13;
 la quantificazione della sanzione fissa prevista dalla  legge  in  un    &#13;
 multiplo del valore delle piante abbattute.                              &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle Norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza delle questioni di legittimità    &#13;
 costituzionale dell'art. 4 del decreto legislativo luogotenenziale 27    &#13;
 luglio 1945 n. 475 (Divieto  di  abbattimento  di  alberi  di  olivo)    &#13;
 sollevate, in riferimento agli artt. 3, 23, 25, secondo comma, e 111,    &#13;
 primo  comma,  della  Costituzione,  dal  Pretore  di Foggia, sezione    &#13;
 distaccata di Trinitapoli, con l'ordinanza indicata in epigrafe.         &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, 20 maggio 1992.                                  &#13;
                       Il Presidente: CORASANITI                          &#13;
                       Il redattore: CAIANIELLO                           &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 3 giugno 1992.                           &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
