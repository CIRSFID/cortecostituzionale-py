<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>152</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:152</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/04/1999</data_decisione>
    <data_deposito>30/04/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 34 del codice    &#13;
 di procedura penale, promosso con ordinanza emessa  il  19  settembre    &#13;
 1997  dal  pretore  di Livorno nel procedimento penale a carico di Z.    &#13;
 A. ed altri, iscritta  al  n.  880  del  registro  ordinanze  1997  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica, prima serie    &#13;
 speciale, dell'anno 1997.                                                &#13;
   Udito nella camera di  consiglio  del  24  marzo  1999  il  giudice    &#13;
 relatore Guido Neppi Modona.                                             &#13;
   Ritenuto  che  con  ordinanza  del  19 settembre 1997 il pretore di    &#13;
 Livorno ha  sollevato,  in  riferimento  agli  artt.  3  e  77  della    &#13;
 Costituzione,  questione  di legittimità costituzionale dell'art. 34    &#13;
 del codice di procedura  penale,  nella  parte  in  cui  non  prevede    &#13;
 l'incompatibilità   ad   esercitare   le  funzioni  di  giudice  del    &#13;
 dibattimento del giudice per le indagini  preliminari  che  abbia  in    &#13;
 precedenza pronunciato nei confronti dei medesimi imputati decreto di    &#13;
 archiviazione  "parziale"  ai  sensi dell'art. 411 cod. proc. pen. in    &#13;
 relazione ad alcuni reati "concorrenti";                                 &#13;
     che il rimettente premette: di avere pronunciato, in qualità  di    &#13;
 giudice  per  le  indagini  preliminari, decreto di archiviazione per    &#13;
 intervenuta prescrizione in relazione ai reati di lesioni volontarie,    &#13;
 oltraggio a pubblico ufficiale e  inosservanza  di  un  provvedimento    &#13;
 dell'Autorità;  di  avere poi ordinato la restituzione degli atti al    &#13;
 pubblico ministero per l'ulteriore corso in relazione al  delitto  di    &#13;
 resistenza  a  pubblico ufficiale, commesso nello stesso contesto dai    &#13;
 medesimi imputati; di essere ora chiamato a giudicare tale  reato  in    &#13;
 qualità  di giudice del dibattimento, pur avendo in precedenza preso    &#13;
 visione di tutti gli atti di indagine  e,  quindi,  anche  di  quelli    &#13;
 relativi all'odierna imputazione;                                        &#13;
     che  ad  avviso  del  rimettente  la  ratio dell'incompatibilità    &#13;
 determinata  da  atti  compiuti  nel  procedimento   va   individuata    &#13;
 nell'esigenza  di  evitare che "il giudice del merito abbia avuto, in    &#13;
 precedenza, conoscenza degli elementi d'accusa relativi al  reato  di    &#13;
 cui  è  chiamato a conoscere e giudicare", nonché di scongiurare il    &#13;
 rischio  di  una  sorta  di  "pregiudizio"  da  parte del giudice del    &#13;
 dibattimento;                                                            &#13;
     che l'omessa previsione nel caso in esame di  una  situazione  di    &#13;
 incompatibilità  si  porrebbe  pertanto  in  contrasto con l'art. 77    &#13;
 Cost., per violazione della direttiva n. 67 della legge delega  -  in    &#13;
 relazione  alla  direttiva n. 52 - stante l'assimilazione del decreto    &#13;
 di archiviazione ex art. 411 cod. proc. pen.  alla  sentenza  di  non    &#13;
 luogo  a  procedere  per  estinzione  del reato, nonché con l'art. 3    &#13;
 della Costituzione per l'ingiustificata disparità di trattamento tra    &#13;
 "situazioni giuridiche processuali identiche".                           &#13;
   Considerato  che,  contrariamente  a  quanto  sostiene  il  giudice    &#13;
 rimettente,  la mera conoscenza degli atti del medesimo procedimento,    &#13;
 non accompagnata da una valutazione contenutistica,  di  merito,  sui    &#13;
 risultati delle indagini, non ha effetti pregiudicanti sulla funzione    &#13;
 di  giudizio  (v. in tale senso sentenze n. 455 del 1994 e n. 502 del    &#13;
 1991);                                                                   &#13;
     che inoltre, alla stregua della costante giurisprudenza di questa    &#13;
 Corte,  l'atto  pregiudicante  che   comporta   una   situazione   di    &#13;
 incompatibilità  per la funzione di giudizio deve comunque avere per    &#13;
 oggetto la medesima res iudicanda (v. sentenze nn. 455 del 1994,  439    &#13;
 del 1993, 186 e 124 del 1992);                                           &#13;
     che  nel caso di specie il rimettente, in qualità di giudice per    &#13;
 le indagini preliminari, ha pronunciato decreto di archiviazione  per    &#13;
 intervenuta  prescrizione nei confronti di reati diversi da quello su    &#13;
 cui è chiamato a svolgere la funzione di giudizio, e si è  limitato    &#13;
 a  prendere  visione  degli  atti di indagine, senza esprimere alcuna    &#13;
 valutazione contenutistica, di merito, sui risultati  delle  indagini    &#13;
 stesse;                                                                  &#13;
     che la questione va pertanto dichiarata manifestamente infondata.    &#13;
   Visti  gli  artt.26,  secondo  comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale dell'art. 34 del codice di procedura penale sollevata,    &#13;
 in  riferimento  agli artt. 3 e 77 della Costituzione, dal pretore di    &#13;
 Livorno, con l'ordinanza in epigrafe.                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 26 aprile 1999.                               &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore: Neppi Modona                          &#13;
                       Il cancelliere: Fruscella                          &#13;
   Depositata in cancelleria il 30 aprile 1999.                           &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
