<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2007</anno_pronuncia>
    <numero_pronuncia>30</numero_pronuncia>
    <ecli>ECLI:IT:COST:2007:30</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>24/01/2007</data_decisione>
    <data_deposito>06/02/2007</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA INCIDENTALE</tipo_procedimento>
    <dispositivo>manifesta infondatezza</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO, Paolo Maria NAPOLITANO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'articolo 163, comma 3, del regio decreto 18 giugno 1931, n. 773 (Approvazione del testo unico delle leggi di pubblica sicurezza), promosso con ordinanza del 17 dicembre 2004 dal Tribunale di Bassano del Grappa, nel procedimento penale a carico di R. M., iscritta al n. 183 del registro ordinanze 2005 e pubblicata nella Gazzetta Ufficiale della Repubblica n. 15, prima serie speciale, dell'anno 2005. &#13;
    Visto l'atto di intervento del Presidente del Consiglio dei ministri; &#13;
      udito nella camera di consiglio del 10 gennaio 2007 il Giudice relatore Ugo De Siervo. &#13;
    Ritenuto che, con ordinanza pronunciata il 17 dicembre 2004 e pervenuta a questa Corte il 16 marzo 2005, il Tribunale di Bassano del Grappa in composizione monocratica, chiamato a pronunciarsi in sede dibattimentale sulla responsabilità penale di imputato per il reato previsto dall'art. 163, comma 3 (recte: comma 4), del regio decreto 18 giugno 1931, n. 773 (Approvazione del testo unico delle leggi di pubblica sicurezza), ha sollevato in via incidentale questione di legittimità costituzionale della richiamata disposizione, per contrasto con l'art. 3 della Costituzione; &#13;
    che la norma in oggetto punisce con l'arresto da uno a sei mesi coloro che, rimpatriati con foglio di via obbligatorio, omettono di presentarsi nel termine ivi prescritto all'autorità di pubblica sicurezza indicata nel foglio di via; &#13;
    che il giudice a quo premette che l'imputato, presa conoscenza del foglio di via il 10 marzo 2004, si presentò presso il Comune di Pozzoleone, ove era stato rimpatriato, alle ore 12,10 dell'11 marzo 2004, anziché alle ore 10,00 di quello stesso giorno, come espressamente prescritto dall'ordine di rimpatrio; &#13;
    che tale ritardo di un'ora e 55 minuti - espone il remittente - conseguì alla decisione dell'imputato, tossicodipendente in cura presso il SERT di Bolzano, di recarvisi nella prima mattina dell'11 marzo 2004 per ricevere una dose di metadone, per poi prendere il treno verso Pozzoleone alle ore 8,15; &#13;
    che, tuttavia, siffatta circostanza non integrerebbe gli estremi di «nessuna possibile causa di giustificazione o comunque di esclusione dell'elemento psicologico», poiché l'imputato avrebbe potuto proseguire la terapia presso «qualunque altro SERT», ponendosi quindi in viaggio tempestivamente, per osservare il termine assegnatoli; &#13;
    che, pertanto, sussisterebbe l'ipotesi di reato contestata, poiché la disposizione impugnata assoggetterebbe a sanzione penale «anche un ritardo di pochi minuti»; &#13;
    che tale contenuto normativo pare al giudice a quo in contrasto con l'art. 3 della Costituzione per un duplice profilo; &#13;
    che, in primo luogo, sarebbe irragionevole sanzionare in termini identici due fattispecie «così diverse» come il ritardo di «uno o più giorni» e quello invece contenuto in un breve lasso di tempo, tale da difettare di «una componente minima» di offensività, e da meritare, piuttosto, di essere configurato quale illecito amministrativo; &#13;
    che, in secondo luogo, il legislatore avrebbe introdotto una disparità di trattamento tra fattispecie analoghe, posto che gli artt. 30, comma 3, 30-ter, comma 6, e 51 della legge 26 luglio 1975, 354 (Norme sull'ordinamento penitenziario e sull'esecuzione delle misure privative e limitative della libertà), relativi ai permessi, ai permessi premio e al regime di semilibertà, prevedono che l'omesso rientro in istituto penitenziario per un tempo non superiore alle dodici ore successive alla scadenza del termine non abbia rilevanza penale, ma unicamente disciplinare; &#13;
    che è intervenuto in giudizio il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura Generale dello Stato, chiedendo che la questione sia dichiarata infondata; &#13;
    che, secondo l'Avvocatura, rientrerebbe nella discrezionalità legislativa «accomunare quoad poenam situazioni di ritardo» da stimarsi identiche, rispetto «all'interesse costituito dall'ordine di rientro»; &#13;
    che, nel caso di specie, peraltro, l'assenza di un SERT presso il Comune di Pozzoleone, ove proseguire la terapia con metadone, integrerebbe a parere dell'Avvocatura una «causa soggettiva esimente»; &#13;
    che, infine, le fattispecie poste a raffronto dal giudice a quo sarebbero meramente «assimilabili» e non «uguali soggettivamente ed oggettivamente». &#13;
    Considerato che il Tribunale di Bassano del Grappa dubita della legittimità costituzionale dell'art. 163, comma 4, del regio decreto 18 giugno 1931, n. 773 (Approvazione del testo unico delle leggi di pubblica sicurezza), in relazione all'art. 3 della Costituzione; &#13;
    che, secondo il remittente, tale disposizione incriminatrice avrebbe irragionevolmente equiparato condotte meritevoli di sanzione penale (ove appaia apprezzabile il ritardo con cui il soggetto raggiunto da foglio di via obbligatorio si presenta all'autorità di pubblica sicurezza) e condotte, viceversa, prive di offensività (ove tale ritardo sia minimo e comunque contenuto in poche ore); &#13;
    che, secondo la costante giurisprudenza di questa Corte (ex plurimis, ordinanze n. 212 e n. 158 del 2004), il potere di configurare le ipotesi criminose e di determinare la pena per ciascuna di esse rientra nell'ambito della discrezionalità del legislatore: discrezionalità che può essere censurata, in sede di sindacato di costituzionalità, nella sola ipotesi in cui sia esercitata in modo manifestamente irrazionale; &#13;
    che, sul piano della formulazione legislativa della disposizione incriminatrice, non può ritenersi manifestamente irrazionale l'assoggettamento a sanzione penale di una condotta normativamente qualificata in base all'inosservanza di un unico termine, sancito per verificare il rispetto dell'ordine recato dal foglio di via; &#13;
    che, peraltro, spetta al giudice ordinario verificare se la condotta così realizzata, per quanto conforme all'astratto modello punitivo delineato dal legislatore, appaia tuttavia, nella sua specifica concretezza, assolutamente inidonea a porre a repentaglio il bene giuridico tutelato, posto che l'art. 25 della Costituzione «quale risulta dalla lettura sistematica a cui fanno da sfondo […] l'insieme dei valori connessi alla dignità umana, postula […] un ininterrotto operare del principio di offensività dal momento della astratta predisposizione normativa a quello della applicazione concreta da parte del giudice, con conseguente distribuzione dei poteri conformativi tra giudice delle leggi e autorità giudiziaria, alla quale soltanto compete di impedire, con un prudente apprezzamento della lesività in concreto, una arbitraria ed illegittima dilatazione della sfera dei fatti da ricondurre al modello legale» (sentenza n. 263 del 2000); &#13;
    che, quanto poi alla denunciata disparità di trattamento ravvisata dal remittente tra l'incriminazione prevista dalla norma oggetto e l'adozione della sola sanzione disciplinare nei casi di cui agli artt. 30, comma 3, 30-ter, comma 6, e 51 della legge 26 luglio 1975, n. 354, si è in presenza di fattispecie non omogenee (concernendo queste ultime il regime di trattamento del detenuto, da collocarsi nella articolata trama dell'ordinamento penitenziario), che il legislatore resta costituzionalmente libero di differenziare conseguentemente, nell'esercizio della propria, non manifestamente irragionevole, discrezionalità; &#13;
    che la questione va dichiarata, pertanto, manifestamente infondata. &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo 1953, n. 87, e 9, comma 2, delle norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara la manifesta infondatezza della questione di legittimità costituzionale dell'art. 163, comma 4, del regio decreto 18 giugno 1931, n. 773 (Approvazione del testo unico delle leggi di pubblica sicurezza), sollevata, in riferimento all'art. 3 della Costituzione, dal Tribunale di Bassano del Grappa con l'ordinanza indicata in epigrafe. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 24 gennaio 2007.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Ugo DE SIERVO, Redattore  &#13;
Giuseppe DI PAOLA, Cancelliere  &#13;
Depositata in Cancelleria il 6 febbraio 2007.  &#13;
Il Direttore della Cancelleria  &#13;
F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
