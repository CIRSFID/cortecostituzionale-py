<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1968</anno_pronuncia>
    <numero_pronuncia>51</numero_pronuncia>
    <ecli>ECLI:IT:COST:1968:51</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SANDULLI</presidente>
    <relatore_pronuncia>Giovanni Battista Benedetti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/05/1968</data_decisione>
    <data_deposito>27/05/1968</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. ALDO SANDULLI, Presidente - Prof. &#13;
 BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. GIUSEPPE BRANCA - &#13;
 Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE &#13;
 CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - &#13;
 Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO &#13;
 DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. &#13;
 VINCENZO MICHELE TRIMARCHI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  degli  artt.  16  del  &#13;
 D.P.R.  4  giugno  1966,  n.  332, recante concessione di amnistia e di  &#13;
 indulto, e 16 della  legge  di  delegazione  3  giugno  1966,  n.  331,  &#13;
 promosso con ordinanza emessa il 10 giugno 1966 dal pretore di Galatina  &#13;
 nel  procedimento  penale a carico di Calabrese Antonio, iscritta al n.  &#13;
 155 del Registro ordinanze 1966 e pubblicata nella  Gazzetta  Ufficiale  &#13;
 della Repubblica n.  226 del 10 settembre 1966.                          &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 Ministri;                                                                &#13;
     udita nell'udienza pubblica del 1  aprile  1968  la  relazione  del  &#13;
 Giudice Giovanni Battista Benedetti;                                     &#13;
     udito il sostituto avvocato generale dello Stato financo Chiarotti,  &#13;
 per il Presidente del Consiglio dei Ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso del procedimento a carico di Calabrese Antonio, imputato  &#13;
 del reato di lesioni colpose  e  di  contravvenzioni  al  Codice  della  &#13;
 strada,  il  pretore di Galatina, pur considerando che i reati commessi  &#13;
 dall'imputato l'11 ottobre 1965 rientravano nel  termine  di  efficacia  &#13;
 (31  gennaio  1966)  previsto dall'art. 16 del D.P.R. 4 giugno 1966, n.  &#13;
 332, recante concessione di amnistia e di indulto, e dal corrispondente  &#13;
 articolo  della  legge  di  delegazione  3  giugno  1966,  n.  331,  ha  &#13;
 soprasseduto  all'applicazione  del provvedimento di clemenza rilevando  &#13;
 che  le  citate  norme  contenenti  il  termine  di  efficacia     sono  &#13;
 costituzionalmente  illegittime  in  riferimento  all'art.  79,  ultima  &#13;
 parte, della Costituzione ai sensi del quale l'amnistia e l'indulto non  &#13;
 possono applicarsi ai reati commessi successivamente alla  proposta  di  &#13;
 delegazione.                                                             &#13;
     Nel  ricordare  le  vicende  parlamentari  del  decreto di amnistia  &#13;
 l'ordinanza precisa che  al  Senato  vennero  presentate  tre  distinte  &#13;
 proposte:  la  prima in data 13 maggio 1965 del Senatore Perugini (doc.  &#13;
 1178); la seconda in data 29 maggio 1965 del  Senatore  Nencioni  (doc.  &#13;
 1225)  e la terza in data 8 marzo 1966 del Senatore Tomassini e altri -  &#13;
 (doc. 1577).                                                             &#13;
     La  prima  fu  poi  ritirata  dal  presentatore mentre le altre due  &#13;
 passarono alla Commissione giustizia in sede referente.                  &#13;
     La Commissione,  accogliendo  la  raccomandazione  del  Governo  di  &#13;
 contenere  in limiti rigorosi il provvedimento di clemenza, nominò una  &#13;
 Sottocommissione per la elaborazione di un nuovo testo  autorizzando  i  &#13;
 relatori a riferire in aula nel senso di non prendere in considerazione  &#13;
 le  precedenti  proposte  n.  1225  e  1577.  In  esecuzione di ciò la  &#13;
 Sottocommissione predispose il testo sottoscritto dal Senatore Monni  e  &#13;
 da  altri  15 Senatori che fu poi presentato alla Presidenza del Senato  &#13;
 come autonomo disegno di legge (doc. n. 1654 del 5 maggio  1966).  Tale  &#13;
 disegno (che fissava l'efficacia del provvedimento di clemenza ai reati  &#13;
 commessi  fino  al 31 dicembre 1965), approvato prima dalla Commissione  &#13;
 giustizia,  passò  poi  in  aula  ove  venne  approvato   con   alcuni  &#13;
 emendamenti,  tra  cui  quello  che spostava il termine di efficacia ai  &#13;
 reati commessi fino a tutto il giorno 31 gennaio 1966.                   &#13;
     Ciò premesso il pretore  osserva  che  il  testo  elaborato  dalla  &#13;
 Sottocommissione  non è altro che un testo unificato ed emendato delle  &#13;
 proposte n. 1225 e 1577 le quali sarebbero quindi rimaste alla base del  &#13;
 processo di formazione della legge di delegazione n. 331 del 1966.    E  &#13;
 poiché la prima delle citate proposte porta la data del 29 maggio 1965  &#13;
 il termine di efficacia del provvedimento definitivo non avrebbe potuto  &#13;
 comprendere  resti  commessi  successivamente  a  tale  data.  Da  ciò  &#13;
 l'illegittimità costituzionale dell'art. 16 della legge di delegazione  &#13;
 e del corrispondente articolo del  decreto  presidenziale  di  clemenza  &#13;
 che, estendendo il termine di efficacia ai reati commessi al 31 gennaio  &#13;
 1966, avrebbero violato l'art. 79, ultima parte, della Costituzione che  &#13;
 vieta  espressamente  di  estendere  l'amnistia  e  l'indulto  ai reati  &#13;
 commessi successivamente alla proposta di delegazione.                   &#13;
     L'ordinanza,  ritualmente  comunicata  e   notificata,   è   stata  &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della  Repubblica n. 226 del 10  &#13;
 settembre 1966.                                                          &#13;
     Nel presente giudizio è intervenuto il  Presidente  del  Consiglio  &#13;
 dei  Ministri,  rappresentato e difeso dall'Avvocatura dello Stato, con  &#13;
 atto depositato in cancelleria il 28 luglio 1966.                        &#13;
     Nell'atto d'intervento l'Avvocatura osserva che - come ha ricordato  &#13;
 lo stesso pretore e come chiaramente emerge dai lavori preparatori - la  &#13;
 Commissione giustizia, dopo aver deliberato di non prendere in esame le  &#13;
 precedenti  proposte,  dette  l'incarico  ad  una  Sottocommissione  di  &#13;
 redigere  una autonoma proposta di legge, firmata dal Senatore Monni ed  &#13;
 altri, che fu presentata alla Presidenza del Senato il 5 maggio 1966 ed  &#13;
 approvata in sede referente dalla Commissione  giustizia  nello  stesso  &#13;
 giorno. Successivamente l'Assemblea deliberò sulla stessa proposta del  &#13;
 Senatore  Monni  concedendo  la  delega  richiesta. Da ciò consegue la  &#13;
 legittimità della data del 31 gennaio 1966 stabilita  nella  legge  di  &#13;
 delegazione e nel provvedimento di clemenza.                             &#13;
     Ma a parte ciò, l'Avvocatura rileva che non è dato riferirsi alla  &#13;
 data  della  proposta  del  Senatore  Nencioni (doc. 1225 del 29 maggio  &#13;
 1965) come  termine  di  efficacia  dell'amnistia,  in  quanto  a  tale  &#13;
 proposta   il   Nencioni  implicitamente  rinunciò  sottoscrivendo  la  &#13;
 proposta del Senatore Monni tanto diversa dalla sua.                     &#13;
     Chiede, pertanto, che la Corte voglia  dichiarare  non  fondata  la  &#13;
 proposta questione di legittimità costituzionale.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  questione  di  legittimità  costituzionale  dell'art. 16 della  &#13;
 legge 3 giugno 1966,  n.  331,  recante  "Delega  al  Presidente  della  &#13;
 Repubblica  per  la  concessione  di  amnistia  e  di  indulto"  e  del  &#13;
 corrispondente art. 16 del D.P.R. 4 giugno 1966, n. 332, in riferimento  &#13;
 all'art.  79, comma secondo, della Costituzione, non è fondata.         &#13;
     L'ordinanza  del  pretore  muove  dall'erroneo  presupposto  che  i  &#13;
 disegni  di  legge  che  dettero luogo ai provvedimenti impegnati siano  &#13;
 quelli del 29 maggio 1965, n. 1225, d'iniziativa del senatore  Nencioni  &#13;
 e dell'8 marzo 1966, n. 1577, d'iniziativa del senatore Tomassini e che  &#13;
 le norme denunciate, estendendo il termine di efficacia dell'amnistia e  &#13;
 dell'indulto  ad  una  data  (31  gennaio 1966) posteriore a quella del  &#13;
 primo disegno, siano pertanto in contrasto con il  richiamato  precetto  &#13;
 costituzionale  che  fa espresso divieto di applicare detti benefici ai  &#13;
 reati commessi successivamente alla proposta di delegazione.             &#13;
     Dagli atti parlamentari risulta invece che la legge 3 giugno  1966,  &#13;
 n.  331, trasse origine esclusivamente dal disegno di legge n. 1654 del  &#13;
 5 maggio 1966 elaborato  da  una  Sottocommissione,  all'uopo  nominata  &#13;
 dalla  Commissione  giustizia, e sottoscritto dal senatore Monni, dallo  &#13;
 stesso Nencioni e da altri 14 senatori.                                  &#13;
     Infondato è il rilievo che quest'ultima  proposta  di  delegazione  &#13;
 sia  un  testo  emendato  ed  unificato  delle  prime due. Ed invero la  &#13;
 differenza tra le disposizioni della proposta di legge Nencioni -  alla  &#13;
 quale  soltanto  occorre  fare  riferimento  ai fini della questione di  &#13;
 legittimità costituzionale - e le disposizioni della proposta Monni è  &#13;
 così profonda da escludere che  questa  possa  considerarsi  un  testo  &#13;
 emendato della prima.                                                    &#13;
     A  parte  tale  considerazione,  dagli  atti  parlamentari  risulta  &#13;
 altresì che nella seduta del 5 maggio 1966  la  Commissione  giustizia  &#13;
 deliberò di non prendere in considerazione le due precedenti proposte.  &#13;
 Può   perciò   affermarsi  che  esse  restarono  completamente  fuori  &#13;
 dell'iter dell'esame parlamentare che, svoltosi unicamente sul  disegno  &#13;
 di  legge  di  iniziativa del senatore Monni, condusse all'approvazione  &#13;
 della legge di delegazione.  E  poiché  questo  disegno  di  legge  fu  &#13;
 comunicato alla Presidenza del Senato il 5 maggio 1966, evidente appare  &#13;
 la  legittimità  del termine di efficacia dei benefici dell'amnistia e  &#13;
 dell'indulto stabilito al 31 gennaio 1966 dalle norme impugnate.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata, in riferimento all'art.  79,  comma  secondo,  &#13;
 della  Costituzione,  la  questione  proposta con l'ordinanza 10 giugno  &#13;
 1966  dal  pretore  di  Galatina  sulla   legittimità   costituzionale  &#13;
 dell'art.    16  della  legge 3 giugno 1966, n. 331, recante "Delega al  &#13;
 Presidente della  Repubblica  per  la  concessione  di  amnistia  e  di  &#13;
 indulto", e dell'art. 16 del D.P.R. 4 giugno 1966, n. 332, contenente "  &#13;
 Concessione di amnistia e di indulto".                                   &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 9 maggio 1968.                                &#13;
                                   ALDO SANDULLI - BIAGIO  PETROCELLI  -  &#13;
                                   ANTONIO  MANCA  -  GIUSEPPE  BRANCA -  &#13;
                                   MICHELE FRAGALI - COSTANTINO  MORTATI  &#13;
                                   -   GIUSEPPE   CHIARELLI  -  GIUSEPPE  &#13;
                                    VERZÌ - GIOVANNI BATTISTA  BENEDETTI  &#13;
                                   -  FRANCESCO  PAOLO BONIFACIO - LUIGI  &#13;
                                   OGGIONI  -  ANGELO  DE MARCO - ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE TRIMARCHI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
