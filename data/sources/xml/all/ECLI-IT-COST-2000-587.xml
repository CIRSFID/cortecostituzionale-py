<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>587</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:587</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Gustavo Zagrebelsky</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/12/2000</data_decisione>
    <data_deposito>29/12/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
 Valerio ONIDA, Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 120, comma 1,    &#13;
 e  130,  comma 1, lettera b), del decreto legislativo 30 aprile 1992,    &#13;
 n. 285  (Nuovo codice della strada), promosso con ordinanza emessa il    &#13;
 18 aprile 2000 dal Tribunale amministrativo regionale del Molise, sul    &#13;
 ricorso  proposto  da  Giuseppe  Rinalducci  contro  il  prefetto  di    &#13;
 Campobasso ed altri, iscritta al n. 536 del registro ordinanze 2000 e    &#13;
 pubblicata  nella Gazzetta Ufficiale della Repubblica n. 41, 1ª serie    &#13;
 speciale, dell'anno 2000.                                                &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 ministri;                                                                &#13;
     Udito  nella  camera di consiglio del 30 novembre 2000 il giudice    &#13;
 relatore Gustavo Zagrebelsky.                                            &#13;
     Ritenuto  che  con  ordinanza  del  18  aprile  2000 il Tribunale    &#13;
 amministrativo regionale del Molise ha sollevato, in riferimento agli    &#13;
 artt.  3,  16,  76 e 97 della Costituzione, questione di legittimità    &#13;
 costituzionale  degli artt. 120, comma 1, e 130, comma 1, lettera b),    &#13;
 del  decreto  legislativo  30 aprile 1992, n. 285 (Nuovo codice della    &#13;
 strada),  nella  parte  in  cui prevedono la revoca della patente, da    &#13;
 parte  del  prefetto,  per  coloro che sono sottoposti alla misura di    &#13;
 prevenzione del foglio di via obbligatorio prevista dall'art. 2 della    &#13;
 legge 27 dicembre 1956, n. 1423;                                         &#13;
         che nell'ordinanza di rimessione si riferisce che il giudizio    &#13;
 principale concerne l'impugnazione di un provvedimento prefettizio di    &#13;
 revoca  della  patente di guida, adottato in data 27 gennaio 1998 nei    &#13;
 riguardi  di  persona  sottoposta  nella  stessa data alla misura del    &#13;
 foglio di via obbligatorio con provvedimento del questore;               &#13;
         che  il  tribunale  amministrativo  rimettente,  premesso  il    &#13;
 carattere vincolato e non discrezionale del provvedimento di revoca e    &#13;
 dunque  ritenuta  la  rilevanza della questione sollevata (essendo il    &#13;
 ricorso,  allo  stato  della  legislazione, da rigettare), osserva in    &#13;
 primo  luogo che le norme sulla base delle quali è stato adottato il    &#13;
 provvedimento  di  revoca  sono  contenute  in un testo avente natura    &#13;
 regolamentare  sprovvisto di forza di legge, e precisamente il d.P.R.    &#13;
 19 aprile 1994, n. 575, emanato in attuazione della "delegificazione"    &#13;
 prevista  dall'art.  2,  commi  7  e 8, della legge 24 dicembre 1993,    &#13;
 n. 537;   tuttavia,   aggiunge   il  rimettente,  la  questione  può    &#13;
 ugualmente  essere  sottoposta alla Corte costituzionale, giacché la    &#13;
 prevista  emanazione  del  regolamento di delegificazione riguardava,    &#13;
 secondo  la  citata  legge  n. 537,  esclusivamente la disciplina del    &#13;
 procedimento  e  non abilitava il Governo a dettare norme concernenti    &#13;
 gli  aspetti  sostanziali  della  materia:  deve  perciò ritenersi -    &#13;
 osserva  su questo aspetto il tribunale amministrativo - che la nuova    &#13;
 formulazione  regolamentare  della  disciplina  sia  priva di effetto    &#13;
 innovativo per quanto riguarda le condizioni sostanziali della revoca    &#13;
 e  che  non si sia verificato l'effetto abrogativo delle disposizioni    &#13;
 sostanziali  di  rango  legislativo preesistenti (artt. 120 e 130 del    &#13;
 codice  della  strada),  disposizioni  nelle  quali il caso di specie    &#13;
 trova   tuttora   la  propria  disciplina  e  delle  quali  può,  in    &#13;
 conclusione, demandarsi il controllo di conformità a Costituzione;      &#13;
         che,  tutto  ciò  premesso,  il giudice rimettente deduce in    &#13;
 primo   luogo   il   profilo  della  violazione  dell'art.  76  della    &#13;
 Costituzione,   perché,  alla  stregua  di  quanto  affermato  nella    &#13;
 sentenza  n. 354 del 1998 della Corte costituzionale circa la portata    &#13;
 "minimale"  della  delega  conferita  con l'art. 2, lettera t), della    &#13;
 legge  13 giugno 1991, n. 190, che non consentiva l'adozione di norme    &#13;
 innovative  rispetto  al  sistema  preesistente,  la previsione della    &#13;
 revoca  della  patente  nel  caso  di  sottoposizione a foglio di via    &#13;
 obbligatorio, ex art. 2 della legge n. 1423 del 1956, in quanto priva    &#13;
 di  riscontro  nella  legislazione  preesistente,  deve  ritenersi in    &#13;
 contrasto  con  i  limiti posti nella legge di delegazione e pertanto    &#13;
 con l'art. 76 della Costituzione;                                        &#13;
         che ulteriori profili di incostituzionalità della disciplina    &#13;
 sono  poi  ravvisati  dal rimettente in riferimento: all'art. 3 della    &#13;
 Costituzione, per difetto di proporzionalità tra la revoca e il tipo    &#13;
 di  misura  che ne è causa, essendo il foglio di via obbligatorio un    &#13;
 provvedimento   che   non   priva   il  soggetto  della  libertà  di    &#13;
 circolazione   e  che  non  costituisce  un  fattore  di  menomazione    &#13;
 dell'individuo,  nonché  per irragionevolezza dell'assimilazione del    &#13;
 foglio di via alle altre eterogenee e più gravi misure che impongono    &#13;
 o  consentono  la  revoca; all'art. 16 della Costituzione, perché la    &#13;
 titolarità  della  patente  viene  a  dipendere  da  una valutazione    &#13;
 compiuta  dall'autorità  di polizia e non da un accertamento in sede    &#13;
 giurisdizionale;   infine,   all'art.   97  della  Costituzione,  per    &#13;
 l'impossibilità  di  una  valutazione caso per caso e per il vincolo    &#13;
 all'emanazione  dell'atto da parte del prefetto, in conseguenza di un    &#13;
 provvedimento   adottato   ad   altri   fini   da   altra   autorità    &#13;
 amministrativa;                                                          &#13;
         che  è  intervenuto  in giudizio il Presidente del Consiglio    &#13;
 dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello    &#13;
 Stato,  secondo cui la questione presenterebbe aspetti di irrilevanza    &#13;
 -  per non avere il rimettente chiarito se il provvedimento di revoca    &#13;
 sia  stato  nella  specie  adottato  prima  o  dopo  l'intervento  di    &#13;
 "delegificazione"  e  pertanto  se  la  questione sia riferibile alle    &#13;
 norme  nella  loro veste regolamentare, ex d.P.R. n. 575 del 1994 - e    &#13;
 sarebbe  comunque,  nel merito, infondata, come le analoghe questioni    &#13;
 portate in precedenza all'esame della Corte.                             &#13;
     Considerato  che il Tribunale amministrativo regionale del Molise    &#13;
 ha sollevato questione di costituzionalità degli artt. 120, comma 1,    &#13;
 e 130, comma 1, lettera b), del nuovo codice della strada, nella loro    &#13;
 originaria  versione  legislativa  -  e non come sostituiti dal d.P.R    &#13;
 n. 575  del  1994  -  sul  presupposto che essi siano tuttora vigenti    &#13;
 nonostante  la  "delegificazione"  cui  sono  stati sottoposti, nella    &#13;
 parte in cui prevedono la revoca della patente di guida nei confronti    &#13;
 di  chi  sia  soggetto  alla  misura di prevenzione del foglio di via    &#13;
 obbligatorio,  a  norma  dell'art.  2  della  legge n. 1423 del 1956,    &#13;
 ritenendo  che  tale  previsione violi gli artt. 3, 16, 76 e 97 della    &#13;
 Costituzione:  a)  l'art.  3,  per  la  sproporzione tra la causa (la    &#13;
 misura  del  foglio di via obbligatorio) e l'effetto (la revoca della    &#13;
 patente)  e per irragionevolezza dell'assimilazione del foglio di via    &#13;
 alle  più  gravi  misure  che  impongono, o consentono, la revoca in    &#13;
 discorso;  b) l'art. 16, perché la titolarità della patente dipende    &#13;
 da una valutazione dell'autorità di polizia e non da un accertamento    &#13;
 giurisdizionale;  c)  l'art.  76,  per  eccesso di delega, poiché la    &#13;
 norma,  innovativa  rispetto  alla  disciplina anteriore, non sarebbe    &#13;
 stata  consentita  alla  luce  del  carattere "minimale" della delega    &#13;
 conferita al Governo con l'art. 2, lettera t), della legge n. 190 del    &#13;
 1991;  d)  l'art.  97,  per  impossibilità  di  un apprezzamento del    &#13;
 singolo  caso e per il vincolo all'adozione della revoca derivante da    &#13;
 un   provvedimento  adottato,  ad  altri  fini,  da  altra  autorità    &#13;
 amministrativa;                                                          &#13;
         che con la sentenza n. 427 del 2000, successiva all'ordinanza    &#13;
 di  rimessione, questa Corte, pronunciandosi su una questione analoga    &#13;
 -  rimessa  in base al medesimo presupposto della persistente vigenza    &#13;
 degli  impugnati  artt. 120, comma 1, e 130, comma 1, lettera b), del    &#13;
 codice  della  strada  nella loro versione legislativa, nonostante la    &#13;
 loro prevista "delegificazione" - ha già dichiarato l'illegittimità    &#13;
 costituzionale, per violazione dell'art. 76 della Costituzione, delle    &#13;
 anzidette  disposizioni  legislative, in combinato disposto tra loro,    &#13;
 nella  parte  in  cui  prevedono la revoca della patente di guida nei    &#13;
 confronti di coloro che sono sottoposti alla misura del foglio di via    &#13;
 obbligatorio di cui all'art. 2 della legge n. 1423 del 1956 (sentenza    &#13;
 n. 427   del  2000  citata,  punto  6  del  diritto  e  capo  1)  del    &#13;
 dispositivo);                                                            &#13;
         che   pertanto,   essendo  state  le  norme  denunciate  già    &#13;
 dichiarate  incostituzionali  nei termini prospettati dal rimettente,    &#13;
 la  questione  ora  in  esame  deve  essere dichiarata manifestamente    &#13;
 inammissibile.                                                           &#13;
     Visti  gli  artt.  26,  secondo comma, della legge 11 marzo 1953,    &#13;
 n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Dichiara   la   manifesta  inammissibilità  della  questione  di    &#13;
 legittimità costituzionale degli artt. 120, comma 1, e 130, comma 1,    &#13;
 lettera  b),  del  decreto  legislativo 30 aprile 1992, n. 285 (Nuovo    &#13;
 codice  della strada), sollevata, in riferimento agli artt. 3, 16, 76    &#13;
 e  97  della Costituzione, dal Tribunale amministrativo regionale del    &#13;
 Molise con l'ordinanza indicata in epigrafe.                             &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 dicembre 2000.                             &#13;
                      Il Presidente: Santosuosso                          &#13;
                       Il redattore: Zagrebelsky                          &#13;
                       Il cancelliere: Fruscella                          &#13;
     Depositata in cancelleria il 29 dicembre 2000.                       &#13;
                       Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
