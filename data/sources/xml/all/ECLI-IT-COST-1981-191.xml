<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>191</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:191</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Arnaldo Maccarone</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>26/11/1981</data_decisione>
    <data_deposito>17/12/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - Prof. GUGLIELMO ROEHRSSEN - &#13;
 Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO &#13;
 MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO NIACCARONE - Prof. &#13;
 ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, &#13;
 Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 9, comma  &#13;
 terzo, della legge 27 dicembre 1956, n. 1423 (Violazione  delle  misure  &#13;
 di sorveglianza speciale), promosso con ordinanza emessa il 24 febbraio  &#13;
 1975  dal  pretore di San Cipriano Picentino, nel procedimento penale a  &#13;
 carico di Costabile Orlando, iscritta al n. 204 del registro  ordinanze  &#13;
 1975  e pubblicata nella Gazzetta Ufficiale della Repubblica n. 181 del  &#13;
 1975.                                                                    &#13;
     Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nell'udienza pubblica del 7 ottobre 1981 il Giudice relatore  &#13;
 Arnaldo Maccarone;                                                       &#13;
     udito  l'avvocato  dello  Stato Giorgio Azzariti, per il Presidente  &#13;
 del Consiglio dei ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     I carabinieri  di  Giffoni  Vallepiana,  avendo  accertato  che  il  &#13;
 sorvegliato speciale Orlando Costabile aveva violato la prescrizione di  &#13;
 "non  rincasare  la  sera  più tardi delle ore 21", procedevano al suo  &#13;
 arresto il 13 febbraio 1975, ai sensi dell'art.  9, terzo comma,  della  &#13;
 legge 27 dicembre 1956, n. 1423.                                         &#13;
     Dispone invero tale norma che:                                       &#13;
     "Il   contravventore   agli  obblighi  inerenti  alla  sorveglianza  &#13;
 speciale è punito con l'arresto da tre mesi ad un anno.                 &#13;
     Se l'inosservanza riguarda la sorveglianza speciale con l'obbligo o  &#13;
 il divieto di soggiorno, si applica la pena dell'arresto da sei mesi  a  &#13;
 due  anni. In ogni caso è consentito l'arresto anche fuori dei casi di  &#13;
 flagranza (...)".                                                        &#13;
     Il pretore di San  Cipriano  Picentino  convalidava  l'arresto  con  &#13;
 decreto del 15 febbraio 1975 ed ordinava che l'imputato fosse condotto,  &#13;
 in stato di detenzione, all'udienza del 18 febbraio 1975 per giudicarlo  &#13;
 col  rito  del  giudizio  direttissimo, ma in udienza rilevava che, non  &#13;
 ricorrendo l'ipotesi della flagranza, mancavano i  presupposti  per  la  &#13;
 celebrazione  del  giudizio  stesso.  Perciò  ordinava la trasmissione  &#13;
 degli atti al suo ufficio, onde procedere con  le  forme  ordinarie,  e  &#13;
 disponeva  la  "liberazione"  dell'arrestato, "essendo egli imputato di  &#13;
 reato per il quale la  legge  non  consente  il  mandato  di  cattura".  &#13;
 Quindi,   con  ordinanza  24  febbraio  1975,  sollevava  questione  di  &#13;
 legittimità costituzionale dell'art. 9, terzo comma, legge 27 dicembre  &#13;
 1956, n. 1423, in riferimento all'art. 13 della Costituzione.            &#13;
     Circa la non manifesta infondatezza della questione  il  giudice  a  &#13;
 quo  assume  che  quest'ultima  disposizione  conferisce una riserva di  &#13;
 potere alla A.G. in materia di provvedimenti limitativi della  libertà  &#13;
 personale   e   che   pertanto   il  legislatore  ordinario,  allorché  &#13;
 attribuisce eccezionalmente simile  potere  all'autorità  di  polizia,  &#13;
 deve  contemporaneamente  assicurare  all'A.G.  il  potere,  in caso di  &#13;
 convalida, di protrarre gli effetti del provvedimento.                   &#13;
     Senonché, nell'ipotesi prevista dall'art. 9, terzo comma, legge 27  &#13;
 dicembre 1956, n. 1423, così come modificato dall'art.    8  legge  14  &#13;
 ottobre  1974,  n.  497,  non  essendo  possibile procedere al giudizio  &#13;
 direttissimo, per difetto della flagranza, e non essendo consentito  il  &#13;
 mandato  di  cattura,  data  la  natura contravvenzionale del reato, il  &#13;
 giudice non potrebbe protrarre lo stato di detenzione,  e  fare  quindi  &#13;
 proprio  il  provvedimento provvisorio adottato dalla P.S., ma dovrebbe  &#13;
 ordinare la liberazione dell'arrestato.                                  &#13;
     Di qui il dubbio che la norma denunziata contrasti, nei sensi sopra  &#13;
 indicati, con l'art. 13, secondo comma, della Costituzione.              &#13;
     In ordine alla rilevanza, il giudice  a  quo  osserva  poi  che  la  &#13;
 questione  sollevata  concerne un aspetto essenziale della controversia  &#13;
 (quello relativo allo stato di detenzione dell'imputato) e che, in caso  &#13;
 di accoglimento, dovrà dichiararsi l'illegittimità  della  detenzione  &#13;
 preventiva  sofferta  dal  Costabile nonostante l'intervenuta convalida  &#13;
 dell'arresto.                                                            &#13;
     L'ordinanza  ritualmente  notificata   e   comunicata,   è   stata  &#13;
 pubblicata  sulla  Gazzetta  Ufficiale  del  9 luglio 1975, n. 181.  In  &#13;
 questa sede si è costituito il Presidente del Consiglio  dei  ministri  &#13;
 rappresentato  e  difeso  dall'Avvocatura generale dello Stato con atto  &#13;
 depositato l'11 giugno 1975, chiedendo che la questione  sollevata  sia  &#13;
 dichiarata  inammissibile  per  difetto  di  rilevanza o, comunque, non  &#13;
 fondata.                                                                 &#13;
     I dubbi circa la rilevanza  della  questione  sono  ricollegati  al  &#13;
 fatto  che  essa  è  stata  sollevata quando l'imputato era stato già  &#13;
 scarcerato.                                                              &#13;
     Nel merito, l'Avvocatura prende  atto  che  secondo  l'orientamento  &#13;
 della  Corte  di  cassazione  l'arresto  eseguito nella flagranza di un  &#13;
 reato per il quale, data la pena edittale, la legge  non  autorizza  il  &#13;
 mandato  di  cattura,  legittima il protrarsi della custodia preventiva  &#13;
 solo quando si proceda nelle forme del giudizio direttissimo e che ove,  &#13;
 invece,  si  proceda  secondo  il  rito   ordinario   debbono   trovare  &#13;
 applicazione  le  norme degli artt. 269 e 270 c.p.p. le quali impongono  &#13;
 la immediata scarcerazione dell'imputato. Ma  ritiene  che  una  esatta  &#13;
 interpretazione  della  norma impugnata porti ad escludere che con essa  &#13;
 si sia voluto autorizzare l'autorità di pubblica sicurezza ad  operare  &#13;
 un  arresto che, sia pure dopo la convalida, deve immediatamente essere  &#13;
 posto nel nulla dall'autorità giudiziaria.                              &#13;
     Per l'Avvocatura la denuncia  contenuta  nell'ordinanza  di  rinvio  &#13;
 deriverebbe "dall'equivoco nel quale è incorso il giudice a quo quando  &#13;
 ha  ritenuto  di dover ordinare la liberazione dell'imputato arrestato,  &#13;
 in applicazione delle disposizioni del codice  di  rito  relative  alla  &#13;
 liberazione   dell'imputato   senza   considerare  che  proprio  queste  &#13;
 disposizioni erano state derogate dalla norma impugnata".                &#13;
     Conclude  pertanto  chiedendo  che  la  questione  sia   dichiarata  &#13;
 inammissibile  perché  irrilevante  o,  in subordine, che la questione  &#13;
 stessa sia dichiarata infondata.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - Come esposto in narrativa,  il  giudice  a  quo  ha  sollevato  &#13;
 questione  di  legittimità  dell'art.  9,  terzo comma, della legge 27  &#13;
 dicembre 1956, n. 1423, nel testo risultante dalle modifiche  apportate  &#13;
 con  l'art.  8  della  legge  14 ottobre 1974, n. 497, assumendo che la  &#13;
 disposizione ivi contenuta, secondo la quale  è  consentito  l'arresto  &#13;
 del  contravventore  agli  obblighi relativi alla sorveglianza speciale  &#13;
 anche fuori dei casi di flagranza, si  porrebbe  in  contrasto  con  la  &#13;
 riserva  di  giurisdizione  in  materia di provvedimenti concernenti la  &#13;
 libertà  personale  posta     dall'art.  13  Cost.   in   quanto   non  &#13;
 attribuirebbe   all'autorità   giudiziaria   il  potere  di  protrarre  &#13;
 l'arresto stesso nel caso in cui, come nella specie, non sia  possibile  &#13;
 avvalersi  del giudizio direttissimo per trascorsa flagranza e si debba  &#13;
 quindi procedere alla liberazione dell'arrestato per un reato che, come  &#13;
 quello ascritto al Costabile, non consenta il mandato di cattura.        &#13;
     2. - E da rilevare pregiudizialmente che  il  giudice  a  quo  come  &#13;
 risulta  dalla stessa ordinanza di rinvio, in applicazione  dei criteri  &#13;
 testé esposti, con provvedimento del 18 febbraio 1975, cioè  di  data  &#13;
 anteriore   all'ordinanza   stessa,   aveva   disposto  la  liberazione  &#13;
 dell'arrestato  ritenendo  appunto  che  la  sua  detenzione  non   era  &#13;
 consentita,  in  base  alla  natura contravvenzionale del reato ed alla  &#13;
 inapplicabilità nella specie del rito direttissimo.                     &#13;
     La evidente definitività del detto provvedimento di  scarcerazione  &#13;
 rende   in  ogni  caso  ininfluente  la  prospettata  dichiarazione  di  &#13;
 illegittimità della norma impugnata ed è quindi palese  l'irrilevanza  &#13;
 della  questione,  la  cui soluzione non potrebbe oramai spiegare alcun  &#13;
 effetto nel giudizio principale.                                         &#13;
     3.  -  La  motivazione  pur formulata dal giudice a quo ai fini del  &#13;
 giudizio   di   rilevanza   di   sua   competenza,   incentrata   sulla  &#13;
 illegittimità  dell'arresto  del Costabile che dovrebbe derivare dalla  &#13;
 prospettata dichiarazione  di  illegittimità  dell'articolo  9,  terzo  &#13;
 comma,  della legge 27 dicembre 1956, n.  1423, appare quindi del tutto  &#13;
 incongrua ed insufficiente e, come tale, censurabile in questa sede.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara inammissibile la questione di  legittimità  dell'art.  9,  &#13;
 terzo  comma,  della  legge  27  dicembre  1956,  n.  1423,  modificato  &#13;
 dall'art. 8  della  legge  14  ottobre  1974,  n.  497,  sollevata  con  &#13;
 ordinanza del pretore di San Cipriano Picentino del 24 febbraio 1975 in  &#13;
 riferimento all'art. 13 della Costituzione.                              &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 26 novembre 1981.                             &#13;
                                   F.to:   LEOPOLDO   ELIA   -   EDOARDO  &#13;
                                   VOLTERRA    -   MICHELE   ROSSANO   -  &#13;
                                   GUGLIELMO ROEHRSSEN - ORONZO REALE  -  &#13;
                                   BRUNETTO  BUCCIARELLI DUCCI - ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO ANDRIOLI - GIUSEPPE FERRARI.  &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
