<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1957</anno_pronuncia>
    <numero_pronuncia>26</numero_pronuncia>
    <ecli>ECLI:IT:COST:1957:26</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>DE NICOLA</presidente>
    <relatore_pronuncia>Gaspare Ambrosini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/01/1957</data_decisione>
    <data_deposito>26/01/1957</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. ENRICO DE NICOLA, Presidente - Dott. &#13;
 GAETANO AZZARITI - Prof. TOMASO PERASSI - Prof. GASPARE AMBROSINI - &#13;
 Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO GABRIELI - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. MARIO BRACCI &#13;
 - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO &#13;
 PETROCELLI - Dott. ANTONIO MANCA, Giudici,</collegio>
    <epigrafe>ha pronunziato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 4 della legge  &#13;
 24  luglio  1930, n. 1278, promosso con  l'ordinanza 25 giugno 1956 del  &#13;
 Pretore di Arienzo  nel  procedimento  penale  a  carico  di  Vacchiano  &#13;
 Francesco   Giuseppe,   pubblicata   nella   Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 227 dell'8 settembre 1956 ed iscritta al n. 256 del  Reg.  &#13;
 ord. 1956.                                                               &#13;
     Vista  la  dichiarazione di intervento del Presidente del Consiglio  &#13;
 dei Ministri;                                                            &#13;
     udita nell'udienza pubblica del 28 novembre 1956 la  relazione  del  &#13;
 Giudice Gaspare Ambrosini;                                               &#13;
     udito il sostituto avvocato generale dello Stato Achille Salerni.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  procedimento  avanti  alla  Pretura  di  Arienzo  a  carico di  &#13;
 Vacchiano Francesco Giuseppe, imputato del reato di  cui  all'art.    4  &#13;
 della  legge  24  luglio  1930,  n.  1278,  per  avere  nel periodo dal  &#13;
 settembre al dicembre 1955 procacciato a scopo  di  lucro  numero  otto  &#13;
 contratti  di lavoro in Gran Bretagna a persone desiderose di emigrare,  &#13;
 la  difesa  dell'imputato  sollevò  la   eccezione   di   legittimità  &#13;
 costituzionale  del  suddetto art. 4, adducendo che è in contrasto con  &#13;
 l'art. 35 della Costituzione, che sancisce il principio della  libertà  &#13;
 di emigrazione.                                                          &#13;
     Il Pretore di Arienzo con ordinanza del 25 giugno 1956, accogliendo  &#13;
 la  richiesta della difesa del Vacchiano, sospese il processo e ordinò  &#13;
 la trasmissione degli atti alla Corte costituzionale per  la  eventuale  &#13;
 dichiarazione  di  illegittimità  costituzionale del succitato art. 4.  &#13;
 Nell'ordinanza non si precisa la disposizione  della  Costituzione  che  &#13;
 sarebbe  stata  violata, ma certamente si tratta dell'art. 35, al quale  &#13;
 aveva fatto esplicito riferimento la difesa del Vacchiano.               &#13;
     L'ordinanza  venne  ritualmente  notificata   al   Presidente   del  &#13;
 Consiglio  dei  Ministri, comunicata ai Presidenti delle due Camere del  &#13;
 Parlamento e pubblicata nella Gazzetta Ufficiale della Repubblica.       &#13;
     Nel giudizio è    intervenuto  il  Presidente  del  Consiglio  dei  &#13;
 Ministri  a  mezzo  dell'Avvocatura generale dello Stato, deducendo che  &#13;
 l'art. 4 della legge 24 luglio 1930, n. 1278, non limita la libertà di  &#13;
 emigrazione affermata dall'art. 35, comma ultimo,  della  Costituzione,  &#13;
 né  contrasta  ma anzi si armonizza col sistema di norme disciplinanti  &#13;
 il collocamento dei lavoratori.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il  principio  della libertà di emigrazione affermato nell'art. 35  &#13;
 ultimo comma, della Costituzione non è violato dall'art. 4 della legge  &#13;
 24 luglio 1930, n. 1278, che non si riferisce a chi aspira ad emigrare.  &#13;
     Dispone l'art. 4: "Chiunque al fine di lucro, procura in  qualsiasi  &#13;
 modo  un  atto  di chiamata od una proposta di contratto di  lavoro per  &#13;
 l'estero ad un cittadino che  intende  emigrare  o  si  intromette  per  &#13;
 ottenere  dalle  autorità  competenti  il rilascio del passaporto o di  &#13;
 altro documento di espatrio, è punito. . . ".                           &#13;
     Questa disposizione è diretta ad impedire  non  l'emigrazione,  ma  &#13;
 l'attività  speculativa  di  chi  può approfittare della necessità o  &#13;
 della speciale condizione psicologica di aspettativa  o  di  credulità  &#13;
 degli  aspiranti  ad  emigrare, specie se disoccupati, per fare da essi  &#13;
 accettare contratti di lavoro rovinosi od  inadeguati  in  rapporto  al  &#13;
 sistema della nostra legislazione sociale.                               &#13;
     Si  tenga  presente  che  lo  stesso  art.  35, ultimo comma, della  &#13;
 Costituzione afferma il principio della libertà  di  emigrare  non  in  &#13;
 modo   assoluto  ed  incontrollabile,  ma  con  qualche  limite:    "La  &#13;
 Repubblica. .  .  riconosce  la  libertà  di  emigrazione,  salvo  gli  &#13;
 obblighi  stabiliti  dalla  legge  nell'interesse generale, e tutela il  &#13;
 lavoro italiano all'estero".                                             &#13;
     Vi è quindi un  limite  che  può  essere  dettato  dall'interesse  &#13;
 generale  della  comunità  statale; e vi è un altro limite dipendente  &#13;
 dalla necessità di tutelare il lavoro italiano all'estero.              &#13;
     Orbene, la norma dell'art. 4 della legge in questione  deve  essere  &#13;
 riguardata  nel  quadro  degli  stessi motivi e delle stesse finalità.  &#13;
 Essa è stata dettata per garantire i lavoratori da possibili  illecite  &#13;
 speculazioni,  in  difesa  perciò  di  un  bene,  che, se per un verso  &#13;
 interessa singoli, per l'altro  non  interessa  meno  la  collettività  &#13;
 nazionale.                                                               &#13;
     Ed  è  ovvio  che  il  legislatore  è  completamente libero nella  &#13;
 valutazione di questi interessi e dei mezzi più adeguati per tutelarli  &#13;
 con norme anche  di  carattere  penale,  come  quella  dell'art.  4  in  &#13;
 questione,  la  quale  per  altro  si armonizza con tutto il sistema di  &#13;
 norme disciplinanti il collocamento dei lavoratori.                      &#13;
     È da rilevare, comunque, che l'art. 4 della legge 24  luglio  1930  &#13;
 non  pone  limiti  al  diritto del cittadino ad emigrare e che pertanto  &#13;
 nessun contrasto esiste tra esso ed il precetto dell'art.   35,  ultimo  &#13;
 comma, della Costituzione.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  infondata  la  questione  di legittimità costituzionale,  &#13;
 sollevata con l'ordinanza resa in data 25 giugno 1956  dal  Pretore  di  &#13;
 Arienzo,  dell'art.  4  della  legge  24  luglio  1930,  n.    1278, in  &#13;
 riferimento alla norma contenuta  nell'art.  35,  comma  ultimo,  della  &#13;
 Costituzione.                                                            &#13;
     Così deciso in Roma, nella sede della Corte costituzionale Palazzo  &#13;
 della Consulta, il 21 gennaio 1957.                                      &#13;
     ENRICO  DE  NICOLA  -  GAETANO  AZZARITI - TOMASO PERASSI - GASPARE  &#13;
 AMBROSINI - MARIO COSATTI -  FRANCESCO  PANTALEO  GABRIELI  -  GIUSEPPE  &#13;
 CASTELLI  AVOLIO  -  ANTONINO  PAPALDO - MARIO BRACCI - NICOLA JAEGER -  &#13;
 GIOVANNI CASSANDRO - BIAGIO PETROCELLI - ANTONIO MANCA.</dispositivo>
  </pronuncia_testo>
</pronuncia>
