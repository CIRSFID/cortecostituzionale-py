<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>369</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:369</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Valerio Onida</relatore_pronuncia>
    <redattore_pronuncia>Valerio Onida</redattore_pronuncia>
    <data_decisione>06/11/2001</data_decisione>
    <data_deposito>16/11/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Massimo VARI, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, &#13;
Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, &#13;
Piero Alberto CAPOTOSTI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 2951, primo e &#13;
terzo  comma,  del  codice  civile,  promosso con ordinanza emessa il &#13;
12 gennaio  2001  dal  Tribunale  di  Genova,  iscritta al n. 177 del &#13;
registro  ordinanze  2001 e pubblicata nella Gazzetta Ufficiale della &#13;
Repubblica n. 11, 1ª serie speciale, dell'anno 2001. &#13;
    Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di consiglio del 10 ottobre 2001 il giudice &#13;
relatore Valerio Onida. &#13;
    Ritenuto che con ordinanza emessa il 12 gennaio 2001, e pervenuta &#13;
in  cancelleria  il  23 febbraio  2001,  nel corso di un procedimento &#13;
promosso dal titolare di una ditta individuale artigiana di trasporti &#13;
nei  confronti della società committente per ottenere la condanna al &#13;
pagamento  di maggiori compensi dovuti in applicazione del sistema di &#13;
tariffe  "a forcella", il Tribunale di Genova, in funzione di giudice &#13;
del   lavoro,   ha   sollevato,   in   riferimento  all'art. 3  della &#13;
Costituzione,     questione     di     legittimità    costituzionale &#13;
dell'art. 2951,  primo e terzo comma, del codice civile, "nella parte &#13;
in  cui non sospende la decorrenza della prescrizione dei diritti del &#13;
prestatore nel corso del rapporto di lavoro parasubordinato"; &#13;
        che il remittente, premesso di avere pronunciato sentenza non &#13;
definitiva  con  cui  è  stato  accertato che il rapporto dedotto in &#13;
giudizio,  e  svoltosi  dal settembre  1989 al dicembre 1993, riveste &#13;
natura   parasubordinata   e   rientra   nella   previsione   di  cui &#13;
all'art. 409,  numero  3), cod. proc. civ., motiva la rilevanza della &#13;
questione   affermando   che   la   declaratoria   di  illegittimità &#13;
costituzionale  della  norma denunciata consentirebbe, in relazione a &#13;
tutti  i  compensi  reclamati  dal  ricorrente  nel giudizio a quo di &#13;
fissare  al dicembre  1993, vale a dire alla cessazione del rapporto, &#13;
la  decorrenza  dei  termini della prescrizione quinquennale prevista &#13;
dall'art. 2  del  decreto legge 29 marzo 1993, n. 82, convertito, con &#13;
modificazioni,  dalla  legge  27 maggio  1993,  n. 162, medio tempore &#13;
intervenuto;   con   l'ulteriore   conseguenza   che  l'eccezione  di &#13;
prescrizione,   sollevata   dal  convenuto,  sarebbe  da  respingere, &#13;
giacché  il  ricorso introduttivo è stato notificato il 16 febbraio &#13;
1996; &#13;
        che,  ad  avviso  del  Tribunale, integrerebbe una violazione &#13;
dell'art. 3   della   Costituzione   l'irragionevole   disparità  di &#13;
trattamento  risultante dalla disciplina rispettivamente dettata, per &#13;
il  lavoratore  parasubordinato,  dall'art. 2951  cod. civ. e, per il &#13;
lavoratore  subordinato,  dall'art. 2948,  numeri 4) e 5), cod. civ., &#13;
come  risultante  dalle sentenze della Corte costituzionale n. 63 del &#13;
1966,  n. 143  del  1969,  n. 86 del 1971 e n. 174 del 1972: solo per &#13;
quest'ultimo, infatti, la prescrizione inizia a decorrere dal momento &#13;
estintivo  del  rapporto  di lavoro, salvo il caso in cui il rapporto &#13;
stesso sia assistito da stabilità reale, benché anche il lavoratore &#13;
parasubordinato  si  trovi,  almeno  di  regola,  in una posizione di &#13;
inferiorità   economica   analoga   a  quella  che  caratterizza  il &#13;
lavoratore subordinato; &#13;
        che,  osserva  il  remittente,  l'analogia  di  posizioni del &#13;
lavoratore  subordinato  e  di  quello  parasubordinato ha indotto il &#13;
legislatore  ad  estendere alle ipotesi di lavoro ex art. 409, numero &#13;
3),  cod.  proc.  civ.  la  disciplina  che  delinea  il  rito  nelle &#13;
controversie   di   lavoro  subordinato,  la  regola  del  cumulo  di &#13;
rivalutazione   monetaria   ed  interessi  in  base  al  terzo  comma &#13;
dell'art. 429  cod.  proc. civ., nonché la disciplina dell'art. 2113 &#13;
cod.  civ.  in  materia di rinunce e transazioni, che fa decorrere il &#13;
termine   di   decadenza  per  l'impugnazione  dalla  cessazione  del &#13;
rapporto: in questo quadro la norma denunciata sarebbe, in parte qua, &#13;
irragionevole, tanto più considerando - secondo l'insegnamento della &#13;
sentenza  n. 63 del 1966 di questa Corte - che l'inerzia del titolare &#13;
del  diritto,  la quale faccia maturare i termini della prescrizione, &#13;
è in definitiva equiparabile ad una rinuncia implicita; &#13;
        che  nel  giudizio  dinanzi  alla  Corte  è  intervenuto  il &#13;
Presidente  del Consiglio dei ministri, chiedendo, e ribadendo in una &#13;
memoria  depositata  in prossimità della camera di consiglio, che la &#13;
questione  sia  dichiarata  inammissibile  e  comunque manifestamente &#13;
infondata; &#13;
        che,   ad   avviso   dell'Avvocatura,  la  questione  sarebbe &#13;
irrilevante  (o in ogni caso non motivata sulla rilevanza) in quanto, &#13;
essendo  stato  il  contratto  stipulato nel settembre 1989, andrebbe &#13;
comunque  applicata la prescrizione annuale: e poiché il rapporto è &#13;
venuto  a  cessare nel dicembre 1993, la prescrizione annuale sarebbe &#13;
comunque  già  maturata alla data della domanda giudiziale, anche se &#13;
si  facesse  decorrere  dalla  cessazione del rapporto, non potendosi &#13;
applicare  la  prescrizione  quinquennale  per rapporti di lavoro che &#13;
hanno  avuto  termine  nel 1993 ma che sono sorti antecedentemente al &#13;
1993; &#13;
        che,  nel merito, la difesa erariale sostiene, richiamando in &#13;
particolare  la  sentenza  di  questa  Corte  n. 365 del 1995, che la &#13;
categoria  della  parasubordinazione  vale ai fini processuali ma non &#13;
impone una assimilazione, ai fini sostanziali, del lavoro autonomo al &#13;
lavoro  subordinato; in ogni caso, ove si ritenga, come nel caso, che &#13;
la  fattispecie concreta integri un'ipotesi di lavoro parasubordinato &#13;
e  si  pretenda  di  estendere  ad essa la disciplina del rapporto di &#13;
lavoro subordinato, compito del giudice sarebbe quello di individuare &#13;
la norma idonea a regolarla e non già di sollecitare la declaratoria &#13;
di incostituzionalità di quella norma che segnatamente si esclude di &#13;
dover applicare. &#13;
    Considerato  che il dubbio di legittimità costituzionale investe &#13;
l'art. 2951,  primo  e  terzo  comma,  cod.  civ.  nella parte in cui &#13;
consente   che  la  prescrizione,  di  durata  annuale,  del  diritto &#13;
dell'autotrasportatore  decorra  durante l'esecuzione del rapporto di &#13;
lavoro parasubordinato; &#13;
        che il giudice remittente non spiega quale rilevanza abbia il &#13;
far decorrere il termine di prescrizione, anziché, secondo la regola &#13;
generale,  dal  momento  in cui il diritto è sorto, dalla cessazione &#13;
del  rapporto, in una fattispecie nella quale il termine, annuale, di &#13;
prescrizione,  stabilito  dalla  norma denunciata, anche se computato &#13;
dalla  data  di cessazione del rapporto, ossia dal dicembre 1993, era &#13;
comunque  maturato  allorché,  nel febbraio  1996,  il ricorrente ha &#13;
azionato in giudizio il credito di natura contrattuale; &#13;
        che  il  giudice  a quo neppure spiega su quale base potrebbe &#13;
trovare  applicazione  nella specie la prescrizione quinquennale, che &#13;
l'art. 2  del  decreto-legge  29 marzo 1993, n. 82, prevede bensì in &#13;
relazione  ai  diritti  derivanti  da  contratti di autotrasporto per &#13;
conto  terzi soggetti al sistema di tariffe "a forcella", ma solo per &#13;
i  contratti stipulati successivamente alla data di entrata in vigore &#13;
del  decreto-legge  medesimo,  e  non  per tutti i diritti non ancora &#13;
prescritti a quella data: mentre il rapporto sub judice, svoltosi dal &#13;
1989  al  1993,  non  può  che  far capo, almeno in massima parte, a &#13;
contratti stipulati anteriormente a tale data; &#13;
        che, d'altra parte, se alla specie fosse applicabile non già &#13;
l'art. 2951  cod. civ., ma l'art. 2 del decreto-legge n. 82 del 1993, &#13;
trattandosi  di  rapporti contrattuali sorti dopo l'entrata in vigore &#13;
di   quest'ultima   norma,   la   questione  della  decorrenza  della &#13;
prescrizione   non   sarebbe   rilevante,   non  essendo  il  termine &#13;
quinquennale  di  prescrizione, in quella ipotesi, ancora decorso, al &#13;
momento  di instaurazione del giudizio, quale che fosse il dies a quo &#13;
della decorrenza del termine medesimo; &#13;
        che,    pertanto,    stante    la   motivazione   carente   e &#13;
contraddittoria  in  ordine  alla rilevanza, la questione deve essere &#13;
dichiarata manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale dell'art. 2951, primo e terzo comma, del &#13;
codice   civile,   sollevata,   in   riferimento   all'art. 3   della &#13;
Costituzione,  dal  Tribunale  di  Genova con l'ordinanza indicata in &#13;
epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 6 novembre 2001. &#13;
                       Il Presidente: Ruperto &#13;
                         Il redattore: Onida &#13;
                      Il cancelliere: Fruscella &#13;
    Depositata in cancelleria il 16 novembre 2001 &#13;
                      Il cancelliere: Fruscella</dispositivo>
  </pronuncia_testo>
</pronuncia>
