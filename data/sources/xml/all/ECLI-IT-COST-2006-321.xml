<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2006</anno_pronuncia>
    <numero_pronuncia>321</numero_pronuncia>
    <ecli>ECLI:IT:COST:2006:321</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BILE</presidente>
    <relatore_pronuncia>Maria Rita Saulle</relatore_pronuncia>
    <redattore_pronuncia>Maria Rita Saulle</redattore_pronuncia>
    <data_decisione>18/07/2006</data_decisione>
    <data_deposito>27/07/2006</data_deposito>
    <tipo_procedimento>GIUDIZIO SULL'AMMISSIBILITÀ DI RICORSO PER CONFLITTO DI ATTRIBUZIONE TRA POTERI DELLO STATO</tipo_procedimento>
    <dispositivo>ammissibile</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Franco BILE; Giudici: Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Paolo MADDALENA, Alfio FINOCCHIARO, Alfonso QUARANTA, Franco GALLO, Luigi MAZZELLA, Gaetano SILVESTRI, Sabino CASSESE, Maria Rita SAULLE, Giuseppe TESAURO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio per conflitto di attribuzione tra poteri dello Stato sorto a seguito della delibera del Senato della Repubblica del 23 marzo 2005, relativa alla insindacabilità delle opinioni espresse dal senatore Nando Dalla Chiesa nei confronti dell'onorevole Cesare Previti, promosso con ricorso del Tribunale di Roma, sezione prima civile, in composizione monocratica, depositato in cancelleria il 2 marzo 2006 ed iscritto al n. 6 del registro conflitti tra poteri dello Stato 2006, fase di ammissibilità. &#13;
    Udito nella camera di consiglio del 5 luglio 2006 il Giudice relatore Maria Rita Saulle. &#13;
    Ritenuto che il Tribunale civile di Roma, sezione prima civile, in composizione monocratica, con ordinanza-ricorso del 3 novembre 2005, ha promosso conflitto di attribuzione tra poteri dello Stato nei confronti del Senato della Repubblica, in relazione alla delibera adottata il 23 marzo 2005, con la quale - in conformità alla proposta della Giunta delle elezioni e delle immunità parlamentari -  è stato dichiarato che i fatti per i quali è pendente un giudizio civile nei confronti del sen. Nando Dalla Chiesa costituiscono opinioni espresse da un membro del Parlamento nell'esercizio delle sue funzioni e, pertanto, sono coperti da insindacabilità ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    che il Tribunale ricorrente osserva di essere chiamato a giudicare della domanda, previo accertamento del reato di diffamazione, di risarcimento dei danni avanzata dall'on. Cesare Previti nei confronti del sen. Nando Dalla Chiesa, conseguente alla pubblicazione di tre articoli, a firma del convenuto, avvenuta il 3 maggio, il 5 maggio e il 14 luglio 2004 sul quotidiano “L'Unità”;  &#13;
    che il Tribunale di Roma, preliminarmente, osserva che l'on. Cesare Previti contesta il contenuto degli articoli citati nella parte in cui si afferma che egli, nella gestione dell'eredità della Marchesa Anna Maria Casati Stampa, «dopo aver patrocinato le ragioni della parte offesa, si offrì in soccorso alla parte vincente» provvedendo alla cessione, ad un prezzo irrisorio e dilazionato, di un immobile e di altri beni mobili all'on. Berlusconi, che si sarebbe giovato di tale operazione negoziale; &#13;
    che il Tribunale ricorrente rileva che l'on. Cesare Previti ritiene siffatte affermazioni false e prive di fondamento e dirette a fornire di lui un'immagine negativa con il preciso obiettivo di screditare la persona dell'on. Berlusconi e delle persone a lui più vicine; &#13;
    che, a parere del Tribunale ricorrente, la delibera impugnata, nell'approvare la proposta con cui la Giunta delle elezioni e delle immunità parlamentari ha ritenuto insindacabili le opinioni espresse dal sen. Nando Dalla Chiesa, non ha correttamente inteso il concetto di nesso funzionale tra dichiarazioni ed attività parlamentare come indicato da questa Corte; &#13;
    che, in particolare, il Tribunale di Roma osserva che questa Corte ha affermato che le «dichiarazioni del parlamentare rese all'esterno degli organi parlamentari sono insindacabili solo ove sia riscontrabile una corrispondenza sostanziale di contenuti della dichiarazione stessa con atti parlamentari»; &#13;
    che, secondo il Tribunale ricorrente, gli articoli pubblicati sul quotidiano “L'Unità” non si possono ritenere in alcun modo collegati alla attività istituzionale del sen. Dalla Chiesa; &#13;
    che, dunque, ad avviso del ricorrente, ai fatti per cui è in corso il processo non sarebbe applicabile l'art. 68, primo comma, della Costituzione e che, quindi, la delibera di insindacabilità del 23 marzo 2005 sarebbe viziata.  &#13;
    Considerato che, in questa fase del giudizio, a norma dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, la Corte costituzionale è chiamata a deliberare, senza contraddittorio, in ordine all'esistenza o meno della «materia di un conflitto la cui risoluzione spetti alla sua competenza», restando impregiudicata ogni ulteriore decisione, anche in punto di ammissibilità; &#13;
    che nella fattispecie sussistono i requisiti soggettivo ed oggettivo del conflitto;  &#13;
    che, infatti, quanto al requisito soggettivo, devono ritenersi legittimati ad essere parte del presente conflitto, sia il Tribunale di Roma, sezione prima civile, in composizione monocratica, in quanto organo giurisdizionale, in posizione di indipendenza costituzionalmente garantita, competente a dichiarare definitivamente, per il procedimento di cui è investito, la volontà del potere cui appartiene, sia il Senato della Repubblica, in quanto organo competente a dichiarare definitivamente la propria volontà in ordine all'applicabilità dell'art. 68, primo comma, della Costituzione; &#13;
    che, quanto al profilo oggettivo, sussiste la materia del conflitto, dal momento che il ricorrente lamenta la lesione della propria sfera di attribuzioni, costituzionalmente garantita, da parte della impugnata deliberazione del Senato della Repubblica;  &#13;
    che, pertanto, esiste la materia di un conflitto, la cui risoluzione spetta alla competenza di questa Corte.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzione proposto dal Tribunale di Roma, sezione prima civile, in composizione monocratica, nei confronti del Senato della Repubblica, con l'atto indicato in epigrafe; &#13;
    dispone: &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al Tribunale di Roma;  &#13;
    b) che, a cura del ricorrente, l'atto introduttivo e la presente ordinanza siano notificati al Senato della Repubblica, in persona del suo Presidente, entro il termine di sessanta giorni dalla comunicazione, per essere successivamente depositati, con la prova dell'avvenuta notifica, presso la cancelleria della Corte entro il termine di venti giorni, previsto dall'art. 26, comma 3, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 18 luglio 2006.  &#13;
F.to:  &#13;
Franco BILE, Presidente  &#13;
Maria Rita SAULLE, Redattore  &#13;
Maria Rosaria FRUSCELLA, Cancelliere  &#13;
Depositata in Cancelleria il 27 luglio 2006.  &#13;
Il Cancelliere  &#13;
F.to: FRUSCELLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
