<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1984</anno_pronuncia>
    <numero_pronuncia>174</numero_pronuncia>
    <ecli>ECLI:IT:COST:1984:174</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Ettore Gallo</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>14/06/1984</data_decisione>
    <data_deposito>20/06/1984</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - &#13;
 Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - Prof. &#13;
 LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA PERGOLA - &#13;
 Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO &#13;
 SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott. ALDO &#13;
 CORASANITI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti  di  legittimità  costituzionale  dell'art.  9  &#13;
 della  legge  24  novembre  1981,  n. 689 (Modifiche al sistema penale)  &#13;
 promossi con le seguenti ordinanze:                                      &#13;
     1) ordinanza emessa il 17 gennaio 1983 dal Tribunale di Perugia nel  &#13;
 procedimento penale a carico di Palombaro Aldo, iscritta al n. 172  del  &#13;
 registro  ordinanze  1983  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 232 dell'anno 1983;                                        &#13;
     2) ordinanza emessa il 7 marzo 1983 dal Tribunale  di  Perugia  nel  &#13;
 procedimento penale a carico di Spazzoni Walter, iscritta al n. 411 del  &#13;
 registro  ordinanze  1983  e  pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n.  288 dell'anno 1983;                                       &#13;
     3) ordinanza emessa il 28 marzo 1983 dal Tribunale di  Perugia  nel  &#13;
 procedimento  penale  a carico di Minni Antonio, iscritta al n. 447 del  &#13;
 registro ordinanze 1983 e pubblicata  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n.  301 dell'anno 1983.                                       &#13;
     Visti  gli  atti  di  intervento  del  Presidente del Consiglio dei  &#13;
 ministri;                                                                &#13;
     Udito nella camera di  consiglio  del  14  marzo  1984  il  Giudice  &#13;
 relatore Ettore Gallo.                                                   &#13;
     Ritenuto  che,  con  le  ordinanze e nei procedimenti penali di cui  &#13;
 all'epigrafe, il Tribunale di Perugia sollevava identica  questione  di  &#13;
 legittimità  costituzionale  dell'art. 9 della l. 24 novembre 1981, n.  &#13;
 689 in relazione all'art. 3 Cost.,                                       &#13;
     - che, secondo il Tribunale - prima dell'entrata  in  vigore  della  &#13;
 citata legge, l'apprensione di selvaggina, in violazione dei divieti ed  &#13;
 oltre i limiti di cui alla specifica legislazione in materia di caccia,  &#13;
 integrava, nella giurisprudenza del Tribunale stesso, il reato di furto  &#13;
 aggravato ai sensi degli artt. 624, 625 nn. 2 e 7 cod.  pen.,            &#13;
     -  che,  però, dopo l'entrata in vigore del denunziato art. 9, che  &#13;
 va applicato ai casi di specie in forza dell'art.  2 cod. pen.,  quella  &#13;
 giurisprudenza  non  potrebbe  più  essere  osservata  in  quanto "una  &#13;
 disposizione della legge statale sulla caccia 27 dicembre 1977, n. 968"  &#13;
 avrebbe carattere di specialità rispetto  alla  fattispecie  di  furto  &#13;
 contemplata  nel  codice  penale,  mentre essa dovrebbe ricevere ancora  &#13;
 ossequio ove il "furto venatorio" fosse commesso mediante violazione di  &#13;
 una disposizione della legge regionale umbra  sulla  caccia  3  gennaio  &#13;
 1980,  n.  1,  in quanto, per il secondo comma dell'articolo impugnato,  &#13;
 l'art. 624 cod. pen. dovrebbe trovare applicazione,                      &#13;
     - che, nei casi di specie, sarebbero state  violate  sia  la  legge  &#13;
 regionale che quella statale sulla caccia,                               &#13;
     -  che,  tuttavia, il diverso trattamento, conseguente alla ipotesi  &#13;
 di violazione di una  sola  delle  due  leggi,  implica  disparità  in  &#13;
 presenza  di  situazioni  sostanzialmente  eguali e determina, perciò,  &#13;
 situazione d'incompatibilità coll'art. 3 Cost..                         &#13;
     Considerato che, per aversi rapporto di specialità ex art.15  cod.  &#13;
 pen.,  è  indispensabile  che  tra le fattispecie raffrontate vi siano  &#13;
 elementi fondamentali comuni, ma una di  esse  abbia  qualche  elemento  &#13;
 caratterizzante in più che la specializzi rispetto all'altra,           &#13;
     -  che  dalle  ordinanze di rimessione non è dato in alcun modo di  &#13;
 capire quale sia il ragionamento seguito dal Tribunale  in  quanto  non  &#13;
 risulta quale sia la norma violata dalla legge statale sulla caccia: il  &#13;
 che è tanto più indispensabile ove si consideri che, in generale, fra  &#13;
 ipotesi di furto previste dal codice penale e norme della legge statale  &#13;
 sulla caccia il rapporto sembra piuttosto di concorso effettivo che non  &#13;
 apparente,                                                               &#13;
     -  che  il  giudizio  sulle questioni sollevate dalle tre ordinanze  &#13;
 dev'essere riunito attesa la loro identità,</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     Riuniti  i  giudizi,  dichiara  manifestamente   inammissibile   la  &#13;
 questione  di  legittimità  costituzionale  sollevata dal Tribunale di  &#13;
 Perugia con le ordinanze in epigrafe in relazione all'art. 3 Cost..      &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 14 giugno 1984.         &#13;
                                   F.to:  LEOPOLDO  ELIA  -  ANTONINO DE  &#13;
                                   STEFANO  -  GUGLIELMO   ROEHRSSEN   -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI.                            &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
