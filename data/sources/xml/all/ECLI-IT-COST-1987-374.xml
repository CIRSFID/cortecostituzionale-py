<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1987</anno_pronuncia>
    <numero_pronuncia>374</numero_pronuncia>
    <ecli>ECLI:IT:COST:1987:374</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Virgilio Andrioli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>15/10/1987</data_decisione>
    <data_deposito>04/11/1987</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Virgilio ANDRIOLI, prof. Giovanni CONSO, prof. Ettore &#13;
 GALLO, dott. Aldo CORASANITI, prof. Giuseppe BORZELLINO, dott. &#13;
 Francesco GRECO, prof. Renato DELL'ANDRO, prof. Gabriele PESCATORE, &#13;
 avv. Ugo SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 2942, n. 1, del    &#13;
 codice civile, promosso con ordinanza emessa il 21 dicembre 1979  dal    &#13;
 Tribunale di Genova, iscritta al n. 148 del registro ordinanze 1980 e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 131 dell'anno    &#13;
 1980;                                                                    &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 14 ottobre 1987 il Giudice    &#13;
 relatore Virgilio Andrioli;                                              &#13;
    Ritenuto   che  I)  con  ordinanza  emessa  il  21  dicembre  1979    &#13;
 (notificata il 18 e comunicata il 28 gennaio 1980;  pubblicata  nella    &#13;
 Gazzetta  Ufficiale  n.  131  del 14 maggio 1980 e iscritta al n. 148    &#13;
 R.O. 1980) nel giudizio promosso, con atto notificato il 17  febbraio    &#13;
 1977, da Ravarotto Rinaldo, quale legale rappresentante in una con la    &#13;
 consorte  Canevello  Laura  del  minore  figlio  Corrado,  che  aveva    &#13;
 convenuto  Antonio  Benvenuto,  il quale aveva investito il 6 ottobre    &#13;
 1969 il figlio minore Corrado, e la  Compagnia  di  Assicurazione  di    &#13;
 Milano,  assicuratrice  del  convenuto per la responsabilità civile,    &#13;
 chiedendone la condanna al risarcimento dei danni  subìti,  l'adi'to    &#13;
 Tribunale  di  Genova  -  premesso  che  i convenuti avevano eccepito    &#13;
 l'intervenuta prescrizione del diritto degli attori  al  risarcimento    &#13;
 dei danni, che il reato di lesioni colpose in danno del minore si era    &#13;
 estinto per amnistia il 22 maggio 1970, che l'attore aveva chiesto il    &#13;
 risarcimento  del  danno alla Compagnia di Assicurazioni di Milano il    &#13;
 26 settembre 1973 e al  Benvenuto  il  9  settembre  1975,  che,  non    &#13;
 essendo  applicabile  alla  specie  la  l.  24 dicembre 1969, n. 990,    &#13;
 l'attore era privo di azione diretta nei  confronti  della  Compagnia    &#13;
 assicuratrice  e  non  poteva  quindi  ravvisarsi  alcun  rapporto di    &#13;
 solidarietà tra il Benvenuto e la Compagnia e,  quindi,  la  lettera    &#13;
 inviata  alla  Compagnia di Assicurazione di Milano sotto la data del    &#13;
 26 settembre 1973 non  poteva  produrre  alcun  effetto  interruttivo    &#13;
 della  prescrizione di guisa che il termine di prescrizione (due anni    &#13;
 dalla lettera 6 ottobre 1971) era già decorso al momento  dell'invio    &#13;
 della  lettera  9  settembre  1975  -  ha  giudicato  rilevante e, in    &#13;
 riferimento agli artt. 24 e 31 Cost., non manifestamente infondata la    &#13;
 questione  di  legittimità  costituzionale dell'art. 2942 n. 1 c.c.,    &#13;
 per il quale la prescrizione  rimane  sospesa  contro  i  minori  non    &#13;
 emancipati  e  gli interdetti per infermità di mente per il tempo in    &#13;
 cui non hanno rappresentante legale e per sei  mesi  successivi  alla    &#13;
 nomina  del medesimo o alla cessazione della incapacità, nella parte    &#13;
 in cui limita la sospensione della  prescrizione  nei  confronti  dei    &#13;
 minori  non  emancipati  al  periodo  in  cui  gli  stessi  non siano    &#13;
 rappresentati legalmente; II) avanti la Corte nessuna delle parti del    &#13;
 giudizio  a  quo  si  è costituita, ma ha spiegato intervento per il    &#13;
 Presidente del Consiglio dei  ministri  l'Avvocatura  generale  dello    &#13;
 Stato, con atto depositato il 3 giugno 1980, nel quale ha argomentato    &#13;
 e concluso per la infondatezza della questione;                          &#13;
    Considerato  che  la questione è manifestamente infondata perché    &#13;
 non è la denunciata norma che contempla la specie  della  negligenza    &#13;
 del genitore esercente la potestà sul minore figlio non emancipato;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 2942 n. 1 c.c. nella parte in  cui  delimita    &#13;
 la  sospensione  della  prescrizione  nei  confronti  dei  minori non    &#13;
 emancipati al solo periodo in cui gli  stessi  non  siano  legalmente    &#13;
 rappresentati,  sollevata in riferimento agli artt. 24 e 31 Cost. con    &#13;
 ordinanza 21 dicembre 1979 del  Tribunale  di  Genova  (n.  148  R.O.    &#13;
 1980).                                                                   &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 15 ottobre 1987.                              &#13;
                       Il Presidente: SAJA                                &#13;
                       Il Redattore: ANDRIOLI                             &#13;
    Depositata in cancelleria il 4 novembre 1987.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
