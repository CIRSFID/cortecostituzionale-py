<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>86</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:86</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/04/1981</data_decisione>
    <data_deposito>01/06/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. &#13;
 ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - &#13;
 Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE &#13;
 FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale degli artt. 6 e 14  del  &#13;
 d.P.R. 26 ottobre 1972, n. 643, modificato  con  d.P.R.    23  dicembre  &#13;
 1974,  n.  688  (imposta  sull'incremento  di   valore degli immobili),  &#13;
 promosso con ordinanza emessa   il  1  giugno  1977  dalla  Commissione  &#13;
 tributaria  di  primo   grado di Massa Carrara, sul ricorso proposto da  &#13;
 Tonarelli   Gino, iscritta al n. 824  del  registro  ordinanze  1980  e  &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n.  41 del 1981.    &#13;
     Udito  nella  camera  di  consiglio dell'8 aprile 1981 il   Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto che la Commissione tributaria di primo  grado  di    Massa  &#13;
 Carrara,  con  ordinanza  emessa  il 1 giugno 1977   (ma pervenuta alla  &#13;
 Corte il 17 novembre 1980), ha   sollevato  questione  di  legittimità  &#13;
 costituzionale  degli artt.  6 e 14 del d.P.R. 26 ottobre 1972, n. 643,  &#13;
 modificato con   d.P.R. 23  dicembre  1974,  n.  688,  "in  quanto  non  &#13;
 prevedono  che l'incremento di valore, soggetto ad Invim,  sia depurato  &#13;
 delle variazioni nominali dovute a svalutazione monetaria": e ciò  per  &#13;
 pretesa violazione  dell'art. 53, primo comma, della Costituzione.       &#13;
     Considerato  che  le stesse questioni sono state già decise  dalla  &#13;
 Corte  con  sentenza  8  novembre  1979,  n.  126,     dichiarando   la  &#13;
 illegittimità costituzionale dell'art. 14 del  d.P.R. 26 ottobre 1972,  &#13;
 n.  643,  e  dell'art.  8 della legge 16  dicembre 1977, n. 904, "nella  &#13;
 parte in cui le disposizioni  concernenti il calcolo dell'incremento di  &#13;
 valore   imponibile netto determinano -  in  relazione  al  periodo  di  &#13;
 formazione  dell'incremento  stesso  -  ingiustificata    disparità di  &#13;
 trattamento tra i soggetti passivi del tributo",    e  dichiarando  non  &#13;
 fondate le questioni di costituzionalità  degli artt. 2, 4, 6, 7, 15 e  &#13;
 16  del  d.P.R. 26 ottobre 1972, n.  643, sollevate in riferimento agli  &#13;
 artt. 3, 42, 47 e 53 della  Costituzione; e che nell'ordinanza non sono  &#13;
 prospettati  profili nuovi, né sono addotti motivi che possano indurre  &#13;
 la Corte a modificare la propria giurisprudenza;                         &#13;
     che, peraltro, successivamente alla decisione di questa  Corte,  la  &#13;
 disciplina  normativa  dell'Invim  è  stata   modificata con decreto -  &#13;
 legge 12 novembre 1979, n. 571, convertito    con  modificazioni  nella  &#13;
 legge  12  gennaio  1980,  n.  2, la quale   ha soppresso l'art. 14 del  &#13;
 d.P.R.  n.  643  del  1972,  sostituito l'art. 15, e regolato le misure  &#13;
 delle aliquote stabilite per  gli anni 1979 e 1980 ai  sensi  dell'art.  &#13;
 16, statuendo che le  nuove disposizioni si applicano anche ai rapporti  &#13;
 sorti  prima    della  loro entrata in vigore ed a tale data non ancora  &#13;
 definiti, "per i quali tuttavia  l'ammontare  dell'imposta  dovuta  non  &#13;
 può in ogni caso superare quello determinabile con i criteri contenuti  &#13;
 nelle norme precedentemente in vigore" (art.  3);                        &#13;
     che  conseguentemente  si  ravvisa  la  necessità  di  disporre la  &#13;
 restituzione degli atti al giudice a quo, perché accerti   se,  ed  in  &#13;
 quale misura, le questioni sollevate siano tuttora  rilevanti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     ordina  la  restituzione degli atti alla Commissione  tributaria di  &#13;
 primo grado di Massa Carrara.                                            &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'8  aprile 1981.          &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE ROSSANO - ANTONINO DE STEFANO  &#13;
                                   -   LEOPOLDO      ELIA   -  GUGLIELMO  &#13;
                                   ROEHRSSEN - ORONZO  REALE -  BRUNETTO  &#13;
                                   BUCCIARELLI     DUCCI    -    ALBERTO  &#13;
                                   MALAGUGINI - LIVIO PALADIN -  ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO      ANDRIOLI   -   GIUSEPPE  &#13;
                                   FERRARI.                               &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
