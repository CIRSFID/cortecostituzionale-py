<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>84</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:84</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GUIZZI</presidente>
    <relatore_pronuncia>Piero Alberto Capotosti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>22/03/2000</data_decisione>
    <data_deposito>28/03/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Francesco GUIZZI; &#13;
 Giudici: Cesare MIRABELLI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, &#13;
 Annibale MARINI, Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale degli articoli 1, comma 2,    &#13;
 2,  3  e  4  della  legge della Regione Piemonte 3 gennaio 1997, n. 4    &#13;
 (Regolamentazione  dell'esercizio dell'attività libero-professionale    &#13;
 dei  medici  veterinari dipendenti dal Servizio Sanitario Nazionale),    &#13;
 promossi  con  due  ordinanze  emesse il 15 luglio 1998 dal Tribunale    &#13;
 amministrativo  regionale  del Piemonte sui ricorsi proposti da G. Z.    &#13;
 contro Azienda sanitaria regionale di Chieri n. 8 ed altra e da L. B.    &#13;
 ed  altri contro Azienda sanitaria regionale n. 16 di Mondovì - Ceva    &#13;
 ed  altra, iscritte ai numeri 672 e 673 del registro ordinanze 1998 e    &#13;
 pubblicate  nella  Gazzetta  Ufficiale  della Repubblica n. 39, prima    &#13;
 serie speciale, dell'anno 1998.                                          &#13;
     Visti  gli  atti di costituzione di G. Z., L. B. ed altri nonché    &#13;
 gli atti di costituzione della Regione Piemonte;                         &#13;
     Udito  nell'udienza  pubblica  dell'8 febbraio  2000  il  giudice    &#13;
 relatore Piero Alberto Capotosti;                                        &#13;
     Uditi  l'avv.  Sebastiano  Zuccarello per G. Z., L. B. ed altri e    &#13;
 Gustavo Romanelli per la Regione Piemonte.                               &#13;
     Ritenuto  che  nel corso di due giudizi aventi rispettivamente ad    &#13;
 oggetto   l'annullamento   dell'atto   con  cui  l'Azienda  sanitaria    &#13;
 regionale  del  Piemonte n. 8 ha intimato ad un medico veterinario da    &#13;
 essa  dipendente  la  chiusura  di  un  ambulatorio  privato, nonché    &#13;
 l'annullamento della nota con cui l'Azienda sanitaria regionale n. 16    &#13;
 del   Piemonte  ha  chiesto  ad  alcuni  medici  veterinari  da  essa    &#13;
 dipendenti informazioni sulla loro attività libero professionale, il    &#13;
 Tribunale  amministrativo  regionale  del Piemonte, con due ordinanze    &#13;
 emesse    il    15 luglio    1998,    ha   sollevato   questione   di    &#13;
 costituzionalità,  nel  primo giudizio dell'art. 2 della legge della    &#13;
 Regione    Piemonte    3 gennaio    1997,    n. 4   (Regolamentazione    &#13;
 dell'esercizio   dell'attività   libero-professionale   dei   medici    &#13;
 veterinari  dipendenti dal Servizio Sanitario Nazionale), nel secondo    &#13;
 degli  articoli 1, comma 2, 2, 3 e 4 della stessa legge, in relazione    &#13;
 agli articoli 3, 4, 35, 117 e 120 della Costituzione;                    &#13;
         che,  ad  avviso  del  collegio,  le  disposizioni  impugnate    &#13;
 disciplinano la libera professione dei medici veterinari del Servizio    &#13;
 Sanitario  Nazionale  con  modalità  così  restrittive da impedirne    &#13;
 sostanzialmente  l'esercizio,  ed  appaiono quindi irragionevoli e in    &#13;
 contrasto  con  il  diritto  costituzionale al lavoro, con i principi    &#13;
 della  legislazione  statale  e  con  il  diritto  dei  cittadini  di    &#13;
 esercitare   in   ogni   parte   del  territorio  nazionale  la  loro    &#13;
 professione;                                                             &#13;
         che   secondo  i  giudici  a  quibus  l'art. 2  della  legge,    &#13;
 prevedendo  il  divieto  di  svolgere,  nel  territorio  dell'azienda    &#13;
 sanitaria  di  appartenenza,  la  libera  professione  sugli "animali    &#13;
 d'affezione", viola gli articoli 4 e 35 della Costituzione, in quanto    &#13;
 "determina  un  grave affievolimento delle facoltà professionali del    &#13;
 veterinario  senza  raccordarsi  funzionalmente a specifiche esigenze    &#13;
 della   struttura  sanitaria  pubblica",  e  sovrappone  il  criterio    &#13;
 territoriale  a  quello della potenziale situazione di conflitto, che    &#13;
 impone di "procedere alla individuazione in concreto delle situazioni    &#13;
 pregiudizievoli  per  i  fini  istituzionali  del  Servizio sanitario    &#13;
 nazionale";                                                              &#13;
         che l'art. 3 della legge impugnata implica, per i rimettenti,    &#13;
 una  analoga  "soppressione  di  ogni possibilità di esercizio della    &#13;
 libera professione", in quanto la consente sugli animali "da reddito"    &#13;
 solo "in caso di carenza di veterinari libero-professionisti", e pone    &#13;
 quindi  una  preclusione  che difetta di "ogni ponderato collegamento    &#13;
 con   le  esigenze  del  servizio  sanitario  pubblico",  sicché  è    &#13;
 inficiato da vizi che riguardano anche il successivo art. 4, il quale    &#13;
 estende  la disciplina degli articoli 2 e 3 all'attività veterinaria    &#13;
 sul   "cavallo   sportivo",   apparendo  altresì  illegittimo  anche    &#13;
 l'art. 1,  comma 2 della legge in ragione della sua "connessione" con    &#13;
 la disciplina dei precedenti articoli 2, 3 e 4;                          &#13;
         che,   ad  avviso  del  Tar,  le  norme  impugnate  vulnerano    &#13;
 l'articolo  3  della Costituzione, dato che prima riconoscono, e poi,    &#13;
 contraddittoriamente,  restringono  fino  a vanificare il diritto dei    &#13;
 veterinari        pubblici        all'esercizio        dell'attività    &#13;
 libero-professionale,  nonché  l'articolo  120 della Costituzione, a    &#13;
 causa dell'indebito limite territoriale che la legge impugnata appone    &#13;
 allo svolgimento della libera professione;                               &#13;
         che  la  legge  regionale  sarebbe  altresì in contrasto con    &#13;
 l'art. 117  della Costituzione, in quanto, in una materia nella quale    &#13;
 "la  competenza  regionale (...) conserva un ruolo secondario, ovvero    &#13;
 attuativo  di  principi  e  norme  stabilite a livello statale", essa    &#13;
 violerebbe  i  principi  fondamentali  posti dall'art. 47 della legge    &#13;
 23 dicembre   1978,   n. 833   (Istituzione  del  Servizio  Sanitario    &#13;
 Nazionale),  dall'art. 36,  primo comma, del d.P.R. 20 dicembre 1979,    &#13;
 n. 761 (Stato giuridico del personale delle unità sanitarie locali),    &#13;
 e  dall'art. 4,  settimo  comma, della legge 30 dicembre 1991, n. 412    &#13;
 (Disposizioni  in  materia di finanza pubblica), i quali stabiliscono    &#13;
 il   diritto   dei   veterinari   pubblici  dipendenti  all'esercizio    &#13;
 dell'attività libero-professionale;                                     &#13;
         che  si  è  costituita  in  giudizio la Regione Piemonte, in    &#13;
 persona  del Presidente della Giunta regionale, convenuta in entrambi    &#13;
 i giudizi principali, chiedendo che le questioni di costituzionalità    &#13;
 siano  dichiarate  inammissibili,  in  quanto il decreto del Ministro    &#13;
 della  sanità 31 luglio 1997 - recante disposizioni sulla "Attività    &#13;
 libero professionale e incompatibilità del personale della dirigenza    &#13;
 sanitaria   del   Servizio   Sanitario   Nazionale"   -   conterrebbe    &#13;
 disposizioni di contenuto analogo a quello delle norme impugnate, con    &#13;
 la   conseguenza   che,   anche   a   seguito  di  una  decisione  di    &#13;
 illegittimità  costituzionale,  esso  rimarrebbe comunque in vigore,    &#13;
 facendo    "quindi    venir    meno    l'interesse   dei   ricorrenti    &#13;
 all'impugnativa";                                                        &#13;
         che,  secondo la difesa della Regione, le questioni sarebbero    &#13;
 comunque infondate, in quanto le norme impugnate disciplinerebbero la    &#13;
 libera  professione  dei veterinari pubblici "secondo un criterio non    &#13;
 irragionevole",  "finalizzato ad assicurare la migliore funzionalità    &#13;
 del  servizio  pubblico  sanitario", mentre il riferimento all'art. 4    &#13;
 della  Costituzione  non  sarebbe  pertinente,  poiché  detta  norma    &#13;
 costituzionale  "concerne  precipuamente  l'accesso  al  mercato  del    &#13;
 lavoro";                                                                 &#13;
         che   si   sono  costituiti  i  ricorrenti  nei  due  giudizi    &#13;
 principali,  svolgendo  argomentazioni  a  sostegno dell'accoglimento    &#13;
 delle  questioni  di costituzionalità e deducendo in particolare che    &#13;
 limiti  all'attività  libero-professionale  dei  veterinari pubblici    &#13;
 possono  venire  disposti soltanto per grave e comprovato pregiudizio    &#13;
 al  servizio  sanitario nazionale, e che il legislatore regionale non    &#13;
 avrebbe  rispettato il principio che essi "devono essere dimensionati    &#13;
 in  relazione al tipo di attività svolta nell'ambito della struttura    &#13;
 pubblica,  e  non  anche  in  riferimento  al  luogo  in cui opera il    &#13;
 veterinario".                                                            &#13;
                                                                          &#13;
     Considerato   che   i   giudizi  hanno  ad  oggetto  le  medesime    &#13;
 disposizioni   di   legge   in   riferimento  agli  stessi  parametri    &#13;
 costituzionali   e   quindi   vanno   riuniti   per   essere   decisi    &#13;
 congiuntamente;                                                          &#13;
         che,  successivamente  alle ordinanze di rimessione, è stato    &#13;
 emanato  il  decreto legislativo 19 giugno 1999, n. 229 (Norme per la    &#13;
 razionalizzazione   del   servizio   sanitario   nazionale,  a  norma    &#13;
 dell'art. 1  della  legge  30 novembre  1998,  n. 419), il quale, fra    &#13;
 l'altro,  ha  stabilito,  all'art. 13,  una  nuova  disciplina  della    &#13;
 dirigenza medica e delle professioni sanitarie, la quale, come questa    &#13;
 Corte  ha rilevato, ha determinato il superamento della "stessa summa    &#13;
 divisio   fra   regime  dei  sanitari  che  svolgono  attività  c.d.    &#13;
 extramuraria   e   regime   dei   sanitari   che  svolgono  attività    &#13;
 intramuraria" (sentenza n. 63 del 2000);                                 &#13;
         che, in particolare, l'art. 15-quater del decreto legislativo    &#13;
 30 dicembre  1992,  n. 502,  nel  testo modificato dall'art. 13 dello    &#13;
 stesso decreto legislativo n. 229 del 1999, ha disciplinato, al comma    &#13;
 3,  anche  il  rapporto  di  lavoro  di  coloro  che  erano già alle    &#13;
 dipendenze  del  Servizio  Sanitario Nazionale, stabilendo, a seguito    &#13;
 della   ulteriore   modifica   introdotta   dall'art. 1  del  decreto    &#13;
 legislativo  2 marzo 2000, n. 49 (Disposizioni correttive del decreto    &#13;
 legislativo 19 giugno 1999, n. 229, concernenti il termine di opzione    &#13;
 per il rapporto esclusivo da parte dei dirigenti sanitari), che entro    &#13;
 il  14 marzo  2000  "tutti  i  dirigenti  in  servizio  alla data del    &#13;
 31 dicembre  1998  sono  tenuti  a  comunicare  al direttore generale    &#13;
 l'opzione  in  ordine al rapporto esclusivo", e che anche "in assenza    &#13;
 di  comunicazione  si  presume  che il dipendente abbia optato per il    &#13;
 rapporto esclusivo", prevedendo altresì, al comma 1, che i dirigenti    &#13;
 sanitari "con i quali sia stato stipulato il contratto di lavoro o un    &#13;
 nuovo  contratto  di  lavoro  in data successiva al 31 dicembre 1998,    &#13;
 nonché  quelli  che,  alla  data  di  entrata in vigore del presente    &#13;
 decreto  (...)  abbiano  optato per l'esercizio dell'attività libero    &#13;
 professionale  intramuraria,  sono assoggettati al rapporto di lavoro    &#13;
 esclusivo";                                                              &#13;
         che,  infine,  il  successivo art. 15-sexies comma 1, dispone    &#13;
 che  lo stesso rapporto di lavoro dei dirigenti sanitari, che abbiano    &#13;
 comunicato   l'opzione   per  l'esercizio  della  libera  professione    &#13;
 extramuraria,   "comporta   la   totale   disponibilità  nell'ambito    &#13;
 dell'impegno   di   servizio,  per  la  realizzazione  dei  risultati    &#13;
 programmati   e  lo  svolgimento  delle  attività  professionali  di    &#13;
 competenza";                                                             &#13;
         che  la  predetta  sopravvenuta disciplina modifica il quadro    &#13;
 normativo   di   riferimento   considerato  dai  giudici  rimettenti,    &#13;
 cosicché si impone un nuovo esame della rilevanza delle questioni di    &#13;
 costituzionalità nei giudizi a quibus.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti  i giudizi ordina la restituzione degli atti al Tribunale    &#13;
 amministrativo regionale del Piemonte.                                   &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 22 marzo 2000.                                &#13;
                         Il Presidente: Guizzi                            &#13;
                        Il redattore: Capotosti                           &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria il 28 marzo 2000.                          &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
