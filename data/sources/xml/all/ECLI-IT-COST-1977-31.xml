<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1977</anno_pronuncia>
    <numero_pronuncia>31</numero_pronuncia>
    <ecli>ECLI:IT:COST:1977:31</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>ROSSI</presidente>
    <relatore_pronuncia>Michele Rossano</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/01/1977</data_decisione>
    <data_deposito>18/01/1977</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. PAOLO ROSSI, Presidente - Dott. LUIGI &#13;
 OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - Prof. ENZO &#13;
 CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO CRISAFULLI &#13;
 - Dott. NICOLA REALE - Avv. LEONETTO AMADEI - Dott. GIULIO GIONFRIDA - &#13;
 Prof. EDOARDO VOLTERRA - Prof. GUIDO ASTUTI - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di legittimità costituzionale degli artt. 111 e 112  &#13;
 del d.P.R. 30 giugno 1965, n. 1124 (Testo unico delle disposizioni  per  &#13;
 l'assicurazione  obbligatoria  contro  gli  infortuni  sul  lavoro e le  &#13;
 malattie professionali), promosso con ordinanza emessa il 3 giugno 1974  &#13;
 dal giudice del lavoro  del  tribunale  di  Macerata  nel  procedimento  &#13;
 civile  vertente  tra  Scaramella Felice Antonio e l'Istituto nazionale  &#13;
 per l'assicurazione contro gli infortuni sul lavoro, iscritta al n. 409  &#13;
 del registro ordinanze 1974 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 296 del 13 novembre 1974.                                  &#13;
     Visti gli atti di  costituzione  di  Scaramella  Felice  Antonio  e  &#13;
 dell'INAIL,  nonché  l'atto  d'intervento del Presidente del Consiglio  &#13;
 dei ministri;                                                            &#13;
     udito  nell'udienza  pubblica  del  24  novembre  1976  il  Giudice  &#13;
 relatore Michele Rossano;                                                &#13;
     uditi   l'avv.  Pasquale  Nappi,  per  Scaramella,  l'avv.    Carlo  &#13;
 Graziani, per l'INAIL, ed il sostituto avvocato  generale  dello  Stato  &#13;
 Giorgio Azzariti, per il Presidente del Consiglio dei ministri;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Nel  corso  del  procedimento  civile  - promosso da Felice Antonio  &#13;
 Scaramella, con citazione 3 maggio 1972, nei  confronti  dell'INAIL  al  &#13;
 fine  di  ottenere le prestazioni spettanti per l'infortunio sul lavoro  &#13;
 subito il 12 aprile 1967 -  il  giudice  istruttore  del  tribunale  di  &#13;
 Macerata  -  in  funzione di giudice del lavoro unico a norma dell'art.  &#13;
 20, comma terzo,  legge  11  agosto  1973,  n.  533  (Disciplina  delle  &#13;
 controversie  in  materia di previdenza e di assistenza obbligatorie) -  &#13;
 sollevava, di ufficio, con ordinanza 3  giugno  1974  le  questioni  di  &#13;
 legittimità  costituzionale  degli  artt.  111  e 112 d.P.R. 30 giugno  &#13;
 1965,  n.  1124  (Testo  unico  delle  disposizioni per l'assicurazione  &#13;
 obbligatoria  contro  gli  infortuni   sul   lavoro   e   le   malattie  &#13;
 professionali),  in  relazione all'art. 58 legge 30 aprile 1969, n. 153  &#13;
 (Revisione degli  ordinamenti  pensionistici  e  norme  in  materia  di  &#13;
 sicurezza   sociale)  ed  in  riferimento  agli  artt.  3  e  24  della  &#13;
 Costituzione.                                                            &#13;
     L'ordinanza è stata pubblicata nella Gazzetta Ufficiale n. 296 del  &#13;
 13 novembre 1974.                                                        &#13;
     Nel giudizio davanti a questa Corte è  intervenuto  il  Presidente  &#13;
 del  Consiglio  dei  ministri  e  si  sono costituite le parti, INAIL e  &#13;
 Felice Antonio Scaramella.                                               &#13;
     Il Presidente del Consiglio dei ministri,  rappresentato  e  difeso  &#13;
 dall'Avvocato  generale  dello Stato, con atto depositato il 18 ottobre  &#13;
 1974, e l'INAIL, con deduzioni depositate il 3 dicembre 1974 e  memoria  &#13;
 depositata  il  12  novembre  1976,  hanno  chiesto che le questioni di  &#13;
 legittimità costituzionale siano dichiarate non fondate.                &#13;
     Felice Antonio Scaramella, con deduzioni depositate il  3  dicembre  &#13;
 1974,  ha chiesto che le questioni di legittimità costituzionale siano  &#13;
 dichiarate fondate.<diritto>Considerato in diritto</diritto>:                          &#13;
     Il giudice del lavoro del tribunale di Macerata  ha  sollevato  due  &#13;
 questioni  di  legittimità costituzionale degli artt. 111 e 112 d.P.R.  &#13;
 30  giugno  1965,  n.  1124  (Testo  unico   delle   disposizioni   per  &#13;
 l'assicurazione  obbligatoria  contro  gli  infortuni  sul  lavoro e le  &#13;
 malattie professionali), con  riferimento  agli  artt.  3  e  24  della  &#13;
 Costituzione.                                                            &#13;
     Secondo  il  giudice a quo gli artt. 111 e 112 citati regolerebbero  &#13;
 fattispecie identiche a quella prevista nell'art. 58 legge n.  153  del  &#13;
 1969  in  quanto  gli  artt.  111  e 112 stabiliscono in tre anni e 150  &#13;
 giorni,  a  decorrere  dalla  data  dell'infortunio,  il   periodo   di  &#13;
 prescrizione  del  diritto alle prestazioni dovute dall'INAIL, e l'art.  &#13;
 58  stabilisce  la  prescrizione  di  dieci  anni  del   diritto   alle  &#13;
 prestazioni  dovute dall'INPS, a decorrere dalla data della definizione  &#13;
 del procedimento amministrativo.  Le  fattispecie  sarebbero  identiche  &#13;
 perché  in  entrambi i casi concernono il diritto dell'assicurato alle  &#13;
 prestazioni dovute dall'ente assicuratore per  le  menomazioni  fisiche  &#13;
 causate,  in  un  caso, da infortunio sul lavoro e, nell'altro caso, da  &#13;
 malattie comuni.                                                         &#13;
     Sarebbero, quindi, violati, gli artt. 3 e 24 della Costituzione.     &#13;
     Le censure non sono fondate.                                         &#13;
     La congruità di un termine di prescrizione va valutata non solo in  &#13;
 rapporto all'interesse di chi ha l'onere di osservarlo,  ma  anche  con  &#13;
 riguardo  alla funzione assegnata al termine nell'ordinamento giuridico  &#13;
 (cfr. sentenze n. 57 del 1962; n. 10 del 1970 e n. 138 del 1975). E  il  &#13;
 termine di prescrizione di tre anni trova giustificazione nell'esigenza  &#13;
 che  il  diritto  al risarcimento del danno da infortunio sia accertato  &#13;
 nel più breve tempo possibile nell'interesse dello stesso  danneggiato  &#13;
 e  per ovvie ragioni obiettive concernenti la raccolta delle prove, che  &#13;
 l'adozione di  un  più  lungo  termine  avrebbe  potuto  pregiudicare;  &#13;
 laddove  siffatte esigenze di acquisizione e conservazione delle prove,  &#13;
 specie di quelle sul rapporto causale tra l'invalidità e l'infortunio,  &#13;
 non sussistono nelle controversie dirette ad  ottenere  le  prestazioni  &#13;
 previdenziali  previste  per  le  infermità comuni, dato che in queste  &#13;
 controversie l'accertamento ha per oggetto solo l'esistenza e la natura  &#13;
 delle stesse infermità.                                                 &#13;
     Non è, quindi, violato l'art. 3 della Costituzione, non essendo le  &#13;
 fattispecie  identiche;  né è violato l'art. 24 della Costituzione in  &#13;
 quanto la lesione del principio del diritto di difesa per brevità  del  &#13;
 termine  di  prescrizione di tre anni non è configurata nell'ordinanza  &#13;
 nemmeno come concreta eventualità.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondate le questioni  di  legittimità  costituzionale  &#13;
 degli  artt.  111 e 112 del d.P.R. 30 giugno 1965, n. 1124 (Testo unico  &#13;
 delle  disposizioni  per  l'assicurazione   obbligatoria   contro   gli  &#13;
 infortuni  sul  lavoro  e  le  malattie  professionali),  sollevate dal  &#13;
 giudice del lavoro del tribunale di Macerata  con  ordinanza  3  giugno  &#13;
 1974, in riferimento agli artt. 3 e 24 della Costituzione.               &#13;
     Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,  &#13;
 Palazzo della Consulta, il 4 gennaio 1977.                               &#13;
                                   F.to: PAOLO ROSSI - LUIGI  OGGIONI  -  &#13;
                                   ANGELO  DE MARCO - ERCOLE ROCCHETTI -  &#13;
                                   ENZO  CAPALOZZA  -  VINCENZO  MICHELE  &#13;
                                   TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
                                   REALE  -  LEONETTO  AMADEI  -  GIULIO  &#13;
                                   GIONFRIDA - EDOARDO VOLTERRA -  GUIDO  &#13;
                                   ASTUTI  -  MICHELE ROSSANO - ANTONINO  &#13;
                                   DE STEFANO - LEOPOLDO ELIA.            &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
