<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1975</anno_pronuncia>
    <numero_pronuncia>215</numero_pronuncia>
    <ecli>ECLI:IT:COST:1975:215</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BONIFACIO</presidente>
    <relatore_pronuncia>Vincenzo Trimarchi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/07/1975</data_decisione>
    <data_deposito>15/07/1975</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. FRANCESCO PAOLO BONIFACIO, Presidente - &#13;
 Dott. LUIGI OGGIONI - Avv. ANGELO DE MARCO - Avv. ERCOLE ROCCHETTI - &#13;
 Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE TRIMARCHI - Prof. VEZIO &#13;
 CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO ROSSI - Avv. LEONETTO &#13;
 AMADEI - Dott. GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Prof. GUIDO &#13;
 ASTUTI - Dott. MICHELE ROSSANO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale degli artt. 261  e  262  &#13;
 del  d.P.R.  29  gennaio  1958,  n.  645 (testo unico delle leggi sulle  &#13;
 imposte dirette), promosso con ordinanza emessa il 12 luglio  1973  dal  &#13;
 tribunale  di  Roma  sul  ricorso  dell'esattore del Comune di Roma per  &#13;
 ottenere  la  dichiarazione  di  fallimento  fiscale   della   società  &#13;
 Farnesinatre,  iscritta  al  n.  309  del  registro  ordinanze  1973  e  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  236  del  12  &#13;
 settembre 1973.                                                          &#13;
     Visto l'atto di costituzione dell'esattore del Comune di Roma;       &#13;
     udito  nell'udienza pubblica del 18 giugno 1975 il Giudice relatore  &#13;
 Vincenzo Michele Trimarchi;                                              &#13;
     udito l'avv. Giuseppe Mesiano, per l'esattore del Comune di Roma.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con ricorso del 31 maggio 1972 l'esattore del Comune di  Roma,  &#13;
 autorizzato dall'Intendente di finanza, ha chiesto al tribunale di Roma  &#13;
 di  pronunciare,  ex artt.  261 e 262 del t.u. 29 gennaio 1958, n. 645,  &#13;
 il fallimento fiscale della s.r.1. Farnesinatre, morosa  nel  pagamento  &#13;
 di   sei  rate  consecutive  d'imposte  relative  a  redditi  derivanti  &#13;
 dall'esercizio  di  attività  commerciale,  per  l'ammontare   di   L.  &#13;
 18.374.659.                                                              &#13;
     A  tale  istanza  si  è  opposta  la  società con ricorso facendo  &#13;
 presente che avverso  gli  accertamenti  delle  imposte  di  cui  sopra  &#13;
 pendeva contestazione davanti alle competenti commissioni tributarie ed  &#13;
 eccependo  l'illegittimità  costituzionale  dei citati artt. 261 e 262  &#13;
 del t.u. n. 645 del  1958,  per  contrasto  con  gli  artt.  24,  comma  &#13;
 secondo, 25 e 101 della Costituzione.                                    &#13;
     Il tribunale di Roma, premesso che in materia di fallimento fiscale  &#13;
 è preventivamente determinata la competenza del tribunale a promuovere  &#13;
 la  dichiarazione  di fallimento, ha, con ordinanza del 12 luglio 1973,  &#13;
 sollevato questione di legittimità costituzionale degli  artt.  261  e  &#13;
 262  del  citato t.u. in riferimento agli artt. 3, 24, comma secondo, e  &#13;
 101 della Costituzione.                                                  &#13;
     a) Secondo il giudice a quo all'imprenditore  contribuente  non  è  &#13;
 assicurato  il  diritto  di  difesa,  perché  non gli è consentito di  &#13;
 eccepire  e  di   dimostrare   davanti   al   tribunale,   nella   fase  &#13;
 prefallimentare,  di  non  essere  affatto  debitore, nonostante che il  &#13;
 credito, in base al quale è chiesto il fallimento, sia iscritto,  come  &#13;
 nella  specie,  provvisoriamente  nei  ruoli nella pendenza del ricorso  &#13;
 davanti alle commissioni tributarie, e  pur  essendo  possibile  che  a  &#13;
 seguito  dell'accoglimento  di  tale  ricorso  risulti  addirittura non  &#13;
 esistente il credito e quindi il presupposto dell'inadempimento.         &#13;
     Violerebbe,  pertanto,  il  diritto  alla   difesa   "un   istituto  &#13;
 processuale  -  il  fallimento - che pretende di essere applicato senza  &#13;
 offrire la possibilità al soggetto passivo di  eccepire  e  dimostrare  &#13;
 che egli non è tale, perché non debitore".                             &#13;
     b) Osservato che al tribunale, giudice naturale di ogni fallimento,  &#13;
 spetta  per  legge  il  potere  di  accertamento dei presupposti per la  &#13;
 dichiarazione di fallimento e  rilevato  che,  in  caso  di  fallimento  &#13;
 fiscale,  "il  giudizio sull'esistenza del presupposto obiettivo per la  &#13;
 dichiarazione di fallimento è affidato  esclusivamente  all'Intendente  &#13;
 di  finanza",  l'art. 101 della Costituzione risulterebbe violato dalle  &#13;
 norme denunciate, in quanto le stesse profilano come dovuta la sentenza  &#13;
 dichiarativa di fallimento ed è completamente soppressa la libertà di  &#13;
 giudizio  del  tribunale  che  rimane  vincolato  alla  volontà  della  &#13;
 pubblica amministrazione e non può convincersi della insussistenza del  &#13;
 credito in oggetto.                                                      &#13;
     c)  Si  avrebbe, infine, la violazione del principio di eguaglianza  &#13;
 perché non  distinguendosi,  in  materia,  tra  credito  definitivo  e  &#13;
 credito  provvisorio,  si  assoggetta alla stessa sanzione (fallimento)  &#13;
 sia   l'imprenditore   moroso   che   non   abbia   mosso   opposizione  &#13;
 all'accertamento  tributario e sia l'imprenditore che, ritenendo di non  &#13;
 dovere il carico di imposte iscritto provvisoriamente a ruolo, ha adito  &#13;
 le competenti commissioni.                                               &#13;
     Viene  così  applicato  il  medesimo  trattamento   a   situazioni  &#13;
 obiettive diverse: al caso di morosità che è palese manifestazione di  &#13;
 insolvenza  e  al  caso  di  morosità  che  da  sola  non  può essere  &#13;
 considerata indice di insolvenza,  dato  che  l'imprenditore  non  paga  &#13;
 perché non si ritiene debitore.                                         &#13;
     2.  -  Davanti  a  questa  Corte  non  ha  spiegato  intervento  il  &#13;
 Presidente  del  Consiglio  dei  ministri;  si  è  invece   costituito  &#13;
 l'esattore del Comune di Roma, che, a mezzo dell'avv. Giuseppe Mesiano,  &#13;
 ha chiesto che la questione fosse dichiarata non fondata.                &#13;
     La   difesa   dell'attore   ha,   anzitutto,  osservato  che  unico  &#13;
 presupposto oggettivo per la dichiarazione di fallimento fiscale è  la  &#13;
 morosità  nei  pagamenti  e  non  la certezza o meno del debito in via  &#13;
 definitiva, e che, in sede di valutazione della  relativa  istanza,  il  &#13;
 tribunale  non  è  tenuto  a  verificare  se l'obbligazione tributaria  &#13;
 sussista o meno in via definitiva quanto, invece, ad  accertare  se  la  &#13;
 pretesa  manifestata  dall'erario con la iscrizione a ruolo del tributo  &#13;
 sia stata o meno soddisfatta.                                            &#13;
     Non può,  quindi,  parlarsi  di  violazione  dell'art.  24,  comma  &#13;
 secondo,  perché  "la  controversia innanzi al tribunale si svolge nei  &#13;
 modi e con le garanzie previste dalla legge fallimentare  e  da  quella  &#13;
 processuale,  né  le  disposizioni  della  legge  speciale frappongono  &#13;
 ostacoli processuali alle parti per far valere le proprie ragioni".      &#13;
     La difesa  dell'esattore  ha  rilevato,  ancora,  che  non  risulta  &#13;
 violato  neppure l'art. 101 perché, di fronte alla documentata domanda  &#13;
 di fallimento fiscale  avanzata,  tramite  l'esattore,  dalla  pubblica  &#13;
 amministrazione,  è  compito del tribunale di accertare la particolare  &#13;
 morosità dell'imprenditore-contribuente (e non se sia legittima o meno  &#13;
 la pretesa fiscale, culminata nell'avviso di accertamento impugnato dal  &#13;
 contribuente, e  non  ancora  definita)  e  di  applicare  la  sanzione  &#13;
 prevista  dalle  norme  impugnate  ed  emettere  la  richiesta sentenza  &#13;
 dichiarativa di fallimento. Il giudice  è,  pertanto,  vincolato  solo  &#13;
 alla  legge,  di  cui  si  richiede l'applicazione, una volta accertata  &#13;
 l'esistenza  dei  presupposti   per   l'emissione   del   provvedimento  &#13;
 richiesto.                                                               &#13;
     Ed  infine,  secondo  la  difesa dell'esattore, non sarebbe violato  &#13;
 l'art. 3. Dato  che  il  tribunale,  ai  fini  della  dichiarazione  di  &#13;
 fallimento,  ha  da  accertare  solo  il  fatto  obiettivo  del mancato  &#13;
 pagamento di sei  rate  consecutive  d'imposte  sui  redditi  derivanti  &#13;
 dall'esercizio  dell'impresa  commerciale,  e  non la certezza o meglio  &#13;
 ancora la definitività del credito tributario, e deve  prescindere  da  &#13;
 ogni  indagine  sulla  sussistenza  di uno stato di insolvenza, "non si  &#13;
 può far distinzione fra i due casi prospettati (dal tribunale di Roma,  &#13;
 nella  specie)  e  rilevarne  disparità  di  trattamento   in   ordine  &#13;
 all'applicazione della sanzione".                                        &#13;
     "L'equiparazione  del presupposto dell'insolvenza, richiesto per la  &#13;
 dichiarazione di fallimento ordinario, a quello dell'inadempimento  del  &#13;
 debito d'imposta, richiesto per la dichiarazione di fallimento fiscale,  &#13;
 appare  pienamente  giustificata,  tenuto conto della difformità delle  &#13;
 posizioni  che,  nell'attuale  ordinamento  economico-  sociale,   sono  &#13;
 rispettivamente  occupate  dal  comune imprenditore e dall'imprenditore  &#13;
 contribuente.  Ciò  essenzialmente,  in  ragione   della   particolare  &#13;
 rilevanza  che  l'inadempimento  di qualsiasi altra obbligazione civile  &#13;
 assume come violazione di un obbligo costituzionalmente  sancito  (art.  &#13;
 53 della Costituzione)".<diritto>Considerato in diritto</diritto>:                          &#13;
     1.  -  Con  l'ordinanza  indicata  in epigrafe il tribunale di Roma  &#13;
 solleva la questione di legittimità costituzionale degli artt.  261  e  &#13;
 262  del  d.P.R. 29 gennaio 1958, n. 645 (testo unico delle leggi sulle  &#13;
 imposte dirette), in riferimento agli artt. 3, 24, secondo comma, e 101  &#13;
 della Costituzione.                                                      &#13;
     2. - La Corte, con sue precedenti pronunce (sentenza n.    114  del  &#13;
 1974 ed ordinanza n. 195 del 1971), ha avuto modo di valutare i rilievi  &#13;
 mossi  da vari giudici circa la legittimità costituzionale delle norme  &#13;
 ancora una volta denunciate, in riferimento agli  artt.  3  e  4  della  &#13;
 Costituzione.                                                            &#13;
     Ora  si insiste sul contrasto delle ripetute norme con il principio  &#13;
 di eguaglianza e si denuncia per la prima  volta  il  mancato  rispetto  &#13;
 degli artt. 24, comma secondo, e 101.                                    &#13;
     3.  - Il profilo sotto il quale si assume che sia violato l'art. 3,  &#13;
 risulta nuovo. Nelle due precedenti occasioni, infatti, si è messo  in  &#13;
 evidenza  o che a fronte di una eguale situazione (mancato pagamento da  &#13;
 parte di contribuenti di sei rate consecutive d'imposta) erano previsti  &#13;
 effetti giuridici diversi (ammenda o fallimento fiscale) a seconda  che  &#13;
 il  contribuente  non  fosse  o fosse un imprenditore commerciale ed in  &#13;
 questo caso il reddito imponibile derivasse dall'esercizio  di  imprese  &#13;
 commerciali;  o  che  relativamente  ad  un'unica  situazione  (mancato  &#13;
 pagamento da parte di imprenditori commerciali di sei rate  consecutive  &#13;
 d'imposta)   erano   previsti  effetti  giuridici  diversi  (ammenda  o  &#13;
 fallimento fiscale) a seconda che  il  contribuente  fosse  moroso  nel  &#13;
 pagamento   di   tributi   sui   redditi   non  derivanti  o  derivanti  &#13;
 dall'esercizio di imprese commerciali, o che in presenza di  situazioni  &#13;
 diverse    (esistenza    o    mancanza    del   presupposto   oggettivo  &#13;
 dell'insolvenza) era previsto un unico effetto  giuridico  (fallimento)  &#13;
 con   conseguenze   anche   nei   confronti   di   tutti  i  creditori.  &#13;
 Nell'ordinanza de qua, invece,  si  denuncia  che  situazioni  diverse,  &#13;
 quali  sarebbero  quelle in cui si trovano gli imprenditori commerciali  &#13;
 che abbiano o non abbiano accettato il carico tributario e però  siano  &#13;
 morosi  nel  pagamento  di  sei  rate consecutive d'imposte sui redditi  &#13;
 derivanti  dall'esercizio  d'imprese  commerciali,  sarebbero  trattate  &#13;
 ingiustificatamente  in  modo  eguale  (con la previsione in entrambi i  &#13;
 casi, della dichiarazione di fallimento fiscale).                        &#13;
     L'assunta violazione del principio d'eguaglianza  non  ricorre.  La  &#13;
 differenza  tra  le  due  situazioni,  secondo  il  tribunale  di Roma,  &#13;
 consisterebbe in ciò che in un caso il credito è "definitivo"  ed  il  &#13;
 contribuente  non  ha mosso opposizione all'accertamento tributario, e,  &#13;
 nell'altro il credito è "provvisorio" ed il contribuente, ritenendo di  &#13;
 non dovere il carico di imposte iscritto provvisoriamente a  ruolo,  ha  &#13;
 adito  le  competenti  commissioni,  ed  in  sostanza  nel fatto che la  &#13;
 morosità in un caso è palese manifestazione d'insolvenza e nell'altro  &#13;
 da sola non può essere considerata  indice  d'insolvenza.    Senonché  &#13;
 tali  punti o profili di differenziazione non sono decisivi o rilevanti  &#13;
 nell'indicato  senso.  Nonostante  l'esistenza  di  qualche   decisione  &#13;
 giurisprudenziale  in  contrario, non è da ritenere che, ai fini della  &#13;
 pronuncia del fallimento fiscale,  l'iscrizione  a  ruolo  del  tributo  &#13;
 debba  essere definitiva e non possa anche essere provvisoria: il fatto  &#13;
 che  condiziona  la  richiesta  e  la  pronuncia  di   fallimento,   è  &#13;
 rappresentato  dall'iscrizione a ruolo a titolo definitivo o operata in  &#13;
 via provvisoria a seguito di accertamento non  definitivo,  di  tributi  &#13;
 sui  redditi  derivanti  dall'esercizio  di  imprese commerciali, e dal  &#13;
 mancato pagamento di sei rate consecutive delle dette  imposte.  E  non  &#13;
 importa  se  l'iscrizione a ruolo sia definitiva o provvisoria, perché  &#13;
 in  ogni  caso,  dato  che  l'iscrizione   sia   legalmente   avvenuta,  &#13;
 l'Amministrazione finanziaria ha diritto di pretendere dal contribuente  &#13;
 il  pagamento  dell'imposta  e  di  perseguire  la  realizzazione della  &#13;
 pretesa in sede di esecuzione individuale o concorsuale.                 &#13;
     E del pari deve escludersi che la morosità ricorrente nei due casi  &#13;
 non  si  presti  ad  essere  univocamente considerata: nella specie, il  &#13;
 motivo per cui il contribuente sia in mora nel  pagamento  dell'imposta  &#13;
 non  ha  rilievo,  e  conta  invece  il  fatto nella sua oggettività e  &#13;
 materialità. E  poi  il  riferimento  all'insolvenza,  nella  speciale  &#13;
 materia  del  fallimento  fiscale, non ha ragione di essere giacché il  &#13;
 presupposto oggettivo di detto fallimento è rappresentato dal ripetuto  &#13;
 inadempimento, e non interessa se  questo  possa  o  non  possa  essere  &#13;
 assunto come indice o sintomo di insolvenza.                             &#13;
     4.  -  Per il fatto che, nella fase del procedimento che precede la  &#13;
 dichiarazione di  fallimento  fiscale,  non  è  dato  all'imprenditore  &#13;
 commerciale  di  eccepire e dimostrare di non essere debitore, non può  &#13;
 ammettersi che,  come  ritiene  invece  il  giudice  a  quo,  le  norme  &#13;
 denunciate  siano  in  contrasto  con  l'art.  24, comma secondo, della  &#13;
 Costituzione.                                                            &#13;
     E così pure non si può convenire con lo stesso  giudice,  che  il  &#13;
 tribunale  fallimentare,  nello  stesso  procedimento, non sia soggetto  &#13;
 soltanto alla legge (e sia per ciò, con le norme  denunciate,  violato  &#13;
 l'art.  101,  comma  secondo, della Costituzione), perché "il giudizio  &#13;
 sull'esistenza  del  presupposto  obiettivo  per  la  dichiarazione  di  &#13;
 fallimento è affidato esclusivamente all'Intendente di finanza".        &#13;
     I  due  profili  si  presentano  connessi  in quanto attengono alle  &#13;
 posizioni giuridiche del soggetto  passivo  e  del  giudice  che  sono,  &#13;
 ancorché  variamente,  interessati  alla vicenda processuale di cui si  &#13;
 tratta.                                                                  &#13;
     Dalla loro congiunta  considerazione  emerge  che  nell'ipotesi  di  &#13;
 fallimento  fiscale  ex  art. 261 e 262 del d.P.R. n. 645 del 1958, sia  &#13;
 l'imprenditore commerciale che  il  tribunale  vengono  a  trovarsi  in  &#13;
 particolari   situazioni   giuridiche   soggettive  perché,  al  posto  &#13;
 dell'insolvenza quale presupposto oggettivo è necessario e sufficiente  &#13;
 il sopraddetto inadempimento ed a questo (e non  anche  all'insolvenza)  &#13;
 è rivolto e limitato l'accertamento da parte del tribunale.             &#13;
     Per ciò che all'imprenditore commerciale sia in tal caso negato di  &#13;
 eccepire   e  dimostrare  di  non  essere  debitore,  non  c'è  alcuna  &#13;
 violazione dell'art. 24, comma  secondo.  Il  diritto  alla  difesa  ed  &#13;
 all'assistenza  professionale  e  tecnica  in giudizio è adeguatamente  &#13;
 assicurato,  avendo  qui  l'interessato  le  stesse  possibilità   che  &#13;
 spettano  ad  ogni altro imprenditore commerciale di cui sia chiesta la  &#13;
 dichiarazione di fallimento, e potendole far valere negli stessi  modi.  &#13;
 Ed  è  giurisprudenza  costante  di  questa Corte che è rispettata la  &#13;
 disposizione di cui all'articolo 24, comma secondo, qualora la norma di  &#13;
 legge ordinaria garantisca l'esercizio nel  processo  delle  situazioni  &#13;
 giuridiche quali emergono dalla legge sostanziale, e nel caso in esame,  &#13;
 questa,  in  presenza di date condizioni, consente l'iscrizione a ruolo  &#13;
 del tributo, in via provvisoria.                                         &#13;
     E così non sussiste nelle norme  denunciate  alcun  contrasto  con  &#13;
 l'art.  101  della  Costituzione.  Il  potere  di decisione normalmente  &#13;
 spettante all'organo giurisdizionale nel fallimento non subisce,  nella  &#13;
 specie, alcuna limitazione o compressione, ed in particolare non si ha,  &#13;
 da  parte dell'organo fallimentare, alcun assoggettamento alla volontà  &#13;
 della pubblica amministrazione: il  tribunale,  in  via  esclusiva,  è  &#13;
 chiamato ad accertare ed accerta, in funzione della richiesta pronuncia  &#13;
 di  fallimento,  l'esistenza in concreto dei presupposti del fallimento  &#13;
 ed  in  particolare  di  quello  obiettivo  costituito   dal   ripetuto  &#13;
 inadempimento.                                                           &#13;
     Stante  ciò,  la  questione  di legittimità costituzionale de qua  &#13;
 anche sotto gli indicati due profili si appalesa non fondata.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara non fondata la questione  di  legittimità  Costituzionale  &#13;
 degli  artt.  261 e 262 del d.P.R. 29 gennaio 1958, n. 645 (testo unico  &#13;
 delle leggi sulle imposte dirette), sollevata in riferimento agli artt.  &#13;
 3, 24, comma secondo, e 101 della Costituzione con l'ordinanza indicata  &#13;
 in epigrafe.                                                             &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, l'8 luglio 1975.                                 &#13;
                                   FRANCESCO  PAOLO  BONIFACIO  -  LUIGI  &#13;
                                   OGGIONI - ANGELO DE  MARCO  -  ERCOLE  &#13;
                                   ROCCHETTI - ENZO CAPALOZZA - VINCENZO  &#13;
                                   MICHELE  TRIMARCHI - VEZIO CRISAFULLI  &#13;
                                   -  NICOLA  REALE  -  PAOLO  ROSSI   -  &#13;
                                   LEONETTO  AMADEI - GIULIO GIONFRIDA -  &#13;
                                   EDOARDO VOLTERRA  -  GUIDO  ASTUTI  -  &#13;
                                   MICHELE ROSSANO.                       &#13;
                                   ARDUINO SALUSTRI - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
