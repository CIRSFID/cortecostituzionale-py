<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2003</anno_pronuncia>
    <numero_pronuncia>272</numero_pronuncia>
    <ecli>ECLI:IT:COST:2003:272</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CHIEPPA</presidente>
    <relatore_pronuncia>Ugo De Siervo</relatore_pronuncia>
    <redattore_pronuncia>Ugo De Siervo</redattore_pronuncia>
    <data_decisione>03/07/2003</data_decisione>
    <data_deposito>22/07/2003</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Presidente: Riccardo CHIEPPA; Giudici: Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK, Francesco AMIRANTE, Ugo DE SIERVO, Romano VACCARELLA, Paolo MADDALENA, Alfio FINOCCHIARO,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di ammissibilità del conflitto tra poteri dello Stato sorto a seguito della delibera della Camera dei deputati dell'11 gennaio 2000, relativa alla insindacabilità delle opinioni espresse dall'onorevole Umberto Bossi, imputato in un procedimento penale per reato di vilipendio alla bandiera, promosso dal Tribunale di Venezia - sezione penale, con ricorso depositato il 2 luglio 2002 ed iscritto al n. 226 del registro ammissibilità conflitti. &#13;
      Udito nella camera di consiglio del 7 maggio 2003 il Giudice relatore Ugo De Siervo. &#13;
    Ritenuto che con ordinanza del 2 marzo 2002, pervenuta alla Corte costituzionale il 2 luglio 2002, il Tribunale di Venezia, ufficio del giudice monocratico penale, nell'ambito del procedimento instaurato nei confronti del deputato Umberto Bossi, in relazione al reato di cui agli artt. 81 cpv e 292, primo e terzo comma del codice penale, ha sollevato conflitto di attribuzioni tra i poteri dello Stato avverso la delibera, adottata in data 11 gennaio 2000, con la quale la Camera dei deputati ha dichiarato che i fatti per i quali è in corso il procedimento penale più sopra citato costituiscono opinioni espresse dal deputato Umberto Bossi nell'esercizio delle sue funzioni a norma dell'art. 68, primo comma, della Costituzione; &#13;
    che nell'atto introduttivo del giudizio si evidenzia come nell'ambito del suddetto procedimento sia contestato all'on. Bossi il reato di vilipendio alla bandiera perché, mentre si trovava in Venezia il 14 settembre 1997, avrebbe rivolto ad una persona che teneva esposta alla finestra la bandiera italiana la seguente frase: "Il tricolore lo metta al cesso, signora” e aveva poi aggiunto: "Ho ordinato un camion di carta igienica tricolore personalmente, visto che è un magistrato che dice che non posso avere la carta igienica tricolore”; &#13;
    che l'autorità giurisdizionale ricorrente rileva come, in data 11 gennaio 2000, la Camera dei deputati abbia adottato una deliberazione con la quale si affermava che le suddette dichiarazioni del deputato Bossi concernevano opinioni espresse da quest'ultimo nell'esercizio delle funzioni parlamentari, ricadendo conseguentemente nell'ambito di applicazione dell'art. 68, primo comma, della Costituzione; &#13;
    che, secondo l'organo giurisdizionale ricorrente, tale delibera della Camera dei deputati sarebbe lesiva delle proprie attribuzioni costituzionali, a causa della mancanza del nesso funzionale tra le opinioni espresse dal parlamentare e la sua attività quale membro di quest'ultima, nesso che, secondo la giurisprudenza della Corte costituzionale, deve sussistere affinché possa operare la garanzia prevista dall'art. 68, primo comma, della Costituzione;  &#13;
    che, peraltro, le dichiarazioni in questione non potrebbero considerarsi riproduttive all'esterno di dichiarazioni rese, mediante atti tipici della funzione parlamentare, all'interno del Parlamento; &#13;
    che, pertanto, la deliberazione di insindacabilità adottata dalla Camera avrebbe illegittimamente interferito sulla sfera di attribuzioni, costituzionalmente garantita dell'autorità giudiziaria. &#13;
    Considerato che, in questa fase, la Corte è chiamata, ai sensi dell'art. 37, terzo e quarto comma, della legge 11 marzo 1953, n. 87, a delibare esclusivamente se il ricorso sia ammissibile, valutando, senza contraddittorio tra le parti, se sussistano i requisiti soggettivo e oggettivo di un conflitto di attribuzione tra poteri dello Stato; &#13;
    che quanto al requisito soggettivo, il Tribunale di Venezia, ufficio del giudice monocratico penale, è legittimato a sollevare il conflitto in quanto competente a dichiarare definitivamente la volontà del potere cui appartiene, in considerazione della posizione di indipendenza, costituzionalmente garantita, di cui godono i singoli organi giurisdizionali; &#13;
    che anche la Camera dei deputati, in relazione alla definizione dell'ambito di insindacabilità di cui all'art. 68, primo comma, della Costituzione, è legittimata ad essere parte del conflitto, in quanto organo competente a dichiarare definitivamente la volontà del potere che rappresenta; &#13;
    che, per quanto concerne l'aspetto oggettivo del conflitto, il ricorrente Tribunale di Venezia lamenta la lesione delle proprie attribuzioni costituzionalmente garantite in relazione alla adozione, da parte della Camera di appartenenza del parlamentare, di una deliberazione ove si afferma, in modo asseritamente arbitrario, l'insindacabilità delle opinioni espresse da quest'ultimo, ai sensi dell'art. 68, primo comma, della Costituzione; &#13;
    che, pertanto, esiste la materia di un conflitto, la cui soluzione è affidata alla competenza di questa Corte, restando impregiudicata ogni ulteriore decisione definitiva - da assumersi a contraddittorio ritualmente instaurato - anche in ordine alla ammissibilità del ricorso.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
LA CORTE COSTITUZIONALE &#13;
    riservato ogni definitivo giudizio, &#13;
    dichiara ammissibile, ai sensi dell'art. 37 della legge 11 marzo 1953, n. 87, il conflitto di attribuzioni proposto dal Tribunale di Venezia, ufficio del giudice monocratico penale, nei confronti della Camera dei deputati con il ricorso in epigrafe; &#13;
    dispone: &#13;
    a) che la cancelleria della Corte dia immediata comunicazione della presente ordinanza al Tribunale di Venezia, ufficio del giudice monocratico penale, ricorrente; &#13;
    b) che, a cura del ricorrente, il ricorso e la presente ordinanza siano notificati alla Camera dei deputati, in persona del suo Presidente, entro il termine di 60 giorni dalla comunicazione di cui al punto a), per essere poi depositati nella cancelleria di questa Corte entro il termine di venti giorni dalla notificazione, a norma dell'art. 26, terzo comma, delle norme integrative per i giudizi davanti alla Corte costituzionale. &#13;
    Cosi deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 3 luglio 2003. &#13;
    F.to: &#13;
    Riccardo CHIEPPA, Presidente &#13;
    Ugo DE SIERVO, Redattore &#13;
    Giuseppe DI PAOLA, Cancelliere &#13;
    Depositata in Cancelleria il 22 luglio 2003. &#13;
    Il Direttore della Cancelleria &#13;
    F.to: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
