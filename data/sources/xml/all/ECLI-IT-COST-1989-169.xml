<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>169</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:169</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CONSO</presidente>
    <relatore_pronuncia>Vincenzo Caianello</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/03/1989</data_decisione>
    <data_deposito>20/03/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Giovanni CONSO; &#13;
 Giudici: prof. Ettore GALLO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. &#13;
 Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio di legittimità costituzionale dell'art. 34 della legge    &#13;
 10 maggio 1983, n.  212  (Norme  sul  reclutamento,  gli  organici  e    &#13;
 l'avanzamento   dei   sottufficiali   dell'Esercito,   della  Marina,    &#13;
 dell'Aeronautica e della Guardia di finanza), promosso con  ordinanza    &#13;
 emessa  il  16  marzo  1988  dal  T.A.R.  per il Piemonte sul ricorso    &#13;
 proposto da Zucca Guido contro il Ministero della Difesa, iscritta al    &#13;
 n.  473  del  registro  ordinanze  1988  e  pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 42,  prima  serie  speciale,  dell'anno    &#13;
 1988.                                                                    &#13;
    Visto  l'atto  di  costituzione  di Zucca Guido, nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nella  camera  di consiglio dell'11 gennaio 1989 il Giudice    &#13;
 relatore Vincenzo Caianiello.                                            &#13;
    Ritenuto  che,  nel  corso di un giudizio diretto all'annullamento    &#13;
 della valutazione di  inidoneità  di  un  brigadiere  dell'Arma  dei    &#13;
 Carabinieri  all'avanzamento  "a  scelta"  al  grado  di  maresciallo    &#13;
 ordinario, il Tribunale amministrativo regionale  del  Piemonte,  con    &#13;
 ordinanza  emessa  il 16 marzo 1988, ha sollevato d'ufficio questioni    &#13;
 di legittimità costituzionale dell'art. 26 della legge  12  novembre    &#13;
 1955,  n.  1137  (recte:  dell'art. 34 della legge 10 maggio 1983, n.    &#13;
 212, recante norme sul reclutamento, gli organici e l'avanzamento dei    &#13;
 sottufficiali  dell'Esercito,  della Marina, dell'Aeronautica e della    &#13;
 Guardia di finanza), in riferimento agli artt. 3, 52, 97 e 113  della    &#13;
 Costituzione;                                                            &#13;
      che il giudice rimettente rileva che ai sensi dell'art. 34 della    &#13;
 legge 10 maggio 1983, n. 212, che reca norme  sul  reclutamento,  gli    &#13;
 organici  e  l'avanzamento  dei  sottufficiali delle diverse armi, le    &#13;
 Commissioni di avanzamento, prima ancora di assegnare ai militari  da    &#13;
 sottoporre  a  scrutinio  per  l'avanzamento  "a scelta" il punteggio    &#13;
 previsto dall'art. 35 della stessa legge,  devono  dichiarare  se  il    &#13;
 sottufficiale sia da considerarsi o meno idoneo all'avanzamento e che    &#13;
 solo in caso positivo è possibile l'inserimento del  dipendente  nel    &#13;
 quadro  di avanzamento per l'anno di riferimento e quindi, nel limite    &#13;
 dei posti assegnabili, la sua promozione;                                &#13;
      che  la  rilevanza  della  questione  viene  individuata  con la    &#13;
 considerazione che,  essendo  stato  il  dipendente  già  dichiarato    &#13;
 inidoneo per ben due volte e impedendo il sistema normativo una nuova    &#13;
 valutazione, la eventuale dichiarazione di incostituzionalità  della    &#13;
 norma  denunciata  (art.  34)  consentirebbe  al soggetto di accedere    &#13;
 direttamente allo scrutinio  di  cui  all'art.  35,  con  conseguente    &#13;
 soddisfazione dell'interesse fatto valere in giudizio;                   &#13;
      che,  nel  merito,  ad  avviso  del giudice rimettente, la norma    &#13;
 impugnata  appare  collidere  innanzitutto  con  l'art.   113   della    &#13;
 Costituzione,   in   quanto,   nella  fase  di  "pre-scrutinio  sulla    &#13;
 idoneità"  del  sottufficiale,  sarebbe   del   tutto   carente   la    &#13;
 prefissazione   di  criteri  di  valutazione  atti  a  delimitare  la    &#13;
 discrezionalità  della  Commissione  di  avanzamento,  la  quale  si    &#13;
 configurerebbe, in questa fase, ancora più lata di quella attribuita    &#13;
 al medesimo organo nella ulteriore fase di attribuzione dei punteggi,    &#13;
 disciplinata dall'art. 35 della medesima legge;                          &#13;
      che   tale  amplissima  discrezionalità,  in  base  alla  quale    &#13;
 l'attività delle Commissioni si sostanzia in una semplice  votazione    &#13;
 non  preceduta da alcuna discussione o valutazione collegiale, né da    &#13;
 alcun esame del libretto personale o del curriculum del candidato,  e    &#13;
 non  suffragata  dall'obbligo di motivazione, impedirebbe l'esercizio    &#13;
 della  tutela  giurisdizionale,  non  consentendo   al   giudice   di    &#13;
 verificare  la corrispondenza dell'azione amministrativa con le norme    &#13;
 regolatrici;                                                             &#13;
      che  un  ulteriore  profilo  di  incostituzionalità della norma    &#13;
 denunciata, per violazione dell'art.  97  della  Costituzione,  viene    &#13;
 ravvisato  proprio  per l'assenza di qualsiasi criterio di massima da    &#13;
 osservare  nell'espressione  del  giudizio  di  idoneità,  sì   che    &#13;
 l'attività  della  Commissione  parrebbe disattendere le esigenze di    &#13;
 imparzialità  e  buon  andamento  della  pubblica   amministrazione,    &#13;
 rendendo possibili immotivate esclusioni di soggetti dallo scrutinio;    &#13;
      che,  ancora,  l'art.  34 si porrebbe in contrasto con l'art. 52    &#13;
 della Costituzione,  poiché  le  particolari  esigenze  delle  forze    &#13;
 armate e le limitazioni ai diritti e alle libertà costituzionali dei    &#13;
 soggetti ad esse appartenenti, pur ammesse nell'invocato parametro di    &#13;
 riferimento  ma  solo  se finalizzate al perseguimento dell'interesse    &#13;
 della difesa della Patria, non  possono  giungere  fino  ad  incidere    &#13;
 sulla tutela giurisdizionale delle posizioni soggettive, intesa quale    &#13;
 cardine irrinunciabile di un ordinamento democratico;                    &#13;
      che,  infine,  la  norma  denunciata, assoggettando i militari a    &#13;
 procedure di promozione diverse e meno garantiste di  quelle  offerte    &#13;
 dagli  artt.  62 e seguenti del d.P.R. 3 maggio 1957, n. 686, a tutti    &#13;
 gli  impiegati  dello  Stato,  violerebbe  altresì  l'art.  3  della    &#13;
 Costituzione,  in  quanto,  pur  nella  diversità dei criteri per la    &#13;
 scelta del personale, non può dubitarsi  che  sussista  il  medesimo    &#13;
 motivo  di  interesse  pubblico ad operare una esatta valutazione per    &#13;
 individuare i soggetti più idonei alla promozione o all'avanzamento;    &#13;
      che si è costituito l'originario ricorrente del giudizio a quo,    &#13;
 associandosi alle considerazioni svolte nell'ordinanza di rimessione;    &#13;
      che  ha  spiegato  intervento  il  Presidente  del Consiglio dei    &#13;
 Ministri, con il patrocinio  dell'Avvocatura  generale  dello  Stato,    &#13;
 rilevando  in  primo  luogo che l'oggetto del giudizio a quo - che ha    &#13;
 dato occasione all'ordinanza di rimessione  -  era  lo  scrutinio  di    &#13;
 avanzamento  "a scelta" e non "ad anzianità", effettuato pertanto ai    &#13;
 sensi dell'art. 35 della legge n.  212 del 1983 e non invece ai sensi    &#13;
 della  norma  impugnata  (art.  34),  donde la inammissibilità della    &#13;
 questione proposta;                                                      &#13;
      che  nel  merito  la difesa dello Stato ha comunque sostenuto la    &#13;
 infondatezza  della  questione,  negando  che  la  norma   denunciata    &#13;
 consenta l'ipotizzato arbitrio delle Commissioni di avanzamento nelle    &#13;
 valutazioni che le stesse sono chiamate ad  effettuare,  poiché,  ai    &#13;
 sensi  dell'art.  33  della  stessa  legge,  qualsiasi  scrutinio  di    &#13;
 avanzamento, sia ad anzianità che a scelta, viene dalle  Commissioni    &#13;
 svolto  "sulla  base  degli  elementi risultanti dalla documentazione    &#13;
 personale di ciascun  sottufficiale",  potendo  altresì  i  suddetti    &#13;
 organi,    a   chiarimento   delle   risultanze   evidenziate   dalla    &#13;
 documentazione di cui sopra,  interpellare  "qualunque  superiore  in    &#13;
 grado,  ancora  in  servizio,  che  abbia  avuto  alle  dipendenze il    &#13;
 sottufficiale";                                                          &#13;
      che,   risultando  così  predeterminati  dal  legislatore  quei    &#13;
 parametri di riferimento obiettivo  idonei  a  sorreggere  i  giudizi    &#13;
 delle  Commissioni,  non  si  ravvisa  la possibilità di valutazioni    &#13;
 arbitrarie e la conseguente violazione dei  parametri  costituzionali    &#13;
 invocati,  peraltro  già  esclusa  dalla  Corte costituzionale nella    &#13;
 sentenza n. 409 del 1988.                                                &#13;
    Considerato  che,  nonostante la diversa indicazione riportata nel    &#13;
 dispositivo dell'ordinanza di rimessione, dal contenuto della  stessa    &#13;
 è  possibile  ricavare  che  il  giudice  a quo ha inteso sottoporre    &#13;
 all'esame della Corte l'art. 34 della legge 10 maggio 1983,  n.  212,    &#13;
 recante  norme  sul  reclutamento,  gli  organici e l'avanzamento dei    &#13;
 sottufficiali dell'Esercito, della Marina, dell'Aeronautica  e  della    &#13;
 Guardia  di  finanza  e  non invece l'art. 26 della legge 12 novembre    &#13;
 1955,  n.  1137,  erroneamente   richiamata   perché   disciplinante    &#13;
 l'avanzamento degli ufficiali delle forze armate;                        &#13;
      che,  peraltro,  quanto alla rilevanza delle questioni proposte,    &#13;
 va  precisato  che  l'oggetto  del  giudizio  a  quo  era  costituito    &#13;
 dall'annullamento  della valutazione di inidoneità del sottufficiale    &#13;
 all'avanzamento "a scelta" nel grado superiore disciplinato dall'art.    &#13;
 35  della  legge  n.  212  del  1983,  mentre,  viceversa,  l'art. 34    &#13;
 impugnato regola le modalità per l'avanzamento dei sottufficali  "ad    &#13;
 anzianità", nei gradi in cui tale sistema è espressamente previsto;    &#13;
      che,   diversamente   da   quanto  affermato  nell'ordinanza  di    &#13;
 rimessione, l'eventuale pronuncia di incostituzionalità della  norma    &#13;
 denunciata  non  recherebbe  alcun  vantaggio  al ricorrente, diverse    &#13;
 essendo le fonti regolatrici  dei  due  sistemi  di  avanzamento  dei    &#13;
 sottufficiali;                                                           &#13;
      che,  infatti,  il  giudice a quo non è chiamato, ai fini della    &#13;
 decisione della controversia sottoposta alla sua cognizione,  a  fare    &#13;
 applicazione  della norma denunciata (art. 34), bensì di altra norma    &#13;
 non espressamente impugnata, ma soltanto richiamata  nella  ordinanza    &#13;
 di rimessione con mero riferimento alla ipotizzata prosecuzione della    &#13;
 procedura di avanzamento;                                                &#13;
      che,  pertanto, deve essere accolta la eccezione sollevata dalla    &#13;
 difesa dello Stato nell'atto di intervento circa la  inammissibilità    &#13;
 delle  questioni  proposte  per  difetto  di  rilevanza  in ordine al    &#13;
 giudizio principale.                                                     &#13;
    Visti  gli  artt. 26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   delle  questioni  di    &#13;
 legittimità costituzionale dell'art. 34 della legge 10 maggio  1983,    &#13;
 n.  212  (Norme  sul  reclutamento,  gli organici e l'avanzamento dei    &#13;
 sottufficiali dell'Esercito, della Marina, dell'Aeronautica  e  della    &#13;
 Guardia di finanza), sollevate, in riferimento agli artt. 3, 52, 97 e    &#13;
 113 della Costituzione, dal Tribunale  amministrativo  regionale  del    &#13;
 Piemonte con l'ordinanza indicata in epigrafe.                           &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 marzo 1989.                                 &#13;
                          Il Presidente: CONSO                            &#13;
                        Il redattore: CAIANIELLO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 29 marzo 1989.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
