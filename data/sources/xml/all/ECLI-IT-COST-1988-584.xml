<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>584</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:584</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Gabriele Pescatore</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/05/1988</data_decisione>
    <data_deposito>19/05/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, dott. Aldo CORASANITI, prof. Giuseppe &#13;
 BORZELLINO, dott. Francesco GRECO, prof. Renato DELL'ANDRO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di  legittimità  costituzionale  degli artt. 43 e 96,    &#13;
 lett.  l),  legge  1°  aprile  1981,  n.  121   ("Nuovo   ordinamento    &#13;
 dell'Amministrazione   della   pubblica   sicurezza"),  promosso  con    &#13;
 ordinanza emessa il 16 dicembre  1985  dal  Tribunale  amministrativo    &#13;
 regionale  per  la  Sicilia  -  sede di Catania - sui ricorsi riuniti    &#13;
 proposti  da  Samperisi  Nicolò  ed  altri   contro   il   Ministero    &#13;
 dell'Interno,  iscritta  al  n.  442  del  registro  ordinanze 1986 e    &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  45,  prima    &#13;
 serie speciale, dell'anno 1986;                                          &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio  del  23 marzo 1988 il Giudice    &#13;
 relatore Gabriele Pescatore;                                             &#13;
    Ritenuto che il Tribunale amministrativo regionale per la Sicilia,    &#13;
 con ordinanza 16 dicembre 1986 (n. 442 del R.O.  1986)  ha  sollevato    &#13;
 questioni di legittimità costituzionale: a) dell'art. 43 della legge    &#13;
 1° aprile 1981 n. 121, nella parte in cui dispone che il  trattamento    &#13;
 economico   del  personale  dirigenziale  dell'Amministrazione  della    &#13;
 pubblica sicurezza è disciplinato secondo le norme stabilite per  la    &#13;
 dirigenza  civile  dello Stato, anziché secondo le norme concernenti    &#13;
 la dirigenza militare; b) dell'art.  96,  lett.  l),  della  medesima    &#13;
 legge  1°  aprile  1981,  n.  121,  nella parte in cui dispone che al    &#13;
 personale dirigenziale dei ruoli civili della pubblica sicurezza  sia    &#13;
 concesso,  sotto  forma  di  assegno personale riassorbibile, il più    &#13;
 favorevole  trattamento  economico  previsto  per  il  corrispondente    &#13;
 personale militare, in via provvisoria anziché in via definitiva;       &#13;
      che  tali questioni sono state sollevate sotto il profilo che le    &#13;
 norme impugnate contrasterebbero con gli artt. 3 e  36  Cost.,  dando    &#13;
 luogo  ad  un'ingiustificata  disparità di trattamento nei confronti    &#13;
 degli ex ufficiali del Corpo  delle  guardie  di  pubblica  sicurezza    &#13;
 transitati  nel  ruolo ad esaurimento di cui all'art. 36 punto X, nn.    &#13;
 24, 27, 29 della stessa legge n. 121 del 1981, i quali pur  svolgendo    &#13;
 le  medesime  funzioni  attribuite  indifferenziatamente  a  tutto il    &#13;
 personale  dirigenziale,  continuano  a  percepire   il   trattamento    &#13;
 economico militare, di fatto più favorevole;                            &#13;
    Considerato  che  l'art.  20  della  l. 10 ottobre 1986, n. 668 ha    &#13;
 modificato tale normativa estendendo al  personale  delle  qualifiche    &#13;
 dirigenziali   dell'Amministrazione   della   pubblica  sicurezza  il    &#13;
 trattamento economico concernente la dirigenza militare;                 &#13;
      che,  tale  modifica non ha effetto retroattivo, per cui permane    &#13;
 la rilevanza delle questioni sollevate, con riferimento agli  effetti    &#13;
 prodotti  dalla  normativa  impugnata  anteriormente  all'entrata  in    &#13;
 vigore della l. n. 668 del 1986;                                         &#13;
      che la creazione di ruoli ad esaurimento per talune categorie di    &#13;
 personale  militare,  in  sede  di  riforma  dell'Ordinamento   della    &#13;
 pubblica sicurezza - concernenti situazioni ritenute meritevoli di un    &#13;
 trattamento particolare, in relazione al  passaggio  dal  vecchio  al    &#13;
 nuovo ordinamento - rientrava nella discrezionalità del legislatore;    &#13;
      che la posizione del personale transitato in tali ruoli, proprio    &#13;
 perché istituiti in  relazione  a  situazioni  particolari,  non  è    &#13;
 omogeneo  rispetto  a  quella  del  personale  inquadrato  nei  ruoli    &#13;
 ordinari;                                                                &#13;
      che  stabilire  la misura del trattamento economico di categorie    &#13;
 di  personale  non  omogenee  -  quali  sono,  appunto,   quelle   in    &#13;
 riferimento  alle  quali  le questioni di legittimità costituzionale    &#13;
 sono  state  sollevate   -   rientra   nella   discrezionalità   del    &#13;
 legislatore, non censurabile in questa sede;                             &#13;
    Visti  gli artt. 26, secondo comma, legge 11 marzo 1953, n. 87 e 9    &#13;
 delle  Norme  integrative  per   i   giudizi   davanti   alla   Corte    &#13;
 costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara   la   manifesta   inammissibilità   della  questione  di    &#13;
 legittimità costituzionale degli artt. 43  e  96,  lett.  l),  della    &#13;
 legge 1° aprile 1981, n. 121 ("Nuovo ordinamento dell'Amministrazione    &#13;
 della pubblica sicurezza"), sollevata con ordinanza 16 dicembre  1986    &#13;
 (n.  442 del R.O. 1986) del Tribunale amministrativo regionale per la    &#13;
 Sicilia, in riferimento agli artt. 3 e 36 Cost.                          &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte Costituzionale,    &#13;
 Palazzo della Consulta l'11 maggio 1988.                                 &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: PESCATORE                           &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 19 maggio 1988.                          &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
