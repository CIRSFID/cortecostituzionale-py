<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1962</anno_pronuncia>
    <numero_pronuncia>18</numero_pronuncia>
    <ecli>ECLI:IT:COST:1962:18</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CAPPI</presidente>
    <relatore_pronuncia>Mario Cosatti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/03/1962</data_decisione>
    <data_deposito>16/03/1962</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. GIUSEPPE CAPPI, Presidente - Prof. &#13;
 GASPARE AMBROSINI - Dott. MARIO COSATTI - Prof. FRANCESCO PANTALEO &#13;
 GABRIELI - Prof. GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - &#13;
 Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO &#13;
 PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - Prof. &#13;
 GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO MORTATI - &#13;
 Prof. GIUSEPPE CHIARELLI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di  legittimità  costituzionale  del  D.    P.  R.  6  &#13;
 settembre  1952,  n.  1397, promosso con ordinanza 19 novembre 1960 del  &#13;
 Pretore di Orbetello nel  procedimento  civile  vertente  tra  Guzolini  &#13;
 Giuseppina, il Comune di Orbetello e l'Ente per la colonizzazione della  &#13;
 Maremma  tosco  - laziale e del territorio del Fucino, iscritta al n. 6  &#13;
 del Registro ordinanze 1961 e pubblicata nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 44 del 18 febbraio 1961.                                   &#13;
     Udita nell'udienza pubblica del 7 febbraio 1962  la  relazione  del  &#13;
 Giudice Mario Cosatti;                                                   &#13;
     udito l'avv. Luigi Tripputi, per la Guzolini.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con  atto  di  citazione  notificato  il  7  aprile  1959  Guzolini  &#13;
 Giuseppina in Andreis conveniva  in  giudizio  dinanzi  al  Pretore  di  &#13;
 Orbetello  il  Comune  di Orbetello in persona del Sindaco protempore e  &#13;
 l'Ente per  la  colonizzazione  della  Maremma    tosco-laziale  e  del  &#13;
 territorio  del  Fucino  in  persona  del  suo presidente, chiedendo in  &#13;
 rivendica, previa  declaratoria  di  illegittimità  costituzionale  da  &#13;
 parte  della Corte costituzionale del D.P.R. 6 settembre 1952, n. 1397,  &#13;
 i  terreni  posti  nel  tombolo  di  Giannella,  Comune  di  Orbetello,  &#13;
 rappresentati  nel  nuovo  catasto del Comune di Orbetello al foglio 58  &#13;
 particelle 54 e 55.                                                      &#13;
     Assumeva la Guzolini di aver acquistato i  terreni  sopra  indicati  &#13;
 dai   signori  Barabino  Vincenzo,  Angela,  Caterina  in  Mercenaro  e  &#13;
 Fortunato con rogito Fausto Ugazzi di Orbetello in data 26 luglio 1941,  &#13;
 ivi registrato l'11 agosto 1941 e trascritto  presso  la  Conservatoria  &#13;
 dei registri immobiliari di Grosseto il 26 settembre 1941.               &#13;
     Il Comune di Orbetello, costituitosi in giudizio, osservava che con  &#13;
 rogito  De Carolis di Grosseto in data 7 marzo 1958, ivi registrato l'8  &#13;
 aprile e trascritto  l'11  dello  stesso  mese,  aveva  effettuato  una  &#13;
 permuta con l'Ente per la colonizzazione della Maremma tosco-laziale in  &#13;
 forza  della  quale aveva acquistato la proprietà di terreni descritti  &#13;
 al catasto al foglio 58, particelle 54 e 55.                             &#13;
     Chiedeva  in  conseguenza  che la domanda attrice fosse rigettata e  &#13;
 che, nell'ipotesi di riconoscimento del  diritto  di  proprietà  della  &#13;
 Guzolini,  l'Ente per la colonizzazione fosse condannato a risarcire il  &#13;
 Comune dei danni e delle spese conseguenti alla evizione.                &#13;
     Si costituiva in giudizio anche l'Ente Maremma, rilevando  di  aver  &#13;
 acquistato la proprietà dei terreni dati in permuta al Comune in forza  &#13;
 del decreto di espropriazione 6 settembre 1952, n.  1397, emesso contro  &#13;
 Barabino Vincenzo.                                                       &#13;
     Il  Pretore,  con  ordinanza  19 novembre 1960, ha ritenuto che dai  &#13;
 documenti prodotti dalla difesa dell'attrice risulta che i  terreni  in  &#13;
 questione  pervennero  alla  Guzolini  con  rogito  Ugazzi  debitamente  &#13;
 trascritto; pertanto,  riconoscendo  non  manifestamente  infondata  la  &#13;
 eccezione di illegittimità costituzionale del decreto n. 1397 del 1952  &#13;
 sollevata  dalla  difesa  medesima,  ha  disposto  la  sospensione  del  &#13;
 giudizio e la trasmissione degli atti alla Corte costituzionale.         &#13;
     L'ordinanza, comunicata ai Presidenti  delle  Camere  e  notificata  &#13;
 alle  parti  e  al  Presidente  del  Consiglio  dei  Ministri, è stata  &#13;
 pubblicata, per  disposizione  del  Presidente  della  Corte  ai  sensi  &#13;
 dell'art. 25 della legge 11 marzo 1953, n. 87, nella Gazzetta Ufficiale  &#13;
 della Repubblica n. 44 del 18 febbraio 1961.                             &#13;
     Nel   giudizio   dinanzi  alla  Corte  si  è  costituita  Guzolini  &#13;
 Giuseppina in persona della sua mandataria generale Guzolini  Elena  in  &#13;
 Ciacci,  depositando  in  cancelleria  il  27  gennaio  1961 le proprie  &#13;
 deduzioni con procura conferita agli avvocati Gino Pietrosanti del foro  &#13;
 di Grosseto e  Luigi  Tripputi  del  foro  di  Roma,  con  elezione  di  &#13;
 domicilio presso quest'ultimo.                                           &#13;
     La  difesa  della Guzolini ha richiamato le sentenze della Corte n.  &#13;
 10 e n. 57 del 1959, secondo  le  quali  nel  sistema  della  legge  di  &#13;
 riforma  agraria  il  procedimento  di esproprio deve essere svolto nei  &#13;
 confronti di colui che è  vero  proprietario,  onde    deve  ritenersi  &#13;
 viziato di illegittimità costituzionale, in riferimento agli artt.  76  &#13;
 e 77 della Costituzione, un decreto del Presidente della Repubblica che  &#13;
 comprenda nello scorporo terreni non appartenenti allo espropriato.      &#13;
     Ciò   premesso,  la  difesa  osserva  che  tali  statuizioni  sono  &#13;
 applicabili nella specie, poiché - come risulta dagli atti prodotti  -  &#13;
 la  Guzolini  acquistò   i  terreni  in contestazione in data 26 luglio  &#13;
 1941, in epoca cioè anteriore alla stessa promulgazione delle leggi di  &#13;
 riforma fondiaria.                                                       &#13;
     La  difesa,  pertanto,  conclude  chiedendo  che  la  Corte  voglia  &#13;
 dichiarare  l'illegittimità  costituzionale  del  decreto  n. 1397 del  &#13;
 1952, emesso contro Barabino, e la inapplicabilità di esso ai  terreni  &#13;
 di proprietà della Guzolini.                                            &#13;
     Nel  giudizio  dinanzi  alla  Corte  non  si sono costituiti né il  &#13;
 Comune di Orbetello, né l'Ente per  la  colonizzazione  della  Maremma  &#13;
 tosco-laziale  e non ha spiegato intervento il Presidente del Consiglio  &#13;
 dei Ministri.                                                            &#13;
     All'udienza pubblica, l'avv. Tripputi, per la Guzolini,  ha  svolto  &#13;
 le deduzioni e confermato le conclusioni scritte.<diritto>Considerato in diritto</diritto>:                          &#13;
     Con  l'ordinanza  di  rimessione  viene  proposta a questa Corte la  &#13;
 questione di legittimità costituzionale del D.P.R. 6  settembre  1952,  &#13;
 n.    1397,  per  aver  compreso  nell'esproprio  a  carico di Barabino  &#13;
 Vincenzo terreni a questo  non  appartenenti,  in  quanto  venduti  con  &#13;
 rogito Ugazzi del 26 luglio 1941 a Guzolini Giuseppina.                  &#13;
     La  Corte  ha  già  avuto  occasione  di  pronunciarsi su analoghe  &#13;
 questioni (cfr. sentenze nn. 8, 10 e 57 del 1959) e non ha  motivo  per  &#13;
 discostarsi dal precedente suo orientamento.                             &#13;
     Il Pretore di Orbetello ha nella sua ordinanza ritenuto, in base ai  &#13;
 documenti  prodotti in quel giudizio, che i terreni corrispondenti alle  &#13;
 particelle 54 e 55 del foglio  58  del  nuovo  catasto  del  Comune  di  &#13;
 Orbetello appartengono alla Guzolini Giuseppina fin dal 1941 e che tali  &#13;
 terreni,  compresi  nella espropriazione disposta con il citato decreto  &#13;
 n. 1397 del 1952 nei riguardi del Barabino, sono gli stessi che  l'Ente  &#13;
 per la colonizzazione della Maremma tosco-laziale ha dato in permuta al  &#13;
 Comune  di  Orbetello con rogito De Carolis di Grosseto in data 7 marzo  &#13;
 1958. Al detto Pretore è risultato, pertanto, che  alla  data  del  15  &#13;
 novembre  1949  -  data  alla  quale nel sistema delle leggi di riforma  &#13;
 fondiaria deve farsi riferimento ai  fini  della  determinazione  della  &#13;
 consistenza  della  proprietà  e  dell'accertamento  della persona del  &#13;
 proprietario gli appezzamenti di terreno rivendicati dalla Guzolini non  &#13;
 appartenevano al Barabino, soggetto passivo dello  scorporo  effettuato  &#13;
 con il decreto impugnato.                                                &#13;
     Detto decreto, in quanto nell'esproprio sono stati compresi terreni  &#13;
 che  al  Barabino non appartenevano, ha ecceduto dai limiti della legge  &#13;
 delega e, pertanto, relativamente ai terreni  in  parola,  deve  essere  &#13;
 dichiarato costituzionalmente illegittimo.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  illegittimità costituzionale del D.P.R.  6 settembre  &#13;
 1952, n. 1397, in relazione all'art. 4 della legge 21 ottobre 1950,  n.  &#13;
 841,  e  in  riferimento  agli  artt.  76  e  77,  primo  comma,  della  &#13;
 Costituzione in quanto ha compreso nella espropriazione terreni che non  &#13;
 appartenevano a Barabino Vincenzo.                                       &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, l'8 marzo 1962.                                  &#13;
                                   GIUSEPPE CAPPI - GASPARE AMBROSINI  -  &#13;
                                   MARIO  COSATTI  -  FRANCESCO PANTALEO  &#13;
                                   GABRIELI - GIUSEPPE  CASTELLI  AVOLIO  &#13;
                                   - ANTONINO PAPALDO - NICOLA JAEGER  -  &#13;
                                   GIOVANNI     CASSANDRO    -    BIAGIO  &#13;
                                   PETROCELLI -  ANTONIO  MANCA  -  ALDO  &#13;
                                   SANDULLI  - GIUSEPPE BRANCA - MICHELE  &#13;
                                   FRAGALI  -  COSTANTINO  MORTATI     -  &#13;
                                   GIUSEPPE CHIARELLI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
