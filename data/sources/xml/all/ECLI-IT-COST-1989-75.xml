<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1989</anno_pronuncia>
    <numero_pronuncia>75</numero_pronuncia>
    <ecli>ECLI:IT:COST:1989:75</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Aldo Corasaniti</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>09/02/1989</data_decisione>
    <data_deposito>23/02/1989</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel  giudizio  di legittimità costituzionale dell'art. 7 della legge    &#13;
 20 novembre 1982, n. 890 (Notificazione di atti a mezzo  posta  e  di    &#13;
 comunicazioni  a  mezzo  posta  connesse con la notificazione di atti    &#13;
 giudiziari), promosso con ordinanza emessa il 21.3.1988  dal  Pretore    &#13;
 di  Morbegno  nel  procedimento civile vertente tra Fattarina Riego e    &#13;
 Pupilella  Pasquale  ed  altra,  iscritta  al  n.  439  del  registro    &#13;
 ordinanze 1988 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 41, prima Serie speciale dell'anno 1988.                              &#13;
    Visto  l'atto  di  intervento  del  Presidente  del  Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 25 gennaio 1989 il Giudice    &#13;
 relatore Aldo Corasaniti;                                                &#13;
    Ritenuto  che  il  Pretore  di  Morbegno,  nel procedimento civile    &#13;
 instaurato da Fattarina Diego contro  Pupilella  Pasquale  ed  altro,    &#13;
 dovendosi  pronunciare  sulla  contumacia  dei convenuti, ai quali la    &#13;
 citazione era stata notificata a mezzo del servizio postale  mediante    &#13;
 consegna  al  portiere  dello  stabile  ove  risiedono,  ha sollevato    &#13;
 d'ufficio questione di legittimità  costituzionale,  in  riferimento    &#13;
 agli  artt.  3 e 24 Costituzione, dell'art. 7 della legge 20 novembre    &#13;
 1982, n. 890 (Notificazione di atti a mezzo posta e di  comunicazioni    &#13;
 a  mezzo  posta  connesse  con  la notificazione di atti giudiziari),    &#13;
 nella parte in cui  non  prevede  l'invio,  da  parte  dell'ufficiale    &#13;
 giudiziario   che   si  sia  avvalso  del  servizio  postale  per  la    &#13;
 notificazione,  di  raccomandata  contenente  l'avviso  dell'avvenuta    &#13;
 notificazione  mediante  consegna  di  copia al portiere, come invece    &#13;
 dispone l'art. 139, quarto comma, codice procedura civile;               &#13;
      che,  ad  avviso  del  giudice  a  quo,  la  lesione dell'art. 3    &#13;
 Costituzione   deriverebbe    dall'ingiustificata    disparità    di    &#13;
 trattamento del destinatario, in senso a lui pregiudizievole, qualora    &#13;
 la notificazione tramite consegna di copia al portiere venga eseguita    &#13;
 ai  sensi  dell'art.  7  della  legge  n.  890 del 1982, in relazione    &#13;
 all'art. 149 codice procedura civile,  anziché  secondo  l'art.  139    &#13;
 codice   procedura   civile;   sarebbe  altresì  violato  l'art.  24    &#13;
 Costituzione, in quanto il congegno della notificazione, ai sensi del    &#13;
 citato  art.  7,  non  sembra  in grado di garantire a sufficienza la    &#13;
 conoscibilità dell'atto all'interessato, con conseguente lesione del    &#13;
 suo diritto di difesa;                                                   &#13;
      che  ha  spiegato  intervento  il  Presidente  del Consiglio dei    &#13;
 ministri, rappresentato dall'Avvocatura dello Stato, concludendo  per    &#13;
 la  manifesta inammissibilità della questione alla stregua di quanto    &#13;
 deciso da questa Corte con le ordinanze nn. 429 e 899 del 1988;          &#13;
    Considerato  che  l'ordinanza  prospetta congiuntamente la lesione    &#13;
 degli  artt.  3  e  24  Costituzione,  in  quanto  fa  discendere  la    &#13;
 violazione  del  diritto di difesa dalla asseritamente ingiustificata    &#13;
 diversità  delle   modalità   di   notificazione   previste   dalle    &#13;
 disposizioni sopra menzionate;                                           &#13;
      che il diritto di difesa del destinatario della notificazione di    &#13;
 atti giudiziari è da  ritenere  adeguatamente  garantito  sempreché    &#13;
 l'atto   pervenga   nella   sfera  di  disponibilità,  e  quindi  di    &#13;
 conoscibilità,   del   destinatario   medesimo,    non    occorrendo    &#13;
 l'acquisizione effettiva dell'atto, e quindi la sua reale conoscenza,    &#13;
 dovendosi  aver  riguardo  anche  alle  contrapposte   esigenze   del    &#13;
 notificante (cfr. sent. n. 213/1975);                                    &#13;
      che  il  giudice a quo ritiene inidonea la notificazione a mezzo    &#13;
 posta -- nell'ipotesi in cui avvenga mediante consegna  dell'atto  al    &#13;
 portiere dello stabile ex art. 7, commi 3 e 4, della legge n. 890 del    &#13;
 1982 - a garantire a sufficienza al  destinatario  la  conoscibilità    &#13;
 dell'atto,   a   causa   della   mancata  previsione,  a  suo  avviso    &#13;
 ingiustificata,    dell'ulteriore    formalità    (rispetto     alla    &#13;
 sottoscrizione  dell'avviso di ricevimento e del registro di consegna    &#13;
 da  parte  del  portiere)  dell'invio   al   destinatario,   a   cura    &#13;
 dell'ufficiale  giudiziario, di un avviso dell'avvenuta notificazione    &#13;
 a mani del portiere mediante lettera raccomandata, come è  previsto,    &#13;
 nell'ipotesi  analoga,  dall'art.  139,  comma  4,  codice  procedura    &#13;
 civile,  qualora  la   notificazione   sia   compiuta   personalmente    &#13;
 dall'ufficiale giudiziario;                                              &#13;
      che,  peraltro,  nell'ipotesi considerata, la consegna del piego    &#13;
 al portiere (art. 7, comma 3)  -  il  quale  sottoscrive,  nella  sua    &#13;
 qualità, l'avviso di ricevimento ed il registro di consegna (art. 7,    &#13;
 comma 4), così assumendo l'incarico della consegna al destinatario e    &#13;
 la correlativa responsabilità - determina l'ingresso dell'atto nella    &#13;
 sfera di disponibilità  del  destinatario,  e  fornisce  sufficienti    &#13;
 garanzie  sulla  successiva  effettività della consegna, senza che a    &#13;
 tal fine sia necessario il successivo  invio  della  raccomandata  al    &#13;
 destinatario,  invio  peraltro  non  richiesto  dall'art.  139, comma    &#13;
 quarto, c.p.c. ai fini del perfezionamento della notificazione;          &#13;
      che,   pertanto,   la  questione  va  dichiarata  manifestamente    &#13;
 infondata;                                                               &#13;
    Visti  gli  artt. 26, comma secondo, della legge 11 marzo 1953, n.    &#13;
 87, e 9, comma secondo, delle Norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale, in  riferimento  agli  artt.  3  e  24  Costituzione,    &#13;
 dell'art.  7  della  legge 20 novembre 1982, n. 890 (Notificazione di    &#13;
 atti a mezzo posta e di comunicazioni a mezzo posta connesse  con  la    &#13;
 notificazione  di atti giudiziari), sollevata dal Pretore di Morbegno    &#13;
 con l'ordinanza indicata in epigrafe.                                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 9 febbraio 1989.                              &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: CORASANITI                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 23 febbraio 1989.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
