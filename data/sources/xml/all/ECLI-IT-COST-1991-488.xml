<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1991</anno_pronuncia>
    <numero_pronuncia>488</numero_pronuncia>
    <ecli>ECLI:IT:COST:1991:488</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>CORASANITI</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>18/12/1991</data_decisione>
    <data_deposito>27/12/1991</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Aldo CORASANITI; &#13;
 Giudici: prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Francesco Paolo &#13;
 CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI, dott. &#13;
 Renato GRANATA, prof. Giuliano VASSALLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio di legittimità costituzionale degli artt. 21, secondo,    &#13;
 terzo e quarto comma, e 50, secondo comma,  del  R.D.L.  30  dicembre    &#13;
 1923,  n. 3267 (Riordinamento e riforma della legislazione in materia    &#13;
 di boschi e di terreni montani), promosso con  ordinanza  emessa  l'8    &#13;
 febbraio  1991  dalla  Corte  di Appello di Cagliari nel procedimento    &#13;
 civile  vertente  tra  Regione  autonoma  della  Sardegna  e  Passino    &#13;
 Giuseppe,  iscritta  al  n.  428 del 1991 e pubblicata nella Gazzetta    &#13;
 Ufficiale della Repubblica n. 25,  prima  serie  speciale,  dell'anno    &#13;
 1991;                                                                    &#13;
    Udito  nella  camera  di consiglio del 20 novembre 1991 il Giudice    &#13;
 relatore Francesco Greco;</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - La Regione autonoma della Sardegna ha impugnato dinanzi  alla    &#13;
 Corte  di  appello di Cagliari il lodo arbitrale del 4 febbraio 1989,    &#13;
 reso esecutivo dal Pretore di Sassari, emesso, ai sensi  degli  artt.    &#13;
 21  e  50  R.D.L.  30  dicembre 1923, n. 3267, dal Collegio arbitrale    &#13;
 adito    per    determinare    la    maggiore    indennità    dovuta    &#13;
 dall'amministrazione  regionale  per  la occupazione temporanea di 70    &#13;
 ettari  di  terreno  siti  nell'agro  di  Villanova  Monteleone,   di    &#13;
 proprietà   dell'avvocato   Passino   Giuseppe,   finalizzata   alla    &#13;
 esecuzione di lavori di sistemazione idraulico-forestale.                &#13;
    La detta indennità inizialmente era stata concordata tra le parti    &#13;
 e liquidata in data 1° maggio 1979.                                      &#13;
    La Corte d'Appello, con ordinanza dell' 8 febbraio 1991  (R.O.  n.    &#13;
 428  del 1991), ha sollevato questione di legittimità costituzionale    &#13;
 degli artt. 21, secondo, terzo e quarto comma, e 50,  secondo  comma,    &#13;
 del   suddetto   R.D.L.  n.  3267  del  1923,  che  prevede,  per  la    &#13;
 determinazione dell'indennizzo in caso di occupazione di terreni  per    &#13;
 sistemazione idraulico-forestale, un arbitrato obbligatorio.             &#13;
    A  parere  della Corte remittente, sarebbero violati gli artt. 24,    &#13;
 primo comma, e 102,  primo  comma,  della  Costituzione,  in  quanto,    &#13;
 secondo  quanto  già  affermato dalla Corte Costituzionale (sent. n.    &#13;
 127 del 1977), la tutela dei diritti e degli  interessi  può  essere    &#13;
 ottenuta  solo dinanzi ad organi giudiziari e non può essere imposto    &#13;
 da una legge il deferimento della controversia ad un arbitro o  a  un    &#13;
 collegio arbitrale.                                                      &#13;
    1.1.  - L'ordinanza è stata regolarmente notificata, comunicata e    &#13;
 pubblicata nella Gazzetta Ufficiale.                                     &#13;
    1.2.  -  Nel  giudizio  non  si  sono costituite le parti e non è    &#13;
 intervenuta l'Avvocatura Generale dello Stato.<diritto>Considerato in diritto</diritto>1. - La Corte è chiamata a verificare se gli artt.  21,  secondo,    &#13;
 terzo,  quarto  comma,  e  50,  secondo comma, del R.D.L. 30 dicembre    &#13;
 1923, n. 3267, nella parte in cui prevedono un arbitrato obbligatorio    &#13;
 per la determinazione dell'indennizzo da corrispondere ai proprietari    &#13;
 dei terreni sottoposti  a  sistemazione  idrogeologica,  violino  gli    &#13;
 artt.  24,  primo  comma,  e 102, primo comma, della Costituzione, in    &#13;
 quanto le parti interessate per la tutela del loro diritto  sarebbero    &#13;
 obbligate  per legge a rivolgersi a un collegio arbitrale anziché al    &#13;
 giudice così come prescritto dagli invocati precetti costituzionali.    &#13;
    1.2. - La questione è fondata.                                       &#13;
    L'art. 17 del R.D.L. 30 dicembre 1923,  n.  3267,  dispone  che  i    &#13;
 boschi,  che  per  la  loro  speciale  ubicazione difendono terreni o    &#13;
 fabbricati dalla caduta di valanghe, dal rotolamento  di  sassi,  dal    &#13;
 sorrenamento  e dalla furia dei venti, e quelli ritenuti utili per le    &#13;
 condizioni igieniche locali, su richiesta delle Province, dei  Comuni    &#13;
 o  di  altri  enti e privati interessati, possono essere sottoposti a    &#13;
 limitazioni nella loro utilizzazione.                                    &#13;
    L'art.  21,  primo  e  secondo  comma,  stabilisce  un  indennizzo    &#13;
 determinato,  sulla  base  dei  minori  redditi derivanti dalle dette    &#13;
 limitazioni imposte alla consuetudinaria  utilizzazione  dei  boschi,    &#13;
 d'accordo tra le parti e, in caso di mancato accordo, da tre arbitri,    &#13;
 nominati  uno  da ciascuna delle due parti e il terzo dagli stessi e,    &#13;
 in caso di disaccordo, dal Presidente del Tribunale, il quale  potrà    &#13;
 nominare  anche  uno  degli  arbitri  delle  parti in caso di mancata    &#13;
 nomina da parte delle stesse.                                            &#13;
    Gli  artt.  39  e  seguenti  dello  stesso  decreto  prevedono  la    &#13;
 sistemazione  e  il  rimboschimento  di  terreni  montani  con  opere    &#13;
 eseguite a cura e spese dello Stato. L'art. 50 dello stesso R.D.L., a    &#13;
 favore dei proprietari dei suddetti terreni,  in  caso  di  totale  o    &#13;
 parziale  sospensione del godimento, stabilisce l'assegnazione di una    &#13;
 indennità determinata nei modi previsti dal precedente art. 21.         &#13;
    Agli interessati è quindi imposto, per la tutela del loro diritto    &#13;
 all'indennizzo o alla indennità, il ricorso a un collegio  arbitrale    &#13;
 anziché al giudice ordinario.                                           &#13;
    2.  -  Come  già  questa Corte ha deciso (sent. n. 127 del 1977),    &#13;
 solo le parti possono  scegliere  altri  soggetti  (arbitri)  per  la    &#13;
 tutela  dei  loro  diritti al posto dei giudici ordinari, ai quali è    &#13;
 demandata la funzione giurisdizionale  in  base  all'art.  102  della    &#13;
 Costituzione.  E  la  scelta è intesa come uno dei modi possibili di    &#13;
 disporre, anche in senso negativo, del diritto di  cui  all'art.  24,    &#13;
 primo comma, della Costituzione.                                         &#13;
    La fonte dell'arbitrato non può, però, ricercarsi e porsi in una    &#13;
 legge  o  generalmente  in  una  volontà autoritativa. Non valgono a    &#13;
 legittimare  il  ricorso  all'arbitrato  obbligatorio  gli  eventuali    &#13;
 vantaggi  connessi  a  siffatta  procedura  (celerità  del giudizio,    &#13;
 possibilità di approfondire  gli  aspetti  tecnici  della  questione    &#13;
 ecc.),  perché  la  decisione  degli  arbitri  rappresenta sempre il    &#13;
 risultato di un procedimento che si  svolge  illegittimamente  al  di    &#13;
 fuori  del  regime  della  sovranità  statuale  e  non  trova alcuna    &#13;
 garanzia costituzionale.                                                 &#13;
    Il  principio  generale,  costituzionalmente  garantito, è quello    &#13;
 dell'art. 806 del codice di procedura civile, secondo  cui  le  parti    &#13;
 sono  libere  di  far  decidere  le  controversie tra loro insorte da    &#13;
 arbitri liberamente scelti.                                              &#13;
    Pertanto, va dichiarata  la  illegittimità  costituzionale  delle    &#13;
 disposizioni impugnate.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la illegittimità costituzionale degli artt. 21, secondo,    &#13;
 terzo, quarto comma, e 50, secondo  comma,  del  R.D.L.  30  dicembre    &#13;
 1923,  n. 3267 (Riordinamento e riforma della legislazione in materia    &#13;
 di boschi e di terreni montani).                                         &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 18 dicembre 1991.                             &#13;
                       Il Presidente: CORASANITI                          &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 27 dicembre 1991.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
