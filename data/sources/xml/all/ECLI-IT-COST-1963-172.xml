<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1963</anno_pronuncia>
    <numero_pronuncia>172</numero_pronuncia>
    <ecli>ECLI:IT:COST:1963:172</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Costantino Mortati</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1963</data_decisione>
    <data_deposito>23/12/1963</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA &#13;
 JAEGER - Prof. GIOVANNI CASSANDRO - Dott. ANTONIO MANCA - Prof. ALDO &#13;
 SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. &#13;
 COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale dell'art. 94, lett.  b,  &#13;
 del D.M. 31 luglio 1934 in relazione all'art. 23 del R.D.L.  2 novembre  &#13;
 1933,  n.  1471,  promosso con ordinanza emessa il 29 dicembre 1962 dal  &#13;
 Pretore di Arsoli nel procedimento penale a, carico di Crecco  Fiore  e  &#13;
 Zeppieri  Pietro,  iscritta  al  n.  74  del  Registro ordinanze 1963 e  &#13;
 pubblicata nella Gazzetta Ufficiale della  Repubblica  n.  107  del  20  &#13;
 aprile 1963.                                                             &#13;
     Visto l'atto di costituzione in giudizio di Zeppieri Pietro;         &#13;
     udita  nell'udienza  pubblica del 20 novembre 1963 la relazione del  &#13;
 Giudice Costantino Mortati;                                              &#13;
     udito l'avv.  Giulio Riccardi, per Zeppieri Pietro.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     Con ordinanza emessa in  data  29  dicembre  1962,  il  Pretore  di  &#13;
 Arsoli, nel corso di un procedimento in opposizione a decreto penale di  &#13;
 condanna  a  carico  di Crecco Fiore e Zeppieri Pietro, per avere fatto  &#13;
 circolare un autobus di  linea  sprovvisto  del  prescritto  estintore,  &#13;
 avendo  ritenuta  non  manifestamente  infondata  l'eccezione sollevata  &#13;
 dalla difesa degli imputati di illegittimità costituzionale  dell'art.  &#13;
 94,  lett.  b,  del  D.M. 31 luglio 1934, in relazione all'asserita sua  &#13;
 esorbitanza dai poteri  conferiti  al  Ministro  dall'articolo  23  del  &#13;
 R.D.L.  2  novembre  1933,  n.  1471, ed avendo implicitamente ritenuto  &#13;
 altresì la rilevanza dell'eccezione stessa al fine  della  risoluzione  &#13;
 della  causa, ebbe a disporre la sospensione del procedimento e l'invio  &#13;
 degli atti alla Corte costituzionale.                                    &#13;
     L'ordinanza, notificata e comunicata ai sensi di  legge,  è  stata  &#13;
 pubblicata nella Gazzetta Ufficiale del 20 aprile 1963, n. 107.          &#13;
     Si  è  costituito  avanti  alla  Corte lo Zeppieri rappresentato e  &#13;
 difeso dall'avv. Giulio Riccardi, con deduzioni depositate il  4  marzo  &#13;
 1963,  nelle  quali  si  fa  osservare  che la disposizione denunciata,  &#13;
 stabilendo  che  gli  autoveicoli  di  grandi  dimensioni  adibiti   al  &#13;
 trasporto  di  persone  e  di  merci  debbano  essere  provvisti  di un  &#13;
 estintore di 5 litri per liquidi infiammabili, non si è mantenuta  nei  &#13;
 limiti  della  delegazione che consentiva al Ministro l'emanazione solo  &#13;
 delle norme di sicurezza riferibili  oltre  che  agli  stabilimenti,  e  &#13;
 depositi  per  la  lavorazione,  immagazzinamento  e vendita degli olii  &#13;
 minerali, al trasporto degli olii medesimi, onde prevenire i pericoli a  &#13;
 questo connessi,  nella  quale  materia  non  possono  farsi  rientrare  &#13;
 trasporti  diversi,  come sono quelli effettuati dal suo rappresentato.  &#13;
 Conclude chiedendo che sia dichiarata  l'illegittimità  costituzionale  &#13;
 della norma denunciata.<diritto>Considerato in diritto</diritto>:                          &#13;
     La  Corte non può pronunciare sul merito della questione sollevata  &#13;
 poiché la disposizione contro cui è fatta valere fa parte di un  atto  &#13;
 normativo  sfornito  della  forza  di legge, e, come tale, sottratto al  &#13;
 giudizio   di   costituzionalità   previsto   dall'art.   134    della  &#13;
 Costituzione.                                                            &#13;
     A  far  ritenere il difetto di tale forza concorrono considerazioni  &#13;
 desumibili tanto  dalla  forma  dell'atto  denunciato  quanto  dal  suo  &#13;
 contenuto.    Che  questo  non  sia  stato  emesso  in virtù di potere  &#13;
 delegato  è  argomentabile,  più  che  dal  termine  "autorizzazione"  &#13;
 adoperato dall'art. 23 del R.D.L. n. 1471 del 1933 (essendo nota la non  &#13;
 infrequente  improprietà  del linguaggio legislativo), dal fatto della  &#13;
 mancanza dei  requisiti  formali  prescritti  per  i  decreti  delegati  &#13;
 dall'art. 3, n. 1, della legge 31 gennaio 1926, n. 100, allora vigente.  &#13;
 È  vero  che  nella  prassi  di  quell'epoca  si  riscontrano  casi di  &#13;
 attribuzione ai Ministri di  potere  delegato,  ma  questa  era  sempre  &#13;
 accompagnata da espresso conferimento della forza di legge.              &#13;
     A non diversa conclusione conduce l'esame che si compia dell'indole  &#13;
 strettamente  tecnica  delle  "norme di sicurezza" emanate dal Ministro  &#13;
 dell'interno, che appaiono perciò tali da potere difficilmente  essere  &#13;
 oggetto  di disciplina da parte della legge formale, e neppure giovarsi  &#13;
 dei pareri e dei controlli prescritti per i regolamenti.   Non a  caso,  &#13;
 infatti,  il decreto-legge n. 1471, mentre con l'art. 23 ha deferito al  &#13;
 Ministro la competenza ad emanare le norme di sicurezza, ha poi, con il  &#13;
 successivo art. 24, rinviato ad un  regolamento,  da  emettere  con  le  &#13;
 forme  di cui al n. 1 della citata legge N. 100, le norme di attuazione  &#13;
 e di esecuzione.  Ed uguale autonomia, in confronto alle norme che sono  &#13;
 oggetto della presente questione, si  rileva  per  l'altro  regolamento  &#13;
 prescritto dall'art. 63 del T.U. delle leggi di p.s., pure esso rivolto  &#13;
 a disciplinare, fra l'altro, il trasporto degli olii minerali.  Sicché  &#13;
 a  fortiori  non  potrebbe  venire  riconosciuta alle norme in esame la  &#13;
 forza di legge  della  quale  difettano  quelle  contenute  nei  comuni  &#13;
 regolamenti esecutivi.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile la questione di legittimità costituzionale  &#13;
 dell'art. 94, lett. b, del D.M. 31 luglio 1934.                          &#13;
     Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,  &#13;
 Palazzo della Consulta, il 12 dicembre 1963.                             &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO - ANTONIO  &#13;
                                   MANCA  -  ALDO  SANDULLI  -  GIUSEPPE  &#13;
                                   BRANCA - MICHELE FRAGALI - COSTANTINO  &#13;
                                   MORTATI   -   GIUSEPPE   CHIARELLI  -  &#13;
                                   GIUSEPPE   VERZÌ  -  FRANCESCO  PAOLO  &#13;
                                   BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
