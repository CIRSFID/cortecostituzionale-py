from cortecostituzionale.parser.main import parse_all
from cortecostituzionale.scraper.main import scrape
from argparse import ArgumentParser


if __name__ == '__main__':
    argparser = ArgumentParser()
    argparser.add_argument("--override", action="store_true", default=False, help="Overrides existing docs")
    argparser.add_argument("--push", action="store_true", default=False, help="Push to eXistDB")
    args = argparser.parse_args()
    scrape()
    parse_all(override=args.override, push=args.push)
