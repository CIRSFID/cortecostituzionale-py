<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2023</anno_pronuncia>
    <numero_pronuncia>187</numero_pronuncia>
    <ecli>ECLI:IT:COST:2023:187</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SCIARRA</presidente>
    <relatore_pronuncia>Augusto Antonio Barbera</relatore_pronuncia>
    <redattore_pronuncia>Augusto Antonio Barbera</redattore_pronuncia>
    <data_decisione>20/09/2023</data_decisione>
    <data_deposito>10/10/2023</data_deposito>
    <tipo_procedimento>GIUDIZIO DI LEGITTIMITÀ COSTITUZIONALE IN VIA PRINCIPALE</tipo_procedimento>
    <dispositivo>estinzione del processo</dispositivo>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori:&#13;
 Presidente: Silvana SCIARRA; Giudici : Daria de PRETIS, Nicolò ZANON, Franco MODUGNO, Augusto Antonio BARBERA, Giulio PROSPERETTI, Giovanni AMOROSO, Luca ANTONINI, Stefano PETITTI, Angelo BUSCEMA, Emanuela NAVARRETTA, Maria Rosaria SAN GIORGIO, Filippo PATRONI GRIFFI,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale della legge della Regione Siciliana 12 maggio 2022, n. 12, recante «Riconoscimento e promozione della Dieta mediterranea», promosso dal Presidente del Consiglio dei ministri con ricorso notificato il 19 luglio 2022, depositato in cancelleria il 25 luglio 2022, iscritto al n. 45 del registro ricorsi 2022 e pubblicato nella Gazzetta Ufficiale della Repubblica n. 36, prima serie speciale, dell'anno 2022.&#13;
 Visto l'atto di costituzione della Regione Siciliana;&#13;
 udito nella camera di consiglio del 20 settembre 2023 il Giudice relatore Augusto Antonio Barbera;&#13;
 deliberato nella camera di consiglio del 20 settembre 2023.&#13;
 Ritenuto che, con ricorso notificato il 19 luglio 2022 e depositato il 25 luglio 2022 (reg. ric. n. 45 del 2022), il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, ha promosso - in riferimento all'art. 81, terzo comma, della Costituzione - questione di legittimità costituzionale dell'intera legge della Regione Siciliana 12 maggio 2022, n. 12, recante «Riconoscimento e promozione della Dieta mediterranea»;&#13;
 che la legge reg. Siciliana n. 12 del 2022 è impugnata in quanto, pur prevedendo l'adozione di un programma contenente gli indirizzi delle politiche regionali volte alla promozione della dieta mediterranea, ed essendo così suscettibile di determinare oneri a carico del bilancio regionale, non quantifica tali oneri, né individua idonea copertura finanziaria per farvi fronte;&#13;
 che, in particolare, l'art. 1, comma 3, indica le finalità dell'intera legge reg. Siciliana n. 12 del 2022, che individua nella valorizzazione e promozione della dieta mediterranea, da attuarsi, fra l'altro mediante «politiche regionali per lo sviluppo del territorio rurale, orientate al sostegno e alla valorizzazione della multifunzionalità e sostenibilità ambientale in agricoltura, del paesaggio, del patrimonio naturalistico e storico-culturale, del turismo e della salute»;&#13;
 che tali politiche sono poi individuate dall'art. 3 della medesima legge regionale nella programmazione di interventi per la promozione, lo sviluppo e la valorizzazione della dieta mediterranea, nella promozione dell'informazione e della comunicazione sulla stessa e nel sostegno alla gestione della «rete di operatori» per la dieta mediterranea di cui al successivo art. 4, coordinata dall'assessorato regionale dell'agricoltura, dello sviluppo rurale e della pesca mediterranea, mediante l'attivazione di un forum «quale luogo deputato all'incontro e allo sviluppo delle tematiche» oggetto della legge regionale;&#13;
 che, inoltre, sono previsti un supporto permanente all'aggiornamento dei docenti della scuola primaria e secondaria e degli operatori della formazione (art. 5) e l'istituzione della «Giornata regionale della Dieta mediterranea patrimonio dell'umanità», nonché di un «apposito albo dei ristoratori che somministrano esclusivamente prodotti tipici siciliani», con l'adozione delle opportune iniziative da parte degli assessorati competenti (art. 6, comma 3);&#13;
 che, infine, l'art. 7 della legge reg. Siciliana n. 12 del 2022 introduce, dopo l'art. 14 della legge della Regione Siciliana 2 agosto 2002, n. 5 (Istituzione delle strade e delle rotte del vino. Norme urgenti sull'inventario viticolo della Sicilia. Altre disposizioni per il settore agricolo), l'art. 14-bis, che disciplina nel dettaglio l'attività di oleoturismo, in particolare con la realizzazione di «[s]trade» finalizzate alla valorizzazione dell'olio di oliva e dei prodotti tipici agro-alimentari;&#13;
 che, pertanto, a fronte della previsione di un tale novero di attività e iniziative, la mancata indicazione, anche in via soltanto presuntiva, degli oneri finanziari a carico dell'ente regionale e delle risorse con le quali farvi fronte, pur in presenza di un evidente aggravio a carico del bilancio, determinerebbe un contrasto con l'obbligo di copertura finanziaria, di cui all'art. 81, terzo comma, Cost., obbligo peraltro ribadito dalla stessa normativa regionale siciliana, che all'art. 7, primo comma, della legge della Regione Siciliana 8 luglio 1977, n. 47 (Norme in materia di bilancio e contabilità della Regione siciliana) dispone che «[l]e leggi della Regione che importino nuove o maggiori spese, ovvero minori entrate, devono indicare la relativa copertura finanziaria [...]»;&#13;
 che, con atto depositato il 23 agosto 2022, si è costituita in giudizio la Regione Siciliana, deducendo l'inammissibilità e la non fondatezza del ricorso, sulla base del fatto che la legge regionale impugnata non determinerebbe alcun nuovo onere a carico delle finanze regionali;&#13;
 che la resistente ha osservato, in ogni caso, che la successiva legge della Regione Siciliana 10 agosto 2022, n. 16, recante «Modifiche alla legge regionale 25 maggio 2022, n. 13 e alla legge regionale 25 maggio 2022, n. 14. Variazioni al Bilancio di previsione della Regione Siciliana per il triennio 2022/2024. Disposizioni varie», all'art. 13, comma 100, aveva disposto che «[p]er le finalità di cui agli articoli 3 e 4 della legge regionale 12 maggio 2022, n. 12, è autorizzata, per l'esercizio finanziario 2022, la spesa di 50 migliaia di euro (Missione 16, Programma 1, capitolo 142602)»;&#13;
 che, con atto del 24 marzo 2023, il Presidente del Consiglio dei ministri, rappresentato e difeso dall'Avvocatura generale dello Stato, in considerazione delle modifiche normative sopravvenute e della conseguente possibilità di una rinuncia al ricorso, ha presentato istanza di rinvio dell'udienza fissata per il 18 aprile 2023, con allegata dichiarazione di adesione da parte dei difensori della Regione Siciliana;&#13;
 che la Presidente di questa Corte, con decreto del 28 marzo 2023, ha rinviato la discussione del giudizio all'udienza pubblica del 19 settembre 2023 e, con successivo decreto del 19 luglio 2023, alla camera di consiglio del 20 settembre 2023;&#13;
 che, con successivo atto depositato il 17 luglio 2023, il Presidente del Consiglio dei ministri ha dichiarato di rinunciare al ricorso, su conforme delibera adottata dal Consiglio dei ministri nella seduta del 31 maggio 2023;&#13;
 che, con atto depositato il 18 luglio 2023, il Presidente della Regione Siciliana ha dichiarato di accettare la rinuncia al ricorso.&#13;
 Considerato che il Presidente del Consiglio dei ministri ha rinunciato al ricorso, previa delibera del Consiglio dei ministri;&#13;
 che la rinuncia è stata accettata dalla Regione Siciliana;&#13;
 che la rinuncia al ricorso, accettata dalla controparte costituita, determina, ai sensi dell'art. 25 delle Norme integrative per i giudizi davanti alla Corte costituzionale, l'estinzione del processo.&#13;
 Visti l'art. 26, secondo comma, della legge 11 marzo 1953, n. 87, e gli artt. 24, comma 1, e 25 delle Norme integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi&#13;
 LA CORTE COSTITUZIONALE&#13;
 dichiara estinto il processo.&#13;
 Così deciso in Roma, nella sede della Corte costituzionale, Palazzo della Consulta, il 20 settembre 2023.&#13;
 F.to:&#13;
 Silvana SCIARRA, Presidente&#13;
 Augusto Antonio BARBERA, Redattore&#13;
 Roberto MILANA, Direttore della Cancelleria&#13;
 Depositata in Cancelleria il 10 ottobre 2023&#13;
 Il Direttore della Cancelleria&#13;
 F.to: Roberto MILANA</dispositivo>
  </pronuncia_testo>
</pronuncia>
