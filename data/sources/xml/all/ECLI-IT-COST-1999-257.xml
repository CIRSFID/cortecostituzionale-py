<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1999</anno_pronuncia>
    <numero_pronuncia>257</numero_pronuncia>
    <ecli>ECLI:IT:COST:1999:257</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/06/1999</data_decisione>
    <data_deposito>23/06/1999</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 16, 17  e  18    &#13;
 del   d.lgs.  28  luglio  1989,  n.  271  (Norme  di  attuazione,  di    &#13;
 coordinamento e transitorie del codice di procedura penale), promosso    &#13;
 con ordinanza emessa il 9 ottobre 1998 dalla Corte di cassazione  sul    &#13;
 ricorso  proposto da Donato Di Natale, iscritta al n. 65 del registro    &#13;
 ordinanze 1999 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 7, prima serie speciale, dell'anno 1999.                              &#13;
   Visto l'atto di costituzione di Donato Di Natale;                      &#13;
   Udito  nella  camera  di  consiglio  del  25 maggio 1999 il giudice    &#13;
 relatore Cesare Mirabelli.                                               &#13;
   Ritenuto che nel corso di un giudizio promosso da un  ufficiale  di    &#13;
 polizia  giudiziaria,  il  quale  aveva  proposto  ricorso  contro la    &#13;
 decisione della commissione di disciplina di secondo  grado  per  gli    &#13;
 ufficiali  e  gli agenti di polizia giudiziaria che lo aveva ritenuto    &#13;
 responsabile di illecito disciplinare applicando  la  sanzione  della    &#13;
 sospensione  dall'impiego  per  un  mese, la Corte di cassazione, con    &#13;
 ordinanza emessa il 9 ottobre  1998,  ha  sollevato,  in  riferimento    &#13;
 all'art.   102,  secondo  comma,  della  Costituzione,  questione  di    &#13;
 legittimità costituzionale degli artt. 16, 17  e  18  (inseriti  nel    &#13;
 capo  III  del  titolo  I,  delle  disposizioni relative alla polizia    &#13;
 giudiziaria) del d.lgs. 28 luglio 1989, n. 271 (Norme di  attuazione,    &#13;
 di coordinamento e transitorie del codice di procedura penale);          &#13;
     che  le  disposizioni  denunciate  regolamentano  le sanzioni, il    &#13;
 procedimento ed i ricorsi disciplinari per gli ufficiali e gli agenti    &#13;
 di  polizia  giudiziaria  relativamente  alle  trasgressioni  che  si    &#13;
 riferiscono  all'esercizio  di  tali  loro  funzioni,  stabilendo, in    &#13;
 particolare:  che competente a  giudicare  dell'azione  disciplinare,    &#13;
 promossa  dal  procuratore generale presso la corte d'appello nel cui    &#13;
 distretto l'ufficiale o l'agente presta servizio, è una  commissione    &#13;
 composta  da due magistrati, nominati dal consiglio giudiziario, e da    &#13;
 un ufficiale di polizia giudiziaria, nominato dall'amministrazione di    &#13;
 appartenenza dell'incolpato (art. 17); che contro la decisione  della    &#13;
 commissione può essere proposto ricorso a una commissione di secondo    &#13;
 grado,  la quale ha sede presso il Ministero di grazia e giustizia ed    &#13;
 è composta da due magistrati, nominati dal Consiglio superiore della    &#13;
 magistratura, e da un ufficiale di polizia  giudiziaria  appartenente    &#13;
 alla  stessa  amministrazione dell'incolpato; che contro le decisioni    &#13;
 di secondo  grado  può  essere  direttamente  proposto  ricorso  per    &#13;
 cassazione per violazione di legge (art. 18);                            &#13;
     che   il   giudice  rimettente  ritiene  che  le  commissioni  di    &#13;
 disciplina  siano  da  qualificare  come  giudice  speciale,  la  cui    &#13;
 istituzione   è   vietata   dall'art.   102,  secondo  comma,  della    &#13;
 Costituzione, essendo ammesso nei  confronti  delle  decisioni  della    &#13;
 commissione   centrale  esclusivamente  ricorso  per  cassazione  per    &#13;
 violazione di legge, e non invece gli ordinari rimedi giurisdizionali    &#13;
 previsti per i provvedimenti amministrativi; le commissioni, inoltre,    &#13;
 configurerebbero  un  giudice  speciale  di  nuova  istituzione,  non    &#13;
 salvaguardato  dalla  VI  disposizione  transitoria  e  finale  della    &#13;
 Costituzione, in quanto in precedenza le  sanzioni  disciplinari  nei    &#13;
 confronti degli ufficiali e degli agenti di polizia giudiziaria erano    &#13;
 applicate  direttamente  dal  procuratore  generale  presso  la corte    &#13;
 d'appello (si veda l'art. 229 cod. proc.  pen. del 1930);                &#13;
     che la parte privata, ricorrente nel giudizio principale,  si  è    &#13;
 costituita   nel  giudizio  dinanzi  alla  Corte,  per  sostenere  la    &#13;
 fondatezza della questione di legittimità costituzionale.               &#13;
   Considerato  che,  successivamente  all'ordinanza  di   rimessione,    &#13;
 questa  Corte,  esaminando una questione identica, con la sentenza n.    &#13;
 394 del 1998 ha dichiarato l'illegittimità costituzionale  dell'art.    &#13;
 18,  comma 5, del d.lgs. 28 luglio 1989, n. 271: ossia di quella, tra    &#13;
 le   disposizioni   denunciate,   che   valeva   a  configurare  come    &#13;
 giurisdizionale,  anziché  come  amministrativa,  l'attività  delle    &#13;
 commissioni  di  disciplina, prevedendo che contro le decisioni della    &#13;
 commissione centrale potesse essere direttamente proposto ricorso per    &#13;
 cassazione per violazione di legge;                                      &#13;
     che, essendo  venuta  meno,  a  seguito  della  dichiarazione  di    &#13;
 illegittimità  costituzionale,  la disposizione dalla quale derivava    &#13;
 il vizio denunciato dal giudice rimettente, la questione ora proposta    &#13;
 deve essere dichiarata manifestamente inammissibile.                     &#13;
   Visti gli artt. 26, secondo comma, della legge 11  marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  degli  artt.  16, 17 e 18 del d.lgs. 28    &#13;
 luglio  1989,  n.  271  (Norme  di  attuazione,  di  coordinamento  e    &#13;
 transitorie   del   codice   di   procedura  penale),  sollevata,  in    &#13;
 riferimento all'art. 102, secondo comma,  della  Costituzione,  dalla    &#13;
 Corte di cassazione con l'ordinanza indicata in epigrafe.                &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, l'11 giugno 1999.                                &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Mirabelli                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 23 giugno 1999.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
