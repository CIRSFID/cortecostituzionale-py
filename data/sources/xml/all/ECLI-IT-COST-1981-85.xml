<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>85</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:85</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/04/1981</data_decisione>
    <data_deposito>01/06/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRIDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. GUGLIELMO ROEHRSSEN - Avv. ORONZO &#13;
 REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - Avv. ALBERTO MALAGUGINI - &#13;
 Prof. LIVIO PALADIN - Dott. ARNALDO MACCARONE - Prof. ANTONIO LA &#13;
 PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt. 6  e  &#13;
 14 del d.P.R. 26 ottobre 1972, n. 643,  come  modif.  dal    d.P.R.  23  &#13;
 dicembre  1974, n. 688, e dell'art. 8 della legge  16 dicembre 1977, n.  &#13;
 904 (imposta sull'incremento di  valore degli immobili),  promossi  con  &#13;
 le seguenti  ordinanze:                                                  &#13;
     1.  -  ordinanza  emessa  il  15  ottobre  1976 dalla   Commissione  &#13;
 tributaria di 1 grado  di  Massa  Carrara  sul    ricorso  proposto  da  &#13;
 Lucchesi  Bruno,  iscritta  al  n.  734 del   registro ordinanze 1980 e  &#13;
 pubblicata nella Gazzetta  Ufficiale della Repubblica n. 345 del 1980;   &#13;
     2. - ordinanza  emessa  il  26  ottobre  1979  dalla    Commissione  &#13;
 tributaria di 1 grado di Larino sul ricorso  proposto da Vocale Matteo,  &#13;
 iscritta  al  n.  764  del registro   ordinanze 1980 e pubblicata nella  &#13;
 Gazzetta Ufficiale  della Repubblica n. 338 del 1980.                    &#13;
     Visti gli atti di intervento del  Presidente  del  Consiglio    dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio  del  5 marzo 1981 il   Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto che la Commissione  tributaria  di  Massa  Carrara,    con  &#13;
 ordinanza  emessa  il  15  ottobre  1976 (ma pervenuta   alla Corte l'8  &#13;
 agosto 1980) ha sollevato questione  di    legittimità  costituzionale  &#13;
 degli  artt.  6  e 14 del d.P.R. 26  ottobre 1972, n. 643 (e successive  &#13;
 modificazioni) - in    riferimento  all'art.  53,  primo  comma,  della  &#13;
 Costituzione  -    "in quanto non prevedono che l'incremento di valore,  &#13;
 soggetto ad INVIM, sia depurato delle variazioni    nominali  dovute  a  &#13;
 svalutazione  monetaria";  che la Commissione tributaria di primo grado  &#13;
 di Larino, con  ordinanza emessa il 26 ottobre 1979 (ma pervenuta  alla  &#13;
 Corte  il  13  ottobre  1980)  ha  impugnato a sua volta l'art. 6   del  &#13;
 predetto decreto presidenziale e l'art. 8  della  legge  16    dicembre  &#13;
 1977,  n.  904,  sempre in riferimento all'art. 53  della Costituzione;  &#13;
 che in entrambi i giudizi ha spiegato   intervento  il  Presidente  del  &#13;
 Consiglio dei ministri,  chiedendo che la Corte dichiari l'infondatezza  &#13;
 di tali  questioni.                                                      &#13;
     Considerato  che  i  giudizi  predetti  vanno  riuniti  in   quanto  &#13;
 prospettano analoghe questioni di legittimità;                          &#13;
     che le stesse questioni sono state già decise dalla  Corte,    con  &#13;
 sentenza  8  novembre  1979,  n.  126,  dichiarando la   illegittimità  &#13;
 costituzionale dell'art. 14 del d.P.R. 26   ottobre  1972,  n.  643,  e  &#13;
 dell'art.  8 della legge 16 dicembre  1977, n. 904, "nella parte in cui  &#13;
 le disposizioni   concernenti  il  calcolo  dell'incremento  di  valore  &#13;
 imponibile  netto  determinano  - in relazione al periodo di formazione  &#13;
 dell'incremento stesso - ingiustificata  disparità di trattamento  tra  &#13;
 i  soggetti  passivi  del  tributo",    e  dichiarando  non  fondate le  &#13;
 questioni di costituzionalità  degli artt. 2, 4, 6, 7,  15  e  16  del  &#13;
 d.P.R. 26 ottobre 1972, n.  643, sollevate in riferimento agli artt. 3,  &#13;
 42,  47  e  53  della    Costituzione;  e  che nelle ordinanze non sono  &#13;
 prospettati  profili nuovi, né sono addotti motivi che possano indurre  &#13;
 la Corte a modificare la propria giurisprudenza;                         &#13;
     che peraltro, successivamente alla decisione di questa   Corte,  la  &#13;
 disciplina  normativa  dell'INVIM  è  stata   modificata con decreto -  &#13;
 legge 12 novembre 1979, n.   571, convertito  con  modificazioni  nella  &#13;
 legge  12  gennaio    1980,  n.  2, la quale ha soppresso l'art. 14 del  &#13;
 d.P.R. n.  643 del 1972, sostituito l'art. 15,  e  regolato  le  misure  &#13;
 delle  aliquote  stabilite per gli anni 1979 e 1980 ai sensi  dell'art.  &#13;
 16, statuendo che le nuove disposizioni si  applicano anche ai rapporti  &#13;
 sorti prima della loro entrata  in vigore ed a  tale  data  non  ancora  &#13;
 definiti,  "per  i quali   tuttavia l'ammontare dell'imposta dovuta non  &#13;
 può in  ogni    caso  superare  quello  determinabile  con  i  criteri  &#13;
 contenuti  nelle norme precedentemente in vigore" (art. 3);              &#13;
     che  conseguentemente  si  ravvisa  la  necessità di disporre   la  &#13;
 restituzione degli atti alle  Commissioni  tributarie    sopraindicate,  &#13;
 perché  accertino se, ed in qual misura, le  questioni sollevate siano  &#13;
 tuttora rilevanti.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     Ordina la restituzione degli atti alla  Commissione  tributaria  di  &#13;
 primo  grado  di  Massa Carrara e alla Commissione  tributaria di primo  &#13;
 grado di Larino.                                                         &#13;
     Così deciso in Roma, in camera di consiglio,  nella  sede    della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'8  aprile 1981.          &#13;
                                   F.to:   LEONETTO   AMADEI   -  GIULIO  &#13;
                                   GIONFRIDA  -   EDOARDO   VOLTERRA   -  &#13;
                                   MICHELE   ROSSANO     -  ANTONINO  DE  &#13;
                                   STEFANO  -  GUGLIELMO   ROEHRSSEN   -  &#13;
                                   ORONZO  REALE  - BRUNETTO BUCCIARELLI  &#13;
                                   DUCCI - ALBERTO  MALAGUGINI  -  LIVIO  &#13;
                                   PALADIN - ARNALDO MACCARONE - ANTONIO  &#13;
                                   LA  PERGOLA  -  VIRGILIO  ANDRIOLI  -  &#13;
                                   GIUSEPPE  FERRARI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
