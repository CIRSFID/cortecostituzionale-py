<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1993</anno_pronuncia>
    <numero_pronuncia>8</numero_pronuncia>
    <ecli>ECLI:IT:COST:1993:8</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BORZELLINO</presidente>
    <relatore_pronuncia>Renato Granata</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/01/1993</data_decisione>
    <data_deposito>19/01/1993</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Giuseppe BORZELLINO; &#13;
 Giudici: dott. Francesco GRECO, prof. Gabriele PESCATORE, avv. Ugo &#13;
 SPAGNOLI, prof. Francesco Paolo CASAVOLA, prof. Antonio &#13;
 BALDASSARRE, prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. &#13;
 Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. &#13;
 Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità  costituzionale  degli  artt.  1,  primo    &#13;
 comma,  2,  primo  comma, 3, primo comma e allegati A, tariffa; parte    &#13;
 prima,  31  della  tariffa  del  d.P.R.  26  ottobre  1972,  n.   642    &#13;
 (Disciplina  dell'imposta  di  bollo)  e successive modificazioni e 7    &#13;
 della legge 29 dicembre 1990, n. 405 ("Disposizioni per la formazione    &#13;
 del bilancio annuale e pluriennale  dello  Stato;  legge  finanziaria    &#13;
 1991")  promosso con ordinanza emessa il 28 novembre 1991 dal Pretore    &#13;
 di Roma nel procedimento civile vertente tra Pratini Augusto e Rienzi    &#13;
 Carlo, iscritta al n. 130 del registro ordinanze  1992  e  pubblicata    &#13;
 nella   Gazzetta  Ufficiale  della  Repubblica  n.  12,  prima  serie    &#13;
 speciale, dell'anno 1992;                                                &#13;
    Visti l'atto di costituzione di Pratini Augusto, nonché l'atto di    &#13;
 intervento del Presidente del Consiglio dei ministri;                    &#13;
    Udito  nell'udienza  pubblica  del  3  novembre  1992  il  Giudice    &#13;
 relatore Renato Granata;                                                 &#13;
    Uditi  l'avv. Roberto Canestrelli per Pratini Augusto e l'Avvocato    &#13;
 dello Stato  Franco  Favara  per  il  Presidente  del  Consiglio  dei    &#13;
 ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  -  Con  ordinanza  del 28 novembre 1991 il Pretore di Roma nel    &#13;
 corso del procedimento civile pendente tra Pratini Augusto  e  Rienzi    &#13;
 Carlo    ha   sollevato   questione   incidentale   di   legittimità    &#13;
 costituzionale degli artt. 1, primo comma, 2, primo comma,  3,  primo    &#13;
 comma, e allegato A, tariffa, parte prima, art. 31 della tariffa, del    &#13;
 d.P.R.  26  ottobre  1972  n.  642,  e  successive  modificazioni,  e    &#13;
 dell'art. 7 legge  29  dicembre  1990  n.  405  nella  parte  in  cui    &#13;
 assoggettano  gli  atti  difensivi  nei  procedimenti giurisdizionali    &#13;
 civili ad imposta di bollo fissa  nella  misura  di  lire  10.000  al    &#13;
 foglio  per  contrasto  con  gli  artt. 3, 24 (primo, secondo e terzo    &#13;
 comma) e 53 Cost.                                                        &#13;
    Il pretore rimettente ritiene che  l'imposta  di  bollo,  riscossa    &#13;
 nella  forma  di fogli da impiegare nella difesa scritta del giudizio    &#13;
 civile, debba essere proporzionale all'ipotizzato  costo  totale  del    &#13;
 processo  atteso  che  i cittadini che accedono alla giustizia devono    &#13;
 pagare secondo criteri  che  rispettino  la  misura  della  spesa  da    &#13;
 ciascuno  di  essi  indotta.  Quindi  tale  imposta  dovrebbe  essere    &#13;
 proporzionale al valore della causa  (analogamente  al  criterio  che    &#13;
 regola  l'imposizione  indiretta  o  sugli  affari) oppure al reddito    &#13;
 delle parti (secondo il criterio di  progressività  dell'imposizione    &#13;
 diretta). Invece la quantità di fogli utilizzati per la difesa - che    &#13;
 attualmente   determina   l'ammontare   dell'imposta   dovuta  -  non    &#13;
 costituisce un criterio adeguato  non  essendo  neppure  univocamente    &#13;
 espressivo  del  costo  del  processo  potendo ipotizzarsi una difesa    &#13;
 scritta assai complessa (e quindi diffusa) in un processo di limitato    &#13;
 valore e viceversa una difesa assai semplice (e quindi concisa  )  in    &#13;
 un  processo  di  elevato  valore.  Inoltre  l'imposta  di  bollo  ha    &#13;
 raggiunto una misura eccessiva, con  conseguente  compressione  della    &#13;
 possibilità di agire e di difendersi in giudizio in funzione inversa    &#13;
 del   reddito,   compressione  non  adeguatamente  contrastata  dalla    &#13;
 normativa sul gratuito patrocinio.                                       &#13;
    La questione, secondo il  pretore  rimettente,  è  rilevante  nel    &#13;
 giudizio  civile  in corso perché - anche se l'art. 19 d.P.R. n. 642    &#13;
 del 1972 cit.  dispone  che  i  giudici  non  possono  rifiutarsi  di    &#13;
 assumere  a base dei loro provvedimenti gli atti e i documenti non in    &#13;
 regola con le disposizioni  sul  bollo  -  la  prospettiva  di  dover    &#13;
 comunque   poi   regolarizzare   tale   aspetto   fiscale  condiziona    &#13;
 l'esercizio del diritto di difesa nel senso che le  parti  potrebbero    &#13;
 con  maggior  aggio  dimensionare lo sviluppo degli scritti difensivi    &#13;
 se, in accoglimento della sollevata questione  di  costituzionalità,    &#13;
 l'imposta  di  bollo  fosse  vuoi  in  assoluto  inferiore  a  quella    &#13;
 attualmente dovuta, vuoi proporzionata  al  costo  e  al  valore  del    &#13;
 processo.                                                                &#13;
    2.   -  Si  è  costituita  la  parte  privata  Pratini  chiedendo    &#13;
 l'accoglimento della sollevata questione di  costituzionalità  senza    &#13;
 ulteriormente argomentare.                                               &#13;
    3.  -  È  intervenuto  il  Presidente del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo   pregiudizialmente   che   la   questione  sia  dichiarata    &#13;
 inammissibile per difetto di rilevanza in  quanto  ininfluente  (come    &#13;
 del   resto  riconosciuto  del  giudice  rimettente)  al  fine  della    &#13;
 decisione del giudizio a quo  (di  cui  peraltro  non  sono  indicati    &#13;
 oggetto  e natura); né dal giudice rimettente sono allegati elementi    &#13;
 di fatto  che  facciano  pensare  ad  un'effettiva  compressione  del    &#13;
 diritto di difesa dell'attore o del convenuto.                           &#13;
    Nel merito l'Avvocatura ritiene non fondata la questione sollevata    &#13;
 perché  la  commisurazione dell'imposta al numero di fogli difensivi    &#13;
 risponde al canone di ragionevolezza e  di  proporzionalità;  invece    &#13;
 l'auspicata  commisurazione  dell'imposta  di  bollo al reddito delle    &#13;
 parti è impraticabile posto che le parti sono due, o più, e che non    &#13;
 può certo farsi la media dei loro redditi; mentre l'altro ipotizzato    &#13;
 parametro (il valore della causa)  non  è  del  tutto  negletto  dal    &#13;
 legislatore  che prevede che l'imposta di bollo per gli atti compiuti    &#13;
 dal giudice e dal cancelliere sia meno  elevata  in  pretura  che  in    &#13;
 tribunale (art. 7, secondo comma, legge n. 405 del 1990 cit.).<diritto>Considerato in diritto</diritto>1.  - È stata sollevata - in riferimento agli artt. 3, 24, primo,    &#13;
 secondo e  terzo  comma,  e  53  Cost.  -  questione  incidentale  di    &#13;
 legittimità  costituzionale  degli  artt.  1,  primo comma, 2, primo    &#13;
 comma,  e  3,  primo  comma,  del  d.P.R.  26  ottobre  1972  n.  642    &#13;
 (Disciplina dell'imposta di bollo), dell'art. 31 della tariffa, parte    &#13;
 prima,  Allegato  A  del  cit.  d.P.R.    26  ottobre  1972 n. 642, e    &#13;
 dell'art.  7  legge  29 dicembre 1990 n. 405 (legge finanziaria 1991)    &#13;
 nella parte in cui assoggettano gli atti difensivi  nei  procedimenti    &#13;
 giurisdizionali  civili  ad  imposta di bollo fissa e nella misura di    &#13;
 lire 10.000 al foglio, anziché in  misura  proporzionata  al  valore    &#13;
 della  causa  o alla capacità contributiva delle parti o comunque in    &#13;
 misura inferiore a quella suddetta.                                      &#13;
    2. - Va premesso  che  nelle  more  del  giudizio  incidentale  di    &#13;
 costituzionalità   la   misura   dell'imposta   di  bollo  è  stata    &#13;
 ulteriormente modificata dagli artt. 9 e  10  del  decreto  legge  11    &#13;
 luglio 1992 n. 333, convertito con modificazioni nella legge 8 agosto    &#13;
 1992  n.  359  (in  particolare  l'imposta  fissa  di  bollo è stata    &#13;
 elevata, a partire  dal  14  luglio  1992,  a  lire  15.000).  Ma  la    &#13;
 sopravvenuta  innovazione  normativa  non preclude, né in alcun modo    &#13;
 influenza,  il  pregiudiziale  esame   della   eccezione,   sollevata    &#13;
 dall'Avvocatura  di  Stato,  di  inammissibilità  della questione di    &#13;
 costituzionalità per difetto del requisito della rilevanza.             &#13;
    3. - L'eccezione è fondata.                                          &#13;
    L'art. 19 del  d.P.R.  n.  642  del  1972  cit.,  come  sostituito    &#13;
 dall'art.  16 del d.P.R. 30 dicembre 1982 n. 955, prevede che - salve    &#13;
 le ipotesi (connotate  quindi  da  carattere  di  specialità)  della    &#13;
 cambiale, del vaglia cambiario e dell'assegno bancario, che non hanno    &#13;
 la qualità di titolo esecutivo se non risultano regolarmente bollati    &#13;
 -  il mancato od insufficiente pagamento dell'imposta di bollo non è    &#13;
 ostativo alla  produzione  in  giudizio  di  documenti  e  di  difese    &#13;
 scritte.  Quindi il cancelliere o il segretario non possono rifiutare    &#13;
 l'attestazione dell'avvenuto deposito, ma sono  unicamente  tenuti  a    &#13;
 trasmetterli  (anche  in  copia) all'ufficio del registro per la loro    &#13;
 regolarizzazione ai sensi del successivo art. 31; e,  quel  che  più    &#13;
 rileva, di tali difese e documenti il giudice deve tenere normalmente    &#13;
 conto, rimanendo così escluso che il profilo tributario dell'imposta    &#13;
 di  bollo  possa  precludere  o pregiudicare l'esercizio del diritto,    &#13;
 costituzionalmente riconosciuto, di agire in giudizio (art. 24, primo    &#13;
 comma, Cost.).                                                           &#13;
    Consegue che la misura dell'obbligazione tributaria gravante sulle    &#13;
 parti in causa a titolo di imposta di bollo non incide al fine  della    &#13;
 decisione  della  controversia devoluta alla cognizione del giudice a    &#13;
 quo.                                                                     &#13;
    4.  -  Neppure  essa  incide   sotto   il   profilo   -   indicato    &#13;
 nell'ordinanza   di   rimessione  a  giustificazione  della  ritenuta    &#13;
 rilevanza della questione di costituzionalità - che il timore  della    &#13;
 successiva  regolarizzazione  potrebbe  condizionare  l'esercizio del    &#13;
 diritto di  difesa  nel  senso  che  le  parti  sarebbero  indotte  a    &#13;
 depositare  documenti ed a svolgere argomentazioni negli atti scritti    &#13;
 in numero e con un'ampiezza inferiore  ed  inadeguata  rispetto  alle    &#13;
 reali  esigenze  difensive.  Non  è  sufficiente infatti allegare un    &#13;
 ipotetico (oltre che indiretto) pregiudizio del diritto di  agire  in    &#13;
 giudizio   per   ritenere  che  il  giudice  non  possa  definire  la    &#13;
 controversia senza che sia prima decisa la questione  incidentale  di    &#13;
 legittimità  costituzionale;  occorre  che questa sospetta lesione -    &#13;
 non altrimenti emendabile, in tesi, se non mediante la  dichiarazione    &#13;
 di  incostituzionalità della norma censurata - sussista nel giudizio    &#13;
 a quo, come concreta possibilità e non già come  astratta  ipotesi.    &#13;
 Invece il giudice rimettente non indica il benché minimo elemento di    &#13;
 fatto  che  possa indurre a ritenere sussistente o probabile od anche    &#13;
 meramente  (in  concreto)  possibile  la  lamentata  compressione del    &#13;
 diritto di difesa conseguente alla  scelta  delle  parti  (o  di  una    &#13;
 parte)  di  limitare  il numero dei documenti prodotti o di contenere    &#13;
 l'estensione degli scritti difensivi al  fine  di  evitare  di  dover    &#13;
 corrispondere un eccessivo importo a titolo di imposta di bollo.         &#13;
    Non  sussiste  pertanto  la rilevanza della sollevata questione di    &#13;
 costituzionalità, che va dichiarata inammissibile.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara inammissibile la questione di legittimità  costituzionale    &#13;
 degli  artt.  1,  primo  comma, 2, primo comma, e 3, primo comma, del    &#13;
 d.P.R. 26 ottobre 1972 n. 642  (Disciplina  dell'imposta  di  bollo),    &#13;
 dell'art.  31  della  tariffa,  parte  prima, Allegato A dello stesso    &#13;
 d.P.R. 26 ottobre 1972 n. 642, e dell'art. 7 legge 29  dicembre  1990    &#13;
 n.  405  (Disposizioni  per  la  formazione  del  bilancio  annuale e    &#13;
 pluriennale dello Stato; legge finanziaria 1991), in riferimento agli    &#13;
 artt. 3, 24, primo, secondo e terzo comma, e 53 Cost., sollevata  dal    &#13;
 Pretore di Roma con l'ordinanza indicata in epigrafe.                    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 gennaio 1993.                              &#13;
                       Il presidente: BORZELLINO                          &#13;
                         Il redattore: GRANATA                            &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 19 gennaio 1993.                         &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
