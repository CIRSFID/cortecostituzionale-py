<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>67</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:67</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/03/1996</data_decisione>
    <data_deposito>08/03/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato &#13;
 GRANATA, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di legittimità costituzionale dell'art. 37 del codice    &#13;
 penale militare di pace promosso con ordinanza emessa il 2 marzo 1995    &#13;
 dal tribunale militare di Padova nel procedimento penale a carico  di    &#13;
 Ferrazini  Corrado,  iscritta al n. 351 del registro ordinanze 1995 e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica  -  prima  serie    &#13;
 speciale - n. 25, dell'anno 1995;                                        &#13;
   Udito  nella  camera  di  consiglio  del 24 gennaio 1996 il giudice    &#13;
 relatore Francesco Guizzi;                                               &#13;
   Ritenuto che  nel  corso  del  procedimento  penale  a  carico  del    &#13;
 maggiore   dell'esercito  Ferrazini  Corrado,  accusato  del  delitto    &#13;
 d'abuso d'ufficio (art.  323,  primo  comma,  del  codice  penale  in    &#13;
 relazione  all'art.  37  del  codice  penale  militare  di  pace), il    &#13;
 tribunale militare di Padova, con ordinanza  del  2  marzo  1995,  ha    &#13;
 sollevato,  per  violazione  degli  artt.  3 e 97, primo comma, della    &#13;
 Costituzione, questione di legittimità costituzionale  dell'art.  37    &#13;
 del codice penale militare di pace;                                      &#13;
     che  secondo detta disposizione "qualunque violazione della legge    &#13;
 penale militare è reato militare";                                      &#13;
     che  reati  "ontologicamente  identici"   sarebbero   considerati    &#13;
 militari  o comuni in forza del citato art. 37, primo comma, e quindi    &#13;
 assoggettati  a  regimi  giuridici  diversi   (elemento   soggettivo,    &#13;
 aggravanti,  attenuanti,  scriminanti,  pene principali e accessorie,    &#13;
 effetti penali della condanna, diversa giurisdizione, ecc.);             &#13;
     che  l'irrazionalità  non  riguarderebbe   soltanto   il   reato    &#13;
 contestato  al  Ferrazini,  ma anche altre figure previste dai codici    &#13;
 penali;                                                                  &#13;
     che si configurerebbero, infatti,  come  reati  militari  l'abuso    &#13;
 dell'ufficio   di   comando,   quando   si   traduce  in  peculato  o    &#13;
 malversazione (artt. 215 e 216), ma non il generico abuso; l'omicidio    &#13;
 a danno del superiore  o  dell'inferiore  (artt.  186  e  195)  nelle    &#13;
 situazioni  di cui all'art. 199, ma non quando commesso nell'ambiente    &#13;
 militare in altre diverse circostanze; le lesioni  volontarie  (artt.    &#13;
 223  e  224)  nei  confronti di qualsiasi militare, ma non l'omicidio    &#13;
 preterintenzionale o volontario; il furto  a  danno  di  militare  in    &#13;
 luogo  militare (art.   239), ma non la rapina; la minaccia rivolta a    &#13;
 un militare (art. 229), ma non la  violenza  privata  o  l'estorsione    &#13;
 (forme  delittuose  in  cui,  a  volte,  si  manifesta  il cosiddetto    &#13;
 nonnismo);  e   persino   l'eccesso   colposo   in   una   causa   di    &#13;
 giustificazione (art. 45), ma non il corrispondente reato colposo;       &#13;
     che vi sarebbe, altresì, lesione del principio di buon andamento    &#13;
 dell'amministrazione  della  giustizia  (art.  97, primo comma, della    &#13;
 Costituzione)  a  causa  dell'artificiosità  e  irrazionalità   dei    &#13;
 criteri  di  delimitazione  della  sfera  di competenza dei tribunali    &#13;
 militari, con  l'effetto  che  l'azione  penale  seguirebbe  percorsi    &#13;
 procedimentali  inutilmente laboriosi, articolandosi su due fasi, una    &#13;
 dinanzi al giudice militare e l'altra davanti a quello ordinario;        &#13;
   Considerato che  identica  questione,  sulla  base  delle  medesime    &#13;
 argomentazioni,  è  stata  esaminata  da  questa  Corte e dichiarata    &#13;
 inammissibile con sentenza n. 298 del 1995, giacché  il  configurare    &#13;
 l'illecito   come   reato   militare  o  comune  rientra  nei  poteri    &#13;
 discrezionali del legislatore, con il solo limite  del  canone  della    &#13;
 ragionevolezza;                                                          &#13;
     che  l'ordinanza  di  rimessione  non  prospetta  nuove o diverse    &#13;
 argomentazioni che possano condurre a conclusioni diverse;               &#13;
     che, pertanto, va dichiarata la manifesta inammissibilità  della    &#13;
 questione;                                                               &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87, e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la  manifesta   inammissibilità   della   questione   di    &#13;
 legittimità  costituzionale  dell'art. 37 del codice penale militare    &#13;
 di pace, sollevata dal tribunale militare  di  Padova,  in  relazione    &#13;
 agli  artt.  3 e 97, primo comma, della Costituzione, con l'ordinanza    &#13;
 indicata in epigrafe.                                                    &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 4 marzo 1996.                                 &#13;
                         Il presidente:  Ferri                            &#13;
                         Il redattore:  Guizzi                            &#13;
                       Il cancelliere:  Di Paola                          &#13;
   Depositata in cancelleria l'8 marzo 1996.                              &#13;
               Il direttore della cancelleria:  Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
