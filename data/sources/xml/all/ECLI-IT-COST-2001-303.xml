<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>303</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:303</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>RUPERTO</presidente>
    <relatore_pronuncia>Annibale Marini</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/2001</data_decisione>
    <data_deposito>25/07/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare RUPERTO; &#13;
 Giudici: Fernando SANTOSUOSSO, Massimo VARI, Riccardo CHIEPPA, &#13;
Gustavo ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, &#13;
Guido NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Giovanni &#13;
Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei giudizi di legittimità costituzionale degli articoli 2, comma 1, &#13;
lett.  pp),  della  legge 30 novembre 1998, n. 419 (Delega al Governo &#13;
per  la  razionalizzazione  del  Servizio  sanitario  nazionale e per &#13;
l'adozione   di  un  testo  unico  in  materia  di  organizzazione  e &#13;
funzionamento  del  Servizio sanitario nazionale. Modifiche al d.lgs. &#13;
30 dicembre  1992,  n. 502),  e  15-nonies  del  decreto  legislativo &#13;
30 dicembre  1992,  n. 502  (Riordino  della  disciplina  in  materia &#13;
sanitaria,  a norma dell'art. 1 della legge 23 ottobre 1992, n. 421), &#13;
aggiunto dall'art. 13 del decreto legislativo 19 giugno 1999, n. 229, &#13;
promossi   con  ordinanze  emesse  il  10 marzo  2000  dal  Tribunale &#13;
amministrativo  regionale della Campania sui ricorsi riuniti proposti &#13;
da  Rossi Luciano contro la Seconda Università degli studi di Napoli &#13;
ed altri, iscritta al n. 748 del registro ordinanze 2000 e pubblicata &#13;
nella  Gazzetta  Ufficiale  della Repubblican. 50, 1ª serie speciale, &#13;
dell'anno  2000,  e  il  10 marzo  2000  dal Tribunale amministrativo &#13;
regionale  della  Campania  sul  ricorso  proposto  da Tirri Giuseppe &#13;
contro  la  Seconda  Università  degli  studi  di  Napoli  ed altra, &#13;
iscritta  al  n. 810  del  registro ordinanze 2000 e pubblicata nella &#13;
Gazzetta   Ufficiale   della  Repubblica  n. 1,  1a  serie  speciale, &#13;
dell'anno 2001. &#13;
    Visti  gli  atti  di  intervento del Presidente del Consiglio dei &#13;
ministri; &#13;
    Udito  nella  camera  di  consiglio del 20 giugno 2001 il giudice &#13;
relatore Annibale Marini. &#13;
    Ritenuto   che   il   Tribunale  amministrativo  regionale  della &#13;
Campania,  con  due ordinanze di contenuto analogo emesse il 10 marzo &#13;
2000,  ha  sollevato,  in  riferimento  agli  artt. 3, 9, 36, 76 e 77 &#13;
Cost.,  questione di legittimità costituzionale degli artt. 2, comma &#13;
1, lett. pp), della legge 30 novembre 1998, n. 419 (Delega al Governo &#13;
per  la  razionalizzazione  del  Servizio  sanitario  nazionale e per &#13;
l'adozione   di  un  testo  unico  in  materia  di  organizzazione  e &#13;
funzionamento  del  Servizio sanitario nazionale. Modifiche al d.lgs. &#13;
30 dicembre  1992,  n. 502),  e  15-nonies  del  decreto  legislativo &#13;
30 dicembre  1992,  n. 502  (Riordino  della  disciplina  in  materia &#13;
sanitaria,  a norma dell'art. 1 della legge 23 ottobre 1992, n. 421), &#13;
aggiunto dall'art. 13 del decreto legislativo 19 giugno 1999, n. 229; &#13;
        che,  ad  avviso  del rimettente, la previsione di cessazione &#13;
dall'attività    assistenziale    ordinaria    "anticipatamente   al &#13;
raggiungimento   dell'età   pensionabile   dei  docenti",  contenuta &#13;
nell'art. 15-nonies del decreto legislativo 30 dicembre 1992, n. 502, &#13;
non  sarebbe  coerente  con  il  principio  del  buon  andamento  sia &#13;
dell'insegnamento  e  della  ricerca  universitaria  che  del sistema &#13;
sanitario; &#13;
        che   tanto  la  suddetta  norma  delegata  quanto  la  norma &#13;
delegante  di  cui all'art. 2, comma 1, lett. pp), della legge n. 419 &#13;
del  1998,  sarebbero  poi lesive del principio di eguaglianza di cui &#13;
all'art. 3    Cost.,    in   quanto,"nell'intento   di   privilegiare &#13;
l'omogeneità  dei  trattamenti  del personale del Servizio sanitario &#13;
nazionale   e   di   quello   universitario"   avrebbero  creato  una &#13;
ingiustificata  discriminazione  tra  docenti,  introducendo  marcate &#13;
differenze  di  stato  giuridico  in funzione dell'età, in danno dei &#13;
docenti  "strutturati",  nell'ambito  di  una categoria indubbiamente &#13;
unitaria; &#13;
        che la norma di delega, sotto altro aspetto, sarebbe altresì &#13;
in  contrasto  con  l'art. 76 Cost., per la mancata predeterminazione &#13;
dei  criteri  idonei  a  definire le modalità ed i termini del nuovo &#13;
assetto funzionale dell'attività assistenziale; &#13;
        che   siffatto   difetto  di  predeterminazione  dei  criteri &#13;
direttivi  sarebbe  poi reso ancor più palese - ad avviso sempre del &#13;
rimettente   -  dal  successivo  rinvio,  da  parte  del  legislatore &#13;
delegato, ad atti di normazione secondaria; &#13;
        che  la materia da disciplinare, riguardando l'individuazione &#13;
della parte di attività assistenziale da lasciarsi affidata, ai fini &#13;
didattici  e  di  ricerca, ai docenti cessati dallo svolgimento delle &#13;
attività    assistenziali,   involgerebbe,   infatti,   i   principi &#13;
fondamentali    relativi    all'istruzione,   con   riferimento   sia &#13;
all'organizzazione  scolastica, di cui le università sono parte, sia &#13;
al  diritto di accedervi e di usufruire delle prestazioni che essa è &#13;
chiamata   a  fornire,  cosicché  la  mancata  indicazione  in  sede &#13;
legislativa delle linee fondamentali della disciplina si risolverebbe &#13;
in una violazione della riserva di legge prevista dagli artt. 33 e 34 &#13;
della Costituzione; &#13;
        che   lo   strumento   convenzionale  prescelto  non  sarebbe &#13;
oltretutto   idoneo   a   garantire  l'uniformità  della  disciplina &#13;
sull'intero  territorio  nazionale,  con  violazione ancora sia della &#13;
riserva  di  legge  in  materia universitaria, sia dell'art. 97 della &#13;
Costituzione; &#13;
        che  un'ulteriore  lesione  dell'art. 97  della  Costituzione &#13;
deriverebbe infine, sempre secondo il rimettente, dalla previsione di &#13;
immediata  cessazione  dall'attività  assistenziale,  pur in difetto &#13;
della  previa regolamentazione del residuo di attività assistenziale &#13;
da  svolgersi  a  fini  di didattica, affidata a futuri protocolli di &#13;
intesa tra le regioni e le università; &#13;
        che  è  intervenuto  in entrambi i giudizi il Presidente del &#13;
Consiglio   dei  ministri,  rappresentato  e  difeso  dall'Avvocatura &#13;
generale dello Stato, concludendo per la declaratoria di infondatezza &#13;
della  questione  sulla base delle argomentazioni svolte nell'atto di &#13;
intervento   in   altro  giudizio,  avente  ad  oggetto  la  medesima &#13;
questione, sollevata dallo stesso giudice; &#13;
        che  in  tale  atto,  depositato  in  allegato,  l'Avvocatura &#13;
innanzitutto osserva come la norma delegante di cui all'art. 2, comma &#13;
1,  lett. pp), della legge n. 419 del 1998 espressamente preveda, tra &#13;
i  principi  e  criteri  direttivi,  quello relativo alla definizione &#13;
delle modalità e dei termini di riduzione dell'età pensionabile per &#13;
il personale della dirigenza dell'area medica dipendente dal Servizio &#13;
sanitario   nazionale  nonché,  per  quanto  riguarda  il  personale &#13;
universitario,  della  cessazione  dell'attività  assistenziale, nel &#13;
rispetto del proprio stato giuridico; &#13;
        che,  ad avviso dell'Avvocatura, siffatto principio, trasfuso &#13;
nell'art. 15-nonies  aggiunto al decreto legislativo n. 502 del 1992, &#13;
non creerebbe alcuna ingiustificata discriminazione nell'ambito della &#13;
categoria   dei   docenti   universitari,  stante  la  strumentalità &#13;
dell'attività  assistenziale  rispetto  all'attività di didattica e &#13;
ricerca; &#13;
        che  la disparità di trattamento rispetto agli altri docenti &#13;
universitari sarebbe pertanto giustificata dalla obiettiva diversità &#13;
delle situazioni a confronto, in relazione all'attribuzione o meno di &#13;
funzioni  assistenziali,  nonché  dalla  finalità  di  evitare  una &#13;
disparità di trattamento tra soggetti, quali i docenti "strutturati" &#13;
e   i   primari   ospedalieri,  che  svolgono  le  medesime  funzioni &#13;
assistenziali; &#13;
        che,  per  quanto  riguarda  il  parametro di cui all'art. 76 &#13;
Cost.,  la parte pubblica rileva infine la completezza dei principi e &#13;
criteri  direttivi  contenuti  nella legge delega ed osserva come, in &#13;
tema  di  rapporti  tra potestà legislativa e potestà normativa del &#13;
Governo,  la Costituzione non escluda l'eventualità che un'attività &#13;
normativa  secondaria  possa  legittimamente  integrare e svolgere in &#13;
concreto i contenuti sostanziali previsti dalla normazione primaria. &#13;
    Considerato   che  i  giudizi,  avendo  ad  oggetto  la  medesima &#13;
questione, vanno riuniti per essere congiuntamente decisi; &#13;
        che,  con  sentenza  n. 71 del 2001, questa Corte, esaminando &#13;
identica    questione,    ha    già    dichiarato   l'illegittimità &#13;
costituzionale  dell'art. 15-nonies, comma 2, del decreto legislativo &#13;
30 dicembre  1992,  n. 502  (Riordino  della  disciplina  in  materia &#13;
sanitaria,  a norma dell'art. 1 della legge 23 ottobre 1992, n. 421), &#13;
aggiunto dall'art. 13 del decreto legislativo 19 giugno 1999, n. 229, &#13;
proprio  "nella  parte  in  cui  dispone  la cessazione del personale &#13;
medico  universitario  di cui all'art. 102 del decreto del Presidente &#13;
della  Repubblica  11 luglio  1980,  n. 382,  dallo svolgimento delle &#13;
ordinarie  attività  assistenziali,  nonché  dalla  direzione delle &#13;
strutture assistenziali, al raggiungimento dei limiti massimi di età &#13;
ivi  indicati,  in  assenza della stipula dei protocolli d'intesa tra &#13;
università  e  regioni  previsti  dalla  stessa  norma ai fini della &#13;
disciplina  delle  modalità  e  dei  limiti  per l'utilizzazione del &#13;
suddetto    personale    universitario   per   specifiche   attività &#13;
assistenziali  strettamente  connesse  all'attività  didattica  e di &#13;
ricerca"; &#13;
        che,   pertanto,   la   questione   deve   essere  dichiarata &#13;
manifestamente inammissibile. &#13;
    Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953, &#13;
n. 87,  e  9,  secondo  comma,  delle norme integrative per i giudizi &#13;
innanzi alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Riuniti i giudizi, &#13;
    Dichiara   la   manifesta  inammissibilità  delle  questioni  di &#13;
legittimità  costituzionale  degli  articoli 2,  comma 1, lett. pp), &#13;
della  legge  30 novembre  1998,  n. 419  (Delega  al  Governo per la &#13;
razionalizzazione  del  Servizio sanitario nazionale e per l'adozione &#13;
di  un  testo  unico in materia di organizzazione e funzionamento del &#13;
Servizio  sanitario  nazionale. Modifiche al d.lgs. 30 dicembre 1992, &#13;
n. 502), e 15-nonies del decreto legislativo 30 dicembre 1992, n. 502 &#13;
(Riordino  della disciplina in materia sanitaria, a norma dell'art. 1 &#13;
della  legge  23 ottobre  1992,  n. 421),  aggiunto  dall'art. 13 del &#13;
decreto legislativo 19 giugno 1999, n. 229, sollevate, in riferimento &#13;
agli  articoli  3,  9,  36, 76 e 77 della Costituzione, dal Tribunale &#13;
amministrativo   regionale   della  Campania,  con  le  ordinanze  in &#13;
epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 12 luglio 2001. &#13;
                       Il Presidente: Ruperto &#13;
Il redattore: Marini &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 25 luglio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
