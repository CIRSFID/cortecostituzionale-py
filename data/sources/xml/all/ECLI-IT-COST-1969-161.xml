<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1969</anno_pronuncia>
    <numero_pronuncia>161</numero_pronuncia>
    <ecli>ECLI:IT:COST:1969:161</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>BRANCA</presidente>
    <relatore_pronuncia>Angelo de Marco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>11/12/1969</data_decisione>
    <data_deposito>22/12/1969</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GIUSEPPE BRANCA, Presidente - Prof. &#13;
 MICHELE FRAGALI - Prof. COSTANTINO MORTATI - Prof. GIUSEPPE CHIARELLI - &#13;
 Dott. GIUSEPPE VERZÌ - Dott. GIOVANNI BATTISTA BENEDETTI - Prof. &#13;
 FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI - Dott. ANGELO DE MARCO &#13;
 - Avv. ERCOLE ROCCHETTI - Prof. ENZO CAPALOZZA - Prof. VINCENZO MICHELE &#13;
 TRIMARCHI - Prof. VEZIO CRISAFULLI - Dott. NICOLA REALE - Prof. PAOLO &#13;
 ROSSI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi  riuniti  di  legittimità  costituzionale del D.P.R. 2  &#13;
 ottobre 1960, n.  1378,  recante  norme  sul  trattamento  economico  e  &#13;
 normativo   dei   lavoratori  dipendenti  dalle  imprese  esercenti  la  &#13;
 produzione del cemento,  promossi  con  due  ordinanze  rispettivamente  &#13;
 emesse  il  26  aprile  1968  ed  il  21 febbraio 1969 dal tribunale di  &#13;
 Palermo nei procedimenti civili vertenti  tra  Tilotta  Giuseppe  e  la  &#13;
 ditta Conigliaro e Ghilardi e tra Lo Forti Rosario e la ditta predetta,  &#13;
 iscritte  al  n.  121  del  registro  ordinanze  1968  ed al n. 181 del  &#13;
 Registro ordinanze 1969 e pubblicate  nella  Gazzetta  Ufficiale  della  &#13;
 Repubblica n. 222 del 31 agosto 1968 e n. 152 del 18 giugno 1969.        &#13;
    Visto l'atto di costituzione degli eredi del Tilotta;                 &#13;
    udito nell'udienza pubblica del 26 novembre 1969 il Giudice relatore  &#13;
 Angelo De Marco.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
    Con  atto  di  citazione  in data 20 settembre 1967 Giuseppe Tilotta  &#13;
 conveniva davanti  al  tribunale  di  Palermo  la  ditta  Conigliaro  e  &#13;
 Ghilardi,  esercente  la  produzione  di  cemento,  per il pagamento di  &#13;
 compenso per lavoro  straordinario  e  di  altre  somme,  che  assumeva  &#13;
 dovutegli in dipendenza del rapporto di lavoro intercorso fra le parti.  &#13;
    Il tribunale adito, con ordinanza 26 aprile 1968, rilevato:           &#13;
    che  nessuna  delle  due  parti  aveva  recepito  la  contrattazione  &#13;
 relativa ai lavoratori addetti all'industria del cemento;                &#13;
    che,  pertanto,  alla  controversia  dovevano  essere  applicati  o,  &#13;
 secondo  la  tesi dell'attore, il contratto collettivo nazionale del 24  &#13;
 ottobre 1958 per gli  "intermedi",  o,  secondo  la  tesi  della  ditta  &#13;
 convenuta,  il contratto collettivo nazionale per gli impiegati dell'11  &#13;
 dicembre 1958, entrambi cogenti in forza del  decreto  presidenziale  2  &#13;
 ottobre  1960,  n.  1378,  ed  emanati  entrambi  in  esecuzione  della  &#13;
 legge-delega 14 luglio 1959, n. 741;                                     &#13;
    che sia l'art. 50 del contratto collettivo per  gli  intermedi,  sia  &#13;
 l'art. 44 del contratto collettivo per gli impiegati, sopra richiamati,  &#13;
 dispongono  che  le  controversie  tra  prestatori  e  datori di lavoro  &#13;
 debbono essere sottoposte alle rispettive associazioni sindacali ed, in  &#13;
 caso di mancato accordo, prima di adire  l'autorità  giudiziaria  alle  &#13;
 competenti associazioni sindacali centrali;                              &#13;
    tanto rilevato e ritenuta la rilevanza ai fini del giudizio e la non  &#13;
 manifesta  infondatezza  della questione di legittimità costituzionale  &#13;
 dell'articolo unico del decreto presidenziale 2 ottobre 1960, n.  1378,  &#13;
 nella  parte  in  cui  rende obbligatorio il tentativo di conciliazione  &#13;
 preveduto dall'art. 50 del contratto collettivo  nazionale  24  ottobre  &#13;
 1958  per  gli  "intermedi"  e  dell'art.   44 del contratto collettivo  &#13;
 nazionale  11  dicembre  1958  per  gli   impiegati   delle   industrie  &#13;
 cementifere,  per violazione dell'art. 1 della legge 14 luglio 1959, n.  &#13;
 741, in  relazione  all'articolo  76  della  Costituzione  (eccesso  di  &#13;
 delega)  sollevava  d'ufficio  tale questione e per l'effetto rimetteva  &#13;
 gli atti a questa Corte, sospendendo il procedimento in corso.           &#13;
    La rilevanza viene motivata col rilievo che, in caso di legittimità  &#13;
 della  norma  denunziata,  la  domanda   giudiziale   dovrebbe   essere  &#13;
 dichiarata improponibile.                                                &#13;
    La  non  manifesta  infondatezza viene motivata con il richiamo alla  &#13;
 sentenza di questa Corte  n.  56  del  1965,  con  la  quale  è  stata  &#13;
 dichiarata  l'illegittimità  costituzionale  dell'articolo  unico  del  &#13;
 decreto presidenziale 14 luglio 1960, n. 1032,  per  la  parte  in  cui  &#13;
 rende  obbligatorio  erga  omnes  l'art.  55  del  contratto  nazionale  &#13;
 collettivo di lavoro 24 luglio  1959  per  gli  edili,  ravvisando  una  &#13;
 perfetta identità di situazione col caso in contestazione.              &#13;
    Dopo  le  pubblicazioni,  notificazioni e comunicazioni di legge, il  &#13;
 giudizio viene ora alla cognizione della Corte.                          &#13;
    Si sono costituiti  gli  eredi  dell'attore  Giuseppe  Tilotta,  nel  &#13;
 frattempo  deceduto, il di cui patrocinio, con memoria depositata il 30  &#13;
 luglio  1968,  chiede  che  la  sollevata  questione  venga  dichiarata  &#13;
 fondata,  riproducendo  le  considerazioni contenute nell'ordinanza di.  &#13;
 rinvio.                                                                  &#13;
    Con altra ordinanza in data 21 febbraio 1969, lo stesso tribunale di  &#13;
 Palermo,  nel corso del giudizio promosso da Rosario Lo Forti contro la  &#13;
 medesima  ditta  Conigliaro  e  Ghilardi,  in  relazione  a   richieste  &#13;
 pecuniarie relative a rapporto impiegatizio, ha sollevata, con identica  &#13;
 motivazione,   analoga   questione   di   legittimità  costituzionale,  &#13;
 limitatamente, peraltro, all'art. 44 del contratto nazionale collettivo  &#13;
 11 dicembre 1958 per gli impiegati dell'industria cementizia.            &#13;
    Nel giudizio così instaurato, che viene pure oggi  alla  cognizione  &#13;
 della Corte, non vi è stata costituzione di parti.<diritto>Considerato in diritto</diritto>:                          &#13;
    1.  -  I due giudizi, coma sopra promossi, vanno riuniti, avendo per  &#13;
 oggetto la medesima questione, quella cioè della validità erga  omnes  &#13;
 delle   clausole  di  contratti  collettivi  che  impongano  il  previo  &#13;
 tentativo  di  conciliazione,  sollevata,  nella  specie,   in   ordine  &#13;
 all'articolo  unico  del  D.P.R.  2  ottobre 1960, n. 1378 - emanato in  &#13;
 forza della delega contenuta nell'art. 1 della legge 14 luglio 1959, n.  &#13;
 741 - con il quale è stato conferito valore di legge  alle  norme  sul  &#13;
 trattamento  economico  e  normativo  dei  lavoratori  dipendenti dalle  &#13;
 imprese esercenti la produzione del cemento  ed  amianto-cemento  e  la  &#13;
 produzione promiscua di cemento, calce e gesso, contenute nel contratto  &#13;
 collettivo 24 ottobre 1958.                                              &#13;
    2. - La questione è stata più volte esaminata da questa Corte', in  &#13;
 relazione   a  contratti  collettivi  riguardanti  altre  categorie  di  &#13;
 lavoratori ed è stata risolta (sentenze n. 129 del 1963; n.  45  e  n.  &#13;
 56  del  1966;  n.  9  del  1967;  n.  12  del  1969)  nel  senso della  &#13;
 illegittimità costituzionale di  tali  clausole:  ciò  in  base  alla  &#13;
 considerazione  che  la  delega  conferita al Governo dall'art. 1 della  &#13;
 legge n. 741 del 1959, per la emanazione di norme aventi forza di legge  &#13;
 alle clausole dei contratti collettivi,  stipulati  anteriormente  alla  &#13;
 legge  stessa, trova un preciso limite nel fine di assicurare minimi di  &#13;
 trattamento economico e normativo per tutti  gli  appartenenti  ad  una  &#13;
 determinata categoria; fine dal quale esorbita, con conseguente eccesso  &#13;
 della  delega,  ogni  estensione  a clausole che abbiano per oggetto la  &#13;
 predisposizione  di  procedimenti  e  modalità,  rivestenti  carattere  &#13;
 meramente strumentale rispetto alla disciplina predetta.                 &#13;
    In  conformità  con  la  richiamata costante giurisprudenza analoga  &#13;
 pronunzia  di  incostituzionalità  deve  adottarsi  in   ordine   alle  &#13;
 disposizioni  denunziate  dal  Tribunale di Palermo con le ordinanze di  &#13;
 cui in epigrafe.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
    dichiara la illegittimità costituzionale  dell'articolo  unico  del  &#13;
 decreto  del Presidente della Repubblica 2 ottobre 1960, n. 1378, nella  &#13;
 parte  in  cui  rende  obbligatorio  erga   omnes   il   tentativo   di  &#13;
 conciliazione,   preveduto   dall'art.   50  del  contratto  collettivo  &#13;
 nazionale 24  ottobre  1958  per  gli  intermedi  e  dall'art.  44  del  &#13;
 contratto  collettivo  nazionale  11  dicembre  1958 per gli impiegati,  &#13;
 dipendenti  dalle  imprese  esercenti  la  produzione  del  cemento  ed  &#13;
 amianto-cemento e la produzione promiscua di cemento, calce e gesso.     &#13;
    Così deciso in Roma, nella sede della Corte costituzionale, Palazzo  &#13;
 della Consulta, l'11 dicembre 1969.                                      &#13;
    GIUSEPPE  BRANCA  -  MICHELE FRAGALI - COSTANTINO MORTATI - GIUSEPPE  &#13;
 CHIARELLI - GIUSEPPE  VERZÌ - GIOVANNI BATTISTA BENEDETTI  -  FRANCESCO  &#13;
 PAOLO  BONIFACIO - LUIGI OGGIONI - ANGELO DE MARCO - ERCOLE ROCCHETTI -  &#13;
 ENZO CAPALOZZA - VINCENZO MICHELE TRIMARCHI - VEZIO CRISAFULLI - NICOLA  &#13;
 REALE - PAOLO ROSSI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
