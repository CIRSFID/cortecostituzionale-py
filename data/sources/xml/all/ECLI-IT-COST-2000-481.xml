<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2000</anno_pronuncia>
    <numero_pronuncia>481</numero_pronuncia>
    <ecli>ECLI:IT:COST:2000:481</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>MIRABELLI</presidente>
    <relatore_pronuncia>Fernanda Contri</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/10/2000</data_decisione>
    <data_deposito>08/11/2000</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Cesare MIRABELLI; &#13;
 Giudici: Francesco GUIZZI, Fernando SANTOSUOSSO, Massimo VARI, &#13;
 Cesare RUPERTO, Riccardo CHIEPPA, Gustavo ZAGREBELSKY, Valerio ONIDA, &#13;
 Carlo MEZZANOTTE, Fernanda CONTRI, Guido NEPPI MODONA, Piero Alberto &#13;
 CAPOTOSTI, Annibale MARINI, Franco BILE, Giovanni Maria FLICK;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nei  giudizi  di  legittimità  costituzionale dell'art. 19, comma 2,    &#13;
 lettera  c)  del  decreto  legislativo  25 luglio 1998, n. 286 (Testo    &#13;
 unico  delle disposizioni concernenti la disciplina dell'immigrazione    &#13;
 e   norme  sulla  condizione  dello  straniero),  promossi  con  n. 3    &#13;
 ordinanze  del  tribunale di Vibo Valentia emesse il 22 e il 21 (n. 2    &#13;
 ordinanze) febbraio  2000, iscritte rispettivamente ai nn. 210, 337 e    &#13;
 338 del registro ordinanze 2000 e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica, 1ª serie speciale, nn. 21 e 25 dell'anno 2000.         &#13;
     Visto  l'atto  di  intervento  del  Presidente  del Consiglio dei    &#13;
 Ministri;                                                                &#13;
     Udito  nella  camera  di consiglio del 12 ottobre 2000 il giudice    &#13;
 relatore Fernanda Contri.                                                &#13;
     Ritenuto  che il tribunale di Vibo Valentia, con ordinanza emessa    &#13;
 il   22 febbraio   2000,   ha  sollevato  questione  di  legittimità    &#13;
 costituzionale   dell'art. 19,  comma  2,  lettera  c),  del  decreto    &#13;
 legislativo  25 luglio  1998,  n. 286 (Testo unico delle disposizioni    &#13;
 concernenti  la disciplina dell'immigrazione e norme sulla condizione    &#13;
 dello  straniero),  nella  parte  in  cui  non  prevede il divieto di    &#13;
 espulsione  dello  straniero  convivente more uxorio con un cittadino    &#13;
 italiano, per violazione dell'art. 3 della Costituzione;                 &#13;
         che  il  giudice  rimettente  è  chiamato  a  decidere su un    &#13;
 ricorso avverso un decreto prefettizio di espulsione, proposto da una    &#13;
 cittadina  straniera  priva  del  permesso  di soggiorno, che risulta    &#13;
 convivere more uxorio con un cittadino italiano;                         &#13;
         che  il  rimettente  ritiene  che  la  norma  impugnata violi    &#13;
 l'art. 3  della  Costituzione  poiché,  non prevedendo il divieto di    &#13;
 espulsione  per lo straniero convivente more uxorio con un cittadino,    &#13;
 non  appresterebbe alcuna tutela per i legami di fatto che, avendo la    &#13;
 stessa dignità del matrimonio, sarebbero a questo assimilabili anche    &#13;
 in  base  alla  ratio  della  norma, che sarebbe quella di evitare lo    &#13;
 sradicamento  dello  straniero  dal nucleo familiare in cui egli vive    &#13;
 nello Stato;                                                             &#13;
         che,  sempre  secondo il giudice a quo la convivenza dovrebbe    &#13;
 essere  tutelata  quale "formazione sociale nella quale si esplica la    &#13;
 personalità  umana",  secondo  un'evoluzione  della concezione della    &#13;
 famiglia   di  fatto  cui  anche  numerose  pronunce  della  Consulta    &#13;
 avrebbero accordato "tutela e dignità di trattamento";                  &#13;
         che  è  intervenuto nel giudizio il Presidente del Consiglio    &#13;
 dei  Ministri,  rappresentato e difeso dall'Avvocatura generale dello    &#13;
 Stato, chiedendo che la questione sia dichiarata infondata;              &#13;
         che  la difesa erariale ritiene che la scelta del legislatore    &#13;
 di  tutelare  la sola famiglia di diritto sia pienamente in linea con    &#13;
 quanto  dispone  l'art. 29  Cost.,  poiché la convivenza more uxorio    &#13;
 sarebbe  un  rapporto  privo dei caratteri di stabilità e certezza e    &#13;
 della  reciprocità  e  corrispettività dei diritti e dei doveri dei    &#13;
 coniugi, propri della sola famiglia legittima;                           &#13;
         che lo stesso tribunale di Vibo Valentia ha pronunciato altre    &#13;
 due ordinanze, in diversi procedimenti di opposizione a provvedimenti    &#13;
 prefettizi di espulsione, aventi contenuto identico alla prima.          &#13;
     Considerato  che le tre ordinanze sollevano la medesima questione    &#13;
 di legittimità costituzionale e vanno perciò decise congiuntamente;    &#13;
         che  il  tribunale di Vibo Valentia dubita della legittimità    &#13;
 costituzionale   dell'art. 19,  comma  2,  lettera  c),  del  decreto    &#13;
 legislativo  25 luglio  1998,  n. 286 (Testo unico delle disposizioni    &#13;
 concernenti  la disciplina dell'immigrazione e norme sulla condizione    &#13;
 dello  straniero),  nella  parte  in  cui  non  prevede il divieto di    &#13;
 espulsione  per  lo straniero convivente more uxorio con un cittadino    &#13;
 italiano, ritenendo che la norma violi il principio di eguaglianza di    &#13;
 cui  all'art. 3  della Costituzione, perché creerebbe una disparità    &#13;
 di  trattamento  tra  tale  soggetto  e  lo straniero che convive col    &#13;
 coniuge cittadino italiano;                                              &#13;
         che   questa   Corte   ha  già  esaminato  la  questione  di    &#13;
 legittimità costituzionale della norma oggi impugnata, dichiarandola    &#13;
 manifestamente infondata con l'ordinanza n. 313 del 2000;                &#13;
         che,  in  quell'occasione,  la  Corte  ha  chiarito  che  "la    &#13;
 previsione  del divieto di espulsione solo per lo straniero coniugato    &#13;
 con un cittadino italiano e per lo straniero convivente con cittadini    &#13;
 che  siano  con  lo  stesso  in rapporto di parentela entro il quarto    &#13;
 grado  risponde  all'esigenza  di tutelare, da un lato l'unità della    &#13;
 famiglia,  dall'altro  il vincolo parentale e riguarda persone che si    &#13;
 trovano  in  una  situazione di certezza di rapporti giuridici che è    &#13;
 invece assente nella convivenza more uxorio";                            &#13;
         che  le  ordinanze in esame non recano argomenti nuovi o tali    &#13;
 da  indurre  questa  Corte a modificare il proprio orientamento e che    &#13;
 perciò  anche  le  questioni  qui sollevate devono essere dichiarate    &#13;
 manifestamente infondate.                                                &#13;
     Visti  gli  artt. 26,  secondo  comma, della legge 11 marzo 1953,    &#13;
 n. 87  e  9,  secondo  comma,  delle  norme integrative per i giudizi    &#13;
 davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                                                                          &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
     Riuniti i giudizi,                                                   &#13;
     Dichiara   la   manifesta   infondatezza   delle   questioni   di    &#13;
 legittimità  costituzionale  dell'art. 19,  comma 2, lettera c), del    &#13;
 decreto   legislativo  25 luglio  1998,  n. 286  (Testo  unico  delle    &#13;
 disposizioni  concernenti  la  disciplina  dell'immigrazione  e norme    &#13;
 sulla   condizione   dello   straniero),  sollevate,  in  riferimento    &#13;
 all'art. 3  della Costituzione, dal tribunale di Vibo Valentia con le    &#13;
 ordinanze indicate in epigrafe.                                          &#13;
     Così  deciso  in  Roma,  nella  sede della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 25 ottobre 2000.                              &#13;
                       Il Presidente: Mirabelli                           &#13;
                         Il redattore: Contri                             &#13;
                       Il cancelliere: Di Paola                           &#13;
     Depositata in cancelleria l'8 novembre 2000.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
