<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1996</anno_pronuncia>
    <numero_pronuncia>168</numero_pronuncia>
    <ecli>ECLI:IT:COST:1996:168</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>FERRI</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/05/1996</data_decisione>
    <data_deposito>24/05/1996</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: avv. Mauro FERRI; &#13;
 Giudici: prof. Luigi MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, &#13;
 prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. Cesare &#13;
 MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, dott. &#13;
 Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, &#13;
 prof. Valerio ONIDA, prof. Carlo MEZZANOTTE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel  giudizio  di  legittimità  costituzionale dell'art. 52, secondo    &#13;
 comma, lettera b), del d.P.R. 29 settembre 1973, n. 602 (Disposizioni    &#13;
 sulla riscossione delle imposte sul reddito) promosso  con  ordinanza    &#13;
 emessa  il  25 gennaio 1995 dal tribunale di Pistoia nel procedimento    &#13;
 civile  vertente  tra  Cristina  Stefania   Lubrani   e   l'Esattoria    &#13;
 consorziale  di  Pescia  ed  altra,  iscritta  al n. 544 del registro    &#13;
 ordinanze 1995 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 40, prima serie speciale, dell'anno 1995;                             &#13;
   Udito nella camera di  consiglio  del  26  marzo  1996  il  giudice    &#13;
 relatore Cesare Mirabelli;                                               &#13;
   Ritenuto che, con ordinanza emessa il 25 gennaio 1995, il tribunale    &#13;
 di  Pistoia  ha  sollevato,  in  riferimento agli artt. 3 e 24, primo    &#13;
 comma, della Costituzione, questione di  legittimità  costituzionale    &#13;
 dell'art.  52,  secondo  comma,  lettera  b), del d.P.R. 29 settembre    &#13;
 1973, n.  602  (Disposizioni  sulla  riscossione  delle  imposte  sul    &#13;
 reddito),  nella  parte in cui non consente al coniuge, ai parenti ed    &#13;
 agli  affini  fino  al  terzo  grado  del  contribuente  di  proporre    &#13;
 l'opposizione  di  terzo  all'esecuzione, prevista dall'art. 619 cod.    &#13;
 proc. civ., per i mobili  pignorati  nella  casa  di  abitazione  del    &#13;
 debitore;                                                                &#13;
     che   la   questione  di  legittimità  costituzionale  è  stata    &#13;
 sollevata nel corso di un  giudizio  promosso  da  Cristina  Stefania    &#13;
 Lubrani  per  opporsi al pignoramento di mobili ad essa venduti dalla    &#13;
 madre  ed  a  quest'ultima  contestualmente  concessi  in   comodato;    &#13;
 pignoramento  effettuato  dall'Esattoria  consorziale di Pescia nella    &#13;
 casa di abitazione della contribuente, madre  della  ricorrente,  per    &#13;
 riscuotere coattivamente imposte non pagate;                             &#13;
     che, ad avviso del giudice rimettente, la disposizione denunciata    &#13;
 configurerebbe  un'ipotesi  di  responsabilità  del terzo per debito    &#13;
 fiscale altrui,  dettando  una  disciplina  irrazionale  e  priva  di    &#13;
 giustificazione  nella  parte  in  cui  differenzia questa ipotesi da    &#13;
 altri casi di responsabilità per  debito  altrui  che  l'ordinamento    &#13;
 prevede  in considerazione della particolare localizzazione del bene;    &#13;
 difatti, mentre in questi casi la  presunzione  di  appartenenza  del    &#13;
 bene  al debitore può essere in vario modo superata, nell'esecuzione    &#13;
 esattoriale, invece, il coniuge, il parente o l'affine non potrebbero    &#13;
 vincere la presunzione di appartenenza del bene al  debitore  con  lo    &#13;
 strumento processuale dell'opposizione di terzo, sicché risulterebbe    &#13;
 anche  menomata la garanzia costituzionale di poter agire in giudizio    &#13;
 a tutela del proprio diritto (art. 24, primo comma, Cost.);              &#13;
   Considerato  che  la  questione  di  legittimità   costituzionale,    &#13;
 coinvolgendo l'intera disciplina dettata dall'art. 52, secondo comma,    &#13;
 lettera  b), del d.P.R. 29 settembre 1973, n. 602, mira ad equiparare    &#13;
 del tutto la posizione del coniuge, dei parenti e degli  affini  fino    &#13;
 al  terzo  grado  a  quella  dei terzi estranei al debitore: essa non    &#13;
 investe, quindi, l'ampiezza dei limiti  che  la  stessa  disposizione    &#13;
 già  pone  alla  regola  della  preclusione  dell'opposizione per le    &#13;
 persone legate da un particolare vincolo al contribuente, ma tende  a    &#13;
 travolgere la regola stessa, finendo in tal modo con l'escludere ogni    &#13;
 pur ragionevole limitazione alla facoltà di proporre opposizione;       &#13;
     che,  prospettata con tale ampiezza ed in riferimento ai medesimi    &#13;
 parametri,  la  questione  è  già  stata  dichiarata  non   fondata    &#13;
 (sentenza  n.  444 del 1995), giacché il legislatore può perseguire    &#13;
 l'esigenza  della  tempestiva  realizzazione  dei  crediti  tributari    &#13;
 mediante  l'espropriazione di beni mobili che, per il luogo in cui si    &#13;
 trovano, si presume siano del contribuente  moroso,  e  può  inoltre    &#13;
 porre  ragionevoli  limitazioni  sia  alla  prova  contraria  a  tale    &#13;
 presunzione sia all'ampiezza dell'opposizione prevista  per  i  terzi    &#13;
 che  assumono di essere proprietari di quei beni (sentenza n. 358 del    &#13;
 1994); limitazioni e preclusioni che, al fine di evitare  fraudolente    &#13;
 elusioni  nell'adempimento del debito tributario, possono essere più    &#13;
 rigorose per le persone legate al  debitore  da  particolari  vincoli    &#13;
 (coniugio,  parentela  o  affinità)  rispetto  a quelle previste per    &#13;
 altre persone, estranee al debitore;                                     &#13;
     che, quanto alla garanzia del diritto di agire  in  giudizio,  la    &#13;
 norma  detta  una  disciplina  che appartiene agli aspetti di diritto    &#13;
 sostanziale  e  non  riguarda  la  difesa  processuale  (così,   con    &#13;
 riferimento  alla  precedente, ma del tutto analoga, disciplina della    &#13;
 riscossione delle imposte sul reddito, sentenze n. 107 del  1969,  n.    &#13;
 129 del 1968 e n. 42 del 1964);                                          &#13;
     che, pertanto, la questione deve essere dichiarata manifestamente    &#13;
 infondata;                                                               &#13;
   Visti  gli  artt.  26, secondo comma, della legge 11 marzo 1953, n.    &#13;
 87 e 9, secondo comma, delle norme integrative per i giudizi  davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  la manifesta infondatezza della questione di legittimità    &#13;
 costituzionale dell'art. 52, secondo comma, lettera  b),  del  d.P.R.    &#13;
 29  settembre  1973,  n.  602  (Disposizioni  sulla riscossione delle    &#13;
 imposte sul reddito), sollevata, in riferimento agli artt.  3  e  24,    &#13;
 primo  comma,  della  Costituzione,  dal  tribunale  di  Pistoia  con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 16 maggio 1996.                               &#13;
                         Il Presidente: Ferri                             &#13;
                        Il redattore: Mirabelli                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 24 maggio 1996.                           &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
