<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>421</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:421</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>16/12/1997</data_decisione>
    <data_deposito>18/12/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, dott. &#13;
 Riccardo CHIEPPA, prof. Gustavo ZAGREBELSKY, prof. Valerio ONIDA, &#13;
 prof. Carlo MEZZANOTTE, avv. Fernanda CONTRI, prof. Guido NEPPI &#13;
 MODONA, prof. Piero Alberto CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 96 e seguenti    &#13;
 del codice di procedura penale, promosso  con  ordinanza  emessa  l'8    &#13;
 gennaio 1997 dal pretore di Roma, sezione distaccata di Frascati, nel    &#13;
 procedimento  penale  a  carico di Grossi Giuliana, iscritta al n. 85    &#13;
 del registro ordinanze 1997 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 10, prima serie speciale, dell'anno 1997;            &#13;
   Visto  l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito nella camera di consiglio del  29  ottobre  1997  il  giudice    &#13;
 relatore Guido Neppi Modona;                                             &#13;
   Ritenuto che il pretore di Roma, sezione distaccata di Frascati, ha    &#13;
 sollevato  questione di legittimità costituzionale degli artt.  96 e    &#13;
 seguenti del codice di procedura penale, assumendo che  il  complesso    &#13;
 normativo   in  questione,  prevedendo  per  l'imputato  l'assistenza    &#13;
 obbligatoria ad opera di un difensore abilitato  all'esercizio  della    &#13;
 professione  forense,  sarebbe  in  contrasto  con  l'art.  10  della    &#13;
 Costituzione, in relazione all'art. 6, lettera c), della  Convenzione    &#13;
 per la salvaguardia dei diritti dell'uomo (adottata a Strasburgo il 4    &#13;
 novembre  1950,  ratificata e resa esecutiva con legge 4 agosto 1955,    &#13;
 n. 848), e con l'art. 24 della Costituzione;                             &#13;
     che ad avviso del rimettente l'art. 6, lettera c), della predetta    &#13;
 Convenzione, nel prevedere  che  ciascun  imputato  abbia  diritto  a    &#13;
 difendersi  personalmente  o  con  l'assistenza  di  difensore di sua    &#13;
 scelta,  implica  il  riconoscimento  del  diritto  dell'imputato   a    &#13;
 difendersi anche solo personalmente;                                     &#13;
     che,  parimenti,  secondo  il  giudice  a  quo,  l'art.  24 della    &#13;
 Costituzione, nell'affermare il principio che tutti possono agire  in    &#13;
 giudizio  a  difesa dei propri diritti e interessi legittimi e che la    &#13;
 difesa è diritto inviolabile in ogni stato e grado del procedimento,    &#13;
 non subordina l'esercizio di tale diritto alla necessaria  assistenza    &#13;
 dell'imputato ad opera di un difensore abilitato;                        &#13;
     che  si  è  costituito il Presidente del Consiglio dei Ministri,    &#13;
 rappresentato  e  difeso  dall'Avvocatura   generale   dello   Stato,    &#13;
 chiedendo che la questione sia dichiarata manifestamente infondata;      &#13;
     che    l'Avvocatura,   richiamate   le   pronunce   della   Corte    &#13;
 costituzionale con le  quali  è  stata  dichiarata  l'illegittimità    &#13;
 costituzionale   delle   disposizioni  che  limitavano  l'obbligo  di    &#13;
 nominare un difensore di ufficio all'imputato sprovvisto di difensore    &#13;
 di fiducia, rileva che la difesa tecnica, oltre ad essere un  diritto    &#13;
 costituzionalmente  protetto,  è  nella  gran  parte  dei  casi  una    &#13;
 necessità;                                                              &#13;
     che  inoltre,  secondo  l'Avvocatura,   le   disposizioni   della    &#13;
 Convenzione  per la salvaguardia dei diritti dell'uomo, avendo valore    &#13;
 di  legge  ordinaria,   non   possono   porsi   come   parametri   di    &#13;
 costituzionalità,  né  essere  invocate nell'ambito di operatività    &#13;
 dell'art. 10 della Costituzione;                                         &#13;
     che, comunque, il diritto alla difesa  personale  costituisce  la    &#13;
 "regola  minima"  per un giusto processo e che il sistema processuale    &#13;
 vigente,  nel  garantire   congiuntamente   la   difesa   tecnica   e    &#13;
 l'autodifesa,  non  si porrebbe in contrasto con i principi affermati    &#13;
 dalla Convenzione, ma ne costituirebbe la massima realizzazione;         &#13;
   Considerato che questa Corte ha già dichiarato  l'infondatezza  di    &#13;
 analoghe  censure mosse alla corrispondente disciplina del diritto di    &#13;
 difesa  contenuta  nel  codice  di  procedura  penale  del  1930,  in    &#13;
 relazione   agli   stessi   parametri   costituzionali   evocati  dal    &#13;
 ricorrente;                                                              &#13;
     che in particolare, in relazione all'art. 24 della  Costituzione,    &#13;
 la  Corte  ha  rilevato  che  la  presenza  del  difensore  "risponde    &#13;
 all'aspirazione a fondare l'intero processo penale sopra un effettivo    &#13;
 contraddittorio tra accusa e difesa" e che "nessuno ha mai dubitato o    &#13;
 dubita  che  alla  specifica  capacità  professionale  del  pubblico    &#13;
 ministero  fosse e sia ragionevole contrapporre quella di un soggetto    &#13;
 di pari qualificazione che affianchi ed assista l'imputato" (sentenza    &#13;
 n. 125 del 1979);                                                        &#13;
     che su un diverso terreno rispetto alla difesa tecnica si colloca    &#13;
 il  parallelo  diritto  all'autodifesa,  operante   nell'ambito   del    &#13;
 principio  del  contraddittorio,  con riferimento ad un "complesso di    &#13;
 attività,  mediante  le  quali  l'imputato,  come  protagonista  del    &#13;
 processo  penale,  ha  facoltà  di  eccitarne lo sviluppo dialettico    &#13;
 contribuendo  all'acquisizione  delle  prove  ed  al   controllo   di    &#13;
 legalità  del  suo svolgimento" (sentenza n. 186 del 1973), sì che,    &#13;
 sotto questo profilo, ai fini  del  rispetto  dell'art.  24,  secondo    &#13;
 comma,  della  Costituzione, rileva che all'imputato sia garantita la    &#13;
 possibilità di intervenire in ogni stato e  grado  del  procedimento    &#13;
 (sentenze n. 280 del 1985 e n. 9 del 1982);                              &#13;
     che,  d'altro canto, "l'imposizione all'imputato di un difensore,    &#13;
 persino suo malgrado, mira ad assicurargli quelle cognizioni  tecnico    &#13;
 giuridiche,   quell'esperienza   processuale   e   quella  distaccata    &#13;
 serenità, che gli consentono di valutare adeguatamente le situazioni    &#13;
 di causa, in  guisa  da  tutelare  la  sua  più  ampia  libertà  di    &#13;
 determinazione  nella  scelta  delle  iniziative  e dei comportamenti    &#13;
 processuali" (sentenza n. 498 del 1989);                                 &#13;
     che, per  quanto  concerne  la  censura  sollevata  in  relazione    &#13;
 all'art.    6  della  Convenzione  per  la  salvaguardia  dei diritti    &#13;
 dell'uomo,  con  riferimento  all'art.  10  della  Costituzione,   va    &#13;
 ribadito  che  il  richiamo  alle  "norme  del diritto internazionale    &#13;
 generalmente  riconosciute"  ai  fini  dell'adeguamento  del  diritto    &#13;
 interno  si  riferisce  soltanto  alle norme internazionali di natura    &#13;
 consuetudinaria  e  non a quelle di natura pattizia (vedi ex plurimis    &#13;
 sentenze nn. 288 del 1997, 15 e 146 del 1996);                           &#13;
     che, comunque, la disposizione  di  cui  all'art.  6,  numero  3,    &#13;
 lettera  c)  della  Convenzione,  concorrendo  alla  definizione  del    &#13;
 "giusto processo", fondato, tra l'altro, sulla parità delle armi, va    &#13;
 interpretata  nel  senso  che,  "il  diritto  all'autodifesa  non  è    &#13;
 assoluto,  ma limitato dal diritto dello Stato interessato ad emanare    &#13;
 disposizioni  concernenti  la  presenza  di   avvocati   davanti   ai    &#13;
 tribunali" (sentenza n. 188 del 1980);                                   &#13;
     che  a  maggior  ragione  nel codice di procedura penale vigente,    &#13;
 ispirato ai principi del sistema accusatorio, le norme che assicurano    &#13;
 la difesa tecnica sono funzionali alla realizzazione  di  un  "giusto    &#13;
 processo",  garantendo  l'effettività  di  un  contraddittorio  più    &#13;
 equilibrato e una più sostanziale parità delle armi  tra  accusa  e    &#13;
 difesa;                                                                  &#13;
     che  la  questione deve essere pertanto dichiarata manifestamente    &#13;
 infondata in relazione ad entrambi i parametri evocati;                  &#13;
   Visti  gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  degli  artt.  96  e  seguenti del codice di procedura    &#13;
 penale,  sollevata,  in  riferimento  agli  artt.  10  e   24   della    &#13;
 Costituzione,  dal  pretore  di Roma, sezione distaccata di Frascati,    &#13;
 con l'ordinanza in epigrafe.                                             &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 16 dicembre 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                      Il redattore:  Neppi Modona                         &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 18 dicembre 1997.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
