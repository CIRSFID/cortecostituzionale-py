<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1981</anno_pronuncia>
    <numero_pronuncia>89</numero_pronuncia>
    <ecli>ECLI:IT:COST:1981:89</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMADEI</presidente>
    <relatore_pronuncia>Livio Paladin</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>08/04/1981</data_decisione>
    <data_deposito>01/06/1981</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Avv. LEONETTO AMADEI, Presidente - Dott. &#13;
 GIULIO GIONFRTDA - Prof. EDOARDO VOLTERRA - Dott. MICHELE ROSSANO - &#13;
 Prof. ANTONINO DE STEFANO - Prof. LEOPOLDO ELIA - Prof. GUGLIELMO &#13;
 ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI DUCCI - &#13;
 Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Prof. &#13;
 GIUSEPPE FERRARI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt.   1,  &#13;
 4,  7,  primo,  secondo  e  quarto comma, e 11 del d.P.R.  29 settembre  &#13;
 1973, n. 599 (istituzione dell'imposta locale   sui redditi),  promossi  &#13;
 con  sette  ordinanze  emesse  il  12    giugno  1979 dalla Commissione  &#13;
 tributaria di 1 grado di  Pistoia, iscritte ai nn. da  844  a  850  del  &#13;
 registro  ordinanze    1980 e pubblicate nella Gazzetta Ufficiale della  &#13;
 Repubblica n. 41 del 1981;                                               &#13;
     udito nella camera di consiglio dell'8  aprile  1981  il    Giudice  &#13;
 relatore Livio Paladin.                                                  &#13;
     Ritenuto che la Commissione tributaria di primo grado di Pistoia ha  &#13;
 sollevato - con sette ordinanze emesse il 12  giugno 1979 (ma pervenute  &#13;
 alla   Corte  il  27  novembre    1980)  -  questione  di  legittimità  &#13;
 costituzionale degli artt.  1, 4, 7, primo, secondo e quarto  comma,  e  &#13;
 11  del  d.P.R.    29  settembre  1973, n. 599, istitutivo dell'imposta  &#13;
 locale    sui  redditi,  in  riferimento  agli  artt.  3  e  53   della  &#13;
 Costituzione.                                                            &#13;
     Considerato  che  i  sette  giudizi  devono  essere  riuniti,  data  &#13;
 l'identità delle questioni così prospettate;                           &#13;
     che la Corte si  è già pronunciata in proposito, con la  sentenza  &#13;
 n.   42   del      1980:   la   quale  ha  dichiarato,  da  una  parte,  &#13;
 "l'illegittimità   costituzionale dell'art. 4, n.  1,  della  legge  9  &#13;
 ottobre  1971,    n.  825,  e dell'art. 1, secondo comma, del d.P.R. 29  &#13;
 settembre 1973, n. 599, in quanto non escludono i redditi    di  lavoro  &#13;
 autonomo,   che   non   siano  assimilabili  ai  redditi     d'impresa,  &#13;
 dall'imposta locale sui  redditi";  ed  ha  invece  precisato,  d'altra  &#13;
 parte,  che  non  si  rende  necessario   pronunciare il corrispondente  &#13;
 annullamento dell'art. 7 del  citato decreto presidenziale, "poiché la  &#13;
 disciplina delle  deduzioni a favore dei lavoratori autonomi è resa  a  &#13;
 sua  volta inoperante, circa i rapporti ai quali non possa più  essere  &#13;
 applicato l'art. 1, già in forza della dichiarazione  d'illegittimità  &#13;
 parziale  della  disciplina  riguardante  il  presupposto  dell'imposta  &#13;
 locale sui redditi";                                                     &#13;
     che, per   converso, le  impugnative  degli  artt.  4  (sulla  base  &#13;
 imponibile  dell'imposta  in esame) ed 11 (sul luogo di  produzione dei  &#13;
 redditi) del d.P.R. n.   599 del 1973 non  sono    sorrette  da  alcuna  &#13;
 motivazione,  concernente  sia  la    rilevanza  sia  la  non manifesta  &#13;
 infondatezza delle  questioni medesime; e che, in ogni caso, i disposti  &#13;
 concernenti i redditi di lavoro  autonomo,  contenuti    nell'art.  11,  &#13;
 primo  comma,  n.  2  e n. 3, sono anch'essi   ormai inoperanti, per le  &#13;
 stesse ragioni già svolte quanto   all'art.  7  del  predetto  decreto  &#13;
 presidenziale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     1)   dichiara   la   manifesta   infondatezza  della  questione  di  &#13;
 legittimità costituzionale degli artt. 1 e 7, primo, secondo  e quarto  &#13;
 comma, del d.P.R. 29 settembre 1973, n. 599  (sollevata in  riferimento  &#13;
 agli  artt. 3 e 53 Cost., con le  ordinanze indicate in epigrafe), già  &#13;
 decisa con sentenza n.  42 del 1980;                                     &#13;
     2) dichiara la  manifesta  inammissibilità  della  questione    di  &#13;
 legittimità  costituzionale degli artt. 4 ed 11 del d.P.R.  n. 599 del  &#13;
 1973, sollevata in riferimento agli  artt.  3  e  53    Cost.,  con  le  &#13;
 ordinanze indicate in epigrafe.                                          &#13;
     Così  deciso  in  Roma,  in camera di consiglio, nella sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, l'8  aprile 1981.          &#13;
                                   F.to:  LEONETTO   AMADEI   -   GIULIO  &#13;
                                   GIONFRIDA   -   EDOARDO   VOLTERRA  -  &#13;
                                   MICHELE  ROSSANO     -  ANTONINO   DE  &#13;
                                   STEFANO GUGLIELMO  ROEHRSSEN - ORONZO  &#13;
                                   REALE - BRUNETTO  BUCCIARELLI DUCCI -  &#13;
                                   AEBERTO MALAGUGINI -  LIVIO PALADIN -  &#13;
                                   ARNALDO   MACCARONE   -   ANTONIO  LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE  FERRARI.                     &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
