<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1997</anno_pronuncia>
    <numero_pronuncia>331</numero_pronuncia>
    <ecli>ECLI:IT:COST:1997:331</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Carlo Mezzanotte</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>10/11/1997</data_decisione>
    <data_deposito>14/11/1997</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, prof. &#13;
 Guido NEPPI MODONA, prof. Piero Alberto CAPOTOSTI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nei giudizi di legittimità costituzionale dell'art. 34, comma 2, del    &#13;
 codice  di procedura penale, promossi con n. 2 ordinanze emesse il 23    &#13;
 maggio 1996 dal tribunale di  Siracusa  e  il  17  gennaio  1997  dal    &#13;
 pretore  di  Bassano  del  Grappa,  iscritte  ai nn. 976 del registro    &#13;
 ordinanze 1996 e 186 del registro ordinanze 1997 e  pubblicate  nella    &#13;
 Gazzetta  Ufficiale  della  Repubblica  nn. 41, prima serie speciale,    &#13;
 dell'anno 1996 e 16, prima serie speciale, dell'anno 1997;               &#13;
    Visto l'atto  di  intervento  del  Presidente  del  Consiglio  dei    &#13;
 Ministri;                                                                &#13;
   Udito  nella  camera  di  consiglio  del  18 giugno 1997 il giudice    &#13;
 relatore Carlo Mezzanotte.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1. - Con ordinanza del 23 maggio 1996 il tribunale di  Siracusa  ha    &#13;
 sollevato  questione  di  legittimità  costituzionale  dell'art. 34,    &#13;
 comma 2, del codice di procedura penale, per contrasto con gli  artt.    &#13;
 3,  24, secondo comma, e 27, secondo comma, della Costituzione, nella    &#13;
 parte in cui non prevede l'incompatibilità a svolgere le funzioni di    &#13;
 giudice del dibattimento per il giudice per le  indagini  preliminari    &#13;
 che  abbia adottato la misura cautelare della custodia in carcere nei    &#13;
 confronti di un correo dell'imputato sottoposto al suo  giudizio  per    &#13;
 il medesimo reato.                                                       &#13;
   Ad  avviso  del  remittente,  l'accertamento  della sussistenza dei    &#13;
 gravi indizi di  colpevolezza  in  ordine  al  medesimo  reato,  già    &#13;
 compiuto,  quale  giudice per le indagini preliminari, dal presidente    &#13;
 del collegio,  pur  se  relativo  a  soggetto  diverso  dall'imputato    &#13;
 sottoposto  al  giudizio del tribunale, postulerebbe "una preliminare    &#13;
 valutazione degli elementi costitutivi della fattispecie  concorsuale    &#13;
 necessariamente  riferiti  a  tutti i concorrenti" e tale valutazione    &#13;
 non potrebbe non riflettersi sulla  serenità  ed  imparzialità  del    &#13;
 giudice   con   violazione   dei   principii   di   eguaglianza,   di    &#13;
 inviolabilità della difesa in ogni stato e grado del procedimento  e    &#13;
 di presunzione di non colpevolezza.                                      &#13;
   2.  -  Con  ordinanza del 17 gennaio 1997 il pretore di Bassano del    &#13;
 Grappa  ha  sollevato  questione   di   legittimità   costituzionale    &#13;
 dell'art.  34, comma 2, del codice di procedura penale, per contrasto    &#13;
 con  gli  artt.  3  e  24  della Costituzione, nella parte in cui non    &#13;
 prevede l'incompatibilità ad esercitare le funzioni di  giudice  del    &#13;
 dibattimento  per  il  giudice  che  abbia  respinto  la richiesta di    &#13;
 applicazione della pena avanzata ai sensi dell'art.  444  cod.  proc.    &#13;
 pen.  da  un  correo  degli imputati sottoposti al suo giudizio per i    &#13;
 medesimi reati.                                                          &#13;
   Ad  avviso  del  giudice  a  quo  nel  respingere  la  richiesta di    &#13;
 applicazione della  pena  avanzata  dal  correo,  egli  avrebbe  già    &#13;
 compiuto  "una valutazione non formale delle risultanze del fascicolo    &#13;
 del pubblico ministero", con particolare riferimento all'esistenza  o    &#13;
 meno  di  argomenti  per  l'eventuale  suo  proscioglimento, e questa    &#13;
 valutazione non avrebbe  potuto  prescindere  dall'esame  incidentale    &#13;
 anche  delle  posizioni  dei coimputati attualmente sottoposti al suo    &#13;
 giudizio. "La formazione inevitabile di un  convincimento,  sia  pure    &#13;
 sommario, anche nei confronti dei coimputati" violerebbe il principio    &#13;
 del  giusto  processo  "in  parallelismo  con la situazione esaminata    &#13;
 dalla Corte costituzionale con sentenza n. 371 del 1996".                &#13;
   2.1. - In quest'ultimo giudizio è intervenuto  il  Presidente  del    &#13;
 Consiglio   dei  Ministri,  rappresentato  e  difeso  dall'Avvocatura    &#13;
 generale dello Stato,  chiedendo  che  la  questione  sia  dichiarata    &#13;
 infondata.                                                               &#13;
   Secondo  l'Avvocatura,  nella  sentenza  n.  371 del 1996 di questa    &#13;
 Corte è espressamente richiesto che  la  precedente  valutazione  di    &#13;
 responsabilità, fonte della causa di incompatibilità, sia contenuta    &#13;
 "in un provvedimento avente forma di sentenza", condizione che non si    &#13;
 sarebbe  verificata  nel  caso di specie, trattandosi di reiezione di    &#13;
 richiesta di applicazione della pena.                                    &#13;
   Inoltre, ad avviso dell'Avvocatura, il rigetto della  richiesta  di    &#13;
 applicazione della pena sarebbe stato deciso dal giudice nella stessa    &#13;
 data  in cui ha sollevato la questione di legittimità costituzionale    &#13;
 e  prima  dell'apertura   del   dibattimento:   conseguentemente   si    &#13;
 verserebbe  nella  stessa  fase  processuale  e, in base ai principii    &#13;
 affermati da questa  Corte  nella  sentenza  n.  177  del  1996,  non    &#13;
 sussisterebbero ragioni di incompatibilità.<diritto>Considerato in diritto</diritto>1. - Il tribunale di Siracusa, nel corso di un procedimento penale,    &#13;
 ha  sollevato  questione di legittimità costituzionale dell'art. 34,    &#13;
 comma 2, del codice di procedura penale, per contrasto con gli  artt.    &#13;
 3,  24, secondo comma, e 27, secondo comma, della Costituzione, nella    &#13;
 parte in cui non prevede l'incompatibilità a svolgere le funzioni di    &#13;
 giudice del dibattimento per il giudice per le  indagini  preliminari    &#13;
 che  abbia adottato la misura della custodia cautelare in carcere nei    &#13;
 confronti di un correo dell'imputato sottoposto al suo  giudizio  per    &#13;
 il medesimo reato.                                                       &#13;
   Il  giudice  remittente  asserisce  di  avere  già  compiuto  "una    &#13;
 preliminare valutazione degli elementi costitutivi della  fattispecie    &#13;
 concorsuale  necessariamente riferiti a tutti i concorrenti": di qui,    &#13;
 a  suo  avviso,  il  pregiudizio  che  deriverebbe  dalla  precedente    &#13;
 ordinanza  di custodia cautelare in carcere contenente l'accertamento    &#13;
 dei gravi indizi di colpevolezza in ordine al  medesimo  reato  e  la    &#13;
 conseguente violazione del principio del giusto processo nei riguardi    &#13;
 dell'imputato.                                                           &#13;
   Il  pretore  di  Bassano  del  Grappa, nel corso di un procedimento    &#13;
 penale,  ha  sollevato  questione  di   legittimità   costituzionale    &#13;
 dell'art.  34, comma 2, del codice di procedura penale, per contrasto    &#13;
 con  gli  artt.  3  e  24  della Costituzione, nella parte in cui non    &#13;
 prevede l'incompatibilità a svolgere  le  funzioni  di  giudice  del    &#13;
 dibattimento  per  il  giudice  che  abbia  respinto  la richiesta di    &#13;
 applicazione della pena avanzata ai sensi dell'art.  444  cod.  proc.    &#13;
 pen.  da  un  correo  degli imputati sottoposti al suo giudizio per i    &#13;
 medesimi reati.                                                          &#13;
   Il  giudice a quo sostiene di avere già effettuato, nel respingere    &#13;
 la richiesta di applicazione della pena  avanzata  dal  correo,  "una    &#13;
 valutazione  non  formale delle risultanze del fascicolo del pubblico    &#13;
 ministero", con particolare riferimento  alla  esistenza  o  meno  di    &#13;
 argomenti  per  il suo eventuale proscioglimento: tale valutazione, a    &#13;
 suo avviso, non aveva potuto prescindere dall'esame incidentale delle    &#13;
 posizioni dei coimputati sottoposti al suo giudizio.  "La  formazione    &#13;
 inevitabile  di  un  convincimento,  sia  pure  sommario,  anche  nei    &#13;
 confronti dei coimputati" determinerebbe, secondo il giudice a quo la    &#13;
 vulnerazione del principio del giusto processo "in  parallelismo  con    &#13;
 la  situazione  esaminata  dalla Corte costituzionale con sentenza n.    &#13;
 371 del 1996".                                                           &#13;
   Poiché le ordinanze di rimessione hanno  ad  oggetto  la  medesima    &#13;
 disposizione e pongono questioni analoghe, i relativi giudizi possono    &#13;
 essere riuniti per essere decisi con unica sentenza.                     &#13;
   2. - Le questioni sono inammissibili.                                  &#13;
   Quale che sia l'esatto contenuto delle valutazioni che il tribunale    &#13;
 di  Siracusa  e  il  pretore di Bassano del Grappa dichiarano di aver    &#13;
 effettuato rispettivamente nell'ordinanza  di  custodia  cautelare  e    &#13;
 nell'ordinanza  di  reiezione  della  richiesta di applicazione della    &#13;
 pena, e pur nell'ipotesi in cui tali  valutazioni  si  siano  risolte    &#13;
 nella  manifestazione  del  convincimento della responsabilità anche    &#13;
 nei confronti dei soggetti imputati nei  giudizi  a  quibus  si  deve    &#13;
 rilevare  che  questa  Corte  ha  già individuato nelle sentenze nn.    &#13;
 306, 307 e 308 del 1997 i limiti  entro  i  quali  il  principio  del    &#13;
 giusto   processo   postula   la   previsione   di   una  ipotesi  di    &#13;
 incompatibilità.                                                        &#13;
   Quando il  pregiudizio  alla  terzietà  del  giudice  provenga  da    &#13;
 funzioni  esercitate  all'interno di un medesimo procedimento penale,    &#13;
 il  pregiudizio  stesso   è   prevenibile:   accanto   alla   tutela    &#13;
 ripristinatoria  rimessa all'iniziativa del giudice e delle parti con    &#13;
 gli  appositi  strumenti  dell'astensione  e  della  ricusazione   è    &#13;
 pertanto esigibile anche una tutela preventiva da attuarsi attraverso    &#13;
 mezzi  organizzativi in grado di assicurare uno svolgimento spontaneo    &#13;
 del  principio   del   giusto   processo   (a   questo   tendono   le    &#13;
 incompatibilità  ex  art.  34  cod.  proc.    pen.).  Tale  onere di    &#13;
 organizzazione preventiva grava altresì  sull'amministrazione  della    &#13;
 giustizia  penale  nelle  ipotesi  in cui, in una vicenda processuale    &#13;
 sostanzialmente unitaria pur trattandosi di procedimenti diversi,  il    &#13;
 pregiudizio  derivi da una sentenza penale dalla quale emerga un già    &#13;
 maturato convincimento del giudice  in  ordine  alla  responsabilità    &#13;
 penale  di  una  persona  formalmente  non  imputata in quel processo    &#13;
 (sentenza  n.  371  del  1996).  È  questo   l'estremo   limite   di    &#13;
 esigibilità  di  una tutela organizzativa e preventiva. Varcato tale    &#13;
 limite, "nella varietà delle relazioni che possono  instaurarsi  tra    &#13;
 procedimenti  distinti,  e  nella  molteplicità  dei contenuti che i    &#13;
 relativi  atti  sono  suscettibili  di  assumere,  si   avrebbe   una    &#13;
 dilatazione  enorme dei casi nei quali un pregiudizio potrebbe essere    &#13;
 ravvisato e l'intera materia delle incompatibilità, dispersa in  una    &#13;
 casistica senza fine, diverrebbe refrattaria a qualsiasi tentativo di    &#13;
 amministrazione mediante atti di organizzazione preventiva" (sentenza    &#13;
 n. 307 del 1997).                                                        &#13;
   È  questa  la  ragione  per  la quale se la forza pregiudicante si    &#13;
 sprigioni non da una sentenza ma, come si assume essere avvenuto  nei    &#13;
 casi  di  specie, da un'ordinanza adottata in un procedimento diverso    &#13;
 (di custodia cautelare nei confronti di un correo ovvero di reiezione    &#13;
 della richiesta di applicazione della pena), lo strumento  di  tutela    &#13;
 non  può  essere  ravvisato in ulteriori sentenze additive sull'art.    &#13;
 34, ma deve essere ricercato nell'area degli istituti dell'astensione    &#13;
 e della ricusazione, anch'essi preordinati  alla  salvaguardia  della    &#13;
 terzietà del giudice.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i   giudizi,   dichiara  inammissibili  le  questioni  di    &#13;
 legittimità costituzionale dell'art. 34,  comma  2,  del  codice  di    &#13;
 procedura penale, sollevate, in riferimento agli artt. 3, 24, secondo    &#13;
 comma,  e  27,  secondo  comma,  della Costituzione, dal tribunale di    &#13;
 Siracusa e, in riferimento agli artt. 3 e 24 della Costituzione,  dal    &#13;
 pretore di Bassano del Grappa, con le ordinanze indicate in epigrafe.    &#13;
   Così  deciso  in  Roma,  nella  sede  della  Corte costituzionale,    &#13;
 Palazzo della Consulta, il 10 novembre 1997.                             &#13;
                        Il Presidente: Granata                            &#13;
                       Il redattore: Mezzanotte                           &#13;
                       Il cancelliere: Di Paola                           &#13;
   Depositata in cancelleria il 14 novembre 1997.                         &#13;
               Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
