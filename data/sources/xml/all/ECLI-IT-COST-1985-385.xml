<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1985</anno_pronuncia>
    <numero_pronuncia>385</numero_pronuncia>
    <ecli>ECLI:IT:COST:1985:385</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>PALADIN</presidente>
    <relatore_pronuncia>Francesco Saja</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>19/12/1985</data_decisione>
    <data_deposito>30/12/1985</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LIVIO PALADIN, Presidente - Avv. ORONZO &#13;
 REALE - Avv. ALBERTO MALAGUGINI - Prof. ANTONIO LA PERGOLA - Prof. &#13;
 VIRGILIO ANDRIOLI - Prof. GIUSEPPE FERRARI - Dott. FRANCESCO SAJA - &#13;
 Prof. GIOVANNI CONSO - Prof. ETTORE GALLO - Dott ALDO CORASANITI - &#13;
 Prof. GIUSEPPE BORZELLINO - Dott. FRANCESCO GRECO, Giudici</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art.  6  ter  d.l.  &#13;
 11  dicembre  1967  n.  1150  (agevolazioni  tributarie  in  materia di  &#13;
 edilizia) convertito con modif. nella legge  7  febbraio  1968  n.  26,  &#13;
 giudizio  promosso  con  ordinanza  emessa  il  15  dicembre 1977 dalla  &#13;
 Commissione tributaria di primo grado di La Spezia sul ricorso proposto  &#13;
 da Guidi Adele, iscritta al  n.  179  del  registro  ordinanze  1978  e  &#13;
 pubblicata  nella  Gazzetta Ufficiale della Repubblica n. 164 dell'anno  &#13;
 1978.                                                                    &#13;
     Visto l'atto di  costituzione  del  Presidente  del  Consiglio  dei  &#13;
 ministri;                                                                &#13;
     udito  nella  camera  di  consiglio del 10 dicembre 1985 il Giudice  &#13;
 relatore dott. Francesco Saja.                                           &#13;
     Ritenuto che nel corso di un  giudizio  promosso  da  Guidi  Adele,  &#13;
 avente ad oggetto agevolazioni tributarie per la costruzione di edifici  &#13;
 di  abitazione,  previste  dall'art.  14  l.  2  luglio 1949 n. 408, la  &#13;
 Commissione tributaria di primo grado di La Spezia con ordinanza del 15  &#13;
 dicembre 1977 (reg. ord. n. 179 del  1978)  sollevava,  in  riferimento  &#13;
 all'art.  3 Cost., questione di legittimità costituzionale dell'art. 6  &#13;
 ter d.l. 11 dicembre 1967 n. 1150 conv. in l. 7 febbraio 1968 n.    26,  &#13;
 concernente l'ambito di applicabilità di dette agevolazioni;            &#13;
     che  la  Commissione  esponeva  che  la  Guidi aveva acquistato nel  &#13;
 territorio del Comune di Montignoso un'area necessaria a costruire  una  &#13;
 casa di abitazione ed aveva invocato il beneficio dell'imposta fissa di  &#13;
 registro;                                                                &#13;
     che  l'Ufficio  del registro, ritenuto che il beneficio non potesse  &#13;
 applicarsi per la  parte  di  suolo  eccedente  l'area  coperta,  aveva  &#13;
 notificato  un'ingiunzione  di  pagamento  per il recupero dell'imposta  &#13;
 relativa a detta eccedenza;                                              &#13;
     che la contribuente aveva presentato ricorso, richiamando il  sopra  &#13;
 citato  art. 6 ter, il quale stabilisce che "nei comuni dotati di piano  &#13;
 regolatore generale o di programma di fabbricazione" i benefici di  cui  &#13;
 alla  l.  n.  408 del 1949 si applicano "all'intera area necessaria per  &#13;
 realizzare i volumi fabbricabili stabiliti dalle norme  e  prescrizioni  &#13;
 urbanistiche per le zone residenziali";                                  &#13;
     che,  ciò  premesso,  la  Commissione osservava come la ricorrente  &#13;
 aveva dovuto lasciare non edificata una parte dell'area acquistata,  in  &#13;
 osservanza del locale piano territoriale paesistico, redatto sulla base  &#13;
 della l. 29 giugno 1939 n. 1497;                                         &#13;
     che  il  citato art. 6 ter era di stretta interpretazione e perciò  &#13;
 non  permetteva  l'applicazione  dei  benefici  tributari   quando   la  &#13;
 necessità  di lasciare non edificata una parte dell'area fosse imposta  &#13;
 da disposizioni diverse (nella specie:   piani  paesistici)  da  quelle  &#13;
 contenute nei piani regolatori o nei programmi di fabbricazione;         &#13;
     che  la Commissione riteneva che, per effetto di tale disciplina, a  &#13;
 situazioni analoghe corrispondevano, senza plausibile  giustificazione,  &#13;
 trattamenti diversi;                                                     &#13;
     che  pertanto  essa  impugnava il citato art. 6 ter "nella parte in  &#13;
 cui non estende l'art. 14  l.  2  luglio  1949  n.  408,  e  successive  &#13;
 modificazioni,  alle  zone  residenziali  che  soggiacciano  a  norme e  &#13;
 prescrizioni  di  piani  territoriali  paesistici  per  l'intera   area  &#13;
 necessaria  a realizzare i volumi fabbricabili stabiliti da dette norme  &#13;
 e prescrizioni";                                                         &#13;
     che  la  Presidenza  del  Consiglio  dei   ministri,   intervenuta,  &#13;
 sosteneva la non fondatezza della questione.                             &#13;
     Considerato  che  le  disposizioni legislative in cui sono previste  &#13;
 agevolazioni tributarie hanno  carattere  derogatorio  e  costituiscono  &#13;
 frutto   di   scelte   del  legislatore,  sindacabili  dal  giudice  di  &#13;
 legittimità costituzionale soltanto se irrazionali o ingiustificate;    &#13;
     che nella specie la norma denunciata  (cit.  art.  6  ter  d.l.  11  &#13;
 dicembre 1967 n. 1150, conv. con modificazioni in legge 7 febbraio 1968  &#13;
 n. 26) prevede i benefici tributari di cui all'art. 14 l. 2 luglio 1949  &#13;
 n. 408 e succ. mod. (imposta fissa di registro e riduzione al quarto di  &#13;
 quella  ipotecaria)  per il trasferimento dell'intera area necessaria a  &#13;
 realizzare in zone residenziali i  volumi  fabbricabili  stabiliti  dal  &#13;
 piano  regolatore generale o dal programma di fabbricazione, mentre non  &#13;
 vi comprende le analoghe prescrizioni dei piani paesistici;              &#13;
     che le due situazioni messe a raffronto,  contrariamente  a  quanto  &#13;
 ritiene il giudice a quo, sono profondamente eterogenee;                 &#13;
     che,  invero,  la  ricordata  agevolazione  tributaria trova il suo  &#13;
 fondamento nel fine, perseguito  dal  legislatore,  di  incentivare  le  &#13;
 costruzioni nelle zone destinate all'espansione edilizia, fine a cui è  &#13;
 data   speciale  rilevanza  anche  rispetto  all'ordinaria  imposizione  &#13;
 tributaria;                                                              &#13;
     che, per contro, nelle località incluse nei piani paesistici viene  &#13;
 prevalentemente      in      considerazione       la       salvaguardia  &#13;
 estetico-paesaggistica  delle  zone  protette, per cui sono imposti dai  &#13;
 detti piani divieti o limitazioni alle costruzioni  (cfr.  cit.  l.  29  &#13;
 giugno  1939  n.  1497 con il relativo regolamento approvato con R.D. 3  &#13;
 giugno 1940 n. 1357;  e,  ora,  anche,  d.l.  27  giugno  1985  n.  312  &#13;
 convertito nella l. 8 agosto 1985 n. 431);                               &#13;
     che  quindi,  rispetto  alle  zone  protette  per  le loro bellezze  &#13;
 naturali, non ricorre certamente la  medesima  ratio  della  denunciata  &#13;
 disposizione,  risultando anzi evidente, in base a quanto ora detto, la  &#13;
 profonda differenza, se non la contrapposizione, tra le due  situazioni  &#13;
 messe a confronto dall'ordinanza di rimessione;                          &#13;
     che  in  conclusione  la  questione  si presenta manifestamente non  &#13;
 fondata.                                                                 &#13;
     Visti  gli  artt.  26  l.  11  marzo  1953  n.  87  e 9 delle Norme  &#13;
 integrative per i giudizi davanti alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara manifestamente non fondata la  questione  di  legittimità  &#13;
 costituzionale  dell'art.  6  ter  d.l.  11 dicembre 1967 n. 1150, come  &#13;
 convertito in l. 7  febbraio  1968  n.  26,  sollevata  in  riferimento  &#13;
 all'art.  3  Cost.  dalla  Commissione  tributaria di primo grado di La  &#13;
 Spezia con l'ordinanza indicata in epigrafe.                             &#13;
     Così deciso in Roma, in camera  di  consiglio,  nella  sede  della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 19 dicembre 1985.       &#13;
                                   F.to:  LIVIO PALADIN - ORONZO REALE -  &#13;
                                   ALBERTO  MALAGUGINI  -   ANTONIO   LA  &#13;
                                   PERGOLA   -   VIRGILIO   ANDRIOLI   -  &#13;
                                   GIUSEPPE FERRARI - FRANCESCO  SAJA  -  &#13;
                                   GIOVANNI  CONSO - ETTORE GALLO - ALDO  &#13;
                                   CORASANITI -  GIUSEPPE  BORZELLINO  -  &#13;
                                   FRANCESCO GRECO.                       &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
