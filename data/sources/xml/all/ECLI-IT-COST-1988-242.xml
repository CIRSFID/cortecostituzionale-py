<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>242</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:242</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Antonio Baldassarre</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>24/02/1988</data_decisione>
    <data_deposito>03/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi promossi con ricorsi della Provincia autonoma di Bolzano    &#13;
 (n. 2 ricorsi), notificati, rispettivamente, il 17 febbraio 1984 e il    &#13;
 19 marzo 1985, depositati in cancelleria l'8 marzo 1984 e il 22 marzo    &#13;
 1985, iscritti al n. 2 del Registro 1984 e  al  n.  13  del  Registro    &#13;
 1985,  per  conflitti di attribuzione sorti a seguito dei decreti del    &#13;
 Ministro del Lavoro e della Previdenza Sociale,  di  concerto  con  i    &#13;
 Ministri di Grazia e Giustizia e della Pubblica Istruzione, aventi ad    &#13;
 oggetto l'indizione di sessioni di esami di Stato per  l'abilitazione    &#13;
 all'esercizio della professione di consulente del lavoro;                &#13;
    Visti  gli  atti  di costituzione del Presidente del Consiglio dei    &#13;
 Ministri;                                                                &#13;
    Udito  nella  Camera  di Consiglio del 25 novembre 1987 il Giudice    &#13;
 relatore Antonio Baldassarre;                                            &#13;
    Ritenuto  che  con due separati ricorsi, di contenuto identico, la    &#13;
 Provincia Autonoma di Bolzano ha sollevato conflitto di  attribuzione    &#13;
 nei  confronti  dello Stato, concernente, rispettivamente, il D.M. 25    &#13;
 novembre 1983 (confl.  n. 2/1984) e il D.M. 19 novembre 1984  (confl.    &#13;
 n.  13/1985)  con i quali il Ministro del Lavoro e Previdenza sociale    &#13;
 ha indetto (rispettivamente, per il 1984  e  il  1985)  due  sessioni    &#13;
 dell'esame   di   stato   per   l'abilitazione   all'esercizio  della    &#13;
 professione di consulente del lavoro, in riferimento all'art. 16,  3°    &#13;
 e  4°  comma  St.   T.A.A.  ed all'art. 3, 1° comma d.P.R. 26 gennaio    &#13;
 1980, n. 197, con riguardo all'art. 3, 2° comma l. 11  gennaio  1979,    &#13;
 n. 12 ed all'art.  un. l. 13 maggio 1982, n. 259;                        &#13;
      che   la   ricorrente  lamenta  che  i  due  decreti  ledano  le    &#13;
 attribuzioni delegate a sé medesima dall'art.  un.  l.  n.  259  del    &#13;
 1982,  in  riferimento  all'art.  3,  2° comma d.P.R. n. 197 del 1980    &#13;
 (norme di attuazione dello  Statuto),  quest'ultimo  fondato,  a  sua    &#13;
 volta,  sulla  previsione  che lo Stato possa delegare con legge alla    &#13;
 provincia funzioni proprie della sua amministrazione, di cui all'art.    &#13;
 16 St. T.A.A.;                                                           &#13;
      che,  il  Presidente  del  Consiglio dei Ministri, costituito in    &#13;
 giudizio a mezzo dell'Avvocatura dello Stato, obbietta  che,  poiché    &#13;
 nella  specie  la  fonte della delega non sarebbe contenuta né nello    &#13;
 Statuto speciale né in norme attuative  di  questo,  ma  nella  sola    &#13;
 legge  ordinaria n. 259 del 1982, i ricorsi - a tenore della sentenza    &#13;
 n. 97 del 1977  di  questa  Corte,  relativa  ad  ipotesi  analoga  -    &#13;
 dovrebbero   dichiararsi   inammissibili,  o  comunque,  nel  merito,    &#13;
 infondati, non avendo ancora la Provincia provveduto ad adottare  una    &#13;
 propria disciplina della materia;                                        &#13;
    Considerato  che, data l'identità dei ricorsi, i relativi giudizi    &#13;
 possono essere riuniti e decisi congiuntamente;                          &#13;
      che  la  giurisprudenza  di questa Corte ha più volte affermato    &#13;
 che, perché si verta in tema di conflitto di  attribuzioni,  occorre    &#13;
 che  "la competenza che si pretende invasa o menomata sia determinata    &#13;
 da   norma   fondamentalmente   costituzionale",    precisando    che    &#13;
 disposizioni  di  leggi ordinarie possono concorrere a configurare il    &#13;
 parametro ove siano "integrative od esecutive di norme costituzionali    &#13;
 di  competenza,  le  quali ultime soltanto costituiscono la fonte del    &#13;
 potere che si invoca";                                                   &#13;
      che  nella presente fattispecie l'attribuzione contestata deriva    &#13;
 esclusivamente dall'art. un. della legge n. 259  del  1982,  giacché    &#13;
 solo  quest'ultima  disposizione  vale  a  ricomprendere  le funzioni    &#13;
 concernenti  l'abilitazione  all'esercizio   della   professione   di    &#13;
 consulente del lavoro nell'ambito della delega disposta genericamente    &#13;
 dall'art. 3, 1° comma d.P.R. n. 197 del 1980 in tema di  vigilanza  e    &#13;
 tutela del lavoro: di conseguenza, fonte della delega è una semplice    &#13;
 legge ordinaria, che non può ritenersi integrativa od  attuativa  di    &#13;
 norme  costituzionali  poiché  è evidente che la previsione, di cui    &#13;
 all'art. 16 St., della possibilità  che  lo  Stato  deleghi  proprie    &#13;
 funzioni  amministrative  alla  Provincia  non  rende per questo solo    &#13;
 integrativa della Costituzione la norma che in concreto  tale  delega    &#13;
 disponga (v. sent. 97 del 1977);                                         &#13;
      che,   pertanto,  i  ricorsi  debbono  ritenersi  manifestamente    &#13;
 inammissibili;                                                           &#13;
    Visto l'art. 27 delle Norme integrative per i giudizi davanti alla    &#13;
 Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  manifestamente  inammissibili  i  ricorsi  proposti dalla    &#13;
 Provincia Autonoma di Bolzano, avverso, rispettivamente il decreto 25    &#13;
 novembre  1983  (confl.  n.  2/1984)  e  il  decreto 19 novembre 1984    &#13;
 (confl. n.  13/1985),  entrambi  del  Ministro  del  Lavoro  e  della    &#13;
 Previdenza sociale.                                                      &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 24 febbraio 1988.                             &#13;
                          Il Presidente: SAJA                             &#13;
                       Il redattore: BALDASSARRE                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 3 marzo 1988.                            &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
