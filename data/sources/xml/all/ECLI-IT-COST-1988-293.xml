<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>293</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:293</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>25/02/1988</data_decisione>
    <data_deposito>10/03/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei  giudizi  di legittimità costituzionale dell'art. 73 della legge    &#13;
 10  agosto  1950  n.  648  (Riordinamento  delle  disposizioni  sulle    &#13;
 pensioni di guerra), degli artt. 64, comma primo, lett. a) e 67 della    &#13;
 legge  18  marzo  1968,  n.  313  (Riordinamento  della  legislazione    &#13;
 pensionistica  di  guerra),  57,  comma primo, lett. a), 58, 70 e 86,    &#13;
 ultimo comma, del d.P.R. 23 dicembre 1978, n. 915 (Testo unico  delle    &#13;
 norme  in  materia  di pensioni di guerra), 12 del d.P.R. 30 dicembre    &#13;
 1981, n. 834 (Definitivo riordinamento delle pensioni di  guerra,  in    &#13;
 attuazione  della delega prevista dall'art. 1 l. 23 settembre 1981 n.    &#13;
 533), promossi con ordinanze emesse il 22 febbraio 1983  dalla  Corte    &#13;
 dei conti, sul ricorso proposto da Maccio' Luigia, iscritta al n. 413    &#13;
 del registro ordinanze 1983 e  pubblicata  nella  Gazzetta  Ufficiale    &#13;
 della Repubblica n. 274 dell'anno 1983 e il 5 maggio 1983 dalla Corte    &#13;
 dei conti - Sezione IV giurisdizionale  -  sul  ricorso  proposto  da    &#13;
 Gallo  Carlo,  iscritta  al  n.  900  del  registro  ordinanze 1983 e    &#13;
 pubblicata nella Gazzetta Ufficiale della Repubblica n. 81  dell'anno    &#13;
 1984;                                                                    &#13;
    Visti  gli  atti  di  intervento  del Presidente del Consiglio dei    &#13;
 ministri;                                                                &#13;
    Udito  nella  camera  di  consiglio del 27 gennaio 1988 il Giudice    &#13;
 relatore Borzellino;                                                     &#13;
    Ritenuto   che   con   ordinanza   emessa   il  22  febbraio  1983    &#13;
 (ord.n.413/1983) la Corte dei conti, sez. II giurisdizionale  per  le    &#13;
 pensioni  di  guerra,  sul  ricorso  proposto  da  Maccio' Luigia, ha    &#13;
 sollevato  questione  incidentale  di   legittimità   costituzionale    &#13;
 "dell'art.  73 della legge 10 agosto 1950, n. 648; dell'art. 67 della    &#13;
 legge 18 marzo 1968, n. 313; degli artt. 58, 70 e 86 - ultimo comma -    &#13;
 del T.U. appovato con d.P.R. 23 dicembre 1978, n. 915, e del connesso    &#13;
 art. 12 del d.P.R. 18 gennaio (recte 30 dicembre) 1981, n.  834,  per    &#13;
 violazione  degli  articoli  2 e 3 della Costituzione, nella parte in    &#13;
 cui, nei riguardi  di  genitori  di  militari  morti  per  causa  del    &#13;
 servizio  di  guerra o attinente alla guerra o di civili deceduti per    &#13;
 fatto di guerra, senza aver lasciato coniuge o figli  con  diritto  a    &#13;
 pensione,  negano  la  concessione  del  trattamento  privilegiato di    &#13;
 guerra, a causa delle condizioni economiche dei genitori medesimi";      &#13;
      che  con ordinanza emessa il 5 maggio 1983 (ord. n. 900/1983) la    &#13;
 Corte dei conti,  sez.  IV  giurisdizionale  ordinaria,  sul  ricorso    &#13;
 proposto  da  Gallo  Carlo,  ha  sollevato  questione  incidentale di    &#13;
 legittimità costituzionale degli "articoli 64,  primo  comma,  lett.    &#13;
 a),  e  67,  della legge 10 marzo 1968 n. 313; 57, primo comma, lett.    &#13;
 a), 58, 70 e 86 del d.P.R. 23 dicembre 1978 n. 915; 12 del d.P.R.  30    &#13;
 dicembre  1981  n.  834  per  contrasto con gli articoli 2 e 3, della    &#13;
 Costituzione, nelle parti  in  cui  nei  riguardi  dei  genitori  dei    &#13;
 militari  deceduti  per  causa  di  servizio senza lasciare coniuge e    &#13;
 figli  con  diritto  a  pensione,  subordinano  la  concessione   del    &#13;
 trattamento  privilegiato indiretto alla sussistenza dei requisiti di    &#13;
 inabilità a proficuo lavoro e di disagiate condizioni economiche dei    &#13;
 genitori   medesimi,  con  riferimento,  per  quanto  in  particolare    &#13;
 riguarda il contrasto con l'art. 3 Cost.,  agli  articoli  71,  primo    &#13;
 comma e 73, secondo comma della legge 18 marzo 1968 n. 313, 62, primo    &#13;
 comma, e 64, secondo comma del d.P.R. 23 dicembre 1978  n.  915,  che    &#13;
 tali requisiti condizionanti non prevedono";                             &#13;
      che  nei  relativi giudizi è intervenuto, per il Presidente del    &#13;
 Consiglio  dei   ministri,   l'Avvocatura   generale   dello   Stato,    &#13;
 concludendo per l'infondatezza delle questioni.                          &#13;
    Considerato  che le ordinanze citate sollevano analoghe o comunque    &#13;
 connesse questioni, talché va  disposta  la  riunione  dei  relativi    &#13;
 giudizi;                                                                 &#13;
      che i giudici a quibus dubitano, in concreto, della legittimità    &#13;
 costituzionale dei requisiti posti dalla legge (inabilità a proficuo    &#13;
 lavoro,  disagiate  condizioni  economiche)  per la concessione della    &#13;
 pensione indiretta di guerra o della pensione privilegiata  indiretta    &#13;
 (la  cui  normativa rinvia sul punto alla prima, ex art. 92 d.P.R. 29    &#13;
 dicembre 1973 n. 1092) in favore dei genitori di militari deceduti in    &#13;
 guerra  o  per  causa di servizio, senza lasciare coniuge e figli con    &#13;
 diritto a pensione; irrazionale disparità di trattamento si  assume,    &#13;
 altresì,  con  riferimento  al  trattamento  riservato ai genitori i    &#13;
 quali abbiano perso l'unico figlio  o  più  figli  per  la  medesima    &#13;
 causa,  in  favore  dei  quali  la liquidazione della pensione non è    &#13;
 subordinata alla sussistenza di tali requisiti;                          &#13;
      che  la  natura  sostanzialmente  risarcitoria  del  trattamento    &#13;
 pensionistico di guerra, invocata dai rimettenti, non implica per  le    &#13;
 connaturali peculiarità proprie di un beneficio posto a carico della    &#13;
 collettività, e pur dovendosi ad esso riconoscere  l'estrinsecazione    &#13;
 di    un    principio   solidaristico,   identità   assoluta   nelle    &#13;
 caratteristiche e negli effetti  giuridici  connessi,  rispetto  alla    &#13;
 pura   obbligazione   civilistica   di  risarcimento;  talché  detta    &#13;
 (peculiare) natura risarcitoria non  rende  di  per  sé  irrazionali    &#13;
 condizioni  e  limiti  posti dal legislatore per il conseguimento del    &#13;
 beneficio, quando nell'esercizio  della  sua  discrezionalità  abbia    &#13;
 esteso  la  provvidenza  in  parola al di là del naturale ambito dei    &#13;
 diretti destinatari di essa (coniuge e figli) nel caso in cui  questi    &#13;
 ultimi  manchino,  fino  a  comprendere,  cioè,  anche  altri aventi    &#13;
 diritto quali, come in fattispecie, i genitori;                          &#13;
      che  in  effetti,  alla  luce di tale precisazione, l'aver fatto    &#13;
 riferimento, per le suddette categorie di soggetti, ad  un  effettivo    &#13;
 stato  di  bisogno (stato di inabilità, disagiate condizioni economi    &#13;
 che) quale condizione di ammissibilità al beneficio,  ovvero  l'aver    &#13;
 ritenuto   di   voler   concedere   il  trattamento  pensionistico  a    &#13;
 prescindere da tale condizione a chi abbia  perso  l'unico  figlio  o    &#13;
 più  figli per dette cause, appare frutto di scelte sufficientemente    &#13;
 razionali, le  quali,  per  la  loro  discrezionalità,  sfuggono  al    &#13;
 sindacato di questa Corte.                                               &#13;
    Visti gli artt. 26, secondo comma, l.11 marzo 1953 n. 87 e 9 delle    &#13;
 Norme integrative per i giudizi davanti alla Corte costituzionale;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti  i  giudizi,  dichiara  la manifesta inammissibilità delle    &#13;
 questioni di legittimità costituzionale dell'art. 73 della legge  10    &#13;
 agosto  1950, n. 648 (Riordinamento delle disposizioni sulle pensioni    &#13;
 di guerra), degli artt. 64, primo comma, lett. a) e 67 della legge 18    &#13;
 marzo  1968 n. 313 (Riordinamento della legislazione pensionistica di    &#13;
 guerra), 57, primo comma, lett. a), 58, 70 e 86, ultimo comma, d.P.R.    &#13;
 23  dicembre  1978  n.  915  (Testo  unico  delle norme in materia di    &#13;
 pensioni di guerra), 12 d.P.R. 30 dicembre 1981, n.  834  (Definitivo    &#13;
 riordinamento  delle  pensioni  di guerra, in attuazione della delega    &#13;
 prevista dall'art. 1 l. 23 settembre  1981  n.  533),  sollevate,  in    &#13;
 riferimento  agli  artt.  2  e  3 della Costituzione, dalla Corte dei    &#13;
 conti con le ordinanze in epigrafe.                                      &#13;
    Così  deciso  in  Roma,  in camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 25 febbraio 1988.       &#13;
                          Il Presidente: SAJA                             &#13;
                        Il redattore: BORZELLINO                          &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 10 marzo 1988.                           &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
