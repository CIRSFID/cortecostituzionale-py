<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1995</anno_pronuncia>
    <numero_pronuncia>353</numero_pronuncia>
    <ecli>ECLI:IT:COST:1995:353</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>BALDASSARRE</presidente>
    <relatore_pronuncia>Fernando Santosuosso</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/07/1995</data_decisione>
    <data_deposito>21/07/1995</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Antonio BALDASSARRE; &#13;
 Giudici: prof. Vincenzo CAIANIELLO, avv. Mauro FERRI, prof. Luigi &#13;
 MENGONI, prof. Enzo CHELI, dott. Renato GRANATA, prof. Giuliano &#13;
 VASSALLI, prof. Francesco GUIZZI, prof. Cesare MIRABELLI, prof. &#13;
 Fernando SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO, &#13;
 dott. Riccardo CHIEPPA;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio di legittimità costituzionale dell'art. 13, commi  2  e    &#13;
 3,  della  legge  23  dicembre  1992,  n.  498 (Interventi urgenti in    &#13;
 materia di finanza pubblica), così come sostituito dall'art.  6-bis,    &#13;
 del  decreto-legge  18  gennaio  1993,  n. 9 (Disposizioni urgenti in    &#13;
 materia   sanitaria   e   socio-assistenziale),    convertito,    con    &#13;
 modificazioni,  nella  legge  18  marzo  1993,  n.  67,  promosso con    &#13;
 ordinanza emessa il 26  ottobre  1993  dal  Pretore  di  Brescia  nel    &#13;
 procedimento  civile  vertente  tra  la Casa di Riposo di Calcinato e    &#13;
 l'I.N.P.S.,  iscritta  al  n.  653  del  registro  ordinanze  1994  e    &#13;
 pubblicata  nella  Gazzetta  Ufficiale  della Repubblica n. 45, prima    &#13;
 serie speciale, dell'anno 1994;                                          &#13;
    Visto l'atto di costituzione dell'I.N.P.S.;                           &#13;
    Udito nella camera di consiglio del  28  giugno  1995  il  Giudice    &#13;
 relatore Fernando Santosuosso;                                           &#13;
    Ritenuto  che  nel corso di un procedimento civile vertente fra la    &#13;
 Casa di Riposo di Calcinato e l'I.N.P.S., il Pretore di Brescia,  con    &#13;
 ordinanza   emessa   il   26   ottobre  1993,  pervenuta  alla  Corte    &#13;
 costituzionale il 19 ottobre 1994, ha sollevato, in riferimento  agli    &#13;
 artt.  3,  4,  24,  35,  38  e  97  della  Costituzione, questione di    &#13;
 legittimità costituzionale dell'art. 13, commi 2 e 3, della legge 23    &#13;
 dicembre 1992, n. 498  (Interventi  urgenti  in  materia  di  finanza    &#13;
 pubblica)  così come sostituito dall'art. 6-bis del decreto-legge 18    &#13;
 gennaio 1993, n. 9  (Disposizioni  urgenti  in  materia  sanitaria  e    &#13;
 socio-assistenziale),  convertito  in legge, con modificazioni, dalla    &#13;
 legge 18 marzo 1993, n. 67, nella parte in cui con l'escludere, per i    &#13;
 contratti  qualificati  d'opera  o  per   prestazioni   professionali    &#13;
 stipulati  da province, comuni, comunità montane e loro consorzi, la    &#13;
 configurabilità di un rapporto di natura subordinata,  impedisce  al    &#13;
 giudice l'accertamento della loro reale natura;                          &#13;
      che  nel  giudizio  avanti  a  questa  Corte  si  è  costituito    &#13;
 l'I.N.P.S. concludendo per l'infondatezza della questione;               &#13;
    Considerato che questione sostanzialmente identica è  stata  già    &#13;
 decisa,  con  pronuncia  di  non  fondatezza  nei  sensi  di  cui  in    &#13;
 motivazione, con la sent. n. 115 del 1994, sul rilievo che "il tenore    &#13;
 delle disposizioni impugnate non impone affatto  la  lettura  che  di    &#13;
 essa  danno  i giudici rimettenti" in quanto la norma di cui all'art.    &#13;
 13, comma 2, della legge 23  dicembre  1992,  n.  498  si  limita  ad    &#13;
 escludere  che ai contratti d'opera e di prestazione professionale da    &#13;
 essa  considerati  siano  estensibili  gli  obblighi  previdenziali e    &#13;
 assistenziali previsti  per  il  lavoro  subordinato,  senza  perciò    &#13;
 escludere  che il giudice possa qualificare diversamente il contratto    &#13;
 nell'ipotesi in cui il rapporto di lavoro, in contrasto con il titolo    &#13;
 contrattuale, abbia di fatto assunto i contenuti e  le  modalità  di    &#13;
 svolgimento proprie del rapporto di lavoro subordinato;                  &#13;
      che, pertanto, essendo venuto meno il presupposto interpretativo    &#13;
 sul  quale  il  giudice  rimettente  fonda  le  proprie  censure,  la    &#13;
 questione deve essere dichiarata manifestamente infondata;               &#13;
    Visti gli artt. 26, secondo comma, della legge 11 marzo  1953,  n.    &#13;
 87  e 9, secondo comma, delle norme integrative per i giudizi davanti    &#13;
 alla Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara la manifesta infondatezza della questione di  legittimità    &#13;
 costituzionale  dell'art.  13,  commi  2 e 3, della legge 23 dicembre    &#13;
 1992, n. 498 (Interventi urgenti in  materia  di  finanza  pubblica),    &#13;
 così  come  sostituito  dall'art. 6-bis del decreto-legge 18 gennaio    &#13;
 1993,  n.  9   (Disposizioni   urgenti   in   materia   sanitaria   e    &#13;
 socio-assistenziale),  convertito  in legge, con modificazioni, dalla    &#13;
 legge 18 marzo 1993, n. 67, sollevata, in riferimento agli  artt.  3,    &#13;
 4, 24, 35, 38, 41 e 97 della Costituzione, dal Pretore di Brescia con    &#13;
 l'ordinanza indicata in epigrafe.                                        &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 12 luglio 1995.                               &#13;
                      Il Presidente: BALDASSARRE                          &#13;
                       Il redattore: SANTOSUOSSO                          &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 21 luglio 1995.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
