<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1988</anno_pronuncia>
    <numero_pronuncia>1110</numero_pronuncia>
    <ecli>ECLI:IT:COST:1988:1110</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Francesco Greco</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>12/12/1988</data_decisione>
    <data_deposito>20/12/1988</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, &#13;
 avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel  giudizio  di  legittimità  costituzionale  dell'art. 371, primo    &#13;
 comma, cod. proc. civ., promosso con ordinanza  emessa  il  13  marzo    &#13;
 1987  dalla Corte di Cassazione sui ricorsi riuniti proposti da Torri    &#13;
 Diego ed altri contro Ubbiali Gesualdo e  da  Cagnoni  Isabella  ved.    &#13;
 Ubbiali  contro  la  s.n.c.  Autosalone  Torri, iscritta al n. 94 del    &#13;
 registro ordinanze 1988 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 13, prima serie speciale dell'anno 1988;                   &#13;
    Udito  nella  camera  di  consiglio del 12 ottobre 1988 il Giudice    &#13;
 relatore Francesco Greco.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>1.  - Il 19 gennaio 1984 decedeva Gesualdo Ubbiali, al quale il 22    &#13;
 dicembre  1983  era  stato  notificato  il  ricorso  principale   per    &#13;
 cassazione  avverso  la  sentenza  della  Corte di Appello di Milano,    &#13;
 depositata il 22 luglio 1983, nel giudizio tra lo  stesso  Ubbiali  e    &#13;
 Diego  Torri  ed altri. In data 8/10 febbraio 1984 veniva notificato,    &#13;
 ad  istanza  degli  eredi  dell'Ubbiali,  controricorso  col  ricorso    &#13;
 incidentale.                                                             &#13;
    La  Corte di cassazione, con ordinanza in data 13 marzo 1987, dopo    &#13;
 aver  escluso  che  la  notifica  del  ricorso  incidentale   potesse    &#13;
 ritenersi  tempestiva,  attesa  l'inapplicabilità  in  via analogica    &#13;
 dell'art. 328, primo comma,  c.p.c.  (prevedente  l'interruzione  del    &#13;
 termine  e  la decorrenza del nuovo dal giorno in cui è rinnovata la    &#13;
 notificazione della sentenza) e dell'art. 299 del codice di rito,  ha    &#13;
 sollevato  d'ufficio  la  questione  di  legittimità  costituzionale    &#13;
 dell'art. 371, primo comma, c.p.c., in  riferimento  agli  artt.  24,    &#13;
 primo  e  secondo  comma, e 3, primo comma, Cost., nella parte in cui    &#13;
 non prevede che, in caso di morte della  parte  contro  la  quale  il    &#13;
 ricorso  principale  è  diretto,  durante  il  termine  per proporre    &#13;
 ricorso incidentale, tale termine  sia  interrotto  e  che  il  nuovo    &#13;
 decorra  dal  giorno in cui la notificazione del ricorso è rinnovata    &#13;
 agli eredi.                                                              &#13;
    Il  giudice a quo opina che la omessa previsione dell'interruzione    &#13;
 del termine per proporre ricorso  incidentale  si  risolva,  per  gli    &#13;
 eredi  della parte cui sia stato notificato il ricorso principale, in    &#13;
 una minorata difesa in assoluto dei propri diritti (non essendo  essi    &#13;
 posti  in  condizione di disporre di un tempo adeguato per apprestare    &#13;
 la propria difesa,  tanto  più  quando  il  termine  sia  pressoché    &#13;
 totalmente,  o  addirittura  completamente,  decorso al momento della    &#13;
 morte del de cuius; ed in una irragionevole disparità di trattamento    &#13;
 rispetto  agli  eredi della parte defunta cui sia stata notificata la    &#13;
 sentenza,  i  quali,  in  ordine  alla   proposizione   del   ricorso    &#13;
 principale,   usufruiscono  dell'interruzione  e  del  nuovo  termine    &#13;
 decorrente dalla rinnovazione della notifica della  sentenza  a  loro    &#13;
 stessi,  sia  pure  collettivamente  ed  impersonalmente  nell'ultimo    &#13;
 domicilio del defunto. E ciò senza alcun giustificato motivo,  posto    &#13;
 che,  in  relazione  al possibile pregiudizio arrecato ad entrambe le    &#13;
 parti dalla sentenza impugnata, è del tutto casuale che l'iniziativa    &#13;
 che  fa decorrere il termine per il notificato (con la notifica della    &#13;
 sentenza ovvero del ricorso principale) sia stata presa da una  parte    &#13;
 anziché dall'altra.                                                     &#13;
    2.  -  Non si sono costituite parti private, né è intervenuto il    &#13;
 Presidente del Consiglio dei Ministri.<diritto>Considerato in diritto</diritto>1.   -   La   Corte   di   cassazione  dubita  della  legittimità    &#13;
 costituzionale dell'art. 371, primo comma,  c.p.c.,  nella  parte  in    &#13;
 cui,  richiamando il termine previsto nell'art. 370 c.p.c., entro cui    &#13;
 la parte contro la  quale  è  diretto  il  ricorso  principale  deve    &#13;
 proporre  il  ricorso  incidentale e la decadenza di cui all'art. 333    &#13;
 c.p.c., con cui la sua violazione è sanzionata, non prevede che,  se    &#13;
 durante  la  decorrenza di tale termine sopravviene la morte di detta    &#13;
 parte, il termine stesso è interrotto e il nuovo termine decorra dal    &#13;
 giorno in cui la notificazione del ricorso è rinnovata agli eredi.      &#13;
    2. - La questione è inammissibile.                                   &#13;
    La Corte remittente ha escluso l'applicazione dell'art. 328, primo    &#13;
 comma, c.p.c., che prevede l'interruzione del  termine  per  proporre    &#13;
 ricorso  principale,  in  caso  di  morte,  in  pendenza dello stesso    &#13;
 termine, della parte alla quale sia stata notificata la  sentenza,  e    &#13;
 il rinnovo di tale notificazione.                                        &#13;
    Ha  escluso, altresì, l'applicazione dell'art. 328, ultimo comma,    &#13;
 c.p.c.,  che  prevede  la   proroga   del   suddetto   termine,   per    &#13;
 l'incompatibilità che sussiste con la natura e la durata del termine    &#13;
 per proporre l'impugnazione  incidentale;  ha  ribadito,  infine,  la    &#13;
 inapplicabilità dell'art. 299 c.p.c. al processo per cassazione.        &#13;
    Ora,  sebbene  non  siano da escludersi le particolari difficoltà    &#13;
 che incontrano gli eredi  del  notificato,  nell'esercitare  la  loro    &#13;
 legittimazione  all'impugnazione,  a  causa  di  un  evento  loro non    &#13;
 imputabile, quale è la morte del de cuius, titolare del  diritto  di    &#13;
 proporre  ricorso  incidentale, e la non sempre tempestiva conoscenza    &#13;
 dell'esistenza del giudizio, si osserva che nel sistema  dell'attuale    &#13;
 codice  di  rito il ricorso principale è l'atto che mette in moto il    &#13;
 termine nei confronti delle altre  parti  del  processo,  legittimate    &#13;
 anch'esse a proporre impugnazione contro la stessa sentenza.             &#13;
    In  altri  termini,  la  parte  che  per prima impugna la sentenza    &#13;
 determina, con il suo comportamento, la modifica dei termini  per  le    &#13;
 altre   parti   del   processo,   in   quanto   ai   termini   propri    &#13;
 dell'impugnazione principale (artt. 325 e  327)  si  sostituiscono  i    &#13;
 termini  previsti per le altre impugnazioni che divengono incidentali    &#13;
 (artt. 343 e 371 c.p.c.).                                                &#13;
    Del  resto,  lo  stesso  ricorrente,  da  considerarsi  ricorrente    &#13;
 incidentale per l'avvenuta proposizione del ricorso principale di  un    &#13;
 altra   parte   processuale,   avrebbe   potuto   anch'egli  proporre    &#13;
 impugnazione autonoma a tutela del suo diritto, anche se non si  può    &#13;
 del  tutto  escludere  che  il  suo  interesse all'impugnazione possa    &#13;
 sorgere dall'avvenuta impugnazione principale.                           &#13;
    La  coesistenza  di  più  impugnazioni  autonome,  ammessa  dalla    &#13;
 giurisprudenza ormai costante, porta poi, in sede di  giudizio  sulle    &#13;
 impugnazioni,  alla classificazione come principale dell'impugnazione    &#13;
 di data anteriore e di "incidentali" delle altre impugnazioni di data    &#13;
 successiva.                                                              &#13;
    È,  comunque, di peculiare evidenza, e la stessa Corte remittente    &#13;
 lo ha notato, che occorre una nuova e specifica disciplina  normativa    &#13;
 che,  inserita  nel  sistema  del  codice di rito, ponga rimedio alla    &#13;
 diversità  di  trattamento  che  attualmente  riceve  il  ricorrente    &#13;
 incidentale  rispetto  al  ricorrente  principale, nei casi in cui si    &#13;
 verifichi uno degli eventi di cui all'art. 299  c.p.c.,  tra  cui  la    &#13;
 morte  dello  stesso  ricorrente incidentale, ed appresti, tra i vari    &#13;
 meccanismi possibili, quello  ritenuto  più  idoneo  a  superare  le    &#13;
 difficoltà  in  cui  possono  trovarsi  gli  eredi, in modo che, con    &#13;
 eguale trattamento, sia ad essi garantita la piena tutela del diritto    &#13;
 di cui sono divenuti titolari per la morte del de cuius.                 &#13;
    Una  sentenza  additiva  o  manipolativa di questa Corte, invocata    &#13;
 dalla Corte remittente, non può sopperire  alla  riscontrata  lacuna    &#13;
 normativa.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara  inammissibile la questione di legittimità costituzionale    &#13;
 dell'art. 371, primo comma, c.p.c., sollevata,  in  riferimento  agli    &#13;
 artt.  3  e  24  Cost.,  dalla Corte di cassazione con l'ordinanza in    &#13;
 epigrafe.                                                                &#13;
    Così  deciso in Roma, nella camera di consiglio, nella sede della    &#13;
 Corte costituzionale, Palazzo della Consulta, il 12 dicembre 1988.       &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: GRECO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 20 dicembre 1988.                        &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
