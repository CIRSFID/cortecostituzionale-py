<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1998</anno_pronuncia>
    <numero_pronuncia>240</numero_pronuncia>
    <ecli>ECLI:IT:COST:1998:240</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>GRANATA</presidente>
    <relatore_pronuncia>Cesare Mirabelli</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/06/1998</data_decisione>
    <data_deposito>03/07/1998</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Renato GRANATA; &#13;
 Giudici: prof. Giuliano VASSALLI, prof. Francesco GUIZZI, prof. &#13;
 Cesare MIRABELLI, prof. Fernando SANTOSUOSSO, avv. Massimo VARI, &#13;
 dott. Cesare RUPERTO, dott. Riccardo CHIEPPA, prof. Gustavo &#13;
 ZAGREBELSKY, prof. Valerio ONIDA, prof. Carlo MEZZANOTTE, avv. &#13;
 Fernanda CONTRI, prof. Guido NEPPI MODONA, prof. Piero Alberto &#13;
 CAPOTOSTI, prof. Annibale MARINI;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Sentenza</titolo>nel  giudizio di legittimità costituzionale dell'art. 27 della legge    &#13;
 4 maggio 1983, n. 184 (Disciplina  dell'adozione  e  dell'affidamento    &#13;
 dei  minori),  promosso  con  ordinanza emessa il 16 gennaio 1997 dal    &#13;
 Tribunale  di  Caltagirone  nel  procedimento  civile  vertente   tra    &#13;
 Francesca  Strataglio  e  Antonina  Velardita, iscritta al n. 291 del    &#13;
 registro ordinanze 1997 e pubblicata nella Gazzetta  Ufficiale  della    &#13;
 Repubblica n. 23, prima serie speciale, dell'anno 1997.                  &#13;
   Udito  nella  camera  di  consiglio del 25 febbraio 1998 il giudice    &#13;
 relatore Cesare Mirabelli.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>nel corso di un giudizio promosso dal figlio adottivo  che  intendeva    &#13;
 partecipare  alla  divisione  ereditaria  dei beni del fratello della    &#13;
 madre adottiva subentrando, per rappresentazione,  nel  luogo  e  nel    &#13;
 grado  di  quest'ultima, il   Tribunale di Caltagirone, con ordinanza    &#13;
 emessa il 16 gennaio 1997, ha sollevato, in riferimento all'art.    3    &#13;
 della   Costituzione,   questione   di   legittimità  costituzionale    &#13;
 dell'art.    27  della  legge  4  maggio  1983,  n.  184  (Disciplina    &#13;
 dell'adozione  e dell'affidamento dei minori), nella parte in cui non    &#13;
 prevede che l'equiparazione dei figli adottivi a quelli legittimi  si    &#13;
 estenda   retroattivamente   anche  ai  minori  adottati  secondo  la    &#13;
 disciplina del codice civile, i quali non sono  ammessi  a  succedere    &#13;
 per rappresentazione nell'asse ereditario dei parenti dell'adottante.    &#13;
   Il Tribunale di Caltagirone ritiene che, nel caso sottoposto al suo    &#13;
 esame,  dovrebbe  essere respinta la pretesa del figlio, adottato nel    &#13;
 1950  quando  era  minorenne,  di  succedere  in  luogo  della  madre    &#13;
 adottiva,  premorta,  nell'eredità  del  fratello  di  quest'ultima.    &#13;
 Difatti  secondo  la  disciplina  dell'adozione  ordinaria  i   figli    &#13;
 adottivi  sono  estranei  alla successione dei parenti dell'adottante    &#13;
 (art. 567, secondo comma, cod.  civ.).  Tuttavia  lo  stesso  giudice    &#13;
 ritiene che l'art.  27 della legge n. 184 del 1983, omettendo di dare    &#13;
 applicazione retroattiva al principio che il minore adottato acquista    &#13;
 lo stato di figlio legittimo degli adottanti, determini, in contrasto    &#13;
 con  l'art. 3 della Costituzione, una disparità di trattamento, agli    &#13;
 effetti della successione nei confronti dei  parenti  dell'adottante,    &#13;
 tra  minori  adottati  prima e dopo la legge n. 184 del 1983, i quali    &#13;
 sarebbero trattati in modo diverso, anche se  titolari  dello  stesso    &#13;
 stato di figlio adottivo.<diritto>Considerato in diritto</diritto>1. - Il dubbio di legittimità costituzionale investe la disciplina    &#13;
 della  condizione, agli effetti successori, di chi sia stato adottato    &#13;
 in  età  minore  prima  della  legge  che,  introducendo  l'adozione    &#13;
 legittimante  per  i  minori,  ha disposto che l'adottato acquista lo    &#13;
 stato  di  figlio  legittimo  degli  adottanti.   Il   Tribunale   di    &#13;
 Caltagirone  ritiene  che questa regola, stabilita dall'art. 27 della    &#13;
 legge  4  maggio   1983,   n.   184   (Disciplina   dell'adozione   e    &#13;
 dell'affidamento  dei  minori),  sia  in contrasto con l'art. 3 della    &#13;
 Costituzione, nella parte in cui non si estende anche a chi sia stato    &#13;
 adottato in precedenza secondo la disciplina dell'adozione ordinaria,    &#13;
 la quale non estraneo alla  successione  dei  parenti  dell'adottante    &#13;
 (art.  567,  secondo comma, cod. civ.)  consente, al figlio adottivo,    &#13;
 di  subentrare   per   rappresentazione   in   luogo   dell'adottante    &#13;
 nell'eredità    alla   quale   quest'ultimo   sia   chiamato.   Ciò    &#13;
 determinerebbe  una  ingiustificata  disparità  di  trattamento  tra    &#13;
 minori  adottati  prima  o  dopo  la  legge  n.  184 del 1983, pur in    &#13;
 presenza, ad avviso del giudice rimettente, di un identico stato.        &#13;
   2. - La questione di legittimità costituzionale non è fondata.       &#13;
   Il rimettente muove dall'inesatta premessa che siano  identiche  la    &#13;
 condizione  del  minore adottato in base alla legge n. 184 del 1983 e    &#13;
 quella di chi è  stato  adottato  secondo  le  regole  dell'adozione    &#13;
 ordinaria disciplinata dal codice civile (ora prevista unicamente per    &#13;
 i maggiori di età: art. 58 della legge n. 184 del 1983).                &#13;
   L'art.  27  della  legge  n. 184 del 1983 non detta una particolare    &#13;
 disciplina dei rapporti successori ai  quali  possa  essere  chiamato    &#13;
 l'adottato. La norma stabilisce, piuttosto, gli effetti dell'adozione    &#13;
 legittimante,   in   forza  della  quale  il  minore  adottato  viene    &#13;
 definitivamente inserito nella famiglia di accoglienza ed acquista lo    &#13;
 stato di figlio legittimo dei coniugi  adottanti,  mentre  cessano  i    &#13;
 suoi rapporti con la famiglia di origine. Gli effetti successori sono    &#13;
 soltanto  una conseguenza di tale mutamento di status: il minore, una    &#13;
 volta adottato, non è equiparato  ai  figli  legittimi,  ma  diviene    &#13;
 figlio  legittimo  degli  adottanti  ed in quanto tale partecipa alla    &#13;
 successione dei parenti di essi.                                         &#13;
   Diversa è la condizione di chi, sia pure in età minore, sia stato    &#13;
 adottato, prima dell'introduzione dell'adozione legittimante, secondo    &#13;
 la disciplina dettata dal codice civile.                                 &#13;
   L'adozione   ordinaria   -  ancorata  a  presupposti,  finalità  e    &#13;
 requisiti  diversi  rispetto  a  quelli   previsti   per   l'adozione    &#13;
 legittimante - fa conservare all'adottato tutti i diritti ed i doveri    &#13;
 verso  la  famiglia d'origine e, parallelamente, non fa sorgere alcun    &#13;
 rapporto tra esso ed i parenti dell'adottante (art. 300 cod. civ.).      &#13;
   Adozione legittimante ed adozione ordinaria configurano  situazioni    &#13;
 diverse,   per   le   quali   non   è  palesemente  irrazionale  né    &#13;
 discriminatoria una differente disciplina  rispondente  alle  diverse    &#13;
 connotazioni dell'istituto e che, quanto alla successione ereditaria,    &#13;
 determini o escluda la possibilità di succedere per rappresentazione    &#13;
 in connessione all'instaurarsi o meno di un rapporto di parentela con    &#13;
 i  congiunti  dell'adottante  e,  correlativamente,  al  cessare o al    &#13;
 permanere dei rapporti con la famiglia di origine.                       &#13;
   Nell'introdurre l'adozione legittimante  (salvo  casi  particolari)    &#13;
 come  unico  modello  per i minori, la legge n. 184 del 1983 ha anche    &#13;
 considerato   coloro   che,   minorenni   all'epoca   del    relativo    &#13;
 provvedimento,  erano  stati  già  adottati  secondo  la  disciplina    &#13;
 precedente, consentendo, per un periodo transitorio, che ad  essi  il    &#13;
 tribunale   per   i  minorenni  potesse  estendere,  ricorrendone  le    &#13;
 condizioni, gli effetti dell'adozione legittimante (art. 79). Neppure    &#13;
 sussiste,   dunque,   quella    irragionevole     "irretroattività"     &#13;
 dell'applicazione  della  nuova disciplina, denunciata dall'ordinanza    &#13;
 di rimessione.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Dichiara non fondata la questione  di  legittimità  costituzionale    &#13;
 dell'art.   27   della  legge  4  maggio  1983,  n.  184  (Disciplina    &#13;
 dell'adozione  e  dell'affidamento   dei   minori),   sollevata,   in    &#13;
 riferimento  all'art.    3  della  Costituzione,  dal    Tribunale di    &#13;
 Caltagirone con l'ordinanza indicata in epigrafe.                        &#13;
   Così deciso  in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, il 1 giugno 1998.                                &#13;
                        Il Presidente: Granata                            &#13;
                        Il redattore: Mirabelli                           &#13;
                        Il cancelliere: Milana                            &#13;
   Depositata in cancelleria il 3 luglio 1998.                            &#13;
                        Il cancelliere: Milana</dispositivo>
  </pronuncia_testo>
</pronuncia>
