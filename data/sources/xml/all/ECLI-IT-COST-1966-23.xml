<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1966</anno_pronuncia>
    <numero_pronuncia>23</numero_pronuncia>
    <ecli>ECLI:IT:COST:1966:23</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giuseppe Branca</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>03/03/1966</data_decisione>
    <data_deposito>10/03/1966</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 GIUSEPPE CASTELLI AVOLIO - Prof. ANTONINO PAPALDO - Prof. NICOLA JAEGER &#13;
 - Prof. GIOVANNI CASSANDRO - Prof. BIAGIO PETROCELLI - Dott. ANTONIO &#13;
 MANCA - Prof. ALDO SANDULLI - Prof. GIUSEPPE BRANCA - Prof. MICHELE &#13;
 FRAGALI - Prof. GIUSEPPE CHIARELLI - Dott. GIUSEPPE VERZÌ - Dott. &#13;
 GIOVANNI BATTISTA BENEDETTI - Prof. FRANCESCO PAOLO BONIFACIO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nel giudizio di legittimità costituzionale della  legge  approvata  &#13;
 dall'Assemblea  regionale  siciliana  nella  seduta del 15 giugno 1965,  &#13;
 recante: "Adeguamento dei termini previsti  dalle  leggi  regionali  28  &#13;
 aprile 1954, n.  11, 18 ottobre 1954, n. 37, 11 gennaio 1963, n. 4, e 6  &#13;
 maggio 1965, n. 12, ai termini previsti dal D.L. 15 marzo 1965, n. 124,  &#13;
 convertito,  con  modifiche,  nella  legge  13  maggio  1965,  n. 431",  &#13;
 promosso con  ricorso  del  Commissario  dello  Stato  per  la  Regione  &#13;
 siciliana,  notificato  il 23 giugno 1965, depositato nella cancelleria  &#13;
 della Corte costituzionale il 2 luglio successivo ed iscritto al n.  14  &#13;
 del Registro ricorsi 1965.                                               &#13;
     Visto   l'atto   di   costituzione  del  Presidente  della  Regione  &#13;
 siciliana;                                                               &#13;
     udita nell'udienza pubblica del 2 febbraio 1966  la  relazione  del  &#13;
 Giudice Giuseppe Branca;                                                 &#13;
     uditi  il  sostituto avvocato generale dello Stato Pietro Peronaci,  &#13;
 per il Commissario dello Stato, e l'avv.  Salvatore Orlando Cascio, per  &#13;
 la Regione siciliana.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>:                             &#13;
     1. - Con atto del 23 giugno 1965 il Commissario dello Stato per  la  &#13;
 Regione  siciliana  ha impugnato la legge regionale siciliana approvata  &#13;
 nella seduta del 15 giugno 1965 recante adeguamento di precedenti leggi  &#13;
 regionali ai termini  previsti  dal  D.L.  15  marzo  1965,  n.    124,  &#13;
 convertito, con modificazioni, nella legge 13 maggio 1965, n. 431.       &#13;
     Afferma il Commissario dello Stato che la legge sarebbe illegittima  &#13;
 per  contrasto  con  gli artt. 17 e 36 dello Statuto siciliano, innanzi  &#13;
 tutto perché, pur uniformandosi al termine di scadenza fissato in quel  &#13;
 D.L.,  se  ne  discosta  concedendo  esenzioni  totali  di  imposte   o  &#13;
 tassazioni in misura fissa invece che riduzioni di aliquote; in secondo  &#13;
 luogo perché, con l'art. 2, riducendo del l'85 per cento per il 1967 e  &#13;
 dell'80  per  cento  per  il  1968 la aliquota dell'imposta comunale di  &#13;
 consumo dei materiali da costruzione, ha  prorogato  fino  al  1968  la  &#13;
 legge  regionale  6  maggio  1965,  n.  12,  su  cui  pende denuncia di  &#13;
 incostituzionalità;  infine   perché   tutte   le   agevolazioni   si  &#13;
 applicherebbero  anche  agli alberghi, mentre di tale applicabilità è  &#13;
 stata contestata, presso questa Corte, la legittimità  costituzionale.  &#13;
 Sarebbe  da  vedere, inoltre, se il contributo di 900 milioni (art. 4),  &#13;
 insieme col precedente stanziamento, di cui i Comuni  beneficieranno  a  &#13;
 compenso  della  diminuzione  d'entrate tributarie causata dalla legge,  &#13;
 basti ad escludere che, con la riduzione dell'imposta di  consumo,  sia  &#13;
 stata  violata  anche  l'autonomia  comunale  (art.  15  dello  Statuto  &#13;
 siciliano).                                                              &#13;
     2. - La Regione siciliana, con deduzioni depositate  il  20  luglio  &#13;
 1965,  risponde  che:  1)  l'esenzione  venticinquennale dell'imposta e  &#13;
 sovrimposta sui  fabbricati,  prorogata  con  la  legge  impugnata,  è  &#13;
 identica  a  quella  ripristinata  sul piano nazionale dall'art. 43 del  &#13;
 citato D.L. 1965; n. 124; che  la  misura  fissa  della  sottoposizione  &#13;
 all'imposta   di  registro,  di  trascrizione  e  di  iscrizione  sulle  &#13;
 alienazioni ecc. e l'esenzione dell'imposta di ricchezza  mobile  sugli  &#13;
 interessi  dei  mutui  corrispondono  a tipi di agevolazioni tributarie  &#13;
 che, pur essendo estranei al predetto  D.L.  statale,  esistevano  già  &#13;
 nelle  leggi  dello  Stato  fino al 1960:   dimodoché sarebbe inesatto  &#13;
 sostenere che non corrispondano ai  principi  ai  quali  si  ispira  la  &#13;
 legislazione  statale  in  materia; 2) la riduzione dell'85 per cento e  &#13;
 dell'80 per cento dell'aliquota dell'imposta di  consumo  è  legittima  &#13;
 per  i  motivi  già dedotti in replica al ricorso promosso dallo Stato  &#13;
 contro la legge regionale approvata il 24 marzo  1965  (divenuta,  poi,  &#13;
 legge  6  maggio 1965, n. 12) e perché col predetto D.L.  del 1965, n.  &#13;
 124 modificato dalla legge 1965, n. 431 lo Stato ha introdotto  analoga  &#13;
 riduzione anche se in misura minore (20 per cento), nonché l'esenzione  &#13;
 totale  nel  campo  dell'edilizia  economica  e  popolare;  3) la legge  &#13;
 impugnata non si estende agli alberghi poiché la Corte  costituzionale  &#13;
 su  questo  punto  ha  già dichiarato la illegittimità delle leggi di  &#13;
 proroga.                                                                 &#13;
     Quanto all'art. 15 dello Statuto  siciliano,  la  difesa  regionale  &#13;
 asserisce  che  esso  non risulta violato poiché la Regione ha preso a  &#13;
 proprio carico gli oneri,  derivanti  dalle  minori  entrate  comunali,  &#13;
 proprio   in   ossequio   alla   sentenza   1965,   n.  2  della  Corte  &#13;
 costituzionale.                                                          &#13;
     3.  -  Il  Commissario dello Stato ha depositato il 20 gennaio 1966  &#13;
 una breve memoria: in essa si ricorda che recenti  sentenze  di  questa  &#13;
 Corte  hanno dichiarato l'incostituzionalità di leggi regionali di cui  &#13;
 quella denunciata costituisce  una  proroga;  inoltre  si  rileva,  con  &#13;
 richiamo  alla  sentenza  n.  90 del 1965, l'inadeguatezza del sistema,  &#13;
 scelto dalla Regione, di concedere soltanto, a  compenso  delle  minori  &#13;
 entrate comunali, riduzioni d'eventuali debiti dei comuni.<diritto>Considerato in diritto</diritto>:                          &#13;
     1. - L'art. 1 della legge impugnata proroga al 1968 le agevolazioni  &#13;
 tributarie  introdotte  in materia di edilizia non di lusso dalle leggi  &#13;
 regionali del 1954, nn. 11 e 37, e successive proroghe e modifiche.      &#13;
     Queste leggi, uniformandosi alla legislazione dello Stato,  avevano  &#13;
 esonerato  per  25  anni  i  contribuenti  della  Regione dal pagamento  &#13;
 dell'imposta e delle sovrimposte sui  fabbricati  e  ridotto  a  misura  &#13;
 fissa  il  tasso, normalmente variabile, di alcune imposte erariali (di  &#13;
 registro, ipotecarie, ecc.) e di ricchezza mobile.                       &#13;
     La denuncia, rivolta genericamente contro l'art.  1,  si  riferisce  &#13;
 sia a quell'esonero sia a questa riduzione.                              &#13;
     Tuttavia,  quanto all'esonero venticinquennale, prorogato dall'art.  &#13;
 1 col  rinvio  all'art.  5  della  legge  regionale  n.  11  del  1954,  &#13;
 l'impugnazione  non  è  fondata rispetto agli edifici non destinati ad  &#13;
 albergo: la legislazione dello Stato contiene una norma  analoga  (art.  &#13;
 43 del D.L. 15 marzo 1965, n. 124; art. 1 della legge del 1965, n. 431)  &#13;
 dimodoché  non  può  vedersi  violazione  dell'art.  36 dello Statuto  &#13;
 siciliano.                                                               &#13;
     La riduzione delle altre imposte alla misura fissa contrasta invece  &#13;
 col tipo di agevolazioni contenute nelle leggi dello Stato,  cioè  nel  &#13;
 predetto D.L. del 1965, n.  124, e nella legge del 1965, n. 431: queste  &#13;
 leggi  infatti non prevedono agevolazioni relativamente ad alcune delle  &#13;
 imposte, a cui si riferisce la legge regionale,  o  prevedono  semplici  &#13;
 riduzioni  di aliquote (imposte sui trasferimenti e sui conferimenti in  &#13;
 società). Poiché il sistema di imposizione a  tassa  fissa,  adottato  &#13;
 dalla  legge  regionale,  risponde  a  un tipo di tassazione diverso da  &#13;
 quello tuttora vigente nelle leggi dello Stato, il contrasto con l'art.  &#13;
 36 dello Statuto siciliano risulta evidente.                             &#13;
     Questa Corte ha da tempo fissato il  principio  che  la  competenza  &#13;
 legislativa  in  materia  tributaria  appartenga  alla Regione solo nei  &#13;
 limiti del rispetto, per ogni singolo tributo, del tipo  di  tassazione  &#13;
 vigente  nell'ordinamento dello Stato all'epoca dell'applicazione della  &#13;
 legge regionale.                                                         &#13;
     2. - L'art. 2 della legge impugnata riduce  l'imposta  comunale  di  &#13;
 consumo  sui  materiali  da costruzione dell'85 per cento per il 1967 e  &#13;
 dell'80 per cento  per  il  1968,  mentre  la  riduzione  prevista  dal  &#13;
 legislatore statale (art. 45 del D.L. del 1965, n. 124, e 1 della legge  &#13;
 1965, n. 431) è limitata al 20 per cento. Anche questa norma regionale  &#13;
 contrasta  con  l'art.  36 dello Statuto siciliano.  La Regione infatti  &#13;
 può  discostarsi,  nella  misura  delle  riduzioni  d'imposta,   dalla  &#13;
 legislazione  dello  Stato,  ma  la  differenza quantitativa tra le due  &#13;
 legislazioni, quando è sensibile come è avvenuto in questo  caso,  si  &#13;
 traduce  in  differenza  qualitativa e pertanto in manifesto privilegio  &#13;
 dei contribuenti d'una Regione rispetto a tutti gli altri.               &#13;
     Dato ciò, nella presente occasione non occorre esaminare,  benché  &#13;
 il  problema meriti particolare attenzione, se una potestà legislativa  &#13;
 regionale, in materia di tributi locali, sia  in  generale  compatibile  &#13;
 con  l'autonomia finanziaria e amministrativa attribuita ai Comuni e ai  &#13;
 Consorzi dall'art. 15 dello Statuto siciliano.                           &#13;
     3. - La illegittimità dell'art. 2 trascina con sé l'art. 4 che vi  &#13;
 è strettamente collegato.                                               &#13;
     4.  -  La  denuncia  di  incostituzionalità  non colpisce, invece,  &#13;
 l'art. 3 della legge regionale né l'art. 1 nella parte in cui  proroga  &#13;
 e  modifica  gli  artt.  1  e  2  della legge regionale del 1963, n. 4:  &#13;
 infatti tali norme, stabilendo  una  certa  proporzione  fra  i  locali  &#13;
 destinati   a   negozi   o   ad   altri   usi   e  l'intera  superficie  &#13;
 dell'imponibile, si discostano  dalla  legislazione  dello  Stato  solo  &#13;
 relativamente  agli  stabili  situati  in Comuni con popolazione fino a  &#13;
 30.000 abitanti; e  per  di  più  non  se  ne  discostano  in  maniera  &#13;
 sensibile.</testo>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  la  illegittimità costituzionale dell'art. 1 della legge  &#13;
 regionale siciliana approvata il 15 giugno 1965 (contenente proroga  di  &#13;
 agevolazioni  tributarie  in  materia  edilizia)  salve le parti in cui  &#13;
 proroga per gli edifici non destinati ad  albergo  le  norme  contenute  &#13;
 nell'art.  5 della legge regionale 28 aprile 1954, n. 11, e negli artt.  &#13;
 1 e 2 della legge regionale 11 gennaio 1963, n. 4;                       &#13;
     dichiara inoltre l'illegittimità costituzionale degli artt. 2 e  4  &#13;
 della predetta legge regionale approvata il 15 giugno 1965.              &#13;
     Cosi deciso in Roma, nella sede della Corte costituzionale, Palazzo  &#13;
 della Consulta, il 3 marzo 1966.                                         &#13;
                                   GASPARE AMBROSINI - GIUSEPPE CASTELLI  &#13;
                                   AVOLIO  -  ANTONINO  PAPALDO - NICOLA  &#13;
                                   JAEGER - GIOVANNI CASSANDRO -  BIAGIO  &#13;
                                   PETROCELLI  -  ANTONIO  MANCA  - ALDO  &#13;
                                   SANDULLI - GIUSEPPE BRANCA -  MICHELE  &#13;
                                   FRAGALI   -   GIUSEPPE   CHIARELLI  -  &#13;
                                   GIUSEPPE  VERZÌ -  GIOVANNI  BATTISTA  &#13;
                                   BENEDETTI     -    FRANCESCO    PAOLO  &#13;
                                   BONIFACIO.</dispositivo>
  </pronuncia_testo>
</pronuncia>
