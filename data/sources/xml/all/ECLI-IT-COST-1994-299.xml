<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1994</anno_pronuncia>
    <numero_pronuncia>299</numero_pronuncia>
    <ecli>ECLI:IT:COST:1994:299</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>CASAVOLA</presidente>
    <relatore_pronuncia>Francesco Guizzi</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>04/07/1994</data_decisione>
    <data_deposito>13/07/1994</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: prof. Francesco Paolo CASAVOLA; &#13;
 Giudici: prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo &#13;
 CHELI, dott. Renato GRANATA, prof. Giuliano VASSALLI, prof. &#13;
 Francesco GUIZZI, prof. Cesare MIRABELLI, prof. Fernando &#13;
 SANTOSUOSSO, avv. Massimo VARI, dott. Cesare RUPERTO;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi di legittimità costituzionale dell'art. 4, comma quinto,    &#13;
 della  legge  23 dicembre 1992, n. 498 (Interventi urgenti in materia    &#13;
 di finanza pubblica), promossi con le seguenti ordinanze:  1)  n.  10    &#13;
 ordinanze  emesse  il  24  giugno  1993  dal Tribunale amministrativo    &#13;
 regionale del Lazio sui ricorsi  proposti  da  Bisesti  Salvatore  ed    &#13;
 altri  contro  il Ministero delle Poste e Telecomunicazioni ed altro,    &#13;
 iscritte ai nn. da 762 a 771 del registro ordinanze 1993 e pubblicate    &#13;
 nella Gazzetta Ufficiale della Repubblica n. 1, prima serie speciale,    &#13;
 dell'anno 1994; 2) ordinanza emessa il 31 marzo  1993  dal  Tribunale    &#13;
 amministrativo  regionale  del  Lazio  sul  ricorso  proposto da Izzo    &#13;
 Aniello ed  altri  contro  l'Istituto  Nazionale  di  Previdenza  dei    &#13;
 Dirigenti  di  Aziende  industriali,  iscritta  al n. 93 del registro    &#13;
 ordinanze 1994 e pubblicata nella Gazzetta Ufficiale della Repubblica    &#13;
 n. 12, prima serie speciale, dell'anno 1994;                             &#13;
    Visto l'atto di costituzione di Izzo Aniello ed altri nonché  gli    &#13;
 atti di intervento del Presidente del Consiglio dei ministri;            &#13;
    Udito  nella  camera  di  consiglio  del 27 aprile 1994 il Giudice    &#13;
 relatore Francesco Guizzi.                                               &#13;
    Ritenuto che numerosi dipendenti del Ministero delle Poste e delle    &#13;
 Telecomunicazioni hanno chiesto, con dieci diversi ricorsi diretti al    &#13;
 Tribunale amministrativo regionale del  Lazio,  la  declaratoria  del    &#13;
 loro  diritto  a  fruire del beneficio dell'anzianità convenzionale,    &#13;
 attribuito dall'art. 1 della legge 24 maggio 1970, n.  336  (Norme  a    &#13;
 favore  dei  dipendenti  civili  dello  Stato  ed  Enti  pubblici  ex    &#13;
 combattenti  ed  assimilati),  agli  ex  combattenti   (e   categorie    &#13;
 equiparate),   e  il  conseguente  diritto  al  computo  della  detta    &#13;
 anzianità nella determinazione della retribuzione, ricostruita sulla    &#13;
 base di disposizioni di carattere generale,  quali  quelle  contenute    &#13;
 negli accordi nazionali di lavoro;                                       &#13;
      che  il  divieto  di  applicazione  per  più  di  una volta dei    &#13;
 benefici  combattentistici,  stabilito  dall'art.  3  della  legge  9    &#13;
 ottobre   1971,   n.  824  (Norme  di  attuazione,  modificazione  ed    &#13;
 integrazione della l. 24 maggio 1970, n.  336,  concernente  norme  a    &#13;
 favore  dei dipendenti dello Stato ed enti pubblici ex combattenti ed    &#13;
 assimilati), circoscrivibile soltanto  all'ipotesi  di  modificazione    &#13;
 della situazione di carriera, non ricorreva nella specie;                &#13;
      che,  successivamente  alla proposizione dei ricorsi, è entrata    &#13;
 in vigore la legge 23 dicembre 1992, n. 498  (Interventi  urgenti  in    &#13;
 materia  di  finanza  pubblica)  la  quale,  all'art.  4, comma 5, ha    &#13;
 stabilito che non si  dovrà  procedere  al  computo  delle  maggiori    &#13;
 anzianità previste dalla legge n. 336 del 1970 in sede di successiva    &#13;
 ricostruzione   economica   prevista  da  disposizioni  di  carattere    &#13;
 generale e che di conseguenza si dovrà procedere  al  riassorbimento    &#13;
 degli eventuali migliori trattamenti già in godimento;                  &#13;
      che  il TAR del Lazio ha, siccome rilevante, sollevato questione    &#13;
 di costituzionalità della detta norma, in riferimento agli artt. 3 e    &#13;
 36 della Costituzione, in  quanto  il  ricorso  andrebbe  accolto  in    &#13;
 ossequio ad una concorde e consolidata giurisprudenza amministrativa,    &#13;
 se  non  vi  ostasse il disposto dell'art. 4, comma 5, della legge n.    &#13;
 498 del 1992;                                                            &#13;
      che in base alla cennata giurisprudenza l'anzianità di servizio    &#13;
 attribuita agli ex combattenti (e categorie equiparate)  dalla  legge    &#13;
 n.  336  del  1970  non  differirebbe  dall'anzianità  derivante dal    &#13;
 servizio effettivamente prestato e spiegherebbe i suoi effetti  anche    &#13;
 nel  computo  delle  retribuzioni  da rideterminare in forza di nuovi    &#13;
 accordi nazionali di lavoro;                                             &#13;
      che la norma sarebbe in contrasto con gli artt.  3  e  36  della    &#13;
 Costituzione   perché,   non   diversamente   da   quanto  la  Corte    &#13;
 costituzionale ha deciso con la sentenza n.  39  del  1993,  essa  ha    &#13;
 determinato   una   ingiustificata   disparità  di  trattamento  tra    &#13;
 dipendenti che si trovano nella stessa condizione di  ex  combattenti    &#13;
 (e  categorie equiparate), essendosi attribuito ad alcuni e negato ad    &#13;
 altri il beneficio;                                                      &#13;
      che nel nostro caso la denunciata disparità non potrebbe  dirsi    &#13;
 sanata   per   effetto   della   disposizione   che   stabilisce   il    &#13;
 riassorbimento dei migliori trattamenti  in  godimento  da  parte  di    &#13;
 taluni,   atteso   che   la   situazione   di   eguaglianza  potrebbe    &#13;
 ristabilirsi, non senza incertezze, per lo meno in un arco  di  tempo    &#13;
 ampio e consistente;                                                     &#13;
      che  con  un ulteriore ricorso, esaminato da altra sezione dello    &#13;
 stesso  Tribunale   amministrativo   regionale,   alcuni   dipendenti    &#13;
 dell'Istituto  Nazionale  di  Previdenza  dei  dirigenti  di  aziende    &#13;
 industriali (I.N.P.D.A.I.)  hanno  fatto  un'uguale  richiesta  e  il    &#13;
 Tribunale  ha,  del pari, sollevato la questione di costituzionalità    &#13;
 dell'art. 4, comma 5, della legge  23  dicembre  1992,  n.  498,  con    &#13;
 riferimento,  questa volta, non solo ai parametri costituzionali già    &#13;
 indicati nelle precedenti ordinanze di rimessione (artt. 3 e  36)  ma    &#13;
 anche  a  quelli indicati dai ricorrenti e riferibili agli artt. 97 e    &#13;
 101 e ss. della Costituzione, in quanto  la  norma,  oltre  a  creare    &#13;
 l'ingiustificata  disparità  di  trattamento già evidenziata con le    &#13;
 precedenti  ordinanze  di  rimessione,  sottrarrebbe  al  giudice  il    &#13;
 compito  istituzionale  di  interpretare ed applicare la legge (artt.    &#13;
 24, 101, 104, 111 e 113 della Costituzione);                             &#13;
      che la sostituzione  del  legislatore  all'interprete  (giudice)    &#13;
 attraverso   l'emanazione   di   una  disposizione  dissimulata  come    &#13;
 interpretativa ma, in realtà, abrogativa con efficacia  retroattiva,    &#13;
 sarebbe   viziata   dalla   illegittimità   perché   interferirebbe    &#13;
 nell'esercizio  delle  funzioni  attribuite  ad   un   altro   potere    &#13;
 costituzionale (onde si potrebbe parlare di uno sviamento strumentale    &#13;
 della funzione legislativa);                                             &#13;
      che  la  norma impugnata, inoltre, violerebbe anche il principio    &#13;
 (peraltro, secondo il rimettente, non assoluto) in virtù  del  quale    &#13;
 la  legge deve disporre solo per l'avvenire (art. 11 delle preleggi),    &#13;
 in tal modo  frustrando  l'affidamento  di  una  vasta  categoria  di    &#13;
 cittadini  nella  certezza giuridica (sentt. n. 255 del 1990, 822 del    &#13;
 1988 e 349 del 1985);                                                    &#13;
      che, in relazione a questo ultimo giudizio, si sono costituiti i    &#13;
 ricorrenti i quali hanno chiesto la  declaratoria  di  illegittimità    &#13;
 costituzionale della norma impugnata;                                    &#13;
      che  è intervenuto il Presidente del Consiglio dei ministri per    &#13;
 mezzo dell'Avvocatura generale dello Stato, la quale ha concluso  per    &#13;
 l'inammissibilità o l'infondatezza della questione prospettata.         &#13;
    Considerato  che  le  ordinanze  sollevano  un'unica  questione e,    &#13;
 pertanto, vanno riunite;                                                 &#13;
      che  i  primi  due  profili  della  stessa  (natura   falsamente    &#13;
 interpretativa,  sostanzialmente  innovativa,  della norma impugnata;    &#13;
 ingiustificata disparità di trattamento dei destinatari della  norma    &#13;
 in  ragione  del  possibile  parziale  insuccesso  del  meccanismo di    &#13;
 riassorbimento dei maggiori trattamenti già in godimento) sono stati    &#13;
 esaminati  dalla  Corte  nell'udienza pubblica del 22 febbraio 1994 e    &#13;
 decisi con la sentenza n. 153 del 1994;                                  &#13;
      che il restante profilo già prospettato  negli  stessi  termini    &#13;
 dalla  parte  privata costituitasi nel giudizio a quo, e davanti alla    &#13;
 Corte, è stato considerato e respinto in quella occasione  (come  da    &#13;
 sentenza n. 153 del 1994);                                               &#13;
      che, pertanto, difettano veri nuovi profili;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i  giudizi;  dichiara  la  manifesta  infondatezza  della    &#13;
 questione di legittimità costituzionale dell'art. 4, comma 5,  della    &#13;
 legge  23  dicembre  1992,  n.  498 (Interventi urgenti in materia di    &#13;
 finanza pubblica), sollevata, in riferimento agli artt. 3, 36,  97  e    &#13;
 101  della  Costituzione,  dal Tribunale amministrativo regionale del    &#13;
 Lazio con le ordinanze in epigrafe.                                      &#13;
    Così deciso in  Roma,  nella  sede  della  Corte  costituzionale,    &#13;
 Palazzo della Consulta, 4 luglio 1994.                                   &#13;
                        Il Presidente: CASAVOLA                           &#13;
                         Il redattore: GUIZZI                             &#13;
                       Il cancelliere: DI PAOLA                           &#13;
    Depositata in cancelleria il 13 luglio 1994.                          &#13;
               Il direttore della cancelleria: DI PAOLA</dispositivo>
  </pronuncia_testo>
</pronuncia>
