<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>2001</anno_pronuncia>
    <numero_pronuncia>20</numero_pronuncia>
    <ecli>ECLI:IT:COST:2001:20</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>SANTOSUOSSO</presidente>
    <relatore_pronuncia>Guido Neppi Modona</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>05/01/2001</data_decisione>
    <data_deposito>23/01/2001</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: Fernando SANTOSUOSSO; &#13;
 Giudici: Massimo VARI, Cesare RUPERTO, Riccardo CHIEPPA, Gustavo &#13;
ZAGREBELSKY, Valerio ONIDA, Carlo MEZZANOTTE, Fernanda CONTRI, Guido &#13;
NEPPI MODONA, Piero Alberto CAPOTOSTI, Annibale MARINI, Franco BILE;</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>Ordinanza</titolo>nel giudizio di legittimità costituzionale degli artt. 11 e 12 della &#13;
legge  8  febbraio  1948, n. 47 (Disposizioni sulla stampa) e 596-bis &#13;
del  codice  penale, promosso, nell'ambito di un procedimento civile, &#13;
con  ordinanza  emessa  il  26  ottobre  1999  dal tribunale di Roma, &#13;
iscritta  al  n. 109  del  registro ordinanze 2000 e pubblicata nella &#13;
Gazzetta  Ufficiale  della  Repubblica  n. 13  -  1ª serie speciale - &#13;
dell'anno 2000. &#13;
    Visti  l'atto  di costituzione dell'attore nel procedimento a quo &#13;
nonché  l'atto  di  intervento  del  Presidente  del  Consiglio  dei &#13;
ministri; &#13;
    Udito  nell'udienza  pubblica  del  12  dicembre  2000 il giudice &#13;
relatore Guido Neppi Modona; &#13;
    Uditi l'avvocato Elio Ripoli per la parte costituita e l'avvocato &#13;
dello  Stato  Giuseppe  Albenzio  per il Presidente del Consiglio dei &#13;
ministri. &#13;
    Ritenuto  che  il  tribunale di Roma ha sollevato, in riferimento &#13;
all'art   68,   primo   comma,   della   Costituzione,  questione  di &#13;
legittimità  costituzionale  degli  artt.  11  e  12  della  legge 8 &#13;
febbraio 1948, n. 47 (Disposizioni sulla stampa) e 596-bis del codice &#13;
penale,  in  quanto interpretati nel senso "della loro applicabilità &#13;
nei confronti del direttore ed editore del giornale, anche ai casi in &#13;
cui  l'autore  delle opinioni sia ammesso alla garanzia dell'art. 68, &#13;
primo comma, Cost."; &#13;
        che  il  rimettente  premette  che  nella  causa  civile  per &#13;
risarcimento dei danni, intentata da persona che si ritiene diffamata &#13;
da  un  articolo  apparso sul quotidiano "il Manifesto" nei confronti &#13;
dell'autore  della  pubblicazione,  del  direttore responsabile e del &#13;
legale  rappresentante  della  società  editrice,  il  tribunale  ha &#13;
dichiarato con sentenza l'inammissibilità della domanda proposta nei &#13;
confronti  dell'autore  dell'articolo,  deputato  Nicola  Vendola,  a &#13;
seguito  della  delibera di insindacabilità pronunciata dalla Camera &#13;
dei deputati a norma dell'art. 68 della Costituzione; &#13;
        che   il   giudice   a   quo   rileva   che,  per  "costante" &#13;
interpretazione  giurisprudenziale,  nei casi di diffamazione a mezzo &#13;
stampa  permane  la  responsabilità del direttore e dell'editore del &#13;
giornale  "anche  in  presenza della causa di esonero riconosciuta al &#13;
parlamentare  ex  art.  68  c.  1  Cost.", in quanto la "oggettività &#13;
dell'illecito   penale   [...]  non  consente  il  venir  meno  della &#13;
responsabilità   per   omissione,   nell'ipotesi   in  cui  l'autore &#13;
dell'illecito  non  è  punito  per  l'applicabilità della specifica &#13;
esenzione soggettiva (e funzionale) prevista dall'art. 68 Cost."; &#13;
        che,    ad    avviso    del    rimettente,   tale   indirizzo &#13;
giurisprudenziale si pone in contrasto con l'art. 68 Cost., in quanto &#13;
"di  fatto  tende  ad  escludere  o  a rendere oltremodo difficile la &#13;
possibilità  per  il parlamentare di esprimere le proprie opinioni a &#13;
mezzo della stampa"; &#13;
        che ne deriverebbe una evidente contraddizione, perché da un &#13;
lato  viene  prevista  una  prerogativa  per  le opinioni espresse in &#13;
connessione  con l'esercizio della funzione parlamentare, dall'altro, &#13;
affermandosi "la responsabilità dei veicoli di divulgazione" di tali &#13;
opinioni,  si  creano  ostacoli  alla  diffusione  del  pensiero  del &#13;
parlamentare fuori dal contesto del Parlamento; &#13;
        che si è costituito nel giudizio l'attore nel procedimento a &#13;
quo  chiedendo  che  la  questione  sia  dichiarata  inammissibile  o &#13;
infondata; &#13;
        che  la  parte  costituita - premesso che avverso la sentenza &#13;
parziale   pronunciata  dal  giudice  rimettente  nei  confronti  del &#13;
parlamentare  autore  dell'articolo  diffamatorio  è  stato proposto &#13;
appello,  con il quale, tra l'altro, viene contestata la legittimità &#13;
della  deliberazione  di  insindacabilità  votata  dalla Camera e si &#13;
sollecita la Corte di appello a sollevare conflitto di attribuzione - &#13;
rileva,  nel  merito, che il sacrificio della giurisdizione derivante &#13;
dalla  prerogativa  soggettiva  dell'art.  68  della Costituzione non &#13;
assicura,  contrariamente  a  quanto  asserisce  il  tribunale,  "una &#13;
copertura  costituzionale  delle opinioni diffamatorie, bensì offre, &#13;
solo  a  favore  del  parlamentare,  una  astensione  dall'intervento &#13;
sanzionatorio   che  non  elide  la  illegittimità  oggettiva  della &#13;
condotta,   né   sopprime   il  dovere  di  controllo  dei  soggetti &#13;
responsabili ai sensi delle norme denunciate"; &#13;
        che  l'accoglimento della censura estenderebbe inopinatamente &#13;
la  insindacabilità a soggetti estranei all'esercizio delle funzioni &#13;
che   costituiscono  il  fondamento  della  prerogativa  stessa,  con &#13;
evidente  "degrado  della  dialettica  politica",  poiché  la libera &#13;
divulgazione   di   espressioni  o  concetti  diffamatori  "non  può &#13;
costituire materia di alcuna garanzia costituzionale"; &#13;
        che,  sotto  il  profilo  della rilevanza della questione, la &#13;
parte  conclude  che, ove la Corte di appello sollevasse il conflitto &#13;
di  attribuzione, la deliberazione della Camera non potrebbe sfuggire &#13;
all'annullamento  da  parte  della  Corte  costituzionale, essendo in &#13;
palese  contrasto  con  i  principi  enunciati in materia dalla Corte &#13;
stessa  circa  il  nesso  funzionale  tra  le  opinioni espresse e la &#13;
funzione parlamentare; &#13;
        che  sussiste,  pertanto,  un profilo di pregiudizialità, in &#13;
quanto l'annullamento della deliberazione della Camera determinerebbe &#13;
l'irrilevanza  della  questione  di  costituzionalità  sollevata dal &#13;
tribunale; &#13;
        che  è  intervenuto nel giudizio il Presidente del Consiglio &#13;
dei  ministri,  rappresentato e difeso dall'Avvocatura generale dello &#13;
Stato,  chiedendo che la questione venga dichiarata inammissibile, o, &#13;
in subordine, infondata; &#13;
        che, secondo l'Avvocatura, le censure prospettate dal giudice &#13;
rimettente   "non   discendono   dal  diritto  vivente  formatosi  in &#13;
riferimento  ad una normativa ordinaria, che si porrebbe, perciò, in &#13;
contrasto   con  la  norma  costituzionale",  ma  si  fondano  "sulla &#13;
riconduzione   della   guarentigia  costituzionale  al  novero  delle &#13;
immunità   ed   alla  riconduzione  di  queste  ultime  (secondo  un &#13;
orientamento prevalente, seppur non unitario) alle cause personali di &#13;
esclusione  della  pena (perciò facenti eccezione all'art. 3 c.p.)": &#13;
di conseguenza, la questione sarebbe inammissibile perché tendente a &#13;
un   "intervento   sul   parametro  valutativo  piuttosto  che  sulla &#13;
disposizione secondaria"; &#13;
        che  in  sede di discussione il difensore della parte privata &#13;
costituita e l'avvocato dello Stato hanno ulteriormente sviluppato le &#13;
ragioni  a  sostegno della inammissibilità e dell'infondatezza della &#13;
questione di legittimità costituzionale. &#13;
    Considerato  che  il  giudice  a  quo  rileva che, per "costante" &#13;
interpretazione  giurisprudenziale,  in  caso di diffamazione a mezzo &#13;
stampa  permane  la  responsabilità  del  direttore  del  giornale e &#13;
dell'editore anche quando nei confronti del parlamentare autore della &#13;
pubblicazione  sia  intervenuta  la deliberazione di insindacabilità &#13;
della  Camera  di  appartenenza  a  norma  dell'art. 68, primo comma, &#13;
Cost.,  e  lamenta  che  tale indirizzo giurisprudenziale, basato sul &#13;
presupposto  che  l'insindacabilità  sia  una  causa  soggettiva  di &#13;
esenzione  dalla  responsabilità, si pone in contrasto con l'art. 68 &#13;
Cost.,   in  quanto  di  fatto  inciderebbe  sulla  possibilità  del &#13;
parlamentare di esprimere le sue opinioni a mezzo della stampa; &#13;
        che  il  rimettente vorrebbe quindi estendere l'esonero dalla &#13;
responsabilità  al direttore del giornale e all'editore, ma non trae &#13;
le  conseguenze  applicative  dell'interpretazione  che  egli  stesso &#13;
considera  conforme  al  parametro  costituzionale  evocato,  a causa &#13;
dell'esistenza    della    "costante"    giurisprudenza   che   segue &#13;
l'interpretazione da lui non condivisa; &#13;
        che,   contrariamente   a  quanto  il  rimettente  mostra  di &#13;
ritenere, nulla osta a che il giudice a quo adotti egli stesso quella &#13;
interpretazione  che,  a  suo avviso, gli consentirebbe di superare i &#13;
prospettati dubbi di costituzionalità; &#13;
        che,  in  definitiva,  il  rimettente  ha sottoposto a questa &#13;
Corte  esclusivamente  una questione di interpretazione dell'art. 68, &#13;
primo comma, Cost., e non già una questione concernente il contrasto &#13;
tra  il  significato  da attribuire alle norme ordinarie da applicare &#13;
nel giudizio a quo e il parametro costituzionale evocato; &#13;
        che    la   questione   deve   pertanto   essere   dichiarata &#13;
manifestamente inammissibile.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi &#13;
                       LA CORTE COSTITUZIONALE &#13;
    Dichiara   la   manifesta  inammissibilità  della  questione  di &#13;
legittimità  costituzionale  degli  artt.  11  e  12  della  legge 8 &#13;
febbraio 1948, n. 47 (Disposizioni sulla stampa) e 596-bis del codice &#13;
penale,  sollevata,  in  riferimento  all'art. 68, primo comma, della &#13;
Costituzione, dal tribunale di Roma, con l'ordinanza in epigrafe. &#13;
    Così  deciso  in  Roma,  nella  sede della Corte costituzionale, &#13;
Palazzo della Consulta, il 5 gennaio 2001. &#13;
                     Il Presidente: Santosuosso &#13;
                     Il redattore: Neppi Modona &#13;
                      Il cancelliere: Di Paola &#13;
    Depositata in cancelleria il 23 gennaio 2001. &#13;
              Il direttore della cancelleria: Di Paola</dispositivo>
  </pronuncia_testo>
</pronuncia>
