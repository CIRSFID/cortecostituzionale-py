<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1983</anno_pronuncia>
    <numero_pronuncia>294</numero_pronuncia>
    <ecli>ECLI:IT:COST:1983:294</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>ELIA</presidente>
    <relatore_pronuncia>Arnaldo Maccarone</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>28/09/1983</data_decisione>
    <data_deposito>05/10/1983</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. LEOPOLDO ELIA, Presidente - Prof. &#13;
 GUGLIELMO ROEHRSSEN - Avv. ORONZO REALE - Dott. BRUNETTO BUCCIARELLI &#13;
 DUCCI - Avv. ALBERTO MALAGUGINI - Prof. LIVIO PALADIN - Dott. ARNALDO &#13;
 MACCARONE - Prof. ANTONIO LA PERGOLA - Prof. VIRGILIO ANDRIOLI - Dott. &#13;
 FRANCESCO SAJA - Prof. GIOVANNI CONSO - Prof. ETTORE GALLO, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nei giudizi riuniti di legittimità costituzionale degli artt. 2 e 3,  &#13;
 lett.  b,  della  legge  8  marzo  1968, n. 152 (Nuove norme in materia  &#13;
 previdenziale per il personale degli  Enti  locali),  promossi  con  le  &#13;
 ordinanze  emesse il 1 aprile 1982 dal Pretore di Venezia, il 26 maggio  &#13;
 1982 dal Tribunale di L'Aquila,  il  7  maggio  1982  dal  TAR  per  la  &#13;
 Lombardia  -  Sezione  staccata di Brescia - ed il 24 novembre 1981 dal  &#13;
 TAR per la Sardegna, rispettivamente iscritte ai nn. 364,  499,  708  e  &#13;
 779  del  registro ordinanze 1982 e pubblicate nella Gazzetta Ufficiale  &#13;
 della Repubblica nn. 283 e 351 dell'anno 1982 e nn. 74 e  88  dell'anno  &#13;
 1983.                                                                    &#13;
    Visti  l'atto  di  costituzione  di  Valente  Assunta  e gli atti di  &#13;
 intervento del Presidente del Consiglio dei Ministri;                    &#13;
    udito nella camera di  consiglio  del  22  giugno  1983  il  Giudice  &#13;
 relatore Arnaldo Maccarone.                                              &#13;
    Ritenuto  che  con  ordinanza 1 aprile 1982 il Pretore di Venezia ha  &#13;
 sollevato, in relazione all'art. 3  Cost.,  questione  di  legittimità  &#13;
 costituzionale  dell'art.  3  lett. b) della legge 8 marzo 1968 n. 152,  &#13;
 secondo cui il riconoscimento dell'indennità premio di fine servizio a  &#13;
 favore dei figli maggiorenni dei dipendenti  di  enti  locali  iscritti  &#13;
 all'INADEL  deceduti  è  subordinato  alle  condizioni di inabilità a  &#13;
 proficuo lavoro, di nullatenenza, di vivenza a carico e dello stato  di  &#13;
 nubile o vedova per le orfane;                                           &#13;
    che,  a  sostegno  della censura il Pretore prospetta la irrazionale  &#13;
 disparità di trattamento che tale disciplina concreterebbe rispetto  a  &#13;
 quanto stabilito dall'art. 7 della legge 29 aprile 1976 n. 177 circa il  &#13;
 riconoscimento  dell'indennità  di  buonuscita  a  favore degli orfani  &#13;
 maggiorenni   dei    dipendenti    statali,    che    avviene    invece  &#13;
 indipendentemente dalle dette condizioni;                                &#13;
    che,  con  ordinanza  26  maggio  1982  il  Tribunale di L'Aquila ha  &#13;
 sollevato, in riferimento all'art. 3 Cost., questione  di  legittimità  &#13;
 costituzionale  dell'art. 2 della legge 8 marzo 1968 n. 152 nella parte  &#13;
 in cui  stabilisce  che  i  lavoratori  dipendenti  degli  Enti  locali  &#13;
 iscritti  all'INADEL  conseguono  il  diritto alla indennità premio di  &#13;
 fine servizio con almeno 25 anni di servizio e due anni  di  iscrizione  &#13;
 qualora la cessazione del rapporto avvenga per dimissioni;               &#13;
     che,  secondo  il giudice "a quo", dette condizioni concreterebbero  &#13;
 una irrazionale disparità di trattamento rispetto  a  quanto  previsto  &#13;
 dall'art.  7  legge  29  aprile  1976  n.  177,  secondo  cui,  per  la  &#13;
 concessione dell'indennità  di  buonuscita  a  favore  dei  dipendenti  &#13;
 statali  è  richiesta  come  unica condizione l'iscrizione per un anno  &#13;
 all'apposito fondo;                                                      &#13;
     che analoghe questioni  sono  state  sollevate  con  l'ordinanza  7  &#13;
 maggio 1982 del TAR della Lombardia, sezione staccata di Brescia, e con  &#13;
 l'ordinanza 24 novembre 1981 del TAR della Sardegna;                     &#13;
     che  nei  giudizi provenienti dal Pretore di Venezia, dal TAR della  &#13;
 Lombardia e dal TAR della Sardegna si è costituito il  Presidente  del  &#13;
 Consiglio  dei  Ministri,  eccependo  l'irrilevanza  della questione in  &#13;
 quello proveniente dal TAR della Lombardia, e sostenendone comunque  in  &#13;
 tutti l'infondatezza;                                                    &#13;
     che  nel  giudizio  proveniente  dal  Tribunale  di  L'Aquila si è  &#13;
 costituita  la  parte  privata,  facendo   proprie   le   tesi   svolte  &#13;
 nell'ordinanza di rinvio;                                                &#13;
     che   i   giudizi  vanno  riuniti  per  l'identità  o  la  stretta  &#13;
 connessione delle questioni sollevate.                                   &#13;
     Considerato che l'eccezione pregiudiziale sollevata dall'Avvocatura  &#13;
 non è fondata giacché il giudice "a quo" ha sufficientemente motivato  &#13;
 sul punto della rilevanza senza incorrere in vizi logici, onde, secondo  &#13;
 la costante giurisprudenza di questa Corte, il relativo giudizio sfugge  &#13;
 a sindacato in questa sede;                                              &#13;
     che  questa  Corte, con la sent. n. 46 del 1983, ha già avuto modo  &#13;
 di affermare che, pur apparendo le due indennità suddette equivalenti,  &#13;
 per finalità e per struttura, non è possibile istituire un  raffronto  &#13;
 fra loro non sussistendo, fra le categorie considerate, sia riguardo al  &#13;
 trattamento economico in attività di servizio, sia riguardo al sistema  &#13;
 contributivo  preordinato  al trattamento di quiescenza, quella parità  &#13;
 di  situazioni  che  è  il  presupposto  per  la   valutazione   della  &#13;
 legittimità   costituzionale   di  una  diversità  di  disciplina  in  &#13;
 riferimento all'art. 3 Cost.;                                            &#13;
    che non sono stati addotti e  non  sussistono  comunque  motivi  che  &#13;
 possano indurre la Corte a discostarsi da tale giurisprudenza.           &#13;
   Visti  gli artt. 26, secondo comma, della legge 11 marzo 1953 n. 87 e  &#13;
 9, secondo comma, delle Norme integrative per i  giudizi  innanzi  alla  &#13;
 Corte costituzionale.</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
    1)   Dichiara   la   manifesta   infondatezza   della  questione  di  &#13;
 legittimità costituzionale dell'art. 3 lett. b) della  legge  8  marzo  &#13;
 1968  n.  152 sollevata in riferimento all'art. 3 Cost. con l'ordinanza  &#13;
 del Pretore di Venezia indicata in epigrafe;                             &#13;
    2)  dichiara  la   manifesta   infondatezza   delle   questioni   di  &#13;
 legittimità  costituzionale dell'art. 2 della menzionata legge 8 marzo  &#13;
 1968 n.  152,  sollevate  in  riferimento  all'art.  3  Cost.,  con  le  &#13;
 ordinanze  del  Tribunale  di  L'Aquila,  del  Tribunale amministrativo  &#13;
 regionale della Lombardia, Sezione staccata di Brescia, e del Tribunale  &#13;
 amministrativo regionale della Sardegna indicate in epigrafe.            &#13;
    Così deciso in Roma, in camera di consiglio, nella sede della Corte  &#13;
 costituzionale, Palazzo della Consulta, il 28 settembre 1983.            &#13;
                                   F.to:  LEOPOLDO  ELIA   -   GUGLIELMO  &#13;
                                   ROEHRSSEN  -  ORONZO REALE - BRUNETTO  &#13;
                                   BUCCIARELLI    DUCCI    -     ALBERTO  &#13;
                                   MALAGUGINI  - LIVIO PALADIN - ARNALDO  &#13;
                                   MACCARONE  -  ANTONIO  LA  PERGOLA  -  &#13;
                                   VIRGILIO  ANDRIOLI - FRANCESCO SAJA -  &#13;
                                   GIOVANNI CONSO - ETTORE GALLO.         &#13;
                                   GIOVANNI VITALE - Cancelliere</dispositivo>
  </pronuncia_testo>
</pronuncia>
