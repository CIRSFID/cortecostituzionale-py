<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1967</anno_pronuncia>
    <numero_pronuncia>10</numero_pronuncia>
    <ecli>ECLI:IT:COST:1967:10</ecli>
    <tipologia_pronuncia>O</tipologia_pronuncia>
    <presidente>AMBROSINI</presidente>
    <relatore_pronuncia>Giovanni Cassandro</relatore_pronuncia>
    <redattore_pronuncia/>
    <data_decisione>01/02/1967</data_decisione>
    <data_deposito>04/02/1967</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: Prof. GASPARE AMBROSINI, Presidente - Prof. &#13;
 ANTONINO PAPALDO - Prof. NICOLA JAEGER - Prof. GIOVANNI CASSANDRO - &#13;
 Prof. BIAGIO PETROCELLI - Dott. ANTONIO MANCA - Prof. ALDO SANDULLI - &#13;
 Prof. GIUSEPPE BRANCA - Prof. MICHELE FRAGALI - Prof. COSTANTINO &#13;
 MORTATI - Prof. GIUSEPPE CHIARELLI - Dott. GIOVANNI BATTISTA BENEDETTI &#13;
 - Prof. FRANCESCO PAOLO BONIFACIO - Dott. LUIGI OGGIONI, Giudici,</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>ORDINANZA</titolo>nel giudizio promosso con "ricorso per conflitto costituzionale  di  &#13;
 attribuzione"  della  Giunta  regionale della Valle d'Aosta, in persona  &#13;
 del suo presidente avv.  Severino Caveri, contro il Consiglio regionale  &#13;
 della medesima Valle, nella persona del suo  presidente  pro-  tempore,  &#13;
 depositato  in  cancelleria  il 30 maggio 1966 ed iscritto al n. 14 dei  &#13;
 Registro  ricorsi  1966,  per  l'annullamento  dei  seguenti  atti  del  &#13;
 Consiglio  della Valle: convocazione del 20 maggio 1966, portante al n.  &#13;
 3 "sfiducia, revoca e sostituzione della Giunta  regionale  e  del  suo  &#13;
 Presidente";  deliberazione  e decisioni adottate in relazione al punto  &#13;
 suindicato dell'ordine del giorno nelle sedute del 23 e del  25  maggio  &#13;
 1966  (oggetto n. 21 del verbale del 23 maggio 1966 e n. 30 del verbale  &#13;
 del 25 maggio 1966); e, per quanto occorra, delle deliberazioni di  cui  &#13;
 agli oggetti nn. 16 e 17 (convalida e giuramento dei consiglieri Barmaz  &#13;
 e  Manganone),  n. 19 (accettazione delle dimissioni del Presidente del  &#13;
 Consiglio Marcoz e  del  vice  Presidente  Perrucon),  nn.    27  e  29  &#13;
 (elezione  di  un  nuovo  Presidente  e  di  un  nuovo vice Presidente,  &#13;
 rispettivamente   nelle   persone   dei   consiglieri    Montesano    e  &#13;
 Personnettaz).                                                           &#13;
     Udita  nella  camera  di consiglio del 19 gennaio 1967 la relazione  &#13;
 del Giudice Giovanni Cassandro;                                          &#13;
     Ritenuto, secondo quanto si legge nel ricorso, che il  Commissario,  &#13;
 nominato  dal  Presidente  del  Consiglio  dei  Ministri con decreto 18  &#13;
 maggio 1966, convocava il Consiglio della  Regione  per  il  23  maggio  &#13;
 1966,  fissandone  l'ordine  del  giorno,  contenente al n. 3 l'oggetto  &#13;
 "sfiducia, revoca e sostituzione  della  Giunta  regionale  e  del  suo  &#13;
 Presidente";                                                             &#13;
     che, nella seduta del 23 maggio 1966, il Presidente dell'Assemblea,  &#13;
 consigliere Personnettaz, dichiarava che dovesse procedersi alla revoca  &#13;
 e  sostituzione  del  Presidente  e  della Giunta ai sensi dell'art. 48  &#13;
 dello Statuto  speciale,  per  le  violazioni  di  legge  compiute  dal  &#13;
 Presidente  e  dalla  Giunta  medesimi, seguendo la procedura stabilita  &#13;
 dalla legge 10 febbraio 1953, n. 62;                                     &#13;
     che  il  Consiglio  regionale, nella seduta del 23 maggio 1966 e in  &#13;
 quella successiva del 25 dello  stesso  mese,  prendeva  atto  che  per  &#13;
 revocare  il  Presidente  e  la  Giunta  sarebbe  stata sufficiente, in  &#13;
 seconda convocazione, la presenza della metà più uno dei  consiglieri  &#13;
 in carica;                                                               &#13;
     che tali atti sarebbero stati già tali da ledere in modo attuale e  &#13;
 concreto  la sfera di competenza costituzionale della Giunta regionale,  &#13;
 non essendo previsto dallo Statuto che il Consiglio  possa  revocare  e  &#13;
 sostituire  la  Giunta,  tranne  che  nella  ipotesi dell'art. 48 dello  &#13;
 Statuto speciale che, nel caso, non ricorre;                             &#13;
     che si verificherebbe perciò l'ipotesi di conflitto costituzionale  &#13;
 tra poteri, poiché sia il Consiglio, sia  la  Giunta  regionale  della  &#13;
 Valle   d'Aosta   sono   entrambi   organi   competenti   a  dichiarare  &#13;
 definitivamente la volontà dei poteri a cui appartengono;               &#13;
     Considerato che il  ricorso  proposto  contro  il  Consiglio  della  &#13;
 Regione  Valle d'Aosta dalla Giunta non può dare adito, in alcun modo,  &#13;
 a un conflitto tra poteri dello Stato ai sensi degli  artt.  134  della  &#13;
 Costituzione,  37 e 38 della legge 11 marzo 1953, n. 87. Quale che sia,  &#13;
 infatti, l'interpretazione da dare alle norme ora  richiamate  e  quali  &#13;
 che  siano  di  conseguenza  i "poteri dello Stato" ai quali esse fanno  &#13;
 riferimento, è di tutta evidenza che non  possono  essere  considerati  &#13;
 "poteri  dello  Stato", né la Regione, né il Consiglio o la Giunta di  &#13;
 una Regione, anche  se  si  volessero  configurare  questi  due  organi  &#13;
 rispettivamente  titolari del potere legislativo e del potere esecutivo  &#13;
 della Regione. E altrettanto evidente è, di conseguenza,  che  né  il  &#13;
 Consiglio,  né la Giunta di una Regione possono essere ritenuti organi  &#13;
 competenti a dichiarare definitivamente la volontà di un potere  dello  &#13;
 Stato;</epigrafe>
    <testo/>
    <dispositivo>per questi motivi                             &#13;
                         LA CORTE COSTITUZIONALE                          &#13;
     dichiara  inammissibile  il  ricorso  per conflitto di attribuzione  &#13;
 promosso dalla Giunta regionale della Valle d'Aosta contro il Consiglio  &#13;
 regionale della medesima Valle, depositato in cancelleria il 30  maggio  &#13;
 1966 e contrassegnato col n. 14 del Registro ricorsi dell'anno 1966.     &#13;
     Così  deciso  in  Roma,  in  camera di consiglio, nella sede della  &#13;
 Corte costituzionale, Palazzo della Consulta, il 1 febbraio 1967.        &#13;
                                   GASPARE AMBROSINI - ANTONINO  PAPALDO  &#13;
                                   -  NICOLA JAEGER - GIOVANNI CASSANDRO  &#13;
                                   - BIAGIO PETROCELLI - ANTONIO MANCA -  &#13;
                                   ALDO SANDULLI  -  GIUSEPPE  BRANCA  -  &#13;
                                   MICHELE  FRAGALI - COSTANTINO MORTATI  &#13;
                                   -  GIUSEPPE  CHIARELLI   -   GIOVANNI  &#13;
                                   BATTISTA  BENEDETTI - FRANCESCO PAOLO  &#13;
                                   BONIFACIO - LUIGI OGGIONI.</dispositivo>
  </pronuncia_testo>
</pronuncia>
