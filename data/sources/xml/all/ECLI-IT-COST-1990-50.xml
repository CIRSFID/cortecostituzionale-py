<pronuncia>
  <pronuncia_testata>
    <anno_pronuncia>1990</anno_pronuncia>
    <numero_pronuncia>50</numero_pronuncia>
    <ecli>ECLI:IT:COST:1990:50</ecli>
    <tipologia_pronuncia>S</tipologia_pronuncia>
    <presidente>SAJA</presidente>
    <relatore_pronuncia>Giuseppe Borzellino</relatore_pronuncia>
    <redattore_pronuncia>Giovanni Conso</redattore_pronuncia>
    <data_decisione>31/01/1990</data_decisione>
    <data_deposito>02/02/1990</data_deposito>
  </pronuncia_testata>
  <pronuncia_testo>
    <collegio><titolo>LA CORTE COSTITUZIONALE</titolo>composta dai signori: &#13;
 Presidente: dott. Francesco SAJA; &#13;
 Giudici: prof. Giovanni CONSO, prof. Ettore GALLO, dott. Aldo &#13;
 CORASANITI, prof. Giuseppe BORZELLINO, dott. Francesco GRECO, prof. &#13;
 Renato DELL'ANDRO, prof. Gabriele PESCATORE, avv. Ugo SPAGNOLI, prof. &#13;
 Francesco Paolo CASAVOLA, prof. Antonio BALDASSARRE, prof. Vincenzo &#13;
 CAIANIELLO, avv. Mauro FERRI, prof. Luigi MENGONI, prof. Enzo CHELI.</collegio>
    <epigrafe>ha pronunciato la seguente<titolo>SENTENZA</titolo>nei  giudizi di legittimità costituzionale dell'art. 5 della legge 2    &#13;
 aprile  1968,  n.   482   (Disciplina   generale   delle   assunzioni    &#13;
 obbligatorie   presso  le  pubbliche  amministrazioni  e  le  aziende    &#13;
 private), in relazione agli artt.  1,  2  e  3  della  stessa  legge,    &#13;
 promossi  con  ordinanze  emesse il 22 settembre e il 19 ottobre 1988    &#13;
 dal Tribunale di Milano, il 16 e il 24 febbraio 1989 dal  Pretore  di    &#13;
 Bologna  e  il  1°  marzo  1989  (nn.  2  ordinanze)  della  Corte di    &#13;
 cassazione, iscritte rispettivamente ai nn. 236, 237, 313, 314, 484 e    &#13;
 485 del registro ordinanze 1989 e pubblicate nella Gazzetta Ufficiale    &#13;
 della Repubblica ai nn. 20, 21 e 43, prima serie speciale,  dell'anno    &#13;
 1989.                                                                    &#13;
    Visti  gli  atti  di costituzione della Ditta Marzocchi S.p.A., di    &#13;
 Ricchi  Alessandro,  della  S.r.l.  Weber  e  della  S.p.A.  Società    &#13;
 Generale  Supermercati, nonché gli atti di intervento del Presidente    &#13;
 del Consiglio dei ministri;                                              &#13;
    Udito  nell'udienza  pubblica  del  12  dicembre  1989  il Giudice    &#13;
 relatore Giuseppe Borzellino;                                            &#13;
    Uditi  l'avv.to Franco Agostini per Ricchi Alessandro e l'Avvocato    &#13;
 dello Stato Sergio La Porta  per  il  Presidente  del  Consiglio  dei    &#13;
 ministri.</epigrafe>
    <testo><fatto>Ritenuto in fatto</fatto>Con  due  ordinanze  emesse  il  22 settembre e il 19 ottobre 1988    &#13;
 (R.O. n. 236  e  n.  237  del  1989)  il  Tribunale  di  Milano,  nei    &#13;
 procedimenti  civili  vertenti rispettivamente tra S.p.A. Rohm e Haas    &#13;
 Italia e Amodeo Massimo e tra S.r.l. Zenith  e  Broglio  Stefano,  ha    &#13;
 sollevato  questione  di  legittimità costituzionale, in riferimento    &#13;
 all'art. 3 della Costituzione, dell'art. 5 della legge 2 aprile  1968    &#13;
 n.  482  (Disciplina generale delle assunzioni obbligatorie presso le    &#13;
 pubbliche amministrazioni e le aziende private), nella parte  in  cui    &#13;
 esclude  gli invalidi civili psichici dalla propria tutela ove invece    &#13;
 "si tengano presenti gli artt. 1, 2, 3, della stessa legge e le norme    &#13;
 si leggano in connessione tra loro".                                     &#13;
    La  disparità di trattamento dunque, e senza giustificato motivo,    &#13;
 si verifica in quanto nessuna considerazione politica può  valere  a    &#13;
 fondare  razionalmente  l'esclusione  degli  invalidi civili psichici    &#13;
 dalla avviabilità al lavoro in presenza di  un  sistema  legislativo    &#13;
 che,  in  via generale, la ammette se trattasi di invalidi di guerra,    &#13;
 del lavoro o di servizio.                                                &#13;
    Con altre due ordinanze emesse in data 16 e 24 febbraio 1989 (R.O.    &#13;
 n. 313 e n. 314) il  Pretore  di  Bologna,  nei  procedimenti  civili    &#13;
 vertenti  rispettivamente tra Naldi Pietro e Ditta Marzocchi S.p.a. e    &#13;
 tra Ricchi Alessandro e Ditta Weber  s.r.l.,  ha  sollevato  identica    &#13;
 questione, in riferimento anche agli artt. 4 e 35 della Costituzione.    &#13;
    Con  due ordinanze emesse entrambe il 1° marzo 1989 (R.O. n. 484 e    &#13;
 n. 485) la Corte di  Cassazione,  nei  procedimenti  civili  vertenti    &#13;
 rispettivamente   tra   Pirro   Vittorio   e   S.p.A.  Soc.  Generale    &#13;
 Supermercati e S.p.A. Borghi Trasporti Spedizioni e  Paparella  Vito,    &#13;
 ha  sollevato  questione  di  legittimità del già citato art. 5, in    &#13;
 riferimento agli artt. 3, 4, 35 e 38 della Costituzione, nella  parte    &#13;
 in  cui,  ravvisandosi invalidi civili, agli effetti della disciplina    &#13;
 sulle assunzioni obbligatorie, soltanto coloro che  sono  affetti  da    &#13;
 minorazione  fisica,  esclude  dall'ambito della sua applicazione gli    &#13;
 invalidi affetti da minorazione di natura  psichica,  pur  prevedendo    &#13;
 (anche  alla  stregua  delle  leggi speciali che disciplinano diverse    &#13;
 categorie di invalidi) l'avviamento obbligatorio di invalidi, affetti    &#13;
 dalla  stessa  malattia psichica, ma appartenenti a categorie diverse    &#13;
 (invalidi di guerra, per lavoro o per servizio).                         &#13;
    Richiamate  le  argomentazioni  di cui a precedenti sentenze della    &#13;
 Corte costituzionale, le  ordinanze  della  Cassazione  rilevano  che    &#13;
 "s'impone  la  necessità  di una tempestiva pronuncia risolutiva che    &#13;
 ponga rimedio al "vuoto" legislativo riscontrato, non ancora  colmato    &#13;
 dall'intervento del legislatore, ai fini della predisposizione di una    &#13;
 appropriata normativa che disciplini, in modo organico ed articolato,    &#13;
 l'avviamento obbligatorio anche degli invalidi affetti da minorazione    &#13;
 psichica, onde consentire a costoro un proficuo inserimento nel mondo    &#13;
 del  lavoro,  osservate  certe cautele ed in ambienti particolarmente    &#13;
 protetti nell'esercizio  di  mansioni  comunque  compatibili  con  la    &#13;
 natura e con il grado della loro minorazione".                           &#13;
    Nel  procedimento  di  cui all'ordinanza n. 313 si sono costituiti    &#13;
 per la Ditta Marzocchi S.p.a. gli avvocati Giuseppe Camorani Scarpa e    &#13;
 Dante  Fedeli chiedendo che la questione sia dichiarata inammissibile    &#13;
 perché già decisa, ovvero non fondata nel merito.                      &#13;
    Nel  procedimento di cui all'ordinanza n. 314 si è costituito per    &#13;
 il sig. Ricchi l'avv.  Franco  Agostini,  concludendo  che  la  norma    &#13;
 impugnata  venga dichiarata illegittima, mentre per la società Weber    &#13;
 si è  costituito  l'avv.  Mattia  Persiani,  con  richiesta  di  una    &#13;
 declaratoria di infondatezza.                                            &#13;
    Nel  procedimento  di  cui  all'ordinanza  n. 484 si è costituito    &#13;
 l'avv. Giancarlo Pezzano sostenendo che  la  presunta  disparità  di    &#13;
 trattamento  fra diverse categorie di invalidi psichici non esiste in    &#13;
 quanto il collocamento obbligatorio è escluso per tutti i  portatori    &#13;
 di  handicap  psichico,  ancorché appartenenti alle categorie di cui    &#13;
 agli artt. 2, 3 e 4 della legge n. 482 del 1968. Chiede pertanto  che    &#13;
 venga dichiarata infondata la questione.                                 &#13;
    In  tutti i giudizi è intervenuto il Presidente del Consiglio dei    &#13;
 ministri rappresentato e difeso dall'Avvocatura generale dello Stato,    &#13;
 chiedendo una declaratoria di inammissibilità ovvero, nel merito, di    &#13;
 infondatezza.<diritto>Considerato in diritto</diritto>1.  -  Le  ordinanze  sollevano identica questione di legittimità    &#13;
 costituzionale; i relativi giudizi vanno riuniti, pertanto,  ai  fini    &#13;
 di un'unica pronuncia.                                                   &#13;
    2.  -  L'art.  5  della  legge  2  aprile 1968, n. 482 (Disciplina    &#13;
 generale  delle   assunzioni   obbligatorie   presso   le   pubbliche    &#13;
 amministrazioni  e le aziende private), considera invalidi civili, ai    &#13;
 fini  delle  relative  provvidenze,  "coloro  che  siano  affetti  da    &#13;
 minorazioni fisiche", restando così esclusi dai benefici gli affetti    &#13;
 da minorazioni di natura psichica.                                       &#13;
    Tale  esclusione  dall'ambito  della  normativa è contrastata dai    &#13;
 giudici a quibus per assunto contrasto con gli artt. 3, 4,  35  e  38    &#13;
 della Costituzione.                                                      &#13;
    3.1. - La questione è fondata.                                       &#13;
    Occorre   premettere   che   la   Corte,  chiamata  in  passato  a    &#13;
 pronunciarsi in materia, ebbe già a riconoscere -  le  ordinanze  di    &#13;
 rimessione  lo  ricordano  -  come non siano ammissibili esclusioni e    &#13;
 limitazioni confliggenti con l'art. 3 Cost., poiché determinano  una    &#13;
 posizione  deteriore  nei  confronti  degli  affetti  da  minorazione    &#13;
 psichica rispetto ai colpiti  da  invalidità  fisica.  Tuttavia,  fu    &#13;
 considerato che le valutazioni che ne conseguono avrebbero richiesto,    &#13;
 per  la  varietà  degli  insorgenti  casi  concreti,  una  serie  di    &#13;
 previsioni  articolate:  talché  a  una  siffatta normativa organica    &#13;
 avrebbe dovuto provvedersi a cura  del  legislatore.  Alla  specifica    &#13;
 attenzione  del Parlamento veniva sottoposta pertanto e ripetutamente    &#13;
 l'urgenza dell'approntamento, nei  descritti  sensi,  di  una  idonea    &#13;
 compiuta disciplina.                                                     &#13;
    In prosieguo, la Corte ebbe ancora a confermare le esigenze di cui    &#13;
 trattasi, con esplicita avvertenza che gli eminenti valori  in  gioco    &#13;
 non  avrebbero  potuto ulteriormente esimerla da una rigorosa diretta    &#13;
 applicazione dei precetti costituzionali (sentenze n. 52 del  1985  e    &#13;
 n. 1088 del 1988; ord. n. 951 del 1988).                                 &#13;
    3.2.  - Allo stato, è da confermarsi il pressante invito a che il    &#13;
 Parlamento possa sollecitamente apprestare una completa normativa  in    &#13;
 tema  di  avviamento  al  lavoro  dei  soggetti  invalidi, per la cui    &#13;
 revisione risultano già  prodotte  alle  Camere  svariate  proposte;    &#13;
 tuttavia,  la  Corte  non  può  ulteriormente  indugiare  in  quegli    &#13;
 improcrastinabili interventi, atti - coerentemente alle sue pregresse    &#13;
 affermazioni - ad assicurare nell'area, con immediatezza, il rispetto    &#13;
 dei precetti e delle relative garanzie costituzionali.                   &#13;
    Si   è   già   rilevato,   infatti,   come   sul  piano  proprio    &#13;
 costituzionale, oltre che su  quello  morale,  non  sono  ammissibili    &#13;
 esclusioni e limitazioni volte a relegare in situazioni di isolamento    &#13;
 e di assurda discriminazione soggetti che,  particolarmente  colpiti,    &#13;
 hanno  all'incontro  pieno diritto di inserirsi capacemente nel mondo    &#13;
 del lavoro, spettando alla Repubblica l'impegno  di  promuovere  ogni    &#13;
 prevedibile    condizione   organizzativa   per   rendere   effettivo    &#13;
 l'esercizio di un tale diritto.                                          &#13;
    4.  -  Quanto  premesso  impone, adunque, di fissare nei descritti    &#13;
 sensi  il  dato  normativo  in  esame  (art.   5   legge   n.   482).    &#13;
 Conseguentemente,   sarà   compito   specifico   degli  accertamenti    &#13;
 (previsti dal successivo art. 20) determinare in  concreto,  in  base    &#13;
 cioè  al grado di percezione dei dati di realtà, l'idoneità o meno    &#13;
 a proficuo impiego dei soggetti incisi  da  distorsioni  della  sfera    &#13;
 psichica: le condizioni, cioè, di compatibilità delle mansioni, nel    &#13;
 reciproco intreccio dei  fattori  soggettivi  e  oggettivi  che  allo    &#13;
 svolgimento di una specifica attività lavorativa ineriscono. In tali    &#13;
 termini va perciò modificato l'art. 20, prescrivendo,  altresì,  la    &#13;
 integrazione   del   Collegio   sanitario,   ivi  previsto,  con  uno    &#13;
 specialista nelle discipline neurologiche o psichiatriche.</testo>
    <dispositivo>per questi motivi                              &#13;
                        LA CORTE COSTITUZIONALE                           &#13;
   Riuniti   i   giudizi;   dichiara  l'illegittimità  costituzionale    &#13;
 dell'art. 5 della legge 2 aprile 1968, n.  482  (Disciplina  generale    &#13;
 delle  assunzioni  obbligatorie presso le pubbliche amministrazioni e    &#13;
 le aziende private) nella parte in cui non considera, ai  fini  della    &#13;
 legge  stessa,  invalidi  civili  anche  gli  affetti  da minorazione    &#13;
 psichica, i quali abbiano una capacità lavorativa che ne consente il    &#13;
 proficuo impiego in mansioni compatibili;                                &#13;
    Dichiara  d'ufficio,  ai  sensi  dell'art. 27 della legge 11 marzo    &#13;
 1953, n. 87, l'illegittimità costituzionale dell'art. 20 della legge    &#13;
 2   aprile   1968,  n.  482  (Disciplina  generale  delle  assunzioni    &#13;
 obbligatorie  presso  le  pubbliche  Amministrazioni  e  le   aziende    &#13;
 private)  nella  parte  in cui in ordine agli accertamenti medici non    &#13;
 prevede anche i minorati psichici,  agli  effetti  della  valutazione    &#13;
 concreta di compatibilità dello stato del soggetto con le mansioni a    &#13;
 lui affidate all'atto dell'assunzione o successivamente, da  disporsi    &#13;
 a  cura  del  Collegio  sanitario  ivi  previsto  ed integrato con un    &#13;
 componente specialista nelle discipline neurologiche o psichiatriche.    &#13;
    Così  deciso  in  Roma,  nella  sede  della Corte costituzionale,    &#13;
 Palazzo della Consulta, il 31 gennaio 1990.                              &#13;
                          Il Presidente: SAJA                             &#13;
                          Il redattore: CONSO                             &#13;
                        Il cancelliere: MINELLI                           &#13;
    Depositata in cancelleria il 2 febbraio 1990.                         &#13;
                Il direttore della cancelleria: MINELLI</dispositivo>
  </pronuncia_testo>
</pronuncia>
